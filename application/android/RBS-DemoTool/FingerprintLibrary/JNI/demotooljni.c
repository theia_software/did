#include <android/log.h>
#include <jni.h>
#include <stdio.h>
#include <string.h>

#include "egis_rbs_api.h"
#include "type_definition.h"
#include "common_definition.h"

#define TAG "RBS-jni"
#define LOGD(...) __android_log_print(ANDROID_LOG_DEBUG, TAG, __VA_ARGS__)
#define LOGE(...) __android_log_print(ANDROID_LOG_ERROR, TAG, __VA_ARGS__)

JavaVM* gJavaVM;
jclass gClass = NULL;
int gEnvVersion;
static jmethodID gNativeCallback = NULL;
operation_callback_t FDoNativeCallback;

extern app_instance_type_t g_app_type;
JNIEXPORT jint JNICALL JNI_OnLoad(JavaVM* vm, void* reserved) {
    JNIEnv* env;
    gJavaVM = vm;
    if ((*vm)->GetEnv(vm, (void**)&env, JNI_VERSION_1_6) != JNI_OK) {
        return -1;
    }
    g_app_type = APP_IS_USING_JNI;
    return JNI_VERSION_1_6;
}

void DoNativeCallBack(int event_id, int first_param, int second_param, unsigned char* data,
                      int data_size) {
    LOGD(
        "DoNativeCallBack enter! event_id = %d, first_param = %d, second_param = %d, data_size = "
        "%d",
        event_id, first_param, second_param, data_size);
    jbyteArray jBuffer;
    // Init
    JNIEnv* env;
    jclass cls = NULL;
    int nIsAttached = FALSE;

    // Get current thread
    int nStatus = (*gJavaVM)->GetEnv(gJavaVM, (void**)&env, gEnvVersion);
    if (nStatus < 0) {
        nStatus = (*gJavaVM)->AttachCurrentThread(gJavaVM, &env, NULL);
        nIsAttached = TRUE;
    }

    if (data_size > 0) {
        // LOGD("OnGetFeature btBuffer = %x", *btBuffer);
        jBuffer = (*env)->NewByteArray(env, data_size);
        (*env)->SetByteArrayRegion(env, jBuffer, 0, data_size, (jbyte*)data);
    } else {
        jBuffer = NULL;
    }

    if (gNativeCallback != 0) {
        (*env)->CallStaticVoidMethod(env, gClass, gNativeCallback, event_id, first_param,
                                     second_param, jBuffer);
    } else {
        LOGE("DoNativeCallBack callback not exist");
    }

    // Update data from Java side
    if (data_size > 0) {
        jbyte* byte_array = (*env)->GetByteArrayElements(env, jBuffer, 0);
        memcpy(data, byte_array, data_size);
        (*env)->ReleaseByteArrayElements(env, jBuffer, byte_array, 0);
    }

    (*env)->DeleteLocalRef(env, jBuffer);

    // DetachCurrentThread
    if (nIsAttached == TRUE) {
        (*gJavaVM)->DetachCurrentThread(gJavaVM);
    }
    // LOGD("DoNativeCallBack end");
}

JNIEXPORT jint JNICALL Java_rbs_egistec_com_fplib_api_RbsLib_SetDataPath(JNIEnv* env, jobject obj,
                                                                         jint type,
                                                                         jstring data_path) {
    LOGD("Java_rbs_egistec_com_fplib_api_RbsLib_SetDataPath enter!");
    int ret = -1;

    unsigned char buf[128];
    const char* str = (*env)->GetStringUTFChars(env, data_path, NULL);
    if (str == NULL) {
        LOGD("Java_rbs_egistec_com_fplib_api_RbsLib_SetDataPath str == NULL");
        return -1;
    }

    ret = rbs_set_data_path(type, str, strlen(str));

    (*env)->ReleaseStringUTFChars(env, data_path, str);
    LOGD("Java_rbs_egistec_com_fplib_api_RbsLib_SetDataPath end! ret = %d", ret);
    return ret;
}

JNIEXPORT jint JNICALL Java_rbs_egistec_com_fplib_api_RbsLib_SetActiveUser(JNIEnv* env, jobject obj,
                                                                           jint user_id,
                                                                           jstring data_path) {
    LOGD("Java_rbs_egistec_com_fplib_api_RbsLib_SetActiveUser enter!");
    int ret = -1;

    unsigned char buf[128];
    const char* str = (*env)->GetStringUTFChars(env, data_path, NULL);
    if (str == NULL) {
        return -1;
    }

    ret = rbs_active_user_group(user_id, str);

    (*env)->ReleaseStringUTFChars(env, data_path, str);
    LOGD("Java_rbs_egistec_com_fplib_api_RbsLib_SetActiveUser end! ret = %d", ret);
    return ret;
}

JNIEXPORT jint JNICALL Java_rbs_egistec_com_fplib_api_RbsLib_Initialize(JNIEnv* env, jobject obj) {
    LOGD("Java_rbs_egistec_com_fplib_api_RbsLib_Initialize enter!");
    int ret = -1;

    ret = rbs_initialize(NULL, 0);
    LOGD("Java_rbs_egistec_com_fplib_api_RbsLib_Initialize end! ret = %d", ret);
    return ret;
}

JNIEXPORT jint JNICALL Java_rbs_egistec_com_fplib_api_RbsLib_Uninitialize(JNIEnv* env,
                                                                          jobject obj) {
    LOGD("Java_rbs_egistec_com_fplib_api_RbsLib_Uninitialize enter!");
    int ret = -1;
    ret = rbs_uninitialize();
    LOGD("Java_rbs_egistec_com_fplib_api_RbsLib_Uninitialize end! ret = %d", ret);
    return ret;
}

JNIEXPORT jint JNICALL Java_rbs_egistec_com_fplib_api_RbsLib_Abort(JNIEnv* env, jobject obj) {
    LOGD("Java_rbs_egistec_com_fplib_api_RbsLib_Abort enter!");
    int ret = -1;
    ret = rbs_cancel();
    LOGD("Java_rbs_egistec_com_fplib_api_RbsLib_Abort end! ret = %d", ret);
    return ret;
}

JNIEXPORT jint JNICALL Java_rbs_egistec_com_fplib_api_RbsLib_Delete(JNIEnv* env, jobject obj,
                                                                    jint user_id, jint finger_id) {
    LOGD("Java_rbs_egistec_com_fplib_api_RbsLib_Delete enter!");
    int ret = -1;
    ret = rbs_remove_fingerprint(user_id, finger_id);
    LOGD("Java_rbs_egistec_com_fplib_api_RbsLib_Delete end! ret = %d", ret);
    return ret;
}

JNIEXPORT jint JNICALL Java_rbs_egistec_com_fplib_api_RbsLib_GetFingerList(JNIEnv* env, jobject obj,
                                                                           jint user_id,
                                                                           jintArray finger_list,
                                                                           jintArray finger_count) {
    LOGD("Java_rbs_egistec_com_fplib_api_RbsLib_GetFingerList enter!");
    int ret, length, ids_count = SUPPORT_MAX_ENROLL_COUNT;
    int i = 0;
    int finger_ids[SUPPORT_MAX_ENROLL_COUNT] = {0};

    ret = rbs_get_fingerprint_ids(user_id, (int*)finger_ids, &ids_count);

    length = (*env)->GetArrayLength(env, finger_list);
    jint* jfinger_list = (*env)->GetIntArrayElements(env, finger_list, 0);

    LOGD(
        "Java_rbs_egistec_com_fplib_api_RbsLib_GetFingerList! ret = %d, length = %d, ids_count = "
        "%d",
        ret, length, ids_count);
    if (length < ids_count) {
        LOGD("Java_rbs_egistec_com_fplib_api_RbsLib_GetFingerList length < ids_count");
        return -1;
    } else {
        for (i = 0; i < ids_count; i++) {
            jfinger_list[i] = finger_ids[i];
        }
    }

    jint* jfinger_count = (*env)->GetIntArrayElements(env, finger_count, 0);
    jfinger_count[0] = ids_count;
    (*env)->ReleaseIntArrayElements(env, finger_list, jfinger_list, 0);
    (*env)->ReleaseIntArrayElements(env, finger_count, jfinger_count, 0);
    LOGD("Java_rbs_egistec_com_fplib_api_RbsLib_GetFingerList end! ret = %d", ret);
    return ret;
}

JNIEXPORT jint JNICALL Java_rbs_egistec_com_fplib_api_RbsLib_EnrollFinger(JNIEnv* env, jobject obj,
                                                                          jint user_id,
                                                                          jint fingerprint_id) {
    LOGD("Java_rbs_egistec_com_fplib_api_RbsLib_EnrollFinger enter!");

    int ret = rbs_pre_enroll(user_id, fingerprint_id);

    if (ret == 0) {
        ret = rbs_enroll();
    }

    LOGD("Java_rbs_egistec_com_fplib_api_RbsLib_EnrollFinger end! ret = %d", ret);
    return ret;
}

JNIEXPORT void JNICALL Java_rbs_egistec_com_fplib_api_RbsLib_SetNativeCallback(JNIEnv* env,
                                                                               jobject obj) {
    LOGD("Java_rbs_egistec_com_fplib_api_RbsLib_SetNativeCallback enter");

    jclass cls = (*env)->GetObjectClass(env, obj);
    gClass = (jclass)(*env)->NewGlobalRef(env, cls);
    gEnvVersion = (*env)->GetVersion(env);
    gNativeCallback = (*env)->GetStaticMethodID(env, cls, "NativeCallback", "(III[B)V");
    if (gNativeCallback == 0) {
        LOGE("nativeInitialize! gNativeCallback is null");
        return;
    }

    FDoNativeCallback = DoNativeCallBack;
    rbs_set_on_callback_proc(FDoNativeCallback);
    LOGD("Java_rbs_egistec_com_fplib_api_RbsLib_SetNativeCallback end!");
}

JNIEXPORT jint JNICALL Java_rbs_egistec_com_fplib_api_RbsLib_Verify(JNIEnv* env, jobject obj,
                                                                    jint user_id,
                                                                    jintArray fingerprint_ids,
                                                                    jint fingerprint_count,
                                                                    jlong wait_finger_off) {
    LOGD("Java_rbs_egistec_com_fplib_api_RbsLib_Verify enter!");

    jint* jfingerprint_ids = (*env)->GetIntArrayElements(env, fingerprint_ids, 0);

    jsize length = (*env)->GetArrayLength(env, fingerprint_ids);

    if (jfingerprint_ids == NULL) {
        LOGE("jfingerprint_ids = NULL");
        return -1;
    }

    int ret = rbs_authenticator(user_id, (unsigned int*)jfingerprint_ids, fingerprint_count,
                                wait_finger_off);

    (*env)->ReleaseIntArrayElements(env, fingerprint_ids, jfingerprint_ids, 0);
    LOGD("Java_rbs_egistec_com_fplib_api_RbsLib_Verify end! ret = %d", ret);
    return ret;
}

JNIEXPORT jint JNICALL Java_rbs_egistec_com_fplib_api_RbsLib_extra_1api(
    JNIEnv* env, jobject obj, jint pid, jbyteArray in_buffer, jint in_buffer_size,
    jbyteArray out_buffer, jintArray out_buffer_size) {
    LOGD("Java_rbs_egistec_com_fplib_api_RbsLib_extra_api enter! %d", in_buffer_size);

    jbyte* jin_buffer = (*env)->GetByteArrayElements(env, in_buffer, 0);
    int ret;
    if (jin_buffer == NULL) {
        LOGE("jin_buffer = NULL");
        return -1;
    }
    if (out_buffer != NULL) {
        jbyte* jout_buffer = (*env)->GetByteArrayElements(env, out_buffer, 0);
        jint* jout_buffer_size = (*env)->GetIntArrayElements(env, out_buffer_size, 0);
        ret = rbs_extra_api(pid, (unsigned char*)jin_buffer, in_buffer_size,
                            (unsigned char*)jout_buffer, jout_buffer_size);
        (*env)->ReleaseByteArrayElements(env, out_buffer, jout_buffer, 0);
        (*env)->ReleaseIntArrayElements(env, out_buffer_size, jout_buffer_size, 0);
    } else {
        ret = rbs_extra_api(pid, (unsigned char*)jin_buffer, in_buffer_size, NULL, NULL);
    }

    (*env)->ReleaseByteArrayElements(env, in_buffer, jin_buffer, 0);

    LOGD("Java_rbs_egistec_com_fplib_api_RbsLib_extra_api end! ret = %d", ret);
    return ret;
}
