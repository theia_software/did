
LOCAL_PATH := $(call my-dir)
GIT_PROJECT_RELATIVE := ../../../../..

JNI_ENABLE=true

include $(CLEAR_VARS)
LOCAL_MODULE	:= demotooljni
LOCAL_STATIC_LIBRARIES := libRbsFlow libegis_fp_core
LOCAL_CFLAGS = -D__LINUX__

LOCAL_C_INCLUDES := $(LOCAL_PATH)/../../../../../sdk/source/flow
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../../sdk/source/common/platform/inc

LOCAL_SRC_FILES := ./demotooljni.c


# 5. In charge of collecting all the information you defined in local
# variables and determine what to build.

LOCAL_LDLIBS :=-llog

LOCAL_PROGUARD_ENABLED:= disabled

include $(BUILD_SHARED_LIBRARY)

include $(LOCAL_PATH)/$(GIT_PROJECT_RELATIVE)/sdk/source/build/Android_flow.mk