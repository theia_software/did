package egistec.optical.demotool.lib;

import android.view.View;

import egistec.optical.demotool.application.DemoToolApp;
import rbs.egistec.com.fplib.api.EgisLog;

public class UIUtils {
    private final static String TAG = UIUtils.class.getSimpleName();
    public static void hideSystemUI(View decorView) {
        EgisLog.d(TAG, "hideSystemUI"+decorView);
        if(decorView == null) {
            return;
        }
        // Enables regular immersive mode.
        // For "lean back" mode, remove SYSTEM_UI_FLAG_IMMERSIVE.
        // Or for "sticky immersive," replace it with SYSTEM_UI_FLAG_IMMERSIVE_STICKY
        decorView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_IMMERSIVE
                        // Set the content to appear under the system bars so that the
                        // content doesn't resize when the system bars hide and show.
                        | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                        // Hide the nav bar and status bar
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN);
    }


    public static void showSystemUI(View decorView) {
        EgisLog.d(TAG, "showSystemUI"+ decorView);
        if(decorView == null) {
            return;
        }
        decorView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        /*| View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION*/
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
    }

}
