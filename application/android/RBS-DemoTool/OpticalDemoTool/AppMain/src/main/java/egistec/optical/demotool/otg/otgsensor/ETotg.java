package egistec.optical.demotool.otg.otgsensor;

import android.util.Log;

import egistec.optical.demotool.otg.utility.USBWrapper;

public class ETotg extends SensorBase {
    private static final int OK = 0;
    private static final int ERROR_COMMAND_FAIL = 1;
    private static final String TAG = "ETotg";
    private static USBWrapper mUSBWrapper;

    private static int NativeCallback(int eventId, int value1, int value2, byte[] byteBuffer) {
        int ret = 0;
        switch (eventId) {
            case SENSOR_CONTROL_WRITE_REGISTER: {
                byte address = byteBuffer[0];
                byte val = byteBuffer[1];
                ret = write_register(address, val);
            }
            break;

            case SENSOR_CONTROL_READ_REGISTER: {
                byte address = byteBuffer[0];
                byte[] val = new byte[1];
                ret = read_register(address, val);
                byteBuffer[0] = val[0];
            }
            break;

            case SENSOR_CONTROL_GET_IMAGE:
            case SENSOR_CONTROL_GET_FRAME: {
                ret = get_frame(byteBuffer);
            }
            break;

            case SENSOR_CONTROL_SPI_WRITE_READ:
                ret = spi_write_read(byteBuffer, value1, byteBuffer, value2);
                break;
            case SENSOR_CONTROL_SET_SPI_CLK:
                ret = set_spi_clk((byte)value1, (byte)value2);
                break;

        }
        return ret;
    }

    private static void init(USBWrapper serialDevice) {
        mUSBWrapper = serialDevice;
    }

    private static synchronized int write_register(short address, byte val) {
        if (mUSBWrapper.getOTGType() != USBWrapper.OTGType.MASS_STORAGE) {
            Log.e(TAG, "not MASS_STORAGE");
            return ERROR_COMMAND_FAIL;
        }
        return (mUSBWrapper.ioDispatchOtgSCSI_Write_Register(address, val) >= 0) ? OK : ERROR_COMMAND_FAIL;
    }

    private static synchronized int read_register(short address, byte[] val) {
        if (mUSBWrapper.getOTGType() != USBWrapper.OTGType.MASS_STORAGE) {
            Log.e(TAG, "not MASS_STORAGE");
            return ERROR_COMMAND_FAIL;
        }
        return (mUSBWrapper.ioDispatchOtgSCSI_Read_Register(address, val) >= 0) ? OK : ERROR_COMMAND_FAIL;
    }

    private static synchronized int get_frame(byte[] imageBuffer) {
        if (mUSBWrapper.getOTGType() != USBWrapper.OTGType.MASS_STORAGE) {
            Log.e(TAG, "not MASS_STORAGE");
            return ERROR_COMMAND_FAIL;
        }
        return (mUSBWrapper.ioDispatchOtgSCSI_Get_Frame(imageBuffer, 0, 0) >= 0) ? OK : ERROR_COMMAND_FAIL;
    }

    private static synchronized int set_spi_clk(byte mode, byte clk) {
        if (mUSBWrapper.getOTGType() != USBWrapper.OTGType.MASS_STORAGE) {
            Log.e(TAG, "not MASS_STORAGE");
            return ERROR_COMMAND_FAIL;
        }
        return (mUSBWrapper.ioDispatchOtgSCSI_Set_SPI_CLK(mode, clk) >= 0) ? OK : ERROR_COMMAND_FAIL;
    }

    private static synchronized int spi_write_read(byte[] writeBuf, int writeLen, byte[] readBuf, int readLen) {
        if (mUSBWrapper.getOTGType() != USBWrapper.OTGType.MASS_STORAGE) {
            Log.e(TAG, "not MASS_STORAGE");
            return ERROR_COMMAND_FAIL;
        }
        return (mUSBWrapper.ioDispatchOtgSCSI_SPI_Write_Read(writeBuf, writeLen, readBuf, readLen) >= 0) ? OK : ERROR_COMMAND_FAIL;
    }


    @Override
    public int initialize(USBWrapper serialDevice) {
        init(serialDevice);
        return OK;
    }

    @Override
    public int eventHandler(int eventId, int value1, int value2, byte[] byteBuffer) {
        return NativeCallback(eventId, value1, value2, byteBuffer);
    }
}