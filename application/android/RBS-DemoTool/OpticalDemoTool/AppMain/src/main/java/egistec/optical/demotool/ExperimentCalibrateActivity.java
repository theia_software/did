package egistec.optical.demotool;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
//import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import egistec.optical.demotool.experiment.ExperimentDef;
import egistec.optical.demotool.experiment.ExperimentSettingValueDB;
import egistec.optical.demotool.experiment.ExperimentUtils;
import egistec.optical.demotool.experiment.FileIoUtils;
import egistec.optical.demotool.otg.manager.OtgSensorManager;
import rbs.egistec.com.fplib.api.FingerprintReceiver;
import rbs.egistec.com.fplib.api.FpResDef;
import rbs.egistec.com.fplib.api.RbsLib;
import rbs.egistec.com.fplib.api.EgisLog;

import static egistec.optical.demotool.experiment.ExperimentDef.PID_BKG_IMG;
import static egistec.optical.demotool.otg.OtgDef.OTG_SENSOR_ID;

public class ExperimentCalibrateActivity extends AppCompatActivity {

    private final String TAG = "ExperimentCalibrateLog";
    private Context mContext = null;
    private Button mButtonStartCalibrate = null;
    private Handler mHandler = null;
    private TextView mTextViewProgress = null;
    private int mMaxScale = 0;
    private int mMinScale = 0;
    private int mCurrentExpTimeScale = 0;
    private ImageView mImageView = null;
    private RbsLib mRbsLib = null;
    private FingerprintReceiver mFingerprintReceiver = null;
    private OtgSensorManager mOtgSensorManager = null;
    private FileIoUtils.WindowPos icWindowPos = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_experiment_calibrate);
        mContext = this;
        mOtgSensorManager = OtgSensorManager.getInstance(OTG_SENSOR_ID, mContext);
        mRbsLib = RbsLib.getInstance(this);
        mHandler = initializeHandler();
        mFingerprintReceiver = initializeFingerprintReceiver();
        initializeLayout();
        ExperimentUtils.changeAppBrightness(mContext, ExperimentSettingValueDB.getBrightness(mContext));
    }
    @Override
    protected void onResume() {
        super.onResume();
        ExperimentUtils.initializeRbsLib(mRbsLib, mFingerprintReceiver);
    }
    @Override
    protected void onPause() {
        super.onPause();
        ExperimentUtils.uninitializeRbsLib(mRbsLib);
    }

    private void initializeLayout() {
        mTextViewProgress = (TextView)findViewById(R.id.text_view_progress);
        mMaxScale = ExperimentSettingValueDB.getExposureTimeScaleMax(mContext);
        mMinScale = ExperimentSettingValueDB.getExposureTimeScaleMin(mContext);
        mButtonStartCalibrate = (Button)findViewById(R.id.button_experiment_calibrate);
        mButtonStartCalibrate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mButtonStartCalibrate.setEnabled(false);
                mTextViewProgress.setText(ExperimentUtils.updateProgressMessage(0, ExperimentDef.processTypeCalibration));
                //calibrate processes
                Thread thread = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        runCalibrationProcesses();
                    }
                });
                thread.start();
            }
        });

        icWindowPos = FileIoUtils.loadIcWindowLayoutData();
        if (icWindowPos != null) {
            ExperimentSettingValueDB.setPressPosX(mContext, icWindowPos.x);
            ExperimentSettingValueDB.setPressPosY(mContext, icWindowPos.y);
        }
        RelativeLayout gifContainer = (RelativeLayout)findViewById(R.id.gif_container);
        Button mainVerifyTouch = (Button)findViewById(R.id.IC_window_main_verify);

        ExperimentUtils.setPressAreaLeftTop(gifContainer, ExperimentSettingValueDB.getPressPosX(mContext), ExperimentSettingValueDB.getPressPosY(mContext), getResources().getDisplayMetrics());
        ExperimentUtils.setPressAreaWidthHeight(gifContainer, mainVerifyTouch, 80, 120, getResources().getDisplayMetrics());

        TextView textViewExpTime = (TextView)findViewById(R.id.text_view_exp_time);
        textViewExpTime.setText(ExperimentUtils.getAllExpTimeString(mContext, textViewExpTime));

        TextView textViewOthers = (TextView)findViewById(R.id.text_view_others);
        textViewOthers.setText(ExperimentUtils.getOtherSettingString(mContext));
        setPressColor();
    }

    private void runCalibrationProcesses() {
        FileIoUtils.cleanDirectory(ExperimentDef.expBkgFileFolder);
        FileIoUtils.createDirectory(ExperimentDef.expBkgFileFolder);
        for (int i = mMinScale; i <= mMaxScale; i++) {
            mCurrentExpTimeScale = i;
            doCalibration(i);
            int progress = i == mMaxScale ? 100 : (Math.abs(i - mMinScale) + 1) * (100/(Math.abs(mMaxScale - mMinScale) + 1));
            mHandler.obtainMessage(ExperimentDef.MESSAGE_PROCESS_UPDATE_PROGRESS, progress, -1).sendToTarget();
            EgisLog.d(TAG, "Calibration progress = " + progress + "%");
        }
        mHandler.obtainMessage(ExperimentDef.MESSAGE_PROCESS_END).sendToTarget();
    }

    private void doCalibration(int expTimeScale) {
        ExperimentUtils.initializeRbsSettings(mRbsLib, ExperimentDef.expTimeBase * expTimeScale, ExperimentSettingValueDB.getIntegrationCount(mContext));
        ExperimentUtils.waitForTimePeriod(500);
        // Call calibrate rbs api, and set save path
        mRbsLib.extraApi(PID_BKG_IMG, ExperimentDef.CMD_BKG_IMG_RUN_CALI_PROC, null, null, null);
        String saveBinName = ExperimentUtils.getSaveExpImgName(mContext, mCurrentExpTimeScale, true, true, ".bin");
        mRbsLib.extraApi(PID_BKG_IMG, ExperimentDef.CMD_BKG_IMG_SAVE_CALI_DATA, (ExperimentDef.expBkgBinFileFolder + saveBinName).getBytes(), null, null);
    }

    private Handler initializeHandler() {
        return new Handler() {
            @Override
            public void handleMessage(Message msg) {
                switch (msg.what) {
                    case ExperimentDef.MESSAGE_PROCESS_START:
                        break;
                    case ExperimentDef.MESSAGE_PROCESS_END:
                        mTextViewProgress.setText(ExperimentUtils.updateProgressMessage(100, ExperimentDef.processTypeCalibration));
                        mButtonStartCalibrate.setEnabled(true);
                        break;
                    case ExperimentDef.MESSAGE_PROCESS_UPDATE_PROGRESS:
                        int progress = msg.arg1;
                        mTextViewProgress.setText(ExperimentUtils.updateProgressMessage(progress, ExperimentDef.processTypeCalibration));
                        break;
                    default:
                        break;
                }
            }
        };
    }

    private void setPressColor() {
        int viewId = ExperimentUtils.getPressViewId(ExperimentSettingValueDB.getPressColorId(mContext));
        if (mImageView != null)
            mImageView.setVisibility(View.INVISIBLE);
        mImageView = (ImageView)findViewById(viewId);
        mImageView.setVisibility(View.VISIBLE);
    }

    private FingerprintReceiver initializeFingerprintReceiver() {
        return new FingerprintReceiver() {
            @Override
            public void onFingerprintEvent(int eventId, int value1, int value2, Object eventObj) {
                switch (eventId) {
                    case FpResDef.EVENT_RETURN_CALIBRATE_IMAGE:
                        EgisLog.d(TAG, "+++++ RETURN_CALIBRATE_IMAGE +++++");
                        if (eventObj == null) {
                            EgisLog.e(TAG, "eventObj is null");
                            return;
                        } else {
                            int width = value1;
                            int height = value2;
                            int imageSize = width * height;
                            byte[] img = new byte[imageSize * 2];
                            short[] img16Bit = new short[imageSize];
                            EgisLog.d(TAG, "width " + width + " height " + height);
                            System.arraycopy(eventObj, 0, img, 0, imageSize * 2);
                            ByteBuffer.wrap(img).order(ByteOrder.LITTLE_ENDIAN).asShortBuffer().get(img16Bit);
                            String saveBinName = ExperimentUtils.getSaveExpImgName(mContext, mCurrentExpTimeScale, true, true, ".bin");
                            String saveCsvName = ExperimentUtils.getSaveExpImgName(mContext, mCurrentExpTimeScale, true, true, ".csv");
                            FileIoUtils.saveImage16Bits(ExperimentDef.expBkgFileFolder + saveBinName, img16Bit);
                            FileIoUtils.saveShortToCsvFile(ExperimentDef.expBkgFileFolder + saveCsvName, img16Bit, width);
                        }
                        break;
                    case FpResDef.EVENT_OTG_WRITE_REGISTER:
                    case FpResDef.EVENT_OTG_READ_REGISTER:
                    case FpResDef.EVENT_OTG_GET_FRAME:
                    case FpResDef.EVENT_OTG_WAKE_UP:
                    case FpResDef.EVENT_OTG_STANDBY:
                    case FpResDef.EVENT_OTG_SPI_WRITE_READ:
                    case FpResDef.EVENT_OTG_SET_SPI_CLK:
                        if (mOtgSensorManager != null) {
                            mOtgSensorManager.nativeCallbackHandler(eventId, value1, value2, (byte[])eventObj);
                        }
                        break;
                    default:
                        break;
                }
            }
        };
    }
}
