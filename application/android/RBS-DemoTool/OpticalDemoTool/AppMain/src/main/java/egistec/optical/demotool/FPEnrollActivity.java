package egistec.optical.demotool;

import java.util.Timer;
import java.util.TimerTask;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import rbs.egistec.com.fplib.api.FingerprintReceiver;
import rbs.egistec.com.fplib.api.FpResDef;
import rbs.egistec.com.fplib.api.RbsLib;
import rbs.egistec.com.fplib.api.EgisLog;

interface ET310Action{
	
	int SHOW_PLACE_FINGER  = 1;
	int SHOW_REMOVE_FINGER = 2;
	int SHOW_HOLD_FINGER   = 3;
	
	void onKeyDown();
}

public class FPEnrollActivity extends AppCompatActivity {

	private static final String TAG = "RBS-EnrollActivity";
	private boolean mKeyBackPressed;
	
	private boolean directlyEnroll;
	private FPEnrollFragment fr;
    private TextView tv;
	private RbsLib mLib;
    private Toast mToast;
	private FingerprintReceiver fingerprintReceiver;
	private boolean canDoEnroll = false;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);		
		setContentView(R.layout.activity_fp_enroll);

		Bundle bundle = this.getIntent().getExtras();
		directlyEnroll = true;/*bundle.getBoolean(FPFingerActivity.EXTRA_DIRECTLY_ENROLL);*/
		tv = (TextView)findViewById(R.id.show_text);
		mLib = RbsLib.getInstance(this);

		if (savedInstanceState == null) {
			setEnrollView(getIntent()
			  .getIntExtra(FPFingerActivity.EXTRA_ENROLL_ID, -1));									
		} else {
			directlyEnroll = true;
		}

		if (true) {
			startEnrollFragment();
		} else {
			fingerprintReceiver = new FingerprintReceiver() {
				@Override
				public void onFingerprintEvent(int eventId, int value1, int value2, Object eventObj) {
					switch(eventId) {
						case FpResDef.EVENT_VERIFY_MATCHED:
							if (mToast != null) mToast.cancel();

							mToast = Toast.makeText(FPEnrollActivity.this, getText(R.string.the_finger_is_registered), Toast.LENGTH_SHORT);
							mToast.show();
							//startVerifyFinger();
							break;
						case FpResDef.EVENT_VERIFY_NOT_MATCHED:
							if (mToast != null) mToast.cancel();
							canDoEnroll = true;
							break;
						case FpResDef.EVENT_FINGER_LEAVE:
							if(canDoEnroll) {
								mLib.cancel();
								startEnrollFragment();
							}
						default:
							EgisLog.d(TAG, "ignore event status " + eventId);
							break;
						}
					}
				};
			mLib.startListening(fingerprintReceiver);
			//startVerifyFinger();
			}
	}

	/**
	 * Set up the {@link android.app.ActionBar}.
	 */

	private void setEnrollView(int fingerNumber){
		int FingerTestCaseidx = getIntent().getIntExtra(FPFingerActivity.EXTRA_FINGER_TESTCASE_IDX, -1);
		String userID = getIntent().getStringExtra(FPFingerActivity.EXTRA_USER_NAME);
		int FingerNumidx = getIntent().getIntExtra(FPFingerActivity.EXTRA_FINGER_NUMBER_IDX, -1);
		boolean mCanSaveImage = getIntent().getBooleanExtra(FPFingerActivity.EXTRA_IS_SAVE_IMAGE, false);
		int fingerOnIdx = getIntent().getIntExtra(FPFingerActivity.EXTRA_FINGER_ON_INDEX,-1);
		boolean optionNoCancelActionUp = getIntent().getBooleanExtra(FPFingerActivity.EXTRA_OPTION_NOCANCEL_ACTIONUP, false);
		fr = FPEnrollFragment.newInstance(FingerTestCaseidx, userID, FingerNumidx, mCanSaveImage, optionNoCancelActionUp, fingerOnIdx);
		fr.setEnrollId(fingerNumber);
	}
	
	@Override
	protected void onResume() {
		super.onResume();
/*		if(mLib != null) {
			//startVerifyFinger();
		}
*/
	}
	
	@Override
	public void onPause() {
		super.onPause();
/*		if(mLib != null) {
			mLib.cancel();
			finish();
		}
*/
	}	
		
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}	
		
	void doFinish(){
		Intent returnIntend =new Intent(); 	
		returnIntend.putExtra("index", ((FPEnrollFragment)getFragmentManager().findFragmentById(R.id.logo_container)).getFingerOnIndex());
		setResult(0, returnIntend);
		finish();
	}	
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		EgisLog.d(TAG, "keyCode="+keyCode);
		if (keyCode == KeyEvent.KEYCODE_BACK) { // BACK button pressed
			if (!mKeyBackPressed)
			{
				mKeyBackPressed = true;
				Toast.makeText(this,"Please click back again to cancel", 
		                Toast.LENGTH_SHORT).show();
				new Timer().schedule(new TimerTask(){
					@Override
					public void run(){
						mKeyBackPressed = false;
					}
				}, 500);
				return false;
			}
			mKeyBackPressed = false;
		
			ET310Action fragment = (ET310Action) getFragmentManager()
					  .findFragmentById(R.id.logo_container);
			if (fragment != null) {
				fragment.onKeyDown();
			}
			return super.onKeyDown(keyCode, event);
		}		
		return super.onKeyDown(keyCode, event);
	}

	private void startEnrollFragment() {
		getFragmentManager().beginTransaction().add(R.id.logo_container, fr).commit();
	}

	private void startVerifyFinger() {
		final int[] fplist = new int[5];
		final int[] fpcount = { 0 };
		int ret = mLib.getFingerprintList(0,fplist, fpcount);
		if(ret < 0) {
			EgisLog.e(TAG, "getFingerList failed");
			Toast.makeText(this, "Failed to get finger list !", Toast.LENGTH_SHORT).show();
			return;
		}

		ret = mLib.verify(0,fplist,fpcount[0],1);
		if(ret < 0) {
			EgisLog.e(TAG, "verify failed");
			Toast.makeText(this, "Failed to verify finger !", Toast.LENGTH_SHORT).show();
			return;
		}
		tv.setText(R.string.put_different_finger);
	}
}
