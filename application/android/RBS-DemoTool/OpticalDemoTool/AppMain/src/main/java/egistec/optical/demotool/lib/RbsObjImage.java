package egistec.optical.demotool.lib;

import android.util.Log;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;

public class RbsObjImage {
    final String TAG = "@@ RBS_" + RbsObjArray.class.getSimpleName();

    public final static int VERIFY_TRY_MATCH_RESULT_LAST_NOT_MATCH = -1;
    public final static int VERIFY_TRY_MATCH_RESULT_NOT_MATCH = 0;
    public final static int VERIFY_TRY_MATCH_RESULT_MATCH = 1;

    public final static int VERIFY_TYPE_NORMAL_MATCH = 1;
    public final static int VERIFY_TYPE_QUICK_MATCH = 2;
    public final static int VERIFY_TYPE_LQM_MATCH = 3;

    private enum ParamType {
        int8,
        int16,
        int32,
        int64,

        uint8,
        uint16,
        uint32,
        uint64,
        float32,
    };

    public static final int OBJ_NAME_SIZE = 16;
    public static final int RBS_OBJ_DEST_SIZE = 34;
    public static final int IMGTYPE_BIN = 0;
    public static final int IMGTYPE_RAW  = 1;
    public static final int IMGTYPE_BKG  = 2;
    private byte[] eventObjBytes;
    private int[] g_obj_pos = new int[1];

    RbsObjArray.rbs_obj_desc_t obj_desc;

    public final int imgtype;
    public final int width;
    public final int height;
    public final int bpp;
    public final int param_type;
    public final int index_fingeron;
    public final int index_try_match;
    public final int index_series;
    public final int is_bad;
    public final int algo_flag;
    public final int reject_reason;
    public final int match_score;
    public final int try_match_result;
    public final int qty;
    public final int partial;
    public final int fake_score;
    public final int is_light;
    public final int stat_min;
    public final int stat_max;
    public final int stat_avg;
    public final int Mc;
    public final int MBc;
    public final int M;
    public final int temperature;
    public final int exposure;
    public final int hw_integrate_count;
    public final int sw_integrate_count;
    public final int extract_qty;
    public final int bds_debug_path;
    public final int bds_pool_add;
    public final int is_learning;
    public final int match_type;
    public final int g2_partial;
    public final int is_last_image;
    public final int black_edge;
    public final int finger_score;
    public final int match_threshold;
    public final int MATCH_SCORES_SIZE = 5;
    public final int[] match_scores;

    public final byte[] image;

    public RbsObjImage(Object eventObj) {
        if (eventObj == null || !(eventObj instanceof byte[])) {
            Log.e(TAG, "got bad eventObj " + eventObj);
            eventObjBytes = new byte[RBS_OBJ_DEST_SIZE];
        } else {
            eventObjBytes = (byte[]) eventObj;
        }
        Log.i(TAG, "eventObjBytes size=" + eventObjBytes.length);
        g_obj_pos[0] = 0;
        obj_desc = new RbsObjArray.rbs_obj_desc_t();
        obj_desc.tag = byteArrayToInt(eventObjBytes, g_obj_pos, ParamType.uint32);
        obj_desc.id = byteArrayToInt(eventObjBytes, g_obj_pos, ParamType.uint32);
        obj_desc.name = byteArrayToString(eventObjBytes, g_obj_pos, OBJ_NAME_SIZE);
        obj_desc.version_major = byteArrayToInt(eventObjBytes, g_obj_pos, ParamType.uint16);
        obj_desc.version_minor = byteArrayToInt(eventObjBytes, g_obj_pos, ParamType.uint16);
        obj_desc.payload_size = byteArrayToInt(eventObjBytes, g_obj_pos, ParamType.uint32);
        obj_desc.header_size = byteArrayToInt(eventObjBytes, g_obj_pos, ParamType.uint16);

        Log.i(TAG, String.format("obj_desc id %d, version %d.%d", obj_desc.id, obj_desc.version_major, obj_desc.version_minor));

        final int objDescSize = 4 + 4 + OBJ_NAME_SIZE + 2 + 2 + 4 + 2;

        imgtype = byteArrayToInt(eventObjBytes, g_obj_pos, ParamType.uint16);
        width = byteArrayToInt(eventObjBytes, g_obj_pos, ParamType.uint16);
        height = byteArrayToInt(eventObjBytes, g_obj_pos, ParamType.uint16);
        bpp = byteArrayToInt(eventObjBytes, g_obj_pos, ParamType.uint16);

        Log.i(TAG, String.format("img_type[%d] %d:%d  bpp %d", imgtype, width, height, bpp));

        param_type = byteArrayToInt(eventObjBytes, g_obj_pos, ParamType.uint16);
        index_fingeron = byteArrayToInt(eventObjBytes, g_obj_pos, ParamType.uint16);
        index_try_match = byteArrayToInt(eventObjBytes, g_obj_pos, ParamType.uint16);
        index_series = byteArrayToInt(eventObjBytes, g_obj_pos, ParamType.uint16);

        Log.i(TAG, String.format("param_type [%d] index_fingeron %d index_try_match %d - %d", param_type, index_fingeron, index_try_match, index_series));

        is_bad = byteArrayToInt(eventObjBytes, g_obj_pos, ParamType.uint16);
        algo_flag = byteArrayToInt(eventObjBytes, g_obj_pos, ParamType.int16);
        reject_reason = byteArrayToInt(eventObjBytes, g_obj_pos, ParamType.uint16);
        match_score = byteArrayToInt(eventObjBytes, g_obj_pos, ParamType.uint16);
        try_match_result = byteArrayToInt(eventObjBytes, g_obj_pos, ParamType.int16);
        qty = byteArrayToInt(eventObjBytes, g_obj_pos, ParamType.int16);
        partial = byteArrayToInt(eventObjBytes, g_obj_pos, ParamType.uint16);
        fake_score = byteArrayToInt(eventObjBytes, g_obj_pos, ParamType.int16);
        is_light = byteArrayToInt(eventObjBytes, g_obj_pos, ParamType.uint16);
        stat_min = byteArrayToInt(eventObjBytes, g_obj_pos, ParamType.uint16);
        stat_max = byteArrayToInt(eventObjBytes, g_obj_pos, ParamType.uint16);
        stat_avg = byteArrayToInt(eventObjBytes, g_obj_pos, ParamType.uint16);

        Log.i(TAG, String.format("fake_score %d " +
                "is_bad %d " +
                "algo_flag %d " +
                "reject_reason %d " +
                "match_score %d " +
                "try_match_result %d " +
                "qty %d " +
                "partial %d " +
                "is_light %d", fake_score, is_bad, algo_flag ,reject_reason, match_score, try_match_result, qty, partial, is_light));

        if(obj_desc.version_minor >= 2) {
            Mc = byteArrayToInt(eventObjBytes, g_obj_pos, ParamType.uint16);
            MBc = byteArrayToInt(eventObjBytes, g_obj_pos, ParamType.uint16);
            M = byteArrayToInt(eventObjBytes, g_obj_pos, ParamType.uint16);
        }else{
            Mc = 0;
            MBc = 0;
            M = 0;
        }

        temperature = byteArrayToInt(eventObjBytes, g_obj_pos, ParamType.int16);
        exposure = byteArrayToInt(eventObjBytes, g_obj_pos, ParamType.uint16);
        hw_integrate_count = byteArrayToInt(eventObjBytes, g_obj_pos, ParamType.uint16);
        sw_integrate_count = byteArrayToInt(eventObjBytes, g_obj_pos, ParamType.uint16);

        Log.i(TAG, String.format("exposure %d hw_cnt %d sw_cnt %d temp %d", exposure, hw_integrate_count, sw_integrate_count, temperature));

        if (g_obj_pos[0] < obj_desc.header_size + objDescSize) {
            extract_qty = byteArrayToInt(eventObjBytes, g_obj_pos, ParamType.uint16);
            bds_debug_path = byteArrayToInt(eventObjBytes, g_obj_pos, ParamType.uint16);
            bds_pool_add = byteArrayToInt(eventObjBytes, g_obj_pos, ParamType.uint16);
            Log.d(TAG, String.format("extract_qty %d, bds_debug_path %d, bds_pool_add %d",
                    extract_qty, bds_debug_path, bds_pool_add));
        } else {
            Log.e(TAG, String.format("obj_desc.header_size not enough"));
            extract_qty = 0;
            bds_debug_path = 0;
            bds_pool_add = 0;
        }
        is_learning = byteArrayToInt(eventObjBytes, g_obj_pos, ParamType.uint16);
        match_type = byteArrayToInt(eventObjBytes, g_obj_pos, ParamType.uint16);
        g2_partial = byteArrayToInt(eventObjBytes, g_obj_pos, ParamType.int16);
        is_last_image = byteArrayToInt(eventObjBytes, g_obj_pos, ParamType.uint16);
        black_edge = byteArrayToInt(eventObjBytes, g_obj_pos, ParamType.int16);

        Log.i(TAG, String.format("is_learning %d match_type %d g2_partial %d", is_learning, match_type, g2_partial));

        match_scores = new int[MATCH_SCORES_SIZE];
        if (obj_desc.version_minor == 0) {
            finger_score = 0;
            match_threshold = 0;
        } else if(obj_desc.version_minor == 1) {
            finger_score = byteArrayToInt(eventObjBytes, g_obj_pos, ParamType.uint16);
            match_threshold = byteArrayToInt(eventObjBytes, g_obj_pos, ParamType.uint16);
            for (int i = 0; i < match_scores.length; i++) {
                match_scores[i] = byteArrayToInt(eventObjBytes, g_obj_pos, ParamType.uint16);
            }
            Log.d(TAG, String.format("match_scores %d, %d, %d, %d, %d", match_scores[0], match_scores[1],
                    match_scores[2], match_scores[3], match_scores[4]));
        } else {
            finger_score = byteArrayToInt(eventObjBytes, g_obj_pos, ParamType.uint16);
            match_threshold = byteArrayToInt(eventObjBytes, g_obj_pos, ParamType.uint16);
            for (int i = 0; i < match_scores.length; i++) {
                match_scores[i] = byteArrayToInt(eventObjBytes, g_obj_pos, ParamType.uint16);
            }
            Log.i(TAG, String.format("match_scores %d, %d, %d, %d, %d", match_scores[0], match_scores[1],
                    match_scores[2], match_scores[3], match_scores[4]));
        }
        image = getImage(obj_desc);
    }

    public byte[] getImage(RbsObjArray.rbs_obj_desc_t obj_desc) {
        int objSize = obj_desc.payload_size;
        if (objSize <= 0) {
            return new byte[0];
        }
        try {
            byte[] img = new byte[objSize];
            System.arraycopy(eventObjBytes, RBS_OBJ_DEST_SIZE + obj_desc.header_size, img, 0, objSize);
            if (bpp == 16) {
                img = FPUtil.AndroidBufferToWindowsBuffer(img);
            }
            return img;
        } catch (Exception e) {
            e.printStackTrace();
            return new byte[0];
        }
    }

    public byte[] getBuffer() {
        return eventObjBytes;
    }

    private int byteArrayToInt(byte[] array, int[] pos, ParamType type) {
        int ret_int = 0;
        ByteBuffer bb;
        switch (type){
            case int16:
                bb = ByteBuffer.wrap(array, pos[0], 2);
                bb.order(ByteOrder.LITTLE_ENDIAN);
                ret_int = bb.getShort();
                pos[0] += 2;
                break;
            case uint32:
                ret_int += (array[pos[0]++] & 0xff) << 0;
                ret_int += (array[pos[0]++] & 0xff) << 8;
                ret_int += (array[pos[0]++] & 0xff) << 16;
                ret_int += (array[pos[0]++] & 0xff) << 24;
                break;
            case uint16:
                ret_int += (array[pos[0]++] & 0xff) << 0;
                ret_int += (array[pos[0]++] & 0xff) << 8;
                break;
            default:
                Log.e(TAG, "not supported type " + type);
                break;

        }
        return ret_int;
    }

    private String byteArrayToString(byte[] array, int[] pos, int char_num) {
        byte[] sub_array = Arrays.copyOfRange(array, pos[0], pos[0] + char_num);
        String ret_str = new String(sub_array, StandardCharsets.UTF_8);
        pos[0] = pos[0] + char_num;
        return ret_str;
    }
}
