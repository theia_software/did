package egistec.optical.demotool.otg.utility;

import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbDeviceConnection;
import android.hardware.usb.UsbManager;
import android.util.Log;
import android.widget.Toast;

import java.util.HashMap;
import java.util.Map;

import egistec.optical.demotool.otg.otgsensor.IODispatchOtgSCSI;

public class USBWrapper {
    private static final String TAG = "USBWrapper";
    private static final String ACTION_USB_PERMISSION = "com.android.example.USB_PERMISSION";
    private UsbManager usbManager;
    private UsbDevice device;
    private UsbDeviceConnection connection;
    private boolean isInitSensor = false;
    private boolean debug = false;
    private static final int ERROR_COMMAND_FAIL = 1;
    static byte[] et727_sige_tag = {'S', 'I', 'G', 'E'};

    private static final int BAUD_RATE = 9600; // BaudRate. Change this value if you need
    private boolean serialPortConnected;
    private boolean isSerialPortInit = false;
    private Context mContext;
    //    private Handler mHandler;
    private static IODispatchOtgSCSI mIoDispatch = null;
    private static OTGType mOTGType;

    public interface USBPermissionListener{
        void onPermissionGet();
    }
    private USBPermissionListener mUSBPermissionListener;

    public enum OTGType {
        SERIAL_PORT,
        MASS_STORAGE,
        UNDEFINED,
        ;

        public static OTGType convert(int ordinal) {
            for (OTGType value : OTGType.values()) {
                if (value.ordinal() == ordinal) {
                    return value;
                }
            }
            return OTGType.UNDEFINED;
        }
    }

    public USBWrapper(Context context, USBPermissionListener listener) {
        mContext = context;
        usbManager = (UsbManager) context.getSystemService(Context.USB_SERVICE);
        mUSBPermissionListener = listener;
        setFilters();
        mIoDispatch = new IODispatchOtgSCSI(mContext, usbManager);
        mOTGType = OTGType.UNDEFINED;
        findSerialPortDevice();
    }

    private void setFilters() {
        Log.d(TAG, "setFilters");
        IntentFilter filter = new IntentFilter();
        filter.addAction(UsbManager.ACTION_USB_DEVICE_ATTACHED);
        filter.addAction(UsbManager.ACTION_USB_DEVICE_DETACHED);
        filter.addAction(UsbManager.ACTION_USB_ACCESSORY_ATTACHED);
        filter.addAction(UsbManager.ACTION_USB_ACCESSORY_DETACHED);
        filter.addAction(ACTION_USB_PERMISSION);
        mContext.registerReceiver(mUsbReceiver, filter);
    }

    private void requestUserPermission() {
        Log.d(TAG, "requestUserPermission");
        PendingIntent mPendingIntent =
                PendingIntent.getBroadcast(mContext, 0, new Intent(ACTION_USB_PERMISSION), 0);
        usbManager.requestPermission(device, mPendingIntent);
    }

    public OTGType getOTGType() {
        // Log.d(TAG, "getOTGType otg type = " + mOTGType);

        return mOTGType;
    }

    private void findSerialPortDevice() {
        // This snippet will try to open the first encountered usb device connected, excluding usb
        // root
        // hubs

        Log.d(TAG, "findSerrialPortDevie");

        UsbDevice localDevice = null ;

        HashMap<String, UsbDevice> usbDevices = usbManager.getDeviceList();
        if (!usbDevices.isEmpty()) {
            Log.e(TAG, "Get USB Devices List size=" + usbDevices.size());
            for (Map.Entry<String, UsbDevice> entry : usbDevices.entrySet()) {

                localDevice = entry.getValue();
                int deviceVID = localDevice.getVendorId();
                int devicePID = localDevice.getProductId();

                if (entry.getValue().getProductId() == 10048
                || entry.getValue().getProductId() == 30720 ) {
                    Log.e(TAG, "可能是開發板上DEFAULT的裝置，跳過不SCAN:" + deviceVID + "," + devicePID) ;
                    continue ;
                }

                if (deviceVID != 0x1d6b
                        && (devicePID != 0x0001 && devicePID != 0x0002 && devicePID != 0x0003)) {
                    // There is a device connected to our Android device. Try to open it as a Serial
                    // Port.

                    // MCU
                    if (deviceVID == IODispatchOtgSCSI.VID_HEARTBEAT
                            && devicePID == IODispatchOtgSCSI.PID_HEARTBEAT) {
                        device = entry.getValue();
                        mOTGType = OTGType.MASS_STORAGE;

                        // 註: 只有在是MASS STORAGE時，才REQUEST PERM，不然是REQUEST到其他裝置
                        requestUserPermission();
                    } else { // Arduno board
                        mOTGType = OTGType.SERIAL_PORT;
                    }
                } else {
                    connection = null;
                    device = null;
                }
            }
        } else {
            Log.d(TAG, "device list is empty");
        }
    }

    private boolean check_is_success(byte[] buffer) {
        for (int i = 0; i < et727_sige_tag.length; i++) {
            if (buffer[i] != et727_sige_tag[i]) return false;
        }

        return buffer[6] == 0x01;
    }

    public int ioDispatchOtgSCSI_Read_Register(short address, byte[] val) {
        return mIoDispatch.ReadRegister(address, val);
    }

    public int ioDispatchOtgSCSI_Write_Register(byte address, byte val) {
        return mIoDispatch.WriteRegister(address, val);
    }

    public int ioDispatchOtgSCSI_Write_Register(short address, byte val) {
        return mIoDispatch.WriteRegister(address, val);
    }

    public int ioDispatchOtgSCSI_Get_Frame(byte[] imageBuffer, int width, int height) {
        return mIoDispatch.FetchImage(imageBuffer, width, height);
    }

    public int ioDispatchOtgSCSI_Set_SPI_CLK(byte mode, byte clk) {
        return mIoDispatch.Set_SPI_CLK(mode, clk);
    }
    public int ioDispatchOtgSCSI_SPI_Write_Read(byte[] writeBuf, int writeLen, byte[] readBuf, int readLen) {
        return mIoDispatch.SPI_Write_Read(writeBuf, writeLen, readBuf, readLen);
    }

    /*
     * Notifications from UsbService will be received here.
     */
    private final BroadcastReceiver mUsbReceiver =
            new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    Log.d(TAG, "BroadcastReceiver " + intent.getAction().toString());

                    switch (intent.getAction()) {
                        case UsbManager.ACTION_USB_DEVICE_ATTACHED: // USB PERMISSION GRANTED
                            //Toast.makeText(context, "USB Attached", Toast.LENGTH_SHORT).show();
                            if (!serialPortConnected)

                                // warning: 這個是掃全部
                                findSerialPortDevice(); // A USB device has been attached. Try to
                            // open it as a
                            // Serial port
                            break;
                        case UsbManager.ACTION_USB_DEVICE_DETACHED: // USB PERMISSION GRANTED
                            //Toast.makeText(context, "USB Detached", Toast.LENGTH_SHORT).show();
                            serialPortConnected = false;
                            break;
                        case ACTION_USB_PERMISSION: // USB PERMISSION GRANTED
                            //Toast.makeText(context, "USB Permission", Toast.LENGTH_SHORT).show();
                            // serialPortConnected = true;

                            boolean granted =
                                    intent.getExtras()
                                            .getBoolean(UsbManager.EXTRA_PERMISSION_GRANTED);
                            Log.d(TAG, "granted = " + granted);
                            if (granted) // User accepted our USB connection. Try to open the device
                            // as a serial
                            // port
                            {
                                connection = usbManager.openDevice(device);
                                serialPortConnected = true;
                                if (mOTGType == OTGType.MASS_STORAGE) {
                                    boolean ret = mIoDispatch.openEndpoint(device);
                                    // startHandler();
                                    if (ret == true) {
                                        // mFoundDevice = 1;
                                        Log.d("OtgDemoApp", "DEV_STATE_CONNECTED");
                                        // mHandler.obtainMessage(FpResDef.DEV_STATE_CONNECTED, 0,
                                        // -1).sendToTarget();
                                    } else {
                                        // mFoundDevice = -1;
                                        Log.d("OtgDemoApp", "DEV_STATE_DISCONNECTED");
                                        // mHandler.obtainMessage(FpResDef.DEV_STATE_DISCONNECTED,
                                        // -1, -1).sendToTarget();
                                    }
//                                    mHandler.sendEmptyMessage(USB_PERMISSION_GRANTED);
                                    if(mUSBPermissionListener == null){
                                        Log.e(TAG,"USB permission callback listener is null!");
                                    }else{
                                        mUSBPermissionListener.onPermissionGet();
                                    }
                                }else {
                                    Log.e("OtgDemoApp", "not MASS_STORAGE, val=" + mOTGType);
                                }

                            } else // User not accepted our USB connection. Send an Intent to the
                            // Main Activity
                            {
                                Log.d(TAG, "not granted permission");
                            }
                            break;
                    }
                }
            };

}