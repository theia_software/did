package egistec.optical.demotool.lib;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import rbs.egistec.com.fplib.api.EgisLog;

public class SQTouchView extends View {
    private static String TAG = SQTouchView.class.getSimpleName();
    public SQTouchView(Context context) {
        super(context);
    }

    public SQTouchView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public SQTouchView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        return true;
    }
}
