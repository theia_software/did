package egistec.optical.demotool.lib;

import android.os.Handler;
//import android.util.Log;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.ShortBuffer;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import rbs.egistec.com.fplib.api.EgisLog;

public class LocationTuner {
	public static final String TAG = "LocationTuner";

	public enum ImageType { EIGHT_BIT, SIXTEEN_BIT }

	class ImageBuffer {
		ImageType type;
		int width;
		int height;
		byte[] data;
	}

	enum State {
		INIT,
		MOVE_ICON_INTO_SENSOR,
		WHITE_SPOT_TO_CENTER,
		CHANGE_LIGHT_SPOT_TO_WHITE,
		FIND_BRIGHTNESS_SPOT,
		WHITE_SPOT_TO_BRIGHT,
		CHANGE_LIGHT_SPOT_TO_GRADIENT,
		FIND_DARKNESS_SPOT,
		TUNING_COMPLETE
	}

	public static final int MSG_DO_FETCH_IMAGE_8BIT = 1;
	public static final int MSG_DO_FETCH_IMAGE_16BIT = 2;
	public static final int MSG_DO_ADJUST_COORDINATE = 3;
	public static final int MSG_DO_CHANGE_LIGHT_SPOT_WHITE = 4;
	public static final int MSG_DO_CHANGE_LIGHT_SPOT_ALLBLACK = 5;
	public static final int MSG_DO_CHANGE_LIGHT_SPOT_GRADIENT = 6;
	public static final int MSG_TUNING_COMPLETE = 7;
	public static final int MSG_STATUS_LOG = 8;

	private static final int ALLOW_TORRANCE = 5;

	private final int RANGE = 5;
	private final int DELAY_TIME = 200;

	private int mFindStartX;
	private int mFindStartY;
	private int mFindEndX;
	private int mFindEndY;
	private int mFirstImg = 0;
	private int mBrightnessThreshold;
	private final int BRIGHTNESS_COUNT = 200;
	private final int MOVE_STAP_X = 25;
	private final int MOVE_STAP_Y = 20;

	private int mAdjustedX;
	private int mAdjustedY;
	private ImageBuffer mImageBuffer;
	private Handler mHandler;
	private final Lock mLock = new ReentrantLock();
	private final Condition mSync;
	private State mState = State.INIT;
	private boolean mIsXtuned = false;
	private boolean mIsYtuned = false;
	private ImageType mImageType = ImageType.EIGHT_BIT;

	private int[] mSpotX = new int[1];
	private int[] mSpotY = new int[1];
	private int mIndexX;
	private int mIndexY;
	private int mStartX;
	private int mStartY;
	private int mFinalX;
	private int mFinalY;
	private boolean mIsTaskComplete = false;
	private int mMinSum = Integer.MAX_VALUE;

	private int mPreDiffY = Integer.MAX_VALUE;
	private int mPreDiffX = Integer.MAX_VALUE;
	private int mErrorCountY = 0;
	private int mErrorCountX = 0;
	private final int CHECK_ERROR_COUNT = 4;
	private boolean mIsTurnX = false;
	private boolean mIsTurnY = false;
	private Thread mThread;

	private Runnable mTask = new Runnable() {
		@Override
		public void run() {
			while (true) {
				if (mIsTaskComplete)
					break;
				int fetchImageDelayTime = 0;
				StringBuilder statusBuilder = new StringBuilder();
				int width = mImageBuffer.width;
				int height = mImageBuffer.height;
				byte[] data = mImageBuffer.data;
				EgisLog.d(TAG, "phase =" + mState);
				switch (mState) {
					case INIT: {
						mHandler.sendEmptyMessage(
						    MSG_DO_CHANGE_LIGHT_SPOT_ALLBLACK);
						mState = State.MOVE_ICON_INTO_SENSOR;
						fetchImageDelayTime = DELAY_TIME;
						statusBuilder.append("find whiteSpot position:\n");
						mHandler
						    .obtainMessage(
							MSG_STATUS_LOG, statusBuilder.toString())
						    .sendToTarget();
						mAdjustedX = mFindStartX;
						mAdjustedY = mFindStartY;
						mHandler
						    .obtainMessage(MSG_DO_ADJUST_COORDINATE,
							mAdjustedX, mAdjustedY)
						    .sendToTarget();
					} break;
					case MOVE_ICON_INTO_SENSOR: {
						adjustIconButtonPosition(data, width, height);
						if (mIsXtuned && mIsYtuned) {
							mState = State.TUNING_COMPLETE;
							mIsXtuned = false;
							mIsYtuned = false;
						} else {
							EgisLog.d(TAG,
							    String.format(
								"adjustedX=%d adjustedY=%d",
								mAdjustedX, mAdjustedY));
							statusBuilder.append(
							    String.format("spotX=%d spotY=%d \n",
								mAdjustedX, mAdjustedY));
							mHandler
							    .obtainMessage(MSG_STATUS_LOG,
								statusBuilder.toString())
							    .sendToTarget();
							if (mAdjustedX < mFindEndX) {
								mAdjustedX += MOVE_STAP_X;
							} else {
								if (mAdjustedY < mFindEndY) {
									mAdjustedY += MOVE_STAP_Y;
									mAdjustedX = mFindStartX;
								} else {
									EgisLog.e(
									    TAG, "Not find sensor");
									mIsXtuned = true;
									mIsYtuned = true;
								}
							}
							mHandler
							    .obtainMessage(MSG_DO_ADJUST_COORDINATE,
								mAdjustedX, mAdjustedY)
							    .sendToTarget();
						}
						fetchImageDelayTime = DELAY_TIME;
					} break;
					case TUNING_COMPLETE: {
						EgisLog.d(TAG, "tuning complete");
						mIsTaskComplete = true;
						mHandler
						    .obtainMessage(
							MSG_TUNING_COMPLETE, mAdjustedX, mAdjustedY)
						    .sendToTarget();
					} break;
				} // end of switch case
				if (mImageType == ImageType.EIGHT_BIT)
					mHandler.sendEmptyMessageDelayed(
					    MSG_DO_FETCH_IMAGE_8BIT, fetchImageDelayTime);
				else
					mHandler.sendEmptyMessageDelayed(
					    MSG_DO_FETCH_IMAGE_16BIT, fetchImageDelayTime);
				mLock.lock();
				try {
					mSync.await();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				mLock.unlock();
			} // end of while
		}
	};

	private Runnable mTask1 = new Runnable() {
		@Override
		public void run() {
			while (true) {
				if (mIsTaskComplete)
					break;
				int fetchImageDelayTime = 0;
				StringBuilder statusBuilder = new StringBuilder();
				int width = mImageBuffer.width;
				int height = mImageBuffer.height;
				byte[] data = mImageBuffer.data;
				EgisLog.d(TAG, "phase =" + mState);
				switch (mState) {
					case INIT: {
						mHandler.sendEmptyMessage(
						    MSG_DO_CHANGE_LIGHT_SPOT_ALLBLACK);
						mState = State.WHITE_SPOT_TO_CENTER;
						fetchImageDelayTime = DELAY_TIME;
						statusBuilder.append("find whiteSpot position:\n");
						mHandler
						    .obtainMessage(
							MSG_STATUS_LOG, statusBuilder.toString())
						    .sendToTarget();
					} break;
					case WHITE_SPOT_TO_CENTER: {
						int targetX = width / 2;
						int targetY = height / 2;
						adjustLightSpotPosition(
						    data, width, height, targetX, targetY);
						if (mIsXtuned && mIsYtuned) {
							mState = State.TUNING_COMPLETE;
							mIsXtuned = false;
							mIsYtuned = false;
						}
						fetchImageDelayTime = 0;
					} break;
					case TUNING_COMPLETE: {
						EgisLog.d(TAG, "tuning complete");
						mIsTaskComplete = true;
						mHandler
						    .obtainMessage(
							MSG_TUNING_COMPLETE, mAdjustedX, mAdjustedY)
						    .sendToTarget();
					} break;
				} // end of switch case
				if (mImageType == ImageType.EIGHT_BIT)
					mHandler.sendEmptyMessageDelayed(
					    MSG_DO_FETCH_IMAGE_8BIT, fetchImageDelayTime);
				else
					mHandler.sendEmptyMessageDelayed(
					    MSG_DO_FETCH_IMAGE_16BIT, fetchImageDelayTime);
				mLock.lock();
				try {
					mSync.await();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				mLock.unlock();
			} // end of while
		}
	};

	private Runnable mTask2 = new Runnable() {
		@Override
		public void run() {
			while (true) {
				if (mIsTaskComplete)
					break;
				int fetchImageDelayTime = 0;
				StringBuilder statusBuilder = new StringBuilder();
				int width = mImageBuffer.width;
				int height = mImageBuffer.height;
				byte[] data = mImageBuffer.data;
				EgisLog.d(TAG, "phase =" + mState);
				switch (mState) {
					case CHANGE_LIGHT_SPOT_TO_WHITE: {
						mHandler.sendEmptyMessage(
						    MSG_DO_CHANGE_LIGHT_SPOT_WHITE);
						mImageType = ImageType.SIXTEEN_BIT;
						mState = State.FIND_BRIGHTNESS_SPOT;
						fetchImageDelayTime = DELAY_TIME;
						statusBuilder.append(
						    "find the brightness spot position\n");
						mHandler
						    .obtainMessage(
							MSG_STATUS_LOG, statusBuilder.toString())
						    .sendToTarget();
					} break;
					case FIND_BRIGHTNESS_SPOT: {
						ImageType type = mImageBuffer.type;
						if (type == ImageType.SIXTEEN_BIT) {
							ShortBuffer sb =
							    ByteBuffer.wrap(data)
								.order(ByteOrder.nativeOrder())
								.asShortBuffer();
							short[] rawImage16 =
							    new short[width * height];
							sb.get(rawImage16);
							findBrightnessSpot(rawImage16, width,
							    height, mSpotX, mSpotY);
							mHandler.sendEmptyMessage(
							    MSG_DO_CHANGE_LIGHT_SPOT_ALLBLACK);
							mImageType = ImageType.EIGHT_BIT;
							mState = State.WHITE_SPOT_TO_BRIGHT;
							fetchImageDelayTime = DELAY_TIME;
							statusBuilder.append(
							    "set layout to fit brightness spot\n");
							statusBuilder.append(
							    String.format("spotX=%d spotY=%d \n",
								mSpotX[0], mSpotY[0]));
							mHandler
							    .obtainMessage(MSG_STATUS_LOG,
								statusBuilder.toString())
							    .sendToTarget();
						} else {
							EgisLog.e(TAG, "please set 16bit image");
						}
					} break;
					case WHITE_SPOT_TO_BRIGHT: {
						EgisLog.d(TAG,
						    String.format(
							"spotX=%d spotY=%d", mSpotX[0], mSpotY[0]));
						adjustLightSpotPosition(
						    data, width, height, mSpotX[0], mSpotY[0]);
						if (mIsXtuned && mIsYtuned) {
							mState = State.CHANGE_LIGHT_SPOT_TO_GRADIENT;
							mIsXtuned = false;
							mIsYtuned = false;
						}
						fetchImageDelayTime = 0;
					} break;
					case CHANGE_LIGHT_SPOT_TO_GRADIENT: {
						mHandler.sendEmptyMessage(
						    MSG_DO_CHANGE_LIGHT_SPOT_GRADIENT);
						mStartX = mAdjustedX - RANGE;
						mStartY = mAdjustedY - RANGE;
						mIndexX = mStartX;
						mIndexY = mStartY;
						mImageType = ImageType.SIXTEEN_BIT;
						mState = State.FIND_DARKNESS_SPOT;
						fetchImageDelayTime = DELAY_TIME;
					} break;
					case FIND_DARKNESS_SPOT: {
						statusBuilder.append(
						    " fine tune to find the darkest layout position\n");
						statusBuilder.append(
						    String.format("layout X=%d layout Y=%d \n",
							mIndexX, mIndexY));
						mHandler
						    .obtainMessage(
							MSG_STATUS_LOG, statusBuilder.toString())
						    .sendToTarget();
						ShortBuffer sb = ByteBuffer.wrap(data)
								     .order(ByteOrder.nativeOrder())
								     .asShortBuffer();
						short[] rawImage16 = new short[width * height];
						sb.get(rawImage16);
						/*
						    int sum = 0;
						    for (int i = 0; i < rawImage16.length; i++) {
							sum += rawImage16[i];
						    }
						    sum /= rawImage16.length;
						*/
						int sum = arrangeMean(rawImage16, mSpotX[0],
						    mSpotY[0], width, height, 20);
						if (sum < mMinSum) {
							mMinSum = sum;
							mFinalX = mIndexX;
							mFinalY = mIndexY;
							EgisLog.d(TAG,
							    String.format("minSum=%d", mMinSum));
						}

						int regionHeight = RANGE * 2;
						int regionWIDTH = RANGE * 2;
						int x = mIndexX, y = mIndexY;
						EgisLog.d(TAG, String.format("index x=%d, y=%d", x, y));
						if (y < mStartY + regionHeight) {
							if (x < mStartX + regionWIDTH) {
								mHandler
								    .obtainMessage(
									MSG_DO_ADJUST_COORDINATE, x,
									y)
								    .sendToTarget();
								mIndexX++;
							} else {
								mIndexY++;
								mIndexX -= regionWIDTH;
							}
							fetchImageDelayTime = DELAY_TIME;
						} else {
							mState = State.TUNING_COMPLETE;
						}
					} break;
					case TUNING_COMPLETE: {
						EgisLog.d(TAG, "tuning complete");
						mIsTaskComplete = true;
						mHandler
						    .obtainMessage(
							MSG_TUNING_COMPLETE, mFinalX, mFinalY)
						    .sendToTarget();
					} break;
				}
				if (mImageType == ImageType.EIGHT_BIT)
					mHandler.sendEmptyMessageDelayed(
					    MSG_DO_FETCH_IMAGE_8BIT, fetchImageDelayTime);
				else
					mHandler.sendEmptyMessageDelayed(
					    MSG_DO_FETCH_IMAGE_16BIT, fetchImageDelayTime);
				mLock.lock();
				try {
					mSync.await();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				mLock.unlock();
			} // end of while
		}
	};

	private void adjustIconButtonPosition(byte[] rawImage, final int width, final int height) {
		if (mIsXtuned && mIsYtuned) {
			EgisLog.i(TAG, "position tuning complete");
			EgisLog.d(TAG,
			    String.format("adjustedX=%d adjustedY=%d", mAdjustedX, mAdjustedY));
			return;
		}
		int brightnessCount = 0;
		int[] rawImage32 = new int[width * height];
		for (int i = 0; i < rawImage32.length; i++) {
			rawImage32[i] = (int) rawImage[i] & 0xFF;
		}
		final int imageSize = width * height;
		int sum = 0;
		for (int i = 0; i < imageSize; i++) {
			if (mFirstImg == 0) {
				sum += rawImage32[i];
			} else {
				if (rawImage32[i] > mBrightnessThreshold) {
					brightnessCount++;
					if (brightnessCount > (BRIGHTNESS_COUNT))
						break;
				}
			}
		}

		EgisLog.d(TAG,
		    String.format("brightnessCount = %d mBrightnessThreshold = %d, ",
			brightnessCount, mBrightnessThreshold));
		if (mFirstImg == 0) {
			mBrightnessThreshold = Math.round((sum / imageSize) - 10);
		} else {
			if (brightnessCount < BRIGHTNESS_COUNT) {
				mIsXtuned = true;
				mIsYtuned = true;
			}
		}
		mFirstImg++;
	}

	private void adjustLightSpotPosition(
	    byte[] rawImage, final int width, final int height, int targetX, int targetY) {
		if (mIsXtuned && mIsYtuned) {
			EgisLog.i(TAG, "position tuning complete");
			EgisLog.d(TAG,
			    String.format("adjustedX=%d adjustedY=%d", mAdjustedX, mAdjustedY));
			return;
		}
		EgisLog.d(TAG, String.format("targetX=%d, targetY=%d", targetX, targetY));
		int[] rawImage32 = new int[width * height];
		for (int i = 0; i < rawImage32.length; i++) {
			rawImage32[i] = (int) rawImage[i] & 0xFF;
		}
		final int imageSize = width * height;
		int whiteSpotIndex = 0;
		int x, y;
		int maxValue = Integer.MIN_VALUE;
		for (int i = 0; i < imageSize; i++) {
			if (rawImage32[i] > maxValue) {
				maxValue = rawImage32[i];
				whiteSpotIndex = i;
			}
		}
		x = (whiteSpotIndex + 1) % width;
		y = (whiteSpotIndex + 1) / width + 1;

		int diffX = targetX - x, diffY = targetY - y;
		if (Math.abs(diffX) <= ALLOW_TORRANCE) {
			mIsXtuned = true;
		}
		if (Math.abs(diffY) <= ALLOW_TORRANCE) {
			mIsYtuned = true;
		}

		EgisLog.d(TAG,
		    String.format(
			"whiteSpotIndex=%d x=%d y=%d maxValue=%d", whiteSpotIndex, x, y, maxValue));
		EgisLog.d(TAG,
		    String.format("preDiffX=%d diffX=%d preDiffY=%d diffY=%d", mPreDiffX, diffX,
			mPreDiffY, diffY));
		EgisLog.d(TAG, "tune x coordinate");

		int diff2X = (diffX > 0) ? 1 : -1;
		if (!mIsXtuned) {
			if (mIsTurnX == false) {
				mAdjustedY = mAdjustedY + diff2X;
			} else {
				mAdjustedY = mAdjustedY - diff2X;
			}

			if (Math.abs(mPreDiffX) <= Math.abs(diffX)) {
				mErrorCountX++;
				if (mErrorCountX == CHECK_ERROR_COUNT) {
					mIsTurnX = !mIsTurnX;
					mErrorCountX = 0;
				}
			}
			mPreDiffX = diffX;
		}

		int diff2Y = (diffY > 0) ? 1 : -1;
		if (!mIsYtuned) {
			if (mIsTurnY == false) {
				mAdjustedX = mAdjustedX - diff2Y;
			} else {
				mAdjustedX = mAdjustedX + diff2Y;
			}

			if (Math.abs(mPreDiffY) <= Math.abs(diffY)) {
				mErrorCountY++;
				if (mErrorCountY == CHECK_ERROR_COUNT) {
					mIsTurnY = !mIsTurnY;
					mErrorCountY = 0;
				}
			}
			mPreDiffY = diffY;
		}
		mHandler.obtainMessage(MSG_DO_ADJUST_COORDINATE, mAdjustedX, mAdjustedY)
		    .sendToTarget();
		EgisLog.d(TAG,
		    String.format("sheldon adjustedX=%d adjustedY=%d", mAdjustedX, mAdjustedY));
	}

	public LocationTuner(Handler handler, int startX, int startY, int endWidth, int endHeight) {
		mHandler = handler;
		mAdjustedX = startX;
		mAdjustedY = startY;
		mFindEndX = endWidth - 110;
		mFindEndY = endHeight - 110;
		mFindStartX = 0;
		mFindStartY = (int) Math.ceil(mFindEndY / 3 * 2);
		mImageBuffer = new ImageBuffer();
		mSync = mLock.newCondition();
		EgisLog.d(TAG,
		    String.format(
			"window size is wstart = %d , hstart = %d , wend = %d , hend = %d",
			mFindStartX, mFindStartY, mFindEndX, mFindEndY));
	}

	public void moveToIconInSensorStart() {
		if (mThread != null && mThread.isAlive()) {
			EgisLog.d(TAG, "the location tuner is working");
			return;
		}
		mIsTaskComplete = false;
		mState = State.INIT;
		mThread = new Thread(mTask);
		mThread.start();
	}

	public void moveToCenterSpotStart() {
		if (mThread != null && mThread.isAlive()) {
			EgisLog.d(TAG, "the location tuner is working");
			return;
		}
		mIsTaskComplete = false;
		mState = State.INIT;
		mThread = new Thread(mTask1);
		mThread.start();
	}

	public void moveToBrightnessSpotStart() {
		EgisLog.d(TAG, "start3");
		if (mThread != null && mThread.isAlive()) {
			EgisLog.d(TAG, "the location tuner is working");
			return;
		}
		EgisLog.d(TAG, "passing");
		mIsTaskComplete = false;
		mState = State.CHANGE_LIGHT_SPOT_TO_WHITE;
		mThread = new Thread(mTask2);
		mThread.start();
	}

	public void stop() {
		mLock.lock();
		mIsTaskComplete = true;
		mSync.signal();
		mLock.unlock();
	}

	public void setImage(ImageType type, byte[] image, int width, int height) {
		mImageBuffer.type = type;
		mImageBuffer.width = width;
		mImageBuffer.height = height;
		mImageBuffer.data = new byte[image.length];
		System.arraycopy(image, 0, mImageBuffer.data, 0, image.length);
	}

	public void imageReady() {
		mLock.lock();
		mSync.signal();
		mLock.unlock();
	}

	private void findBrightnessSpot(short[] img, int width, int height, int[] cx, int[] cy) {
		int sum, max_sum = 0;
		int i, j;

		for (i = 0; i < height; i++) {
			sum = 0;
			for (j = 0; j < width; j++) {
				sum += img[i * width + j];
			}
			if (sum > max_sum) {
				max_sum = sum;
				cy[0] = i;
			}
		}

		max_sum = 0;
		for (j = 0; j < width; j++) {
			sum = 0;
			for (i = 0; i < height; i++) {
				sum += img[i * width + j];
			}
			if (sum > max_sum) {
				max_sum = sum;
				cx[0] = j;
			}
		}
		EgisLog.d(TAG, String.format("cx = %d, cy = %d", cx[0], cy[0]));
	}

	private int arrayToIdx(int x, int y, int width) {
		return (x - 1) + ((y - 1) * width);
	}
	private int arrangeMean(short[] raw, int centX, int centY, int width, int height, int div) {
		int mean = 0;
		int sum_i = 0;
		int leftX = centX - div;
		int topY = centY - div;

		for (int y = topY; y < (topY + 2 * div); y++) {
			for (int x = leftX; x < (leftX + 2 * div); x++) {
				mean += raw[arrayToIdx(x, y, width)];
				sum_i++;
			}
		}
		mean /= sum_i;
		EgisLog.d(TAG, "mean = " + mean);
		return mean;
	}

	// NOTE: this only for ET711
	private void findCenter_sounder(
	    short[] a_Image, short a_Width, short a_Height, int[] x, int[] y) {
		final int QUANTIZE_FACTOR = 1000;
		// int Sum_x[1024] = {0};
		// int Sum_y[1024] = {0};
		int[] Sum_x = new int[1024];
		int[] Sum_y = new int[1024];
		int index_x1 = 0, index_y1 = 0;
		int index_x2 = 0, index_y2 = 0;
		int max_x1 = 0, max_y1 = 0;
		int max_x2 = 0, max_y2 = 0;
		int crop = 0;
		int i, j;

		int value = 0;
		int index = 0;

		for (i = 0; i < a_Width; i++) {
			for (j = 0; j < a_Height; j++) {
				Sum_x[i] += a_Image[j * a_Width + i];
			}
			Sum_x[i] /= QUANTIZE_FACTOR;
		}
		for (i = 0; i < a_Height; i++) {
			for (j = 0; j < a_Width; j++) {
				Sum_y[i] += a_Image[i * a_Width + j];
			}
			Sum_y[i] /= QUANTIZE_FACTOR;
		}

		for (i = 0; i < a_Width; i++) {
			if (Sum_x[i] > max_x1) {
				max_x1 = Sum_x[i];
				index_x1 = i;
			}
			index = (a_Width - 1 - i);
			if (Sum_x[index] > max_x2) {
				max_x2 = Sum_x[index];
				index_x2 = index;
			}
		}

		for (i = 0; i < a_Height; i++) {
			if (Sum_y[i] > max_y1) {
				max_y1 = Sum_y[i];
				index_y1 = i;
			}
			index = (a_Height - 1 - i);
			if (Sum_y[index] > max_y2) {
				max_y2 = Sum_y[index];
				index_y2 = index;
			}
		}

		/*switch(m_DeviceType)
		{
			case DEVICE_ET736_SPI:
			case DEVICE_ET728_SPI:
			case DEVICE_ET736_OTG:
			case DEVICE_ET728_OTG:
				break;
			case DEVICE_ET711_SPI:
			case DEVICE_ET711_OTG:
				index_x1 -= 6;
				index_x2 += 2;
				break;
		}*/

		index_x1 -= 6;
		index_x2 += 2;

		x[0] = (index_x1 + index_x2) / 2 + 1; // +1 for display start with 1
		y[0] = (index_y1 + index_y2) / 2 + 1;
	}
}
