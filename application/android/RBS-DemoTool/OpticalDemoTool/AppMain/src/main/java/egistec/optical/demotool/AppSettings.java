package egistec.optical.demotool;

import android.content.Context;
import android.content.SharedPreferences;

import java.security.MessageDigest;

public class AppSettings {
    private Context mContext;
    private SharedPreferences mSharedPreferences;
    private static final String FILE_NAME = "AppSettings";

    public static final int PRESS_AREA_TYPE_ID_GREEN = 0;
    public static final int PRESS_AREA_TYPE_ID_GREEN_GRADIENT = 1;
    public static final int PRESS_AREA_TYPE_ID_GREEN_GRADIENT_DARKER = 2;
    public static final int PRESS_AREA_TYPE_ID_GREEN_GRADIENT_DARKEST = 3;
    public static final int PRESS_AREA_TYPE_ID_GREEN_GRADATION = 4;
    public static final int PRESS_AREA_TYPE_ID_AQUA = 5;
    public static final int PRESS_AREA_TYPE_ID_AQUA_CTD = 6;
    public static final int PRESS_AREA_TYPE_ID_WHITE_CIRCLE = 7;

    private static final String KEY_NOCANCEL_ACTIONUP = "no_cancel_action_up";
    private static final String KEY_SCR_BRIGHTNESS = "screen_brightness";
    private static final String KEY_BUCKUP_SCR_BRIGHTNESS = "backup_screen_brightness";
    private static final String KEY_ENGINEER_PW = "engineer_password";
    private static final String KEY_PRESS_AREA_TYPE_ID = "press_area_type_id";
    private static final String KEY_PRESS_AREA_DIAMETER_INDEX = "press_area_diameter_index";
    private static final String KEY_ADD_VIEWTREEOBSERVER = "add_viewtreeobserver";
    private static final String KEY_CUSTOMIZE_PRESS_AREA_ICON = "customize_press_area_icon";
    private static final String KEY_SENSOR_SIZE_INDEX = "sensor_size_index";

    private static final boolean DEFAULT_NOCANCEL_ACTIONUP = false;
    private static final int DEFAULT_SCR_BRIGHTNESS = 0;
    private static final String DEFAULT_ENGINEER_PASSWORD = "5078BCA8D0B0F7F9D30F3C1883E2B42F6D616761D312331A6CA7DD5776D590A8";
    private static final int DEFAULT_PRESS_AREA_TYPE_ID = PRESS_AREA_TYPE_ID_GREEN;
    private static final int DEFAULT_PRESS_AREA_DIAMETER_INDEX = 2;
    private static final boolean DEFAULT_ADD_VIEWTREEOBSERVER = false;
    private static final boolean DEFAULT_CUSTOMIZE_PRESS_AREA_ICON = false;
    private static final int DEFAULT_SENSOR_SIZE_INDEX = 1;

    public AppSettings(Context context) {
        mContext = context;
        if (mContext != null) {
            mSharedPreferences = mContext.getSharedPreferences(FILE_NAME, Context.MODE_PRIVATE);
        }
    }

    public boolean getNoCancelActionUp() {
        return (mSharedPreferences == null) ? DEFAULT_NOCANCEL_ACTIONUP :
                mSharedPreferences.getBoolean(KEY_NOCANCEL_ACTIONUP, DEFAULT_NOCANCEL_ACTIONUP);
    }

    public void setNoCancelActionUp(boolean noCancelActionUp) {
        if (mSharedPreferences == null) {
            return;
        }
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putBoolean(KEY_NOCANCEL_ACTIONUP, noCancelActionUp);
        editor.apply();
    }

    public int getScreenBrightness() {
        return (mSharedPreferences == null) ? DEFAULT_SCR_BRIGHTNESS :
                mSharedPreferences.getInt(KEY_SCR_BRIGHTNESS, DEFAULT_SCR_BRIGHTNESS);
    }

    public void setScreenBrightness(int brightness) {
        if (mSharedPreferences == null) {
            return;
        }
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putInt(KEY_SCR_BRIGHTNESS, brightness);
        editor.apply();
    }

    public int getBackupScreenBrightness() {
        return (mSharedPreferences == null) ? DEFAULT_SCR_BRIGHTNESS :
                mSharedPreferences.getInt(KEY_BUCKUP_SCR_BRIGHTNESS, DEFAULT_SCR_BRIGHTNESS);
    }

    public void setBackupScreenBrightness(int brightness) {
        if (mSharedPreferences == null) {
            return;
        }
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putInt(KEY_BUCKUP_SCR_BRIGHTNESS, brightness);
        editor.apply();
    }

    public boolean getAddViewTreeObserver() {
        return (mSharedPreferences == null) ? DEFAULT_ADD_VIEWTREEOBSERVER :
                mSharedPreferences.getBoolean(KEY_ADD_VIEWTREEOBSERVER, DEFAULT_ADD_VIEWTREEOBSERVER);
    }

    public void setAddViewTreeObserver(boolean addViewTreeObserver) {
        if (mSharedPreferences == null) {
            return;
        }
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putBoolean(KEY_ADD_VIEWTREEOBSERVER, addViewTreeObserver);
        editor.apply();
    }

    public boolean getCustomizePressAreaIcon() {
        return (mSharedPreferences == null) ? DEFAULT_CUSTOMIZE_PRESS_AREA_ICON :
                mSharedPreferences.getBoolean(KEY_CUSTOMIZE_PRESS_AREA_ICON, DEFAULT_CUSTOMIZE_PRESS_AREA_ICON);
    }

    public void setCustomizePressAreaIcon(boolean customizePressAreaIcon) {
        if (mSharedPreferences == null) {
            return;
        }
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putBoolean(KEY_CUSTOMIZE_PRESS_AREA_ICON, customizePressAreaIcon);
        editor.apply();
    }

    public String getEngineerModePassword() {
        return (mSharedPreferences == null) ?  DEFAULT_ENGINEER_PASSWORD:
                mSharedPreferences.getString(KEY_ENGINEER_PW, DEFAULT_ENGINEER_PASSWORD);
    }

    private static String byte2hex(byte[] b){
        String hs = "";
        String stmp = "";
        for (int n = 0; n < b.length; n++){
            stmp = Integer.toHexString(b[n] & 0XFF);
            if (stmp.length() == 1)
                hs = hs + "0" + stmp;
            else
                hs = hs + stmp;
        }
        return hs.toUpperCase();
    }

    public String encrypt(String s) {
        MessageDigest sha = null;
        try {
            sha = MessageDigest.getInstance("SHA-256");
            sha.update(s.getBytes());
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
        return byte2hex(sha.digest());
    }

    public int getPressAreaTypeId() {
        return (mSharedPreferences == null) ? DEFAULT_PRESS_AREA_TYPE_ID :
                mSharedPreferences.getInt(KEY_PRESS_AREA_TYPE_ID, DEFAULT_PRESS_AREA_TYPE_ID);
    }

    public void setPressAreaTypeId(int pressAreaTypeId) {
        if (mSharedPreferences == null) {
            return;
        }
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putInt(KEY_PRESS_AREA_TYPE_ID, pressAreaTypeId);
        editor.apply();
    }

    public int getPressAreaDiameterIndex() {
        return (mSharedPreferences == null) ? DEFAULT_PRESS_AREA_DIAMETER_INDEX :
                mSharedPreferences.getInt(KEY_PRESS_AREA_DIAMETER_INDEX, DEFAULT_PRESS_AREA_DIAMETER_INDEX);
    }

    public int getSensorSizeIndex() {
        return (mSharedPreferences == null) ? DEFAULT_SENSOR_SIZE_INDEX :
                mSharedPreferences.getInt(KEY_SENSOR_SIZE_INDEX, DEFAULT_SENSOR_SIZE_INDEX);
    }

    public void setPressAreaDiameterIndex(int diameterIndex) {
        if (mSharedPreferences == null) {
            return;
        }
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putInt(KEY_PRESS_AREA_DIAMETER_INDEX, diameterIndex);
        editor.apply();
    }

    public void setSensorSizeIndex(int sensorSizeIndex){
        if(mSharedPreferences == null){
            return;
        }
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putInt(KEY_SENSOR_SIZE_INDEX, sensorSizeIndex);
        editor.apply();
    }
}
