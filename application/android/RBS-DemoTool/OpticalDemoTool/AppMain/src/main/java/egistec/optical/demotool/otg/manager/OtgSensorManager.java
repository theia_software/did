package egistec.optical.demotool.otg.manager;

import android.content.Context;
import android.util.Log;

import java.lang.ref.WeakReference;

import egistec.optical.demotool.otg.otgsensor.ETotg;
import egistec.optical.demotool.otg.otgsensor.ET736;
import egistec.optical.demotool.otg.otgsensor.SensorBase;
import egistec.optical.demotool.otg.utility.USBWrapper;

public class OtgSensorManager {

    public static final String TAG = "OtgSensorManager";

    public static final int SENSOR_ID_UNDEFINED = -1;
    public static final int SENSOR_ID_ET725     = 0;
    public static final int SENSOR_ID_ET727     = 1;
    public static final int SENSOR_ID_ET736     = 2;

    private static OtgSensorManager instance = null;

    private Context mContext = null;
    private SensorBase mSensor = null;

    public interface OtgSensorEventListener{
        void onOtgSensorInited();
    }
    private WeakReference<OtgSensorEventListener> refOtgSensorEventListener = null;

    public static OtgSensorManager getInstance(int sensorId, Context context) {
        if (instance == null) {
            instance = new OtgSensorManager();
            instance.mContext = context;
            instance.sensorInit(sensorId);
        }
        return instance;
    }

    public void nativeCallbackHandler(int eventId, int value1, int value2, byte[] byteBuffer) {
        if (mSensor == null) {
            return;
        }
        Log.d(TAG, "[NativeCallbackHandler] eventId = " + eventId);
        mSensor.eventHandler(eventId, value1, value2, byteBuffer);
    }

    public void setOtgSensorEventListener(OtgSensorEventListener listener){
        refOtgSensorEventListener = new WeakReference<>(listener);
    }

    private int sensorInit(int sensorId) {
        int ret = -1;
        switch (sensorId) {
            case SENSOR_ID_ET727:
                Log.d(TAG, "[sensorInit] new ETotg().");
                mSensor = new ETotg();
                break;
            case SENSOR_ID_ET736:
                Log.d(TAG, "[sensorInit] new ET736().");
                mSensor = new ET736();
                break;
            default:
                Log.e(TAG, "[sensorInit] Not supported sensor id = " + sensorId);
                return ret;
        }
        Log.d(TAG, "[sensorInit] Call initialize().");
        ret = mSensor.initialize(new USBWrapper(mContext, new USBWrapper.USBPermissionListener() {
            @Override
            public void onPermissionGet() {
                Log.d(TAG, "onPermissionGet");
                if(refOtgSensorEventListener != null && refOtgSensorEventListener.get() != null){
                    refOtgSensorEventListener.get().onOtgSensorInited();
                }
            }
        }));
        return ret;
    }
}
