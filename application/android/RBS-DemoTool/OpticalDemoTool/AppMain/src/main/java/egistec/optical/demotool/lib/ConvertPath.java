package egistec.optical.demotool.lib;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

public class ConvertPath {
    public static final String TAG = "RBS";
    private static String timestamp = "000";
    private static int IndexSeries = -1;
    private static boolean saveInImageObj;

    public static String ImageType(int imagetype) {
        String image_root = "image_bin";
        switch(imagetype) {
            case 0:
                image_root = "image_bin";
                break;
            case 1:
                image_root = "image_raw"; // modify for g_inc mode
                break;
            case 2:
                image_root = "image_bkg";
                break;
        }
        return image_root;
    }

    public static String CaseName(int test_case) {
        char startChar = test_case < 15 ? 'P' : 'E';
        int offset = test_case < 15 ? 0 : 15;
        return Character.toString((char) (startChar + (char) ((test_case - offset) / 3)));
    }

    public static String CaseSt(int test_case) {
        String test_case_st = "st";
        switch (test_case % 3) {
            case 0:
                test_case_st = "st";
                break;
            case 1:
                test_case_st = "45d";
                break;
            case 2:
                test_case_st = "90d";
                break;
        }
        return test_case_st;
    }

    public static String getTimeStamp(RbsObjImage rbsObjImage) {
        if(rbsObjImage.index_series != IndexSeries) {
            Long currentTime = (System.currentTimeMillis());
            timestamp = currentTime.toString();
        }
        IndexSeries = rbsObjImage.index_series;
        return timestamp;
    }


    // static public DateFormat et760rbsTimeFormat = new SimpleDateFormat("yyMMddHHmmssSSS");
    static public DateFormat et760rbsTimeFormat = new SimpleDateFormat("yyMMdd-HHmmss-SS");
    public static String getTimeStampToYMDHMS(RbsObjImage rbsObjImage) {
        String dateFormatted = null ;

        if(rbsObjImage.index_series != IndexSeries) {
            Long currentTime = (System.currentTimeMillis());
            timestamp = currentTime.toString();
            Date date = new Date();
            date.setTime(currentTime);
            dateFormatted = et760rbsTimeFormat.format(date);
        }

        if (dateFormatted == null ) {
            dateFormatted = et760rbsTimeFormat.format(new Date());
        }
        IndexSeries = rbsObjImage.index_series;
        // return timestamp;
        return dateFormatted ;
    }

    public static String EnrollCase(int IsBad, int try_match_result, int algo_flag) {//For enroll
        final int FP_MERGE_ENROLL_FEATURE_LOW = -2;
        final int FP_MERGE_ENROLL_REDUNDANT_INPUT = -8;
        final int FP_MERGE_ENROLL_TOO_FAST = -9;
        final int FP_MERGE_ENROLL_LOW_QTY = -10;
        final int FP_DUPLICATE = -1004;

        if (try_match_result == 0) {
            return "_TRY";
        }

        String match_case;
        if (IsBad == 1) {
            match_case = "_B";
        } else {
            switch (algo_flag) {
                case FP_MERGE_ENROLL_REDUNDANT_INPUT:
                    match_case = "_RD";
                    break;
                case FP_MERGE_ENROLL_LOW_QTY:
                    match_case = "_LQ";
                    break;
                case FP_DUPLICATE:
                    match_case = "_DP";
                    break;
                case FP_MERGE_ENROLL_FEATURE_LOW:
                    match_case = "_FEATLOW";
                    break;
                default:
                    match_case = "_OK";
                    break;
            }
        }
        return match_case;
    }

    public static String MatchCase(int isBad, boolean IsVerifySuccess) {//For verify
        String match_case;
        if (IsVerifySuccess)
            match_case = "_P";
        else {
            match_case = (isBad == 1) ? "_B": "_F";
        }
        return match_case;
    }

    public static boolean isSaveInImageObj() {
        return saveInImageObj;
    }

    public static void setSaveInImageObj(boolean saveInImageObj) {
        ConvertPath.saveInImageObj = saveInImageObj;
    }

    public static String FileNameMd5(byte[] array) {

        try {
            MessageDigest digest = MessageDigest.getInstance("md5");
            byte[] result = digest.digest(array);
            StringBuffer buffer = new StringBuffer();
            for(int k = 0; k < 3; k ++)
            {
                int number = result[k] & 0xff;
                String str = Integer.toHexString(number);
                if (str.length() == 1) {
                    buffer.append("0");
                }
                buffer.append(str);
            }
            return buffer.toString();
        } catch (NoSuchAlgorithmException e) {
            return "";
        }
    }

}
