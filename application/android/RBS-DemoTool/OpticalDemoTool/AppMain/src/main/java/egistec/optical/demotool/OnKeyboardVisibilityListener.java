package egistec.optical.demotool;

public interface OnKeyboardVisibilityListener {
    void onKBVisibilityChanged(boolean visible);
}

