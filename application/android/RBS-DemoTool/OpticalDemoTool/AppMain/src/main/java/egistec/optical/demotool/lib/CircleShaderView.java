package egistec.optical.demotool.lib;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RadialGradient;
import android.graphics.Shader;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;

public class CircleShaderView extends View {
    private static final String TAG = "CircleShaderView";
    public static final float GRADATION_RADIUS = 2.5f;
    private float mCenterPosX;
    private float mCenterPosY;
    private float mRadius;

    public CircleShaderView(Context context) {
        super(context);
    }

    public void setCircleCanvas(float x , float y) {
        mCenterPosX = x / 2;
        mCenterPosY = y / 2;
    }

    public void setRadius(float radius) {
        mRadius = radius;
    }
    @Override
    public void onDraw(Canvas canvas) {
        Paint pnt1 = new Paint();
        pnt1.setAntiAlias(true);
        int[] colors = {Color.parseColor("#FF00FF00"), Color.TRANSPARENT};
        float gradation_pos = (mRadius - GRADATION_RADIUS) / mRadius;
        float[] color_pos = {gradation_pos, 1.0f};
        pnt1.setShader(new RadialGradient(mCenterPosX, mCenterPosY,
                TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_MM, mRadius,
                getResources().getDisplayMetrics()), colors, color_pos, Shader.TileMode.CLAMP));
        canvas.drawCircle(mCenterPosX, mCenterPosY,
                TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_MM, mRadius,
                getResources().getDisplayMetrics()), pnt1);
    }

}
