package egistec.optical.demotool.igistec;


import android.content.ClipData;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import egistec.optical.demotool.R;

public class SkinColorAdapter extends BaseAdapter {

    private int resourceLayout;
    private Context mContext;

    ArrayList<SkinColorItem> skin_list = new ArrayList<SkinColorItem>() ;

    public SkinColorAdapter(Context context, int resource, ArrayList<SkinColorItem> items) {
        this.resourceLayout = resource;
        this.mContext = context;
        skin_list = items ;
    }

    @Override
    public int getCount() {
        return skin_list.size();
    }

    @Override
    public Object getItem(int i) {
        return skin_list.get(i) ;
    }

    @Override
    public long getItemId(int i) {
        return i ;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = convertView;

        if (v == null) {
            LayoutInflater vi;
            vi = LayoutInflater.from(mContext);
            v = vi.inflate(resourceLayout, null);
        }

        SkinColorItem p = (SkinColorItem) getItem(position);

        if (p != null) {
            TextView tvTitle = (TextView) v.findViewById(R.id.tvTitle);
            TextView tvColor = (TextView) v.findViewById(R.id.tvColor);
            TextView tvSubTitle = (TextView) v.findViewById(R.id.tvSubTitle);

            tvTitle.setText( p.title );
            tvSubTitle.setText( p.subTitle );
            tvColor.setBackgroundColor( p.color );
            tvColor.setTextColor( p.color );
            tvColor.setHighlightColor( p.color );
            tvColor.setHintTextColor(p.color);
        }

        return v;
    }

}