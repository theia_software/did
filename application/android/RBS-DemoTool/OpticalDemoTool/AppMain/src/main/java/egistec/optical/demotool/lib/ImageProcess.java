package egistec.optical.demotool.lib;

import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.util.Log;
import android.widget.ImageView;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Arrays;

public class ImageProcess {
    public static final String TAG = "ETS-IMAGE";

    public static byte[] raw16To8bit(byte[] raw16) {
        int size = raw16.length / 2;
        short [] shorts = new short[size];
        ByteBuffer.wrap(raw16).order(ByteOrder.LITTLE_ENDIAN).asShortBuffer().get(shorts);
        short max = shorts[0];
        short min = shorts[0];
        for (int i = 1; i < shorts.length; i++) {
            if (shorts[i] > max) {
                max = shorts[i];
            } else if (shorts[i] < min) {
                min = shorts[i];
            }
        }
        byte[] img8 = new byte[size];
        int dec = max - min;
        if (dec > 0) {
            for (int i = 0; i < size; i++) {
                img8[i] = (byte)((shorts[i] - min) * 255 / dec);
            }
        }
        return img8;
    }

    public static byte[] raw32To8bit(byte[] raw32) {
        int size = raw32.length / 4;
        int[]ints = new int[size];
        ByteBuffer.wrap(raw32).order(ByteOrder.LITTLE_ENDIAN).asIntBuffer().get(ints);
        int max = ints[0];
        int min = ints[0];
        for (int i = 1; i < ints.length; i++) {
            if (ints[i] > max) {
                max = ints[i];
            } else if (ints[i] < min) {
                min = ints[i];
            }
        }
        byte[] img8 = new byte[size];
        int dec = max - min;
        if (dec > 0) {
            for (int i = 0; i < size; i++) {
                img8[i] = (byte)((ints[i] - min) * 255 / dec);
            }
        }
        return img8;
    }

    public static byte[] raw_inverse(byte[] rawData) {
        byte[] rawDataTemp = Arrays.copyOf(rawData, rawData.length);
        for (int index = 0; index < rawDataTemp.length; index ++) {
            rawDataTemp[index] = (byte) ~rawDataTemp[index];
        }
        return rawDataTemp;
    }

    public static Bitmap rawToBmp(byte[] rawData, int width, int height) {
        Bitmap rawImage = Bitmap.createBitmap(width, height, Bitmap.Config.ALPHA_8);
        for (int index = 0; index < rawData.length; index++) {
            rawData[index] = (byte) ~rawData[index];
        }
        rawImage.copyPixelsFromBuffer(ByteBuffer.wrap(rawData));
        return rawImage;
    }

    public static Bitmap rawToBmpRotate(
            byte[] rawData, int width, int height, int degree, Bitmap.Config config) {
        Bitmap raw_image = Bitmap.createBitmap(width, height, config);
        raw_image.copyPixelsFromBuffer(ByteBuffer.wrap(rawData));
        Matrix matrix = new Matrix();
        matrix.setRotate(degree);
        return Bitmap.createBitmap(
                raw_image, 0, 0, raw_image.getWidth(), raw_image.getHeight(), matrix, true);
    }

    public static Bitmap showImage(ImageView imageView, int width, int height, byte[] rawData){
        if (imageView == null || rawData == null || width <= 0 || height <= 0) {
            Log.e(TAG, "showImage got bad null input");
            return null;
        }
        int rawlen = rawData.length;
        int bpp = rawlen * 8 / (width * height);
        byte[] rawDataTemp = null;
        switch (bpp) {
            case 8:
                rawDataTemp = raw_inverse(rawData);
                break;
            case 16:
                rawDataTemp = raw_inverse(raw16To8bit(rawData));
                break;
            case 32:
                rawDataTemp = raw_inverse(raw32To8bit(rawData));
                break;
                default:
                    Log.w(TAG, "unsupport bpp "+bpp+", width = "+width+", height = "+height+", datalen = "+rawlen);
                    return null;
        }
        Bitmap bitmapImage;
        if ((float) height / width > 1.3) {
            Log.v(TAG, "Do not rotate image");
        }
        bitmapImage = rawToBmpRotate(rawDataTemp, width, height, 0, Bitmap.Config.ALPHA_8);

        imageView.setImageBitmap(bitmapImage);
        imageView.postInvalidate();
        return bitmapImage;
    }
}
