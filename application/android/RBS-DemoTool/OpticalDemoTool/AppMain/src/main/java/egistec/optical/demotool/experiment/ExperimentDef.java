package egistec.optical.demotool.experiment;

public class ExperimentDef {
    public static final int MAX_WAIT_IMAGE_TIME = 5000;
    public static final int WAIT_IMAGE_TIME = 100;
    public static final float expTimeBase = 16.67f;
    public static final int imgWidth = 180;
    public static final int imgHeight = 90;
    public static final int processTypeCalibration = 0;
    public static final int processTypeCapture0 = 1;
    public static final int processTypeCapture1 = 2;
    public static final String expBkgBinFileFolder = "/data/";
    public static final String expBkgFileFolder = "/sdcard/temp/ExperimentBackground/";
    public static final String expCapFileFolder0 = "/sdcard/temp/ExperimentCapture/0/";
    public static final String expCapFileFolder1 = "/sdcard/temp/ExperimentCapture/1/";
    public static final String expCapQualityCsvFolder = "/sdcard/temp/ExperimentCapture/";
    public static final String expCapQualityCsvPath = "/sdcard/temp/ExperimentCapture/Quality_List.csv";
    public static final String bkgDoneMessage = "Saved in " + expBkgFileFolder;
    public static final String capDoneMessage0 = "Saved in " + expCapFileFolder0;
    public static final String capDoneMessage1 = "Saved in " + expCapFileFolder1;
    // Capture group id
    public static final int CAPTURE_LOW_TO_HIGH = 0;
    public static final int CAPTURE_HIGH_TO_LOW = 1;
    // Handler message id
    public static final int MESSAGE_PROCESS_START = 0;
    public static final int MESSAGE_PROCESS_UPDATE_PROGRESS = 1;
    public static final int MESSAGE_PROCESS_END = 2;
    // Press Color Id
    public static final int COLOR_WHITE = 0;
    public static final int COLOR_GREEN = 1;
    public static final int COLOR_BLUE = 2;
    public static final int COLOR_AQUA = 3;
    public static final int COLOR_RED = 4;

    public static final int PID_FAETOOL = 4;
    public static final int FAETOOL_GET_IMAGE = 1104;
    public static final int FAETOOL_GET_FFC_IMAGE =  7601104;
    public static final int FAETOOL_GET_FFC_IMAGE_NO_UPDATE_BDS = 17601104;
    public static final int FAETOOL_GET_IMAGE_SIZE_INFO = 1111;

    // PID for extra api of rbs
    public static final int PID_BKG_IMG                    = 6;
    // Cmd id for PID_BKG_IMG
    public static final int CMD_BKG_IMG_RUN_CALI_PROC      = 2001;
    public static final int CMD_BKG_IMG_SAVE_CALI_DATA     = 2002;
    public static final int CMD_BKG_IMG_LOAD_CALI_DATA     = 2003;
    public static final int CMD_BKG_IMG_GET_CALI_IMAGE     = 2004;
    public static final int CMD_BKG_IMG_RETURN_IMAGE       = 2005;
    public static final int CMD_BKG_IMG_REMOVE_CALI_DATA   = 2006;
    public static final int CMD_BKG_IMG_REMOVE_BDS         = 2007;
    public static final int CMD_BKG_BBKG_FFC               = 2008;
    public static final int CMD_BKG_WBKG_FFC               = 2009;
    public static final int CMD_BKG_IMG_SAVE_BDS           = 2010;


    // PID for extra api of rbs
    public static final int PID_HOST_TOUCH                 = 7;
    // Cmd id for PID_HOST_TOUCH
    public static final int CMD_HOST_TOUCH_ENABLE          = 100;
    public static final int CMD_HOST_TOUCH_SET_TOUCHING    = 101;
    public static final int CMD_HOST_TOUCH_RESET_TOUCHING  = 102;
    public static final int CMD_IC_SET_EXPOSURE_TIME       = 103;
    public static final int CMD_IC_SET_DC_OFFSET           = 104;
    public static final int CMD_IC_SET_INTEGRATE_COUNT     = 105;
    public static final int CMD_DEMOTOOL_SENSOR_POWER_ON   = 106;
    public static final int CMD_DEMOTOOL_SENSOR_POWER_OFF  = 107;
    public static final int CMD_RETURN_16BIT_IMAGE_ENABLE  = 108;
    public static final int CMD_HOST_TOUCH_SEND_TEMPERATURE = 111;
    public static final int CMD_HOST_TOUCH_SET_TOUCH_POS = 114;

}
