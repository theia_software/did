package egistec.optical.demotool.lib;

import egistec.optical.demotool.R;
import android.content.Context;

public class FPFingerInfo{
	
	private String mFingerName;
	private boolean mIsMatched;
	private int mFingerId;

	public FPFingerInfo(String fingerName, int fingerId, Context context){
		if (fingerName.equals("AddFinger")){
			this.mFingerName = context.getString(R.string.add_finger);
			this.mFingerId = -1;
			return;
		}
		this.mFingerName = fingerName;
		this.mFingerId = fingerId;
	}
	public String getFingerName(){
		return mFingerName;
	}
	public int getFingerId(){
		return mFingerId;
	}
	public void setFingerMatched(){
		mIsMatched = true;
	}	
	public void resetFingerMatched(){
		mIsMatched = false;
	}
	public boolean isFingerMatched(){
		return mIsMatched;
	}
	public void setFingerName(String fingerName){
		this.mFingerName = fingerName;
	}
}
