package egistec.optical.demotool.igistec;

import android.util.Log;

import java.util.HashMap;

import egistec.optical.demotool.otg.otgsensor.IOtgIO;
import egistec.optical.demotool.otg.otgsensor.OtgIO_V8_et760;


public class SPIUtility {

    private static Object lock = new Object() ;
    private static final String TAG = "SPIUtility";

    static public int SPI_SLAVE_BASE_ADDR_1 = 0x6000000c;
    static public int SPI_SLAVE_BASE_ADDR_2 = 0x2000000c;
    static public int BASE_ADDRESS = 0;


    static public IOtgIO otgio = null ;

    @Deprecated // 之後不需要以字串作INDEX的版本
    private static HashMap<String, Integer> OPCODE_LIST = new HashMap<String, Integer>(){{
        put("F1",  0xF1);
        put("F2",  0xF2);
        put("F3",  0xF3);
        put("F4",  0xF4);
        put("F5",  0xF5);

        put("A1",  0xA1);
        put("A2",  0xA2);
        put("A3",  0xA3);
        put("A4",  0xA4); // add for Set Exposure Time by US

        put("E3", 0xE3) ;
        put("E4", 0xE4) ;
        put("E5", 0xE5) ;
        put("E6", 0xE6) ;
    }};


    /**
     * 傳送OP CODE
     * @param key
     * @param value
     */
    @Deprecated // 之後不需要以字串作INDEX的版本
    public static void setRiscVOpcode(String key, int value)
    {
        Log.i(TAG,"setRiscVOpcode "+key);
        if(OPCODE_LIST.get(key) == null)
        {
            Log.e(TAG,"[Error]<setRiscVOpcode> key does not exist "+key);
            return;
        }
        byte[] addr = Common.intToByteArray(value);
        int opcode = OPCODE_LIST.get(key);

        byte[] tx_data = new byte[8];
        tx_data[0] = (byte)opcode;
        System.arraycopy(addr, 0, tx_data, 1, addr.length);

        if ( otgio != null) {
            if ( otgio instanceof OtgIO_V8_et760)
                ((OtgIO_V8_et760)otgio).riscvOpcode(tx_data) ;
            else
                otgio.Raw_SPI_Write(tx_data, tx_data.length) ;
        }

        return;
    }


    /**
     * 傳送OP CODE 全部用RAW SPI
     * @param data
     */
    public static void setRiscVOpcode(byte [] data)
    {
        synchronized (lock) {

            if ( otgio != null) {
                if ( otgio instanceof OtgIO_V8_et760)
                    ((OtgIO_V8_et760)otgio).riscvOpcode(data) ;
                else
                    otgio.Raw_SPI_Write(data, data.length) ;
            }
        }

        return;
    }

    public static void writeBurst(byte offset, byte[] data, int len) {
        synchronized (lock) {

            if ( otgio != null) {
                if ( otgio instanceof OtgIO_V8_et760) {

                    if ( len % 512 != 0 ) {
                        Log.e(TAG, "OTG-SPI BURST READ/WRITE單位必需是512!!!") ;
                        return ;
                    }
                    ((OtgIO_V8_et760)otgio).BurstWriteV2(offset ,data, len ) ;
                }
                else {
                    byte [] head = new byte[2] ;
                    head[0] = 0x26 ;
                    head[1] = offset ;
                    otgio.Raw_SPI_Write(head, 2, 0);
                    otgio.Raw_SPI_Write(data, data.length, 1);
                }
            }
        }
        return;
    }

    public static byte[] readBurst(byte offset,byte [] buf, int length) {
        synchronized (lock) {
            if ( otgio != null) {
                if ( otgio instanceof OtgIO_V8_et760) {
                    if ( length % 512 != 0 ) {
                        Log.e(TAG, "OTG-SPI BURST READ/WRITE單位必需是512!!!") ;
                        return buf ;
                    }
                    ((OtgIO_V8_et760)otgio).BurstReadV2(offset ,buf, length) ;
                }
                else {
//                    if ( SensorControl.NEED_SPI_SPEED_MODE ) {
                        byte [] writeBuf = new byte[3] ;
                        writeBuf[0] = 0x22 ;
                        writeBuf[1] = offset ;
                        writeBuf[2] = 0;
                        otgio.SPI_Write_Read(writeBuf,3, buf, length) ;
//                    }
//                    else {
//                        byte [] writeBuf = new byte[2] ;
//                        writeBuf[0] = 0x22 ;
//                        writeBuf[1] = offset ;
//                        otgio.SPI_Write_Read(writeBuf,2, buf, length) ;
//                    }

                }
            }
        }
        return buf ;
    }
//
//    @Deprecated
//    public static byte[] readBurst(byte offset, int length) {
//        byte [] ret = new byte[length] ;
//        synchronized (lock) {
//            if ( otgio != null) {
//                if ( otgio instanceof OtgIO_V8_et760)
//                    ((OtgIO_V8_et760)otgio).BurstReadV2(offset ,ret, ret.length ) ;
//                else {
//
//                    otgio.Raw_SPI_Write(tx_data, tx_data.length);
//                }
//            }
//        }
//        return ret ;
//    }

    @Deprecated
    public static void writeSingle8(byte offset, byte data) {
    }

    @Deprecated
    public static byte readSingle8(byte offset) {
        return 0;
    }

    public static void writeSingle32(int address, int data) {

        synchronized (lock) {
            if ( otgio != null) {
                if ( otgio instanceof OtgIO_V8_et760)
                    ((OtgIO_V8_et760)otgio).writeSingle32(address,data) ;
                else {
                    otgio.WriteRegister32(address, data) ;
                }
            }
        }
        return;
    }

    public static void resetTouch() {
        // todo: wait for focal
    }

    public static void blockTouch(boolean isEnable) {

        // todo: create new block touch with tm
        Log.e(TAG, "@@ Block Touch not implement with focal @@@") ;
//        Log.i(TAG, "Block Touch(Driver) = " + isEnable ) ;
//
//        if ( isEnable ) {
//            JNIMethod.setTouchEnable(0) ;
//        }
//        else {
//            JNIMethod.setTouchEnable(1) ;
//        }
    }

    public static int readSingle32(int address) {
        synchronized (lock) {
            if ( otgio != null) {
                if ( otgio instanceof OtgIO_V8_et760)
                    return ((OtgIO_V8_et760)otgio).readSingle32(address) ;
                else {
                    byte value[] = new byte[4] ;
                    otgio.ReadRegister32(address, value) ;
                    return Common.byteArrayToInt(value) ;
                }
            }
        }
        return 0 ;
    }

    /**
     * 重置位子
     */
    public static void resetDefaultAddress() {
        SPIUtility.writeSingle32(SPI_SLAVE_BASE_ADDR_1,0) ;
        SPIUtility.writeSingle32(SPI_SLAVE_BASE_ADDR_2,0) ;
    }

    /**
     * 修改DEFAULT位子
     */
    public static void setDefaultAddress(int addr) {
        Log.d(TAG, "Change Base ADDR (both spi 0x60 & 0x20)") ;
        Log.d(TAG,"setDefaultAddress: "+String.format("0x%08X", addr));
        SPIUtility.writeSingle32(SPI_SLAVE_BASE_ADDR_1,addr) ;
        SPIUtility.writeSingle32(SPI_SLAVE_BASE_ADDR_2,addr) ;
    }


}
