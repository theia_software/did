package egistec.optical.demotool.lib;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

public class IniHashMap<K, V> extends LinkedHashMap {

    final static String TAG = IniHashMap.class.getSimpleName();

    @Override
    public String toString() {
        Set<Entry<K, V>> keySet = this.entrySet();
        Iterator<Entry<K, V>> i = keySet.iterator();
        if (!i.hasNext())
            return "";
        StringBuffer buffer = new StringBuffer();
        for (;;) {
            Map.Entry<K, V> map = i.next();
            K key = map.getKey();
            V value = map.getValue();
            if (value instanceof IniHashMap) {
                buffer.append(key.toString() + "\n");
                IniHashMap<String, String> sectionHashMap = (IniHashMap<String, String>)value;
                buffer.append(sectionHashMap.toString());
            } else if (value instanceof String) {
                buffer.append(key.toString() + " = ");
                buffer.append(value.toString() + "\n");
            }
            if (!i.hasNext()) {
                return buffer.toString();
            }
        }
    }
}
