package egistec.optical.demotool.application;

import android.app.Application;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.util.Log;

import egistec.optical.demotool.BuildConfig;

public class DemoToolApp extends Application {
    public static String TAG = "RBS_app";
    private static DemoToolApp instance;
    private static String MyDataRoot = "";
    private boolean engineerMode;

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        MyDataRoot = BuildConfig.MYDATA_ROOT;
        Log.d(TAG, "MyDataRoot " + MyDataRoot);
        if (BuildConfig.DEBUG) {
            engineerMode = true;
        }
    }

    public static DemoToolApp getInstance() {
        return instance;
    }

    public static String getMyDataRoot() {
        return MyDataRoot;
    }

    public boolean isEngineerMode() {
        return engineerMode;
    }

    public void setEngineerMode(boolean engineerMode) {
        this.engineerMode = engineerMode;
    }

    public static String getVersionName(Context context) {
        PackageManager packageManager = context.getPackageManager();
        PackageInfo packageInfo;
        String versionName = "Egistec Inc";
        try {
            packageInfo = packageManager.getPackageInfo(context.getPackageName(), 0);
            versionName = packageInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return versionName;
    }
}
