package egistec.optical.demotool;

import android.databinding.DataBindingUtil;
import android.graphics.Point;
import android.os.AsyncTask;
import android.support.constraint.ConstraintLayout;
import android.support.constraint.ConstraintSet;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.util.Base64;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.File;
import java.lang.ref.WeakReference;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import egistec.optical.demotool.application.DemoToolApp;
import egistec.optical.demotool.databinding.ActivityLiveViewBinding;
import egistec.optical.demotool.experiment.ExperimentDef;
import egistec.optical.demotool.experiment.ExperimentUtils;
import egistec.optical.demotool.experiment.FileIoUtils;
import egistec.optical.demotool.igistec.AuoOledFingerprintReceiverHelper;
import egistec.optical.demotool.igistec.Common;
import egistec.optical.demotool.lib.CircleShaderView;
import egistec.optical.demotool.lib.FileObj;
import egistec.optical.demotool.lib.LightDotControl;
import egistec.optical.demotool.lib.LiveImageInfo;
import egistec.optical.demotool.lib.LiveImageViewer;
import egistec.optical.demotool.lib.MainFileSession;
import egistec.optical.demotool.lib.RbsUtil;
import rbs.egistec.com.fplib.api.RbsLib;
import rbs.egistec.com.fplib.api.EgisLog;

import static android.support.constraint.ConstraintLayout.LayoutParams.BOTTOM;
import static android.support.constraint.ConstraintLayout.LayoutParams.TOP;
import static android.support.constraint.ConstraintLayout.LayoutParams.LEFT;
import static android.support.constraint.ConstraintLayout.LayoutParams.RIGHT;

public class LiveViewActivity extends AppCompatActivity {
    final static String TAG = "RBS_" + LiveViewActivity.class.getSimpleName();
    private ActivityLiveViewBinding mBinding;

    private boolean mIsTouching; // et760新增 如果有按到VIEW，傳的參數就要是可以UPDATE BDS，不然就是 不能UPDATE BDS
    private LightDotControl mLightDot;
    private RbsLib mLib;

    private Button btnExit ;

    private LiveImageViewer liveImageViewer;
    private CaptureFrameAsyncTask currentAsyncTask;

    ImageView ivDebug ;

    private MainFileSession mainFileSession;
    private AppSettings mAppSettings;
    private CircleShaderView mGreenGradation;
    private AppSettingsPreference.SENSOR_SIZE sensorSize = AppSettingsPreference.SENSOR_SIZE.SENSOR_SIZE_LARGE;


    private Long tt_btn_show_et760_down; // for count press time to custom 5 sec

    ConstraintLayout mLayout ;

    // for et760 only
    TextView tvDebug ;
    static public boolean isShowEt760Debug = false ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mAppSettings = new AppSettings(this);
        sensorSize = AppSettingsPreference.getSensorSize(mAppSettings.getSensorSizeIndex());
        if(sensorSize == AppSettingsPreference.SENSOR_SIZE.SENSOR_SIZE_LARGE) {
            //UIUtils.hideSystemUI(getWindow().getDecorView());
            Common.hideSystemUI(LiveViewActivity.this);
        }
        setContentView(R.layout.activity_live_view);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_live_view);
        mainFileSession = new MainFileSession(DemoToolApp.getMyDataRoot());



        btnExit = findViewById(R.id.btnExit) ;
        btnExit.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                finish() ;
                return false;
            }
        });

        mLayout = findViewById(R.id.live_layout) ;
        mLayout.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {

                Common.hideSystemUI(LiveViewActivity.this);
                return false;
            }
        });

        // inject et760
        tvDebug = findViewById(R.id.tvDebug) ;

        mLib = RbsLib.getInstance(this);

        mBinding.ICWindowVerify.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN)
                {
                    /* Determine touch position */
                    float x = motionEvent.getX();
                    float y = motionEvent.getY();

                    int x_ratio_x100 = (int)(x * 100 / mBinding.ICWindowVerify.getWidth());
                    int y_ratio_x100 = (int)(y * 100 / mBinding.ICWindowVerify.getHeight());
                    List<Point> listTouchPoint = new ArrayList<>();
                    listTouchPoint.add(new Point(x_ratio_x100, y_ratio_x100));
                    RbsUtil.setTouchDownEventToSDK(mBinding.textViewShowTemperatureInfo);
                    RbsUtil.setTouchPosToSDK(1, listTouchPoint);
                    mIsTouching = true;
                    if(mLightDot != null) {
                        mLightDot.playStart();
                    }
                    if(mBinding.checkBoxSaveWhenTouch.isChecked()){
                        if(currentAsyncTask != null && !currentAsyncTask.isCancelled()){
                            if(currentAsyncTask.isSavingTouchFrame()){
                                return true;
                            }
                            currentAsyncTask.cancel(false);
                        }
                        mBinding.imageView8bit.setBackgroundResource(R.drawable.bg_border);
                        mBinding.imageViewRaw.setBackgroundResource(R.drawable.bg_border);
                        RbsUtil.setTouchDownEventToSDK(mBinding.textViewShowTemperatureInfo);
                        currentAsyncTask = new CaptureFrameAsyncTask();
                        currentAsyncTask.setSaveTouchFrame(true, mBinding.textViewCountTouchFrames, x_ratio_x100, y_ratio_x100, 1);
                        currentAsyncTask.execute();
                    }
                    return true;
                }
                else if (motionEvent.getAction() == MotionEvent.ACTION_UP)
                {
                    EgisLog.i("log", "action_up ");
                    mIsTouching = false;
                    //RbsUtil.setTouchPosToSDK(0, 0);
                    mLib.extraApi(ExperimentDef.PID_HOST_TOUCH, ExperimentDef.CMD_HOST_TOUCH_RESET_TOUCHING, null, null, null);
                    if(mLightDot != null) {
                        mLightDot.playStop();
                        mLightDot.reset();
                    }
                    return true;
                }

                return false;
            }
        });

        mBinding.buttonSaveCurrentFrame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (currentAsyncTask != null) {
                    currentAsyncTask.setSaveCurrentFrame(true, mBinding.textViewCountCurrentFrame);
                }
            }
        });
        mBinding.checkBoxSaveAllImages.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (currentAsyncTask != null) {
                    currentAsyncTask.setSaveAllFrames(isChecked, mBinding.textViewCountAllFrames);
                    if(isChecked) {
                        mBinding.checkBoxSaveWhenTouch.setChecked(false);
                    }
                }
            }
        });
        mBinding.checkBoxSaveWhenTouch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked) {
                    mBinding.switchCaptureFrames.setChecked(false);
                    mBinding.checkBoxSaveAllImages.setChecked(false);
                    if (currentAsyncTask != null && !currentAsyncTask.isCancelled()) {
                        currentAsyncTask.cancel(false);
                    }
                }
            }
        });
        mBinding.SQWindow.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if(event.getAction() == MotionEvent.ACTION_DOWN){
                    mIsTouching = true;
                    float x = event.getX();
                    float y = event.getY();
                    int x_ratio_x100 = (int)(x * 100 / mBinding.SQWindow.getWidth());
                    int y_ratio_x100 = (int)(y * 100 / mBinding.SQWindow.getHeight());
                    List<Point> listTouchPoint = new ArrayList<>();
                    listTouchPoint.add(new Point(x_ratio_x100, y_ratio_x100));
                    RbsUtil.setTouchPosToSDK(1, listTouchPoint);
                    if(mBinding.checkBoxSaveWhenTouch.isChecked()) {
                        if(currentAsyncTask != null && !currentAsyncTask.isCancelled()){
                            if(currentAsyncTask.isSavingTouchFrame()){
                                return true;
                            }
                            currentAsyncTask.cancel(false);
                        }
                        mBinding.imageView8bit.setBackgroundResource(R.drawable.bg_border);
                        mBinding.imageViewRaw.setBackgroundResource(R.drawable.bg_border);
                        RbsUtil.setTouchDownEventToSDK(mBinding.textViewShowTemperatureInfo);
                        currentAsyncTask = new CaptureFrameAsyncTask();
                        currentAsyncTask.setSaveTouchFrame(true, mBinding.textViewCountTouchFrames, x_ratio_x100, y_ratio_x100, 1);
                        currentAsyncTask.execute();
                    }
                    return true;
                }else if(event.getAction() == MotionEvent.ACTION_UP) {
                    mIsTouching = false;
                    //RbsUtil.setTouchPosToSDK(0, 0);
                    mLib.extraApi(ExperimentDef.PID_HOST_TOUCH, ExperimentDef.CMD_HOST_TOUCH_RESET_TOUCHING, null, null, null);
                    return true;
                }
                return false;
            }
        });
        liveImageViewer = new LiveImageViewer(this);
        liveImageViewer.setImageView(mBinding.imageView8bit, mBinding.imageViewRaw, mBinding.statisticsDataView);
        mBinding.switchCaptureFrames.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                EgisLog.i(TAG, "onCheckedChanged " + isChecked);
                if (isChecked) {
                    mBinding.checkBoxSaveWhenTouch.setChecked(false);
                    getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
                    if (currentAsyncTask != null && !currentAsyncTask.isCancelled()) {
                        currentAsyncTask.cancel(false);
                    }
                    CaptureFrameAsyncTask asyncTask = new CaptureFrameAsyncTask();
                    currentAsyncTask = asyncTask;
                    currentAsyncTask.setSaveAllFrames(mBinding.checkBoxSaveAllImages.isChecked(), mBinding.textViewCountAllFrames);
                    currentAsyncTask.execute();

                    mBinding.imageView8bit.setBackgroundResource(R.drawable.bg_border);
                    mBinding.imageViewRaw.setBackgroundResource(R.drawable.bg_border);
                } else {
                    getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
                    if (currentAsyncTask != null) {
                        currentAsyncTask.cancel(false);
                    }
                }

                // some times S10 will display the bar
                Common.hideSystemUI(LiveViewActivity.this);
            }
        });

        switch (sensorSize){
            case SENSOR_SIZE_LARGE:
                FileIoUtils.SQWindowPos sqWindowPos = FileIoUtils.loadSQWindowLayoutData();
                ExperimentUtils.setSQPressArea(mBinding.SQWindowContainer, sqWindowPos.x, sqWindowPos.y,
                        sqWindowPos.width, sqWindowPos.height, getResources().getDisplayMetrics());
                mBinding.ICWindowVerifyContainer.setVisibility(View.GONE);
                mBinding.SQWindowContainer.setVisibility(View.VISIBLE);
                ConstraintSet set = new ConstraintSet();
                float density = getResources().getDisplayMetrics().density;
                set.constrainHeight(R.id.imageView8bit, (int)(200 * density));
                set.constrainWidth(R.id.imageView8bit, (int)(200 * density));
                set.constrainHeight(R.id.imageViewRaw, (int)(200 * density));
                set.constrainWidth(R.id.imageViewRaw, (int)(200 * density));


                set.connect(R.id.imageView8bit, TOP, R.id.btnExit, BOTTOM, 10);
                set.connect(R.id.imageViewRaw, TOP, R.id.btnExit, BOTTOM, 10);
                set.connect(R.id.imageViewRaw, LEFT, R.id.live_layout, LEFT, 10);
                set.connect(R.id.imageView8bit, RIGHT, R.id.live_layout, RIGHT, 10);
                set.applyTo(mBinding.liveLayout);


                break;
            case SENSOR_SIZE_NORMAL:
                FileIoUtils.WindowPos icWindowPos = FileIoUtils.loadIcWindowLayoutData();
                ExperimentUtils.setPressAreaLeftTop(mBinding.ICWindowVerifyContainer, icWindowPos.x, icWindowPos.y, getResources().getDisplayMetrics());
                mBinding.ICWindowVerifyContainer.setVisibility(View.VISIBLE);
                mBinding.SQWindowContainer.setVisibility(View.GONE);
                mGreenGradation = new CircleShaderView(this);
                mGreenGradation.setCircleCanvas(mBinding.ICWindowVerifyContainer.getLayoutParams().width, mBinding.ICWindowVerifyContainer.getLayoutParams().height);
                float diameter = AppSettingsPreference.getPressAreaDiameter(mAppSettings.getPressAreaDiameterIndex());
                mGreenGradation.setRadius(diameter/2 + CircleShaderView.GRADATION_RADIUS);
                mBinding.ICWindowVerifyContainer.addView(mGreenGradation);
                mLightDot = new LightDotControl(mBinding.enrollGif, mGreenGradation, mAppSettings);
                break;
            default:
                break;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        mBinding.switchCaptureFrames.setChecked(false);
        super.onPause();
    }

    public class CaptureFrameAsyncTask extends AsyncTask<Void, LiveImageInfo, Integer> {
        private boolean running;
        private int countAllFrames;
        private boolean saveAllFrames;
        private WeakReference<TextView> textViewCountAllFrames;
        private int countCurrentFrame;
        private boolean saveCurrentFrame;
        private boolean saveWhenTouch;
        private WeakReference<TextView> textViewCountCurrentFrame;
        private WeakReference<TextView> textViewCountTouchFrame;

        // variables for touch capture
        private int x_ratio_x100;
        private int y_ratio_x100;
        private int totalTouchFrameCount;
        private int targetCaptureCount;
        private int savedTouchFrameCount;

        public void setSaveAllFrames(boolean save, TextView textViewCount) {
            EgisLog.d("RBS", "saveCurrent");
            this.saveAllFrames = save;
            this.textViewCountAllFrames = new WeakReference<>(textViewCount);
            countAllFrames = Integer.valueOf(textViewCount.getText().toString());
        }

        public void setSaveCurrentFrame(boolean save, TextView textViewCount) {
            EgisLog.d(TAG, "saveCurrent");
            this.saveCurrentFrame = save;
            this.textViewCountCurrentFrame = new WeakReference<>(textViewCount);
            countCurrentFrame = Integer.valueOf(textViewCount.getText().toString());
        }

        public void setSaveTouchFrame(boolean save, TextView textViewCount, int x_ratio_x100, int y_ratio_x100, int captureCount) {
            EgisLog.d(TAG, "setSaveTouchFrame saveWhenTouch = "+saveWhenTouch+", captureCount = "+captureCount);
            if(saveWhenTouch == true){
                return;
            }
            this.saveWhenTouch = save;
            this.textViewCountTouchFrame = new WeakReference<>(textViewCount);
            this.x_ratio_x100 = x_ratio_x100;
            this.y_ratio_x100 = y_ratio_x100;
            this.targetCaptureCount = captureCount;
            totalTouchFrameCount = Integer.valueOf(textViewCount.getText().toString());
        }

        public boolean isSavingTouchFrame(){
            return saveWhenTouch;
        }

        CaptureFrameAsyncTask() {
            if (mainFileSession == null) {
                mainFileSession = new MainFileSession(DemoToolApp.getMyDataRoot());
            }
            targetCaptureCount = 1;
        }
        @Override
        protected Integer doInBackground(Void... voids) {
            running = true;
            boolean useIPP = true;
            boolean isUsingTouchGetImage = false ;
            if (mIsTouching ) {
                isUsingTouchGetImage = true ;
            }
            int count = 0;
            if(saveWhenTouch){
                int captureCount = 0;
                savedTouchFrameCount = 0;
                while (captureCount < this.targetCaptureCount){
                    LiveImageInfo imageInfo = liveImageViewer.runGetLiveImageInfo(useIPP, mIsTouching);
                    imageInfo.sequential_number = totalTouchFrameCount / targetCaptureCount;
                    publishProgress(imageInfo);
                    captureCount++;
                }
                return captureCount;
            }else{
                do {
                    LiveImageInfo imageInfo = liveImageViewer.runGetLiveImageInfo(useIPP, mIsTouching);
                    imageInfo.sequential_number = count;
                    publishProgress(imageInfo);
                    count++;
                    try {
                        Thread.sleep(50);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    if (isCancelled()) {
                        liveImageViewer.updateBDS();
                        break;
                    }
                } while (true);
            }
            if ( isUsingTouchGetImage == true ) {
                Log.e(TAG, "@@ ONLY send one image to BDS") ;
                mIsTouching = false ; // 每次按下只給一張進BDS，其他不要
            }
            return Integer.valueOf(count);
        }

        private boolean needSaveFile() {
            return saveAllFrames || saveCurrentFrame || saveWhenTouch ;
        }

        @Override
        protected void onProgressUpdate(LiveImageInfo... values) {
            liveImageViewer.updateImageView(values[0]);
            //tvDebug.setText("Raw: " + AuoOledFingerprintReceiverHelper.DEBUG_LAST_AVG );



            if ( AuoOledFingerprintReceiverHelper.G_DEMO_INC_DEFINE_IN_MK ) {
                // if Google: Save Imo Raw only
                if (needSaveFile()) {
                    prepareSaveFile_for_google_imo( values[0]) ;
                }
            }
            else {
                // if QA: save the bin
                prepareSaveFile(values[0]);
            }




            if (textViewCountAllFrames != null && textViewCountAllFrames.get() != null) {
                textViewCountAllFrames.get().setText(String.valueOf(countAllFrames));
            }
            if (textViewCountCurrentFrame != null && textViewCountCurrentFrame.get() != null) {
                textViewCountCurrentFrame.get().setText(String.valueOf(countCurrentFrame));
            }
            if (textViewCountTouchFrame != null && textViewCountTouchFrame.get() != null) {
                textViewCountTouchFrame.get().setText(String.valueOf(totalTouchFrameCount));
            }

            if (saveCurrentFrame)  saveCurrentFrame = false;
            if (saveWhenTouch && this.savedTouchFrameCount >= this.targetCaptureCount) saveWhenTouch = false;
            super.onProgressUpdate(values);
        }

        @Override
        protected void onCancelled(Integer integer) {
            EgisLog.d(TAG, "onCancelled, count " + integer.intValue());
            running = false;
            if (mainFileSession != null) {
                mainFileSession.Destroy();
                mainFileSession = null;
            }
            super.onCancelled(integer);
        }

        public boolean isRunning() {
            return running;
        }

        private void prepareSaveFile(LiveImageInfo imageInfo) {
            StringBuilder builder = new StringBuilder();
            final String RootPath = DemoToolApp.getMyDataRoot();
            String FolderPath;
            String FileName;
            Long tsLong = System.currentTimeMillis()/1000;
            String TimeStamp = tsLong.toString();
            int indexOfVersionNumEnd = BuildConfig.VERSION_NAME.indexOf('-');
            int cx = imageInfo.reservered_1;
            int cy = imageInfo.reservered_2;

            if(!saveWhenTouch) {
                FileName = builder.append("RBS-")
                        .append(BuildConfig.VERSION_NAME, 0, indexOfVersionNumEnd)
                        .append("_").append(TimeStamp)
                        .append("_").append(imageInfo.sequential_number)
                        .append("_cx_").append(cx)
                        .append("_cy_").append(cy).toString();
            }else{
                Calendar cal = Calendar.getInstance(Locale.getDefault());
                cal.setTimeInMillis(tsLong * 1000L);
                String date = DateFormat.format("yyyy-MM-dd-hh-mm-ss", cal).toString();
                FileName = builder.append("RBS-")
                        .append(BuildConfig.VERSION_NAME, 0, indexOfVersionNumEnd)
                        .append("_").append(date)
                        .append("_touch_").append(imageInfo.sequential_number)
                        .append("_frame_").append(savedTouchFrameCount+1)
                        .append("_cx_").append(cx)
                        .append("_cy_").append(cy).toString();
            }

            String [] subFolders = { "liveview_all", "liveview_frame", "liveview_touch"};

            for (int i = 0; i < subFolders.length; i++) {
                if (i == 0 && !saveAllFrames) continue;
                if (i == 1 && !saveCurrentFrame) continue;
                if (i == 2 && !saveWhenTouch) continue;

                String subFolder = subFolders[i];
                boolean saveSuccess;

                builder.setLength(0);
                FolderPath = builder.append(RootPath)
                        .append("/").append(subFolder)
                        .append("/bin")
                        .append("/").append(FileName)
                        .append(".bin").toString();

                saveSuccess = saveFile(FolderPath, imageInfo.getImage());

                builder.setLength(0);
                FolderPath = builder.append(RootPath)
                        .append("/").append(subFolder)
                        .append("/raw")
                        .append("/").append(FileName)
                        .append(".bin").toString();
                saveSuccess = saveFile(FolderPath, imageInfo.getRawImage(false));

                if (saveSuccess) {
                    switch (i) {
                        case 0:
                            countAllFrames++;
                            break;
                        case 1:
                            countCurrentFrame++;
                            break;
                        case 2:
                            totalTouchFrameCount++;
                            savedTouchFrameCount++;
                            break;
                        default:
                            EgisLog.e(TAG, "unexpected flow");
                            break;
                    }
                }
            }
        }



        // todo: head -c 120 test.imo | base64
        public String imo_header_backup_base64_8bit  = "MUpCT5cAAABJTUFHRQAAAAAAAAAAAAAAAQACAECcAABWAAAAyADIAAgAAAAGAAAABQAAAP//AAAAAAAAZABkAP//AAAAAAAAAADFLwAAYy8ZAEBZAQABABQAAAAAAAAAAQAAAAAA//8AAAAAAAAAAAAAAAAAAAAA" ;
        public String imo_header_backup_base64_16bit = "MUpCT5cAAABJTUFHRQAAAAAAAAAAAAAAAQACAIA4AQBWAAEAyADIABAAAAACAAAAAQAAAP//AAAAAP//ZABkAP//AAAAAAAAAACmOxwc0z4ZAEBZAQABABQAAAAAAAAAAQAAAAEA//8AAAAAAAAAAAAAAAAAAAAA" ;


        public java.text.DateFormat et760rbsTimeFormat = new SimpleDateFormat("yyMMdd-HHmmss-SS");
        private void prepareSaveFile_for_google_imo(LiveImageInfo imageInfo) {

            final String RootPath = DemoToolApp.getMyDataRoot();
            String FolderPath;
            String FileName;

            Long currentTime = (System.currentTimeMillis());
            Date date = new Date();
            date.setTime(currentTime);
            String yymmdd_hhmmss_ss = et760rbsTimeFormat.format(date);

            FileName = "liveview_" + yymmdd_hhmmss_ss + ".imo" ;

            FolderPath = RootPath + "image_liveview/" ;
            File file = new File(FolderPath);
            if (!file.exists()) {
                file.mkdirs();
            }

            File myDrawFile;
            FileObj fileObj;
            myDrawFile = new File(file.getAbsolutePath() + "/" + FileName) ;

            byte [] imo_header_8bit = Base64.decode(imo_header_backup_base64_8bit, Base64.DEFAULT) ;
            byte [] imo_header_16bit = Base64.decode(imo_header_backup_base64_16bit, Base64.DEFAULT) ;

            byte [] raw_image = imageInfo.getRawImage(false) ;
            byte [] bin_image = imageInfo.getImage() ;

            byte [] imo_8bit = new byte[ imo_header_8bit.length + bin_image.length] ;
            byte [] imo_16bit = new byte[ imo_header_16bit.length + raw_image.length] ;

            // save for 8 bit
            System.arraycopy(imo_header_8bit, 0, imo_8bit, 0, imo_header_8bit.length) ;
            System.arraycopy(bin_image, 0, imo_8bit, imo_header_8bit.length, bin_image.length) ;
            saveFile( FolderPath + "bin/" + FileName.replace(".imo", ".imo"), imo_8bit );

            // save for 16 bit
            if ( raw_image != null ) {
                System.arraycopy(imo_header_16bit, 0, imo_16bit, 0, imo_header_16bit.length) ;
                System.arraycopy(raw_image, 0, imo_16bit, imo_header_16bit.length, raw_image.length) ;
                saveFile( FolderPath + "debug/" + FileName.replace(".imo", ".imo"), imo_16bit );
            }


            if (false) {
                // save both 16 and 8bit imo
                byte [] raw_event_obj = imageInfo.getRawEventObj() ;
                Log.e(TAG, "Raw Image byte length=" + raw_event_obj.length) ;
                saveFile( FolderPath + FileName.replace(".imo", "_8and16.imo"), raw_event_obj );
            }

            int folder_types = 3 ; // for old frame counter use
            for (int i = 0; i < 3; i++) {
                if (i == 0 && !saveAllFrames) continue;
                if (i == 1 && !saveCurrentFrame) continue;
                if (i == 2 && !saveWhenTouch) continue;
                switch (i) {
                    case 0:
                        countAllFrames++;
                        break;
                    case 1:
                        countCurrentFrame++;
                        break;
                    case 2:
                        totalTouchFrameCount++;
                        savedTouchFrameCount++;
                        break;
                    default:
                        EgisLog.e(TAG, "unexpected flow");
                        break;
                }
            }

        }

        private boolean saveFile(String path, byte [] img) {
            File file = new File(path);
            if (!file.getParentFile().exists()) {
                boolean makeDir = file.getParentFile().mkdirs();
                if (!makeDir)  return false;
            }
            FileObj fileObj = new FileObj(file, img);
            if(mainFileSession != null) {
                mainFileSession.getSaveFileHelper().Save(fileObj);
            }
            return true;
        }
    }

}
