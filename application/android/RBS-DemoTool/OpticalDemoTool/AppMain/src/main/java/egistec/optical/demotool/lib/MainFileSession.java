package egistec.optical.demotool.lib;

import android.util.Log;

import java.io.File;
import java.util.ArrayList;

public class MainFileSession {
    final String TAG = "RBS_" + MainFileSession.class.getSimpleName();
    private int mainFileSessionIndex;
    private String mainFilenameInEVTool;
    private ArrayList<FileObj> tryMatchFiles;
    private SaveFileHelper saveFileHelper;
    private String rootPath;
    final String ROOT_TRY_FOLDER = "image_try";
    final String [] ImageTypeFolders = { "image_raw/", "image_bin/" };

    public MainFileSession(String rootPath) {
        this.rootPath = rootPath;
        mainFilenameInEVTool = "";
        tryMatchFiles = new ArrayList();
        saveFileHelper = new SaveFileHelper();
        saveFileHelper.Create();
    }

    public void Destroy() {
        saveFileHelper.Destroy();
    }

    public void newMainFileSession(int index_fingeron) {
        Log.i(TAG, "newMainFileSession " + index_fingeron);
        if (tryMatchFiles.size() != 0) {
            Log.e(TAG, "unexpected flow. tryMatchFiles is not zero");
            for (FileObj fileObj1: tryMatchFiles) {
                FileObj fileObjNew = moveInsideTryFolder(fileObj1, "");
                saveFileHelper.Save(fileObjNew);
            }
        }
        mainFileSessionIndex = index_fingeron;
        mainFilenameInEVTool = "";
        tryMatchFiles.clear();
    }

    public void addFileObj(FileObj fileObj, boolean isMainFile) {
        if (fileObj == null) {
            Log.e(TAG, "unexpected flow. fileObj is null");
            return;
        }
        if (isMainFile && !mainFilenameInEVTool.isEmpty() &&
                mainFilenameInEVTool.compareTo(extractMainFileName(fileObj)) != 0) {
            Log.e(TAG, "unexpected flow. mainFilenameInEVTool = " + mainFilenameInEVTool);
            Log.e(TAG, "unexpected flow. fileObj. = " + fileObj.getSaveFile().getName());
        }
        if (!isMainFile) {
            if (mainFilenameInEVTool.isEmpty()) {
                tryMatchFiles.add(fileObj);
                int size = tryMatchFiles.size();
                Log.d(TAG, String.format("Array add (%d): %s", size, fileObj.getSaveFile().getName()));
            } else {
                FileObj fileObjNew = moveInsideTryFolder(fileObj, mainFilenameInEVTool);
                saveFileHelper.Save(fileObjNew);
            }
            return;
        }
        mainFilenameInEVTool = extractMainFileName(fileObj);
        Log.i(TAG, "mainFileName " + mainFilenameInEVTool);
        for (FileObj fileObj1: tryMatchFiles) {
            FileObj fileObjNew = moveInsideTryFolder(fileObj1, mainFilenameInEVTool);
            saveFileHelper.Save(fileObjNew);
        }
        saveFileHelper.Save(fileObj);
        tryMatchFiles.clear();
    }

    public SaveFileHelper getSaveFileHelper() {
        return saveFileHelper;
    }

    private FileObj moveInsideTryFolder(FileObj fileObj, String tryFolderName) {
        String evToolFolder = fileObj.getSaveFile().getParent();
        String fileName = fileObj.getSaveFile().getName();
        String filePath = fileObj.getSaveFile().getPath();
        String path = null;
        String tryFolderRoot = rootPath + "/" + ROOT_TRY_FOLDER + "/";
        for (int i = 0; i < ImageTypeFolders.length; i++) {
            if (filePath.contains(ImageTypeFolders[i])) {
                path = tryFolderRoot + ImageTypeFolders[i] + tryFolderName + "/" + fileName;
                break;
            }
        }
        if (path == null) {
            Log.w(TAG, "unexpected flow. filePath=" + filePath);
            path = tryFolderRoot + "/" + tryFolderName + "/" + fileName;
        }
        return new FileObj(new File(path), fileObj.getContent());
    }

    private String extractMainFileName(FileObj fileObj) {
        String fileName = removeExtension(fileObj.getSaveFile().getName());
        int [] underscore_pos = new int[4];
        underscore_pos[0] = 0;
        for (int i = 0; i < underscore_pos.length - 1; i++) {
            underscore_pos[i+1] = fileName.indexOf("_", underscore_pos[i] + 1);
            if (underscore_pos[i+1] < 0) {
                break;
            }
        }
        int firstIndex = 0;
        int lastIndex = fileName.length();

        if (underscore_pos[1] > 0) {
            firstIndex = underscore_pos[1] + 1;
            for (int i = 2; i < underscore_pos.length; i++) {
                if (underscore_pos[i] >= 0) {
                    lastIndex = underscore_pos[i];
                } else {
                    break;
                }
            }
            if (lastIndex - firstIndex > 0) {
                fileName = fileName.substring(firstIndex, lastIndex);
            }
        }
        return fileName;
    }

    public static String removeExtension(String fileName) {
        return replaceExtension(fileName, ".bin", "");
    }

    public static String replaceExtension(String filePath, String Ori, String New) {
        int idx = filePath.lastIndexOf(Ori);
        if (idx >= 0) {
            return filePath.replace(filePath.substring(idx, idx + Ori.length()), New);
        }
        return filePath;
    }
}
