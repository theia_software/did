package egistec.optical.demotool.otg.otgsensor;

import egistec.optical.demotool.otg.utility.USBWrapper;

public abstract class SensorBase {
    // Define eventId in eventHandler
    public static final int SENSOR_CONTROL_WRITE_REGISTER = 10000;
    public static final int SENSOR_CONTROL_READ_REGISTER  = 10001;
    public static final int SENSOR_CONTROL_GET_FRAME      = 10002;
    public static final int SENSOR_CONTROL_STANDBY        = 10003;
    public static final int SENSOR_CONTROL_WAKE_UP        = 10004;
    public static final int SENSOR_CONTROL_GET_IMAGE      = 10005;
    public static final int SENSOR_CONTROL_SPI_WRITE_READ    = 10007;
    public static final int SENSOR_CONTROL_SET_SPI_CLK      = 10008;

    protected static final int OK = 0;
    protected static final int ERROR_COMMAND_FAIL = 1;

    public abstract int initialize(USBWrapper serialDevice);
    public abstract int eventHandler(int eventId, int value1, int value2, byte[] byteBuffer);
}

