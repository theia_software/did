package egistec.optical.demotool.otg.otgsensor;

import android.hardware.usb.UsbDeviceConnection;
import android.hardware.usb.UsbEndpoint;
import android.util.Log;
//import android.util.Log;
import egistec.optical.demotool.igistec.Common;
import egistec.optical.demotool.igistec.SPIUtility;
import rbs.egistec.com.fplib.api.EgisLog;

public class OtgIO_V9 implements IOtgIO{
    private static final String TAG = "OTG-IOV9";
    UsbDeviceConnection conn;
    UsbEndpoint epIn;
    UsbEndpoint epOut;
    int m_dummyLen;
    boolean m_fastOPCode;

    OtgIO_V9(UsbDeviceConnection conn, UsbEndpoint epin, UsbEndpoint epout){
        EgisLog.d(TAG, "create OtgIO_V9");
        this.conn = conn;
        this.epIn = epin;
        this.epOut = epout;
        this.m_dummyLen = 0;
        this.m_fastOPCode = false;
        byte [] value = new byte[100];
        //set_spi_mux((byte)0);
        this.Set_SPI_CLK((byte)0, (byte) 19) ;

//        this.WriteRegister((short) 0, (byte) 0xFD) ;
//        this.WriteRegister((short) 2, (byte) 0xFD) ;
//        this.WriteRegister((short) 4, (byte) 0xFD) ;
//
//
//
//        if ((RESULT_OK == this.ReadRegister((short) 0, value)))
//        {
//            EgisLog.d(TAG,String.format("id: %02X, %02X, %02X", value[0], value[1], value[2]));
//            // ET702: 0x00, 0x07, 0x02
//            // FPGA(ET901F): 0x00, 0x09, 0x01
//            // ET902A: 0x09, 0x02, 0x00
//
//            //int devID = (value[1] << 8)|(value[2]); // 0x0901 or 0x0702
//            Log.e(TAG, "@@ Teest Read Register=" + Common.byteArrayToString(value)) ;
//            if (value[0] != 7 || value[0] == 9)
//            {
//                this.m_dummyLen = 1;
//            }
//            if (value[0] == 0x09 && value[1] == 0x02)
//            {
//                this.m_fastOPCode = true;
//            }
//        }

        Log.e(TAG, "設定了global OTG IO") ;
        SPIUtility.otgio = this ; // 設定GLOBAL後其他人才可跑
        EgisLog.e(TAG, "init: SPI read dummy = "+this.m_dummyLen+", fast mode OP ="+this.m_fastOPCode);
    }

    @Override
    public int ReadRegister(short address, byte[] value) {
        if (epIn == null || epOut == null || conn == null) {
            return RESULT_FAIL;
        }
        int readLen = value.length;
        if (readLen > 256)
        {
            readLen = 256;
        }
        SCSICommandBlock cmdBlock = new SCSICommandBlock(readLen, (byte) 0x80);
        byte[] commandBlock = new byte[16];
        commandBlock[0] = (byte) 0xEE;
        commandBlock[1] = (byte) 0x00;
        commandBlock[2] = (byte) 0x00;
        commandBlock[3] = (byte) 0xFF;
        commandBlock[4] = (byte) 0x00;
        commandBlock[5] = (byte) 0x00;
        commandBlock[6] = (byte) (0x02 + m_dummyLen);  //write length
        commandBlock[7] = (byte) 0xFF;
        commandBlock[8] = (byte) 0x00;
        commandBlock[9] = (byte) ((readLen & 0xFF00) >>8);
        commandBlock[10] = (byte) (readLen & 0xFF); //read length
        commandBlock[11] = (byte) 0x01;
        commandBlock[12] = m_fastOPCode? (byte)0x28:(byte) 0x22; //OP code
        commandBlock[13] = (byte) address;
        commandBlock[14] = (byte) 0x00; //dummy

        cmdBlock.setCommandBlock(commandBlock);
        byte[] command = cmdBlock.getCBWBuffer();

        int response = conn.bulkTransfer(epOut, command, command.length, 500);

        byte status = 0;
        if (response == 31) {
            response = conn.bulkTransfer(epIn, value, readLen, 500);
            if (response != readLen) status = 1;
        } else {
            status = 1;
        }

        SCSICommandStatus cmdStatus = new SCSICommandStatus(response, status);
        byte[] commandStatus = cmdStatus.getCSWBuffer();
        response = conn.bulkTransfer(epIn, commandStatus, commandStatus.length, 500);

        //Log.d(TAG, String.format("read reg 0x%02X = 0x%02X", address, value[0]));
        if (response == 13) {
            return RESULT_OK;
        } else {
            return RESULT_FAIL;
        }
    }

    @Override
    public int WriteRegister(short address, byte value) {
        if (epIn == null || epOut == null || conn == null) {
            return RESULT_FAIL;
        }

        SCSICommandBlock cmdBlock = new SCSICommandBlock(0, (byte) 0x00);
        byte[] commandBlock = new byte[16];
        commandBlock[0] = (byte) 0xEE;
        commandBlock[1] = (byte) 0x00;
        commandBlock[2] = (byte) 0x00;
        commandBlock[3] = (byte) 0xFF;
        commandBlock[4] = (byte) 0x00;
        commandBlock[5] = (byte) 0x00;
        commandBlock[6] = (byte) 0x03;  //write length
        commandBlock[7] = (byte) 0xFF;
        commandBlock[8] = (byte) 0x00;
        commandBlock[9] = (byte) 0x00;
        commandBlock[10] = (byte) 0x00; //read length
        commandBlock[11] = (byte) 0x01;
        commandBlock[12] = (byte) 0x24; //OP code
        commandBlock[13] = (byte) address;
        commandBlock[14] = value; //write value

        cmdBlock.setCommandBlock(commandBlock);
        byte[] command = cmdBlock.getCBWBuffer();
        int response = conn.bulkTransfer(epOut, command, command.length, 500);
        byte status = 0;
        if (response != 31) {
            status = 1;
        }

        SCSICommandStatus cmdStatus = new SCSICommandStatus(response, status);
        byte[] commandStatus = cmdStatus.getCSWBuffer();

        response = conn.bulkTransfer(epIn, commandStatus, commandStatus.length, 500);;

        if (response == 13) {
            return RESULT_OK;
        } else {
            return RESULT_FAIL;
        }
    }

    @Override
    public int FetchImage(byte[] data, int width, int height) {
        if (epIn == null || epOut == null || conn == null) return -1;
        int pageNum = 0;
        final int pagelen = 16384;
        byte[] buffer = new byte[pagelen];
        int len = 0;
        int ret = 1;
        boolean lastest;
        int len_to_read;
        boolean first = true;
        while (len < data.length) {
            if(data.length - len > pagelen){
                len_to_read = pagelen;
                lastest = false;
            }else{
                len_to_read = data.length -len;
                lastest = true;
            }

            ret = GetRawImage(buffer, pageNum, len_to_read, lastest, first);
            if (ret != RESULT_OK) {
                EgisLog.e(TAG, "GetRawImage fail");
                return RESULT_FAIL;
            }
            first = false;

            len += len_to_read;
            System.arraycopy(buffer, 0, data, pageNum * pagelen, len_to_read);
            pageNum++;
        }
        EgisLog.d(TAG, "read image(size="+data.length+")");
        return RESULT_OK;
    }

    @Override
    public int Set_SPI_CLK(byte mode, byte clk) {
        if (epIn == null || epOut == null || conn == null) {
            return RESULT_FAIL;
        }

        SCSICommandBlock cmdBlock = new SCSICommandBlock(0, (byte) 0x00);
        byte[] commandBlock = new byte[16];
        commandBlock[0] = (byte) 0xEE;
        commandBlock[1] = (byte) 0x03;
        commandBlock[2] = mode;
        commandBlock[3] = clk;


        cmdBlock.setCommandBlock(commandBlock);
        byte[] command = cmdBlock.getCBWBuffer();
        int response = conn.bulkTransfer(epOut, command, command.length, 500);
        byte status = 0;
        if (response != 31) {
            status = 1;
        }

        SCSICommandStatus cmdStatus = new SCSICommandStatus(response, status);
        byte[] commandStatus = cmdStatus.getCSWBuffer();

        response = conn.bulkTransfer(epIn, commandStatus, commandStatus.length, 500);
        EgisLog.d(TAG, "set spi mode: "+String.format("%02X", mode)+", clk: "+String.format("%02X", clk));
        if (response == 13) {
            return RESULT_OK;
        } else {
            return RESULT_FAIL;
        }
    }

    @Override
    public int SPI_Write_Read(byte[] writeBuf, int writeLen, byte[] readBuf, int readLen) {
        int result;
        if (writeLen <= 4)
        {
            if (readLen <= 16384)
            {
                return SPI_Write_Read_HalfDuplex_16K(writeBuf, writeLen, readBuf, readLen, (byte)1);
            }
            // readLen > 16384
            readLen = 16384;
            result = SPI_Write_Read_HalfDuplex_16K(writeBuf, writeLen, readBuf, 16384, (byte)0);
            if (result != RESULT_OK)
            {
                return result;
            }
            readLen -= 16384;
        }
        else if (writeLen > 0)
        {
            if (readLen == 0)
            {
                return SPI_Write(writeBuf, writeLen, (byte)1);
            }
            result = SPI_Write(writeBuf, writeLen, (byte)0);
            if (result != RESULT_OK)
            {
                return result;
            }
        }

        return SPI_Read(readBuf, readLen);
    }

    @Override
    public int ReadRegister32(int a, byte[] value) {

        int writeLen = 5 ; // or 6 @@ spi speed bug
        byte [] addrBuf = Common.intToByteArray(a) ;
        // byte [] readBuf = value ;
        int readLen = value.length ;

        writeLen = writeLen + 1 ;

        byte [] writeBuf = new byte[writeLen] ;
        writeBuf[0] = 0x32 ;

        writeBuf[1] = addrBuf[0] ;
        writeBuf[2] = addrBuf[1] ;
        writeBuf[3] = addrBuf[2] ;
        writeBuf[4] = addrBuf[3] ;

        writeBuf[5] = 0 ; // 需要加一個

        SPI_Write_Read(writeBuf, writeLen, value, readLen ) ;
        Log.e(TAG, "@@ SPI 32 bit write len=" + writeLen + " data= " + Common.byteArrayToString(writeBuf));
        Log.e(TAG, "@@ SPI 32 bit read 4 = " + Common.byteArrayToString(value));

        int read_result = Common.byteArrayToInt(value) ;
        return read_result ;
    }

    @Override
    public int WriteRegister32(int a, int v) {

        byte [] writeBuf = new byte[9] ;

        byte [] addrBuf = Common.intToByteArray(a) ;
        byte [] valBuf = Common.intToByteArray(v) ;
        writeBuf[0] = 0x33 ;
        writeBuf[1] = addrBuf[0] ;
        writeBuf[2] = addrBuf[1] ;
        writeBuf[3] = addrBuf[2] ;
        writeBuf[4] = addrBuf[3] ;

        writeBuf[5] = valBuf[0] ;
        writeBuf[6] = valBuf[1] ;
        writeBuf[7] = valBuf[2] ;
        writeBuf[8] = valBuf[3] ;

        SPI_Write(writeBuf, writeBuf.length, (byte)1) ;
        return 0;
    }

    @Override
    public int Raw_SPI_Write(byte[] writeBuf, int len) {
        return SPI_Write(writeBuf, len, (byte)1) ;
    }

    @Override
    public int Raw_SPI_Write(byte[] writeBuf, int len, int cs) {
        return SPI_Write(writeBuf, len, (byte)cs) ;
    }

    @Override
    public int BurstRead(int address, byte[] buffer, int len) {
        return 0;
    }

    @Override
    public int BurstWrite(int address, byte[] buffer, int len) {
        return 0;
    }

    private int SPI_Write(byte[] writeBuf, int writeLen, byte CS_HIGH_LOW){
        int result;
        byte [] buf = new byte[16384];
        int offset = 0;
        while (writeLen > 16384)
        {
            System.arraycopy(writeBuf, offset, buf, 0,16384);
            result = SPI_Write_16K(buf, 16384, (byte)0);
            if (result != RESULT_OK)
            {
                return result;
            }
            offset += 16384;
            writeLen -= 16384;
        }
        System.arraycopy(writeBuf, offset, buf, 0, writeLen);
        return SPI_Write_16K(buf, writeLen, CS_HIGH_LOW);
    }

    private int SPI_Write_16K(byte[] writeBuf, int writeLen, byte CS_HIGH_LOW) {
        if (epIn == null || epOut == null || conn == null) {
            return RESULT_FAIL;
        }
        if (writeBuf.length < writeLen) {
            writeLen = writeBuf.length;
            EgisLog.e(TAG, "SPI_Write_Read_HalfDuplex: writeBuf not enough memory");
        }
        if (writeLen > 16384) {
            writeLen = 16384;
            EgisLog.e(TAG, "SPI_Write_Read_HalfDuplex: writeLen is over 16384");
        }

        SCSICommandBlock cmdBlock = new SCSICommandBlock(writeLen, (byte) 0x00); // OUT
        byte[] commandBlock = new byte[16];
        commandBlock[0] = (byte) 0xEE;
        commandBlock[1] = (byte) 0x01;
        commandBlock[2] = (byte) 0x00; // cs option
        commandBlock[3] = (byte) 0xFF;
        commandBlock[4] = (byte) ((writeLen & 0xFF0000) >> 16);
        commandBlock[5] = (byte) ((writeLen & 0xFF00) >> 8);
        commandBlock[6] = (byte) (writeLen & 0xFF);
        commandBlock[7] = (byte) 0xFF;
        commandBlock[8] = (byte) 0;
        commandBlock[9] = (byte) 0;
        commandBlock[10] = (byte) 0; // read length
        commandBlock[11] = CS_HIGH_LOW; // cs high/low

        cmdBlock.setCommandBlock(commandBlock);
        byte[] command = cmdBlock.getCBWBuffer();

        int response = conn.bulkTransfer(epOut, command, command.length, 500);

        byte status = 0;
        if (response == 31) {
            if (writeLen > 0) {
                response = conn.bulkTransfer(epOut, writeBuf, writeLen, 500);

                if (response != writeLen) {
                    Log.e(TAG, "@@ SPI Response:" + response + "!= writelen: " + writeLen) ;
                    status = 1;
                }
            }
        } else {
            Log.e(TAG, "@@ SPI_Write_16K Error " + response + " command len=" +  command.length) ;
            status = 1;
        }
        if (status != 0)
        {
            EgisLog.e(TAG, "@@ SPI_Write_16K error");
        }
        SCSICommandStatus cmdStatus = new SCSICommandStatus(response, status);
        byte[] commandStatus = cmdStatus.getCSWBuffer();
        response = conn.bulkTransfer(epIn, commandStatus, commandStatus.length, 500);
        if (response == 13) {
            return RESULT_OK;
        } else {
            return RESULT_FAIL;
        }
    }
    private int SPI_Read(byte[] readBuf, int readLen)
    {
        int result;
        byte [] buf = new byte[16384];
        int offset = 0;
        while (readLen > 16384)
        {
            result = SPI_Read_16K(buf, 16384, (byte)0);
            if (result != RESULT_OK)
            {
                return result;
            }
            System.arraycopy(readBuf, offset, buf, 0,16384);
            offset += 16384;
            readLen -= 16384;
        }
        result = SPI_Read_16K(buf, readLen, (byte)1);
        if (result != RESULT_OK)
        {
            return result;
        }

        //System.arraycopy(readBuf, offset, buf, 0,readLen);
        System.arraycopy(buf, offset, readBuf, 0,readLen);
        return RESULT_OK;
    }

    private int SPI_Read_16K(byte[] readBuf, int readLen, byte CS_HIGH_LOW) {

        return SPI_Write_Read_HalfDuplex_16K(null, 0, readBuf, readLen, CS_HIGH_LOW);
    }

    private int SPI_Write_Read_HalfDuplex_16K(byte[] writeBuf, int writeLen, byte[] readBuf, int readLen, byte CS_HIGH_LOW) {
        if (epIn == null || epOut == null || conn == null) {
            return RESULT_FAIL;
        }
        if(writeLen > 4) {
            writeLen = 4;
            EgisLog.e(TAG, "SPI_Write_Read_HalfDuplex: writeLen > 4");
        }
        if (readBuf.length < readLen) {
            readLen = readBuf.length;
            EgisLog.e(TAG, "SPI_Write_Read_HalfDuplex: readBuf not enough memory");
        }
        if (readLen > 16384) {
            readLen = 16384;
            EgisLog.e(TAG, "SPI_Write_Read_HalfDuplex: readLen > 16384");
        }

        SCSICommandBlock cmdBlock = new SCSICommandBlock(readLen, (byte) 0x80);
        byte[] commandBlock = new byte[16];
        commandBlock[0] = (byte) 0xEE;
        commandBlock[1] = (byte) 0x00;
        commandBlock[2] = (byte) 0x00; // cs option
        commandBlock[3] = (byte) 0xFF;
        commandBlock[4] = (byte) 0x00;
        commandBlock[5] = (byte) 0x00;
        commandBlock[6] = (byte) writeLen;
        commandBlock[7] = (byte) 0xFF;
        commandBlock[8] = (byte) ((readLen & 0xFF0000) >> 16);
        commandBlock[9] = (byte) ((readLen & 0xFF00) >> 8);
        commandBlock[10] = (byte) (readLen & 0xFF); //read length
        commandBlock[11] = CS_HIGH_LOW; // cs high/low
        for (int i = 0; i < writeLen; i++) {
            commandBlock[12+i] = writeBuf[i];
        }

        cmdBlock.setCommandBlock(commandBlock);
        byte[] command = cmdBlock.getCBWBuffer();

        int response = conn.bulkTransfer(epOut, command, command.length, 500);

        byte status = 0;
        if (response == 31) {
            if (readLen > 0)
            {
                response = conn.bulkTransfer(epIn, readBuf, readLen, 500);
                if (response != readLen) status = 1;
            }
        } else {
            Log.e(TAG, "@@ SPI Status=1") ;
            status = 1;
        }

        SCSICommandStatus cmdStatus = new SCSICommandStatus(response, status);
        byte[] commandStatus = cmdStatus.getCSWBuffer();
        response = conn.bulkTransfer(epIn, commandStatus, commandStatus.length, 500);
        if (response == 13) {
            return RESULT_OK;
        } else {
            Log.e(TAG, "@@ SPI RESULT_FAIL=1") ;
            return RESULT_FAIL;
        }
    }

    private int GetRawImage(byte[] buffer, int pagenum, int pagesize, boolean lastest, boolean first) {
        SCSICommandBlock cmdBlock = new SCSICommandBlock(pagesize, (byte) 0x80);
        byte[] commandBlock = new byte[16];
        commandBlock[0] = (byte) 0xEE;
        commandBlock[1] = (byte) 0x00;
        commandBlock[2] = (byte) 0x00;
        commandBlock[3] = (byte) 0xFF;
        commandBlock[4] = (byte) 0x00;
        commandBlock[5] = (byte) 0x00;
        byte write_len = first ? (byte)(0x01+m_dummyLen): (byte)0x00;
        commandBlock[6] = write_len;  //write length
        commandBlock[7] = (byte) 0xFF;
        commandBlock[8] = (byte) ((pagesize >> 16) & 0xFF);
        commandBlock[9] = (byte) ((pagesize >> 8) & 0xFF);
        commandBlock[10] = (byte) (pagesize & 0xFF); //read length
        byte cs = lastest ? (byte)0x01:(byte)0x00;
        commandBlock[11] = cs;
        commandBlock[12] = m_fastOPCode?(byte)0x51:(byte)0x50; //OP code
        commandBlock[13] = (byte) 0x00; //dummy

        cmdBlock.setCommandBlock(commandBlock);
        byte[] command = cmdBlock.getCBWBuffer();
        int response = conn.bulkTransfer(epOut, command, command.length, 500);
        byte status = 0;
        if (response == 31) {
            response = conn.bulkTransfer(epIn, buffer, pagesize, 500);
            if (response != pagesize) status = 1;
        } else {
            return RESULT_FAIL;
        }

        SCSICommandStatus cmdStatus = new SCSICommandStatus(response, status);
        byte[] commandStatus = cmdStatus.getCSWBuffer();
        response = conn.bulkTransfer(epIn, commandStatus, commandStatus.length, 500);
        if (response == 13) {
            return RESULT_OK;
        } else {
            return RESULT_FAIL;
        }
    }

    private int set_spi_mux(byte i) {
        byte[] cmd = new byte[2];
        cmd[0] = (byte)0xFD;
        cmd[1] = i;
        return SPI_Write(cmd, 2, (byte)1);
    }

    /**
     * 每次I
     */
    public int reset(){
        if (epIn == null || epOut == null || conn == null) {
            return RESULT_FAIL;
        }

        SCSICommandBlock cmdBlock = new SCSICommandBlock(0, (byte) 0x00);
        byte[] commandBlock = new byte[16];
        commandBlock[0] = (byte) 0xEE;
        commandBlock[1] = (byte) 0xFF;
        commandBlock[2] = 0;


        cmdBlock.setCommandBlock(commandBlock);
        byte[] command = cmdBlock.getCBWBuffer();
        int response = conn.bulkTransfer(epOut, command, command.length, 500);
        byte status = 0;
        if (response != 31) {
            status = 1;
        }

        SCSICommandStatus cmdStatus = new SCSICommandStatus(response, status);
        byte[] commandStatus = cmdStatus.getCSWBuffer();

        response = conn.bulkTransfer(epIn, commandStatus, commandStatus.length, 500);
        if (response == 13) {
            Log.e(TAG, "Reset Mcu Success") ;
            return RESULT_OK;
        } else {
            Log.e(TAG, "Reset Mcu Fail") ;
            return RESULT_FAIL;
        }
    }
}
