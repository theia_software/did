package egistec.optical.demotool.experiment;

import android.util.Log;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.nio.ShortBuffer;

import egistec.optical.demotool.lib.FPUtil;
import rbs.egistec.com.fplib.api.EgisLog;

import egistec.optical.demotool.application.DemoToolApp;

public class FileIoUtils {
    private static final String TAG = "ExperimentUtilsLog";
    private static final String COMMA_DELIMITER = ",";
    private static final String NEW_LINE_SEPARATOR = "\n";
    private static final String QUALITY_HEADER = "group,imageName,integrate_qty,extract_qty";

    public static void saveQualityCsvFileHeader(String fileName) {
        FileWriter fileWriter = null;
        try {
            fileWriter = new FileWriter(fileName);
            fileWriter.write(QUALITY_HEADER.toString());
            fileWriter.write(NEW_LINE_SEPARATOR);

        } catch (Exception e) {
            EgisLog.e(TAG, "Error in saveQualityCsvFileHeader !!! error msg = " + e.getMessage());
            e.printStackTrace();
        } finally {
            try {
                fileWriter.flush();
                fileWriter.close();
            } catch (IOException e) {
                EgisLog.e(TAG, "Error while flushing/closing fileWriter !!!");
                e.printStackTrace();
            }
        }
    }

    public static void saveQualityCsvFile(String fileName, boolean isAppend, int group, String imageName, int intQuality, int extQuality) {
        FileWriter fileWriter = null;
        try {
            fileWriter = new FileWriter(fileName, isAppend);
            fileWriter.append(Integer.toString(group));
            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append(imageName);
            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append(Integer.toString(intQuality));
            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append(Integer.toString(extQuality));
            fileWriter.append(NEW_LINE_SEPARATOR);
        } catch (Exception e) {
            EgisLog.e(TAG, "Error in saveQualityCsvFile !!! error msg = " + e.getMessage());
            e.printStackTrace();
        } finally {
            try {
                fileWriter.flush();
                fileWriter.close();
            } catch (IOException e) {
                EgisLog.e(TAG, "Error while flushing/closing fileWriter !!!");
                e.printStackTrace();
            }
        }
    }

    public static void saveByteToCsvFile(String fileName, byte[] data, int width) {
        if (data.length <= 0) {
            return;
        }
        short[] temp = new short[data.length];
        for (int i = 0; i < data.length; i++) {
            temp[i] = (short)data[i];
        }
        saveShortToCsvFile(fileName, temp, width);
    }

    public static void saveShortToCsvFile(String fileName, short[] data, int width) {
        if (data.length <= 0) {
            return;
        }
        FileWriter fileWriter = null;
        try {
            fileWriter = new FileWriter(fileName);
            //Write short list to the CSV file
            for (int i = 0; i < data.length; i++) {
                fileWriter.append(Short.toString(data[i]));
                if ((i+1)%width != 0)
                    fileWriter.append(COMMA_DELIMITER);
                else
                    fileWriter.append(NEW_LINE_SEPARATOR);
            }
            EgisLog.d(TAG, "CSV file was created successfully, name = " + fileName);
        } catch (Exception e) {
            EgisLog.e(TAG, "Error in saveShortToCsvFile !!! error msg = " + e.getMessage());
            e.printStackTrace();
        } finally {
            try {
                fileWriter.flush();
                fileWriter.close();
            } catch (IOException e) {
                EgisLog.e(TAG, "Error while flushing/closing fileWriter !!!");
                e.printStackTrace();
            }
        }
    }

    public static String[] getFileNameList(String folderPath) {
        File file = new File(folderPath);
        if (!file.exists()) {
            EgisLog.e(TAG, folderPath + " does not exist!!!");
            return null;
        }
        File[] fileList = file.listFiles();
        String[] nameList = new String[fileList.length];
        for (int i = 0; i < fileList.length; i++) {
            nameList[i] = fileList[i].getName();
        }
        return nameList;
    }

    public static short[] readImage16Bits(String readPath, int width, int height) {
        short[] image_16bits = new short [width * height];
        int fisSize = 0;
        File myInFile = new File(readPath);
        if (!myInFile.exists()) {
            EgisLog.e(TAG, readPath + " does not exist!!!");
        } else {
            FileInputStream fis = null;
            try {
                fis = new FileInputStream(myInFile);
                fisSize = fis.available();
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            if (fisSize != 0) {
                try {
                    BufferedInputStream ois = new BufferedInputStream(fis);
                    byte[] data = new byte[ois.available()];
                    if (ois.read(data) != -1) {
                        for (int i = 0, j = 0; i < data.length; i += 2, j++) {
                            image_16bits[j] = (short) ((data[i] & 0xFF) << 8 | (data[i + 1] & 0xFF));
                        }
                    }
                    ois.close();
                    fis.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return image_16bits;
    }

    public static void saveImage16Bits(String savePath, short[] image_buf) {
        if (image_buf.length <= 0) {
            return;
        }
        File myDrawFile = new File(savePath);
        FileOutputStream fos;
        try {
            fos = new FileOutputStream(myDrawFile);
            if (fos != null) {
                /* 16bit */
                ByteBuffer byteBuffer = ByteBuffer.allocate(image_buf.length * 2);
                ShortBuffer shortBuffer = byteBuffer.asShortBuffer();
                shortBuffer.put(image_buf);
                byte[] array = byteBuffer.array();
                fos.write(array);

                fos.close();
                EgisLog.d(TAG, "save image filename = " + savePath);
            }
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } finally {
        }
    }

    public static void saveImage8Bits(String savePath, byte[] image_buf) {
        if (image_buf.length <= 0) {
            return;
        }
        File myDrawFile = new File(savePath);
        FileOutputStream fos;
        try {
            fos = new FileOutputStream(myDrawFile);
            if (fos!=null) {
                /* 8bit */
                fos.write(image_buf);
                fos.close();
                Log.d(TAG, "save image filename = " + savePath);
            }
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } finally {
        }
    }

    public static boolean isFileExist(String fileName) {
        File file = new File(fileName);
        return file.exists();
    }

    public static void cleanDirectory(String name) {
        File dir = new File(name);
        if (!dir.exists() || !dir.isDirectory())
            return;
        for (File file: dir.listFiles()) {
            file.delete();
        }

    }

    public static void createDirectory(String name) {
        File file = new File(name);
        if (!file.exists()) {
            file.mkdirs();
        }
    }

    public static boolean deleteDirectory(String name) {
        File dir = new File(name);
        if (!dir.exists() || !dir.isDirectory())
            return false;
        File[] files = dir.listFiles();
        for (int i = 0; i < files.length; i++) {
            if (files[i].isDirectory()) {
                deleteDirectory(files[i].getAbsolutePath());
            } else {
                files[i].delete();
            }
        }
        return dir.delete();
    }

    public static void copyFile(String sourcePath, String destinationPath, boolean isAppend) {
        try {
            File f1 = new File(sourcePath);
            File f2 = new File(destinationPath);
            InputStream in = new FileInputStream(f1);

            OutputStream out;
            if (isAppend)
                out = new FileOutputStream(f2,true);
            else
                out = new FileOutputStream(f2);

            byte[] buf = new byte[1024];
            int len;
            while ((len = in.read(buf)) > 0) {
                out.write(buf, 0, len);
            }
            in.close();
            out.close();
            Log.d(TAG, sourcePath + " is copied to " + destinationPath);
        }
        catch (FileNotFoundException ex) {
            Log.e(TAG, ex.getMessage());
        }
        catch (IOException e) {
            Log.e(TAG, e.getMessage());
        }
    }

    public static String[] listSubDirectoryName(String directoryName) {
        File file = new File(directoryName);
        if (!file.exists())
            return null;
        File[] files = file.listFiles(new FileFilter() {
            @Override
            public boolean accept(File file) {
                return file.isDirectory();
            }
        });
        if (files.length <= 0)
            return null;
        String[] nameList = new String[files.length];
        for (int i = 0; i < nameList.length; i++) {
            nameList[i] = files[i].getName();
        }
        return nameList;
    }

    public static class WindowPos {
        public int x = 0;
        public int y = 0;
    }

    public static class SQWindowPos {
        public int x = 0;
        public int y = 0;
        public int width = 250;
        public int height = 320;
    }

    public static WindowPos loadIcWindowLayoutData() {
        int FisSize = 0;
        File myInFile = new File(DemoToolApp.getMyDataRoot() + "/layout_rbs_demo.bin");
        if (!myInFile.exists()) {
            //
            File legacyBinFile = new File("/sdcard/temp/layout_rbs_demo.bin");
            if (legacyBinFile.exists()) {
                FileIoUtils.copyFile(legacyBinFile.getPath(), myInFile.getPath(), false);
            }
        }
        WindowPos icPos = new WindowPos();
        if (myInFile.exists()) {
            FileInputStream fis = null;
            try {
                fis = new FileInputStream(myInFile);
                FisSize = fis.available();
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            if (FisSize != 0) {
                try {
                    BufferedInputStream ois = new BufferedInputStream(fis);
                    byte[] data = new byte[ois.available()];
                    if (ois.read(data) != -1) {
                        icPos.x = (data[0] & 0xFF) << 8 | (data[1] & 0xFF);
                        icPos.y = (data[2] & 0xFF) << 8 | (data[3] & 0xFF);
                    }
                    ois.close();
                    fis.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        } else {
            icPos.x = 130;
            icPos.y = 500;
        }
        return icPos;
    }

    public static void saveIcWindowLayoutData(int x, int y) {
        File file = new File(DemoToolApp.getMyDataRoot());
        if (!file.exists()) {
            file.mkdirs();
        }

        short[] temp_array = new short[2];
        temp_array[0] = (short) x;
        temp_array[1] = (short) y;

        File myDrawFile = new File(file.getAbsolutePath() + "/layout_rbs_demo.bin");
        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(myDrawFile);
            if (fos != null) {
                /* 16bit */
                ByteBuffer byteBuffer = ByteBuffer.allocate(4);
                ShortBuffer shortBuffer = byteBuffer.asShortBuffer();
                shortBuffer.put(temp_array);
                byte[] array = byteBuffer.array();
                fos.write(array);

                fos.close();
            }
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } finally {
        }
    }

    public static SQWindowPos loadSQWindowLayoutData() {
        int FisSize = 0;
        File myInFile = new File(DemoToolApp.getMyDataRoot() + "/layout_rbs_demo_sq.bin");
        if (!myInFile.exists()) {
            //
            File legacyBinFile = new File("/sdcard/temp/layout_rbs_demo_sq.bin");
            if (legacyBinFile.exists()) {
                FileIoUtils.copyFile(legacyBinFile.getPath(), myInFile.getPath(), false);
            }
        }
        SQWindowPos sqPos = new SQWindowPos();
        if (myInFile.exists()) {
            FileInputStream fis = null;
            try {
                fis = new FileInputStream(myInFile);
                FisSize = fis.available();
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            if (FisSize != 0) {
                try {
                    BufferedInputStream ois = new BufferedInputStream(fis);
                    byte[] data = new byte[ois.available()];
                    if (ois.read(data) != -1) {
                        int[] intdata = FPUtil.byteArrayTointArray(data);
                        sqPos.x = intdata[0];
                        sqPos.y = intdata[1];
                        sqPos.width = intdata[2];
                        sqPos.height = intdata[3];
                    }
                    ois.close();
                    fis.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        } else {
            // set a3 demo for g_inc default value may need adjust
            sqPos.x = 360;
            sqPos.y = 870;
            sqPos.width = 636;
            sqPos.height = 848;
        }
        return sqPos;
    }

    public static void saveSQWindowLayoutData(int x, int y, int width, int height) {
        File file = new File(DemoToolApp.getMyDataRoot());
        if (!file.exists()) {
            file.mkdirs();
        }

        int[] temp_array = new int[4];
        temp_array[0] = x;
        temp_array[1] = y;
        temp_array[2] = width;
        temp_array[3] = height;

        File myDrawFile = new File(file.getAbsolutePath() + "/layout_rbs_demo_sq.bin");
        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(myDrawFile);
            if (fos != null) {
                fos.write(FPUtil.intArrayToBytes(temp_array));
                fos.close();
            }
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } finally {
        }
    }
}
