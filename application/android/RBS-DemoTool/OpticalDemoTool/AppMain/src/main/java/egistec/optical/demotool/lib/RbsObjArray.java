package egistec.optical.demotool.lib;

import android.util.Log;

import java.nio.charset.StandardCharsets;
import java.util.Arrays;

public class RbsObjArray {
    final String TAG = "RBS_" + RbsObjArray.class.getSimpleName();
    public static final int uint32 = 4;
    public static final int uint16 = 2;
    public static final int uint8  = 1;
    public static final int OBJ_NAME_SIZE = 16;
    public static final int RBS_OBJ_DEST_SIZE = 34;
    private byte[] eventObjBytes;
    private int[] g_obj_pos = new int[1];

    public static class rbs_obj_desc_t {
        public int tag;  // RBS_OBJ1_TAG
        public int id;   // Uniqure id
        public String name;
        public int  version_major;
        public int  version_minor;
        public int  payload_size;
        public int  header_size;
    }
    rbs_obj_desc_t obj_desc;

    public final int index_start;
    public final int count;
    public final int has_more_object;
    public final byte[] rbs_obj_img;

    public RbsObjArray(Object eventObj){
        if (eventObj == null || !(eventObj instanceof byte[])) {
            Log.e(TAG, "got bad eventObj " + eventObj);
            eventObjBytes = new byte[RBS_OBJ_DEST_SIZE];
        } else {
            eventObjBytes = (byte[]) eventObj;
        }
        Log.d(TAG, "eventObjBytes size=" + eventObjBytes.length);
        g_obj_pos[0] = 0;
        obj_desc = new rbs_obj_desc_t();
        obj_desc.tag = byteArrayToInt(eventObjBytes, g_obj_pos, uint32);
        obj_desc.id = byteArrayToInt(eventObjBytes, g_obj_pos, uint32);
        obj_desc.name = byteArrayToString(eventObjBytes, g_obj_pos, OBJ_NAME_SIZE);
        obj_desc.version_major = byteArrayToInt(eventObjBytes, g_obj_pos, uint16);
        obj_desc.version_minor = byteArrayToInt(eventObjBytes, g_obj_pos, uint16);
        obj_desc.payload_size = byteArrayToInt(eventObjBytes, g_obj_pos, uint32);
        obj_desc.header_size = byteArrayToInt(eventObjBytes, g_obj_pos, uint16);

        index_start = byteArrayToInt(eventObjBytes, g_obj_pos, uint16);
        count = byteArrayToInt(eventObjBytes, g_obj_pos, uint16);
        has_more_object = byteArrayToInt(eventObjBytes, g_obj_pos, uint16);
        rbs_obj_img = getObject(obj_desc);
    }

    public byte[] getObject(rbs_obj_desc_t obj_desc) {
        int objSize = obj_desc.payload_size;
        if (objSize <= 0) {
            return new byte[0];
        }

        try {
            byte[] obj = new byte[objSize];
            System.arraycopy(eventObjBytes, RBS_OBJ_DEST_SIZE + obj_desc.header_size, obj, 0, objSize);
            return obj;
        } catch (Exception e) {
            e.printStackTrace();
            return new byte[0];
        }
    }

    private int byteArrayToInt(byte[] array, int[] pos, int byte_num) {
            int ret_int = 0;
            switch (byte_num){
                case uint32:
                    ret_int += (array[pos[0]++] & 0xff) << 0;
                    ret_int += (array[pos[0]++] & 0xff) << 8;
                    ret_int += (array[pos[0]++] & 0xff) << 16;
                    ret_int += (array[pos[0]++] & 0xff) << 24;
                    break;
                case uint16:
                    ret_int += (array[pos[0]++] & 0xff) << 0;
                    ret_int += (array[pos[0]++] & 0xff) << 8;
                    break;
            }
        return ret_int;
    }

    private String byteArrayToString(byte[] array, int[] pos, int char_num) {
        byte[] sub_array = Arrays.copyOfRange(array, pos[0], pos[0] + char_num);
        String ret_str = new String(sub_array, StandardCharsets.UTF_8);
        pos[0] = pos[0] + char_num;
        return ret_str;
    }
}
