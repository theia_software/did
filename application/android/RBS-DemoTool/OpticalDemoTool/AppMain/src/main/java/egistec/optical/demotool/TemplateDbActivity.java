package egistec.optical.demotool;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
//import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;
import rbs.egistec.com.fplib.api.EgisLog;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import egistec.optical.demotool.application.DemoToolApp;
import egistec.optical.demotool.experiment.FileIoUtils;

public class TemplateDbActivity extends AppCompatActivity {
    private static final String TAG = "RBS_TemplateDbActivity";

    public static final String EXTRA_USER_ID = "extra_user_id";

    private String PATH_CURRENT_TEMPLATE;

    private String PATH_BACKUP_TEMPLATE;

    // init by intent, changes if user replaced current template with saved template
    private String mCurrentUserId;

    // UI elements
    private Button mButtonBackup;
    private Button mButtonUse;
    private Button mButtonRemove;
    private ListView mListViewCurrentTemplate;
    private ListAdapter mListAdapterCurrentTemplate;
    private ListView mListViewSavedTemplate;
    private ListAdapter mListAdapterSavedTemplate;

    // Remember number of files in each user's template folder
    private Map<String, String> mNumFingers;

    private boolean mIsCheckedCurrentTemplate = false;
    private int mNumFingersCurrentTemplate = 0;
    private String[] mCurrentTemplateDB = {
            "(Empty)"
    };

    private String[] mSavedTemplateDBs = {
            // empty initially
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_template_database);

        PATH_CURRENT_TEMPLATE = DemoToolApp.getMyDataRoot() + File.separator +
                FPFingerActivity.TEMPLATE_PATH;
        PATH_BACKUP_TEMPLATE = DemoToolApp.getMyDataRoot() + File.separator +
                FPFingerActivity.BACKUP_TEMPLATE_PATH;

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            mCurrentUserId = bundle.getString(FPFingerActivity.EXTRA_USER_NAME);
            EgisLog.d(TAG, "onCreate(): mCurrentUserId = " + mCurrentUserId);
        }

        mNumFingers = new HashMap<>();

        //
        // find and init UI elements
        //

        mButtonBackup = (Button) findViewById(R.id.buttonBackup);
        mButtonBackup.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                mButtonBackup.setEnabled(false);

                // Backup current template
                if (backupTemplate() == 0) {
                    Toast.makeText(TemplateDbActivity.this, R.string.backup_template_db_success, Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(TemplateDbActivity.this, R.string.backup_template_db_fail, Toast.LENGTH_SHORT).show();
                }

                mButtonBackup.setEnabled(true);
                refreshUI();
            }
        });

        mButtonUse = (Button) findViewById(R.id.buttonUse);
        mButtonUse.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                mButtonUse.setEnabled(false);

                // Replace current template with selected saved template
                int[] checkedPositions = getCheckedPositions(mListViewSavedTemplate);
                if ((checkedPositions == null) || (checkedPositions.length != 1)) {
                    return;
                }

                String newUser = mSavedTemplateDBs[checkedPositions[0]].split(" ")[0];
                EgisLog.d(TAG, "Replace current template with saved template: " + newUser);
                if (useTemplate(newUser) == 0) {
                    Toast.makeText(TemplateDbActivity.this, "Successfully replaced current template with backup template User ID = " + newUser, Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(TemplateDbActivity.this, "Failed to replace current template", Toast.LENGTH_SHORT).show();
                }

                mButtonUse.setEnabled(true);
                refreshUI();
            }
        });

        mButtonRemove = (Button) findViewById(R.id.buttonRemove);
        mButtonRemove.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                mButtonRemove.setEnabled(false);
                AlertDialog mAlertDialog =
                new AlertDialog.Builder(TemplateDbActivity.this)
                        .setTitle("Remove template database(s)")
                        .setMessage("Do you really want to remove?")
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                EgisLog.d(TAG, "Dialog pressed button OK");

                                boolean currentTemplateRemoved = false;
                                int backupTemplateRemoved = 0;

                                // Delete selected template
                                if (mIsCheckedCurrentTemplate) {
                                    if (0 == removeTemplate(null)) {
                                        currentTemplateRemoved = true;
                                    }
                                }

                                int[] checkedPositions = getCheckedPositions(mListViewSavedTemplate);
                                if ((checkedPositions != null) || (checkedPositions.length > 0)) {
                                    for (int i = 0; i < checkedPositions.length; i++) {
                                        String userId = mSavedTemplateDBs[checkedPositions[i]].split(" ")[0];
                                        EgisLog.d(TAG, "");
                                        if (0 == removeTemplate(userId)) {
                                            backupTemplateRemoved++;
                                        }
                                    }
                                }

                                Toast.makeText(TemplateDbActivity.this, "Removed " +
                                        ((currentTemplateRemoved) ? "current template and " : "") +
                                        backupTemplateRemoved + " backup template(s)", Toast.LENGTH_SHORT).show();

                                mButtonRemove.setEnabled(true);
                                refreshUI();
                            }})
                        .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                EgisLog.d(TAG, "Dialog pressed button Cancel");
                                mButtonRemove.setEnabled(true);
                            }}).show();
                mAlertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(final DialogInterface arg0) {
                        if(!mButtonRemove.isEnabled()) mButtonRemove.setEnabled(true);
                    }
                });
            }
        });

        mListViewCurrentTemplate = (ListView) findViewById(R.id.listViewCurrentTemplate);
        mListViewCurrentTemplate.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
        mListViewCurrentTemplate.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) {
                    mIsCheckedCurrentTemplate = !mIsCheckedCurrentTemplate;
                    refreshButtons();
                }
            }
        });
        mListAdapterCurrentTemplate = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_checked, mCurrentTemplateDB);
        mListViewCurrentTemplate.setAdapter(mListAdapterCurrentTemplate);

        mListViewSavedTemplate = (ListView) findViewById(R.id.listViewSavedTemplateDBs);
        mListViewSavedTemplate.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
        mListViewSavedTemplate.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                refreshButtons();
            }
        });

        mListAdapterSavedTemplate = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_checked, mSavedTemplateDBs);
        mListViewSavedTemplate.setAdapter(mListAdapterSavedTemplate);
    }

    @Override
    protected void onResume() {
        EgisLog.d (TAG, "onResume()");

        refreshUI();

        super.onResume();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // when action bar's back arrow is clicked, redirect it to onBackPressed().
                onBackPressed();
                return true;
        }
        return false;
    }

    @Override
    public void onBackPressed() {
        // set current user id as activity result
        EgisLog.d(TAG, "onBackpPressed(): sending back current user ID = " + mCurrentUserId);
        Intent intent = new Intent();
        intent.putExtra(TemplateDbActivity.EXTRA_USER_ID, mCurrentUserId);
        setResult(Activity.RESULT_OK, intent);
        finish();
    }

    // Get number of fingers for each saved template
    private void gatherData() {
        mNumFingers.clear();

        String[] fileNamesBackupTemplate = new File(PATH_BACKUP_TEMPLATE).list();
        if (fileNamesBackupTemplate == null) {
            return;
        }

        for (String fileName : fileNamesBackupTemplate) {
            String userPath = PATH_BACKUP_TEMPLATE + File.separator + fileName;
            File userFile = new File(userPath);
            if (userFile == null) {
                continue;
            }
            if (userFile.isDirectory()) {
                EgisLog.d(TAG, "Found directory in backup template: " + fileName);
                mNumFingers.put(fileName, String.valueOf(getNumFingers(fileName)));
            }
        }
    }

    /**
     * Gets the checked positions in a list view.
     *
     * @param list the list view
     */
    private int[] getCheckedPositions(ListView list) {
        SparseBooleanArray positions  = list.getCheckedItemPositions();
        ArrayList<Integer> arrayList = new ArrayList<Integer>();
        for (int i = 0; i < positions.size(); i++) {
            int key = positions.keyAt(i);
            if (positions.valueAt(i)) {
                arrayList.add(key);
            }
        }
        int[] result = new int[arrayList.size()];
        for (int i = 0; i < arrayList.size(); i++) {
            result[i] = arrayList.get(i);
        }
        return result;
    }

    private void refreshButtons() {
        int[] checkedPositions = getCheckedPositions(mListViewSavedTemplate);

        EgisLog.d(TAG, "refreshButtons(): mCheckBoxCurrentTemplate.isChecked() = " + mIsCheckedCurrentTemplate);
        EgisLog.d(TAG, "refreshButtons(): Selected " + String.valueOf(checkedPositions.length) + " backup templates");
        for (int i = 0; i < checkedPositions.length; i++) {
            EgisLog.d(TAG, "refreshButtons():   => item[" + checkedPositions[i] + "] = " + mSavedTemplateDBs[checkedPositions[i]]);
        }

        // Update BACKUP button
        mButtonBackup.setEnabled((checkedPositions.length == 0) && (mNumFingersCurrentTemplate != 0));

        // Update Use button
        mButtonUse.setEnabled((!mIsCheckedCurrentTemplate) && (checkedPositions.length == 1));

        // Update Remove button
        mButtonRemove.setEnabled((mIsCheckedCurrentTemplate && ((mNumFingersCurrentTemplate != 0))) || (checkedPositions.length != 0));
    }

    private void refreshUI() {
        gatherData();

        // refresh current template
        mCurrentTemplateDB[0] = getTemplateDisplayName(null);
        mListAdapterCurrentTemplate = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_checked, mCurrentTemplateDB);
        mListViewCurrentTemplate.setAdapter(mListAdapterCurrentTemplate);
        mIsCheckedCurrentTemplate = false;

        int numSavedTemplates = mNumFingers.size();
        mSavedTemplateDBs = new String[numSavedTemplates];

        int i = 0;
        String displayString;
        for (String userId : mNumFingers.keySet()) {
            displayString = userId + " (" + mNumFingers.get(userId) + ")";
            EgisLog.d(TAG, "mSavedTemplateDBs <-" + displayString);
            mSavedTemplateDBs[i] = displayString;
            i++;
        }

        mListAdapterSavedTemplate = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_checked, mSavedTemplateDBs);
        mListViewSavedTemplate.setAdapter(mListAdapterSavedTemplate);

        // Buttons should be refreshed after the list view got re-constructed for nothing is selected
        refreshButtons();
    }

    // Find out how many fingers registered in current or saved template DB
    // If userId is 0, find in the current template DB,
    // else find in the saved template DB.
    private int getNumFingers(String userId) {
        int ret = 0;

        String path;

        if (userId == null) {
            // find in the current template DB
            path = PATH_CURRENT_TEMPLATE;
        } else {
            // find in the saved template DB
            path = PATH_BACKUP_TEMPLATE + File.separator + userId;
        }

        // calculate total files in search path
        ret = new File(path).list().length;

        return ret;
    }

    // Get the template name to be displayed on UI.
    // If userId is 0, returns the current template DB,
    // else return the corresponding name for saved template DB.
    private String getTemplateDisplayName(String userId) {
        String ret;

        int num = getNumFingers(userId);
        if (userId == null) {
            mNumFingersCurrentTemplate = num;
        }

        EgisLog.d(TAG, "getTemplateDisplayName(): userId = " + userId);
        EgisLog.d(TAG, "getTemplateDisplayName(): Finger num = " + num);

        if ((mNumFingersCurrentTemplate == 0) && (userId == null)) {
            ret = "(Empty)";
        } else {
            ret = ((userId == null) ? mCurrentUserId : userId) + " (" + String.valueOf(num) + ")";
        }

        EgisLog.d(TAG, "getTemplateDisplayName(): Display name = " + ret);

        return ret;
    }

    // Copy src dir to dest recursively
    static private int copyDir(File src, File dest) {
        int ret = -1;

        // parameter check
        if ((src == null) || (dest == null)) {
            return ret;
        }

        // do nothing if src is not directory
        if (!src.isDirectory()) {
            return ret;
        }

        // make sure dest is a directory
        if (dest.exists()) {
            if (!dest.isDirectory()) {
                return ret;
            }
        } else {
            dest.mkdir();
        }

        // do nothing if src dir is empty
        if((src.listFiles() == null) || (src.listFiles().length == 0)) {
            return ret;
        }

        // copy each file in src
        for (File file : src.listFiles()) {
            File fileDest = new File(dest, file.getName());

            if (file.isDirectory()) {
                copyDir(file, fileDest);
            } else {
                if (fileDest.exists()) {
                    fileDest.delete();
                }

                EgisLog.d(TAG, "copying " + file.toString() + " to " + fileDest.toString());
                FileIoUtils.copyFile(file.toString(), fileDest.toString(), false);
            }
        }

        // copy operations are succcessful
        ret = 0;

        return ret;
    }

    private int backupTemplate() {
        int ret = -1;

        if ((mCurrentUserId == null) || mCurrentUserId.isEmpty()) {
            EgisLog.d(TAG, "backupTemplate(): mCurrentUserId is not valid");
            return ret;
        }

        File templateDir = new File(PATH_CURRENT_TEMPLATE);
        File backupDir = new File(PATH_BACKUP_TEMPLATE);
        File backupUserDir = new File(PATH_BACKUP_TEMPLATE + File.separator + mCurrentUserId);

        if (!backupDir.exists()) {
            backupDir.mkdir();
        }

        // backup all template files
        removeTemplate(mCurrentUserId);
        ret = copyDir(templateDir, backupUserDir);

        return ret;
    }

    private int useTemplate(String userId) {
        int ret = -1;

        if ((userId == null) || userId.isEmpty()) {
            EgisLog.d(TAG, "useTemplate(): userId is not valid");
            return ret;
        }

        File templateDir = new File(PATH_CURRENT_TEMPLATE);
        File userDir = new File(PATH_BACKUP_TEMPLATE + File.separator + userId);
        if (!userDir.isDirectory()) {
            EgisLog.d(TAG, "Backup template dir for user " + userId + " does not exist!");
            return ret;
        }

        removeTemplate(null);

        // copy template files
        ret = copyDir(userDir, templateDir);

        // finally update the user ID
        if (ret == 0) {
            mCurrentUserId = userId;
            EgisLog.d(TAG, "useTemplate(): mCurrentUserId changed to " + mCurrentUserId);
        }

        return ret;
    }

    // Remove current template or backup template
    // if userId is null then delete all files in current template,
    // else delete corresponding backup template
    private int removeTemplate(String userId) {
        int ret = -1;

        EgisLog.d(TAG, "removeTemplate(): userId = " + userId);

        File deleteDir;
        if (userId == null) {
            deleteDir = new File(PATH_CURRENT_TEMPLATE);
        } else {
            deleteDir = new File(PATH_BACKUP_TEMPLATE + File.separator + userId);
        }

        // The return code of FileIoUtils.deleteDirectory is not reliable, so skip checking it
        if (!FileIoUtils.deleteDirectory(deleteDir.toString())) {
            EgisLog.d(TAG, "removeTemplate(): Failed to remove dir = " + deleteDir.toString());
            return ret;
        }

        if (userId == null) {
            deleteDir.mkdir();
            EgisLog.d(TAG, "removeTemplate(): removed current template");
        } else {
            EgisLog.d(TAG, "removeTemplate(): removed backup template for user " + userId);
        }

        ret = 0;

        return ret;
    }
}
