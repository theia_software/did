package egistec.optical.demotool.widget;

import android.app.Dialog;
//import android.os.SystemProperties;
import android.widget.TextView;
import android.widget.NumberPicker;
import android.widget.Button;
import android.view.View;
import android.os.Bundle;
import android.app.Activity;
import android.view.WindowManager;
import android.view.Window;
import android.view.Display;
import android.util.Log;
import egistec.optical.demotool.R;

public class EnvPicker extends Dialog {

    private NumberPicker mpick;
    private TextView     mtv;
    private Button      mbtok;
    private Button      mbtcan;
    private int         setid;
    private String      mvalue;
    Activity mcontext;

    private View.OnClickListener onClickListener ;

    public EnvPicker(Activity context) {
        super(context);
        mcontext = context;
    }

    public EnvPicker(Activity context, int theme, View.OnClickListener clickListener , int setid) {
        super(context, theme);
        mcontext = context;
        onClickListener =clickListener;
        this.setid= setid;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
         super.onCreate(savedInstanceState);
        setContentView(R.layout.fp_widget_evn_picker);

        mtv = (TextView) findViewById(R.id.tv);
        mpick = (NumberPicker) findViewById(R.id.picker);
        mpick.setMinValue(0);
        Log.d("ETS-FingerActivity","setid="+setid);
      /*  switch (setid){
            case 0:
                mtv.setText("Duplicate:0:disable 1:enable, on/off duplicate finger");
                mpick.setMaxValue(1);
                mvalue = SystemProperties.get("egis.ets.enroll.duplicate", "0");
                mpick.setValue(Integer.valueOf(mvalue));
                break;
            case 1:
                mtv.setText("Redundant Set:[0-11] redundant fingerprint picture check");
                mpick.setMaxValue(11);
                mvalue = SystemProperties.get("egis.ets.enroll.redundancy", "0");
                mpick.setValue(Integer.valueOf(mvalue));
                break;
            case 2:
                mtv.setText("Matcher Adaptor:0:G1 1:G2 2:aggressive");
                mpick.setMaxValue(2);
                mvalue = SystemProperties.get("egis.ets.matcher.adaptor", "0");
                mpick.setValue(Integer.valueOf(mvalue));
                break;
        }
        */
        mpick.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
        mbtok = (Button) findViewById(R.id.ok);
        mbtcan = (Button) findViewById(R.id.cancel);

        mbtok.setOnClickListener(onClickListener);
        mbtcan.setOnClickListener(onClickListener);


        Window dialogWindow = this.getWindow();
        WindowManager m = mcontext.getWindowManager();
        Display d = m.getDefaultDisplay();
        WindowManager.LayoutParams p = dialogWindow.getAttributes();
      //  Point mpoint = new Point();
      //  d.getSize(mpoint);
     //   p.height = (int) (mpoint.x * 0.6); 
      //  p.width = (int) (mpoint.y * 0.8); 
        dialogWindow.setAttributes(p);
        this.setCancelable(true);
    }

    public void setSetidId(int id){
        setid = id;
    }

    public int getSetid(){
        return setid;
    }

    public int getPickValue(){
        return  mpick.getValue();
    }
}
