package egistec.optical.demotool;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by jensen.lin on 4/16/2018.
 */

public class KeyValueDB {
    private SharedPreferences sharedPreferences;
    private static String PREF_NAME = "SettingsDB";
    public static int mDefaulIntegrationCount = 8;
    public static float mDefaultExposureTime = 119.0f;
    public static int mDefaultDCOffset = 50;

    public KeyValueDB() {
        // Blank
    }

    private static SharedPreferences getPrefs(Context context) {
        return context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
    }

    public static int getIntegrationCount(Context context) {
        return getPrefs(context).getInt("Integration_Count", mDefaulIntegrationCount);
    }

    public static float getExposureTime(Context context) {
        return getPrefs(context).getFloat("Exposure_Time", mDefaultExposureTime);
    }

    public static int getDCOffset(Context context){
        return getPrefs(context).getInt("DC_Offset", mDefaultDCOffset);
    }

    public static void setIntegrationCount(Context context, int input) {
        SharedPreferences.Editor editor = getPrefs(context).edit();
        editor.putInt("Integration_Count", input);
        editor.commit();
    }

    public static void setExposureTime(Context context, float input) {
        SharedPreferences.Editor editor = getPrefs(context).edit();
        editor.putFloat("Exposure_Time", input);
        editor.commit();
    }

    public static void setDCOffset(Context context, int input) {
        SharedPreferences.Editor editor = getPrefs(context).edit();
        editor.putInt("DC_Offset", input);
        editor.commit();
    }
}