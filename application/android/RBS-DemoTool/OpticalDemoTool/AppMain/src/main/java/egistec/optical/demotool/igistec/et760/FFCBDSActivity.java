package egistec.optical.demotool.igistec.et760;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.util.ArrayList;

import egistec.optical.demotool.R;
import egistec.optical.demotool.application.DemoToolApp;
import egistec.optical.demotool.experiment.ExperimentDef;
import egistec.optical.demotool.experiment.FileIoUtils;
import egistec.optical.demotool.igistec.AuoOledFingerprintReceiverHelper;
import egistec.optical.demotool.igistec.Common;
import egistec.optical.demotool.igistec.PNGHelper;
import egistec.optical.demotool.lib.MainFileSession;
import rbs.egistec.com.fplib.api.EgisLog;
import rbs.egistec.com.fplib.api.RbsLib;


/**
 * 用來處理FFC + BDS的VIEW
 * 取代 RBS的Calibration 之後會進MASTER用的
 *
 * 各家SENSOR必需IMPLEMENT各種取的方式，這裡就是個UI/存檔器
 */
public class FFCBDSActivity extends AppCompatActivity {

    static public String TAG = "RBS-FFCBDSActivity" ;
    Button btnBack ;
    ImageButton btnMore ;
    Button btnDeleteCali;
    Button btnStartWhite;
    Button btnStartBlack ;
    Button btnStartSkin ;
    LinearLayout llControl ;

    Handler handler = new Handler() ;
    ImageView ivLive ;
    boolean isRunningAsyncTask = false ;

    Object lock = new Object() ;
    ProgressBar pbLoading ;

    TextView tvDebug ;
    FileIoUtils.SQWindowPos sqWindowPos ;
    RbsLib mLib ;

    ArrayList<int[][]> skin10List = new ArrayList<int[][]>() ;

    Button btnTestBlack ;

    // todo: 註 路徑決定G5 OR G7 切為G7記得要改
    static public String FFC_BDS_PATH = "/sdcard/RbsG5Temp/image_ffc/" ;

    private static final int TYPE_WHITE_BACKGROUND = 100;
    private static final int TYPE_BLACK_BACKGROUND = 101;

    int skin_image_cnt = 0 ;
    int TYPE_SKIN_PRESS = 1010101 ;


    int multi_level_ffc_array[] = {255, (int)(255*0.9), (int)(255*0.8), (int)(255*0.7), (int)(255*0.6), (int)(255*0.5) };
    // 每一種都使用 黑白膚 GAIN是零

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ffcbds);
        Common.hideSystemUI(this);
        Log.d(TAG, "debug_check");


        if ( !AuoOledFingerprintReceiverHelper.G_DEMO_INC_DEFINE_IN_MK ) {
            try {
                File f = new File("/sdcard/RbsG5Temp/image_ffc/") ;
                f.mkdirs() ;
            }
            catch (Exception ex) {
                Log.e(TAG, "image_ffc mkdir fail") ;
            }

        }



        mLib = RbsLib.getInstance(this);
        Common.mainFileSession = new MainFileSession(DemoToolApp.getMyDataRoot());


        btnDeleteCali = findViewById(R.id.btnDeleteCali) ;
        btnStartWhite = findViewById(R.id.btnStartWhite) ;
        btnStartBlack = findViewById(R.id.btnStartBlack) ;
        btnStartSkin = findViewById(R.id.btnStartSkin) ;

        llControl = findViewById(R.id.llControl) ;

        ivLive = findViewById(R.id.ivLive ) ;
        btnBack = findViewById(R.id.btnBack );
        btnMore = findViewById(R.id.btnMore) ;
        pbLoading = findViewById(R.id.pbLoading) ;
        pbLoading.getIndeterminateDrawable().setColorFilter(0xFF333333, android.graphics.PorterDuff.Mode.MULTIPLY);

        tvDebug = findViewById(R.id.tvDebug ) ;

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


        btnTestBlack = findViewById(R.id.btnTestBlack) ;
        btnTestBlack.setVisibility(View.GONE);


        btnDeleteCali.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                RemoveAlertDialog() ;
                Common.hideSystemUI(getThis());
            }
        });

        btnStartWhite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                blockView();
                GetBackgroundTask gbt = new GetBackgroundTask() ;
                gbt.which = TYPE_WHITE_BACKGROUND ;
                gbt.execute() ;
                Common.hideSystemUI(getThis());
            }
        });

        btnStartBlack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                blockView();
                GetBackgroundTask gbt = new GetBackgroundTask() ;
                gbt.which = TYPE_BLACK_BACKGROUND;
                gbt.execute() ;
                Common.hideSystemUI(getThis());
            }
        });



        // 不啟動BDS，將SKIN10張隱藏。
        btnStartSkin.setVisibility(View.GONE) ;

        setPos() ;

        pbLoading.setVisibility(View.GONE);
        tvDebug.setText("Calibration View");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Log.e(TAG, "@@ Back ID=" + item.getItemId()) ;
        switch(item.getItemId()) {
            case android.R.id.home:
                this.finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if ( getActionBar() != null ) {
            getActionBar().setDisplayHomeAsUpEnabled(true);
        }
        if ( getSupportActionBar() != null ) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        return super.onCreateOptionsMenu(menu);
    }

    void setPos(){
        sqWindowPos = FileIoUtils.loadSQWindowLayoutData();
        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) ivLive.getLayoutParams();
        params.leftMargin = sqWindowPos.x ;
        params.topMargin = sqWindowPos.y ;
        params.width = sqWindowPos.width ;
        params.height = sqWindowPos.height ;

        ivLive.setLayoutParams(params) ;
    }


    Activity getThis() {
        return this ;
    }


    static public int [][] backup_black_back = null ;
    static public int [][] backup_white_back = null ;

    private void RemoveAlertDialog() {
        new AlertDialog.Builder(this)
                .setTitle("WARNING")
                .setMessage("Do you want to remove calibration.bin and calibration_bds.bin?")
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        int calibrateRet = mLib.extraApi(ExperimentDef.PID_BKG_IMG, ExperimentDef.CMD_BKG_IMG_REMOVE_CALI_DATA, null, null, null);
                        if (calibrateRet != 0) {
                            EgisLog.e(TAG, String.format("CMD_BKG_IMG_REMOVE_CALI_DATA failed ret=%d", calibrateRet));
                            Toast.makeText(getThis(), "Delete fail!!!", Toast.LENGTH_SHORT).show();
                        }
                        else {
                            Toast.makeText(getThis(), "Delete calibration.bin and calibration_bds.bin", Toast.LENGTH_SHORT).show();
                        }
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                })
                .show();
    }


    public class GetBackgroundTask extends AsyncTask {

        public int which = 0 ;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            blockView();
            tvDebug.setText("Calibrating...");

            synchronized (lock) {
                isRunningAsyncTask = true;
            }
        }

        @Override
        protected void onProgressUpdate(Object[] values) {
            super.onProgressUpdate(values);
            Common.hideSystemUI(getThis());
        }

        @Override
        protected Object doInBackground(Object[] objects) {
            Common.sleep(2000);
            switch (which){
                case TYPE_BLACK_BACKGROUND:
                    mLib.extraApi(ExperimentDef.PID_BKG_IMG, ExperimentDef.CMD_BKG_BBKG_FFC, null, null, null);
                    break;
                case TYPE_WHITE_BACKGROUND:
                    mLib.extraApi(ExperimentDef.PID_BKG_IMG, ExperimentDef.CMD_BKG_WBKG_FFC, null, null, null);
                    break;
                default:
                    Log.e(TAG, "not support calibration type");
                    break;
            }
            mLib.extraApi(ExperimentDef.PID_BKG_IMG, ExperimentDef.CMD_BKG_IMG_RUN_CALI_PROC, null, null, null);

            return null;
        }

        @Override
        protected void onPostExecute(Object o) {
            super.onPostExecute(o);

            Common.showAlert(getThis(),"Alert", "Calibration Finish");
            synchronized (lock) {
                isRunningAsyncTask = false;
            }
            tvDebug.setText("Batch Calibration Finish");
            showView();
        }
    }
    public void blockView() {
        pbLoading.setVisibility(View.VISIBLE) ;
        btnBack.setVisibility(View.GONE);
        llControl.setVisibility(View.GONE); ;
    }
    public void showView() {
        llControl.setVisibility(View.VISIBLE);
        pbLoading.setVisibility(View.GONE) ;
        btnBack.setVisibility(View.VISIBLE);
    }

}


