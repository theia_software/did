package egistec.optical.demotool.lib;

import android.os.Handler;
import android.os.Looper;
//import android.util.Log;
import android.view.View;
import android.view.ViewTreeObserver;

import egistec.optical.demotool.AppSettings;
import egistec.optical.demotool.AppSettingsPreference;
import egistec.optical.demotool.R;
import pl.droidsonroids.gif.GifDrawable;
import pl.droidsonroids.gif.GifImageView;
import rbs.egistec.com.fplib.api.EgisLog;

public class LightDotControl {
    private final static String TAG = "RBS-LightDotControl";

    private GifImageView gifImageView;
    private GifDrawable gifDrawable;
    private CircleShaderView mGreenGradation;

    private AppSettings mAppSettings;
    private Handler mHandler;
    private boolean mIsAddOnDrawListener = false;

    public LightDotControl(GifImageView gifImageView, CircleShaderView mGreenGradation, AppSettings mAppSettings) {
        this.gifImageView = gifImageView;
        this.mGreenGradation = mGreenGradation;
        this.mAppSettings = mAppSettings;

        mHandler = new Handler(Looper.getMainLooper());
        reset();
    }

    public void playStart() {
        int resId = AppSettingsPreference.getPressAreaDrawableId(mAppSettings.getPressAreaTypeId());
        gifImageView.setScaleX(1f);
        gifImageView.setScaleY(1f);
        if (resId >= 0) {
            if(mAppSettings.getCustomizePressAreaIcon())
                gifImageView.setImageResource(resId);
            gifDrawable = (GifDrawable) this.gifImageView.getDrawable();
            gifDrawable.seekToFrame(0);
            gifDrawable.seekTo(0);
            gifDrawable.reset();
        } else {
            if(mAppSettings.getCustomizePressAreaIcon()){
                gifImageView.setVisibility(View.GONE);
                mGreenGradation.setVisibility(View.VISIBLE);
            }
        }
    }

    public void playStop() {
        int resId = AppSettingsPreference.getPressAreaDrawableId(mAppSettings.getPressAreaTypeId());
        if(resId >= 0){
            gifDrawable.stop();
            gifDrawable.seekTo(0);
            gifDrawable.seekToFrame(0);
        }
        removeOnGifLoadCB();
    }

    public void reset() {
        if(mAppSettings.getCustomizePressAreaIcon()){
            gifImageView.setVisibility(View.VISIBLE);
            gifImageView.setImageResource(R.drawable.fp_icon_b);
            gifImageView.setScaleX(0.5f);
            gifImageView.setScaleY(0.5f);
            mGreenGradation.setVisibility(View.GONE);
        } else{
            int resId = AppSettingsPreference.getPressAreaDrawableId(mAppSettings.getPressAreaTypeId());
            if(resId >= 0){
                gifImageView.setVisibility(View.VISIBLE);
                gifImageView.setImageResource(resId);
                gifImageView.setScaleX(0.75f);
                gifImageView.setScaleY(0.75f);
                gifDrawable = (GifDrawable) gifImageView.getDrawable();
                gifDrawable.stop();
                gifDrawable.seekToFrame(0);
                gifDrawable.seekTo(0);
                mGreenGradation.setVisibility(View.GONE);
            } else {
                gifImageView.setVisibility(View.GONE);
                mGreenGradation.setVisibility(View.VISIBLE);
            }
        }
    }

    private OnGifLoadCB onGifLoadCB = null;

    public interface OnGifLoadCB {
        void onGifLoad();
    }

    private void onGifLoad(){
    }

    public void setOnGifLoadCB(OnGifLoadCB callback) {
        EgisLog.d(TAG, "setOnGifLoadCB ");
        gifImageView.getViewTreeObserver().addOnPreDrawListener (mGifOnDrawListener);
        onGifLoadCB = callback;
        mIsAddOnDrawListener = true;
    }

    public void removeOnGifLoadCB() {
        EgisLog.d(TAG, "removeOnGifLoadCB ");
        onGifLoadCB = null;
        mIsAddOnDrawListener = false;
        if (gifImageView.getViewTreeObserver().isAlive())
            gifImageView.getViewTreeObserver().removeOnPreDrawListener(mGifOnDrawListener);
    }

    final ViewTreeObserver.OnPreDrawListener mGifOnDrawListener = new ViewTreeObserver.OnPreDrawListener() {

        @Override
        public boolean  onPreDraw() {
            if(AppSettingsPreference.getPressAreaDrawableId(mAppSettings.getPressAreaTypeId()) >= 0) {
                if (gifDrawable.getNumberOfFrames() > 1){
                    if (gifDrawable.getCurrentFrameIndex() >= 1 && gifDrawable.getCurrentPosition() > 0)
                        mHandler.post(runnableGifLoadCB);
                } else {
                    if (!gifDrawable.isRunning()){
                        mHandler.postDelayed(runnableGifLoadCB, 50);
                    }
                }
            }else{
                mHandler.postDelayed(runnableGifLoadCB, 50);
            }
            return true;
        }
    };

    private Runnable runnableGifLoadCB = new Runnable() {
        @Override
        public void run(){
            if (onGifLoadCB != null)
                onGifLoadCB.onGifLoad();
            else
                EgisLog.d(TAG, "onGifLoadCB is null ");
        }
    };
}
