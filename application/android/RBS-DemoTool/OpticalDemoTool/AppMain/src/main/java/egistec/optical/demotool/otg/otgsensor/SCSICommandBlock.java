package egistec.optical.demotool.otg.otgsensor;

import java.nio.ByteBuffer;
import java.util.Random;

public class SCSICommandBlock
{
    public static final int CBW_SIGNATURE = 0x43425355;
    //public static final int CBS_SIGNATURE = 0x53425355;
    public static final int CBW_SIZE = 31;
    public static final int CBS_SIZE = 13;

    private int dCBWSignature;
    private int dCBWTag;
    private int dCBWDataLength;
    private byte bmCBWFlags;
    private byte bCBWLUN;
    private byte bCBWCBLength;
    private byte[] commandBlock;


    public SCSICommandBlock(int dCBWDataLength, byte bmCBWFlags) {
        this.dCBWSignature = CBW_SIGNATURE;
        this.dCBWTag = generateTag();
        this.dCBWDataLength = dCBWDataLength;
        this.bmCBWFlags = bmCBWFlags;   // bit 7 : Direction, 0 = Data-Out from host to the device, 1 = Data-In from the device to the host.
        this.bCBWLUN = 0;
        this.bCBWCBLength = 16;
    }

    public void setCommandBlock(byte[] commandBlock)
    {
        this.commandBlock = commandBlock;
    }

    public int getdCBWDataLength()
    {
        return dCBWDataLength;
    }

    public int generateTag() {
        Random random = new Random();
        return random.nextInt();
    }

    public static int swapEndianess(int i) {
        return((i & 0xff) << 24) + ((i & 0xff00) << 8) + ((i & 0xff0000) >> 8) + ((i >> 24) & 0xff);
    }

    public byte[] getCBWBuffer() {
        ByteBuffer buffer = ByteBuffer.allocate(CBW_SIZE);
        buffer.putInt(swapEndianess(dCBWSignature));
        buffer.putInt(dCBWTag);
        buffer.putInt(swapEndianess(dCBWDataLength));
        buffer.put(bmCBWFlags);
        buffer.put(bCBWLUN);
        buffer.put(bCBWCBLength);
        buffer.put(commandBlock);

        return buffer.array();
    }
}
