package egistec.optical.demotool.otg.otgsensor;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.hardware.usb.UsbConstants;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbDeviceConnection;
import android.hardware.usb.UsbEndpoint;
import android.hardware.usb.UsbInterface;
import android.hardware.usb.UsbManager;
import android.os.Handler;
import android.util.Log;
import android.widget.Toast;

import java.util.HashMap;
import java.util.Iterator;

public class IODispatchOtgSCSI {

    public static final int VID_WBF = 0x1C7A;
    public static final int PID_WBF = 0x0570;
    public static final int VID_ARDUINO = 0x2341;
    public static final int PID_ARDUINO = 0x003E;
    public static final int VID_HEARTBEAT = 0x0483;
    public static final int PID_HEARTBEAT = 0x5710;


    private static final int OTG_FW_V1 = 1;
    private static final int OTG_FW_V2 = 2;
    private static final int OTG_FW_UNKNOWN = -1;
    private int otgVersion = OTG_FW_UNKNOWN;
    private IOtgIO otgIO = null;

    private static final byte VALUE_FW_OTG_VERSION_V1 = 0x08;
    private static final byte VALUE_FW_OTG_VERSION_V2 = 0x09;
    private static final byte VALUE_FW_OTG_VERSION_ET760 = (byte) 0xF8;

    public static int mBoardType = 0; // 0 for wbf board, 1 for arduino board

    private UsbManager mUsbManager = null;
    protected static final String ACTION_USB_PERMISSION = "egistec.otgdemoapp.USB";
    public int mFoundDevice = 0;

    private static UsbDeviceConnection mConn;
    private static UsbEndpoint mEpIn = null;
    private static UsbEndpoint mEpOut = null;

    private Context mApplicationContext = null;
    private Handler mHandler = null;
    private static final String TAG = "OTG-SCSI";
    public IODispatchOtgSCSI(Context context, UsbManager usbManager) {
        mApplicationContext = context;
        // mUsbManager = (UsbManager) mApplicationContext.getSystemService(Context.USB_SERVICE);
        // mHandler = handler;

        // init();
        mUsbManager = usbManager;
    }

    private void enumerate(IPermissionListener listener) {
        HashMap<String, UsbDevice> devlist = mUsbManager.getDeviceList();
        Iterator<UsbDevice> deviter = devlist.values().iterator();

        while (deviter.hasNext()) {
            UsbDevice dev = deviter.next();
            // String msg = "Found device: " + String.format("%04X:%04X", dev.getVendorId(),
            // dev.getProductId());
            // Log.d("OtgDemoApp", msg);
            // Toast.makeText(mApplicationContext, msg, Toast.LENGTH_LONG).show();
            int vid = dev.getVendorId();
            int pid = dev.getProductId();
            if ((vid == VID_WBF && pid == PID_WBF)
                    || (vid == VID_ARDUINO && pid == PID_ARDUINO)
                    || (vid == VID_HEARTBEAT && pid == PID_HEARTBEAT)) {
                if (!mUsbManager.hasPermission(dev)) {
                    listener.onPermissionDenied(dev);
                    mFoundDevice = -1;
                } else {
                    if (vid == VID_WBF && pid == PID_WBF) mBoardType = 0;
                    else if (vid == VID_ARDUINO && pid == PID_ARDUINO) mBoardType = 1;
                    boolean ret = openEndpoint(dev);
                    // startHandler();
                    if (ret == true) {
                        mFoundDevice = 1;
                        Log.d("OtgDemoApp", "DEV_STATE_CONNECTED");
                        // mHandler.obtainMessage(FpResDef.DEV_STATE_CONNECTED, 0,
                        // -1).sendToTarget();
                    } else {
                        mFoundDevice = -1;
                        Log.d("OtgDemoApp", "DEV_STATE_DISCONNECTED");
                        // mHandler.obtainMessage(FpResDef.DEV_STATE_DISCONNECTED, -1,
                        // -1).sendToTarget();
                    }
                    return;
                }
                break;
            }
        }
        Log.d("OtgDemoApp", "DEV_STATE_DISCONNECTED");
        // mHandler.obtainMessage(FpResDef.DEV_STATE_DISCONNECTED, -1, -1).sendToTarget();
        mFoundDevice = -1;
        // mConnectionHandler.onDeviceNotFound();
    }

    public boolean openEndpoint(UsbDevice dev) {
        mConn = mUsbManager.openDevice(dev);

        int count = dev.getInterfaceCount();
        if (count < 1) {
            Toast.makeText(mApplicationContext, "can't find interface!", Toast.LENGTH_SHORT).show();
            return false;
        }

        mEpIn = null;
        mEpOut = null;
        UsbInterface usbIf;
        for (int i = 0; i < count; i++) {
            usbIf = dev.getInterface(i);
            // Log.d("OtgDemoApp", "openEndpoint claimInterface");
            if (!mConn.claimInterface(usbIf, true)) {
                continue;
            }
            // Log.d("OtgDemoApp", "openEndpoint controlTransfer");
            try {
                mConn.controlTransfer(0x21, 34, 0, 0, null, 0, 0);
                // Log.d("OtgDemoApp", "openEndpoint controlTransfer1111");
                mConn.controlTransfer(
                        0x21,
                        32,
                        0,
                        0,
                        new byte[] {(byte) 0x80, 0x25, 0x00, 0x00, 0x00, 0x00, 0x08},
                        7,
                        0); // 9600, 8N1
            } catch (Exception e) {
                e.printStackTrace();
            }

            // Log.d("OtgDemoApp", "openEndpoint getEndpointCount");
            int epcount = usbIf.getEndpointCount();
            for (int j = 0; j < epcount; j++) {
                UsbEndpoint usbEndpoint = usbIf.getEndpoint(j);
                // Log.d("OtgDemoApp", "openEndpoint getEndpoint getType " + usbEndpoint.getType());
                if (usbEndpoint.getType() == UsbConstants.USB_ENDPOINT_XFER_BULK) {
                    int dir = usbEndpoint.getDirection();
                    if (dir == UsbConstants.USB_DIR_IN) mEpIn = usbEndpoint;
                    else if (dir == UsbConstants.USB_DIR_OUT) mEpOut = usbEndpoint;
                }
            }
            if (mEpIn != null && mEpOut != null) {
                break;
            }
        }

        // Log.d("OtgDemoApp", "openEndpoint success");
        // Log.d("OtgDemoApp", "openEndpoint failed");
        otgVersion = getOTGVersion();
        if ( otgVersion == VALUE_FW_OTG_VERSION_ET760 ) {
            otgIO = new OtgIO_V8_et760(mConn, mEpIn, mEpOut);
        }
        else if(otgVersion == OTG_FW_V1){
            //otgIO = new OtgIO_V8(mConn, mEpIn, mEpOut);
            // 相容舊的裝置
            otgIO = new OtgIO_V8_et760(mConn, mEpIn, mEpOut);
        }else{
            otgIO = new OtgIO_V9(mConn, mEpIn, mEpOut);
        }
        return mEpIn != null && mEpOut != null;
    }

    public void closeEndpoint() {
        mConn.close();
        mEpIn = null;
        mEpOut = null;
    }

    private class PermissionReceiver extends BroadcastReceiver {
        private final IPermissionListener mPermissionListener;

        public PermissionReceiver(IPermissionListener permissionListener) {
            mPermissionListener = permissionListener;
        }

        @Override
        public void onReceive(Context context, Intent intent) {
            // Log.d("OtgDemoApp", "onReceive");
            mApplicationContext.unregisterReceiver(this);
            if (intent.getAction().equals(ACTION_USB_PERMISSION)) {
                if (!intent.getBooleanExtra(UsbManager.EXTRA_PERMISSION_GRANTED, false)) {
                    mPermissionListener.onPermissionDenied(
                            (UsbDevice) intent.getParcelableExtra(UsbManager.EXTRA_DEVICE));
                } else {
                    Toast.makeText(mApplicationContext, "Permission granted", Toast.LENGTH_SHORT)
                            .show();
                    UsbDevice dev = intent.getParcelableExtra(UsbManager.EXTRA_DEVICE);
                    if (dev != null) {
                        int vid = dev.getVendorId();
                        int pid = dev.getProductId();
                        if ((vid == VID_WBF && pid == PID_WBF)
                                || (vid == VID_ARDUINO && pid == PID_ARDUINO)
                                || (vid == VID_HEARTBEAT && pid == PID_HEARTBEAT)) {
                            if (openEndpoint(dev)) {
                                // mHandler.obtainMessage(FpResDef.DEV_STATE_CONNECTED, -1,
                                // -1).sendToTarget();
                                Log.d("OtgDemoApp", "onReceive openEndpoint success");
                            } else {
                                // mHandler.obtainMessage(FpResDef.DEV_STATE_DISCONNECTED, -1,
                                // -1).sendToTarget();
                                Log.d("OtgDemoApp", "onReceive openEndpoint failed");
                            }
                        }
                    } else {
                        Toast.makeText(
                                mApplicationContext,
                                "device not present!",
                                Toast.LENGTH_SHORT)
                                .show();
                    }
                }
            }
        }
    }

    private BroadcastReceiver mPermissionReceiver =
            new PermissionReceiver(
                    new IPermissionListener() {
                        @Override
                        public void onPermissionDenied(UsbDevice d) {
                            Toast.makeText(
                                    mApplicationContext,
                                    "Permission denied on " + d.getDeviceId(),
                                    Toast.LENGTH_SHORT)
                                    .show();
                        }
                    });

    private interface IPermissionListener {
        void onPermissionDenied(UsbDevice d);
    }

    public void stop() {
        try {
            mApplicationContext.unregisterReceiver(mPermissionReceiver);
        } catch (IllegalArgumentException e) {
        }
    }

    public int getOTGVersion(){
        byte version = 0;
        SCSICommandBlock cmdBlock = new SCSICommandBlock(512, (byte) 0x80);
        byte[] commandBlock = new byte[16];
        commandBlock[0] = (byte) 0xE0;
        commandBlock[1] = (byte) 0xFF;
        commandBlock[2] = (byte) 0xFF;
        cmdBlock.setCommandBlock(commandBlock);
        byte[] command = cmdBlock.getCBWBuffer();

        Log.d(TAG, "getOTGVersion start");
        int response = mConn.bulkTransfer(mEpOut, command, command.length, 500);
        Log.d(TAG, "getOTGVersion end "+response);

        byte status = 0;
        if (response == 31) {
            byte[] dataBuffer = new byte[512];

            Log.d(TAG, "getOTGVersion start");
            response = mConn.bulkTransfer(mEpIn, dataBuffer, dataBuffer.length, 500);
            Log.d(TAG, "getOTGVersion end "+response+", "+dataBuffer[0]);

            version = dataBuffer[0];
            if (response != 512) status = 1;
        } else {
            status = 1;
        }

        SCSICommandStatus cmdStatus = new SCSICommandStatus(response, status);
        byte[] commandStatus = cmdStatus.getCSWBuffer();

        response = mConn.bulkTransfer(mEpIn, commandStatus, commandStatus.length, 500);

        Log.d(TAG, "getOTGVersion ReadRegister_v1 version = "+version);
        switch (version){
            case VALUE_FW_OTG_VERSION_V1:
                return OTG_FW_V1;
            case VALUE_FW_OTG_VERSION_V2:
                return OTG_FW_V2;
            case VALUE_FW_OTG_VERSION_ET760:
                return VALUE_FW_OTG_VERSION_ET760 ;
            default:
                return OTG_FW_UNKNOWN;
        }
    }

    public int ReadRegister(short address, byte[] value) {
        if (otgIO == null){
            Log.e(TAG, "ReadRegister invalid otgIO");
            return -1;
        }

        return otgIO.ReadRegister(address, value);
    }

    public int WriteRegister(short address, byte value){
        if (otgIO == null){
            Log.e(TAG, "WriteRegister invalid otgIO");
            return -1;
        }

        return otgIO.WriteRegister(address, value);
    }

    public int FetchImage(byte[] data, int width, int height){
        if (otgIO == null){
            Log.e(TAG, "FetchImage invalid otgIO");
            return -1;
        }

        return otgIO.FetchImage(data, width, height);
    }

    public int SPI_Write_Read(byte[] writeBuf, int writeLen, byte[] readBuf, int readLen){
        if (otgIO == null){
            Log.e(TAG, "SPI_Write_Read invalid otgIO");
            return -1;
        }

        return otgIO.SPI_Write_Read(writeBuf, writeLen, readBuf, readLen);
    }
    public int Set_SPI_CLK(byte mode, byte clk){
        if (otgIO == null){
            Log.e(TAG, "Set_SPI_CLK invalid otgIO");
            return -1;
        }

        return otgIO.Set_SPI_CLK(mode, clk);
    }

}