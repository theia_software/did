package egistec.optical.demotool.lib;

import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.util.Log;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.ShortBuffer;

public class SaveFileHelper {
    final String TAG = "RBS_" + SaveFileHelper.class.getSimpleName();

    final static int MSG_SAVE = 1001;
    final static int MSG_SAVE_16BIT = 1002;
    final static int MSG_DESTROY = 1100;

    private FileObj toSaveFileObj;
    private HandlerThread handlerThread;
    private Handler myHandler;
    private boolean ignoreThisToSave;

    public SaveFileHelper() {
    }

    public void Create() {
        handlerThread = new HandlerThread(TAG);
        handlerThread.start();
        myHandler = new IoHandler(handlerThread.getLooper());
    }

    public void Destroy() {
        myHandler.sendEmptyMessage(MSG_DESTROY);
        handlerThread.quitSafely();
        myHandler = null;
    }

    public void addToSave(File file) {
        if (file == null) {
            Log.w(TAG, "bad file");
            return;
        }
        Log.i(TAG, "addToSave file");
        if (toSaveFileObj == null) {
            toSaveFileObj = new FileObj();
        }
        if (toSaveFileObj.getSaveFile() == null) {
            toSaveFileObj.setSaveFile(file);
            /*if (toSaveFileObj.getContent() != null) {
                Save(toSaveFileObj);
                toSaveFileObj = null;
            }*/
        } else {
            Log.e(TAG, "Unexpected sequence! got file again.");
        }
    }

    public void addToSave(byte [] content) {
        if (content == null) {
            Log.w(TAG, "bad content");
            return;
        }
        if (ignoreThisToSave) {
            Log.d(TAG, "ignoreThisToSave");
            return;
        }
        Log.i(TAG, "addToSave content");
        if (toSaveFileObj == null) {
            toSaveFileObj = new FileObj();
        }
        if (toSaveFileObj.getContent() == null) {
            toSaveFileObj.setContent(content);
            if (toSaveFileObj.getSaveFile() != null) {
                Save(toSaveFileObj);
                toSaveFileObj = null;
            }
        } else {
            Log.e(TAG, "Unexpected sequence! got content again.");
        }
    }

    public void setIgnoreThisToSave() {
        this.ignoreThisToSave = true;
    }

    public void resetToSave() {
        if (toSaveFileObj != null && !ignoreThisToSave) {
            Log.w(TAG, "resetToSave");
        } else {
            Log.i(TAG, "resetToSave");
        }
        ignoreThisToSave = false;
        toSaveFileObj = null;
    }

    public void Save(FileObj fileObj) {
        if (fileObj == null || fileObj.getSaveFile() == null || fileObj.getContent() == null) {
            Log.e(TAG, "bad fileObj");
            return;
        }
        if (myHandler == null) {
            Log.w(TAG, "call Create() first");
            return;
        }
        Log.d(TAG, "Saving " + fileObj.getSaveFile().getName());
        myHandler.obtainMessage(MSG_SAVE, fileObj).sendToTarget();
    }

    public void Save16Bit(FileObj fileObj) {
        if (fileObj == null || fileObj.getSaveFile() == null || fileObj.get16BitContent() == null) {
            Log.e(TAG, "bad fileObj");
            return;
        }
        if (myHandler == null) {
            Log.w(TAG, "call Create() first");
            return;
        }
        Log.d(TAG, "Saving " + fileObj.getSaveFile().getName());
        myHandler.obtainMessage(MSG_SAVE_16BIT, fileObj).sendToTarget();
    }

    class IoHandler extends Handler {

        private boolean destroyed;
        public IoHandler(Looper looper) {
            super(looper);
        }

        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case MSG_SAVE:
                    FileObj fileObj = (FileObj) msg.obj;
                    if (destroyed) {
                        Log.w(TAG, "ignore saving " + fileObj.saveFile.getName());
                        break;
                    }
                    File f = fileObj.saveFile;
                    if (!f.getParentFile().exists()) {
                        f.getParentFile().mkdirs();
                    }
                    FileOutputStream fileOutputStream;
                    try {
                        fileOutputStream = new FileOutputStream(f);
                        fileOutputStream.write(fileObj.content);
                        fileOutputStream.close();
                        Log.i(TAG, "Successful save " + f.getPath() );
                        break;
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    Log.e(TAG, "Failed to save " + f.getPath() );
                    break;
                case MSG_SAVE_16BIT:
                    fileObj = (FileObj) msg.obj;
                    if (destroyed) {
                        Log.w(TAG, "ignore saving " + fileObj.saveFile.getName());
                        break;
                    }
                    f = fileObj.saveFile;
                    fileOutputStream = null;
                    try {
                        fileOutputStream = new FileOutputStream(f);
                        byte[] TempByteContent = new byte[fileObj.content16Bit.length * 2];
                        ByteBuffer.wrap(TempByteContent).order(ByteOrder.BIG_ENDIAN).asShortBuffer().put(fileObj.content16Bit);
                        fileOutputStream.write(TempByteContent);
                        fileOutputStream.close();
                        Log.i(TAG, "Successful save " + f.getPath() );
                        break;
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    Log.e(TAG, "Failed to save " + f.getPath() );
                    break;
                case MSG_DESTROY:
                    Log.d(TAG, "MSG_DESTROY");
                    destroyed = true;
                    break;
            }
        }
    }

}
