package egistec.optical.demotool.lib;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.BatteryManager;
import android.util.Log;
import android.widget.TextView;

import java.nio.ByteBuffer;

import rbs.egistec.com.fplib.api.Common;
import rbs.egistec.com.fplib.api.RbsLib;

import static egistec.optical.demotool.experiment.ExperimentDef.CMD_HOST_TOUCH_SEND_TEMPERATURE;
import static egistec.optical.demotool.experiment.ExperimentDef.PID_HOST_TOUCH;

public class TemperatureReceiver extends BroadcastReceiver{
    static final String TAG = "RBS_" + TemperatureReceiver.class.getSimpleName();

    private TextView mViewTemperature;

    public void setTextView(TextView textView) {
        this.mViewTemperature = textView;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        if(intent.getAction().equals(Intent.ACTION_BATTERY_CHANGED)) {
            readBattery(intent);
        }
    }

    public void readBattery(Intent intent) {
        int temperature = intent.getIntExtra(BatteryManager.EXTRA_TEMPERATURE,0);
        Log.v(TAG, "TEMPERATURE: " + (float)temperature/10);
        mViewTemperature.setText("Temp: " + (float)temperature/10);
        ByteBuffer Temp = ByteBuffer.allocate(4);
        Common.intToBytes(temperature, Temp.array());
        RbsLib.getInstance(mViewTemperature.getContext()).extraApi(
                PID_HOST_TOUCH, CMD_HOST_TOUCH_SEND_TEMPERATURE, Temp.array(),
                null, null);
    }

}
