package egistec.optical.demotool;

import android.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class AppSettingsActivity extends AppCompatActivity {
    private static final String TAG = "AppSettingsActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        AppSettingsPreference preference = new AppSettingsPreference();
        transaction.replace(android.R.id.content, preference);
        transaction.commit();
    }
}
