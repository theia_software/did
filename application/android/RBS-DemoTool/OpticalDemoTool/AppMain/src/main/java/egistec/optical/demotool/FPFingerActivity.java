package egistec.optical.demotool;

import android.Manifest;
import android.animation.Animator;
import android.animation.AnimatorInflater;
import android.animation.AnimatorSet;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.drawable.PaintDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.preference.PreferenceManager;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.InputFilter;
import android.text.InputType;
import android.util.Log;
import android.util.TypedValue;
import android.view.ContextMenu;
import android.view.Display;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import egistec.optical.demotool.application.DemoToolApp;
import egistec.optical.demotool.databinding.FpActivityFingerBinding;
import egistec.optical.demotool.experiment.ExperimentDef;
import egistec.optical.demotool.experiment.ExperimentUtils;
import egistec.optical.demotool.experiment.FileIoUtils;

//import egistec.optical.demotool.igistec.ET760_FingerprintReceiverHelper;
import egistec.optical.demotool.igistec.AuoOledFingerprintReceiverHelper;

import egistec.optical.demotool.igistec.et760.FFCBDSActivity;
import egistec.optical.demotool.lib.CircleShaderView;
import egistec.optical.demotool.lib.ConvertPath;
import egistec.optical.demotool.lib.DebugUtil;
import egistec.optical.demotool.lib.FPAdapter;
import egistec.optical.demotool.lib.FPFingerInfo;
import egistec.optical.demotool.lib.FPList;
import egistec.optical.demotool.lib.FPUtil;
import egistec.optical.demotool.lib.FileObj;
import egistec.optical.demotool.lib.ImageEnlarge;
import egistec.optical.demotool.lib.ImageProcess;
import egistec.optical.demotool.lib.LightDotControl;
import egistec.optical.demotool.lib.MainFileSession;
import egistec.optical.demotool.lib.RbsObjArray;
import egistec.optical.demotool.lib.RbsObjImage;
import egistec.optical.demotool.lib.RbsUtil;
import egistec.optical.demotool.lib.ReturnImageInfo;
import egistec.optical.demotool.lib.UIUtils;
import egistec.optical.demotool.otg.manager.OtgSensorManager;
import egistec.optical.demotool.widget.EnvPicker;
import pl.droidsonroids.gif.GifImageView;
import rbs.egistec.com.fplib.api.Common;
import rbs.egistec.com.fplib.api.EgisLog;
import rbs.egistec.com.fplib.api.ExtraApiDef;
import rbs.egistec.com.fplib.api.FingerprintReceiver;
import rbs.egistec.com.fplib.api.FpResDef;
import rbs.egistec.com.fplib.api.RbsLib;

import static egistec.optical.demotool.otg.OtgDef.OTG_SENSOR_ID;
import static java.lang.Math.pow;

//import android.util.Log;

//import android.os.SystemProperties;

public class FPFingerActivity extends AppCompatActivity implements OtgSensorManager.OtgSensorEventListener, OnKeyboardVisibilityListener{
	private static final String TAG = "RBS-FingerActivity";

	public static final int REQ_ENROLL = 2;
	public static final int REQ_CURRENT_TEMPLATE_USER_ID = 3;
	private static final int MAX_FINGERS = 6;
	public static final int TIMER_DELAY = 600;
	public static final String EXTRA_ENROLL_ID = "ENROLL_ID";
	public static final String EXTRA_OPTION_NOCANCEL_ACTIONUP = "EXTRA_OPTION_NOCANCEL_ACTIONUP";
	private static final int ADD_FINGER_POSITION = 0;
	private static final String FP_NAME_PREFIX = "FINGER_";
	private final int NON_MATCH_ANIMATION_DURATION = 500;
	private int bottomBarAnimationDuration;
	public String IMAGE_ROOT_PATH;
    public String RootPath;
    public String PATH_CURRENT_TEMPLATE;
	public static final String TEMPLATE_PATH = "template";
	public static final String BACKUP_TEMPLATE_PATH = "backup_template";
	private static final int CALI_BKG_IMG = 6;
	static final int PID_COMMAND = 0;
	static final int PID_HOST_TOUCH = 7;
	static final int CMD_HOST_TOUCH_ENABLE = 100;
	static final int CMD_HOST_TOUCH_SET_TOUCHING = 101;
	static final int CMD_HOST_TOUCH_RESET_TOUCHING = 102;
	static final int CMD_DEMOTOOL_SET_SCREEN_BRIGHTNESS = 112;
    static final int REQUEST_CODE_ASK_WRITE_EXTERNAL_STORAGE_PERMISSIONS = 101;

	private RbsLib mLib;
	private EnvPicker mpick;
	private LightDotControl mLightDot;
	private FileIoUtils.WindowPos icWindowPos = null;
	private FileIoUtils.SQWindowPos sqWindowPos = null;

	private ListView mFingerListView;
	private List<FPFingerInfo> mFpList;
	private FPAdapter mAdapter;
	private FPFingerInfo mClickedFingerInfo;

	private boolean mIsVerifying;
	private boolean mIsVerifyAnimate;
	private LinearLayout mBtnContainer;
	private boolean mKeyBackPressed;
	private List<Integer> mFingers;	
	private Button mVerifyBtn;
	private Button mSet;
	private Button mTouchEvent;
	private TextView mVerifyStatus;
	private ImageView mFingerStatus;
	private PopupWindow mPopupMenu;
	private int mVerifySuccessCount;
	private int mVerifyFailCount;
	private int mVerifyErrorCount;
	private int mVerifyQuickMatchCount;
	private int mVerifyFakeFingerCount;
	private long mLastDownTime = 0;
	private Handler mHandler;
	private boolean mIsVerifySucess;
	private boolean mShowVerifyCount = true;
	private boolean mShowVerifyLimit = true;

	private final int REQ_CODE_SETTINGS = 5;

	private boolean optionVerifyAutoStart = true;
	private boolean verifyAutoStarting;
	private boolean optionNoCancelActionUp;
	private boolean hasActionUp;
	private boolean hasResult;

	private int optionScreenBrightness;

	//save image feature
	private Spinner mFingerNumSpinner;
    private String[] mFingerNum = {"0-LT","1-LF","2-LM","3-LR","4-LL","5-RT","6-RF","7-RM","8-RR","9-RL"};
	private Spinner mTestCaseSpinner;
	private String[] mFingerTestCase = {"Normal", "Wash hand", "280um Protect film", "Chamber -5",  "Chamber -20",
			"Xiaomi Wash hand", "Fake Ink", "Fake Type", "Fake Ptr", "Sun 90d", "Sun 45d", "Sun 10d",
			"Lotion on fingers", "330um Protect film", "Chamber 40", "90um Protect film", "230um Glass Protect film",
			"320um Glass Protect film", "380um Glass Protect film", "400um Glass Protect film",
			"600um Glass Protect film", "Lotion on finger/Wash hand", "Wet Tissue"};
	int mSpecialCaseIdx = 14;
    private String[] mFingerDegree = {"0", "45", "90"};
    private Spinner verifyCountSpinner;
	private int verifyCount = 9; // add for verify count
    private String[] verifyCountList = {"10", "20", "30", "40", "50", "60", "70", "80", "90", "100","*" + verifyCount, "new..." };
    private ArrayAdapter verifyCountAdapter ;
    private CheckBox mSaveImageBox;
    public static int mFingerNumidx = 0;
    private static int FingerTestCaseidx = 0;
    private static int mFingerDegreeidx = 0;
    private int verifyCountIndex = 9;
	private boolean mIsReachedLimit = false;
    private int mFingerOnIndex = 0;
    public EditText mUserName;
	private SharedPreferences mSetting;
	private AppSettings mAppSettings;
	private AppSettingsPreference.SENSOR_SIZE sensorSize = AppSettingsPreference.SENSOR_SIZE.SENSOR_SIZE_LARGE;
	private FingerUserSettings mFingerUserSettings;
    public static final String EXTRA_USER_NAME = "USER_NAME";
	public static final String EXTRA_FINGER_NUMBER_IDX = "mFingerNumidx";
	public static final String EXTRA_FINGER_TESTCASE_IDX = "FingerTestCaseidx";
	public static final String EXTRA_IS_SAVE_IMAGE = "CanSaveImage";
	public static final String EXTRA_FINGER_ON_INDEX = "FingerOnIndex";
    private boolean mIsBadImage;
	private MainFileSession mainFileSession;
	private ImageView mVerifyImage;
	private ImageView mVerifyImage2;
	private Point mDisplaySize;
	private AnimatorSet animatorSet;

    private int mEnrollMethod = FpResDef.ENROLL_METHOD_TOUCH;
    private int mFrameCount = 0;
    private boolean mIsStable;
    private boolean mIsTouching;
    private boolean mIsFastCase;
    private boolean mIsGetImageReady = true;
	private boolean mSetEngineerMode = false;

	private GifImageView gifImageView;
    private RelativeLayout mICWindowContainer;
	private RelativeLayout mSQWindowContainer;
	private FpActivityFingerBinding mBinding;
	private long mNonMatchAnimationStartTime;
	private CircleShaderView mGreenGradation;
	private Switch mVerifyCountSwitch;

    private int mTryMatchCount = 0;
    private int mVerifyFingerCount = 1;
    private int mMatchScore = 0;
    private int mLastTriedIndex = 0;
    private int mMatchedFPIndex = -1;
    private static final String VERIFY_REPORT_FILE_NAME = "rbs_verify_report.csv";
    private String[] timestamp = {"0", "0", "0"};

    private OtgSensorManager mOtgSensorManager = null;
    private Bitmap mVerifyImgEnlarge = null;
    private Bitmap mVerifyImgEnlarge2 = null;
    private Map<Integer, Point> mapTouchPoints = null;
    private static final int MOVE_DIST_THRESHOLD = 15*15;

    public Toast touchToast = null ;
    private TestCaseSettings mTestCaseSettings;
	private String gTempActivityLastUserID; // 解決 BACKUP時ID被COPY的問題

	private int mFingerImageCount = 0;

	@Override
	public void onKBVisibilityChanged(boolean visible) {
		Log.d(TAG, "onKBVisibilityChanged "+visible+", mIsVerifying");
		if(!visible && mIsVerifying){
			//UIUtils.hideSystemUI(getWindow().getDecorView());
			egistec.optical.demotool.igistec.Common.hideSystemUI(this);
		}
	}

	private class DebugMemFinishThread extends Thread {
		@Override
		public void run() {
			super.run();
			EgisLog.d(TAG, "DebugMemFinishThread run");
			if (mLib != null) {
				mLib.uninitialize();
				mLib = null;
				EgisLog.d(TAG, "BuildConfig.BUILD_TYPE = " + BuildConfig.BUILD_TYPE);
				int pid = android.os.Process.myPid();
				DebugUtil.killProcess(pid, 28);
				DebugUtil.dumpMemLog();
			}
			runOnUiThread(new Runnable() {
				@Override
				public void run() {
					finish();
				}
			});
		}
	}

	private View.OnClickListener onClickListener = new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			int pickvalue;
			switch (v.getId()) {
				case R.id.ok:
					EgisLog.d(TAG,"the picker value is "+mpick.getPickValue() + "id is"+ mpick.getSetid());
					pickvalue = mpick.getPickValue();
					switch (mpick.getSetid()){
						case 0:
							//SystemProperties.set("egis.ets.enroll.duplicate" ,String.valueOf(pickvalue));
							break;
						case 1:
							//SystemProperties.set("egis.ets.enroll.redundancy" ,String.valueOf(pickvalue));
							break;
						case 2:
							//SystemProperties.set("egis.ets.matcher.adaptor" ,String.valueOf(pickvalue));
							break;
					}
					mpick.dismiss();
					break;
				case R.id.cancel:
					mpick.dismiss();
					break;
			}
		}};

	@Override
	public void onOtgSensorInited() {
		EgisLog.d(TAG, "onOtgSensorInited ");
		mHandler.post(runnableRbsInitialization);
	}

	final class VerifyTask implements Runnable{
		@Override
		public void run(){
			if(!mIsVerifying || mIsReachedLimit) {
				return;
			}

			int[] fplist = new int[MAX_FINGERS];
			int fpcount = 0;

			fpcount = mFingers.size();
			for(int i = 0; i < mFingers.size(); i++) {
				fplist[i] = mFingers.get(i);
			}

			mLib.extraApi(PID_HOST_TOUCH, CMD_HOST_TOUCH_ENABLE, null, null, null);
			int ret = mLib.verify(0, fplist, fpcount, 1);
			
			if (ret == 0) {
				RefreshVerifyCounter();
				return;
			}
		}
	}

	private void RefreshVerifyCounter(){
		int success = mVerifySuccessCount;
		int fail = mVerifyFailCount;
		int error = mVerifyErrorCount;
		mVerifyStatus.setText(String.format("total=%d, success=%d, fail=%d, error=%d", success + fail + error, success, fail, error));
	}

	private List<Point> getFirstNFingers(int number) {
		List<Point> retList = new ArrayList<>();
		if(number <= 0 || mapTouchPoints.size() <=0) {
			return retList;
		}
		List<Integer> keys = new ArrayList<>(mapTouchPoints.keySet());
		Collections.sort(keys);
		for(int key:keys) {
			EgisLog.d(TAG, "PAPA MT KEY "+key +" ("+mapTouchPoints.get(key).x+", "+mapTouchPoints.get(key).y+")");
		}
		int count = keys.size() > number ? number : keys.size();
		for(int i=0;i<count;i++) {
			retList.add(mapTouchPoints.get(keys.get(i)));
		}
		return retList;
	}

	private Runnable runnableActionDown = new Runnable() {
		@Override
		public void run() {
			if (!hasActionUp) {
				EgisLog.d(TAG, "PAPA MT runnableActionDown hasActionUp == flase, SKIP!");
				return;
			}
			EgisLog.d(TAG, "PAPA MT runnableActionDown execute ");
			hasActionUp = false;
			if (!mIsVerifying && optionVerifyAutoStart) {
				EgisLog.i(TAG, "runnable D1 AutoStart");
				verifyAutoStarting = true;
				onVerifyClick(null);
			}
			if (mIsVerifying) {
				List<Point> listPoints = getFirstNFingers(mVerifyFingerCount);
				for(int i=0; i<listPoints.size();i++) {
					int x = listPoints.get(i).x;
					int y = listPoints.get(i).y;
					int x_ratio_x100 = (int)(x * 100 / mBinding.SQWindow.getWidth());
					int y_ratio_x100 = (int)(y * 100 / mBinding.SQWindow.getHeight());
					listPoints.set(i,new Point(x_ratio_x100, y_ratio_x100));
				}
				RbsUtil.setTouchPosToSDK(listPoints.size(), listPoints);
				for(Point point : listPoints){
					EgisLog.e(TAG, "PAPA MT : ("+point.x+", "+point.y+")");
				}
				hasResult = false;
				EgisLog.i(TAG, "runnable D1 action_down. optionNoCancelActionUp " + optionNoCancelActionUp);
				byte[] out_buf = new byte[4];
				int[] out_size = new int[1];
				out_size[0] = 4;
				mLib.extraApi(PID_HOST_TOUCH, CMD_HOST_TOUCH_SET_TOUCHING, null, out_buf, out_size);
				ByteBuffer bb = ByteBuffer.wrap(out_buf, 0, 4);
				bb.order(ByteOrder.LITTLE_ENDIAN);
				int temp_x10 = bb.getInt();
				EgisLog.d(TAG, "sys temperature = " + temp_x10);
				mBinding.textViewShowTemperatureInfo.setText("BTP: " + String.valueOf(temp_x10 / 10f));
				mIsTouching = true;
			}
		}
	};

	private Runnable runnableForceTriggerActionDown = new Runnable() {
		@Override
		public void run() {
			EgisLog.d(TAG, "PAPA MT runnableForceTriggerActionDown execute ");
			if(hasActionUp){
				mHandler.post(runnableActionDown);
			}
		}
	};

	private Runnable runnableRbsInitialization = new Runnable(){

		@Override
		public void run() {
		    EgisLog.i(TAG, "runnableRbsInitialization");
			if(mLib != null){
				int initResult = mLib.initialize();
				mLib.setActiveUserGroup(PATH_CURRENT_TEMPLATE);
				updateRbsInitStatus(initResult == 0);
			}
		}
	};

	private void setHasResult() {
		if (hasResult) {
			EgisLog.d(TAG, "hasResult : Already done");
			return;
		}
		hasResult = true;
		if (hasActionUp) {
			setHostTouchReset(mAppSettings.getPressAreaTypeId());
		} else {
			EgisLog.d(TAG, "hasResult : Skip");
		}
	}

	private void setHostTouchReset(int pressAreaTypeId) {
		EgisLog.i(TAG, "setHostTouchReset()");
		mLightDot.playStop();
		mLightDot.reset();
		mIsStable = false;
		mIsTouching = false;
		mLib.extraApi(PID_HOST_TOUCH, CMD_HOST_TOUCH_RESET_TOUCHING, null, null, null);
	}

	private Runnable runnableActionUp = new Runnable() {
		@Override
		public void run() {
			EgisLog.i(TAG, "runnable D1 action_up " + (hasActionUp ? "1" : "0"));
			hasActionUp = true;
			if (hasResult) {
				setHostTouchReset(mAppSettings.getPressAreaTypeId());
			} else if (!optionNoCancelActionUp) {
				hasResult = true;
				setHostTouchReset(mAppSettings.getPressAreaTypeId());
			}
		}
	};

    @Override
	protected void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.fp_activity_finger);

		// 啟動APP時設定一個ACTIVITY給ET760後台使用
		egistec.optical.demotool.igistec.Common.act = this ;

		Log.d(TAG, "" + BuildConfig.CUSTOMER);
		Log.d(TAG, "AuoOledFingerprintReceiverHelper.G_DEMO_INC_DEFINE_IN_MK" + AuoOledFingerprintReceiverHelper.G_DEMO_INC_DEFINE_IN_MK);

		mapTouchPoints = new HashMap<>();
		hasActionUp = true;

		mSetEngineerMode = DemoToolApp.getInstance().isEngineerMode();
		IMAGE_ROOT_PATH = DemoToolApp.getMyDataRoot();
		RootPath = DemoToolApp.getMyDataRoot();
		PATH_CURRENT_TEMPLATE = DemoToolApp.getMyDataRoot() + File.separator +
				FPFingerActivity.TEMPLATE_PATH;
		File templateDir = new File(PATH_CURRENT_TEMPLATE);
		try {
			if (!templateDir.exists()) {
				templateDir.mkdirs();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this,
                        new String[] {
                                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                Manifest.permission.WRITE_EXTERNAL_STORAGE
                        },
                REQUEST_CODE_ASK_WRITE_EXTERNAL_STORAGE_PERMISSIONS);
            }
        }

        Display display = getWindowManager().getDefaultDisplay();
		mDisplaySize = new Point();
		display.getSize(mDisplaySize);
		
		mainFileSession = new MainFileSession(RootPath);
		mAppSettings = new AppSettings(this);
		sensorSize = AppSettingsPreference.getSensorSize(mAppSettings.getSensorSizeIndex());

		mBinding = DataBindingUtil.setContentView(this, R.layout.fp_activity_finger);

		mUserName = (EditText) findViewById(R.id.userID);
		mVerifyBtn = (Button) findViewById(R.id.verify_btn);
		mSet		 = (Button) findViewById(R.id.setting);
		mTouchEvent = (Button) findViewById(R.id.IC_window_verify);
		mICWindowContainer = (RelativeLayout) findViewById(R.id.IC_window_verify_container);
		mSQWindowContainer = (RelativeLayout) findViewById(R.id.SQ_Window_container);
		mBtnContainer = (LinearLayout) findViewById(R.id.verify_btn_container);
		mVerifyStatus = (TextView) findViewById(R.id.verify_status);
		mFingerStatus = (ImageView) findViewById(R.id.fingerprint);
		mVerifyImage = (ImageView) findViewById(R.id.verify_image);
		gifImageView = (GifImageView)findViewById(R.id.enroll_gif);
		mVerifyImage2 = (ImageView) findViewById(R.id.verify_image_2);
		mVerifyCountSwitch = (Switch) findViewById(R.id.verify_count_switch);
		mGreenGradation = new CircleShaderView(this);
		mGreenGradation.setCircleCanvas(mICWindowContainer.getLayoutParams().width, mICWindowContainer.getLayoutParams().height);
		float diameter = AppSettingsPreference.getPressAreaDiameter(mAppSettings.getPressAreaDiameterIndex());
		mGreenGradation.setRadius(diameter / 2 + CircleShaderView.GRADATION_RADIUS);
		mICWindowContainer.addView(mGreenGradation);
		mLightDot = new LightDotControl(gifImageView, mGreenGradation, mAppSettings);

		switch (sensorSize){
			case SENSOR_SIZE_LARGE:
				mICWindowContainer.setVisibility(View.GONE);
				mSQWindowContainer.setVisibility(View.VISIBLE);
				break;
			case SENSOR_SIZE_NORMAL:
				mICWindowContainer.setVisibility(View.VISIBLE);
				mSQWindowContainer.setVisibility(View.GONE);
				break;
			default:
				break;
		}

		setKeyboardVisibilityListener(this);

		mTouchEvent.setOnTouchListener(new View.OnTouchListener() {
			@Override
			public boolean onTouch(View view, MotionEvent motionEvent) {
				int action = motionEvent.getAction();
				switch (action) {
					case MotionEvent.ACTION_DOWN: {
						mLightDot.playStart();
						if(mAppSettings.getCustomizePressAreaIcon() && mAppSettings.getAddViewTreeObserver()) {
							mLightDot.setOnGifLoadCB(new LightDotControl.OnGifLoadCB() {
								public void onGifLoad() {
									EgisLog.i(TAG, "Receive onGifLoad ");
									mHandler.post(runnableActionDown);
									mLightDot.removeOnGifLoadCB();
								}
							});
						} else mHandler.post(runnableActionDown);
						break;
					}
					case MotionEvent.ACTION_UP: {
						mHandler.post(runnableActionUp);
						break;
					}
					default:
						break;
				}
				return false;
			}
		});

		mBinding.SQWindow.setOnTouchListener(new View.OnTouchListener() {
			@Override
			public boolean onTouch(View view, MotionEvent motionEvent) {
				int action = motionEvent.getActionMasked();
				switch (action) {
					case MotionEvent.ACTION_DOWN: {
						int x = (int)motionEvent.getX(motionEvent.getActionIndex());
						int y = (int)motionEvent.getY(motionEvent.getActionIndex());
						if(mapTouchPoints == null) {
							mapTouchPoints = new HashMap<>();
						}

						mapTouchPoints.put(motionEvent.getActionIndex(), new Point(x, y));
						EgisLog.d(TAG, "PAPA MT runnableForceTriggerActionDown trigger "+x+", "+y);
						if(mapTouchPoints.size() == mVerifyFingerCount) {
							mHandler.post(runnableActionDown);
						}else{
							mHandler.postDelayed(runnableForceTriggerActionDown, 4000);
						}
						break;
					}
					case MotionEvent.ACTION_UP: {
						if(hasActionUp) {
							mHandler.removeCallbacks(runnableForceTriggerActionDown);
							EgisLog.d(TAG, "PAPA MT ACTION_UP without trigger action down");
							break;
						}
						mHandler.post(runnableActionUp);
						if(mapTouchPoints != null) {
							mapTouchPoints.clear();
						}
						break;
					}
					case MotionEvent.ACTION_MOVE: {
						int actionIndex = motionEvent.getActionIndex();
						int x = (int)motionEvent.getX(actionIndex);
						int y = (int)motionEvent.getY(actionIndex);
						if(mapTouchPoints.containsKey(actionIndex)) {
							float old_x = mapTouchPoints.get(actionIndex).x;
							float old_y = mapTouchPoints.get(actionIndex).y;
							if((pow(x-old_x, 2) + pow(y-old_y, 2)) < MOVE_DIST_THRESHOLD) {
								break;
							}
							mapTouchPoints.remove(actionIndex);
							mapTouchPoints.put(actionIndex, new Point(x, y));
						}
						mapTouchPoints.put(motionEvent.getActionIndex(), new Point(x, y));
						break;
					}
					case MotionEvent.ACTION_POINTER_DOWN: {
						int x = (int)motionEvent.getX(motionEvent.getActionIndex());
						int y = (int)motionEvent.getY(motionEvent.getActionIndex());
						if(mapTouchPoints == null) {
							mapTouchPoints = new HashMap<>();
						}
						mapTouchPoints.put(motionEvent.getActionIndex(), new Point(x, y));
						if(mapTouchPoints.size() == mVerifyFingerCount) {
							mHandler.removeCallbacks(runnableForceTriggerActionDown);
							mHandler.post(runnableActionDown);
						}
						break;
					}
					case MotionEvent.ACTION_POINTER_UP: {
						int actionIndex = motionEvent.getActionIndex();
						if(mapTouchPoints != null && mapTouchPoints.containsKey(actionIndex)) {
							EgisLog.d(TAG, "PAPA MT onTouch ACTION_POINTER_UP mapTouchPoints size becomes "+mapTouchPoints.size());
							mapTouchPoints.remove(actionIndex);
						}
						break;
					}
					default:
						break;
				}
				return false;
			}
		});

		View.OnClickListener clickListener = new View.OnClickListener() {
			public void onClick(View v) {
				if (v.equals(mVerifyImage)) {
					ImageEnlarge.ImgEnlarge(FPFingerActivity.this, mVerifyImgEnlarge);
				} else if (v.equals(mVerifyImage2)) {
					ImageEnlarge.ImgEnlarge(FPFingerActivity.this, mVerifyImgEnlarge2);
				}
			}
		};
		mVerifyImage.setOnClickListener(clickListener);
		mVerifyImage2.setOnClickListener(clickListener);

		mVerifyCountSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				// Do not show toast if toggled programmatically
				if (isChecked) {
					mVerifyImage2.setVisibility(View.VISIBLE);
					mVerifyFingerCount = 2;
				} else {
					mVerifyImage2.setVisibility(View.GONE);
					mVerifyFingerCount = 1;
				}
				String[][] ini_item_update = {
						{FpResDef.KEY_VERIFY_FINGER_COUNT, String.format("%d", mVerifyFingerCount)},
				};
				int retval = IniSettings.updateIniSettings(mLib, IniSettings.INI_CONFIG_TYPE, FpResDef.INI_SECTION_VERIFY, ini_item_update);
				if (retval == FpResDef.RESULT_OK)
					Toast.makeText(getApplicationContext(), "Success update "+ mVerifyFingerCount+ " finger to ini", Toast.LENGTH_SHORT).show();
				else
					Toast.makeText(getApplicationContext(), "!!! Fail update "+ mVerifyFingerCount+ " finger to ini", Toast.LENGTH_SHORT).show();
			}
		});

		//CheckBox
		mSaveImageBox=(CheckBox)findViewById(R.id.CheckBoxSaveImage);
		mSaveImageBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener(){ 
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				// Do not show toast if toggled programmatically
				if (!mSaveImageBox.isPressed()) {
					return;
				}

				if (isChecked) {
					//Toast.makeText(FPFingerActivity.this, "Save", Toast.LENGTH_SHORT).show();
					AlertDialog.Builder dialog = new AlertDialog.Builder(FPFingerActivity.this);
					dialog.setMessage("Start to save images ?");
					dialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							mSaveImageBox.setChecked(false);
						}
					});
					dialog.setPositiveButton("Save", new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							Toast.makeText(FPFingerActivity.this, "Save", Toast.LENGTH_SHORT).show();
						}
					});
					dialog.show();
				} else {
					Toast.makeText(FPFingerActivity.this, "Not Save", Toast.LENGTH_SHORT).show();
				}
			}
		});
		
		//Spinner
		mFingerNumSpinner = (Spinner)findViewById(R.id.SpinnerFingerNumber);
		ArrayAdapter aaFingerNumSpinner = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, mFingerNum) ;
		aaFingerNumSpinner.setDropDownViewResource(R.layout.spinner_test_case);
	    mFingerNumSpinner.setAdapter(aaFingerNumSpinner);
	    mFingerNumSpinner.setOnItemSelectedListener(new OnItemSelectedListener() {

				@Override
				public void onItemSelected(AdapterView<?> arg0, View arg1,
						int position, long arg3) {
					mFingerNumidx = position ;
				}

				@Override
				public void onNothingSelected(AdapterView<?> arg0) {
					// TODO Auto-generated method stub
					
				}
	    });

		mTestCaseSpinner = (Spinner) findViewById(R.id.FingerTestCase);
		mTestCaseSpinner.setAdapter(new ArrayAdapter<String>(this, R.layout.exp_spinner_row, mFingerTestCase));
		mTestCaseSpinner.setOnItemSelectedListener(new OnItemSelectedListener(){
			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
									   int position, long arg3) {
				FingerTestCaseidx = position ;
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {

			}
		});
		mTestCaseSpinner.setVisibility(View.INVISIBLE);

	    verifyCountSpinner = (Spinner) findViewById(R.id.verifyCountSpinner);
	    verifyCountAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, verifyCountList) ;
	    verifyCountAdapter.setDropDownViewResource(R.layout.spinner_test_case);
	    verifyCountSpinner.setAdapter(verifyCountAdapter);
	    verifyCountSpinner.setOnItemSelectedListener(new OnItemSelectedListener(){
			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int position, long arg3) {

				if ( position <= 10) {
					// 0 ~ 9 = 10 ~ 100
					verifyCountIndex = position ;
					String temp = verifyCountList[verifyCountIndex] ;
					temp = temp.replace("*", "") ;
					verifyCount = Integer.valueOf(temp) ;
					EgisLog.d(TAG, "+++ verifyCountList +++ " + verifyCountIndex + "position" +position);
				}
				else if ( position == 11 ) {
					// 1.show custom value input
					// 2. set position = 11, save value
					// 3. update array list, notifi data changed
					showVerifyCountUI() ;
				}

			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				
			}
	    });

		if(sensorSize == AppSettingsPreference.SENSOR_SIZE.SENSOR_SIZE_LARGE) {
			sqWindowPos = FileIoUtils.loadSQWindowLayoutData();
			ExperimentUtils.setSQPressArea(mSQWindowContainer, sqWindowPos.x, sqWindowPos.y, sqWindowPos.width, sqWindowPos.height
					, getResources().getDisplayMetrics());
		} else {
			icWindowPos = FileIoUtils.loadIcWindowLayoutData();
			ExperimentUtils.setPressAreaLeftTop(mICWindowContainer, icWindowPos.x, icWindowPos.y, getResources().getDisplayMetrics());
		}

	    //UI control
	    if(!Boolean.valueOf(getString(R.string.save_img))){
			//mUserName.setVisibility(View.GONE);
			//ViewUserID.setVisibility(View.GONE);
			mSaveImageBox.setVisibility(View.GONE);
			mFingerNumSpinner.setVisibility(View.GONE);
			//ViewFingerNumber.setVisibility(View.GONE);
            mTestCaseSpinner.setVisibility(View.GONE);
	    }

		mShowVerifyCount = Boolean.valueOf(getString(R.string.verify_count));
		mShowVerifyLimit = Boolean.valueOf(getString(R.string.verify_count_limit));
		if(!mShowVerifyCount){
			mVerifyStatus.setVisibility(View.GONE);
			verifyCountSpinner.setVisibility(View.GONE);
		}
		else{
			if(!mShowVerifyLimit){
				verifyCountSpinner.setVisibility(View.GONE);
			}
		}

		// Show Info Table
		if (BuildConfig.ALGO_VER == 2) {
			mBinding.qtyPartial.setText("SPP");
		}

		RefreshVerifyCounter();

		mOtgSensorManager = OtgSensorManager.getInstance(OTG_SENSOR_ID, this);
		mOtgSensorManager.setOtgSensorEventListener(this);
		mLib = RbsLib.getInstance(this);
		if (BuildConfig.JNI_INTERFACE) {
			mLib.setActiveUserGroup(PATH_CURRENT_TEMPLATE);
			mLib.extraApi(ExtraApiDef.PID_DEMOTOOL, ExtraApiDef.EXTRA_DT_SET_INI_CONFIG_PATH,
					FPUtil.StringToCByteArray(IMAGE_ROOT_PATH), null, null);
		}
		boolean rbsInitSuccess = false;
		if(mLib != null) {
			mLib.startListening(mFingerprintReceiver);
			int initResult = mLib.initialize();
			mLib.setActiveUserGroup(PATH_CURRENT_TEMPLATE);
			EgisLog.i(TAG, "initResult " + initResult);
			mHandler = new Handler(Looper.getMainLooper());
			mFingers = new ArrayList<Integer>();
			initFingerListView();
			initPopupWindow();
			mSetting = getSharedPreferences("EGISFP", 0);
            rbsInitSuccess = initResult == 0;
		} else {
			mSaveImageBox.setEnabled(false);
			verifyCountSpinner.setEnabled(false);
			mFingerNumSpinner.setEnabled(false);
            mTestCaseSpinner.setEnabled(false);
			mSet.setEnabled(false);
			mVerifyBtn.setEnabled(false);
			mUserName.setEnabled(false);
		}
		updateRbsInitStatus(rbsInitSuccess);

		mFingerUserSettings = new FingerUserSettings(this);

		mSQWindowContainer.bringToFront(); //拉到前面

		mTestCaseSettings = TestCaseSettings.getInstance();
		mTestCaseSettings.init(this);
		mTestCaseSettings.updateBrief();


		//Intent intent = new Intent(this,  egistec.optical.demotool.igistec.t6.SelectActivity.class);
		//startActivity(intent);
		egistec.optical.demotool.igistec.Common.hideSystemUI(this);
		//AuoOledFingerprintReceiverHelper.find_crash_bug() ;
	}

	/**
	 * show custom ui for input custom verify count
	 */
	private void showVerifyCountUI() {

		AlertDialog.Builder editDialog = new AlertDialog.Builder(FPFingerActivity.this);
		editDialog.setTitle("Custom Verify Count");

		final EditText editText = new EditText(FPFingerActivity.this);
		editText.setText("" + verifyCount);
		editText.setInputType(InputType.TYPE_CLASS_NUMBER);

		int maxLength = 3; // so max is 999
		editText.setFilters(new InputFilter[] {new InputFilter.LengthFilter(maxLength)});

		editDialog.setView(editText);

		editDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
			// do something when the button is clicked
			public void onClick(DialogInterface arg0, int arg1) {
				verifyCount = Integer.parseInt(editText.getText().toString()) ;
				verifyCountList[10] =  "*"  + verifyCount ;
				verifyCountAdapter.notifyDataSetChanged();
				verifyCountSpinner.setSelection(10); // 10 = selected
			}
		});
		editDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
			// do something when the button is clicked
			public void onClick(DialogInterface arg0, int arg1) {
				verifyCountIndex = 0 ;
				verifyCountSpinner.setSelection(verifyCountIndex);
			}
		});

		editDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
			@Override
			public void onCancel(DialogInterface dialogInterface) {
				verifyCountIndex = 0 ;
				verifyCountSpinner.setSelection(verifyCountIndex);
			}
		}) ;
		editDialog.show();

	}

	@Override
	protected void onResume(){
		EgisLog.d(TAG, "+++++ onResume +++++");
		super.onResume();

		if(sensorSize == AppSettingsPreference.SENSOR_SIZE.SENSOR_SIZE_LARGE) {
			sqWindowPos = FileIoUtils.loadSQWindowLayoutData();
			ExperimentUtils.setSQPressArea(mSQWindowContainer, sqWindowPos.x, sqWindowPos.y, sqWindowPos.width, sqWindowPos.height
					, getResources().getDisplayMetrics());
		} else {
			icWindowPos = FileIoUtils.loadIcWindowLayoutData();
			ExperimentUtils.setPressAreaLeftTop(mICWindowContainer, icWindowPos.x, icWindowPos.y, getResources().getDisplayMetrics());
		}


		// Load settings from shared preference
		mUserName.setText(mFingerUserSettings.getUserId());
		mFingerNumidx = mFingerUserSettings.getFingerNumberIndex();
        FingerTestCaseidx = mFingerUserSettings.getFingerTestCaseIndex();
        mFingerDegreeidx = mFingerUserSettings.getFingerDegreeIndex();
		verifyCountIndex = mFingerUserSettings.getFingerLimitIndex();
		verifyCount = mFingerUserSettings.getVerifyCountCustomValue();
		verifyCountList[10] = "*" + verifyCount ;
		mSaveImageBox.setChecked(mFingerUserSettings.getIsSaveImage());
		mVerifyFingerCount = mFingerUserSettings.getVerifyFingerCount();

		mVerifyImgEnlarge = null;
		mVerifyImgEnlarge2 = null;
		optionNoCancelActionUp = mAppSettings.getNoCancelActionUp();

		optionScreenBrightness = mAppSettings.getScreenBrightness();
		int backup_brightness = mAppSettings.getBackupScreenBrightness();

		mLightDot.reset();

		if (mVerifyFingerCount == 2) {
			mVerifyImage2.setVisibility(View.VISIBLE);
			mVerifyCountSwitch.setChecked(true);
		} else {
			mVerifyImage2.setVisibility(View.GONE);
			mVerifyCountSwitch.setChecked(false);
		}

		if(mLib != null) {
			mLib.startListening(mFingerprintReceiver);
			mIsVerifyAnimate = false;
			retryFresh = 0;
			mHandler.postDelayed(freshList, 500);
			mFingerNumSpinner.setSelection(mFingerNumidx);
            mTestCaseSpinner.setSelection(FingerTestCaseidx);
			verifyCountSpinner.setSelection(verifyCountIndex);
			ByteBuffer Temp = ByteBuffer.allocate(4);
			Common.intToBytes(optionScreenBrightness, Temp.array());
			int ret;
			if (optionScreenBrightness == 0)
			    ret = 0;
			else
			    ret = mLib.extraApi(PID_HOST_TOUCH, CMD_DEMOTOOL_SET_SCREEN_BRIGHTNESS, Temp.array(), null, null);
			EgisLog.d(TAG, "set brightness ret = " + ret);

			if (ret < 0) {
				Toast.makeText(this, "set brightness fail", Toast.LENGTH_LONG).show();
				mAppSettings.setScreenBrightness(backup_brightness);
			} else
				mAppSettings.setBackupScreenBrightness(optionScreenBrightness);
			if (!mIsVerifying) {
				mVerifyBtn.setText(R.string.verify);
				setButtonStatus(true);
			}

			mLib.setActiveUserGroup();
		}else{
			Toast.makeText(this,"so load fail or device do't sup fp", Toast.LENGTH_LONG).show();
		}
	}

	@Override
	protected void onPause(){
		EgisLog.d(TAG, "+++++ onPause +++++");

		if(mIsVerifying){
			if (mLib != null) {
				mLib.cancel();
			}
			mIsVerifying = false;
			verifyCountSpinner.setEnabled(true);
		}

		if(sensorSize == AppSettingsPreference.SENSOR_SIZE.SENSOR_SIZE_LARGE) {
			UIUtils.showSystemUI(getWindow().getDecorView());
		}

		// Store settings to shared preference
		mFingerUserSettings.setUserId(mUserName.getText().toString());
		mFingerUserSettings.setFingerNumberIndex(mFingerNumidx);
        mFingerUserSettings.setFingerTestCaseIndex(FingerTestCaseidx);
        mFingerUserSettings.setFingerDegreeIndex(mFingerDegreeidx);
		mFingerUserSettings.setFingerLimitIndex(verifyCountIndex);
		mFingerUserSettings.setVerifyCountCustomValue(verifyCount);
		mFingerUserSettings.setIsSaveImage(mSaveImageBox.isChecked());
		mFingerUserSettings.setVerifyFingerCount(mVerifyFingerCount);

		super.onPause();
	}

	@Override
	protected void onDestroy(){
		mainFileSession.Destroy();
		super.onDestroy();
	}

    private void SetEngineerLayoutVisibility(Menu menu, boolean isVisible) {
        MenuItem MenuSettings = (MenuItem) menu.findItem(R.id.menu_settings);
        MenuItem MenuCalibrate = (MenuItem) menu.findItem(R.id.menu_calibrate);
        MenuItem MenuFindDotLocation = (MenuItem) menu.findItem(R.id.menu_find_dot_location);
        MenuItem MenuLiveView = (MenuItem) menu.findItem(R.id.menu_live_view);
        MenuItem MenuBackupTemplate = (MenuItem)  menu.findItem(R.id.menu_backup_template);
        MenuItem MenuAppSettings = (MenuItem) menu.findItem(R.id.menu_app_settings);


		MenuSettings.setVisible(true);
        MenuCalibrate.setVisible(isVisible);
        MenuLiveView.setVisible(isVisible);
        MenuBackupTemplate.setVisible(isVisible);

		if ( AuoOledFingerprintReceiverHelper.G_DEMO_INC_DEFINE_IN_MK ) {
			MenuAppSettings.setVisible(false);
			MenuFindDotLocation.setVisible(false);
			MenuBackupTemplate.setVisible(false);

		} else if (BuildConfig.BUILD_TYPE_RELEASE == 1){
			MenuCalibrate.setVisible(false);
			MenuAppSettings.setVisible(false);
			MenuFindDotLocation.setVisible(false);
			MenuItem menuFFC_BDS_view = (MenuItem) menu.findItem(R.id.menu_ffc_bds_view);
			menuFFC_BDS_view.setVisible(false);
		}

        int visibility = isVisible ? View.VISIBLE : View.INVISIBLE;
        mBinding.extractQty.setVisibility(visibility);
        mBinding.matchScore.setVisibility(visibility);
        mBinding.qtyPartial.setVisibility(visibility);
        mBinding.bdsPath.setVisibility(visibility);
        mBinding.pol.setVisibility(visibility);
        mBinding.isLearning.setVisibility(visibility);
        mBinding.tableFake.setVisibility(visibility);
        mBinding.CheckBoxSaveImage.setVisibility(visibility);
        mBinding.blackEdge.setVisibility(visibility);

        mBinding.extractQtyValue.setVisibility(visibility);
        mBinding.matchScoreValue.setVisibility(visibility);
        mBinding.qtyPartialValue.setVisibility(visibility);
        mBinding.bdsPathValue.setVisibility(visibility);
        mBinding.polValue.setVisibility(visibility);
        mBinding.isLearningValue.setVisibility(visibility);
        mBinding.tableFakeValue.setVisibility(visibility);
        mTestCaseSpinner.setVisibility(View.INVISIBLE);
        mBinding.blackEdgeValue.setVisibility(visibility);
    }

	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		menu.clear(); // Clear the menu first

		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.fp_set, menu);
        SetEngineerLayoutVisibility(menu, mSetEngineerMode);
        return super.onPrepareOptionsMenu(menu);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu){
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.fp_set, menu);
		return super.onCreateOptionsMenu(menu);
	}

	private void initPopupWindow() {
		View view = this.getLayoutInflater().inflate(R.layout.setting, null);
		mPopupMenu = new PopupWindow(view,
		  android.view.ViewGroup.LayoutParams.WRAP_CONTENT,
		  android.view.ViewGroup.LayoutParams.WRAP_CONTENT);
		mPopupMenu.setFocusable(true);
		mPopupMenu.setBackgroundDrawable(new PaintDrawable());
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		boolean isConsumed = true;
        Intent i;
        if(mIsVerifying) {
			onVerifyClick(null);
		}
		if (BuildConfig.BUILD_TYPE_RELEASE == 1) {
			displayVersionDialog();
			return super.onOptionsItemSelected(item);
		}
		if(!mSetEngineerMode) {
			passwordDialogEvent();
			return super.onOptionsItemSelected(item);
		}
		switch (item.getItemId()) {
		case R.id.menu_settings:
			i = new Intent(this, SettingsActivity.class);
			startActivityForResult(i, REQ_CODE_SETTINGS);
			break;
		case R.id.menu_app_settings:
			i = new Intent(this, AppSettingsActivity.class);
			startActivity(i);
			break;
		case R.id.menu_calibrate:
			i = new Intent(this, CalibrateActivity.class);
			startActivity(i);
			break;
		case R.id.menu_find_dot_location:
			i = new Intent(this, LocationActivity.class);
			startActivity(i);
			break;
		case R.id.menu_live_view:
			i = new Intent(this, LiveViewActivity.class);
			startActivity(i);
			break;
		case R.id.menu_backup_template:
			String userId = "";
			if (mFpList.size() > 1) {
				userId = mFpList.get(1).getFingerName().split("-")[0];
			}

			callTemplateDatabaseActivity(userId);

			// 修正 BACKUP TEMPLATE 問題
			gTempActivityLastUserID = mUserName.getText().toString() ;
			Log.e(TAG, "@@ temp backup user name:" + gTempActivityLastUserID ) ;
			break;
			/*
		case R.id.menu_temp_live_view:
			i = new Intent(this, TempLiveViewActivity.class);
			startActivity(i);
			break ;
			 */

		case R.id.menu_ffc_bds_view:
			i = new Intent(this, FFCBDSActivity.class);
			startActivity(i);
			break ;

		default:
			isConsumed = false;
			break;
		}
        return isConsumed || super.onOptionsItemSelected(item);
    }

	// create menu for del and switch DB
	@Override
	public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo){
		if (v.getId() == R.id.finger_list){
			ListView lv = (ListView) v;
			AdapterView.AdapterContextMenuInfo acmi = (AdapterContextMenuInfo) menuInfo;
			mClickedFingerInfo = (FPFingerInfo) lv.getItemAtPosition(acmi.position); 

			if (ADD_FINGER_POSITION == acmi.position)
				return;

			super.onCreateContextMenu(menu, v, menuInfo);
			menu.setHeaderTitle(mClickedFingerInfo.getFingerName());
			MenuInflater inflater = getMenuInflater();
			inflater.inflate(R.menu.fp_action, menu);
		}
	}

	@Override
	public boolean onContextItemSelected(MenuItem item){
		EgisLog.d(TAG, "onContextItemSelected: " + item.getItemId() + "getTitle: " + item.getTitle());
		if (mIsVerifying) {
			onVerifyClick(null);
		}
		if (item.getTitle().equals(getResources().getString(R.string.context_menu_del))) {
			AlertDialog.Builder dialog = FPUtil.getDilog(this, R.string.dialog_delete_msg);
			dialog.setPositiveButton(R.string.dialog_ok, new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int whichButton) {
					if (mLib.removeFingerprint(0, mClickedFingerInfo.getFingerId()) == FpResDef.RESULT_OK) {
						mFpList.remove(mClickedFingerInfo);
						mAdapter.notifyDataSetChanged();
						saveFingerListToCSV();
					} else {
						Toast.makeText(FPFingerActivity.this, R.string.toast_delete_finger_failed, Toast.LENGTH_SHORT).show();
					}
				}
			}).setNegativeButton(R.string.dialog_cancel, new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int whichButton) {
				}
			}).create().show();
		} else if (item.getTitle().equals(getResources().getString(R.string.context_menu_backup_template))) {
			EgisLog.d(TAG, "Manage template database");
			callTemplateDatabaseActivity(mClickedFingerInfo.getFingerName().split("-")[0]);

			gTempActivityLastUserID = mUserName.getText().toString() ;
			Log.e(TAG, "@@ temp backup user name(at long click user name):" + gTempActivityLastUserID ) ;
		}
		return true;
	}

	private void resetVerifyCounter() {
		mVerifySuccessCount = 0;
		mVerifyFailCount = 0;
		mVerifyErrorCount = 0;
		mVerifyQuickMatchCount = 0;
		mVerifyFakeFingerCount = 0;
	}

	public void onResetClick(View v) {
		resetVerifyCounter();
		RefreshVerifyCounter();
	}
	
	private void initFingerListView(){
		mFingerListView = (ListView) findViewById(R.id.finger_list);
		mFingerListView.setOnItemClickListener(new OnItemClickListener(){
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id){
				EgisLog.d(TAG, "click id: " + id + " position: " + position + " isVerifing " + mIsVerifying);

				if (ADD_FINGER_POSITION == (int) id){
					if ((MAX_FINGERS + 1) <= mFpList.size()){ // we can only register 4 fingers
						Toast.makeText(view.getContext(), "can only register " + MAX_FINGERS + " fingers", Toast.LENGTH_SHORT).show();
						return;
					}

					if (mIsVerifying){
						Toast.makeText(FPFingerActivity.this, R.string.dialog_abort_verify_first, Toast.LENGTH_SHORT).show();
						return;
					} else{
						doEnroll();
					}
				}
			}
		});

		registerForContextMenu(mFingerListView);
		mFpList = new FPList(this);
		mFpList.add(new FPFingerInfo("AddFinger", -1, this));
		mAdapter = new FPAdapter(this, mFpList, mHandler);
		mFingerListView.setAdapter(mAdapter);
	}


	private Intent mLocalStartDoEnrollIntent = null ;
	private void doEnroll(){
		int fingerNumber = findNextFinger();

		Intent intent = null;
		intent = new Intent(FPFingerActivity.this, FPEnrollActivity.class);

		intent.putExtra(EXTRA_ENROLL_ID, fingerNumber);
		intent.putExtra(EXTRA_USER_NAME, mUserName.getText().toString());
        intent.putExtra(EXTRA_FINGER_TESTCASE_IDX, FingerTestCaseidx);
        intent.putExtra(EXTRA_FINGER_NUMBER_IDX, mFingerNumidx);
		intent.putExtra(EXTRA_IS_SAVE_IMAGE, mSaveImageBox.isChecked());
		intent.putExtra(EXTRA_FINGER_ON_INDEX, mFingerOnIndex);
		intent.putExtra(EXTRA_OPTION_NOCANCEL_ACTIONUP, optionNoCancelActionUp);
		Bundle bundle = new Bundle();

		intent.putExtras(bundle);

		// for google ui only:
		// 如果已經有該人&該指，提示警告
		boolean isDuplicate = false ;
		mLocalStartDoEnrollIntent = intent ;

		for (FPFingerInfo info : mFpList) {
			if ( info.getFingerId() == -1) {
				continue ;
			}
			String currentUserID = TestCaseSettings.FormatID(mUserName.getText().toString()) ;
			Log.e(TAG, info.getFingerName() + " / " + currentUserID) ;

			int idxInt = -100 ;
			String userId = "none_of_this_id" ;
			try {
				userId = info.getFingerName().split("-")[0] ;
				String idxString = info.getFingerName().split("-")[1];
				idxInt = Integer.parseInt(idxString) ;
			}
			catch (Exception ex) {
				Log.e(TAG, "Parse Name to get IDX Error") ;
			}
			if ( userId.equalsIgnoreCase( currentUserID))
			{
				if (idxInt == mFingerNumidx ) {
					isDuplicate = true ;
				}
			}
		}

		if (isDuplicate) {
			AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);

			alertDialog.setTitle("Notice!");
			alertDialog.setMessage( "you have a same User ID and Finger in database" +
					", do you went to continue??" );

			alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog,int which) {
					dialog.cancel();
				}
			});
			alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog,int which) {
					startActivityForResult(mLocalStartDoEnrollIntent, REQ_ENROLL);
				}
			});

			alertDialog.show() ;
		}
		else {
			// 沒人&該指，持續進行
			startActivityForResult(intent, REQ_ENROLL);
		}
	}

	private void callTemplateDatabaseActivity(String userId){
		Intent intent = new Intent(FPFingerActivity.this, TemplateDbActivity.class);

		intent.putExtra(EXTRA_USER_NAME, userId);

		Bundle bundle = new Bundle();

		intent.putExtras(bundle);
		startActivityForResult(intent, REQ_CURRENT_TEMPLATE_USER_ID);
	}

	private void updateFingerList(List<Integer> fingersId){
		EgisLog.d(TAG, "updatefingerList = " + fingersId);
		mFpList.clear();
		mFpList.add(new FPFingerInfo("AddFinger", -1, this));
		if (fingersId == null){
			return;
		}
		if (fingersId.size() <= 0){
			mAdapter.notifyDataSetChanged();
			saveFingerListToCSV();
			return;
		}		
		for(int i = 0; i < fingersId.size(); i++){					
			Integer id = fingersId.get(i);
			String fingerName = getFingerName(id);
			EgisLog.d(TAG, "fingerName = " + fingerName +", id =" + id);
			if(fingerName == null){
				fingerName = mUserName.getText().toString() + "-" + mFingerNum[mFingerNumidx] + " (" + id + ")";
				setFingerName(id, fingerName);				
			}
			EgisLog.d(TAG, "fingerName = " + fingerName + " fingersId = " + id);
			FPFingerInfo info = new FPFingerInfo(fingerName, id, this);
			mFpList.add(info);
		}
		mAdapter.notifyDataSetChanged();
		saveFingerListToCSV();
	}
	
	private int findNextFinger(){
		EgisLog.d(TAG, "+++++ findNextFinger +++++");
		Random random = new Random();
		int fingerId;
		boolean isDuplicate;

		do {
			fingerId = Math.abs(random.nextInt());
			isDuplicate = false;

			for(int i = 0; i < mFingers.size(); i++) {
				if(fingerId == mFingers.get(i)) {
					isDuplicate = true;
					break;
				}
			}
		} while(isDuplicate);

		return fingerId;
	}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event){
		if (keyCode == KeyEvent.KEYCODE_BACK){ // BACK button pressed
			if(isFinishing()){
				return true;
			}
			if (!mKeyBackPressed){
				mKeyBackPressed = true;
				mLastDownTime = event.getDownTime();
				Toast.makeText(this, "Please click back again to cancel", Toast.LENGTH_SHORT).show();
				new Timer().schedule(new TimerTask(){
					@Override
					public void run(){
						mKeyBackPressed = false;
						mLastDownTime = 0;
					}
				}, 500);
				return false;
			}
			mKeyBackPressed = false;
			boolean isDubClick = (event.getDownTime() != mLastDownTime);
			EgisLog.d(TAG, "onKeyDown ret=" + (isDubClick ? "true" : "false") + " t1" + event.getDownTime() + "t2=" + mLastDownTime);
			if(isDubClick) {
				if (mIsVerifying) {
					mIsVerifying = !mIsVerifying;
					mLib.cancel();
				}
				if(BuildConfig.BUILD_TYPE == "debugmem") {
					DebugMemFinishThread finishThread = new DebugMemFinishThread();
					finishThread.start();
					return true;
				}
			}
			return super.onKeyDown(keyCode, event);
		}
		return super.onKeyDown(keyCode, event);
	}

	@Override
	public void onWindowFocusChanged(boolean hasFocus) {
		super.onWindowFocusChanged(hasFocus);
		Log.d(TAG, "onWindowFocusChanged "+hasFocus);
		if(sensorSize == AppSettingsPreference.SENSOR_SIZE.SENSOR_SIZE_LARGE) {
			if (hasFocus && mIsVerifying) {
				//UIUtils.hideSystemUI(getWindow().getDecorView());
				egistec.optical.demotool.igistec.Common.hideSystemUI(this);
			} else {
				//UIUtils.showSystemUI(getWindow().getDecorView());
				egistec.optical.demotool.igistec.Common.hideSystemUI(this);
			}
		}
	}

	public void onVerifyClick(View v){
		Button btn = (Button) findViewById(R.id.verify_btn);

		if (!mIsVerifying){
			if(sensorSize == AppSettingsPreference.SENSOR_SIZE.SENSOR_SIZE_LARGE) {
				//UIUtils.hideSystemUI(getWindow().getDecorView());
				egistec.optical.demotool.igistec.Common.hideSystemUI(this);
			}
			getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
			FPUtil.setBlockHomeKey(FPFingerActivity.this, true);
		} else{
			if(sensorSize == AppSettingsPreference.SENSOR_SIZE.SENSOR_SIZE_LARGE) {
				UIUtils.showSystemUI(getWindow().getDecorView());
			}
			getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
			FPUtil.setBlockHomeKey(FPFingerActivity.this, false);
		}

		if (!mIsVerifying){
			verifyCountSpinner.setEnabled(false);
			resetVerifyCounter();
			RefreshVerifyCounter();
			mHandler.postDelayed(new VerifyTask(), 0);
			mIsVerifying = !mIsVerifying;
			btn.setText(R.string.abort);
			setButtonStatus(false);
		} else{
			mFingerStatus.setImageDrawable(getResources().getDrawable(R.drawable.fp_icon_b));
			btn.setText(R.string.verify);
			verifyAnimationEnd();
			mLib.cancel();
			mIsVerifying = !mIsVerifying;
			verifyCountSpinner.setEnabled(true);
			setButtonStatus(true);
		}
		egistec.optical.demotool.igistec.Common.hideSystemUI(this);
	}

	public void onSetClick(View v){

		final PopupMenu mpopmenu = new PopupMenu(this,v);
		mpopmenu.getMenuInflater().inflate(R.menu.fp_set,mpopmenu.getMenu());
		mpopmenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
		    public boolean onMenuItemClick(MenuItem item) {
				switch (item.getItemId()) {
					case R.id.menu_calibrate:
						//mpick.setMinPic(0);
						//mpick.setMaxPic(2);
						mpick = new EnvPicker(FPFingerActivity.this,R.style.dialog,onClickListener , 2);
						mpick.setSetidId(2);
						mpick.show();
						break;
					case R.id.menu_find_dot_location:
						//mpick.setMinPic(0);
						//mpick.setMaxPic(2);
						mpick = new EnvPicker(FPFingerActivity.this,R.style.dialog,onClickListener , 2);
						mpick.setSetidId(3);
						mpick.show();
						break;
				}
				 return true;
			 }
		 });
		mpopmenu.show();
	}

	public void onCaliClick(View v){
		mLib.extraApi(CALI_BKG_IMG, ExperimentDef.CMD_BKG_IMG_RUN_CALI_PROC, null, null, null);
	}

	public void onTestCaseSettingsClick(View v) {
    	Log.d(TAG, "debug_check onTestCaseSettingsClick");
    	Button btnOpen = findViewById(R.id.test_case_settings_btn) ;
    	RelativeLayout testCaseLayout = findViewById(R.id.test_case_settings_layout);
		testCaseLayout.bringToFront();

    	if (testCaseLayout.getVisibility() == View.VISIBLE) {
			testCaseLayout.setVisibility(View.GONE);
			btnOpen.setVisibility(View.VISIBLE);
		}
    	else {
			//testCaseLayout.setVisibility(View.VISIBLE);
			mTestCaseSettings.makeShowAnimation() ;
			btnOpen.setVisibility(View.GONE);
		}
    	mTestCaseSettings.setTestCaseMode() ;
		egistec.optical.demotool.igistec.Common.hideSystemUI(this);
	}

	private void verifyAnimationEnd(){
        EgisLog.d(TAG, "verifyAnimationEnd");
		mLightDot.playStop();
		if (!mIsVerifyAnimate)
			return;
		Display display = getWindowManager().getDefaultDisplay();
		Point size = new Point();
		display.getSize(size);
	}

	private void triggerMessageAnimator(final View v, int eventId) {
		int animator_id;
		final int reverse_animator_id;
		bottomBarAnimationDuration = NON_MATCH_ANIMATION_DURATION;
		switch (eventId) {
			case FpResDef.EVENT_VERIFY_NOT_MATCHED:
				animator_id = R.animator.not_match_animator;
				reverse_animator_id = R.animator.not_match_reverse_animator;
				break;
			case FpResDef.EVENT_IMG_BAD_QLTY:
			case FpResDef.EVENT_IMG_PARTIAL:
			case FpResDef.EVENT_IMG_WATER:
				animator_id = R.animator.bad_image_animator;
				reverse_animator_id = R.animator.bad_image_reverse_animator;
				break;
			case FpResDef.EVENT_IMG_FAKE_FINGER:
				animator_id = R.animator.fake_finger_animator;
				reverse_animator_id = R.animator.fake_finger_reverse_animator;
				break;
			case FpResDef.EVENT_ERR_IDENTIFY:
				animator_id = R.animator.error_identify_animator;
				reverse_animator_id = R.animator.error_identify_reverse_animator;
				break;
			case FpResDef.EVENT_IMG_FAST:
				bottomBarAnimationDuration = 200;
				animator_id = R.animator.msg_too_fast;
				reverse_animator_id = R.animator.msg_too_fast_reverse;
				break;
			default:
				EgisLog.e(TAG, "[triggerMessageAnimator] unknown event id!!");
				return;
		}
		if (animatorSet != null) {
			animatorSet.cancel();
		}
		animatorSet = (AnimatorSet)AnimatorInflater.loadAnimator(v.getContext(), animator_id);

		animatorSet.addListener(new Animator.AnimatorListener() {
			@Override
			public void onAnimationStart(Animator animation) {
				mNonMatchAnimationStartTime = System.currentTimeMillis();
			}
			@Override
			public void onAnimationEnd(Animator animation) {
				EgisLog.d(TAG, "onAnimationEnd: duration = " + (System.currentTimeMillis() - mNonMatchAnimationStartTime));
				if ((System.currentTimeMillis() - mNonMatchAnimationStartTime) < bottomBarAnimationDuration * 2) {
					mHandler.postDelayed(new Runnable() {
						@Override
						public void run() {
							Animator reverse_animator = AnimatorInflater.loadAnimator(v.getContext(), reverse_animator_id);
							reverse_animator.setTarget(v);
							reverse_animator.start();
						}
					}, bottomBarAnimationDuration);
				}
			}
			@Override
			public void onAnimationCancel(Animator animation) {
			}
			@Override
			public void onAnimationRepeat(Animator animation) {
			}
		});
		animatorSet.setTarget(v);
		animatorSet.start();
	}

	private void notVefiryAnimation(int idRes, int eventId){
		mVerifyBtn.setText(idRes);
		triggerMessageAnimator(mBtnContainer, eventId);
		mHandler.postDelayed(new Runnable() {
			@Override
			public void run() {
				if (mIsVerifying){
					mVerifyBtn.setText(R.string.abort);
				}
			}
		}, TIMER_DELAY);
	}

	private void showFinishDialog(){

		String ui_alert ;
		if ( AuoOledFingerprintReceiverHelper.G_DEMO_INC_DEFINE_IN_MK) {
			ui_alert = "finger count=" + (mVerifySuccessCount+mVerifyFailCount) + "\n" ;
		}
		else {
			ui_alert = "success count=" + mVerifySuccessCount + "\n" + "fail count=" + mVerifyFailCount +
					"\n" + "fake finger count=" + mVerifyFakeFingerCount +
					"\n" + "quick match count=" + mVerifyQuickMatchCount;
		}

		AlertDialog.Builder dialog = new AlertDialog.Builder(this)
		   							.setIconAttribute(android.R.attr.alertDialogIcon)
		   							.setTitle("verify finish");
		dialog.setMessage(ui_alert);
		dialog.setCancelable(false);
		dialog.setPositiveButton(R.string.dialog_ok,
		  new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {
				mIsReachedLimit = false;
				mIsVerifying = false;
				verifyCountSpinner.setEnabled(true);
				resetVerifyCounter();
            	RefreshVerifyCounter();
				mLib.cancel();
				mVerifyBtn.setText(R.string.verify);
			}
		  }).create().show();
	}
	private int currentCount(){
		return mVerifySuccessCount + mVerifyFailCount;
	}
	private void checkVerifyFinish() {

//		if (currentCount() == Integer.valueOf(verifyCountList[verifyCountIndex])) {
//			if (mShowVerifyLimit && mShowVerifyCount) {
//				mIsReachedLimit = true;
//				// wait all counters are finished...
//				mHandler.postDelayed(new Runnable() {
//					@Override
//					public void run() {
//						showFinishDialog();
//					}
//				}, 300);
//			}
//		}

		// 修改為 可以自由選擇數量
		if (currentCount() == verifyCount ) {
			if (mShowVerifyLimit && mShowVerifyCount) {
				mIsReachedLimit = true;
				// wait all counters are finished...
				mHandler.postDelayed(new Runnable() {
					@Override
					public void run() {
						showFinishDialog();
					}
				}, 300);
			}
		}
	}
	private void doVerifyFail(){
		if (mIsVerifying){
			verifyAnimationEnd();
			notVefiryAnimation(R.string.not_match, FpResDef.EVENT_VERIFY_NOT_MATCHED);
			mVerifyFailCount++;
			checkVerifyFinish();
			RefreshVerifyCounter();
		}
	}
	private void doVerifySuccess(int value2){
		EgisLog.d(TAG, "doVerifySuccess");

		if (mIsVerifying){
			verifyAnimationEnd();
			boolean findCorrectmatchedID = false;
			Button btn = (Button) findViewById(R.id.verify_btn);
			btn.setText(R.string.abort);
			int matchedId = value2;
			mVerifySuccessCount++;
			checkVerifyFinish();
			for (FPFingerInfo f : mFpList){
				EgisLog.d(TAG, "f = " + f.getFingerId() + "matchedId = " + matchedId);
				if (matchedId == f.getFingerId()){
					f.setFingerMatched();
					findCorrectmatchedID = true;
					mMatchedFPIndex = mFpList.indexOf(f) - 1;
					break;
				}
			}
			if (findCorrectmatchedID) {
				mAdapter.notifyDataSetChanged();
				saveFingerListToCSV();
			}
			RefreshVerifyCounter();
		}
	}

	private void RenameFile(String fileSubPath, String fileName, String renameFileName) {
        String rename_bit[] = {"/image_bin/", "/image_raw/"};
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i< 2 ; i ++) {
            String rename_Path = builder.append(RootPath)
                    .append(rename_bit[i])
                    .append(fileSubPath)
                    .append("/").toString();

            builder.setLength(0);
            String rename_string = builder.append(rename_Path)
                    .append(fileName)
                    .append(".bin")
                    .toString();

            builder.setLength(0);
            String rename_md5_string = builder.append(rename_Path)
                    .append(renameFileName)
                    .append(".bin").toString();

            File rename_file = new File(rename_string);
            File rename_md5 = new File(rename_md5_string);
            while (!rename_file.exists()) {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            rename_file.renameTo(rename_md5);
        }
	}




	private void prepareSaveFile_google(RbsObjArray objArray, RbsObjImage objImage) {

		if (!mSaveImageBox.isChecked()) {
			return;
		}

		ConvertPath.setSaveInImageObj(true);
		boolean saveInImageObj = ConvertPath.isSaveInImageObj();

		StringBuilder builder = new StringBuilder();
		if (objImage.imgtype == RbsObjImage.IMGTYPE_BIN && objImage.index_try_match == 0) {
			mainFileSession.newMainFileSession(objImage.index_fingeron);
		}
		boolean isMainFile = true;

		File file;
		final String RootPath = DemoToolApp.getMyDataRoot();
		String FolderPath;
		String FileName;
		String SequenceID = String.format("%04d", objImage.index_fingeron);
		String tryMatchIndex = String.format("_TRY_%d", objImage.index_try_match);
		String test_case_id = ConvertPath.CaseName(FingerTestCaseidx);
		//String test_case_st = ConvertPath.CaseSt(FingerTestCaseidx);
		String test_case_st = ConvertPath.CaseSt(0);
		String match_case = ConvertPath.EnrollCase(objImage.is_bad, objImage.try_match_result, objImage.algo_flag);
		String md5 = "";
		String TimestampID = ConvertPath.getTimeStamp(objImage);
		String TimestampID_normal = ConvertPath.getTimeStampToYMDHMS(objImage);

		String bitPath = ConvertPath.ImageType(objImage.imgtype);
		String add_bds = String.format("%02d", objImage.bds_pool_add);


		// new field for google
		String ISP = "0" ;
		if (objImage.getBuffer().length >= 200*200*2) {
			ISP = "1"; // is raw image
		}
		String username = TestCaseSettings.FormatID(getStringWithEditText(mUserName));
		username = TestCaseSettings.FormatID(username) ;

		String file_name = mTestCaseSettings.getFileName(username, mFingerNumidx, ISP,String.format("%d", objImage.index_try_match));

		Log.d(TAG, "debug_check file name = " + file_name);

		FileName = builder
				.append(file_name)
				//.append("_").append(SequenceID)
				//.append(match_case)
				//.append(tryMatchIndex)
				//.append("_").append(TimestampID_normal)
				.toString();

		builder.setLength(0);
		FolderPath = builder.append(RootPath)
				.append(bitPath)
				.append("/" + mTestCaseSettings.getConditionString())
				//.append("/").append(test_case_id)
				.append("/").append(username)
				.append("/").append(mFingerNumidx)
				.append("/verify")
				.append("/").append(test_case_st)
				.append("/").toString();

		if (objImage.imgtype == RbsObjImage.IMGTYPE_BKG) {
			FolderPath = RootPath + bitPath;

			return ; // 強制不要存image_bkg
		}

		file = new File(FolderPath);

		if (!file.exists()) {
			file.mkdirs();
		}

		String rbsFileName = getRbsFileName(objArray, objImage);
		Log.d(TAG, "debug_check rbsFileName = " + rbsFileName);

		File myDrawFile;
		FileObj fileObj;
		myDrawFile = new File(file.getAbsolutePath() + "/" + rbsFileName + ".imo");
		fileObj = new FileObj(myDrawFile, objImage.getBuffer());

		if (objImage.imgtype == RbsObjImage.IMGTYPE_BKG) {
			mainFileSession.getSaveFileHelper().Save(fileObj);
		} else {
			mainFileSession.addFileObj(fileObj, isMainFile);
		}
	}

	private String getRbsFileName(RbsObjArray objArray, RbsObjImage objImage) {
		boolean saveInImageObj = ConvertPath.isSaveInImageObj();

		StringBuilder builder = new StringBuilder();
		boolean isResultMatch = objImage.try_match_result == RbsObjImage.VERIFY_TRY_MATCH_RESULT_MATCH;
		boolean isMainFile = true;
/*		if (isResultMatch) {
			isMainFile = true;
		} else {
			isMainFile = objImage.is_last_image == 1;
		}*/
		EgisLog.d(TAG, "isMainFile = " + isMainFile);

		File file;
		String fileName, renameFileName = "", filePath = "", fileSubPath = "";
		String userName = TestCaseSettings.FormatID(getStringWithEditText(mUserName)) ;
		String TimestampID = ConvertPath.getTimeStamp(objImage);
		String SequenceID = String.format("%04d", objImage.index_fingeron);
		String test_case_id = ConvertPath.CaseName(FingerTestCaseidx);
		String test_case_st = ConvertPath.CaseSt(FingerTestCaseidx);
		String match_case = ConvertPath.MatchCase(objImage.is_bad, isResultMatch);
		String tryMatchIndex = String.format("_TRY_%d", objImage.index_try_match);
		String HW_integrate_count = String.format("%02x", objImage.hw_integrate_count);
		String md5 = "";
		String bitPath = ConvertPath.ImageType(objImage.imgtype);

		builder.setLength(0);
		fileSubPath = builder.append("/").append(test_case_id)
				.append("/").append(userName)
				.append("/").append(mFingerNumidx)
				.append("/verify")
				.append("/").append(test_case_st)
				.append("/").toString();

		//fileSubPath = "/" + test_case_id + "/" + userName + "/" + mFingerNumidx + "/verify" + "/" + test_case_st + "/";

		String match_type;
		switch ((objImage.match_type)) {
			case RbsObjImage.VERIFY_TYPE_NORMAL_MATCH:
				match_type = "NM";
				break;
			case RbsObjImage.VERIFY_TYPE_QUICK_MATCH:
				match_type = "QM";
				break;
			case RbsObjImage.VERIFY_TYPE_LQM_MATCH:
				match_type = "LQM";
				break;
			default:
				match_type = "NA";
				break;
		}

		int indexOfVersionNumEnd = BuildConfig.VERSION_NAME.indexOf('_');
		if (indexOfVersionNumEnd == -1) {
			indexOfVersionNumEnd = BuildConfig.VERSION_NAME.indexOf('-');
		}
		//String versionName = "RBS-" + BuildConfig.VERSION_NAME.substring(0, indexOfVersionNumEnd);
		//fileName = versionName + "_" + userName + "_" + mFingerNumidx + "_" + TimestampID + "_" + SequenceID;
		String add_bds = String.format("%02d", objImage.bds_pool_add);
		String learning = String.format("%02d", objImage.is_learning);
		builder.setLength(0);
		if (saveInImageObj) {
			fileName = builder
					.append(BuildConfig.VERSION_NAME, 0, indexOfVersionNumEnd)
					.append("_").append(TimestampID)
					.append("_").append(userName)
					.append("_").append(mFingerNumidx)
					.append("_").append(SequenceID)
					.append(match_case)
					.append(tryMatchIndex).toString();
		} else {
			builder = builder.append("RBS-")
					.append(BuildConfig.VERSION_NAME, 0, indexOfVersionNumEnd)
					.append("_").append(TimestampID)
					.append("_").append(userName)
					.append("_").append(mFingerNumidx)
					.append("_").append(SequenceID)
					.append(match_case)
					.append(tryMatchIndex)
					.append("_").append(objImage.match_score);
			if (BuildConfig.ALGO_VER == 2) {
				builder = builder.append("_PQ_").append(objImage.g2_partial);
			}
			builder = builder.append("_FAKE_").append(objImage.fake_score)
					.append("_").append((float) objImage.exposure / 10).append("x")
					.append(HW_integrate_count)
					.append("_BTP_").append(objImage.temperature)
					.append("_BDS_").append(objImage.bds_debug_path)
					.append("_POL_").append(add_bds);
			switch (BuildConfig.ALGO_VER) {
				case 2:
					builder = builder.append("_").append(match_type);
					break;
				case 3:
					builder = builder.append("_eqty_").append(objImage.extract_qty);
					break;
			}
			fileName = builder.append("_LN_").append(learning)
					.append("_").append(objImage.sw_integrate_count)
					.append("_midx_").append(mMatchedFPIndex)
					.append("_DP_").append(objImage.partial).toString();

			if (BuildConfig.ALGO_VER == 2) {
				fileName = builder.append("_mqty_").append(objImage.qty).toString();
			}
		}

		Log.d(TAG, "debug_check rbsFileName = " + fileName);
		return fileName;
	}

	private void prepareSaveFile(RbsObjArray objArray, RbsObjImage objImage) {
    	Log.d(TAG, "debug_check mSaveImageBox.isChecked() = " + mSaveImageBox.isChecked());
		if (!mSaveImageBox.isChecked()) {
			return;
		}

		ConvertPath.setSaveInImageObj(true);
		boolean saveInImageObj = ConvertPath.isSaveInImageObj();

		StringBuilder builder = new StringBuilder();
		if (objImage.imgtype == RbsObjImage.IMGTYPE_BIN && objImage.index_try_match == 0) {
			mainFileSession.newMainFileSession(objImage.index_fingeron);
		}
		boolean isResultMatch = objImage.try_match_result == RbsObjImage.VERIFY_TRY_MATCH_RESULT_MATCH;
		boolean isMainFile = true;
/*		if (isResultMatch) {
			isMainFile = true;
		} else {
			isMainFile = objImage.is_last_image == 1;
		}*/
		EgisLog.d(TAG, "isMainFile = " + isMainFile);

		File file;
		String fileName, renameFileName = "", filePath = "", fileSubPath = "";
		String userName = getStringWithEditText(mUserName);
		String TimestampID = ConvertPath.getTimeStamp(objImage);
		String TimestampID_normal = ConvertPath.getTimeStampToYMDHMS(objImage);
		String SequenceID = String.format("%04d", objImage.index_fingeron);
		String test_case_id = ConvertPath.CaseName(FingerTestCaseidx);
        String test_case_st = ConvertPath.CaseSt(FingerTestCaseidx);
        String match_case = ConvertPath.MatchCase(objImage.is_bad, isResultMatch);
		String tryMatchIndex = String.format("_TRY_%d", objImage.index_try_match);
        String HW_integrate_count = String.format("%02x", objImage.hw_integrate_count);
        String md5 = "";
        String bitPath = ConvertPath.ImageType(objImage.imgtype);

        String userCondition = mTestCaseSettings.getUserCondition();
        Log.d(TAG, "debug_check user condition = " + userCondition);

		builder.setLength(0);
		fileSubPath = builder.append("/").append(test_case_id)
				.append("/").append(userName)
				.append("/").append(mFingerNumidx)
				.append("/verify")
				.append("/").append(test_case_st)
				.append("/").toString();

        //fileSubPath = "/" + test_case_id + "/" + userName + "/" + mFingerNumidx + "/verify" + "/" + test_case_st + "/";

        String match_type;
        switch ((objImage.match_type)) {
			case RbsObjImage.VERIFY_TYPE_NORMAL_MATCH:
				match_type = "NM";
				break;
			case RbsObjImage.VERIFY_TYPE_QUICK_MATCH:
				match_type = "QM";
				break;
			case RbsObjImage.VERIFY_TYPE_LQM_MATCH:
				match_type = "LQM";
				break;
			default:
				match_type = "NA";
				break;
		}

		int indexOfVersionNumEnd = BuildConfig.VERSION_NAME.indexOf('_');
		if (indexOfVersionNumEnd == -1) {
			indexOfVersionNumEnd = BuildConfig.VERSION_NAME.indexOf('-');
		}
		//String versionName = "RBS-" + BuildConfig.VERSION_NAME.substring(0, indexOfVersionNumEnd);
		//fileName = versionName + "_" + userName + "_" + mFingerNumidx + "_" + TimestampID + "_" + SequenceID;
		String add_bds = String.format("%02d", objImage.bds_pool_add);
        String learning = String.format("%02d", objImage.is_learning);
		builder.setLength(0);
		if (saveInImageObj) {
			fileName = builder.append("G").append(BuildConfig.ALGO_VER)
					.append("_").append(BuildConfig.VERSION_NAME, 0, indexOfVersionNumEnd)
					.append("_").append(TimestampID)
					.append("_").append(userName)
					.append("_").append(mFingerNumidx)
					.append("_").append(SequenceID)
					.append(match_case)
					.append(tryMatchIndex).toString();
		} else {
			builder = builder.append("RBS-")
					.append(BuildConfig.VERSION_NAME, 0, indexOfVersionNumEnd)
					.append("_").append(TimestampID)
					.append("_").append(userName)
					.append("_").append(mFingerNumidx)
					.append("_").append(SequenceID)
					.append(match_case)
					.append(tryMatchIndex)
					.append("_").append(objImage.match_score);
			if (BuildConfig.ALGO_VER == 2) {
				builder = builder.append("_PQ_").append(objImage.g2_partial);
			}
			builder = builder.append("_FAKE_").append(objImage.fake_score)
					.append("_").append((float) objImage.exposure / 10).append("x")
					.append(HW_integrate_count)
					.append("_BTP_").append(objImage.temperature)
					.append("_BDS_").append(objImage.bds_debug_path)
					.append("_POL_").append(add_bds);
			switch (BuildConfig.ALGO_VER) {
				case 2:
					builder = builder.append("_").append(match_type);
					break;
				case 3:
					builder = builder.append("_eqty_").append(objImage.extract_qty);
					break;
			}
			fileName = builder.append("_LN_").append(learning)
					.append("_").append(objImage.sw_integrate_count)
					.append("_midx_").append(mMatchedFPIndex)
					.append("_DP_").append(objImage.partial).toString();

			if (BuildConfig.ALGO_VER == 2) {
				fileName = builder.append("_mqty_").append(objImage.qty).toString();
			}
		}

        builder.setLength(0);
        filePath = builder.append(RootPath)
				.append(bitPath)
				.append(fileSubPath).toString();
    	//filePath = RootPath + bitPath + fileSubPath;
        EgisLog.d(TAG, "filePath= " + filePath);
        EgisLog.d(TAG, "filename= " + fileName);


		EgisLog.d(TAG, "filename= " + fileName);
		EgisLog.d(TAG, "fingerDegree= " + mFingerDegreeidx);

		file = new File(filePath);
		if (!file.exists()) {
			file.mkdirs();
		}

		File myDrawFile;
		FileObj fileObj;
		if (saveInImageObj) {
			myDrawFile = new File(file.getAbsolutePath() + "/" + fileName + ".imo");
			fileObj = new FileObj(myDrawFile, objImage.getBuffer());
		} else {
			myDrawFile = new File(file.getAbsolutePath() + "/" + fileName + ".bin");
			fileObj = new FileObj(myDrawFile, objImage.image);
		}

		if (objImage.imgtype == RbsObjImage.IMGTYPE_BKG) {
			mainFileSession.getSaveFileHelper().Save(fileObj);
		} else {
			mainFileSession.addFileObj(fileObj, isMainFile);
		}
		mVerifyBtn.setText(mVerifyBtn.getText());
	}

	private void SaveVerifyReport(int retryCount, int matchScore) {
		File file = null;
		String FileName = mUserName.getText().toString() + "_" + mFingerNumidx;
		String FilePath = IMAGE_ROOT_PATH;

		file = new File(FilePath);
		if (!file.exists()) {
			file.mkdirs();
		}

		String column = FileName + String.format("_%04d", mFingerOnIndex);
		String index = String.format("%02d", retryCount);
		String score = String.format("%d", matchScore);

		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append(column);
		stringBuilder.append(",");
		stringBuilder.append(index);
		stringBuilder.append(",");
		stringBuilder.append(score);
		stringBuilder.append("\n");

		try {
			File csvFile = new File(IMAGE_ROOT_PATH + VERIFY_REPORT_FILE_NAME);
			FileOutputStream fos = new FileOutputStream(csvFile, true);
			fos.write(stringBuilder.toString().getBytes());
			fos.flush();
			fos.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void deleteVerifyReport() {
		String FilePath = IMAGE_ROOT_PATH;

		File file = new File(FilePath);
		if (!file.exists()) {
			Toast.makeText(this, "Verify report not exist", Toast.LENGTH_SHORT).show();
			return;
		}

		File csvFile = new File(FilePath + VERIFY_REPORT_FILE_NAME);
		if (csvFile.exists()) {
			csvFile.delete();
			Toast.makeText(this, "Delete Success", Toast.LENGTH_SHORT).show();
		} else {
			Toast.makeText(this, "Verify report not exist", Toast.LENGTH_SHORT).show();
		}

		return;
	}

	private void showImageInfo(RbsObjImage objImage) {
		mBinding.extractQtyValue.setText(String.valueOf(objImage.extract_qty));
		mBinding.matchScoreValue.setText(String.valueOf(objImage.match_score));
//		mBinding.qtyValue.setText(String.valueOf(objImage.qty));
		if (BuildConfig.ALGO_VER == 2) {
			mBinding.qtyPartialValue.setText(String.valueOf(objImage.g2_partial));
		} else {
			mBinding.qtyPartialValue.setText(String.valueOf(objImage.partial));
		}
		mBinding.bdsPathValue.setText(String.valueOf(objImage.bds_debug_path));
		mBinding.polValue.setText(String.format("%02d", objImage.bds_pool_add));
		mBinding.isLearningValue.setText(String.format("%02d", objImage.is_learning));
		mBinding.tableFakeValue.setText(String.valueOf(objImage.fake_score));
		mBinding.blackEdgeValue.setText(String.valueOf(objImage.black_edge));
	}

	private boolean isOtgEvent(int eventId) {
		return (eventId >= FpResDef.EVENT_OTG_WRITE_REGISTER && eventId <= FpResDef.EVENT_OTG_RESERVED_END);
	}

	private FingerprintReceiver mFingerprintReceiver = new FingerprintReceiver(){

		@Override
		public void onFingerprintEvent(int eventId, int value1, int value2, Object eventObj) {
			EgisLog.d(TAG, "onFingerprintEvent eventId = " + eventId + " value1 = " + value1 + " value2 = " + value2);
			if(!mIsVerifying && !isOtgEvent(eventId)) {
				return;
			}

			switch(eventId){
				case FpResDef.EVENT_VERIFY_MATCHED:
					EgisLog.d(TAG, "onFingerprintEvent EVENT_VERIFY_MATCHED");
					setHasResult();
					mIsVerifySucess = true;
					doVerifySuccess(value2);
					break;

				case FpResDef.EVENT_VERIFY_NOT_MATCHED:
					EgisLog.d(TAG, "onFingerprintEvent EVENT_VERIFY_NOT_MATCHED");
					setHasResult();
					mIsVerifySucess = false;
					mMatchedFPIndex = -1;
					doVerifyFail();
					break;

				case FpResDef.EVENT_IDENTIFY_FINISH:
					EgisLog.d(TAG, "onFingerprintEvent EVENT_IDENTIFY_FINISH");
					mIsVerifySucess = false;
					break;

				case FpResDef.EVENT_ERR_IDENTIFY:
					verifyAnimationEnd();
					notVefiryAnimation(R.string.system_error, eventId);
					mVerifyErrorCount++;
					RefreshVerifyCounter();
					mMatchedFPIndex = -1;
//					if (mIsVerifying){
//						mHandler.postDelayed(new VerifyTask(), TIMER_DELAY);
//					}
					break;

				case FpResDef.EVENT_IMG_BAD_QLTY:
					EgisLog.w(TAG, "CAPTURE_BAD_IMAGE");
					verifyAnimationEnd();
					notVefiryAnimation(R.string.bad_image, eventId);
					mVerifyErrorCount++;
					RefreshVerifyCounter();
					mIsBadImage = true;
                    mIsGetImageReady = true;
                    mMatchedFPIndex = -1;
					break;

				case FpResDef.EVENT_IMG_FAKE_FINGER:
					EgisLog.w(TAG, "CAPTURE_FAKE_FINGER");
					verifyAnimationEnd();
					notVefiryAnimation(R.string.fake_finger, eventId);
					mVerifyErrorCount++;
					mVerifyFakeFingerCount++;
					RefreshVerifyCounter();
					mIsBadImage = true;
					mIsGetImageReady = true;
					mMatchedFPIndex = -1;
					break;
				case FpResDef.EVENT_IMG_PARTIAL:
                    EgisLog.w(TAG, "CAPTURE_PARTIAL_IMAGE");
                    verifyAnimationEnd();
                    notVefiryAnimation(R.string.touch_partial, eventId);
					mVerifyErrorCount++;
					RefreshVerifyCounter();
					mIsBadImage = true;
                    mIsGetImageReady = true;
                    mMatchedFPIndex = -1;
					break;

				case FpResDef.EVENT_IMG_FAST:
                    EgisLog.w(TAG, "CAPTURE_FAST_IMAGE");
                    notVefiryAnimation(R.string.touch_too_fast, eventId);
                    //verifyAnimationEnd();
                    //notVefiryAnimation(R.string.bad_image, eventId);
					//mIsBadImage = true;
                    //mIsGetImageReady = true;
                    //mIsFastCase = true;
					break;
				case FpResDef.EVENT_IMG_WATER:
                    EgisLog.w(TAG, "CAPTURE_WATER_IMAGE");
                    verifyAnimationEnd();
                    notVefiryAnimation(R.string.bad_image, eventId);
					mVerifyErrorCount++;
					RefreshVerifyCounter();
					mIsBadImage = true;
                    mIsGetImageReady = true;
                    mMatchedFPIndex = -1;
					break;

				case FpResDef.EVENT_FINGER_WAIT:
					EgisLog.d(TAG, "+++++ FingerActivity EVENT_FINGER_WAIT +++++");
					if (verifyAutoStarting) {
						EgisLog.d(TAG, "verifyAutoStarting");
						verifyAutoStarting = false;
						mLib.extraApi(PID_HOST_TOUCH, CMD_HOST_TOUCH_SET_TOUCHING, null, null, null);
					}
					mFingerImageCount = 0;
					break;

                case FpResDef.EVENT_FINGER_TOUCH :
                    mIsGetImageReady = false;
                    //verifyAnimationStart();
                    break;

                case FpResDef.EVENT_FINGER_READY:
					EgisLog.d(TAG, "+++++ FingerActivity EVENT_FINGER_READY +++++");
                    mFingerStatus.setImageDrawable(getResources().getDrawable(R.drawable.fp_icon_b));
                    mFingerStatus.postInvalidate();
                    mIsGetImageReady = true;
					mIsVerifySucess = false;
					mIsBadImage = false;
					mMatchedFPIndex = -1;
                    break;

				case FpResDef.EVENT_FINGER_LEAVE:
                    EgisLog.d(TAG, "+++++ FingerActivity EVENT_FINGER_LEAVE +++++");
                    if (mIsVerifying && mIsVerifySucess) {
                        mHandler.postDelayed(new VerifyTask(), 10);
                        if (mIsTouching && mIsFastCase) {
							mIsFastCase = false;
                            mLib.extraApi(PID_HOST_TOUCH, CMD_HOST_TOUCH_SET_TOUCHING, null, null, null);
                            //verifyAnimationStart();
                        }
                    }
					break;
				case FpResDef.CAPTURE_FINISH:
					EgisLog.d(TAG, "+++++ CAPTURE_FINISH +++++");
					break;

                case FpResDef.EVENT_RETURN_IMAGE_INFO:
                    if (eventObj != null) {
                        mFrameCount = value1;
                        mIsBadImage = value2 == 0;

                        ReturnImageInfo returnImageInfo = new ReturnImageInfo();
                        returnImageInfo.parseImageInfo(eventObj);
                        mTryMatchCount = returnImageInfo.getTryMatchCount();
                        mLastTriedIndex = returnImageInfo.getLastTriedIndex();
                        mMatchScore = returnImageInfo.getMatchScore();

                        if (returnImageInfo.isNewFingerOn()) {
                            mFingerOnIndex++;
                        }

                        if (!mIsBadImage) {
                            SaveVerifyReport(mTryMatchCount, mMatchScore);
                        }
                    } else {
                        EgisLog.e(TAG, "eventObj is null");
                    }
                    break;

				case FpResDef.EVENT_RETURN_IMAGE:
					EgisLog.d(TAG, "+++++ RETURN_IMAGE +++++");
					break;
				case FpResDef.EVENT_RETURN_IMAGEOBJ:
					EgisLog.d(TAG, "+++++ EVENT_RETURN_IMAGEOBJ +++++");
					setHasResult();
					if (eventObj != null) {
						RbsObjArray objArray = new RbsObjArray(eventObj);
						RbsObjImage objImage = new RbsObjImage(objArray.rbs_obj_img);
						if (objImage.imgtype == RbsObjImage.IMGTYPE_BIN) {
							if (mFingerImageCount == 1 && mVerifyFingerCount == 2) {
								mVerifyImgEnlarge2 = ImageProcess.showImage(mVerifyImage2, objImage.width, objImage.height, objImage.image);
							} else
								mVerifyImgEnlarge = ImageProcess.showImage(mVerifyImage, objImage.width, objImage.height, objImage.image);

							showImageInfo(objImage);
							updateQuickMatchCount(objImage.try_match_result, objImage.match_type);
							EgisLog.d(TAG, "+++++ mfinger image count = " + objImage.index_try_match + objImage.is_last_image + mFingerImageCount);
							if (objImage.is_last_image == 1) {
								mFingerImageCount++;
							}
						}
						prepareSaveFile(objArray, objImage);
					} else {
						EgisLog.e(TAG, "eventObj is null");
					}
					break;
				case FpResDef.EVENT_OTG_WRITE_REGISTER:
				case FpResDef.EVENT_OTG_READ_REGISTER:
				case FpResDef.EVENT_OTG_GET_FRAME:
				case FpResDef.EVENT_OTG_WAKE_UP:
				case FpResDef.EVENT_OTG_STANDBY:
				case FpResDef.EVENT_OTG_SPI_WRITE_READ:
				case FpResDef.EVENT_OTG_SET_SPI_CLK:
					if (mOtgSensorManager != null) {
						mOtgSensorManager.nativeCallbackHandler(eventId, value1, value2, (byte[])eventObj);
					}
					break;

				case FpResDef.EVENT_OTG_GET_SENSOR_ID:

					break;
				case FpResDef.EVENT_OTG_INIT_SENSOR:

					break;
				case FpResDef.EVENT_OTG_SET_SENSOR_PARAM:
					break;

				case FpResDef.EVENT_OTG_GET_IMAGE:
					break;

				case FpResDef.EVENT_OTG_ET760_RAW_SPI:
					AuoOledFingerprintReceiverHelper.rawSPI(eventId,value1,value2,eventObj);
					break;
				default:
					EgisLog.i(TAG, "ET760 GET UNKNOW ID=" + eventId + "," + value1 + "," + value2 );
					break;
			}
		}
	};

	private void updateQuickMatchCount(int try_match_result, int match_type) {
		if (try_match_result == RbsObjImage.VERIFY_TRY_MATCH_RESULT_MATCH &&
				match_type == RbsObjImage.VERIFY_TYPE_QUICK_MATCH) {
			mVerifyQuickMatchCount++;
		}
	}

	private int retryFresh = 0;

	private Runnable freshList = new Runnable(){
		@Override
		public void run(){
			int ret = 0;
			mFingers.clear();
			int[] fplist = new int[MAX_FINGERS];
			int[] fpcount = { 0 };

			ret = mLib.getFingerprintList(0,fplist, fpcount);
            if (ret != FpResDef.RESULT_OK) {
                EgisLog.e(TAG, "getFingerList failed. retry = " + retryFresh);
				if (retryFresh < 3) {
					retryFresh++;
					mHandler.postDelayed(freshList, 400);
					return;
				} else {
					Toast.makeText(FPFingerActivity.this, "getFingerList failed.", Toast.LENGTH_SHORT).show();
				}
            }

			for(int i = 0; i < fpcount[0]; i++) {
            	mFingers.add(fplist[i]);
			}

			EgisLog.d(TAG, "freshList(): mFingers = " + mFingers.toString());
			updateFingerList(mFingers);
		}
	};

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {

		EgisLog.d(TAG, "onActivityResult requestCode =" + requestCode + " resultCode = " + resultCode);
		EgisLog.e(TAG, "@@ onActivityResult requestCode =" + requestCode + " resultCode = " + resultCode);

		if(data != null) {
			switch (requestCode) {
				case REQ_CURRENT_TEMPLATE_USER_ID:
					if (resultCode == RESULT_OK) {
						String newUserId = data.getStringExtra(TemplateDbActivity.EXTRA_USER_ID);
						EgisLog.d(TAG, "TemplateDbActivity.EXTRA_USER_ID = " + newUserId);
						if ((newUserId != null) && (!newUserId.isEmpty())) {
							mFingerUserSettings.setUserId(newUserId);
							mUserName.setText(newUserId);
						}
					}



					if ( gTempActivityLastUserID != null ) {
						mUserName.setVisibility(View.GONE);
						Log.e(TAG, "On Activity Result when backup resulted, set username back to:" + gTempActivityLastUserID) ;
						mHandler.postDelayed(new Runnable() {
							@Override
							public void run() {
								mUserName.setVisibility(View.VISIBLE);
								mUserName.setText(gTempActivityLastUserID) ;
								Log.i(TAG, "Temp Fix for User ID Change Problem:" + mUserName.getText().toString()) ;
								gTempActivityLastUserID = null ;
							}
						}, 200) ;
					}
				break;
                case REQ_CODE_SETTINGS:
                    if (resultCode == RESULT_OK) {
                        String iniSettings = data.getStringExtra(SettingsActivity.EXTRA_INI_SETTINGS);
                        EgisLog.d(TAG, "iniSettings\n" + iniSettings);

                        //TODO: Fix sensor settings for isensor_open won't work
                        byte[] in_bytes = iniSettings.getBytes();
                        int ret = mLib.extraApi(PID_COMMAND, FpResDef.CMD_UPDATE_CONFIG, in_bytes, null, null);
                        if (ret == 0) {
                            mLib.uninitialize();
                            ret = mLib.initialize();
                        }
                        if (ret == 0) {
                            Toast.makeText(this, R.string.toast_update_ini_success, Toast.LENGTH_SHORT).show();
                            return;
                        } else {
                            EgisLog.e(TAG, "extraApi failed " + ret);
                            Toast.makeText(this, R.string.toast_update_ini_fail, Toast.LENGTH_SHORT).show();
                        }
                    }
		        default:
                    mFingerOnIndex = data.getIntExtra("index", -1);
                break;
            }
		}
	}

	public void saveFingerListToCSV() {
		StringBuilder builder = new StringBuilder();
		builder.setLength(0);
		String filePath = builder.append(RootPath)
				.append("FPList.csv").toString();
		FileWriter fileWriter = null;
		try {
			fileWriter = new FileWriter(filePath, false);
			int idx = 0;
			for (FPFingerInfo f : mFpList) {
				if (idx++ == 0)
					continue;
				fileWriter.append(f.getFingerName());
				fileWriter.append("\n");
			}
		} catch (Exception e) {
				e.printStackTrace();
		} finally {
			try {
				fileWriter.flush();
				fileWriter.close();
			} catch (IOException e) {
				EgisLog.e(TAG, "Error while flushing/closing fileWriter !!!");
				e.printStackTrace();
			}
		}
	}

	public int setFingerName(int fingerId, String fingerName){
		boolean ret;
		String key = FP_NAME_PREFIX + String.valueOf(fingerId);
		if (fingerName != null) {
			EgisLog.d(TAG, "++++++ setFingerName +++++ set " + fingerId + " fingerName =" + fingerName);
			ret = mSetting.edit().putString(key, fingerName).commit();
		} else {
			// to remove from shared preference
			EgisLog.d(TAG, "++++++ setFingerName +++++ remove finger " + key);
			ret = mSetting.edit().remove(key).commit();
		}
		if (!ret) {
			EgisLog.e(TAG, "++++++ setFingerName +++++ failed");
		}
		return (ret ? FpResDef.RESULT_OK : FpResDef.RESULT_FAILED);
	}
	

	
	public String getFingerName(int fingerId){
		EgisLog.d(TAG, "++++++ getFingerName +++++ fingerId = " + fingerId);
		return mSetting.getString(FP_NAME_PREFIX + String.valueOf(fingerId), null);
	}

	private String getStringWithEditText(EditText edText) {

		if (edText == null)
			return null;

		String sTemp = null;

		sTemp = edText.getText().toString();
		if (sTemp.isEmpty()) {
			sTemp = edText.getHint().toString();
		}
		return sTemp;
	}

	private void setButtonStatus(boolean enableFlag) {

		if (mSaveImageBox != null) {
			mSaveImageBox.setEnabled(enableFlag);

			if (enableFlag) {
				mSaveImageBox.setTextColor(Color.WHITE);
			} else {
				mSaveImageBox.setTextColor(Color.GRAY);
			}
		}

		if (mVerifyCountSwitch != null) {
			mVerifyCountSwitch.setEnabled(enableFlag);

			if(enableFlag) {
				mVerifyCountSwitch.setTextColor(Color.WHITE);
			} else {
				mVerifyCountSwitch.setTextColor(Color.GRAY);
			}
		}
	}

    public static String getFolderNameByEnrollMethod(int enrollMethod) {
        String folderName = "Unknown";

        switch (enrollMethod) {
            case FpResDef.ENROLL_METHOD_TOUCH:
                folderName = "";
                break;

            case FpResDef.ENROLL_METHOD_SWIPE:
                folderName = "SwipeV1/";
                break;

            case FpResDef.ENROLL_METHOD_PAINT:
                folderName = "SwipeV2/";
                break;
            default:
                EgisLog.e(TAG, "Unknown enroll mode, save image to unknown folder");
        }

        return folderName;
    }

	private void passwordDialogEvent() {
		final View item = this.getLayoutInflater().inflate(R.layout.pw_text, null);
		new android.support.v7.app.AlertDialog.Builder(FPFingerActivity.this)
				.setTitle("Enter password to proceed:")
				.setView(item)
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						EditText editText = (EditText) item.findViewById(R.id.edit_pw);
						String name = editText.getText().toString();
						String pw = mAppSettings.getEngineerModePassword();
						mSetEngineerMode = mAppSettings.encrypt(name).equals(pw);
						DemoToolApp.getInstance().setEngineerMode(mSetEngineerMode);
						invalidateOptionsMenu();
					}
				})
				.setNegativeButton("Cancel",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,int id) {
								dialog.cancel();
                                invalidateOptionsMenu();
                            }
						})
				.show();
	}

	private void displayVersionDialog() {
		new android.support.v7.app.AlertDialog.Builder(FPFingerActivity.this)
				.setTitle(DemoToolApp.getVersionName(this))
                .setPositiveButton("OK", null)
				.show();
	}

	private void updateRbsInitStatus(boolean inited){
		EgisLog.d(TAG, "updateRbsInitStatus("+inited+")");
		if(mVerifyStatus == null){
			return;
		}

		if(inited){
			mVerifyStatus.setText("");
			mVerifyStatus.setVisibility(mShowVerifyCount ? View.VISIBLE : View.GONE);
		}else{
			mVerifyStatus.setText("Failed to initialize (click to retry)");
			mVerifyStatus.setVisibility(View.VISIBLE);
			mVerifyStatus.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					v.setOnClickListener(null);
					mVerifyStatus.setText("retry initialization");
					mHandler.post(runnableRbsInitialization);
				}
			});
		}
		mVerifyStatus.setTextColor(Color.rgb(255,0,0));
	}

	private void setKeyboardVisibilityListener(final OnKeyboardVisibilityListener onKeyboardVisibilityListener) {
		final View parentView = ((ViewGroup) findViewById(android.R.id.content)).getChildAt(0);
		parentView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {

			private boolean alreadyOpen;
			private final int defaultKeyboardHeightDP = 100;
			private final int EstimatedKeyboardDP = defaultKeyboardHeightDP + (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP ? 48 : 0);
			private final Rect rect = new Rect();

			@Override
			public void onGlobalLayout() {
				int estimatedKeyboardHeight = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, EstimatedKeyboardDP, parentView.getResources().getDisplayMetrics());
				parentView.getWindowVisibleDisplayFrame(rect);
				int heightDiff = parentView.getRootView().getHeight() - (rect.bottom - rect.top);
				boolean isShown = heightDiff >= estimatedKeyboardHeight;

				if (isShown == alreadyOpen) {
					Log.i("Keyboard state", "Ignoring global layout change...");
					return;
				}
				alreadyOpen = isShown;
				onKeyboardVisibilityListener.onKBVisibilityChanged(isShown);
			}
		});
	}
}
