package egistec.optical.demotool;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * SharedPreferences for user settings in FPFingerActivity.java
 *   UserID (String)
 *   Finger Number (Finger Index)
 *   Angle (Degree Index)
 *   Save Image (boolean)
 *   Save Bad Image (boolean)
 *   Count (Finger Limit Index)
 */

public class FingerUserSettings {
    private Context mContext;
    private SharedPreferences mSharedPreferences;
    private static final String FILE_NAME = "FingerUserSettings";
    private String mDefaultUserId;
    private static final int DEFAULT_FINGER_NUMBER_INDEX = 0;
    private static final int DEFAULT_FINGER_TESTCASE_INDEX = 0;
    private static final int DEFAULT_FINGER_DEGREE_INDEX = 0;
    private static final int DEFAULT_FINGER_LIMIT_INDEX = 0;
    private static final boolean DEFAULT_IS_SAVE_IMAGE = false;
    private static final boolean DEFAULT_IS_SAVE_BAD_IMAGE = false;
    private static final int DEFAULT_VERIFY_FINGER_COUNT = 1;

    private static final String KEY_USER_ID = "user_id";
    private static final String KEY_FINGER_NUMBER_INDEX = "finger_number_index";
    private static final String KEY_FINGER_TESTCASE_INDEX = "finger_testcase_index";
    private static final String KEY_FINGER_DEGREE_INDEX = "finger_degree_index";
    private static final String KEY_FINGER_LIMIT_INDEX = "finger_limit_index";
    private static final String KEY_VERIFY_COUNT_CUSTOM = "verify_custom_index" ;
    private static final String KEY_IS_SAVE_IMAGE = "is_save_image";
    private static final String KEY_IS_SAVE_BAD_IMAGE = "is_save_bad_image";
    private static final String KEY_VERIFY_FINGER_COUNT = "verify_finger_count";

    public FingerUserSettings(Context context) {
        mContext = context;

        if (mContext != null) {
            mDefaultUserId = mContext.getResources().getString(R.string.default_user_id);
            mSharedPreferences = mContext.getSharedPreferences(FILE_NAME, Context.MODE_PRIVATE);
        }
    }

    public String getUserId() {
        return (mSharedPreferences == null) ? mDefaultUserId :
                mSharedPreferences.getString(KEY_USER_ID, mDefaultUserId);
    }

    public void setUserId(String userId) {
        if ((mSharedPreferences == null) || (userId == null)) {
            return;
        }

        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(KEY_USER_ID, userId);
        editor.apply();
    }

    public int getFingerNumberIndex() {
        return (mSharedPreferences == null) ? DEFAULT_FINGER_NUMBER_INDEX :
                mSharedPreferences.getInt(KEY_FINGER_NUMBER_INDEX, DEFAULT_FINGER_NUMBER_INDEX);
    }

    public void setFingerNumberIndex(int index) {
        if (mSharedPreferences == null) {
            return;
        }

        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putInt(KEY_FINGER_NUMBER_INDEX, index);
        editor.apply();
    }

    public int getFingerTestCaseIndex() {
        return (mSharedPreferences == null) ? DEFAULT_FINGER_TESTCASE_INDEX :
                mSharedPreferences.getInt(KEY_FINGER_TESTCASE_INDEX, DEFAULT_FINGER_TESTCASE_INDEX);
    }

    public int getFingerDegreeIndex() {
        return (mSharedPreferences == null) ? DEFAULT_FINGER_DEGREE_INDEX :
                mSharedPreferences.getInt(KEY_FINGER_DEGREE_INDEX, DEFAULT_FINGER_DEGREE_INDEX);
    }

    public void setFingerTestCaseIndex(int index) {
        if (mSharedPreferences == null) {
            return;
        }

        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putInt(KEY_FINGER_TESTCASE_INDEX, index);
        editor.apply();
    }

    public void setFingerDegreeIndex(int index) {
        if (mSharedPreferences == null) {
            return;
        }

        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putInt(KEY_FINGER_DEGREE_INDEX, index);
        editor.apply();
    }

    public int getFingerLimitIndex() {
        return (mSharedPreferences == null) ? DEFAULT_FINGER_LIMIT_INDEX :
                mSharedPreferences.getInt(KEY_FINGER_LIMIT_INDEX, DEFAULT_FINGER_LIMIT_INDEX);
    }


    // add for google ui
    public int getVerifyCountCustomValue() {
        return (mSharedPreferences == null) ? 5 :
                mSharedPreferences.getInt(KEY_VERIFY_COUNT_CUSTOM, 5);
    }


    public void setFingerLimitIndex(int index) {
        if (mSharedPreferences == null) {
            return;
        }

        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putInt(KEY_FINGER_LIMIT_INDEX, index);
        editor.apply();
    }

    public void setVerifyCountCustomValue(int count) {
        if (mSharedPreferences == null) {
            return;
        }

        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putInt(KEY_VERIFY_COUNT_CUSTOM, count);
        editor.apply();
    }

    public boolean getIsSaveImage() {
        return (mSharedPreferences == null) ? DEFAULT_IS_SAVE_IMAGE :
                mSharedPreferences.getBoolean(KEY_IS_SAVE_IMAGE, DEFAULT_IS_SAVE_IMAGE);
    }

    public void setIsSaveImage(boolean isSaveImage) {
        if (mSharedPreferences == null) {
            return;
        }

        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putBoolean(KEY_IS_SAVE_IMAGE, isSaveImage);
        editor.apply();
    }

    public boolean getIsSaveBadImage() {
        return (mSharedPreferences == null) ? DEFAULT_IS_SAVE_BAD_IMAGE :
                mSharedPreferences.getBoolean(KEY_IS_SAVE_BAD_IMAGE, DEFAULT_IS_SAVE_BAD_IMAGE);
    }

    public void setIsSaveBadImage(boolean isSaveBadImage) {
        if (mSharedPreferences == null) {
            return;
        }

        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putBoolean(KEY_IS_SAVE_BAD_IMAGE, isSaveBadImage);
        editor.apply();
    }

    public int getVerifyFingerCount() {
        return (mSharedPreferences == null) ? DEFAULT_VERIFY_FINGER_COUNT :
                mSharedPreferences.getInt(KEY_VERIFY_FINGER_COUNT, DEFAULT_VERIFY_FINGER_COUNT);
    }

    public void setVerifyFingerCount(int fingerCount) {
        if (mSharedPreferences == null) {
            return;
        }

        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putInt(KEY_VERIFY_FINGER_COUNT, fingerCount);
        editor.apply();
    }
}
