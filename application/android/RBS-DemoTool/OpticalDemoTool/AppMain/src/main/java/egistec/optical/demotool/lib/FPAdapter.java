package egistec.optical.demotool.lib;

import java.util.List;

import egistec.optical.demotool.R;

import android.animation.Animator;
import android.animation.AnimatorInflater;
import android.animation.AnimatorSet;
import android.content.Context;
import android.graphics.Color;
import android.os.Handler;
//import android.util.Log;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import rbs.egistec.com.fplib.api.EgisLog;

public class FPAdapter extends ArrayAdapter<FPFingerInfo> {

	private static final String TAG = "FingerprintAdapter";
	private static final int MATCH_ANIMATION_DURATION = 200;
	private List<FPFingerInfo> mList;
	private Context mContext;
	private ViewHolder viewHolder;
	private Handler mHandler;
	private long mMatchAnimationStartTime;

	public FPAdapter(Context context, List<FPFingerInfo> list, Handler handler){
		super(context, R.layout.fp_finger_cell, list);
		
		this.mList = list;
		this.mContext = context;
		this.mHandler = handler;
	}
	
	static class ViewHolder {
		protected TextView fingerName;
	}



	static public View noFinalView = null ;
	static public FPFingerInfo lvAnimateItem = null ;
	@Override
	public View getView (int position, View convertView, ViewGroup parent){		
		View v;
		FPFingerInfo listItem = mList.get( position );

		if (convertView == null) {
			LayoutInflater vi = (LayoutInflater) mContext
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			v = vi.inflate(R.layout.fp_finger_cell, parent, false);
			viewHolder = new ViewHolder();
			viewHolder.fingerName = (TextView)v.findViewById(R.id.finger_name);
			v.setTag( viewHolder );
		} else {
			v = convertView;			
		}

		if ( listItem.isFingerMatched() ) {
			lvAnimateItem = listItem ;
			//listItem.resetFingerMatched();
		}
		if (lvAnimateItem != null &&  listItem.getFingerId() == lvAnimateItem.getFingerId()){
			noFinalView = v ;
			mHandler.postDelayed(new Runnable() {
				@Override
				public void run() {
					if ( lvAnimateItem != null ) {
						lvAnimateItem.resetFingerMatched();
					}
					lvAnimateItem = null ;
				}
			}, (int)( MATCH_ANIMATION_DURATION * 2.5)) ;

			AnimatorSet set = (AnimatorSet)AnimatorInflater.loadAnimator(v.getContext(), R.animator.match_finger);
			set.setTarget(v);
			set.addListener(new Animator.AnimatorListener() {
				@Override
				public void onAnimationStart(Animator animation) {
					mMatchAnimationStartTime = System.currentTimeMillis();
				}
				@Override
				public void onAnimationEnd(Animator animation) {
					EgisLog.d(TAG, "onAnimationEnd: duration = " + (System.currentTimeMillis() - mMatchAnimationStartTime));
					if ((System.currentTimeMillis() - mMatchAnimationStartTime) < MATCH_ANIMATION_DURATION * 2) {
						mHandler.postDelayed(new Runnable() {
							@Override
							public void run() {
								Animator reverse_animator = AnimatorInflater.loadAnimator(noFinalView.getContext(), R.animator.match_finger_reverse);
								reverse_animator.setTarget(noFinalView);
								reverse_animator.start();

							}
						}, MATCH_ANIMATION_DURATION);
					}
				}
				@Override
				public void onAnimationCancel(Animator animation) {
				}
				@Override
				public void onAnimationRepeat(Animator animation) {
				}
			});
			set.start();
		}

		ViewHolder holder = (ViewHolder)v.getTag();
		holder.fingerName.setText(listItem.getFingerName());
		
		if (0 == position){
			holder.fingerName.setTextColor(Color.parseColor("#3786c8"));
		} else {
			holder.fingerName.setTextColor(Color.WHITE);
		}

		return v;
	}	
	
}
