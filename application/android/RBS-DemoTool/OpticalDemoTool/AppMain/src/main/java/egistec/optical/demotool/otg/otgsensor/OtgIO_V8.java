package egistec.optical.demotool.otg.otgsensor;

import android.hardware.usb.UsbDeviceConnection;
import android.hardware.usb.UsbEndpoint;
import android.util.Log;

public class OtgIO_V8 implements IOtgIO{
    private static final String TAG = "OTG-IOV8";
    UsbDeviceConnection conn;
    UsbEndpoint epIn;
    UsbEndpoint epOut;

    OtgIO_V8(UsbDeviceConnection conn, UsbEndpoint epin, UsbEndpoint epout){
        Log.d(TAG, "create OtgIO_V8");
        this.conn = conn;
        this.epIn = epin;
        this.epOut = epout;
    }

    @Override
    public int ReadRegister(short address, byte[] value) {
        if (epIn == null || epOut == null || conn == null) {
            return RESULT_FAIL;
        }

        SCSICommandBlock cmdBlock = new SCSICommandBlock(512, (byte) 0x80);
        byte[] commandBlock = new byte[16];
        commandBlock[0] = (byte) 0xE0;
        commandBlock[1] = (byte) ((address & 0xff00) >> 8);
        commandBlock[2] = (byte) (address & 0x00ff);
        cmdBlock.setCommandBlock(commandBlock);
        byte[] command = cmdBlock.getCBWBuffer();

        int response = conn.bulkTransfer(epOut, command, command.length, 500);

        byte status = 0;
        if (response == 31) {
            byte[] dataBuffer = new byte[512];
            response = conn.bulkTransfer(epIn, dataBuffer, dataBuffer.length, 500);
            value[0] = dataBuffer[0];
            if (response != 512) status = 1;
        } else {
            status = 1;
        }

        SCSICommandStatus cmdStatus = new SCSICommandStatus(response, status);
        byte[] commandStatus = cmdStatus.getCSWBuffer();

        response = conn.bulkTransfer(epIn, commandStatus, commandStatus.length, 500);

        // if (response == 13)
        return RESULT_OK;
        // else
        //    return -1;
    }

    @Override
    public int WriteRegister(short address, byte value) {
        if (epIn == null || epOut == null || conn == null) {
            return RESULT_FAIL;
        }

        SCSICommandBlock cmdBlock = new SCSICommandBlock(512, (byte) 0x00);
        byte[] commandBlock = new byte[16];
        commandBlock[0] = (byte) 0xE1;
        commandBlock[1] = (byte) 0x01;
        commandBlock[2] = (byte) ((address & 0xff00) >> 8);
        commandBlock[3] = (byte) (address & 0x00ff);
        commandBlock[4] = value;
        cmdBlock.setCommandBlock(commandBlock);
        byte[] command = cmdBlock.getCBWBuffer();
        // Log.d(TAG, "TM_WriteRegister bulkTransfer 1 start");
        int response = conn.bulkTransfer(epOut, command, command.length, 500);
        // Log.d(TAG, "TM_WriteRegister bulkTransfer 1 end");
        byte status = 0;
        if (response == 31) {
            byte[] dataBuffer = new byte[512];
            dataBuffer[0] = 0x01;
            dataBuffer[1] = (byte) ((address & 0xff00) >> 8);
            dataBuffer[2] = (byte) (address & 0x00ff);
            dataBuffer[3] = value;

            // Log.d(TAG, "TM_WriteRegister bulkTransfer 2 start");
            response = conn.bulkTransfer(epOut, dataBuffer, dataBuffer.length, 500);
            // Log.d(TAG, "TM_WriteRegister bulkTransfer 2 end");

            if (response != 512)
                status = 1;
        } else {
            status = 1;
        }

        SCSICommandStatus cmdStatus = new SCSICommandStatus(response, status);
        byte[] commandStatus = cmdStatus.getCSWBuffer();

        // Log.d(TAG, "TM_WriteRegister bulkTransfer 3 start");
        response = conn.bulkTransfer(epIn, commandStatus, commandStatus.length, 500);
        // Log.d(TAG, "TM_WriteRegister bulkTransfer 3 end");

        if (response == 13)
            return RESULT_OK;
        else
            return RESULT_FAIL;
    }

    @Override
    public int FetchImage(byte[] data, int width, int height) {
        if (epIn == null || epOut == null || conn == null) {
            return RESULT_FAIL;
        }
        if (StartCapture(width, height) == RESULT_FAIL) {
            Log.e(TAG, "StartCapture fail");
            return RESULT_FAIL;
        }

        int pageNum = 0;
        int pagesize = 16;
        int pagelen = pagesize * 1024;
        byte[] buffer = new byte[pagelen];
        int len = 0;
        int read_len;
        int ret;
        while (len < data.length) {
            ret = GetRawImage(buffer, pageNum, pagesize);
            if (ret < 0) {
                Log.e(TAG, "GetRawImage fail");
                return RESULT_FAIL;
            }

            if ((len + pagelen) > data.length) {
                read_len = data.length - len;
            } else {
                read_len = pagelen;
            }
            len += read_len;
            System.arraycopy(buffer, 0, data, pageNum * pagelen, read_len);
            pageNum++;
        }
        return RESULT_OK;
    }

    @Override
    public int Set_SPI_CLK(byte mode, byte clk) {
        return RESULT_FAIL;
    }

    @Override
    public int SPI_Write_Read(byte[] writeBuf, int writeLen, byte[] readBuf, int readLen) {
        return RESULT_FAIL;
    }

    @Override
    public int ReadRegister32(int a, byte[] value) {
        return 0;
    }

    @Override
    public int WriteRegister32(int a, int v) {
        return 0;
    }

    @Override
    public int Raw_SPI_Write(byte[] writeBuf, int len) {
        return 0;
    }

    @Override
    public int Raw_SPI_Write(byte[] writeBuf, int len, int cs) {
        return 0;
    }

    @Override
    public int BurstRead(int address, byte[] buffer, int len) {
        return 0;
    }

    @Override
    public int BurstWrite(int address, byte[] buffer, int len) {
        return 0;
    }

    private int StartCapture(int width, int height) {
        int image_width = width, image_height = height;
        SCSICommandBlock cmdBlock = new SCSICommandBlock(512, (byte) 0x00);
        byte[] commandBlock = new byte[16];
        commandBlock[0] = (byte) 0xE3;
        commandBlock[1] = (byte) 10; // bit num
        commandBlock[2] = (byte) ((image_width & 0xff00) >> 8); // full frame width
        commandBlock[3] = (byte) (image_width & 0x00ff);
        commandBlock[4] = (byte) ((image_height & 0xff00) >> 8); // full frame width
        commandBlock[5] = (byte) (image_height & 0x00ff);
        cmdBlock.setCommandBlock(commandBlock);
        byte[] command = cmdBlock.getCBWBuffer();
        int response = conn.bulkTransfer(epOut, command, command.length, 500);
        byte status = 0;
        if (response == 31) {
            byte[] dataBuffer = new byte[512];
            response = conn.bulkTransfer(epOut, dataBuffer, dataBuffer.length, 500);
            if (response != 512) status = 1;
        } else {
            return RESULT_FAIL;
        }

        SCSICommandStatus cmdStatus = new SCSICommandStatus(response, status);
        byte[] commandStatus = cmdStatus.getCSWBuffer();
        response = conn.bulkTransfer(epIn, commandStatus, commandStatus.length, 500*4);

        if (response == 13) {
            return RESULT_OK;
        } else {
            return RESULT_FAIL;
        }
    }

    private int GetRawImage(byte[] buffer, int pagenum, int pagesize) {
        SCSICommandBlock cmdBlock = new SCSICommandBlock(512, (byte) 0x80);
        byte[] commandBlock = new byte[16];
        commandBlock[0] = (byte) 0xE2;
        commandBlock[1] = (byte) pagenum; // page number
        commandBlock[2] = (byte) pagesize; // page size
        cmdBlock.setCommandBlock(commandBlock);
        byte[] command = cmdBlock.getCBWBuffer();
        int response = conn.bulkTransfer(epOut, command, command.length, 500);
        byte status = 0;
        if (response == 31) {
            response = conn.bulkTransfer(epIn, buffer, buffer.length, 500);
            if (response != pagesize * 1024) status = 1;
        } else {
            return RESULT_FAIL;
        }

        SCSICommandStatus cmdStatus = new SCSICommandStatus(response, status);
        byte[] commandStatus = cmdStatus.getCSWBuffer();
        response = conn.bulkTransfer(epIn, commandStatus, commandStatus.length, 500);

        if (response == 13) {
            return RESULT_OK;
        } else {
            return RESULT_FAIL;
        }
    }
}
