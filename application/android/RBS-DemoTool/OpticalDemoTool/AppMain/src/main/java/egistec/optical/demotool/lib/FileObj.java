package egistec.optical.demotool.lib;

import java.io.File;

public class FileObj {
    File saveFile;
    byte[] content = null;
    short[] content16Bit = null;

    FileObj() {};

    public FileObj(File file, byte [] content) {
        this.saveFile = file;
        this.content = content;
    }

    File getSaveFile() {
        return saveFile;
    }

    void setSaveFile(File saveFile) {
        this.saveFile = saveFile;
    }

    byte[] getContent() {
        return content;
    }

    short[] get16BitContent() {
        return content16Bit;
    }

    void setContent(byte[] content) {
        this.content = content;
    }

    void set16BitContent(short[] content16Bit) {
        this.content16Bit = content16Bit;
    }
}
