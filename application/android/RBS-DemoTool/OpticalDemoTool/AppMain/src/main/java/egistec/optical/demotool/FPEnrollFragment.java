package egistec.optical.demotool;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import android.annotation.TargetApi;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Point;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
//import android.util.Log;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.text.style.StyleSpan;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import egistec.optical.demotool.application.DemoToolApp;
import egistec.optical.demotool.databinding.FragmentFpEnrollBinding;
import egistec.optical.demotool.experiment.ExperimentUtils;
import egistec.optical.demotool.experiment.FileIoUtils;
//import egistec.optical.demotool.igistec.ET760_FingerprintReceiverHelper;
import egistec.optical.demotool.igistec.AuoOledFingerprintReceiverHelper;
import egistec.optical.demotool.igistec.Common;
import egistec.optical.demotool.lib.CircleShaderView;
import egistec.optical.demotool.lib.ConvertPath;
import egistec.optical.demotool.lib.FileObj;
import egistec.optical.demotool.lib.ImageProcess;
import egistec.optical.demotool.lib.LightDotControl;
import egistec.optical.demotool.lib.MainFileSession;
import egistec.optical.demotool.lib.RbsObjArray;
import egistec.optical.demotool.lib.RbsObjImage;
import egistec.optical.demotool.lib.RbsUtil;
import egistec.optical.demotool.lib.ReturnImageInfo;
import egistec.optical.demotool.lib.ImageEnlarge;
import egistec.optical.demotool.lib.UIUtils;
import egistec.optical.demotool.otg.manager.OtgSensorManager;
import pl.droidsonroids.gif.GifImageView;
import rbs.egistec.com.fplib.api.FingerprintReceiver;
import rbs.egistec.com.fplib.api.FpResDef;
import rbs.egistec.com.fplib.api.RbsLib;
import rbs.egistec.com.fplib.api.EgisLog;

import static egistec.optical.demotool.otg.OtgDef.OTG_SENSOR_ID;

public class FPEnrollFragment extends Fragment implements ET310Action
{
	private static final String TAG = "RBS-EnrollFragment";
    private static final String APK_VERSION = "2.0.6";
	private RbsLib mLib;
	private LightDotControl mLightDot;
	private TextView mEnrollStatus;
	private Button mEnrollOKView;
	private Button mTouchEvent;
	private GifImageView gifImageView;
	private Toast  mToast;
	private int mFingerNo;
	EditText username;
	private TextView mHintTitleView, mEnrollHint;
	private ImageView mEnrollImage;
	private Point mDisplaySize;
	private Handler mHandler = new Handler();
	private FileIoUtils.WindowPos icWindowPos = null;
	private FileIoUtils.SQWindowPos sqWindowPos = null;

	private static final String EXTRA_FINGER_ON_IDX = "EXTRA_FINGER_ON_IDX";
	private static final String BKG_IMAGE_PATH = "Bkg";
	private FragmentFpEnrollBinding mBinding;
	private ProgressBar mEnrollProgress;
	private TextView mEnrollProgressNumber;
	private int mEnrollTotalCount = 0;
	public int FingerNumidx;
	public int FingerTestCaseidx;
	boolean mCanSaveImage;
	boolean mCanSaveBadImage;
	private int mFingerOnIndex;
	public static final int ET320_SENSOR_WIDTH_FOR_SHOWIMAGE = 114; //ET320

	private String mUserName;
	private boolean mIsBadImage;
	private int enrollLQCount = 0;
	private int mFrameCount = 0;
	private int mEnrollOKCount = 0;
	static final int PID_HOST_TOUCH = 7;
	static final int CMD_HOST_TOUCH_ENABLE = 100;
	static final int CMD_HOST_TOUCH_SET_TOUCHING = 101;
	static final int CMD_HOST_TOUCH_RESET_TOUCHING = 102;
	static final int MAX_INTEGRATING_COUNT = 5;
	private boolean mIsStable;
	private boolean mIsTouching;
	private boolean mIsFastCase;
	private boolean mIsGetImageReady = true;
	private AppSettingsPreference.SENSOR_SIZE sensorSize = AppSettingsPreference.SENSOR_SIZE.SENSOR_SIZE_LARGE;
	private RelativeLayout mICWindowContainer;
	private RelativeLayout mSQWindowContainer;
	private LinearLayout mTopContainer;
	private boolean mSetDemoMode = false;
	private boolean mSetCalibrationMode = false;
	private static float pre_x = 0, pre_y = 0, pre_p, pre_s, diff_x, diff_y;
	private OtgSensorManager mOtgSensorManager = null;
	private boolean isEnrollComplete = false;
	private String[] timestamp = {"0", "0", "0"};
	private MainFileSession mainFileSession;
	private boolean optionNoCancelActionUp;
	private boolean hasActionUp;
	private boolean hasResult;
	private AppSettings mAppSettings;
	private CircleShaderView mGreenGradation;
	private Bitmap mEnrollImgEnlarge = null;
	private List<Point> mTouchPointList = null;

	private TestCaseSettings mTestCaseSettings;

	public Toast touchToast = null ;

	public static FPEnrollFragment newInstance(int finger_testcase_idx, String user_name, int finger_num_idx, boolean is_save, boolean optionNoCancelActionUp, int fingerOnIdx) {
		FPEnrollFragment newFragment = new FPEnrollFragment();
		Bundle bundle = new Bundle();
		bundle.putInt("FingerTestCaseidx", finger_testcase_idx);
		bundle.putString("UserName", user_name);
		bundle.putInt("FingerNumidx", finger_num_idx);
		bundle.putBoolean("CanSaveImage",is_save);
		bundle.putBoolean(FPFingerActivity.EXTRA_OPTION_NOCANCEL_ACTIONUP, optionNoCancelActionUp);
		bundle.putInt(EXTRA_FINGER_ON_IDX, fingerOnIdx);
		newFragment.setArguments(bundle);
		return newFragment;
	}

	public FPEnrollFragment(){
		FingerTestCaseidx = 0;
		mUserName = "";
		FingerNumidx = 0;
		mCanSaveImage = false;
		mCanSaveBadImage = false;
		mFingerOnIndex = 0;
	}
	
	public int getFingerOnIndex(){
		return mFingerOnIndex;
	}
	
	public void setEnrollId(int fingerNumber){
		mFingerNo = fingerNumber;
		EgisLog.d(TAG, "+++ setFingerNo +++" + mFingerNo);
	}

	private void updateEnrollTitle(int resId){
		mHintTitleView.setText(resId);
		if (resId == R.string.place_your_finger){
			mHintTitleView.setTextColor(Color.WHITE);
		} else if (resId == R.string.remove_your_finger){
			mHintTitleView.setTextColor(Color.GREEN);
		} else if (resId == R.string.the_finger_is_registered){
			mHintTitleView.setText(resId);
			mHintTitleView.setTextColor(Color.RED);
		} else {
			mHintTitleView.setTextColor(Color.YELLOW);
		}
		mHintTitleView.postInvalidate();
	}

	@Override
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		mAppSettings = new AppSettings(getActivity().getApplicationContext());
		sensorSize = AppSettingsPreference.getSensorSize(mAppSettings.getSensorSizeIndex());
		Bundle args = getArguments();
		if (args != null) {
			FingerTestCaseidx	= args.getInt("FingerTestCaseidx");
			mUserName 			= args.getString("UserName");
			FingerNumidx 			= args.getInt("FingerNumidx");
			mCanSaveImage 	= args.getBoolean("CanSaveImage");
			mCanSaveBadImage 	= mCanSaveImage;
			optionNoCancelActionUp = args.getBoolean(FPFingerActivity.EXTRA_OPTION_NOCANCEL_ACTIONUP, false);
			mFingerOnIndex		= args.getInt(EXTRA_FINGER_ON_IDX);
		}
		EgisLog.i(TAG, String.format("EnrollUser %s, FingerIdx %d, TestCase %d", mUserName, FingerNumidx, FingerTestCaseidx));
		mainFileSession = new MainFileSession(DemoToolApp.getMyDataRoot());
		Display display = getActivity().getWindowManager().getDefaultDisplay();
		mDisplaySize = new Point();
		display.getSize(mDisplaySize);

		mTestCaseSettings = TestCaseSettings.getInstance();
	}

	private void setHasResult() {
		if (hasResult) {
			EgisLog.d(TAG, "hasResult : Already done");
			return;
		}
		hasResult = true;
		if (hasActionUp) {
			setHostTouchReset(mAppSettings.getPressAreaTypeId());
		} else {
			EgisLog.d(TAG, "hasResult : Skip");
		}
	}

	private void setHostTouchSet() {
		EgisLog.i(TAG, "setHostTouchSet");
		hasActionUp = false;
		hasResult = false;
		mLib.extraApi(PID_HOST_TOUCH, CMD_HOST_TOUCH_SET_TOUCHING, null , null, null);
		mIsTouching = true;
		updateEnrollTitle(R.string.hold_your_finger);
	}

	private void setHostTouchReset(int pressAreaTypeId) {
		EgisLog.i(TAG, "setHostTouchReset");
		int resId = AppSettingsPreference.getPressAreaDrawableId(pressAreaTypeId);
		mIsStable = false;
		mIsTouching = false;
		mLib.extraApi(PID_HOST_TOUCH, CMD_HOST_TOUCH_RESET_TOUCHING, null, null, null);
		if (!mIsGetImageReady) {
			updateEnrollTitle(R.string.touch_too_fast);
		}
		updateEnrollTitle(R.string.place_your_finger);
		mLightDot.playStop();
		mLightDot.reset();
	}

	@TargetApi(Build.VERSION_CODES.M)
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
		View rootView;
		mTouchPointList = new ArrayList<>();
		mBinding = DataBindingUtil.inflate(
				inflater, R.layout.fragment_fp_enroll, container, false);
		rootView = mBinding.getRoot();
		mOtgSensorManager = OtgSensorManager.getInstance(OTG_SENSOR_ID, getActivity().getApplicationContext());
		mLib = RbsLib.getInstance(this.getContext());
		mEnrollStatus = (TextView) rootView.findViewById(R.id.enroll_status);
		mEnrollOKView = (Button) rootView.findViewById(R.id.tv_enroll_OK);
		mTouchEvent = (Button) rootView.findViewById(R.id.IC_window_enroll);
		mICWindowContainer = (RelativeLayout) rootView.findViewById(R.id.IC_window_enroll_container);
		mSQWindowContainer = (RelativeLayout) rootView.findViewById(R.id.SQ_Window_container);
		mTopContainer = (LinearLayout) rootView.findViewById(R.id.top_container);
		mHintTitleView = (TextView) rootView.findViewById(R.id.hint_title);
		mEnrollHint = (TextView) rootView.findViewById(R.id.hint_description);
		mEnrollImage = (ImageView) rootView.findViewById(R.id.enroll_map);
		mEnrollOKView.setOnClickListener(new View.OnClickListener(){
			@Override
			public void onClick(View v){
				Intent returnIntend = new Intent();
				returnIntend.putExtra("index", mFingerOnIndex);
				getActivity().setResult(0, returnIntend);
				getActivity().finish();
				return;

			}
		});

		EgisLog.d(TAG, "sensorSize = "+sensorSize);
		switch (sensorSize){
			case SENSOR_SIZE_LARGE:
				mICWindowContainer.setVisibility(View.GONE);
				mSQWindowContainer.setVisibility(View.VISIBLE);
				mTopContainer.setVisibility(View.GONE);
				break;
			case SENSOR_SIZE_NORMAL:
				mICWindowContainer.setVisibility(View.VISIBLE);
				mSQWindowContainer.setVisibility(View.GONE);
				mTopContainer.setVisibility(View.VISIBLE);
				break;
			default:
				break;
		}

		View.OnClickListener clickListener = new View.OnClickListener() {
			public void onClick(View v) {
				if (v.equals(mEnrollImage)) {
					ImageEnlarge.ImgEnlarge(getActivity(), mEnrollImgEnlarge);
				}
			}
		};
		mEnrollImage.setOnClickListener(clickListener);

		mEnrollProgress = (ProgressBar) rootView.findViewById(R.id.ProgressBar);
		mEnrollProgressNumber = (TextView) rootView.findViewById(R.id.ProgressNumber);
		gifImageView = (GifImageView)rootView.findViewById(R.id.enroll_gif);

		enrollLQCount = 0;
		//TouchEvent
		mTouchEvent.setOnTouchListener(new View.OnTouchListener() {
			@Override
			public boolean onTouch(View view, MotionEvent motionEvent) {
				if (isEnrollComplete) {
					return false;
				}
				if (motionEvent.getAction() == MotionEvent.ACTION_DOWN)
				{
					float x = motionEvent.getX();
					float y = motionEvent.getY();
					EgisLog.i("log", "action_down x = " + x + " y = " + y);
					mLightDot.playStart();
					if(mAppSettings.getCustomizePressAreaIcon() && mAppSettings.getAddViewTreeObserver()) {
						mLightDot.setOnGifLoadCB(new LightDotControl.OnGifLoadCB() {
							public void onGifLoad() {
								EgisLog.i("log", "onGifLoad ");
								setHostTouchSet();
								mLightDot.removeOnGifLoadCB();
							}
						});
					} else setHostTouchSet();
					return true;
				}
                else if (motionEvent.getAction() == MotionEvent.ACTION_MOVE)
                {
                    return true;
                }
				else if (motionEvent.getAction() == MotionEvent.ACTION_UP)
				{
					hasActionUp = true;
					if (hasResult) {
						setHostTouchReset(mAppSettings.getPressAreaTypeId());
					} else if (!optionNoCancelActionUp) {
						hasResult = true;
						setHostTouchReset(mAppSettings.getPressAreaTypeId());
					}
					return true;
				}

				return false;
			}
		});



		mBinding.SQWindow.setOnTouchListener(new View.OnTouchListener() {
			@Override
			public boolean onTouch(View view, MotionEvent motionEvent) {
				if (isEnrollComplete) {
					return false;
				}
				if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
					Common.hideSystemUI(getActivity());

					float x = motionEvent.getX();
					float y = motionEvent.getY();
					int x_ratio_x100 = (int) (x * 100 / mBinding.SQWindow.getWidth());
					int y_ratio_x100 = (int) (y * 100 / mBinding.SQWindow.getHeight());
					List<Point> listTouchPoint = new ArrayList<>();
					listTouchPoint.add(new Point(x_ratio_x100, y_ratio_x100));
					EgisLog.d(TAG, "ROI onTouch " + x + ", " + y + ", " + mBinding.SQWindow.getWidth() + ", " + mBinding.SQWindow.getHeight()
							+ ", " + x_ratio_x100 + ", " + y_ratio_x100);
					RbsUtil.setTouchPosToSDK(1, listTouchPoint);
					setHostTouchSet();
					return true;
				} else if (motionEvent.getAction() == MotionEvent.ACTION_MOVE) {
					return true;
				} else if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
					//RbsUtil.setTouchPosToSDK(0, 0);
					hasActionUp = true;
					if (hasResult) {
						setHostTouchReset(mAppSettings.getPressAreaTypeId());
					} else if (!optionNoCancelActionUp) {
						hasResult = true;
						setHostTouchReset(mAppSettings.getPressAreaTypeId());
					}
				}

				return false;
			}
		});

		//Load IC window layout
		if (!mSetDemoMode && !(APK_VERSION.equals("1.0.4"))) {
			icWindowPos = FileIoUtils.loadIcWindowLayoutData();
			ExperimentUtils.setPressAreaLeftTop(mICWindowContainer, icWindowPos.x, icWindowPos.y, getResources().getDisplayMetrics());
		}
		mGreenGradation = new CircleShaderView(getActivity().getApplicationContext());
		mGreenGradation.setCircleCanvas(mICWindowContainer.getLayoutParams().width, mICWindowContainer.getLayoutParams().height);
		float diameter = AppSettingsPreference.getPressAreaDiameter(mAppSettings.getPressAreaDiameterIndex());
		mGreenGradation.setRadius(diameter/2 + CircleShaderView.GRADATION_RADIUS);
		mICWindowContainer.addView(mGreenGradation);
		mLightDot = new LightDotControl(gifImageView,mGreenGradation, mAppSettings);

		//Set demo mode
		if (mSetDemoMode || mSetCalibrationMode || APK_VERSION.equals("1.0.4")) {
			mEnrollImage.setVisibility(View.INVISIBLE);
		}

		isEnrollComplete = false;
		return rootView;
	}

	@Override
	public void onDestroyView() {
		super.onDestroyView();
		mainFileSession.Destroy();
	}

	@Override
	public void onResume(){
		super.onResume();
		EgisLog.d(TAG,"onResume");

		//Load IC window layout
		if (!mSetDemoMode && !APK_VERSION.equals("1.0.4")) {
			if(sensorSize == AppSettingsPreference.SENSOR_SIZE.SENSOR_SIZE_LARGE) {
				UIUtils.hideSystemUI(getActivity().getWindow().getDecorView());
				sqWindowPos = FileIoUtils.loadSQWindowLayoutData();
				ExperimentUtils.setSQPressArea(mSQWindowContainer, sqWindowPos.x, sqWindowPos.y, sqWindowPos.width, sqWindowPos.height
						, getResources().getDisplayMetrics());
			} else {
				icWindowPos = FileIoUtils.loadIcWindowLayoutData();
				ExperimentUtils.setPressAreaLeftTop(mICWindowContainer, icWindowPos.x, icWindowPos.y, getResources().getDisplayMetrics());
			}
		}

		mEnrollImgEnlarge = null;
		mLib.startListening(mFingerprintReceiver);
		mHandler.postDelayed(enroll, 200);
	}

	@Override
	public void onPause(){
		super.onPause();
		mLib.cancel();


		try {
			getActivity().onBackPressed();
		}
		catch (Exception ex) {
			// temp skip error
			Log.e(TAG, "Skip java.lang.IllegalStateException: FragmentManager is already executing transactions") ;
			Log.e(TAG, "e=" + ex.getMessage()) ;
		}
		if(sensorSize == AppSettingsPreference.SENSOR_SIZE.SENSOR_SIZE_LARGE) {
			UIUtils.showSystemUI(getActivity().getWindow().getDecorView());
		}
//		mLib.stopListening(mFingerprintReceiver);
	}

	@Override
	public void onDestroy(){
		super.onDestroy();
	}

	private Runnable enroll = new Runnable(){
		@Override
		public void run(){
			captureEnroll(mFingerNo);
		}
	};
	private void captureEnroll(int fingerNumber){
		int i = 5;
		int ret = 0;
		while(i > 0) {
			EgisLog.d(TAG, String.format("+++ Enroll +++ %d, %d", PID_HOST_TOUCH, CMD_HOST_TOUCH_ENABLE));
			mLib.extraApi(PID_HOST_TOUCH, CMD_HOST_TOUCH_ENABLE, null, null, null);
			ret = mLib.enroll(0,fingerNumber);
			if (ret != 0) {
				try {
					Thread.sleep(30, 1);
				}catch (Exception e){
				}
				i--;
				EgisLog.d(TAG, "captureEnroll enroll ret : " + ret);
				continue;
			}else {
				break;
			}
		}
		if(i == 0 && ret != 0) {
			doEnrollFailed();
		}
	}
	@Override
	public void onKeyDown(){
		((FPEnrollActivity) getActivity()).doFinish();
	}

	private void doEnrollRemaining(int remaining){
		int progress = 0;
		if (mEnrollTotalCount == 0) {
			mEnrollTotalCount = remaining + 1;
		}
		if (mEnrollTotalCount != 0) {
			progress = (mEnrollTotalCount - remaining) * 100 / mEnrollTotalCount;
		}
		mEnrollProgress.setProgress(progress);
		mEnrollProgressNumber.setText("Remaining " + remaining);
		updateEnrollTitle(R.string.remove_your_finger);
	}

	private void doEnrollProgress(int progress){
		mEnrollProgress.setProgress(progress);
		mEnrollProgressNumber.setText(String.valueOf(progress) + " %");
		updateEnrollTitle(R.string.remove_your_finger);
		mLightDot.playStop();
		return;
	}

	private void doEnrollComplete() {
		EgisLog.d(TAG, "doEnrollComplete");
		mEnrollStatus.setText(R.string.enroll_success);
		mEnrollOKView.setVisibility(View.VISIBLE);
		mEnrollOKView.setEnabled(true);
		mLightDot.playStop();
		isEnrollComplete = true;
	}
	
	private void doEnrollFailed(){
		setHasResult();
		mEnrollStatus.setText(R.string.enrol_failed);

		// info by sparkle
		String s= "System Failed\nplease restart enroll process";
		SpannableString ss1=  new SpannableString(s);
		ss1.setSpan(new RelativeSizeSpan(1.1f), 0, 13, 0); // set size
		ss1.setSpan(new RelativeSizeSpan(0.7f), 14, s.length(), 0); // set size
		ss1.setSpan(new StyleSpan(android.graphics.Typeface.NORMAL), 14, s.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE); // 正常
		ss1.setSpan(new ForegroundColorSpan(Color.DKGRAY), 14, s.length(), 0);// set color
		mEnrollStatus.setGravity(Gravity.CENTER | Gravity.CENTER_HORIZONTAL | Gravity.CENTER_VERTICAL);
		mEnrollStatus.setText(ss1);

		mEnrollOKView.setVisibility(View.VISIBLE);
		mEnrollOKView.setEnabled(true);
	}

	private String getRbsFileName(RbsObjArray objArray, RbsObjImage objImage) {
		boolean saveInImageObj = ConvertPath.isSaveInImageObj();

		StringBuilder builder = new StringBuilder();
		boolean isMainFile = true;
		//isMainFile = objImage.try_match_result == 1;

		File file;
		final String RootPath = DemoToolApp.getMyDataRoot();
		String FolderPath;
		String FileName;
		String SequenceID = String.format("%04d", objImage.index_fingeron);
		String tryMatchIndex = String.format("_TRY_%d", objImage.index_try_match);
		String test_case_id = ConvertPath.CaseName(FingerTestCaseidx);
		String test_case_st = ConvertPath.CaseSt(FingerTestCaseidx);
		// EgisLog.d(TAG, String.format("index_fingeron %d, %d. %d", objImage.index_fingeron, objImage.index_try_match, objArray.index_start));
		String match_case = ConvertPath.EnrollCase(objImage.is_bad, objImage.try_match_result, objImage.algo_flag);
		String md5 = "";
		String TimestampID = ConvertPath.getTimeStamp(objImage);
		String bitPath = ConvertPath.ImageType(objImage.imgtype);
		String add_bds = String.format("%02d", objImage.bds_pool_add);


		int indexOfVersionNumEnd = BuildConfig.VERSION_NAME.indexOf('-');
		if (saveInImageObj) {
			FileName = builder
					.append(BuildConfig.VERSION_NAME, 0, indexOfVersionNumEnd)
					.append("_").append(TimestampID)
					.append("_").append(mUserName)
					.append("_").append(FingerNumidx)
					.append("_").append(SequenceID)
					.append(match_case)
					.append(tryMatchIndex).toString();
		} else {
			FileName = builder.append("RBS-")
					.append(BuildConfig.VERSION_NAME, 0, indexOfVersionNumEnd)
					.append("_").append(TimestampID)
					.append("_").append(mUserName)
					.append("_").append(FingerNumidx)
					.append("_").append(SequenceID)
					.append(match_case)
					.append(tryMatchIndex)
					.append("_BTP_").append(objImage.temperature)
					.append("_BDS_").append(objImage.bds_debug_path)
					.append("_POL_").append(add_bds)
					.append("_DP_").append(objImage.partial).toString();
		}

		Log.d(TAG, "debug_check rbsFileName = " + FileName);
		return FileName;
	}

	/**
	 * 原SAVE檔案功能
	 * @param objArray
	 * @param objImage
	 */
	private void prepareSaveFile(RbsObjArray objArray, RbsObjImage objImage) {
		Log.d(TAG, "debug_check mCanSaveImage = " + mCanSaveImage);
		if (!mCanSaveImage) {
			return;
		}

		ConvertPath.setSaveInImageObj(true);
		boolean saveInImageObj = ConvertPath.isSaveInImageObj();

		StringBuilder builder = new StringBuilder();
		if (objImage.imgtype == RbsObjImage.IMGTYPE_BIN && objImage.index_try_match == 0) {
			mainFileSession.newMainFileSession(objImage.index_fingeron);
		}
		boolean isMainFile = true;
		//isMainFile = objImage.try_match_result == 1;

		File file;
		final String RootPath = DemoToolApp.getMyDataRoot();
		String FolderPath;
		String FileName;
        String SequenceID = String.format("%04d", objImage.index_fingeron);
        String tryMatchIndex = String.format("_TRY_%d", objImage.index_try_match);
        String test_case_id = ConvertPath.CaseName(FingerTestCaseidx);
        String test_case_st = ConvertPath.CaseSt(FingerTestCaseidx);
        // EgisLog.d(TAG, String.format("index_fingeron %d, %d. %d", objImage.index_fingeron, objImage.index_try_match, objArray.index_start));
        String match_case = ConvertPath.EnrollCase(objImage.is_bad, objImage.try_match_result, objImage.algo_flag);
        String md5 = "";
        String TimestampID = ConvertPath.getTimeStamp(objImage);
		String TimestampID_normal = ConvertPath.getTimeStampToYMDHMS(objImage);

        String bitPath = ConvertPath.ImageType(objImage.imgtype);
        String add_bds = String.format("%02d", objImage.bds_pool_add);

		String file_name = mTestCaseSettings.getFileName(mUserName, FingerNumidx, "Temp");
		Log.d(TAG, "debug_check file name = " + file_name);

		int indexOfVersionNumEnd = BuildConfig.VERSION_NAME.indexOf('-');
		if (saveInImageObj) {
			FileName = builder.append("G").append(BuildConfig.ALGO_VER)
					.append("_").append(BuildConfig.VERSION_NAME, 0, indexOfVersionNumEnd)
					.append("_").append(TimestampID)
					.append("_").append(mUserName)
					.append("_").append(FingerNumidx)
					.append("_").append(SequenceID)
					.append("_c").append(String.format("%02d", mEnrollOKCount))
					.append(match_case)
					.append(tryMatchIndex).toString();
		} else {
			FileName = builder.append("RBS-")
					.append(BuildConfig.VERSION_NAME, 0, indexOfVersionNumEnd)
					.append("_").append(TimestampID)
					.append("_").append( TestCaseSettings.FormatID(mUserName))
					.append("_").append(FingerNumidx)
					.append("_").append(SequenceID)
					.append(match_case)
					.append(tryMatchIndex)
					.append("_BTP_").append(objImage.temperature)
					.append("_BDS_").append(objImage.bds_debug_path)
					.append("_POL_").append(add_bds)
					.append("_DP_").append(objImage.partial).toString();
		}
        builder.setLength(0);
        //FolderPath = RootPath + bitPath + "/" + test_case_id + "/" + mUserName + "/" + FingerNumidx + "/enroll" + "/st";


		// new = image_bin/normal/userID/0.1.2.5.6.7/ enroll &verify /st

		FolderPath = builder.append(RootPath)
				.append(bitPath)
				.append("/").append(test_case_id)
				.append("/").append(mUserName)
				.append("/").append(FingerNumidx)
				.append("/enroll")
				.append("/").append(test_case_st)
				.append("/").toString();


		file = new File(FolderPath);

		if (!file.exists()) {
			file.mkdirs();
		}

		EgisLog.d(TAG, "filePath= " + file.getAbsolutePath());
		EgisLog.d(TAG, "filename= " + FileName);

		File myDrawFile;
		FileObj fileObj;
		if (saveInImageObj) {
			myDrawFile = new File(file.getAbsolutePath() + "/" + FileName + ".imo");
			fileObj = new FileObj(myDrawFile, objImage.getBuffer());
		} else {
			myDrawFile = new File(file.getAbsolutePath() + "/" + FileName + ".bin");
			fileObj = new FileObj(myDrawFile, objImage.image);
		}

		if (objImage.imgtype == RbsObjImage.IMGTYPE_BKG) {
			mainFileSession.getSaveFileHelper().Save(fileObj);
		} else {
			mainFileSession.addFileObj(fileObj, isMainFile);
		}
	}


	private void prepareSaveFile_google(RbsObjArray objArray, RbsObjImage objImage) {
		if (!mCanSaveImage) {
			return;
		}

		ConvertPath.setSaveInImageObj(true);
		boolean saveInImageObj = ConvertPath.isSaveInImageObj();

		StringBuilder builder = new StringBuilder();
		if (objImage.imgtype == RbsObjImage.IMGTYPE_BIN && objImage.index_try_match == 0) {
			mainFileSession.newMainFileSession(objImage.index_fingeron);
		}
		boolean isMainFile = true;

		File file;
		final String RootPath = DemoToolApp.getMyDataRoot();
		String FolderPath;
		String FileName;
		String SequenceID = String.format("%04d", objImage.index_fingeron);
		String tryMatchIndex = String.format("_TRY_%d", objImage.index_try_match);
		String test_case_id = ConvertPath.CaseName(FingerTestCaseidx);
		//String test_case_st = ConvertPath.CaseSt(FingerTestCaseidx);
		String test_case_st = ConvertPath.CaseSt(0);
		String match_case = ConvertPath.EnrollCase(objImage.is_bad, objImage.try_match_result, objImage.algo_flag);
		String md5 = "";
		String TimestampID = ConvertPath.getTimeStamp(objImage);
		String TimestampID_normal = ConvertPath.getTimeStampToYMDHMS(objImage);

		String bitPath = ConvertPath.ImageType(objImage.imgtype);
		String add_bds = String.format("%02d", objImage.bds_pool_add);


		// new field for google
		String ISP = "0" ;
		if (objImage.getBuffer().length >= 200*200*2) {
			ISP = "1"; // is raw image
		}
		String file_name = mTestCaseSettings.getFileName(mUserName, FingerNumidx, ISP);

		Log.d(TAG, "debug_check file name = " + file_name);

		FileName = builder
				.append(file_name)
				//.append("_").append(SequenceID)
				//.append(match_case)
				//.append(tryMatchIndex)
				//.append("_").append(TimestampID_normal)
				.toString();

		builder.setLength(0);
		FolderPath = builder.append(RootPath)
				.append(bitPath)
				.append("/" + mTestCaseSettings.getConditionString())
				//.append("/").append(test_case_id)
				.append("/").append(mUserName)
				.append("/").append(FingerNumidx)
				.append("/enroll")
				.append("/").append(test_case_st)
				.append("/").toString();

		if (objImage.imgtype == RbsObjImage.IMGTYPE_BKG) {
			FolderPath = RootPath + bitPath;
			return ; // 強制不存IMAGE TYPE BKG
		}

		file = new File(FolderPath);

		if (!file.exists()) {
			file.mkdirs();
		}

		String rbsFileName = getRbsFileName(objArray, objImage);

		File myDrawFile;
		FileObj fileObj;
		myDrawFile = new File(file.getAbsolutePath() + "/" + rbsFileName + ".imo");
		fileObj = new FileObj(myDrawFile, objImage.getBuffer());

		if (objImage.imgtype == RbsObjImage.IMGTYPE_BKG) {
			mainFileSession.getSaveFileHelper().Save(fileObj);
		} else {
			mainFileSession.addFileObj(fileObj, isMainFile);
		}
	}

	private FingerprintReceiver mFingerprintReceiver = new FingerprintReceiver(){

		@Override
		public void onFingerprintEvent(int eventId, int value1, int value2, Object eventObj) {
			EgisLog.d(TAG,"mFingerprintReceiver event id = " + eventId);
			switch(eventId){
				case FpResDef.EVENT_ENROLL_OK:
					setHasResult();
					doEnrollProgress(value2);
					mEnrollOKCount++;
					break;

				case FpResDef.EVENT_ENROLL_LQ:
					enrollLQCount++;
					EgisLog.d(TAG, "enrollLQCount " + enrollLQCount);
					break;

				case FpResDef.EVENT_ENROLL_SUCCESS:
					setHasResult();
					doEnrollComplete();
					break;

				case FpResDef.EVENT_ENROLL_FAILED:
				case FpResDef.EVENT_ERR_ENROLL:
					doEnrollFailed();
					break;

				case FpResDef.ERROR_STATUS :
					EgisLog.e(TAG, "ERROR_STATUS v1="+value1+",v2="+value2);
					doEnrollFailed();
					break;
				case FpResDef.EVENT_FINGER_WAIT :
					//updateEnrollTitle(R.string.place_your_finger);
					mIsBadImage = false;
					break;

				case FpResDef.EVENT_FINGER_TOUCH :
					mIsGetImageReady = false;
					break;

				case FpResDef.EVENT_FINGER_LEAVE :
					if (mHintTitleView == null || getActivity() == null){
						return;
					}
					//updateEnrollTitle(R.string.place_your_finger);
					if (mIsTouching && mIsFastCase) {
						mIsFastCase = false;
						mLib.extraApi(PID_HOST_TOUCH, CMD_HOST_TOUCH_SET_TOUCHING, null, null, null);
					}
					break;

				case FpResDef.EVENT_IMG_FAST:
					//updateEnrollTitle(R.string.touch_too_fast);
					mIsBadImage = true;
                    mIsGetImageReady = true;
					mIsFastCase = true;
					break;

				case FpResDef.EVENT_IMG_PARTIAL:
					updateEnrollTitle(R.string.touch_partial);
					mLightDot.playStop();
					mLightDot.reset();
					mIsBadImage = true;
                    mIsGetImageReady = true;
					break;

				case FpResDef.EVENT_IMG_BAD_QLTY:
					updateEnrollTitle(R.string.bad_image);
					mIsBadImage = true;
                    mIsGetImageReady = true;
					break;

				case FpResDef.EVENT_IMG_WATER:
					mIsBadImage = true;
                    mIsGetImageReady = true;
					break;

				case FpResDef.EVENT_ENROLL_REDUNDANT:
					updateEnrollTitle(R.string.touch_similar_position);
					break;

				case FpResDef.EVENT_ENROLL_HIGHLY_SIMILAR:
					updateEnrollTitle(R.string.touch_similar_position);
					break;

				case FpResDef.EVENT_ENROLL_DUPLICATE_FINGER:
					updateEnrollTitle(R.string.the_finger_is_registered);
					/*try {
						Thread.sleep(1000);
						getActivity().onBackPressed();
					} catch (InterruptedException e) {
						e.printStackTrace();
					}*/
					break;

				case FpResDef.EVENT_FINGER_READY:
					EgisLog.d(TAG, "CAPTURE_FINISH");
                    mIsGetImageReady = true;
					break;

                case FpResDef.EVENT_RETURN_IMAGE_INFO:
                    mFrameCount = value1;
                    mIsBadImage = value2 == 0;

                    ReturnImageInfo returnImageInfo = new ReturnImageInfo();
                    returnImageInfo.parseImageInfo(eventObj);
                    if (returnImageInfo.isNewFingerOn()) {
                        mFingerOnIndex++;
                        EgisLog.d(TAG, String.format("+++++ EVENT_RETURN_IMAGE_INFO ++ (%d) count=%d. bad=%d", mFingerOnIndex, mFrameCount, mIsBadImage ? 1 : 0));
                    } else {
                        EgisLog.v(TAG, String.format("+++++ EVENT_RETURN_IMAGE_INFO ++ (%d) count=%d. bad=%d", mFingerOnIndex, mFrameCount, mIsBadImage ? 1 : 0));
                    }
                    break;

				case FpResDef.EVENT_RETURN_IMAGE:
					EgisLog.d(TAG, "+++++ RETURN_IMAGE +++++");
					break;
				case FpResDef.EVENT_RETURN_IMAGEOBJ:
					EgisLog.d(TAG, "+++++ EVENT_RETURN_IMAGEOBJ +++++");
					setHasResult();
					if (eventObj != null) {
						RbsObjArray objArray = new RbsObjArray(eventObj);
						RbsObjImage objImage = new RbsObjImage(objArray.rbs_obj_img);
                        if (objImage.imgtype == RbsObjImage.IMGTYPE_BIN) {
							mEnrollImgEnlarge = ImageProcess.showImage(mEnrollImage, objImage.width, objImage.height, objImage.image);
						}
                        prepareSaveFile(objArray, objImage);
					} else {
						EgisLog.e(TAG, "eventObj is null");
					}
					break;
				case FpResDef.EVENT_OTG_WRITE_REGISTER:
				case FpResDef.EVENT_OTG_READ_REGISTER:
				case FpResDef.EVENT_OTG_GET_FRAME:
				case FpResDef.EVENT_OTG_WAKE_UP:
				case FpResDef.EVENT_OTG_STANDBY:
				case FpResDef.EVENT_OTG_SPI_WRITE_READ:
				case FpResDef.EVENT_OTG_SET_SPI_CLK:
					if (mOtgSensorManager != null) {
						mOtgSensorManager.nativeCallbackHandler(eventId, value1, value2, (byte[])eventObj);
					}
					break;
				case FpResDef.EVENT_OTG_GET_SENSOR_ID:
					break;
				case FpResDef.EVENT_OTG_INIT_SENSOR:
					break;
				case FpResDef.EVENT_OTG_SET_SENSOR_PARAM:
					break;

				case FpResDef.EVENT_OTG_GET_IMAGE:
					break;

				case FpResDef.EVENT_OTG_ET760_RAW_SPI:
					AuoOledFingerprintReceiverHelper.rawSPI(eventId,value1,value2,eventObj);
					break;

				default:
					break;
			}
		}
	};

    private void setToast(Context context, String text, boolean ShowFlag) {
        if (mToast != null) {
            mToast.cancel();
            mToast=Toast.makeText(context,text,Toast.LENGTH_SHORT);
        } else {
            mToast=Toast.makeText(context,text,Toast.LENGTH_SHORT);
        }

        if (ShowFlag == true) {
			mToast.show();
		} else {
        	mToast.cancel();
		}
    }
}
