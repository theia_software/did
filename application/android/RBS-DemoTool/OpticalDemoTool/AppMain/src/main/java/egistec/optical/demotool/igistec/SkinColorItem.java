package egistec.optical.demotool.igistec;

import android.graphics.Color;

public class SkinColorItem {
    String title ;
    int color ;
    String subTitle ;
    int value ;

    public SkinColorItem(String _title, String _subTitle, int _color) {
        this.title = _title ;
        this.subTitle = _subTitle ;
        this.color = _color;
    }
}