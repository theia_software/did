/**
 * Copyright 2018 Egis Technology Inc.
 */
package egistec.optical.demotool.lib;

import android.content.Context;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Arrays;

import egistec.optical.demotool.experiment.ExperimentDef;
import egistec.optical.demotool.igistec.AuoOledFingerprintReceiverHelper;
import rbs.egistec.com.fplib.api.Common;
import rbs.egistec.com.fplib.api.RbsLib;

/**
 * Help get 8bit image + RAW 16bit image from RbsLib
 * Support to display them on two ImageView
 */
public class LiveImageViewer {
    final static String TAG = LiveImageViewer.class.getSimpleName();
    private ImageView rawImageView;
    private ImageView imageView;
    private TextView statisticsdataView;
    private RbsLib rbsLib;
    private static final int DEFAULT_LIVE_IMAGE_BUFFER_SIZE = 640 * 500 * 6 + LiveImageInfo.LIVEIMAGE_MAX_HEADER_LEN;
    private int[] g_img_length;

    public LiveImageViewer(Context context) {
        rbsLib = RbsLib.getInstance(context);
        int imageSize = getLiveImageSize();
        g_img_length = new int[]{imageSize};
    }

    public void setImageView(ImageView imageView, ImageView rawImageView, TextView statisticsdataView) {
        this.imageView = imageView;
        this.rawImageView = rawImageView;
        this.statisticsdataView = statisticsdataView;
    }



    private int getLiveImageSize(){
        byte[] img_info = new byte[ImageSizeInfo.DATA_SIZE_IMAGE_SIZE_INFO]; //width, height, bin_bpp, raw_bpp
        Arrays.fill(img_info, (byte) 0);
        int out_length[] = {ImageSizeInfo.DATA_SIZE_IMAGE_SIZE_INFO};
        rbsLib.extraApi(ExperimentDef.PID_FAETOOL, ExperimentDef.FAETOOL_GET_IMAGE_SIZE_INFO, null, img_info, out_length);
        ImageSizeInfo imageSizeInfo = new ImageSizeInfo(img_info);
        int sensor_w = imageSizeInfo.img_width;
        int sensor_h = imageSizeInfo.img_height;
        int bin_bpp = imageSizeInfo.img_bpp;
        int raw_bpp = imageSizeInfo.raw_bpp;
        int imageSize = DEFAULT_LIVE_IMAGE_BUFFER_SIZE;
        if(sensor_w > 0 && sensor_h > 0 && bin_bpp > 0 &&  raw_bpp > 0) {
            imageSize = sensor_w * sensor_h * (bin_bpp + raw_bpp) / 8 + LiveImageInfo.LIVEIMAGE_MAX_HEADER_LEN;
        }
        Log.d(TAG, "getLiveImageSize "+sensor_w+", "+sensor_h+", "+bin_bpp+", "+raw_bpp+", alloc size "+imageSize+", out_length = "+out_length[0]);
        return imageSize;
    }


    /**
     * add for Et760 update bds
     * 有手指在LIVE VIEW上=> update bds, 反之 沒有
     * @param useIPP
     * @param isUpdateBDS
     * @return
     */
    public LiveImageInfo runGetLiveImageInfo(boolean useIPP, boolean isUpdateBDS) {
        byte[] img = new byte[g_img_length[0]];

        if (useIPP) {
            byte [] in_buffer = new byte[4];
            Common.intToBytes(LiveImageInfo.LIVING_IMAGETYPE_STATISTICSDATA, in_buffer);

            // rule:
            // 如果是G_DEMO_INC 則 RAW/BIN都只顯示FFC後的，但BIN要存RAW (尚未完成)
            //
            // rule2:
            // 如果是QA的，則可以開關要顯示FFC還是RAW
            // ISP尚未更新 還不確定

            if ( AuoOledFingerprintReceiverHelper.G_DEMO_INC_DEFINE_IN_MK ) {
                rbsLib.extraApi(ExperimentDef.PID_FAETOOL, ExperimentDef.FAETOOL_GET_IMAGE, in_buffer, img, g_img_length);
            }
            else {
                rbsLib.extraApi(ExperimentDef.PID_FAETOOL, ExperimentDef.FAETOOL_GET_IMAGE, in_buffer, img, g_img_length);
            }

        } else {
            rbsLib.extraApi(ExperimentDef.PID_BKG_IMG, ExperimentDef.CMD_BKG_IMG_RETURN_IMAGE, null, img, g_img_length);
        }

        LiveImageInfo imageInfo = new LiveImageInfo(img);
        return imageInfo;
    }

    public LiveImageInfo runGetLiveImageInfo(boolean useIPP) {
        byte[] img = new byte[g_img_length[0]];
        if (useIPP) {
            byte [] in_buffer = new byte[4];
            Common.intToBytes(LiveImageInfo.LIVING_IMAGETYPE_STATISTICSDATA, in_buffer);
            // for not et760:
            // rbsLib.extraApi(ExperimentDef.PID_FAETOOL, ExperimentDef.FAETOOL_GET_IMAGE, in_buffer, img, g_img_length);
            // for et760 get ffc_image only
            rbsLib.extraApi(ExperimentDef.PID_FAETOOL, ExperimentDef.FAETOOL_GET_FFC_IMAGE, in_buffer, img, g_img_length);
        } else {

            rbsLib.extraApi(ExperimentDef.PID_BKG_IMG, ExperimentDef.CMD_BKG_IMG_RETURN_IMAGE, null, img, g_img_length);
        }

        LiveImageInfo imageInfo = new LiveImageInfo(img);
        return imageInfo;
    }

    public void updateImageView(LiveImageInfo imageInfo) {
        Log.i(TAG, "updateImageView ");
        ImageProcess.showImage(imageView, imageInfo.img_width, imageInfo.img_height, imageInfo.getImage());


        if (rawImageView != null) {
            if ( AuoOledFingerprintReceiverHelper.G_DEMO_INC_DEFINE_IN_MK ) {
                // 顯示給GOOGLE 故意只顯示DEONISE過後的，放在ISP欄位
                ImageProcess.showImage(rawImageView, imageInfo.img_width, imageInfo.img_height, imageInfo.getImage());
                showstatisticsdata(imageInfo);
            }
            else {
                byte[] img8 = ImageProcess.raw16To8bit(imageInfo.getRawImage(false));
                ImageProcess.showImage(rawImageView, imageInfo.img_width, imageInfo.img_height, img8);
                showstatisticsdata(imageInfo);
            }
        }
    }

    public void updateBDS() {
        Log.d(TAG, "update BDS");
        rbsLib.extraApi(ExperimentDef.PID_BKG_IMG, ExperimentDef.CMD_BKG_IMG_SAVE_BDS, null, null, null);
    }

    public void showstatisticsdata(LiveImageInfo imageInfo) {
        if ((statisticsdataView != null) && (imageInfo.live_image_type == LiveImageInfo.LIVING_IMAGETYPE_STATISTICSDATA)){
            statisticsdataView.setText("max:" + imageInfo.image_statistics_data.max + " min:" + imageInfo.image_statistics_data.min + " mean:" + imageInfo.image_statistics_data.mean);
        }
    }

}
