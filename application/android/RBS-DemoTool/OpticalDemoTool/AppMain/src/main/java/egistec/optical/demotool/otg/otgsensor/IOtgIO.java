package egistec.optical.demotool.otg.otgsensor;

public interface IOtgIO {
    int RESULT_OK = 1;
    int RESULT_FAIL = -1;

    int ReadRegister(short address, byte[] value);
    int WriteRegister(short address, byte value);
    int FetchImage(byte[] data, int width, int height);

    int Set_SPI_CLK(byte mode, byte clk);
    int SPI_Write_Read(byte[] writeBuf, int writeLen, byte[] readBuf, int readLen);


    // add by yougangkuo: temp modify for et760
    int ReadRegister32(int a, byte[] value);
    int WriteRegister32(int a, int v);
    int Raw_SPI_Write(byte [] writeBuf, int len) ; // 把PROJECT的SPI_WRITE拉出來
    int Raw_SPI_Write(byte [] writeBuf, int len, int cs) ; // 把PROJECT的SPI_WRITE拉出來

    int BurstRead(int address, byte[] buffer, int len) ;
    int BurstWrite(int address, byte[] buffer, int len ) ;
}
