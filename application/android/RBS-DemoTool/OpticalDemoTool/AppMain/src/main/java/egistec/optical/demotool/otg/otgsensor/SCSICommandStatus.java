package egistec.optical.demotool.otg.otgsensor;

import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.Random;

public class SCSICommandStatus
{
    public static final int CBS_SIGNATURE = 0x53425355;
    public static final int CBS_SIZE = 13;

    private int dCSWSignature;
    private int dCSWTag;
    private int dCSWDataResidue;
    private byte bCSWStatus;

    public SCSICommandStatus(int dCSWDataResidue, byte bCSWStatus) {
        this.dCSWSignature = CBS_SIGNATURE;
        this.dCSWTag = generateTag();
        this.dCSWDataResidue = dCSWDataResidue;
        this.bCSWStatus = bCSWStatus;
    }

    public static SCSICommandStatus getCSWStatus(byte[] buffer) {
        int signature = convertToInt(Arrays.copyOfRange(buffer, 0, 4));
        int tag = convertToInt(Arrays.copyOfRange(buffer, 4, 8));
        int residue = convertToInt(Arrays.copyOfRange(buffer, 8, 12));
        byte status = buffer[12];
        return new SCSICommandStatus(residue, status);
    }

    public int generateTag() {
        Random random = new Random();
        return random.nextInt();
    }

    private static int convertToInt(byte[] buffer) {
        int result;
        result = buffer[0] & 0xff;
        result = result + ((buffer[1] & 0xff) << 8);
        result = result + ((buffer[2] & 0xff) << 16);
        return result + ((buffer[3] & 0xff) << 24);
    }

    public byte[] getCSWBuffer() {
        ByteBuffer buffer = ByteBuffer.allocate(CBS_SIZE);
        buffer.putInt(dCSWSignature);
        buffer.putInt(dCSWTag);
        buffer.putInt(dCSWDataResidue);
        buffer.put(bCSWStatus);
        return buffer.array();
    }

    public int getdCSWSignature()
    {
        return dCSWSignature;
    }

    public void setdCSWSignature(int dCSWSignature)
    {
        this.dCSWSignature = dCSWSignature;
    }

    public int getdCSWTag()
    {
        return dCSWTag;
    }

    public void setdCSWTag(int dCSWTag)
    {
        this.dCSWTag = dCSWTag;
    }

    public int getdCSWDataResidue()
    {
        return dCSWDataResidue;
    }

    public void setdCSWDataResidue(int dCSWDataResidue)
    {
        this.dCSWDataResidue = dCSWDataResidue;
    }

    public byte getbCSWStatus()
    {
        return bCSWStatus;
    }

    public void setbCSWStatus(byte bCSWStatus)
    {
        this.bCSWStatus = bCSWStatus;
    }
}
