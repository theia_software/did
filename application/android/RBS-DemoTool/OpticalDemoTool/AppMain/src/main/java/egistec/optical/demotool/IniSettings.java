package egistec.optical.demotool;

//import android.util.Log;

import java.nio.charset.StandardCharsets;
import java.util.Arrays;

import egistec.optical.demotool.lib.IniHashMap;
import rbs.egistec.com.fplib.api.ExtraApiDef;
import rbs.egistec.com.fplib.api.FpResDef;
import rbs.egistec.com.fplib.api.RbsLib;
import rbs.egistec.com.fplib.api.EgisLog;

public class IniSettings {

    final static String TAG = IniSettings.class.getSimpleName();
    public final static int INI_CONFIG_TYPE = 0;
    public final static int INI_DB_TYPE = 1;

    public final static int INI_ITEM_ID_SENSOR = 0;
    public final static int INI_ITEM_ID_ENROLL = 1;
    public final static int INI_ITEM_ID_VERIFY = 2;
    public final static int INI_ITEM_ID_CALIBRATE = 3;

    private static String[][] ini_item_sensor_g2 = {
            {FpResDef.KEY_EXPOSURE_TIME, "10"},
            {FpResDef.KEY_INTEGRATE_COUNT_HW, "1"},
            {FpResDef.KEY_BKG_IMG_USE_OPTION, "0"},
            {FpResDef.KEY_BDS_LOW_TEMP_MIN_COUNT, "4"},
            {FpResDef.KEY_AVG_ZONE_STABLE_TRY, "0"},
            {FpResDef.KEY_BAC_THRESHOLD, "40"},
            {FpResDef.KEY_BAC_EXPOSURE_PERCENTAGE, "33"},
            {FpResDef.KEY_CIRCLE_CENTER_X, "0"},
            {FpResDef.KEY_CIRCLE_CENTER_Y, "0"},
            {FpResDef.KEY_TWO_TIMES_GAIN, "0"},
            {FpResDef.KEY_QUICK_CAPTURE_COUNT, "0"},
            {FpResDef.KEY_QUICK_CAPTURE_HW_INT, "0"},
            {FpResDef.KEY_QUICK_CAPTURE_EXP_TIME, "0"},
            {FpResDef.KEY_BAC_BLACK_IMG_AVG_THRESHOLD, "90"},
            {FpResDef.KEY_RAW_NORMALIZATION_ROI, "0"},
            {FpResDef.KEY_INPUT_SEQ_PROC_MODE, "0"},
            {FpResDef.KEY_BAD_PARTIAL_THRESHOLD, "30"},
            {FpResDef.KEY_IPP_FILTER_OVERFLOW_THRESHOLD_PERCENTAGE, "95"},
            {FpResDef.KEY_SENSOR_ORIENTATION, "0"},
            {FpResDef.KEY_ENABLE_CAPTURE_ROI, "1"},
            {FpResDef.KEY_CAPTURE_ROI_WIDTH, "200"},
            {FpResDef.KEY_CAPTURE_ROI_HEIGHT, "200"},
            {FpResDef.KEY_CAPTURE_ROI_START_POINT, "5"}
    };

    private static String[][] ini_item_sensor_g3plus = {
            {FpResDef.KEY_EXPOSURE_TIME, "10"},
            {FpResDef.KEY_INTEGRATE_COUNT_HW, "1"},
            {FpResDef.KEY_BKG_IMG_USE_OPTION, "0"},
            {FpResDef.KEY_BDS_LOW_TEMP_MIN_COUNT, "8"},
            {FpResDef.KEY_AVG_ZONE_STABLE_TRY, "0"},
            {FpResDef.KEY_BAC_THRESHOLD, "40"},
            {FpResDef.KEY_BAC_EXPOSURE_PERCENTAGE, "33"},
            {FpResDef.KEY_CIRCLE_CENTER_X, "0"},
            {FpResDef.KEY_CIRCLE_CENTER_Y, "0"},
            {FpResDef.KEY_TWO_TIMES_GAIN, "0"},
            {FpResDef.KEY_QUICK_CAPTURE_COUNT, "0"},
            {FpResDef.KEY_QUICK_CAPTURE_HW_INT, "0"},
            {FpResDef.KEY_QUICK_CAPTURE_EXP_TIME, "0"},
            {FpResDef.KEY_BAC_BLACK_IMG_AVG_THRESHOLD, "90"},
            {FpResDef.KEY_RAW_NORMALIZATION_ROI, "0"},
            {FpResDef.KEY_INPUT_SEQ_PROC_MODE, "0"},
            {FpResDef.KEY_BAD_PARTIAL_THRESHOLD, "30"},
            {FpResDef.KEY_IPP_FILTER_OVERFLOW_THRESHOLD_PERCENTAGE, "95"},
            {FpResDef.KEY_SENSOR_ORIENTATION, "0"},
            {FpResDef.KEY_ENABLE_CAPTURE_ROI, "1"},
            {FpResDef.KEY_CAPTURE_ROI_WIDTH, "200"},
            {FpResDef.KEY_CAPTURE_ROI_HEIGHT, "200"},
            {FpResDef.KEY_CAPTURE_ROI_START_POINT, "5"}
    };

    private static String[][] ini_item_sensor_g4 = {
            {FpResDef.KEY_EXPOSURE_TIME, "10"},
            {FpResDef.KEY_INTEGRATE_COUNT_HW, "1"},
            {FpResDef.KEY_BKG_IMG_USE_OPTION, "0"},
            {FpResDef.KEY_BDS_LOW_TEMP_MIN_COUNT, "8"},
            {FpResDef.KEY_AVG_ZONE_STABLE_TRY, "0"},
            {FpResDef.KEY_BAC_THRESHOLD, "40"},
            {FpResDef.KEY_BAC_EXPOSURE_PERCENTAGE, "33"},
            {FpResDef.KEY_CIRCLE_CENTER_X, "0"},
            {FpResDef.KEY_CIRCLE_CENTER_Y, "0"},
            {FpResDef.KEY_TWO_TIMES_GAIN, "0"},
            {FpResDef.KEY_QUICK_CAPTURE_COUNT, "0"},
            {FpResDef.KEY_QUICK_CAPTURE_HW_INT, "0"},
            {FpResDef.KEY_QUICK_CAPTURE_EXP_TIME, "0"},
            {FpResDef.KEY_BAC_BLACK_IMG_AVG_THRESHOLD, "90"},
            {FpResDef.KEY_RAW_NORMALIZATION_ROI, "0"},
            {FpResDef.KEY_INPUT_SEQ_PROC_MODE, "0"},
            {FpResDef.KEY_BAD_PARTIAL_THRESHOLD, "30"},
            {FpResDef.KEY_IPP_FILTER_OVERFLOW_THRESHOLD_PERCENTAGE, "95"},
            {FpResDef.KEY_SENSOR_ORIENTATION, "0"},
            {FpResDef.KEY_ENABLE_CAPTURE_ROI, "1"},
            {FpResDef.KEY_CAPTURE_ROI_WIDTH, "200"},
            {FpResDef.KEY_CAPTURE_ROI_HEIGHT, "200"},
            {FpResDef.KEY_CAPTURE_ROI_START_POINT, "5"}
    };

    private static String[][] ini_item_sensor_g5 = {
            {FpResDef.KEY_EXPOSURE_TIME, "200"},
            {FpResDef.KEY_ET760_ALL_ADC_DAC, "42"},
            {FpResDef.KEY_ET760_VGH_R, "16"},
            {FpResDef.KEY_ET760_VGL_R, "17"},
            {FpResDef.KEY_ET760_VGH_W, "8"},
            {FpResDef.KEY_ET760_VGL_W, "8"},
            {FpResDef.KEY_ET760_SVDD, "10"},
            {FpResDef.KEY_ET760_SAMPLE_MODE, "2"},
            {FpResDef.KEY_ET760_BINNING, "0"},
            {FpResDef.KEY_ET760_MATCHER_THRESHOLD_1, "100000"},
            {FpResDef.KEY_ET760_MATCHER_THRESHOLD_2, "100000"},
            {FpResDef.KEY_ET760_MATCHER_THRESHOLD_3, "100000"},
            {FpResDef.KEY_ET760_MATCHER_THRESHOLD_4, "100000"},
            {FpResDef.KEY_ET760_MATCHER_THRESHOLD_5, "100000"},
            {FpResDef.KEY_INTEGRATE_COUNT_HW, "1"},
            {FpResDef.KEY_BKG_IMG_USE_OPTION, "0"},
            {FpResDef.KEY_BDS_LOW_TEMP_MIN_COUNT, "8"},
            {FpResDef.KEY_AVG_ZONE_STABLE_TRY, "0"},
            {FpResDef.KEY_BAC_THRESHOLD, "40"},
            {FpResDef.KEY_BAC_EXPOSURE_PERCENTAGE, "33"},
            {FpResDef.KEY_CIRCLE_CENTER_X, "100"},
            {FpResDef.KEY_CIRCLE_CENTER_Y, "100"},
            {FpResDef.KEY_TWO_TIMES_GAIN, "0"},
            {FpResDef.KEY_QUICK_CAPTURE_COUNT, "0"},
            {FpResDef.KEY_QUICK_CAPTURE_HW_INT, "0"},
            {FpResDef.KEY_QUICK_CAPTURE_EXP_TIME, "0"},
            {FpResDef.KEY_BAC_BLACK_IMG_AVG_THRESHOLD, "90"},
            {FpResDef.KEY_RAW_NORMALIZATION_ROI, "0"},
            {FpResDef.KEY_INPUT_SEQ_PROC_MODE, "0"},
            {FpResDef.KEY_BAD_PARTIAL_THRESHOLD, "30"},
            {FpResDef.KEY_IPP_FILTER_OVERFLOW_THRESHOLD_PERCENTAGE, "95"},
            {FpResDef.KEY_SENSOR_ORIENTATION, "0"},
            {FpResDef.KEY_ENABLE_CAPTURE_ROI, "1"},
            {FpResDef.KEY_CAPTURE_ROI_WIDTH, "200"},
            {FpResDef.KEY_CAPTURE_ROI_HEIGHT, "200"},
            {FpResDef.KEY_CAPTURE_ROI_START_POINT, "5"}
    };

    private static String[][] ini_item_enroll_g2 = {
            {FpResDef.KEY_MAX_ENROLL_COUNT, "17"},
            {FpResDef.KEY_REDUNDANT_LEVEL, "7"},
            {FpResDef.KEY_CAPTURE_DELAY_ENABLE, "1"},
            {FpResDef.KEY_CAPTURE_DELAY_START_PROGRESS, "80"},
            {FpResDef.KEY_CAPTURE_DELAY_WAITING_TIME, "450"},
            {FpResDef.KEY_ENROLL_QUALITY_THRESHOLD, "20"},
            {FpResDef.KEY_ENROLL_QUALITY_THRESHOLD_DEC, "5"},
            {FpResDef.KEY_ENROLL_EXTRA_THE_FIRST_ENABLE, "0"},
            {FpResDef.KEY_ENROLL_ENABLE_UK, "0"},
            {FpResDef.KEY_ENROLL_EXTRA_THE_FIRST_BEFORE_PROGRESS, "40"},
            {FpResDef.KEY_ENROLL_PARTIAL_THRESHOLD, "80"},
    };

    private static String[][] ini_item_enroll_g3plus = {
            {FpResDef.KEY_MAX_ENROLL_COUNT, "15"},
            {FpResDef.KEY_REDUNDANT_LEVEL, "10"},
            {FpResDef.KEY_CAPTURE_DELAY_ENABLE, "1"},
            {FpResDef.KEY_CAPTURE_DELAY_START_PROGRESS, "80"},
            {FpResDef.KEY_CAPTURE_DELAY_WAITING_TIME, "200"},
            {FpResDef.KEY_ENROLL_QUALITY_THRESHOLD, "40"},
            {FpResDef.KEY_ENROLL_QUALITY_THRESHOLD_DEC, "5"},
            {FpResDef.KEY_ENROLL_EXTRA_THE_FIRST_ENABLE, "1"},
            {FpResDef.KEY_ENROLL_ENABLE_UK, "0"},
            {FpResDef.KEY_ENROLL_EXTRA_THE_FIRST_BEFORE_PROGRESS, "90"},
            {FpResDef.KEY_ENROLL_PARTIAL_THRESHOLD, "80"},
    };

    private static String[][] ini_item_enroll_g4 = {
            {FpResDef.KEY_MAX_ENROLL_COUNT, "15"},
            {FpResDef.KEY_REDUNDANT_LEVEL, "10"},
            {FpResDef.KEY_CAPTURE_DELAY_ENABLE, "1"},
            {FpResDef.KEY_CAPTURE_DELAY_START_PROGRESS, "80"},
            {FpResDef.KEY_CAPTURE_DELAY_WAITING_TIME, "200"},
            {FpResDef.KEY_ENROLL_QUALITY_THRESHOLD, "40"},
            {FpResDef.KEY_ENROLL_QUALITY_THRESHOLD_DEC, "5"},
            {FpResDef.KEY_ENROLL_EXTRA_THE_FIRST_ENABLE, "1"},
            {FpResDef.KEY_ENROLL_ENABLE_UK, "0"},
            {FpResDef.KEY_ENROLL_EXTRA_THE_FIRST_BEFORE_PROGRESS, "90"},
            {FpResDef.KEY_ENROLL_PARTIAL_THRESHOLD, "80"},
    };

    private static String[][] ini_item_enroll_g5 = {
            {FpResDef.KEY_MAX_ENROLL_COUNT, "6"},
            {FpResDef.KEY_REDUNDANT_LEVEL, "70"},
            {FpResDef.KEY_CAPTURE_DELAY_ENABLE, "1"},
            {FpResDef.KEY_CAPTURE_DELAY_START_PROGRESS, "80"},
            {FpResDef.KEY_CAPTURE_DELAY_WAITING_TIME, "200"},
            {FpResDef.KEY_ENROLL_QUALITY_THRESHOLD, "10"},
            {FpResDef.KEY_ENROLL_QUALITY_THRESHOLD_DEC, "5"},
            {FpResDef.KEY_ENROLL_EXTRA_THE_FIRST_ENABLE, "1"},
            {FpResDef.KEY_ENROLL_ENABLE_UK, "0"},
            {FpResDef.KEY_ENROLL_EXTRA_THE_FIRST_BEFORE_PROGRESS, "90"},
            {FpResDef.KEY_ENROLL_PARTIAL_THRESHOLD, "80"},
    };

    private static String[][] ini_item_verify_g2 = {
            {FpResDef.KEY_FLOW_TRY_MATCH, "2"},
            {FpResDef.KEY_VERIFY_FINGER_COUNT, "1"},
            {FpResDef.KEY_VERIFY_PARTIAL_THRESHOLD, "70"},
            {FpResDef.KEY_EXT_FEAT_QUALITY_TRYMATCH_THRESHOLD, "500"},
            {FpResDef.KEY_EXT_FEAT_QUALITY_BADIMG_THRESHOLD, "0"},
            {FpResDef.KEY_IMAGE_QTY_THRESHOLD, "-1"},
            {FpResDef.KEY_FAKE_FINGER_DETECTION, "0"},
            {FpResDef.KEY_DETECT_SCRATCH_MASK, "0"},
            {FpResDef.KEY_LIGHT_DETECTION, "0"},
            {FpResDef.KEY_CONFIG_MATCH_MODE2, "0"},
            {FpResDef.KEY_PIPELINE_CAPTURE, "0"},
            {FpResDef.KEY_EXT_FEAT_QUALITY_LQMATCH_THRESHOLD, "20"},
            {FpResDef.KEY_VERIFY_IMAGE_DPI, "500"},
            {FpResDef.KEY_TRY_MATCH_EVEN_TOO_PARTIAL, "0"}
    };

    private static String[][] ini_item_verify_g3plus = {
            {FpResDef.KEY_FLOW_TRY_MATCH, "4"},
            {FpResDef.KEY_VERIFY_FINGER_COUNT, "1"},
            {FpResDef.KEY_VERIFY_PARTIAL_THRESHOLD, "70"},
            {FpResDef.KEY_EXT_FEAT_QUALITY_TRYMATCH_THRESHOLD, "100"},
            {FpResDef.KEY_EXT_FEAT_QUALITY_BADIMG_THRESHOLD, "10"},
            {FpResDef.KEY_IMAGE_QTY_THRESHOLD, "-1"},
            {FpResDef.KEY_FAKE_FINGER_DETECTION, "0"},
            {FpResDef.KEY_DETECT_SCRATCH_MASK, "0"},
            {FpResDef.KEY_LIGHT_DETECTION, "0"},
            {FpResDef.KEY_CONFIG_MATCH_MODE2, "1"},
            {FpResDef.KEY_PIPELINE_CAPTURE, "0"},
            {FpResDef.KEY_EXT_FEAT_QUALITY_LQMATCH_THRESHOLD, "20"},
            {FpResDef.KEY_VERIFY_IMAGE_DPI, "500"},
            {FpResDef.KEY_TRY_MATCH_EVEN_TOO_PARTIAL, "0"}
    };

    private static String[][] ini_item_verify_g4 = {
            {FpResDef.KEY_FLOW_TRY_MATCH, "4"},
            {FpResDef.KEY_VERIFY_FINGER_COUNT, "1"},
            {FpResDef.KEY_VERIFY_PARTIAL_THRESHOLD, "70"},
            {FpResDef.KEY_EXT_FEAT_QUALITY_TRYMATCH_THRESHOLD, "100"},
            {FpResDef.KEY_EXT_FEAT_QUALITY_BADIMG_THRESHOLD, "10"},
            {FpResDef.KEY_IMAGE_QTY_THRESHOLD, "-1"},
            {FpResDef.KEY_FAKE_FINGER_DETECTION, "0"},
            {FpResDef.KEY_DETECT_SCRATCH_MASK, "0"},
            {FpResDef.KEY_LIGHT_DETECTION, "0"},
            {FpResDef.KEY_CONFIG_MATCH_MODE2, "1"},
            {FpResDef.KEY_PIPELINE_CAPTURE, "0"},
            {FpResDef.KEY_EXT_FEAT_QUALITY_LQMATCH_THRESHOLD, "20"},
            {FpResDef.KEY_VERIFY_IMAGE_DPI, "500"},
            {FpResDef.KEY_TRY_MATCH_EVEN_TOO_PARTIAL, "0"}
    };

    private static String[][] ini_item_verify_g5 = {
            {FpResDef.KEY_FLOW_TRY_MATCH, "0"},
            {FpResDef.KEY_VERIFY_FINGER_COUNT, "1"},
            {FpResDef.KEY_VERIFY_PARTIAL_THRESHOLD, "70"},
            {FpResDef.KEY_EXT_FEAT_QUALITY_TRYMATCH_THRESHOLD, "100"},
            {FpResDef.KEY_EXT_FEAT_QUALITY_BADIMG_THRESHOLD, "10"},
            {FpResDef.KEY_IMAGE_QTY_THRESHOLD, "-1"},
            {FpResDef.KEY_FAKE_FINGER_DETECTION, "0"},
            {FpResDef.KEY_DETECT_SCRATCH_MASK, "0"},
            {FpResDef.KEY_LIGHT_DETECTION, "0"},
            {FpResDef.KEY_CONFIG_MATCH_MODE2, "1"},
            {FpResDef.KEY_PIPELINE_CAPTURE, "0"},
            {FpResDef.KEY_EXT_FEAT_QUALITY_LQMATCH_THRESHOLD, "20"},
            {FpResDef.KEY_VERIFY_IMAGE_DPI, "500"},
            {FpResDef.KEY_TRY_MATCH_EVEN_TOO_PARTIAL, "1"}
    };

    private static String[][] ini_item_calibrate_g2 = {
            {FpResDef.KEY_CALIBRATE_625AVG_TARGET_PERCENTAGE, "55"},
            {FpResDef.KEY_CALIBRATE_TARGET_CAPTURE_TIME, "150"},
            {FpResDef.KEY_CALIBRATE_EXPOSURE_START_WK, "10"},
            {FpResDef.KEY_CALIBRATE_EXPOSURE_END_WK, "60"}
    };

    private static String[][] ini_item_calibrate_g3plus = {
            {FpResDef.KEY_CALIBRATE_625AVG_TARGET_PERCENTAGE, "55"},
            {FpResDef.KEY_CALIBRATE_TARGET_CAPTURE_TIME, "150"},
            {FpResDef.KEY_CALIBRATE_EXPOSURE_START_WK, "10"},
            {FpResDef.KEY_CALIBRATE_EXPOSURE_END_WK, "60"}
    };

    private static String[][] ini_item_calibrate_g4 = {
            {FpResDef.KEY_CALIBRATE_625AVG_TARGET_PERCENTAGE, "55"},
            {FpResDef.KEY_CALIBRATE_TARGET_CAPTURE_TIME, "150"},
            {FpResDef.KEY_CALIBRATE_EXPOSURE_START_WK, "10"},
            {FpResDef.KEY_CALIBRATE_EXPOSURE_END_WK, "60"}
    };

    private static String[][] ini_item_calibrate_g5 = {
            {FpResDef.KEY_CALIBRATE_625AVG_TARGET_PERCENTAGE, "75"},
            {FpResDef.KEY_CALIBRATE_TARGET_CAPTURE_TIME, "150"},
            {FpResDef.KEY_CALIBRATE_EXPOSURE_START_WK, "10"},
            {FpResDef.KEY_CALIBRATE_EXPOSURE_END_WK, "60"}
    };

    public static String[][] getIniItem(int itemId) {
        String[][] result = null;
        switch (itemId) {
            case INI_ITEM_ID_SENSOR:
                result = getSensorInit();
                break;
            case INI_ITEM_ID_ENROLL:
                result = getEnrollInit();
                break;
            case INI_ITEM_ID_VERIFY:
                result = getVerifyInit();
                break;
            case INI_ITEM_ID_CALIBRATE:
                result = getCalibrationInit();
                break;
            default:
                EgisLog.e(TAG, "Invalid ini item id!");
                break;
        }
        return result;
    }

    private static String[][] getSensorInit(){
        switch (BuildConfig.ALGO_VER){
            case 2:
                return ini_item_sensor_g2;
            case 3:
                return ini_item_sensor_g3plus;
            case 4:
                return ini_item_sensor_g4;
            case 5:
            default:
                return ini_item_sensor_g5;
        }
    }

    private static String[][] getEnrollInit(){
        switch (BuildConfig.ALGO_VER){
            case 2:
                return ini_item_enroll_g2;
            case 3:
                return ini_item_enroll_g3plus;
            case 4:
                return ini_item_enroll_g4;
            case 5:
            default:
                return ini_item_enroll_g5;
        }
    }

    private static String[][] getVerifyInit(){
        switch (BuildConfig.ALGO_VER){
            case 2:
                return ini_item_verify_g2;
            case 3:
                return ini_item_verify_g3plus;
            case 4:
                return ini_item_verify_g4;
            case 5:
            default:
                return ini_item_verify_g5;
        }
    }

    private static String[][] getCalibrationInit(){
        switch (BuildConfig.ALGO_VER){
            case 2:
                return ini_item_calibrate_g2;
            case 3:
                return ini_item_calibrate_g3plus;
            case 4:
                return ini_item_calibrate_g4;
            case 5:
            default:
                return ini_item_calibrate_g5;
        }
    }

    public static int updateIniSettings(RbsLib rbsLib, int ini_type, String sectionName, String[][] updatedKeyValueTable) {
        if (rbsLib == null || updatedKeyValueTable == null) {
            EgisLog.e(TAG, "[updateIniSettings] invalid parameter!!!");
            return FpResDef.RESULT_FAILED;
        }
        byte[] ini_buf = new byte[FpResDef.INI_CONFING_FILE_MAX_SIZE];
        int[] ini_buf_size = new int[1];
        int retval = FpResDef.RESULT_OK;
        if (ini_type == INI_CONFIG_TYPE)
            retval = rbsLib.extraApi(ExtraApiDef.PID_COMMAND, FpResDef.CMD_GET_CONFIG, null, ini_buf, ini_buf_size);
        if (retval != FpResDef.RESULT_OK) {
            EgisLog.e(TAG, "Get ini config fail!!!");
            return retval;
        }
        byte[] ini_data = Arrays.copyOf(ini_buf, ini_buf_size[0]);
        String ini_str = new String(ini_data, StandardCharsets.UTF_8);
        // Build ini hash map from ini string
        IniHashMap<String, IniHashMap<String, String>> iniHashMap = new IniHashMap<String, IniHashMap<String, String>>();
        String[] pairs = ini_str.split("\n");
        String currentSection = "";
        for (int i = 0; i < pairs.length; i++) {
            if (pairs[i].contains("[")) {
                currentSection = pairs[i];
                iniHashMap.put(currentSection, new IniHashMap<String, String>());
            } else if (pairs[i].contains("=")) {
                String[] keyValue = pairs[i].split("=");
                IniHashMap<String, String> iniSectionHashMap = (IniHashMap<String, String>)iniHashMap.get(currentSection);
                if (iniSectionHashMap != null) {
                    iniSectionHashMap.put(keyValue[0].trim(), keyValue[1].trim());
                    iniHashMap.put(currentSection, iniSectionHashMap);
                }
            } else {
                EgisLog.d(TAG, "invalid data = " + pairs[i]);
            }
        }
        // Update key and value to ini hash map
        String sectionKey = "[" + sectionName + "]";
        for (String[] item : updatedKeyValueTable) {
            IniHashMap<String, String> iniSectionHashMap = (IniHashMap<String, String>)iniHashMap.get(sectionKey);
            if (iniSectionHashMap == null) {
                EgisLog.d(TAG, sectionKey + " does not exist! create new section.");
                iniSectionHashMap = new IniHashMap<String, String>();
                iniHashMap.put(sectionKey, iniSectionHashMap);
            }
            iniSectionHashMap.put(item[0], item[1]);
        }
        // Convert ini hash map to string
        String resultIniString = iniHashMap.toString();
        EgisLog.d(TAG, "resultIniString = \n" + resultIniString);
        byte[] ret_ini_data = resultIniString.getBytes();
        EgisLog.d(TAG, "original ini size = " + ini_buf_size[0] + ", new ini size = " + ret_ini_data.length);
        if (ini_type == INI_CONFIG_TYPE)
            retval = rbsLib.extraApi(ExtraApiDef.PID_COMMAND, FpResDef.CMD_UPDATE_CONFIG, ret_ini_data, null, null);
        else if (ini_type == INI_DB_TYPE)
            retval = rbsLib.extraApi(ExtraApiDef.PID_COMMAND, FpResDef.CMD_UPDATE_DB_CONFIG, ret_ini_data, null, null);
        else {
            EgisLog.d(TAG, "unknow ini_type = " + ini_type);
            retval = FpResDef.RESULT_FAILED;
        }
        EgisLog.d(TAG, "updateIniSettings retval = " + retval);
        return retval;
    }

    public static String GetIniSettingSValue(String ini_str, String FindName) {
        String[] pairs = ini_str.split("\n");
        for (int i = 0; i < pairs.length; i++) {
            if(pairs[i].contains(FindName)) {
                String[] keyValue = pairs[i].split("=");
                return keyValue[1].trim();
            }
        }
        return "";
    }
}
