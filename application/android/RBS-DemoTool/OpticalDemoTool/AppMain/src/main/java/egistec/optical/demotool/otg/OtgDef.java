package egistec.optical.demotool.otg;

import egistec.optical.demotool.BuildConfig;
import egistec.optical.demotool.otg.manager.OtgSensorManager;

public class OtgDef {
    public static final int OTG_SENSOR_ID = BuildConfig.OTG_SENSOR_ID;
}
