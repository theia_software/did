package egistec.optical.demotool.lib;


//import android.util.Log;
import rbs.egistec.com.fplib.api.EgisLog;

import static rbs.egistec.com.fplib.api.FpResDef.LIVIMG_IMAGE_VERSION_V1;
import static rbs.egistec.com.fplib.api.FpResDef.LIVIMG_IMAGE_VERSION_V2;

public class LiveImageInfo {

    final String TAG = "RBS_" + LiveImageInfo.class.getSimpleName();

    public static final int LIVEIMAGE_MAX_HEADER_LEN = 256;
    public static final int LIVIMG_IMAGETYPE_FPIMAGELITE = 1;
    public static final int LIVING_IMAGETYPE_QMEXTRACT = 2;
    public static final int LIVING_IMAGETYPE_STATISTICSDATA = 3;
    public static final int LIVING_IMAGETYPE_CALI_IMAGE = 4;
    public static int LIVEIMGE_HEADER_LEN;

    private byte[] eventObjBytes;

    //fields for live image v1 - start
    public final int live_image_header_ver;
    public final int framecount;
    public final int img_width;
    public final int img_height;
    public final int raw_width;
    public final int raw_height;
    public final int raw_bpp;
    public final int live_image_type;
    public final int fingeron;
    public param_imageqmlib_t qm_parameter;
    public param_imageprocessing_t process_parameter;
    public param_image_statistics_data_t image_statistics_data;
    public param_cali_image_data_t image_cali_image_data;
    public int sequential_number;
    //fields for live image v1 - end

    //fields added to live image v2 - start
    public final int img_bpp;
    public final int reservered_1;
    public final int reservered_2;
    //fields added to live image v2 - end

    public class param_imageqmlib_t {
        public int isFinger;
        public int qty;
        public int size;
        public int percentage;
        public int img_level;
        public int gap;
        public int qm_score;
        public int dx;
        public int dy;
    }
    public class param_imageprocessing_t{
        public int isFinger;
        public int percentage;
        public int corner_count;
        public int img_qty;
        public int cover_count;
        public int img_level;
        public int bsd2;
    }
    public class param_image_statistics_data_t {
        public int max;
        public int min;
        public int mean;
    }
    public class param_cali_image_data_t {
        public int exp_time_x10;
        public int hw_integrate_count;
        public int bkg_cx;
        public int bkg_cy;
    }

    public LiveImageInfo(Object eventObj){
        if (eventObj == null || !(eventObj instanceof byte[])) {
            EgisLog.e(TAG, "got bad eventObj " + eventObj);
            live_image_header_ver = 0;
            framecount = 0;
            img_width = 0;
            img_height = 0;
            raw_width = 0;
            raw_height = 0;
            raw_bpp = 0;
            live_image_type = 0;
            fingeron = 0;
            img_bpp = 0;
            reservered_1 = 0;
            reservered_2 = 0;
            return;
        }
        eventObjBytes = (byte[]) eventObj;
        EgisLog.d(TAG, "eventObjBytes size=" + eventObjBytes.length);
        int [] result_header = FPUtil.byteArrayTointArray(eventObjBytes);
        int start_pos = 0;
        live_image_header_ver = result_header[start_pos++];
        if(live_image_header_ver == LIVIMG_IMAGE_VERSION_V1){
            LIVEIMGE_HEADER_LEN = 68;
        }else if(live_image_header_ver == LIVIMG_IMAGE_VERSION_V2){
            LIVEIMGE_HEADER_LEN = 80;
        }else{
            EgisLog.e(TAG, "unsupported live image version "+live_image_header_ver);
        }
        framecount = result_header[start_pos++];
        img_width = result_header[start_pos++];
        img_height = result_header[start_pos++];
        raw_width = result_header[start_pos++];
        raw_height = result_header[start_pos++];
        raw_bpp = result_header[start_pos++];
        live_image_type = result_header[start_pos++];
        if(live_image_header_ver == LIVIMG_IMAGE_VERSION_V2){
            img_bpp = result_header[start_pos++];
            reservered_1 = result_header[start_pos++];
            reservered_2 = result_header[start_pos++];
        }else{
            img_bpp = 8;
            reservered_1 = 0;
            reservered_2 = 0;
        }

        if (LIVIMG_IMAGETYPE_FPIMAGELITE == live_image_type){
            process_parameter = new param_imageprocessing_t();
            process_parameter.isFinger = result_header[start_pos++];
            process_parameter.percentage = result_header[start_pos++];
            process_parameter.corner_count = result_header[start_pos++];
            process_parameter.img_qty = result_header[start_pos++];
            process_parameter.cover_count = result_header[start_pos++];
            process_parameter.img_level = result_header[start_pos++];
            process_parameter.bsd2 = result_header[start_pos++];
            fingeron = process_parameter.isFinger;
        }else if (LIVING_IMAGETYPE_QMEXTRACT == live_image_type){
            qm_parameter = new param_imageqmlib_t();
            qm_parameter.isFinger = result_header[start_pos++];
            qm_parameter.qty = result_header[start_pos++];
            qm_parameter.size = result_header[start_pos++];
            qm_parameter.percentage = result_header[start_pos++];
            qm_parameter.img_level = result_header[start_pos++];
            qm_parameter.gap = result_header[start_pos++];
            qm_parameter.qm_score = result_header[start_pos++];
            qm_parameter.dx = result_header[start_pos++];
            qm_parameter.dy = result_header[start_pos++];
            fingeron = qm_parameter.isFinger;
        }else if (LIVING_IMAGETYPE_STATISTICSDATA == live_image_type) {
            image_statistics_data = new param_image_statistics_data_t();
            image_statistics_data.max = result_header[start_pos++];
            image_statistics_data.min = result_header[start_pos++];
            image_statistics_data.mean = result_header[start_pos++];
            fingeron = 0;
        }else if (LIVING_IMAGETYPE_CALI_IMAGE == live_image_type) {
            image_cali_image_data = new param_cali_image_data_t();
            image_cali_image_data.exp_time_x10 = result_header[start_pos++];
            image_cali_image_data.hw_integrate_count = result_header[start_pos++];
            if (start_pos < result_header.length) {
                image_cali_image_data.bkg_cx = result_header[start_pos++];
                image_cali_image_data.bkg_cy = result_header[start_pos++];
            }
            fingeron = 0;
        }else {
            fingeron = 0;
        }
        EgisLog.d(TAG, String.format("header_ver=%d - %d (%d) %d:%d. raw %d:%d, img_bpp %d, raw_bpp %d.", live_image_header_ver, live_image_type, framecount,
                img_width, img_height, raw_width, raw_height, img_bpp, raw_bpp));
    }
    
    public byte[] getImage() {
        int imageSize = img_width * img_height * img_bpp / 8;
        if (imageSize <= 0) {
            return new byte[0];
        }

        try {
            byte[] img = new byte[imageSize];
            System.arraycopy(eventObjBytes, LiveImageInfo.LIVEIMGE_HEADER_LEN, img, 0, imageSize);
            return img;
        } catch (Exception e) {
            e.printStackTrace();
            return new byte[0];
        }
    }

    public byte[] getRawImage(boolean toBigEndian) {
        int imageSize = img_width * img_height * img_bpp / 8;
        int rawImageSize = raw_width * raw_height * raw_bpp / 8;
        if (rawImageSize <= 0) {
            return new byte[0];
        }
        try {
            byte[] img = new byte[rawImageSize];
            System.arraycopy(eventObjBytes, LiveImageInfo.LIVEIMGE_HEADER_LEN + imageSize, img, 0, rawImageSize);
            if (toBigEndian) {
                img = FPUtil.AndroidBufferToWindowsBuffer(img);
            }
            return img;
        } catch (Exception e) {
            e.printStackTrace();
            return new byte[0];
        }
    }

    public byte[] getRawEventObj() {
        return eventObjBytes ;
    }
}
