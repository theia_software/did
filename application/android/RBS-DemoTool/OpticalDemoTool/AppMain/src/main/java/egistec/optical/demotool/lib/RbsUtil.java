package egistec.optical.demotool.lib;

import android.graphics.Point;
import android.util.Log;
import android.widget.TextView;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.List;

import egistec.optical.demotool.IniSettings;
import egistec.optical.demotool.application.DemoToolApp;
import egistec.optical.demotool.experiment.ExperimentDef;
import rbs.egistec.com.fplib.api.EgisLog;
import rbs.egistec.com.fplib.api.FpResDef;
import rbs.egistec.com.fplib.api.RbsLib;

public class RbsUtil {
    private static final String TAG = RbsUtil.class.getSimpleName();
    public static int applyIniCxCy(int cx, int cy) {
        String[][] ini_item_update = {
                {FpResDef.KEY_CIRCLE_CENTER_X, String.valueOf(cx)},
                {FpResDef.KEY_CIRCLE_CENTER_Y, String.valueOf(cy)}
        };
        EgisLog.d(TAG, "applyIniCxCy "+cx+", "+cy);
        int retval = IniSettings.updateIniSettings(RbsLib.getInstance(DemoToolApp.getInstance())
                , IniSettings.INI_CONFIG_TYPE, FpResDef.INI_SECTION_SENSOR, ini_item_update);
        return retval;
    }

    public static void setTouchPosToSDK(int touch_count, List<Point> listPoint) {
        EgisLog.d(TAG, "PAPA MT setTouchPosToSDK touch_count = "+touch_count);
        if(touch_count < 0 || listPoint == null){
            EgisLog.e(TAG, "setTouchPosToSDK: invvalid parameters "+touch_count+", "+listPoint);
            return;
        }

        RbsLib rbsLib = RbsLib.getInstance(DemoToolApp.getInstance());
        if (rbsLib == null) {
            Log.e(TAG, "mLib is null");
            return;
        }


        if(touch_count != listPoint.size()) {
            Log.e(TAG, "setTouchPosToSDK: invalid input parameters "+touch_count+", "+listPoint.size());
            return;
        }

        if(touch_count == 0 || listPoint.isEmpty()){
            int[] in_buffer = new int[1];
            in_buffer[0]= 0;
            byte[] in_byte_buffer = FPUtil.intArrayToBytes(in_buffer);
            rbsLib.extraApi(ExperimentDef.PID_HOST_TOUCH, ExperimentDef.CMD_HOST_TOUCH_SET_TOUCH_POS, in_byte_buffer, null, null);
            return;
        }


        int in_buffer_size = 1 + touch_count * 2;
        int[] in_buffer = new int[in_buffer_size];
        in_buffer[0]= touch_count;
        for(int i=0; i < touch_count; i++) {
            in_buffer[2*i+1]=listPoint.get(i).x;
            in_buffer[2*i+2]=listPoint.get(i).y;
            Log.d(TAG, "PAPA MT XY = "+in_buffer[2*i+1]+", "+in_buffer[2*i+2]);
        }
        byte[] in_byte_buffer = FPUtil.intArrayToBytes(in_buffer);
        rbsLib.extraApi(ExperimentDef.PID_HOST_TOUCH, ExperimentDef.CMD_HOST_TOUCH_SET_TOUCH_POS, in_byte_buffer, null, null);
    }

    public static void setTouchDownEventToSDK(TextView textView) {
        RbsLib rbsLib = RbsLib.getInstance(DemoToolApp.getInstance());
        if (rbsLib == null) {
            Log.e(TAG, "mLib is null");
            return;
        }
        byte[] out_buf = new byte[4];
        int[] out_size = new int[1];
        out_size[0] = 4;
        rbsLib.extraApi(ExperimentDef.PID_HOST_TOUCH, ExperimentDef.CMD_HOST_TOUCH_SET_TOUCHING, null, out_buf, out_size);
        ByteBuffer bb = ByteBuffer.wrap(out_buf, 0, 4);
        bb.order(ByteOrder.LITTLE_ENDIAN);
        int temp_x10 = bb.getInt();
        Log.d(TAG, "sys temperature = " + temp_x10);
        if (textView != null) {
            textView.setText("BTP: " + String.valueOf(temp_x10 / 10f));
        }
    }
}
