package egistec.optical.demotool.igistec;


import android.util.Log;

import egistec.optical.demotool.BuildConfig;
import egistec.optical.demotool.experiment.ExperimentDef;
import egistec.optical.demotool.otg.otgsensor.OtgIO_V9;
import rbs.egistec.com.fplib.api.RbsLib;

/**
 * 用在AUO OLED SENSOR 200*200專用
 *
 */
public class AuoOledFingerprintReceiverHelper {
    static public String TAG = "AuoOledFingerprintReceiverHelper" ;

    static public Object syncLock = new Object() ;

    /**
     * 先用AUO HACK 相關的功能以便用FFT + BDS減背
     */
    static public boolean G_DEMO_INC_DEFINE_IN_MK = BuildConfig.CUSTOMER;

    /**
     * Define From C
     */
    public enum et760_param {
        ET760_RAW_SPI(9000);  // 使用純C時使用

        private int value;
        et760_param(int value) {
            this.value = value;
        }
        public int value() {
            return value;
        }
    }

    /**
     * 為了轉C
     * @param eventId
     * @param value1
     * @param value2
     * @param eventObj
     */
    static public void rawSPI(int eventId, int value1, int value2, Object eventObj) {

        synchronized (syncLock) {
            byte [] b = (byte[]) eventObj ;
            Log.d(TAG, "ET760 GET RAW SPI=" + eventId + "," + value1 + "," + value2 + " first byte = " + Integer.toHexString(b[0]) );
//            if ( b.length < 10) {
//                Log.d(TAG, "ET760 GET RAW SPI Data=" + Common.byteArrayToString(b));
//            }
            if ( b == null) {
                Log.i(TAG, "Event Obj is null") ;
                return ;
            }

            // SPEED in ascii is 83, 80, 69, 69, 68
            if (b[0] == 83) {
                if (b[1] == 80) {
                    if (b[2] == 69) {
                        if ( b[3] == 69 ) {
                            if (b[4] == 68) {
                                ((OtgIO_V9)SPIUtility.otgio).Set_SPI_CLK((byte)0, (byte)b[9]);
                                Log.i(TAG, "Set OTG_V9 SPI Clock=" + b[9]) ;
                                return ;
                            }
                        }
                    }
                }
            }

            // 32bit write
            if ( b[0] == 0x33 ) {
                int addr = Common.byteArrayToInt(1, b) ;
                int value = Common.byteArrayToInt(5, b) ;
                SPIUtility.writeSingle32(addr, value);
                Log.e(TAG, "RAW WRITE SPI=" + Integer.toHexString(addr) + "," + Integer.toHexString(value)) ;
            }

            // 32bit read
            if ( b[0] == 0x32 ) {

                // 預期 一定會打開SPI MODE
                int addr = Common.byteArrayToInt(1, b) ;
                int value = SPIUtility.readSingle32(addr);
                byte[] byteValue = Common.intToByteArray(value) ;
                Log.e(TAG, "RAW READ SPI=" + Integer.toHexString(addr) + "," + Common.byteArrayToString(byteValue) ) ;
                for ( int i = 0 ; i < 4 ; i++ ) {
                    b[i+6] = byteValue[i] ; // dummy byte +1
                }

            }

            // burst write
            if ( b[0] == 0x26 ) {
                Log.d(TAG, "RAW SPI BURST WRITE START") ;
                byte offset = 0 ;
                byte [] data = new byte[ b.length - 2 ] ; // 2=opcode & offset
                for ( int i = 2 ; i < b.length ; i++ ) {
                    data[i-2] = b[i] ;
                }
                SPIUtility.writeBurst( offset, data, data.length);
                Log.e(TAG, "RAW SPI BURST WRITE=" + offset + " len=" + data.length) ;
            }

            // burst read
            if ( b[0] == 0x22 ) {
                byte offset = 0 ;
                byte [] data = new byte[ b.length - 3 ] ;
                SPIUtility.readBurst( offset, data, data.length);
                int sum = 0 ;
                for ( int i = 0 ; i < data.length ; i++ ) {
                    b[i+3] = data[i] ;
                    sum = sum + data[i] ;
                }
                Log.e(TAG, "RAW SPI BURST READ=" + offset + " len=" + data.length + " byte.sum=" + sum) ;
            }

            if ( b.length == 8 ) {
                // 推測是OPCODE !!
                // TODO: 但有可能會有問題，這裡應該去CHECK OPCODE列表，沒有的話就無視，但尚未去作這件事。
                Log.e(TAG, "RAW SPI OPCODE=" + Common.byteArrayToString(b));
                SPIUtility.setRiscVOpcode(b);
            }
        }
    }


    /**
     * 如果要在JAVA直接存取IMAGE (之後CONFIG都要寫進RBS)
     * 先從RBS叫一次GET IMAGE
     * 把RBS內部的CONFIG帶到IC來
     * @param lib
     */
    static public void focus_get_image_from_rbs_once(RbsLib lib) {

        int img_raw_length = 200*200*2 ;
        int img_8bit_length = 200*200 ;
        int header_length = 1024 ; // todo: fix from consts
        int img_length = img_8bit_length + img_raw_length + header_length ;
        byte[] img = new byte[img_length] ;
        int [] img_length_array = new int[]{img_length};
        byte [] in_buffer = new byte[4];

        lib.extraApi(ExperimentDef.PID_FAETOOL, ExperimentDef.FAETOOL_GET_IMAGE, in_buffer, img, img_length_array);
    }
}

