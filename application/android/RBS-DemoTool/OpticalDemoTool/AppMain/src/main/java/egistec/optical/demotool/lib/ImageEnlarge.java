package egistec.optical.demotool.lib;

import android.app.AlertDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;

import egistec.optical.demotool.R;

public class ImageEnlarge {
    public static void ImgEnlarge(Context context, Bitmap bitmap) {
        if(bitmap != null) {
            LayoutInflater inflater = LayoutInflater.from(context);
            View imgEntryView = inflater.inflate(R.layout.activity_dialog_image_enlarge, null);
            final AlertDialog dialog = new AlertDialog.Builder(context, R.style.EnlargeImageDialogStyle).create();
            ImageView img = (ImageView) imgEntryView.findViewById(R.id.large_image);
            WindowManager.LayoutParams params = dialog.getWindow().getAttributes();
            img.setImageBitmap(bitmap);
            dialog.setView(imgEntryView);
            dialog.show();

            params.gravity = Gravity.CENTER;
            dialog.getWindow().setAttributes(params);

            imgEntryView.setOnClickListener(new View.OnClickListener() {
                public void onClick(View paramView) {
                    dialog.cancel();
                }
            });
        }
    }
}
