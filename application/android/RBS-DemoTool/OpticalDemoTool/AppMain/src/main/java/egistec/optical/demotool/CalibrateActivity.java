package egistec.optical.demotool;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.graphics.Rect;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.util.TypedValue;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;

import java.io.File;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;

import egistec.optical.demotool.application.DemoToolApp;
import egistec.optical.demotool.databinding.ActivityCalibrateBinding;
import egistec.optical.demotool.experiment.ExperimentDef;
import egistec.optical.demotool.experiment.ExperimentUtils;
import egistec.optical.demotool.experiment.FileIoUtils;
import egistec.optical.demotool.igistec.AuoOledFingerprintReceiverHelper;
import egistec.optical.demotool.igistec.Common;
import egistec.optical.demotool.lib.CircleShaderView;
import egistec.optical.demotool.lib.FPUtil;
import egistec.optical.demotool.lib.FileObj;
import egistec.optical.demotool.lib.ImageProcess;
import egistec.optical.demotool.lib.LiveImageInfo;
import egistec.optical.demotool.lib.RbsUtil;
import egistec.optical.demotool.lib.SaveFileHelper;
import egistec.optical.demotool.lib.TemperatureReceiver;
import egistec.optical.demotool.lib.UIUtils;
import egistec.optical.demotool.otg.manager.OtgSensorManager;
import rbs.egistec.com.fplib.api.FingerprintReceiver;
import rbs.egistec.com.fplib.api.FpResDef;
import rbs.egistec.com.fplib.api.RbsLib;
import rbs.egistec.com.fplib.api.SensorDef;
import rbs.egistec.com.fplib.api.EgisLog;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;
import static egistec.optical.demotool.otg.OtgDef.OTG_SENSOR_ID;

public class CalibrateActivity extends AppCompatActivity implements OnKeyboardVisibilityListener{
    Button mbtnCalibration;
    Button mbtnRemoveCalibration;
    Button mbtnRemoveBDS;
    Button mbtnAutoCalibration;
    TextView mTextView;
    ImageView mImageView;
    Button btnExit ;

    private static final String TAG = "RBS_" + CalibrateActivity.class.getSimpleName();

    private RbsLib mLib;

    private Context mContext;
    private Button mButtonICWindow;
    private Button mButtonSaveCoordinate;
    private EditText mEditTextICwindowX;
    private EditText mEditTextICwindowY;
    private EditText mEditTextICwindowW;
    private EditText mEditTextICwindowH;

    private AppSettingsPreference.SENSOR_SIZE sensorSize = AppSettingsPreference.SENSOR_SIZE.SENSOR_SIZE_LARGE;
    private RelativeLayout mICWindowContainer;
    private RelativeLayout mSQWindowContainer;

    private BarChart mHisImage;
    private TextView mTextViewBarChart1Info2;
    private TextView mTextViewExpTime;
    private TextView mTextViewHwIntegrateCount;
    private FileIoUtils.WindowPos icWindowPos = null;
    private FileIoUtils.SQWindowPos sqWindowPos = null;

    private TemperatureReceiver mReceiver;

    private SaveFileHelper saveFileHelper;
    private OtgSensorManager mOtgSensorManager;
    private ActivityCalibrateBinding mBinding;
    private AppSettings mAppSettings;
    private CircleShaderView mGreenGradation;
    private AutoCalibrateAyncTask mAutoAsyncTask;
    private LiveImageInfo mImageInfo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mAppSettings = new AppSettings(this);
        sensorSize = AppSettingsPreference.getSensorSize(mAppSettings.getSensorSizeIndex());
        if(sensorSize == AppSettingsPreference.SENSOR_SIZE.SENSOR_SIZE_LARGE) {
            UIUtils.hideSystemUI(getWindow().getDecorView());
        }
        setContentView(R.layout.activity_calibrate);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_calibrate);
        mContext = this;
        mReceiver = new TemperatureReceiver();
        mReceiver.setTextView(mBinding.textViewShowTemperatureInfo);

        mTextView = (TextView) findViewById(R.id.textView);
        mTextView.setMovementMethod(ScrollingMovementMethod.getInstance());
        mImageView = (ImageView) findViewById(R.id.imageView);
        mTextViewExpTime =  (TextView) findViewById(R.id.exp_time);
        mTextViewHwIntegrateCount =  (TextView) findViewById(R.id.hw_integrate_count);

        if (OTG_SENSOR_ID > 0) {
            mOtgSensorManager = OtgSensorManager.getInstance(OTG_SENSOR_ID, this);
        }
        mLib = RbsLib.getInstance(this);

        saveFileHelper = new SaveFileHelper();
        saveFileHelper.Create();

        // mBarChart1
        mHisImage = (BarChart)findViewById(R.id.barChart2);

        ArrayList<BarEntry> entries = new ArrayList<>();
        for (int i = 0; i < 255; i++)
            entries.add(new BarEntry(i, i));
        BarDataSet barDataSet1 = new BarDataSet(entries, "Intensity");
        BarData barData1 = new BarData(barDataSet1);
        mHisImage.setData(barData1);
        mTextViewBarChart1Info2 = (TextView)findViewById(R.id.textViewBarChart1Info2);
        mEditTextICwindowX = (EditText)findViewById(R.id.editICWindowX);
        mEditTextICwindowY = (EditText)findViewById(R.id.editICWindowY);
        mEditTextICwindowW = (EditText)findViewById(R.id.editICWindowW);
        mEditTextICwindowH = (EditText)findViewById(R.id.editICWindowH);
        mICWindowContainer = (RelativeLayout)findViewById(R.id.IC_window_setting_container);
        mSQWindowContainer = (RelativeLayout)findViewById(R.id.SQ_Window_container);
        EgisLog.d(TAG, "PAPA density = "+ getResources().getDisplayMetrics().density * (500 * 0.07 / 2.54)*160+", "+getResources().getDisplayMetrics().density * (640 * 0.07 / 2.54) * 160);
        switch (sensorSize){
            case SENSOR_SIZE_LARGE:
                mICWindowContainer.setVisibility(GONE);
                mSQWindowContainer.setVisibility(VISIBLE);
                sqWindowPos = FileIoUtils.loadSQWindowLayoutData();
                ExperimentUtils.setSQPressArea(mSQWindowContainer, sqWindowPos.x, sqWindowPos.y, sqWindowPos.width, sqWindowPos.height
                        , getResources().getDisplayMetrics());
                mEditTextICwindowX.setVisibility(VISIBLE);
                mEditTextICwindowX.setText(String.valueOf(sqWindowPos.x));
                mEditTextICwindowY.setVisibility(VISIBLE);
                mEditTextICwindowY.setText(String.valueOf(sqWindowPos.y));
                mEditTextICwindowW.setVisibility(VISIBLE);
                mEditTextICwindowW.setText(String.valueOf(sqWindowPos.width));
                mEditTextICwindowH.setVisibility(VISIBLE);
                mEditTextICwindowH.setText(String.valueOf(sqWindowPos.height));
                mBinding.buttonResetCoordinate.setVisibility(VISIBLE);
                break;
            case SENSOR_SIZE_NORMAL:
                mICWindowContainer.setVisibility(VISIBLE);
                mSQWindowContainer.setVisibility(GONE);
                icWindowPos = FileIoUtils.loadIcWindowLayoutData();
                ExperimentUtils.setPressAreaLeftTop(mICWindowContainer, icWindowPos.x, icWindowPos.y, getResources().getDisplayMetrics());
                mEditTextICwindowX.setVisibility(VISIBLE);
                mEditTextICwindowX.setText(String.valueOf(icWindowPos.x));
                mEditTextICwindowY.setVisibility(VISIBLE);
                mEditTextICwindowY.setText(String.valueOf(icWindowPos.y));
                mEditTextICwindowW.setVisibility(GONE);
                mEditTextICwindowW.setText(String.valueOf(0));
                mEditTextICwindowH.setVisibility(GONE);
                mEditTextICwindowH.setText(String.valueOf(0));
                mBinding.textWidth.setVisibility(GONE);
                mBinding.textHeight.setVisibility(GONE);
                break;
            default:
                break;
        }
        mButtonICWindow = findViewById(R.id.IC_window_setting);
        mButtonICWindow.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN)
                {
                    EgisLog.i("log", "action_down");
                    return true;
                }
                else if (motionEvent.getAction() == MotionEvent.ACTION_UP)
                {
                    EgisLog.i("log", "action_up");
                    return true;
                }

                return false;
            }
        });

        // Request permissions
        if (Build.VERSION.SDK_INT >= 23) {
            int REQUEST_CODE_CONTACT = 101;
            String[] permissions = {Manifest.permission.WRITE_EXTERNAL_STORAGE};
            for (String str : permissions) {
                if (this.checkSelfPermission(str) != PackageManager.PERMISSION_GRANTED) {
                    this.requestPermissions(permissions, REQUEST_CODE_CONTACT);
                    return;
                }
            }
        }

        mBinding.buttonResetCoordinate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (sensorSize){
                    case SENSOR_SIZE_LARGE:
                        sqWindowPos.x = 0;
                        sqWindowPos.y = 0;
                        sqWindowPos.width = 200;
                        sqWindowPos.height = 200;
                        ExperimentUtils.setSQPressArea(mSQWindowContainer, sqWindowPos.x, sqWindowPos.y, sqWindowPos.width, sqWindowPos.height
                                , getResources().getDisplayMetrics());
                        FileIoUtils.saveSQWindowLayoutData(sqWindowPos.x, sqWindowPos.y, sqWindowPos.width, sqWindowPos.height);
                        mEditTextICwindowX.setText(String.valueOf(sqWindowPos.x));
                        mEditTextICwindowY.setText(String.valueOf(sqWindowPos.y));
                        mEditTextICwindowW.setText(String.valueOf(sqWindowPos.width));
                        mEditTextICwindowH.setText(String.valueOf(sqWindowPos.height));
                        break;
                    case SENSOR_SIZE_NORMAL:
                    default:
                        EgisLog.e(TAG, "mButtonSaveCoordinate invalid sensor size "+sensorSize);
                        break;
                }
            }
        });

        mButtonSaveCoordinate = (Button)findViewById(R.id.buttonSaveCoordinate);
        mButtonSaveCoordinate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switch (sensorSize){
                    case SENSOR_SIZE_LARGE:
                        try {
                            sqWindowPos.x = Integer.parseInt(mEditTextICwindowX.getText().toString(), 10);
                            sqWindowPos.y = Integer.parseInt(mEditTextICwindowY.getText().toString(), 10);
                            sqWindowPos.width = Integer.parseInt(mEditTextICwindowW.getText().toString(), 10);
                            sqWindowPos.height = Integer.parseInt(mEditTextICwindowH.getText().toString(), 10);
                        } catch (NumberFormatException ex) {
                            sqWindowPos.x = 0;
                            sqWindowPos.y = 0;
                            sqWindowPos.width = 200;
                            sqWindowPos.height = 200;
                        }
                        ExperimentUtils.setSQPressArea(mSQWindowContainer, sqWindowPos.x, sqWindowPos.y, sqWindowPos.width, sqWindowPos.height
                                , getResources().getDisplayMetrics());
                        FileIoUtils.saveSQWindowLayoutData(sqWindowPos.x, sqWindowPos.y, sqWindowPos.width, sqWindowPos.height);
                        mEditTextICwindowX.setText(String.valueOf(sqWindowPos.x));
                        mEditTextICwindowY.setText(String.valueOf(sqWindowPos.y));
                        mEditTextICwindowW.setText(String.valueOf(sqWindowPos.width));
                        mEditTextICwindowH.setText(String.valueOf(sqWindowPos.height));
                        break;
                    case SENSOR_SIZE_NORMAL:
                        try {
                            icWindowPos.x = Integer.parseInt(mEditTextICwindowX.getText().toString(), 10);
                            icWindowPos.y = Integer.parseInt(mEditTextICwindowY.getText().toString(), 10);
                        } catch (NumberFormatException ex) {
                            icWindowPos.x = 0;
                            icWindowPos.y = 0;
                        }
                        ExperimentUtils.setPressAreaLeftTop(mICWindowContainer, icWindowPos.x, icWindowPos.y, getResources().getDisplayMetrics());
                        FileIoUtils.saveIcWindowLayoutData(icWindowPos.x, icWindowPos.y);
                        mEditTextICwindowX.setText(String.valueOf(icWindowPos.x));
                        mEditTextICwindowY.setText(String.valueOf(icWindowPos.y));
                        mEditTextICwindowW.setText(String.valueOf(0));
                        mEditTextICwindowH.setText(String.valueOf(0));
                        break;
                    default:
                        EgisLog.e(TAG, "mButtonSaveCoordinate invalid sensor size "+sensorSize);
                        break;
                }
            }
        });

        mBinding.buttonApplyBkgCenter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int retval = RbsUtil.applyIniCxCy(mImageInfo.image_cali_image_data.bkg_cx, mImageInfo.image_cali_image_data.bkg_cy);
                if (retval == FpResDef.RESULT_OK)
                    Toast.makeText(getApplicationContext(), R.string.toast_update_ini_success, Toast.LENGTH_SHORT).show();
                else
                    Toast.makeText(getApplicationContext(), R.string.toast_update_ini_fail, Toast.LENGTH_SHORT).show();
            }
        });

        mbtnCalibration = (Button)findViewById(R.id.buttonCalibration);
        mbtnCalibration.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
                mbtnCalibration.setEnabled(false);
                mImageView.setVisibility(View.INVISIBLE);
                mBinding.buttonApplyBkgCenter.setVisibility(View.INVISIBLE);
                CalibrateAyncTask asyncTask = new CalibrateAyncTask() {
                    @Override
                    protected void onPostExecute(Integer ret) {
                        super.onPostExecute(ret);
                        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
                        mbtnCalibration.setEnabled(true);
                        mImageView.setVisibility(View.VISIBLE);
                    }
                };
                asyncTask.setParams(v.getContext(), 0, true, mTextView);
                asyncTask.execute();
            }
        });

        mbtnRemoveCalibration = (Button)findViewById(R.id.buttonRemoveCalibration);
        mbtnRemoveCalibration.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RemoveAlertDialog();
            }
        });
        mbtnRemoveBDS = (Button)findViewById(R.id.buttonRemoveBDS);
        mbtnRemoveBDS.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RemoveBDSAlertDialog();
            }
        });
        CalibrateAyncTask asyncTask = new CalibrateAyncTask();
        asyncTask.setParams(this, 500, false, mTextView);
        asyncTask.execute();

        mbtnAutoCalibration = (Button)findViewById(R.id.buttonAutoCalibration);
        mbtnAutoCalibration.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mbtnAutoCalibration.setEnabled(false);
                mImageView.setVisibility(View.INVISIBLE);
                mBinding.buttonApplyAutoCalibrationResult.setVisibility(View.INVISIBLE);
                mAutoAsyncTask = new AutoCalibrateAyncTask() {
                    @Override
                    protected void onPostExecute(Integer ret) {
                        super.onPostExecute(ret);
                        mbtnAutoCalibration.setEnabled(true);
                        mImageView.setVisibility(View.VISIBLE);
                    }
                };
                mAutoAsyncTask.setParams(v.getContext(), 0, true, mBinding.textAutoCalibrationResult);
                mAutoAsyncTask.execute();
            }
        });
        mBinding.buttonApplyAutoCalibrationResult.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String[][] ini_item_update = {
                        {FpResDef.KEY_EXPOSURE_TIME, String.format("%.1f", mAutoAsyncTask.expo)},
                        {FpResDef.KEY_INTEGRATE_COUNT_HW, String.valueOf(mAutoAsyncTask.hw_int)}
                };
                int retval = IniSettings.updateIniSettings(mLib, IniSettings.INI_CONFIG_TYPE, FpResDef.INI_SECTION_SENSOR, ini_item_update);
                if (retval == FpResDef.RESULT_OK)
                    Toast.makeText(getApplicationContext(), R.string.toast_update_ini_success, Toast.LENGTH_SHORT).show();
                else
                    Toast.makeText(getApplicationContext(), R.string.toast_update_ini_fail, Toast.LENGTH_SHORT).show();
            }
        });
        int resId = AppSettingsPreference.getPressAreaDrawableId(mAppSettings.getPressAreaTypeId());
        if (mGreenGradation != null)
            mICWindowContainer.removeView(mGreenGradation);
        if (resId >= 0) {
            mBinding.enrollGif.setVisibility(View.VISIBLE);
            mBinding.enrollGif.setImageResource(resId);
        } else {
            mBinding.enrollGif.setVisibility(View.INVISIBLE);
            mGreenGradation = new CircleShaderView(this);
            mGreenGradation.setCircleCanvas(mICWindowContainer.getLayoutParams().width, mICWindowContainer.getLayoutParams().height);
            float diameter = AppSettingsPreference.getPressAreaDiameter(mAppSettings.getPressAreaDiameterIndex());
            mGreenGradation.setRadius(diameter/2 + CircleShaderView.GRADATION_RADIUS);
            mICWindowContainer.addView(mGreenGradation);
        }

        setKeyboardVisibilityListener(this);


        btnExit = findViewById(R.id.btnExit) ;
        btnExit.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                finish();
                return false;
            }
        });
    }

    @Override
    public void onKBVisibilityChanged(boolean visible) {
        Log.d(TAG, "onKBVisibilityChanged "+visible);
        if(!visible){
            UIUtils.hideSystemUI(getWindow().getDecorView());
        }
    }

    private void RemoveAlertDialog() {
        new AlertDialog.Builder(this)
                .setTitle("WARNING")
                .setMessage("Do you want to remove calibration.bin and calibration_bds.bin?")
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        int calibrateRet = mLib.extraApi(ExperimentDef.PID_BKG_IMG, ExperimentDef.CMD_BKG_IMG_REMOVE_CALI_DATA, null, null, null);
                        if (calibrateRet != 0) {
                            EgisLog.e(TAG, String.format("CMD_BKG_IMG_REMOVE_CALI_DATA failed ret=%d", calibrateRet));
                            Toast.makeText(mContext, "Delete fail!!!", Toast.LENGTH_SHORT).show();
                        }
                        else {
                            Toast.makeText(mContext, "Delete calibration.bin and calibration_bds.bin", Toast.LENGTH_SHORT).show();
                        }
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                })
                .show();
    }

    private void RemoveBDSAlertDialog() {
        new AlertDialog.Builder(this)
                .setTitle("WARNING")
                .setMessage("Do you want to remove calibration_bds.bin?")
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        int calibrateRet = mLib.extraApi(ExperimentDef.PID_BKG_IMG, ExperimentDef.CMD_BKG_IMG_REMOVE_BDS, null, null, null);
                        if (calibrateRet != 0) {
                            EgisLog.e(TAG, String.format("CMD_BKG_IMG_REMOVE_BDS failed ret=%d", calibrateRet));
                            Toast.makeText(mContext, "Delete fail!!!", Toast.LENGTH_SHORT).show();
                        }
                        else {
                            Toast.makeText(mContext, "Delete calibration_bds.bin", Toast.LENGTH_SHORT).show();
                        }
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                })
                .show();
    }

    private void updateBarChart(int width, int height, byte[] image) {
        EgisLog.d(TAG, "updateBarChart in");
        if(width <= 0 || height <= 0 || image == null || image.length <= 0){
            EgisLog.e(TAG, "updateBarChart invalid input parameters");
            return;
        }
        ArrayList<BarEntry> entries = new ArrayList<>();
        BarChart barChart;
        BarDataSet barDataSet;

        int[] histogram = new int[256];
        int[] data = new int[width * height];
        int[] barChartData;

        for (int i = 0; i < image.length; i++) {
            data[i] = (~image[i]) & 0xFF;
        }
        EgisLog.d(TAG, "image size= " + data.length + "  data 1 = " + data[1]);

        barChartData = data;

        for (int i = 0; i < barChartData.length; i++)
            histogram[Math.abs(barChartData[i])]++;

        for (int i = 0; i < 255; i++) entries.add(new BarEntry(i, histogram[i]));

        barChart = mHisImage;
        if (barChart.getVisibility() == VISIBLE) {
            for (int i = 0; i < barChartData.length; i++)
                histogram[Math.abs(barChartData[i])]++;

            for (int i = 0; i < 255; i++) entries.add(new BarEntry(i, histogram[i]));

            barDataSet = (BarDataSet) barChart.getData().getDataSetByIndex(0);
            barDataSet.setValues(entries);
            barChart.getData().notifyDataChanged();
            barChart.notifyDataSetChanged();
            barChart.invalidate();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        boolean isConsumed = false;
        switch (item.getItemId()) {
            case R.id.menu_calibrate:
                Intent i = new Intent(this, CalibrateActivity.class);
                startActivity(i);
                isConsumed = true;
                break;
            case R.id.menu_find_dot_location:
                Intent j = new Intent(this, LocationActivity.class);
                startActivity(j);
                isConsumed = true;
                break;
        }
        return isConsumed || super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mLib != null) {
            mLib.startListening(mFingerprintReceiver);
        }
        mLib.extraApi(ExperimentDef.PID_HOST_TOUCH, ExperimentDef.CMD_DEMOTOOL_SENSOR_POWER_ON, null, null, null);
        registerReceiver(mReceiver, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
    }

    @Override
    protected void onDestroy() {
        saveFileHelper.Destroy();
        super.onDestroy();
    }

    @Override
    protected void onPause() {
        unregisterReceiver(mReceiver);
        if (mLib != null) {
            mLib.cancel();
        }
        // mLib.extraApi(ExperimentDef.PID_HOST_TOUCH, ExperimentDef.CMD_DEMOTOOL_SENSOR_POWER_OFF, null, null, null);
        super.onPause();
    }

    private void showInfo(LiveImageInfo liveImageInfo) {
        if(liveImageInfo == null || liveImageInfo.image_cali_image_data == null){
            EgisLog.e(TAG, "showInfo : invalid object");
            mTextViewExpTime.setText("");
            mTextViewHwIntegrateCount.setText("");
            mBinding.bkgCenter.setText("");
            return;
        }
        mTextViewExpTime.setText("Exposure_Time_x10: " + liveImageInfo.image_cali_image_data.exp_time_x10);
        mTextViewHwIntegrateCount.setText("HW_Integrate_Count: " + liveImageInfo.image_cali_image_data.hw_integrate_count);
        mBinding.bkgCenter.setText(String.format("Bkg center: %d, %d", liveImageInfo.image_cali_image_data.bkg_cx, liveImageInfo.image_cali_image_data.bkg_cy));
    }

    private FingerprintReceiver mFingerprintReceiver = new FingerprintReceiver(){

        @Override
        public void onFingerprintEvent(int eventId, int value1, int value2, Object eventObj) {
            EgisLog.d(TAG, "onFingerprintEvent eventId = " + eventId + " value1 = " + value1 + " value2 = " + value2);

            switch(eventId){
                case FpResDef.EVENT_VERIFY_MATCHED:

                    break;

                case FpResDef.EVENT_VERIFY_NOT_MATCHED:

                    break;

                case FpResDef.EVENT_ERR_IDENTIFY:

                    break;

                case FpResDef.EVENT_IMG_BAD_QLTY:

                    break;

                case FpResDef.EVENT_IMG_PARTIAL:

                    break;

                case FpResDef.EVENT_IMG_FAST:

                    break;
                case FpResDef.EVENT_IMG_WATER:

                    break;

                case FpResDef.EVENT_FINGER_WAIT:

                    break;

                case FpResDef.EVENT_FINGER_TOUCH :

                    break;

                case FpResDef.EVENT_FINGER_READY:
                    EgisLog.d(TAG, "+++++ EVENT_FINGER_READY +++++");
                    saveFileHelper.resetToSave();
                    break;

                case FpResDef.EVENT_FINGER_LEAVE:
                    EgisLog.d(TAG, "+++++ SettingActivity2 EVENT_FINGER_LEAVE +++++");
                    break;

                case FpResDef.CAPTURE_FINISH:

                    break;

                case FpResDef.EVENT_RETURN_IMAGE_COUNT:
                    break;

                case FpResDef.EVENT_RETURN_IMAGE:
                    EgisLog.e(TAG, "+++++ RETURN_IMAGE +++++ not handling");
                    break;
                case FpResDef.EVENT_RETURN_LIVE_IMAGE_OUT:
                    EgisLog.d(TAG, "+++++ RETURN_CALIBRATE_IMAGE +++++");
                    if (eventObj != null) {
                        mImageInfo = new LiveImageInfo(eventObj);
                        if(mImageInfo.image_cali_image_data == null){
                            EgisLog.e(TAG, "invalid image_cali_image_data");
                            return;
                        }
                        int width = mImageInfo.img_width;
                        int height = mImageInfo.img_height;
                        byte[] img = mImageInfo.getImage();
                        ImageProcess.showImage(mImageView, width, height, img);
                        showInfo(mImageInfo);
                        byte[] img_inverse = ImageProcess.raw_inverse(img);
                        updateBarChart(width, height, img_inverse);
                        mBinding.buttonApplyBkgCenter.setVisibility(View.VISIBLE);
                        int sensorId = SensorDef.GetSensorIdByImageSize(width, height);
                        EgisLog.d(TAG, "sensor id " + sensorId);
                        String[][] db_ini_item_update = {
                                {FpResDef.KEY_IMAGE_DB_TYPE, "0"},
                                {FpResDef.KEY_DB_SENSOR_SERIES, String.valueOf(sensorId / 100)},
                                {FpResDef.KEY_DB_SENSOR_TYPE, String.valueOf(sensorId % 100)},
                                {FpResDef.KEY_DB_IMAGE_BIN_FOLDER, "image_bin"},
                                {FpResDef.KEY_DB_IMAGE_WIDTH, String.valueOf(mImageInfo.img_width)},
                                {FpResDef.KEY_DB_IMAGE_HEIGHT, String.valueOf(mImageInfo.img_height)},
                                {FpResDef.KEY_DB_IMAGE_CENTER_X, String.valueOf(mImageInfo.image_cali_image_data.bkg_cx)},
                                {FpResDef.KEY_DB_IMAGE_CENTER_Y, String.valueOf(mImageInfo.image_cali_image_data.bkg_cy)}
                        };
                        IniSettings.updateIniSettings(mLib, IniSettings.INI_DB_TYPE, FpResDef.INI_SECTION_DB, db_ini_item_update);

                        String bkgFilePath = DemoToolApp.getMyDataRoot() + "bkg.bin";
                        saveFile(bkgFilePath, mImageInfo.getRawImage(true));
                    } else {
                        EgisLog.e(TAG, "eventObj is null");
                    }
                    break;
                case FpResDef.EVENT_OTG_WRITE_REGISTER:
                case FpResDef.EVENT_OTG_READ_REGISTER:
                case FpResDef.EVENT_OTG_GET_FRAME:
                case FpResDef.EVENT_OTG_WAKE_UP:
                case FpResDef.EVENT_OTG_STANDBY:
                case FpResDef.EVENT_OTG_SPI_WRITE_READ:
                case FpResDef.EVENT_OTG_SET_SPI_CLK:
                    if (mOtgSensorManager != null) {
                        mOtgSensorManager.nativeCallbackHandler(eventId, value1, value2, (byte[])eventObj);
                    }
                    break;
                case FpResDef.EVENT_OTG_GET_SENSOR_ID:
                    break;
                case FpResDef.EVENT_OTG_INIT_SENSOR:
                    break;
                case FpResDef.EVENT_OTG_SET_SENSOR_PARAM:
                    break;

                case FpResDef.EVENT_OTG_GET_IMAGE:
                    break;

                case FpResDef.EVENT_OTG_ET760_RAW_SPI:
                    AuoOledFingerprintReceiverHelper.rawSPI(eventId,value1,value2,eventObj);
                    break;

                default:
                    break;
            }
        }
    };

    private boolean saveFile(String path, byte [] img) {
        File file = new File(path);
        if (!file.getParentFile().exists()) {
            boolean makeDir = file.getParentFile().mkdirs();
            if (!makeDir)  return false;
        }
        FileObj fileObj = new FileObj(file, img);
        saveFileHelper.Save(fileObj);
        return true;
    }

    class CalibrateAyncTask extends AsyncTask<Void, Integer, Integer> {

        Context context;
        int delayMs;
        boolean doCalibrate;
        TextView textView;
        public void setParams(Context context, int delayMs, boolean doCalibrate, TextView textView) {
            this.context = context;
            this.delayMs = delayMs;
            this.doCalibrate = doCalibrate;
            this.textView = textView;
        }

        @Override
        protected void onPreExecute() {
            textView.setText(R.string.calibrate_backlight_hint);
            super.onPreExecute();
        }

        @Override
        protected Integer doInBackground(Void... voids) {
            if (delayMs > 0) {
                try {
                    Thread.sleep(delayMs);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            int calibrateRet = 0, ret = 0;
            if (doCalibrate) {
                calibrateRet = mLib.extraApi(ExperimentDef.PID_BKG_IMG, ExperimentDef.CMD_BKG_IMG_RUN_CALI_PROC, null, null, null);
                if (calibrateRet != 0) {
                    EgisLog.e(TAG, String.format("CMD_BKG_IMG_RUN_CALI_PROC failed ret=%d", calibrateRet));
                }
            }
            ret = mLib.extraApi(ExperimentDef.PID_BKG_IMG, ExperimentDef.CMD_BKG_IMG_GET_CALI_IMAGE, null, null, null);
            return calibrateRet != 0 ? calibrateRet : ret;
        }

        @Override
        protected void onPostExecute(Integer ret) {
            EgisLog.d(TAG, String.format("CalibrateAyncTask onPostExecute ret=%d", ret));
            if (ret != 0) {
                EgisLog.e(TAG, String.format("CalibrateAyncTask ret=%d", ret));
            }
            if (context != null) {
                if (ret != 0) {
                    Toast.makeText(context, R.string.failed, Toast.LENGTH_SHORT).show();
                    textView.setText(textView.getText() + "\n");
                    textView.append(FPUtil.getColorStringBuilder("Failed", Color.RED));
                } else {
                    textView.setText(textView.getText() + "\n");
                    textView.append(FPUtil.getColorStringBuilder("Success", Color.BLUE));
                }
            }
        }
    };

    class AutoCalibrateAyncTask extends AsyncTask<Void, Integer, Integer> {

        Context context;
        int delayMs;
        boolean doCalibrate;
        public float expo;
        public int hw_int;
        TextView textView;
        public void setParams(Context context, int delayMs, boolean doCalibrate, TextView textView) {
            this.context = context;
            this.delayMs = delayMs;
            this.doCalibrate = doCalibrate;
            this.textView = textView;
        }

        @Override
        protected void onPreExecute() {
            textView.setText(R.string.processing);
            super.onPreExecute();
        }

        @Override
        protected Integer doInBackground(Void... voids) {
            if (delayMs > 0) {
                try {
                    Thread.sleep(delayMs);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            int calibrateRet = 0, ret = 0;
            if (doCalibrate) {
                byte [] out_buf = new byte[1024];
                int [] out_size = new int[1];
                out_size[0] = 1024;
                calibrateRet = mLib.extraApi(FpResDef.PID_7XX_INLINETOOL, FpResDef.FP_INLINE_7XX_SNR_INIT, null, null, null);
                calibrateRet = mLib.extraApi(FpResDef.PID_7XX_INLINETOOL, FpResDef.FP_INLINE_7XX_SNR_WKBOX_ON, null, null, null);
                mLib.extraApi(FpResDef.PID_7XX_INLINETOOL, FpResDef.FP_INLINE_7XX_SNR_GET_DATA, null, out_buf, out_size);
                mLib.extraApi(FpResDef.PID_7XX_INLINETOOL, FpResDef.FP_INLINE_7XX_CASE_UNINIT, null, null, null);
                if (calibrateRet != 0) {
                    EgisLog.e(TAG, String.format("FP_INLINE_7XX_SNR_WKBOX_ON failed ret=%d", calibrateRet));
                }
                ByteBuffer bb = ByteBuffer.wrap(out_buf, 0, 1024);
                bb.order(ByteOrder.LITTLE_ENDIAN);
                expo = bb.getFloat();
                hw_int = bb.getInt();
                EgisLog.d(TAG, "expo = " + expo + " hw_int = " + hw_int);

            }


            return calibrateRet != 0 ? calibrateRet : ret;
        }

        @Override
        protected void onPostExecute(Integer ret) {
            EgisLog.d(TAG, String.format("CalibrateAyncTask onPostExecute ret=%d", ret));
            if (ret != 0) {
                EgisLog.e(TAG, String.format("CalibrateAyncTask ret=%d", ret));
            }
            if (context != null) {
                if (ret != 0) {
                    Toast.makeText(context, R.string.failed, Toast.LENGTH_SHORT).show();
                    textView.setText(FPUtil.getColorStringBuilder("Failed", Color.RED));
                } else {
                    textView.setText(FPUtil.getColorStringBuilder("Success ", Color.BLUE));
                    textView.append("  expo:" + String.format("%.1f", expo) + ", hw_int: " + hw_int);
                    mBinding.buttonApplyAutoCalibrationResult.setVisibility(View.VISIBLE);
                }
            }
        }
    };

    private void setKeyboardVisibilityListener(final OnKeyboardVisibilityListener onKeyboardVisibilityListener) {
        final View parentView = ((ViewGroup) findViewById(android.R.id.content)).getChildAt(0);
        parentView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {

            private boolean alreadyOpen;
            private final int defaultKeyboardHeightDP = 100;
            private final int EstimatedKeyboardDP = defaultKeyboardHeightDP + (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP ? 48 : 0);
            private final Rect rect = new Rect();

            @Override
            public void onGlobalLayout() {
                int estimatedKeyboardHeight = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, EstimatedKeyboardDP, parentView.getResources().getDisplayMetrics());
                parentView.getWindowVisibleDisplayFrame(rect);
                int heightDiff = parentView.getRootView().getHeight() - (rect.bottom - rect.top);
                boolean isShown = heightDiff >= estimatedKeyboardHeight;

                if (isShown == alreadyOpen) {
                    Log.i("Keyboard state", "Ignoring global layout change...");
                    return;
                }
                alreadyOpen = isShown;
                onKeyboardVisibilityListener.onKBVisibilityChanged(isShown);
            }
        });
    }
}



