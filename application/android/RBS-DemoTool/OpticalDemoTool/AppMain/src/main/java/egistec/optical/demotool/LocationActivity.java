package egistec.optical.demotool;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
//import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;

import java.nio.ByteBuffer;
import java.util.ArrayList;

import egistec.optical.demotool.experiment.ExperimentDef;
import egistec.optical.demotool.experiment.FileIoUtils;
import egistec.optical.demotool.lib.LiveImageInfo;
import egistec.optical.demotool.lib.LocationTuner;
import egistec.optical.demotool.otg.manager.OtgSensorManager;
import rbs.egistec.com.fplib.api.FingerprintReceiver;
import rbs.egistec.com.fplib.api.FpResDef;
import rbs.egistec.com.fplib.api.RbsLib;
import rbs.egistec.com.fplib.api.EgisLog;

import static android.view.View.VISIBLE;
import egistec.optical.demotool.lib.ImageProcess;
import static egistec.optical.demotool.otg.OtgDef.OTG_SENSOR_ID;

public class LocationActivity extends AppCompatActivity {
    private static final String TAG = "RBS-SensorLocate";
    boolean isGetimage = false;
    int g_width;
    int g_height;
    byte[] g_img;
    int g_raw_width;
    int g_raw_height;
    byte[] g_raw_img;

    private OtgSensorManager mOtgSensorManager = null;
    private LocationTuner mLocationTuner;
    private RbsLib mLib;
   	private TextView mLogView;
    private BarChart mHisImage;
    private Button mFindIconIn;
    private Boolean mFindIconInStart = false;
    private Button mFindSensorCenter;
    private Boolean mFindCenterStart = false;
    private Button mFindSensorBrightness;
    private Boolean mFindBrightnessStart = false;
    TextView mEditTextICwindowX;
    TextView mEditTextICwindowY;
    ImageView gifImageView;
    ImageView mImageView;
    private RelativeLayout mGifContainer;

    private Handler mHandler;

    @SuppressLint({"ClickableViewAccessibility", "HandlerLeak"})
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fp_activity_location);
        mLib = RbsLib.getInstance(this);
        mLogView = (TextView) findViewById(R.id.logView);
        mHisImage = (BarChart) findViewById(R.id.histogram_image);
        mEditTextICwindowX = (TextView)findViewById(R.id.editICWindowX);
        mEditTextICwindowY = (TextView)findViewById(R.id.editICWindowY);
        gifImageView = (ImageView)findViewById(R.id.enroll_gif);
        mImageView = (ImageView)findViewById(R.id.imageView);

        mOtgSensorManager = OtgSensorManager.getInstance(OTG_SENSOR_ID, this);

        histogram_init();

        FileIoUtils.WindowPos dotPos = FileIoUtils.loadIcWindowLayoutData();
        final int currentX = dotPos.x;
        final int currentY = dotPos.y;
        EgisLog.d(TAG,
                String.format(
                        "current light spot currentX=%d currentY=%d", currentX, currentY));

        final float density = getResources().getDisplayMetrics().density;
        final RelativeLayout ll = (RelativeLayout) findViewById(R.id.main_layout);
        ViewTreeObserver vto = ll.getViewTreeObserver();
        vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                ll.getViewTreeObserver().removeOnGlobalLayoutListener(this);

                int width = Math.round(ll.getWidth() / density);
                int height = Math.round(ll.getHeight() / density);
                EgisLog.d(TAG,
                        String.format("window size is w = %d , h = %d", width, height));
                mLocationTuner =
                        new LocationTuner(mHandler, currentX, currentY, width, height);
            }
        });

        mFindIconIn = (Button) findViewById(R.id.buttonFindIconIn);
        mFindIconIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mFindIconInStart = !mFindIconInStart;
                if (mFindIconInStart) {
                    mFindIconIn.setText("Find Icon In Stop");
                    mLocationTuner.moveToIconInSensorStart();
                } else {
                    mFindIconIn.setText("Find Icon In Start");
                    mLocationTuner.stop();
                }
            }
        });

        mFindSensorCenter = (Button) findViewById(R.id.buttonFindCenter);
        mFindSensorCenter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mFindCenterStart = !mFindCenterStart;
                if (mFindCenterStart) {
                    mFindSensorCenter.setText("Find Center Stop");
                    mLocationTuner.moveToCenterSpotStart();
                } else {
                    mFindSensorCenter.setText("Find Center Start");
                    mLocationTuner.stop();
                }
            }
        });

        mFindSensorBrightness = (Button) findViewById(R.id.buttonFindBrightness);
        mFindSensorBrightness.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mFindBrightnessStart = !mFindBrightnessStart;
                if (mFindBrightnessStart) {
                    mFindSensorBrightness.setText("Find Brightness Stop");
                    mLocationTuner.moveToBrightnessSpotStart();
                } else {
                    mFindSensorBrightness.setText("Find Brightness Start");
                    mLocationTuner.stop();
                }
            }
        });

        mHandler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                switch (msg.what) {
                    case LocationTuner.MSG_DO_FETCH_IMAGE_8BIT: {
                        GetImage();
                        int width = g_width;
                        int height = g_height;
                        byte[] rawData = g_img;
                        byte[] rawImage = new byte[rawData.length];
//                        byte[] capImage = new byte[rawData.length / 2];
                        System.arraycopy(rawData, 0, rawImage, 0, rawImage.length);
//                        System.arraycopy(
//                                rawData, rawImage.length, capImage, 0, capImage.length);

                        mLocationTuner.setImage(LocationTuner.ImageType.EIGHT_BIT,
                                rawImage, width, height);
                        mLocationTuner.imageReady();
                        Bitmap rotatedBmp = ImageProcess.rawToBmpRotate(rawImage, width, height, 90, Bitmap.Config.ALPHA_8);
                        ByteBuffer buf =
                                ByteBuffer.allocate(rotatedBmp.getByteCount());
                        rotatedBmp.copyPixelsToBuffer(buf);
                        byte[] rotateImg = buf.array();
                        width = rotatedBmp.getWidth();
                        height = rotatedBmp.getHeight();
                        mImageView.setImageBitmap(ImageProcess.rawToBmp(rotateImg, width, height));
                        updateBarChart(width, height, rotateImg);

//                        rotatedBmp = rawToBmpRotate(capImage, width, height, 90, Bitmap.Config.ALPHA_8);
//                        buf = ByteBuffer.allocate(rotatedBmp.getByteCount());
//                        rotatedBmp.copyPixelsToBuffer(buf);
//                        rotateImg = buf.array();

                        //                    mSubImage.setImageBitmap(
                        //                            rawToBmp(rotateImg, width,
                        //                            height));
                    } break;
                    case LocationTuner.MSG_DO_FETCH_IMAGE_16BIT: {
                        GetImage();
                        int width = g_raw_width;
                        int height = g_raw_height;
                        byte[] rawImage = new byte[width * height * (Short.SIZE/Byte.SIZE)];
                        byte[] ippImage = new byte[width * height];
                        ByteBuffer rawBb = ByteBuffer.wrap(g_raw_img);
                        rawBb.get(rawImage, 0, width * height * (Short.SIZE/Byte.SIZE));

                        mLocationTuner.setImage(LocationTuner.ImageType.SIXTEEN_BIT,
                                rawImage, width, height);
                        mLocationTuner.imageReady();

//                        rawBb.get(ippImage, 0, width * height);
//                        Bitmap rotatedBmp = rawToBmpRotate(ippImage, width, height, 90, Bitmap.Config.ALPHA_8);
//                        ByteBuffer buf =
//                                ByteBuffer.allocate(rotatedBmp.getByteCount());
//                        rotatedBmp.copyPixelsToBuffer(buf);
//                        byte[] rotateImg = buf.array();
//                        width = rotatedBmp.getWidth();
//                        height = rotatedBmp.getHeight();

                        //                    mSubImage.setImageBitmap(
                        //                            rawToBmp(rotateImg, width,
                        //                            height));
                    } break;
                    case LocationTuner.MSG_DO_ADJUST_COORDINATE: {
                        int x = msg.arg1;
                        int y = msg.arg2;
                        mEditTextICwindowX.setText(String.valueOf(x));
                        mEditTextICwindowY.setText(String.valueOf(y));
                        setMargin(x, y);
                    } break;
                    case LocationTuner.MSG_DO_CHANGE_LIGHT_SPOT_WHITE: {
                        gifImageView.setImageResource(R.drawable.press);
                    } break;
                    case LocationTuner.MSG_DO_CHANGE_LIGHT_SPOT_ALLBLACK: {
                        gifImageView.setImageResource(R.drawable.press_all_black_1p);
                    } break;
                    case LocationTuner.MSG_DO_CHANGE_LIGHT_SPOT_GRADIENT: {
                        gifImageView.setImageResource(R.drawable.press_gradient);
                    } break;
                    case LocationTuner.MSG_STATUS_LOG: {
                        String statusMsg = (String) msg.obj;
                        mLogView.setText("");
                        mLogView.append(statusMsg);

                    } break;
                    case LocationTuner.MSG_TUNING_COMPLETE: {
                        int x = msg.arg1;
                        int y = msg.arg2;
                        mLogView.setText("");
                        mLogView.append("tune sensor position complete:");
                        mLogView.append("\n");
                        mLogView.append("X:" + x + "\n");
                        mLogView.append("Y:" + y);
                        mFindIconIn.setText("Find Icon In Start");
                        mFindSensorCenter.setText("Find Center Start");
                        mFindSensorBrightness.setText("Find Brightness Start");
                        mEditTextICwindowX.setText(String.valueOf(x));
                        mEditTextICwindowY.setText(String.valueOf(y));
                        savePosition(x, y);
                    } break;
                    default:
                        break;
                }
            }
        };
        mHandler.obtainMessage(LocationTuner.MSG_DO_ADJUST_COORDINATE, currentX, currentY).sendToTarget();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mLib != null) {
            mLib.startListening(mFingerprintReceiver);
        }
        mLib.extraApi(ExperimentDef.PID_BKG_IMG, ExperimentDef.CMD_DEMOTOOL_SENSOR_POWER_ON, null, null, null);
    }

    private void histogram_init() {
        ArrayList<BarEntry> entries = new ArrayList<>();
        for (int i = 0; i < 255; i++) entries.add(new BarEntry(i, i));
        BarDataSet barDataSet1 = new BarDataSet(entries, "Intensity");
        BarData barData1 = new BarData(barDataSet1);
        mHisImage.setData(barData1);
    }

    private FingerprintReceiver mFingerprintReceiver = new FingerprintReceiver(){

        @SuppressLint("LongLogTag")
        @Override
        public void onFingerprintEvent(int eventId, int value1, int value2, Object eventObj) {
            EgisLog.d(TAG, "onFingerprintEvent eventId = " + eventId + " value1 = " + value1 + " value2 = " + value2);

            switch(eventId){
                case FpResDef.EVENT_RETURN_LIVE_IMAGE_OUT:
                    EgisLog.d(TAG, "+++++ RETURN_IMAGE +++++");
                    if (eventObj != null) {

                        LiveImageInfo imageinfo = new LiveImageInfo(eventObj);
                        g_width = imageinfo.img_width;
                        g_height = imageinfo.img_height;
                        g_raw_width = imageinfo.raw_width;
                        g_raw_height = imageinfo.raw_height;
                        g_img = imageinfo.getImage();
                        g_raw_img = imageinfo.getRawImage(false);
                        ImageProcess.showImage(mImageView, g_width, g_height, g_img);
                        isGetimage = true;
                    } else {
                        EgisLog.e(TAG, "eventObj is null");
                    }
                    break;
                case FpResDef.EVENT_OTG_WRITE_REGISTER:
                case FpResDef.EVENT_OTG_READ_REGISTER:
                case FpResDef.EVENT_OTG_GET_FRAME:
                case FpResDef.EVENT_OTG_WAKE_UP:
                case FpResDef.EVENT_OTG_STANDBY:
                case FpResDef.EVENT_OTG_SPI_WRITE_READ:
                case FpResDef.EVENT_OTG_SET_SPI_CLK:
                    if (mOtgSensorManager != null) {
                        mOtgSensorManager.nativeCallbackHandler(eventId, value1, value2, (byte[])eventObj);
                    }
                    break;
                default:
                    break;
            }
        }
    };

    private void updateBarChart(int width, int height, byte[] image) {
        EgisLog.d(TAG, "updateBarChart in");
        ArrayList<BarEntry> entries = new ArrayList<>();
        BarChart barChart;
        BarDataSet barDataSet;

        int[] histogram = new int[256];
        int[] data = new int[width * height];
        int[] barChartData;

        for (int i = 0; i < image.length; i++) {
            data[i] = (~image[i]) & 0xFF;
        }
        EgisLog.d(TAG, "image size= " + data.length + "  data 1 = " + data[1]);

        barChartData = data;

        for (int i = 0; i < barChartData.length; i++)
            histogram[Math.abs(barChartData[i])]++;

        for (int i = 0; i < 255; i++) entries.add(new BarEntry(i, histogram[i]));

        barChart = mHisImage;
        if (barChart.getVisibility() == VISIBLE) {
            for (int i = 0; i < barChartData.length; i++)
                histogram[Math.abs(barChartData[i])]++;

            for (int i = 0; i < 255; i++) entries.add(new BarEntry(i, histogram[i]));

            barDataSet = (BarDataSet) barChart.getData().getDataSetByIndex(0);
            barDataSet.setValues(entries);
            barChart.getData().notifyDataChanged();
            barChart.notifyDataSetChanged();
            barChart.invalidate();
        }
    }

    void setMargin(int left_dp, int top_dp) {
        if (mGifContainer == null) {
            mGifContainer = (RelativeLayout) findViewById(R.id.gif_container);
        }
        RelativeLayout.LayoutParams params =
                (RelativeLayout.LayoutParams) mGifContainer.getLayoutParams();
        int left_Pixel = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,
                left_dp, getResources().getDisplayMetrics());
        int top_Pixel = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, top_dp,
                getResources().getDisplayMetrics());
        params.setMarginStart(left_Pixel);
        params.setMargins(left_Pixel, top_Pixel, 0, 0);
        mGifContainer.setLayoutParams(params);
    }

    void savePosition(int x, int y) {
        EgisLog.d(TAG, "++++++ savePosition +++++ BT_address = " + x + "   " + y);
        FileIoUtils.saveIcWindowLayoutData(x, y);
    }

    private void GetImage() {
        byte[] img = new byte[255*255*10];
        int[] img_length = new int[1];
        img_length[0] = img.length;
        mLib.extraApi(ExperimentDef.PID_BKG_IMG, ExperimentDef.CMD_BKG_IMG_RETURN_IMAGE, null, img, img_length);

        LiveImageInfo imageinfo = new LiveImageInfo(img);
        g_width = imageinfo.img_width;
        g_height = imageinfo.img_height;
        g_raw_width = imageinfo.raw_width;
        g_raw_height = imageinfo.raw_height;
        g_img = imageinfo.getImage();
        g_raw_img = imageinfo.getRawImage(false);
        ImageProcess.showImage(mImageView, g_width, g_height, g_img);
    }

}
