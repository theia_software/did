package egistec.optical.demotool;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.preference.EditTextPreference;
import android.preference.ListPreference;
import android.preference.Preference;
import android.app.ActionBar;
import android.preference.PreferenceCategory;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.preference.PreferenceScreen;
import android.preference.RingtonePreference;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
//import android.util.Log;
import android.util.Log;
import android.view.MenuItem;

import java.nio.charset.StandardCharsets;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import egistec.optical.demotool.igistec.AuoOledFingerprintReceiverHelper;
import egistec.optical.demotool.lib.FPUtil;
import rbs.egistec.com.fplib.api.ExtraApiDef;
import rbs.egistec.com.fplib.api.FpResDef;
import rbs.egistec.com.fplib.api.RbsLib;
import rbs.egistec.com.fplib.api.EgisLog;


public class SettingsActivity extends AppCompatActivity {
    final static String TAG = SettingsActivity.class.getSimpleName();
    public final static String EXTRA_INI_SETTINGS = "EXTRA_INI_SETTINGS";
    private static RbsLib mLib;

    private static boolean hasChanged = false;
    private static boolean hasUnknownValue = false;
    /**
     * A preference value change listener that updates the preference's summary
     * to reflect its new value.
     */
    private static Preference.OnPreferenceChangeListener sBindPreferenceSummaryToValueListener = new Preference.OnPreferenceChangeListener() {
        @Override
        public boolean onPreferenceChange(Preference preference, Object value) {
            String stringValue = value.toString();

            if (preference instanceof ListPreference) {
                // For list preferences, look up the correct display value in
                // the preference's 'entries' list.
                ListPreference listPreference = (ListPreference) preference;
                int index = listPreference.findIndexOfValue(stringValue);

                // Set the summary to reflect the new value.
                preference.setSummary(
                        index >= 0
                                ? listPreference.getEntries()[index]
                                : null);

            } else if (preference instanceof RingtonePreference) {
                // For ringtone preferences, look up the correct display value
                // using RingtoneManager.
                if (TextUtils.isEmpty(stringValue)) {
                    // Empty values correspond to 'silent' (no ringtone).
                    preference.setSummary(R.string.pref_ringtone_silent);

                } else {
                    Ringtone ringtone = RingtoneManager.getRingtone(
                            preference.getContext(), Uri.parse(stringValue));

                    if (ringtone == null) {
                        // Clear the summary if there was a lookup error.
                        preference.setSummary(null);
                    } else {
                        // Set the summary to reflect the new ringtone display
                        // name.
                        String name = ringtone.getTitle(preference.getContext());
                        preference.setSummary(name);
                    }
                }

            } else {
                // For all other preferences, set the summary to the value's
                // simple string representation.
                preference.setSummary(stringValue);
            }
            hasChanged = true;
            return true;
        }
    };

    /**
     * Helper method to determine if the device has an extra-large screen. For
     * example, 10" tablets are extra-large.
     */
    private static boolean isXLargeTablet(Context context) {
        return (context.getResources().getConfiguration().screenLayout
                & Configuration.SCREENLAYOUT_SIZE_MASK) >= Configuration.SCREENLAYOUT_SIZE_XLARGE;
    }

    /**
     * Binds a preference's summary to its value. More specifically, when the
     * preference's value is changed, its summary (line of text below the
     * preference title) is updated to reflect the value. The summary is also
     * immediately updated upon calling this method. The exact display format is
     * dependent on the type of preference.
     *
     * @see #sBindPreferenceSummaryToValueListener
     */
    private static void bindPreferenceSummaryToValue(PreferenceFragment fragment, CharSequence key) {
        Preference preference = fragment.findPreference(key);
        if (preference == null) {
            EgisLog.e(TAG, "can not find the key " + key);
            return;
        }
        // Set the listener to watch for value changes.
        preference.setOnPreferenceChangeListener(sBindPreferenceSummaryToValueListener);

        // Trigger the listener immediately with the preference's
        // current value.
        sBindPreferenceSummaryToValueListener.onPreferenceChange(preference,
                PreferenceManager
                        .getDefaultSharedPreferences(preference.getContext())
                        .getString(preference.getKey(), ""));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setupActionBar();
        getFragmentManager().beginTransaction().replace(android.R.id.content, new GeneralPreferenceFragment(), "PreferenceFragment").commit();
        mLib = RbsLib.getInstance(this);
    }

    /**
     * Set up the {@link android.app.ActionBar}, if the API is available.
     */
    private void setupActionBar() {
        ActionBar actionBar = getActionBar();
        if (actionBar != null) {
            // Show the Up button in the action bar.
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    @Override
    public void onBackPressed() {
        if (!hasChanged && !hasUnknownValue) {
            EgisLog.d(TAG, "preferences has no change");
            super.onBackPressed();
            return;
        }
        EgisLog.d(TAG, "onBackPress hasChanged="+hasChanged+ " hasUnknownValue="+hasUnknownValue);
        Fragment fragment = getFragmentManager().findFragmentByTag("PreferenceFragment");
        if (fragment != null && fragment instanceof PreferenceFragment) {
            SharedPreferences prefs = ((PreferenceFragment) fragment).getPreferenceScreen().getSharedPreferences();
            String iniSettings = FPUtil.SharePreferenceToIniSettings(prefs);
            if (hasUnknownValue) {
                iniSettings = iniSettings.replace("(default)", "");
            }
            Intent i = new Intent();
            i.putExtra(SettingsActivity.EXTRA_INI_SETTINGS, iniSettings);
            setResult(Activity.RESULT_OK, i);
            hasUnknownValue = true;
            finish();
        }
        super.onBackPressed();
    }

    /**
     * This method stops fragment injection in malicious applications.
     * Make sure to deny any unknown fragments here.
     */
    protected boolean isValidFragment(String fragmentName) {
        return PreferenceFragment.class.getName().equals(fragmentName)
                || GeneralPreferenceFragment.class.getName().equals(fragmentName);
    }

    /**
     * This fragment shows general preferences only. It is used when the
     * activity is showing a two-pane settings UI.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public static class GeneralPreferenceFragment extends PreferenceFragment {
        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            addPreferencesFromResource(R.xml.pref_general);
            setHasOptionsMenu(true);
            Preference preference;
            ListPreference listpreference;
            SharedPreferences.Editor editor;

            // Bind the summaries of EditText/List/Dialog/Ringtone preferences
            // to their values. When their values change, their summaries are
            // updated to reflect the new value, per the Android Design
            // guidelines.

            getConfigWriteToPreference(FpResDef.INI_SECTION_SENSOR, IniSettings.getIniItem(IniSettings.INI_ITEM_ID_SENSOR));
            getConfigWriteToPreference(FpResDef.INI_SECTION_ENROLL, IniSettings.getIniItem(IniSettings.INI_ITEM_ID_ENROLL));
            getConfigWriteToPreference(FpResDef.INI_SECTION_VERIFY, IniSettings.getIniItem(IniSettings.INI_ITEM_ID_VERIFY));
            getConfigWriteToPreference(FpResDef.INI_SECTION_CALIBRATE, IniSettings.getIniItem(IniSettings.INI_ITEM_ID_CALIBRATE));

            bindPreferenceSummaryToValue(this, "et0xx:DB_PATH");
            bindPreferenceSummaryToValue(this, "et0xx:ENROLL_FINGER_INDEX");
            bindPreferenceSummaryToValue(this, "et0xx:VERIFY_FINGER_INDEX");

            PreferenceScreen screen = getPreferenceScreen();

            PreferenceCategory mSensorCategory = (PreferenceCategory) findPreference("sensor_settings");

            PreferenceCategory mEt0xxCategory = (PreferenceCategory) findPreference("et0xx_settings");
            screen.removePreference(mEt0xxCategory);

            preference = findPreference("egis:INI_VERSION");
            editor = preference.getSharedPreferences().edit();
            editor.putString(preference.getKey(), String.valueOf(FpResDef.INI_VERSION_FLAG));
            editor.commit();
            bindPreferenceSummaryToValue(this, preference.getKey());


            String appendString = "Egis QA ver" ;
            if (AuoOledFingerprintReceiverHelper.G_DEMO_INC_DEFINE_IN_MK) {
                appendString = "Google UI" ;
            }
//            if ( AuoOledFingerprintReceiverHelper.FULL_AREA_MODE_ENABLE) {
//                appendString += System.lineSeparator() + "Auo Full Sensor Mode";
//            }
//            else {
            appendString += System.lineSeparator() + "Single Area";
//            }
            preference = findPreference("egis:APK_VERSION");
            preference.setSummary(BuildConfig.VERSION_NAME + System.lineSeparator() + appendString);
            

            String algo_version = getVersionFromSdk(ExtraApiDef.CMD_VERSION_ALGO);

            preference = findPreference("egis:ALGO_VERSION");
            preference.setSummary(algo_version);

            String ip_version = getVersionFromSdk(ExtraApiDef.CMD_VERSION_IP);

            preference = findPreference("egis:IP_VERSION");
            preference.setSummary(ip_version);

            hasChanged = false;
        }

        private String getVersionFromSdk(int versionType) {
            String defaultVersion = "unknown version";

            byte[] testResultBuf = new byte[100];
            int[] testResultLenth = new int[2];
            testResultLenth[0] = testResultBuf.length;
            int retval = mLib.extraApi(ExtraApiDef.PID_DEMOTOOL, versionType, null, testResultBuf, testResultLenth);
            byte[] outData = new byte[testResultLenth[0]];
            System.arraycopy(testResultBuf, 0, outData, 0, testResultLenth[0]);
            String outInfo = null;
            if (retval != FpResDef.RESULT_OK) {
                outInfo = defaultVersion;
            } else {
                outInfo = new String(outData);
            }
            return outInfo;
        }

        private void getConfigWriteToPreference(String iniSectionName, String[][] KeyName_array) {
            EgisLog.d(TAG, "====Get ini config==== " + iniSectionName);

            byte[] ini_buf = new byte[FpResDef.INI_CONFING_FILE_MAX_SIZE];
            int[] ini_buf_size = new int[1];

            int retval = mLib.extraApi(ExtraApiDef.PID_COMMAND, FpResDef.CMD_GET_CONFIG, null, ini_buf, ini_buf_size);
            if (retval != FpResDef.RESULT_OK) {
                EgisLog.e(TAG, "Get ini config fail!!!");
                return;
            }
            String ini_str = new String(ini_buf, StandardCharsets.UTF_8);

            int ini_item_pos;
            int next_line_pos;
            String item_value = "?";

            for(String[] item : KeyName_array) {
                Pattern pt = Pattern.compile(item[0] + ".*=.*");
                Matcher mt = pt.matcher(ini_str);
                boolean is_found = false;
                if(mt.find())
                {
                    is_found = true;
                    EgisLog.d(TAG, mt.group(0));
                    item_value = mt.group(0).split("=[:space:]*")[1];
                }
                else {
                    is_found = false;
                    hasUnknownValue = true;
                }

                String preferenceKey = iniSectionName + ":" + item[0];
                Preference preference = findPreference(preferenceKey);
                if (preference == null) {
                    EgisLog.e(TAG, "bad (section:key) " + preferenceKey);
                }

                switch (preference.getClass().getSimpleName()){
                    case "EditTextPreference":
                        if (!is_found) {
                            item_value = "(default)" + item[1];
                        }
                        EditTextPreference editTextPreference = (EditTextPreference)preference;
                        editTextPreference.setText(item_value);
                        break;
                    case "ListPreference":
                        if (!is_found) {
                            item_value = item[1];
                        }
                        ListPreference listpreference = (ListPreference) preference;
                        int list_index = listpreference.findIndexOfValue(item_value);
                        if (list_index != -1)
                            listpreference.setValueIndex(list_index);
                        else
                            listpreference.setValueIndex(listpreference.getEntryValues().length - 1);
                        break;
                }

                bindPreferenceSummaryToValue(this, preferenceKey);
            }
        }

        @Override
        public boolean onOptionsItemSelected(MenuItem item) {
            int id = item.getItemId();
            if (id == android.R.id.home) {
                getActivity().onBackPressed();
                return true;
            }
            return super.onOptionsItemSelected(item);
        }
    }
}
