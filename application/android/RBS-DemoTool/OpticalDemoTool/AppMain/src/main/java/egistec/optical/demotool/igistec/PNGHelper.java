package egistec.optical.demotool.igistec;

import android.graphics.Bitmap;
import android.os.Environment;
import android.util.Log;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import ar.com.hjg.pngj.ImageInfo;
import ar.com.hjg.pngj.ImageLineInt;
import ar.com.hjg.pngj.PngReaderInt;
import ar.com.hjg.pngj.PngWriter;
import ar.com.hjg.pngj.chunks.PngChunkTextVar;


public class PNGHelper {


    static public String TAG = "PNGHelper" ;
    static public String ROOT_PATH = Environment.DIRECTORY_DOWNLOADS;
    static public File ROOT_DIR = Environment.getExternalStoragePublicDirectory(ROOT_PATH);

    public static void testWritePNG() {


        int [][] frame = new int[210][105] ;
        int sum = 0 ;
        for ( int i = 0 ; i < 210 ; i++ ) {
            for ( int j = 0 ; j < 105 ; j++ ) {
                frame[i][j] = (i * j * 3) ;
                sum = sum + frame[i][j] ;
            }
        }

        savePNG("test.png", frame, "ASDF 1234") ;
        int [][] testRead = readPNG("test.png") ;

        savePNG("copy.png", testRead, "ABCDEFGT ") ;

        savePNG("copy2.png", testRead) ;
        //Log.e(TAG, "Get SUm=" + Common.getSum(frame) + "," + Common.getSum(testRead)) ;

    }


    /**
     * read 16bit gray png to int [][]
     * no metadata
     * @param path
     * @return
     */
    public static int[][] readPNG(String path) {

        try {
            Log.i(TAG, "Read PNG file:" + path) ;
            File pngFile = new File(ROOT_DIR, path) ;
            InputStream is = new FileInputStream(pngFile) ;
            PngReaderInt reader = new PngReaderInt(is) ;

            //validateImageType(reader);
            int[][] pixels = new int[reader.imgInfo.rows][reader.imgInfo.cols];
            int rowNumber = 0;
            while (reader.hasMoreRows()) {
                ImageLineInt row = reader.readRowInt();
                int[] rowValues = row.getScanline();
                //System.arraycopy(rowValues,0,pixels[rowNumber],);
                //System.arraycopy(rowValues, 0, pixels[rowNumber], 0, rowValues.length);
                for ( int h = 0 ; h < reader.imgInfo.cols; h++ ) {
                    pixels[rowNumber][h] = rowValues[h] ;
                }
                rowNumber++;
            }
            reader.close();

            return pixels;
        }
        catch (Exception ex) {
            Log.e(TAG, "Read PNG " + path + " Fail:" + ex.getMessage()) ;
        }
        return null ;
    }


    public static boolean savePNG(String path, int[][] pixelValues) {
        return savePNG(path, pixelValues, null) ;
    }



    /**
     * 如果平均值小於256，假設它是8BTI的
     * @param frame
     */
    static public boolean is8Bit(int [][] frame) {

        long sum = 0 ;
        int cnt = 0 ;
        for ( int w = 0 ; w < frame.length ; w++ ) {
            for ( int h = 0 ; h < frame[0].length ; h ++ ) {
                if ( frame[w][h] >= 0 ) {
                    cnt ++ ;
                    sum = sum + frame[w][h] ;
                }
            }
        }
        long avg =  (sum / cnt) ;
        if ( avg <= 256 ) {
            // 應該是8 BIT啦
            return true ;
        }
        return false ;

    }

    public static boolean savePNG(String filename, Bitmap b) {

        File pngFile = new File(ROOT_DIR, filename) ;
        try (FileOutputStream out = new FileOutputStream(pngFile)) {
            b.compress(Bitmap.CompressFormat.PNG, 100, out); // bmp is your Bitmap instance
            out.flush();
            out.close();
            return true ;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false ;
    }

    /**
     * int [][] to png with metadata
     */
    public static boolean savePNG(String path, int[][] pixelValues, String metadata) {

        try {
            Log.i(TAG, "Write PNG file:" + path) ;
            int tileWidth = pixelValues[0].length; // 105
            int tileHeight  = pixelValues.length; // 210

            //Log.e(TAG, "tile h/w = " + tileHeight + "," + tileWidth) ;

            //ElevationPngImage image = createImage(tileWidth, tileHeight);

            int bitNum = 16 ;
//            if ( is8Bit(pixelValues )){
//                bitNum = 8 ;
//            }
            ImageInfo info = new ImageInfo(pixelValues[0].length, pixelValues.length,bitNum,false,true,false) ;

            File f = new File(ROOT_DIR,path) ;
            PngWriter writer = new PngWriter(f, info);


            for (int y = 0; y < tileHeight; y++) {
                ImageLineInt row = new ImageLineInt(writer.imgInfo, new int[tileWidth]);
                int[] rowLine = row.getScanline();
                for (int x = 0; x < tileWidth; x++) {
                    int pixelValue = pixelValues[y][x];
                    rowLine[x] = pixelValue;
                }
                writer.writeRow(row);
            }

            if ( metadata != null ) {
                //writer.getMetadata().setText("text", metadata);
                //writer.getMetadata().setText(PngChunkTextVar.KEY_Comment, "just a text") ;
                writer.getMetadata().setText(PngChunkTextVar.KEY_Description, metadata) ;

            }
            writer.getMetadata().setText(PngChunkTextVar.KEY_Author, "YouGangKuo") ;
            writer.getMetadata().setText(PngChunkTextVar.KEY_Copyright, "IgisTec") ;
            writer.getMetadata().setText(PngChunkTextVar.KEY_Title, "Et760Test") ;
            writer.end();
            writer.close();

            //Log.e(TAG, "PNG FIle len=" + f.length()) ;
            return true;
        }
        catch (Exception ex) {
            Log.e(TAG, "PNG Write Error:" + ex.getMessage()) ;
        }
        return false ;
    }



    /**
     * int [][] to png with metadata
     */
    public static boolean savePNG_RbsG5Temp(String path, int[][] pixelValues) {

        try {
            Log.i(TAG, "Write PNG file:" + path) ;
            int tileWidth = pixelValues[0].length; // 105
            int tileHeight  = pixelValues.length; // 210

            //Log.e(TAG, "tile h/w = " + tileHeight + "," + tileWidth) ;

            //ElevationPngImage image = createImage(tileWidth, tileHeight);

            int bitNum = 16 ;
//            if ( is8Bit(pixelValues )){
//                bitNum = 8 ;
//            }
            ImageInfo info = new ImageInfo(pixelValues[0].length, pixelValues.length,bitNum,false,true,false) ;

            File f = new File("/sdcard/RbsG5Temp/", path) ;
            PngWriter writer = new PngWriter(f, info);


            for (int y = 0; y < tileHeight; y++) {
                ImageLineInt row = new ImageLineInt(writer.imgInfo, new int[tileWidth]);
                int[] rowLine = row.getScanline();
                for (int x = 0; x < tileWidth; x++) {
                    int pixelValue = pixelValues[y][x];
                    rowLine[x] = pixelValue;
                }
                writer.writeRow(row);
            }

            writer.getMetadata().setText(PngChunkTextVar.KEY_Author, "YouGangKuo") ;
            writer.getMetadata().setText(PngChunkTextVar.KEY_Copyright, "IgisTec") ;
            writer.getMetadata().setText(PngChunkTextVar.KEY_Title, "Et760Test") ;
            writer.end();
            writer.close();

            //Log.e(TAG, "PNG FIle len=" + f.length()) ;
            return true;
        }
        catch (Exception ex) {
            Log.e(TAG, "PNG Write Error:" + ex.getMessage()) ;
        }
        return false ;
    }

}
