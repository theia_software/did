package egistec.optical.demotool;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
//import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import egistec.optical.demotool.experiment.ExperimentSettingValueDB;
import egistec.optical.demotool.experiment.ExperimentUtils;
import egistec.optical.demotool.experiment.FileIoUtils;
import rbs.egistec.com.fplib.api.EgisLog;

public class ExperimentSettingsActivity extends AppCompatActivity {

    private final String TAG = "ExperimentSettingsLog";
    String[] press_color_items = new String[]{ "w", "g", "b", "a", "r" };
    private Context mContext = null;
    private TextView mTextViewUserId = null;
    private Spinner mSpinnerPressColor = null;
    private TextView mTextViewExpScaleMin = null;
    private TextView mTextViewExpScaleMax = null;
    private TextView mTextViewPressPosX = null;
    private TextView mTextViewPressPosY = null;
    private TextView mTextViewIntegrationCount = null;
    private TextView mTextViewBrightness = null;
    private RelativeLayout mGifContainer = null;
    private Button mButtonApply = null;
    private ImageView mImageView = null;
    private FileIoUtils.WindowPos icWindowPos = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_experiment_settings);
        mContext = this;
        initializeLayout();
    }

    private void initializeLayout() {
        mTextViewUserId = (TextView) findViewById(R.id.user_id);
        mTextViewUserId.setText(ExperimentSettingValueDB.getUserName(mContext));
        mSpinnerPressColor = (Spinner)findViewById(R.id.press_color);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(mContext, R.layout.exp_spinner_row, press_color_items);
        mSpinnerPressColor.setAdapter(adapter);
        mSpinnerPressColor.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                EgisLog.v(TAG, (String) parent.getItemAtPosition(position));
                ExperimentSettingValueDB.setPressColorId(mContext, position);
                setPressColor();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub
            }
        });

        icWindowPos = FileIoUtils.loadIcWindowLayoutData();
        if (icWindowPos != null) {
            ExperimentSettingValueDB.setPressPosX(mContext, icWindowPos.x);
            ExperimentSettingValueDB.setPressPosY(mContext, icWindowPos.y);
        }
        mSpinnerPressColor.setSelection(ExperimentSettingValueDB.getPressColorId(mContext));
        mTextViewExpScaleMin = (TextView)findViewById(R.id.exp_scale_min);
        mTextViewExpScaleMin.setText(String.valueOf(ExperimentSettingValueDB.getExposureTimeScaleMin(mContext)));
        mTextViewExpScaleMax = (TextView)findViewById(R.id.exp_scale_max);
        mTextViewExpScaleMax.setText(String.valueOf(ExperimentSettingValueDB.getExposureTimeScaleMax(mContext)));
        mTextViewPressPosX = (TextView)findViewById(R.id.x_axis);
        mTextViewPressPosX.setText(String.valueOf(ExperimentSettingValueDB.getPressPosX(mContext)));
        mTextViewPressPosY = (TextView)findViewById(R.id.y_axis);
        mTextViewPressPosY.setText(String.valueOf(ExperimentSettingValueDB.getPressPosY(mContext)));
        mTextViewIntegrationCount = (TextView)findViewById(R.id.integrate_count);
        mTextViewIntegrationCount.setText(String.valueOf(ExperimentSettingValueDB.getIntegrationCount(mContext)));
        mTextViewBrightness= (TextView)findViewById(R.id.brightness);
        mTextViewBrightness.setText(String.valueOf(ExperimentSettingValueDB.getBrightness(mContext)));
        Button mainVerifyTouch = (Button)findViewById(R.id.IC_window_main_verify);
        mGifContainer = (RelativeLayout)findViewById(R.id.gif_container);
        ExperimentUtils.setPressAreaLeftTop(mGifContainer, ExperimentSettingValueDB.getPressPosX(mContext), ExperimentSettingValueDB.getPressPosY(mContext), getResources().getDisplayMetrics());
        ExperimentUtils.setPressAreaWidthHeight(mGifContainer, mainVerifyTouch, 80, 120, getResources().getDisplayMetrics());
        mButtonApply = (Button)findViewById(R.id.button_apply_settings);
        mButtonApply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                applyAllSettings();
            }
        });
        setPressColor();
    }

    private void applyAllSettings() {
        ExperimentSettingValueDB.setUserName(mContext, mTextViewUserId.getText().toString());
        ExperimentSettingValueDB.setExposureTimeScaleMin(mContext, Integer.parseInt(mTextViewExpScaleMin.getText().toString()));
        ExperimentSettingValueDB.setExposureTimeScaleMax(mContext, Integer.parseInt(mTextViewExpScaleMax.getText().toString()));
        ExperimentSettingValueDB.setIntegrationCount(mContext, Integer.parseInt(mTextViewIntegrationCount.getText().toString()));
        ExperimentSettingValueDB.setBrightness(mContext, Integer.parseInt(mTextViewBrightness.getText().toString()));
        ExperimentSettingValueDB.setPressPosX(mContext, Integer.parseInt(mTextViewPressPosX.getText().toString()));
        ExperimentSettingValueDB.setPressPosY(mContext, Integer.parseInt(mTextViewPressPosY.getText().toString()));
        ExperimentUtils.setPressAreaLeftTop(mGifContainer, ExperimentSettingValueDB.getPressPosX(mContext), ExperimentSettingValueDB.getPressPosY(mContext), getResources().getDisplayMetrics());
        FileIoUtils.saveIcWindowLayoutData(ExperimentSettingValueDB.getPressPosX(mContext), ExperimentSettingValueDB.getPressPosY(mContext));
    }


    private void setPressColor() {
        int viewId = ExperimentUtils.getPressViewId(ExperimentSettingValueDB.getPressColorId(mContext));
        if (mImageView != null)
            mImageView.setVisibility(View.INVISIBLE);
        mImageView = (ImageView)findViewById(viewId);
        mImageView.setVisibility(View.VISIBLE);
    }
}
