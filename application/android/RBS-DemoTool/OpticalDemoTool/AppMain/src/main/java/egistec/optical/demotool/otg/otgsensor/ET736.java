package egistec.optical.demotool.otg.otgsensor;

import android.util.Log;
//import java.util.logging.Handler;

import com.felhr.usbserial.UsbSerialDevice;

import egistec.optical.demotool.otg.utility.USBWrapper;

public class ET736 extends SensorBase {
    public static final int SENSOR_WIDTH = 324;
    public static final int SENSOR_HEIGHT = 324;
    public static final int CE_ARDUINO_PIN = 40;
    private static final int IMAGE_HEADER_SIZE = 0;
    private static final int EXPO_STS_BIT = 0;
    private static final int CHIP_RDY_BIT = 4;
    private static final int CHECK_TIMEOUT = 5; // 5s

    private static final String TAG = "OTG_test_ET736";
    private static boolean debug = false;

    static byte[] et736_egis_tag;
    static byte[] command;
    static byte[] buffer;
    private static UsbSerialDevice serialPort;
    private static USBWrapper mUSBWrapper;

    public static int NativeCallback(int eventId, int value1, int value2, byte[] byteBuffer) {
        int ret = 0;

        if (debug) {
            Log.d(
                    TAG,
                    "[NativeCallback], event id="
                            + eventId
                            + " value1 = "
                            + value1
                            + " value2 = "
                            + value2);
            // for (int i = 0; i < byteBuffer.length; i++)
            //    Log.d(TAG, "[NativeCallback] byteBuffer [" + i + "] = " + byteBuffer[i]);
            if (byteBuffer != null)
                Log.d(TAG, "[NativeCallback] byteBuffer.length = " + byteBuffer.length);
        }

        switch (eventId) {
            case SENSOR_CONTROL_WRITE_REGISTER:
            {
                byte address = byteBuffer[0];
                byte val = byteBuffer[1];
                if (debug)
                    Log.d(
                            TAG,
                            "SENSOR_CONTROL_WRITE_REGISTER addredd = "
                                    + address
                                    + " val = "
                                    + val);
                ret = et736_write_register(address, val);
            }
            break;

            case SENSOR_CONTROL_READ_REGISTER:
            {
                byte address = byteBuffer[0];
                byte[] val = new byte[1];
                ret = et736_read_register(address, val);
                if (debug)
                    Log.d(
                            TAG,
                            "SENSOR_CONTROL_READ_REGISTER address = "
                                    + address
                                    + " val = "
                                    + val[0]);
                byteBuffer[0] = val[0];
            }
            break;

            case SENSOR_CONTROL_GET_FRAME:
            {
                int frames = value1;
                ret = get_frame(byteBuffer);
                Log.d(TAG, "SENSOR_CONTROL_GET_FRAME");
            }
            break;

            case SENSOR_CONTROL_WAKE_UP:
            case SENSOR_CONTROL_STANDBY:
                break;

        }

        return ret;
    }


    public static void init(USBWrapper serialDevice) {
        mUSBWrapper = serialDevice;
    }

    private static void clear_buffer() {
        for (int i = et736_egis_tag.length; i < command.length; i++) command[i] = 0;
        for (int i = 0; i < buffer.length; i++) buffer[i] = 0;
    }

    public static synchronized int et736_write_register(short address, byte val) {
        if (mUSBWrapper.getOTGType() != USBWrapper.OTGType.MASS_STORAGE) {
            Log.e(TAG, "not MASS_STORAGE");
            clear_buffer();
            return ERROR_COMMAND_FAIL;
        }
        int ret = (mUSBWrapper.ioDispatchOtgSCSI_Write_Register(address, val) >= 0) ? OK : ERROR_COMMAND_FAIL;
        return ret;
    }


    public static synchronized int et736_read_register(byte address, byte[] val) {
        if (mUSBWrapper.getOTGType() != USBWrapper.OTGType.MASS_STORAGE) {
            Log.e(TAG, "not MASS_STORAGE");
            clear_buffer();
            return ERROR_COMMAND_FAIL;
        }
        int ret = (mUSBWrapper.ioDispatchOtgSCSI_Read_Register(address, val) >= 0) ? OK : ERROR_COMMAND_FAIL;
        return ret;
    }

    public static synchronized int get_frame(byte[] imageBuffer) {
        if (mUSBWrapper.getOTGType() != USBWrapper.OTGType.MASS_STORAGE) {
            Log.e(TAG, "not MASS_STORAGE");
            clear_buffer();
            return ERROR_COMMAND_FAIL;
        }
        int ret = (mUSBWrapper.ioDispatchOtgSCSI_Get_Frame(imageBuffer, SENSOR_WIDTH, SENSOR_HEIGHT) >= 0) ? OK : ERROR_COMMAND_FAIL;
        return ret;
    }

    public static int get_frame_mean(byte[] frame, int width, int height) {
        int size = width * height;
        int mean = 0;

        for (int i = 0; i < size; i++) {
            mean += frame[i];
        }
        mean /= size;

        return mean;
    }

    public static int read_id(byte[] id) {
        byte[] val = new byte[1];
        for (int i = 0; i < 5; i++) {
            et736_read_register((byte) (i + 1), val);
            id[i] = val[0];
        }

        return OK;
    }

    @Override
    public int initialize(USBWrapper serialDevice) {
        init(serialDevice);
        return OK;
    }

    @Override
    public int eventHandler(int eventId, int value1, int value2, byte[] byteBuffer) {
        return NativeCallback(eventId, value1, value2, byteBuffer);
    }

}