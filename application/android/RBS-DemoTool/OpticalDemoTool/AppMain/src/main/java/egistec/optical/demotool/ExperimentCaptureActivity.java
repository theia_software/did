package egistec.optical.demotool;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
//import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import egistec.optical.demotool.experiment.ExperimentDef;
import egistec.optical.demotool.experiment.ExperimentSettingValueDB;
import egistec.optical.demotool.experiment.ExperimentUtils;
import egistec.optical.demotool.experiment.FileIoUtils;
import egistec.optical.demotool.otg.manager.OtgSensorManager;
import rbs.egistec.com.fplib.api.FingerprintReceiver;
import rbs.egistec.com.fplib.api.FpResDef;
import rbs.egistec.com.fplib.api.RbsLib;
import rbs.egistec.com.fplib.api.EgisLog;

import static egistec.optical.demotool.experiment.ExperimentDef.CMD_RETURN_16BIT_IMAGE_ENABLE;
import static egistec.optical.demotool.experiment.ExperimentDef.PID_BKG_IMG;
import static egistec.optical.demotool.experiment.ExperimentDef.PID_HOST_TOUCH;
import static egistec.optical.demotool.otg.OtgDef.OTG_SENSOR_ID;

public class ExperimentCaptureActivity extends AppCompatActivity {

    private final String TAG = "ExperimentCaptureLog";
    private final int EXPECTED_CALLBACK_COUNT = 5;
    private Context mContext = null;
    private Button mButtonStartCaptureLowToHigh = null;
    private Button mButtonStartCaptureHighToLow = null;
    private Handler mHandler = null;
    private int mCurrentExpTimeScale = 0;
    private String mCurrentSaveFolder = ExperimentDef.expCapFileFolder0;
    private TextView mTextViewProgress = null;
    private int mGroupId = 0;
    private ImageView mImageView = null;
    private RbsLib mRbsLib = null;
    private FingerprintReceiver mFingerprintReceiver = null;
    private int mFrameCount = 0;
    private int mIntQty = 0;
    private int mExtQty = 0;
    private int mCurrentCallbackCount = 0;
    private float mCurrentWaitImgTime = 0;
    private OtgSensorManager mOtgSensorManager = null;
    private FileIoUtils.WindowPos icWindowPos = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_experiment_capture);
        mContext = this;
        if (!FileIoUtils.isFileExist(ExperimentDef.expCapQualityCsvPath)) {
            FileIoUtils.createDirectory(ExperimentDef.expCapQualityCsvFolder);
            FileIoUtils.saveQualityCsvFileHeader(ExperimentDef.expCapQualityCsvPath);
        }
        mOtgSensorManager = OtgSensorManager.getInstance(OTG_SENSOR_ID, mContext);
        mRbsLib = RbsLib.getInstance(this);
        mHandler = initializeHandler();
        mFingerprintReceiver = initializeFingerprintReceiver();
        initializeLayout();
        ExperimentUtils.changeAppBrightness(mContext, ExperimentSettingValueDB.getBrightness(mContext));
    }
    @Override
    protected void onResume() {
        super.onResume();
        ExperimentUtils.initializeRbsLib(mRbsLib, mFingerprintReceiver);
    }
    @Override
    protected void onPause() {
        super.onPause();
        ExperimentUtils.uninitializeRbsLib(mRbsLib);
    }

    private void initializeLayout() {
        mTextViewProgress = (TextView)findViewById(R.id.text_view_progress);
        mButtonStartCaptureLowToHigh = (Button)findViewById(R.id.button_experiment_capture_low_to_high);
        setOnClickButtonListener(mButtonStartCaptureLowToHigh, ExperimentDef.CAPTURE_LOW_TO_HIGH);
        mButtonStartCaptureHighToLow = (Button)findViewById(R.id.button_experiment_capture_high_to_low);
        setOnClickButtonListener(mButtonStartCaptureHighToLow, ExperimentDef.CAPTURE_HIGH_TO_LOW);

        icWindowPos = FileIoUtils.loadIcWindowLayoutData();
        if (icWindowPos != null) {
            ExperimentSettingValueDB.setPressPosX(mContext, icWindowPos.x);
            ExperimentSettingValueDB.setPressPosY(mContext, icWindowPos.y);
        }
        RelativeLayout gifContainer = (RelativeLayout)findViewById(R.id.gif_container);
        Button buttonStartCapture = (Button)findViewById(R.id.IC_window_main_verify);

        ExperimentUtils.setPressAreaLeftTop(gifContainer, ExperimentSettingValueDB.getPressPosX(mContext), ExperimentSettingValueDB.getPressPosY(mContext), getResources().getDisplayMetrics());
        ExperimentUtils.setPressAreaWidthHeight(gifContainer, buttonStartCapture,80, 120, getResources().getDisplayMetrics());

        TextView textViewExpTime = (TextView)findViewById(R.id.text_view_exp_time);
        textViewExpTime.setText(ExperimentUtils.getAllExpTimeString(mContext, textViewExpTime));

        TextView textViewOthers = (TextView)findViewById(R.id.text_view_others);
        textViewOthers.setText(ExperimentUtils.getOtherSettingString(mContext));
        setPressColor();
    }

    private void setOnClickButtonListener(final Button button, final int captureGroupId) {
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mHandler.obtainMessage(ExperimentDef.MESSAGE_PROCESS_START, captureGroupId, -1).sendToTarget();
                initializeSaveFolder(captureGroupId);
                //capture processes
                Thread thread = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        runCaptureProcesses(captureGroupId);
                    }
                });
                thread.start();
            }
        });
    }

    private void runCaptureProcesses(int captureGroupId) {
        int maxScale = ExperimentSettingValueDB.getExposureTimeScaleMax(mContext);
        int minScale = ExperimentSettingValueDB.getExposureTimeScaleMin(mContext);
        mGroupId = captureGroupId;
        if (captureGroupId == ExperimentDef.CAPTURE_LOW_TO_HIGH) {
            for (int i = minScale; i <= maxScale; i++) {
                doCapture(i, minScale, maxScale);
            }
        } else {
            for (int i = maxScale; i >= minScale; i--) {
                doCapture(i, maxScale, minScale);
            }
        }
        mHandler.obtainMessage(ExperimentDef.MESSAGE_PROCESS_END, captureGroupId, -1, null).sendToTarget();
    }

    private void initializeSaveFolder(int captureGroupId) {
        mCurrentSaveFolder  = captureGroupId == ExperimentDef.CAPTURE_LOW_TO_HIGH ? ExperimentDef.expCapFileFolder0 : ExperimentDef.expCapFileFolder1;
        FileIoUtils.cleanDirectory(mCurrentSaveFolder);
        FileIoUtils.createDirectory(mCurrentSaveFolder);
    }

    private void loadBackgroundImage(int expTimeScale, int width, int height) {
        // Call rbs api to load calibrate data by saved path
        String bkgFilePath = ExperimentDef.expBkgBinFileFolder + ExperimentUtils.findBackgroundImageName(expTimeScale);
        mRbsLib.extraApi(PID_BKG_IMG, ExperimentDef.CMD_BKG_IMG_LOAD_CALI_DATA, bkgFilePath.getBytes(), null, null);
    }

    private void doCapture(int curScale, int startScale, int endScale) {
        ExperimentUtils.initializeRbsSettings(mRbsLib, ExperimentDef.expTimeBase * curScale, ExperimentSettingValueDB.getIntegrationCount(mContext));
        ExperimentUtils.waitForTimePeriod(500);
        mCurrentExpTimeScale = curScale;
        loadBackgroundImage(curScale, ExperimentDef.imgWidth, ExperimentDef.imgHeight);
        getImage();
        int progress = curScale == endScale ? 100 : (Math.abs(curScale - startScale) + 1) * (100/(Math.abs(startScale - endScale) + 1));
        mHandler.obtainMessage(ExperimentDef.MESSAGE_PROCESS_UPDATE_PROGRESS, progress, -1).sendToTarget();
        EgisLog.d(TAG, "Capture progress = " + progress + "%");
    }

    private void getImage() {
        mCurrentCallbackCount = 0;
        mCurrentWaitImgTime = 0;
        mRbsLib.extraApi(PID_HOST_TOUCH, CMD_RETURN_16BIT_IMAGE_ENABLE, null, null, null);
        final int[] fplist = new int[5];
        final int[] fpcount = { 0 };
        int ret = mRbsLib.verify(0,fplist,fpcount[0],1);
        if (ret < 0) {
            EgisLog.e(TAG, "getImage failed");
            return;
        }
        while (mCurrentCallbackCount < EXPECTED_CALLBACK_COUNT && mCurrentWaitImgTime < ExperimentDef.MAX_WAIT_IMAGE_TIME) {
            ExperimentUtils.waitForTimePeriod(ExperimentDef.WAIT_IMAGE_TIME);
            mCurrentWaitImgTime += ExperimentDef.WAIT_IMAGE_TIME;
        }
        EgisLog.d(TAG, "getImage ends, callback count = " + mCurrentCallbackCount);
    }

    private void updateLayout(int captureGroupId, boolean isEnd) {
        int processType = captureGroupId == ExperimentDef.CAPTURE_LOW_TO_HIGH ? ExperimentDef.processTypeCapture0 : ExperimentDef.processTypeCapture1;
        mTextViewProgress.setText(ExperimentUtils.updateProgressMessage(isEnd ? 100 : 0, processType));
        if (captureGroupId == ExperimentDef.CAPTURE_LOW_TO_HIGH) {
            mButtonStartCaptureLowToHigh.setEnabled(isEnd);
            mButtonStartCaptureHighToLow.setVisibility(isEnd ? View.VISIBLE : View.INVISIBLE);
        } else {
            mButtonStartCaptureHighToLow.setEnabled(isEnd);
            mButtonStartCaptureLowToHigh.setVisibility(isEnd ? View.VISIBLE : View.INVISIBLE);
        }
    }

    private Handler initializeHandler() {
        return new Handler() {
            @Override
            public void handleMessage(Message msg) {
                switch (msg.what) {
                    case ExperimentDef.MESSAGE_PROCESS_START:
                        updateLayout(msg.arg1, false);
                        break;
                    case ExperimentDef.MESSAGE_PROCESS_END:
                        updateLayout(msg.arg1, true);
                        break;
                    case ExperimentDef.MESSAGE_PROCESS_UPDATE_PROGRESS:
                        mTextViewProgress.setText(ExperimentUtils.updateProgressMessage(msg.arg1, -1));
                        break;
                    default:
                        break;
                }
            }
        };
    }

    private void setPressColor() {
        int viewId = ExperimentUtils.getPressViewId(ExperimentSettingValueDB.getPressColorId(mContext));
        if (mImageView != null) {
            mImageView.setVisibility(View.INVISIBLE);
        }
        mImageView = (ImageView)findViewById(viewId);
        mImageView.setVisibility(View.VISIBLE);
    }

    private FingerprintReceiver initializeFingerprintReceiver() {
        return new FingerprintReceiver() {
            @Override
            public void onFingerprintEvent(int eventId, int value1, int value2, Object eventObj) {
                // expect two event sequences:
                // 1. EVENT_RETURN_IMAGE_QTY -> EVENT_RETURN_IMAGE_COUNT -> EVENT_RETURN_IMAGE
                // 2. EVENT_RETURN_IMAGE_COUNT -> EVENT_RETURN_16BIT_IMAGE
                switch (eventId) {
                    case FpResDef.EVENT_RETURN_IMAGE_QTY:
                        EgisLog.d(TAG, "+++++ RETURN_IMAGE_QTY +++++");
                        mIntQty = value1;
                        mExtQty = value2;
                        mCurrentCallbackCount++;
                        break;
                    case FpResDef.EVENT_RETURN_IMAGE_COUNT:
                        EgisLog.d(TAG, "+++++ RETURN_IMAGE_COUNT +++++");
                        mFrameCount = value1;
                        mCurrentCallbackCount++;
                        break;
                    case FpResDef.EVENT_RETURN_IMAGE:
                        EgisLog.d(TAG, "+++++ RETURN_IMAGE +++++");
                        if (eventObj == null) {
                            EgisLog.e(TAG, "eventObj is null");
                        } else {
                            int width = value1;
                            int height = value2;
                            int imageSize = width * height;
                            byte[] img = new byte[imageSize];
                            // expect to get one integrated 8-bit image
                            EgisLog.d(TAG, "frame count: " + mFrameCount + " width " + width + " height " + height);
                            for (int i = 0; i < mFrameCount; i++) {
                                System.arraycopy(eventObj, i * imageSize, img, 0, imageSize);
                                String qualityInfo = "ExtQty_" + Integer.toString(mExtQty) + "_IntQty_" + Integer.toString(mIntQty) + "_";
                                String saveBinName = qualityInfo + ExperimentUtils.getSaveExpImgName(mContext, mCurrentExpTimeScale, false, false, ".bin");
                                String saveCsvName = qualityInfo + ExperimentUtils.getSaveExpImgName(mContext, mCurrentExpTimeScale, false, false, ".csv");
                                FileIoUtils.saveImage8Bits(mCurrentSaveFolder + saveBinName, img);
                                FileIoUtils.saveByteToCsvFile(mCurrentSaveFolder + saveCsvName, img, width);
                                FileIoUtils.saveQualityCsvFile(ExperimentDef.expCapQualityCsvPath, true, mGroupId, saveBinName, mIntQty, mExtQty);
                            }
                        }
                        mCurrentCallbackCount++;
                        break;
                    case FpResDef.EVENT_RETURN_16BIT_IMAGE:
                        EgisLog.d(TAG, "+++++ RETURN_16BIT_IMAGE +++++");
                        if (eventObj == null) {
                            EgisLog.e(TAG, "eventObj is null");
                        } else {
                            int width = value1;
                            int height = value2;
                            int imageSize = width * height;
                            byte[] img = new byte[imageSize * 2];
                            EgisLog.d(TAG, "16-bit frame count: " + mFrameCount + " width " + width + " height " + height);
                            short[] img16Bit = new short[imageSize];
                            for (int i = 0; i < mFrameCount; i++) {
                                System.arraycopy(eventObj, i * imageSize * 2, img, 0, imageSize * 2);
                                ByteBuffer.wrap(img).order(ByteOrder.LITTLE_ENDIAN).asShortBuffer().get(img16Bit);
                                String saveBinName = ExperimentUtils.getFrameNoString(i) + ExperimentUtils.getSaveExpImgName(mContext, mCurrentExpTimeScale, false, true, ".bin");
                                String saveCsvName = ExperimentUtils.getFrameNoString(i) + ExperimentUtils.getSaveExpImgName(mContext, mCurrentExpTimeScale, false, true, ".csv");
                                FileIoUtils.saveImage16Bits(mCurrentSaveFolder + saveBinName, img16Bit);
                                FileIoUtils.saveShortToCsvFile(mCurrentSaveFolder + saveCsvName, img16Bit, width);
                            }
                        }
                        mCurrentCallbackCount++;
                        break;
                    case FpResDef.EVENT_OTG_WRITE_REGISTER:
                    case FpResDef.EVENT_OTG_READ_REGISTER:
                    case FpResDef.EVENT_OTG_GET_FRAME:
                    case FpResDef.EVENT_OTG_WAKE_UP:
                    case FpResDef.EVENT_OTG_STANDBY:
                    case FpResDef.EVENT_OTG_SPI_WRITE_READ:
                    case FpResDef.EVENT_OTG_SET_SPI_CLK:
                        if (mOtgSensorManager != null) {
                            mOtgSensorManager.nativeCallbackHandler(eventId, value1, value2, (byte[])eventObj);
                        }
                        break;
                    default:
                        break;
                }
            }
        };
    }
}
