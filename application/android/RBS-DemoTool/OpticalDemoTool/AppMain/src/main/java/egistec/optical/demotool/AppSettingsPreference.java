package egistec.optical.demotool;

import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.EditTextPreference;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.widget.RelativeLayout;
//import android.util.Log;
import rbs.egistec.com.fplib.api.EgisLog;

import static egistec.optical.demotool.AppSettingsPreference.SENSOR_SIZE.SENSOR_SIZE_LARGE;
import static egistec.optical.demotool.AppSettingsPreference.SENSOR_SIZE.SENSOR_SIZE_NORMAL;

public class AppSettingsPreference extends PreferenceFragment {
    private final static String TAG = "AppSettingsPreference";
    private final static String KEY_OPTION_NOCANCEL_ACTIONUP = "OPTION_NOCANCEL_ACTIONUP";
    private final static String KEY_OPTION_BRIGHTNESS = "OPTION_BRIGHTNESS";
    private final static String KEY_OPTION_PRESS_AREA_TYPE = "OPTION_PRESS_AREA_TYPE";
    private final static String KEY_OPTION_PRESS_AREA_DIAMETER = "OPTION_PRESS_AREA_DIAMETER";
    private final static String KEY_OPTION_ADD_VIEWTREEOBSERVER = "OPTION_ADD_VIEWTREEOBSERVER";
    private final static String KEY_OPTION_CUSTOMIZE_PRESS_AREA_ICON = "OPTION_CUSTOMIZE_PRESS_AREA_ICON";
    private final static String KEY_OPTION_SENSOR_SIZE = "OPTION_SENSOR_SIZE";

    public enum SENSOR_SIZE {
        SENSOR_SIZE_NORMAL,
        SENSOR_SIZE_LARGE
    };

    private final static float[] pressAreaDiameterList = {10f, 11f, 12f, 12.5f};
    private final static SENSOR_SIZE[] sensorSizeList = {SENSOR_SIZE_NORMAL, SENSOR_SIZE_LARGE};

    private AppSettings mAppSettings;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.pref_app_settings);
        mAppSettings = new AppSettings(getActivity().getApplicationContext());
        CheckBoxPreference checkBoxPreference = (CheckBoxPreference)this.findPreference(KEY_OPTION_NOCANCEL_ACTIONUP);
        EditTextPreference editTextPreference = (EditTextPreference)this.findPreference(KEY_OPTION_BRIGHTNESS);
        setPreference(checkBoxPreference, mAppSettings.getNoCancelActionUp());
        checkBoxPreference.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                setPreference(preference, newValue);
                mAppSettings.setNoCancelActionUp((boolean)newValue);
                return false;
            }
        });
        setPreference(editTextPreference, String.valueOf(mAppSettings.getScreenBrightness()));
        editTextPreference.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                setPreference(preference, newValue);
                mAppSettings.setScreenBrightness(Integer.valueOf(newValue.toString()));
                return false;
            }
        });
        ListPreference listPreferencePressAreaType = (ListPreference)this.findPreference(KEY_OPTION_PRESS_AREA_TYPE);
        try {
            setPreference(listPreferencePressAreaType, listPreferencePressAreaType.getEntryValues()[mAppSettings.getPressAreaTypeId()]);
        } catch (Exception e) {
            e.printStackTrace();
            mAppSettings.setPressAreaTypeId(0);
            setPreference(listPreferencePressAreaType, listPreferencePressAreaType.getEntryValues()[mAppSettings.getPressAreaTypeId()]);
        }
        listPreferencePressAreaType.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                setPreference(preference, newValue);
                ListPreference listPreference = (ListPreference)preference;
                mAppSettings.setPressAreaTypeId(listPreference.findIndexOfValue((String)newValue));
                return false;
            }
        });
        ListPreference listPreferencePressAreaDiameter = (ListPreference)this.findPreference(KEY_OPTION_PRESS_AREA_DIAMETER);
        setPreference(listPreferencePressAreaDiameter, listPreferencePressAreaDiameter.getEntryValues()[mAppSettings.getPressAreaDiameterIndex()]);
        listPreferencePressAreaDiameter.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                setPreference(preference, newValue);
                ListPreference listPreference = (ListPreference)preference;
                mAppSettings.setPressAreaDiameterIndex(listPreference.findIndexOfValue((String)newValue));
                return false;
            }
        });
        CheckBoxPreference checkBoxPreferenceObserver = (CheckBoxPreference)this.findPreference(KEY_OPTION_ADD_VIEWTREEOBSERVER);
        setPreference(checkBoxPreferenceObserver, mAppSettings.getAddViewTreeObserver());
        checkBoxPreferenceObserver.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                setPreference(preference, newValue);
                mAppSettings.setAddViewTreeObserver((boolean)newValue);
                return false;
            }
        });
        CheckBoxPreference checkBoxPreferenceCustomizeIcon = (CheckBoxPreference)this.findPreference(KEY_OPTION_CUSTOMIZE_PRESS_AREA_ICON);
        setPreference(checkBoxPreferenceCustomizeIcon, mAppSettings.getCustomizePressAreaIcon());
        checkBoxPreferenceCustomizeIcon.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                setPreference(preference, newValue);
                mAppSettings.setCustomizePressAreaIcon((boolean)newValue);
                return false;
            }
        });

        ListPreference listPreferenceSensorSize = (ListPreference)this.findPreference(KEY_OPTION_SENSOR_SIZE);
        setPreference(listPreferenceSensorSize, listPreferenceSensorSize.getEntryValues()[mAppSettings.getSensorSizeIndex()]);
        listPreferenceSensorSize.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                setPreference(preference, newValue);
                ListPreference listPreference = (ListPreference)preference;
                mAppSettings.setSensorSizeIndex(listPreference.findIndexOfValue((String)newValue));
                return false;
            }
        });
    }

    private void setPreference(Preference preference, Object value) {
        if (preference instanceof CheckBoxPreference) {
            CheckBoxPreference checkBoxPreference = (CheckBoxPreference)preference;
            checkBoxPreference.setChecked((boolean)value);
            checkBoxPreference.setSummary((boolean)value == false ? "Off" : "On");
        } else if (preference instanceof EditTextPreference) {
            EditTextPreference editTextPreference = (EditTextPreference)preference;
            editTextPreference.setText((String)value);
            editTextPreference.setSummary((String)value);
        } else if (preference instanceof ListPreference) {
            ListPreference listPreference = (ListPreference)preference;
            listPreference.setValue((String)value);
            listPreference.setSummary((String)value);
        }
    }

    public static int getPressAreaDrawableId(int press_area_type_id) {
        switch (press_area_type_id) {
            case AppSettings.PRESS_AREA_TYPE_ID_GREEN:
                return R.drawable.press_green;
            case AppSettings.PRESS_AREA_TYPE_ID_GREEN_GRADIENT:
                return R.drawable.press_gradient;
            case AppSettings.PRESS_AREA_TYPE_ID_GREEN_GRADIENT_DARKER:
                return R.drawable.press_940_60;
            case AppSettings.PRESS_AREA_TYPE_ID_GREEN_GRADIENT_DARKEST:
                return R.drawable.press_4800_150;
            case AppSettings.PRESS_AREA_TYPE_ID_GREEN_GRADATION:
                return -2;
            case AppSettings.PRESS_AREA_TYPE_ID_AQUA:
                return R.drawable.press_aqua;
            case AppSettings.PRESS_AREA_TYPE_ID_AQUA_CTD:
                return R.drawable.press_aqua_ctd;
            case AppSettings.PRESS_AREA_TYPE_ID_WHITE_CIRCLE:
                return R.drawable.press_aqua_white;
            default:
                EgisLog.e(TAG, "Invalid press_area_type_id!");
                return -1;
        }
    }

    public static float getPressAreaDiameter(int index) {
        return pressAreaDiameterList[index];
    }

    public static SENSOR_SIZE getSensorSize(int index) {
        return sensorSizeList[index];
    }
}
