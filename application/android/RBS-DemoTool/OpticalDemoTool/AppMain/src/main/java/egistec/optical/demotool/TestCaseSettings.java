package egistec.optical.demotool;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.TranslateAnimation;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import org.json.JSONArray;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import egistec.optical.demotool.igistec.Common;
import egistec.optical.demotool.igistec.SkinColorAdapter;
import egistec.optical.demotool.igistec.SkinColorItem;

public class TestCaseSettings {
    private static String TAG = "debug_check_TestCaseSettings";

    private Button btnOK ;
    private TextView tvTitle ;
    private TextView tvTestBrief ;
    private RelativeLayout mTestCaseLayout;

    private Spinner mSkinSpinner;

    private String[] mSkinCase = {
            "Type I",
            "Type II",
            "Type III",
            "Type IV",
            "Type V",
            "Type VI" };

    private String[] mSkinSubTitle = {
            "Pale white",
            "White, fair",
            "Light brown",
            "Mid-brown",
            "Dark brown",
            "Brown to black" };


    private int [] mSkinColorCase = {
            Color.rgb(238,208,180),
            Color.rgb(224,181,148),
            Color.rgb(203,159,129),
            Color.rgb(177,121,87),
            Color.rgb(157,95,54),
            Color.rgb(56,34,29	),
    };

    private Map<String, Integer> mSkinTable = new HashMap<String, Integer>();
    private int mSkinCaseIdx = 0;

    private Spinner mAgeSpinner;
    private String[] mAgeCase = {
            "0-9",
            "10-19",
            "20-29",
            "30-39",
            "40-49",
            "50-59",
            "60-69",
            "70-79",
            "80-89",
            "above90",
    };
    private int mAgeCaseIdx = 0;

    private Spinner mGenderSpinner;
    private String[] mGenderCase = {"Male", "Female"};
    private int mGenderCaseIdx = 0;

    private Spinner mFingerConditionSpinner;
    private String[] mFingerConditionCase = {"Normal", "Dry", "Wet"}; // 注意會變成路徑的一部份
    private int mFingerConditionCaseIdx = 0;

    private Spinner mTemperatureSpinner;
    private String[] mTemperatureCase = {
            "-50 ~ -30",
            "-30 ~ -21",
            "-20 ~ -11",
            "-10 ~  -1",
            "  0 ~   9",
            " 10 ~  19",
            " 20 ~  29",
            " 30 ~  39",
            " 40 ~  49",
            " 50 ~  60",
    };
    private int mTemperatureCaseIdx = 0;

    private Spinner mLightSpinner;
    private String[] mLightCase = {
            "Black",
            "Light Level 1",
            "Light Level 2",
            "Light Level 3",
            "Light Level 4",
            "Light Level 5",
            "Light Level 6",
            "Light Level 7",
            "Light Level 8",
            "Max"};
    private int mLightCaseIdx = 0;

    private Spinner mPadSpinner;
    private String[] mPadCase = { // 注意會變成路徑的一部份
            "Real Finger",
            "2d Spoof",
            "2.5d Spoof",
    };
    private int mPadCaseIdx = 0;


    private Spinner mInkSpinner;
    private String[] mInkCase = { // 注意會變成路徑的一部份
            "Black",
            "Red"
    };
    private int mInkCaseIdx = 0;

    private Spinner m2dMetrialSpinner;
    private String[] m2dMetrialCase = { // 注意會變成路徑的一部份, 空白被replace
            "White Copy Paper",
            "Pink Copy Paper",
            "Orange Copy Paper",
            "Green Copy Paper",
            "Light Blue Copy Paper",
            "Yello copy paper",
            "Transparency",
            "Matte Photo Paper",
            "Luster Photo Paper",
            "Semi-gloss Photo Paper",
            "Glossy Photo Paper"

    };
    private int m2DMetrialIdx = 0;

    private Spinner mMoleSpinner;
    private String[] mMoleCase = { // 注意會變成路徑的一部份
            "PCB",
            "transparency"
    };
    private int mMoleInx = 0;


    private Spinner m25dMetrialSpinner;
    private String[] m25dMetrialCase = { // 注意會變成路徑的一部份
        "Gelatin",
        "Latex",
        "Silicone",
        "Glue",
        "Wood glue",
        "Elmers Glue-All",
        "Fabric glue"
    };
    private int m25DMetrialIdx = 0;

    LinearLayout llSkin ;
    LinearLayout llAge ;
    LinearLayout llGender ;

    LinearLayout llfingercond;
    LinearLayout lltemp ;
    LinearLayout lllight ;
    LinearLayout llcover ;
    LinearLayout llpad ;

    LinearLayout ll2d ;
    LinearLayout ll25d ;
    LinearLayout llink ;
    LinearLayout llmole ;

    private Spinner mCoverSpinner;
    private String[] mCoverCase = {
            "without Cover",
            "with Cover"
    };
    private int mCoverIdx = 0;


    private Button mBackBtn;
    private Button btnOpen ;
    private Button btnUserInfoCase ;

    static private TestCaseSettings mInstance;

    static public TestCaseSettings getInstance() {
        if(mInstance == null)
            mInstance = new TestCaseSettings();

        return mInstance;
    }

    public TestCaseSettings() {
        load_setting();
    }


    /**
        spec source:
        •0= Caucasian, European immigrated Latin American
        •1=  Asian
        •2 = Indian, middle Eastern Asian
        •3 = African, American African, European African
        •4 = American Indian, original American
    **/

    void initSkinTable() {

        Log.e(TAG, "@@ Init Skin Table!") ;

        mSkinTable.put("Caucasian", 0) ;
        mSkinTable.put("European Immigrated Latin American", 0) ;

        // mSkinTable.put( "Immigrated Latin American",0);
        mSkinTable.put("Asian",1);

        mSkinTable.put("Indian",2);
        mSkinTable.put("Middle Eastern Asian",2);

        mSkinTable.put("African",3);
        mSkinTable.put("American African",3);
        mSkinTable.put("European African",3);

        mSkinTable.put("American Indian",4);
        mSkinTable.put("Original American",4);
    }

    void init(final Activity activity) {
        Log.d(TAG, "init");
        if ( mSkinTable.size() == 0 ) {
            initSkinTable() ;
        }

        mTestCaseLayout = activity.findViewById(R.id.test_case_settings_layout);
        mTestCaseLayout.bringToFront();
        mTestCaseLayout.setVisibility(View.GONE);

        tvTitle = activity.findViewById(R.id.tvTitle) ;

        btnOK = activity.findViewById(R.id.btnOK) ;
        btnOpen = activity.findViewById(R.id.test_case_settings_btn) ;
        btnUserInfoCase = activity.findViewById(R.id.btnUserInfoCase) ;

        mSkinSpinner = activity.findViewById(R.id.testCaseSkinSpinner);

        tvTestBrief = activity.findViewById(R.id.tvTestBrief) ;

        //Object[] newSkinCase = (new ArrayList<String>(mSkinTable.keySet())).toArray();
        //List<String> l = new ArrayList<String>(mSkinTable.keySet());
        //Collections.sort(l);
        //mSkinCase = l.toArray(new String[0]) ;


        btnOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mBackBtn.callOnClick() ;
            }
        });


        ArrayList<SkinColorItem> slist = new ArrayList<SkinColorItem>() ;
        for (int i = 0 ; i < mSkinCase.length ; i++ ) {
            slist.add( new SkinColorItem(mSkinCase[i], mSkinSubTitle[i], mSkinColorCase[i]) ) ;
        }


        SkinColorAdapter aaSkinAdapter = new SkinColorAdapter(activity, R.layout.spinner_skin_color_item, slist);
        // aaSkinAdapter.setDropDownViewResource();
        // aaSkinAdapter.set

        mSkinSpinner.setAdapter(aaSkinAdapter);
        mSkinSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                mSkinCaseIdx = position;
                Log.d(TAG, "skin = " + mSkinCase[mSkinCaseIdx]);
                updateBrief();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        mSkinSpinner.setSelection(mSkinCaseIdx);

        mAgeSpinner = activity.findViewById(R.id.testCaseAgeSpinner);

        ArrayAdapter aaAge = new ArrayAdapter<String>(activity, android.R.layout.simple_list_item_1, mAgeCase) ;
        aaAge.setDropDownViewResource(R.layout.spinner_test_case);
        mAgeSpinner.setAdapter(aaAge);
        mAgeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                mAgeCaseIdx = position;
                Log.d(TAG, "age = " + mAgeCase[mAgeCaseIdx]);
                updateBrief();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        mAgeSpinner.setSelection(mAgeCaseIdx);

        mGenderSpinner = activity.findViewById(R.id.testCaseGenderSpinner);
        ArrayAdapter aaGender = new ArrayAdapter<String>(activity, android.R.layout.simple_list_item_1, mGenderCase) ;
        aaGender.setDropDownViewResource(R.layout.spinner_test_case);
        mGenderSpinner.setAdapter(aaGender);
        mGenderSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                mGenderCaseIdx = position;
                Log.d(TAG, "gender = " + mGenderCase[mGenderCaseIdx]);
                updateBrief();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        mGenderSpinner.setSelection(mGenderCaseIdx);

        mFingerConditionSpinner = activity.findViewById(R.id.testCaseFingerConditionSpinner);
        ArrayAdapter aaFingerCond = new ArrayAdapter<String>(activity, android.R.layout.simple_list_item_1, mFingerConditionCase) ;
        aaFingerCond.setDropDownViewResource(R.layout.spinner_test_case);
        mFingerConditionSpinner.setAdapter(aaFingerCond);
        mFingerConditionSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                mFingerConditionCaseIdx = position;
                Log.d(TAG, "FingerCondition = " + mFingerConditionCase[mFingerConditionCaseIdx]);
                updateBrief();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        mFingerConditionSpinner.setSelection(mFingerConditionCaseIdx);

        mTemperatureSpinner = activity.findViewById(R.id.testCaseTemperatureSpinner);
        ArrayAdapter aaTemp = new ArrayAdapter<String>(activity, android.R.layout.simple_list_item_1, mTemperatureCase);
        aaTemp.setDropDownViewResource(R.layout.spinner_test_case);
        mTemperatureSpinner.setAdapter(aaTemp);
        mTemperatureSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                mTemperatureCaseIdx = position;
                Log.d(TAG, "Temperature = " + mTemperatureCase[mTemperatureCaseIdx]);
                updateBrief();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        mTemperatureSpinner.setSelection(mTemperatureCaseIdx);

        mLightSpinner = activity.findViewById(R.id.testCaseLightSpinner);
        ArrayAdapter aaLightAdapter = new ArrayAdapter<String>(activity, android.R.layout.simple_list_item_1, mLightCase);
        aaLightAdapter.setDropDownViewResource(R.layout.spinner_test_case);
        mLightSpinner.setAdapter(aaLightAdapter);
        mLightSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                mLightCaseIdx = position;
                Log.d(TAG, "Light = " + mLightCase[mLightCaseIdx]);
                updateBrief();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        mLightSpinner.setSelection(mLightCaseIdx);


        // Cover
        mCoverSpinner = activity.findViewById(R.id.spCoverSpinner);
        ArrayAdapter aaCoverAdapter = new ArrayAdapter<String>(activity, android.R.layout.simple_list_item_1, mCoverCase);
        aaCoverAdapter.setDropDownViewResource(R.layout.spinner_test_case);
        mCoverSpinner.setAdapter(aaCoverAdapter);
        mCoverSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                mCoverIdx = position;
                Log.d(TAG, "Cover = " + mCoverCase[mCoverIdx]);
                updateBrief();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        mCoverSpinner.setSelection(mCoverIdx);

        mBackBtn = activity.findViewById(R.id.testCaseBack);
        mBackBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "Back Btn");

                save_setting() ;

                mTestCaseLayout.setVisibility(View.GONE);
                btnOpen.setVisibility(View.VISIBLE) ;
                btnOpen.bringToFront();

                btnUserInfoCase.setVisibility(View.VISIBLE) ;
                btnUserInfoCase.bringToFront();


                updateBrief();
            }
        });



        btnUserInfoCase.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "UserInfoCase");

                makeShowAnimation() ;
                //mTestCaseLayout.setVisibility(View.VISIBLE);
                btnOpen.setVisibility(View.GONE) ;
                btnUserInfoCase.setVisibility(View.GONE) ;
                setUserInfoMode();
                Common.hideSystemUI(activity);
                updateBrief();
            }
        });


        // 2d ink
        mInkSpinner = activity.findViewById(R.id.spInk);
        ArrayAdapter aaInkAdapter = new ArrayAdapter<String>(activity, android.R.layout.simple_list_item_1, mInkCase) ;
        aaInkAdapter.setDropDownViewResource(R.layout.spinner_test_case);
        mInkSpinner.setAdapter(aaInkAdapter);
        mInkSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                mInkCaseIdx = position;
                Log.d(TAG, "Ink = " + mInkCase[mInkCaseIdx]);
                updateBrief();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        mInkSpinner.setSelection(mInkCaseIdx);

        // 2d metrial
        m2dMetrialSpinner = activity.findViewById(R.id.sp2dMetrial);
        ArrayAdapter aa2dAdapter = new ArrayAdapter<String>(activity, android.R.layout.simple_list_item_1, m2dMetrialCase);
        aa2dAdapter.setDropDownViewResource(R.layout.spinner_test_case);
        m2dMetrialSpinner.setAdapter(aa2dAdapter);
        m2dMetrialSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                m2DMetrialIdx = position;
                Log.d(TAG, "2D Metrial = " + m2dMetrialCase[m2DMetrialIdx]);
                updateBrief();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        m2dMetrialSpinner.setSelection(m2DMetrialIdx);



        // 2d mole
        mMoleSpinner = activity.findViewById(R.id.spMole);
        ArrayAdapter aaMoleAdapter = new ArrayAdapter<String>(activity, android.R.layout.simple_list_item_1, mMoleCase);
        aaMoleAdapter.setDropDownViewResource(R.layout.spinner_test_case);
        mMoleSpinner.setAdapter(aaMoleAdapter);
        mMoleSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                mMoleInx = position;
                Log.d(TAG, "Mole = " + mMoleCase[mMoleInx]);
                updateBrief();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        mMoleSpinner.setSelection(mMoleInx);





        // 2.5 d metrial
        m25dMetrialSpinner = activity.findViewById(R.id.sp25dMetrials);
        ArrayAdapter aa25dAdapter = new ArrayAdapter<String>(activity, android.R.layout.simple_list_item_1, m25dMetrialCase);
        aa25dAdapter.setDropDownViewResource(R.layout.spinner_test_case);
        m25dMetrialSpinner.setAdapter(aa25dAdapter);
        m25dMetrialSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                m25DMetrialIdx = position;
                Log.d(TAG, "25D Metrial = " + m25dMetrialCase[m25DMetrialIdx]);
                updateBrief();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        m25dMetrialSpinner.setSelection(m25DMetrialIdx);


        llSkin = activity.findViewById(R.id.llskin);
        llAge = activity.findViewById(R.id.llage);
        llGender = activity.findViewById(R.id.llgender);

        llfingercond = activity.findViewById(R.id.llfingercond);
        lltemp = activity.findViewById(R.id.lltemp) ;
        lllight = activity.findViewById(R.id.lllight);
        llcover = activity.findViewById(R.id.llcover);
        llpad = activity.findViewById(R.id.llpad);


        ll2d = activity.findViewById(R.id.ll2dmetrial) ;
        ll25d = activity.findViewById(R.id.ll25dmetrials) ;

        llink = activity.findViewById(R.id.llink) ;
        llmole = activity.findViewById(R.id.llmole) ;

        // update pad and display
        mPadSpinner = activity.findViewById(R.id.testCasePadSpinner);
        ArrayAdapter aaPadAdapter = new ArrayAdapter<String>(activity, android.R.layout.simple_list_item_1, mPadCase);
        aaPadAdapter.setDropDownViewResource(R.layout.spinner_test_case);
        mPadSpinner.setAdapter(aaPadAdapter);
        mPadSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                mPadCaseIdx = position;
                Log.d(TAG, "Pad = " + mPadCase[mPadCaseIdx]);


                togglePadShow(position) ;

                updateBrief();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        mPadSpinner.setSelection(mPadCaseIdx);

        SetReleaseUiSetting();
    }

    private void SetReleaseUiSetting(){
        if (BuildConfig.BUILD_TYPE_RELEASE == 1) {
            try {
                btnUserInfoCase.setVisibility(View.GONE);
                btnOpen.setVisibility(View.GONE);
                tvTestBrief.setVisibility(View.GONE);
                Log.d(TAG, "Hide release UI");
            }
            catch (Exception ex) {
                Log.e(TAG, "Has null element") ;
            }
        }
    }

    private void togglePadShow(int position) {
        switch(position){
            case 0:
                // Real hide both
                llfingercond.setVisibility(View.VISIBLE);
                llink.setVisibility(View.GONE);
                ll2d.setVisibility(View.GONE);
                llmole.setVisibility(View.GONE);
                ll25d.setVisibility(View.GONE);
                break;
            case 1:
                llfingercond.setVisibility(View.GONE);
                llink.setVisibility(View.VISIBLE);
                ll2d.setVisibility(View.VISIBLE);
                llmole.setVisibility(View.GONE);
                ll25d.setVisibility(View.GONE);
                break ;
            case 2:
                llfingercond.setVisibility(View.GONE);
                llink.setVisibility(View.GONE);
                ll2d.setVisibility(View.GONE);
                llmole.setVisibility(View.VISIBLE);
                ll25d.setVisibility(View.VISIBLE);
                break ;
        }
    }


    public void makeShowAnimation() {

        int animate_ms = 300 ;
        Animation a = new AlphaAnimation(0f, 1f);
        a.setDuration(animate_ms);
        a.setFillAfter(true);

        mTestCaseLayout.setAlpha(1f) ;
        mTestCaseLayout.setVisibility(View.VISIBLE);
        mTestCaseLayout.bringToFront();


        TranslateAnimation translateAnimation = new TranslateAnimation( //X軸初始位置
                Animation.RELATIVE_TO_SELF, 0f, //X軸移動的結束位置
                Animation.RELATIVE_TO_SELF,0f, //y軸開始位置
                Animation.RELATIVE_TO_SELF,0.3f, //y軸移動後的結束位置
                Animation.RELATIVE_TO_SELF,0f); //3秒完成動畫
        translateAnimation.setDuration(animate_ms);


        AnimationSet set = new AnimationSet(true);
        set.addAnimation(a) ;
        set.addAnimation(translateAnimation) ;
        set.setFillAfter(true);

        set.setAnimationListener(new Animation.AnimationListener() {

            public void onAnimationStart(Animation animation) {
                // TODO Auto-generated method stub

            }

            public void onAnimationRepeat(Animation animation) {
                // TODO Auto-generated method stub

            }

            public void onAnimationEnd(Animation animation) {
                mTestCaseLayout.clearAnimation();
            }
        });

        mTestCaseLayout.startAnimation(set);
    }


    @Deprecated
    public String getSkin() {
        try {
            return "" + mSkinTable.get(mSkinCase[mSkinCaseIdx]) ;
        }
        catch (Exception ex) {
            return "0" ;
        }
        //return mSkinCase[mSkinCaseIdx];
    }

    public String getUserCondition() {
        String ret =
                String.valueOf(mSkinCaseIdx) +
                String.valueOf(mAgeCaseIdx) +
                String.valueOf(mGenderCaseIdx) +
                String.valueOf(mFingerConditionCaseIdx);
        Log.d(TAG, "user condition = " + ret);

        return ret;
    }

    /**
     * get string for folder path
     */
    public String getConditionString() {

        // if normal & real finger: return real/normal, dry, wet
        // else 2d and 2.5d:
        if ( mPadCaseIdx == 1 ) {

            return "2dSpoof/" + (mInkCase[mInkCaseIdx] + "_" + m2dMetrialCase[m2DMetrialIdx]).replace(" ", "") ;
        }
        else if ( mPadCaseIdx == 2 ) {

            return "25dSpoof/" + (mMoleCase[mMoleInx] + "_" + m25dMetrialCase[m25DMetrialIdx]).replace(" ", "") ;
        }
        else {
//            if ( mPadCaseIdx == 0 ) { // real finger
            return "real/" + mFingerConditionCase[mFingerConditionCaseIdx] ;
//            }
        }
    }

    public String getTemperature() {
        String ret = String.format(Locale.US, "%01d", mTemperatureCaseIdx);
        Log.d(TAG, "temperature = " + ret);

        return ret;
    }

    public String getLight() {
        String ret = String.format(Locale.US, "%02d", mLightCaseIdx);
        Log.d(TAG, "ambient light = " + ret);

        return ret;
    }


    /**
     * 數字擠不到單個CHAR裡，而產生的number to char的東西
     * 0-9, A-Z, a-z
     */
    public String idxToStr(int offset, int val) {

        String order = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz" ;
        return order.substring(offset + val, offset + val + 1) ;
    }


    /**
     * override
     * @return
     */
    public String getPad() {
        return getPad(true) ;
    }

    /**
     * new reference from sparkle:
     * https://igistec-my.sharepoint.com/:x:/r/personal/sparkle_wang_igistec_com/_layouts/15/Doc.aspx?action=edit&sourcedoc=%7B33b36b93-5233-4925-bd65-ba5c115c99e0%7D
     * @return
     *
     * returnID = true: return code
     * returnID = false: return human readable text
     */
    public String getPad(boolean returnID) {

        String ret = "00" ;
        String note = "" ;
        if (mPadCaseIdx == 0 ) {// real
            ret = "00" ;
            note = mPadCase[mPadCaseIdx] ;
        }
        else if ( mPadCaseIdx == 1 ) {
            if ( mInkCaseIdx == 0 ) { // black
                ret =  "1" + idxToStr(0, m2DMetrialIdx) ;
            }
            else if (mInkCaseIdx == 1 ) { // red
                ret =  "1" + idxToStr(11, m2DMetrialIdx) ;
            }
            note = mPadCase[mPadCaseIdx] + "/" + mInkCase[mInkCaseIdx] + "/" + m2dMetrialCase[m2DMetrialIdx] ;
        }
        else if ( mPadCaseIdx == 2 ) {
            if ( mMoleInx == 0 ) { // PCB
                ret =  "2" + idxToStr(0, m25DMetrialIdx) ;
            }
            else if (mMoleInx == 1 ) { // transprent
                ret =  "2" + idxToStr(7, m25DMetrialIdx) ;
            }
            note = mPadCase[mPadCaseIdx] + "/" + mMoleCase[mMoleInx] + "/" + m25dMetrialCase[m25DMetrialIdx] ;
        }

        Log.d(TAG, "get Pad = " + ret);
        if (returnID) {
            return ret ;
        }
        else {
            return note ;
        }
    }

    /**
     * note: with out try match
     * @param user_id
     * @param finger_idx
     * @param isp
     * @return
     */
    public String getFileName(String user_id, int finger_idx, String isp) {

        SimpleDateFormat sDateFormat = new SimpleDateFormat("yyMMdd-HHmmss-SS");
        String date = sDateFormat.format(new java.util.Date());

        String ret = date + "-" +
                FormatID(user_id) +
                "-" +
                finger_idx +
                "-" +
                isp +
                "-" +
                getUserCondition() +
                "-" +
                getTemperature() +
                "-" +
                getLight() +
                "-" +
                getPad() +
                "-" +
                "0" + mCoverIdx + "00" ;

        return ret;
    }

    public String getFileName(String user_id, int finger_idx, String isp, String tryMatch) {

        if ( tryMatch == null ) {
            tryMatch = "0" ;
        }
        if ( tryMatch.length() != 1 ) {
            Log.e(TAG, "error try match should 1 char:" + tryMatch) ;
            tryMatch = tryMatch.substring(0,1) ;
        }

        SimpleDateFormat sDateFormat = new SimpleDateFormat("yyMMdd-HHmmss-SS");
        String date = sDateFormat.format(new java.util.Date());

        String ret = date + "-" +
                FormatID(user_id) +
                "-" +
                finger_idx +
                "-" +
                isp +
                "-" +
                getUserCondition() +
                "-" +
                getTemperature() +
                "-" +
                getLight() +
                "-" +
                getPad() +
                "-" + tryMatch + mCoverIdx + "00" ;

        return ret;
    }


    /**
     * 取得簡短介紹文字
     * @return
     */
    public String getBrief() {
        String output = "" ;

//        output += mSkinCase[mSkinCaseIdx] + ", " ;
//        output += mAgeCase[mAgeCaseIdx] + ", " ;
//        output += mGenderCase[mGenderCaseIdx] + ", " ;

        output += mFingerConditionCase[mFingerConditionCaseIdx] + ", " ;
        output += mTemperatureCase[mTemperatureCaseIdx] + ", " ;
        output += mLightCase[mLightCaseIdx] + ", " ;
        output += getPad(false) + ", " ;
        output += mCoverCase[mCoverIdx] ;

        return output;
    }

    public void updateBrief() {
        if ( tvTestBrief != null ) {
            tvTestBrief.setText(getBrief()) ;
        }
    }

    public void setUserInfoMode() {
        // 註: 只留下USER INFO相關選項

        tvTitle.setText("User Info") ;

        llSkin.setVisibility(View.VISIBLE);
        llAge.setVisibility(View.VISIBLE);
        llGender.setVisibility(View.VISIBLE);

        llfingercond.setVisibility(View.GONE);
        lltemp.setVisibility(View.GONE);
        lllight.setVisibility(View.GONE);
        llcover.setVisibility(View.GONE);
        llpad.setVisibility(View.GONE);

        ll2d.setVisibility(View.GONE);
        ll25d.setVisibility(View.GONE);
        llink.setVisibility(View.GONE);
        llmole.setVisibility(View.GONE);
    }

    public void setTestCaseMode() {
        // 註: 使用TEST CASE SETTING 設定

        tvTitle.setText("Condition Settings") ;

        btnUserInfoCase.setVisibility(View.GONE);

        llSkin.setVisibility(View.GONE);
        llAge.setVisibility(View.GONE);
        llGender.setVisibility(View.GONE);

        llfingercond.setVisibility(View.VISIBLE);
        lltemp.setVisibility(View.VISIBLE);
        lllight.setVisibility(View.VISIBLE);
        llcover.setVisibility(View.VISIBLE);
        llpad.setVisibility(View.VISIBLE);

        togglePadShow(mPadCaseIdx) ;
//
//        ll2d.setVisibility(View.VISIBLE);
//        ll25d.setVisibility(View.VISIBLE);
//        llink.setVisibility(View.VISIBLE);
//        llmole.setVisibility(View.VISIBLE);
    }

    static public String FormatID(String id) {
        int id_len = id.length() ;

        for ( int i = id_len ; i < 5 ; i++ ) {
            id = '0' + id ;
        }
        return id ;
    }



    static public String KEY_TEST_CASE_BACKUP = "KEY_TEST_CASE_BACKUP" ;
    public void save_setting() {

        try {
            int result[] = {
                    // 順序使用此JAVA檔案的宣告順序
                    mSkinCaseIdx,
                    mAgeCaseIdx,
                    mGenderCaseIdx,
                    mFingerConditionCaseIdx,
                    mTemperatureCaseIdx,

                    mLightCaseIdx,
                    mPadCaseIdx,

                    mInkCaseIdx,
                    m2DMetrialIdx,
                    mMoleInx,
                    m25DMetrialIdx,
                    mCoverIdx } ;
                    JSONArray array = new JSONArray(result) ;
            Common.writeSharePerf(KEY_TEST_CASE_BACKUP, array.toString()) ;
        }
        catch (Exception ex) {
            Log.e(TAG, "save setting fail") ;
        }
    }

    public void load_setting() {

        try {
            String backupStringArray = Common.readSharePerf(KEY_TEST_CASE_BACKUP);
            JSONArray arr =  new JSONArray(backupStringArray) ;

            mSkinCaseIdx = arr.getInt(0) ;
            mAgeCaseIdx = arr.getInt(1) ;
            mGenderCaseIdx = arr.getInt(2) ;
            mFingerConditionCaseIdx = arr.getInt(3) ;
            mTemperatureCaseIdx = arr.getInt(4) ;

            mLightCaseIdx = arr.getInt(5) ;
            mPadCaseIdx = arr.getInt(6) ;

            mInkCaseIdx = arr.getInt(7) ;
            m2DMetrialIdx = arr.getInt(8) ;
            mMoleInx = arr.getInt(9) ;
            m25DMetrialIdx = arr.getInt(10) ;
            mCoverIdx = arr.getInt(11) ;
        }
        catch ( Exception ex) {
            Log.e(TAG, "Load Setting Fail, do not modify default setting") ;
        }
    }

}
