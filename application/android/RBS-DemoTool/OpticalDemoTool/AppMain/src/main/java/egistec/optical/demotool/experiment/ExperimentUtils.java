package egistec.optical.demotool.experiment;

import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import android.support.constraint.ConstraintLayout;
import android.util.DisplayMetrics;
//import android.util.Log;
import android.util.TypedValue;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.nio.ByteBuffer;

import egistec.optical.demotool.R;
import rbs.egistec.com.fplib.api.FingerprintReceiver;
import rbs.egistec.com.fplib.api.RbsLib;
import rbs.egistec.com.fplib.api.EgisLog;

import static egistec.optical.demotool.experiment.ExperimentDef.CMD_DEMOTOOL_SENSOR_POWER_OFF;
import static egistec.optical.demotool.experiment.ExperimentDef.CMD_DEMOTOOL_SENSOR_POWER_ON;
import static egistec.optical.demotool.experiment.ExperimentDef.CMD_IC_SET_DC_OFFSET;
import static egistec.optical.demotool.experiment.ExperimentDef.CMD_IC_SET_EXPOSURE_TIME;
import static egistec.optical.demotool.experiment.ExperimentDef.CMD_IC_SET_INTEGRATE_COUNT;
import static egistec.optical.demotool.experiment.ExperimentDef.PID_HOST_TOUCH;

public class ExperimentUtils {
    private static final String TAG = "ExperimentUtilsLog";

    public static String updateProgressMessage(int percentage, int processType) {
        String message = String.valueOf(percentage) + "%";
        String doneMessage = "100% ";
        switch (processType) {
            case ExperimentDef.processTypeCalibration:
                doneMessage += ExperimentDef.bkgDoneMessage;
                break;
            case ExperimentDef.processTypeCapture0:
                doneMessage += ExperimentDef.capDoneMessage0;
                break;
            case ExperimentDef.processTypeCapture1:
                doneMessage += ExperimentDef.capDoneMessage1;
                break;
            default:
                break;
        }
        return percentage == 100 ? doneMessage : message;
    }

    public static String findBackgroundImageName(int expTimeScale) {
        String resFileName = "";
        String[] nameList = FileIoUtils.getFileNameList(ExperimentDef.expBkgFileFolder);
        if (nameList == null)
            return resFileName;
        for (int i = 0; i < nameList.length; i++) {
            int end = nameList[i].indexOf("_");
            if (end > 0) {
                String imgIdx = nameList[i].substring(0, end);
                if (expTimeScale == Integer.parseInt(imgIdx)) {
                    resFileName = nameList[i];
                    break;
                }
            }
        }
        // make sure ext name is bin
        if (resFileName.substring(resFileName.length() - 3, resFileName.length()) != "bin")
            resFileName = resFileName.substring(0, resFileName.length() - 3) + "bin";
        EgisLog.d(TAG, "findBackgroundImageName expTimeScale = " + expTimeScale + "; name = " + resFileName);
        return resFileName;
    }

    public static String getFrameNoString(int frameNo) {
        return "FrameNo_" + Integer.toString(frameNo) + "_";
    }

    public static String getSaveExpImgName(Context context, int expTimeScale, boolean isBkg, boolean is16Bit, String extName) {
        String imgType = isBkg ? "BKG" : "CAP";
        String pixelType = is16Bit ? "16Bit" : "8Bit";
        return String.valueOf(expTimeScale) +
                "_" + imgType +
                "_Exp_" + String.valueOf(expTimeScale * ExperimentDef.expTimeBase) +
                "_Dc_"  + String.valueOf(0) +
                "_Int_" + String.valueOf(ExperimentSettingValueDB.getIntegrationCount(context)) +
                "_Bri_" + String.valueOf(ExperimentSettingValueDB.getBrightness(context)) +
                "_" + pixelType +
                extName;
    }

    public static String getOtherSettingString(Context context) {
        return  "UID = " + ExperimentSettingValueDB.getUserName(context) +
                "/Int = " + String.valueOf(ExperimentSettingValueDB.getIntegrationCount(context)) +
                "/Bri = " + String.valueOf(ExperimentSettingValueDB.getBrightness(context));
    }

    public static String getAllExpTimeString(Context context, TextView textView) {
        String expTimeString = textView.getText().toString();
        for (int i = ExperimentSettingValueDB.getExposureTimeScaleMin(context); i <= ExperimentSettingValueDB.getExposureTimeScaleMax(context); i++) {
            float expTime = ExperimentDef.expTimeBase * i;
            if (i == ExperimentSettingValueDB.getExposureTimeScaleMax(context))
                expTimeString += String.valueOf(expTime) + ".";
            else
                expTimeString += String.valueOf(expTime) + ", ";
        }
        return expTimeString;
    }


    /**
     * 改成用PIXEL為單位佳
     * @param sqContainer
     * @param left_Pixel
     * @param top_Pixel
     * @param width_Pixel
     * @param height_Pixel
     * @param metric
     */
    public static void setSQPressArea(RelativeLayout sqContainer,int left_Pixel, int top_Pixel, int width_Pixel, int height_Pixel, DisplayMetrics metric) {

        ViewGroup.LayoutParams layoutParams = sqContainer.getLayoutParams();
        if (layoutParams instanceof RelativeLayout.LayoutParams) {
            RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) layoutParams;
            params.setMargins(left_Pixel, top_Pixel, 0, 0);
            params.width = width_Pixel;
            params.height = height_Pixel;
            sqContainer.setLayoutParams(params);
        } else {
            ConstraintLayout.LayoutParams params = (ConstraintLayout.LayoutParams) layoutParams;
            params.setMargins(left_Pixel, top_Pixel, 0, 0);
            params.width = width_Pixel;
            params.height = height_Pixel;
            sqContainer.setLayoutParams(params);
        }
    }

    public static void setSQPressAreaDP(RelativeLayout sqContainer,int left_dp, int top_dp, int width_dp, int height_dp, DisplayMetrics metrics) {
        int left_Pixel= (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, left_dp, metrics);
        int top_Pixel= (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, top_dp, metrics);
        int width_Pixel= (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, width_dp, metrics);
        int height_Pixel= (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, height_dp, metrics);

        ViewGroup.LayoutParams layoutParams = sqContainer.getLayoutParams();
        if (layoutParams instanceof RelativeLayout.LayoutParams) {
            RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) layoutParams;
            params.setMargins(left_Pixel, top_Pixel, 0, 0);
            params.width = width_Pixel;
            params.height = height_Pixel;
            sqContainer.setLayoutParams(params);
        } else {
            ConstraintLayout.LayoutParams params = (ConstraintLayout.LayoutParams) layoutParams;
            params.setMargins(left_Pixel, top_Pixel, 0, 0);
            params.width = width_Pixel;
            params.height = height_Pixel;
            sqContainer.setLayoutParams(params);
        }
    }

    public static void setPressAreaLeftTop(RelativeLayout gifContainer,int left_dp, int top_dp, DisplayMetrics metrics) {
        int left_Pixel= (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, left_dp, metrics);
        int top_Pixel= (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, top_dp, metrics);

        ViewGroup.LayoutParams layoutParams = gifContainer.getLayoutParams();
        if (layoutParams instanceof RelativeLayout.LayoutParams) {
            RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) layoutParams;
            params.setMargins(left_Pixel, top_Pixel, 0, 0);
            gifContainer.setLayoutParams(params);
        } else {
            ConstraintLayout.LayoutParams params = (ConstraintLayout.LayoutParams) layoutParams;
            params.setMargins(left_Pixel, top_Pixel, 0, 0);
            gifContainer.setLayoutParams(params);
        }
    }

    public static void setPressAreaWidthHeight(RelativeLayout gifContainer, Button mainVerifyTouch, int touch_dp, int gif_dp, DisplayMetrics metrics) {
        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams)mainVerifyTouch.getLayoutParams();
        int width_Pixel= (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, touch_dp, metrics);
        int height_Pixel= (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, touch_dp, metrics);
        params.width = width_Pixel;
        params.height = height_Pixel;
        mainVerifyTouch.setLayoutParams(params);

        params = (RelativeLayout.LayoutParams)gifContainer.getLayoutParams();
        width_Pixel= (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, gif_dp, metrics);
        height_Pixel= (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, gif_dp, metrics);
        params.width = width_Pixel;
        params.height = height_Pixel;
        gifContainer.setLayoutParams(params);
    }

    public static void initializeRbsLib(RbsLib rbsLib, FingerprintReceiver fingerprintReceiver) {
        if (rbsLib != null) {
            rbsLib.startListening(fingerprintReceiver);
            rbsLib.extraApi(PID_HOST_TOUCH, CMD_DEMOTOOL_SENSOR_POWER_ON, null, null, null);
        }
    }

    public static void uninitializeRbsLib(RbsLib rbsLib) {
        if (rbsLib != null) {
            rbsLib.cancel();
            rbsLib.extraApi(PID_HOST_TOUCH, CMD_DEMOTOOL_SENSOR_POWER_OFF, null, null, null);
        }
    }

    /* Note: need to set exp time before call calibration, verify, enroll */
    public static void initializeRbsSettings(RbsLib rbsLib, float expTime, int intCount) {
        if (rbsLib == null)
            return;
        rbsLib.extraApi(PID_HOST_TOUCH, CMD_IC_SET_EXPOSURE_TIME, ByteBuffer.allocate(4).putInt((int)(expTime * 100)).array(), null, null);
        rbsLib.extraApi(PID_HOST_TOUCH, CMD_IC_SET_INTEGRATE_COUNT, ByteBuffer.allocate(4).putInt(intCount).array(), null, null);
        rbsLib.extraApi(PID_HOST_TOUCH, CMD_IC_SET_DC_OFFSET, ByteBuffer.allocate(4).putInt(0).array(), null, null);
    }

    public static void changeAppBrightness(Context context, int brightness) {
        Window window = ((Activity) context).getWindow();
        WindowManager.LayoutParams layout = window.getAttributes();
        if(brightness >= 100) brightness = 100;
        if(brightness < 0) brightness = 0;
        float setBright = (float)brightness / 100.0f;
        layout.screenBrightness = setBright;
        window.setAttributes(layout);
    }

    public static void waitForTimePeriod(int millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static int getPressViewId(int pressColorId) {
        int viewId = R.id.enroll_gif2;
        switch (pressColorId) {
            case ExperimentDef.COLOR_WHITE:
                viewId = R.id.enroll_gif2;
                break;
            case ExperimentDef.COLOR_GREEN:
                viewId = R.id.enroll_gif_green;
                break;
            case ExperimentDef.COLOR_BLUE:
                viewId = R.id.enroll_gif_blue;
                break;
            case ExperimentDef.COLOR_AQUA:
                viewId = R.id.enroll_gif_aqua;
                break;
            case ExperimentDef.COLOR_RED:
                viewId = R.id.enroll_gif_red;
                break;
            default:
                break;
        }
        return viewId;
    }
}
