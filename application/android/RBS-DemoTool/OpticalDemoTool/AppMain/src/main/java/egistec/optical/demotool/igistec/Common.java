package egistec.optical.demotool.igistec;


import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;

import egistec.optical.demotool.lib.FileObj;
import egistec.optical.demotool.lib.MainFileSession;


/**
 * do something COMMON
 */
public class Common {

    public static final int MATCHER_THRESHOLD = 1000;
    public static final int MATCHER_THRESHOLD_DOUBLE = 500;

    static public String TAG = "RBS-Common" ;
    static public Activity act;

    static public boolean TRIGGER_AFTER_BURN = true ; // 設定 TRIGGER後必定BURN一次

    static public boolean isNoSpi = false ;

    static public MainFileSession mainFileSession ;

    // 1022 最新的
    //static public int GLOBAL_BIN_CODE_ID = R.raw.ep3545_1022 ;

    public static byte[] intToByteArray(int value)
    {
        byte[] ret = new byte[4];
        ret[3] = (byte) (value & 0xFF);
        ret[2] = (byte) ((value >> 8) & 0xFF);
        ret[1] = (byte) ((value >> 16) & 0xFF);
        ret[0] = (byte) ((value >> 24) & 0xFF);
        return ret;
    }

    public static int byteArrayToInt(byte[] value)
    {
        return (value[3] & 0xFF) + ((value[2] & 0xFF) << 8) + ((value[1] & 0xFF) << 16) + ((value[0] & 0xFF) << 24);
    }

    public static int byteArrayToInt(int offset, byte[] value)
    {
        return (value[3+offset] & 0xFF) + ((value[2+offset] & 0xFF) << 8) + ((value[1+offset] & 0xFF) << 16) + ((value[0+offset] & 0xFF) << 24);
    }

    public static String byteArrayToString(byte[] array) {

        String output = "" ;
        for ( int i = 0 ; i < array.length ; i++ ) {
            output += Integer.toHexString(array[i] & 0x000000FF ) + ",";
        }
        return output;
    }


    static public long getSum(int frame[][]) {

        if (frame == null ) {
            return 0 ;
        }
        long sum = 0 ;
        for ( int i = 0 ; i < frame.length ; i++ ) {
            for ( int j = 0 ; j < frame[0].length ; j++ ) {
                sum = sum + frame[i][j] ;
            }
        }
        return sum ;
    }


    static public double getAVG(int frame[][] ) {
        if ( frame != null ) {
            return getSum(frame) / (frame.length * frame[0].length);
        }
        else {
            return 0 ;
        }
    }

    static public void sleep(int ms) {
        try {
            Thread.sleep(ms) ;
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * Fuck Hide Navi Bar, for S10, 仍然需要多實驗
     */
    static public void hideSystemUI_S10(Activity act) {
        act.getWindow().getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        //| View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION // hide nav bar
                        | View.SYSTEM_UI_FLAG_FULLSCREEN // hide status bar
                        | View.SYSTEM_UI_FLAG_IMMERSIVE);

    }

    static public void hideSystemUI_S10_Test(Activity act) {
        if (act.getActionBar() != null) {
            act.getActionBar().hide();
        }
        act.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        act.getWindow().getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        //| View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION // hide nav bar
                        | View.SYSTEM_UI_FLAG_FULLSCREEN // hide status bar??
                        | View.SYSTEM_UI_FLAG_IMMERSIVE);
    }


    /**
     * Fuck Android Status Bar
     * 只藏STATUS不髒MENU
     */
    static public void hideSystemUI_Default(Activity act) {

        act.getWindow().getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_FULLSCREEN // hide status bar
                        | View.SYSTEM_UI_FLAG_IMMERSIVE);
    }

    /**
     * Fuck Android Status Bar
     * 只藏STATUS不髒MENU
     */
    static public void hideSystemUI(Activity act) {

        // if default:
        // hideSystemUI_Default(act) ;

        // if S10
        hideSystemUI_S10_Test(act);
    }


    /**
     * 把FRAME 反向
     *
     */
    static public int [][] copy(int [][] f) {
        int [][] ret = new int [f.length][f[0].length] ;

        for ( int h= 0 ; h < f.length ; h++ ) {
            for ( int w = 0 ; w < f[0].length ; w++ ) {
                ret[h][w] = f[h][w] ;
            }
        }
        return ret ;
    }

    static public int [][] avgFrame(ArrayList<int[][]> frameList) {

        if ( frameList == null ) {
            return null ;
        }
        if ( frameList.size() == 0 ) {
            return null ;
        }
        else {
            int [][] avgBack1 = frameList.get(0) ;
            int [][] avgFrame = new int[avgBack1.length][avgBack1[0].length] ;
            for (int f = 0 ; f < frameList.size() ; f++ ) {
                int [][]currentFrame = frameList.get(f) ;

                for ( int w = 0 ; w < avgFrame.length ; w++ ) {
                    for ( int h = 0 ; h < avgFrame[0].length ; h++ ) {
                        avgFrame[w][h] = avgFrame[w][h] + currentFrame[w][h] ;
                    }
                }
            }

            for ( int w = 0 ; w < avgFrame.length ; w++ ) {
                for ( int h = 0 ; h < avgFrame[0].length ; h++ ) {
                    avgFrame[w][h] = avgFrame[w][h] / frameList.size() ;
                }
            }

            Log.i(TAG, "取得平均背 !!! avg=" + Common.getAVG(avgFrame)) ;
            return avgFrame ;
        }

    }


    /**
     * 中位數FRAME
     * 奇數取中間，偶數中間二個平均
     *
     * @param frameList
     * @return
     */
    static public int [][] middleFrame(ArrayList<int[][]> frameList) {

        if ( frameList == null ) {
            return null ;
        }
        if ( frameList.size() == 0 ) {
            return null ;
        }
        else {
            int [][] avgBack1 = frameList.get(0) ;
            int [] toSort = new int [frameList.size()] ;
            int [][] middleFrame = new int[avgBack1.length][avgBack1[0].length] ;

            int width = frameList.get(0).length ;
            int height = frameList.get(0)[0].length ;
            for ( int w = 0 ; w < width; w++ ) {
                for ( int h = 0 ; h < height ; h++ ) {
                    for (int f = 0 ; f < frameList.size() ; f++ ) {
                        toSort[f] = frameList.get(f)[w][h] ;
                    }

                    Arrays.sort(toSort) ;
                    if ( frameList.size() % 2 == 0 ) {
                        middleFrame[w][h] = (toSort[ toSort.length / 2 ] + toSort[ toSort.length / 2 + 1 ])/2 ;
                    }
                    else {
                        middleFrame[w][h] = toSort[ toSort.length / 2 ] ;
                    }

                }
            }
            Log.i(TAG, "取得中位數背 !!! middle=" + Common.getAVG(middleFrame)) ;
            return middleFrame ;
        }

    }


    /**
     * 註 Y在前 X在後 SESNOR座標
     */
    static public int [][] getPartArray(int [][] src, int start_x, int start_y, int width, int height) {

        int [][] back = new int[height][width] ;
        try {

            Log.d(TAG, "src wh=" + src.length + "," + src[0].length) ;
            int t ;
            int base_x = start_x ;
            int base_y = start_y ;
            for ( int y = 0 ; y < back.length ; y++ ) {
                for ( int x = 0 ; x < back[0].length ; x++ ) {
                    t = src[base_y + y][base_x + x] ;
                    back[y][x] = t ;
                }
            }
        }
        catch (Exception ex) {
            Log.e(TAG,"Get Part Back fail ex=" + ex.getMessage() ) ;
        }



        return back ;
    }


    /**
     * put array to larger array
     */
    static public void putPartArray(int [][] dest, int [][] src, int start_x, int start_y) {
        try {
            for ( int h = 0 ; h < src.length ; h++ ) {
                for ( int w = 0 ; w < src[0].length ; w++ ) {
                    dest[start_y+h][start_x+w] = src[h][w] ;
                }
            }
        }
        catch (Exception ex) {
            Log.e(TAG,"Put Part Back fail ex=" + ex.getMessage() ) ;
        }
    }

    static public Bitmap justNormalizeImage( int [][] frame) {
        int avg = (int) Common.getAVG(frame) ;

//        int lower = avg - avg / 5 ;
//        int upper = avg + avg / 5 ;

        int step = 500 ;

        int lower = avg - step ;
        int upper = avg + step ;

        return ArrToBitmap(frame, frame[0].length, frame.length,null,lower, upper, null) ;
    }


    /**
     * 排序後，去除極值 上下 x 比例
     * @param frame
     */
    static public void hack_normalize(int [][] frame) {
        double precent = 0.95;
        int avg = (int) Common.getAVG(frame) ;

        int len = frame.length * frame[0].length;

        int [] pixel1d = new int[len] ;
        int p = 0 ;
        for ( int h = 0 ; h < frame.length ; h++ ) {
            for ( int w = 0 ; w < frame[0].length ; w++) {
                pixel1d[p++] = frame[h][w] ;
            }
        }
        Arrays.sort(pixel1d); // 會由小而大排列
        int upper = pixel1d[ (int)(len * (precent))] ; // 後5%
        int lower = pixel1d[ (int)(len * (1-precent))] ; // 前5%

        Log.e(TAG, "Hack normalize: up/low = " + upper + "/" + lower + " avg=" + avg) ;
        // 在前後5%的通通變成5%
        for ( int h = 0 ; h < frame.length ; h++ ) {
            for ( int w = 0 ; w < frame[0].length ; w++) {
                if (frame[h][w] < lower ) {
                    frame[h][w] = lower ;
                }
                if (frame[h][w] > upper ) {
                    frame[h][w] = upper ;
                }
            }
        }


    }

    static public Bitmap justNormalizeImage( int [][] frame, int range) {
        int avg = (int) Common.getAVG(frame) ;

//        int lower = avg - avg / 5 ;
//        int upper = avg + avg / 5 ;

        int step = range ;

        int lower = avg - step ;
        int upper = avg + step ;

        return ArrToBitmap(frame, frame[0].length, frame.length,null,lower, upper, null) ;
    }

    static public Bitmap justNormalizeImage( int [][] frame, int upper, int lower) {
        return ArrToBitmap(frame, frame[0].length, frame.length,null,lower, upper, null) ;
    }

    /**
     * ARR TO BITMAP WITH LOWER AND UPPER
     * lower = 0, upper = 65536 同等 256 BIT
     * @param arr
     * @param b
     * @param lower
     * @param upper
     * @param offset, if NULL than do not use
     */
    static public Bitmap ArrToBitmap(int [][] arr,int panel_width, int panel_height, Bitmap b, int lower, int upper, int [][] offset ) {

        int [] buffer = new int[arr.length * arr[0].length] ;
        int [][] lastArray = new int[arr.length][arr[0].length] ;

        Bitmap bitmap ;
        if ( b == null ) {
            Bitmap.Config conf = Bitmap.Config.ARGB_8888; // see other conf types
            bitmap = Bitmap.createBitmap(panel_width, panel_height, conf); // this creates a MUTABLE bitmap
        }
        else {
            bitmap = b;
        }

        bitmap.getPixels(buffer,0,panel_width,0,0,panel_width,panel_height) ;

        int value ;
        int range = upper - lower ;
        if ( range == 0 ) {
            range = 1 ;
            upper = upper + 1 ;
        }

        for ( int h = 0 ; h < panel_height ; h++ ){
            for ( int w = 0 ; w < panel_width ; w++ ) {
                if ( offset != null ) {
                    value = arr[h][w] - offset[h][w];
                }
                else {
                    value = arr[h][w] ;
                }

                if ( value < lower ) { value = lower ; }
                else if ( value > upper ) { value = upper ; }

                value = value - lower ;
                value = 255 * value / range ;
                value = value & 0xFF ;
                buffer[ h * panel_width + w] = 0xFF000000 | value << 16 | value << 8 | value;
                lastArray[h][w] = buffer[ h * panel_width + w] ;
                // temp debug ;
                //pixels[h*panel_width+w] = arr[h][w] ;
            }
        }
        //Log.e("PANEL", "PIXEL ROOT=" + Integer.toHexString(pixels[0])) ;
        bitmap.setPixels(buffer,0,panel_width,0,0,panel_width, panel_height);

        return bitmap ;
    }



    static public void drawOnFull(Bitmap target, int left, int top, Bitmap part) {
        Canvas canvas = new Canvas(target);

        // 注意 LEFT TOP 反向
        canvas.drawBitmap(part, left, top, null );
    }

    static public Bitmap flipBitmapHori(Bitmap b) {
        if ( b != null) {
            Log.d(TAG, "Flip Image Hori");
            Matrix matrix = new Matrix();
            matrix.postScale(-1, 1, b.getWidth() / 2, b.getHeight() / 2);
            return Bitmap.createBitmap(b, 0, 0, b.getWidth(), b.getHeight(), matrix, true);
        }
        return b ;
    }

    static public Bitmap rotate(Bitmap b, int degree) {
        Matrix matrix = new Matrix();
        matrix.postRotate(degree);
        Bitmap rotatedBitmap = Bitmap.createBitmap(b, 0, 0, b.getWidth(), b.getHeight(), matrix, true);
        return rotatedBitmap ;
    }


    static public void arrayMul(int [][]arr, int mul) {

        for ( int i = 0 ; i < arr.length ; i++ ) {
            for (int j = 0 ; j < arr[0].length ; j++ ) {
                arr[i][j] = arr[i][j] * mul ;
            }
        }
    }

    static public void arrayAdd(int [][]arr, int add) {
        for ( int i = 0 ; i < arr.length ; i++ ) {
            for (int j = 0 ; j < arr[0].length ; j++ ) {
                arr[i][j] = arr[i][j] + add ;
            }
        }
    }

    static public int [][] flipArray(int [][] arr) {

        int [][] flip = new int[arr[0].length][arr.length] ;
        for ( int x = 0 ; x < arr.length ; x++ ) {
            for ( int y = 0 ; y < arr[0].length ; y++ ) {
                flip[y][x] = arr[x][y] ;
            }
        }
        return flip ;
    }




    static public boolean writeSharePerf(String tag, int data)
    {
        return writeSharePerf(tag, data + "") ;
    }

    static public boolean writeSharePerf(String tag, String data)
    {
        if ( data == null ) return false ;

        try {
            SharedPreferences pref = act.getSharedPreferences(tag, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = pref.edit() ;
            editor.putString(tag, data ) ;
            editor.commit() ;
            editor.apply() ;
            return true;
        }
        catch (Exception ex)
        {
            Log.e("SharePref" , "存檔失敗" + ex.getMessage()) ;
            return false ;
        }

    }

    static public String readSharePerf( String tag)
    {
        SharedPreferences pref = act.getSharedPreferences(tag, Context.MODE_PRIVATE);
        String data = pref.getString(tag, "") ;

        if ( data.equalsIgnoreCase(""))
            return null ;
        else
            return data ;
    }


    static public int readSharePerfInt( String tag, int def)
    {
        SharedPreferences pref = act.getSharedPreferences(tag, Context.MODE_PRIVATE);
        String data = pref.getString(tag, "") ;

        if ( data.equalsIgnoreCase(""))
            return def ;
        else
            return Integer.parseInt(data) ;
    }


    static public String tempTitle, tempMsg ;
    static public void showAlert(Activity aa, String title, String msg)
    {
        tempTitle = title ;
        tempMsg = msg ;
        final Activity f_aa = aa ;
        aa.runOnUiThread( new Runnable() {

            @Override
            public void run() {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(f_aa);

                alertDialog.setTitle(tempTitle);
                alertDialog.setMessage( tempMsg );

                alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int which) {
                        dialog.cancel();
                        Common.hideSystemUI(f_aa);
                    }
                });

                alertDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialogInterface) {
                        Common.hideSystemUI(f_aa);
                    }
                });

                alertDialog.show() ;
            }
        });
    }





    /**
     * 把UNSIGNED INT 16 轉為 byte arr
     * @param frame200
     * @param isBigEndian
     * @return
     */
    public static byte[] toByteArray(int[][] frame200, boolean isBigEndian ) {

        int len = frame200.length * frame200[0].length ;
        int p = 0 ;
        byte [] b = new byte[len*2] ;


        if ( isBigEndian ) {
            for ( int h = 0 ; h < frame200.length ; h++ ) {
                for ( int w = 0 ; w < frame200[0].length ; w++ ) {
                    b[p] = (byte) ((frame200[h][w] & 0xFF00) >> 8 ) ;
                    p++ ;
                    b[p] = (byte) (frame200[h][w] & 0x00FF) ;
                    p++ ;
                }
            }
        }
        else {
            for ( int h = 0 ; h < frame200.length ; h++ ) {
                for ( int w = 0 ; w < frame200[0].length ; w++ ) {
                    b[p] = (byte) (frame200[h][w] & 0x00FF) ;
                    p++ ;
                    b[p] = (byte) ((frame200[h][w] & 0xFF00) >> 8 ) ;
                    p++ ;

                }
            }
        }

        return b ;
    }


    /**
     * 故意要轉90 度跟SCREEN 一樣
     * @param frame
     * @return
     */
    public static int [][] rotateFrame(int [][] frame) {

        int [][] ret = new int[frame[0].length][frame.length] ;

        for ( int h = 0 ; h < frame.length ; h++ ) {
            for ( int w = 0 ; w < frame[0].length ; w++ ) {
                //ret[w][h] = frame[h][frame[0].length - w - 1] ;
                ret[w][h] = frame[frame.length - h - 1][w] ;
            }
        }
        return ret ;
    }

    public static Bitmap merge(Bitmap bmp1, Bitmap bmp2) {
        Bitmap bmOverlay = Bitmap.createBitmap(bmp1.getWidth()*2, bmp1.getHeight(), bmp1.getConfig());

        Paint paint = new Paint() ;
        paint.setColor(Color.GREEN);
        Canvas canvas = new Canvas(bmOverlay);
        canvas.drawBitmap(bmp1, 0, 0, null);
        canvas.drawBitmap(bmp2, bmp2.getWidth(), 0, null);
        return bmOverlay;
    }

    static public boolean saveFile(String path, int [][] frame) {
        return saveFile(path, Common.toByteArray(frame, false)) ;
    }

    static public boolean saveFile(String path, byte [] img) {
        Log.d(TAG, "debug_check path = " + path);
        File file = new File(path);
        if (!file.getParentFile().exists()) {
            boolean makeDir = file.getParentFile().mkdirs();
            if (!makeDir)  return false;
        }
        FileObj fileObj = new FileObj(file, img);
        if(mainFileSession != null) {
            mainFileSession.getSaveFileHelper().Save(fileObj);
        }
        return true;
    }

    public static Bitmap getBitmapByGaryScale(int currentColor, int w, int h) {
        Bitmap b = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888) ;
        Paint p = new Paint() ;
        b.eraseColor(Color.rgb(currentColor, currentColor, currentColor)) ;
        return b;
    }

    public boolean deleteFile(String path) {
        File fdelete = new File(path);
        if (fdelete.exists()) {
            fdelete.delete();
        }
        return true;
    }

    private void getBkgImages() {
//        if(mstate == state.idle) {
//            deleteFile("/sdcard/RbsG5Temp/image_ffc/bkg_bbkg.bin");
//            deleteFile("/sdcard/RbsG5Temp/image_ffc/bkg_wbkg.bin");
//            deleteFile("/sdcard/RbsG5Temp/image_ffc/bkg_data.bin");
//            mLib.extraApi(ExperimentDef.PID_BKG_IMG, ExperimentDef.CMD_BKG_DEL_FFC, null, null, null);
//            Toast.makeText(LiveViewActivity.this, "black",Toast.LENGTH_SHORT).show();
//            mstate = state.black;
//            try {
//                Thread.sleep(1000);
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
//        }
//        else if(mstate == state.black) {
//            offset = 0;
//            for (int i = 0; i < frames; i++) {
//                imageInfo = liveImageViewer.runGetLiveImageInfo(true);
//                byte [] image = imageInfo.getRawImage(false);
//                System.arraycopy(image, 0, image16, offset, image.length);
//                saveFile("/sdcard/RbsG5Temp/image_ffc/bkg_" + i + ".bin", image);
//                offset += image.length;
//                try {
//                    Thread.sleep(100);
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }
//            }
//            byte []out_buffer = new byte[width * height * 2];
//            int []out_buffer_size = new int[1];
//            mLib.extraApi(ExperimentDef.PID_BKG_IMG, ExperimentDef.CMD_BKG_BBKG_FFC, image16, out_buffer, out_buffer_size);
//            saveFile("/sdcard/RbsG5Temp/image_ffc/bkg_bbkg.bin", out_buffer);
//            Toast.makeText(LiveViewActivity.this, "white",Toast.LENGTH_SHORT).show();
//            mstate = state.white;
//        }
//        else if(mstate == state.white) {
//            offset = 0;
//            for (int i = 0; i < frames; i++) {
//                imageInfo = liveImageViewer.runGetLiveImageInfo(true);
//                byte [] image = imageInfo.getRawImage(false);
//                System.arraycopy(image, 0, image16, offset, image.length);
//                saveFile("/sdcard/RbsG7Temp/image_ffc/wbkg_" + i + ".bin", image);
//                offset += image.length;
//                try {
//                    Thread.sleep(100);
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }
//            }
//            byte []out_buffer = new byte[width * height * 2];
//            int []out_buffer_size = new int[1];
//            mLib.extraApi(ExperimentDef.PID_BKG_IMG, ExperimentDef.CMD_BKG_WBKG_FFC, image16, out_buffer, out_buffer_size);
//            saveFile("/sdcard/RbsG7Temp/image_ffc/bkg_wbkg.bin", out_buffer);
//            Toast.makeText(LiveViewActivity.this, "image",Toast.LENGTH_SHORT).show();
//            mstate = state.image;
//            offset = 0;
//        }
//        else if(mstate == state.image) {
//            try {
//                Thread.sleep(1000);
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
//            imageInfo = liveImageViewer.runGetLiveImageInfo(true);
//            byte [] image = imageInfo.getRawImage(false);
//            System.arraycopy(image, 0, image16, offset, image.length);
//            offset += image.length;
//            saveFile("/sdcard/RbsG7Temp/image_ffc/image_" + image_index + ".bin", image);
//            if (image_index == (frames - 1)) {
//                byte []out_buffer = new byte[480064];
//                int []out_buffer_size = new int[1];
//                mLib.extraApi(ExperimentDef.PID_BKG_IMG, ExperimentDef.CMD_BKG_IMG_FFC, image16, out_buffer, out_buffer_size);
//                saveFile("/sdcard/RbsG7Temp/image_ffc/bkg_data.bin", out_buffer);
//                Toast.makeText(LiveViewActivity.this, "finished", Toast.LENGTH_SHORT).show();
//                mstate = state.idle;
//                offset = 0;
//            }
//            else {
//                Toast.makeText(LiveViewActivity.this, "got " + (image_index + 1) + " image(s)", Toast.LENGTH_SHORT).show();
//                image_index++;
//            }
//        }

    }



}
