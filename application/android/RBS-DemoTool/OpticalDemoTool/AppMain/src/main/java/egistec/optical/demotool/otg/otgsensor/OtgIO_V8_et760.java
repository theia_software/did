package egistec.optical.demotool.otg.otgsensor;

import android.hardware.usb.UsbDeviceConnection;
import android.hardware.usb.UsbEndpoint;
import android.util.Log;

import egistec.optical.demotool.igistec.Common;
import egistec.optical.demotool.igistec.SPIUtility;

public class OtgIO_V8_et760 implements IOtgIO {
    private static final String TAG = "OTG-IOV8-ET760";
    UsbDeviceConnection conn;
    UsbEndpoint epIn;
    UsbEndpoint epOut;

    static public int MAX_LEN = 16384 ; // 16 * 1024 ;

    //static public OtgIO_V8_et760 debug = null ;

    static public byte[] dummy = new byte[512] ; // 純DUMMY使用，不要拿來存值也不要讀 它是依照原SCSI COMMAND最低的512 BYTE

    OtgIO_V8_et760(UsbDeviceConnection conn, UsbEndpoint epin, UsbEndpoint epout){
        Log.i(TAG, "create OtgIO_V8_et760");

        this.conn = conn;
        this.epIn = epin;
        this.epOut = epout;

        SPIUtility.otgio = this ; // 設定GLOBAL後其他人才可跑

        //SPIUtility.testSPIWorking() ;
        //if ( SPIUtility.testSPIWorking()) {
            // todo: 待接回主要RBS後，穩定後才在這裡燒CODE
//            if ( BurnBinCode.BURN_CODE_WHEN_PLUG ) {
//                boolean ret = BurnBinCode.burn(Common.act, Common.GLOBAL_BIN_CODE_ID) ;
//                if ( ret == false ){
//                    Common.showAlert("Error", "Burn Code Fail請檢查SPI或重插USB" ) ;
//                }
//                else {
//                    Toast.makeText(Common.act, "自動BURN CODE成功", Toast.LENGTH_LONG).show();
//                }
//            }
//        }
//        else {
//            //Common.showAlert("Error", "USB接觸不良 請重插USB") ;
//        }
    }

    public void testWrite8() {
        byte [] b = new byte[10] ;

        WriteRegister((short) 0x0F0F, (byte) 0x0F0F ) ;
        WriteRegister((short) 0xF0F0, (byte) 0xF0F0 ) ;
//        for ( int i = 0 ; i < 1 ;i++ ) {
//            int retR = WriteRegister((short) i, (byte) i ) ;
//            int retW = ReadRegister((short) i, b) ;
//            Log.e(TAG, "ReadReg ret=" + retW + "," + retR + " value=" + i + " return=" + b[0]) ;
//        }
    }


    @Override
    public int ReadRegister(short address, byte[] value) {

        //Log.e(TAG, "Read Register Start !! addr=" + Integer.toHexString(address)) ;
        if (epIn == null || epOut == null || conn == null) {
            return RESULT_FAIL;
        }

        SCSICommandBlock cmdBlock = new SCSICommandBlock(512, (byte) 0x80);
        byte[] commandBlock = new byte[16];
        commandBlock[0] = (byte) 0xE0;
        commandBlock[1] = (byte) ((address & 0xff00) >> 8);
        commandBlock[2] = (byte) (address & 0x00ff);
        cmdBlock.setCommandBlock(commandBlock);
        byte[] command = cmdBlock.getCBWBuffer();

        int response = conn.bulkTransfer(epOut, command, command.length, 500);

        byte status = 0;
        if (response == 31) {
            byte[] dataBuffer = new byte[512];
            response = conn.bulkTransfer(epIn, dataBuffer, dataBuffer.length, 500);
            value[0] = dataBuffer[0];
            if (response != 512) status = 1;
        } else {
            status = 1;
        }

        SCSICommandStatus cmdStatus = new SCSICommandStatus(response, status);
        byte[] commandStatus = cmdStatus.getCSWBuffer();

        response = conn.bulkTransfer(epIn, commandStatus, commandStatus.length, 500);

        // if (response == 13)
        return RESULT_OK;
        // else
        //    return -1;
    }

    @Override
    public int WriteRegister(short address, byte value) {

        //Log.d(TAG, "Write Register Start !! addr=" + Integer.toHexString(address) + " value=" + value) ;

        if (epIn == null || epOut == null || conn == null) {
            return RESULT_FAIL;
        }

        SCSICommandBlock cmdBlock = new SCSICommandBlock(512, (byte) 0x00);
        byte[] commandBlock = new byte[16];
        commandBlock[0] = (byte) 0xE1;
        commandBlock[1] = (byte) 0x01;
        commandBlock[2] = (byte) ((address & 0xff00) >> 8);
        commandBlock[3] = (byte) (address & 0x00ff);
        commandBlock[4] = value;
        cmdBlock.setCommandBlock(commandBlock);
        byte[] command = cmdBlock.getCBWBuffer();
        // Log.d(TAG, "TM_WriteRegister bulkTransfer 1 start");
        //Log.d(TAG, "command length=" + command.length + " raw=" + Common.byteArrayToString(command)) ;
        int response = conn.bulkTransfer(epOut, command, command.length, 500);
        // Log.d(TAG, "TM_WriteRegister bulkTransfer 1 end");
        byte status = 0;
        if (response == 31) {
            byte[] dataBuffer = new byte[512];
            dataBuffer[0] = 0x01;
            dataBuffer[1] = (byte) ((address & 0xff00) >> 8);
            dataBuffer[2] = (byte) (address & 0x00ff);
            dataBuffer[3] = value;

            // Log.d(TAG, "TM_WriteRegister bulkTransfer 2 start");
            response = conn.bulkTransfer(epOut, dataBuffer, dataBuffer.length, 500);
            // Log.d(TAG, "TM_WriteRegister bulkTransfer 2 end");

            if (response != 512)
                status = 1;
        } else {
            status = 1;
        }

        SCSICommandStatus cmdStatus = new SCSICommandStatus(response, status);
        byte[] commandStatus = cmdStatus.getCSWBuffer();

        // Log.d(TAG, "TM_WriteRegister bulkTransfer 3 start");
        response = conn.bulkTransfer(epIn, commandStatus, commandStatus.length, 500);
        // Log.d(TAG, "TM_WriteRegister bulkTransfer 3 end");

        if (response == 13)
            return RESULT_OK;
        else
            return RESULT_FAIL;
    }


    /**
     * 轉換器
     * @param a
     * @return
     */
    public int readSingle32(int a) {
        byte [] b = new byte[4] ;
        int ret = ReadRegister32(a,b) ;
        if ( ret == RESULT_FAIL ) {
            Log.d(TAG, "Read Single 32 FAIL") ;
        }
        return Common.byteArrayToInt(b) ;
    }


    /**
     * 同名用轉換 -___-
     * @param a
     * @param v
     * @return
     */
    public int writeSingle32(int a, int v) {
        int ret = WriteRegister32(a, v) ;
        if ( ret == RESULT_FAIL ) {
            Log.d(TAG, "Write Single 32 FAIL") ;
        }
        return ret ;
    }

    @Override
    public int ReadRegister32(int address, byte[] value) {

        //Log.d(TAG, "Read Register32 Start !! addr=" + Integer.toHexString(address)) ;
        if (epIn == null || epOut == null || conn == null) {
            return RESULT_FAIL;
        }

        SCSICommandBlock cmdBlock = new SCSICommandBlock(512, (byte) 0x80);
        byte[] commandBlock = new byte[16];
        commandBlock[0] = (byte) 0xE4;


        byte [] bAddr = Common.intToByteArray(address) ;

        for (int i = 0 ; i < 4 ; i++ ) {
            commandBlock[i + 1] = bAddr[i] ;
        }
        cmdBlock.setCommandBlock(commandBlock);
        byte[] command = cmdBlock.getCBWBuffer();

        int response = conn.bulkTransfer(epOut, command, command.length, 500);

        byte status = 0;
        if (response == 31) {
            byte[] dataBuffer = new byte[512];
            response = conn.bulkTransfer(epIn, dataBuffer, dataBuffer.length, 500);
            value[0] = dataBuffer[0];
            value[1] = dataBuffer[1];
            value[2] = dataBuffer[2];
            value[3] = dataBuffer[3];
            if (response != 512) status = 1;
        } else {
            status = 1;
        }

        SCSICommandStatus cmdStatus = new SCSICommandStatus(response, status);
        byte[] commandStatus = cmdStatus.getCSWBuffer();

        response = conn.bulkTransfer(epIn, commandStatus, commandStatus.length, 500);

        if (response == 13)
            return RESULT_OK;
        else
            return -1;
    }

    @Override
    public int WriteRegister32(int address, int value) {
        //Log.d(TAG, "Write Register32 Start !! addr=" + Integer.toHexString(address) + " value=" + value) ;

        if (epIn == null || epOut == null || conn == null) {
            return RESULT_FAIL;
        }

        SCSICommandBlock cmdBlock = new SCSICommandBlock(512, (byte) 0x00);
        byte[] commandBlock = new byte[16];
        commandBlock[0] = (byte) 0xE5;
        //commandBlock[1] = (byte) 0x01;  // unknow block


        byte [] bAddr = Common.intToByteArray(address) ;
        byte [] bValue = Common.intToByteArray(value ) ;

        for (int i = 0 ; i < 4 ; i++ ) {
            commandBlock[i + 1] = bAddr[i];
            //commandBlock[i + 6] = bValue[i];
        }
        //Log.d(TAG, "commandBlock=" + Common.byteArrayToString(commandBlock));


        cmdBlock.setCommandBlock(commandBlock);
        byte[] command = cmdBlock.getCBWBuffer();
        // Log.d(TAG, "TM_WriteRegister bulkTransfer 1 start");
        int response = conn.bulkTransfer(epOut, command, command.length, 500);
        // Log.d(TAG, "TM_WriteRegister bulkTransfer 1 end");
        byte status = 0;
        if (response == 31) {
            byte[] dataBuffer = new byte[512];
            //dataBuffer[0] = 0x01;
            for (int i = 0 ; i < 4 ; i++ ) {
                dataBuffer[i] = bValue[i];
            }

            // Log.d(TAG, "TM_WriteRegister bulkTransfer 2 start");
            response = conn.bulkTransfer(epOut, dataBuffer, dataBuffer.length, 500);
            // Log.d(TAG, "TM_WriteRegister bulkTransfer 2 end");

            if (response != 512)
                status = 1;
        } else {
            status = 1;
        }

        SCSICommandStatus cmdStatus = new SCSICommandStatus(response, status);
        byte[] commandStatus = cmdStatus.getCSWBuffer();

        // Log.d(TAG, "TM_WriteRegister bulkTransfer 3 start");
        response = conn.bulkTransfer(epIn, commandStatus, commandStatus.length, 500);
        // Log.d(TAG, "TM_WriteRegister bulkTransfer 3 end");

        if (response == 13)
            return RESULT_OK;
        else
            return RESULT_FAIL;
    }

    @Override
    public int Raw_SPI_Write(byte[] writeBuf, int len) {
        return 0;
    }

    @Override
    public int Raw_SPI_Write(byte[] writeBuf, int len, int cs) {
        return 0;
    }

    @Override
    public int BurstRead(int address, byte[] buffer, int len) {

        // todo: max in define
        if ( len > 16384 ) {
            Log.e(TAG, "不可以傳超過16KB !!! ") ;
            return RESULT_FAIL ;
        }
        if ( buffer.length < len ) {
            Log.e(TAG, "資料長度出錯 BUFFER要比LEN長 ") ;
            return RESULT_FAIL ;
        }

        SCSICommandBlock cmdBlock = new SCSICommandBlock(512, (byte) 0x80);
        byte[] commandBlock = new byte[16];
        commandBlock[0] = (byte) 0xE6;

        // 最多16KB = 16382 二個BYTE = 65536 暫時可行
        commandBlock[1] = (byte) ( (0xFF00 & len) >> 8) ;
        commandBlock[2] = (byte) (0x00FF & len) ;

        cmdBlock.setCommandBlock(commandBlock);
        byte[] command = cmdBlock.getCBWBuffer();
        int response = conn.bulkTransfer(epOut, command, command.length, 500);
        byte status = 0;
        if (response == 31) {
            //byte[] dataBuffer = new byte[512];
            response = conn.bulkTransfer(epIn, buffer, len, 500);
//            for ( int i = 0 ; i < buffer.length ; i++ ) {
//                buffer[i] = dataBuffer[i] ;
//            }

            // 註: 回傳的數字即是回應多少BYTE
            if (response != len) status = 1;
        } else {
            return RESULT_FAIL;
        }

        SCSICommandStatus cmdStatus = new SCSICommandStatus(response, status);
        byte[] commandStatus = cmdStatus.getCSWBuffer();
        response = conn.bulkTransfer(epIn, commandStatus, commandStatus.length, 500);

        if (response == 13) {
            return RESULT_OK;
        } else {
            return RESULT_FAIL;
        }
    }



    public int BurstReadED(int address, byte[] buffer, int len) {

        if ( len > MAX_LEN ) {
            Log.e(TAG, "不可以傳超過16KB !!! ") ;
            return RESULT_FAIL ;
        }
        if ( buffer.length < len ) {
            Log.e(TAG, "資料長度出錯 BUFFER要比LEN長 ") ;
            return RESULT_FAIL ;
        }

        SCSICommandBlock cmdBlock = new SCSICommandBlock(512, (byte) 0x80);
        byte[] commandBlock = new byte[16];
        commandBlock[0] = (byte) 0xED;

        cmdBlock.setCommandBlock(commandBlock);
        byte[] command = cmdBlock.getCBWBuffer();
        int response = conn.bulkTransfer(epOut, command, command.length, 500);
        byte status = 0;
        if (response == 31) {
            //byte[] dataBuffer = new byte[512];
            response = conn.bulkTransfer(epIn, buffer, len, 500);
//            for ( int i = 0 ; i < buffer.length ; i++ ) {
//                buffer[i] = dataBuffer[i] ;
//            }

            // 註: 回傳的數字即是回應多少BYTE
            if (response != len) status = 1;
        } else {
            return RESULT_FAIL;
        }

        SCSICommandStatus cmdStatus = new SCSICommandStatus(response, status);
        byte[] commandStatus = cmdStatus.getCSWBuffer();
        response = conn.bulkTransfer(epIn, commandStatus, commandStatus.length, 500);

        if (response == 13) {
            return RESULT_OK;
        } else {
            return RESULT_FAIL;
        }
    }

    @Override
    public int BurstWrite(int address, byte[] buffer, int len) {

        //Log.e(TAG, "BurstWrite Start") ;

        if (epIn == null || epOut == null || conn == null) {
            return RESULT_FAIL;
        }

        if (buffer.length < len ) {
            Log.e(TAG, "BurstWrite, Buffer.length < len!!!") ;
            return RESULT_FAIL;
        }

        if ( len > MAX_LEN ) {
            Log.e(TAG, "BurstWrite, Buffer Len > max  16k -2") ;
            return RESULT_FAIL;
        }


        // 減2尚未解決
        SCSICommandBlock cmdBlock = new SCSICommandBlock(512, (byte) 0x00);
        byte[] commandBlock = new byte[16];
        commandBlock[0] = (byte) 0xE7; // convert to 0x26 under SPI
        //commandBlock[1] = (byte) 0x01; // unknow QQ
        //commandBlock[2] = (byte) address ; // offset one byte

        // 只有16 塞不下資料 等下塞

        cmdBlock.setCommandBlock(commandBlock);
        byte[] command = cmdBlock.getCBWBuffer();

        int response = conn.bulkTransfer(epOut, command, command.length, 500);
        // Log.d(TAG, "TM_WriteRegister bulkTransfer 1 end");
        byte status = 0;
        if (response == 31) {
            response = conn.bulkTransfer(epOut, buffer, buffer.length, 500);
            if (response != buffer.length)
                status = 1;
        } else {
            status = 1;
        }

        SCSICommandStatus cmdStatus = new SCSICommandStatus(response, status);
        byte[] commandStatus = cmdStatus.getCSWBuffer();


        response = conn.bulkTransfer(epIn, commandStatus, commandStatus.length, 500);
        // Log.d(TAG, "TM_WriteRegister bulkTransfer 3 end");

        if (response == 13)
            return RESULT_OK;
        else
            return RESULT_FAIL;
    }

    public int BurstReadV2(int address, byte[] buffer, int len) {

        Log.d(TAG, "BurstRead V2 Start") ;

        if (epIn == null || epOut == null || conn == null) {
            Log.e(TAG, "NO USB DEVICE") ;
            return RESULT_FAIL;
        }

        if (buffer.length < len ) {
            Log.e(TAG, "BurstRead, Buffer.length < len!!!") ;
            return RESULT_FAIL;
        }

        if ( len > MAX_LEN ) {
            Log.e(TAG, "BurstRead, Buffer Len > max  16k") ;
            return RESULT_FAIL;
        }

        SCSICommandBlock cmdBlock = new SCSICommandBlock(512, (byte) 0x00);
        byte[] commandBlock = new byte[16];
        commandBlock[0] = (byte) 0xEA; // convert to 0x26 under SPI
        commandBlock[1] = (byte) ((len >> 8) & 0xFF) ;
        commandBlock[2] = (byte) (len & 0xFF ) ;

        cmdBlock.setCommandBlock(commandBlock);
        byte[] command = cmdBlock.getCBWBuffer();

        int response = conn.bulkTransfer(epOut, command, command.length, 500);

        byte status = 0;
        if (response == 31) {
            response = conn.bulkTransfer(epOut, dummy, dummy.length, 500);
            //Log.e(TAG, "EA Data's Output:" + Common.byteArrayToString(dummy) );
            if (response != dummy.length) {
                status = 1;
                Log.e(TAG, "EA DUMMY FAIL") ;
            }
        } else {
            Log.e(TAG, "EA 31bit FAIL") ;
            status = 1;
        }
        SCSICommandStatus cmdStatus = new SCSICommandStatus(response, status);
        byte[] commandStatus = cmdStatus.getCSWBuffer();


        response = conn.bulkTransfer(epIn, commandStatus, commandStatus.length, 500);

        if (response != 13) {
            Log.e(TAG, "EA 13 Status Send FAIL!!!" + status) ;
            return RESULT_FAIL;
        }

        //return BurstReadED(address,buffer,len) ;

        // recv byte start!!!
        cmdBlock = new SCSICommandBlock(512, (byte) 0x80);
        commandBlock = new byte[16];
        commandBlock[0] = (byte) 0xEB; // convert to 0x26 under SPI
        commandBlock[1] = (byte) ((len >> 8) & 0xFF) ;
        commandBlock[2] = (byte) (len & 0xFF ) ;

        cmdBlock.setCommandBlock(commandBlock);
        command = cmdBlock.getCBWBuffer();

        response = conn.bulkTransfer(epOut, command, command.length, 500);

        status = 0;
        if (response == 31) {
            //Log.e(TAG, "ED 31 REsponse = 31 OK") ;
            response = conn.bulkTransfer(epIn, buffer, len, 500);

            if (response != len) {
                status = 1;
                Log.e(TAG, "bluk transfer different!! len=" + len + " != " + response ) ;
            }
        } else {
            status = 1;
            Log.e(TAG, "CMD BUF != " + len + " != " + response ) ;
        }
        cmdStatus = new SCSICommandStatus(response, status);
        commandStatus = cmdStatus.getCSWBuffer();

        response = conn.bulkTransfer(epIn, commandStatus, commandStatus.length, 500);

        if (response != 13) {
            Log.e(TAG, "EB SEND FAIL!!!") ;
            return RESULT_FAIL;
        }


        return RESULT_OK ;
    }

    public int BurstWriteV2(int address, byte[] buffer, int len) {

        if (epIn == null || epOut == null || conn == null) {
            return RESULT_FAIL;
        }

        if (buffer.length < len ) {
            Log.e(TAG, "BurstWrite, Buffer.length < len!!!") ;
            return RESULT_FAIL;
        }

        if ( len > MAX_LEN ) {
            Log.e(TAG, "BurstWrite, Buffer Len > max  16k -2") ;
            return RESULT_FAIL;
        }

        SCSICommandBlock cmdBlock = new SCSICommandBlock(512, (byte) 0x00);
        byte[] commandBlock = new byte[16];
        commandBlock[0] = (byte) 0xE8; // convert to 0x26 under SPI
        commandBlock[1] = (byte) ((len >> 8) & 0xFF) ;
        commandBlock[2] = (byte) (len & 0xFF ) ;

        cmdBlock.setCommandBlock(commandBlock);
        byte[] command = cmdBlock.getCBWBuffer();

        int response = conn.bulkTransfer(epOut, command, command.length, 500);

        byte status = 0;
        if (response == 31) {
            response = conn.bulkTransfer(epOut, buffer, len, 500);
            if (response != buffer.length)
                status = 1;
        } else {
            status = 1;
        }
        SCSICommandStatus cmdStatus = new SCSICommandStatus(response, status);
        byte[] commandStatus = cmdStatus.getCSWBuffer();


        response = conn.bulkTransfer(epIn, commandStatus, commandStatus.length, 500);

        if (response != 13)
            return RESULT_FAIL;

        // recv byte start!!!
        cmdBlock = new SCSICommandBlock(512, (byte) 0x00);
        commandBlock = new byte[16];
        commandBlock[0] = (byte) 0xE9; // convert to 0x26 under SPI
        commandBlock[1] = (byte) ((len >> 8) & 0xFF) ;
        commandBlock[2] = (byte) (len & 0xFF ) ;

        cmdBlock.setCommandBlock(commandBlock);
        command = cmdBlock.getCBWBuffer();

        response = conn.bulkTransfer(epOut, command, command.length, 500);

        status = 0;
        if (response == 31) {
            response = conn.bulkTransfer(epOut, dummy, dummy.length, 500);
            if (response != dummy.length)
                status = 1;
        } else {
            status = 1;
        }
        cmdStatus = new SCSICommandStatus(response, status);
        commandStatus = cmdStatus.getCSWBuffer();

        response = conn.bulkTransfer(epIn, commandStatus, commandStatus.length, 500);

        if (response != 13)
            return RESULT_FAIL;


        return RESULT_OK ;
    }

    /**
     * 傳輸OPCODE的介面
     * @param data len must = 8: [opcode, data, data, data ,data ,0 ,0 ,0]
     */
    public int riscvOpcode( byte [] data) {
        //Log.d(TAG, "SendOPCode Start !! addr=" + Integer.toHexString(data[0])) ;

        if (epIn == null || epOut == null || conn == null) {
            return RESULT_FAIL;
        }

        SCSICommandBlock cmdBlock = new SCSICommandBlock(512, (byte) 0x00);
        byte[] commandBlock = new byte[16];
        commandBlock[0] = (byte) 0xEC;
        for ( int i = 0 ; i < data.length ; i++ ) {
            commandBlock[i+1] = data[i] ;
        }
        Log.d(TAG, "opcode command raw=" + Common.byteArrayToString(commandBlock));


        cmdBlock.setCommandBlock(commandBlock);
        byte[] command = cmdBlock.getCBWBuffer();
        int response = conn.bulkTransfer(epOut, command, command.length, 500);
        byte status = 0;
        if (response == 31) {
            response = conn.bulkTransfer(epOut, dummy, dummy.length, 500);
            if (response != dummy.length)
                status = 1;
        } else {
            status = 1;
        }

        SCSICommandStatus cmdStatus = new SCSICommandStatus(response, status);
        byte[] commandStatus = cmdStatus.getCSWBuffer();

        response = conn.bulkTransfer(epIn, commandStatus, commandStatus.length, 500);

        if (response == 13)
            return RESULT_OK;
        else
            return RESULT_FAIL;
    }

    @Override
    public int FetchImage(byte[] data, int width, int height) {
        if (epIn == null || epOut == null || conn == null) {
            return RESULT_FAIL;
        }
        if (StartCapture(width, height) == RESULT_FAIL) {
            Log.e(TAG, "StartCapture fail");
            return RESULT_FAIL;
        }

        int pageNum = 0;
        int pagesize = 16;
        int pagelen = pagesize * 1024;
        byte[] buffer = new byte[pagelen];
        int len = 0;
        int read_len;
        int ret;
        while (len < data.length) {
            ret = GetRawImage(buffer, pageNum, pagesize);
            if (ret < 0) {
                Log.e(TAG, "GetRawImage fail");
                return RESULT_FAIL;
            }

            if ((len + pagelen) > data.length) {
                read_len = data.length - len;
            } else {
                read_len = pagelen;
            }
            len += read_len;
            System.arraycopy(buffer, 0, data, pageNum * pagelen, read_len);
            pageNum++;
        }
        return RESULT_OK;
    }


    /**
     * 註 目前ET760 OTG 架構下無效用 固定5.5M左右
     * @param mode
     * @param clk
     * @return
     */
    @Override
    public int Set_SPI_CLK(byte mode, byte clk) {
        return 0;
    }

    /**
     * 註: 不知道其功能 但只好先OVERRIDE 囧
     * @param writeBuf
     * @param writeLen
     * @param readBuf
     * @param readLen
     * @return
     */
    @Override
    public int SPI_Write_Read(byte[] writeBuf, int writeLen, byte[] readBuf, int readLen) {
        return 0;
    }

    private int StartCapture(int width, int height) {
        int image_width = width, image_height = height;
        SCSICommandBlock cmdBlock = new SCSICommandBlock(512, (byte) 0x00);
        byte[] commandBlock = new byte[16];
        commandBlock[0] = (byte) 0xE3;
        commandBlock[1] = (byte) 10; // bit num
        commandBlock[2] = (byte) ((image_width & 0xff00) >> 8); // full frame width
        commandBlock[3] = (byte) (image_width & 0x00ff);
        commandBlock[4] = (byte) ((image_height & 0xff00) >> 8); // full frame width
        commandBlock[5] = (byte) (image_height & 0x00ff);
        cmdBlock.setCommandBlock(commandBlock);
        byte[] command = cmdBlock.getCBWBuffer();
        int response = conn.bulkTransfer(epOut, command, command.length, 500);
        byte status = 0;
        if (response == 31) {
            byte[] dataBuffer = new byte[512];
            response = conn.bulkTransfer(epOut, dataBuffer, dataBuffer.length, 500);
            if (response != 512) status = 1;
        } else {
            return RESULT_FAIL;
        }

        SCSICommandStatus cmdStatus = new SCSICommandStatus(response, status);
        byte[] commandStatus = cmdStatus.getCSWBuffer();
        response = conn.bulkTransfer(epIn, commandStatus, commandStatus.length, 500*4);

        if (response == 13) {
            return RESULT_OK;
        } else {
            return RESULT_FAIL;
        }
    }

    public int GetRawImage(byte[] buffer, int pagenum, int pagesize) {
        SCSICommandBlock cmdBlock = new SCSICommandBlock(512, (byte) 0x80);
        byte[] commandBlock = new byte[16];
        commandBlock[0] = (byte) 0xE2;
        commandBlock[1] = (byte) pagenum; // page number
        commandBlock[2] = (byte) pagesize; // page size
        cmdBlock.setCommandBlock(commandBlock);
        byte[] command = cmdBlock.getCBWBuffer();
        int response = conn.bulkTransfer(epOut, command, command.length, 500);
        byte status = 0;
        if (response == 31) {
            response = conn.bulkTransfer(epIn, buffer, buffer.length, 500);
            if (response != pagesize * 1024) status = 1;
        } else {
            return RESULT_FAIL;
        }

        SCSICommandStatus cmdStatus = new SCSICommandStatus(response, status);
        byte[] commandStatus = cmdStatus.getCSWBuffer();
        response = conn.bulkTransfer(epIn, commandStatus, commandStatus.length, 500);

        if (response == 13) {
            return RESULT_OK;
        } else {
            return RESULT_FAIL;
        }
    }



}
