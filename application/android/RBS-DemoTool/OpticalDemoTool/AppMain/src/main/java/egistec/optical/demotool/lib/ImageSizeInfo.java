package egistec.optical.demotool.lib;

public class ImageSizeInfo {
    public static final int DATA_SIZE_IMAGE_SIZE_INFO = 16;
    public int img_width;
    public int img_height;
    public int img_bpp;
    public int raw_bpp;
    ImageSizeInfo(byte[] info){
        int[] intInfo = FPUtil.byteArrayTointArray(info);
        img_width = intInfo[0];
        img_height = intInfo[1];
        img_bpp = intInfo[2];
        raw_bpp = intInfo[3];
    }
}
