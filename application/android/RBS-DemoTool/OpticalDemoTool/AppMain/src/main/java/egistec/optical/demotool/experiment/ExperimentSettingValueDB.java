package egistec.optical.demotool.experiment;

import android.content.Context;
import android.content.SharedPreferences;

public class ExperimentSettingValueDB {
    private SharedPreferences sharedPreferences;
    private static String PREF_NAME = "ExperimentSettingValueDB";

    public static int mDefaultIntegrationCount = 3;
    public static int mDefaultExposureTimeScaleMin = 1;
    public static int mDefaultExposureTimeScaleMax = 15;
    public static int mDefaultBrightness = 100;
    public static int mDefaultPressPosX = 145;
    public static int mDefaultPressPosY = 235;
    public static String mDefaultUserName = "finger";
    public static int mDefaultPressColorId = 0;

    public ExperimentSettingValueDB() {
    }

    private static SharedPreferences getPrefs(Context context) {
        return context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
    }

    public static int getIntegrationCount(Context context) {
        return getPrefs(context).getInt("integrationCount", mDefaultIntegrationCount);
    }

    public static void setIntegrationCount(Context context, int input) {
        SharedPreferences.Editor editor = getPrefs(context).edit();
        editor.putInt("integrationCount", input);
        editor.commit();
    }

    public static int getExposureTimeScaleMin(Context context) {
        return getPrefs(context).getInt("exposureTimeScaleMin", mDefaultExposureTimeScaleMin);
    }

    public static void setExposureTimeScaleMin(Context context, int input) {
        SharedPreferences.Editor editor = getPrefs(context).edit();
        editor.putInt("exposureTimeScaleMin", input);
        editor.commit();
    }

    public static int getExposureTimeScaleMax(Context context) {
        return getPrefs(context).getInt("exposureTimeScaleMax", mDefaultExposureTimeScaleMax);
    }

    public static void setExposureTimeScaleMax(Context context, int input) {
        SharedPreferences.Editor editor = getPrefs(context).edit();
        editor.putInt("exposureTimeScaleMax", input);
        editor.commit();
    }

    public static int getBrightness(Context context) {
        return getPrefs(context).getInt("brightness", mDefaultBrightness);
    }

    public static void setBrightness(Context context, int input) {
        SharedPreferences.Editor editor = getPrefs(context).edit();
        editor.putInt("brightness", input);
        editor.commit();
    }

    public static int getPressPosX(Context context) {
        return getPrefs(context).getInt("pressPosX", mDefaultPressPosX);
    }

    public static void setPressPosX(Context context, int input) {
        SharedPreferences.Editor editor = getPrefs(context).edit();
        editor.putInt("pressPosX", input);
        editor.commit();
    }

    public static int getPressPosY(Context context) {
        return getPrefs(context).getInt("pressPosY", mDefaultPressPosY);
    }

    public static void setPressPosY(Context context, int input) {
        SharedPreferences.Editor editor = getPrefs(context).edit();
        editor.putInt("pressPosY", input);
        editor.commit();
    }

    public static String getUserName(Context context) {
        return getPrefs(context).getString("userName", mDefaultUserName);
    }

    public static void setUserName(Context context, String input) {
        SharedPreferences.Editor editor = getPrefs(context).edit();
        editor.putString("userName", input);
        editor.commit();
    }

    public static int getPressColorId(Context context) {
        return getPrefs(context).getInt("pressColorId", mDefaultPressColorId);
    }

    public static void setPressColorId(Context context, int input) {
        SharedPreferences.Editor editor = getPrefs(context).edit();
        editor.putInt("pressColorId", input);
        editor.commit();
    }
}
