package egistec.optical.demotool.lib;

import static rbs.egistec.com.fplib.api.Common.bytesToint;

public class ReturnImageInfo {
    private boolean mIsNewFingerOn;
    private int mTryMatchCount;
    private int mMatchScore;
    private int mLastTriedIndex;

    public void parseImageInfo(Object data) {
        byte[] info = new byte[Integer.SIZE / Byte.SIZE];
        System.arraycopy(data, 4, info, 0, info.length);
        int isNewFingerOn = bytesToint(info);
        if (isNewFingerOn == 1) {
            mIsNewFingerOn = true;
        } else {
            mIsNewFingerOn = false;
        }

        System.arraycopy(data, 8, info, 0, info.length);
        mTryMatchCount = bytesToint(info);

        System.arraycopy(data, 12, info, 0, info.length);
        mMatchScore = bytesToint(info);

        System.arraycopy(data, 16, info, 0, info.length);
        mLastTriedIndex = bytesToint(info);
    }

    public boolean isNewFingerOn() {
        return mIsNewFingerOn;
    }

    public int getTryMatchCount() {
        return mTryMatchCount;
    }

    public int getMatchScore() {
        return mMatchScore;
    }

    public int getLastTriedIndex() {
        return mLastTriedIndex;
    }

    public static boolean isReturnImageRaw16bit(int width, int height, int frameCount, Object eventObj) {
        byte[] dataTemp = (byte[]) eventObj;

        return dataTemp.length == width * height * 2 * frameCount;

    }

}
