package egistec.optical.demotool.lib;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Method;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.IBinder;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.KeyEvent;

public class FPUtil {
	
	public static final String TAG = "RBS_FPUtil";
	private static int[] mPicPixels = new int[512*512];		
	private static int[] mMaskPixels = new int[380*380];
	
	public static Bitmap createMask(int mapW, int mapH, byte[] enrollMap){
		Bitmap mask = Bitmap.createBitmap(mapW, mapH, Bitmap.Config.ARGB_8888);
		for(int i = 0; i < enrollMap.length; i++){
			mPicPixels[i] = (enrollMap[i] == 0)? Color.BLACK:0;
		}
		mask.setPixels(mPicPixels, 0, mapW, 0, 0, mapW, mapH);
		return mask;
	}	
			
	public static Bitmap composeBitmap(int w, int h, Bitmap scaledBaseBmp, Bitmap maskBitmap, int[] scaledFrontPixels){
		
		int[] frontPixels = maskFrontBmp(w, h, maskBitmap, scaledFrontPixels);			
		return composeBitmap(w, h, scaledBaseBmp, frontPixels);
	}	
	
	private static int[] maskFrontBmp(int w, int h, Bitmap maskBitmap, int[] frontPixels){

		Arrays.fill(mMaskPixels, 0);
		maskBitmap.getPixels(mMaskPixels, 0, w, 0, 0, w, h);

		int idx=0;
		for(int value : mMaskPixels){
			if(0x00000000 == value){
				frontPixels[idx] = value;
			}
			idx++;
		}
		return frontPixels;
	}
	
	private static Bitmap composeBitmap(int w, int h, Bitmap baseBitmap, int[] resultPixels){
		
		Bitmap composedBitmap = baseBitmap.copy(Bitmap.Config.ARGB_8888, true);
		Canvas cv = new Canvas(composedBitmap);
		cv.drawBitmap(resultPixels, 0, w, 0, 0, w, h, true, new Paint());
		cv.save(Canvas.ALL_SAVE_FLAG);
		cv.restore();	
		return composedBitmap;
	}
	
	public static int[] bmpToPixels(Bitmap bitmap, int w, int h){    	
		
    	int[] maskPixels = new int[w*h];
    	bitmap.getPixels(maskPixels, 0, w, 0, 0, w, h);
    	return maskPixels;
	}	
	
	public static void countArrayContent(int[] array){
		int[] counter = new int[array.length];
		boolean bFlag=false;
		for(int i=0 ; i<array.length; i++){
			if(counter[i] != Color.BLACK && counter[i] != 0){
				bFlag=true;
				Log.d(TAG, "content["+i+"]= " + counter[i]);
			}	
		}
		
		if(!bFlag){
			Log.d(TAG, "++++++++++++++++++not into log++++++++++++++++");
		}		
	}
	
	public static void countArrayContent(byte[] array){
		
		int[] counter = new int[array.length];
		for(int i=0 ; i<array.length; i++){
			int index = array[i];
			
			if(index == -1)
				index = array.length-1;
			
			if(index < 0)
				index = -index;
			
			counter[index]++;			
		}
		Log.d(TAG, "first: " + counter[0]);
		Log.d(TAG, "last: " + counter[array.length-1]);
		for(int j=1 ; j<counter.length-1; j++){
			if (counter[j] > 0) Log.d(TAG, "counter["+j+"]= " + counter[j]);	
		}
		
	}
		
	public static AlertDialog.Builder getDilog(Context context, int title){
		return new AlertDialog.Builder(context)
			   .setIconAttribute(android.R.attr.alertDialogIcon)
			   .setTitle(title);			   			
	}
	
	public static void mirrorContent(byte[] map){		
		
		for (int k = 0; k < map.length/2; k++) {
	        byte temp = map[k];
	        map[k] = map[map.length-(1+k)];
	        map[map.length-(1+k)] = temp;
	    }		
		
		/*
		int y, x, diff;
		int w = 512;
		for (y=0; y<w; y++) {
			diff=0;
			int idx = y*w;
			for(x=idx; x< (idx+(idx+w))/2; x++){				
				byte temp = map[x];
			    map[x] = map[((y+1)*w)-(1+diff)];
			    map[((y+1)*w)-(1+diff)] = temp;
			    diff++;
			}			
		}
		*/						
	}	
	
	public static void saveRawDataToHex(File file, String fileName, byte[] bytes){		
		int cnt=0;
		FileOutputStream f=null;
		PrintWriter pw=null;
		final int line = 100;
		try {
			f = new FileOutputStream(new File(file, fileName));
			pw = new PrintWriter(f);
			for(byte b : bytes){				  				 
			  pw.print(String.format("%02X", b&0xff) + " ");
			  if(cnt%line == 0)
				pw.print("\n"); 
			  cnt++;
			}
		} catch (FileNotFoundException e) {			
			e.printStackTrace();
		}finally{
			 pw.flush();
		     pw.close();
		     try {
				f.close();
			} catch (IOException e) {				
				e.printStackTrace();
			}
		}	   
	}
	
	public static void setBlockHomeKey(Activity activity, boolean bBlock) {
        try {
            Class<?> classServiceManager = Class.forName("android.os.ServiceManager");
            Class<?> classWindowManager = Class.forName("android.view.IWindowManager");
            Class<?> classWindowManagerStub = Class.forName("android.view.IWindowManager$Stub");

            Method method = classServiceManager.getMethod("getService", new Class[] { String.class });
            IBinder serviceManagerBinder = (IBinder)method.invoke(classServiceManager, new Object[] { Context.WINDOW_SERVICE });

            method = classWindowManagerStub.getMethod("asInterface", new Class[] { IBinder.class });
            Object windowManager = method.invoke(classWindowManagerStub, new Object[] { serviceManagerBinder });

            method = classWindowManager.getMethod("requestSystemKeyEvent", new Class[] { Integer.TYPE, ComponentName.class, Boolean.TYPE });
            method.invoke(windowManager, new Object[] { KeyEvent.KEYCODE_HOME, activity.getComponentName(), bBlock });
        } catch (Throwable t) {
        }
    }	
	
	public static int[] toIntArray(List<Integer> list){
		int[] ret = new int[list.size()];
		for(int i = 0;i < ret.length;i++)
		   ret[i] = list.get(i);
		return ret;
	}

	public static String SharePreferenceToIniSettings(SharedPreferences prefs) {
		Map<String, ?> keys = prefs.getAll();

		int total = keys.size();
		Log.d(TAG, ">> Total preferences: " + total);

		Map<String, String> sections = new HashMap<>();
		for (Map.Entry<String, ?> entry : keys.entrySet()) {
			String key = entry.getKey();
			Log.d(TAG, "map values | " + key + " = " + entry.getValue());
			if (entry.getValue().toString().trim().compareTo("?") == 0) {
				Log.v(TAG, "Ignore " + key + " = \"?\"");
				continue;
			}
			String[] iniKeyNames = key.split(":");
			if (iniKeyNames != null && iniKeyNames.length > 1) {
				String sectionName = iniKeyNames[0];
                String keyName = iniKeyNames[1];
				if (!sections.containsKey(sectionName)) {
					sections.put(sectionName, "[" + sectionName + "]\n");
				}
				String updateSection = sections.get(sectionName).concat(keyName +" = " + entry.getValue() + "\n");
				sections.put(sectionName, updateSection);
			} else {
                Log.v(TAG, "skip | " + key + " = " + entry.getValue());
            }
		}
		String iniSettings = "";
		for (String section : sections.values()) {
			iniSettings = iniSettings.concat(section);
		}
        Log.v(TAG, "iniSettings\n" + iniSettings);
        return iniSettings;
	}

	public static String GetSharePreferenceKeyValue(SharedPreferences prefs, String FindName) {
		Map<String, ?> keys = prefs.getAll();
		for (Map.Entry<String, ?> entry : keys.entrySet()) {
			String key = entry.getKey();
			String[] iniKeyNames = key.split(":");
			if (iniKeyNames != null && iniKeyNames.length > 1) {
				String sectionName = iniKeyNames[0];
				String keyName = iniKeyNames[1];
				if (keyName.contentEquals(keyName)) {
                    Log.v(TAG, "get | " + keyName + " = " + entry.getValue());
                    return entry.getValue().toString();
				}
			} else {
				Log.v(TAG, "skip | " + key + " = " + entry.getValue());
			}
		}
        Log.v(TAG, "not find | " + FindName);
        return "";
	}

	public static SpannableStringBuilder getColorStringBuilder(String text, int color) {
		SpannableStringBuilder builder = new SpannableStringBuilder();

		SpannableString redSpannable= new SpannableString(text);
		redSpannable.setSpan(new ForegroundColorSpan(color), 0, text.length(), 0);
		builder.append(redSpannable);

		return builder;
	}

	public static byte[] AndroidBufferToWindowsBuffer(byte[] buf) {
		Log.d(TAG, "AndroidBufferToWindowsBuffer");
//		PrintOutByteBuffer("Ori 8bit", buf);
	    ByteBuffer bb = ByteBuffer.wrap(buf).order(ByteOrder.LITTLE_ENDIAN);
	    ByteBuffer bb_new = ByteBuffer.allocate(bb.capacity());
	    bb.rewind();
		bb_new.rewind();
		while (bb.hasRemaining())
            bb_new.putShort(bb.getShort());

		byte[] ret = bb_new.array();
//		PrintOutByteBuffer("After 8bit", ret);
		return ret;
	}

	public static void PrintOutByteBuffer(String title, byte[] buf) {
		int i, j;
		if (buf == null) {
			Log.e(TAG, "bad buf: " + title);
			return;
		}
		Log.d(TAG, "PrintOutByteBuffer " + title + " : " + buf.length);

		int loop = buf.length / 8;
		if (loop > 6) loop = 6;

		j = 0;
		for (i = 0; i < loop; i++) {
			Log.d(TAG, String.format("[%3d] %02X %02X %02X %02X %02X %02X %02X %02X", j,
					buf[j], buf[j + 1], buf[j + 2], buf[j + 3], buf[j + 4], buf[j + 5], buf[j + 6], buf[j + 7]));
			j += 8;
		}
		return;
	}

	public static byte[] StringToCByteArray(String path) {
		byte[] path_bytes = path.getBytes();
		ByteBuffer bb = ByteBuffer.allocate(path_bytes.length + 1);
		bb.put(path_bytes);
		bb.put((byte) '\0');
		return bb.array();
	}

	public static int[] byteArrayTointArray(byte[] src) {
		int dstLength = src.length >>> 2;
		int[] dst = new int[dstLength];

		for (int i = 0; i < dstLength; i++) {
			int j = i << 2;
			int x = 0;
			x += (src[j++] & 0xff) << 0;
			x += (src[j++] & 0xff) << 8;
			x += (src[j++] & 0xff) << 16;
			x += (src[j++] & 0xff) << 24;
			dst[i] = x;
		}
		return dst;
	}

	public static byte[] intArrayToBytes (int[] integers) {
		if (integers != null) {
			byte[] outputBytes = new byte[integers.length * 4];

			for(int i = 0, k = 0; i < integers.length; i++) {
				int integerTemp = integers[i];
				for(int j = 0; j < 4; j++, k++) {
					outputBytes[k] = (byte)((integerTemp >> (8 * j)) & 0xFF);
					//Log.d(TAG, "PAPA intArrayToBytes ["+k+"] = "+outputBytes[k]+" from "+integerTemp);
				}
			}
			return outputBytes;
		} else {
			return null;
		}
	}
}
