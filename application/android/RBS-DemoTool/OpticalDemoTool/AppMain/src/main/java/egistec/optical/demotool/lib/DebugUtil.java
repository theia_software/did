package egistec.optical.demotool.lib;

//import android.util.Log;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import rbs.egistec.com.fplib.api.EgisLog;

public class DebugUtil {
    private static final String TAG = "RBS-DebugUtil";

    public static void killProcess(int pid, int signal){
        EgisLog.d(TAG, "killProcess "+signal);
        try {
            String s = "\n";
            String sErr = "\n";
            String cmd[] = {"sh", "-c", "kill -"+signal+" "+pid};

            Process p = Runtime.getRuntime().exec(cmd);

            InputStream is = p.getInputStream();
            InputStream ers = p.getErrorStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(is));
            DataInputStream ise = new DataInputStream(ers);
            String line = null;
            String error = null;
            while ((line = reader.readLine()) != null) {
                s += line + "\n";
            }
            EgisLog.d(TAG,"s=" + s);
            while((error = ise.readLine()) != null) {
                sErr += error + "\n";
            }
            EgisLog.d(TAG,"sErr=" + sErr);
            int result = p.waitFor();
            EgisLog.d(TAG,"result=" + result);

            is.close();
            ers.close();
            reader.close();
            ise.close();
            p.destroy();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void dumpMemLog(){
        try {
            String s = "\n";
            String sErr = "\n";
            String cmd[] = {"sh", "-c", "logcat -t 30000 | grep malloc_debug | grep -i -e \"libRbsFlow.so\" -e \"libegis_fp_core.so\"| tee /mnt/sdcard/logmem.txt"};

            Process p = Runtime.getRuntime().exec(cmd);

            InputStream is = p.getInputStream();
            InputStream ers = p.getErrorStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(is));
            DataInputStream ise = new DataInputStream(ers);
            String line = null;
            String error = null;
            while ((line = reader.readLine()) != null) {
                s += line + "\n";
            }
            EgisLog.d(TAG,"s=" + s);
            while((error = ise.readLine()) != null) {
                sErr += error + "\n";
            }
            EgisLog.d(TAG,"sErr=" + sErr);
            int result = p.waitFor();
            EgisLog.d(TAG,"result=" + result);

            is.close();
            ers.close();
            reader.close();
            ise.close();
            p.destroy();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
