package rbs.egistec.com.fplib.api;

import android.content.Context;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;
import android.util.Log;
//import android.util.Log;
import rbs.egistec.com.fplib.api.EgisLog;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;

public class RbsLib {

    private static final String TAG = "RbsLib-JNI";
    private static RbsLib mInstance;
    private static FingerprintReceiver mFingerprintReceiver = null;
    private static String appFileDirPath = "/sdcard/temp/template";
    private static final int JNIAPI_CALLBACK_ON_LOG = 4001;
    private static HandlerThread mLogHandlerThread;
    private static Handler mWorkHandler;


    private static final int LOG_NONE = 0;
    private static final int LOG_ERROR = 1;
    private static final int LOG_INFO = 2;
    private static final int LOG_DEBUG = 3;

    public static RbsLib getInstance(Context context) {
        EgisLog.d(TAG,"+++ getInstance +++");
        if(mInstance == null) {
            EgisLog.d(TAG,"get new RbsLib");
            mInstance = new RbsLib();

            System.loadLibrary("demotooljni");
            Initialize();

            // create appFileDirPath if it does not exist
            File directory = new File(appFileDirPath);
            if (!directory.exists()){
                directory.mkdirs();
            }
        }
        return mInstance;
    }

    private RbsLib(){
        mLogHandlerThread = new HandlerThread("handlerThread");
        mLogHandlerThread.start();
        mWorkHandler = new Handler(mLogHandlerThread.getLooper());
    }

    private static final Handler mHandler = new Handler(){
        public void handleMessage(Message msg){
            if(mFingerprintReceiver == null){
                EgisLog.e(TAG, "mFingerprintReceiver == null, handleMessage return");
                return;
            }

            mFingerprintReceiver.onFingerprintEvent(msg.what, msg.arg1, msg.arg2, msg.obj);
        };
    };

    private static void NativeCallback(final int eventId, final int value1, final int value2,
                                         final byte[] byteBuffer) {
        EgisLog.v(TAG, "NativeCallback send : event = " + eventId);
        switch (eventId) {
            case FpResDef.FACTORY_TEST_EVT_SNSR_TEST_SCRIPT_START:
            case FpResDef.FACTORY_TEST_EVT_SNSR_TEST_SCRIPT_END:
            case FpResDef.FACTORY_TEST_EVT_SNSR_TEST_PUT_WKBOX:
            case FpResDef.FACTORY_TEST_EVT_SNSR_TEST_PUT_BKBOX:
            case FpResDef.FACTORY_TEST_EVT_SNSR_TEST_PUT_CHART:
            case FpResDef.EVT_ERROR:
            case FpResDef.EVENT_OTG_WRITE_REGISTER:
            case FpResDef.EVENT_OTG_READ_REGISTER:
            case FpResDef.EVENT_OTG_GET_FRAME:
            case FpResDef.EVENT_OTG_WAKE_UP:
            case FpResDef.EVENT_OTG_STANDBY:
            case FpResDef.EVENT_OTG_SPI_WRITE_READ:
            case FpResDef.EVENT_OTG_SET_SPI_CLK:
            case FpResDef.EVENT_OTG_GET_SENSOR_ID:
            case FpResDef.EVENT_OTG_INIT_SENSOR:
            case FpResDef.EVENT_OTG_SET_SENSOR_PARAM:
            case FpResDef.EVENT_OTG_GET_SENSOR_PARAM:
            case FpResDef.EVENT_OTG_GET_IMAGE:
            case FpResDef.EVENT_OTG_ET760_READ_REGISTER:
            case FpResDef.EVENT_OTG_ET760_WRITE_REGISTER:
            case FpResDef.EVENT_OTG_ET760_RAW_SPI:
                if (mFingerprintReceiver != null) {
                    mFingerprintReceiver.onFingerprintEvent(eventId, value1, value2, byteBuffer);
                }
                return;
            case JNIAPI_CALLBACK_ON_LOG:
                mWorkHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        switch (value1){
                            case LOG_NONE:
                                break;
                            case LOG_ERROR:
                            case LOG_INFO:
                            case LOG_DEBUG:
                                String[] splitLine = new String(byteBuffer).split("\n");
                                for (String line: splitLine) {
                                    EgisLog.native_log(line);
                                }
                                break;
                        }
                    }
                });
                break;
            default:
                EgisLog.d(TAG, "Not OTG events, go to handler, event = " + eventId);
                break;
        }
        if(mHandler != null) {
            mHandler.obtainMessage(eventId,value1, value2, byteBuffer).sendToTarget();
        }
    }

    public int sensorTest(int testId) {
        byte[] test_result_buf = new byte[4];
        int[] test_result_lenth = new int[1];
        test_result_lenth[0] = test_result_buf.length;
        int[] test_result = new int[1];
        test_result[0] = -1;

        int retval = extraApi(3, testId, null, test_result_buf, test_result_lenth);
        if(retval == 0) {
            Common.bytesToint(test_result_buf, test_result);
        }

        return test_result[0];
    }

    public int sensorProb() {
        return 0;
    }

    public void startListening(FingerprintReceiver receiver) {
        mFingerprintReceiver = receiver;
        SetNativeCallback();
        return;
    }

    public int extraApi(int pid, int cid, byte[] in_buffer, byte[] out_buffer, int[] out_buffer_size) {
        int in_buffer_size = 0;
        if(in_buffer != null) {
            in_buffer_size = in_buffer.length;
        }
        byte[] in_msg = new byte[in_buffer_size + Integer.SIZE / 8];
        byte[] cid_buf = new byte[Integer.SIZE / 8];
        Common.intToBytes(cid, cid_buf);

        Log.d(TAG, "debug_check in_msg.length = " + in_msg.length);

        System.arraycopy(cid_buf, 0, in_msg, 0, cid_buf.length);
        if(in_buffer != null) {
            System.arraycopy(in_buffer, 0, in_msg, cid_buf.length, in_buffer.length);
        }
        if (out_buffer == null || out_buffer_size == null) {
            byte[] tmp_buffer = new byte[4];
            int[] tmp_size = new int[1];
            tmp_size[0] = 4;
            EgisLog.d(TAG, "out_buffer is not set");
            return extra_api(pid, in_msg, in_msg.length, tmp_buffer, tmp_size);
        } else {
            return extra_api(pid, in_msg, in_msg.length, out_buffer, out_buffer_size);
        }
    }

    public int initialize() {
        return Initialize();
    }

    public int uninitialize() {
        return Uninitialize();
    }

    public int cancel() {
        return Abort();
    }

    public int setActiveUserGroup() {
        return SetActiveUser(0, appFileDirPath);
    }

    public int setActiveUserGroup(String userPath) {
        if (userPath != null) {
            int ret = SetActiveUser(0, userPath);
            if (ret == 0) {
                appFileDirPath = userPath;
            }
            return ret;
        } else {
            return setActiveUserGroup();
        }
    }

    public int setDataPath(int type, String data_path) {
        return SetDataPath(type, data_path);
    }

    public int enroll(int user_id, int fingerprint_id) {
        return EnrollFinger(user_id, fingerprint_id);
    }

    public int postEnroll() {
        return 0;
    }

    public int verify(int user_id, int[] finger_ids, int finger_cout, long challenge) {
        return Verify(user_id, finger_ids, finger_cout, challenge);
    }

    public int removeFingerprint(int user_id, int finger_id) {
        return Delete(user_id, finger_id);
    }

    public int getFingerprintList(int user_id, int[] figner_ids, int[] finger_cout) {
        return GetFingerList(user_id, figner_ids, finger_cout);
    }

    public static native int SetDataPath(int type, String data_path);

    public static native int SetActiveUser(int user_id, String data_path);

    public static native int Initialize();

    public static native int Uninitialize();

    public static native int Abort();

    public static native int Delete(int uid, int fpid);

    public static native int GetFingerList(int uid, int[] fplist, int[] fpcount);

    public native int EnrollFinger(int user_id, int fingerprint_id);

    public native void SetNativeCallback();

    public native int Verify(int user_id, int[] fingerprint_ids,
                             int fingerprint_count, long wait_finger_off);

    public native int extra_api(int pid, byte[] in_buffer, int in_buffer_size, byte[] out_buffer, int[] out_buffer_size);
}
