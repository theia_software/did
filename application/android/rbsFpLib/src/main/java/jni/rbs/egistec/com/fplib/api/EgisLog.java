package rbs.egistec.com.fplib.api;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.util.Log;

public class EgisLog {
	private static final Logger LOGGER = LoggerFactory.getLogger("logtest");

	public static void v(String TAG, String str) {
		int pid = android.os.Process.myPid();
		int tid = android.os.Process.myTid();
		String log_mesg = String.format("%5d %5d %s %s %s", pid, tid, "V", TAG, str);
		Log.v(TAG, str);
		LOGGER.trace(log_mesg);
	}

	public static void d(String TAG, String str) {
		int pid = android.os.Process.myPid();
		int tid = android.os.Process.myTid();
		String log_mesg = String.format("%5d %5d %s %s %s", pid, tid, "D", TAG, str);
		Log.d(TAG, str);
		LOGGER.debug(log_mesg);
	}

	public static void e(String TAG, String str) {
		int pid = android.os.Process.myPid();
		int tid = android.os.Process.myTid();
		String log_mesg = String.format("%5d %5d %s %s %s", pid, tid, "E", TAG, str);
		Log.e(TAG, str);
		LOGGER.error(log_mesg);
	}

	public static void i(String TAG, String str) {
		int pid = android.os.Process.myPid();
		int tid = android.os.Process.myTid();
		String log_mesg = String.format("%5d %5d %s %s %s", pid, tid, "I", TAG, str);
		Log.i(TAG, str);
		LOGGER.info(log_mesg);
	}

	public static void w(String TAG, String str) {
		int pid = android.os.Process.myPid();
		int tid = android.os.Process.myTid();
		String log_mesg = String.format("%5d %5d %s %s %s", pid, tid, "W", TAG, str);
		Log.w(TAG, str);
		LOGGER.warn(log_mesg);
	}

	public static void native_log(String str) {
		LOGGER.debug(str);
	}
}
