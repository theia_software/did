package rbs.egistec.com.fplib.api;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import rbs.egistec.com.fplib.service.FPRbsNative;

public class RbsLib{
    
	private static final String TAG = "RbsLib-binder";
	private static RbsLib mInstance;
	private FingerprintReceiver mFingerprintReceiver;
	private FPRbsNative mFPDev;
	private static final String DATA_PATH = "/data/fpdata";
	
	public static RbsLib getInstance(Context context){
		Log.d(TAG,"+++ getInstance +++");
		if(mInstance == null) {
			Log.d(TAG,"get new RbsLib");
			mInstance = new RbsLib();
		}
		return mInstance;
	}
	
	private final Handler mHandler = new Handler(){
		public void handleMessage(Message msg){
			if(mFingerprintReceiver == null){
				Log.e(TAG, "mFingerprintReceiver == null, handleMessage return");
				return;
			}
			
			mFingerprintReceiver.onFingerprintEvent(msg.what, msg.arg1, msg.arg2, msg.obj);
		};
	};
	
	public int sensorTest(int testId){

		int retval =  mFPDev.extraApi(3, testId, null, null, null);
		return retval;

	}
	
	public int sensorProb(){
		Log.d(TAG,"+++ sensorProb +++");
		return 0;
	}
	
	private RbsLib(){
		Log.d(TAG, "RbsLib Constructor");
		mFPDev = new FPRbsNative(mHandler);
	}
	
	public void startListening(FingerprintReceiver receiver){
		Log.d(TAG,"+++ startListening +++");
		mFingerprintReceiver = receiver;
		setCallback();
	}
	
	public int extraApi(int pid, int cid, byte[] in_buffer, byte[] out_buffer, int[] out_buffer_size){
		Log.d(TAG,"+++ extraApi +++");
		return mFPDev.extraApi(pid, cid, in_buffer, out_buffer, out_buffer_size);
	}

	public int initialize() {
		return 0;
	}

	public int uninitialize(){
		return 0;
	}
	
	public int cancel(){
		Log.d(TAG,"+++ cancel +++");
		return mFPDev.cancel();
	}
	
	public int setActiveUserGroup(){
		Log.d(TAG,"+++ setActiveUserGroup +++");
		return mFPDev.setActiveUserGroup(0, DATA_PATH);
	}

	public int setActiveUserGroup(String userPath) {
		if (userPath != null) {
			return mFPDev.setActiveUserGroup(0, userPath);
		} else {
			return setActiveUserGroup();
		}
	}

	public int setDataPath(int type, String data_path){
		Log.d(TAG,"+++ setDataPath +++");
		return mFPDev.setDataPath(type, data_path);
	}
	
	public int enroll(int user_id, int fingerprint_id){
		Log.d(TAG,"+++ enroll +++");
		return mFPDev.enroll(user_id, fingerprint_id);
	}
	
	public int postEnroll(){
		Log.d(TAG, "+++ postEnroll +++");
		return mFPDev.postEnroll();
	}
	
	public int verify(int user_id, int[] finger_ids, int finger_cout, long challenge){
		Log.d(TAG,"+++ verify +++");
		return mFPDev.verify(user_id, finger_ids, finger_cout, challenge);
	}
	
	public int removeFingerprint(int user_id, int finger_id){
		Log.d(TAG,"+++ removeFingerprint +++");
		return mFPDev.removeFingerprint(user_id, finger_id);
	}
	
	public int getFingerprintList(int user_id, int[] figner_ids, int[] finger_cout){
		Log.d(TAG,"+++ getFingerprintList +++");
		return mFPDev.getFingerprintList(user_id, figner_ids, finger_cout);
	}

	private int setCallback() {
		return mFPDev.setCallback();
	}
}
