package rbs.egistec.com.fplib.service;

public interface IRbsServiceNative_CB extends android.os.IInterface
{
/** Local-side IPC implementation stub class. */
public static abstract class Stub extends android.os.Binder implements IRbsServiceNative_CB
{
private static final java.lang.String DESCRIPTOR = "rbs.egistec.com.binder_client.service.IRbsServiceNative_CB";
/** Construct the stub at attach it to the interface. */
public Stub()
{
this.attachInterface(this, DESCRIPTOR);
}
/**
 * Cast an IBinder object into an egistec.edk.service.IRbsServiceNative_CB interface,
 * generating a proxy if needed.
 */
public static IRbsServiceNative_CB asInterface(android.os.IBinder obj)
{
if ((obj==null)) {
return null;
}
android.os.IInterface iin = obj.queryLocalInterface(DESCRIPTOR);
if (((iin!=null)&&(iin instanceof IRbsServiceNative_CB))) {
return ((IRbsServiceNative_CB)iin);
}
return new IRbsServiceNative_CB.Stub.Proxy(obj);
}
@Override public android.os.IBinder asBinder()
{
return this;
}
@Override public boolean onTransact(int code, android.os.Parcel data, android.os.Parcel reply, int flags) throws android.os.RemoteException
{
switch (code)
{
case INTERFACE_TRANSACTION:
{
reply.writeString(DESCRIPTOR);
return true;
}
case TRANSACTION_do_native_callback:
{
data.enforceInterface(DESCRIPTOR);
int _arg0;
_arg0 = data.readInt();
int _arg1;
_arg1 = data.readInt();
int _arg2;
_arg2 = data.readInt();
byte[] _arg3;
_arg3 = data.createByteArray();
int _arg4;
_arg4= _arg3.length;
this.do_native_callback(_arg0, _arg1, _arg2, _arg3, _arg4);
reply.writeNoException();
return true;
}
}
return super.onTransact(code, data, reply, flags);
}
private static class Proxy implements IRbsServiceNative_CB
{
private android.os.IBinder mRemote;
Proxy(android.os.IBinder remote)
{
mRemote = remote;
}
@Override public android.os.IBinder asBinder()
{
return mRemote;
}
public java.lang.String getInterfaceDescriptor()
{
return DESCRIPTOR;
}
@Override public void do_native_callback(int event_id, int value1, int value2, byte[] buffer, int buffer_size) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
try {
_data.writeInterfaceToken(DESCRIPTOR);
_data.writeInt(event_id);
_data.writeInt(value1);
_data.writeInt(value2);
if ((buffer==null)) {
_data.writeInt(-1);
}
else {
_data.writeByteArray(buffer);
_data.writeInt(buffer_size);
}
mRemote.transact(Stub.TRANSACTION_do_native_callback, _data, _reply, 0);
_reply.readException();
}
finally {
_reply.recycle();
_data.recycle();
}
}
}
static final int TRANSACTION_do_native_callback = (android.os.IBinder.FIRST_CALL_TRANSACTION + 0);
}
public void do_native_callback(int event_id, int value1, int value2, byte[] buffer, int buffer_size) throws android.os.RemoteException;
}
