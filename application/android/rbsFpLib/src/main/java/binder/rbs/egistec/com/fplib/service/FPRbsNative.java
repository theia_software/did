package rbs.egistec.com.fplib.service;

import android.os.Handler;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;
import java.lang.reflect.Method;
import java.util.Arrays;

import rbs.egistec.com.fplib.api.Common;
import rbs.egistec.com.fplib.api.FpResDef;

public class FPRbsNative implements IBinder.DeathRecipient{

	private static final int NO_SERVICE = -1;
	private static final int RUN_ERROR	 = -2;
	private static final String SERVICE_NAME = "egistec.rbs.service.daemon";
	private static final String TAG = "FPRbsNative";
	private static Handler mApHandler;
	private IRbsServiceNative mRbsService;
	
	public FPRbsNative(Handler handler){
		Log.d(TAG, "FPRbsNative Constructor");
		mApHandler = handler;
		getService();
	}
	
	public boolean getService() {
		Log.d(TAG, "getService enter");
		if(mRbsService != null){
			return true;
		}

		try {
			Method method = Class.forName("android.os.ServiceManager").getMethod("getService", String.class);
			IBinder binder = (IBinder) method.invoke(null, SERVICE_NAME);
			mRbsService = IRbsServiceNative.Stub.asInterface(binder);
		} catch (Exception e) {
			e.printStackTrace();
		}

		if(mRbsService == null){
			Log.e(TAG, "get service fail");
			return false;
		}
		
		Log.d(TAG, "get service success");
		try {
			mRbsService.asBinder().linkToDeath(this, 0);
		} catch (RemoteException e1) {
			e1.printStackTrace();
			mRbsService = null;
			return false;
		}
		
		return true;
	}
	
	static IRbsServiceNative_CB mCallback = new IRbsServiceNative_CB.Stub() {
		
		@Override
		public void do_native_callback(int event_id, int value1, int value2,
				byte[] buffer, int buffer_size) throws RemoteException {
			Log.d(TAG, "IRbsServiceNative_CB mCallback enter");
			mApHandler.obtainMessage(event_id, value1, value2, buffer).sendToTarget();
		}
	};
	
	public int extraApi(int pid, int cid, byte[] in_buffer, byte[] out_buffer, int[] out_buffer_size){
		int ret = 0;
		if(getService()) {
			try {
				int in_buffer_size = 0;
				if(in_buffer != null) {
					in_buffer_size = in_buffer.length;
				}
				byte[] in_msg = new byte[in_buffer_size + Integer.SIZE / 8];
				byte[] cid_buf = new byte[Integer.SIZE / 8];
				Common.intToBytes(cid, cid_buf);

				System.arraycopy(cid_buf, 0, in_msg, 0, cid_buf.length);
				if(in_buffer != null) {
					System.arraycopy(in_buffer, 0, in_msg, cid_buf.length, in_buffer.length);
				}

				ret = mRbsService.extra_api(pid, in_msg, out_buffer, out_buffer_size);
				Log.d(TAG, "extra_api, ret: " + ret);
			} catch (RemoteException e) {
				ret = RUN_ERROR;
				e.printStackTrace();
			}
		}else{
			Log.d(TAG, "no service");
			ret = NO_SERVICE;
		}
		
		return ret;
	}

	public int uninitialize(){
		int ret = 0;		
		if(getService()) {
			try {
				ret = mRbsService.uninitialize();
				Log.d(TAG, "uninitialize, ret: " + ret);
			} catch (RemoteException e) {
				ret = RUN_ERROR;
				e.printStackTrace();
			}
		} else {
			Log.d(TAG, "no service");
			ret = NO_SERVICE;
		}
		
		return ret;
	}
	
	public int cancel(){
		int ret = 0;
		
		if(getService()) {
			try {
				ret = mRbsService.cancel();
				Log.d(TAG, "cancel, ret: " + ret);
			} catch (RemoteException e) {
				ret = RUN_ERROR;
				e.printStackTrace();
			}
		} else {
			Log.d(TAG, "no service");
			ret = NO_SERVICE;
		}
		
		return ret;
	}
	
	public int setActiveUserGroup(int user_id, String data_path){
		int ret = 0;

		if(getService()) {
			try {
                byte[] path_buf = data_path.getBytes();
                byte[] path_dest = new byte[path_buf.length + 1];
                Arrays.fill(path_dest,(byte)0);
                System.arraycopy(path_buf, 0, path_dest, 0, path_buf.length);
                ret = mRbsService.active_user_group(user_id, path_dest, path_dest.length);
				Log.d(TAG, "active_user_group, ret: " + ret);
			} catch (RemoteException e) {
				ret = RUN_ERROR;
				e.printStackTrace();
			}
		} else {
			Log.d(TAG, "no service");
			ret = NO_SERVICE;
		}
		
		return ret;
	}
	
	public int setDataPath(int type, String data_path){
		int ret = 0;
		if(getService()) {
			try {
				ret = mRbsService.set_data_path(type, data_path.getBytes(), data_path.length());
				Log.d(TAG, "set_data_path, ret: " + ret);
			} catch (RemoteException e) {
				ret = RUN_ERROR;
				e.printStackTrace();
			}
		} else {
			Log.d(TAG, "no service");
			ret = NO_SERVICE;
		}
		
		return ret;
	}
	
	public int enroll(int user_id, int fingerprint_id){
		int ret = 0;
		if(getService()) {
			try {
				ret = mRbsService.pre_enroll(user_id, fingerprint_id);
				Log.d(TAG, "pre_enroll, ret: " + ret);
			} catch (RemoteException e1) {
				ret = RUN_ERROR;
				e1.printStackTrace();
			}
			Log.d(TAG, "preEnroll success");
			
			if(ret == 0){
				try {
					ret = mRbsService.enroll();
					Log.d(TAG, "enroll, ret: " + ret);
				} catch (RemoteException e) {
					ret = RUN_ERROR;
					e.printStackTrace();
				}
			}
		} else {
			Log.d(TAG, "no service");
			ret = NO_SERVICE;
		}
		
		return ret;
	}
	
	public int postEnroll() {
		int ret = 0;
		if(getService()) {
			try {
				ret = mRbsService.post_enroll();
				Log.d(TAG, "post_enroll, ret: " + ret);
			} catch (RemoteException e) {
				ret = RUN_ERROR;
				e.printStackTrace();
			}
		} else {
			Log.d(TAG, "no service");
			ret = NO_SERVICE;
		}
		
		return ret;
	}
	
	public int verify(int user_id, int[] finger_ids, int finger_cout, long challenge){
		int ret = 0;
		if(getService()) {
			try {
				ret = mRbsService.verify(user_id, finger_ids, finger_cout, challenge);
				Log.d(TAG, "verify, ret: " + ret);
			} catch (RemoteException e) {
				ret = RUN_ERROR;
				e.printStackTrace();
			}
		} else {
			Log.d(TAG, "no service");
			ret = NO_SERVICE;
		}
		
		return ret;
	}
	
	public int removeFingerprint(int user_id, int finger_id) {
		int ret = 0;
		if(getService()){
			try {
				ret = mRbsService.remove_fingerprint(user_id, finger_id);
				Log.d(TAG, "remove_fingerprint, ret: " + ret);
			} catch (RemoteException e) {
				ret = RUN_ERROR;
				e.printStackTrace();
			}
		} else {
			Log.d(TAG, "no service");
			ret = NO_SERVICE;
		}
		
		return ret;
	}
	
	public int getFingerprintList(int user_id, int[] figner_ids, int[] finger_cout) {
		int ret = 0;
		if(getService()) {
			try {
				ret = mRbsService.get_fingerprint_ids(user_id, figner_ids, finger_cout);
				Log.d(TAG, "get_fingerprint_ids, ret: " + ret);
			} catch (RemoteException e) {
				ret = RUN_ERROR;
				e.printStackTrace();
			}
		} else {
			Log.d(TAG, "no service");
			ret = NO_SERVICE;
		}
		
		return ret;
	}

	public int setCallback() {
		int retval = NO_SERVICE;
		Log.d(TAG, "setCallback enter");

		if(getService()) {
			try {
				if(mRbsService.set_on_callback_proc(mCallback) != 0) {
                    retval = RUN_ERROR;
                }
			} catch (RemoteException e) {
				e.printStackTrace();
			}
		}

		return retval;

	}

	@Override
	public void binderDied() {
		Log.d(TAG, "RBS Daemon binder Died");
		mRbsService = null;
	}
}






























