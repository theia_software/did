package rbs.egistec.com.fplib.service;

public interface IRbsServiceNative extends android.os.IInterface {
	/** Local-side IPC implementation stub class. */
	public static abstract class Stub extends android.os.Binder implements IRbsServiceNative
	{
		private static final java.lang.String DESCRIPTOR = "rbs.egistec.com.binder_client.service.IRbsServiceNative";
		/** Construct the stub at attach it to the interface. */
		public Stub()
		{
			this.attachInterface(this, DESCRIPTOR);
		}
		/**
 * Cast an IBinder object into an rbs.egistec.com.binder_client.service.IRbsServiceNative interface,
 * generating a proxy if needed.
 */
		public static IRbsServiceNative asInterface(android.os.IBinder obj)
		{
			if ((obj == null)) {
				return null;
			}
			android.os.IInterface iin = obj.queryLocalInterface(DESCRIPTOR);
			if (((iin != null) && (iin instanceof IRbsServiceNative))) {
				return ((IRbsServiceNative) iin);
			}
			return new IRbsServiceNative.Stub.Proxy(obj);
		}
		@Override
		public android.os.IBinder asBinder()
		{
			return this;
		}
		@Override
		public boolean onTransact(int code, android.os.Parcel data, android.os.Parcel reply, int flags) throws android.os.RemoteException
		{
			switch (code) {
				case INTERFACE_TRANSACTION: {
					reply.writeString(DESCRIPTOR);
					return true;
				}
				case TRANSACTION_extra_api: {
					data.enforceInterface(DESCRIPTOR);
					int _arg0;
					_arg0 = data.readInt();
					byte[] _arg1;
					_arg1 = data.createByteArray();
					int _arg2;
					_arg2 = data.readInt();
					byte[] _arg3;
					int _arg3_length = data.readInt();
					if ((_arg3_length < 0)) {
						_arg3 = null;
					} else {
						_arg3 = new byte[_arg3_length];
					}
					int[] _arg4;
					int _arg4_length = data.readInt();
					if ((_arg4_length < 0)) {
						_arg4 = null;
					} else {
						_arg4 = new int[_arg4_length];
					}
					int _result = this.extra_api(_arg0, _arg1, _arg3, _arg4);
					reply.writeNoException();
					reply.writeInt(_result);
					reply.writeByteArray(_arg3);
					reply.writeIntArray(_arg4);
					return true;
				}
				case TRANSACTION_set_on_callback_proc: {
					data.enforceInterface(DESCRIPTOR);
					IRbsServiceNative_CB _arg0;
					_arg0 = IRbsServiceNative_CB.Stub.asInterface(data.readStrongBinder());
					int _result = this.set_on_callback_proc(_arg0);
					reply.writeNoException();
					reply.writeInt(_result);
					return true;
				}
				case TRANSACTION_uninitialize: {
					data.enforceInterface(DESCRIPTOR);
					int _result = this.uninitialize();
					reply.writeNoException();
					reply.writeInt(_result);
					return true;
				}
				case TRANSACTION_cancel: {
					data.enforceInterface(DESCRIPTOR);
					int _result = this.cancel();
					reply.writeNoException();
					reply.writeInt(_result);
					return true;
				}
				case TRANSACTION_active_user_group: {
					data.enforceInterface(DESCRIPTOR);
					int _arg0;
					_arg0 = data.readInt();
					byte[] _arg1;
					_arg1 = data.createByteArray();
					int _arg2;
					_arg2 = data.readInt();
					int _result = this.active_user_group(_arg0, _arg1, _arg2);
					reply.writeNoException();
					reply.writeInt(_result);
					return true;
				}
				case TRANSACTION_set_data_path: {
					data.enforceInterface(DESCRIPTOR);
					int _arg0;
					_arg0 = data.readInt();
					byte[] _arg1;
					_arg1 = data.createByteArray();
					int _arg2;
					_arg2 = data.readInt();
					int _result = this.set_data_path(_arg0, _arg1, _arg2);
					reply.writeNoException();
					reply.writeInt(_result);
					return true;
				}
				case TRANSACTION_pre_enroll: {
					data.enforceInterface(DESCRIPTOR);
					int _arg0;
					_arg0 = data.readInt();
					int _arg1;
					_arg1 = data.readInt();
					int _result = this.pre_enroll(_arg0, _arg1);
					reply.writeNoException();
					reply.writeInt(_result);
					return true;
				}
				case TRANSACTION_enroll: {
					data.enforceInterface(DESCRIPTOR);
					int _result = this.enroll();
					reply.writeNoException();
					reply.writeInt(_result);
					return true;
				}
				case TRANSACTION_post_enroll: {
					data.enforceInterface(DESCRIPTOR);
					int _result = this.post_enroll();
					reply.writeNoException();
					reply.writeInt(_result);
					return true;
				}
				case TRANSACTION_verify: {
					data.enforceInterface(DESCRIPTOR);
					int _arg0;
					_arg0 = data.readInt();
					int[] _arg1;
					_arg1 = data.createIntArray();
					int _arg2;
					_arg2 = data.readInt();
					long _arg3;
					_arg3 = data.readLong();
					int _result = this.verify(_arg0, _arg1, _arg2, _arg3);
					reply.writeNoException();
					reply.writeInt(_result);
					return true;
				}
				case TRANSACTION_remove_fingerprint: {
					data.enforceInterface(DESCRIPTOR);
					int _arg0;
					_arg0 = data.readInt();
					int _arg1;
					_arg1 = data.readInt();
					int _result = this.remove_fingerprint(_arg0, _arg1);
					reply.writeNoException();
					reply.writeInt(_result);
					return true;
				}
				case TRANSACTION_get_fingerprint_ids: {
					data.enforceInterface(DESCRIPTOR);
					int _arg0;
					_arg0 = data.readInt();
					int[] _arg1;
					int _arg1_length = data.readInt();
					if ((_arg1_length < 0)) {
						_arg1 = null;
					} else {
						_arg1 = new int[_arg1_length];
					}
					int[] _arg2;
					int _arg2_length = data.readInt();
					if ((_arg2_length < 0)) {
						_arg2 = null;
					} else {
						_arg2 = new int[_arg2_length];
					}
					int _result = this.get_fingerprint_ids(_arg0, _arg1, _arg2);
					reply.writeNoException();
					reply.writeInt(_result);
					reply.writeIntArray(_arg1);
					reply.writeIntArray(_arg2);
					return true;
				}
			}
			return super.onTransact(code, data, reply, flags);
		}
		private static class Proxy implements IRbsServiceNative {
			private android.os.IBinder mRemote;

			Proxy(android.os.IBinder remote) {
				mRemote = remote;
			}

			@Override
			public android.os.IBinder asBinder() {
				return mRemote;
			}

			public java.lang.String getInterfaceDescriptor() {
				return DESCRIPTOR;
			}

			@Override
			public int extra_api(int pid, byte[] in_buffer, byte[] out_buffer, int[] out_buffer_size) throws android.os.RemoteException {
				android.os.Parcel _data = android.os.Parcel.obtain();
				android.os.Parcel _reply = android.os.Parcel.obtain();
				int _result;
				try {
					_data.writeInterfaceToken(DESCRIPTOR);
					_data.writeInt(pid);
					_data.writeByteArray(in_buffer);
					_data.writeByteArray(out_buffer);

					mRemote.transact(Stub.TRANSACTION_extra_api, _data, _reply, 0);
					_reply.readException();
					_result = _reply.readInt();
					if (_result == 0 && out_buffer != null && out_buffer_size != null) {
						byte[] tmp_buf = _reply.createByteArray();
						if (tmp_buf != null && tmp_buf.length <= out_buffer.length) {
							System.arraycopy(tmp_buf, 0, out_buffer, 0, tmp_buf.length);
							out_buffer_size[0] = tmp_buf.length;
						} else {
							out_buffer_size[0] = 0;
						}
					}
				} finally {
					_reply.recycle();
					_data.recycle();
				}
				return _result;
			}

				@Override
				public int set_on_callback_proc(IRbsServiceNative_CB callback) throws android.os.RemoteException
				{
					android.os.Parcel _data = android.os.Parcel.obtain();
					android.os.Parcel _reply = android.os.Parcel.obtain();
					int _result;
					try {
						_data.writeInterfaceToken(DESCRIPTOR);
						_data.writeStrongBinder((((callback != null)) ? (callback.asBinder()) : (null)));
						mRemote.transact(Stub.TRANSACTION_set_on_callback_proc, _data, _reply, 0);
						_reply.readException();
						_result = _reply.readInt();
					} finally {
						_reply.recycle();
						_data.recycle();
					}
					return _result;
				}
				@Override
				public int uninitialize() throws android.os.RemoteException
				{
					android.os.Parcel _data = android.os.Parcel.obtain();
					android.os.Parcel _reply = android.os.Parcel.obtain();
					int _result;
					try {
						_data.writeInterfaceToken(DESCRIPTOR);
						mRemote.transact(Stub.TRANSACTION_uninitialize, _data, _reply, 0);
						_reply.readException();
						_result = _reply.readInt();
					} finally {
						_reply.recycle();
						_data.recycle();
					}
					return _result;
				}
				@Override
				public int cancel() throws android.os.RemoteException
				{
					android.os.Parcel _data = android.os.Parcel.obtain();
					android.os.Parcel _reply = android.os.Parcel.obtain();
					int _result;
					try {
						_data.writeInterfaceToken(DESCRIPTOR);
						mRemote.transact(Stub.TRANSACTION_cancel, _data, _reply, 0);
						_reply.readException();
						_result = _reply.readInt();
					} finally {
						_reply.recycle();
						_data.recycle();
					}
					return _result;
				}
				@Override
				public int active_user_group(int user_id, byte[] data_path, int data_path_size) throws android.os.RemoteException
				{
					android.os.Parcel _data = android.os.Parcel.obtain();
					android.os.Parcel _reply = android.os.Parcel.obtain();
					int _result;
					try {
						_data.writeInterfaceToken(DESCRIPTOR);
						_data.writeInt(user_id);
						_data.writeByteArray(data_path);
						_data.writeInt(data_path_size);
						mRemote.transact(Stub.TRANSACTION_active_user_group, _data, _reply, 0);
						_reply.readException();
						_result = _reply.readInt();
					} finally {
						_reply.recycle();
						_data.recycle();
					}
					return _result;
				}
				@Override
				public int set_data_path(int type, byte[] data_path, int path_len) throws android.os.RemoteException
				{
					android.os.Parcel _data = android.os.Parcel.obtain();
					android.os.Parcel _reply = android.os.Parcel.obtain();
					int _result;
					try {
						_data.writeInterfaceToken(DESCRIPTOR);
						_data.writeInt(type);
						_data.writeByteArray(data_path);
						_data.writeInt(path_len);
						mRemote.transact(Stub.TRANSACTION_set_data_path, _data, _reply, 0);
						_reply.readException();
						_result = _reply.readInt();
					} finally {
						_reply.recycle();
						_data.recycle();
					}
					return _result;
				}
				@Override
				public int pre_enroll(int user_id, int fingerprint_id) throws android.os.RemoteException
				{
					android.os.Parcel _data = android.os.Parcel.obtain();
					android.os.Parcel _reply = android.os.Parcel.obtain();
					int _result;
					try {
						_data.writeInterfaceToken(DESCRIPTOR);
						_data.writeInt(user_id);
						_data.writeInt(fingerprint_id);
						mRemote.transact(Stub.TRANSACTION_pre_enroll, _data, _reply, 0);
						_reply.readException();
						_result = _reply.readInt();
					} finally {
						_reply.recycle();
						_data.recycle();
					}
					return _result;
				}
				@Override
				public int enroll() throws android.os.RemoteException
				{
					android.os.Parcel _data = android.os.Parcel.obtain();
					android.os.Parcel _reply = android.os.Parcel.obtain();
					int _result;
					try {
						_data.writeInterfaceToken(DESCRIPTOR);
						mRemote.transact(Stub.TRANSACTION_enroll, _data, _reply, 0);
						_reply.readException();
						_result = _reply.readInt();
					} finally {
						_reply.recycle();
						_data.recycle();
					}
					return _result;
				}
				@Override
				public int post_enroll() throws android.os.RemoteException
				{
					android.os.Parcel _data = android.os.Parcel.obtain();
					android.os.Parcel _reply = android.os.Parcel.obtain();
					int _result;
					try {
						_data.writeInterfaceToken(DESCRIPTOR);
						mRemote.transact(Stub.TRANSACTION_post_enroll, _data, _reply, 0);
						_reply.readException();
						_result = _reply.readInt();
					} finally {
						_reply.recycle();
						_data.recycle();
					}
					return _result;
				}
				@Override
				public int verify(int user_id, int[] finger_ids, int finger_cout, long challenge) throws android.os.RemoteException
				{
					android.os.Parcel _data = android.os.Parcel.obtain();
					android.os.Parcel _reply = android.os.Parcel.obtain();
					int _result;
					try {
						_data.writeInterfaceToken(DESCRIPTOR);
						_data.writeInt(user_id);
						_data.writeIntArray(finger_ids);
						_data.writeInt(finger_cout);
						_data.writeLong(challenge);
						mRemote.transact(Stub.TRANSACTION_verify, _data, _reply, 0);
						_reply.readException();
						_result = _reply.readInt();
					} finally {
						_reply.recycle();
						_data.recycle();
					}
					return _result;
				}
				@Override
				public int remove_fingerprint(int user_id, int finger_id) throws android.os.RemoteException
				{
					android.os.Parcel _data = android.os.Parcel.obtain();
					android.os.Parcel _reply = android.os.Parcel.obtain();
					int _result;
					try {
						_data.writeInterfaceToken(DESCRIPTOR);
						_data.writeInt(user_id);
						_data.writeInt(finger_id);
						mRemote.transact(Stub.TRANSACTION_remove_fingerprint, _data, _reply, 0);
						_reply.readException();
						_result = _reply.readInt();
					} finally {
						_reply.recycle();
						_data.recycle();
					}
					return _result;
				}
				@Override
				public int get_fingerprint_ids(int user_id, int[] finger_ids, int[] finger_cout) throws android.os.RemoteException
				{
					android.os.Parcel _data = android.os.Parcel.obtain();
					android.os.Parcel _reply = android.os.Parcel.obtain();
					int _result;
					try {
						_data.writeInterfaceToken(DESCRIPTOR);
						_data.writeInt(user_id);
						if ((finger_ids == null)) {
							_data.writeInt(-1);
						} else {
							_data.writeInt(finger_ids.length);
						}
						if ((finger_cout == null)) {
							_data.writeInt(-1);
						} else {
							_data.writeInt(finger_cout.length);
						}
						mRemote.transact(Stub.TRANSACTION_get_fingerprint_ids, _data, _reply, 0);
						_reply.readException();
						_result = _reply.readInt();
						if (_result == 0 && finger_ids != null && finger_cout != null) {
							int[] tmp_buf = _reply.createIntArray();
							finger_cout[0] = tmp_buf.length;
							System.arraycopy(tmp_buf, 0, finger_ids, 0, finger_cout[0]);
						}
					} finally {
						_reply.recycle();
						_data.recycle();
					}
					return _result;
				}
			}
			static final int TRANSACTION_extra_api = (android.os.IBinder.FIRST_CALL_TRANSACTION + 0);
			static final int TRANSACTION_set_on_callback_proc = (android.os.IBinder.FIRST_CALL_TRANSACTION + 1);
			static final int TRANSACTION_uninitialize = (android.os.IBinder.FIRST_CALL_TRANSACTION + 2);
			static final int TRANSACTION_cancel = (android.os.IBinder.FIRST_CALL_TRANSACTION + 3);
			static final int TRANSACTION_active_user_group = (android.os.IBinder.FIRST_CALL_TRANSACTION + 4);
			static final int TRANSACTION_set_data_path = (android.os.IBinder.FIRST_CALL_TRANSACTION + 5);
			static final int TRANSACTION_pre_enroll = (android.os.IBinder.FIRST_CALL_TRANSACTION + 6);
			static final int TRANSACTION_enroll = (android.os.IBinder.FIRST_CALL_TRANSACTION + 7);
			static final int TRANSACTION_post_enroll = (android.os.IBinder.FIRST_CALL_TRANSACTION + 8);
			static final int TRANSACTION_verify = (android.os.IBinder.FIRST_CALL_TRANSACTION + 9);
			static final int TRANSACTION_remove_fingerprint = (android.os.IBinder.FIRST_CALL_TRANSACTION + 10);
			static final int TRANSACTION_get_fingerprint_ids = (android.os.IBinder.FIRST_CALL_TRANSACTION + 11);
		}
		public int extra_api(int pid, byte[] in_buffer, byte[] out_buffer, int[] out_buffer_size) throws android.os.RemoteException;
		public int set_on_callback_proc(IRbsServiceNative_CB callback) throws android.os.RemoteException;
		public int uninitialize() throws android.os.RemoteException;
		public int cancel() throws android.os.RemoteException;
		public int active_user_group(int user_id, byte[] data_path, int data_path_size) throws android.os.RemoteException;
		public int set_data_path(int type, byte[] data_path, int path_len) throws android.os.RemoteException;
		public int pre_enroll(int user_id, int fingerprint_id) throws android.os.RemoteException;
		public int enroll() throws android.os.RemoteException;
		public int post_enroll() throws android.os.RemoteException;
		public int verify(int user_id, int[] finger_ids, int finger_cout, long challenge) throws android.os.RemoteException;
		public int remove_fingerprint(int user_id, int finger_id) throws android.os.RemoteException;
		public int get_fingerprint_ids(int user_id, int[] figner_ids, int[] finger_cout) throws android.os.RemoteException;
	}
