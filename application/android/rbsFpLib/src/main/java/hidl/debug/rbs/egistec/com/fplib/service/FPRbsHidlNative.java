package rbs.egistec.com.fplib.service;

import android.os.Handler;
import android.os.IHwBinder;
import android.os.RemoteException;
import android.util.Log;

import java.util.ArrayList;

import rbs.egistec.com.fplib.api.Common;
import rbs.egistec.com.fplib.api.FpResDef;
import vendor.egistec.hardware.fingerprintdebug.V1_0.IBiometricsFingerprintRbsDebug;
import vendor.egistec.hardware.fingerprintdebug.V1_0.IBiometricsFingerprintRbsDebugCallback;

public class FPRbsHidlNative implements IHwBinder.DeathRecipient{

	private static final int NO_SERVICE = -1;
	private static final int RUN_ERROR	 = -2;
	private static final String SERVICE_NAME = "egistec.rbs.service.daemon";
	private static final String TAG = "FPRbsNative";
	private static Handler mApHandler;
	private IBiometricsFingerprintRbsDebug mRbsService;

	public FPRbsHidlNative(Handler handler){
		Log.d(TAG, "FPRbsNative debug Constructor");
		mApHandler = handler;
		getService();
	}
	
	public boolean getService() {
		Log.d(TAG, "getService enter");
		if(mRbsService != null){
			return true;
		}

		try {
			mRbsService = IBiometricsFingerprintRbsDebug.getService();
		} catch (Exception e) {
			e.printStackTrace();
		}

		if(mRbsService == null){
			Log.e(TAG, "get service fail");
			return false;
		}
		
		Log.d(TAG, "get service success");
		mRbsService.asBinder().linkToDeath(this, 0);

		return true;
	}
	
	private IBiometricsFingerprintRbsDebugCallback mCallback = new IBiometricsFingerprintRbsDebugCallback.Stub() {

		@Override
		public void ipc_callback(int event_id, int value1, int value2, ArrayList<Byte> buffer, int buffer_size) throws RemoteException {
			Log.d(TAG, "ipc_callback() eventId=" + event_id + " value1 = " + value1 + " value2 = " + value2 + "buffer_size = " + buffer_size);
			int size = buffer.size();
			byte [] buffer2 = new byte[size];
			for(int i =0; i < size; i++)
			{
				buffer2[i] = buffer.get(i);
			}

			mApHandler.obtainMessage(event_id, value1, value2, buffer2).sendToTarget();
		}
	};
	
	public int extraApi(int pid, int cid, byte[] in_buffer, byte[] out_buffer, int[] out_buffer_size){
		int ret = 0;
		if(getService()) {
			try {
				int in_buffer_size = 0;
				if(in_buffer != null) {
					in_buffer_size = in_buffer.length;
				}
				byte[] in_msg = new byte[in_buffer_size + Integer.SIZE / 8];
				byte[] cid_buf = new byte[Integer.SIZE / 8];
				Common.intToBytes(cid, cid_buf);

				ArrayList vec_buf = new ArrayList<Byte>();

				for(int i = 0 ; i < cid_buf.length ; i++) {
					vec_buf.add(cid_buf[i]);
				}

				if(in_buffer != null) {
					for(int i = cid_buf.length ; i < (cid_buf.length + in_buffer.length); i++) {
						vec_buf.add(in_buffer[i - cid_buf.length]);
					}
				}

				ArrayList<Byte> vec_outBuf= mRbsService.extra_api(pid, vec_buf);
 				ret = vec_outBuf.get(0)<<24 + vec_outBuf.get(1)<<16 +vec_outBuf.get(2)<<8 + vec_outBuf.get(3);

				if ((out_buffer != null) && (vec_outBuf.size() > 4)){
					for(int i = 0 ; i < vec_outBuf.size() ; i++){
						Log.d("TAG", String.valueOf(vec_outBuf.get(i)));
						out_buffer[i] = vec_outBuf.get(i + 4);
					}
				}

				if (out_buffer_size != null)
					out_buffer_size[0] = vec_outBuf.size() - 4;

				Log.d(TAG, "extra_api, ret: " + ret);
			} catch (RemoteException e) {
				ret = RUN_ERROR;
				e.printStackTrace();
			}
		}else{
			Log.d(TAG, "no service");
			ret = NO_SERVICE;
		}
		
		return ret;
	}
	
	public int uninitialize(){
		int ret = 0;		
		if(getService()) {
			try {
				ret = mRbsService.uninitialize();
				Log.d(TAG, "uninitialize, ret: " + ret);
			} catch (RemoteException e) {
				ret = RUN_ERROR;
				e.printStackTrace();
			}
		} else {
			Log.d(TAG, "no service");
			ret = NO_SERVICE;
		}
		
		return ret;
	}
	
	public int cancel(){
		int ret = 0;
		
		if(getService()) {
			try {
				ret = mRbsService.cancel();
				Log.d(TAG, "cancel, ret: " + ret);
			} catch (RemoteException e) {
				ret = RUN_ERROR;
				e.printStackTrace();
			}
		} else {
			Log.d(TAG, "no service");
			ret = NO_SERVICE;
		}
		
		return ret;
	}
	
	public int setActiveUserGroup(int user_id, String data_path){
		int ret = 0;

		if(getService()) {
			try {
//                byte[] path_buf = data_path.getBytes();
//                byte[] path_dest = new byte[path_buf.length + 1];
//                Arrays.fill(path_dest,(byte)0);
//                System.arraycopy(path_buf, 0, path_dest, 0, path_buf.length);
                ret = mRbsService.active_user_group(user_id, data_path);
				Log.d(TAG, "active_user_group, ret: " + ret);
			} catch (RemoteException e) {
				ret = RUN_ERROR;
				e.printStackTrace();
			}
		} else {
			Log.d(TAG, "no service");
			ret = NO_SERVICE;
		}
		
		return ret;
	}
	
	public int setDataPath(int type, String data_path){
		int ret = 0;
		if(getService()) {
			try {
				ret = mRbsService.set_data_path(type, data_path);
				Log.d(TAG, "set_data_path, ret: " + ret);
			} catch (RemoteException e) {
				ret = RUN_ERROR;
				e.printStackTrace();
			}
		} else {
			Log.d(TAG, "no service");
			ret = NO_SERVICE;
		}
		
		return ret;
	}
	
	public int enroll(int user_id, int fingerprint_id){
		int ret = 0;
		if(getService()) {
			try {
				ret = mRbsService.pre_enroll(user_id, fingerprint_id);
				Log.d(TAG, "pre_enroll, ret: " + ret);
			} catch (RemoteException e1) {
				ret = RUN_ERROR;
				e1.printStackTrace();
			}
			Log.d(TAG, "preEnroll success");
			
			if(ret == 0){
				try {
					ret = mRbsService.enroll();
					Log.d(TAG, "enroll, ret: " + ret);
				} catch (RemoteException e) {
					ret = RUN_ERROR;
					e.printStackTrace();
				}
			}
		} else {
			Log.d(TAG, "no service");
			ret = NO_SERVICE;
		}
		
		return ret;
	}
	
	public int postEnroll() {
		int ret = 0;
		if(getService()) {
			try {
				ret = mRbsService.post_enroll();
				Log.d(TAG, "post_enroll, ret: " + ret);
			} catch (RemoteException e) {
				ret = RUN_ERROR;
				e.printStackTrace();
			}
		} else {
			Log.d(TAG, "no service");
			ret = NO_SERVICE;
		}
		
		return ret;
	}
	
	public int verify(int user_id, int[] finger_ids, int finger_cout, long challenge){
		int ret = 0;
		if(getService()) {
			try {
				ArrayList<Integer> vec_finger_ids =  new ArrayList<Integer>();

				for(int i = 0 ; i < finger_ids.length ; i++) {
					vec_finger_ids.add(finger_ids[i]);
				}

				ret = mRbsService.verify(user_id, vec_finger_ids, finger_cout, challenge);
				Log.d(TAG, "verify, ret: " + ret);
			} catch (RemoteException e) {
				ret = RUN_ERROR;
				e.printStackTrace();
			}
		} else {
			Log.d(TAG, "no service");
			ret = NO_SERVICE;
		}
		
		return ret;
	}
	
	public int removeFingerprint(int user_id, int finger_id) {
		int ret = 0;
		if(getService()){
			try {
				ret = mRbsService.remove_fingerprint(user_id, finger_id);
				Log.d(TAG, "remove_fingerprint, ret: " + ret);
			} catch (RemoteException e) {
				ret = RUN_ERROR;
				e.printStackTrace();
			}
		} else {
			Log.d(TAG, "no service");
			ret = NO_SERVICE;
		}
		
		return ret;
	}
	
	public int getFingerprintList(int user_id, int[] finger_ids, int[] finger_cout) {
		int ret = 0;
		if(getService()) {
			try {
				ArrayList<Integer> fingerList;
				fingerList = mRbsService.get_fingerprint_ids(user_id);
				ret = fingerList.get(0);

				for(int i = 1 ; i < 6 ; i++) {
					finger_ids[i - 1] = fingerList.get(i);
				}

				for(int i = 6 ; i < fingerList.size() ; i++) {
					finger_cout[i - 6] = fingerList.get(i);
				}

				Log.d(TAG, "get_fingerprint_ids, ret: " + ret);
			} catch (RemoteException e) {
				ret = RUN_ERROR;
				e.printStackTrace();
			}
		} else {
			Log.d(TAG, "no service");
			ret = NO_SERVICE;
		}
		
		return ret;
	}
/*
	public int intToBytes( int value , byte[] des)
	{
		des[3] =  (byte) ((value>>24) & 0xFF);
		des[2] =  (byte) ((value>>16) & 0xFF);
		des[1] =  (byte) ((value>>8) & 0xFF);
		des[0] =  (byte) (value & 0xFF);
		return 0;
	}

	public int bytesToint(byte[] src, int[] value)
	{
		value[0] = src[3] << 24 | src[2] << 16 | src[1] << 8 | src[0];
		return 0;
	}
*/
	@Override
	public void serviceDied(long l) {
		Log.d(TAG, "ETS Daemon binder Died");
		mRbsService = null;
	}
}






























