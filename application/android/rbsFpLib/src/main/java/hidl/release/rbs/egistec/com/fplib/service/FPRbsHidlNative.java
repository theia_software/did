package rbs.egistec.com.fplib.service;

import android.os.Handler;
import android.os.IHwBinder;
import android.os.RemoteException;
import android.util.Log;

import java.util.ArrayList;

import rbs.egistec.com.fplib.api.Common;
import rbs.egistec.com.fplib.api.FpResDef;
import vendor.egistec.hardware.fingerprint.V4_0.IBiometricsFingerprintRbs;
import vendor.egistec.hardware.fingerprint.V4_0.IBiometricsFingerprintRbsCallback;

public class FPRbsHidlNative implements IHwBinder.DeathRecipient{

	private static final int NO_SERVICE = -1;
	private static final int RUN_ERROR	 = -2;
	private static final String SERVICE_NAME = "egistec.rbs.service.daemon";
	private static final String TAG = "FPRbsNative";
	private static Handler mApHandler;
	private IBiometricsFingerprintRbs mRbsService;

	public FPRbsHidlNative(Handler handler){
		Log.d(TAG, "FPRbsNative release Constructor");
		mApHandler = handler;
		getService();
	}
	
	public boolean getService() {
		Log.d(TAG, "getService enter");
		if(mRbsService != null){
			return true;
		}

		try {
			mRbsService = IBiometricsFingerprintRbs.getService();
			mRbsService.set_on_callback_proc(mCallback);
		} catch (Exception e) {
			e.printStackTrace();
		}

		if(mRbsService == null){
			Log.e(TAG, "get service fail");
			return false;
		}
		
		Log.d(TAG, "get service success");
		mRbsService.asBinder().linkToDeath(this, 0);

		return true;
	}
	
	private IBiometricsFingerprintRbsCallback mCallback = new IBiometricsFingerprintRbsCallback.Stub() {

		@Override
		public void ipc_callback(int event_id, int value1, int value2, ArrayList<Byte> buffer, int buffer_size) throws RemoteException {
			Log.d(TAG, "ipc_callback() eventId=" + event_id + " value1 = " + value1 + " value2 = " + value2 + "buffer_size = " + buffer_size);
			int size = buffer.size();
			byte [] buffer2 = new byte[size];
			for(int i =0; i < size; i++)
			{
				buffer2[i] = buffer.get(i);
			}

			mApHandler.obtainMessage(event_id, value1, value2, buffer2).sendToTarget();
		}
	};

	public int extraApi(int pid, int cid, byte[] in_buffer, byte[] out_buffer, int[] out_buffer_size){
		int ret = 0;
		if(getService()) {
			try {
				int in_buffer_size = 0;
				if(in_buffer != null) {
					in_buffer_size = in_buffer.length;
				}
				byte[] in_msg = new byte[in_buffer_size + Integer.SIZE / 8];
				byte[] cid_buf = new byte[Integer.SIZE / 8];
				Common.intToBytes(cid, cid_buf);

				ArrayList vec_buf = new ArrayList<Byte>();

				for(int i = 0 ; i < cid_buf.length ; i++) {
					vec_buf.add(cid_buf[i]);
				}

				if(in_buffer != null) {
					for(int i = cid_buf.length ; i < (cid_buf.length + in_buffer.length); i++) {
						vec_buf.add(in_buffer[i - cid_buf.length]);
					}
				}

				ArrayList<Byte> vec_outBuf= mRbsService.extra_api(pid, vec_buf);
				/*
 				ret = vec_outBuf.get(0)<<24 + vec_outBuf.get(1)<<16 +vec_outBuf.get(2)<<8 + vec_outBuf.get(3);

				if ((out_buffer != null) && (vec_outBuf.size() > 4)){
					for(int i = 0 ; i < vec_outBuf.size() ; i++){
						Log.d("TAG", String.valueOf(vec_outBuf.get(i)));
						out_buffer[i] = vec_outBuf.get(i + 4);
					}
				}

				if (out_buffer_size != null)
					out_buffer_size[0] = vec_outBuf.size() - 4;
				*/
				int val1 = vec_outBuf.get(0)<<24;
				int val2 = vec_outBuf.get(1)<<16;
				int val3 = vec_outBuf.get(2)<<8;
				int val = vec_outBuf.get(3);
				ret = val1 + val2 + val3 + val;

				Log.d(TAG, "extra_api, ret: " + ret);
			} catch (RemoteException e) {
				ret = RUN_ERROR;
				e.printStackTrace();
			}
		}else{
			Log.d(TAG, "no service");
			ret = NO_SERVICE;
		}
		
		return ret;
	}
/*
	public int intToBytes( int value , byte[] des)
	{
		des[3] =  (byte) ((value>>24) & 0xFF);
		des[2] =  (byte) ((value>>16) & 0xFF);
		des[1] =  (byte) ((value>>8) & 0xFF);
		des[0] =  (byte) (value & 0xFF);
		return 0;
	}

	public int bytesToint(byte[] src, int[] value)
	{
		value[0] = src[3] << 24 | src[2] << 16 | src[1] << 8 | src[0];
		return 0;
	}
*/
	@Override
	public void serviceDied(long l) {
		Log.d(TAG, "ETS Daemon binder Died");
		mRbsService = null;
	}
}






























