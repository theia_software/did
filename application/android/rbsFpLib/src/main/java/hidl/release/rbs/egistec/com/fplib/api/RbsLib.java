package rbs.egistec.com.fplib.api;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import rbs.egistec.com.fplib.service.FPRbsHidlNative;

public class RbsLib{

	private static final String TAG = "RbsLib-HIDL-RELEASE";
	private static RbsLib mInstance;
	private FingerprintReceiver mFingerprintReceiver;
	private FPRbsHidlNative mFPDev;
	private static final String DATA_PATH = "/data/fpdata";

	public static RbsLib getInstance(Context context){
		Log.d(TAG,"+++ getInstance +++");
		if(mInstance == null) {
			Log.d(TAG,"get new RbsLibBinder");
			mInstance = new RbsLib();
		}
		return mInstance;
	}

	private final Handler mHandler = new Handler(){
		public void handleMessage(Message msg){
			if(mFingerprintReceiver == null){
				Log.e(TAG, "mFingerprintReceiver == null, handleMessage return");
				return;
			}

			mFingerprintReceiver.onFingerprintEvent(msg.what, msg.arg1, msg.arg2, msg.obj);
		};
	};

	public int sensorTest(int testId){
		byte[] test_result_buf = new byte[4];
		int[] test_result_lenth = new int[1];
		test_result_lenth[0] = test_result_buf.length;
		int[] test_result = new int[1];
		test_result[0] = -1;

		int retval =  mFPDev.extraApi(3, testId, null, test_result_buf, test_result_lenth);
		/*
		if(retval == 0) {
			Common.bytesToint(test_result_buf, test_result);
		}

		return test_result[0];
		*/
		return retval;
	}

	public int sensorProb(){
		Log.d(TAG,"+++ sensorProb +++");
		return 0;
	}

	private RbsLib(){
		Log.d(TAG, "RbsLibBinder Constructor");
		mFPDev = new FPRbsHidlNative(mHandler);
	}
	
	public void startListening(FingerprintReceiver receiver){
		Log.d(TAG,"+++ startListening +++");
		mFingerprintReceiver = receiver;
	}

	public int extraApi(int pid, int cid, byte[] in_buffer, byte[] out_buffer, int[] out_buffer_size){
		Log.d(TAG,"+++ extraApi +++");
		return mFPDev.extraApi(pid, cid, in_buffer, out_buffer, out_buffer_size);
	}
}
