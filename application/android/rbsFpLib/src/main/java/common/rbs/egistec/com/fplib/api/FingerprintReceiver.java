package rbs.egistec.com.fplib.api;

public interface FingerprintReceiver {
	void onFingerprintEvent(int eventId, int value1, int value2, Object eventObj);
}
