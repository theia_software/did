package rbs.egistec.com.fplib.api;

public class ExtraApiDef {
    public final static int PID_COMMAND = 0;
    public final static int PID_DEMOTOOL = 5;

    public final static int CMD_VERSION_ALGO = 1000;
    public final static int CMD_VERSION_IP = 1003;

    public final static int EXTRA_DT_SET_INI_CONFIG_PATH = 1010;

}
