package rbs.egistec.com.fplib.api;

import android.util.Log;

public class Common {
    public static int intToBytes(int value, byte[] des) {
        des[3] =  (byte) ((value>>24) & 0xFF);
        des[2] =  (byte) ((value>>16) & 0xFF);
        des[1] =  (byte) ((value>>8) & 0xFF);
        des[0] =  (byte) (value & 0xFF);
        return 0;
    }

    public static int bytesToint(byte[] src, int[] des) {
        des[0] = (src[3] & 0xFF) << 24 | (src[2] & 0xFF) << 16 | (src[1] & 0xFF) << 8 | src[0] & 0xFF;
        return 0;
    }

    public static int bytesToint(byte[] src) {
        return (src[3] & 0xFF) << 24 | (src[2] & 0xFF) << 16 | (src[1] & 0xFF) << 8 | src[0] & 0xFF;
    }
}
