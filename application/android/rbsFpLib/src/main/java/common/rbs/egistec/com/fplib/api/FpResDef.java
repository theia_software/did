package rbs.egistec.com.fplib.api;

public class FpResDef {
	
	public static final int EVENT_STATUS					= 20;
	public static final int EVENT_RETURN_16BIT_IMAGE        = 22;
	public static final int EVENT_ABORT						= 23;
	public static final int EVENT_RETURN_IMAGE_INFO 		= 24;
	public static final int EVENT_RETURN_IMAGE				= 25;
	public static final int EVENT_RETURN_IMAGE_COUNT        = 26;
	public static final int EVENT_RETURN_IMAGE_QTY          = 27;
    public static final int EVENT_RETURN_CALIBRATE_IMAGE    = 28;
	public static final int EVENT_RETURN_LIVE_IMAGE_OUT     = 29;
	public static final int EVENT_RETURN_IMAGEOBJ     		= 30;

	public static final int CAPTURE_WAIT_FINGER_ON 			= 2030;
	public static final int CAPTURE_FINGER_DETECTED 		= 2031;
	public static final int CAPTURE_FINEGR_REMOVED			= 2032;
	public static final int CAPTURE_FINISH					= 2033;
	public static final int CAPTURE_BAD_IMAGE				= 2034;
	public static final int CALIBRATION_RETRY				= 2035;
	public static final int ERROR_STATUS					= 2036;
	
	public static final int ABORT_OK						= 2020;
	public static final int ABORT_FAIL						= 2021;
	
	public static final int RESULT_OK						=  0;
	public static final int RESULT_FAILED					= -1;
	
	// enroll event
	public static final int EVENT_ENROLL_SUCCESS = 1000;
	public static final int EVENT_ENROLL_FAILED = 1001;
	public static final int EVENT_ENROLL_OK = 1002;
	public static final int EVENT_ENROLL_CANCELED = 1003;
	// EVENT_ENROLL_SUSPEND,
	public static final int EVENT_ENROLL_IMG_TOO_SMALL = 1004;
	public static final int EVENT_ENROLL_FEATURE_LOW = 1005;
	public static final int EVENT_ENROLL_HIGHLY_SIMILAR = 1006;
	public static final int EVENT_ENROLL_REDUNDANT = 1007;
	public static final int EVENT_ENROLL_DUPLICATE_FINGER = 1008;

	// verify event
	public static final int EVENT_VERIFY_FAILED = 1009;
	public static final int EVENT_VERIFY_MATCHED = 1010;
	public static final int EVENT_VERIFY_NOT_MATCHED = 1011;
	public static final int EVENT_VERIFY_CANCELED = 1012;

	public static final int EVENT_IDENTIFY_FINISH = 1100;
	// image quality
	public static final int EVENT_IMG_BAD_QLTY = 1013;
	public static final int EVENT_IMG_BLANK = 1014;
	public static final int EVENT_IMG_PARTIAL = 1015;
	public static final int EVENT_IMG_TOO_PARTIAL = 1016;
	public static final int EVENT_IMG_FAST = 1017;
	public static final int EVENT_IMG_COVER = 1018;
	public static final int EVENT_IMG_WATER = 1019;
	// finger state
	public static final int EVENT_FINGER_WAIT = 1020;
	public static final int EVENT_FINGER_TOUCH = 1021;
	public static final int EVENT_FINGER_READY = 1022;
	public static final int EVENT_FINGER_LEAVE = 1023;

	// error
	public static final int EVENT_ERR_GET_IMAGE = 1024;
	public static final int EVENT_ERR_ENROLL = 1025;
	public static final int EVENT_ERR_IDENTIFY = 1026;

	public static final int EVENT_NAVIGATION = 1027;
	//merge map
	public static final int EVENT_ENROLL_MAP = 1028;
	public static final int EVENT_ENROLL_TOO_FAST = 1031;
	public static final int EVENT_SWIPE_DX_DY = 1033;
	public static final int EVENT_MERGE_MAP_SIZE = 1035;
	public static final int EVENT_SENSROR_TEST_NVM_UID = 1036;

	public static final int EVENT_ENROLL_LQ = 1050;

	// image quality extension
	public static final int EVENT_IMG_FAKE_FINGER = 1060;

	//extra api command
	public static final int CMD_GET_CONFIG = 2001;
	public static final int CMD_UPDATE_CONFIG = 2002;
	public static final int CMD_UPDATE_DB_CONFIG = 2005;

	//enroll method
	public static final int ENROLL_METHOD_TOUCH = 0x01;
	public static final int ENROLL_METHOD_SWIPE = 0x02;
	public static final int ENROLL_METHOD_PAINT = 0x04;

	public static final int USB_PERMISSION_GRANTED = 2089;
	// OTG events
	public static final int EVENT_OTG_WRITE_REGISTER = 10000;
	public static final int EVENT_OTG_READ_REGISTER  = 10001;
	public static final int EVENT_OTG_GET_FRAME      = 10002;
	public static final int EVENT_OTG_STANDBY        = 10003;
	public static final int EVENT_OTG_WAKE_UP        = 10004;
	public static final int EVENT_OTG_SPI_WRITE_READ = 10007;
	public static final int EVENT_OTG_SET_SPI_CLK 	 = 10008;

	public static final int EVENT_OTG_GET_SENSOR_ID = 10900;
	public static final int EVENT_OTG_INIT_SENSOR = 10901;
	public static final int EVENT_OTG_SET_SENSOR_PARAM = 10902;
	public static final int EVENT_OTG_GET_IMAGE = 10903;
	public static final int EVENT_OTG_GET_SENSOR_PARAM = 10904;
	public static final int EVENT_OTG_ET760_READ_REGISTER = 10905;
	public static final int EVENT_OTG_ET760_WRITE_REGISTER = 10906;
	public static final int EVENT_OTG_ET760_RAW_SPI = 10907;


	public static final int EVENT_OTG_RESERVED_END   = 10999;

	// For Navigation
	public static final int NAVI_EVENT_UNKNOWN = 0;
	public static final int NAVI_EVENT_HOLD = 28;
	public static final int NAVI_EVENT_CLICK = 174;
	public static final int NAVI_EVENT_DCLICK = 111;
	public static final int NAVI_EVENT_UP = 103;
	public static final int NAVI_EVENT_DOWN = 108;
	public static final int NAVI_EVENT_LEFT = 105;
	public static final int NAVI_EVENT_RIGHT = 106;
	public static final int NAVI_EVENT_LOST = -1;

	//RGB Level
	public static final int LEVEL_INCREMENT = 0xF;
	public static final int LEVEL_0 = 0;
	public static final int LEVEL_1 = 1;
	public static final int LEVEL_2 = 2;
	public static final int LEVEL_3 = 3;
	public static final int LEVEL_4 = 4;
	public static final int LEVEL_5 = 5;
	public static final int LEVEL_6 = 6;
	public static final int LEVEL_7 = 7;
	public static final int LEVEL_8 = 8;
	public static final int LEVEL_9 = 9;
	public static final int LEVEL_10 = 10;
	public static final int LEVEL_11 = 11;
	public static final int LEVEL_12 = 12;

	public static final int LIVIMG_IMAGE_VERSION_V1 = 0xA1; //for G2, G3PLUS, and G5. algo output image bpp == 8
	public static final int LIVIMG_IMAGE_VERSION_V2 = 0xA2; //for G4. algo output image bpp >= 16
	public static final int OPERATION_TYPE = 0;

	public static final int INI_CONFING_FILE_MAX_SIZE = 10*1024;

	public static final String INI_SECTION_SENSOR = "sensor" ;

	public static final int PID_7XX_INLINETOOL = 10;

	public static final int FP_INLINE_7XX_CASE_INIT = 700;
	public static final int FP_INLINE_7XX_NORMALSCAN = 701;
	public static final int FP_INLINE_7XX_SNR_INIT = 702;
	public static final int FP_INLINE_7XX_SNR_WKBOX_ON = 703;
	public static final int FP_INLINE_7XX_SNR_BKBOX_ON = 704;
	public static final int FP_INLINE_7XX_SNR_CHART_ON = 705;
	public static final int FP_INLINE_7XX_SNR_CHART_ON_WITHOUT_SAVE_EFS_SCRIPT_ID = 706;
	public static final int FP_INLINE_7XX_FLASH_TEST = 717;
	public static final int FP_INLINE_7XX_SNR_GET_DATA = 709;

	public static final int FP_INLINE_7XX_CASE_UNINIT = 799;

	public static final int FACTORY_TEST_EVT_SNSR_TEST_SCRIPT_START = 0x11000003;
	public static final int FACTORY_TEST_EVT_SNSR_TEST_SCRIPT_END = 0x11000004;
	public static final int FACTORY_TEST_EVT_SNSR_TEST_PUT_WKBOX = 0x11000006;
	public static final int FACTORY_TEST_EVT_SNSR_TEST_PUT_BKBOX = 0x11000007;
	public static final int FACTORY_TEST_EVT_SNSR_TEST_PUT_CHART = 0x11000008;
	public static final int EVT_ERROR = 2020;

	public static final String KEY_SAVE_IMAGE		= 	"SAVE_IMAGE";
	public static final String KEY_BURST_IMAGE_COUNT = 	"BURST_IMAGE_COUNT";
	public static final String KEY_SENSING_MODE		= "SENSING_MODE";
	public static final String KEY_PGA_GAIN			= "PGA_GAIN";
	public static final String KEY_BOOST_GAIN		= 	"BOOST_GAIN";
	public static final String KEY_GAIN2			= 	"GAIN2";
	public static final String KEY_RECONSTRUCT		= 	"RECONSTRUCT";
	public static final String KEY_ADC_GAIN			= "ADC_GAIN";
	public static final String KEY_OSC40M			= 	"OSC40M";
	public static final String KEY_SPICLOCK 		= 	"SPICLOCK";
	public static final String KEY_CUT_IMAGE		= 	"CUT_IMAGE";
	public static final String KEY_ALGO_INIT_SENSOR_TYPE = "ALGO_INIT_SENSOR_TYPE";
	public static final String KEY_CUT_IMAGE_WIDTH	= 	"CUT_IMAGE_WIDTH";
	public static final String KEY_CUT_IMAGE_HEIGHT	= "CUT_IMAGE_HEIGHT";
	public static final String KEY_EXPOSURE_TIME	= "EXPOSURE_TIME";

	public static final String KEY_ET760_ALL_ADC_DAC	= "KEY_ET760_ALL_ADC_DAC";
	public static final String KEY_ET760_GAIN	= "KEY_ET760_GAIN";
	public static final String KEY_ET760_SAMPLE_MODE	= "KEY_ET760_SAMPLE_MODE";
	public static final String KEY_ET760_BINNING	= "KEY_ET760_BINNING";
	public static final String KEY_ET760_VGH_R	= "KEY_ET760_VGH_R";
	public static final String KEY_ET760_VGL_R	= "KEY_ET760_VGL_R";
	public static final String KEY_ET760_VGH_W	= "KEY_ET760_VGH_W";
	public static final String KEY_ET760_VGL_W	= "KEY_ET760_VGL_W";
	public static final String KEY_ET760_SVDD	= "KEY_ET760_SVDD" ;

	public static final String KEY_ET760_MATCHER_THRESHOLD_1	= "KEY_ET760_MATCHER_THRESHOLD_1";
	public static final String KEY_ET760_MATCHER_THRESHOLD_2	= "KEY_ET760_MATCHER_THRESHOLD_2";
	public static final String KEY_ET760_MATCHER_THRESHOLD_3	= "KEY_ET760_MATCHER_THRESHOLD_3";
	public static final String KEY_ET760_MATCHER_THRESHOLD_4	= "KEY_ET760_MATCHER_THRESHOLD_4";
	public static final String KEY_ET760_MATCHER_THRESHOLD_5	= "KEY_ET760_MATCHER_THRESHOLD_5";

	public static final String KEY_INTEGRATE_COUNT_HW = "INTEGRATE_COUNT_HW";
	public static final String KEY_INTEGRATE_COUNT_SW = "INTEGRATE_COUNT_SW";
	public static final String KEY_BKG_IMG_USE_OPTION = "BKG_IMG_USE_OPTION";
	public static final String KEY_BDS_LOW_TEMP_MIN_COUNT = "BDS_LOW_TEMP_MIN_COUNT";
	public static final String KEY_AVG_ZONE_STABLE_TRY = "AVG_ZONE_STABLE_TRY";
	public static final String KEY_BAC_THRESHOLD = "BAC_THRESHOLD";
	public static final String KEY_BAC_EXPOSURE_PERCENTAGE = "BAC_EXPOSURE_PERCENTAGE";
	public static final String KEY_TWO_TIMES_GAIN = "TWO_TIMES_GAIN";
	public static final String KEY_QUICK_CAPTURE_COUNT = "QUICK_CAPTURE_COUNT";
	public static final String KEY_QUICK_CAPTURE_HW_INT = "QUICK_CAPTURE_HW_INT";
	public static final String KEY_QUICK_CAPTURE_EXP_TIME = "QUICK_CAPTURE_EXP_TIME";
	public static final String KEY_CIRCLE_CENTER_X = "CIRCLE_CENTER_X";
	public static final String KEY_CIRCLE_CENTER_Y = "CIRCLE_CENTER_Y";
	public static final String KEY_BAC_BLACK_IMG_AVG_THRESHOLD = "BAC_BLACK_IMG_AVG_THRESHOLD";
	public static final String KEY_RAW_NORMALIZATION_ROI = "RAW_NORMALIZATION_ROI";
	public static final String KEY_IPP_FILTER_OVERFLOW_THRESHOLD_PERCENTAGE = "IPP_FILTER_OVERFLOW_THRESHOLD_PERCENTAGE";
	public static final String KEY_INPUT_SEQ_PROC_MODE = "INPUT_SEQ_PROC_MODE";
	public static final String KEY_SENSOR_ORIENTATION = "SENSOR_ORIENTATION";
	public static final String KEY_ENABLE_CAPTURE_ROI = "ENABLE_CAPTURE_ROI";
	public static final String KEY_CAPTURE_ROI_WIDTH = "CAPTURE_ROI_WIDTH";
	public static final String KEY_CAPTURE_ROI_HEIGHT = "CAPTURE_ROI_HEIGHT";
	public static final String KEY_CAPTURE_ROI_START_POINT = "CAPTURE_ROI_START_POINT";

	public static final String INI_SECTION_EGIS 	= "egis";

	public static final String KEY_INI_VERSION		= "INI_VERSION";
	public static final int INI_VERSION_FLAG		= 3;

	public static final String INI_SECTION_ENROLL	 =	"enroll";

	public static final String KEY_ENROLL_METHOD	 =	"ENROLL_METHOD";
	public static final String KEY_SWIPE_COUNT_Y	 =	"SWIPE_COUNT_Y";
	public static final String KEY_SWIPE_COUNT_X	 =	"SWIPE_COUNT_X";
	public static final String KEY_SWIPE_DIRECTION_MODE	= "SWIPE_DIRECTION_MODE";
	public static final String KEY_MAX_ENROLL_COUNT		= "MAX_ENROLL_COUNT";
	public static final String KEY_REDUNDANT_LEVEL = "REDUNDANT_LEVEL";
	public static final String KEY_CAPTURE_DELAY_ENABLE = "CAPTURE_DELAY_ENABLE";
	public static final String KEY_CAPTURE_DELAY_START_PROGRESS = "CAPTURE_DELAY_START_PROGRESS";
	public static final String KEY_CAPTURE_DELAY_WAITING_TIME = "CAPTURE_DELAY_WAITING_TIME";
	public static final String KEY_ENROLL_QUALITY_THRESHOLD = "ENROLL_QUALITY_THRESHOLD";
	public static final String KEY_ENROLL_QUALITY_THRESHOLD_DEC = "ENROLL_QUALITY_THRESHOLD_DEC";
	public static final String KEY_ENROLL_EXTRA_THE_FIRST_ENABLE = "ENROLL_EXTRA_THE_FIRST_ENABLE";
	public static final String KEY_ENROLL_ENABLE_UK = "ENROLL_ENABLE_UK";
	public static final String KEY_ENROLL_EXTRA_THE_FIRST_BEFORE_PROGRESS = "ENROLL_EXTRA_THE_FIRST_BEFORE_PROGRESS";

	public static final String KEY_SWITCH_VERTICAL		= "SWITCH_VERTICAL";
	public static final String KEY_SWITCH_HORIZONTAL	= "SWITCH_HORIZONTAL";
	public static final String KEY_SWITCH_XY			= "SWITCH_XY";
	public static final String KEY_ENROLLEX_SELECT_THRESHOLED	 = "ENROLLEX_SELECT_THRESHOLED";
	public static final String KEY_ENROLLEX_TOO_FAST_LEVEL =	"ENROLLEX_TOO_FAST_LEVEL";
	public static final String KEY_REDUNDANT_IMAGE_POLICY	 = "REDUNDANT_IMAGE_POLICY";
	public static final String KEY_CHECK_TOO_FAST		=	"TOO_FAST_CHECK";
	public static final String KEY_SWIPE_QUICK_TIMEOUT	=	"SWIPE_QUICK_TIMEOUT";
	public static final String KEY_ENROLLEX_TOO_FAST_ROLLBACK	=	"ENROLLEX_TOO_FAST_ROLLBACK";

	public static final String INI_SENSOR_TYPE	 =	"et0xx";
	public static final String KEY_DB_PATH_ET0XX	=	"PATH";
	public static final String KEY_DB_TYPE_ET0XX	 =	"FIMAGE_DB_TYPE";
	public static final String SET_DB_PATH_ET0XX	=	"/data/FP7";
	public static final String SET_DB_TYPE_ET0XX	 =	"DB_SIMEPLE";


	public static final String INI_SECTION_VERIFY		=	"verify";

	public static final String KEY_FLOW_TRY_MATCH	=	"FLOW_TRY_MATCH";
	public static final String KEY_VERIFY_FINGER_COUNT	=	"VERIFY_FINGER_COUNT";
	public static final String KEY_ENABLE_TRY_MATCH	=	"ENABLE_TRY_MATCH";
	public static final String KEY_APPEND_IMG_INTO_TEMPLATE = "APPEND_IMG_INTO_TEMPLATE";
	public static final String KEY_USER_NAME		=		"USER_NAME";
	public static final String KEY_FINGER_NAME		=		"FINGER_NAME";
	public static final String KEY_ANGLE			=		"ANGLE";
	public static final String KEY_VERIFY_COUNT		=	"VERIFY_COUNT";
	public static final String KEY_VERIFY_SUCCESS_COUNT	= "VERIFY_SUCCESS";
	public static final String KEY_VERIFY_FAIL_COUNT	= 	"VERIFY_FAIL";
	public static final String KEY_VERIFY_SET_COUNT		= "VERIFY_SET_COUNT";
	public static final String KEY_VERIFY_IMAGE_DPI		= "VERIFY_IMAGE_DPI";
	public static final String KEY_EXT_FEAT_QUALITY_TRYMATCH_THRESHOLD = "EXT_FEAT_QUALITY_TRYMATCH_THRESHOLD";
	public static final String KEY_EXT_FEAT_QUALITY_BADIMG_THRESHOLD = "EXT_FEAT_QUALITY_BADIMG_THRESHOLD";
	public static final String KEY_VERIFY_PARTIAL_THRESHOLD = "VERIFY_PARTIAL_THRESHOLD";
	public static final String KEY_ENROLL_PARTIAL_THRESHOLD = "ENROLL_PARTIAL_THRESHOLD";
	public static final String KEY_BAD_PARTIAL_THRESHOLD = "BAD_PARTIAL_THRESHOLD";
	public static final String KEY_IMAGE_QTY_THRESHOLD = "IMAGE_QTY_THRESHOLD";
	public static final String KEY_FAKE_FINGER_DETECTION = "FAKE_FINGER_DETECTION";
	public static final String KEY_DETECT_SCRATCH_MASK = "DETECT_SCRATCH_MASK";
	public static final String KEY_LIGHT_DETECTION = "LIGHT_DETECTION";
	public static final String KEY_CONFIG_MATCH_MODE2 = "CONFIG_MATCH_MODE2";
	public static final String KEY_PIPELINE_CAPTURE = "PIPELINE_CAPTURE";
	public static final String KEY_EXT_FEAT_QUALITY_LQMATCH_THRESHOLD = "EXT_FEAT_QUALITY_LQMATCH_THRESHOLD";
	public static final String KEY_TRY_MATCH_EVEN_TOO_PARTIAL = "TRY_MATCH_EVEN_TOO_PARTIAL";


	public static final String INI_SECTION_NAVI = "navigation";

	public static final String KEY_NAVI_MODE			=	"NAVI_MODE";
	public static final String KEY_NAVI_CHANGE_X_Y		=	"NAVI_CHANGE_X_Y";
	public static final String KEY_NAVI_CHANGE_UP_DOWN	=	"NAVI_CHANGE_UP_DOWN";
	public static final String KEY_NAVI_CHANGE_LEFT_RIGHT =	"NAVI_CHANGE_LEFT_RIGHT";

	public static final String INI_SECTION_READ_FINGER = "read_finger";
	public static final String KEY_FINGER_INDEX		= "FINGER_INDEX";

	public static final String INI_SECTION_CALIBRATE = "calibrate";

	public static final String KEY_CALIBRATE_625AVG_TARGET_PERCENTAGE = "CALIBRATE_625AVG_TARGET_PERCENTAGE";
	public static final String KEY_CALIBRATE_TARGET_CAPTURE_TIME = "CALIBRATE_TARGET_CAPTURE_TIME";
	public static final String KEY_CALIBRATE_EXPOSURE_START_WK = "CALIBRATE_EXPOSURE_START_WK";
	public static final String KEY_CALIBRATE_EXPOSURE_END_WK = "CALIBRATE_EXPOSURE_END_WK";

	public static final String  INI_SECTION_DB = "image_db";

	public static final String  KEY_IMAGE_DB_TYPE = "IMAGE_DB_TYP";
	public static final String  KEY_DB_SENSOR_SERIES = "DB_SENSOR_SERIES";
	public static final String  KEY_DB_SENSOR_TYPE = "DB_SENSOR_TYPE";
	public static final String  KEY_DB_IMAGE_WIDTH = "DB_IMAGE_WIDTH";
	public static final String  KEY_DB_IMAGE_HEIGHT = "DB_IMAGE_HEIGHT";
	public static final String  KEY_DB_IMAGE_CENTER_X = "DB_IMAGE_CENTER_X";
	public static final String  KEY_DB_IMAGE_CENTER_Y = "DB_IMAGE_CENTER_Y";
	public static final String  KEY_DB_IMAGE_BIN_FOLDER = "DB_IMAGE_BIN_FOLDER";
	public static final String  KEY_DB_IMAGE_TRY_FOLDER = "DB_IMAGE_TRY_FOLDER";
}
