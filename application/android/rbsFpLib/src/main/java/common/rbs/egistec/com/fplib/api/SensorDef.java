package rbs.egistec.com.fplib.api;

import android.util.Log;

import java.util.HashMap;

public class SensorDef {
    final static String TAG = "RBS-sensor";
    private static HashMap<Integer, Integer> sizeToSensorIdMap;

    public static int GetSensorIdByImageSize(int width, int height) {
        if (sizeToSensorIdMap == null) {
            sizeToSensorIdMap = new HashMap<>();
            sizeToSensorIdMap.put(Integer.valueOf(200 * 200), Integer.valueOf(713));
            sizeToSensorIdMap.put(Integer.valueOf(137 * 114), Integer.valueOf(726));
            sizeToSensorIdMap.put(Integer.valueOf(134 * 188), Integer.valueOf(702));
            sizeToSensorIdMap.put(Integer.valueOf(500 * 640), Integer.valueOf(901)); //TODO: ET901
        }
        int size = width * height;
        if (sizeToSensorIdMap.containsKey(Integer.valueOf(size))) {
            return sizeToSensorIdMap.get(Integer.valueOf(size));
        }
        Log.w(TAG, String.format("Unrecognized size %d:%d", width, height));
        return 0;
    }
}
