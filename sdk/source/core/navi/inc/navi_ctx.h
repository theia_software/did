#ifndef NAVI_CTX_H_
#define NAVI_CTX_H_
#include "type_definition.h"

#define FRAME_COUNTS 4
#define NUM_OF_CONT_CAPTURE 4

#define NAVI_SWIPE_X
//#define NAVI_SWIPE_Y
#define NAVI_X_H_THRESHOLD 7  // 3
#define NAVI_X_L_THRESHOLD 3  // 3
#define NAVI_Y_H_THRESHOLD 7
#define NAVI_Y_L_THRESHOLD 3
#define NAVI_Y_BREAK_THRESHOLD 8  // if (dy>NAVI_Y_BREAK_THRESHOLD), DO NOT send swipe event.
#define NAVI_XY_DIFF 2            // if (dx-dy < NAVI_XY_DIFF), DO NOT send swipe event.
#define LOOP_STEP 1

#define NAVI_DELAY_STEP 250

typedef enum {
    NAVI_OP_START = 0,
    NAVI_OP_GETPOS,
    NAVI_OP_GETTOUCH,
    NAVI_OP_DONE,
} navi_op_t;

typedef struct _navi_movement_t {
    int x;
    int y;
} navi_movement_t;

typedef struct _navi_para_t {
    navi_op_t op;
    navi_movement_t mov;
    BOOL touch;
    int mov_threshold_x;
    int mov_threshold_y;
    int time_out;
    BOOL finger_was_on;
    BOOL no_swipe;
} navi_ctx_t;

int navigateion_cal_pos(navi_ctx_t* ctx);
int navigation_pos(navi_ctx_t* ctx, unsigned long longClickDuration);
#endif
