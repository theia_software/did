#include <stdio.h>

#include "ImageProcessingLib.h"
#include "egis_fp_get_image.h"
#include "egis_log.h"
#include "image_analysis.h"
#include "isensor_api.h"
#include "isensor_definition.h"
#include "navi_ctx.h"
#include "navi_frame.h"
#include "plat_file.h"

#define LOG_TAG "RBS-NAVI"
extern int image_range(int data);
#define MAX_NAVI_SIZE 4096
static BYTE g_frame_data[MAX_NAVI_SIZE * FRAME_COUNTS * NUM_OF_CONT_CAPTURE];
static BYTE g_temp_frame[MAX_NAVI_SIZE];

#define IDX_INVALID -1
static int startIdx = IDX_INVALID;
static int endIdx = 0;
static BOOL hasFoundFinger = FALSE;
static int frameWidth = 0;
static int frameHeight = 0;
static int frameSize = 0;
static int frameCount = FRAME_COUNTS * NUM_OF_CONT_CAPTURE;
static naviFrameGet getFrameCallback = NULL;
static int debugLoadOriFrameIndex = 0;
static int debugLoadOriFrameSubIndex = 0;
static char debugLoadFolder[255] = {0};

#define INC_FRAME_IDX(idx, count) \
    idx += count;                 \
    idx = (idx % frameCount);
int naviFrameStart(int width, int height) {
    startIdx = IDX_INVALID;
    endIdx = 0;
    hasFoundFinger = FALSE;
    const int imageSize = width * height;
    if (imageSize > NAV_WINDOW_W * NAV_WINDOW_H) {
        egislog_e("wrong size or g_frame_data is not big enough");
        return -2;
    }
    frameWidth = width;
    frameHeight = height;
    frameSize = frameWidth * frameHeight;
    debugLoadOriFrameIndex++;
    debugLoadOriFrameSubIndex = 0;
    return 0;
}

void naviFrameStop() {
    startIdx = IDX_INVALID;
    endIdx = 0;
    hasFoundFinger = FALSE;
    frameWidth = 0;
    frameHeight = 0;
    frameSize = 0;
}

void naviFrameSetCallbackGetFrame(naviFrameGet pFunc) {
    getFrameCallback = pFunc;
}

static int g_finger_off_intensity = 0;
static int g_total_finger_off_intensity = 0;
static int g_finger_off_count = 0;
int analyzeNaviFrame(BYTE* frame_data, int* okToAdd, int* foundFinger) {
    int qty, corner_count = 0;
    int cover_count = 0;
    int img_intensity;

    BYTE* pTmp = frame_data;
    int i;
    for (i = 0; i < frameSize; i++) {
        *pTmp = 0xff - image_range(*pTmp);
        pTmp++;
    }
    IPimage_medium_filter(frame_data, frameWidth, frameHeight);

    qty = resample_IsFPImage_Lite(frame_data, frameWidth, frameHeight, &corner_count, &cover_count,
                                  FALSE, &img_intensity);

    egislog_v("qty=%d, corner=%d, cover=%d, intensity=%d (%d)", qty, corner_count, cover_count,
              img_intensity, g_finger_off_intensity);

    *okToAdd = 0;
    if (qty >= 70 || (qty >= 30 && corner_count >= 2 && cover_count >= 2)) {
        *okToAdd = 1;
    }

    *foundFinger = 0;
    if (*okToAdd) {
        if (qty >= 120 && corner_count >= 2) {
            if (g_finger_off_intensity == 0 || g_finger_off_intensity - img_intensity > 8)
                *foundFinger = 1;
            else if (g_finger_off_intensity - img_intensity < -4) {
                // g_finger_off_intensity could be wrong because
                // water was on the sensor
                // Reset it to re calibrate it
                g_finger_off_intensity = 0;
            }
        }
    }

    // Realtime calibrate g_finger_off_intensity
    BOOL resetTotalFingerOffCount = TRUE;
    if (qty == 0 && corner_count == 0 && cover_count == 0) {
        g_finger_off_count++;
        g_total_finger_off_intensity += img_intensity;
        egislog_v("(%d) g_total_finger_off_intensity=%d", g_finger_off_count,
                  g_total_finger_off_intensity);
        if (g_finger_off_count >= FRAME_COUNTS) {
            g_finger_off_intensity = g_total_finger_off_intensity / g_finger_off_count;
            resetTotalFingerOffCount = TRUE;
            egislog_v("g_finger_off_intensity=%d", g_finger_off_intensity);
        } else
            resetTotalFingerOffCount = FALSE;
    }
    if (resetTotalFingerOffCount) {
        g_finger_off_count = 0;
        g_total_finger_off_intensity = 0;
    }
    return 0;
}

int naviFrameAdd(int count) {
    if (getFrameCallback == NULL) {
        egislog_e("getFrameCallback is NULL");
        return -1;
    }
    int i;
    BYTE* pBuf = g_frame_data + endIdx * frameSize;
    int retSize = getFrameCallback(pBuf, frameWidth, frameHeight, count);

    int okToAdd = 0, foundFinger = 0;
    if (retSize >= 0) {
        for (i = 0; i < count; i++) {
            memcpy(g_temp_frame, pBuf, frameSize);
            analyzeNaviFrame(g_temp_frame, &okToAdd, &foundFinger);
            if (okToAdd || foundFinger) break;

            pBuf += frameWidth * frameHeight;
        }
        if (foundFinger) hasFoundFinger = TRUE;
    }
    if (okToAdd) {
        if (startIdx == IDX_INVALID) {
            startIdx = endIdx;
        }
        if (foundFinger ||
            (!hasFoundFinger && naviFrameGetTotalFrame() < frameCount - FRAME_COUNTS)) {
            INC_FRAME_IDX(endIdx, count)
            if (startIdx == endIdx) {
                INC_FRAME_IDX(startIdx, count)
            }
            egislog_v("naviFrameAdd [%d] [%d]", startIdx, endIdx);
            return EGIS_OK;
        }
    }
    egislog_v("EXIT(%d) naviFrameAdd(%d)(%d) [%d] [%d]", hasFoundFinger, okToAdd, foundFinger,
              startIdx, endIdx);
    return 1;
}

int naviFrameGetTotalFrame() {
    if (startIdx == IDX_INVALID) return 0;
    int total = (endIdx + frameCount - startIdx) % frameCount;
    egislog_v("naviFrameGetTotalFrame total=%d", total);
    return total;
}

BYTE* _naviFrameGetFrame(int idx) {
    int realIdx;
    if (startIdx == IDX_INVALID) {
        realIdx = idx % frameCount;
    } else
        realIdx = (startIdx + idx) % frameCount;

    return g_frame_data + realIdx * frameWidth * frameHeight;
}

BYTE* naviFrameGetFrame(int idx) {
    if (startIdx == IDX_INVALID) {
        egislog_e("naviFrameGetFrame has no available frame");
        return NULL;
    }
    int realIdx = (startIdx + idx) % frameCount;
    egislog_v("naviFrameGetFrame [%d]", realIdx);
    return g_frame_data + realIdx * frameWidth * frameHeight;
}

int naviFrameSave() {
    int retSize = -1;
#if !defined(__TRUSTONIC__) && !defined(QSEE)
    int i;
    char path[255];
    int total = naviFrameGetTotalFrame();
    static int saveIndex = 0;
    saveIndex++;
    for (i = 0; i < total + FRAME_COUNTS; i++) {
        sprintf(path, "/data/misc/debug/navi_image_ori_%03d_%d.bin", saveIndex, i);
        egislog_d("saving to %s", path);
        retSize = plat_save_raw_image(path, _naviFrameGetFrame(i), frameWidth, frameHeight);
    }
#endif
    return retSize;
}

int _naviFrameDebugLoad(BYTE* frame_data, int width, int height, int count) {
    int retSize = -1;
#if !defined(__TRUSTONIC__) && !defined(QSEE)
    int i;
    char path[255];
    const int navi_frame_size = width * height;
    BYTE* pFrame = frame_data;
    for (i = debugLoadOriFrameSubIndex; i < debugLoadOriFrameSubIndex + count; i++) {
        if (strlen(debugLoadFolder) > 0) {
            sprintf(path, "%s/navi_image_ori_%03d_%d.bin", debugLoadFolder, debugLoadOriFrameIndex,
                    i);
        } else {
            sprintf(path, "/data/misc/debug/navi_image_ori_%03d_%d.bin", debugLoadOriFrameIndex, i);
        }

        egislog_d("loading %s", path);
        printf("loading %s\n", path);
        retSize = plat_load_raw_image(path, pFrame, width, height);
        if (retSize <= 0) {
            egislog_e("Failed to load %s", path);
            break;
        }
        pFrame += navi_frame_size;
    }
    debugLoadOriFrameSubIndex += count;
#endif
    return retSize;
}

void naviFrameEnableDebugLoad(int loadIndex, char* folder) {
    if (debugLoadOriFrameIndex <= 0) {
        debugLoadOriFrameIndex = loadIndex;
    }
#if !defined(QSEE)
    strcpy(debugLoadFolder, folder);
#endif
    naviFrameSetCallbackGetFrame(_naviFrameDebugLoad);
}
