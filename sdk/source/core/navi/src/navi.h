#ifndef NAVIGATION_H_
#define NAVIGATION_H_
#include "egis_definition.h"
#include "extra_control_handler.h"

void navigation_operation(mainDoExtraFunc_t pf);
int start_navigation(mainDoExtraFunc_t pf);
int end_navigation(mainDoExtraFunc_t pf);
int set_navi_node_status(BOOL enable);
int get_navi_node_status(BOOL* enable);
#endif
