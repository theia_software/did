#ifndef __NAVI_FRAME_H_
#define __NAVI_FRAME_H_

#include "type_definition.h"

typedef int (*naviFrameGet)(unsigned char* frame, int width, int height, int count);

int naviFrameStart(int width, int height);
void naviFrameStop();
void naviFrameSetCallbackGetFrame(naviFrameGet pFunc);
int naviFrameAdd(int count);

int naviFrameGetTotalFrame();
BYTE* naviFrameGetFrame(int idx);

int naviFrameSave();
void naviFrameEnableDebugLoad(int loadIndex, char* folder);

int analyzeNaviFrame(BYTE* frame_data, int* okToAdd, int* foundFinger);

#endif
