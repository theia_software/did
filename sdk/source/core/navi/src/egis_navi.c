#include "egis_navi.h"
#include "egis_definition.h"
#include "egis_log.h"
#include "fp_sensormodule.h"
#include "isensor_api.h"
#include "navi_ctx.h"
#include "plat_time.h"

#define LOG_TAG "RBS-Navi"

#define abs(x) ((x) >= 0 ? (x) : -(x))

static NaviConfig g_navi_config;

#define INTERVAL_FALSE_TRIGGER 500
#define THRESHOLD_SEQ_LOST_EVENT 4
static uint16_t gCountLostEvent = 0;
int AddCountSequentialLostEvent(uint64_t timeCurrent) {
    static uint64_t timeLastLostEvent = 0;
    if (timeLastLostEvent == 0 || (timeCurrent - timeLastLostEvent) > INTERVAL_FALSE_TRIGGER) {
        gCountLostEvent = 1;
    } else {
        gCountLostEvent++;
    }
    egislog_d("%s return %d (%d)", __func__, gCountLostEvent,
              (int)(timeCurrent - timeLastLostEvent));
    timeLastLostEvent = timeCurrent;
    return gCountLostEvent;
}
void ResetCountSequentialLostEvent() {
    egislog_d("%s ", __func__);
    gCountLostEvent = 0;
}
int GetCountSequentialLostEvent() {
    return gCountLostEvent;
}
NaviEventCode analyze_dx_dy_send_swipe_event(int dx, int dy, int loop_count, int x_threshold,
                                             int y_threshold) {
    NaviEventCode sendevent = EVENT_UNKNOWN;
    if (abs(dx) >= x_threshold && abs(dy) >= y_threshold) {
        if (abs(dx) >= abs(dy)) {
            if (dx < 0)
                sendevent = EVENT_RIGHT;
            else
                sendevent = EVENT_LEFT;
        } else  // y
        {
            if (dy < 0)
                sendevent = EVENT_DOWN;
            else
                sendevent = EVENT_UP;
        }
    } else if (abs(dx) >= x_threshold) {
        if (dx < 0)
            sendevent = EVENT_RIGHT;
        else
            sendevent = EVENT_LEFT;
    } else if (abs(dy) >= y_threshold) {
        if (dy < 0)
            sendevent = EVENT_DOWN;
        else
            sendevent = EVENT_UP;
    }

    // if (sendevent != EVENT_UNKNOWN && loop_count > 8)
    return sendevent;
}

#define NAVI_TZ_TIMEOUT 4  // TODO: (FRAME_COUNTS-1)
NaviEventCode process_nav_window_detect_swipe(unsigned long long begin_touch_time, BOOL noSwipe) {
    unsigned long long total_touch_time;
    unsigned int accum_dx, accum_dy;
    int loop_count = 0;
    navi_ctx_t navi_ctx;
    BOOL has_error = FALSE;
    NaviEventCode navi_event_code = EVENT_UNKNOWN;
    accum_dx = 0;
    accum_dy = 0;

    navi_ctx.op = NAVI_OP_GETPOS;
    navi_ctx.touch = FALSE;
    navi_ctx.time_out = NAVI_TZ_TIMEOUT;
    navi_ctx.finger_was_on = FALSE;
    navi_ctx.mov_threshold_x = g_navi_config.xThreshold;
    navi_ctx.mov_threshold_y = g_navi_config.yThreshold;
    navi_ctx.no_swipe = noSwipe;
    navi_ctx.mov.x = 0;
    navi_ctx.mov.y = 0;
    if ((navigation_pos(&navi_ctx, g_navi_config.longTouchDuration))) {
        egislog_e("navigation_pos fail;");
        has_error = TRUE;
        navi_event_code = EVENT_LOST;
    } else {
        accum_dx += navi_ctx.mov.x;
        accum_dy += navi_ctx.mov.y;

        total_touch_time = plat_get_diff_time(begin_touch_time);

        egislog_d("navi[%d] (%d, %d) (accum_dx,dy)=(%d,%d)", loop_count, navi_ctx.mov.x,
                  navi_ctx.mov.y, accum_dx, accum_dy);
        // egislog_d("navi[%d]  touch=%d, duration=%d", loop_count,
        // navi_ctx.touch, total_touch_time);

        if (total_touch_time > g_navi_config.longTouchDuration &&
            accum_dx < g_navi_config.xThreshold && accum_dy < g_navi_config.yThreshold) {
            navi_event_code = EVENT_HOLD;
        } else {
            navi_event_code = analyze_dx_dy_send_swipe_event(
                accum_dx, accum_dy, loop_count, g_navi_config.xThreshold, g_navi_config.yThreshold);
        }
    }

    if (navi_event_code == EVENT_UNKNOWN) {
        navi_event_code = (!has_error && navi_ctx.finger_was_on) ? EVENT_CLICK : EVENT_LOST;
    }
    return navi_event_code;
}

int egis_navGetEvent(NaviConfig* navi_config, navigation_info_common* pNaviInfo) {
    BYTE IntStatus = 0;

    if (navi_config == NULL) {
        egislog_e("egis_navGetEvent wrong input");
        return EGIS_INCORRECT_PARAMETER;
    }
    memcpy(&g_navi_config, navi_config, sizeof(NaviConfig));
    egislog_i("egis_navGetEvent config longTouch=%d, threshold %d, %d, dclick=%d",
              g_navi_config.longTouchDuration, g_navi_config.xThreshold, g_navi_config.yThreshold,
              g_navi_config.doubleClickDuration);
    int ret = 0;

    isensor_read_int_status(&IntStatus);
    if (INT_STATUS_THL(IntStatus)) isensor_calibrate(FPS_CALI_SDK_THL);

    unsigned long long begin_touch_time = plat_get_time();
    NaviEventCode eventCode = process_nav_window_detect_swipe(begin_touch_time, FALSE);
    unsigned long duration = plat_get_diff_time(begin_touch_time);

    if (eventCode == EVENT_LOST) {
        AddCountSequentialLostEvent(begin_touch_time);
    }

    if (INT_STATUS_THH(IntStatus)) {
        int seqLostEventCount = GetCountSequentialLostEvent();
        if (seqLostEventCount > THRESHOLD_SEQ_LOST_EVENT) {
            isensor_calibrate(FPS_CALI_SDK_THH_FALSE_TRIGGER);
            ResetCountSequentialLostEvent();
        }
    }

    if (g_navi_config.doubleClickDuration > 0 && eventCode == EVENT_CLICK) {
        NaviEventCode secondEventCode = EVENT_UNKNOWN;
        egislog_d("Detecting double click");

        while (duration < g_navi_config.doubleClickDuration) {
            plat_wait_time(10);
            BOOL noSwipe = TRUE;
            secondEventCode = process_nav_window_detect_swipe(plat_get_time(), noSwipe);
            duration = plat_get_diff_time(begin_touch_time);

            if (secondEventCode == EVENT_LOST)
                continue;
            else if (secondEventCode == EVENT_CLICK) {
                eventCode = EVENT_DCLICK;
                break;
            } else
                break;
        }
    }
    pNaviInfo->detail.time = (int)duration;
    pNaviInfo->eventValue = eventCode;

    egislog_i("egis_navGetEvent %d. duration=%d.", pNaviInfo->eventValue, pNaviInfo->detail.time);
    return EGIS_OK;
}
