#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#include "isensor_api.h"
#include "isensor_definition.h"
#include "navi_ctx.h"
#include "navi_frame.h"
#include "plat_log.h"
#include "plat_mem.h"
#include "plat_spi.h"
#include "plat_time.h"
#include "qm_lib.h"

#define LOG_TAG "RBS-NAVI"

typedef enum { NAVI_STATUS_DETECT_MODE, NAVI_STATUS_SENSOR_MODE, NAVI_STATUS_INIT } navi_mode_t;

#define RESAMPLE_DPI(x) \
    ((((x * 1434 / 1024) + 15) / 16) * 16)  // dpi resample ratio, 508/363 ~ 1.4 ~ 1434/1024

#ifdef CROP_IMAGE_NAVI
#define get_navi_frames(f, w, h) fp_check_get_partial_frame(nav_image, NAV_WINDOW_H, NAV_WINDOW_W);
#else
#define get_navi_frames(f) isensor_get_frame(f, NAV_WINDOW_H, NAV_WINDOW_W, FRAME_COUNTS);
#endif
#define MAX_NAVI_SIZE 4096
static int StartNavigation(void);
static BYTE g_nav_bkg[MAX_NAVI_SIZE];
static BYTE g_nav_ref[MAX_NAVI_SIZE];
static BOOL g_navi_first_touch = FALSE;
static navi_mode_t g_Navi_Mode_Status = NAVI_STATUS_INIT;
#ifdef TEEIGP
#define abs(x) ((x) >= 0 ? (x) : -(x))
#endif

static QMFeatures last_feat;

static void CopyNavRef(BYTE* ref, BYTE* img, int offsetX, int offsetY) {
    int i;

    img += offsetY * NAV_WINDOW_W + offsetX;
    for (i = 0; i < NAV_REF_H; i++, img += NAV_WINDOW_W, ref += NAV_REF_W)
        memcpy(ref, img, NAV_REF_W);
}
static int SetNavigationMode(navi_mode_t mode) {
    int ret = 0;
    if (g_Navi_Mode_Status != mode) {
        g_Navi_Mode_Status = mode;
        egislog_d("SetNavigationMode %s", mode ? ("Sensor_mode") : ("Detect_mode"));
        ret = (mode == NAVI_STATUS_SENSOR_MODE) ? (isensor_set_navigation_sensor_mode() == EGIS_OK)
                                                : (isensor_set_navigation_detect_mode() == EGIS_OK);
    }
    return ret;
}

static int EndNavigation(void) {
    io_dispatch_connect(NULL);

    int ret = isensor_set_power_off();
    io_dispatch_disconnect();
    return ret;
}

static int StartNavigation(void) {
    io_dispatch_connect(NULL);  //

    egislog_d("StartNavigation() enter");
    g_Navi_Mode_Status = NAVI_STATUS_DETECT_MODE;
    isensor_set_navigation_detect_mode();

    memset(g_nav_bkg, 0, NAV_WINDOW_W * NAV_WINDOW_H);

    io_dispatch_disconnect();
    return EGIS_OK;
}

#ifdef CROP_IMAGE_NAVI
static void CropImage(BYTE* src, int src_w, int src_h, BYTE* tar, int tar_w, int tar_h) {
    int c;
    int offset_x = (src_w - 1) / 2 - tar_w / 2;
    int offset_y = (src_h - 1) / 2 - tar_h / 2;
    BYTE *p_tar = tar, *p_src = src + offset_x + offset_y * src_w;
    if (tar_w > src_w || tar_h > src_h) return;
    for (c = 0; c < tar_h; c++, p_tar += tar_w, p_src += src_w) memcpy(p_tar, p_src, tar_w);
}
static int fp_check_get_partial_frame(BYTE* partial_frame, UINT height, UINT width) {
    int sensor_width, sensor_height;
    BYTE frame[MAX_IMG_SIZE];

    if (isensor_get_sensor_size(&sensor_width, &sensor_height) != EGIS_OK) {
        return EGIS_COMMAND_FAIL;
    }

    isensor_get_frame(frame, sensor_height, sensor_width);
    CropImage(frame, sensor_width, sensor_height, partial_frame, width, height);
    return 0;
}
#endif

#define Sleep(ms) usleep(1000 * ms)
int navigateion_cal_pos(navi_ctx_t* ctx) {
    if (ctx == NULL) return EGIS_INCORRECT_PARAMETER;

    int dx = 0, dy = 0;
    int frameIndex;
    BYTE* nav_image;
    UINT navi_off_count;
    const int navi_frame_size = NAV_WINDOW_W * NAV_WINDOW_H;
    g_navi_first_touch = FALSE;
    navi_off_count = 0;
    QMFeatures feat;
    QMMatchDetail md;
    int e_count = 0;
    BYTE* image = plat_alloc(NAV_WINDOW_W * NAV_WINDOW_H);
    if (image == NULL) return EGIS_COMMAND_FAIL;

    int total_frame_counts = naviFrameGetTotalFrame();
    egislog_d("navigateion_cal_pos total=%d", total_frame_counts);
    for (frameIndex = 0; frameIndex < total_frame_counts; frameIndex++) {
        TIME_MEASURE_START(a)

        nav_image = naviFrameGetFrame(frameIndex);
        int okToAdd, foundFinger;
        analyzeNaviFrame(nav_image, &okToAdd, &foundFinger);

        if (!okToAdd) {
            navi_off_count++;
            if (navi_off_count >= 3)  // check if over 3 bad qty frames, break.
            {
                if (!g_navi_first_touch) {
                    ctx->touch = FALSE;
                }
                navi_off_count = 0;
                goto end;
            }
        }
        if (okToAdd) {
            // Normalize nav_image for SWIPE
            // Navi_NormalizeImage(nav_image, NAV_WINDOW_W,
            // NAV_WINDOW_H);
            // IPotsu(nav_image, NAV_WINDOW_W, NAV_WINDOW_H, 1);

            if (ctx->touch == TRUE && ctx->no_swipe == FALSE)  // continue touch, calculate dx/dy
            {
                memcpy(image, nav_image, NAV_WINDOW_W * NAV_WINDOW_H);
                qm_extract(image, NAV_WINDOW_W, NAV_WINDOW_H, &feat, NULL);
                if (e_count == 0) goto next_frame;

                qm_match(&last_feat, &feat, &md, NULL);

                dx += md.dx;
                dy += md.dy;
            next_frame:
                egislog_d("dx = %d, dy = %d", dx, dy);
                qm_copy_features(&last_feat, &feat);
                e_count++;
            }

            if (foundFinger) {
                ctx->finger_was_on = TRUE;
                ctx->touch = TRUE;
                g_navi_first_touch = TRUE;
            }
        }
        TIME_MEASURE_STOP(a, "---- loop:")
    }

end:
    PLAT_FREE(image);

    ctx->mov.x += dx;
    ctx->mov.y += dy;
    return EGIS_OK;
}
#ifdef OLD_DRIVER_SUPPORT_TWO_FRAME
// Workaround for some old driver which only allow to read at most two frames
// once
int getFrameFromSensor(BYTE* frame, int width, int height, int count) {
    int i, ret = EGIS_OK;
    int to_get, remaining = count;
    int size = height * width;
    for (i = 0; i < count;) {
        to_get = (remaining >= 2) ? 2 : remaining;
        egislog_d("%s, to_get=%d, remaining=%d", __func__, to_get, remaining);

        ret = isensor_get_frame(frame, height, width, to_get);
        if (ret != EGIS_OK) {
            return ret;
        }
        frame += (to_get * size);
        i += to_get;
        remaining -= to_get;
    }
    return ret;
}
#else
int getFrameFromSensor(BYTE* frame, int width, int height, int count) {
    return isensor_get_frame(frame, height, width, count);
}
#endif

#define ESTIMATE_TIME_TO_CALC_DXDY 30
int navigation_pos(navi_ctx_t* ctx, unsigned long longClickDuration) {
    unsigned long long begin_touch_time = plat_get_time();
    const int navi_frame_size = NAV_WINDOW_W * NAV_WINDOW_H;
    unsigned long total_touch_time;
    int ret = EGIS_OK;
    BYTE IntStatus = 0;
    isensor_read_int_status(&IntStatus);
    if (INT_STATUS_THL(IntStatus)) isensor_calibrate(FPS_CALI_SDK_THL);

    SetNavigationMode(NAVI_STATUS_SENSOR_MODE);
    naviFrameStart(NAV_WINDOW_W, NAV_WINDOW_H);

    // naviFrameEnableDebugLoad(1);
    naviFrameSetCallbackGetFrame(getFrameFromSensor);
    // naviFrameEnableDebugLoad(1, "/data/misc/navi_66x54_106");

    do {
        ret = naviFrameAdd(FRAME_COUNTS);
        total_touch_time = plat_get_diff_time(begin_touch_time);
    } while (ret == EGIS_OK && total_touch_time < longClickDuration - ESTIMATE_TIME_TO_CALC_DXDY);

    // For Debug.
    if (naviFrameGetTotalFrame() > 0) {
        // naviFrameSave();
    }

    navigateion_cal_pos(ctx);

    SetNavigationMode(NAVI_STATUS_DETECT_MODE);

    naviFrameStop();
    egislog_d("%d, %d, %d", ctx->mov.x, ctx->mov.y, ctx->touch);
    return EGIS_OK;
}
