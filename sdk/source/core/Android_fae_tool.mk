LOCAL_PATH := $(call my-dir)
$(info LOCAL_PATH=$(LOCAL_PATH))
GIT_PROJECT_RELATIVE := ../..

COMMON_PLATFORM=linux

#---------------------------------------------
#
#	Build egis core.
#
include $(CLEAR_VARS)


LOCAL_MODULE := egis_fae_core
LOCAL_LDLIBS := -L$(SYSROOT)/usr/lib -llog
LOCAL_EXPORT_LDLIBS := $(LOCAL_LDLIBS)
LOCAL_CFLAGS := -D__LINUX__ -D__ET_5XX_SERIES__ -DDEVICE_DRIVER_HAS_SPI -DETS -DEGIS_DBG
LOCAL_CFLAGS += -DMTK_EVB
LOCAL_CFLAGS += -DxHEAP_LOG -DSIMULATE_LOAD_IMAGE
LOCAL_CFLAGS += -DENROLL_POLICY_REDUNDANT_ACCPET
LOCAL_CFLAGS += -DxFOD_CALIBRATION_DCFG
LOCAL_EXPORT_CFLAGS := $(LOCAL_CFLAGS)

ifeq ($(ALGORITHM_VERSION),3)
LOCAL_CFLAGS += -DG3_MATCHER
endif

LOCAL_C_INCLUDES := \
	$(LOCAL_PATH)/../common/platform/inc \
	$(LOCAL_PATH)/fp_api/include \
	$(LOCAL_PATH)/fp_api/inc \
	$(LOCAL_PATH)/enroll_verify/inc \
	$(LOCAL_PATH)/sensor_control/inc \
	$(LOCAL_PATH)/finger_detect/inc \
	$(LOCAL_PATH)/sensor_test/inc

LOCAL_EXPORT_C_INCLUDES += $(LOCAL_C_INCLUDES)

LOCAL_SRC_FILES := \
	../common/platform/src/$(COMMON_PLATFORM)/plat_log_$(COMMON_PLATFORM).c \
	../common/platform/src/$(COMMON_PLATFORM)/plat_mem_$(COMMON_PLATFORM).c \
	../common/platform/src/$(COMMON_PLATFORM)/plat_spi_$(COMMON_PLATFORM).c \
	../common/platform/src/$(COMMON_PLATFORM)/plat_file_$(COMMON_PLATFORM).c \
	../common/platform/src/$(COMMON_PLATFORM)/plat_time_$(COMMON_PLATFORM).c \
	../common/platform/src/egis_sprintf.c \
	inline_handler/inline_handler.c

LOCAL_SRC_FILES += \
	sensor_test/src/fae_sensor_test.c

include $(BUILD_SHARED_LIBRARY)
