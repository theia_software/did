#ifndef __COMMAND_HANDLER_H__
#define __COMMAND_HANDLER_H__

#define SIZEOF_INT sizeof(int)
#define BASE_TRANSFER_SIZE 256

int command_handler(unsigned char* msg_data, int msg_size, unsigned char* outdata,
                    int* outdata_size);

#endif