#include "algomodule.h"
#include "common_definition.h"
#include "egis_algomodule.h"
#include "egis_definition.h"
#include "egis_fp_get_image.h"
#include "egis_navi.h"
#include "egis_sensormodule.h"
#include "egis_sprintf.h"
#include "extra_operations.h"
#include "fp_algomodule.h"
#include "fp_custom.h"
#include "fp_sensormodule.h"
#include "plat_file.h"
#include "plat_log.h"
#include "plat_mem.h"
#include "puzzle_image.h"
#include "template_manager.h"
#include "uk_manager.h"
#define LOG_TAG "RBS"
static BOOL g_is_verify_mode = FALSE;
#ifdef AUTHTOKEN_HMAC
#include "group_manager.h"
#include "permission_ops.h"
#include "plat_hmac.h"
#endif

#ifdef EGIS_DEBUG_MEMORY
#include "egis_mem_debug.h"
#define MEMORY_NOT_FORCED_FREE 0
#define MEMORY_FORCED_FREE 1
#endif

#if defined(AUTHTOKEN_HMAC) && !defined(EGIS_DBG)
#include "plat_time.h"

#define AUTHTOKEN_PASS_INTERVAL 500  // ms

static BOOL g_authtoken_is_pass = FALSE;
static unsigned long long g_authtoken_pass_time = 0;
#endif

#ifdef __ET7XX__
#include "bkg_manager.h"
#endif

#ifdef SUPPORT_PAYMENT
#include "payment.h"
static uint8_t g_ifaa_fplist_path[PATH_MAX] = "";
static uint8_t g_ifaa_version_path[PATH_MAX] = "";
#endif

//#define SUPPORT_MAX_ENROLL_COUNT 5
//#define SUPPORT_MAX_ENROLL_COUNT 6

#define POWER_ON 1
#define POWER_OFF 0

struct TransferPackage {
    uint32_t process;
    uint32_t command;
    uint32_t args1;
    uint32_t args2;
    uint32_t indata_len;
    uint8_t indata[0];
};

// Common
#define INTIALIZATION_NONE 0x00000000
#define INTIALIZATION_ENV 0x00000001
#define INTIALIZATION_HW 0x00000002
#define INTIALIZATION_ALGO 0x00000004
#define INTIALIZATION_READY (INTIALIZATION_ENV | INTIALIZATION_HW | INTIALIZATION_ALGO)

enum CAPTURE_IMAGE_TYPE {
    TYPE_ENROLLING = 0,
    TYPE_OTHER,
};

uint32_t g_initial_state = INTIALIZATION_NONE;

struct TemplateHolder {
    fp_lib_template_t tmpl;
    uint32_t is_tmpl_valid;
    fp_lib_template_t* updated_tmpl;
//#if defined(ALGO_GEN_7)
//    fp_lib_template_t* updated_not_match_tmpl[SUPPORT_MAX_ENROLL_COUNT];
//#endif
    int updated_pos;
};

struct ImageHolder g_img_holder;
struct TemplateHolder g_tmpl_holder;
#ifdef G3PLUS_MATCHER
// #define FREE_TEMPLATE_AFTER_PASS_TO_ALGO
#endif

fp_lib_identify_data_t g_identify_data;

static BOOL g_auto_spi_control = TRUE;
#ifdef __TRUSTONIC__
#include "template_file.h"

template_file_data g_templ_file[SUPPORT_MAX_ENROLL_COUNT] = {0};
int _trustlet_custom_save_file(int file_index, unsigned char* buf, unsigned int len) {
    int size_file_data = sizeof(template_file_data);
    ex_log(LOG_DEBUG, "%s [%d] len %d, %d.", __func__, file_index, len, size_file_data);
    if (size_file_data == len) {
        memcpy(&g_templ_file[file_index], buf, len);
        return len;
    }
    ex_log(LOG_ERROR, "wrong size !!!");
    return -1;
}

int _trustlet_custom_load_file(int file_index, unsigned char* buf, unsigned int len,
                               unsigned int* real_size) {
    int size_file_data = sizeof(template_file_data);
    ex_log(LOG_DEBUG, "%s [%d] len %d, %d.", __func__, file_index, len, size_file_data);
    if (real_size) *real_size = 0;

    if (size_file_data == len) {
        memcpy(buf, &g_templ_file[file_index], len);
        if (real_size) *real_size = len;
        return len;
    }
    ex_log(LOG_ERROR, "wrong size !!!");
    return -1;
}

int _trustlet_custom_remove_file(int file_index) {
    ex_log(LOG_DEBUG, "%s [%d]", __func__, file_index);
    int size_file_data = sizeof(template_file_data);
    memset((void*)&g_templ_file[file_index], 0, size_file_data);
    return 0;
}
#endif

BOOL is_sensor_command(unsigned int command);
int update_authenticator_id();
#ifdef FEATURE_NAVIGATION
int navi_control_api(int extra_id, int img_width, int img_height);
#endif
int update_ifaa_fpid_list(uint32_t* fingerprint_ids, int fingerprint_count, const char* path);

static int __uninit_sensor(void) {
    /* close fp sensor */
    int retval = 0;
    if (g_initial_state & INTIALIZATION_HW) {
        g_initial_state &= ~INTIALIZATION_HW;
        EGIS_IMAGE_FREE(&g_img_holder.fingerprint_image);
        retval = fp_deInitSensor();
    }
    return retval;
}

BOOL g_stop_DQE = FALSE;
int command_handler(unsigned char* msg_data, int msg_size, unsigned char* out_data,
                    int* out_data_size) {
    int retval = 0, ret = 0;
    uint32_t FORCED_DETECT_MODE = 0;

    struct TransferPackage* package;
    int img_width, img_height, img_dpi;

    fp_lib_enroll_data_t enroll_data;  // should be maintain?

    int fid_count = 0;
    uint32_t enrolled_count, enroll_option;
    uint8_t is_identified_update;

    // set work mode
    uint32_t mode;
    uint32_t wait_time;
    // set active group
    uint32_t group_id;
    uint32_t fingerprint_id;
    // identify start
    uint32_t need_liveness_auth;
    unsigned long long challenge, secure_group_id = 0;
    int finger_status;
    cmd_fingerprint_ids_t* fingerprint_ids;
    cmd_identifyresult_t* identify_result;
    cmd_enrollresult_t* enroll_result;
    cmd_imagestatus_t* img_info;
    unsigned long pass_diff_time = 0;

    if (msg_data == NULL || msg_size < (int)sizeof(uint32_t) * 5) {
        ex_log(LOG_ERROR, "invalid parameter! \n");
        return -6;
    }

    package = (struct TransferPackage*)msg_data;

#ifdef EGIS_DBG
    ex_log(LOG_INFO, "EGIS_DBG commander_handler enter! package->command:%d", package->command);
#else
    ex_log(LOG_INFO, "EGIS_RELEASE commander_handler enter!");
    ex_log(LOG_DEBUG, "package->command:%d", package->command);
#endif

    if (is_sensor_command(package->command) && g_auto_spi_control) {
        ret = fp_setPower(SPI_POWER_ON);
        if (ret != FP_LIB_OK) ex_log(LOG_ERROR, "fp_setPower:open fail");
        ex_log(LOG_DEBUG, "open SPI power ret = %d\n", ret);
    }

    switch (package->command) {
        case EX_CMD_INIT_SDK:
#ifdef __ET7XX__
            bkm_init();
            bkm_set_folder_path(FILE_DATA_BASE);
#endif
            template_set_max_count(SUPPORT_MAX_ENROLL_COUNT);
#ifdef __TRUSTONIC__
            template_set_custom_file_routin(_trustlet_custom_save_file, _trustlet_custom_load_file,
                                            _trustlet_custom_remove_file);
#ifdef TRUSTONIC_AES_KEY
            retval = plat_get_key();
            if (retval != EGIS_OK) {
                ex_log(LOG_ERROR, "plat_get_key result = %d\n", retval);
                break;
            }
#endif

#endif

            if (!(g_initial_state & INTIALIZATION_ENV)) {
                g_tmpl_holder.tmpl.tpl = NULL;
                g_tmpl_holder.tmpl.size = 0;

#ifdef AUTHTOKEN_HMAC
                retval =
                    hmac_setup_platform_sec_key(package->indata, package->indata_len, NULL, NULL);
                ex_log(LOG_DEBUG, "hmac_setup_platform_sec_key result = %d\n", retval);
#endif

#ifdef SUPPORT_PAYMENT
                payment_setAuthenticatorVersion(1, (char*)g_ifaa_version_path);
#endif
                g_initial_state |= INTIALIZATION_ENV;
            }
            break;

        case EX_CMD_INIT_ALGO:
            if (!(g_initial_state & INTIALIZATION_ALGO)) {
                retval = fp_initAlgAndPPLib();
                ex_log(LOG_DEBUG, "fp_initAlgAndPPLib retval = %d\n", retval);
                if (retval == 0) {
                    ex_log(LOG_DEBUG, "fp_initAlgAndPPLib success, retval = %d\n", retval);
                    g_initial_state |= INTIALIZATION_ALGO;
                }
            }
            break;

        case EX_CMD_INIT_SENSOR:
            retval = __uninit_sensor();
            if (retval != 0) ex_log(LOG_ERROR, "__uninit_sensor retval = %d\n", retval);

            if ((package->args1 == 0) && (package->args2 == 0)) {
                retval = fp_initSensor();
                ex_log(LOG_DEBUG, "fp_initSensor retval = %d\n", retval);
                if (retval != 0) {
                    ex_log(LOG_ERROR, "fp_initSensor failed, retval = %d\n", retval);
                    break;
                }
            }

            fp_getImageSize(&img_width, &img_height, &img_dpi);
            memset((void*)&g_img_holder.fingerprint_image, 0,
                   sizeof(g_img_holder.fingerprint_image));
            EGIS_IMAGE_CREATE(&g_img_holder.fingerprint_image, img_width, img_height,
                              EGIS_ENROLL_MAX_IMAGE_COUNTS);
            memset(&g_img_holder.fingerprint_image.quality, 0,
                   sizeof(g_img_holder.fingerprint_image.quality));
            if (!egis_image_is_allocated(&g_img_holder.fingerprint_image)) {
                ex_log(LOG_ERROR, "g_img_holder.fingerprint_image.img_data.buffer is NULL\n");
                retval = -1;  // need definition
                break;
            }
            fd_egis_image_init();
            g_initial_state |= INTIALIZATION_HW;
            break;

        case EX_CMD_UNINIT_SDK:
            /* release the global heap data which be used to hold
             * the fingerprint image */
#ifdef __ET7XX__
            bkm_uninit();
#endif
            if (g_initial_state & INTIALIZATION_ENV) {
                if (g_tmpl_holder.tmpl.tpl != NULL) {
                    plat_free(g_tmpl_holder.tmpl.tpl);
                    g_tmpl_holder.tmpl.tpl = NULL;
                    g_tmpl_holder.tmpl.size = 0;
                }

                template_uninit();
                g_initial_state &= ~INTIALIZATION_ENV;
            }

            break;

        case EX_CMD_UNINIT_ALGO:
            /* deinit algorithm
             *  the doubt : perhaps the successive callings will
             * make it works in confusion
             */
            if (g_initial_state & INTIALIZATION_ALGO) {
                g_initial_state &= ~INTIALIZATION_ALGO;
                fp_deInitAlg();
            }
            break;

        case EX_CMD_UNINIT_SENSOR:
            retval = __uninit_sensor();
            if (retval != 0) ex_log(LOG_ERROR, "__uninit_sensor retval = %d\n", retval);
            fd_egis_image_deinit();
            break;

        case EX_CMD_GET_RAW_IMAGE: {
            int x0_ratio_x100 = package->indata[0];
            int y0_ratio_x100 = package->indata[1];
            fd_egis_set_capture_roi(x0_ratio_x100, y0_ratio_x100);
            ex_log(LOG_DEBUG, "EX_CMD_GET_RAW_IMAGE %d, %d", x0_ratio_x100, y0_ratio_x100);
            retval = egis_captureImage_only_raw((void*)&g_img_holder.fingerprint_image, NULL, 0);
            if (retval != EGIS_OK) {
                ex_log(LOG_ERROR, "egis_captureImage_only_raw failed (%d)", retval);
                break;
            }
            break;
        }
        case EX_CMD_GET_IMAGE_IPP: {
            if (!out_data || !out_data_size || *out_data_size < (int)sizeof(cmd_imagestatus_t)) {
                retval = -1;
                break;
            }
            int x0_ratio_x100 = package->indata[0];
            int y0_ratio_x100 = package->indata[1];
            fd_egis_set_capture_roi(x0_ratio_x100, y0_ratio_x100);
            /* the method contains mode selection */
            retval = fp_captureImage(&g_img_holder.fingerprint_image, TYPE_ENROLLING,
                                     FD_PROCESS_OPTION_USE_OLD_RAW);
            ex_log(LOG_DEBUG, "fp_captureImage, retval = %d\n", retval);
            ex_log(LOG_DEBUG, "fp_captureImage, coverage = %d, quality = %d\n",
                   g_img_holder.fingerprint_image.quality.coverage,
                   g_img_holder.fingerprint_image.quality.quality);
            ex_log(LOG_DEBUG, "fp_captureImage, frame_count = %d, reject_reason = %d",
                   g_img_holder.fingerprint_image.img_data.frame_count,
                   g_img_holder.fingerprint_image.quality.reject_reason);

            if (retval == 0) g_img_holder.is_img_valid = 1;

            img_info = (cmd_imagestatus_t*)out_data;
            img_info->status = g_img_holder.fingerprint_image.quality.reject_reason;

            if (img_info->status == FP_LIB_ENROLL_SUCCESS && ukm_is_enabled() &&
                egis_fp_is_enroll() && !ukm_is_bkg_ready()) {
                uint8_t* raw_img = egis_image_get_pointer_raw(&g_img_holder.fingerprint_image, 0);
                if (raw_img != NULL) {
                    ukm_add_image(raw_img);
                }
            }

            *out_data_size = (int)sizeof(cmd_imagestatus_t);
            break;
        }
        case EX_CMD_GET_IMAGE: {
            if (!out_data || !out_data_size || *out_data_size < (int)sizeof(cmd_imagestatus_t)) {
                retval = -1;
                break;
            }

            int x0_ratio_x100 = package->indata[0];
            int y0_ratio_x100 = package->indata[1];
            fd_egis_set_capture_roi(x0_ratio_x100, y0_ratio_x100);
            retval = egis_captureImage_only_raw((void*)&g_img_holder.fingerprint_image, NULL, 0);
            if (retval != EGIS_OK) {
                ex_log(LOG_ERROR, "egis_captureImage_only_raw failed (%d)", retval);
                break;
            }
            /* the method contains mode selection */
            retval = fp_captureImage(&g_img_holder.fingerprint_image, TYPE_ENROLLING,
                                     FD_PROCESS_OPTION_NORMAL);
            ex_log(LOG_DEBUG, "fp_captureImage, retval = %d\n", retval);
            ex_log(LOG_DEBUG, "fp_captureImage, coverage = %d, quality = %d\n",
                   g_img_holder.fingerprint_image.quality.coverage,
                   g_img_holder.fingerprint_image.quality.quality);
            ex_log(LOG_DEBUG, "fp_captureImage, frame_count = %d\n",
                   g_img_holder.fingerprint_image.img_data.frame_count);

            if (retval == 0) g_img_holder.is_img_valid = 1;

            img_info = (cmd_imagestatus_t*)out_data;
            img_info->status = g_img_holder.fingerprint_image.quality.reject_reason;

            *out_data_size = (int)sizeof(cmd_imagestatus_t);
            break;
        }
        case EX_CMD_GET_ENROLLED_COUNT:
            enrolled_count = template_get_enrolled_count();
            memcpy(out_data, &enrolled_count, sizeof(uint32_t));
            *out_data_size = (int)sizeof(uint32_t);
            break;

        case EX_CMD_ENROLL_INIT:
            g_stop_DQE = FALSE;
            /*clean frame count, swipe/paint enroll do not reset frame count in finger detect.*/
            EGIS_IMAGE_RESET_FRAME_COUNT(&g_img_holder.fingerprint_image);
#ifdef AUTHTOKEN_PASS_INTERVAL
            pass_diff_time = plat_get_diff_time(g_authtoken_pass_time);
            if (!g_authtoken_is_pass || pass_diff_time > AUTHTOKEN_PASS_INTERVAL) {
                g_authtoken_is_pass = FALSE;
                ex_log(LOG_ERROR, "permission denied (1). %d, %d", g_authtoken_is_pass,
                       pass_diff_time);
                retval = FP_LIB_ERROR_GENERAL;
                break;
            }
#endif
#ifdef FREE_TEMPLATE_AFTER_PASS_TO_ALGO
            {
                cmd_fingerprint_ids_t st_fingerprint_ids;
                int fingerprint_count;
                template_load_all(st_fingerprint_ids.ids, &fingerprint_count);
            }
#endif
            retval = fp_startEnroll();
            ex_log(LOG_DEBUG, "fp_startEnroll, retval = %d\n", retval);
            if (retval == FP_LIB_OK && ukm_is_enabled()) {
                ukm_init();
            }
            break;

        case EX_CMD_ENROLL:
            /* redundancy check */
            enroll_option = package->args1;
            enrolled_count = package->args2;
            enroll_result = (cmd_enrollresult_t*)out_data;

            egislog_d("EX_CMD_ENROLL %d", enroll_option);
            if (!out_data || !out_data_size || *out_data_size < (int)sizeof(cmd_enrollresult_t)) {
                ex_log(LOG_ERROR, "EX_CMD_ENROLL, out_data_size:%d ,enrollresult size:%d\n",
                       *out_data_size, (int)sizeof(cmd_enrollresult_t));
                retval = -1;  // need definition
                break;
            }

#ifdef AUTHTOKEN_PASS_INTERVAL
            if (!g_authtoken_is_pass) {
                ex_log(LOG_ERROR, "permission denied.(2) %d, %d", g_authtoken_is_pass,
                       pass_diff_time);
                retval = FP_LIB_ERROR_GENERAL;
                if (out_data) {
                    int redundant_result = FP_LIB_ENROLL_PERMISSION_DENIED;
                    memcpy(out_data, &redundant_result, sizeof(redundant_result));
                }
                break;
            }
#endif
            if (enroll_option == ENROLL_OPTION_STOP_DQE) {
                g_stop_DQE = TRUE;
                enroll_result->status = FP_LIB_ENROLL_SUCCESS;
                retval = FP_LIB_OK;
                break;
            } else if (enroll_option == ENROLL_OPTION_CLEAR_IMAGES) {
                // KEEP_ENROLL_RETRY_IMAGES
                egislog_v("enroll - clear enroll images");
                EGIS_IMAGE_RESET_FRAME_COUNT(&g_img_holder.fingerprint_image);
                enroll_result->status = FP_LIB_ENROLL_SUCCESS;
                retval = FP_LIB_OK;
                break;
            }

            if (ukm_is_enabled() && !ukm_is_bkg_ready()) {
                retval = FP_LIB_OK;
                enroll_result->status = FP_LIB_ENROLL_SUCCESS;
                enroll_result->percentage = ukm_get_uk_progress(0);
                break;
            }

            if (enroll_option == ENROLL_OPTION_MERGE) {
                retval = fp_mergeEnrollTemp(&enroll_data, &g_img_holder.fingerprint_image);
            } else {
                if (enroll_option == ENROLL_OPTION_FINGERON) {
                    fp_setFingerState(ENROLL_OPTION_FINGERON);
                    enroll_data.enroll_option = ENROLL_OPTION_NORMAL;
                } else {
                    enroll_data.enroll_option = enroll_option;
                }

                if (!g_img_holder.is_img_valid && enroll_option != ENROLL_OPTION_ENROLL_THE_FIRST) {
                    ex_log(LOG_ERROR, "ERR : no fingerprint image or has enrolled");
                    retval = EGIS_COMMAND_FAIL;
                    break;
                }

                retval = fp_egis_enroll((void*)&g_img_holder.fingerprint_image, &enroll_data,
                                        enrolled_count, &(enroll_result->swipe_info));
                ex_log(LOG_DEBUG, "fp_egis_enroll, retval = %d", retval);
                ex_log(LOG_DEBUG, "fp_egis_enroll, enroll_data.result = %d", enroll_data.result);
            }

            /* image will be used and lose efficacy */
            g_img_holder.is_img_valid = 0;

            if (retval) {
                ex_log(LOG_ERROR, "fp_egis_enroll, retval = %d\n", retval);
                break;
            }

            enroll_result->status = enroll_data.result;
            if (enroll_data.result == FP_LIB_ENROLL_FAIL_NONE ||
                enroll_data.result == FP_LIB_ENROLL_TOO_MANY_ATTEMPTS ||
                enroll_data.result == FP_LIB_ENROLL_TOO_MANY_FAILED_ATTEMPTS) {
                enroll_result->percentage = 0;
            } else {
                if (ukm_is_enabled()) {
                    if (enroll_option == ENROLL_OPTION_NORMAL && ukm_is_bkg_ready()) {
                        ukm_do_delayed_enroll(&g_img_holder.fingerprint_image, &enroll_data,
                                              enroll_data.progress, enrolled_count);
                    }
                    enroll_result->percentage = ukm_get_uk_progress(enroll_data.progress);
                } else {
                    enroll_result->percentage = enroll_data.progress;
                }
            }

            *out_data_size = (int)sizeof(cmd_enrollresult_t);
            break;

        case EX_CMD_ENROLL_UNINIT:
            /* enroll & pack template */
            if (g_tmpl_holder.tmpl.tpl != NULL && g_tmpl_holder.tmpl.size > 0 &&
                g_tmpl_holder.is_tmpl_valid) {
                plat_free(g_tmpl_holder.tmpl.tpl);
                g_tmpl_holder.tmpl.tpl = NULL;
                g_tmpl_holder.tmpl.size = 0;
            }
#ifdef FREE_TEMPLATE_AFTER_PASS_TO_ALGO
            ex_log(LOG_DEBUG, "FREE_TEMPLATE: free before finish enroll");
            template_delete_all(FALSE);
#endif
            retval = fp_finishEnroll(&g_tmpl_holder.tmpl);
            ex_log(LOG_INFO, "fp_finishEnroll, retval = %d", retval);

#ifdef AUTHTOKEN_PASS_INTERVAL
            g_authtoken_is_pass = FALSE;
#endif
            if (ukm_is_inited()) {
                ukm_deinit();
            }
            if (retval == 0) g_tmpl_holder.is_tmpl_valid = 1;
            break;

        case EX_CMD_ENROLL_SAVE_TEMPLATE:
            fingerprint_id = package->args2;
            if (!g_tmpl_holder.is_tmpl_valid) {
                ex_log(LOG_ERROR, "ERR : no fingerprint image to save\n");
                retval = -1;
                break;
            }
            g_tmpl_holder.is_tmpl_valid = 0;
            retval = template_add(fingerprint_id, g_tmpl_holder.tmpl.tpl, g_tmpl_holder.tmpl.size);
            if (retval == EGIS_OK) {
                retval = update_authenticator_id();
#ifdef SUPPORT_PAYMENT
                update_ifaa_fpid_list(NULL, 0, (const char*)g_ifaa_fplist_path);
#endif
            } else {
                ex_log(LOG_ERROR, "template_add failed ,retval = %d", retval);
            }

            if (g_tmpl_holder.tmpl.tpl != NULL) {
                plat_free(g_tmpl_holder.tmpl.tpl);
                g_tmpl_holder.tmpl.tpl = NULL;
                g_tmpl_holder.tmpl.size = 0;
            }
            break;
#ifdef __ET7XX__
        case EX_CMD_RESET_BDS:
            retval = bkm_reset_bds();
            break;
        case EX_CMD_SAVE_BDS:
            retval = bkm_save_bds();
            break;
#endif
        case EX_CMD_IDENTIFY_START:
            need_liveness_auth = package->args1;
            ex_log(LOG_DEBUG, "need_liveness_auth = %d\n", need_liveness_auth);
            g_is_verify_mode = TRUE;
            EGIS_IMAGE_RESET_FRAME_COUNT(&g_img_holder.fingerprint_image);

#ifdef FREE_TEMPLATE_AFTER_PASS_TO_ALGO
            {
                cmd_fingerprint_ids_t st_fingerprint_ids;
                int fingerprint_count;
                template_load_all(st_fingerprint_ids.ids, &fingerprint_count);
            }
#endif
            retval = fp_identifyStart(need_liveness_auth);
            if (retval) {
                ex_log(LOG_ERROR, "fp_identifyStart, retval = %d", retval);
                break;
            }
#ifdef FREE_TEMPLATE_AFTER_PASS_TO_ALGO
            ex_log(LOG_DEBUG, "FREE_TEMPLATE: free after verify init");
            template_delete_all(FALSE);
#endif
            break;

        case EX_CMD_IDENTIFY:

            identify_result = (cmd_identifyresult_t*)out_data;
            unsigned char* x_data = out_data + sizeof(cmd_identifyresult_t);
            challenge = *(uint64_t*)package->indata;
            enrolled_count = template_get_enrolled_count();
            img_width = g_img_holder.fingerprint_image.img_data.format.width;
            img_height = g_img_holder.fingerprint_image.img_data.format.height;
            ex_log(LOG_DEBUG, "EX_CMD_VERIFY enrolled_count = %d", enrolled_count);

            /* clear the record of last learning template*/
            g_tmpl_holder.updated_pos = -1;

#ifdef SUPPORT_PAYMENT
            payment_update_last_identify_result(NULL);
#endif
            memset(&g_identify_data, 0, sizeof(g_identify_data));
            retval = fp_identifyImage_identify(&g_img_holder.fingerprint_image, &g_identify_data,
                                               enrolled_count);
            ex_log(LOG_DEBUG, "fp_identifyImage_identify, retval = %d\n", retval);
            ex_log(LOG_DEBUG, "fp_identifyImage_identify, g_identify_data.result = %d\n",
                   g_identify_data.result);
            ex_log(LOG_DEBUG, "fp_identifyImage_identify, g_identify_data.index = %d\n",
                   g_identify_data.index);
            ex_log(LOG_DEBUG, "fp_identifyImage_identify, g_identify_data.score = %d\n",
                   g_identify_data.score);
            if (retval) {
                ex_log(LOG_ERROR, "fp_identifyImage_identify, retval != %d\n", retval);
                break;
            }

            if (g_identify_data.result == FP_LIB_IDENTIFY_MATCH_UPDATED_TEMPLATE) {
                g_tmpl_holder.updated_pos = g_identify_data.index;
            }

            identify_result->status = g_identify_data.result;
            identify_result->ext_feat_quality = g_identify_data.extract_result.ext_feat_quality;
            identify_result->x_len = 0;
            if (g_identify_data.result == FP_LIB_IDENTIFY_MATCH ||
                g_identify_data.result == FP_LIB_IDENTIFY_MATCH_UPDATED_TEMPLATE) {
                identify_result->matched_id = template_get_fid(g_identify_data.index);
                identify_result->is_tmpl_update =
                    (g_identify_data.result == FP_LIB_IDENTIFY_MATCH_UPDATED_TEMPLATE) ? 1 : 0;
#ifdef AUTHTOKEN_HMAC
                identify_result->x_len =
                    (out_data_size != NULL) ? (*out_data_size - IDENTIFYRESULT_X_DATA_OFFSET) : 0;
                retval = perms_generate_aosp_hw_auth_token(challenge, x_data,
                                                           (unsigned int*)&identify_result->x_len);
                ex_log(LOG_DEBUG, "perms_generate_aosp_hw_auth_token, result = %d\n", retval);
                ex_log(LOG_DEBUG,
                       "perms_generate_aosp_hw_auth_token, identify_result->x_len = %d\n",
                       identify_result->x_len);
#endif
#ifdef SUPPORT_PAYMENT
                struct LastIdentifyResult last_result;
                last_result.matched_fingerprint_id = identify_result->matched_id;
                last_result.operation_id = challenge;
                payment_update_last_identify_result(&last_result);
#endif
            } else if (g_identify_data.result == FP_LIB_IDENTIFY_NO_MATCH) {
                identify_result->matched_id = 0;
                identify_result->is_tmpl_update = 0;
//#if defined(ALGO_GEN_7)
//                uint8_t is_update;
//                int i;
//                enrolled_count = template_get_enrolled_count();
//                ex_log(LOG_DEBUG, "not matched, update not match template enrolled_count = %d",
//                       enrolled_count);
//                for (i = 0; i < enrolled_count; i++) {
//                    g_tmpl_holder.updated_not_match_tmpl[i] = NULL;
//                    retval = fp_identifyUpdateNotMatchTemplate(
//                        (void*)&(g_tmpl_holder.updated_not_match_tmpl[i]), &is_update, i);
//                }
//                // if (retval == FP_LIB_OK) {
//                //	*out_data_size = sizeof(uint8_t);
//                //	memcpy(out_data, &is_update, sizeof(uint8_t));
//                //}
//#endif
            } else {
                identify_result->matched_id = 0;
                identify_result->is_tmpl_update = 0;
            }
            *out_data_size = IDENTIFYRESULT_X_DATA_OFFSET + identify_result->x_len;

            break;

        case EX_CMD_IDENTIFY_FINISH:
            fp_identifyFinish();
            g_is_verify_mode = FALSE;
            EGIS_IMAGE_RESET_FRAME_COUNT(&g_img_holder.fingerprint_image);
            retval = 0;
            break;

        case EX_CMD_IDENTIFY_TMPL_UPDATE:
#ifdef FREE_TEMPLATE_AFTER_PASS_TO_ALGO
            g_tmpl_holder.updated_tmpl = &g_tmpl_holder.tmpl;
            fplib_template_create(g_tmpl_holder.updated_tmpl);
#else
            g_tmpl_holder.updated_tmpl = NULL;
#endif
            retval =
                fp_identifyUpdateTemplate((void*)&(g_tmpl_holder.updated_tmpl),
                                          &is_identified_update, &g_img_holder.fingerprint_image);
            if (retval == FP_LIB_OK) {
                *out_data_size = sizeof(uint8_t);
                memcpy(out_data, &is_identified_update, sizeof(uint8_t));
            }
#ifdef FREE_TEMPLATE_AFTER_PASS_TO_ALGO
            if (!is_identified_update) {
                fplib_template_destroy(g_tmpl_holder.updated_tmpl);
            }
#endif
            break;

        case EX_CMD_IDENTIFY_TMPL_SAVE:
//#if defined(ALGO_GEN_7)
//        {
//            int i;
//            uint8_t is_update;
//            enrolled_count = template_get_enrolled_count();
//
//            ex_log(LOG_DEBUG, "matched, update and save not match template, enrolled_count = %d",
//                   enrolled_count);
//            ex_log(LOG_DEBUG, "g_tmpl_holder.updated_pos = %d", g_tmpl_holder.updated_pos);
//            for (i = 0; i < enrolled_count; i++) {
//                if (i == g_tmpl_holder.updated_pos) continue;
//                g_tmpl_holder.updated_not_match_tmpl[i] = NULL;
//                retval = fp_identifyUpdateNotMatchTemplate(
//                    (void*)&(g_tmpl_holder.updated_not_match_tmpl[i]), &is_update, i);
//
//                if (retval != FP_LIB_OK) {
//                    ex_log(LOG_ERROR, "update fail %d", retval);
//                    continue;
//                } else if (is_update == FALSE) {
//                    ex_log(LOG_DEBUG, "not need update");
//                    continue;
//                }
//
//                ex_log(LOG_DEBUG, "template_update[%d] %p %d", i,
//                       g_tmpl_holder.updated_not_match_tmpl[i]->tpl,
//                       g_tmpl_holder.updated_not_match_tmpl[i]->size);
//
//                retval = template_update(i, g_tmpl_holder.updated_not_match_tmpl[i]->tpl,
//                                         g_tmpl_holder.updated_not_match_tmpl[i]->size);
//            }
//        }
//#endif
            if (g_tmpl_holder.updated_pos >= 0) {
                retval = template_update(g_tmpl_holder.updated_pos, g_tmpl_holder.updated_tmpl->tpl,
                                         g_tmpl_holder.updated_tmpl->size);
                g_tmpl_holder.updated_pos = -1;
            }
#ifdef FREE_TEMPLATE_AFTER_PASS_TO_ALGO
            fplib_template_destroy(g_tmpl_holder.updated_tmpl);
#endif
            break;

#ifdef FEATURE_NAVIGATION
        case EX_CMD_NAVIGATION: {
            uint32_t navi_mode = NAVI_MODE_NORMAL;
            if (out_data == NULL || out_data_size == NULL) {
                retval = EGIS_INCORRECT_PARAMETER;
                break;
            }
            if (navi_mode != NAVI_MODE_OLD) {
                if (*out_data_size < sizeof(struct NaviStatusInfo)) {
                    retval = EGIS_INCORRECT_PARAMETER;
                    break;
                }
                struct NaviStatusInfo* navi_status_info = (struct NaviStatusInfo*)out_data;
                retval = fp_navGetInfo(navi_status_info);
                if (retval == EGIS_OK) {
                    *out_data_size = sizeof(*navi_status_info);
                    memcpy(out_data, navi_status_info, sizeof(*navi_status_info));
                } else {
                    *out_data_size = 0;
                }
            } else {
                if (*out_data_size < sizeof(int)) {
                    retval = EGIS_INCORRECT_PARAMETER;
                    break;
                }
                navigation_info_common naviInfo;
                retval = fp_navGetEvent(&naviInfo);
                if (retval == EGIS_OK) {
                    memcpy(out_data, &naviInfo.eventValue, sizeof(int));
                    *out_data_size = sizeof(int);
                } else {
                    *out_data_size = 0;
                }
            }
        } break;
#endif
        case EX_CMD_REMOVE:
            fingerprint_id = package->args2;
            if (fingerprint_id == 0) {
                retval = template_delete_all(TRUE);
            } else {
                retval = template_delete(fingerprint_id);
            }

            if (retval == TEMPL_ERROR_PARAM) {
                retval = FP_LIB_ERROR_INVALID_FINGERID;
            }

            if (retval == EGIS_OK) {
                retval = update_authenticator_id();
#ifdef SUPPORT_PAYMENT
                update_ifaa_fpid_list(NULL, 0, (const char*)g_ifaa_fplist_path);
#endif
            }

            break;

        case EX_CMD_GET_FP_IDS:

            fingerprint_ids = (cmd_fingerprint_ids_t*)out_data;

            enrolled_count = template_get_enrolled_count();
            ex_log(LOG_DEBUG, "enrolled_count=%d", enrolled_count);

            if (!out_data || !out_data_size ||
                *out_data_size < (int)(sizeof(uint32_t) * enrolled_count)) {
                ex_log(LOG_ERROR,
                       "EX_CMD_GET_FP_IDS, out_data_size:%d, (int)(sizeof(uint32_t) * "
                       "enrolled_count):%d\n",
                       *out_data_size, (int)(sizeof(uint32_t) * enrolled_count));
                retval = -1;
                break;
            }

            retval = template_load_all(fingerprint_ids->ids, &fid_count);
            ex_log(LOG_DEBUG, "retval=%d, fid_count=%d", retval, fid_count);
            fingerprint_ids->count = fid_count;
            *out_data_size = sizeof(cmd_fingerprint_ids_t);
            break;

        case EX_CMD_SET_WORK_MODE:
            mode = package->args1;
            if (mode == DETECT_MODE || mode == NAVI_DETECT_MODE) {
                /*
                 *  FORCED_DETECT_MODE == 0 --> isensor_set_detect_mode
                 *  FORCED_DETECT_MODE == 1 --> isensor_set_navigation_detect_mode
                 */
                FORCED_DETECT_MODE = (mode == DETECT_MODE) ? 0 : 1;
                retval = fp_initFingerDetect(&wait_time, FORCED_DETECT_MODE);
            } else if (mode == 2) {
                retval = fp_sensorSleep();
            }
            break;

        case EX_CMD_SET_ACTIVE_USER: {
            group_id = package->args1;
            cmd_fingerprint_ids_t fingerprint_ids;
#ifdef SUPPORT_PAYMENT
            plat_set_ifaa_path_prefix((char*)g_ifaa_fplist_path, PATH_MAX,
                                      (const char*)package->indata, "ifaa_fplist");
            ex_log(LOG_DEBUG, "g_ifaa_fplist_path=%s", g_ifaa_fplist_path);
            plat_set_ifaa_path_prefix((char*)g_ifaa_version_path, PATH_MAX,
                                      (const char*)package->indata, "fingerprint_version");
            ex_log(LOG_DEBUG, "g_ifaa_version_path=%s", g_ifaa_version_path);
#endif
            retval = template_set_active_group(group_id, (const char*)package->indata, "template");
            template_load_all(fingerprint_ids.ids, &fid_count);
            fingerprint_ids.count = fid_count;

#ifdef AUTHTOKEN_HMAC
            if (retval == EGIS_OK) {
                retval = gpmngr_setup_group_info(group_id, (unsigned int*)fingerprint_ids.ids,
                                                 fingerprint_ids.count,
                                                 (const char*)package->indata, package->indata_len);
                if (retval != 0) {
                    ex_log(LOG_ERROR, "gpmngr_setup_group_info failed, retval = %d\n", retval);
                }
            }
#endif
#ifdef SUPPORT_PAYMENT
            update_ifaa_fpid_list(fingerprint_ids.ids, fid_count, (const char*)g_ifaa_fplist_path);
#endif
        } break;

        case EX_CMD_CHECK_SID:

#ifdef AUTHTOKEN_HMAC
            secure_group_id = *(unsigned long long*)package->indata;
            enrolled_count = template_get_enrolled_count();

            if (enrolled_count > 0) {
                retval = gpmngr_check_secure_group_id(secure_group_id);
            } else {
                retval = EGIS_SECURE_ID_NULL;
            }

            if (retval == EGIS_SECURE_ID_NULL) {
                retval = gpmngr_update_secure_group_id(secure_group_id);
                ex_log(LOG_ERROR, "gpmngr_update_secure_group_id, retval = %d\n", retval);
            }
#endif
            break;

        case EX_CMD_CHECK_AUTH_TOKEN:

#ifdef AUTHTOKEN_HMAC
            retval = perms_check_aosp_hw_auth_token(package->indata, package->indata_len);
            ex_log(LOG_DEBUG, "perms_check_aosp_hw_auth_token, retval = %d\n", retval);
            memcpy(out_data, &retval, sizeof(int));
            *out_data_size = (int)sizeof(int);
#endif
#ifdef AUTHTOKEN_PASS_INTERVAL
            g_authtoken_is_pass = (retval == EGIS_OK) ? TRUE : FALSE;
            g_authtoken_pass_time = plat_get_time();
#endif

            break;

        case EX_CMD_GET_AUTH_ID:

            if (!out_data || !out_data_size || *out_data_size < sizeof(unsigned long long)) {
                ex_log(LOG_ERROR, "EX_CMD_GET_AUTH_ID invalid parameter\n");
                retval = EGIS_INCORRECT_PARAMETER;
                break;
            }

#ifdef AUTHTOKEN_HMAC
            retval = gpmngr_get_authenticator_id((unsigned long long*)out_data);
            if (retval == EGIS_OK) {
                *out_data_size = sizeof(unsigned long long);
            } else {
                *out_data_size = 0;
            }
#endif
            break;

        case EX_CMD_SENSOR_TEST: {
            fp_mmi_info* info = (fp_mmi_info*)plat_alloc(sizeof(fp_mmi_info));
            if (NULL != info) {
                if (strcmp((const char*)package->indata, "FP_MMI_DIRTYDOTS_TEST") == 0)
                    info->testType = FP_MMI_DIRTYDOTS_TEST;
                else if (strcmp((const char*)package->indata, "FP_MMI_READ_REV_TEST") == 0)
                    info->testType = FP_MMI_READ_REV_TEST;
                else if (strcmp((const char*)package->indata, "FP_MMI_REGISTER_RW_TEST") == 0)
                    info->testType = FP_MMI_REGISTER_RW_TEST;
            }
            retval = fp_mmiDoTest(info);
            if (NULL != info) plat_free(info);
        } break;

        case EX_CMD_CHECK_FINGER_LOST:
            if (g_is_verify_mode) EGIS_IMAGE_RESET_FRAME_COUNT(&g_img_holder.fingerprint_image);
            wait_time = package->args1;
            if (!out_data || !out_data_size || *out_data_size < sizeof(uint32_t)) {
                ex_log(LOG_ERROR,
                       "EX_CMD_CHECK_FINGER_LOST, out_data_size:%d, sizeof(uint32_t):%d\n",
                       *out_data_size, sizeof(uint32_t));
                retval = -1;
                break;
            }

            finger_status = fp_checkFingerLost(&wait_time);
            memcpy(out_data, (unsigned char*)&finger_status, sizeof(int));
            *out_data_size = (int)sizeof(int);
            break;

#ifdef FEATURE_NAVIGATION
        case EX_CMD_NAVI_CONTROL: {
            int extra_cmd = *(int*)package->indata;
            fp_getImageSize(&img_width, &img_height, &img_dpi);
            retval = navi_control_api(extra_cmd, img_width, img_height);
            break;
        }
#endif
        case EX_CMD_OPEN_SPI:
            g_auto_spi_control = FALSE;
            ex_log(LOG_DEBUG, "EX_CMD_OPEN_SPI, auto spi ctrl = %d", g_auto_spi_control);
            break;

        case EX_CMD_CLOSE_SPI:
            g_auto_spi_control = TRUE;
            ex_log(LOG_DEBUG, "EX_CMD_CLOSE_SPI, auto spi ctrl = %d", g_auto_spi_control);
            break;
        case EX_CMD_SET_DATA:
        case EX_CMD_GET_DATA:
            retval = extra_operations(package->command, package->args1, package->indata,
                                      package->indata_len, out_data, out_data_size);
            break;
        case EX_CMD_PRINT_MEMORY:
#ifdef EGIS_DEBUG_MEMORY
            ex_log(LOG_INFO, "\n egis_memory_check Start, level = %d", package->args1);
            egis_memory_set_log_level(package->args1);
            egis_memory_check(MEMORY_NOT_FORCED_FREE);
            ex_log(LOG_INFO, "egis_memory_check End \n");
#endif
            break;
        case EX_CMD_SET_VERIFY_FINGER_INFO: {
            ex_log(LOG_DEBUG, "multi_threshold EX_CMD_SET_VERIFY_FINGER_INFO");
            ex_log(LOG_DEBUG, "multi_threshold package->indata_len %d", package->indata_len);
            int verify_count = package->indata[0];
            int verified_count = package->indata[1];
            ex_log(LOG_DEBUG, "multi_threshold verify_count %d", verify_count);
            ex_log(LOG_DEBUG, "multi_threshold verified_count %d", verified_count);
            fp_set_verify_finger_info(verify_count, verified_count);
        }
            break;
        default:
            break;
    }

#if !defined __DEVICE_DRIVER_MEIZU_1712__ && !defined _WINDOWS
    if (is_sensor_command(package->command) && g_auto_spi_control) {
        ret = fp_setPower(SPI_POWER_OFF);
        if (ret != FP_LIB_OK) ex_log(LOG_ERROR, "fp_setPower:close fail");
        ex_log(LOG_VERBOSE, "close spi power ret = %d", ret);
    }
#endif

    if (fp_getRecoveryEvent() == TRUE) retval = EGIS_ESD_NEED_RESET;

    ex_log(LOG_DEBUG, "command_handler package->command:%d, return = %d", package->command, retval);
    return retval;
}

BOOL is_sensor_command(unsigned int command) {
    BOOL retval = FALSE;
    switch (command) {
        case EX_CMD_INIT_SENSOR:
        case EX_CMD_UNINIT_SENSOR:
        case EX_CMD_GET_IMAGE:
        case EX_CMD_GET_RAW_IMAGE:
        case EX_CMD_NAVIGATION:
        case EX_CMD_SET_WORK_MODE:
        case EX_CMD_SENSOR_TEST:
        case EX_CMD_CHECK_FINGER_LOST:
        case EX_CMD_NAVI_CONTROL:
        case EX_CMD_OPEN_SPI:
        case EX_CMD_CLOSE_SPI:
        case EX_CMD_GET_DEV_HANDLE:
            ex_log(LOG_DEBUG, "a sensor command:%d", command);
            retval = TRUE;
            break;
        default:
            ex_log(LOG_DEBUG, "not a sensor command:%d", command);
            break;
    }
    return retval;
}

int update_authenticator_id() {
    int retval = EGIS_OK;

#ifdef AUTHTOKEN_HMAC
    cmd_fingerprint_ids_t fingerprint_ids;
    int fid_count;
    retval = template_load_all(fingerprint_ids.ids, &fid_count);
    fingerprint_ids.count = fid_count;
    if (retval == EGIS_OK) {
        retval = gpmngr_update_authenticator_id((unsigned int*)fingerprint_ids.ids,
                                                fingerprint_ids.count);
    }

    ex_log(LOG_ERROR, "update_authenticator_id ,result = %d\n", retval);
#endif

    return retval;
}

#ifdef FEATURE_NAVIGATION
int navi_control_api(int extra_id, int img_width, int img_height) {
    int retval = EGIS_OK;
    switch (extra_id) {
        case NAVI_CMD_START: {
            ex_log(LOG_DEBUG, "start qm for navi");
            g_auto_spi_control = FALSE;
            init_qm(img_width, img_height);
        } break;
        case NAVI_CMD_STOP: {
            ex_log(LOG_DEBUG, "stop qm for navi");
            g_auto_spi_control = TRUE;
            uninit_qm();
        } break;
        default:
            retval = EGIS_INCORRECT_PARAMETER;
            break;
    }
    return retval;
}
#endif

int update_ifaa_fpid_list(uint32_t* fingerprint_ids, int fingerprint_count, const char* path) {
    int retval = EGIS_OK;

#ifdef SUPPORT_PAYMENT
    ex_log(LOG_DEBUG, "%s entry", __func__);
    if (fingerprint_ids == NULL) {
        cmd_fingerprint_ids_t st_fingerprint_ids;
        retval = template_load_all(st_fingerprint_ids.ids, &fingerprint_count);
        fingerprint_ids = st_fingerprint_ids.ids;
    }
    retval = payment_update_fingerprint_ids(fingerprint_ids, fingerprint_count, path);
    if (retval != 0) {
        ex_log(LOG_ERROR, "%s, set FpIdList failed, retval = %d", __func__, retval);
        return retval;
    }
    ex_log(LOG_DEBUG, "%s leave, retval = %d", __func__, retval);
#endif
    return retval;
}
