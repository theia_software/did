#include <limits.h>

#include "egis_definition.h"
#include "egis_sprintf.h"
#include "group_manager.h"
#include "plat_file.h"
#include "plat_hmac.h"
#include "plat_log.h"
#include "plat_mem.h"
#include "plat_prng.h"

#define LOG_TAG "RBS-GROUPMANAGER"

struct GroupKeyData g_data = {0};
static char g_tee_default_path[PATH_MAX] = {"/data/misc/vendor/egistec/gpinfo"};
static char g_gourp_info_path[PATH_MAX];
static unsigned int g_group_info_loaded = 0;

int gpmngr_setup_group_info(unsigned int group_id, unsigned int* fingerprint_ids,
                            unsigned int fingerprint_count, const char* path, unsigned int len) {
    int retval, retSize;
    unsigned int read_len = 0;
    struct PlatSecureKey key;
    unsigned char hmac[HMAC_SHA256_BYTES];
    g_group_info_loaded = 0;

    plat_set_template_path_prefix(g_gourp_info_path, PATH_MAX, path, "gpinfo");
    egislog_d("%s path_prefix=%s", __func__, g_gourp_info_path);

    if (path != NULL && len > 0)
        get_app_ctx_path(g_gourp_info_path, PATH_MAX, g_gourp_info_path, group_id);
    else
        get_app_ctx_path(g_gourp_info_path, PATH_MAX, g_tee_default_path, group_id);

    retSize = plat_load_file(g_gourp_info_path, (unsigned char*)&g_data,
                             sizeof(struct GroupKeyData), &read_len);

    egislog(LOG_DEBUG, "gpmngr_setup_group_info plat_load_file %d ,%d,%d", retSize, read_len,
            fingerprint_count);
    if (retSize < sizeof(struct GroupKeyData) || read_len < sizeof(struct GroupKeyData)) {
        if (fingerprint_count > 0) {
            egislog(LOG_ERROR, "%s failed to load %s (%d)", __func__, g_gourp_info_path, retSize);
            return EGIS_COMMAND_FAIL;
        } else {
            g_data.secure_group_id = (unsigned long long)-1;
            g_data.authenticator_id = (unsigned long long)-1;
            g_data.group_id = group_id;
            g_group_info_loaded = 1;
            return EGIS_OK;
        }
    } else if (g_data.group_id != group_id)
        return EGIS_COMMAND_FAIL;

    g_group_info_loaded = 1;

    retval = hmac_setup_platform_sec_key(NULL, 0, key.key, &key.len);
    if (retval != EGIS_OK) {
        return retval;
    }

    retval = hmac_sha256((unsigned char*)fingerprint_ids, sizeof(unsigned int) * fingerprint_count,
                         key.key, key.len, hmac);
    if (retval != EGIS_OK) {
        return EGIS_COMMAND_FAIL;
    }

    // Skip the checking because master key changes whenever reboot
    // if (0 != mem_compare(g_data.fp_hmac, hmac, HMAC_SHA256_BYTES)) {
    // 	return EGIS_PERMISSION_DENIED;
    // }

    return EGIS_OK;
}

int gpmngr_check_secure_group_id(unsigned long long sid) {
    if (!g_group_info_loaded) return EGIS_PERMISSION_DENIED;

    /*
     *	no fingerprints , no secure_group_id
     */
    if (g_data.secure_group_id == 0) return EGIS_SECURE_ID_NULL;

    /*
     *	sid is matched
     */
    if (g_data.secure_group_id == sid) return EGIS_OK;

    return EGIS_SECURE_ID_NOTMATCH;
}

int gpmngr_update_secure_group_id(unsigned long long sid) {
    int retSize;
    ex_log(LOG_DEBUG, "gpmngr_update_secure_group_id g_group_info_loaded = %d, new sid = %d",
           g_group_info_loaded, sid);
    if (!g_group_info_loaded) return EGIS_PERMISSION_DENIED;

    g_data.secure_group_id = sid;

#ifndef __TRUSTONIC__
    retSize =
        plat_save_file(g_gourp_info_path, (unsigned char*)&g_data, sizeof(struct GroupKeyData));

    if (retSize < sizeof(struct GroupKeyData)) return EGIS_COMMAND_FAIL;
#endif
    return EGIS_OK;
}

int gpmngr_get_secure_group_id(unsigned long long* sid) {
    if (sid == NULL) return EGIS_SECURE_ID_NULL;

    if (!g_group_info_loaded) return EGIS_PERMISSION_DENIED;

    *sid = g_data.secure_group_id;

    return EGIS_OK;
}

int gpmngr_update_authenticator_id(unsigned int* fingerprint_ids, unsigned int fingerprint_count) {
    int retval, retSize;
    unsigned long long authenticator_id;
    struct PlatSecureKey key;

    if (fingerprint_count == 0) {
        g_data.authenticator_id = (unsigned long long)-1;
        mem_set(g_data.fp_hmac, 0, HMAC_SHA256_BYTES);
    } else {
        if (fingerprint_ids == NULL) return EGIS_COMMAND_FAIL;

        plat_prn_generate(&g_data.authenticator_id, sizeof(unsigned long long));
        /*
         *	To generate HMAC using fingerprint_ids
         */
        retval = hmac_setup_platform_sec_key(NULL, 0, key.key, &key.len);
        if (retval != EGIS_OK) {
            return retval;
        }

        retval =
            hmac_sha256((unsigned char*)fingerprint_ids, sizeof(unsigned int) * fingerprint_count,
                        key.key, key.len, g_data.fp_hmac);
        if (retval != EGIS_OK) {
            return retval;
        }
    }

#ifndef __TRUSTONIC__
    /*
     *	save GroupKeyData to file
     */
    retSize =
        plat_save_file(g_gourp_info_path, (unsigned char*)&g_data, sizeof(struct GroupKeyData));

    if (retSize < sizeof(struct GroupKeyData)) return EGIS_COMMAND_FAIL;
#endif
    return EGIS_OK;
}

int gpmngr_get_authenticator_id(unsigned long long* authenticator_id) {
    if (authenticator_id == NULL) return EGIS_COMMAND_FAIL;

    if (!g_group_info_loaded) return EGIS_PERMISSION_DENIED;

    *authenticator_id = g_data.authenticator_id;
    return EGIS_OK;
}
