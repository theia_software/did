#ifndef __GROUP_MANAGER_H
#define __GROUP_MANAGER_H

#include "plat_hmac.h"

struct GroupKeyData {
    unsigned int group_id;
    unsigned long long secure_group_id;
    unsigned long long authenticator_id;
    unsigned char fp_hmac[HMAC_SHA256_BYTES];
};

/*
 *	- load the group-info file specified by { @group_id } & { @path }
 *	- use { @fingerprint_ids } to check the compatibility
 */

int gpmngr_setup_group_info(unsigned int group_id, unsigned int* fingerprint_ids,
                            unsigned int fingerprint_count, const char* path, unsigned int len);

/*	secure_group_id
 *	a unique id distinguishes a specific group from the other
 */
int gpmngr_check_secure_group_id(unsigned long long sid);

int gpmngr_update_secure_group_id(unsigned long long sid);

int gpmngr_get_secure_group_id(unsigned long long* sid);

/*	authenticator_id
 *	a unique id marks the changes of fingerprints is the current group
 */
// int gpmngr_check_authenticator_id();
int gpmngr_update_authenticator_id(unsigned int* fingerprint_ids, unsigned int fingerprint_count);
int gpmngr_get_authenticator_id(unsigned long long* authenticator_id);

#endif
