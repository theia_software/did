#include <string.h>

#include "egis_definition.h"
#include "group_manager.h"
#include "permission_ops.h"
#include "plat_hmac.h"
#include "plat_log.h"
#include "plat_mem.h"
#include "plat_time.h"

static void swap_bytes(unsigned char* bytes, unsigned int size) {
    unsigned char* first;
    unsigned char* last;
    unsigned char temp;
    for (first = bytes, last = bytes + size - 1; first < last; ++first, --last) {
        temp = *first;
        *first = *last;
        *last = temp;
    }
}

int perms_generate_aosp_hw_auth_token(unsigned long long challenge, unsigned char* out,
                                      unsigned int* out_len) {
    int retval;
    unsigned long long timestamp;
    unsigned long long authenticator_id;
    unsigned int authenticator_type;
    struct AospHwAuthToken token;
    struct PlatSecureKey secure_key;

    if (out == NULL || *out_len < sizeof(struct AospHwAuthToken)) {
        *out_len = 0;
        return EGIS_INCORRECT_PARAMETER;
    }
    secure_key.len = 0;

    timestamp = plat_get_time();
    swap_bytes((unsigned char*)&timestamp, sizeof(timestamp));
    authenticator_type = HW_AUTH_FINGERPRINT;
    swap_bytes((unsigned char*)&authenticator_type, sizeof(authenticator_type));
    token.version = HW_AUTH_TOKEN_VERSION;
    token.challenge = challenge;

    // It would crash when directly sending the pointer of token.user_id
    // So we pass a pointer of temporary variable to gpmngr_get_xxx()
    unsigned long long tmp_id;
    gpmngr_get_secure_group_id(&tmp_id);
    token.user_id = tmp_id;

    gpmngr_get_authenticator_id(&tmp_id);
    token.authenticator_id = tmp_id;

    token.authenticator_type = authenticator_type;
    token.timestamp = timestamp;
    authenticator_id = token.authenticator_id;

    retval = hmac_setup_platform_sec_key(NULL, 0, secure_key.key, &secure_key.len);
    if (retval != EGIS_OK) {
        *out_len = 0;
        return retval;
    }

    retval = hmac_sha256((unsigned char*)&token, sizeof(token) - sizeof(token.hmac), secure_key.key,
                         secure_key.len, token.hmac);
    if (retval != EGIS_OK) {
        *out_len = 0;
        return retval;
    }

    ex_log(LOG_DEBUG, "token.challenge: 0x%x 0x%x ", (unsigned int)(token.challenge >> 32),
           (unsigned int)token.challenge);
    ex_log(LOG_DEBUG, "token.user_id:0x%x 0x%x ", (unsigned int)(token.user_id >> 32),
           (unsigned int)token.user_id);
    ex_log(LOG_INFO, "token.authenticator_id: 0x%x 0x%x", (unsigned int)(authenticator_id >> 32),
           (unsigned int)authenticator_id);

    ex_log(LOG_DEBUG, "token.timestamp:0x%x 0x%x", (unsigned int)(token.timestamp >> 32),
           (unsigned int)token.timestamp);

    *out_len = sizeof(struct AospHwAuthToken);
    mem_move(out, &token, sizeof(struct AospHwAuthToken));

    return EGIS_OK;
}

int perms_check_aosp_hw_auth_token(unsigned char* token, unsigned int len) {
    int retval;
    struct AospHwAuthToken* auth_token;
    struct PlatSecureKey secure_key;
    unsigned char hmac[HMAC_SHA256_BYTES];
    unsigned long long token_timestamp = 0;

    if (token == NULL || len != sizeof(struct AospHwAuthToken)) {
        return EGIS_INCORRECT_PARAMETER;
    }
    secure_key.len = 0;

    auth_token = (struct AospHwAuthToken*)token;

    retval = hmac_setup_platform_sec_key(NULL, 0, secure_key.key, &secure_key.len);
    if (retval != EGIS_OK) {
        ex_log(LOG_ERROR, "%s no key", __func__);
        return retval;
    }

    retval = hmac_sha256((unsigned char*)auth_token,
                         sizeof(struct AospHwAuthToken) - sizeof(auth_token->hmac), secure_key.key,
                         secure_key.len, hmac);
    if (retval != EGIS_OK) {
        ex_log(LOG_ERROR, "%s sha failed", __func__);
        return retval;
    }

    if (0 != mem_compare(auth_token->hmac, hmac, HMAC_SHA256_BYTES)) {
        ex_log(LOG_ERROR, "%s hmac different", __func__);
        return EGIS_PERMISSION_DENIED;
    }

    token_timestamp = auth_token->timestamp;
    swap_bytes((unsigned char*)&token_timestamp, sizeof(unsigned long long));
    if (AOSP_AUTH_TOKEN_LIFETIME < plat_get_diff_time(token_timestamp)) {
        ex_log(LOG_ERROR, "%s diff time to long ", __func__);
        return EGIS_TIMEOUT;
    }

    return EGIS_OK;
}
