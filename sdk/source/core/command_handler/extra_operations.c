#include "extra_operations.h"
#include "algomodule.h"
#include "egis_definition.h"
#include "fp_algomodule.h"
#include "fp_sensormodule.h"
#include "object_def_image.h"
#include "plat_encryption.h"
#include "plat_log.h"
#include "plat_mem.h"
#ifdef __ET7XX__
#include "work_with_G2/ImageProcessingLib.h"
#endif
#include "AES.h"
#include "common_definition.h"
#include "core_config.h"
#include "group_manager.h"
#include "isensor_api.h"
#include "template_manager.h"
static int get_data(int type, unsigned char* in_data, int in_data_size, unsigned char* out_data,
                    int* out_data_size);
static int set_data(int type, unsigned char* data, int data_size);

extern struct ImageHolder g_img_holder;
extern fp_lib_identify_data_t g_identify_data;

static templ_info_t g_tmpl_info;

extern uint8_t g_aes_key[AES_KEY_LENGTH];

#ifdef __OTG_SENSOR__
#include "plat_otg.h"
extern event_callback_t g_event_callback;
#endif

#ifdef __TRUSTONIC__
extern struct GroupKeyData g_data;
extern uint32_t g_group_id;
extern int g_add_count;
#include "egis_algomodule.h"
#include "template_file.h"
extern template_file_data g_templ_file[SUPPORT_MAX_ENROLL_COUNT];
#define CRYPTION_CHECKHEADER_LEN 92
static unsigned char* g_encryp_data = NULL;
#ifdef ALGO_GEN_4
extern uint8_t* g_debase_db;
extern int g_debase_size;
#else
#include "bkimg_pool.h"
extern uint8_t* g_bkgimg_db;
extern int g_bkg_pool_size;

extern void* g_scratch_info;
extern unsigned int g_scratch_info_len;
extern unsigned int g_scratch_update_count;
#endif

#endif

int extra_operations(int cid, int command, unsigned char* indata, int indata_len,
                     unsigned char* out_data, int* out_data_size) {
    int retval = 0;
    switch (cid) {
        case EX_CMD_SET_DATA:
            retval = set_data(command, indata, indata_len);
            break;
        case EX_CMD_GET_DATA:
            retval = get_data(command, indata, indata_len, out_data, out_data_size);
            break;
        default:
            retval = 1;
            break;
    }
    return retval;
}

int get_data(int type, unsigned char* in_data, int in_data_size, unsigned char* out_data,
             int* out_data_size) {
    int retval = 0, image_size = 0, image_cnt = 0;
    int index = 0;
    unsigned int fid = 0;
    static unsigned char* pencry_data = NULL;
    static int encry_len = 0;
    int str_len = 0;
    switch (type) {
        case TYPE_RECEIVE_CALIBRATION_DATA:
#ifdef __TRUSTONIC__

            if (out_data == NULL || out_data_size == NULL || *out_data_size <= 0) {
                ex_log(LOG_ERROR, "get_data out_data/out_data_size incorrect");
                retval = EGIS_INCORRECT_PARAMETER;
                break;
            }

            retval = isensor_get_buffer(PARAM_BUF_GET_CALI_DATA, out_data, out_data_size);

            ex_log(LOG_DEBUG, "TYPE_RECEIVE_CALIBRATION_DATA end retval = %d", retval);
#else
            ex_log(LOG_ERROR, "get_data TYPE_RECEIVE_CALIBRATION_DATA not supported");
#endif
            break;
        case TYPE_RECEIVE_BDS_START: {
#ifdef ALGO_GEN_4
            ex_log(LOG_DEBUG, "Not support BDS");
#else
#if defined(__TRUSTONIC__) && defined(G3PLUS_MATCHER)
            if (g_bkg_pool_size <= 0 || g_bkgimg_db == NULL) {
                ex_log(LOG_DEBUG, "no bds data");
                retval = EGIS_COMMAND_FAIL;
                break;
            }

            if (g_add_count <= 0) {
                ex_log(LOG_DEBUG, "no need update data");
                retval = EGIS_NO_NEED_UPDATE;
                break;
            }

            encry_len = g_bkg_pool_size + bds_pool_header_get_size();

            if (encry_len > *out_data_size) {
                ex_log(LOG_ERROR, "TYPE_RECEIVE_BDS_START receive buf length is not enough");
                ((int*)out_data)[0] = encry_len;
                *out_data_size = sizeof(int);
                retval = EGIS_RECEIVE_BUF_LEN_IS_NOT_ENOUGH;
                break;
            }

            PLAT_FREE(pencry_data);
            pencry_data = plat_alloc(encry_len);
            if (pencry_data == NULL) {
                ex_log(LOG_ERROR, "TYPE_RECEIVE_BDS_START alloc failed");
                retval = EGIS_OUT_OF_MEMORY;
                break;
            }
            memcpy(pencry_data, (unsigned char*)g_bkgimg_db, encry_len);
            AESEncrypt(pencry_data, Key, encry_len, 32, NULL, ECB_MODE);
            ex_log(LOG_DEBUG, "TYPE_RECEIVE_BDS_START encry_len = %d,g_add_count %d", encry_len,
                   g_add_count);
#endif
#endif
        } break;
        case TYPE_RECEIVE_BDS: {
#ifdef ALGO_GEN_4
            ex_log(LOG_DEBUG, "Not support BDS");
#else
#if defined(__TRUSTONIC__) && defined(G3PLUS_MATCHER)
            int residual_length;
            int recive_pos = ((int*)in_data)[0];
            unsigned char* pRecivePos = NULL;
            int copy_size = 0;
            int totalsize = encry_len;
            ex_log(LOG_DEBUG, "TYPE_RECEIVE_BDS recive_pos %d,outsize %d,encry_len=%d", recive_pos,
                   out_data_size, encry_len);
            if (out_data == NULL || out_data_size == NULL || encry_len == 0) {
                ex_log(LOG_ERROR, "TYPE_RECEIVE_BDS EGIS_INCORRECT_PARAMETER");
                retval = EGIS_INCORRECT_PARAMETER;
                break;
            }

            if (recive_pos >= totalsize) {
                retval = EGIS_COMMAND_FAIL;
                break;
            }

            residual_length = totalsize - recive_pos;
            if (residual_length < *out_data_size) {
                *out_data_size = residual_length;
                g_add_count = 0;
                retval = EGIS_OK;
            } else {
                retval = EGIS_GET_SET_CONTINUE;
            }
            ex_log(LOG_DEBUG, "TYPE_RECEIVE_BDS residual len %d,revice pos %d,recive size %d,%d",
                   residual_length, recive_pos, out_data_size, g_add_count);

            mem_move(out_data, pencry_data + recive_pos, *out_data_size);
#endif
#endif
        } break;
        case TYPE_RECEIVE_BDS_END: {
#ifdef ALGO_GEN_4
            ex_log(LOG_DEBUG, "Not support BDS");
#else
            PLAT_FREE(pencry_data);
            encry_len = 0;
#endif
        } break;
        case TYPE_RECEIVE_USER_INFO: {
#ifdef __TRUSTONIC__
            ex_log(LOG_DEBUG, "TYPE_RECEIVE_USER_INFO enter");
            encry_len = sizeof(g_data) + CRYPTION_CHECKHEADER_LEN;
            if (out_data == NULL || out_data_size == NULL || *out_data_size < encry_len) {
                ex_log(LOG_ERROR, "get_data out_data/out_data_size incorrect %d", *out_data_size);
                retval = EGIS_INCORRECT_PARAMETER;
                break;
            }

            pencry_data = (unsigned char*)mem_alloc(encry_len);
            if (pencry_data == NULL) {
                retval = EGIS_COMMAND_FAIL;
                break;
            }

            // ex_log(LOG_DEBUG, "group_id = %d", g_data.group_id);
            ex_log(LOG_DEBUG, "secure_group_id = %d", g_data.secure_group_id);
            ex_log(LOG_DEBUG, "authenticator_id = %d", g_data.authenticator_id);

            retval =
                plat_encryption((unsigned char*)&g_data, sizeof(g_data), pencry_data, &encry_len);
            if (retval != EGIS_OK) {
                ex_log(LOG_ERROR, "plat_encryption failed");
                retval = EGIS_COMMAND_FAIL;
                goto exit2;
            }

            if (*out_data_size < encry_len) {
                ex_log(LOG_ERROR, "out data size not large enough");
                retval = EGIS_OUT_OF_MEMORY;
                goto exit2;
            }

            mem_move(out_data, pencry_data, encry_len);
            *out_data_size = encry_len;

        exit2:
            if (pencry_data != NULL) {
                mem_free(pencry_data);
                pencry_data = NULL;
            }
            ex_log(LOG_DEBUG, "TYPE_RECEIVE_USER_INFO leave");
            retval = EGIS_OK;
#endif
        } break;
        TYPE_RECEIVE_DEBASE : {
#if defined(__TRUSTONIC__) && defined(ALGO_GEN_4)
            unsigned char* pdata = NULL;
            unsigned int data_len;
            ex_log(LOG_DEBUG, "get_data enter type %d", type);

            if (out_data == NULL || out_data_size == NULL) {
                ex_log(LOG_ERROR, "get_data out_data/out_data_size incorrect %d", *out_data_size);
                retval = EGIS_INCORRECT_PARAMETER;
                break;
            }

            pdata = (unsigned char*)g_debase_db;
            data_len = g_debase_size;

            ex_log(LOG_DEBUG, "get_data enter data_len %d", data_len);
            encry_len = data_len + CRYPTION_CHECKHEADER_LEN;
            pencry_data = (unsigned char*)mem_alloc(encry_len);
            if (pencry_data == NULL) {
                retval = EGIS_COMMAND_FAIL;
                break;
            }

            retval = plat_encryption(pdata, data_len, pencry_data, &encry_len);
            if (retval != EGIS_OK) {
                ex_log(LOG_DEBUG, "plat_encryption failed");
                retval = EGIS_COMMAND_FAIL;
            } else {
                if (*out_data_size < encry_len) {
                    ex_log(LOG_DEBUG, "out data size not large enough");
                    retval = EGIS_OUT_OF_MEMORY;
                } else {
                    mem_move(out_data, pencry_data, encry_len);
                    *out_data_size = encry_len;
                    retval = EGIS_OK;
                }
            }
            PLAT_FREE(pencry_data);
            ex_log(LOG_DEBUG, "TYPE_RECEIVE_DEBASE leave,retval = %d", retval);
#endif
        } break;
        case TYPE_RECEIVE_SCRATCH: {
#if defined(__TRUSTONIC__) && defined(G3PLUS_MATCHER)
            unsigned char* pdata = NULL;
            unsigned int data_len;
            ex_log(LOG_DEBUG, "get_data enter type %d", type);

            if (out_data == NULL || out_data_size == NULL) {
                ex_log(LOG_ERROR, "get_data out_data/out_data_size incorrect %d", *out_data_size);
                retval = EGIS_INCORRECT_PARAMETER;
                break;
            }

            if (g_scratch_info_len <= 0 || g_scratch_update_count <= 0) {
                ex_log(LOG_DEBUG, "no need TYPE_RECEIVE_SCRATCH");
                return EGIS_NO_NEED_UPDATE;
            }
            pdata = (unsigned char*)g_scratch_info;
            data_len = g_scratch_info_len;

            ex_log(LOG_DEBUG, "get_data enter data_len %d", data_len);
            encry_len = data_len + CRYPTION_CHECKHEADER_LEN;
            pencry_data = (unsigned char*)mem_alloc(encry_len);
            if (pencry_data == NULL) {
                retval = EGIS_COMMAND_FAIL;
                break;
            }

            retval = plat_encryption(pdata, data_len, pencry_data, &encry_len);
            if (retval != EGIS_OK) {
                ex_log(LOG_ERROR, "plat_encryption failed");
                retval = EGIS_COMMAND_FAIL;
            } else {
                if (*out_data_size < encry_len) {
                    ex_log(LOG_ERROR, "out data size not large enough");
                    retval = EGIS_OUT_OF_MEMORY;
                } else {
                    mem_move(out_data, pencry_data, encry_len);
                    *out_data_size = encry_len;
                    retval = EGIS_OK;
                    g_scratch_update_count = 0;
                }
            }
            PLAT_FREE(pencry_data);
            ex_log(LOG_DEBUG, "TYPE_RECEIVE_SCRATCH leave,retval = %d", retval);
#endif
        } break;
        case TYPE_RECEIVE_TEMPLATE_START: {
#ifdef __TRUSTONIC__
            fid = ((int*)in_data)[0];

            PLAT_FREE(pencry_data);
            encry_len = 0;
            for (index = 0; index < SUPPORT_MAX_ENROLL_COUNT; index++) {
                ex_log(LOG_DEBUG, "TYPE_RECEIVE_TEMPLATE_START %d fingerid %d : %d ", index, fid,
                       g_templ_file[index].fingerprint_id);

                if (fid == g_templ_file[index].fingerprint_id) {
                    int temp_totalsize = encry_len =
                        g_templ_file[index].templ_data_size + TEMPLATE_FILE_DATA_HEADER;
                    if (*out_data_size < temp_totalsize) {
                        ex_log(LOG_ERROR,
                               "TYPE_RECEIVE_TEMPLATE_START receive buf length is not enough");
                        ((int*)out_data)[0] = encry_len;
                        *out_data_size = sizeof(int);
                        retval = EGIS_RECEIVE_BUF_LEN_IS_NOT_ENOUGH;
                        break;
                    }

                    pencry_data = plat_alloc(encry_len);
                    if (pencry_data == NULL) {
                        retval = EGIS_COMMAND_FAIL;
                        break;
                    }
                    mem_move(pencry_data, &g_templ_file[index], temp_totalsize);

                    AESEncrypt(pencry_data, g_aes_key, encry_len, 32, NULL, ECB_MODE);

                    break;
                }
            }
            if (pencry_data == NULL) retval = EGIS_COMMAND_FAIL;
#endif
        } break;
        case TYPE_RECEIVE_TEMPLATE: {
#ifdef __TRUSTONIC__
            int residual_length;
            int recive_pos = ((int*)in_data)[1];
            unsigned char* pRecivePos;
            int copy_size = 0;
            fid = ((int*)in_data)[0];
            ex_log(LOG_DEBUG,
                   "TYPE_RECEIVE_TEMPLATE recive_pos %d ,fid %d ,outsize %d,encry_len=%d",
                   recive_pos, fid, out_data_size, encry_len);
            if (out_data == NULL || out_data_size == NULL || fid == 0 || encry_len == 0) {
                ex_log(LOG_ERROR, "get_data EGIS_INCORRECT_PARAMETER");
                retval = EGIS_INCORRECT_PARAMETER;
                break;
            }

            int temp_totalsize = encry_len;
            if (recive_pos >= temp_totalsize) {
                retval = EGIS_COMMAND_FAIL;
                break;
            }

            residual_length = temp_totalsize - recive_pos;
            if (residual_length < *out_data_size) {
                *out_data_size = residual_length;
                retval = EGIS_OK;
            } else {
                retval = EGIS_GET_SET_CONTINUE;
            }
            ex_log(LOG_DEBUG, "fid template len %d,residual len %d,revice pos %d ,recive size %d",
                   temp_totalsize, residual_length, recive_pos, out_data_size);
            mem_move(out_data, pencry_data + recive_pos, *out_data_size);
#endif
        } break;
        case TYPE_RECEIVE_TEMPLATE_END: {
            PLAT_FREE(pencry_data);
            encry_len = 0;
        } break;
#ifdef EGIS_DBG
        case TYPE_RECEIVE_MULTIPLE_IMAGE: {
            if (in_data == NULL || in_data_size < sizeof(receive_images_in_t)) {
                ex_log(LOG_ERROR, "in_data = %p, in_data_size = %u", in_data, in_data_size);
                retval = EGIS_INCORRECT_PARAMETER;
                break;
            }

            receive_images_in_t* in_buffer = (receive_images_in_t*)in_data;
            receive_images_out_t* out_buffer = (receive_images_out_t*)out_data;
            unsigned char* image_buf = out_data + sizeof(receive_images_out_t);
            BOOL bRaw = FALSE;
            int iRaw_len = 0;
            BOOL transfer_bad = FALSE;

            image_size = g_img_holder.fingerprint_image.img_data.format.width *
                         g_img_holder.fingerprint_image.img_data.format.height;
            ex_log(LOG_DEBUG, "get_data TYPE_RECEIVE_MULTIPLE_IMAGE size=%d, count=%d", image_size,
                   g_img_holder.fingerprint_image.img_data.frame_count);

            if (*out_data_size < image_size * in_buffer->image_count_request) {
                ex_log(LOG_ERROR, "out buffer size %d is not enough", *out_data_size);
                retval = EGIS_INCORRECT_PARAMETER;
                break;
            }

            int enroll_method =
                core_config_get_int(INI_SECTION_ENROLL, KEY_ENROLL_METHOD, ENROLL_METHOD_TOUCH);
            out_buffer->image_type = in_buffer->image_type;
            out_buffer->format.width = g_img_holder.fingerprint_image.img_data.format.width;
            out_buffer->format.height = g_img_holder.fingerprint_image.img_data.format.height;
            out_buffer->image_count_included = 0;
            out_buffer->format.bpp = 8;

            ex_log(LOG_DEBUG,
                   "transfer image type : %d, start index : %d, image_count_request : %d, "
                   "reset_mode : %d",
                   in_buffer->image_type, in_buffer->image_index_start,
                   in_buffer->image_count_request, in_buffer->reset_mode);

            index = in_buffer->image_index_start;

#ifdef SUPPORT_KEEP_RAW_16BIT
            bRaw = (in_buffer->image_type == TRANSFER_VERIFY_RAW ||
                    in_buffer->image_type == TRANSFER_ENROLL_RAW)
                       ? TRUE
                       : FALSE;
            if (bRaw) {
                egis_image_get_size(&g_img_holder.fingerprint_image, IMGTYPE_RAW,
                                    &out_buffer->format.width, &out_buffer->format.height,
                                    &out_buffer->format.bpp);
                iRaw_len = egis_image_get_buffer_size(&g_img_holder.fingerprint_image, IMGTYPE_RAW);
            }
            if (bRaw && *out_data_size < iRaw_len * in_buffer->image_count_request) {
                ex_log(LOG_ERROR, "RAW out buffer size %d is not enough", *out_data_size);
                retval = EGIS_INCORRECT_PARAMETER;
                break;
            }
#endif
            BOOL for_verify = (in_buffer->image_type == TRANSFER_VERIFY_IMAGE_V2 ||
                               in_buffer->image_type == TRANSFER_VERIFY_RAW);
            BOOL is_bad = (g_img_holder.fingerprint_image.quality.reject_reason ==
                               FP_LIB_ENROLL_FAIL_LOW_QUALITY ||
                           g_img_holder.fingerprint_image.quality.reject_reason ==
                               FP_LIB_ENROLL_FAIL_LOW_QUALITY_AND_LOW_COVERAGE ||
                           g_img_holder.fingerprint_image.quality.reject_reason ==
                               FP_LIB_ENROLL_FAIL_SPOOF_FINGER);
            if (for_verify && is_bad) {
                unsigned char* saved_bad_img = egis_image_get_bad_pointer(
                    &g_img_holder.fingerprint_image, bRaw ? IMGTYPE_RAW : IMGTYPE_BIN);
                if (saved_bad_img != NULL) {
                    memcpy(image_buf + out_buffer->image_count_included * image_size, saved_bad_img,
                           bRaw ? iRaw_len : image_size);
                    out_buffer->image_count_included++;
                } else {
                    ex_log(LOG_ERROR, "no bad image pointer to keep");
                }
                out_buffer->identify_info.save_index = 0;
                transfer_bad = TRUE;
                ex_log(LOG_DEBUG, "transfer bad %d",
                       g_img_holder.fingerprint_image.quality.reject_reason);
            } else {
                for (; index < g_img_holder.fingerprint_image.img_data.frame_count; index++) {
                    if (EGIS_IMAGE_get_algo_flag(&g_img_holder.fingerprint_image, index) ==
                        ALGO_FLAG_KEEP) {
                        if (bRaw) {
                            memcpy(
                                image_buf + out_buffer->image_count_included * iRaw_len,
                                egis_image_get_pointer_raw(&g_img_holder.fingerprint_image, index),
                                iRaw_len);
                        } else {
                            memcpy(image_buf + out_buffer->image_count_included *
                                                   out_buffer->format.width *
                                                   out_buffer->format.height,
                                   egis_image_get_pointer(&g_img_holder.fingerprint_image, index),
                                   out_buffer->format.width * out_buffer->format.height);
                        }
                        if (in_buffer->image_type == TRANSFER_VERIFY_IMAGE_V2) {
                            out_buffer->identify_info.try_count = g_identify_data.try_match_count;
                            out_buffer->identify_info.match_score = g_identify_data.score;

                            if (index == g_identify_data.last_tried_index) {
                                out_buffer->identify_info.save_index =
                                    out_buffer->image_count_included;
                            }
                        }

                        out_buffer->image_count_included++;

                        if (out_buffer->image_count_included == in_buffer->image_count_request) {
                            index++;
                            break;
                        }
                    }
                }
            }

            out_buffer->image_index_end = index;
            if (index >= g_img_holder.fingerprint_image.img_data.frame_count || transfer_bad) {
                out_buffer->has_more_image = TRANSFER_MORE_NONE;
#ifdef SUPPORT_KEEP_RAW_16BIT
                if (!bRaw) {
                    ex_log(LOG_DEBUG, "has RAW image to transfer");
                    out_buffer->has_more_image = TRANSFER_MORE_NEXT_RAW;
                }
#endif
            } else {
                out_buffer->has_more_image = TRANSFER_MORE_TRUE;
            }

            // Jack: No need to set *out_data_size
            // *out_data_size = out_buffer->format.width * out_buffer->format.height *
            // out_buffer->image_count_included + sizeof(receive_images_out_t);
            if (in_buffer->reset_mode == FRAMES_RESET_ALWAYS ||
                (in_buffer->reset_mode == FRAMES_RESET_AUTO &&
                 out_buffer->has_more_image == TRANSFER_MORE_NONE)) {
                ex_log(LOG_DEBUG, "Reset g_img_holder.fingerprint_image");
                EGIS_IMAGE_RESET_FRAME_COUNT(&g_img_holder.fingerprint_image);
                out_buffer->has_more_image = TRANSFER_MORE_NONE;
            }

            ex_log(LOG_DEBUG,
                   "out : image_index_end : %d, image_count_included : %d, has_more_image : %d",
                   out_buffer->image_index_end, out_buffer->image_count_included,
                   out_buffer->has_more_image);

            retval = EGIS_OK;
        } break;
#if defined(__ET7XX__)
        case TYPE_RECEIVE_IMAGE_OBJECT_ARRAY_SIZE:{
            if(out_data == NULL || *out_data_size < sizeof(uint16_t)){
                retval = EGIS_INCORRECT_PARAMETER;
                break;
            }
            memcpy(out_data, &g_img_holder.fingerprint_image.img_data.frame_count, sizeof(uint16_t));
            retval = EGIS_OK;
            break;
        }
        case TYPE_RECEIVE_IMAGE_OBJECT_ARRAY: {
            if (in_data == NULL || in_data_size < sizeof(rbs_obj_array_request_t)) {
                ex_log(LOG_ERROR, "in_data = %p, in_data_size = %u", in_data, in_data_size);
                retval = EGIS_INCORRECT_PARAMETER;
                break;
            }
            rbs_obj_array_request_t* array_req = (rbs_obj_array_request_t*)in_data;
            int img_type = array_req->param1;
            BOOL is_bad =
                (g_img_holder.fingerprint_image.quality.reject_reason != FP_LIB_ENROLL_SUCCESS &&
                 g_img_holder.fingerprint_image.quality.reject_reason != FP_LIB_ENROLL_FAIL_NONE);
            int width, height, bpp;
            egis_image_get_size(&g_img_holder.fingerprint_image, img_type, &width, &height, &bpp);

            int frame_count = g_img_holder.fingerprint_image.img_data.frame_count;

            int idx_start = array_req->index_start;
            ex_log(LOG_DEBUG, "RECEIVE_IMAGE_OBJECT_ARRAY [%d] index_start=%d, count=%d", img_type,
                   idx_start, array_req->count);
            ex_log(LOG_DEBUG, "RECEIVE_IMAGE_OBJECT_ARRAY is_bad=%d, frame_count=%d", is_bad,
                   frame_count);

            rbs_obj_array_t* obj_array_out = (rbs_obj_array_t*)out_data;
            RBSOBJ_init_OBJARRAY_v1_0(obj_array_out, idx_start, 1);
            if (idx_start >= frame_count && !is_bad) {
                RBSOBJ_init_OBJARRAY_v1_0(obj_array_out, idx_start, 0);
                ex_log(LOG_ERROR, "index_start is wrong or no image");
                break;
            }
            rbs_obj_image_v1_0_t* img_obj =
                (rbs_obj_image_v1_0_t*)RBSOBJ_get_payload_pointer(obj_array_out);
            RBSOBJ_set_IMAGE_v1_0(img_obj, img_type, width, height, bpp);
#ifdef EGIS_IMAGE_V2
            rbs_obj_image_v1_0_t* src_img_obj = egis_image_copy_params(idx_start, img_type);
            RBSOBJ_copy_IMAGE_params(src_img_obj, img_obj);
#endif
            RBSOBJ_set_objarray_payload_size(obj_array_out, img_obj);
            uint8_t* img_buf = RBSOBJ_get_payload_pointer(img_obj);
            uint32_t img_size = RBSOBJ_get_payload_size(img_obj);
            uint8_t* src_img = NULL;
            switch (img_type) {
                case IMGTYPE_BIN:
                    ex_log(LOG_DEBUG, "egis_image_get_pointer [%d]", idx_start);
                    src_img = egis_image_get_pointer(&g_img_holder.fingerprint_image, idx_start);
                    break;
                case IMGTYPE_RAW:
                    ex_log(LOG_DEBUG, "egis_image_get_pointer_raw [%d]", idx_start);
                    src_img =
                        egis_image_get_pointer_raw(&g_img_holder.fingerprint_image, idx_start);
                    break;
                case IMGTYPE_BKG:
                    ex_log(LOG_DEBUG, "egis_image_get_bkg_pointer_raw [%d]", idx_start);
                    src_img =
                        egis_image_get_bkg_pointer_raw(&g_img_holder.fingerprint_image, idx_start);
                    break;
            }
            if (src_img == NULL) {
                ex_log(LOG_ERROR, "src_img is NULL");
                RBSOBJ_init_OBJARRAY_v1_0(obj_array_out, idx_start, 0);
                obj_array_out->has_more_object = TRANSFER_MORE_NONE;
                break;
            }
            memcpy(img_buf, src_img, img_size);
            if (idx_start + 1 >= frame_count) {
                obj_array_out->has_more_object = TRANSFER_MORE_NONE;
            } else {
                obj_array_out->has_more_object = TRANSFER_MORE_TRUE;
            }
            break;
        }
#endif  // SUPPORT_KEEP_RAW_16BIT

#endif
        case TYPE_TEST_TRANSPORTER: {
            int test_type, test_step, test_data_size;
            int in_checksum = 0, out_checksum = 0;
            transporter_test_in_head_t* test_in_header;
            transporter_test_out_head_t* test_out_header;
            unsigned short *test_in_data, *test_out_data;

            if (in_data == NULL || in_data_size <= 0 || out_data == NULL || out_data_size == NULL) {
                retval = EGIS_INCORRECT_PARAMETER;
                break;
            }

            test_in_header = (transporter_test_in_head_t*)in_data;
            test_out_header = (transporter_test_out_head_t*)out_data;
            test_in_data = (unsigned short*)((unsigned char*)test_in_header +
                                             sizeof(transporter_test_in_head_t));
            test_out_data = (unsigned short*)((unsigned char*)test_out_header +
                                              sizeof(transporter_test_out_head_t));

            test_type = test_in_header->type;
            test_step = test_in_header->step;
            test_data_size = test_in_header->in_data_size;

            ex_log(LOG_DEBUG, "test_in_header = %p, test_in_data = %p", test_in_header,
                   test_in_data);
            ex_log(LOG_DEBUG, "TYPE_TEST_TRANSPORTER test type = %d, test_data_size = %d",
                   test_type, test_data_size);

            test_out_header->out_data_size = test_data_size;
            test_out_header->type = test_type;

            if (test_type == DUPLICATE_TEST) {
                memcpy(test_out_data, test_in_data, test_data_size);
                for (index = 0; index < test_data_size / ((int)sizeof(short)); index++) {
                    in_checksum += test_in_data[index];
                }
            } else if (test_type == INCREMENTAL_TEST) {
                for (index = 0; index < test_data_size / ((int)sizeof(short)); index++) {
                    test_out_data[index] = test_in_data[0] + index * test_step;
                }
                in_checksum = test_in_data[0];
            }

            for (index = 0; index < test_data_size / ((int)sizeof(short)); index++) {
                out_checksum += test_out_data[index];
            }

            test_out_header->in_data_checksum = in_checksum;
            test_out_header->out_data_checksum = out_checksum;

            ex_log(LOG_DEBUG, "TYPE_TEST_TRANSPORTER in checksum = %d, out checksum = %d",
                   in_checksum, out_checksum);

            *out_data_size = test_data_size + sizeof(transporter_test_out_head_t);
            retval = EGIS_OK;
        } break;
#ifdef EGIS_DBG
        case TYPE_RECEIVE_LIVE_IMAGE: {
            receive_images_out_t* out_buffer = (receive_images_out_t*)out_data;
            unsigned char* image_buf = out_data + sizeof(receive_images_out_t);

            out_buffer->format.width = g_img_holder.fingerprint_image.img_data.format.width;
            out_buffer->format.height = g_img_holder.fingerprint_image.img_data.format.height;
            out_buffer->image_count_included = 0;

            image_size = g_img_holder.fingerprint_image.img_data.format.width *
                         g_img_holder.fingerprint_image.img_data.format.height;
            ex_log(LOG_DEBUG, "get_data TYPE_RECEIVE_LIVE_IMAGE size=%d, count=%d", image_size,
                   g_img_holder.fingerprint_image.img_data.frame_count);

            index = g_img_holder.fingerprint_image.img_data.frame_count - 1;
            if (index < 0) {
                index = 0;
            }

            if ((g_img_holder.fingerprint_image.quality.reject_reason ==
                     FP_LIB_ENROLL_FAIL_LOW_QUALITY ||
                 g_img_holder.fingerprint_image.quality.reject_reason ==
                     FP_LIB_ENROLL_FAIL_LOW_QUALITY_AND_LOW_COVERAGE)) {
                memcpy(image_buf,
                       egis_image_get_bad_pointer(&g_img_holder.fingerprint_image, IMGTYPE_BIN),
                       out_buffer->format.width * out_buffer->format.height);
                out_buffer->image_count_included++;
            } else {
                memcpy(image_buf, egis_image_get_pointer(&g_img_holder.fingerprint_image, index),
                       out_buffer->format.width * out_buffer->format.height);
                out_buffer->image_count_included++;
            }

            *out_data_size = out_buffer->format.width * out_buffer->format.height *
                                 out_buffer->image_count_included +
                             sizeof(receive_images_out_t);
            retval = EGIS_OK;

        } break;
#endif
        case TYPE_RECEIVE_ALGO_VERSION:
            ex_log(LOG_DEBUG, "GET ALGMODULE VERSION ");
            if (out_data == NULL) {
                retval = EGIS_INCORRECT_PARAMETER;
                break;
            }
            fp_getAlgVersion(out_data, *out_data_size);
            break;

#ifdef __ET7XX__
        case TYPE_RECEIVE_IP_VERSION:
            ex_log(LOG_DEBUG, "GET IP VERSION ");
            if (out_data == NULL) {
                retval = EGIS_INCORRECT_PARAMETER;
                break;
            }
            get_ipp_lib_version(out_data);
            break;
#endif
        case TYPE_RECEIVE_TA_VERSION:
            ex_log(LOG_DEBUG, "GET TA VERSION ");
            if (out_data == NULL || out_data_size == NULL) {
                retval = EGIS_INCORRECT_PARAMETER;
                break;
            }

            str_len = sizeof("UNKNOWN");
            if (str_len <= *out_data_size) {
                *out_data_size = str_len;
                memcpy(out_data, "UNKNOWN", str_len);
            }
            break;

        case TYPE_RECEIVE_SENSOR_BUF: {
            int* param = (int*)in_data;
            ex_log(LOG_DEBUG, "TYPE_RECEIVE_SENSOR_BUF %d", *param);
            isensor_get_buffer(*param, out_data, out_data_size);
            break;
        }
        case TYPE_EVTOOL_RECEIVE_MATCH_SCORE:
            memcpy(out_data, &g_identify_data.score, sizeof(g_identify_data.score));
            *out_data_size = sizeof(g_identify_data.score);
            break;
        case TYPE_RECEIVE_SENSOR_INT_PARAM: {
            int* param = (int*)in_data;
            ex_log(LOG_DEBUG, "TYPE_RECEIVE_SENSOR_INT_PARAM %d", *param);
            isensor_get_int(*param, (int*)out_data);
            break;
        }
        default:
            ex_log(LOG_ERROR, "unknown type %d", type);
            break;
    }

    return retval;
}

int set_data(int type, unsigned char* data, int data_size) {
    int retval = 0;
    static int template_pos = 0;
    static unsigned char* ptemplate = NULL;
    static int template_len = 0;
    unsigned int fid = 0;

    ex_log(LOG_INFO, "ex_set_data type %d", type);
    switch (type) {
        case TYPE_SEND_CALIBRATION_DATA: {
#ifdef __TRUSTONIC__
            ex_log(LOG_DEBUG, "set_data TYPE_SEND_CALIBRATION_DATA supported");
            retval = isensor_set_buffer(PARAM_BUF_SET_CALI_DATA, data, data_size);
            ex_log(LOG_DEBUG, "TYPE_SEND_CALIBRATION_DATA retval =%d , %d", retval, data_size);
#else
            ex_log(LOG_ERROR, "set_data TYPE_SEND_CALIBRATION_DATA not supported");
#endif

        } break;
        case TYPE_SEND_SCRATCH: {
#if defined(__TRUSTONIC__) && defined(G3PLUS_MATCHER)
            unsigned char* pdecryption_data = NULL;
            int decryption_data_len = data_size;

            ex_log(LOG_DEBUG, "TYPE_SEND_SCRATCH enter");
            if (data == NULL || data_size <= 0) {
                ex_log(LOG_ERROR, "set_data data/data_size incorrect %d", data_size);
                retval = EGIS_INCORRECT_PARAMETER;
                break;
            }

            pdecryption_data = (unsigned char*)mem_alloc(decryption_data_len);
            if (pdecryption_data == NULL) {
                retval = EGIS_COMMAND_FAIL;
            }

            retval = plat_decryption(data, data_size, pdecryption_data, &decryption_data_len);
            if (retval == EGIS_OK) {
                ex_log(LOG_DEBUG, "decrypt user info success");

                if (g_scratch_info == NULL) g_scratch_info = plat_alloc(decryption_data_len);

                memcpy(g_scratch_info, pdecryption_data, decryption_data_len);
                g_scratch_info_len = decryption_data_len;
            }
            ex_log(LOG_DEBUG, "TYPE_SEND_SCRATCH leave %d", retval);
            PLAT_FREE(pdecryption_data);
#endif
        } break;
        case TYPE_SEND_DEBASE: {
#if defined(__TRUSTONIC__) && defined(ALGO_GEN_4)
            unsigned char* pdecryption_data = NULL;
            int decryption_data_len = data_size;

            ex_log(LOG_DEBUG, "TYPE_SEND_DEBASE enter, data_size = %d,g_debase_size = %d",
                   data_size, g_debase_size);
            if (data == NULL || data_size <= 0) {
                ex_log(LOG_ERROR, "set_data data/data_size incorrect %d", data_size);
                retval = EGIS_INCORRECT_PARAMETER;
                break;
            }

            pdecryption_data = (unsigned char*)mem_alloc(decryption_data_len);
            if (pdecryption_data == NULL) {
                retval = EGIS_COMMAND_FAIL;
                break;
            }

            retval = plat_decryption(data, data_size, pdecryption_data, &decryption_data_len);
            if (retval == EGIS_OK) {
                ex_log(LOG_DEBUG, "decrypt debase info success");

                if (g_debase_db == NULL) g_debase_db = plat_alloc(decryption_data_len);

                g_debase_size = decryption_data_len;
                memcpy(g_debase_db, pdecryption_data, decryption_data_len);
            }
            ex_log(LOG_DEBUG, "TYPE_SEND_DEBASE leave %d, decryption_data_len = %d", retval,
                   decryption_data_len);
            PLAT_FREE(pdecryption_data);
#endif
        } break;
        case TYPE_SEND_BDS_START: {
#ifdef ALGO_GEN_4
            ex_log(LOG_DEBUG, "Not support BDS");
#else
            if (data == NULL || data_size <= 0) {
                retval = EGIS_INCORRECT_PARAMETER;
                break;
            }
            template_len = *(unsigned int*)data;
            template_pos = 0;
            if (ptemplate) PLAT_FREE(ptemplate);

            if (template_len > 0) {
                ptemplate = (unsigned char*)mem_alloc(template_len);
            } else {
                retval = EGIS_COMMAND_FAIL;
            }

            ex_log(LOG_DEBUG, "TYPE_SEND_BDS_START template_len %d", template_len);
#endif
        } break;
        case TYPE_SEND_BDS: {
#ifdef ALGO_GEN_4
            ex_log(LOG_DEBUG, "Not support BDS");
#else
#ifdef __TRUSTONIC__
            unsigned char* pbuffer_pos = ptemplate + template_pos;
            if (data == NULL || data_size <= 0 || ptemplate == NULL || template_len <= 0) {
                ex_log(LOG_ERROR, "TYPE_SEND_BDS set_data data/data_size incorrect");
                retval = EGIS_INCORRECT_PARAMETER;
                break;
            }

            template_pos += data_size;
            ex_log(LOG_DEBUG, "TYPE_SEND_BDS data_size = %d,template_pos = %d, template_len = %d",
                   data_size, template_pos, template_len);
            if (template_len < template_pos) {
                ex_log(LOG_ERROR, "TYPE_SEND_BDS EGIS_OUT_OF_MEMORY");
                retval = EGIS_OUT_OF_MEMORY;
                break;
            }
            mem_move(pbuffer_pos, data, data_size);
#endif
#endif
        } break;
        case TYPE_SEND_BDS_END: {
#ifdef ALGO_GEN_4
            ex_log(LOG_DEBUG, "Not support BDS");
#else
            ex_log(LOG_DEBUG,
                   "TYPE_SEND_BDS_END,template_pos = %d,template_len = %d, data_size = %d",
                   template_pos, template_len, data_size);
#if defined(__TRUSTONIC__) && defined(G3PLUS_MATCHER)
            if (template_pos == template_len && template_len > 0) {
                unsigned char* pdecryption_data = NULL;
                int decryption_data_len = template_len;
                pdecryption_data = (unsigned char*)mem_alloc(decryption_data_len);
                if (pdecryption_data == NULL) {
                    retval = EGIS_COMMAND_FAIL;
                    break;
                }

                mem_move(pdecryption_data, ptemplate, decryption_data_len);
                AESDecrypt(pdecryption_data, g_aes_key, decryption_data_len, 32, NULL, ECB_MODE);

                if (g_bkgimg_db == NULL) g_bkgimg_db = plat_alloc(decryption_data_len);

                g_bkg_pool_size = decryption_data_len - bds_pool_header_get_size();
                memcpy(g_bkgimg_db, pdecryption_data, decryption_data_len);

                ex_log(LOG_DEBUG, "TYPE_SEND_BDS_END end");

                PLAT_FREE(pdecryption_data);
            }
            PLAT_FREE(ptemplate);
#endif
#endif
        } break;
        case TYPE_SEND_TEMPLATE_START: {
            if (data == NULL || data_size <= 0) {
                retval = EGIS_INCORRECT_PARAMETER;
                break;
            }
            g_tmpl_info.fingerid = 0;
            template_len = *(unsigned int*)data;
            template_pos = 0;

            PLAT_FREE(ptemplate);

            if (template_len > 0) {
                ptemplate = (unsigned char*)mem_alloc(template_len);
            } else {
                retval = EGIS_COMMAND_FAIL;
            }

            ex_log(LOG_DEBUG, "TYPE_SEND_TEMPLATE_START template_len %d", template_len);
        } break;
        case TYPE_SEND_TEMPLATE: {
#ifdef __TRUSTONIC__
            unsigned char* pbuffer_pos;
            if (data == NULL || data_size <= 0 || ptemplate == NULL || template_len <= 0) {
                ex_log(LOG_ERROR, "set_data data/data_size incorrect");
                retval = EGIS_INCORRECT_PARAMETER;
                break;
            }

            pbuffer_pos = ptemplate + template_pos;
            template_pos += data_size;
            if (template_len < template_pos) {
                ex_log(LOG_ERROR, "EGIS_OUT_OF_MEMORY template_len %d,template_pos %d ,%d",
                       template_len, template_pos, data_size);
                retval = EGIS_OUT_OF_MEMORY;
                break;
            }
            mem_move(pbuffer_pos, data, data_size);

            ex_log(LOG_DEBUG, "TYPE_SEND_TEMPLATE get  %d data_size %d ,template pos %d ", type,
                   template_pos, data_size);
#endif
        } break;
        case TYPE_SEND_TEMPLATE_END: {
#ifdef __TRUSTONIC__
            if (template_pos == template_len && template_len > 0) {
                unsigned char* pdecryption_data = NULL;
                int decryption_data_len = template_len;
                pdecryption_data = (unsigned char*)mem_alloc(decryption_data_len);
                if (pdecryption_data == NULL) {
                    retval = EGIS_COMMAND_FAIL;
                }

                // retval = plat_decryption(ptemplate, template_len, pdecryption_data,
                // &decryption_data_len);
                mem_move(pdecryption_data, ptemplate, decryption_data_len);
                AESDecrypt(pdecryption_data, g_aes_key, decryption_data_len, 32, NULL, ECB_MODE);

                template_file_data* ptr = (template_file_data*)pdecryption_data;

                fid = ptr->fingerprint_id;

                ex_log(LOG_DEBUG, "TYPE_SEND_TEMPLATE_END (0x%X)(0x%X) fid %d, template_len=%d ",
                       ptr->version, TEMPL_VER_CURRENT, fid, template_len);

                if (ptr->version == TEMPL_VER_CURRENT) {
                    retval = template_add(fid, ptr->templ_data, ptr->templ_data_size);
                }

#ifdef TEMPLATE_UPGRADE_FROM_SAVED_IMAGE
                if (ptr->version != TEMPL_VER_CURRENT) {
                    ex_log(LOG_ERROR, "%s, upgrade %d -> %d", __func__, ptr->version,
                           TEMPL_VER_CURRENT);
                    retval = EGIS_TEMPL_NEED_UPGRADED;
                    g_tmpl_info.fingerid = fid;
                    g_tmpl_info.max_enroll_count = 15;
                }
#endif
                PLAT_FREE(pdecryption_data);
            } else {
                ex_log(LOG_DEBUG, "TYPE_SEND_TEMPLATE_END fid  %d data_size %d ", fid, data_size);
            }

            PLAT_FREE(ptemplate);
#endif
        } break;
        case TYPE_SEND_USER_INFO: {
#ifdef __TRUSTONIC__
            unsigned char* pdecryption_data = NULL;
            int decryption_data_len = sizeof(g_data) + CRYPTION_CHECKHEADER_LEN;

            ex_log(LOG_DEBUG, "TYPE_SEND_USER_INFO enter");
            if (data == NULL || data_size < decryption_data_len) {
                ex_log(LOG_ERROR, "set_data data/data_size incorrect %d", data_size);
                retval = EGIS_INCORRECT_PARAMETER;
                break;
            }

            pdecryption_data = (unsigned char*)mem_alloc(decryption_data_len);
            if (pdecryption_data == NULL) {
                retval = EGIS_COMMAND_FAIL;
            }

            retval = plat_decryption(data, data_size, pdecryption_data, &decryption_data_len);
            if (retval == EGIS_OK) {
                ex_log(LOG_DEBUG, "decrypt user info success");
                memcpy(&g_data, pdecryption_data, sizeof(g_data));
                g_group_id = g_data.group_id;
                ex_log(LOG_DEBUG, "group_id = %d", g_data.group_id);
                ex_log(LOG_DEBUG, "secure_group_id = %d", g_data.secure_group_id);
                ex_log(LOG_DEBUG, "authenticator_id = %d", g_data.authenticator_id);
            }
            ex_log(LOG_DEBUG, "TYPE_SEND_USER_INFO leave %d", retval);
            PLAT_FREE(pdecryption_data);
#endif
        } break;
        case TYPE_DELETE_TEMPLATE: {
            retval = template_delete_all(TRUE);
        } break;

        case TYPE_SEND_INI_CONFIG: {
            retval = core_config_create(CONFIG_BUF_TYPE_INI, data, data_size);
            fp_updateConfig();
        } break;

        case TYPE_DESTROY_INI_CONFIG: {
            core_config_destroy();
        } break;

        case TYPE_SEND_CALLBACK_FUNCTION:
#ifdef __OTG_SENSOR__
            g_event_callback = ((event_callback_t*)data)[0];
            ex_log(LOG_INFO, "Callback function = %p", g_event_callback);
#endif
            break;

        case TYPE_SEND_HOST_DEVICE_INFO: {
            host_device_info_t* p_info = (host_device_info_t*)data;
            isensor_set_int(PARAM_INT_TEMPERATUREX10, p_info->temperature_x10);
            ex_log(LOG_DEBUG, "host_device_info temperture %d", p_info->temperature_x10);
            break;
        }
        case TYPE_SEND_RESET_IMAGE_FRAME_COUNT:
            ex_log(LOG_DEBUG, "OBJECT_ARRAY reset frame_count");
            EGIS_IMAGE_RESET_FRAME_COUNT(&g_img_holder.fingerprint_image);
            break;
        default:
            ex_log(LOG_ERROR, "unknown type %d", type);
            break;
    }

    return retval;
}
