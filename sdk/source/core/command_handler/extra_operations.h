#ifndef __EXTRA_OPERATIONS_H__
#define __EXTRA_OPERATIONS_H__
int extra_operations(int cid, int command, unsigned char* indata, int indata_len,
                     unsigned char* out_data, int* out_data_size);
#endif