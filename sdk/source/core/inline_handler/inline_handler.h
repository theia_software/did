#ifndef _INLINE_HANDLER_H
#define _INLINE_HANDLER_H

int inline_handler(unsigned char* msg_data, int msg_size, unsigned char* out_data,
                   int* out_data_size);

#endif