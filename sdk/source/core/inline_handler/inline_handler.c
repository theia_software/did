#include <isensor_definition.h>
#include "core_config.h"
#include "common_definition.h"
#include "egis_fp_get_image.h"
#include "egis_sensor_test.h"
#include "egis_sensormodule.h"
#include "fd_process.h"
#include "fp_algomodule.h"
#include "fp_definition.h"
#include "fp_sensormodule.h"
#include "image_analysis.h"
#include "isensor_api.h"
#include "liver_image.h"
#include "plat_log.h"
#include "plat_mem.h"
#include "plat_spi.h"
#if defined(__ET7XX__) && !defined(__ET0XX__)
#include "rbs_7xx_inline_handler.h"
#endif
#define SENSOR_NEED_RESET 21
#define LOG_TAG "RBS-inline"
#define GET_IMAGE_COUNT 1
#define GET_IMAGE_TYPE 0
int g_img_width = 0;
int g_img_height = 0;
BOOL g_enable_crop_img = FALSE;

struct TransferPackage {
    uint32_t process;
    uint32_t command;
    uint32_t args1;
    uint32_t args2;
    uint32_t indata_len;
    uint8_t indata[0];
};

static void setup_liver_image_out_header(liver_image_out_header_t* pheader, int width, int height,
                                         int live_image_type, int img_bpp, int raw_bpp, int cx, int cy) {
    pheader->live_image_header_ver = LIVE_IMG_VER;
    pheader->live_image_type = live_image_type;
    pheader->framecount = 1;
    pheader->img_width = width;
    pheader->img_height = height;
    pheader->raw_width = width;
    pheader->raw_height = height;
    pheader->raw_bpp = raw_bpp;
#ifdef LIVE_IMAGE_V2
    pheader->img_bpp = img_bpp;
    pheader->reserved_1 = cx;
    pheader->reserved_2 = cy;
#endif
}

static uint8_t* get_liver_image_out_img_pointer(liver_image_out_header_t* pheader) {
    uint8_t* ret_pointer = (uint8_t*)pheader;
    return ret_pointer + sizeof(liver_image_out_header_t);
}

static uint8_t* get_liver_image_out_raw_pointer(liver_image_out_header_t* pheader) {
    uint8_t* ret_pointer = (uint8_t*)pheader;
    ret_pointer += sizeof(liver_image_out_header_t);
    ret_pointer += (get_image_bpp(pheader) / 8) * (pheader->img_width * pheader->img_height);
    return ret_pointer;
}

int inline_handler(unsigned char* msg_data, int msg_size, unsigned char* out_data,
                   int* out_data_size) {
    int retval = FP_LIB_ERROR_GENERAL, ret = 0;
    BOOL copy_test_result_first_out_int = FALSE;
    ex_log(LOG_DEBUG, "GIT_SHA1 = UNKNOWN");
    egislog_d("inline_handler enter msg_size = %d", msg_size);

    if (msg_data == NULL || msg_size < (int)sizeof(uint32_t) * 5) {
        ex_log(LOG_ERROR, "invalid parameter!");
        return FP_LIB_ERROR_PARAMETER;
    }
    if (!out_data || !out_data_size || *out_data_size < (int)(sizeof(uint32_t))) {
        ex_log(LOG_ERROR, "out_data is NULL or out_data_size < 4");
        retval = FP_LIB_ERROR_PARAMETER;
        return retval;
    }

    struct TransferPackage* package;
    package = (struct TransferPackage*)msg_data;
    int command = *(int*)package->indata;
    fp_mmi_info* info = (fp_mmi_info*)plat_alloc(sizeof(fp_mmi_info));
    info->testResult = MMI_TEST_SUCCESS;

    egislog_i("inline_handler command: %d", command);
    ret = fp_setPower(SPI_POWER_ON);
    if (ret != 0) ex_log(LOG_ERROR, "inline_handler, fp_setPower:open fail=%d", ret);
    switch (command) {
#if defined(__ET7XX__) && !defined(__ET0XX__)
        case FP_INLINE_7XX_CASE_INIT:  // init
            egislog_d("FP_INLINE_7XX_CASE_INIT start");
            et7xx_inline_init();

            retval = FP_LIB_OK;
            egislog_d("FP_INLINE_7XX_CASE_INIT end");
            break;
        case FP_INLINE_7XX_NORMALSCAN:
        case FP_INLINE_7XX_NORMAL_GET_IMAGE:
        case FP_INLINE_7XX_SNR_INIT:
        case FP_INLINE_7XX_SNR_WKBOX_ON:
        case FP_INLINE_7XX_SNR_BKBOX_ON:
        case FP_INLINE_7XX_SNR_CHART_ON:
        case FP_INLINE_7XX_SNR_GET_ALL_IMAGE_16_BIT_INFO:
        case FP_INLINE_7XX_SNR_GET_ALL_IMAGE_16_BIT:
        case FP_INLINE_7XX_SNR_GET_ALL_IMAGE_8_BIT_INFO:
        case FP_INLINE_7XX_SNR_GET_ALL_IMAGE_8_BIT:
        case FP_INLINE_7XX_SNR_GET_DATA:
        case FP_INLINE_7XX_FLASH_TEST:
        case FP_INLINE_7XX_SNR_SAVE_LOG_ENABLE_EXTRA_INFO:
            retval = et7xx_inline_test_handler(command, package->indata, package->indata_len,
                                               out_data, (UINT32*)out_data_size);
            break;
        case FP_INLINE_7XX_CASE_UNINIT:
            egislog_d("FP_INLINE_7XX_CASE_UNINIT start");
            et7xx_inline_uninit();
            retval = FP_LIB_OK;
            egislog_d("FP_INLINE_7XX_CASE_UNINIT end");
            break;
        case FP_READ_REG: {
            uint16_t addr = package->indata[1];
            io_7xx_dispatch_read_register(addr, out_data);
            egislog_d("FP_READ_REG value: 0x%02x", out_data[0]);
        } break;
        case FP_WRITE_REG: {
            uint16_t addr = package->indata[1];
            uint8_t value = package->indata[2];
            egislog_d("FP_WRITE_REG addr: 0x%04x, value: %#x", addr, value);
            io_7xx_dispatch_write_register(addr, value);
        } break;
        case FP_INLINE_7XX_GET_UUID: {
            unsigned char uuid[10] = {0};
            int size = 10;
            egislog_d("FP_INLINE_7XX_GET_UUID start");
            retval = isensor_get_buffer(PARAM_BUF_SENOR_OTP_DATA, uuid, &size);
            memcpy(out_data, uuid, size);
            egislog_d("FP_INLINE_7XX_GET_UUID end");
        } break;
#endif
        case FP_MMI_AUTO_TEST:
        case FP_MMI_TYPE_INTERRUPT_TEST:
        case FP_MMI_FAKE_FINGER_TEST:
        case FP_MMI_SNR_SINGAL_IMAGE_TEST:
        case FP_MMI_SNR_WHITE_IMAGE_TEST:
        case FP_MMI_BUBBLE_TEST:
        case FP_MMI_SN_TEST:
        case FP_MMI_DIRTYDOTS_TEST:
        case FP_MMI_READ_REV_TEST:
        case FP_MMI_REGISTER_RW_TEST:
        case FP_MMI_REGISTER_RECOVERY:
        case FP_MMI_FOD_TEST:
        case FP_MMI_GET_FINGER_IMAGE: {
            copy_test_result_first_out_int = TRUE;
            info->testType = command;
            info->testResult = MMI_TEST_FAIL;
            retval = egis_mmiDoTest(info);
            egislog_d("egis_mmiDoTest [%d] info->testResult %d.", command, info->testResult);
            break;
        }
#ifdef EGIS_DBG
        case FP_GET_IMAGE_SIZE_INFO:{
            if (out_data == NULL || out_data_size == NULL ||*out_data_size < sizeof(image_size_info_t)) {
                retval = FP_LIB_ERROR_PARAMETER;
                break;
            }
            int sensor_width, sensor_height;
            isensor_get_sensor_roi_size(&sensor_width, &sensor_height);
            image_size_info_t* out_int_array = (image_size_info_t*)out_data;
            out_int_array->img_width = sensor_width;
            out_int_array->img_height = sensor_height;
            out_int_array->img_bpp = fp_getAlgoBpp();
            out_int_array->raw_bpp = 16;
            break;
        }

        case FP_CAPTURE_IMG_RBS_760_FFC_IMAGE:
        case FP_CAPTURE_IMG_RBS_760_FFC_IMAGE_NO_UPDATE_BDS:
        case FP_CAPTURE_IMG: {

            if (out_data == NULL || out_data_size == NULL ||
                *out_data_size <= sizeof(liver_image_out_header_t)) {
                retval = FP_LIB_ERROR_PARAMETER;
                break;
            }
            liver_image_out_header_t* pheader = (liver_image_out_header_t*)out_data;

            int img_width, img_height, img_dpi;
            int sensor_width, sensor_height;
            int flag_checkfinger = ((int*)package->indata)[1];
            int x0_ratio_x100 = ((int*)package->indata)[2];
            int y0_ratio_x100 = ((int*)package->indata)[3];
            BOOL isFinger = 0;
            if (g_enable_crop_img) {
                img_width = g_img_width;
                img_height = g_img_height;
            } else {
                fp_getImageSize(&img_width, &img_height, &img_dpi);
            }

            retval = isensor_get_sensor_roi_size(&sensor_width, &sensor_height);
            if (retval != FP_LIB_OK) {
                egislog_e("isensor_get_sensor_roi_size failed");
                break;
            }
            if (sensor_width <= 0 || sensor_height <= 0) {
                egislog_e("sensor size has problem %d:%d", sensor_width, sensor_height);
                break;
            }
            uint32_t image_size = sensor_width * sensor_height;

#if defined(__SENSOR_ET602__) || defined(__SENSOR_ET601__) || defined(__SENSOR_ET603A__)
            // egislog_e("isensor_set_custom_sensor_mode : not supported!");
            retval = isensor_set_sensor_mode();  // SINGLE_SENSING_MODE = 1
#else
            retval = isensor_set_custom_sensor_mode(1);  // SINGLE_SENSING_MODE = 1
#endif
            if (retval != EGIS_OK) {
                egislog_e("isensor_set_custom_sensor_mode failed");
            }
            fd_egis_set_capture_roi(x0_ratio_x100, y0_ratio_x100);
            int x0, y0;
            isensor_get_int(PARAM_INT_ROI_Y, &x0);
            isensor_get_int(PARAM_INT_ROI_Y, &y0);
            setup_liver_image_out_header(pheader, sensor_width, sensor_height,
                                         LIVIMG_IMAGETYPE_NONE, fp_getAlgoBpp(), 16, x0, y0);
            uint8_t* img_pointer = get_liver_image_out_img_pointer(pheader);

            fd_process_create(NULL, 0, 1, FD_PROCESS_OPTION_NORMAL);

            if ( command == FP_CAPTURE_IMG_RBS_760_FFC_IMAGE) {
                // for et760 hack liveview show ffc image
                fd_acquire_image(img_pointer, img_width, img_height, FD_ACQUIREI_OPTION_GET_FFC_IMAGE);
            }
            else if ( command == FP_CAPTURE_IMG_RBS_760_FFC_IMAGE_NO_UPDATE_BDS ) {
                // for et760 hack liveview show ffc image, but no update bds
                fd_acquire_image(img_pointer, img_width, img_height, FD_ACQUIREI_OPTION_GET_FFC_IMAGE_NO_UPDATE_BDS);
            }
            else {
                fd_acquire_image(img_pointer, img_width, img_height, FD_ACQUIREI_OPTION_FROM_INLINE);
            }
            if (retval == EGIS_OK) {
                isFinger = egis_checkIsFinger(img_pointer, img_width, img_height, flag_checkfinger,
                                              pheader);

                unsigned char* raw = fd_get_current_raw_pointer(&pheader->raw_bpp);
                unsigned char* praw = get_liver_image_out_raw_pointer(pheader);
                memcpy(praw, raw, image_size * pheader->raw_bpp / 8);
            } else {
                setup_liver_image_out_header(pheader, 0, 0, LIVIMG_IMAGETYPE_NONE, fp_getAlgoBpp(),
                                             16, x0, y0);
                egislog_e("isensor_get_frame failed, ret=%d", retval);
            }
            fd_process_destroy();
            break;
        }
#endif
        case FP_MMI_SET_CROP_INFO: {
            int sensor_width, sensor_height;

            g_enable_crop_img = ((int*)package->indata)[1];
            int crop_width = ((int*)package->indata)[2];
            int crop_height = ((int*)package->indata)[3];

            retval = isensor_get_sensor_roi_size(&sensor_width, &sensor_height);
            if (retval != FP_LIB_OK) {
                egislog_e("FP_MMI_SET_CROP_INFO isensor_get_sensor_roi_size failed");
                break;
            }
            if (sensor_width <= 0 || sensor_height <= 0) {
                egislog_e("FP_MMI_SET_CROP_INFO sensor size has problem %d:%d", sensor_width,
                          sensor_height);
                break;
            }

            if (crop_width <= 0 || crop_height <= 0 || crop_width > sensor_width ||
                crop_height > sensor_height) {
                g_img_width = sensor_width;
                g_img_height = sensor_height;
            } else {
                g_img_width = crop_width;
                g_img_height = crop_height;
            }

        } break;
        case FP_MMI_GET_NVM_UID: {
            retval = egis_mmiGetNVMUid(out_data, out_data_size);
            break;
        }
        case FP_INLINE_SENSOR_CALIBRATE: {
            FPS_CALIBRATE_OPTION option = (FPS_CALIBRATE_OPTION)((int*)package->indata)[1];
            ex_log(LOG_DEBUG, "FP_INLINE_SENSOR_CALIBRATE %d", option);
            retval = isensor_calibrate(option);
            break;
        }
        case FP_INLINE_SENSOR_GET_CALI_IMAGE: {
            int position_raw_img;
            int sensor_width = SENSOR_FULL_WIDTH, sensor_height = SENSOR_FULL_HEIGHT;
            int img_size, raw_size;
            unsigned char* raw_img;
            //retval = isensor_get_sensor_roi_size(&sensor_width, &sensor_height);
            img_size = sensor_width * sensor_height;
            raw_size = img_size * 2;
            position_raw_img = sizeof(liver_image_out_header_t) + img_size;
            if (*out_data_size < position_raw_img + raw_size) {
                ex_log(LOG_ERROR, "*out_data_size %d too small (< %d)", *out_data_size,
                       position_raw_img + raw_size);
                retval = FP_LIB_ERROR_PARAMETER;
                break;
            }
            int bkg_cx = core_config_get_int(INI_SECTION_SENSOR, KEY_CIRCLE_CENTER_X, sensor_width/2);
            int bkg_cy = core_config_get_int(INI_SECTION_SENSOR, KEY_CIRCLE_CENTER_Y, sensor_height/2);
            liver_image_out_header_t* pheader = (liver_image_out_header_t*)out_data;
            setup_liver_image_out_header(pheader, sensor_width, sensor_height,
                                         LIVING_IMAGETYPE_CALI_IMAGE, 8, 16, bkg_cx, bkg_cy);
            isensor_get_int(PARAM_INT_CALI_BKG_EXPOSURE_TIME_X10,
                            &pheader->image_par_t.cali_image_data.exp_time_x10);
            isensor_get_int(PARAM_INT_CALI_BKG_HW_INTEGRATE_COUNT,
                            &pheader->image_par_t.cali_image_data.hw_integrate_count);
            isensor_get_int(PARAM_INT_CALI_BKG_CX, &pheader->image_par_t.cali_image_data.bkg_cx);
            isensor_get_int(PARAM_INT_CALI_BKG_CY, &pheader->image_par_t.cali_image_data.bkg_cy);
            raw_img = out_data + position_raw_img;
            retval = isensor_get_buffer(PARAM_BUF_CALI_BKG_IMAGE, raw_img, &raw_size);
            if (retval == EGIS_OK && raw_size > 0) {
                image_16bitsTo8bits((unsigned short*)raw_img, raw_img - img_size, sensor_width,
                                    sensor_height);
            } else {
                retval = EGIS_COMMAND_FAIL;
            }
            break;
        }
        case FP_INLINE_SENSOR_GET_IMAGE: {

            int position_raw_img;
            int sensor_width, sensor_height;
            int img_size, raw_size;
            unsigned char* raw_img;
            retval = isensor_get_sensor_roi_size(&sensor_width, &sensor_height);
            img_size = sensor_width * sensor_height;
            raw_size = img_size * 2;
            position_raw_img = sizeof(liver_image_out_header_t) + img_size;
            if (*out_data_size < position_raw_img + raw_size) {
                ex_log(LOG_ERROR, "*out_data_size %d too small (< %d)", *out_data_size,
                       position_raw_img + raw_size);
                retval = FP_LIB_ERROR_PARAMETER;
                break;
            }
            int x_ratio_x100 = ((int*)package->indata)[1];
            int y_ratio_x100 = ((int*)package->indata)[2];
            fd_egis_set_capture_roi(x_ratio_x100, y_ratio_x100);
            int x0, y0;
            isensor_get_int(PARAM_INT_ROI_Y, &x0);
            isensor_get_int(PARAM_INT_ROI_Y, &y0);
            ex_log(LOG_DEBUG, "FP_INLINE_SENSOR_GET_IMAGE %d %d %d %d", x_ratio_x100, y_ratio_x100, x0, y0);
            liver_image_out_header_t* pheader = (liver_image_out_header_t*)out_data;
            setup_liver_image_out_header(pheader, sensor_width, sensor_height,
                                         LIVIMG_IMAGETYPE_NONE, fp_getAlgoBpp(), 16, x0, y0);
            raw_img = out_data + position_raw_img;
            retval = isensor_get_dynamic_frame_16bit((unsigned short*)raw_img, (UINT)sensor_height,
                                                     (UINT)sensor_width, 1);
            // retval = isensor_get_buffer(PARAM_BUF_CALI_BKG_IMAGE, raw_img, &raw_size);
            if (retval == EGIS_OK) {
                image_16bitsTo8bits((unsigned short*)raw_img, raw_img - img_size, sensor_width,
                                    sensor_height);
            }
            break;
        }
        default:
            egislog_e("unknown inline cmd");
            retval = FP_LIB_ERROR_GENERAL;
            break;
    }

    if (copy_test_result_first_out_int) {
        if (FP_MMI_REGISTER_RW_TEST == command && SENSOR_NEED_RESET == retval) {
            memcpy(out_data, &retval, sizeof(int));
            retval = FP_LIB_OK;
        } else {
            memcpy(out_data, &(info->testResult), sizeof(int));
        }
        egislog_i("inline_handler copy_test_result %d", *((int*)out_data));
    }

    ret = fp_setPower(SPI_POWER_OFF);
    if (ret != 0) ex_log(LOG_ERROR, "inline_handler, fp_setPower:close fail");
    PLAT_FREE(info)

    ex_log(retval == 0 ? LOG_DEBUG : LOG_ERROR, "inline_handler return %d", retval);
    return retval;
}
