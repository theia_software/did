#ifndef FP_ALGOMODULE_H
#define FP_ALGOMODULE_H

#include "fp_definition.h"
#ifndef HW_HAWAII_Y6
#include "tee_internal_api.h"
#endif
#include "egis/algomodule_ext.h"

#define NO_NEED_UPDATE_TEMPLETE 119

#define SYNA_TEMPLATE_VERSION 0x01
#define FPC_TEMPLATE_VERSION_sw19 0x02
#define GOODIX_TEMPLATE_VERSION 0x04
#define EGIS_TEMPLATE_VERSION 0x0a

enum {
    KNIGHT = 0,
    FARADAY,
    NEXT,
    EVA,
    VIENNA,
    VENUS,
    EDISON,
    BTV,
    NATASHA,
    MILAN,
    CANNES,
    MAX_PRODUCT_ID,
};

typedef enum _enroll_direction_msg_t {
    ENROLL_DIRECTION_NA = 0,
    ENROLL_DIRECTION_SW,
    ENROLL_DIRECTION_S,
    ENROLL_DIRECTION_SE,
    ENROLL_DIRECTION_NW,
    ENROLL_DIRECTION_N,
    ENROLL_DIRECTION_NE,
    ENROLL_DIRECTION_E,
    ENROLL_DIRECTION_W
} enroll_direction_msg_t;

typedef struct {
    uint32_t enroll_option;
    uint32_t progress;                /*progress of the current enroll process in percent */
    uint32_t quality;                 /*Quality for the image*/
    fp_lib_enroll_result_t result;    /*Status of current enroll attempt*/
    uint32_t nr_successful;           /*number of successful enroll attempts so far*/
    uint32_t nr_failed;               /*number of failed enroll attempts so far*/
    uint32_t enrolled_template_size;  /*size of the enrolled template*/
    uint32_t extended_enroll_size;    /*size of the data part in the structure
                         used for extended enroll*/
    uint32_t coverage;                /*coverage of the image*/
    int8_t user_touches_too_immobile; /*used to indicate that touches are
                         too similar*/
    int8_t guide_direction;           // 0: ENROLL_DIRECTION_NA
} fp_lib_enroll_data_t;

typedef struct {
    uint8_t* tpl;  /*pointer to a buffer for template data*/
    uint32_t size; /*size of the buffer for template data*/
} fp_lib_template_t;

typedef fp_lib_template_t fp_template_t;

typedef struct {
    fp_lib_identify_result_t result; /*result of the identification attempt*/
    uint32_t score;                  /*Matching score*/
    uint32_t index;                  /*Index of the indentification template*/
    uint32_t updated_template_size;  // size of the update template if one exits
    int coverage;
    int quality;
    // TODO:Sync HUA project
    int try_match_count;
    int last_tried_index;
    feature_extract_result_t extract_result;
} fp_lib_identify_data_t;

void setGlobalImgQuality(void* inImgQty);
int fp_initAlgAndPPLib(void);
// check template version with algo version  -1:uncheck   0:check ok  1:unmatch
// 2:error
int fp_initCheckTemplateVersion(uint32_t* templateVersion);
uint32_t fp_getCurrentTemplateVersion(void);
void fp_deInitAlg(void);
void fp_getImageSize(int* width, int* height, int* dpi);
int fp_identifyStart(uint32_t need_liveness_authentication);
void fp_identifyFinish(void);
int fp_identifyImage_enroll(void* image, fp_lib_enroll_data_t* enroll_data_t, int size);
int fp_identifyImage_identify(void* image, fp_lib_identify_data_t* fp_identify_data_t, int size);
int fp_identifyUpdateTemplate(void** unpack_tplt, uint8_t* isTemplateUpdate, void* image);
//#if defined(ALGO_GEN_7)
//int fp_identifyUpdateNotMatchTemplate(void** unpack_tplt, uint8_t* isTemplateUpdate, int index);
//#endif
int fp_startEnroll(void);
int fp_updateEnrolData(fp_lib_enroll_data_t* fp_enrol_data, void* image);
int fp_setFingerState(unsigned int finger_on);
int fp_mergeEnrollTemp(fp_lib_enroll_data_t* enrol_data, void* image);
int fp_finishEnroll(fp_lib_template_t* template);
int fp_packTemplate(fp_lib_template_t* template_t, void* unpack_tplt);
int fp_unpackTemplate(fp_lib_template_t* tplt, int* candidate_size_p);
int fp_deleteTemplate(int index);
// DTS
void fp_getAlgVersion(uint8_t* version, uint32_t version_len);
int fp_getAlgSecurityThr(uint32_t* data);

int fp_egis_enroll(void* image, fp_lib_enroll_data_t* enroll_data_t, int size, void* swipe_info);
int fp_getAlgoBpp();
int fp_set_verify_finger_info(int verify_count, int verified_count);
#endif
