#ifndef FP_SENSORMODULE_H
#define FP_SENSORMODULE_H

#include "fd_process.h"
#include "mmi_testalgo.h"
#include "tee_internal_api.h"

#ifdef NOT_HUA_PROJ
#include "common_definition.h"
#endif

#define SPI_POWER_OFF 0
#define SPI_POWER_ON 1

typedef enum {
    FP_MMI_TEST_PASS = 1,
    FP_MMI_TESTING = 2,
} mmi_test_result;

#if !defined(NOT_HUA_PROJ)
typedef enum {
    FP_MMI_TEST_NONE = 0,
    FP_MMI_AUTO_TEST = 1,  // for MMI1 and MMI2
    FP_MMI_TYPE_INTERRUPT_TEST = 2,
    FP_MMI_FAKE_FINGER_TEST = 3,  // juse for MMI1
    FP_MMI_SNR_SINGAL_IMAGE_TEST = 4,
    FP_MMI_SNR_WHITE_IMAGE_TEST = 5,
    FP_MMI_BUBBLE_TEST = 6,
    FP_MMI_SN_TEST = 7,
} fp_mmi_test_type;
#endif

typedef struct {
    fp_mmi_test_type testType;
    uint32_t testResult;
} fp_mmi_info;

typedef struct {
    int CoatValue1;
    int CoatValue2;
    int CoatValue3;
    int CoatValue4;
    int LlrSubAreas[8];
    int Value1SubAreas[8];
    bool pass;
} fp_Coatingcheck;

typedef struct {
    uint16_t snr_adc;
    uint32_t snr_pxlCtrl;
    uint32_t snr_noise;
    uint32_t snr_singal;
    uint32_t mqt_sore;
    uint32_t mqt_result;
    uint32_t result;
    bool pass;
} snr_result;

typedef struct {
    uint32_t fp_pn_size;
    uint32_t wholetilt;
    uint32_t block;
    uint32_t result;
    bool pass;
    //    fp_bubble_parameter_t parm_b;
    //    fp_uniformity_parameter_t parm_u;
} bubble_result;

#if 0
typedef struct
{
	uint32_t width;
	uint32_t height;
	float fGrav;
	uint32_t thr_total_blob_pxls;
	uint32_t thr_max_length_blobl;
	uint32_t	thr_max_pxl_per_blobl;
	uint32_t	thr_ink_dot_pxls;
} fp_bubble_parameter_t;

typedef struct
{
	uint32_t width;		
	uint32_t height;	
	uint8_t edge;		
	uint32_t thr_whole;	
	uint32_t thr_block;	
} fp_uniformity_parameter_t;

typedef struct 
{
	uint32_t width;					
	uint32_t height;				
	uint32_t image_counts;			
	uint32_t cut_white_pixels;		
	uint32_t cut_signal_pixels;		
	uint32_t calibration;			
	float cut_black_percent;		
	float cut_white_percent;		
	float snr_threshold;
} fp_snr_parameter_t;
#endif

typedef struct {
    uint8_t* reg_addr;
    uint8_t* buf_addr;
    unsigned int reg_len;
    unsigned int buf_len;
} spi_transaction_info;

enum {
    FINGERPRINT_WAKEUP_DEV_ERROR = 11,
    FINGERPRINT_RESET_ENTRY_ERROR,                 /*12*/
    FINGERPRINT_VERIFY_HW_ID_ERROR,                /*13*/
    FINGERPRINT_CAPTURE_IMAGE_ERROR,               /*14*/
    FINGERPRINT_IRQ_NOT_PULL_UP_ERROR,             /*15*/
    FINGERPRINT_WAIT_IRQ_ERROR,                    /*16*/
    FINGERPRINT_CLEAR_IRQ_ERROR,                   /*17*/
    FINGERPRINT_IRQ_NOT_PULL_LOW_ERROR,            /*18*/
    FINGERPRINT_READ_STATUS_REG_ERROR,             /*19*/
    FINGERPRINT_TOO_MANY_DEADPIXEL_ERROR,          /*20*/
    FINGERPRINT_RESET_EXIT_ERROR,                  /*21*/
    FINGERPRINT_WAIT_FOR_FINGER_ERROR,             /*22*/
    FINGERPRINT_FINGER_DETECT_ERROR,               /*23*/
    FINGERPRINT_MIN_CHECKER_DIFF_ERROR,            /*24*/
    FINGERPRINT_CB_TYPE1_MEDIAN_BOUNDARY_ERROR,    /*25*/
    FINGERPRINT_CB_TYPE2_MEDIAN_BOUNDARY_ERROR,    /*26*/
    FINGERPRINT_ICB_TYPE1_MEDIAN_BOUNDARY_ERROR,   /*27*/
    FINGERPRINT_ICB_TYPE2_MEDIAN_BOUNDARY_ERROR,   /*28*/
    FINGERPRINT_DEADPIXEL_DETECT_ZONE_ERROR,       /*29*/
    FINGERPRINT_DEADPIXEL_DETECT_ZONE_HW_ID_ERROR, /*30*/
    FINGERPRINT_COATING_TEST_FAIL_ERROR,           /*31*/
    FINGERPRINT_SNR_WAIT_FINGER_UP_ERROR,          /*32*/
    FINGERPRINT_SNR_WAIT_FINGER_DOWN_ERROR,        /*33*/
    FINGERPRINT_SNR_TEST_FAIL_ERROR,               /*34*/
    FINGERPRINT_BUBBLE_TEST_FAIL_ERROR,
    FINGERPRINT_FP_CAL_FAIL_ERROR,
    FINGERPRINT_FP_CHECK_SN_FAIL_ERROR
};

// sensor
int fp_initSensor(void);
int fp_initFingerDetect(uint32_t* wait_time, uint32_t mode);  // wait_time
int fp_captureImage(void* pFingerPrintImage, uint32_t is_Enrolling,
                    enum fd_process_option fd_option);
int fp_sensorSleep(void);
int fp_checkFingerLost(uint32_t* wait_time);
int fp_getSensorId(uint32_t* sensor_info);
int fp_deInitSensor(void);
int fp_getImageInformation(void* data);
int fp_setSensingArea(int left, int top, int width, int height);
int fp_setPower(const int option);
int fp_getRecoveryEvent(void);
int fp_updateConfig(void);

// debug
void fp_debugStubImageTest(void);
int fp_debugInjectImage(void* image, uint32_t len, uint32_t qsee_mark, uint32_t offset);
int fp_debugRetrieveImage(void* image, uint32_t len, uint32_t qsee_mark, uint32_t offset);
void fp_memMoveImageBuffer(unsigned char* dst, void* src);

// navigation
int fp_navSetStatus(bool enable);
int fp_navGetEvent(void* data);
int fp_navGetZones(void* data, int count);
int fp_navGetInfo(void* data);
// mmi
int fp_mmiDoTest(fp_mmi_info* pMmiInfo);
int fp_mmiDeadPixelResult(uint32_t* data);
int fp_mmiCoatingResult(fp_Coatingcheck* data);
int fp_getSnrImage(unsigned char* buffer, unsigned int length, unsigned index);
int fp_mmiSnrResult(snr_result* data);
int fp_getBubbleImage(uint8_t* buffer, uint32_t length);
int fp_mmiBubbleResult(bubble_result* data);

int fp_getCoatingImage(unsigned char* buffer, unsigned int length);

// void fp_setSnrParameter(fp_snr_parameter_t* snr_parameter);
// int fp_captureSnrImage(uint8_t* buffer, uint32_t image_counts, uint32_t
// image_size, uint8_t type);
// int fp_runSnrTest(uint8_t type, uint32_t *result);

// void fp_setBubbleParameter(fp_bubble_parameter_t* bubble_parameter);
// fp_captureBubbleImage(uint8_t* buffer, uint32_t image_size);
// extern int fp_runBubbleTest(uint32_t *result);

// TEE driver

int fp_spiDuplexCommunication(void* p_write_info, void* p_read_info, int speed);

#endif
