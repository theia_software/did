#ifndef __MMI_TESTALGO_H__
#define __MMI_TESTALGO_H__

/*
 *	Dummy header File usage.
 *
 * !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 *
 *	SHOULD NOT USE THIS FILE IN RELEASE
 *
 * !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 */

typedef struct {
    uint32_t width;
    uint32_t height;
    uint32_t image_counts;
    uint32_t cut_white_pixels;
    uint32_t cut_signal_pixels;
    uint32_t calibration;
    float cut_black_percent;
    float cut_white_percent;
    float snr_threshold;
} fp_snr_parameter_t;

#endif