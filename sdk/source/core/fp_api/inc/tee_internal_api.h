#ifndef __TEE_INTERNAL_API_H_
#define __TEE_INTERNAL_API_H_

/*
 *	Dummy header File usage.
 *
 * !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 *
 *	SHOULD NOT USE THIS FILE IN RELEASE
 *
 * !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 */

#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#define TEE_Result int
#define TEE_SUCCESS (int)0
#define TEE_FAIL (int)-1

#include <string.h>
#include "plat_log.h"
#define TEE_MemMove mem_move

#define NOT_HUA_PROJ

#endif
