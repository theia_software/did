/**
 * @file algomodule_ext.h
 * @brief To extend more declartions for fp_algomodule.h
 * @author Weining Chen, william.chen@egistec.com
 * @version
 * @date 2019-01-04
 */

#ifndef FP_ALGOMODULE_EXT_H
#define FP_ALGOMODULE_EXT_H

/**
 * @brief The struct to storage output of extract_feature().
 */
typedef struct _feature_extract_result_t_ {
    int32_t ext_feat_retval;
    int32_t ext_feat_quality;
} feature_extract_result_t;

typedef struct {
    int16_t width;              // width in pixels
    int16_t height;             // height in pixels
    int16_t ppi;                // sampling intensity
    int8_t bits_per_pixels;     // greyscale bit depth of the pixels
    int8_t channels;            // number of channels
    int8_t greyscale_polarity;  // 0 indicate that ridges are black, valleys
                                // are white, 1 indeicate inverse situation
} Fp_FrameFormat;

#endif  // end of FP_ALGOMODULE_EXT_H
