project(fp_api)

## For fp Interface
list(APPEND RBS_INCLUDE_PATH include)
list(APPEND RBS_INCLUDE_PATH inc)

add_library(fp_api STATIC
	${CMAKE_CURRENT_LIST_DIR}/src/egis_common.c	
	${CMAKE_CURRENT_LIST_DIR}/src/fp_algomodule.c	
	${CMAKE_CURRENT_LIST_DIR}/src/fp_sensormodule.c	
	)

