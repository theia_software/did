#include <stdint.h>

#include "algomodule.h"
#include "common_definition.h"
#include "core_config.h"
#include "egis_algomodule.h"
#include "egis_navi.h"
#include "egis_sensormodule.h"
#include "fp_algomodule.h"
#include "fp_custom.h"
#include "fp_sensormodule.h"
#include "tee_internal_api.h"

#ifndef NULL
#define NULL 0
#endif
#define EGIS_RETURN_ERROR -1
#define LOG_TAG "RBS-Sensor"

#ifdef NOT_HUA_PROJ
#include "plat_mem.h"
#else
#include "fpc_log.h"
#include "fpc_mem.h"
#include "qsee_tlmm.h"
#define plat_alloc malloc
#define plat_free free
#define PLAT_FREE(x)  \
    if (x != NULL) {  \
        plat_free(x); \
        x = NULL;     \
    }
void mdelay(uint32_t msecs);
extern uint32_t gpio_reset;
#endif

int fp_initSensor(void) {
    int result = FP_LIB_OK;
    int img_width = 0;
    int img_height = 0;
    int dpi = 0;

    result = egis_initSensor(0);
    LOGD("@egis-hfsi-ret:%d", result);
    return result;
}

int fp_initFingerDetect(uint32_t* wait_time, uint32_t mode) {
    int result = FP_LIB_OK;
    LOGD("@HFSI:%d", mode);
    if (NULL == wait_time) {
        LOGE("@egis-hfsi-ERROR_PARAMETER");
        return FP_LIB_ERROR_PARAMETER;
    }
    result = egis_initFingerDetect(wait_time, mode);
    LOGD("@egis-hfsi-ret:%d", result);
    return result;
}

void setGlobalImgQuality(void* inImgQty);
int fp_captureImage(void* pFingerPrintImage, uint32_t is_Enrolling,
                    enum fd_process_option fd_option) {
    int result = FP_LIB_OK;
    if (NULL == pFingerPrintImage) {
        LOGE("@egis-hfsi-ERROR_PARAMETER");
        return FP_LIB_ERROR_PARAMETER;
    }
    result = egis_captureImage(pFingerPrintImage, 0, fd_option);

    LOGD("@egis-hfsi-ret:%d", result);
    /* Both FP_LIB_FINGER_PRESENT & FP_LIB_FINGER_LOST are expected */
    if (result == FP_LIB_FINGER_PRESENT || result == FP_LIB_FINGER_LOST) {
        result = FP_LIB_OK;
    }
    return result;
}

int fp_sensorSleep(void) {
    int result = FP_LIB_OK;
    result = egis_sensorSleep();
    LOGD("@egis-hfsi-ret:%d", result);
    return result;
}

int fp_checkFingerLost(uint32_t* wait_time) {
    int result = FP_LIB_OK;
    LOGD("@HFSI:%d", *wait_time);
    if (NULL == wait_time) {
        LOGE("@egis-hfsi-ERROR_PARAMETER");
        return FP_LIB_ERROR_PARAMETER;
    }
    result = egis_checkFingerLost(wait_time);

    LOGD("@egis-hfsi-ret:%d", result);
    return result;
}

int fp_getSensorId(uint32_t* sensor_info) {
    int result = 0;
    if (NULL == sensor_info) {
        LOGE("@egis-hfsi-ERROR_PARAMETER");
        return FP_LIB_ERROR_PARAMETER;
    }
    result = egis_getSensorId(sensor_info);
    LOGD("@egis-hfsi-ret:%d", result);
    return result;
}

int fp_deInitSensor(void) {
    int result = 0;
    result = egis_deInitSensor();
    LOGD("@egis-hfsi-ret:%d", result);
    return result;
}

int fp_getImageInformation(void* data) {
    int result = FP_LIB_OK;
    if (NULL == data) {
        LOGE("@egis-hfsi-ERROR_PARAMETER");
        return FP_LIB_ERROR_PARAMETER;
    }
    result = egis_getImageInformation(data);
    LOGD("@egis-hfsi-ret:%d", result);
    return result;
}

// STUB_IMAGE_FP_TEST looks not used
void fp_debugStubImageTest(void) {
    egis_debugStubImageTest();
}
#define SHORT_PACKAGE 1
#define NORMAL_PACKAGE 0
int fp_debugInjectImage(void* image, uint32_t len, uint32_t qsee_mark, uint32_t offset) {
    int result = FP_LIB_OK, frame_size = 0;
    LOGE("@HFSI: %p - %d, %d, %d", image, len, qsee_mark, offset);
    return result;
}

int fp_debugRetrieveImage(void* image, uint32_t len, uint32_t qsee_mark, uint32_t offset) {
    int result = FP_LIB_OK, frame_size = 0;
    LOGE("@HFSI: %p - %d, %d, %d", image, len, qsee_mark, offset);
    return result;
}

void fp_getImageSize(int* width, int* height, int* dpi) {
    if ((NULL == width) || (NULL == height) || (NULL == dpi)) {
        LOGE("@egis-hfsi-ERROR_PARAMETER");
        return;
    }
    egis_getImageSize(width, height, dpi);
}

void fp_memMoveImageBuffer(unsigned char* dst, void* src) {
    if ((NULL == dst) || (NULL == src)) {
        LOGE("@egis-hfsi-ERROR_PARAMETER!");
        return;
    }
    egis_memMoveImageBuffer(dst, src);
}

// navigation
int fp_navSetStatus(bool enable) {
    int result = FP_LIB_OK;
    return result;
}

#define LONG_TOUCH_DURATION 500
#define DOUBLE_CLICK_DURATION 300
#define DX_THRESHOLD 12
#define DY_THRESHOLD 12

int fp_navGetEvent(void* data) {
#ifdef FEATURE_NAVIGATION
    int result = FP_LIB_OK;
    NaviConfig naviConfig;
    navigation_info_common naviInfo;
    if (NULL == data) {
        LOGE("@egis-hfsi-ERROR_PARAMETER");
        return FP_LIB_ERROR_PARAMETER;
    }
    naviConfig.longTouchDuration = LONG_TOUCH_DURATION;
    naviConfig.xThreshold = DX_THRESHOLD;
    naviConfig.yThreshold = DY_THRESHOLD;
    naviConfig.doubleClickDuration = DOUBLE_CLICK_DURATION;
    memset(&naviInfo, 0, sizeof(navigation_info_common));
    naviInfo.eventValue = EVENT_LOST;
    result = egis_navGetEvent(&naviConfig, &naviInfo);
    navigation_info_common* pRetNaviInfo = &naviInfo;
    navigation_info_common* pOutNaviInfo = (navigation_info_common*)data;
    pOutNaviInfo->eventValue = pRetNaviInfo->eventValue;
    LOGD("@egis-hfsi-navGetEvent D1 eV=%d, nwI=%d, pc=%d.", pOutNaviInfo->eventValue,
         pOutNaviInfo->needWaitIrq, pOutNaviInfo->poll_count);
    return FP_LIB_OK;
#else
    return FP_LIB_ERROR_GENERAL;
#endif
}

// looks not used just fixed compile error
int fp_navGetZones(void* data, int count) {
    int result = FP_LIB_OK;
    if (NULL == data) {
        LOGE("@egis-hfsi-ERROR_PARAMETER");
        return FP_LIB_ERROR_PARAMETER;
    }
    return result;
}

int fp_navGetInfo(void* data) {
#ifdef FEATURE_NAVIGATION
    return egis_navGetInfo(data);
#else
    LOGE("@egis-hfsi Not supported");
    return FP_LIB_ERROR_GENERAL;
#endif
}
// mmi
int fp_mmiDoTest(fp_mmi_info* pMmiInfo) {
    int result = FP_LIB_OK;
    if (NULL == pMmiInfo) {
        LOGE("@egis-hfsi-ERROR_PARAMETER");
        return FP_LIB_ERROR_PARAMETER;
    }

#ifdef __ET5XX__
    result = egis_mmiDoTest(pMmiInfo);
    LOGD("@egis-hfsi-ret:%d", result);
#endif
    return result;
}
#if defined(NOT_HUA_PROJ)
int fp_runSnrTest(uint8_t type, uint32_t* result) {
    egislog_e("%s not supported", __func__);
    return 0;
}

void egis_device_reset() {
    egislog_e("%s not supported", __func__);
    return;
}
#else
// MMI_FAKE_FINGER_TEST:
int fp_mmiCoatingResult(fp_Coatingcheck* data) {
    int result = FP_LIB_OK;
    if (NULL == data) {
        LOGE("@egis-hfsi-ERROR_PARAMETER");
        return FP_LIB_ERROR_PARAMETER;
    }
    result = egis_mmiCoatingResult(data);
    LOGD("@egis-hfsi-ret:%d", result);
    return result;
}

// open_hal->self_test
int fp_mmiDeadPixelResult(uint32_t* data) {
    int result = FP_LIB_OK;
    if (NULL == data) {
        LOGE("@egis-hfsi-ERROR_PARAMETER");
        return FP_LIB_ERROR_PARAMETER;
    }
    result = egis_mmiDeadPixelResult(data);
    LOGD("@egis-hfsi-ret:%d", result);
    return result;
}

// MMI_BUBBLE_TEST:
int fp_getBubbleImage(uint8_t* buffer, uint32_t length) {
    int result = 0;
    if (NULL == buffer) {
        LOGE("@egis-hfsi-ERROR_PARAMETER");
        return FP_LIB_ERROR_PARAMETER;
    }
    return result;
}
// Save bubble image;
int fp_mmiBubbleResult(bubble_result* data) {
    int result = 0;
    if (NULL == data) {
        LOGE("@egis-hfsi-ERROR_PARAMETER");
        return FP_LIB_ERROR_PARAMETER;
    }
    return result;
}

// looks not used just fixed compile error
int fp_getCoatingImage(unsigned char* buffer, unsigned int length) {
    int result = FP_LIB_OK;
    if (NULL == buffer) {
        LOGE("@egis-hfsi-ERROR_PARAMETER");
        return FP_LIB_ERROR_PARAMETER;
    }
    return result;
}

void fp_setSnrParameter(fp_snr_parameter_t* snr_parameter) {
    if (NULL == snr_parameter) {
        LOGE("@egis-hfsi-ERROR_PARAMETER");
        return;
    }
    egis_setSnrParameter(snr_parameter);
}

int fp_captureSnrImage(uint8_t* buffer, uint32_t image_counts, uint32_t image_size, uint8_t type) {
    LOGD("@HFSI, img_cnt:%d, img_size:%d, img_type:%02X", image_counts, image_size, type);
    int result = FP_LIB_OK;
    if (NULL == buffer) {
        LOGE("@egis-hfsi-ERROR_PARAMETER");
        return FP_LIB_ERROR_PARAMETER;
    }
    result = egis_captureSnrImage(buffer, image_counts, image_size, type);
    LOGD("@egis-hfsi-ret:%d", result);
    return result;
}

#define RESET_DELAY_10MS 10
#define RESET_DELAY_20MS 20
void egis_device_reset() {
    int result = -1;
    LOGD("@egis-hfsi-gpio_reset %u", gpio_reset);

    if (gpio_reset == 0) {
        LOGE("@egis-hfsi-gpio_reset fail %u", gpio_reset);
        return;
    }

    LOGD("@HFSI,reset pin and pull low");
    result = qsee_tlmm_gpio_id_out(gpio_reset, QSEE_GPIO_LOW);
    if (result != 0) {
        LOGE("@HFSI,reset low failed %d", result);
        return;
    }
    mdelay(RESET_DELAY_10MS);
    result = qsee_tlmm_gpio_id_out(gpio_reset, QSEE_GPIO_HIGH);
    if (result != 0) {
        LOGE("@HFSI,reset high failed %d", result);
        return;
    }
    mdelay(RESET_DELAY_20MS);
    LOGD("@HFSI,reset sceeuss %d", result);

    return;
}
#endif

#define FP_MODULE_INFO_LEN 256
#define OTP_SIZE 64
int fp_getModuleInfo64(char* data) {
    int result = 0;
    uint8_t module_id_buf[FP_MODULE_INFO_LEN] = {'E', 'g', 'i', 's', '\0'};
    if (NULL == data) {
        LOGE("@egis-hfsi-ERROR_PARAMETER");
        return FP_LIB_ERROR_PARAMETER;
    }
    result = egis_read_nvram(module_id_buf);
    memcpy(data, (const char*)module_id_buf, OTP_SIZE);
    return result;
}

int fp_setPower(const int option) {
    return egis_set_spi_power(option);
}

int fp_getRecoveryEvent(void) {
    return egis_get_recovery_event();
}

int fp_updateConfig(void) {
    return egis_updateConfig();
}
