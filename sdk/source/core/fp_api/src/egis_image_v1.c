#include "algomodule.h"
#include "egis_definition.h"
#include "fp_algomodule.h"
#include "object_def_image.h"
#include "plat_log.h"
#include "plat_mem.h"

#define LOG_TAG "RBS"

int egis_image_is_allocated(const egis_image_t* img) {
    return img->img_data._buffer != NULL ? 1 : 0;
}

unsigned char* egis_image_get_pointer(const egis_image_t* img, int index) {
    if (img == NULL || index < 0) return NULL;

    unsigned int img_size = img->img_data.format.width * img->img_data.format.height;
    unsigned int offset = index * img_size;
    if (offset + img_size > img->img_data.capacity) return NULL;

    return (img->img_data._buffer + offset);
}

unsigned char* egis_image_get_pointer_raw(const egis_image_t* img, int index) {
    if (img == NULL || index < 0) return NULL;
#ifdef SUPPORT_KEEP_RAW_16BIT
    unsigned int img_size =
        img->img_data.raw_width * img->img_data.raw_height * img->img_data.raw_bpp / 8;
    unsigned int offset = index * img_size;
    if (offset + img_size > img->img_data.capacity * 2) return NULL;

    return (img->img_data.raw_buffer + offset);
#endif
    return NULL;
}

unsigned char* egis_image_get_bkg_pointer_raw(const egis_image_t* img, int index) {
    if (img == NULL || index < 0) return NULL;
#if defined(SUPPORT_KEEP_RAW_16BIT) && defined(__ET7XX__)
    unsigned int img_size =
        img->img_data.raw_width * img->img_data.raw_height * img->img_data.raw_bpp / 8;
    unsigned int offset = index * img_size;
    if (offset + img_size > img->img_data.capacity * 2) return NULL;

    return (img->img_data.bkg_raw_buffer + offset);
#endif
    return NULL;
}

unsigned char* egis_image_get_best_pointer(egis_image_t* img) {
    if (img->img_data.frame_count <= 0) return NULL;

    // For now, just take the last image
    return egis_image_get_pointer(img, img->img_data.frame_count - 1);
}

unsigned char* egis_image_get_bad_pointer(egis_image_t* img, int img_type) {
    switch (img_type) {
        case IMGTYPE_BIN:
            return img->_bad_img_buffer;
#ifdef SUPPORT_KEEP_RAW_16BIT
        case IMGTYPE_RAW:
            return img->img_data.raw_bad;
        case IMGTYPE_BKG:
            egislog_e("No bad background image");
            return img->img_data.raw_bad;
#endif
        default:
            egislog_e("Unknown image type");
            return img->_bad_img_buffer;
    }
}

void egis_image_get_size(egis_image_t* egis_img, int img_type, int* width, int* height, int* bpp) {
    if (egis_img == NULL) return;

    if (width != NULL) {
        *width = egis_img->img_data.format.width;
    }
    if (height != NULL) {
        *height = egis_img->img_data.format.height;
    }
    if (bpp != NULL) {
#ifdef SUPPORT_KEEP_RAW_16BIT
        if (img_type == IMGTYPE_BIN) {
            *bpp = 8;
        } else {
            *bpp = egis_img->img_data.raw_bpp;
        }
#else
        *bpp = 8;
#endif
    }
}

int egis_image_get_buffer_size(egis_image_t* egis_img, int img_type) {
    if (egis_img == NULL) return 0;

    int img_size = egis_img->img_data.format.width * egis_img->img_data.format.height;
    switch (img_type) {
        case IMGTYPE_BIN:
            return img_size;
#ifdef SUPPORT_KEEP_RAW_16BIT
        case IMGTYPE_RAW:
        case IMGTYPE_BKG:
            return img_size * egis_img->img_data.raw_bpp / 8;
#endif
        default:
            egislog_e("wrong img_type");
            return 0;
    }
}

int EGIS_IMAGE_CREATE(egis_image_t* egis_img, int img_width, int img_height,
                      int number_of_image_to_allocate) {
    int keep_t_dummy = FALSE;
    // keep_t_dummy = core_config_get_int(INI_SECTION_SENSOR, KEY_KEEP_T_DUMMY, 0);

    egis_img->img_data.format.width = img_width;
    egis_img->img_data.format.height = img_height;
    egis_img->img_data.frame_count = 0;

    if (keep_t_dummy) {
        img_width += 1;
        img_height += 1;
    }

    egis_img->img_data.capacity = number_of_image_to_allocate * img_width * img_height;
    egis_img->img_data._buffer = plat_alloc(egis_img->img_data.capacity);

#ifdef SUPPORT_KEEP_RAW_16BIT
    egis_img->img_data.raw_bpp = 16;
    egis_img->img_data.raw_width = img_width;
    egis_img->img_data.raw_height = img_height;
    egis_img->img_data.raw_buffer = plat_alloc(egis_img->img_data.capacity * 2);
    egis_img->img_data.raw_bad = plat_alloc(img_width * img_height * 2);
    egis_img->img_data.bkg_raw_buffer = plat_alloc(egis_img->img_data.capacity * 2);
    if (egis_img->img_data.bkg_raw_buffer == NULL) {
        egislog_e("failed to allocate egis_img");
        goto alloc_fail;
    }
#endif

    egis_img->imgdata_ext_info.max_count = number_of_image_to_allocate;
    egis_img->imgdata_ext_info.algoflag_buf =
        plat_alloc(number_of_image_to_allocate * sizeof(ALGO_FLAG_T));

    egis_img->_bad_img_buffer = plat_alloc(img_width * img_height);

    if (egis_img->img_data._buffer == NULL || egis_img->_bad_img_buffer == NULL ||
        egis_img->imgdata_ext_info.algoflag_buf == NULL) {
        egislog_e("failed to allocate egis_img (num = %d)", number_of_image_to_allocate);
        goto alloc_fail;
    }
    return EGIS_OK;

alloc_fail:
    egis_img->img_data.capacity = 0;
    PLAT_FREE(egis_img->img_data._buffer);

    PLAT_FREE(egis_img->_bad_img_buffer);
    egis_img->imgdata_ext_info.max_count = 0;
    PLAT_FREE(egis_img->imgdata_ext_info.algoflag_buf);

#ifdef SUPPORT_KEEP_RAW_16BIT
    PLAT_FREE(egis_img->img_data.raw_buffer);
    PLAT_FREE(egis_img->img_data.raw_bad);
    PLAT_FREE(egis_img->img_data.bkg_raw_buffer);
#endif

    return EGIS_OUT_OF_MEMORY;
}

void EGIS_IMAGE_FREE(egis_image_t* egis_img) {
    PLAT_FREE(egis_img->img_data._buffer);
    PLAT_FREE(egis_img->_bad_img_buffer);
    PLAT_FREE(egis_img->imgdata_ext_info.algoflag_buf);
#ifdef SUPPORT_KEEP_RAW_16BIT
    PLAT_FREE(egis_img->img_data.raw_buffer);
    PLAT_FREE(egis_img->img_data.raw_bad);
    PLAT_FREE(egis_img->img_data.bkg_raw_buffer);
#endif
    memset(egis_img, 0, sizeof(egis_image_t));

    return;
}

void EGIS_IMAGE_RESET_FRAME_COUNT(egis_image_t* egis_img) {
    egis_img->img_data.frame_count = 0;
    memset(egis_img->imgdata_ext_info.algoflag_buf, 0,
           egis_img->imgdata_ext_info.max_count * sizeof(ALGO_FLAG_T));
}

int EGIS_IMAGE_set_algo_flag(egis_image_t* egis_img, int index, int algo_flag) {
    if (egis_img == NULL || index < 0) {
        egislog_e("invalid parameter");
        return EGIS_COMMAND_FAIL;
    }
    egis_img->imgdata_ext_info.algoflag_buf[index] = algo_flag;
    return EGIS_OK;
}

int EGIS_IMAGE_get_algo_flag(egis_image_t* egis_img, int index) {
    if (egis_img == NULL || index < 0) {
        egislog_e("invalid parameter");
        return 0;
    }
    return egis_img->imgdata_ext_info.algoflag_buf[index];
}
