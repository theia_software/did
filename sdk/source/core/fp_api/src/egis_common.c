#include <comdef.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include "algomodule.h"
#include "bsp_tz_log.h"
#include "fp_algomodule.h"
#include "fpc_log.h"
#include "fpc_mem.h"
#include "plat_spi.h"
#include "qsee_log.h"
#include "qsee_spi.h"
#include "tee_internal_api.h"

#define MAX_BUFLEN 1024
#define EGIS_COMMAND_FAIL 3
#define FP_REDUCE_SPI_CALLED
#ifdef FP_CHIPSET_8937
#define SPI_DEVICE_ID QSEE_SPI_DEVICE_6
#else
#define SPI_DEVICE_ID QSEE_SPI_DEVICE_7
#endif

static qsee_spi_config_t egis_spi_config = {
    .spi_bits_per_word = 8,
    .spi_clk_always_on = QSEE_SPI_CLK_NORMAL,
    .spi_clk_polarity = QSEE_SPI_CLK_IDLE_LOW,
    .spi_cs_mode = QSEE_SPI_CS_KEEP_ASSERTED,
    .spi_cs_polarity = QSEE_SPI_CS_ACTIVE_LOW,
    .spi_shift_mode = QSEE_SPI_INPUT_FIRST_MODE,
    .max_freq = 9600000,
};

int bsp_tzspi_open(void) {
#ifdef FP_REDUCE_SPI_CALLED
    return 0;
#else
    int retval = 0;
    retval = qsee_spi_open(SPI_DEVICE_ID);
    return retval;
#endif
}

#ifdef FP_REDUCE_SPI_CALLED
int fp_spi_init(void) {
    int retval = 0;
    retval = qsee_spi_open(SPI_DEVICE_ID);
    if (retval != 0) {
        LOGE("@egis-fps-qsee_spi_open fail:%d", retval);
    }
    return retval;
}

int fp_spi_exit(void) {
    int retval = 0;
    retval = qsee_spi_close(SPI_DEVICE_ID);
    if (retval != 0) {
        LOGE("@egis-fps-qsee_spi_close fail:%d", retval);
    }
    return retval;
}
#endif

void bsp_tzlog_printf(int level, const char* format, ...) {
    char buffer[MAX_BUFLEN];
    uint8_t l = (uint8_t)QSEE_LOG_MSG_DEBUG;
    va_list vl;
    if (format == NULL) {
        return;
    }
    switch (level) {
        case BSP_TZ_LOG_LEVEL_NONE:
            return;
        case BSP_TZ_LOG_LEVEL_ERROR:
            l = (uint8_t)QSEE_LOG_MSG_FATAL;
            break;
        case BSP_TZ_LOG_LEVEL_INFO:
        case BSP_TZ_LOG_LEVEL_DEBUG:
            l = (uint8_t)QSEE_LOG_MSG_DEBUG;
            break;
        default:
            return;
    }
    va_start(vl, format);
    vsnprintf(buffer, MAX_BUFLEN, format, vl);
    va_end(vl);
    QSEE_LOG(l, "%s", buffer);
    return;
}

int bsp_tzspi_read(bsp_tzspi_transfer_t* p_read_info) {
    int retval = 0;
    qsee_spi_transaction_info_t r = {
        .buf_addr = NULL,
        .buf_len = 0,
        .total_bytes = 0,
    };
    if (NULL == p_read_info) {
        return EGIS_COMMAND_FAIL;
    }
    r.buf_addr = p_read_info->buf_addr;
    r.buf_len = p_read_info->buf_len;
    retval = qsee_spi_read(SPI_DEVICE_ID, &egis_spi_config, &r);
    if (retval != 0) {
        LOGE("@egis-fps-qsee_spi_read fail:%d", retval);
    }
    p_read_info->total_len = r.total_bytes;
    return retval;
}

int bsp_tzspi_write(bsp_tzspi_transfer_t* p_write_info) {
    int retval = 0;
    qsee_spi_transaction_info_t t = {
        .buf_addr = NULL,
        .buf_len = 0,
        .total_bytes = 0,
    };
    if (NULL == p_write_info) {
        return EGIS_COMMAND_FAIL;
    }
    t.buf_addr = p_write_info->buf_addr;
    t.buf_len = p_write_info->buf_len;
    retval = qsee_spi_write(SPI_DEVICE_ID, &egis_spi_config, &t);
    if (retval != 0) {
        LOGE("@egis-fps-qsee_spi_write fail:%d", retval);
    }
    p_write_info->total_len = t.total_bytes;
    return retval;
}

int bsp_tzspi_full_duplex(bsp_tzspi_transfer_t* p_write_info, bsp_tzspi_transfer_t* p_read_info) {
    int retval = 0;
    qsee_spi_transaction_info_t t = {
        .buf_addr = NULL,
        .buf_len = 0,
        .total_bytes = 0,
    };
    qsee_spi_transaction_info_t r = {
        .buf_addr = NULL,
        .buf_len = 0,
        .total_bytes = 0,
    };
    if ((NULL == p_write_info) || (NULL == p_read_info)) {
        return EGIS_COMMAND_FAIL;
    }

    t.buf_addr = p_write_info->buf_addr;
    t.buf_len = p_write_info->buf_len;
    r.buf_addr = p_read_info->buf_addr;
    r.buf_len = p_read_info->buf_len;
    retval = qsee_spi_full_duplex(SPI_DEVICE_ID, &egis_spi_config, &t, &r);
    if (retval != 0) {
        LOGE("@egis-fps-qsee_spi_full_duplex fail:%d", retval);
    }
    p_read_info->total_len = t.total_bytes;
    p_write_info->total_len = t.total_bytes;
    return retval;
}

int bsp_set_spi_clk(uint32_t clk) {
    uint32_t originalClk = egis_spi_config.max_freq;
    egis_spi_config.max_freq = clk;
    return originalClk;
}

int bsp_tzspi_close(void) {
#ifdef FP_REDUCE_SPI_CALLED
    return 0;
#else
    int ret;
    ret = qsee_spi_close(SPI_DEVICE_ID);
    return ret;
#endif
}

unsigned long long FP_Gettime(void);
void mdelay(uint32_t msecs) {
    uint32_t times_now, times_start = FP_Gettime();
    do {
        times_now = FP_Gettime();
    } while ((times_now - times_start) < msecs);
    return;
}

int fingerprint_tee_nav_set_poll_count(int count) {
    return 0;
}
