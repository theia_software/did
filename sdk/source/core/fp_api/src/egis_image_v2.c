
#include "algomodule.h"
#include "egis_definition.h"
#include "fp_algomodule.h"
#include "object_def_image.h"
#include "plat_log.h"
#include "plat_mem.h"

#define LOG_TAG "RBS"

#ifdef G3PLUS_MATCHER
#define COMPRESS_BUFFER
#endif
#ifdef COMPRESS_BUFFER
#include "image_cut.h"
#include "isensor_api.h"
#endif

static int g_keep_param_index = 0;
static rbs_obj_image_v1_0_t** g_param_keeper_img_obj = NULL;

rbs_obj_image_v1_0_t* EGIS_IMAGE_get_param_keeper() {
    return g_param_keeper_img_obj[g_keep_param_index];
}

#define TOTAL_IMG_TYPE 3
static int g_num_per_image_type = 0;
static int _index_to_arrayidx(int index, int image_type) {
    int arrayidx;
    switch (image_type) {
        case IMGTYPE_BIN:
            arrayidx = index;
            break;
        case IMGTYPE_RAW:
            arrayidx = index + g_num_per_image_type;
            break;
        case IMGTYPE_BKG:
            arrayidx = index + g_num_per_image_type * 2;
            break;
        default:
            egislog_e("not supported image type %d", image_type);
            arrayidx = index;
            break;
    }
    return arrayidx;
}

static int _allocate_img_array(rbs_obj_image_v1_0_t** pp_image_obj, int num, int image_type,
                               int width, int height, int bpp) {
    int i, obj_size;
    rbs_obj_image_v1_0_t tmp_image_obj;
    RBSOBJ_set_IMAGE_v1_0((&tmp_image_obj), image_type, width, height, bpp);
    obj_size = RBSOBJ_get_obj_total_size((&tmp_image_obj));
    egislog_d("%s [%d] obj_size=%d", __func__, image_type, obj_size);

    for (i = 0; i < num; i++) {
        *pp_image_obj = plat_alloc(obj_size);
        RBS_CHECK_IF_NULL(*pp_image_obj, EGIS_OUT_OF_MEMORY);
        RBSOBJ_set_IMAGE_v1_0((*pp_image_obj), image_type, width, height, bpp);
        pp_image_obj++;
    }
    return EGIS_OK;
}

void _set_image_default_param(rbs_obj_image_v1_0_t* obj){
//    obj->algo_flag = FP_LIB_ALGO_FLAG_CAPTURE_TOO_FAST;
    obj->algo_result = 99;
    obj->reject_reason = 99;

    RBSOBJ_set_IMAGE_param_v2(obj, Group1xx, "G100", 0);
    RBSOBJ_set_IMAGE_param_v2(obj, Group2xx, "G200", 0);
    RBSOBJ_set_IMAGE_param_v2(obj, Group3xx, "G300", 0);
    RBSOBJ_set_IMAGE_param_v2(obj, Group4xx, "G400", 0);
    RBSOBJ_set_IMAGE_param_v2(obj, sig, "sig", 0);
    RBSOBJ_set_IMAGE_param_v2(obj, all_adc_dac, "dac", 0);
#ifdef ALGO_GEN_7
    RBSOBJ_set_IMAGE_param_v2(obj, afm, "afm", 0);
    // RBSOBJ_set_IMAGE_param_v2(obj, ta_obj_size, "objs", sizeof(rbs_obj_image_v1_0_t));
#endif

    obj->STRING_AREA_START_OFFSET_ADDR = \
        (uint8_t*)(&obj->Group1xx) - (uint8_t*)(&obj->imgtype);
    obj->STRING_AREA_END_OFFSET_ADDR = 
        (uint8_t*)(&obj->STRING_AREA_END_TAG) - (uint8_t*)(&obj->imgtype);
}

int EGIS_IMAGE_CREATE(egis_image_t* egis_img, int img_width, int img_height,
                      int num_per_image_type) {
    int i, obj_count, retval;

    if (g_param_keeper_img_obj == NULL) {
        int obj_cnt = TOTAL_IMG_TYPE * num_per_image_type;
        egislog_d("%s, obj_cnt=%d", __func__, obj_cnt);
        g_param_keeper_img_obj =
            (rbs_obj_image_v1_0_t**)plat_alloc(obj_cnt * sizeof(rbs_obj_image_v1_0_t*));
        memset(g_param_keeper_img_obj, 0, obj_cnt * sizeof(rbs_obj_image_v1_0_t*));
        for (i = 0; i < obj_cnt; i++) {
            g_param_keeper_img_obj[i] =
                (rbs_obj_image_v1_0_t*)plat_alloc(sizeof(rbs_obj_image_v1_0_t));
            g_param_keeper_img_obj[i]->param_type = 1;
            _set_image_default_param(g_param_keeper_img_obj[i]);
        }
    }

    egis_img->img_data.format.width = img_width;
    egis_img->img_data.format.height = img_height;
    egis_img->img_data.frame_count = 0;

    obj_count = TOTAL_IMG_TYPE * num_per_image_type;
    egis_img->img_obj_list = plat_alloc(obj_count * sizeof(*(egis_img->img_obj_list)));
    RBS_CHECK_IF_NULL(egis_img->img_obj_list, EGIS_OUT_OF_MEMORY);
    memset(egis_img->img_obj_list, 0, obj_count * sizeof(*(egis_img->img_obj_list)));

    rbs_obj_image_v1_0_t** pp_image_obj = egis_img->img_obj_list;
    _allocate_img_array(pp_image_obj, num_per_image_type, IMGTYPE_BIN, img_width, img_height,
                        fp_getAlgoBpp());

    pp_image_obj += num_per_image_type;
    _allocate_img_array(pp_image_obj, num_per_image_type, IMGTYPE_RAW, img_width, img_height, 16);
    pp_image_obj += num_per_image_type;
    retval = _allocate_img_array(pp_image_obj, num_per_image_type, IMGTYPE_BKG, img_width,
                                 img_height, 16);
    if (retval != EGIS_OK) {
        egislog_e("%s out of memory, %d:%d %d", __func__, img_width, img_height,
                  num_per_image_type);
        EGIS_IMAGE_FREE(egis_img);
        return EGIS_OUT_OF_MEMORY;
    }
    g_num_per_image_type = num_per_image_type;
    return EGIS_OK;
}

void EGIS_IMAGE_FREE(egis_image_t* egis_img) {
    if (egis_img->img_obj_list == NULL) {
        g_num_per_image_type = 0;
        return;
    }
    int obj_count = TOTAL_IMG_TYPE * g_num_per_image_type;
    egislog_d("%s, obj_count=%d", __func__, obj_count);
    int i;
    for (i = 0; i < obj_count; i++) {
        PLAT_FREE(egis_img->img_obj_list[i]);
        PLAT_FREE(g_param_keeper_img_obj[i]);
    }
    PLAT_FREE(egis_img->img_obj_list);
    PLAT_FREE(g_param_keeper_img_obj);
    memset(egis_img, 0, sizeof(egis_image_t));
    g_num_per_image_type = 0;
    return;
}

int egis_image_is_allocated(const egis_image_t* img) {
    return g_num_per_image_type > 0 ? 1 : 0;
}

int egis_image_set_buffer(const egis_image_t* img, int index, int image_type, uint8_t* src_buf,
                          int buf_size) {
    if (img == NULL || index < 0 || index >= g_num_per_image_type) return EGIS_COMMAND_FAIL;

    int arrayidx = _index_to_arrayidx(index, image_type);
    unsigned char* dest = RBSOBJ_get_payload_pointer(img->img_obj_list[arrayidx]);
    int payload_size = RBSOBJ_get_payload_size(img->img_obj_list[arrayidx]);
    if (buf_size < payload_size) {
        egislog_e("set buffer size %d < %d", buf_size, payload_size);
        return EGIS_COMMAND_FAIL;
    } else {
        memcpy(dest, src_buf, payload_size);
#ifdef COMPRESS_BUFFER
        if (image_type == IMGTYPE_BIN) {
            const uint8_t VER = 0x1;
            uint8_t my_info[8] = {0xE,         'G', 'I', 'S', VER, (uint8_t)(20 * 2),
                                  (uint8_t)20, 0};  // Must be ended with zero
            int btemp;
            isensor_get_int(PARAM_INT_TEMPERATURE, &btemp);
            my_info[5] = (uint8_t)(btemp * 2);
            my_info[6] = (uint8_t)(btemp);
            compress_info(dest, img->img_obj_list[arrayidx]->width,
                          img->img_obj_list[arrayidx]->height, my_info);
        }
#endif
        return EGIS_OK;
    }
}

int egis_image_erase_buffer(const egis_image_t* img, int index, int image_type) {
    RBS_CHECK_IF_NULL(img, EGIS_INCORRECT_PARAMETER);
    int arrayidx = _index_to_arrayidx(index, image_type);
    unsigned char* dest = RBSOBJ_get_payload_pointer(img->img_obj_list[arrayidx]);
    int payload_size = RBSOBJ_get_payload_size(img->img_obj_list[arrayidx]);
    memset(dest, 0, payload_size);
    return EGIS_OK;
}

unsigned char* egis_image_get_pointer(const egis_image_t* img, int index) {
    if (img == NULL || index < 0 || index >= g_num_per_image_type) return NULL;

    int arrayidx = _index_to_arrayidx(index, IMGTYPE_BIN);
    return RBSOBJ_get_payload_pointer(img->img_obj_list[arrayidx]);
}

unsigned char* egis_image_get_pointer_raw(const egis_image_t* img, int index) {
    if (img == NULL || index < 0 || index >= g_num_per_image_type) return NULL;

    int arrayidx = _index_to_arrayidx(index, IMGTYPE_RAW);
    return RBSOBJ_get_payload_pointer(img->img_obj_list[arrayidx]);
}

unsigned char* egis_image_get_bkg_pointer_raw(const egis_image_t* img, int index) {
    if (img == NULL || index < 0 || index >= g_num_per_image_type) return NULL;

    int arrayidx = _index_to_arrayidx(index, IMGTYPE_BKG);
    return RBSOBJ_get_payload_pointer(img->img_obj_list[arrayidx]);
}

unsigned char* egis_image_get_best_pointer(egis_image_t* img) {
    if (img->img_data.frame_count <= 0) return NULL;

    // For now, just take the last image
    return egis_image_get_pointer(img, img->img_data.frame_count - 1);
}

unsigned char* egis_image_get_bad_pointer(egis_image_t* img, int img_type) {
    switch (img_type) {
        case IMGTYPE_BIN:
            break;
        case IMGTYPE_RAW:
            break;
        case IMGTYPE_BKG:
            break;
        default:
            egislog_e("Unknown image type");
            return NULL;  // img->_bad_img_buffer;
    }
    egislog_e("No bad image [%d]", img_type);
    return NULL;
}

void egis_image_get_size(egis_image_t* egis_img, int img_type, int* width, int* height, int* bpp) {
    if (egis_img == NULL) return;

    if (width != NULL) {
        *width = egis_img->img_data.format.width;
    }
    if (height != NULL) {
        *height = egis_img->img_data.format.height;
    }

    if (bpp == NULL) {
        return;
    }

    if (img_type == IMGTYPE_BIN) {
        *bpp = fp_getAlgoBpp();
    } else {
        *bpp = 16;
    }
}

int egis_image_set_keep_param_index(int index) {
    if (index < 0) {
        egislog_e("invalid parameter");
        return EGIS_COMMAND_FAIL;
    }
    if (index >= g_num_per_image_type) {
        index = g_num_per_image_type - 1;
    }
    g_keep_param_index = index;
    egislog_d("%s g_keep_param_index=%d", __func__, g_keep_param_index);
    return EGIS_OK;
}

rbs_obj_image_v1_0_t* egis_image_copy_params(int index, int image_type) {
    int arrayidx = 0, arrayidx_raw, arrayidx_bkg;

    switch (image_type) {
        case IMGTYPE_BIN:
            arrayidx = _index_to_arrayidx(index, IMGTYPE_BIN);
            egislog_d("%s IMGTYPE_BIN [%d]", __func__, arrayidx);
            arrayidx_raw = _index_to_arrayidx(index, IMGTYPE_RAW);
            egislog_d("%s COPY TO IMGTYPE_RAW [%d]", __func__, arrayidx_raw);
            RBSOBJ_copy_IMAGE_params(g_param_keeper_img_obj[arrayidx],
                                     g_param_keeper_img_obj[arrayidx_raw]);
            arrayidx_bkg = _index_to_arrayidx(index, IMGTYPE_BKG);
            egislog_d("%s COPY TO IMGTYPE_BKG [%d]", __func__, arrayidx_bkg);
            RBSOBJ_copy_IMAGE_params(g_param_keeper_img_obj[arrayidx],
                                     g_param_keeper_img_obj[arrayidx_bkg]);
            break;
        case IMGTYPE_RAW:
            arrayidx = _index_to_arrayidx(index, IMGTYPE_RAW);
            egislog_d("%s IMGTYPE_RAW [%d]", __func__, arrayidx);
            break;
        case IMGTYPE_BKG: {
#ifdef ONLY_REQUEST_IMGTYPE_BKG
            int arrayidx_8BIT = _index_to_arrayidx(index, IMGTYPE_BIN);
            arrayidx = _index_to_arrayidx(index, IMGTYPE_BKG);
            RBSOBJ_copy_IMAGE_params(g_param_keeper_img_obj[arrayidx_8BIT],
                                     g_param_keeper_img_obj[arrayidx]);
#else
            arrayidx = _index_to_arrayidx(index, IMGTYPE_BKG);
#endif
            egislog_d("%s IMGTYPE_BKG [%d]", __func__, arrayidx);
            break;
        }
        default:
            egislog_e("%s IMGTYPE_XXX(%d) FAIL", __func__, image_type);
            break;
    }

    return g_param_keeper_img_obj[arrayidx];
}

int egis_image_get_buffer_size(egis_image_t* egis_img, int img_type) {
    if (g_num_per_image_type <= 0) return 0;

    int arrayidx = _index_to_arrayidx(0, img_type);
    return RBSOBJ_get_payload_size(egis_img->img_obj_list[arrayidx]);
}

void EGIS_IMAGE_RESET_FRAME_COUNT(egis_image_t* egis_img) {
    if (egis_img->img_data.frame_count == 0) {
        return;
    }
    int i;
    egislog_v("%s, %d -> 0", __func__, egis_img->img_data.frame_count);
    egis_img->img_data.frame_count = 0;
    if (g_param_keeper_img_obj != NULL && g_num_per_image_type > 0) {
        int obj_cnt = TOTAL_IMG_TYPE * g_num_per_image_type;
        egislog_v("%s, obj_cnt=%d", __func__, obj_cnt);
        for (i = 0; i < obj_cnt; i++) {
            RBSOBJ_copy_IMAGE_params(g_param_keeper_img_obj[i], NULL);
            _set_image_default_param(g_param_keeper_img_obj[i]);
        }
    }
}

int EGIS_IMAGE_set_algo_flag(egis_image_t* egis_img, int index, int algo_flag) {
    if (egis_img == NULL || index < 0) {
        egislog_e("invalid parameter");
        return EGIS_COMMAND_FAIL;
    }

    int arrayidx = _index_to_arrayidx(index, IMGTYPE_BIN);
    egislog_d("%s [%d] algo_flag=%d", __func__, arrayidx, algo_flag);
    RBSOBJ_set_IMAGE_param(egis_img->img_obj_list[arrayidx], algo_flag, algo_flag);

    arrayidx = _index_to_arrayidx(index, IMGTYPE_RAW);
    egislog_d("%s [%d] algo_flag=%d", __func__, arrayidx, algo_flag);
    RBSOBJ_set_IMAGE_param(egis_img->img_obj_list[arrayidx], algo_flag, algo_flag);

    arrayidx = _index_to_arrayidx(index, IMGTYPE_BKG);
    egislog_d("%s [%d] algo_flag=%d", __func__, arrayidx, algo_flag);
    RBSOBJ_set_IMAGE_param(egis_img->img_obj_list[arrayidx], algo_flag, algo_flag);
    // egis_img->imgdata_ext_info.algoflag_buf[index] = algo_flag;

    return EGIS_OK;
}

int EGIS_IMAGE_get_algo_flag(egis_image_t* egis_img, int index) {
    if (egis_img == NULL || index < 0) {
        egislog_e("invalid parameter");
        return 0;
    }
    // return egis_img->imgdata_ext_info.algoflag_buf[index];
    int arrayidx = _index_to_arrayidx(index, IMGTYPE_BIN);

    return RBSOBJ_get_IMAGE_param(egis_img->img_obj_list[arrayidx], algo_flag);
}
