#include "common_definition.h"
#include "fp_algomodule.h"
#include "algomodule.h"
#include "egis_algomodule.h"
#include "tee_internal_api.h"

#ifdef NOT_HUA_PROJ
#include "plat_mem.h"
#else
#include "TA_FpLibs.h"
#include "fpc_log.h"
#include "fpc_mem.h"
// TODO: check if gAlgorithm is really needed
// ialgorithm_t gAlgorithm;
#endif

//#define MAX_TEMPLATE 5
#define MAX_TEMPLATE SUPPORT_MAX_ENROLL_COUNT
fp_image_quality_t* gQuality = NULL;
fp_image_quality_t* gQuality_temp = NULL;
static fp_template_t* candidate_templates[MAX_TEMPLATE] = {0};
static int candidate_templates_size = 0;

#define NO_WINNING_INDEX -1
static int gwinning_index = NO_WINNING_INDEX;
#define WINNER_INDEX_IS_VALID (gwinning_index != NO_WINNING_INDEX)

int fp_initAlgAndPPLib() {
    int retval = FP_LIB_OK;

    retval = egis_fp_initAlgAndPPLib(0);
    gQuality = plat_alloc(sizeof(fp_image_quality_t));
    if (NULL == gQuality) {
        LOGE("@egis-hfai-ERROR_MEMORY");
        return FP_LIB_ERROR_MEMORY;
    }
    gQuality_temp = plat_alloc(sizeof(fp_image_quality_t));
    if (NULL == gQuality_temp) {
        LOGE("@egis-hfai-ERROR_MEMORY");
        plat_free(gQuality);
        gQuality = NULL;
        return FP_LIB_ERROR_MEMORY;
    }
    return retval;
}

int fp_initCheckTemplateVersion(uint32_t* templateVersion) {
    int retval = FP_LIB_OK;
    if (NULL == templateVersion) {
        LOGE("@egis-hfai-ERROR_MEMORY");
        return FP_LIB_ERROR_MEMORY;
    }
    retval = egis_fp_initCheckTemplateVersion(templateVersion);
    LOGD("@egis-hfai-ret:%d", retval);
    return retval;
}

uint32_t fp_getCurrentTemplateVersion() {
    return EGIS_TEMPLATE_VERSION;
}
void fp_deInitAlg() {
    if (NULL != gQuality) {
        plat_free(gQuality);
        gQuality = NULL;
    }
    if (NULL != gQuality_temp) {
        plat_free(gQuality_temp);
        gQuality_temp = NULL;
    }
    egis_fp_deInitAlg();
}

int fp_identifyStart(uint32_t need_liveness_authentication) {
    int retval = FP_LIB_OK;

    retval = egis_fp_identifyStart(need_liveness_authentication, candidate_templates,
                                   candidate_templates_size);
    LOGD("@egis-hfai-ret:%d", retval);
    return retval;
}

void fp_identifyFinish() {
    egis_fp_identifyFinish();
}
int fp_startEnroll() {
    int retval = FP_LIB_OK;
    retval = egis_fp_startEnroll();
    LOGD("@egis-hfai-ret:%d", retval);
    if (retval != FP_LIB_OK) {
        LOGE("@egis-hfai-EROLL_START_FAIL");
        retval = FP_TA_ERROR_EROLL_START_FAIL;
    }
    return retval;
}

int fp_egis_enroll(void* image, fp_lib_enroll_data_t* enroll_data_t, int size, void* swipe_info) {
    int result = FP_LIB_OK;

    if ((NULL == image) || (NULL == enroll_data_t) || (size < 0)) {
        LOGE("@egis-hfai-ERROR_PARAMETER");
        return FP_LIB_ERROR_PARAMETER;
    }
    if (size > 0 && NULL == candidate_templates[0]) {
        LOGE("@egis-hfai-candidate-template is NULL!");
        return result;
    }
    result =
        egis_fp_identifyImage_enroll(image, enroll_data_t, candidate_templates, size, swipe_info);

    return result;
}
int fp_identifyImage_enroll(void* image, fp_lib_enroll_data_t* enroll_data_t, int size) {
    int result = FP_LIB_OK;

    if ((NULL == image) || (NULL == enroll_data_t) || (size < 0)) {
        LOGE("@egis-hfai-ERROR_PARAMETER");
        return FP_LIB_ERROR_PARAMETER;
    }
    if (NULL == candidate_templates[0]) {
        LOGE("@egis-hfai-candidate-template is NULL!");
        return result;
    }
    result = egis_fp_identifyImage_enroll(image, enroll_data_t, candidate_templates, size, NULL);

    LOGD("@egis-hfai-ret:%d", result);
    return result;
}

int fp_updateEnrolData(fp_lib_enroll_data_t* fp_enrol_data, void* image) {
    int status = FP_LIB_OK;
    // signed char immobile;
    if ((NULL == fp_enrol_data) || (NULL == image)) {
        LOGE("@egis-hfai-ERROR_PARAMETER");
        return FP_LIB_ERROR_PARAMETER;
    }

    status = egis_fp_updateEnrolData(fp_enrol_data, image);
    LOGD("@egis-hfai-ret:%d", status);
    if (status != FP_LIB_OK) {
        if ((status < FP_LIB_ENROLL_SUCCESS) ||
            (status > FP_LIB_ENROLL_FAIL_LOW_QUALITY_AND_LOW_COVERAGE)) {
            status = FP_LIB_ENROLL_FAIL_NONE;
        }
        fp_enrol_data->result = FP_LIB_ENROLL_FAIL_NONE;
    }
    return FP_LIB_OK;
}

int fp_mergeEnrollTemp(fp_lib_enroll_data_t* enrol_data, void* img) {
    return egis_fp_mergeEnrollTemp(enrol_data, img);
}

int fp_setFingerState(unsigned int finger_on) {
    return egis_fp_setFingerState(finger_on);
}

int fp_finishEnroll(fp_lib_template_t* template) {
    if (NULL == template) {
        LOGE("@egis-hfai-ERROR_PARAMETER");
        return FP_LIB_ERROR_PARAMETER;
    }
#ifdef PACK_TEMPLATE_BUFFER
    fp_lib_template_t ptemplate;
    int result = FP_LIB_OK;
    egis_fp_getTemplateSize(&ptemplate.size);
    LOGD("@egis-hfai-ptemplate.size %d", ptemplate.size);
    ptemplate.tpl = plat_alloc(ptemplate.size);
    if (NULL == ptemplate.tpl) {
        LOGE("@egis-hfai-ERROR_MEMORY");
        result = FP_LIB_ERROR_MEMORY;
        return result;
    }
    result = egis_fp_finishEnroll(&ptemplate);
    LOGD("@egis-hfai-ptemplate.2size %d", ptemplate.size);
    LOGD("@egis-hfai-ret:%d", result);
    if (FP_LIB_OK != result) {
        LOGE("@egis-hfai-EROLL_GET_TEMPLATE_FAIL");
        if (ptemplate.tpl != NULL) {
            plat_free(ptemplate.tpl);
            ptemplate.tpl = NULL;
        }
        return FP_TA_ERROR_EROLL_GET_TEMPLATE_FAIL;
    }
    result = fp_packTemplate(template, &ptemplate);
    LOGD("@egis-hfai-ret:%d", result);
    if (ptemplate.tpl != NULL) {
        plat_free(ptemplate.tpl);
        ptemplate.tpl = NULL;
    }
    return result;
#else
    egis_fp_getTemplateSize(&template->size);
    template->tpl = plat_alloc(template->size);
    if (NULL == template->tpl) {
        LOGE("@egis-hfai-ERROR_MEMORY2");
        return FP_LIB_ERROR_MEMORY;
    }
    int result = egis_fp_finishEnroll(template);
    if (FP_LIB_OK != result) {
        LOGE("@egis-hfai-EROLL_GET_TEMPLATE_FAIL2");
        if (template->tpl != NULL) {
            plat_free(template->tpl);
            template->tpl = NULL;
        }
        return FP_TA_ERROR_EROLL_GET_TEMPLATE_FAIL;
    }
    return FP_LIB_OK;
#endif
}

int fp_packTemplate(fp_lib_template_t* template_t, void* unpack_tplt) {
    if ((NULL == template_t) || (NULL == unpack_tplt)) {
        LOGE("@egis-hfai-ERROR_PARAMETER");
        return FP_LIB_ERROR_PARAMETER;
    }
    int result = FP_LIB_OK;
    fp_template_t* tplt;
    tplt = (fp_template_t*)unpack_tplt;
    template_t->size = tplt->size;
    LOGD("@egis-hfai-ptemplate.3size %d", template_t->size);
    template_t->tpl = plat_alloc(template_t->size);
    if (NULL == template_t->tpl) {
        LOGE("@egis-hfai-ERROR_MEMORY");
        result = FP_LIB_ERROR_MEMORY;
        return result;
    }
    result = egis_fp_packTemplate(tplt, (uint8_t*)template_t->tpl);
    LOGD("@egis-hfai-ret:%d", result);
    if (FP_LIB_OK != result) {
        LOGE("@egis-hfai-EROLL_PACK_TEMPLATE_FAIL");
        plat_free(template_t->tpl);
        template_t->tpl = NULL;
        return FP_TA_ERROR_EROLL_PACK_TEMPLATE_FAIL;
    }
    return result;
}

int fp_unpackTemplate(fp_lib_template_t* tplt, int* candidate_size_p) {
    int result = FP_LIB_OK;
    uint32_t DEFAULT_TPLT_SIZE = 0;
    if ((NULL == tplt) || (NULL == candidate_size_p)) {
        LOGE("@egis-hfai-ERROR_PARAMETER");
        return FP_LIB_ERROR_PARAMETER;
    }
    fp_deleteTemplate(*candidate_size_p);
    if ((tplt->tpl != NULL) && (tplt->size > DEFAULT_TPLT_SIZE)) {
        result = egis_fp_unpackTemplate(tplt, candidate_templates, *candidate_size_p);
        LOGD("@egis-hfai-ret:%d", result);
        (*candidate_size_p)++;
        candidate_templates_size = *candidate_size_p;
    }
    return result;
}

int fp_identifyImage_identify(void* image, fp_lib_identify_data_t* fp_identify_data_t, int size) {
    int winning_index = -1;
    int score = 0;
    int result = FP_LIB_OK;
    int candidate_index = -1;
    if ((NULL == image) || (NULL == fp_identify_data_t)) {
        LOGE("@egis-hfai-ERROR_PARAMETER");
        return FP_LIB_ERROR_PARAMETER;
    }
    result = egis_fp_identifyImage_identify(image, fp_identify_data_t, &candidate_index);
    LOGD("@egis-hfai-ret:%d", result);
    fp_identify_data_t->index = candidate_index;
    winning_index = fp_identify_data_t->index;

    if (winning_index >= MAX_TEMPLATE) {
        LOGE("@egis-hfai-ERROR_MEMORY");
        return FP_LIB_ERROR_MEMORY;
    }
    score = fp_identify_data_t->score;
    if (fp_identify_data_t->result == FP_LIB_IDENTIFY_MATCH && winning_index >= 0 &&
        winning_index < size) {
        gwinning_index = winning_index;
        fp_identify_data_t->result = FP_LIB_IDENTIFY_MATCH_UPDATED_TEMPLATE;
    }
    return FP_LIB_OK;
}

int fp_deleteTemplate(int index) {
    int result = FP_LIB_OK;
    LOGD("@HFAI:%d", index);
    if (index >= MAX_TEMPLATE) {
        LOGE("@egis-hfai-ERROR_MEMORY");
        return FP_LIB_ERROR_MEMORY;
    }
    if (NULL != candidate_templates[index]) {
        result = egis_fp_deleteTemplate(candidate_templates, index);
        LOGD("@egis-hfai-ret:%d", result);
        candidate_templates[index] = NULL;
    }
    return result;
}

#define TEMPLATE_NEED_UPDATE 1
#define TEMPLATE_NOT_UPDATE 0
int fp_identifyUpdateTemplate(void** unpack_tplt, uint8_t* isTemplateUpdate, void* image) {
    int result = FP_LIB_OK;
    unsigned char isUpdate = 0;

    if ((NULL == unpack_tplt) || (NULL == isTemplateUpdate)) {
        LOGE("@egis-hfai-ERROR_PARAMETER");
        return FP_LIB_ERROR_PARAMETER;
    }

    if (*unpack_tplt == NULL && candidate_templates[gwinning_index] != NULL) {
        LOGD("@egis *unpack_tplt is from buffer");
        *unpack_tplt = (void*)candidate_templates[gwinning_index];
    } else if (*unpack_tplt == NULL) {
        LOGE("@egis *unpack_tplt is NULL");
        return FP_LIB_ERROR_PARAMETER;
    }

    result = egis_fp_identifyUpdateTemplate(unpack_tplt, &isUpdate);
    if (result != FP_LIB_OK) {
        LOGE("@egis UpdateTemplate Fail, ret = %d", result);
        goto exit;
    }
    if (isUpdate && !WINNER_INDEX_IS_VALID) {
        LOGE("winner index is wrong! %d", gwinning_index);
        return FP_LIB_ERROR_INVALID_FINGERID;
    }

    // Append (try-learning)
    egis_image_t* egis_image = (egis_image_t*)image;
    if (egis_image != NULL && isUpdate) {
        fp_template_t* winner_fp_template = *unpack_tplt;
        egis_fp_appendImgIntoTemp(egis_image, winner_fp_template->tpl,
                                  (int*)&(winner_fp_template->size));
    }

    if (isUpdate) {
        *isTemplateUpdate = TEMPLATE_NEED_UPDATE;
    } else {
        *isTemplateUpdate = TEMPLATE_NOT_UPDATE;
    }

exit:
    gwinning_index = NO_WINNING_INDEX;
    return result;
}

//#if defined(ALGO_GEN_7)
//int fp_identifyUpdateNotMatchTemplate(void** unpack_tplt, uint8_t* isTemplateUpdate, int index) {
//    int result = FP_LIB_OK;
//    unsigned char isUpdate = 0;
//
//    LOGD("@egis fp_identifyUpdateNotMatchTemplate index = %d", index);
//
//    if (index == gwinning_index) {
//        LOGE("@egis matched index index %d == gwinning_index %d, skip", index, gwinning_index);
//        goto exit;
//    }
//
//    if ((NULL == unpack_tplt) || (NULL == isTemplateUpdate)) {
//        LOGE("@egis-hfai-ERROR_PARAMETER");
//        return FP_LIB_ERROR_PARAMETER;
//    }
//
//    if (*unpack_tplt == NULL && candidate_templates[index] != NULL) {
//        LOGD("@egis *unpack_tplt is from buffer");
//        *unpack_tplt = (void*)candidate_templates[index];
//    } else if (*unpack_tplt == NULL) {
//        LOGE("@egis *unpack_tplt is NULL");
//        return FP_LIB_ERROR_PARAMETER;
//    } else if (*unpack_tplt != NULL) {
//        LOGE("@egis unpack_tplt is not NULL");
//        // return FP_LIB_ERROR_PARAMETER;
//    }
//
//    result = egis_fp_identifyUpdateNotMatchTemplate(unpack_tplt, &isUpdate, index);
//    if (result != FP_LIB_OK) {
//        LOGE("@egis UpdateTemplate Fail, ret = %d", result);
//        goto exit;
//    }
//
//    // if (isUpdate && !WINNER_INDEX_IS_VALID) {
//    //	LOGE("winner index is wrong! %d", gwinning_index);
//    //	return FP_LIB_ERROR_INVALID_FINGERID;
//    //}
//
//    // Append (try-learning)
//    // egis_image_t* egis_image = (egis_image_t*)image;
//    // if (egis_image != NULL && isUpdate) {
//    //	fp_template_t* winner_fp_template = *unpack_tplt;
//    //	egis_fp_appendImgIntoTemp(egis_image, winner_fp_template->tpl,
//    //(int*)&(winner_fp_template->size));
//    //}
//
//    if (isUpdate) {
//        *isTemplateUpdate = TEMPLATE_NEED_UPDATE;
//    } else {
//        *isTemplateUpdate = TEMPLATE_NOT_UPDATE;
//    }
//
//exit:
//    // gwinning_index = NO_WINNING_INDEX;
//    return result;
//}
//#endif

void fp_getAlgVersion(uint8_t* version, uint32_t version_len) {
    if (NULL == version || version_len == 0) {
        LOGE("@egis-hfai-ERROR_PARAMETER");
        return;
    }
    LOGD("@egis-hfai len=%d", version_len);
    egis_fp_getAlgVersion(version, &version_len);
    return;
}

// fpc_tac_get_alg_security_thr(&g_iStatsSecurityScore)
int fp_getAlgSecurityThr(uint32_t* data) {
    if (NULL == data) {
        LOGE("@egis-hfai-ERROR_PARAMETER");
        return FP_LIB_ERROR_PARAMETER;
    }
    return FP_LIB_OK;
}

int fp_getAlgoBpp() {
    return egis_fp_getAlgoBpp();
}

int fp_set_verify_finger_info(int verify_count, int verified_count)
{
    return egis_fp_set_verify_finger_info(verify_count, verified_count);
}
