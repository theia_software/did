#define __ET538__

#ifdef __ET538__
#define EGIS_CMD_ERROR -1
#define EGIS_CMD_OK 0
/*EGIS_538 function declarations*/
static int egis_tzspi_init(void);
static int egis_tzspi_close(void);
static int egis_check_hw_id(uint32_t* buf);
static int egis_read_one_reg(uint8_t addr, uint8_t* value);
static int egis_tzspi_write(uint8_t* buff, size_t num_bytes);
static int fp_read_et5xxchip_id(void);
static int egis_tzspi_full_duplex(uint8_t* tx, size_t tx_bytes, uint8_t* rx, size_t rx_bytes);
/*end of EGIS_538 function declarations*/
#endif

#ifdef __ET538__
/*EGIS_538 function Definitions*/
static int egis_tzspi_init(void) {
    int retval = 0;
    QSEE_LOG(QSEE_LOG_MSG_ERROR, "@egis-sensorInfo-egis_tzspi_init");
    retval = qsee_spi_open(FP_SPI_DEVICE_ID);
    if (retval != 0) {
        QSEE_LOG(QSEE_LOG_MSG_ERROR,
                 "@egis-sensorInfo-spi open FAILED on device:%d with "
                 "retval=%d\n",
                 FP_SPI_DEVICE_ID, retval);
    } else {
        QSEE_LOG(QSEE_LOG_MSG_ERROR, "@egis-sensorInfo-spi open OK on device:%d\n",
                 FP_SPI_DEVICE_ID);
    }
    return retval;
}

static int egis_tzspi_close(void) {
    int retval = 0;
    QSEE_LOG(QSEE_LOG_MSG_ERROR, "@egis-sensorInfo-egis_tzspi_close");
    retval = qsee_spi_close(FP_SPI_DEVICE_ID);
    if (retval != 0) {
        QSEE_LOG(QSEE_LOG_MSG_ERROR,
                 "@egis-sensorInfo-spi close FAILED on device:%d with "
                 "retval=%d\n",
                 FP_SPI_DEVICE_ID, retval);
    } else {
        QSEE_LOG(QSEE_LOG_MSG_ERROR, "@egis-sensorInfo-spi close OK on device:%d\n",
                 FP_SPI_DEVICE_ID);
    }
    return retval;
}

#define RETRY_CNT 300
#define REG_ADDR_ID 0x00
#define REG_ID_VALUE 0xAA
#define SENSOR_NAME 0x538
#define ID_MASK 0xFE

#define BUF_LEN 8
#define TX_LEN 2
#define RX_LEN 1
#define REG_OFFSET 2
#define CMD_RD_REG 0x20
#define CMD_WR_REG 0x24
#define BUF_WR_LEN 3

#define CMD_LEN 2
#define OP_LEN 3
#define CMD_NV_EN 0x44
#define CMD_NV_RD1 0x40
#define CMD_NV_RD2 0x01
#define CMD_NV_DIS 0x48
#define CMD_DUMMY 0x00
#define SPI_LOW_CLK 4000000
#define ID_LEN 5
#define NVBUF_LEN 10

static int egis_check_hw_id(uint32_t* buf) {
    int ret = EGIS_CMD_ERROR;
    uint8_t data = 0;
    int i = 0;
    uint8_t write_buffer[BUF_WR_LEN] = {CMD_WR_REG, CMD_DUMMY, CMD_DUMMY};

    if (NULL == buf) {
        return EGIS_CMD_ERROR;
    }
    // QSEE_LOG(QSEE_LOG_MSG_ERROR,"@egis-sensorInfo-check_hw_id");
    for (i = 0; i < RETRY_CNT; i++) {
        egis_read_one_reg(REG_ADDR_ID, &data);
        if (REG_ID_VALUE == (data & ID_MASK)) {
            QSEE_LOG(QSEE_LOG_MSG_ERROR, "@egis-sensorInfo-ID:%02x\n", data);

            write_buffer[1] = 0xA;
            write_buffer[2] = 0xFD;
            egis_tzspi_write(write_buffer, BUF_WR_LEN);
            // disable POACF-Calibration
            write_buffer[1] = 0x35;
            write_buffer[2] = 0x2;
            egis_tzspi_write(write_buffer, BUF_WR_LEN);
            // STOP POACF (Power-ON Auto Calibration & FOD)
            write_buffer[1] = 0x80;
            write_buffer[2] = 0x0;
            egis_tzspi_write(write_buffer, BUF_WR_LEN);
            mdelay(DELAY_1MS);
            write_buffer[1] = 0xA;
            write_buffer[2] = 0xFC;
            egis_tzspi_write(write_buffer, BUF_WR_LEN);

            ret = fp_read_et5xxchip_id();
            if (ret == EGIS_CMD_OK) *buf = SENSOR_NAME;
            break;
        }
    }
    QSEE_LOG(QSEE_LOG_MSG_ERROR, "@egis-sensorInfo-ID:%02x\n", data);
    return ret;
}

static int egis_read_one_reg(uint8_t addr, uint8_t* value) {
    uint8_t Buf[BUF_LEN] = {0};
    int ret_value = 0;
    if (NULL == value) {
        return EGIS_CMD_ERROR;
    }
    Buf[0] = CMD_RD_REG;
    Buf[1] = addr;
    ret_value = egis_tzspi_full_duplex(Buf, TX_LEN, Buf, RX_LEN);
    *value = Buf[REG_OFFSET];
    return ret_value;
}

#define DELAY_1MS 1
#define DELAY_5MS 5
#define DELAY_10MS 10
static int egistec_get_sensor_info(uint32_t* data) {
    int error = EGIS_CMD_ERROR;
    int i = 0;
    if (NULL == data) {
        return EGIS_CMD_ERROR;
    }
    SLogTrace("@egis-sensorInfo-egistec_get_sensor_info");
    if (gpio_reset == 0) {
        SLogError("@egis-sensorInfo-invalid-gpio_reset =  %u", gpio_reset);
        goto out;
    }
    for (i = 0; i < RETRY_CNT; i++) {
        SLogTrace("@egis-sensorInfo--->reset pin and pull low");
        error = qsee_tlmm_gpio_id_out(gpio_reset, QSEE_GPIO_LOW);
        if (error != 0) {
            SLogError("@egis-sensorInfo-reset low failed %d", error);
            goto out;
        }
        mdelay(DELAY_1MS);
        error = qsee_tlmm_gpio_id_out(gpio_reset, QSEE_GPIO_HIGH);
        if (error != 0) {
            SLogError("@egis-sensorInfo-reset high failed %d", error);
            goto out;
        }
        mdelay(DELAY_10MS);
        SLogTrace("@egis-sensorInfo--->reset pin and pull high");
        egis_tzspi_init();
        error = egis_check_hw_id(data);
        egis_tzspi_close();
        if (error == EGIS_CMD_OK) break;
    }

out:
    return error;
}

// nvm read chip id for egis_check_hw_id
static int fp_read_et5xxchip_id(void) {
    uint8_t nvm_read_enable[CMD_LEN] = {CMD_NV_EN, CMD_DUMMY};
    uint8_t read_chip_op[OP_LEN] = {CMD_NV_RD1, CMD_NV_RD2, CMD_DUMMY};
    uint8_t nvm_disable[CMD_LEN] = {CMD_NV_DIS, CMD_DUMMY};
    uint32_t spi_freq = 0;
    int retval = 0;
    uint8_t idBuf[NVBUF_LEN] = {0};
    spi_freq = egis_spi_config.max_freq;
    egis_spi_config.max_freq = SPI_LOW_CLK;
    egis_tzspi_write(nvm_read_enable, CMD_LEN);
    retval = egis_tzspi_full_duplex(read_chip_op, OP_LEN, idBuf, ID_LEN);
    QSEE_LOG(QSEE_LOG_MSG_ERROR, "@egis-sensorInfo [NVID: %s]\n", &idBuf[3]);
    egis_tzspi_write(nvm_disable, CMD_LEN);
    egis_spi_config.max_freq = spi_freq;
    return retval;
}

static int egis_tzspi_write(uint8_t* buff, size_t num_bytes) {
    int retval = 0;

    if (NULL == buff) {
        return EGIS_CMD_ERROR;
    }
    qsee_spi_transaction_info_t write_info = {
        .buf_addr = buff, .buf_len = num_bytes, .total_bytes = 0};
    retval = qsee_spi_write(FP_SPI_DEVICE_ID, &egis_spi_config, &write_info);
    if (retval != 0) {
        QSEE_LOG(QSEE_LOG_MSG_ERROR, "@egis-sensorInfo-egis_tzspi_write-retval: %d\n", retval);
    }
    return retval;
}

static int egis_tzspi_full_duplex(uint8_t* tx, size_t tx_bytes, uint8_t* rx, size_t rx_bytes) {
    int retval = 0;
    if ((NULL == tx) || (NULL == rx)) {
        return EGIS_CMD_ERROR;
    }
    qsee_spi_transaction_info_t write_info = {
        .buf_addr = tx, .buf_len = tx_bytes, .total_bytes = 0};
    qsee_spi_transaction_info_t read_info = {
        .buf_addr = rx, .buf_len = rx_bytes + tx_bytes, .total_bytes = 0};
    retval = qsee_spi_full_duplex(FP_SPI_DEVICE_ID, &egis_spi_config, &write_info, &read_info);
    if (retval != 0) {
        QSEE_LOG(QSEE_LOG_MSG_ERROR, "@egis-sensorInfo-egis_tzspi_full_duplex-retval: %d\n",
                 retval);
    }
    return retval;
}
/*end of EGIS_538 function Definitions*/
#endif
