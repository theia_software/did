﻿#ifndef __ALGO_MODULE_HEADER__
#define __ALGO_MODULE_HEADER__
#include <tee_internal_api.h>
#include "egis/algomodule_ext.h"
#if defined(EGIS_IMAGE_V2)
#include "object_def_image.h"
#endif

typedef struct {
    Fp_FrameFormat format;
    uint16_t frame_count;
#if !defined(EGIS_IMAGE_V2)
    uint8_t* _buffer;
    uint32_t capacity;
#ifdef SUPPORT_KEEP_RAW_16BIT
    int raw_width;
    int raw_height;
    int raw_bpp;
    uint8_t* raw_buffer;
    uint8_t* raw_bad;
    uint8_t* bkg_raw_buffer;
#endif
#endif
} fp_image_t;

#define ALGO_FLAG_T uint8_t
typedef struct {
    uint16_t max_count;
    ALGO_FLAG_T* algoflag_buf;
} fp_image_t_ext_info_t;

typedef struct {
    int32_t coverage;
    int32_t quality;
    int32_t acceptance;
    int32_t reject_reason;
    int32_t stitched;
    int32_t spoof;
} fp_image_quality_t;

#if defined(EGIS_IMAGE_V2)
typedef struct {
    fp_image_t img_data;
    fp_image_quality_t quality;
    fp_image_t_ext_info_t imgdata_ext_info;
    rbs_obj_image_v1_0_t** img_obj_list;
} egis_image_t;

rbs_obj_image_v1_0_t* EGIS_IMAGE_get_param_keeper();
rbs_obj_image_v1_0_t* egis_image_copy_params(int index, int image_type);

#define EGIS_IMAGE_KEEP_PARAM(param, value) \
    RBSOBJ_set_IMAGE_param(EGIS_IMAGE_get_param_keeper(), param, value)

#define EGIS_IMAGE_OBTAIN_PARAM(param) RBSOBJ_get_IMAGE_param(EGIS_IMAGE_get_param_keeper(), param)

#else
#define EGIS_IMAGE_KEEP_PARAM(param, value) ex_log(LOG_DEBUG, "ignore getting ")

#define EGIS_IMAGE_OBTAIN_PARAM(param) ex_log(LOG_DEBUG, "ignore getting ")

typedef struct {
    fp_image_t img_data;
    fp_image_quality_t quality;
    fp_image_t_ext_info_t imgdata_ext_info;
    uint8_t* _bad_img_buffer;
} egis_image_t;
#endif

/*// EGIS_IMAGE_KEEP_PARAM_v2
    EGIS_IMAGE_KEEP_PARAM_v2(test1, "T1", 1);
    EGIS_IMAGE_KEEP_PARAM_v2(test1, "T11", 11);
    EGIS_IMAGE_KEEP_PARAM_v2(test1, "T111", 111); //param_tag Max size=OBJ_TAG_LENS
    EGIS_IMAGE_KEEP_PARAM_v2(test2, "T2", 2);
    int test1 = EGIS_IMAGE_OBTAIN_PARAM_v2(test1);
    int test2 = EGIS_IMAGE_OBTAIN_PARAM_v2(test2);
RESULT:
test1 = 111
test2 = 2
*/
#define EGIS_IMAGE_KEEP_PARAM_v2(param, param_tag, param_value) \
    RBSOBJ_set_IMAGE_param_v2(EGIS_IMAGE_get_param_keeper(), param, param_tag, param_value)

#define EGIS_IMAGE_OBTAIN_PARAM_v2(param) RBSOBJ_get_IMAGE_param_v2(EGIS_IMAGE_get_param_keeper(), param)


struct ImageHolder {
    egis_image_t fingerprint_image;
    uint32_t is_img_valid;
};

#define ALGO_FLAG_KEEP 1
#define ALGO_FLAG_DROP 0

#ifdef __ET7XX__
#ifdef TZ_MODE
#define EGIS_ENROLL_MAX_IMAGE_COUNTS 3
#else
#define EGIS_ENROLL_MAX_IMAGE_COUNTS 8
#endif
#else
#define EGIS_ENROLL_MAX_IMAGE_COUNTS 200
#endif

#ifdef __ET7XX__
#define DISABLE_TRY_MATCH_LEARNING
#ifdef DISABLE_TRY_MATCH_LEARNING
#define FLOW_TRY_MATCH_TIMEOUT 1500
#else
#define FLOW_TRY_MATCH_TIMEOUT 2500
#endif
#else  // ET5XX, ET6XX
#define FLOW_TRY_MATCH_TIMEOUT 1500
#endif

int EGIS_IMAGE_CREATE(egis_image_t* egis_img, int img_width, int img_height,
                      int number_of_image_to_allocate);
void EGIS_IMAGE_FREE(egis_image_t* egis_img);
void EGIS_IMAGE_RESET_FRAME_COUNT(egis_image_t* egis_img);

int egis_image_is_allocated(const egis_image_t* img);
int egis_image_set_buffer(const egis_image_t* img, int index, int image_type, uint8_t* src_buf,
                          int buf_size);
int egis_image_erase_buffer(const egis_image_t* img, int index, int image_type);
unsigned char* egis_image_get_pointer(const egis_image_t* img, int index);
unsigned char* egis_image_get_pointer_raw(const egis_image_t* img, int index);
#ifdef __ET7XX__
unsigned char* egis_image_get_bkg_pointer_raw(const egis_image_t* img, int index);
#endif

unsigned char* egis_image_get_bad_pointer(egis_image_t* img, int img_type);
unsigned char* egis_image_get_best_pointer(egis_image_t* img);

void egis_image_get_size(egis_image_t* egis_img, int img_type, int* width, int* height, int* bpp);
int egis_image_get_buffer_size(egis_image_t* egis_img, int img_type);
int egis_image_set_keep_param_index(int index);

int EGIS_IMAGE_set_algo_flag(egis_image_t* egis_img, int index, int algo_flag);
int EGIS_IMAGE_get_algo_flag(egis_image_t* egis_img, int index);

#endif
