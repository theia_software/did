#ifndef _EGIS_FP_SENSOR_INTERFACE_H__
#define _EGIS_FP_SENSOR_INTERFACE_H__
#include <stdint.h>
#include "fd_process.h"
#include "fp_sensormodule.h"

#define SENSOR_DEBUG_OPT_A 1
int egis_initFakeSensor(int width, int height);
int egis_initSensor(uint32_t debug_opt);
int egis_deInitSensor(void);
int egis_initFingerDetect(uint32_t* wait_time, uint32_t mode);
int egis_updateConfig(void);
// int egis_captureImage(void* pFingerPrintImage, uint32_t is_Enrolling, void*
// outImgqty);
int egis_captureImage_only_raw(void* egis_image, unsigned char* ext_buffer, int ext_buffer_size);
int egis_captureImage(void* pFingerPrintImage, uint32_t debug_opt,
                      enum fd_process_option fd_option);
int egis_captureRawImage(void* pFingerPrintImage, uint32_t reserve);
int egis_sensorSleep(void);
int egis_checkFingerLost(uint32_t* wait_time);
int egis_getSensorId(uint32_t* sensor_info);
int egis_getImageInformation(void* data);
uint32_t egis_getImagefmisize(void);
int egis_getModuleInfo64(char* data);

void egis_debugStubImageTest(void);
int egis_debugInjectImage(void* image);
int egis_debugRetrieveImage(void* image, uint32_t len);
void egis_getImageSize(int* width, int* height, int* dpi);
void egis_memMoveImageBuffer(unsigned char* dst, void* src);

int egis_mmiDoTest(fp_mmi_info* pMmiInfo);
int egis_mmiCoatingResult(fp_Coatingcheck* data);
int egis_mmiDeadPixelResult(uint32_t* data);
int egis_getCoatingImage(unsigned char* buffer, unsigned int length);
void egis_setSnrParameter(fp_snr_parameter_t* snr_parameter);
int egis_captureSnrImage(uint8_t* buffer, uint32_t image_counts, uint32_t image_size, uint8_t type);
int egis_read_nvram(uint8_t* buffer);
int egis_recovery();
int egis_set_spi_power(const int option);
int egis_get_recovery_event(void);

int egis_navGetInfo(void* data);

int egis_receive_cal_data(unsigned char* buffer, int* length);
int egis_send_cal_data(unsigned char* buffer, int length);
#endif
