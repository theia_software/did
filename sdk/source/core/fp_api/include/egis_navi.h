#ifndef __EGIS_NAVI_H__
#define __EGIS_NAVI_H__

typedef enum {
    EVENT_UNKNOWN = 0,
    EVENT_HOLD = 28,  // get click event   long press timeout
    EVENT_CLICK = 174,
    EVENT_DCLICK = 111,
    EVENT_UP = 103,
    EVENT_DOWN = 108,
    EVENT_LEFT = 105,
    EVENT_RIGHT = 106,
    EVENT_LOST = -1,
} NaviEventCode;

typedef struct {
    unsigned long longTouchDuration;
    unsigned int xThreshold;
    unsigned int yThreshold;
    unsigned int doubleClickDuration;
} NaviConfig;

typedef struct {
    int time;
    int zones;
    int x, y;
    int dx, dy;
    int fp_mode;
    int reserve;  // for reserved use
} nav_info_poll;

typedef struct {
    int eventValue;
    int needWaitIrq;
    int poll_count;
    nav_info_poll detail;
} navigation_info_common;

int egis_navGetEvent(NaviConfig* navi_config, navigation_info_common* pNaviInfo);

#endif
