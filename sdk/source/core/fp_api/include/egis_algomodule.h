#pragma once
#include <stdint.h>

#include "algomodule.h"
#include "common_definition.h"
#include "fp_algomodule.h"
#include "type_definition.h"

#define EGIS_COVERAGE_DEFAULT_VALUE 100
#define EGIS_QUALITY_DEFAULT_VALUE 100

int egis_fp_initAlgAndPPLib(int snsr_type);
int egis_fp_initCheckTemplateVersion(uint32_t* templateVersion);
void egis_fp_deInitAlg(void);

int egis_fp_identifyStart(uint32_t need_liveness_authentication,
                          fp_lib_template_t** candidate_templates, int size);

void egis_fp_identifyFinish(void);
int egis_fp_identifyImage_enroll(egis_image_t* image, fp_lib_enroll_data_t* enroll_data_t,
                                 fp_lib_template_t** candidate_templates, int size,
                                 algo_swipe_info_t* swipe_info);

int egis_fp_identifyImage_identify(egis_image_t* image, fp_lib_identify_data_t* fp_identify_data_t,
                                   int* candidate_index);

int egis_fp_identifyUpdateTemplate(void** unpack_tplt, uint8_t* inTemplateUpdate);

//#if defined(ALGO_GEN_7)
//int egis_fp_identifyUpdateNotMatchTemplate(void** unpack_tplt, uint8_t* isTemplateUpdate,
//                                           int index);
//#endif

void egis_fp_appendImgIntoTemp(const egis_image_t* image, unsigned char* enroll_templ,
                               int* enroll_temp_size);

int egis_fp_setFingerState(unsigned int finger_on);
int egis_fp_startEnroll(void);
int egis_fp_updateEnrolData(fp_lib_enroll_data_t* fp_enrol_data, egis_image_t* image);
int egis_fp_mergeEnrollTemp(fp_lib_enroll_data_t* enrol_data, egis_image_t* image);
int egis_fp_finishEnroll(fp_lib_template_t* template_t);

int egis_fp_packTemplate(void* unpack_tplt, uint8_t* template_buf);
int egis_fp_unpackTemplate(fp_lib_template_t* tplt, fp_lib_template_t** candidate_templates,
                           int candidate_size_p);

int egis_fp_deleteTemplate(fp_lib_template_t** candidate_templates, int candidate_index);

void egis_fp_preprocessor_cleanup(void);

int egis_fp_preprocessor(void* image);

void egis_fp_getAlgVersion(uint8_t* out_version_buf, uint32_t* out_version_len);
int egis_fp_getAlgSecurityThr(uint32_t* data);
void egis_fp_getTemplateSize(uint32_t* size);
int egis_fp_getAlgoBpp();
int egis_fp_get_enroll_config_max_count();
BOOL egis_fp_is_enroll();
int egis_fp_set_verify_finger_info(int verify_count, int verified_count);
