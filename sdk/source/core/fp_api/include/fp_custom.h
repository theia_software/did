
#ifndef __FP_CUSTOM_H_
#define __FP_CUSTOM_H_

#define EGIS_TRY_MATCH_MAX_COUNTS 5

#ifdef TZ_MODE
#define FILE_DATA_BASE "/data/vendor_de/0/fpdata/"
#else
#ifdef G3PLUS_MATCHER
#define FILE_DATA_BASE "/sdcard/RbsG3Temp/"
#elif defined(ALGO_GEN_2)
#define FILE_DATA_BASE "/sdcard/RbsG2Temp/"
#elif defined(ALGO_GEN_4)
#define FILE_DATA_BASE "/sdcard/RbsG4Temp/"
#elif defined(ALGO_GEN_5) && !defined(__ET0XX__)
#define FILE_DATA_BASE "/sdcard/RbsG5Temp/"
#elif defined(ALGO_GEN_7)
#define FILE_DATA_BASE "/sdcard/RbsG7Temp/"
#else
#define FILE_DATA_BASE "/data/fpdata/"
#endif
#endif  // TZ_MODE

#endif
