#pragma once
#include "egis_definition.h"
#include "type_definition.h"

#define PARAM_UPDATE_CONFIG "update_config"
#define PARAM_BOOST_MODE "boost_mode"

typedef enum {
    FP_MODE_DONT_CARE = -1,
    FP_STANDBY_MODE = 0,
    FP_DETECT_MODE,
    FP_SENSOR_MODE,
    FP_NAVI_DETECT_MODE,
    FP_NAVI_SENSOR_MODE
} FP_MODE;

extern FP_MODE g_fp_mode;

typedef int (*fn_on_calib_data_update)(uint16_t* calib_bkg, uint16_t* calib_wkg, int hw_int_count,
                                       float expo_time);
int isensor_open(int* timeout);
int isensor_close(void);
int isensor_set_detect_mode(void);
int isensor_set_sensor_mode(void);
int isensor_set_Z1_area(BOOL forceSetFull);
int isensor_set_power_off(void);
int isensor_get_frame(BYTE* frame, UINT height, UINT width, UINT number_of_frames);
int isensor_get_dynamic_frame(BYTE* frame, UINT height, UINT width, UINT number_of_frames);
int isensor_get_dynamic_frame_16bit(unsigned short* frame, UINT height, UINT width,
                                    UINT number_of_frames);
int isensor_pullup_int(BOOL polarity);
int isensor_set_custom_sensor_mode(int sensing_mode);
int isensor_get_sensor_roi_size(int* width, int* height);
int isensor_get_sensor_full_size(int* width_full, int* height_full);
int isensor_read_nvm(unsigned char* buff);
int isensor_set_calib_update_listener(fn_on_calib_data_update calib_data_update_listener);
void isensor_remove_calib_update_listener(fn_on_calib_data_update calib_data_update_listener);

typedef enum _sensing_type_t {
    SENSING_MODE_TYPE_DELTA = 0,
    SENSING_MODE_TYPE_SINGLE,
} FPS_SENSING_TYPE_T;

#define INT_STATUS_THH(x) (x & 0x4)
#define INT_STATUS_THL(x) (x & 0x8)
#define FOD_STATUS(x) (x & 0x81)
int isensor_read_int_status(BYTE* IntStatus);

#define SENSOR_REV_ID_LENGTH 3
int isensor_read_rev_id(BYTE* read_buf, int length);

int isensor_set_navigation_detect_mode(void);
int isensor_set_navigation_sensor_mode(void);

int isensor_recovery(FP_MODE fp_mode);

int isensor_set_feature(const char* param, int value);
int isensor_set_spi_power(const int option);

typedef enum {
    FPS_CALI_INIT,
    FPS_CALI_SDK_THH_FALSE_TRIGGER,
    FPS_CALI_SDK_THL,
    FPS_CALI_SDK_CHECK_FINGER_REMOVE,
    //
    FPS_CALI_ET7XX_BKG = 7,
    FPS_SAVE_ET7XX_CALI_DATA,
    FPS_LOAD_ET7XX_CALI_DATA,
    FPS_REMOVE_ET7XX_CALI_DATA,
    FPS_SAVE_ET7XX_CALI_DATA_TO_FLASH,
    FPS_CALI_UNINIT,
    FPS_REMOVE_ET7XX_BDS = 13,  // deprecated
    // Add new enum below, instead of adding new enum above
    FPS_SET_CALI_TYPE_BK,
    FPS_SET_CALI_TYPE_WK,
} FPS_CALIBRATE_OPTION;

int isensor_calibrate(FPS_CALIBRATE_OPTION option);
int isensor_get_bad_pixels(BYTE** bad_pixel, int* bad_pixel_count);
void isensor_dump_data();
int isensor_get_recovery_event(void);

typedef enum isensor_param_type {
    PARAM_INT_GET_IS_ET0XX = 0,
    PARAM_INT_DB_IS_RAW_IMAGE,
    PARAM_INT_TRY_INDEX,

    PARAM_INT_RAW_IMAGE_BPP = 10,
    PARAM_INT_ADC_GAIN,
    PARAM_BUF_CALIB_FOLDER_PATH,
    PARAM_INT_SENSOR_ID,

    PARAM_BUF_BKG_IMAGE_POINTER = 600,
    PARAM_INT_REF_LINE_IS_BAD,
    PARAM_BUF_CALIBRATION_DATA_POINTER,
    PARAM_BUF_VDM_DATA_POINTER,
    // ET7XX parameters
    PARAM_INT_FIRMWARE_VERSION = 701,
    PARAM_BUF_FIRMWARE_BUFFER,
    PARAM_INT_DC_OFFSET,
    PARAM_INT_INTEGRATE_COUNT,
    PARAM_BUF_TEMP_CALIBRATE,
    PARAM_BUF_CALI_BKG_IMAGE,
    PARAM_INT_REINIT_SENSOR,
    PARAM_BUF_SENSOR_TEMPERATURE,
    PARAM_INT_HW_INTEGRATE_COUNT,
    PARAM_INT_TEMPERATURE,  // 710
    PARAM_INT_GET_REAL_RAW_BPP,
    PARAM_BUF_SET_RENEW_BDS,
    PARAM_BUF_SET_GOOD_RAW,
    PARAM_INT_TEMPERATUREX10,
    PARAM_BUF_GET_RAWFRAME_STATISTICS,
    PARAM_BUF_BDS_BKG_IMAGE,
    PARAM_INT_GAIN2_ENABLE,
    PARAM_INT_CALI_BKG_EXPOSURE_TIME_X10,
    PARAM_INT_CALI_BKG_HW_INTEGRATE_COUNT,  // 719
    PARAM_INT_EXPOSURE_TIME_X10,
    PARAM_INT_ALL_ADC_DAC,
    PARAM_INT_CALI_BKG_CX,
    PARAM_INT_CALI_BKG_CY,
    PARAM_INT_BAC_RESULT,
    PARAM_INT_ENABLE_BAC,
    PARAM_INT_ENABLE_PIPELINE_CAPTURE,
    PARAM_INT_TRY_MATCH_COUNT,
    PARAM_INT_RUN_QUICK_CAPTURE_HW_INT,
    PARAM_INT_BDS_DEBUG_PATH,
    PARAM_INT_BDS_PATH_CHANGE,
    PARAM_INT_DB_TOTAL_FINGER,
    PARAM_BUF_DB_TOTAL_FINGERPRINT,
    PARAM_BUF_CALI_NORMALIZE_BKG_IMAGE,
    PARAM_BUF_SENOR_OTP_DATA,
    // ET0xx parameters
    PARAM_BUF_ENROLLED_PATH = 1000,
    PARAM_BUF_VERIFIED_PATH,
    PARAM_BUF_CALI_BKG_IMAGE_FROM_FLASH,
    PARAM_BUF_GET_CALI_DATA,
    PARAM_BUF_SET_CALI_DATA,
    PARAM_INT_RUN_QUICK_CAPTURE_EXP_TIME_X10,
    PARAM_INT_ROI_X,
    PARAM_INT_ROI_Y,
    PARAM_INT_SENSOR_ORIENTATION,
} isensor_param_type_t;

int isensor_get_int(isensor_param_type_t param_type, int* value);
int isensor_set_int(isensor_param_type_t param_type, int value);
int isensor_get_buffer(isensor_param_type_t param_type, unsigned char* out_buffer,
                       int* out_buffer_size);
int isensor_set_buffer(isensor_param_type_t param_type, unsigned char* in_buffer,
                       int in_buffer_size);
int isensor_get_sensor_id(unsigned short* id);
int isensor_receive_cal_data(unsigned char* buffer, int* length);
int isensor_send_cal_data(unsigned char* buffer, int length);
