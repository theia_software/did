#ifndef __FP_CAPTURE_COUNT_HEADER__
#define __FP_CAPTURE_COUNT_HEADER__

#include <stdint.h>

#include "type_definition.h"

#define THRESHOLD_SEQ_BAD_FRAME 4
int AddCountSequentialBadFrame(uint64_t timeCurrent);
void ResetCountSequentialBadFrame();
int GetCountSequentialBadFrame();

BOOL SetCaptureImageStart(uint64_t timeCapture);
int SetCaptureImageFingerTouch();
BOOL IsCaptureImageForEnroll();

#endif
