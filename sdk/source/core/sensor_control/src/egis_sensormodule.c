#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>

#include "EgisAlgorithmAPI.h"
#include "algomodule.h"
#include "core_config.h"
#include "egis_algomodule.h"
#include "egis_definition.h"
#include "egis_fp_get_image.h"
#include "egis_handle_esd.h"
#include "egis_sensor_test.h"
#include "egis_sensormodule.h"
#include "fd_process.h"
#include "fp_algomodule.h"
#include "fp_capture_count.h"
#include "fp_custom.h"
#include "fp_sensormodule.h"
#include "isensor_api.h"
#include "object_def_image.h"
#include "plat_heap.h"
#include "plat_log.h"
#include "plat_spi.h"
#include "plat_time.h"
#include "puzzle_image.h"

#define LOG_TAG "RBS-Sensor"

#define esd_recover_delay_time ((int)500)
extern FP_MODE g_fp_mode;

static void ConvertImgQuality(fd_img_analysis_info_t* bestImg, fp_image_quality_t* outImageQty) {
    if (outImageQty != NULL && bestImg != NULL) {
        outImageQty->quality = bestImg->qty / 256 * 75;
        outImageQty->quality += (bestImg->img_level > 10) ? 20 : bestImg->img_level * 2;
        outImageQty->quality += (bestImg->cover_count > 6) ? 5 : bestImg->cover_count;
        if (outImageQty->quality > 100) outImageQty->quality = 100;

        outImageQty->coverage = bestImg->percentage;
    }
}

int convert_result(int* retval) {
    int ret = *retval;
    egislog_d("convert_result ret = %d", ret);
    switch (ret) {
        case EGIS_OK:
            *retval = FP_LIB_OK;
            break;

        case EGIS_COMMAND_FAIL:
            *retval = FP_LIB_ERROR_SENSOR;
            break;

        case EGIS_NO_DEVICE:
            *retval = FP_LIB_ERROR_SENSOR;
            break;

        case EGIS_RESET_FAIL:
            *retval = FP_LIB_ERROR_SENSOR;
            break;

        case EGIS_FINGER_TOUCH:
        case EGIS_FINGER_NOT_REMOVED:
            *retval = FP_LIB_FINGER_PRESENT;
            break;

        case EGIS_OUT_OF_MEMORY:
            *retval = FP_LIB_ERROR_MEMORY;
            break;

        case EGIS_NEED_TO_REPEAT:
            egislog_d(
                "convert_result should not receive "
                "EGIS_NEED_TO_REPEAT !!!");
            *retval = FP_LIB_ERROR_GENERAL;
            break;

        case EGIS_CALIBRATION_DVR_FAIL:  //?? never happened
            *retval = FP_LIB_ERROR_SENSOR;
            break;

        case EGIS_FINGER_NOT_TOUCH:
        case EGIS_FINGER_ON_NOT_STABLE:
        case EGIS_PARTIAL_FINGER:
        case EGIS_WET_FINGER:
            *retval = FP_LIB_FINGER_LOST;
            break;

        default:
            *retval = FP_LIB_ERROR_GENERAL;
            break;
    }

    egislog_d("convert_result retval = %d", *retval);

    return *retval;
}

BOOL g_using_fake_sensor = FALSE;
int egis_initFakeSensor(int width, int height) {
    g_using_fake_sensor = TRUE;
    isensor_set_feature("sensing_area_width", width);
    isensor_set_feature("sensing_area_height", height);
    return EGIS_OK;
}

int egis_initSensor(uint32_t debug_opt) {
    int ret = EGIS_OK;
    int timeout;
    int sensor_width, sensor_height;

    isensor_set_feature("boost_mode", (debug_opt == SENSOR_DEBUG_OPT_A) ? TRUE : FALSE);

    isensor_get_sensor_roi_size(&sensor_width, &sensor_height);
#ifdef __ET538__
#define ET538_SENSOR_WIDTH 67
#error Use S/W Crop instead.
#endif

    egis_updateConfig();

    isensor_set_buffer(PARAM_BUF_CALIB_FOLDER_PATH, (unsigned char*)FILE_DATA_BASE,
                       strlen(FILE_DATA_BASE));
    ret = isensor_open(&timeout);
    if (ret != EGIS_OK) {
        egislog_e("isensor_open fail, ret = %d", ret);
    }

    sensor_exception_reset();

    convert_result(&ret);
    egislog_d("%s ret = %d", __func__, ret);
    return ret;
}

int egis_deInitSensor(void) {
    int ret = EGIS_OK;
    if (!g_using_fake_sensor) {
        ret = isensor_set_power_off();
        if (ret != EGIS_OK) {
            egislog_e("isensor_set_power_off fail, ret = %d", ret);
            goto exit;
        }

        ret = isensor_close();
        if (ret != EGIS_OK) {
            egislog_e("isensor_close fail, ret = %d", ret);
            goto exit;
        }
    }

exit:
    convert_result(&ret);
    egislog_d("egis_deInitSensor ret = %d", ret);
    return ret;
}

int egis_initFingerDetect(uint32_t* wait_time /*output, not need to implement*/, uint32_t mode) {
    int ret = EGIS_OK;
    egislog_d("egis_initFingerDetect mode = %d", mode);

    if (sensor_exception_is_active()) {
#ifdef ESD_HANDLE_BIG_LOOP
        ret = isensor_pullup_int(1);

        if (sensor_exception_is_over_duration()) {
            sensor_exception_reset();
        }
        goto exit;
#endif
    }

    if (mode == 0) {
        ret = isensor_set_detect_mode();
        if (ret != EGIS_OK) egislog_e("isensor_set_detect_mode ret = %d", ret);
    } else if (mode == 1) {
        /**
         * Navigation detection
         */
        ret = isensor_set_navigation_detect_mode();
        if (ret != EGIS_OK) egislog_e("isensor_set_navigation_detect_mode ret = %d", ret);
    } else if (mode == 2) {
        /**
         *	MMI Interrupt Test;
         */
        egislog_i("mmi Irq Test");
        ret = isensor_set_detect_mode();
        if (ret != EGIS_OK) egislog_e("isensor_set_detect_mode ret = %d", ret);
    }
#ifdef ESD_HANDLE_BIG_LOOP
exit:
#endif
    convert_result(&ret);
    egislog_d("egis_initFingerDetect ret = %d", ret);
    return ret;
}

int egis_captureRawImage(void* pFingerPrintImage, uint32_t reserve) {
    int ret = EGIS_OK;
    if (pFingerPrintImage == NULL) {
        egislog_e("pFingerPrintImage == NULL");
        return EGIS_INCORRECT_PARAMETER;
    }
    fp_image_t* image = (fp_image_t*)pFingerPrintImage;
    egis_image_t* egis_image = (egis_image_t*)pFingerPrintImage;
    if (!egis_image_is_allocated((egis_image_t*)image)) {
        egislog_e("wrong image pointer");
        return EGIS_INCORRECT_PARAMETER;
    }
    ret = fpsGetRawImage(egis_image_get_pointer(egis_image, 0), -1, -1, image->format.width,
                         image->format.height, image->frame_count);
    if (ret != EGIS_OK) {
        egislog_e("fpsGetRawImage failed. ret = %d", ret);
    }
    return ret;
}

int egis_captureImage_only_raw(void* pFingerPrintImage, unsigned char* ext_buffer,
                               int ext_buffer_size) {
    egis_image_t* egis_image = (egis_image_t*)pFingerPrintImage;
    return fd_finger_detect_only_get_raw(ext_buffer, ext_buffer_size, egis_image);
}

int egis_captureImage(void* pFingerPrintImage, uint32_t debug_mode,
                      enum fd_process_option fd_option) {
    uint64_t times_captureImage_start = plat_get_time();
    TIME_MEASURE_START(egis_captureImage);
    static uint8_t* gPrevBestImg;
    int ret = EGIS_OK;
    FINGER_QUALITY finger_image_status = FINGER_QUALITY_NONE;
    fp_image_t* image = (fp_image_t*)pFingerPrintImage;
    egis_image_t* egis_image = (egis_image_t*)image;
    BYTE IntStatus = 0;
    int width, height;
    egis_image_get_size(egis_image, IMGTYPE_BIN, &width, &height, NULL);

    SetCaptureImageStart(times_captureImage_start);
    if (image == NULL) {
        egislog_e("pFingerPrintImage == NULL");
        ret = EGIS_INCORRECT_PARAMETER;
        goto exit;
    }
    if (!egis_image_is_allocated(egis_image)) {
        egislog_e("image.buffer is NULL");
        ret = EGIS_INCORRECT_PARAMETER;
        goto exit;
    }
    if (!egis_image_is_allocated(egis_image)) {
        egislog_e("img_data.capacity is not set");
        ret = EGIS_INCORRECT_PARAMETER;
        goto exit;
    }
detect_start:

    if (!g_using_fake_sensor) {
        isensor_read_int_status(&IntStatus);
        egislog_d("isensor_read_int_status got 0x%X", IntStatus);

        if (INT_STATUS_THL(IntStatus)) {
            isensor_calibrate(FPS_CALI_SDK_THL);
            egislog_d("@egis-CaptureImg[TLK]");
            ret = EGIS_FINGER_NOT_TOUCH;
            goto exit;
        }
    }
    egislog_d("%s debug_mode = %d. (%d:%d)", __func__, debug_mode, image->format.width,
              image->format.height);

    if (g_fp_mode != FP_SENSOR_MODE && !g_using_fake_sensor) {
        ret = isensor_set_sensor_mode();
        if (ret != EGIS_OK) {
            egislog_e("isensor_set_sensor_mode, ret = %d", ret);
            goto exit;
        }
    }

    BOOL forEnroll = egis_fp_is_enroll();
    fd_img_analysis_info_t bestImgInfo;
    if (debug_mode) forEnroll = IsCaptureImageForEnroll();

#if defined(__ET0XX__)
    enum FCHECK_FLOW fcheck_flow;
#ifdef RBS_EVTOOL
    if (forEnroll) {
        fcheck_flow = FCHECK_FLOW_ENROLL;
    } else {
        fcheck_flow = FCHECK_FLOW_VERIFY;
    }
#else
    fcheck_flow = forEnroll ? FCHECK_FLOW_ENROLL_NO_STABLE : FCHECK_FLOW_VERIFY_NO_STABLE;
#endif
#elif defined(__ET7XX__)
    enum FCHECK_FLOW fcheck_flow =
        forEnroll ? FCHECK_FLOW_ENROLL_NO_STABLE : FCHECK_FLOW_VERIFY_NO_STABLE;
#else
    enum FCHECK_FLOW fcheck_flow = forEnroll ? FCHECK_FLOW_ENROLL : FCHECK_FLOW_VERIFY;

    if (forEnroll) {
        int enroll_mode =
            core_config_get_int(INI_SECTION_ENROLL, KEY_ENROLL_METHOD, ENROLL_METHOD_TOUCH);
        if (enroll_mode == ENROLL_METHOD_SWIPE || enroll_mode == ENROLL_METHOD_PAINT) {
            fcheck_flow = FCHECK_FLOW_ENROLL_NO_STABLE;
        }
    } else {
        int flow_try_match =
            core_config_get_int(INI_SECTION_VERIFY, KEY_FLOW_TRY_MATCH, INID_FLOW_TRY_MATCH);
        if (flow_try_match > 0) {
            fcheck_flow = FCHECK_FLOW_VERIFY_NO_STABLE;
        }
    }
#endif
    TIME_MEASURE_STOP_AND_RESTART(egis_captureImage, "fd_finger_detect start :");
    ret = fd_finger_detect(fcheck_flow, egis_image, &bestImgInfo, &finger_image_status, fd_option);
    TIME_MEASURE_STOP_AND_RESTART(egis_captureImage, "fd_finger_detect end :");
    ConvertImgQuality(&bestImgInfo, &egis_image->quality);

    if (ret != EGIS_FINGER_TOUCH) {
        if (INT_STATUS_THH(IntStatus)) AddCountSequentialBadFrame(times_captureImage_start);
        egislog_v("@egis-CaptureImg[Detect FAIL] f_count=%d", egis_image->img_data.frame_count);
        /*
         *	Maybe COMMAND_FAIL or EGIS_FINGER_NOT_TOUCH
         *	And the finger_image_status will be the default value
         *	FINGER_QUALITY_NONE
         */
        goto exit;
    }

    egislog_v("@egis-CaptureImg[Detect OK] f_count=%d", egis_image->img_data.frame_count);

    if (debug_mode) {
        // Take the last image
        unsigned char* img = egis_image_get_best_pointer(egis_image);
        int img_size = width * height * fp_getAlgoBpp() / 8;
        if (img != NULL) {
            if (gPrevBestImg == NULL) {
                if (width * height <= img_size) {
                    gPrevBestImg = plat_alloc(img_size);
                }
            }
            if (gPrevBestImg != NULL) {
                if (IsTwoImageSimilar(gPrevBestImg, width, height, img, width, height, forEnroll)) {
                    egislog_v("@egis-CaptureImg[Similar]");
                    ret = EGIS_FINGER_NOT_TOUCH;
                    finger_image_status = FINGER_QUALITY_BAD;
                    goto exit;
                }
                memcpy(gPrevBestImg, img, img_size);
            } else {
                egislog_e("debug_mode not supported");
            }
        }
    }

    ret = EGIS_FINGER_TOUCH;
exit:
    convert_result(&ret);

    switch (finger_image_status) {
        case FINGER_QUALITY_GOOD:
            egislog_i("@egis-CaptureImg[GOOD!]");
            egis_image->quality.reject_reason = 0;
            if (egis_image->quality.spoof) {
                egis_image->quality.reject_reason = forEnroll ? 0 : FP_LIB_ENROLL_FAIL_SPOOF_FINGER;
                egislog_i("@egis-CaptureImg[FakeFinger!]");
            }
            break;
        case FINGER_QUALITY_BAD:
            egislog_i("@egis-CaptureImg[LOW_QUALITY]");
            egis_image->quality.reject_reason = FP_LIB_ENROLL_FAIL_LOW_QUALITY;
            break;
        case FINGER_QUALITY_PARTIAL:
            egislog_i("@egis-CaptureImg[LOW_COVERAGE]");
            egis_image->quality.reject_reason = FP_LIB_ENROLL_FAIL_LOW_COVERAGE;
            break;
        case FINGER_QUALITY_PARTIAL | FINGER_QUALITY_BAD:
            egislog_i("@egis-CaptureImg[LOW_QUALITY&LOW_COVERAGE]");
            egis_image->quality.reject_reason = FP_LIB_ENROLL_FAIL_LOW_QUALITY;
            break;
        case FINGER_QUALITY_RESIDUAL:
            egislog_i("@egis-CaptureImg[FAIL_RESIDUAL]");
            egis_image->quality.reject_reason = FP_LIB_ENROLL_FAIL_RESIDUAL;
            break;
        case FINGER_QUALITY_NONE:
        default:
            egislog_i("@egis-CaptureImg[????]");
            egis_image->quality.reject_reason = FP_LIB_ENROLL_FAIL_NONE;
            break;
    }
#ifdef __ET7XX__
    int is_bad = FALSE;
    if (egis_image->quality.reject_reason != FP_LIB_ENROLL_SUCCESS &&
        egis_image->quality.reject_reason != FP_LIB_ENROLL_FAIL_NONE)
        is_bad = TRUE;
    EGIS_IMAGE_KEEP_PARAM(is_bad, is_bad);
    EGIS_IMAGE_KEEP_PARAM(reject_reason, egis_image->quality.reject_reason);
#endif
    int seqBadFrameCount = GetCountSequentialBadFrame();

    if (egis_fp_is_enroll() && (finger_image_status != FINGER_QUALITY_GOOD)) {
        // ret = (seqBadFrameCount < 3) ? FP_LIB_FINGER_PRESENT
        //			     : FP_LIB_FAIL_LOW_QUALITY;
        ret = (seqBadFrameCount < 3) ? FP_LIB_FINGER_PRESENT : FP_LIB_FINGER_LOST;
    }

    if (seqBadFrameCount > THRESHOLD_SEQ_BAD_FRAME) {
        if (!g_using_fake_sensor) {
            isensor_calibrate(FPS_CALI_SDK_THH_FALSE_TRIGGER);
        }
        ResetCountSequentialBadFrame();
        egislog_d("@egis-CaptureImg[THK]");
    }

    if (sensor_exception_is_active()) {
        /* For at least loop another time*/
        if (esd_recover_delay_time > 0) {
            // plat_wait_time(esd_recover_delay_time);
            uint64_t exception_loop_time_now = plat_get_diff_time(times_captureImage_start);
            if (exception_loop_time_now < esd_recover_delay_time) {
                if (ret != FP_LIB_FINGER_PRESENT) goto detect_start;
            }
        }
        /*else case, no need to delay and redo detect*/
    }

#ifndef ESD_HANDLE_BIG_LOOP
    /* clean esd exception value, to avoid next api called do workaround
     * again.*/
    sensor_exception_reset();
#endif

    TIME_MEASURE_STOP(egis_captureImage, "captureImage total");
    return ret;
}

int egis_sensorSleep(void) {
    int ret = EGIS_OK;

    if (!g_using_fake_sensor) {
        ret = isensor_set_power_off();
    }
    if (ret != EGIS_OK) {
        egislog_e("fp_tz_secure_set_power_off_mode fail, ret = %d", ret);
    }

    convert_result(&ret);
    egislog_d("egis_sensorSleep ret = %d", ret);
    return ret;
}

int egis_checkFingerLost(uint32_t* wait_time /*output*/) {
    int ret = EGIS_FINGER_NOT_TOUCH;

    // Check THL before isensor_set_detect_mode()
    BYTE IntStatus = 0;
    isensor_read_int_status(&IntStatus);
    if (INT_STATUS_THL(IntStatus)) {
        egislog_d("isensor_read_int_status got 0x%X", IntStatus);
        isensor_calibrate(FPS_CALI_SDK_THL);
        egislog_d("@egis-FingerLost[TLK]");
        goto exit;
    }

    BOOL forEnroll = egis_fp_is_enroll();
    enum FCHECK_FLOW fcheck_flow =
        forEnroll ? FCHECK_FLOW_ENROLL_REMOVE : FCHECK_FLOW_VERIFY_REMOVE;

    ret = fd_check_finger_remove(fcheck_flow);

    if (ret == EGIS_FINGER_NOT_REMOVED) {
        isensor_set_detect_mode();
        egislog_d("@egis-FingerLost[NO]");
    } else if (ret == EGIS_OK) {
        ret = EGIS_FINGER_NOT_TOUCH;
        egislog_d("@egis-FingerLost[YES]");
    }
/* Hal loop break with return value is FP_LIB_FINTER_LOST or FP_LIB_ERROR_SENSOR
 * other repeat
 */
exit:
    convert_result(&ret);
    egislog_d("@egis-FingerLost[ret:%d]", ret);
    return ret;
}

int egis_getSensorId16(uint16_t* sensor_id) {
    return isensor_get_sensor_id(sensor_id);
}

#ifdef NOT_HUA_PROJ
int egis_getSensorId(uint32_t* sensor_info) {
    /*
     * TODO
     * read nvm to fetch sensor ID
     */
    *sensor_info = 538;

    return 0;
}
#else
int egis_getSensorId(uint32_t* sensor_info) {
    uint16_t id = 0;
    int ret;
    ret = egis_getSensorId16(&id);
    *sensor_info = (uint32_t)id;

    return ret;
}
#endif
int egis_getImageInformation(void* data) {
    /*
     * TODO:
     * Check the code for returned data structure.
     */
    int img_width = 0;
    int img_height = 0;
    int dpi = 0;

    egis_getImageSize(&img_width, &img_height, &dpi);

    Fp_FrameFormat* out = (Fp_FrameFormat*)data;
    out->width = img_width;       // width in pixels
    out->height = img_height;     // height in pixels
    out->ppi = 0;                 // sampling intensity
    out->bits_per_pixels = 8;     // greyscale bit depth of the pixels
    out->channels = 60;           // number of channels
    out->greyscale_polarity = 0;  // 0 indicate that ridges are black,
                                  // valleys are white, 1 indeicate inverse
                                  // situation

    return 0;
}

int egis_getModuleInfo64(char* data)  // module id lenth must < 64 and need to be unique
{
    return 0;
}

void egis_debugStubImageTest(void) {
    return;
}
int egis_debugInjectImage(void* image) {
    return 0;
}
int egis_debugRetrieveImage(void* image, uint32_t len) {
    SetCaptureImageFingerTouch();
    ResetCountSequentialBadFrame();

    return 0;
}

void egis_getImageSize(int* width, int* height, int* dpi) {
    int enable_cut_image;
    int image_width, image_height;
    int sensor_width, sensor_height;
    isensor_get_sensor_roi_size(&sensor_width, &sensor_height);

    enable_cut_image = core_config_get_int(INI_SECTION_SENSOR, KEY_CUT_IMAGE, 0);
    if (enable_cut_image) {
        image_width = core_config_get_int(INI_SECTION_SENSOR, KEY_CUT_IMAGE_WIDTH, 0);
        image_height = core_config_get_int(INI_SECTION_SENSOR, KEY_CUT_IMAGE_HEIGHT, 0);

        if (image_width <= 0 || image_height <= 0 || image_width > sensor_width ||
            image_height > sensor_height) {
            *width = sensor_width;
            *height = sensor_height;
        } else {
            *width = image_width;
            *height = image_height;
        }
    } else {
        *width = sensor_width;
        *height = sensor_height;
    }

    return;
}

void egis_memMoveImageBuffer(unsigned char* dst, void* src) {
    return;
}
int egis_read_nvram(uint8_t* buffer) {
    int result = 0;
    result = isensor_read_nvm(buffer);
    return result;
}

int egis_recovery() {
    return isensor_recovery(g_fp_mode);
}
int egis_set_spi_power(const int option) {
    return isensor_set_spi_power(option);
}
int egis_get_recovery_event(void) {
    return isensor_get_recovery_event();
}

#ifdef FEATURE_NAVIGATION
int egis_navGetInfo(void* data) {
    int retval;
    int score, dx, dy, try_count = 0;
    unsigned char* navi_image = NULL;
    int sensor_width, sensor_height;
    ex_log(LOG_DEBUG, "navi from auto detect enter!");
    struct NaviStatusInfo* navi_status_info = (struct NaviStatusInfo*)data;

    if (data == NULL) {
        return EGIS_INCORRECT_PARAMETER;
    }

    navi_status_info->is_finger = FALSE;

    isensor_get_sensor_roi_size(&sensor_width, &sensor_height);

    navi_image = plat_alloc(sensor_height * sensor_width);
    if (navi_image == NULL) {
        ex_log(LOG_DEBUG, "navi alloc fail");
        return FP_LIB_ERROR_MEMORY;
    }

#define MAX_NAVIMAGE_COUNT 3
    do {
        retval = isensor_set_sensor_mode();
        if (retval != FP_LIB_OK) {
            egislog_e("egis_navGetInfo set_sensor_mode, ret = %d", retval);
            goto exit;
        }

        retval = isensor_get_dynamic_frame(navi_image, sensor_height, sensor_width, 1);
        if (retval == FP_LIB_OK) {
            process_image_in_place(PROC_INVERT, navi_image, sensor_width, sensor_height, 0, 0);
        } else {
            egislog_e("egis_navGetInfo isensor_get_dynamic_frame, ret = %d", retval);
            goto exit;
        }

        try_count++;
        score = 0;
        dx = 0;
        dy = 0;
        retval = count_offset_qm(navi_image, &score, &dx, &dy);
        ex_log(LOG_DEBUG,
               "egis_navGetInfo count_offset_qm score ret = %d score = %d,dx = %d,dy = %d", retval,
               score, dx, dy);
        if (retval == FP_LIB_OK) {
            qm_update_feat();
            navi_status_info->is_finger = TRUE;
        }
    } while (retval == QM_NOT_FINGER && try_count < MAX_NAVIMAGE_COUNT);

    navi_status_info->navi_score = score;
    navi_status_info->navi_dx = dx;
    navi_status_info->navi_dy = dy;
    ex_log(LOG_DEBUG, " SCORE:%d, X:%d, Y:%d", score, dx, dy);
exit:
    if (navi_image) plat_free(navi_image);

    return retval;
}
#endif
int egis_updateConfig(void) {
    return isensor_set_feature(PARAM_UPDATE_CONFIG, 0);
}
