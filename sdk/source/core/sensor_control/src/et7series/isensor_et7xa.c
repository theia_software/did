#include <stdint.h>
#include <stdio.h>
#include "calibration_manager.h"
#include "core_config.h"
#include "et7xa_sensor_control.h"
#include "fp_err.h"
#include "fpsensor_7xa_definition.h"
#include "ini_definition.h"
#include "isensor_api.h"
#include "isensor_definition.h"
#include "plat_file.h"
#include "plat_log.h"
#include "plat_mem.h"
#include "plat_time.h"
#include "sensor_config.h"
#include "sensor_control_common.h"
#include "et760_sensor_control.h"

#ifndef __OTG_SENSOR__
#include "plat_spi.h"
#endif

#define LOG_TAG "RBS-Sensor"
#define RAW_IMAGE_BPP_ET7XA (16)
FP_MODE g_fp_mode = FP_MODE_DONT_CARE;

float g_temperature = 25.f;
float g_exp_time;

int g_integrate_count;
int g_hw_integrate_count;



BOOL g_gain2_enable = 0;  // 0:1x gain , 1:2x gain
BOOL g_enable_bac = FALSE;
BOOL g_enable_pipeline_capture = FALSE;
int g_run_quick_capture_hw_int = 0;
float g_run_quick_capture_exp_time = 0;
int g_try_match_count = 0;
uint8_t g_sensor_pix_id = 0;
static int g_real_raw_bpp = 0;
static int g_bds_debug_path = 0;
static BOOL g_bds_path_change = FALSE;
static int g_touch_x = 0;
static int g_touch_y = 0;
static BOOL g_capture_roi_enabled = TRUE;
static int g_capture_roi_x = 0;
static int g_capture_roi_y = 0;

int isensor_calibrate(FPS_CALIBRATE_OPTION option) {
    egislog_i("isensor_calibrate %d", option);
    int ret = RESULT_OK;

    switch (option) {
        case FPS_CALI_INIT:
            calibration_load();
            break;

        case FPS_CALI_ET7XX_BKG:
            ret = calibration_generate();
            break;

        case FPS_SAVE_ET7XX_CALI_DATA:
            ret = calibration_save();
            break;

        case FPS_SAVE_ET7XX_CALI_DATA_TO_FLASH:
            ret = calibration_save_to_flash();
            break;

        case FPS_LOAD_ET7XX_CALI_DATA:
            ret = calibration_load();
            break;

        case FPS_REMOVE_ET7XX_CALI_DATA:
            ret = calibration_remove();
            break;

        case FPS_CALI_UNINIT:
            break;

        default:
            break;
    }
    return ret == RESULT_OK ? EGIS_OK : EGIS_COMMAND_FAIL;
}

static int _read_et7xx_id_to_init() {
    int ret = et7xa_get_sensor_id(&g_egis_sensortype.dev_id0, &g_egis_sensortype.dev_id1);
    if (ret != RESULT_OK) {
        egislog_e("et7xa_get_sensor_id fail!");
        ex_log(LOG_ERROR, "%s ret = %d", __func__, ret);
        return EGIS_COMMAND_FAIL;
    }
    uint16_t dev_id = DEVID(g_egis_sensortype.dev_id0, g_egis_sensortype.dev_id1);
    if (g_egis_sensortype.dev_id0 != 7 && g_egis_sensortype.dev_id0 != 9) {
        egislog_e("!!! expecting ET7XX or ET7XX. dev_id0=%d", g_egis_sensortype.dev_id0);
        ex_log(LOG_ERROR, "%s ret = %d", __func__, ret);
        return EGIS_COMMAND_FAIL;
    }
    switch (dev_id) {
        case DEVID(7, 29):
            g_exp_time = ET729_DEFAULT_EXPOSE_TIME;
            g_integrate_count = ET729_DEFAULT_INTEGRATE_COUNT;
            g_hw_integrate_count = ET729_DEFAULT_HW_INTEGRATE_COUNT;

            SENSOR_WIDTH = ET729_SENSOR_WIDTH;
            SENSOR_HEIGHT = ET729_SENSOR_HEIGHT;
            SENSOR_WIDTH_HW = ET729_SENSOR_WIDTH_HW;
            SENSOR_HEIGHT_HW = ET729_SENSOR_HEIGHT_HW;

            SENSOR_FULL_WIDTH = SENSOR_WIDTH;
            SENSOR_FULL_HEIGHT = SENSOR_HEIGHT;

            g_real_raw_bpp = 10;
            break;
        case DEVID(7,26):
            g_exp_time = ET726_DEFAULT_EXPOSE_TIME;
            g_integrate_count = ET726_DEFAULT_INTEGRATE_COUNT;
            g_hw_integrate_count = ET726_DEFAULT_HW_INTEGRATE_COUNT;

            SENSOR_WIDTH = ET726_SENSOR_WIDTH;
            SENSOR_HEIGHT = ET726_SENSOR_HEIGHT;
            SENSOR_WIDTH_HW = ET726_SENSOR_WIDTH_HW;
            SENSOR_HEIGHT_HW = ET726_SENSOR_HEIGHT_HW;

            SENSOR_FULL_WIDTH = SENSOR_WIDTH;
            SENSOR_FULL_HEIGHT = SENSOR_HEIGHT;

            g_real_raw_bpp = 14;
            break;
        case DEVID(7,11):
            g_exp_time = ET711_DEFAULT_EXPOSE_TIME;
            g_integrate_count = ET711_DEFAULT_INTEGRATE_COUNT;
            g_hw_integrate_count = ET711_DEFAULT_HW_INTEGRATE_COUNT;
            g_gain2_enable = ET711_DEFAULT_GAIN2_ENABLE;

            SENSOR_WIDTH = ET711_SENSOR_WIDTH;
            SENSOR_HEIGHT = ET711_SENSOR_HEIGHT;
            SENSOR_WIDTH_HW = ET711_SENSOR_WIDTH_HW;
            SENSOR_HEIGHT_HW = ET711_SENSOR_HEIGHT_HW;

            SENSOR_FULL_WIDTH = SENSOR_WIDTH;
            SENSOR_FULL_HEIGHT = SENSOR_HEIGHT;

            g_real_raw_bpp = 10;
            break;
        case DEVID(7,13):
            g_exp_time = ET713_DEFAULT_EXPOSE_TIME;
            g_integrate_count = ET713_DEFAULT_INTEGRATE_COUNT;
            g_hw_integrate_count = ET713_DEFAULT_HW_INTEGRATE_COUNT;
            g_gain2_enable = ET713_DEFAULT_GAIN2_ENABLE;

            et7xa_get_sensor_pix_id(&g_sensor_pix_id);
            egislog_d("ET713 pix_id = %d", g_sensor_pix_id);
            if (g_sensor_pix_id == PIX_ID_ET713C) {
                SENSOR_WIDTH = ET713_SENSOR_WIDTH;
                SENSOR_HEIGHT = ET713_SENSOR_HEIGHT;
                SENSOR_WIDTH_HW = ET713C_SENSOR_WIDTH_HW;
                SENSOR_HEIGHT_HW = ET713C_SENSOR_HEIGHT_HW;
            } else if (g_sensor_pix_id == PIX_ID_ET713D) {
                SENSOR_WIDTH = ET713_SENSOR_WIDTH;
                SENSOR_HEIGHT = ET713_SENSOR_HEIGHT;
                SENSOR_WIDTH_HW = ET713D_SENSOR_WIDTH_HW;
                SENSOR_HEIGHT_HW = ET713D_SENSOR_HEIGHT_HW;
            } else {
                SENSOR_WIDTH = ET713_SENSOR_WIDTH;
                SENSOR_HEIGHT = ET713_SENSOR_HEIGHT;
                SENSOR_WIDTH_HW = ET713_SENSOR_WIDTH_HW;
                SENSOR_HEIGHT_HW = ET713_SENSOR_HEIGHT_HW;
            }

            SENSOR_FULL_WIDTH = SENSOR_WIDTH;
            SENSOR_FULL_HEIGHT = SENSOR_HEIGHT;

            g_real_raw_bpp = 11;
            break;
        case DEVID(7,2):
            g_exp_time = ET702_DEFAULT_EXPOSE_TIME;
            g_integrate_count = ET702_DEFAULT_INTEGRATE_COUNT;
            g_hw_integrate_count = ET702_DEFAULT_HW_INTEGRATE_COUNT;

            SENSOR_WIDTH = ET702_SENSOR_WIDTH;
            SENSOR_HEIGHT = ET702_SENSOR_HEIGHT;
            SENSOR_WIDTH_HW = ET702_SENSOR_WIDTH_HW;
            SENSOR_HEIGHT_HW = ET702_SENSOR_HEIGHT_HW;

            SENSOR_FULL_WIDTH = SENSOR_WIDTH;
            SENSOR_FULL_HEIGHT = SENSOR_HEIGHT;

            g_real_raw_bpp = 14;
            break;
        case DEVID(7,15):
            g_exp_time = ET716_DEFAULT_EXPOSE_TIME;
            g_integrate_count = ET716_DEFAULT_INTEGRATE_COUNT;
            g_hw_integrate_count = ET716_DEFAULT_HW_INTEGRATE_COUNT;

            SENSOR_WIDTH = ET716_SENSOR_WIDTH;
            SENSOR_HEIGHT = ET716_SENSOR_HEIGHT;
            SENSOR_WIDTH_HW = ET716_SENSOR_WIDTH_HW;
            SENSOR_HEIGHT_HW = ET716_SENSOR_HEIGHT_HW;

            SENSOR_FULL_WIDTH = SENSOR_WIDTH;
            SENSOR_FULL_HEIGHT = SENSOR_HEIGHT;

            g_real_raw_bpp = 12;
            break;
        case DEVID(9,1):
            // ET901 init
            g_exp_time = ET901_DEFAULT_EXPOSE_TIME;
            g_integrate_count = 1;
            g_hw_integrate_count = 1;

            SENSOR_FULL_WIDTH = ET901_SENSOR_WIDTH;
            SENSOR_FULL_HEIGHT = ET901_SENSOR_HEIGHT;
            SENSOR_WIDTH_HW = ET901_SENSOR_WIDTH_HW;
            SENSOR_HEIGHT_HW = ET901_SENSOR_HEIGHT_HW;
            bool capture_roi_enabled = core_config_get_int(INI_SECTION_SENSOR, KEY_ENABLE_CAPTURE_ROI, FALSE);
            if(capture_roi_enabled) {
                SENSOR_WIDTH = core_config_get_int(INI_SECTION_SENSOR, KEY_CAPTURE_ROI_WIDTH, SENSOR_WIDTH);
                SENSOR_HEIGHT = core_config_get_int(INI_SECTION_SENSOR, KEY_CAPTURE_ROI_HEIGHT, SENSOR_HEIGHT);
            }else{
                SENSOR_WIDTH = ET901_SENSOR_WIDTH;
                SENSOR_HEIGHT = ET901_SENSOR_HEIGHT;
            }

            g_real_raw_bpp = 14;
            break;
        default:
            egislog_e("unknown sensor!");
            ex_log(LOG_ERROR, "%s ret = %d", __func__, ret);
            return EGIS_COMMAND_FAIL;
    }
    return EGIS_OK;
}

int isensor_open(int* timeout) {
    egislog_d("%s is called.", __func__);
    int ret;
#ifndef __OTG_SENSOR__
    io_dispatch_connect(NULL);
#endif
    ret = _read_et7xx_id_to_init();
    if (ret != EGIS_OK) {
        ex_log(LOG_ERROR, "%s ret = %d", __func__, ret);
        return ret;
    }
    isensor_set_feature(PARAM_UPDATE_CONFIG, 0);
    if (et7xa_init_sensor() != RESULT_OK) {
        egislog_e("et7xa_init_sensor fail!");
        ex_log(LOG_ERROR, "%s ret = %d", __func__, ret);
        return EGIS_COMMAND_FAIL;
    }
    if (isensor_calibrate(FPS_CALI_INIT) != RESULT_OK) {
        egislog_e("isensor_calibrate fail!");
        ex_log(LOG_ERROR, "%s ret = %d", __func__, ret);
        return EGIS_COMMAND_FAIL;
    }
    return EGIS_OK;
}

int isensor_close() {
    egislog_e("%s, not implemented. return OK directly", __func__);
    return EGIS_OK;
}

int isensor_get_frame(BYTE* frame, UINT height, UINT width, UINT number_of_frames) {
    egislog_e("%s, not implemented. return OK directly", __func__);
    return EGIS_OK;
}

int isensor_set_detect_mode() {
    egislog_v("%s, skipped", __func__);
    return EGIS_OK;
}
int isensor_set_sensor_mode() {
    egislog_v("%s, skipped", __func__);
    return EGIS_OK;
}
int isensor_set_Z1_area(BOOL forceSetFull) {
    egislog_e("%s, not implemented. return OK directly", __func__);
    return EGIS_OK;
}
int isensor_set_power_off() {
    egislog_v("%s, skipped", __func__);
    return EGIS_OK;
}

int isensor_get_dynamic_frame(BYTE* frame, UINT height, UINT width, UINT number_of_frames) {
    egislog_e("%s, not implemented. return OK directly", __func__);
    return EGIS_OK;
}

static int g_bac_result = RESULT_OK;

int isensor_get_dynamic_frame_16bit(unsigned short* frame, UINT height, UINT width,
                                    UINT number_of_frames) {
    int ret;
    int frame_sz = height * width * sizeof(uint16_t);
    if (frame == NULL) return EGIS_COMMAND_FAIL;
    int zone_avg_try_times = core_config_get_int(INI_SECTION_SENSOR, KEY_AVG_ZONE_STABLE_TRY, 0);
    TIME_MEASURE_START(et7xa_fetch_zone_avg_check_stable);
    if (g_enable_bac)
        g_bac_result = et7xa_fetch_zone_avg_check_stable(zone_avg_try_times);
    else
        g_bac_result = RESULT_OK;
    TIME_MEASURE_STOP(et7xa_fetch_zone_avg_check_stable, "et7xa_fetch_zone_avg_check_stable");

    int x0, y0;
    isensor_get_int(PARAM_INT_ROI_X, &x0);
    isensor_get_int(PARAM_INT_ROI_Y, &y0);
    for (int i = 0; i < number_of_frames; i++) {
        TIME_MEASURE_START(et7xa_fetch_raw_data);
        ret = et7xa_fetch_raw_data(frame + i * frame_sz, width, height, x0, y0);
        TIME_MEASURE_STOP_AND_RESTART(et7xa_fetch_raw_data, "et7xa_fetch_raw_data");
    }

    return ret == RESULT_OK ? EGIS_OK : EGIS_COMMAND_FAIL;
}

int fp_tz_secure_pullup_int(BOOL polarity) {
    egislog_e("%s, not implemented. return OK directly", __func__);
    return EGIS_OK;
}
int isensor_set_custom_sensor_mode(int sensing_mode) {
    // egislog_v("%s, not implemented. return OK directly", __func__);
    return EGIS_OK;
}

int isensor_get_sensor_roi_size(int* width, int* height) {
    egislog_d("%s is called.", __func__);
    if (SENSOR_WIDTH == 0 && SENSOR_HEIGHT == 0) {
        int ret = _read_et7xx_id_to_init();
        if (ret == EGIS_OK) {
            *width = SENSOR_WIDTH;
            *height = SENSOR_HEIGHT;
        }
        return ret;
    }
    *width = SENSOR_WIDTH;
    *height = SENSOR_HEIGHT;
    return EGIS_OK;
}

int isensor_get_sensor_full_size(int* width_full, int* height_full) {
    egislog_d("%s is called.", __func__);
    if (SENSOR_FULL_WIDTH <= 0 || SENSOR_FULL_HEIGHT <= 0) {
        int ret = _read_et7xx_id_to_init();
        if (ret != EGIS_OK) {
            return ret;
        }
    }
    *width_full = SENSOR_FULL_WIDTH;
    *height_full = SENSOR_FULL_HEIGHT;
    return EGIS_OK;
}

int isensor_read_nvm(unsigned char* buff) {
    egislog_e("%s, not implemented. return OK directly", __func__);
    return EGIS_OK;
}
int isensor_set_calib_update_listener(fn_on_calib_data_update calib_data_update_listener) {
    return calibration_set_calib_update_fn(calib_data_update_listener);
}
void isensor_remove_calib_update_listener(fn_on_calib_data_update calib_data_update_listener) {
    calibration_remove_calib_update_fn(calib_data_update_listener);
}
int isensor_read_int_status(BYTE* IntStatus) {
    egislog_v("%s, skipped", __func__);
    return EGIS_OK;
}
int isensor_read_rev_id(BYTE* read_buf, int length) {
    egislog_e("%s, not implemented. return OK directly", __func__);
    return EGIS_OK;
}
int isensor_set_navigation_detect_mode() {
    egislog_v("%s, skipped", __func__);
    return EGIS_OK;
}
int isensor_set_navigation_sensor_mode() {
    egislog_v("%s, skipped", __func__);
    return EGIS_OK;
}
int isensor_recovery(FP_MODE fp_mode) {
    egislog_v("%s, skipped", __func__);
    return EGIS_OK;
}

int isensor_set_feature(const char* param, int value) {
    if (param == NULL) {
        egislog_e("isensor_set_feature illegal parameter");
        return EGIS_INCORRECT_PARAMETER;
    }

    if (strncmp(param, PARAM_UPDATE_CONFIG, sizeof(PARAM_UPDATE_CONFIG)) == 0) {
        egislog_d("%s, PARAM_UPDATE_CONFIG %d", __func__, value);
        g_gain2_enable =
            core_config_get_int(INI_SECTION_SENSOR, KEY_TWO_TIMES_GAIN, g_gain2_enable);
        g_exp_time = core_config_get_double(INI_SECTION_SENSOR, KEY_EXPOSURE_TIME, g_exp_time);
        g_hw_integrate_count =
            core_config_get_int(INI_SECTION_SENSOR, KEY_INTEGRATE_COUNT_HW, g_hw_integrate_count);
        return EGIS_OK;
    } else {
        egislog_d("%s, not support. %d", param, value);
    }
    return EGIS_OK;
}

int isensor_get_bad_pixels(BYTE** bad_pixel, int* bad_pixel_count) {
    egislog_e("%s, not implemented. return OK directly", __func__);
    return EGIS_OK;
}

int isensor_get_int(isensor_param_type_t param_type, int* value) {
    switch (param_type) {
        case PARAM_INT_GET_IS_ET0XX:
            *value = 0;
            break;
        case PARAM_INT_SENSOR_ID:
            *value = g_egis_sensortype.dev_id0 * 100 + g_egis_sensortype.dev_id1;
            break;
        case PARAM_INT_EXPOSURE_TIME_X10:
            *value = (int)(g_exp_time * 10);
            break;
        case PARAM_INT_INTEGRATE_COUNT:
            *value = g_integrate_count;
            egislog_d("%s, get integrate count = %d", __func__, g_integrate_count);
            break;
        case PARAM_INT_RAW_IMAGE_BPP:
            *value = RAW_IMAGE_BPP_ET7XA;
            break;
        case PARAM_INT_HW_INTEGRATE_COUNT:
            *value = g_hw_integrate_count;
            egislog_d("%s, get hardware integrate count = %d", __func__, g_hw_integrate_count);
            break;
        case PARAM_INT_TEMPERATURE:
            *value = (int)(g_temperature + 0.5);
            egislog_d("%s, get temperature %d", __func__, *value);
            break;
        case PARAM_INT_GET_REAL_RAW_BPP:
            *value = g_real_raw_bpp;
            break;
        case PARAM_INT_GAIN2_ENABLE:
            *value = g_gain2_enable;
            break;
        case PARAM_INT_CALI_BKG_EXPOSURE_TIME_X10:
        case PARAM_INT_CALI_BKG_HW_INTEGRATE_COUNT:
        case PARAM_INT_CALI_NORMALIZE_IS_ENABLED:
            *value = calibration_get_bkg_integer(param_type);
            egislog_d("%s, get cali param %d = %d", __func__, param_type, *value);
            break;
        case PARAM_INT_CALI_BKG_CX: {
            int temp = 0;
            calibration_get_bkg_center(value, &temp);
            egislog_d("%s, get bkg_cx = %d", __func__, *value);
        } break;
        case PARAM_INT_CALI_BKG_CY: {
            int temp = 0;
            calibration_get_bkg_center(&temp, value);
            egislog_d("%s, get bkg_cy = %d", __func__, *value);
        } break;
        case PARAM_INT_BAC_RESULT:
            *value = g_bac_result;
            break;
        case PARAM_INT_BDS_DEBUG_PATH:
            *value = g_bds_debug_path;
            break;
        case PARAM_INT_BDS_PATH_CHANGE:
            *value = g_bds_path_change;
            break;
        case PARAM_INT_ROI_X:
            *value = g_capture_roi_x;
            break;
        case PARAM_INT_ROI_Y:
            *value = g_capture_roi_y;
            break;
        default:
            egislog_e("%s, not supported param id = %d", __func__, param_type);
            break;
    }
    return EGIS_OK;
}

int isensor_set_int(isensor_param_type_t param_type, int value) {
    switch (param_type) {
        case PARAM_INT_EXPOSURE_TIME_X10:
            g_exp_time = (float)value / 10;
            egislog_i("%s, set exp_time_x10 = %d", __func__, (int)(g_exp_time * 10));
            break;
        case PARAM_INT_INTEGRATE_COUNT:
            g_integrate_count = value;
            egislog_d("%s, set integrate count = %d", __func__, g_integrate_count);
            break;
        case PARAM_INT_HW_INTEGRATE_COUNT:
            g_hw_integrate_count = value;
            egislog_d("%s, set hardware integrate count = %d", __func__, g_hw_integrate_count);
            break;
        case PARAM_INT_TEMPERATURE:
            g_temperature = (float)value;
            egislog_d("%s, temperature = %d", __func__, (int)g_temperature);
            break;
        case PARAM_INT_TEMPERATUREX10:
            g_temperature = (float)value / 10;
            egislog_d("%s, temperature = %d.%d", __func__, (int)g_temperature, value % 10);
            break;
        case PARAM_INT_ENABLE_BAC:
            g_enable_bac = value;
            egislog_d("%s, g_enable_bac = %d", __func__, g_enable_bac);
            break;
        case PARAM_INT_ENABLE_PIPELINE_CAPTURE:
            g_enable_pipeline_capture = value;
            egislog_d("%s, g_enable_pipeline_capture = %d", __func__, g_enable_pipeline_capture);
            break;
        case PARAM_INT_TRY_MATCH_COUNT:
            g_try_match_count = value;
            egislog_d("%s, g_try_match_count = %d", __func__, g_try_match_count);
            break;
        case PARAM_INT_RUN_QUICK_CAPTURE_HW_INT:
            g_run_quick_capture_hw_int = value;
            egislog_d("%s, g_run_quick_capture = %d", __func__, g_run_quick_capture_hw_int);
            break;
        case PARAM_INT_RUN_QUICK_CAPTURE_EXP_TIME_X10:
            g_run_quick_capture_exp_time = (float)value / 10;
            egislog_d("%s, g_run_quick_capture_exp_time_x10 = %d", __func__,
                      (int)(g_run_quick_capture_exp_time * 10));
            break;
        case PARAM_INT_BDS_DEBUG_PATH:
            if (g_bds_debug_path != 0 && value != g_bds_debug_path) {
                g_bds_path_change = TRUE;
                egislog_d("%s, g_bds_path_change = %d", __func__, g_bds_path_change);
            } else
                g_bds_path_change = FALSE;
            g_bds_debug_path = value;
            egislog_d("%s, g_bds_debug_path = %d", __func__, g_bds_debug_path);
            break;
        case PARAM_INT_ROI_X:
            g_capture_roi_x = value;
            break;
        case PARAM_INT_ROI_Y:
            g_capture_roi_y = value;
            break;
        default:
            egislog_e("%s, not supported type = %d", __func__, param_type);
            break;
    }
    return EGIS_OK;
}

int isensor_get_buffer(isensor_param_type_t param_type, unsigned char* out_buffer,
                       int* out_buffer_size) {
    if (out_buffer == NULL || out_buffer_size == NULL || *out_buffer_size <= 0) {
        egislog_e("%s [%d] wrong output parameter", __func__, param_type);
        return EGIS_INCORRECT_PARAMETER;
    }
    int ret = EGIS_OK;
    int width = SENSOR_WIDTH;
    int height = SENSOR_HEIGHT;
    int frame_sz = width * height * sizeof(uint16_t);

    switch (param_type) {
        case PARAM_BUF_CALI_BKG_IMAGE: {
            uint16_t* bkg_img = calibration_get_bkg();
            if (bkg_img == NULL) {
                *out_buffer_size = 0;
            } else {
                *out_buffer_size = SENSOR_FULL_WIDTH * SENSOR_FULL_HEIGHT * sizeof(uint16_t);
                memcpy((void*)out_buffer, (void*)bkg_img, *out_buffer_size);
            }
        } break;
        case PARAM_BUF_CALI_BKG_IMAGE_FROM_FLASH: {
            calibration_get_bkg_from_flash(out_buffer, out_buffer_size);
        } break;
        case PARAM_BUF_SENSOR_TEMPERATURE: {
            int code, temp;
            et7xa_get_temperature(&code, &temp);
            memcpy(out_buffer, &code, sizeof(int));
            memcpy(out_buffer + sizeof(int), &temp, sizeof(int));
            *out_buffer_size = 2 * sizeof(int);
            break;
        }
        case PARAM_BUF_GET_RAWFRAME_STATISTICS: {
            et7xa_statistics_data(out_buffer);
            break;
        }
        case PARAM_BUF_GET_CALI_DATA: {
            ret = calibration_save_to_buffer(out_buffer, out_buffer_size);
            break;
        }
        case PARAM_BUF_SENOR_OTP_DATA: {
            egislog_d("PARAM_BUF_SENOR_OTP_DATA start");
            et7xx_read_otp_uuid(out_buffer);
        } break;
        default: { egislog_e("%s, not supported type = %d", __func__, param_type); } break;
    }

    return ret;
}

int isensor_set_buffer(isensor_param_type_t param_type, unsigned char* in_buffer,
                       int in_buffer_size) {
    switch (param_type) {
        case PARAM_BUF_SET_CALI_DATA: {
            return calibration_load_from_buffer(in_buffer, in_buffer_size);
        }
        case PARAM_BUF_CALIB_FOLDER_PATH:
            calibration_set_folder_path((const char*)in_buffer);
            break;
        default:
            egislog_e("%s, not supported type = %d", __func__, param_type);
            break;
    }
    return EGIS_OK;
}

int isensor_get_sensor_id(unsigned short* id) {
    uint8_t* buf = (unsigned char*)id;
    uint8_t sensor_id_0 = 0, sensor_id_1 = 0;

    et7xx_read_register(ET7XA_ADDR_DEV_ID0, &sensor_id_0);
    et7xx_read_register(ET7XA_ADDR_DEV_ID1, &sensor_id_1);
    buf[0] = sensor_id_0;
    buf[1] = sensor_id_1;

    return EGIS_OK;
}

int isensor_set_spi_power(const int option) {
    int ret = 0;
#ifdef __OTG_SENSOR__
    // Do nothing
#else
    switch (option) {
        case 0:
            ret = io_dispatch_disconnect();
            break;
        case 1:
            ret = io_dispatch_connect(NULL);
            break;
        default:
            break;
    }
#endif
    return ret;
}

int isensor_get_recovery_event(void) {
    // Not care ESD
    return FALSE;
}
