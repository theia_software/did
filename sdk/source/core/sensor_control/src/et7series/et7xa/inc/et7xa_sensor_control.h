#ifndef __ET7XA_SENSOR_CONTROL_H__
#define __ET7XA_SENSOR_CONTROL_H__
#include <stdint.h>

int et7xa_fetch_raw_data(uint16_t* raw_image, int width, int height, int x0, int y0);
int et7xa_fetch_raw_data_without_setting(uint16_t* raw_image, int width, int height, int x0, int y0);
void et7xx_read_otp_uuid(unsigned char* uuid);
void et7xa_statistics_data(void* statistics_data);
int et7xa_get_sensor_id(uint8_t* dev_id0, uint8_t* dev_id1);
int et7xa_init_sensor(void);
int et7xa_set_expo_time_sw(float expo_time);
int et7xa_get_expo_time_sw(float* expo_time);

// void et7xa_set_expo_time_mty(int expo_time);
void et7xa_set_frame_num(int frames);
void et7xa_get_frame_num(int* frames);
void et7xa_get_temperature(int* code_out, int* temperature);
int et7xa_fetch_zone_avg_check_stable(int fetch_times);
void et7xa_set_gain(int gain2_enable);
int et7xa_sensor_mux_select(uint16_t select);
int et7xa_get_sensor_pix_id(uint8_t* p_pix_id);

#endif
