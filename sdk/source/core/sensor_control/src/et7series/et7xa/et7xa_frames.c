#include <stdbool.h>
#include "isensor_api.h"

#include "core_config.h"
#include "egis_definition.h"
#include "et7xa_sensor_control.h"
#include "fp_err.h"
#include "fpsensor_7xa_definition.h"
#include "ini_definition.h"
#include "isensor_definition.h"
#include "plat_log.h"
#include "plat_mem.h"
#include "plat_spi.h"
#include "plat_time.h"
#include "sensor_control_common.h"

#define LOG_TAG "RBS-Sensor"

static bool check_frame_ready(int hw_integrate_count, float exposure_time);

extern float g_exp_time;
extern int g_hw_integrate_count;
extern int g_zone_average_stabled;
extern BOOL g_gain2_enable;
extern BOOL g_enable_pipeline_capture;
extern BOOL g_run_quick_capture_hw_int;
extern float g_run_quick_capture_exp_time;
extern int g_try_match_count;
extern uint8_t g_sensor_pix_id;
static void print_statistics_data(void);
static long long g_time_last_scan;
typedef struct {
    int max;
    int min;
    int mean;
} statistics_data_t;

statistics_data_t g_statistics_data = {0};

static int et7xa_fetch_zone_avg(unsigned char* buf, int* buf_size, int bac_exp_time) {
    if (bac_exp_time > 0) {
        et7xa_set_expo_time_sw(bac_exp_time);
    }

    int ret = et7xx_write_register(ET7XA_ADDR_HDR_CSR, 0x00);  // Default
    et7xx_write_register(ET7XA_ADDR_SCAN_FRAMES, 0x00);        // capture one image
    et7xx_write_register(ET7XA_ADDR_SCAN_CSR, 0x21);           // start capture
    if (0 != et7xx_polling_registry(ET7XA_ADDR_SYS_STUS, 0x1, 0x1)) {
        return -1;
    }
    return et7xx_io_command(IOCMD_READ_ZONE_AVERAGE, 0, 0, buf, buf_size);
}

#define ZONE_AVG_AREA_WIDTH 6
#define ZONE_AVG_AREA_HEIGHT 6
#define ZONE_AVG_SIZE (ZONE_AVG_AREA_WIDTH * ZONE_AVG_AREA_HEIGHT) /*36*/

static BOOL is_dark_image(unsigned short* pbuf, int black_img_avg_threshold) {
    if (pbuf == NULL) return false;
    int i, total_block_avgs = 0;
    int all_block_avg = 0.0;
    for (i = 0; i < ZONE_AVG_SIZE; i++) {
        total_block_avgs += *pbuf++;
    }
    all_block_avg = total_block_avgs / ZONE_AVG_SIZE;
    egislog_d("%s, all block avg = %d, threshold = %d", __func__, all_block_avg,
              black_img_avg_threshold);
    return all_block_avg < black_img_avg_threshold;
}

#define _ABS(x, y) (short)((x > y) ? (x - y) : (y - x))
int et7xa_fetch_zone_avg_check_stable(int fetch_times) {
#define MAX_ZONE_AVG_TEST_COUNT 10
#define MAX_DARK_IMAGE_COUNT 10
    int i, j, k, ret = RESULT_BAC_UNSTABLE;
    int found_frame_count = 0;
    int count_big_diff = 0;
    // due to calculate abs, need +1 frame than test count.
    unsigned short out_buf[ZONE_AVG_SIZE * (MAX_ZONE_AVG_TEST_COUNT + 1)];
    unsigned short *pbuf_current, *pbuf_current_prev;
    unsigned short *p_current, *p_current_prev;
    int buf_abs_value;
    int frame_abs_max = 0;
    int out_buf_size = sizeof(out_buf);
    int frame_abs_threshold = 0;

    if (fetch_times > MAX_ZONE_AVG_TEST_COUNT) egislog_e("fetch_times >10 ");

    int bac_exp_percentage =
        core_config_get_int(INI_SECTION_SENSOR, KEY_BAC_EXPOSURE_PERCENTAGE, 33);
    int bac_exp_time = (int)(g_exp_time * (float)bac_exp_percentage / 100.0);
    egislog_d("bac_exp_time = %d", bac_exp_time);

    pbuf_current = out_buf;
    pbuf_current_prev = out_buf;
    if (fetch_times == 0) return RESULT_OK;

    int black_img_avg_threshold =
        core_config_get_int(INI_SECTION_SENSOR, KEY_BAC_BLACK_IMG_AVG_THRESHOLD, 90);
    frame_abs_threshold = core_config_get_int(INI_SECTION_SENSOR, KEY_BAC_THRESHOLD, 40);
    egislog_d("%s, frame_abs_threshold = %d, fetch_times = %d", __func__, frame_abs_threshold,
              fetch_times);

    int sequential_dark_image_count = 0;
    // pbuf_current = out_buf;
    for (i = 0; i < fetch_times + 1; i++) {
        et7xa_fetch_zone_avg((unsigned char*)pbuf_current, &out_buf_size,
                             i == 0 ? bac_exp_time : 0);

        // If detect dark image, re-try to get zone avg
        if (is_dark_image(pbuf_current, black_img_avg_threshold) &&
            sequential_dark_image_count < MAX_DARK_IMAGE_COUNT) {
            i--;
            sequential_dark_image_count++;
            egislog_d("%s, detect black img: sequential count = %d", __func__,
                      sequential_dark_image_count);
            if (sequential_dark_image_count >= MAX_DARK_IMAGE_COUNT) {
                egislog_e("%s, RESULT_BAC_DARK_IMAGE", __func__);
                return RESULT_BAC_DARK_IMAGE;
            }
            continue;
        }
        sequential_dark_image_count = 0;

        if (i == 0) {
            pbuf_current_prev = pbuf_current;
            pbuf_current += ZONE_AVG_SIZE;
            continue;
        }

        count_big_diff = 0;
        frame_abs_max = 0;
        p_current = pbuf_current;
        p_current_prev = pbuf_current_prev;
        for (j = 0; j < ZONE_AVG_AREA_HEIGHT; j++) {
            for (k = 0; k < ZONE_AVG_AREA_WIDTH; k++) {
                buf_abs_value = _ABS(*p_current, *p_current_prev);
                if (buf_abs_value > frame_abs_max) {
                    frame_abs_max = buf_abs_value;
                    // egislog_v("-    [%d][%d][%d] max diff = %0d", i, j, k, frame_abs_max);
                }
                if (buf_abs_value > frame_abs_threshold) {
                    count_big_diff++;
                }
                p_current++;
                p_current_prev++;
            }
        }
        egislog_d("[%d] max diff value = %0d (%d)", i, frame_abs_max, count_big_diff);

        found_frame_count = i;
        if (frame_abs_max < frame_abs_threshold || count_big_diff <= 2) {
            ret = RESULT_OK;
            break;
        }

        pbuf_current_prev = pbuf_current;
        pbuf_current += ZONE_AVG_SIZE;
    }
    egislog_d("[%d] BAC: the last max diff value = %d (%d)", found_frame_count, frame_abs_max,
              count_big_diff);

#ifdef ZONE_AVG_DEBUG
    pbuf_current = out_buf;
    for (i = 0; i < found_frame_count; i++) {
        egislog_d("-- [%d] --", i);
        for (j = 0; j < ZONE_AVG_AREA_HEIGHT; j++) {
            egislog_d("%04X %04X %04X %04X %04X %04X", pbuf_current[0], pbuf_current[1],
                      pbuf_current[2], pbuf_current[3], pbuf_current[4], pbuf_current[5]);
            pbuf_current += ZONE_AVG_AREA_WIDTH;
        }
    }

    egislog_d("Difference", __func__);
    int buf_abs[ZONE_AVG_AREA_WIDTH];
    pbuf_current = out_buf;
    pbuf_current_prev = out_buf + ZONE_AVG_SIZE;
    for (i = 0; i < found_frame_count - 1; i++) {
        egislog_d("-- [%d][%d] --", i, i + 1);
        for (j = 0; j < ZONE_AVG_AREA_HEIGHT; j++) {
            for (k = 0; k < ZONE_AVG_AREA_WIDTH; k++) {
                buf_abs[k] = _ABS(pbuf_current_prev[k], pbuf_current[k]);
                if (buf_abs[k] > frame_abs_max) frame_abs_max = buf_abs[k];
            }
            egislog_d("%04X %04X %04X %04X %04X %04X ", buf_abs[0], buf_abs[1], buf_abs[2],
                      buf_abs[3], buf_abs[4], buf_abs[5]);
            pbuf_current += ZONE_AVG_AREA_WIDTH;
            pbuf_current_prev += ZONE_AVG_AREA_WIDTH;
        }
    }
#endif
    return ret;
}
static void __set_capture_settings(int hw_integrate_count, float exp_time) {
    int gain2_enable = g_gain2_enable;

    et7xa_sensor_mux_select(SENSOR_A0);
    egislog_d("set_capture exp_time_x10 = %d, hw integrate count %d, gain2 %d",
              (int)(exp_time * 10), hw_integrate_count, gain2_enable);

    et7xa_set_gain(gain2_enable);
    et7xa_set_expo_time_sw(exp_time);
    et7xa_set_frame_num(hw_integrate_count);
    et7xx_write_register(ET7XA_ADDR_HDR_CSR, INTEG_EN);
}

static void __trigger_scan(BOOL is_need_setting, int quick_capture_hw_int,
                           float quick_capture_exp_time) {
    if (is_need_setting) {
        int hw_int = quick_capture_hw_int ? quick_capture_hw_int : g_hw_integrate_count;
        float exp_time = quick_capture_exp_time ? quick_capture_exp_time : g_exp_time;
        __set_capture_settings(hw_int, exp_time);
    }
    egislog_d("Trigger SCAN_CSR_ME_SCAN_START");
    g_time_last_scan = plat_get_time();
    et7xx_write_register(ET7XA_ADDR_SCAN_CSR, SCAN_CSR_ME_SCAN_START);
}

static void __trigger_short_scan() {
    __set_capture_settings(1, 42);
    egislog_d("%s, Trigger SCAN_CSR_ME_SCAN_START", __func__);
    et7xx_write_register(ET7XA_ADDR_SCAN_CSR, SCAN_CSR_ME_SCAN_START);
}

#define NON_FINGERPRINT_SAMPLE_PIXEL_NUMBER 25

static int __get_border_pixel_value(uint8_t* source, uint16_t* result) {
    int i = 0, ret = RESULT_OK;
    uint16_t mean = 0;
    for (i = 0; i < NON_FINGERPRINT_SAMPLE_PIXEL_NUMBER * sizeof(uint16_t); i += 2) {
        mean += (*(source + i + 1)) << 8 | (*(source + i));
    }
    mean = mean / 25;
    *result = mean;
    return RESULT_OK;
}

static int __add_border(uint16_t* src_img, uint16_t* dst_img, int src_width, int src_height,
                        int dst_width, int dst_height) {
    if (src_img == NULL || dst_img == NULL || src_width > dst_width || src_height > dst_height) {
        return RESULT_FAILED;
    }
    int i = 0, ret = RESULT_OK;
    uint16_t border_pixel_value = 0;
    __get_border_pixel_value((uint8_t*)src_img, &border_pixel_value);
    egislog_d("border_pixel_value = %d", border_pixel_value);
    uint8_t* dst = (uint8_t*)dst_img;
    for (i = 0; i < dst_width * dst_height * sizeof(uint16_t); i += 2) {
        *(dst + i) = border_pixel_value & 0xFF;
        *(dst + i + 1) = border_pixel_value >> 8;
    }

    int target_width = dst_width * sizeof(uint16_t);
    int ori_width = src_width * sizeof(uint16_t);
    int offset_width = (dst_width - src_width) / 2;
    int offset_height = (dst_height - src_height) / 2;
    uint8_t* line = ((uint8_t*)src_img);
    uint8_t* dest = (uint8_t*)dst_img + offset_height * target_width;
    for (i = offset_height; i < dst_height - offset_height; i++) {
        memcpy(dest + offset_width * sizeof(uint16_t), line, ori_width);
        line += ori_width;
        dest += target_width;
    }
    return ret;
}

static int __crop(uint16_t* src_img, uint16_t* dst_img, int src_width, int src_height,
                  int dst_width, int dst_height) {
    if (src_img == NULL || dst_img == NULL || dst_width > src_width || dst_height > src_height) {
        return RESULT_FAILED;
    }
    int i = 0, ret = RESULT_OK;
    int crop_width = dst_width * sizeof(uint16_t);
    int ori_width = src_width * sizeof(uint16_t);
    int offset_width = (src_width - dst_width) * sizeof(uint16_t) / 2;
    int offset_height = (src_height - dst_height) / 2;
    uint8_t* src = (uint8_t*)src_img;
    uint8_t* dest = (uint8_t*)dst_img;
    for (i = offset_height; i < (dst_height + offset_height); i++) {
        memcpy(dest, src + i * ori_width + offset_width, crop_width);
        dest += crop_width;
    }
    return ret;
}

static int __fetch_raw_data(uint16_t* raw_image, int width, int height, BOOL is_need_setting, int x0, int y0) {
    if(x0 < 0 || y0 < 0 || x0 > (SENSOR_FULL_WIDTH - width/2) || y0 > (SENSOR_FULL_HEIGHT - height/2)){
        egislog_d("%s, invalid parameters %d %d %d %d", __func__, x0, y0, width, height);
        return RESULT_INVALID_PARAMETER;
    }
    int ret;
    BOOL need_sw_crop;
    uint8_t dev_id0 = g_egis_sensortype.dev_id0;
    uint8_t dev_id1 = g_egis_sensortype.dev_id1;
    int frame_sz = width * height * sizeof(uint16_t);
    int width_hw = SENSOR_WIDTH_HW;
    int height_hw = SENSOR_HEIGHT_HW;
    int frame_sz_hw = width_hw * height_hw * sizeof(uint16_t);
    uint16_t* raw_image_hw;
    uint16_t dev_id = DEVID(g_egis_sensortype.dev_id0, g_egis_sensortype.dev_id1);
    BOOL drop_two_frame = FALSE;
    switch (dev_id) {
        case DEVID(7,29):
        case DEVID(7,13):
        case DEVID(7,26):
        case DEVID(7,15):
            need_sw_crop = TRUE;
            raw_image_hw = plat_alloc(frame_sz_hw);
            RBS_CHECK_IF_NULL(raw_image_hw, EGIS_OUT_OF_MEMORY);
            break;
        case DEVID(7,2):
            need_sw_crop = FALSE;
            raw_image_hw = raw_image;
            break;
        case DEVID(9,1):
            need_sw_crop = FALSE;
            raw_image_hw = raw_image;
            drop_two_frame = ((plat_get_time() - g_time_last_scan)) > 3000;
            break;
        default:
            need_sw_crop = FALSE;
            raw_image_hw = raw_image;
            break;
    }

    if(drop_two_frame) {
        for(int i=0; i<2; i++) {
            __trigger_short_scan();
            BOOL need_set_roi =
                    (SENSOR_WIDTH < SENSOR_FULL_WIDTH) || (SENSOR_HEIGHT < SENSOR_FULL_HEIGHT);
            if (need_set_roi) {
                frame_sz_hw = width * height * 2;

                et7xx_write_register(ET9XX_ADDR_ROI_CSR, 0x00);
                et7xx_write_register(ET9XX_ADDR_IMG_SA_COL, x0);
                et7xx_write_register(ET9XX_ADDR_IMG_SA_ROW, y0);
                et7xx_write_register(ET9XX_ADDR_IMG_SA_HBYTE,
                                     ((x0 & 0x0F00) >> 4) | ((y0 & 0x0F00) >> 8));
                et7xx_write_register(ET9XX_ADDR_IMG_COL, width & 0xFF);
                et7xx_write_register(ET9XX_ADDR_IMG_ROW, height & 0xFF);
                et7xx_write_register(ET9XX_ADDR_IMG_HBYTE,
                                     ((width & 0x0F00) >> 4) | ((height & 0x0F00) >> 8));
                et7xx_write_register(ET9XX_ADDR_ROI_CSR, 0x04);
            } else {
                egislog_d("ROI do not use ROI CAPTURE frame_sz_hw %d", frame_sz_hw);
                et7xx_write_register(ET9XX_ADDR_ROI_CSR, 0x00);
            }

            et7xx_read_frame((uint8_t *) raw_image_hw, frame_sz_hw);
        }
    }
    egislog_d("g_enable_pipeline_capture = %d, g_try_match_count %d", g_enable_pipeline_capture,
              g_try_match_count);
    if (g_enable_pipeline_capture) {
        if (g_try_match_count == 0) {
            __trigger_scan(is_need_setting, g_run_quick_capture_hw_int,
                           g_run_quick_capture_exp_time);
        }
    } else {
        __trigger_scan(is_need_setting, g_run_quick_capture_hw_int
                , g_run_quick_capture_exp_time);
    }
    et7xa_sensor_mux_select(SENSOR_A0);
    int hw_integrate_count;
    float sw_expo_time;
    et7xa_get_frame_num(&hw_integrate_count);
    ret = et7xa_get_expo_time_sw(&sw_expo_time);
    if (ret != RESULT_OK) {
        sw_expo_time = g_exp_time;
    }
    egislog_d("sw_expo_time = %d, hw_integrate_count %d", (int)sw_expo_time * 10,
              hw_integrate_count);
    if (!check_frame_ready(hw_integrate_count, sw_expo_time)) {
        egislog_e("SYS_STUS check fail");
        ret = RESULT_FAILED;
        goto exit;
    }

    if(need_set_roi) {
        frame_sz_hw = width * height * 2;

        et7xx_write_register(ET9XX_ADDR_ROI_CSR, 0x00);
        et7xx_write_register(ET9XX_ADDR_IMG_SA_COL, x0);
        et7xx_write_register(ET9XX_ADDR_IMG_SA_ROW, y0);
        et7xx_write_register(ET9XX_ADDR_IMG_SA_HBYTE, ((x0 & 0x0F00) >> 4) | ((y0 & 0x0F00) >> 8));
        et7xx_write_register(ET9XX_ADDR_IMG_COL, width & 0xFF);
        et7xx_write_register(ET9XX_ADDR_IMG_ROW, height & 0xFF);
        et7xx_write_register(ET9XX_ADDR_IMG_HBYTE, ((width & 0x0F00) >> 4) | ((height & 0x0F00) >> 8));
        et7xx_write_register(ET9XX_ADDR_ROI_CSR, 0x04);
    } else {
        egislog_d("ROI do not use ROI CAPTURE frame_sz_hw %d", frame_sz_hw);
        et7xx_write_register(ET9XX_ADDR_ROI_CSR, 0x00);
    }

    ret = RESULT_OK;
    print_statistics_data();
    et7xx_read_frame((uint8_t*)raw_image_hw, frame_sz_hw);
    if (need_sw_crop) {
        switch (dev_id1) {
            case 29: {
                const int shift = 6 * width_hw * sizeof(uint16_t);
                memcpy((void*)raw_image, (void*)raw_image_hw + shift, frame_sz);
                break;
            }
            case 13: {
                if (g_sensor_pix_id == PIX_ID_ET713C) {
                    // crop image to 200x200 from 218x218
                    __crop(raw_image_hw, raw_image, width_hw, height_hw, width, height);
                } else if (g_sensor_pix_id == PIX_ID_ET713D) {
                    // crop image to 170x170 from 172x172
                    uint16_t* raw_image_hw_crop =
                        plat_alloc(ET713D_SENSOR_WIDTH * ET713D_SENSOR_HEIGHT * sizeof(uint16_t));
                    __crop(raw_image_hw, raw_image_hw_crop, width_hw, height_hw,
                           ET713D_SENSOR_WIDTH, ET713D_SENSOR_HEIGHT);
                    // add 170x170 to 200x200
                    __add_border(raw_image_hw_crop, raw_image, ET713D_SENSOR_WIDTH,
                                 ET713D_SENSOR_HEIGHT, width, height);
                    plat_free(raw_image_hw_crop);
                } else {
                    int j;
                    int crop_width = width * sizeof(uint16_t);
                    int ori_width = width_hw * sizeof(uint16_t);
                    uint8_t* line = ((uint8_t*)raw_image_hw) + sizeof(uint16_t);
                    uint8_t* dest = (uint8_t*)raw_image;
                    for (j = 0; j < height; j++) {
                        memcpy(dest, line, crop_width);
                        line += ori_width;
                        dest += crop_width;
                    }
                }
                break;
            }
            case 26: {
                int j;
                int crop_width = width * sizeof(uint16_t);
                int ori_width = width_hw * sizeof(uint16_t);
                uint8_t* line = ((uint8_t*)raw_image_hw) + sizeof(uint16_t);
                uint8_t* dest = (uint8_t*)raw_image;
                for (j = 0; j < height; j++) {
                    memcpy(dest, line, crop_width);
                    line += ori_width;
                    dest += crop_width;
                }
                break;
            }
            case 15: {
                int j;
                int crop_width = width * sizeof(uint16_t);
                int ori_width = width_hw * sizeof(uint16_t);
                uint8_t* line = ((uint8_t*)raw_image_hw) + sizeof(uint16_t);
                uint8_t* dest = (uint8_t*)raw_image;
                for (j = 0; j < height; j++) {
                    memcpy(dest, line, crop_width);
                    line += ori_width;
                    dest += crop_width;
                }
                break;
            }
            default:
                break;
        }
    }

    if (g_enable_pipeline_capture) {
        __trigger_scan(is_need_setting, FALSE, FALSE);
    }
exit:
    if (raw_image_hw != raw_image) plat_free(raw_image_hw);
    return ret;
}

int et7xa_fetch_raw_data(uint16_t* raw_image, int width, int height, int x0, int y0) {
    int ret;
    ret = __fetch_raw_data(raw_image, width, height, TRUE, x0, y0);
    return ret;
}

int et7xa_fetch_raw_data_without_setting(uint16_t* raw_image, int width, int height, int x0, int y0) {
    int ret;
    ret = __fetch_raw_data(raw_image, width, height, FALSE, x0, y0);
    return ret;
}
#ifdef QSEE
extern int io_et713_lense_type_uuid(unsigned char* uuid);
#endif

void et7xx_read_otp_uuid(unsigned char* uuid) {
    int ret = 0;
    egislog_d("et7xx_read_otp_uuid start");
#ifdef QSEE
    ret = io_et713_lense_type_uuid(uuid);
#endif
    egislog_d("et7xx_read_otp_uuid end, %d", ret);
    return;
}

void et7xa_statistics_data(void* statistics_data) {
    memcpy(statistics_data, &g_statistics_data, sizeof(statistics_data_t));
    egislog_i("mean=%d max=%d min=%d", ((statistics_data_t*)statistics_data)->mean,
              ((statistics_data_t*)statistics_data)->max,
              ((statistics_data_t*)statistics_data)->min);
}

// Note: statistic data must be readout before image frame
static void print_statistics_data(void) {
    uint16_t max, min, mean;
    et7xx_read_register_16(ET7XA_ADDR_STAT_MEAN_16, &mean);  // Mean
    et7xx_read_register_16(ET7XA_ADDR_STAT_MAX_16, &max);    // Maximum
    et7xx_read_register_16(ET7XA_ADDR_STAT_MIN_16, &min);    // Minimum
    egislog_i("mean=%d max=%d min=%d", mean, max, min);
    g_statistics_data.mean = (int)mean;
    g_statistics_data.max = (int)max;
    g_statistics_data.min = (int)min;
}

static bool check_frame_ready(int hw_integrate_count, float exposure_time) {
    int retry;
    uint8_t SYS_STUS = 0x00;
    bool success = false;
    const int max_retry = 1000;

    if (!g_enable_pipeline_capture || g_try_match_count == 0) {
        plat_sleep_time((uint32_t)(exposure_time * hw_integrate_count));
    }

    for (retry = 0; retry < max_retry; retry++) {
        et7xx_read_register(ET7XA_ADDR_SYS_STUS, &SYS_STUS);
        if ((SYS_STUS & IMG_RDY) == 1) {
            success = true;
            break;
        }
        plat_sleep_time(1);
    }
    if (success) {
        egislog_d("retry = %d, SYS_STUS = 0x%x", retry, SYS_STUS);
    } else {
        egislog_e("retry = %d, SYS_STUS = 0x%x", retry, SYS_STUS);
    }
    return success;
}
