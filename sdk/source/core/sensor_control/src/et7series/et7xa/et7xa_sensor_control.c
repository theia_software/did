#include "et7xa_sensor_control.h"
#include "egis_definition.h"
#include "fp_err.h"
#include "fpsensor_7xa_definition.h"
#include "plat_log.h"
#include "plat_time.h"
#include "sensor_config.h"
#include "sensor_control_common.h"
#include "type_definition.h"

#define LOG_TAG "RBS-Sensor"

uint8_t g_pix_id = 0;

int et7xa_get_sensor_id(uint8_t* dev_id0, uint8_t* dev_id1) {
    // et7xa_sensor_mux_select(SENSOR_A0);
    et901_set_spi_mux(0);
    et7xx_read_register(ET7XA_ADDR_DEV_ID0, dev_id0);
    et7xx_read_register(ET7XA_ADDR_DEV_ID1, dev_id1);
    return RESULT_OK;
}

int et7xa_get_sensor_pix_id(uint8_t* p_pix_id) {
    et7xx_write_register(ET7XA_ADDR_REG_KEY, 0x88);
    et7xx_read_register(ET7XA_ADDR_PIX_ID, p_pix_id);
    et7xx_write_register(ET7XA_ADDR_REG_KEY, 0x00);
    g_pix_id = *p_pix_id;
    return RESULT_OK;
}

int et7xa_init_sensor(void) {
    uint8_t SYS_STUS = 0;
/*    uint8_t dev_id0 = g_egis_sensortype.dev_id0;
    uint8_t dev_id1 = g_egis_sensortype.dev_id1;
    egislog_d("init et7xa sensor %d %d", dev_id0, dev_id1);*/
    uint16_t dev_id = DEVID(g_egis_sensortype.dev_id0, g_egis_sensortype.dev_id1);
    egislog_d("init et7xa sensor %04X", dev_id);
    et7xa_sensor_mux_select(SENSOR_A0);
    et7xx_read_register(ET7XA_ADDR_SYS_STUS, &SYS_STUS);
    egislog_d("SYS_STUS = 0x%x", SYS_STUS);

    switch (dev_id) {
        case DEVID(7, 29):
        case DEVID(7, 11):
            et7xx_write_register(ET7XA_ADDR_BOOST_CP, 0xC1);
            et7xx_write_register(ET7XA_ADDR_PWR_CTRL2, 0xF6);
            et7xx_write_register(ET7XA_ADDR_PWR_CTRL3, 0x13);
            et7xx_write_register(ET7XA_ADDR_VCM_CTRL, 0x62);
            break;
        case DEVID(7, 26):
            et7xx_write_register(ET7XA_ADDR_BOOST_CP, 0xC1);
            et7xx_write_register(ET7XA_ADDR_PWR_CTRL2, 0xF6);
            et7xx_write_register(ET7XA_ADDR_PWR_CTRL3, 0x13);
            break;
        case DEVID(7, 13):
            // Pixel array control Register
            et7xx_write_register(ET7XA_ADDR_PXL_CTRL1, 0x02);
            // adjust vcm bias pull up range
            et7xx_write_register(ET7XA_ADDR_VCM_CTRL, 0x61);
            break;
        case DEVID(7, 2):
            // adjust vcm bias pull up range
            et7xx_write_register(ET7XA_ADDR_VCM_CTRL, 0x66);
            break;
        case DEVID(7, 15):
            et7xx_write_register(ET7XA_ADDR_PGA_CSR, 0x04);
            et7xx_write_register(ET7XA_ADDR_VCM_CTRL, 0x63);
            et7xx_write_register(ET7XA_ADDR_PWR_CTRL2, 0xF0);
            break;
        case DEVID(9,1): // TODO: ET901 init reg
            et901_set_spi_mux(0);
            et7xx_write_register(0xFF, 0x03);
            et7xx_write_register(ET9XX_ADDR_ROI_CSR, 0x00);
            plat_sleep_time(10);
            et7xx_write_register(0xFF, 0x07);
            plat_sleep_time(50);

            et901_set_spi_mux(2);
            et901_fpga_set_HIVDAC(V2Code(4.3), V2Code(10.0), V2Code(2.3), V2Code(10.0), V2Code(-3.0));

            et901_set_spi_mux(1);
            et7xx_write_register(0x50, 0x03);
            et7xx_write_register(0x08, 0x07);
            et7xx_write_register(0x10, 0x17);
            et7xx_write_register(0x55, 0x18);
            et901_set_spi_mux(0);
            //et7xx_write_register(0x06, 0x02); // big-endian
            et7xx_write_register(0x12, 0x08); // row time 54us
            et7xx_write_register(0x13, 0x70);
            et7xx_write_register(0x14, 0x08);
            et7xx_write_register(0x15, 0x70);
            break;
        default:
            egislog_e("unknown sensor!");
            return EGIS_COMMAND_FAIL;
    }
    et7xx_write_register(ET7XA_ADDR_CONF, 0x02 | Low_byte_first); //bit 1 is for ET901 exposure time
    return RESULT_OK;
}

int et7xa_set_expo_time_sw(float expo_time)  // ms
{
    uint16_t expo_time_setting = 0;
    uint16_t dev_id = DEVID(g_egis_sensortype.dev_id0, g_egis_sensortype.dev_id1);

    egislog_d("exp_time_x10=%d", (int)(expo_time * 10));
    switch (dev_id) {
        case DEVID(7,29):
        case DEVID(7,11):
            expo_time_setting = (uint16_t)((expo_time * 1000) / 44 - 1);
            break;
        case DEVID(7,26):
            expo_time_setting = (uint16_t)((expo_time * 1000) / 104 - 1);
            break;
        case DEVID(7,13):
            if (g_pix_id == PIX_ID_ET713C)
                expo_time_setting = (uint16_t)((expo_time * 1000) / 56.44 - 1);
            else if (g_pix_id == PIX_ID_ET713D)
                expo_time_setting = (uint16_t)((expo_time * 1000) / 53.88 - 1);
            else
                expo_time_setting = (uint16_t)((expo_time * 1000) / 54 - 1);
            break;
        case DEVID(7,2):
            expo_time_setting = (uint16_t)((expo_time * 1000) / 40 - 1);
            break;
        case DEVID(7,15):
            expo_time_setting = (uint16_t)((expo_time * 1000) / 53.88 - 1);
            break;
        case DEVID(9,1):
            expo_time_setting = (uint16_t)((expo_time * 1000) / 54 - 1);
            if (expo_time_setting < 650){
                expo_time_setting = 650; // minimal register setting
            }
            break;
        default:
            egislog_e("unknown sensor!");
            break;
    }
    if(dev_id == DEVID(9,1)){
        egislog_d("set expo_time %d", expo_time_setting);
        return et7xx_write_register_16(ET9XX_ADDR_EXP_TIME_SW_16, expo_time_setting);
    }else {
        return et7xx_write_register_16(ET7XA_ADDR_EXP_TIME_SW_16, expo_time_setting);
    }
}

int et7xa_get_expo_time_sw(float* expo_time) {
    uint16_t expo_time_16;
    uint16_t dev_id = DEVID(g_egis_sensortype.dev_id0, g_egis_sensortype.dev_id1);
    if(dev_id == DEVID(9,1)){
        et7xx_read_register_16(ET9XX_ADDR_EXP_TIME_SW_16, &expo_time_16);
    }else {
        et7xx_read_register_16(ET7XA_ADDR_EXP_TIME_SW_16, &expo_time_16);
    }
    egislog_d("expo_time_16=%d, g_pix_id=%d", expo_time_16, g_pix_id);

    switch (dev_id) {
        case DEVID(7,29):
        case DEVID(7,11):
            *expo_time = (float)((expo_time_16 + 1) * 44) / (float)1000;
            break;
        case DEVID(7,26):
            *expo_time = (float)((expo_time_16 + 1) * 104) / (float)1000;
            break;
        case DEVID(7,13):
            if (g_pix_id == PIX_ID_ET713C)
                *expo_time = (float)((expo_time_16 + 1) * 56.44) / (float)1000;
            else if (g_pix_id == PIX_ID_ET713D)
                *expo_time = (float)((expo_time_16 + 1) * 53.88) / (float)1000;
            else
                *expo_time = (float)((expo_time_16 + 1) * 54) / (float)1000;
            break;
        case DEVID(7,2):
            *expo_time = (float)((expo_time_16 + 1) * 40) / (float)1000;
            break;
        case DEVID(7,15):
            *expo_time = (float)((expo_time_16 + 1) * 53.88) / (float)1000;
            break;
        case DEVID(9,1):
            *expo_time = (float)((expo_time_16 + 1) * 54) / (float)1000;
            break;
        default:
            egislog_e("unknown sensor!");
            return RESULT_FAILED;
    }
    egislog_d("expo_time=%d", (int)((*expo_time) * 10));
    return RESULT_OK;
}

#if 0  // unused
void et7xa_set_expo_time_mty(int expo_time)  // ms
{
	uint16_t expo_time_setting = 0;
	uint8_t dev_id0 = g_egis_sensortype.dev_id0;
	uint8_t dev_id1 = g_egis_sensortype.dev_id1;

	if (dev_id0 == 7 && dev_id1 == 29) {
		expo_time_setting = (uint16_t)((expo_time * 1000) / 44 - 1);
	} else if (dev_id0 == 7 && dev_id1 == 26) {
		expo_time_setting = (uint16_t)((expo_time * 1000) / 104 - 1);
	} else if (dev_id0 == 7 && dev_id1 == 11) {
		expo_time_setting = (uint16_t)((expo_time * 1000) / 44 - 1);
	} else {
		egislog_e("unknown sensor!");
	}
	et7xx_write_register_16(ET7XA_ADDR_EXP_TIME_MTY_16, expo_time_setting);
}
#endif

void et7xa_set_frame_num(int frames) {
    uint16_t frames_setting;
    frames_setting = (uint16_t)(frames - 1);
    et7xx_write_register(ET7XA_ADDR_SCAN_FRAMES, frames_setting);
}

void et7xa_get_frame_num(int* frames) {
    if (frames == NULL)
        return;
    uint8_t frames_setting;
    et7xx_read_register(ET7XA_ADDR_SCAN_FRAMES, &frames_setting);
    *frames = frames_setting + 1;
}

int get_delay_time_for_temperature_sensor_enable() {
    // in us
    uint16_t dev_id = DEVID(g_egis_sensortype.dev_id0, g_egis_sensortype.dev_id1);
    if (dev_id == DEVID(7,2)) {
        return 2000;  // delay 2ms
    } else if (dev_id == DEVID(9,1)) {
        return 0; // no temperature sensor for ET901
    } else {
        return 100;  // delay 100us
    }

}
void et7xa_get_temperature(int* code_out, int* temperature) {
#define G 2.86
#define K 20
#define C_REF 155
    uint8_t code = 0;
    if (code_out == NULL || temperature == NULL)
        return;
    if (g_egis_sensortype.dev_id0 == 7) {
        et7xx_write_register(ET7XA_ADDR_PWR_CTRL0, 0x00);  // Enable temperature sensor
        plat_sleep_time(get_delay_time_for_temperature_sensor_enable() / 1000);
        et7xx_write_register(ET7XA_ADDR_DAC_CSR, 0x00);
        et7xx_write_register(ET7XA_ADDR_DAC_CSR, 0x10);  // Temperature Sensing Go...
        plat_sleep_time(700 / 1000);                     // delay 700us
        et7xx_read_register(ET7XA_ADDR_TS_DOUT, &code);
        et7xx_write_register(ET7XA_ADDR_PWR_CTRL0, 0x40);  // Power-Down Temperature Sensor
        *code_out = code;
        *temperature = G * (code - C_REF) + K;
    } else{
        // no temperature sensor for ET901
        *temperature = 0;
    }
    egislog_d("get temperature code=%d temperature=%d", code, *temperature);
}

void et7xa_set_gain(BOOL gain2_enable) {
    uint8_t dev_id0 = g_egis_sensortype.dev_id0;
    uint8_t dev_id1 = g_egis_sensortype.dev_id1;

    switch (dev_id1) {
        case 29:
        case 11:
        case 13:
        case 2:
            if (gain2_enable == TRUE) {
                et7xx_write_register(ET711_ADDR_PGA_CSR, 0x02);
            } else {
                et7xx_write_register(ET711_ADDR_PGA_CSR, 0x00);
            }
            break;
        case 15:
            if (gain2_enable == TRUE) {
                et7xx_write_register(ET7XA_ADDR_PGA_CSR, 0x06);
            } else {
                et7xx_write_register(ET7XA_ADDR_PGA_CSR, 0x04);
            }
            break;
        case 26:
        default:
            break;
    }
}

int et7xa_sensor_mux_select(uint16_t select) {
    uint8_t dev_id0 = g_egis_sensortype.dev_id0;
    uint8_t dev_id1 = g_egis_sensortype.dev_id1;

    int ret = EGIS_OK;
    switch (dev_id1) {
        case 26:
            ret = et7xx_sensor_select(select);
            break;
        case 29:
        case 11:
        case 13:
        default:
            break;
    }

    return (ret == EGIS_OK) ? RESULT_OK : RESULT_FAILED;
}
