#include "bkimg_pool-header.h"
#include "core_config.h"
#include "ini_definition.h"
#include "isensor_api.h"
#include "plat_log.h"

#define LOG_TAG(x) (x == 0 ? LOG_DEBUG : LOG_ERROR)

#define ex_log_valid(x, format, ...)                                    \
    do {                                                                \
        if (x != 0)                                                     \
            ex_log(LOG_TAG(x), "compared fail," format, ##__VA_ARGS__); \
        else                                                            \
            ex_log(LOG_TAG(x), format, ##__VA_ARGS__);                  \
    } while (0)

bkg_pool_header_t bkimg_pool_get_configed_header(void) {
    bkg_pool_header_t header;
    header.version = BKG_POOL_VERSION;
    isensor_get_int(PARAM_INT_INTEGRATE_COUNT, &header.integrate_sw_count);
    isensor_get_int(PARAM_INT_HW_INTEGRATE_COUNT, &header.integrate_hw_count);
    isensor_get_int(PARAM_INT_EXPOSURE_TIME_X10, &header.exposure_time_x10);
    isensor_get_int(PARAM_INT_GAIN2_ENABLE, &header.gain2_enable);
    return header;
}

BOOL bkimg_pool_header_compare_valid(void* test, void* control) {
    bkg_pool_header_t *t = test, *c = control;
    int is_valid_version = memcmp(&t->version, &c->version, sizeof(t->version));
    int is_valid_exp =
        memcmp(&t->exposure_time_x10, &c->exposure_time_x10, sizeof(t->exposure_time_x10));
    int is_valid_sw_inte =
        memcmp(&t->integrate_sw_count, &c->integrate_sw_count, sizeof(t->integrate_sw_count));
    int is_valid_hw_inte =
        memcmp(&t->integrate_hw_count, &c->integrate_hw_count, sizeof(t->exposure_time_x10));
    int is_valid_gain2_enable = memcmp(&t->gain2_enable, &c->gain2_enable, sizeof(t->gain2_enable));

    int cmp_result = is_valid_version || is_valid_sw_inte || is_valid_hw_inte || is_valid_exp;

    ex_log_valid(is_valid_version, "bds;[version]  db: %d, present: %d", t->version, c->version);
    ex_log_valid(is_valid_sw_inte, "bds;[sw_count] db: %d, present: %d", t->integrate_sw_count,
                 c->integrate_sw_count);
    ex_log_valid(is_valid_hw_inte, "bds;[hw_count] db: %d, present: %d", t->integrate_hw_count,
                 c->integrate_hw_count);
    ex_log_valid(is_valid_exp, "bds;[exp_time_x10] db: %d, present: %d", t->exposure_time_x10,
                 c->exposure_time_x10);
    ex_log_valid(is_valid_gain2_enable, "bds;[gain2_enable] db: %d, present: %d", t->gain2_enable,
                 c->gain2_enable);

    return cmp_result == 0 ? TRUE : FALSE;
}

size_t bds_pool_header_get_size(void) {
    return sizeof(bkg_pool_header_t);
}
