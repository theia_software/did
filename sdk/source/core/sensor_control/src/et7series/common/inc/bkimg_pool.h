#ifndef __FD_PROCESS_BKG_H__
#define __FD_PROCESS_BKG_H__

#include <stdint.h>

#include "type_definition.h"

int bkimg_pool_init(uint8_t* bkgimg_pool, int* img_pool_size, int width, int height,
                    int integrate_count, uint16_t* default_bkg, int real_bpp);
void bkimg_pool_uninit();
BOOL bkimg_pool_is_initialized();
void bkimg_pool_set_sensor_id(uint8_t series, uint8_t type);

int bkimg_pool_add(uint16_t* raw16, uint16_t* img_detect, int temperature);
int bkimg_pool_add_get_slot_id();
int bkimg_pool_get(uint16_t** bds_bkg, uint16_t* raw16, int temperature, int integrate_count);

int bkimg_pool_load(char* path, int width, int height, int integrate_count, uint16_t* default_bkg,
                    int real_bpp);
int bkimg_pool_save(char* path);
int bkimg_pool_remove(char* path);

#endif
