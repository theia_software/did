#ifndef __SENSOR_CONFIG_H__
#define __SENSOR_CONFIG_H__

#define ET729_DEFAULT_HW_INTEGRATE_COUNT (8)
#define ET729_DEFAULT_INTEGRATE_COUNT (1)
#define ET729_DEFAULT_EXPOSE_TIME (12.f)

#define ET726_DEFAULT_HW_INTEGRATE_COUNT (1)
#define ET726_DEFAULT_INTEGRATE_COUNT (1)
#define ET726_DEFAULT_EXPOSE_TIME (12.f)

#define ET702_DEFAULT_HW_INTEGRATE_COUNT (1)
#define ET702_DEFAULT_INTEGRATE_COUNT (1)
#define ET702_DEFAULT_EXPOSE_TIME (12.f)

#define ET901_DEFAULT_HW_INTEGRATE_COUNT (1)
#define ET901_DEFAULT_INTEGRATE_COUNT (1)
#define ET901_DEFAULT_EXPOSE_TIME (40.f)

#define ET711_DEFAULT_HW_INTEGRATE_COUNT (8)
#define ET711_DEFAULT_INTEGRATE_COUNT (1)
#define ET711_DEFAULT_EXPOSE_TIME (12.f)
#define ET711_DEFAULT_GAIN2_ENABLE (FALSE)

#define ET713_DEFAULT_HW_INTEGRATE_COUNT (4)
#define ET713_DEFAULT_INTEGRATE_COUNT (1)
#define ET713_DEFAULT_EXPOSE_TIME (30.f)
#define ET713_DEFAULT_GAIN2_ENABLE (FALSE)

#define ET716_DEFAULT_HW_INTEGRATE_COUNT (1)
#define ET716_DEFAULT_INTEGRATE_COUNT (1)
#define ET716_DEFAULT_EXPOSE_TIME (12.f)

#define ET760_DEFAULT_HW_INTEGRATE_COUNT (1)
#define ET760_DEFAULT_INTEGRATE_COUNT (1)
//#define ET760_DEFAULT_EXPOSE_TIME (40.f)
#define ET760_DEFAULT_EXPOSE_TIME (200.f)

#define EXP_TIME_MS_TO_US 1000
#define FLOAT_TO_INT(x) ((int)(x + 0.5f))

#endif
