#ifndef __FD_PROCESS_BKG_INFO_H__
#define __FD_PROCESS_BKG_INFO_H__

#include "type_definition.h"

#if defined(G3PLUS_MATCHER)
#define __BDS3__
#elif defined(ALGO_GEN_5)
#ifdef LA_BDS
#define __LA_BDS__
#else
#define __BDS31__
#endif
#elif defined(ALGO_GEN_7)
#ifdef LA_BDS
#define __LA_BDS__
#else
#define __NBDS__
#endif
#endif

#if defined(__BDS3__)
#define BKG_POOL_VERSION (4)
#elif defined(__BDS31__)
#define BKG_POOL_VERSION (5)
#elif defined(__NBDS__)
#define BKG_POOL_VERSION (7)
#else
#define BKG_POOL_VERSION (3)
#endif

#define HASH_LEN (32)

typedef struct _bkg_pool_header_t_ {
    int version;
    int reserve[3];

    int exposure_time_x10;
    int integrate_hw_count;
    int integrate_sw_count;
    int gain2_enable;

    /* TODO:
    unsigned char sha256[HASH_LEN];
    */
} bkg_pool_header_t;

bkg_pool_header_t bkimg_pool_get_configed_header(void);
BOOL bkimg_pool_header_compare_valid(void* test, void* control);
size_t bds_pool_header_get_size(void);

#endif
