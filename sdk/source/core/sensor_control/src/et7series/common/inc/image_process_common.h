#ifndef __IMAGE_PROCESS_COMMON_H__
#define __IMAGE_PROCESS_COMMON_H__

void buf_16bit_to_8bit(uint8_t* tar, uint16_t* src, int size);
void buf_8bit_to_16bit(uint16_t* tar, uint8_t* src, int size);
int get_frame_mean(uint16_t* frame, int width, int height);
int get_frame_mean_8bit(uint8_t* frame, int width, int height);

#endif