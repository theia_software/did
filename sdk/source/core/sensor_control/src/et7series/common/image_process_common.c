#include <stdbool.h>
#include <stdint.h>
#include <string.h>

void buf_16bit_to_8bit(uint8_t* tar, uint16_t* src, int size) {
    for (int i = 0; i < size; i++) tar[i] = src[i] & 0xFF;
}

void buf_8bit_to_16bit(uint16_t* tar, uint8_t* src, int size) {
    for (int i = 0; i < size; i++) {
        tar[i] = (uint16_t)src[i];
    }
}

int get_frame_mean(uint16_t* frame, int width, int height) {
    int size = width * height;
    int mean = 0;
    int i;

    for (i = 0; i < size; i++) {
        mean += frame[i];
    }
    mean /= size;

    return mean;
}

int get_frame_mean_8bit(uint8_t* frame, int width, int height) {
    int size = width * height;
    int mean = 0;
    int i;

    for (i = 0; i < size; i++) {
        mean += frame[i];
    }
    mean /= size;

    return mean;
}
