#include "core_config.h"
#include "egis_definition.h"
#include "ini_definition.h"
#include "plat_file.h"
#include "plat_log.h"
#include "plat_mem.h"
#if defined(G3_MATCHER) && defined(G3PLUS_MATCHER)
#include "ImageProcessingLib.h"
#else
#include "work_with_G2/ImageProcessingLib.h"
#ifdef LA_BDS
#include "work_with_G2/la_bds.h"
#define LOG_TAG "RBS_LA_BDS"
#endif
#endif
#include "bkimg_pool-header.h"
#include "bkimg_pool.h"
#if defined(ALGO_GEN_7)
#include "work_with_G7/GoImageProcessingLib.h"
#endif

// For EGIS_IMAGE_KEEP_PARAM
#include "algomodule.h"

#include "isensor_api.h"

static uint8_t* g_bkgimg_db = NULL;
static int g_bkg_pool_size = 0;
#if defined(__BDS3__)
IP_BDS3_ImagePool* g_bds3_image_pool;
#define DEFAULT_BDS_BKG_COUNT 10
#ifdef LARGE_AREA
#define LESSTHAN_10_DEGREE_BDS_BKG_COUNT 8
#else
#define LESSTHAN_10_DEGREE_BDS_BKG_COUNT 4
#endif
#define MORIE_THRESHOLD 1000
#elif defined(__BDS31__)
IP_BDS3_ImagePool* g_bds3_image_pool;
#elif defined(__NBDS__)
GoIpp711param g_ipp711param;
#else
Ipp711param g_ipp711param;
#endif

static uint8_t g_sensor_seriers = 0;
static uint8_t g_sensor_type = 0;

static int g_min_morie;
static int g_min_morie_index;
static int g_max_morie;
static int g_max_morie_index;
static int g_min_morie_index_cnt;
static int g_slot_morie;

#ifdef __BDS31__
/* Temperature */
#define VERY_LOW_TEMPERATURE (-10)
#define LOW_TEMPERATURE 10
#define HIGH_TEMPERATURE 45
#define IS_VERY_LOW_TEMPERATURE(x) (x < VERY_LOW_TEMPERATURE)
#define IS_LOW_TEMPERATURE(x) ((x < LOW_TEMPERATURE) && (x >= VERY_LOW_TEMPERATURE))
#define IS_HIGH_TEMPERATURE(x) (x > HIGH_TEMPERATURE)
#define IS_NORMAL_TEMPERATURE(x) ((x >= LOW_TEMPERATURE) && (x <= HIGH_TEMPERATURE))

#define MIN_BKG_CNT 4
#define MAX_BKG_CNT 8

#define BDS_BK_BKG_SLOT 0
#define BDS_VERY_LOW_TEMP_SLOT 11
#define BDS_HIGH_TEMP_SLOT 12
#define BDS_LOW_TEMP_SLOT 13
#define BDS_MOIRE_SLOT 14
#define BDS_NORMAL_TEMP_SLOT 15

#define BDS_NORMAL_TEMP_SLOT_MAX_CNT 32

static int choose_bds_slot(const int temperature) {
    int slot_id;
    if (IS_VERY_LOW_TEMPERATURE(temperature)) {
        slot_id = BDS_VERY_LOW_TEMP_SLOT;
    } else if (IS_HIGH_TEMPERATURE(temperature)) {
        slot_id = BDS_HIGH_TEMP_SLOT;
    } else if (IS_LOW_TEMPERATURE(temperature)) {
        slot_id = BDS_LOW_TEMP_SLOT;
    } else {
        slot_id = BDS_NORMAL_TEMP_SLOT;
    }
    return slot_id;
}

#define SLOT_TOTAL_COUNT 16
static int _bds_get_slot_count(void* bds3_image_pool, int slot_index) {
    int total, count_array[SLOT_TOTAL_COUNT] = {0};
    IP_BDS3_get_pool_counter(bds3_image_pool, &total, count_array, SLOT_TOTAL_COUNT);
    return count_array[slot_index];
}
static void _bds_print_slot_count(void* bds3_image_pool, int slot_index) {
    int total, count_array[SLOT_TOTAL_COUNT] = {0};
    IP_BDS3_get_pool_counter(bds3_image_pool, &total, count_array, SLOT_TOTAL_COUNT);
    ex_log(LOG_DEBUG,
           "get_pool_counter, slot_id = %d, [0] %d, [10] %d,  [11] %d, [12] %d,"
           " [13] %d, [14] %d, [15] %d, total=%d",
           slot_index, count_array[0], count_array[10], count_array[11], count_array[12],
           count_array[13], count_array[14], count_array[15], total);
    return;
}
#endif

int bkimg_pool_init(uint8_t* bkgimg_pool, int* img_pool_size, int width, int height,
                    int integrate_count, uint16_t* default_bkg, int real_bpp) {
    if (width <= 0 || height <= 0 || img_pool_size == NULL) {
        ex_log(LOG_ERROR, "%s, bad w:h is %d:%d", __func__, width, height);
        return EGIS_INCORRECT_PARAMETER;
    }

    if (bkgimg_pool == NULL || *img_pool_size == 0) {
        ex_log(LOG_ERROR, "%s buffer error", __func__);
        return EGIS_INCORRECT_PARAMETER;
    }

    if (bkimg_pool_is_initialized()) {
        ex_log(LOG_ERROR, "%s - already initialized", __func__);
        return EGIS_INCORRECT_STATUS;
    }
//#ifdef LA_BDS
//    {
//        egislog_i("darkresearch LA_BDS");
//        int sensor_width;
//        int sensor_height;
//        isensor_get_sensor_roi_size(&sensor_width, &sensor_height);
//        labkg_init_bkg_data(default_bkg, sensor_width, sensor_height);
//     }
//#endif

    int size;
#if defined(__BDS3__) || defined(__BDS31__)
    size = IP_BDS3_get_img_pool_size(width, height);
#elif defined(__NBDS__)
    size = go_IPget_img_pool_size(width, height);
#elif defined(__LA_BDS__)
    {
        egislog_i("darkresearch LA_BDS");
        int sensor_width;
        int sensor_height;
        isensor_get_sensor_roi_size(&sensor_width, &sensor_height);
        //size = labkg_get_pool_size(width, height);
        size = labkg_get_pool_size(sensor_width, sensor_height);
    }
#else
    size = IPget_img_pool_size(width, height);
#endif
    if (*img_pool_size < size) {
        ex_log(LOG_ERROR, "%s, %d:%d, intput size %d < %d ", __func__, width, height,
               *img_pool_size, size);
        return EGIS_COMMAND_FAIL;
    } else if (*img_pool_size > size) {
        // Warning !!
        ex_log(LOG_ERROR, "%s, Warning %d:%d, intput size %d > %d ", __func__, width, height,
               *img_pool_size, size);
    }
#if defined(__BDS3__) || defined(__BDS31__)
    BOOL need_init = TRUE;
    if (((IP_BDS3_ImagePool*)bkgimg_pool)->version != 0) {
        int ret = IP_BDS3_check_img_pool(bkgimg_pool);
        if (ret != BDS3_IMAGE_POOL_OK) {
            ex_log(LOG_ERROR, "%s - check_img_pool failed %d", __func__, ret);
            // reset BDS
            memset(bkgimg_pool, 0, size);
        } else {
            ex_log(LOG_DEBUG, "%s - pool ok", __func__);
            need_init = FALSE;
        }
    }
#else
#if defined(__NBDS__)
    int ret = go_IPcheck_img_pool(bkgimg_pool);
#elif defined(__LA_BDS__)
    int ret = labkg_check_pool(bkgimg_pool);
#else
    int ret = IPcheck_img_pool(bkgimg_pool);
#endif
    if (ret != IMAGE_POOL_OK) {
        ex_log(LOG_ERROR, "%s - check_img_pool failed %d", __func__, ret);
        // reset BDS
        memset(bkgimg_pool, 0, size);
    }

    g_ipp711param.width = width;
    g_ipp711param.height = height;
    g_ipp711param.method = -1;
    g_ipp711param.integral_count = integrate_count;
    g_ipp711param.img_pool = bkgimg_pool;
    g_ipp711param.img_pool_max_count = 64;
    g_ipp711param.ipp_sp = NULL;
#endif

    switch (g_sensor_type) {
        case 11:
        case 29:
#if defined(__BDS3__) || defined(__BDS31__)

#elif defined(__NBDS__)
            go_init_ipp(&g_ipp711param, default_bkg, real_bpp);
#elif defined(__LA_BDS__)
            {
                egislog_i("darkresearch LA_BDS");
                int sensor_width;
                int sensor_height;
                isensor_get_sensor_roi_size(&sensor_width, &sensor_height);
                labkg_init_bkg_data(bkgimg_pool, default_bkg, sensor_width, sensor_height);
            }
#else
            init_ipp_711bg(&g_ipp711param, default_bkg, real_bpp);
#endif
            break;
        case 13:
        default: {
#if defined(__BDS3__)
            IP_BDS3_init_img_pool(bkgimg_pool, default_bkg, width, height, integrate_count,
                                  DEFAULT_BDS_BKG_COUNT, real_bpp);
            g_bds3_image_pool = (IP_BDS3_ImagePool*)bkgimg_pool;
#elif defined(__BDS31__)
            if (need_init) {
                IP_BDS3_init_img_pool(bkgimg_pool, default_bkg, width, height, integrate_count,
                                      MAX_BKG_CNT, real_bpp, 0);
                IP_BDS3_change_pool_count(bkgimg_pool, BDS_MOIRE_SLOT, NULL, 0, 4);
                IP_BDS3_change_pool_count(bkgimg_pool, BDS_NORMAL_TEMP_SLOT, NULL, 0,
                                          BDS_NORMAL_TEMP_SLOT_MAX_CNT);
            }
            g_bds3_image_pool = (IP_BDS3_ImagePool*)bkgimg_pool;
#elif defined(__NBDS__)
            go_init_ipp(&g_ipp711param, default_bkg, real_bpp);
#elif defined(__LA_BDS__)
            {
                egislog_i("darkresearch LA_BDS");
                int sensor_width;
                int sensor_height;
                isensor_get_sensor_roi_size(&sensor_width, &sensor_height);
                labkg_init_bkg_data(bkgimg_pool, default_bkg, sensor_width, sensor_height);
            }
#else
            int variant_th = IMG_POOL_VARIANT_THRESHOLD;
            int min_distance = IMG_POOL_MIN_DISTANCE;
            int var_mode = IMG_POOL_VAR_MODE_ON;

            init_ipp_variant(&g_ipp711param, variant_th, min_distance, var_mode);
            init_ipp_713bg(&g_ipp711param, default_bkg, real_bpp);
#endif
        } break;
    }

    *img_pool_size = size;
    g_bkg_pool_size = size;

    return EGIS_OK;
}

void bkimg_pool_uninit() {
#ifdef LA_BDS
	labkg_free_bkg_data();
#endif

    if (g_bkg_pool_size == 0) {
        return;
    }

    switch (g_sensor_type) {
        case 11:
        case 29:
#if defined(__BDS3__) || defined(__BDS31__)
#elif defined(__NBDS__)
            go_uninit_ipp(&g_ipp711param);
#elif defined(__LA_BDS__)
            labkg_free_bkg_data();
#else
            uninit_ipp_711();
#endif
            break;
        case 13:
        default:
#if defined(__BDS3__) || defined(__BDS31__)
            IP_BDS3_uninit_img_pool();
#elif defined(__NBDS__)
            go_uninit_ipp(&g_ipp711param);
#elif defined(__LA_BDS__)
            labkg_free_bkg_data();
#else
            uninit_ipp_713();
#endif
            break;
    }
    PLAT_FREE(g_bkgimg_db);

    g_bkg_pool_size = 0;
#if defined(__BDS3__) || defined(__BDS31__)
#elif defined(__NBDS__)
    g_ipp711param.width = 0;
    g_ipp711param.height = 0;
#elif defined(__LA_BDS__)
    g_ipp711param.width = 0;
    g_ipp711param.height = 0;
#else
    g_ipp711param.width = 0;
    g_ipp711param.height = 0;
#endif
}

BOOL bkimg_pool_is_initialized() {
    return g_bkg_pool_size > 0;
}

void bkimg_pool_set_sensor_id(uint8_t series, uint8_t type) {
    ex_log(LOG_DEBUG, "%s, %d %d", __func__, series, type);
    g_sensor_seriers = series;
    g_sensor_type = type;
}

int bkimg_pool_add(uint16_t* raw16, uint16_t* img_detect, int temperature) {
    int retval, bkgcount;
    RBS_CHECK_IF_NULL(raw16, EGIS_INCORRECT_PARAMETER);
    if (!bkimg_pool_is_initialized()) {
        return EGIS_INCORRECT_STATUS;
    }
#if defined(__BDS3__)
    if (temperature < 10) {
        bkgcount = LESSTHAN_10_DEGREE_BDS_BKG_COUNT;
    } else {
        bkgcount = DEFAULT_BDS_BKG_COUNT;
    }
    retval = IP_BDS3_update_bkg((void*)g_bds3_image_pool, raw16, g_min_morie, g_min_morie_index,
                                g_max_morie, g_max_morie_index, bkgcount, MORIE_THRESHOLD);
    ex_log(LOG_DEBUG,
           "%s, min_morie=%d, min_morie_index=%d, max_morie=%d, max_morie_index=%d, bkgcount=%d, "
           "morei_threshold=%d",
           __func__, g_min_morie, g_min_morie_index, g_max_morie, g_max_morie_index, bkgcount,
           MORIE_THRESHOLD);
#elif defined(__BDS31__)
    const int slot_id = choose_bds_slot(temperature);
    g_min_morie_index = slot_id;
    ex_log(LOG_DEBUG, "%s, slot_id %d", __func__, slot_id);
    retval = IP_BDS3_add_img_into_pool((void*)g_bds3_image_pool, raw16, slot_id, NULL);
#elif defined(__NBDS__)
    retval = go_IPadd_BDS(&g_ipp711param, raw16, temperature);
#elif defined(__LA_BDS__)
    egislog_d("LA_BDS labkg_update");
    retval = labkg_update(NULL);
#else
    int i, size = g_ipp711param.width * g_ipp711param.height;

    ex_log(LOG_DEBUG, "%s, T=%d.", __func__, temperature);
    // IPadd_img_into_pool(g_bkgimg_pool, raw16, g_bkg_width, g_bkg_height, temperature,
    // integrate_count);
    retval = IPadd_BDS(&g_ipp711param, raw16, temperature);
#endif
    if (retval != BDS3_IMAGE_POOL_OK) {
        ex_log(LOG_ERROR, "%s, IP_BDS3_update_bkg failed! ret=%d", __func__, retval);
    } else {
        ex_log(LOG_DEBUG, "%s, retval=%d", __func__, retval);
    }
    return retval;
}

int bkimg_pool_add_get_slot_id() {
    return g_min_morie_index;
}

int bkimg_pool_get(uint16_t** bds_bkg, uint16_t* raw16, int temperature, int integrate_count) {
    int bds_debug_path = 0, valid_temp = 0, bkgcount = 0;
    RBS_CHECK_IF_NULL(raw16, EGIS_INCORRECT_PARAMETER);
    if (!bkimg_pool_is_initialized()) {
        return EGIS_INCORRECT_STATUS;
    }

#if defined(__BDS3__)
    ex_log(LOG_DEBUG, "%s, t=%d", __func__, temperature);
    if (temperature < 10)
        bkgcount = core_config_get_int(INI_SECTION_SENSOR, KEY_BDS_LOW_TEMP_MIN_COUNT,
                                       LESSTHAN_10_DEGREE_BDS_BKG_COUNT);
    else
        bkgcount = DEFAULT_BDS_BKG_COUNT;
    ex_log(LOG_DEBUG, "%s, bkgcount=%d", __func__, bkgcount);
#elif defined(__BDS31__)
#define BDS_ENABLE 1
#define BDS_DISABLE 2
#define BDS_DEBUG_PATH_AUTO 3
    int min_bkg_cnt = MIN_BKG_CNT, max_bkg_cnt = MAX_BKG_CNT;
    BOOL slot_not_found = false;
    int slot_id = choose_bds_slot(temperature);
    ex_log(LOG_DEBUG, "%s entry, t=%d, slot_id %d", __func__, temperature, slot_id);
#endif

    switch (g_sensor_type) {
        case 11:
        case 29:
#if defined(__BDS3__) || defined(__BDS31__)
#elif defined(__NBDS__)
            *bds_bkg = go_IPBDS_get(&g_ipp711param);
#else
            *bds_bkg = ipp_711bg_debug_v2_fastBDS(&g_ipp711param, temperature, &valid_temp,
                                                  &bds_debug_path, bkgcount);
#endif
            break;
        case 13:
        default:
#if defined(__BDS3__)
            *bds_bkg = IP_BDS3_output_bkg((void*)g_bds3_image_pool, raw16, bkgcount, &g_min_morie,
                                          &g_min_morie_index, &g_max_morie, &g_max_morie_index);
            ex_log(LOG_DEBUG,
                   "%s, min_morie=%d, min_morie_index=%d, max_morie=%d, max_morie_index=%d "
                   "min_morie_index_cnt=%d",
                   __func__, g_min_morie, g_min_morie_index, g_max_morie, g_max_morie_index,
                   g_min_morie_index_cnt);
            if (*bds_bkg == NULL) {
                ex_log(LOG_ERROR, "%s, IP_BDS3_output_bkg failed", __func__);
                bds_debug_path = -1;
            } else {
                bds_debug_path = 30;
            }
#elif defined(__BDS31__)
            if (slot_id == BDS_NORMAL_TEMP_SLOT) {
                int normal_temp_count =
                    _bds_get_slot_count((void*)g_bds3_image_pool, BDS_NORMAL_TEMP_SLOT);
                if (normal_temp_count < min_bkg_cnt) {
                    slot_id = BDS_BK_BKG_SLOT;
                    ex_log(LOG_DEBUG, "%s, (%d < %d) slot_id=%d", __func__, normal_temp_count,
                           min_bkg_cnt, slot_id);
                }
            }
            *bds_bkg = IP_BDS3_get_bkg((void*)g_bds3_image_pool, slot_id, raw16, &g_slot_morie, 0,
                                       min_bkg_cnt);
            if (*bds_bkg == NULL) {
                ex_log(LOG_ERROR, "%s, IP_BDS3_get_bkg [%d] failed", __func__, slot_id);
                bds_debug_path = BDS_DISABLE;
                slot_not_found = TRUE;
            } else {
                bds_debug_path = BDS_ENABLE;
            }
            if (slot_not_found) {
                *bds_bkg = IP_BDS3_output_bkg(
                    (void*)g_bds3_image_pool, raw16, min_bkg_cnt, max_bkg_cnt, &g_min_morie,
                    &g_min_morie_index, &g_max_morie, &g_max_morie_index, &g_min_morie_index_cnt);
                ex_log(LOG_DEBUG,
                       "%s, min_morie=%d, min_morie_index=%d, max_morie=%d, max_morie_index=%d "
                       "min_morie_index_cnt=%d",
                       __func__, g_min_morie, g_min_morie_index, g_max_morie, g_max_morie_index,
                       g_min_morie_index_cnt);
                if (*bds_bkg == NULL) {
                    ex_log(LOG_ERROR, "%s, IP_BDS3_output_bkg failed", __func__);
                    bds_debug_path = BDS_DISABLE;
                } else {
                    slot_id = g_min_morie_index;
                    bds_debug_path = BDS_DEBUG_PATH_AUTO;
                }
            }
            bds_debug_path = bds_debug_path * 10000 + slot_id * 100;
            if (*bds_bkg != NULL) {
                bds_debug_path += _bds_get_slot_count((void*)g_bds3_image_pool, slot_id);
            }
            _bds_print_slot_count((void*)g_bds3_image_pool, *bds_bkg != NULL ? slot_id : -1);
#elif defined(__NBDS__)
            *bds_bkg = go_IPBDS_get(&g_ipp711param);
#elif defined(__LA_BDS__)
            egislog_d("LA_BDS __LA_BDS__");
            {
                int sensor_width, sensor_height;
                isensor_get_sensor_roi_size(&sensor_width, &sensor_height);
                *bds_bkg = labkg_get_bds_bkg(raw16, sensor_width, sensor_height);
            }
#else
            *bds_bkg = ipp_713bg_debug_v2_fastBDS(&g_ipp711param, temperature, &valid_temp,
                                                  &bds_debug_path, bkgcount);
#endif
            break;
    }
#if defined(__BDS3__) || defined(__BDS31__)

#else
    ex_log(LOG_DEBUG, "%s, debug_path=%d, valid_temp=%d", __func__, bds_debug_path, valid_temp);
    // BDS-bkg:1, PRE-bkg:2, BDS-CNT-bkg:3

    int roi_length = core_config_get_int(INI_SECTION_SENSOR, KEY_RAW_NORMALIZATION_ROI,
                                         INID_RAW_NORMALIZATION_ROI);
    if (roi_length) {
        // Use white box for default background, and raw normalization is ON.
        if (bds_debug_path == 3) {
            bds_debug_path = 32;
            ex_log(LOG_DEBUG, "%s, debug_path=3 -> %d", __func__, bds_debug_path);
            PLAT_FREE(*bds_bkg);
        }
    }
    if (*bds_bkg == NULL) {
        ex_log(LOG_DEBUG, "%s, got NULL", __func__);
    }
#endif
    EGIS_IMAGE_KEEP_PARAM(bds_debug_path, bds_debug_path);
    isensor_set_int(PARAM_INT_BDS_DEBUG_PATH, bds_debug_path);
    return EGIS_OK;
}

int bkimg_pool_load(char* path, int width, int height, int integrate_count, uint16_t* default_bkg,
                    int real_bpp) {
    unsigned int size, db_header_length;
    uint8_t* bkgimg_pool;
    unsigned int real_size = 0;
    int ret_size;
    int pool_size;
#if defined(__BDS3__) || defined(__BDS31__)
    pool_size = IP_BDS3_get_img_pool_size(width, height);
#elif defined(__NBDS__)
    pool_size = go_IPget_img_pool_size(width, height);
#elif defined(__LA_BDS__)
    {
        int sensor_width, sensor_height;
        isensor_get_sensor_roi_size(&sensor_width, &sensor_height);
        pool_size = labkg_get_pool_size(sensor_width, sensor_height);
    }
#else
    pool_size = IPget_img_pool_size(width, height);
#endif
    if (pool_size == 0) {
        ex_log(LOG_ERROR, "%s,  %d:%d, img_pool_size is 0", __func__, width, height);
        return EGIS_COMMAND_FAIL;
    }

    db_header_length = bds_pool_header_get_size();
    size = pool_size + db_header_length;

    bkimg_pool_uninit();

    if (g_bkgimg_db == NULL) {
        g_bkgimg_db = plat_alloc(size);
    } else {
        ex_log(LOG_ERROR, "%s, db pointer isn't NULL. unexpected flow", __func__);
    }
    RBS_CHECK_IF_NULL(g_bkgimg_db, EGIS_OUT_OF_MEMORY);

    bkg_pool_header_t present_configed_header = bkimg_pool_get_configed_header();
    ret_size = plat_load_file(path, g_bkgimg_db, size, &real_size);
    if (ret_size <= 0) {
        ex_log(LOG_ERROR, "%s, failed to load %s, ret_size=%d", __func__, path, ret_size);
        // Ignore failure of loading image pool file,
        // continue to call bkimg_pool_init() to initialize global variables
        memset(g_bkgimg_db, 0x0, size);
        memcpy(g_bkgimg_db, &present_configed_header, sizeof(bkg_pool_header_t));
    } else {
        bkg_pool_header_t expect_h = present_configed_header;
        ex_log(LOG_DEBUG, "%s, load size = %d , real_size = %d", __func__, size, real_size);
        int valid = bkimg_pool_header_compare_valid(g_bkgimg_db, &expect_h);
        if (!valid) {
            ex_log(LOG_ERROR, "%s, bkimg_pool_header_compare_valid = %d", __func__, valid);
            // Due to header compared failed.
            // The data from plat_load_file was invalid.
            // Prepared buffer with correct present header for DB memory
            memset(g_bkgimg_db, 0x0, size);
            memcpy(g_bkgimg_db, &present_configed_header, sizeof(bkg_pool_header_t));
        }
    }

    // Shift header size for BDS pool start addr.
    bkgimg_pool = g_bkgimg_db + db_header_length;
    return bkimg_pool_init(bkgimg_pool, &pool_size, width, height, integrate_count, default_bkg,
                           real_bpp);
}

int bkimg_pool_save(char* path) {
    int ret_size;
    RBS_CHECK_IF_NULL(g_bkgimg_db, EGIS_INCORRECT_STATUS);
    RBS_CHECK_IF_NULL(path, EGIS_INCORRECT_PARAMETER);

    unsigned int write_size = g_bkg_pool_size + bds_pool_header_get_size();
    ret_size = plat_save_file(path, g_bkgimg_db, write_size);
    if (ret_size > 0) {
        ex_log(LOG_DEBUG, "%s, %s is saved successfully %d", __func__, path, ret_size);
        return EGIS_OK;
    } else {
        ex_log(LOG_ERROR, "%s, %s failed to save. ret=%d", __func__, path, ret_size);
        return EGIS_COMMAND_FAIL;
    }
}

int bkimg_pool_remove(char* path) {
    int ret;
    RBS_CHECK_IF_NULL(path, EGIS_INCORRECT_PARAMETER);

    ret = plat_remove_file(path);
    ex_log(LOG_DEBUG, "%s %s ret=%d", __func__, path, ret);
    return ret;
}
