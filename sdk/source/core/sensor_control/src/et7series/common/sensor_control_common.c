#include <math.h>
#include <stdbool.h>

#include "egis_definition.h"
#include "fp_err.h"
#include "plat_log.h"
#include "plat_mem.h"
#include "plat_spi.h"
#include "plat_time.h"
#include "sensor_control_common.h"
#include "type_definition.h"
#ifdef __OTG_SENSOR__
#include "plat_otg.h"
#endif

#define LOG_TAG "RBS-Sensor"

int et7xx_write_register(uint8_t address, uint8_t val) {
    int ret;

    ret = io_7xx_dispatch_write_register(address, val);
    if (ret != EGIS_OK) {
        egislog_e("et7xx_write_register error");
        return RESULT_FAILED;
    }
    return RESULT_OK;
}

int et7xx_write_register_16(uint16_t address, uint16_t val) {
    int ret;

    ret = et7xx_write_register((address >> 8 & 0xFF), (val >> 8) & 0xFF);
    if (ret != RESULT_OK) return RESULT_FAILED;
    ret = et7xx_write_register(address & 0xFF, val & 0xFF);
    if (ret != RESULT_OK) return RESULT_FAILED;
    return RESULT_OK;
}

int et7xx_read_register(uint8_t address, uint8_t* val) {
    int ret;

    ret = io_7xx_dispatch_read_register(address, val);
    if (ret != EGIS_OK) {
        egislog_e("et7xx_read_register error");
        return RESULT_FAILED;
    }
    return RESULT_OK;
}

int et7xx_read_register_16(uint16_t address, uint16_t* val) {
    int ret;
    uint8_t temp_val, temp_val2;

    ret = et7xx_read_register((address >> 8) & 0xFF, &temp_val);
    if (ret != RESULT_OK) return RESULT_FAILED;
    ret = et7xx_read_register(address & 0xFF, &temp_val2);
    if (ret != RESULT_OK) return RESULT_FAILED;
    *val = ((temp_val << 8) & 0xFF00) | (temp_val2 & 0xFF);
    return RESULT_OK;
}

int et7xx_read_frame(uint8_t* buffer, int length) {
    int ret;

    ret = io_7xx_dispatch_get_frame(buffer, length, 1);
    if (ret != EGIS_OK) {
        egislog_e("et7xx_read_frame error");
    }
    return (ret == EGIS_OK) ? RESULT_OK : RESULT_FAILED;
}

int et7xx_polling_registry(uint8_t address, uint8_t expect, uint8_t mask) {
    int i;
    BYTE value;
    // egislog_d("%s",__func__);
    for (i = 0; i < 3000; i++) {
        if (io_7xx_dispatch_read_register(address, &value) != EGIS_OK) return RESULT_FAILED;
        if ((value & mask) == expect) return EGIS_OK;
        plat_sleep_time(1);
    }
    egislog_e("%s, value&mask = 0x%x, expect = 0x%x", __func__, value & mask, expect);

    return RESULT_FAILED;
}

int et7xx_io_command(int cmd, int param1, int param2, uint8_t* out_buf, int* out_buf_size) {
    return io_dispatch_command_read((enum io_dispatch_cmd)cmd, param1, param2, out_buf,
                                    out_buf_size);
}

int et7xx_sensor_select(uint16_t select) {
    int ret;
    egislog_d("%s,  0x%x", __func__, select);
    ret = io_7xx_dispatch_sensor_select(select);
    return (ret == EGIS_OK) ? RESULT_OK : RESULT_FAILED;
}

int et7xx_spi_write_read_halfduplex(uint8_t* writeBuf, int writeLen, uint8_t* readBuf, int readLen) {
    int ret;
    int bufLen = writeLen > readLen? writeLen:readLen;
    uint8_t* buffer = (uint8_t*)plat_alloc(bufLen);
    memcpy(buffer, writeBuf, writeLen);
    ret = io_7xx_dispatch_spi_write_read_halfduplex(buffer, writeLen, readLen);
    memcpy(readBuf, buffer, readLen);
    plat_free(buffer);
    return (ret == EGIS_OK) ? RESULT_OK : RESULT_FAILED;
}

int et7xx_set_spi_clk(uint8_t mode, uint8_t clk) {
    int ret;
    ret = io_7xx_dispatch_set_spi_clk(mode, clk);
    return (ret == EGIS_OK) ? RESULT_OK : RESULT_FAILED;
}

// mux 0: ET901 CTRL, 1: ET901 ADC, 2: HIVDAC
int et901_set_spi_mux(uint8_t mux) {
    int ret;

    uint8_t cmd[2];
    cmd[0] = 0xFD;
    cmd[1] = mux;

    ret = et7xx_spi_write_read_halfduplex(cmd, 2, NULL, 0);
    if (ret != RESULT_OK) {
        return ret;
    }
    return et7xx_set_spi_clk(0, (mux == 0)? 30 : 8);

}

// default 4.3, 10, 2.3, 10, -3
int et901_fpga_set_HIVDAC(uint16_t VPP1, uint16_t VPP2, uint16_t VCOM, uint16_t VGH, uint16_t VGL) {
    int ret;
    uint8_t cmd[3];

    egislog_d(LOG_TAG, "set HIVDAC %04X, %04X, %04X, %04X, %04X", VPP1, VPP2, VCOM, VGH, VGL);
    cmd[0] = 0x05;
    cmd[1] = (uint8_t)((VPP1 & 0xFF00) >> 8);
    cmd[2] = (uint8_t)(VPP1 & 0xFF);
    ret = et7xx_spi_write_read_halfduplex(cmd, 3, NULL, 0);
    if (ret != RESULT_OK) {
        return ret;
    }

    cmd[0] = 0x04;
    cmd[1] = (uint8_t)((VPP2 & 0xFF00) >> 8);
    cmd[2] = (uint8_t)(VPP2 & 0xFF);
    ret = et7xx_spi_write_read_halfduplex(cmd, 3, NULL, 0);
    if (ret != RESULT_OK) {
        return ret;
    }

    cmd[0] = 0x07;
    cmd[1] = (uint8_t)((VCOM & 0xFF00) >> 8);
    cmd[2] = (uint8_t)(VCOM & 0xFF);
    ret = et7xx_spi_write_read_halfduplex(cmd, 3, NULL, 0);
    if (ret != RESULT_OK) {
        return ret;
    }

    cmd[0] = 0x06;
    cmd[1] = 0x7F;
    cmd[2] = 0xFF;
    ret = et7xx_spi_write_read_halfduplex(cmd, 3, NULL, 0);
    if (ret != RESULT_OK) {
        return ret;
    }
    cmd[0] = 0x01;
    ret = et7xx_spi_write_read_halfduplex(cmd, 3, NULL, 0);
    if (ret != RESULT_OK) {
        return ret;
    }
    // update all

    cmd[0] = 0x90;
    cmd[1] = 0x00;
    cmd[2] = 0x00;
    ret = et7xx_spi_write_read_halfduplex(cmd, 3, NULL, 0);
    if (ret != RESULT_OK) {
        return ret;
    }
    plat_sleep_time(10);
    // set VGH
    cmd[0] = 0x06;
    cmd[1] = (uint8_t)((VGH & 0xFF00) >> 8);
    cmd[2] = (uint8_t)(VGH & 0xFF);
    ret = et7xx_spi_write_read_halfduplex(cmd, 3, NULL, 0);
    if (ret != RESULT_OK) {
        return ret;
    }
    // set VGL
    cmd[0] = 0x01;
    cmd[1] = (uint8_t)((VGL & 0xFF00) >> 8);
    cmd[2] = (uint8_t)(VGL & 0xFF);
    ret = et7xx_spi_write_read_halfduplex(cmd, 3, NULL, 0);
    if (ret != RESULT_OK) {
        return ret;
    }
    // update all
    cmd[0] = 0x90;
    cmd[1] = 0x00;
    cmd[2] = 0x00;
    ret = et7xx_spi_write_read_halfduplex(cmd, 3, NULL, 0);
    if (ret != RESULT_OK) {
        return ret;
    }
    return RESULT_OK;
}


