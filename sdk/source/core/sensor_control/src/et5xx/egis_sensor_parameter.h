#ifndef __EGIS_SENSOR_PARAMETER__H_
#define __EGIS_SENSOR_PARAMETER__H_

#include "egis_definition.h"

typedef struct _SensorParameter {
    BYTE bkg_average;
    BYTE bkg_standard_deviation;
    BYTE calibration_flag;

    BOOL sensor_is_long;
    BYTE sensor_gain;
    BYTE sensor_ds_vref_sel;
    BYTE sensor_ds_dc_p;
    BYTE sensor_ds_dc_c;
#ifdef FOD_CALIBRATION_DCFG
    BYTE sensor_ds_dc_f_g;
#endif
    BYTE sensor_ss_vref_sel;
    BYTE sensor_ss_dc_p;
    BYTE sensor_ss_dc_c;
#ifdef FOD_CALIBRATION_DCFG
    BYTE sensor_ss_dc_f_g;
#endif
    BYTE detect_gain;
    BYTE detect_vref_sel;
    BYTE detect_dc_p;
    BYTE detect_dc_c;
#ifdef FOD_CALIBRATION_DCFG
    BYTE detect_dc_f_g;
#endif
    BYTE detect_mean;
    BYTE detect_thh;
    BYTE detect_thl;

    BYTE realtime_dc_c;

    BYTE detect_threshold;
    BYTE vdm_bk[MAX_IMG_SIZE];
    BYTE vdm_hw_mean;

    BYTE bad_pixel[MAX_IMG_SIZE];
    int bad_pixel_count;
    BYTE z2_col_begin;
    BYTE z2_col_end;
    BYTE z2_col_step;
    BYTE z2_row_begin;
    BYTE z2_row_end;
    BYTE z2_row_step;

    /* legacy */
    union {
        BYTE sensor_dc_offset;
        BYTE detect_dc_offset;
        BYTE detect_cds_out;
    };

} SensorParameter;
typedef SensorParameter ET5XXCalibrationData;

#endif
