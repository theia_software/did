/******************************************************************************\
|*
*|
|*  calibrate_et510                                                           *|
|*  Version: 0.9.5.12                                                         *|
|*  Date: 2015/12/16                                                          *|
|*  Revise Date: 2016/06/27                                                   *|
|*  Copyright (C) 2007-2016 Egis Technology Inc.                              *|
|*                                                                            *|
\******************************************************************************/
#include <stdint.h>
#include <stdlib.h>

#include "../egis_handle_esd.h"
#include "EgisAlgorithmAPI.h"
#include "ICTeam/ET538LDOCalibrationFlow.h"
#include "ImageProcessingLib.h"
#include "common_definition.h"
#include "core_config.h"
#include "egis_fp_calibration.h"
#include "egis_fp_common_5XX.h"
#include "egis_log.h"
#include "egis_sensor_parameter.h"
#include "isensor_api.h"
#include "isensor_definition.h"
#include "plat_heap.h"
#include "plat_spi.h"
#include "plat_time.h"
#include "sensor_param.h"

#ifndef _WINDOWS
#ifndef TZ_MODE
#include <unistd.h>
#endif
#endif

#define LOG_TAG "RBS-EGISFP"

#ifdef HW_HAWAII_Y6
void egis_device_reset();
#endif
extern SensorParameter g_sensor_parameter;
BOOL gBoostModeOn = FALSE;
void fpsSetBoostModeOn(BOOL on) {
    egislog_d("%s %s", __func__, on ? "TRUE" : "FALSE");
    gBoostModeOn = on;
}
BOOL fpsGetBoostMode() {
    return gBoostModeOn;
}
FP_MODE g_fp_mode = FP_MODE_DONT_CARE;
BOOL g_Zone2_bad_pixel = FALSE;

static BYTE g_INT_CONF_value;

/*
 * 5XX specific control marco
 */
#include <time.h>
#define Sleep(ms) usleep(1000 * ms)
#define POLL_TIMEOUT 1000

#define ET5XX_MIN_INTENSITY 64
/*
 * 5XX specific control global Parameter
 */
int g_et5XX_sensor_gain = SENSOR_GAIN;
extern int g_sensing_mode;
int g_gain2_enable = FALSE;
int g_auto_calibrate_all = 0;

int g_target_detect_mean;
/*
 * 5XX specific function.
 */

/*
 * 5XX specific marco.
 */
#define EDGE_TRIGGER 0
#define LEVEL_TRIGGER 4

#define FOD_ACTIVE_HIGH 0
#define FOD_ACTIVE_LOW 1

#ifdef HW_HAWAII_Y6
#define FOD_TRIGGER_MODE EDGE_TRIGGER
#define INTERRUPT_POLARITY FOD_ACTIVE_HIGH
#else
#define FOD_TRIGGER_MODE LEVEL_TRIGGER
#define INTERRUPT_POLARITY FOD_ACTIVE_LOW
#endif

#define SCAN_ZONE_Z1 0
#define SCAN_ZONE_Z2 1
/*
 * 5XX specific parameters.
 */
static int g_LDO_enable = FALSE;
BOOL sensor_is_LDO_enable() {
    return g_LDO_enable;
}
static BYTE g_LDO_voltage = 0x0;   // REG_VSET=3(2.6V)
const int g_FOD_mode = FOD_MODE0;  // FOD_MODE0:mean value detect, FOD_MODE1:valleys detect mode

#define FOD_DCC_TARGET_MEAN 128
#define FOD_DCF_TARGET_MEAN 100

#ifdef HW_HAWAII_Y6
#define FOD_THH_DIFF 70
#define FOD_THL_DIFF 90
#define FOD_EXPECT_THL_DIFF (FOD_THH_DIFF - 15)
#else
#define FOD_THH_DIFF 80
#define FOD_THL_DIFF 40
#define FOD_EXPECT_THL_DIFF FOD_THL_DIFF
#endif

#define HW_DC_C_MAX 0x3F
#define HW_DC_C_MIN 0x0A
#define HW_TH_MAX 0xF0
#define HW_TH_MIN 0x32

/* Function Prototypes */
static int check_reset(void);
int isensor_recovery(FP_MODE fp_mode);
// int fp_check_read_register(BYTE address, BYTE *value);
inline int check_and_recovery(FP_MODE fp_mode);
int et5xx_fetch_intensity(BYTE* raw, unsigned int width, unsigned int height,
                          ET5XXCalibrationData* cd, unsigned int number_of_frames);
int et5xx_fetch_dynamic_intensity(BYTE* raw, unsigned int width, unsigned int height,
                                  ET5XXCalibrationData* cd, unsigned int number_of_frames);
static int calculate_statistic_by_hw(BYTE* pMin, BYTE* pMax, BYTE* pMean, int scan_zone);

static void sum_block(BYTE* img, int w, int h, int qty, int* sum, int* count) {
    int i, x, idx;
    if (qty < 60)
        idx = 0;
    else if (qty > 120)
        idx = 1;
    else
        return;
    for (i = 0; i < 8; i++, img += w) {
        for (x = 0; x < 8; x++) sum[idx] += img[x];
    }
    count[idx] += 8 * 8;
}

int bsearch_dc_c(BYTE target, BYTE* hw_mean, BYTE zone) {
    BYTE dc_c, dc_c_max = 0x3F, dc_c_min = 0x00, mean, max_diff = 255, diff,
               best_dc_c = (dc_c_min + dc_c_max) / 2, best_mean = 0;
    int i;
    for (i = 0; i < 6; i++) {  // adjust hw_mean near to target
        dc_c = (dc_c_min + dc_c_max) / 2;
        io_5xx_dispatch_write_register(DC_C_ET5XX_ADDR, dc_c);
        if (calculate_statistic_by_hw(NULL, NULL, &mean, zone) != EGIS_OK) return EGIS_COMMAND_FAIL;
        if ((diff = abs(mean - target)) < max_diff) {
            best_dc_c = dc_c;
            best_mean = mean;
            max_diff = diff;
        }
        if (mean > target)
            dc_c_max = dc_c;
        else
            dc_c_min = dc_c;
    }
    io_5xx_dispatch_write_register(DC_C_ET5XX_ADDR, best_dc_c);
    if (hw_mean != NULL) *hw_mean = best_mean;
    return best_dc_c;
}

static void offset_min(BYTE* raw, BYTE min, BYTE height, BYTE width) {
    int32_t image_size = width * height;
    int i;
    for (i = 0; i < image_size; i++) raw[i] -= min;
}

inline int get_gain(BYTE* gain) {
    *gain = 0;
    return EGIS_OK;
}

inline int set_gain(BYTE gain) {
    return EGIS_OK;
}
inline int get_dc(BYTE* dc) {
    *dc = 0;
    return EGIS_OK;
}

inline int set_dc(BYTE dc) {
    return EGIS_OK;
}
inline int fps_get_scan_z2_info(void) {
    if (io_5xx_dispatch_read_register(SCAN_Z2_COL_BEGIN_ET5XX_ADDR, (BYTE*)&SCAN_Z2_COL_BEGIN) !=
        EGIS_OK)
        return EGIS_COMMAND_FAIL;
    if (io_5xx_dispatch_read_register(SCAN_Z2_COL_END_ET5XX_ADDR, (BYTE*)&SCAN_Z2_COL_END) !=
        EGIS_OK)
        return EGIS_COMMAND_FAIL;
    if (io_5xx_dispatch_read_register(SCAN_Z2_COL_STEP_ET5XX_ADDR, (BYTE*)&SCAN_Z2_COL_STEP) !=
        EGIS_OK)
        return EGIS_COMMAND_FAIL;
    if (io_5xx_dispatch_read_register(SCAN_Z2_ROW_BEGIN_ET5XX_ADDR, (BYTE*)&SCAN_Z2_ROW_BEGIN) !=
        EGIS_OK)
        return EGIS_COMMAND_FAIL;
    if (io_5xx_dispatch_read_register(SCAN_Z2_ROW_END_ET5XX_ADDR, (BYTE*)&SCAN_Z2_ROW_END) !=
        EGIS_OK)
        return EGIS_COMMAND_FAIL;
    if (io_5xx_dispatch_read_register(SCAN_Z2_ROW_STEP_ET5XX_ADDR, (BYTE*)&SCAN_Z2_ROW_STEP) !=
        EGIS_OK)
        return EGIS_COMMAND_FAIL;

    return EGIS_OK;
}

int isensor_get_sensor_size(int* width, int* height) {
    int ret;

    if (SENSOR_WIDTH != 0 && SENSOR_HEIGHT != 0) {
        *width = SENSOR_WIDTH;
        *height = SENSOR_HEIGHT;
        return EGIS_OK;
    }

    // io_dispatch_connect(NULL);
    io_5xx_dispatch_read_register(REV_ID, &g_egis_sensortype.rev_id);
    io_5xx_dispatch_read_register(DEV_ID0, &g_egis_sensortype.dev_id0);
    io_5xx_dispatch_read_register(DEV_ID1, &g_egis_sensortype.dev_id1);
    egislog_i("%s, Sensor ID=%d %d-%d", __func__, g_egis_sensortype.series, g_egis_sensortype.type,
              g_egis_sensortype.rev_id);

    if (g_egis_sensortype.series != 0x05) {
        egislog_e("%s, Read_register FAIL, Sensor ID=%d %d-%d", __func__, g_egis_sensortype.series,
                  g_egis_sensortype.type, g_egis_sensortype.rev_id);
        return EGIS_NO_DEVICE;
    }

    switch (g_egis_sensortype.type) {
        case ET518:
            g_sensor_parameter.sensor_is_long = FALSE;
            *width = 114;
            *height = 114;
            SENSOR_WIDTH = 114;
            SENSOR_HEIGHT = 114;
            ret = fps_get_scan_z2_info();
            break;
        case ET510:  // 580
            g_sensor_parameter.sensor_is_long = TRUE;
            *width = 103;
            *height = 52;
            SENSOR_WIDTH = 103;
            SENSOR_HEIGHT = 52;
            NAV_WINDOW_W = 96;
            NAV_WINDOW_H = 40;
            NAV_REF_W = 72;
            NAV_REF_H = 32;
            ret = fps_get_scan_z2_info();
            break;
        case ET511:
            g_sensor_parameter.sensor_is_long = FALSE;
            *width = 69;
            *height = 69;
            SENSOR_WIDTH = 69;
            SENSOR_HEIGHT = 69;
            NAV_WINDOW_W = 52;
            NAV_WINDOW_H = 52;
            NAV_REF_W = 44;
            NAV_REF_H = 44;
            ret = fps_get_scan_z2_info();
            break;
        case ET512:
            g_sensor_parameter.sensor_is_long = TRUE;
            *width = 137;
            *height = 40;
            SENSOR_WIDTH = 137;
            SENSOR_HEIGHT = 40;
            NAV_WINDOW_W = 120;
            NAV_WINDOW_H = 32;
            NAV_REF_W = 90;
            NAV_REF_H = 28;
            ret = fps_get_scan_z2_info();
            break;
        case ET516:
            g_sensor_parameter.sensor_is_long = FALSE;
            *width = 70;
            *height = 57;
            SENSOR_WIDTH = 70;
            SENSOR_HEIGHT = 57;
            NAV_WINDOW_W = 48;
            NAV_WINDOW_H = 48;
            NAV_REF_W = 16;
            NAV_REF_H = 16;
            ret = fps_get_scan_z2_info();
            break;
        case ET520:
            g_sensor_parameter.sensor_is_long = FALSE;
            *width = 57;
            *height = 46;
            SENSOR_WIDTH = 57;
            SENSOR_HEIGHT = 46;
            // TODO: Fine tune NAVI info
            NAV_WINDOW_W = 40;
            NAV_WINDOW_H = 40;
            NAV_REF_W = 32;
            NAV_REF_H = 32;
            ret = fps_get_scan_z2_info();
            break;
        case ET522:
            g_sensor_parameter.sensor_is_long = FALSE;
            *width = 41;
            *height = 41;
            SENSOR_WIDTH = 41;
            SENSOR_HEIGHT = 41;
            // TODO: Fine tune NAVI info
            NAV_WINDOW_W = 40;
            NAV_WINDOW_H = 40;
            NAV_REF_W = 32;
            NAV_REF_H = 32;
            ret = fps_get_scan_z2_info();
            break;
        case ET523:
            g_sensor_parameter.sensor_is_long = TRUE;
            *width = 120;
            *height = 28;
            SENSOR_WIDTH = 120;
            SENSOR_HEIGHT = 28;
            // TODO: Fine tune NAVI info
            NAV_WINDOW_W = 120;
            NAV_WINDOW_H = 28;
            NAV_REF_W = 120;
            NAV_REF_H = 28;
            ret = fps_get_scan_z2_info();
            break;
        default:
            egislog_e("%s, Unknow Sensor type", __func__);
            return EGIS_NO_DEVICE;
            break;
    }
    if (SENSOR_WIDTH * SENSOR_HEIGHT > MAX_IMG_SIZE) {
        egislog_e("SENSOR_WIDTH * SENSOR_HEIGHT (%d*%d) > %d", SENSOR_WIDTH, SENSOR_HEIGHT,
                  MAX_IMG_SIZE);
        return EGIS_NO_DEVICE;
    }
    SENSOR_FULL_WIDTH = SENSOR_WIDTH;
    NAV_START_ROW = ((SENSOR_HEIGHT - NAV_WINDOW_H) / 2);
    NAV_START_COL = ((SENSOR_WIDTH - NAV_WINDOW_W) / 2);
    NAV_END_ROW = (NAV_START_ROW + NAV_WINDOW_H - 1);
    NAV_END_COL = (NAV_START_COL + NAV_WINDOW_W - 1);
    NAV_REF_OFFSET_W = ((NAV_WINDOW_W - NAV_REF_W) / 2);
    NAV_REF_OFFSET_H = ((NAV_WINDOW_H - NAV_REF_H) / 2);
    Z2_WIDTH = ((SCAN_Z2_COL_END - SCAN_Z2_COL_BEGIN) / SCAN_Z2_COL_STEP + 1);
    Z2_HEIGHT = ((SCAN_Z2_ROW_END - SCAN_Z2_ROW_BEGIN) / SCAN_Z2_ROW_STEP + 1);
    g_sensor_parameter.z2_col_begin = SCAN_Z2_COL_BEGIN;
    g_sensor_parameter.z2_col_end = SCAN_Z2_COL_END;
    g_sensor_parameter.z2_col_step = SCAN_Z2_COL_STEP;
    g_sensor_parameter.z2_row_begin = SCAN_Z2_ROW_BEGIN;
    g_sensor_parameter.z2_row_end = SCAN_Z2_ROW_END;
    g_sensor_parameter.z2_row_step = SCAN_Z2_ROW_STEP;
    egislog_d("%s, Sensor type = %d, (%d*%d), ret=%d", __func__, g_egis_sensortype.type,
              SENSOR_WIDTH, SENSOR_HEIGHT, ret);
    // io_dispatch_disconnect();
    return ret;
}

int et5xx_calibrate_bad_pixel(int sensorWidth, int sensorHeight, ET5XXCalibrationData* cd,
                              int* bad_count) {
    const int multi_count = 2;
    const int SensorFullSize = sensorWidth * sensorHeight;
    // BYTE buf[11];
    BYTE* raw = plat_alloc(SensorFullSize * (multi_count + 1));
    BYTE avg, *bp = cd->bad_pixel, *tmp = raw;
    int i, j, count = 0, x, y, dy, dx = 0;
    int ret;

    memset(bp, 0, sizeof(cd->bad_pixel));
    /*
    buf[0] = g_LDO_enable ? g_LDO_voltage : (0x80 + g_LDO_voltage); //0x09
    PWR_CTRL0 REG28 -> ON, REG_VSET
    buf[1] = 0x24; //0x0A PWR_CTRL1 Turn on VREF,PGA,ADC,V2I,BG and BG-SW
    buf[2] = 0x00; //0x0B PWR_CTRL2 Turn on CDA Amp and FPS array
    buf[3] = 0x44; //0x0C TurnOn 40MHz
    buf[4] = 0xF;// 0x0D VREF_SEL = 1.20V
    buf[5] = 0xC;// 0x0E DC_P
    buf[6] = 0x20;// 0x0F DC_C Vdcp-Vdcn=0V
    buf[7] = 0x20;// 0x10 DC_F_G padding
    buf[8] = 0; // 0x011 DC_F_SEL Don't use VDM
    buf[9] = 0; // 0x012 PGA_GAIN x1
    buf[10] = 0x32;//0x13 Single-Sensing Mode
    if (g_gain2_enable) buf[10] |= 0x40;
    io_5xx_dispatch_write_burst_register( PWR_CTRL0_ET5XX_ADDR, 11, buf);
    */

    io_5xx_dispatch_write_register(SCAN_FRAMES_ET5XX_ADDR, 0);
    /*
     * Below codes were used for fetch multi frame, and which were marked
     * because multi frame doesn't work due to unknow reason.
     *
    //if (io_5xx_dispatch_write_register(SCAN_CSR_ET5XX_ADDR, 0x13) !=
    EGIS_OK) return EGIS_COMMAND_FAIL;
    //polling_registry(STUS_ET5XX_ADDR,0x01,0x01);
    //io_5xx_dispatch_get_frame(raw, sensorHeight, sensorWidth,
    multi_count);
    */
    egislog_d("%s, bp_p=0x%p", __func__, bp);
    for (i = 0; i < (multi_count + 1); i++, tmp += SensorFullSize) {
        if (io_5xx_dispatch_write_register(SCAN_CSR_ET5XX_ADDR, 0x13) != EGIS_OK) {
            ret = EGIS_COMMAND_FAIL;
            goto fail;
        }
        polling_registry(STUS_ET5XX_ADDR, 0x01, 0x01);
        io_5xx_dispatch_get_frame(tmp, sensorHeight, sensorWidth, 0, 1);
        io_5xx_dispatch_write_register(SCAN_CSR_ET5XX_ADDR, 0x20);

        avg = IPcount_average_intensity(tmp, sensorWidth, sensorHeight, sensorWidth, 0);
        for (j = 0; j < SensorFullSize; j++) {
            if (abs(avg - tmp[j]) > 32) {
                bp[j] = 1;
                // egislog_d("bad pixel (%d,%d)", j %
                // sensorWidth, j / sensorHeight);
            }
        }
    }

    i = 0;
    for (j = 0; j < SensorFullSize; j++) {
        if (bp[j] == 1 || i == 1) {
            if (bp[j] == 1)
                i = 1;
            else {
                i = 0;
                bp[j] = 1;
            }
            count++;
        }
    }
    if (bad_count != NULL) *bad_count = count;

    egislog_d("Total bad pixels: %d", count);
    egislog_i("DEBUG_ENABLE_CALIBRATE_BAD_PIXEL");
    //--- DEBUG_DISABLE_CALIBRATE_BAD_PIXEL
    // egislog_i("DEBUG_DISABLE_CALIBRATE_BAD_PIXEL");
    // memset(cd->bad_pixel, 0, sizeof(cd->bad_pixel));
    // if (bad_count != NULL) *bad_count = 0;
    //^^^ DEBUG_DISABLE_CALIBRATE_BAD_PIXEL

    // check center pixel
    if (bp[((sensorHeight - 1) / 2) * sensorWidth + (sensorWidth - 1) / 2] == 1) {
        egislog_e("Central pixel Failure");
        ret = EGIS_CALIBRATION_DVR_FAIL;
        goto fail;
    }
    if (count > SensorFullSize / 2) {
        egislog_e("Too many bad pixels: %d.", count);
        ret = EGIS_CALIBRATION_DVR_FAIL;
        goto fail;
    }
    // Zone2 Test
    io_5xx_dispatch_read_burst_register(SCAN_Z2_COL_BEGIN_ET5XX_ADDR, 6, &(cd->z2_col_begin));
    if (cd->z2_col_step == 0 || cd->z2_row_step == 0) {
        egislog_e("(cd->z2_col_step == 0 || cd->z2_row_step == 0)");
        egislog_i("cd->z2_col_begin = 0x%x, cd->z2_col_end = 0x%x", cd->z2_col_begin,
                  cd->z2_col_end);
        ret = EGIS_CALIBRATION_DVR_FAIL;
        g_Zone2_bad_pixel = TRUE;
        goto fail;
    }
    do {  // try dx:0, 1,-1,2,-2,3,-3,4,-4 ...
        int x_begin = cd->z2_col_begin + dx, x_end = cd->z2_col_end + dx;
        if (x_begin < 0 || x_end >= sensorWidth) {
            egislog_e("(x_begin < 0 || x_end >= sensorWidth)");
            egislog_i("cd->z2_col_begin = 0x%x, cd->z2_col_end = 0x%x", cd->z2_col_begin,
                      cd->z2_col_end);
            egislog_i("x_begin = 0x%x x_end = 0x%x", x_begin, x_end);
            egislog_i("dx = %d", dx);
            ret = EGIS_CALIBRATION_DVR_FAIL;
            g_Zone2_bad_pixel = TRUE;
            goto fail;
        }

        dy = 0;
        do {  // try dy:0, 1,-1,2,-2,3,-3,4,-4 ...
            int y_begin = cd->z2_row_begin + dy, y_end = cd->z2_row_end + dy;
            if (y_begin < 0 || y_end >= sensorHeight) goto next_dx;
            bp = cd->bad_pixel + y_begin * sensorWidth;
            for (y = y_begin; y <= y_end;
                 y += cd->z2_row_step, bp += sensorWidth * cd->z2_row_step) {
                for (x = x_begin; x <= x_end; x += cd->z2_col_step) {
                    if (bp[x] == 1) goto next_dy;
                }
            }
            cd->z2_row_begin = y_begin;
            cd->z2_row_end = y_end;
            cd->z2_col_begin = x_begin;
            cd->z2_col_end = x_end;
            goto exit;
        next_dy:
            if (dy == 0)
                dy = 1;
            else if (dy > 0)
                dy = -dy;
            else
                dy = 1 - dy;
        } while (1);
    next_dx:
        if (dx == 0)
            dx = 1;
        else if (dx > 0)
            dx = -dx;
        else
            dx = 1 - dx;
    } while (1);

exit:

fail:
    plat_free(raw);
    if (g_Zone2_bad_pixel == TRUE) {
        egislog_e("g_Zone2_bad_pixel is TRUE");
        // Set Zone2 to default setting
        cd->z2_col_begin = SCAN_Z2_COL_BEGIN;
        cd->z2_col_end = SCAN_Z2_COL_END;
        cd->z2_col_step = SCAN_Z2_COL_STEP;
        cd->z2_row_begin = SCAN_Z2_ROW_BEGIN;
        cd->z2_row_end = SCAN_Z2_ROW_END;
        cd->z2_row_step = SCAN_Z2_ROW_STEP;
    }
    ret =
        io_5xx_dispatch_write_burst_register(SCAN_Z2_COL_BEGIN_ET5XX_ADDR, 6, &(cd->z2_col_begin));
    return ret;
}

static int sensor_power_control(BOOL on_off) {
    int ret;
    if (on_off) {
        if (g_LDO_enable == TRUE)
            io_5xx_dispatch_write_register(PWR_CTRL0_ET5XX_ADDR,
                                           g_LDO_voltage);           // REG28 -> ON, REG_VSET
        io_5xx_dispatch_write_register(PWR_CTRL1_ET5XX_ADDR, 0xF4);  // BG, BG-SW and V2I Power-ON
        io_5xx_dispatch_write_register(OSC40M_CSR_ET5XX_ADDR,
                                       0x44);  // OSC40M/CK40M ON!
        ret = EGIS_OK;
    } else {
        io_5xx_dispatch_write_register(OSC40M_CSR_ET5XX_ADDR, 0x22);
        io_5xx_dispatch_write_register(PWR_CTRL2_ET5XX_ADDR, 0x03);
        io_5xx_dispatch_write_register(PWR_CTRL1_ET5XX_ADDR, 0xFC);
        if (g_LDO_enable == TRUE)
            io_5xx_dispatch_write_register(PWR_CTRL0_ET5XX_ADDR, 0x40 + g_LDO_voltage);
        ret = EGIS_OK;
    }
    return ret;
}

static int calculate_statistics_of_frame(BYTE* in_frame, int width, int height, BYTE* pMin,
                                         BYTE* pMax, BYTE* pMean) {
    ET5XXCalibrationData* cd = &g_sensor_parameter;
    if (in_frame == NULL) {
        egislog_e("%s wrong param in_frame", __func__);
        return -1;
    }
    if (width > SENSOR_FULL_WIDTH || height != SENSOR_HEIGHT) {
        egislog_e("%s wrong param %d:%d", __func__, width, height);
        return -2;
    }
    int32_t image_size = width * height;
    int bad_pixel_count = 0;
    BYTE min = 255, max = 0, mean = 0;
    long sum = 0;
    int i, j;
    BYTE* pBadTemp = cd->bad_pixel + SENSOR_FRAME_OFFSET_X;
    BYTE* raw = in_frame;
    for (j = 0; j < height; j++) {
        for (i = 0; i < width; i++) {
            if (pBadTemp[i] == 1) {
                // egislog_e("change frame [%d] (%d,%d)",
                // bad_pixel_count, i + SENSOR_FRAME_OFFSET_X,
                // j);
                bad_pixel_count++;
                raw[i] = cd->vdm_hw_mean + 1;
            } else {
                if (raw[i] < min) min = raw[i];
                if (raw[i] > max) max = raw[i];
                sum += raw[i];
            }
        }
        pBadTemp += SENSOR_FULL_WIDTH;
        raw += width;
    }
    mean = (BYTE)(sum / (image_size - bad_pixel_count));
    if (pMin) *pMin = min;
    if (pMax) *pMax = max;
    if (pMean) *pMean = mean;

    egislog_v("SW statistics: min 0x%x, max 0x%x, mean 0x%x", min, max, mean);
    return EGIS_OK;
}

static int calculate_statistic_by_frames(BYTE* pMin, BYTE* pMax, BYTE* pMean, BYTE* in_frame,
                                         BYTE height, BYTE width) {
    BYTE min = 255, max = 0, mean = 0;
    int ret = EGIS_OK;
    long sum = 0;
    BYTE* raw = NULL;
    ET5XXCalibrationData* cd = &g_sensor_parameter;
    int32_t image_size = width * height;
    int X1 = 0, Y1 = 0, x_step = 1,
        y_step = 1;  // For Normal Size SENSOR_FULL_WIDTH, SENSOR_HEIGHT

    //==========For NAVI =============
    if (height < SENSOR_HEIGHT || width < SENSOR_FULL_WIDTH) {
        X1 = NAV_START_COL;
        Y1 = NAV_START_ROW;
    }

    if (in_frame == NULL) {
        raw = (BYTE*)plat_alloc(image_size);
        BYTE buf[2], retry = 0;
        buf[0] = 0x00;  // SCAN_FRAMES
        buf[1] = 0x50;  // SCAN_GO, , Zone-2

        ret = io_5xx_dispatch_write_burst_register(SCAN_FRAMES_ET5XX_ADDR, 2, buf);
        if (ret != EGIS_OK) goto exit;

        do {
            retry++;
            if (polling_registry(STUS_ET5XX_ADDR, 0x01, 0x01) != EGIS_OK) {
                egislog_e("STUS_ET5XX_ADDR EGIS_COMMAND_FAIL");
                if (retry >= 5) goto exit;
            } else
                break;
        } while (retry < 5);

        ret = io_5xx_dispatch_get_frame(raw, height, width, 0, 1);
        if (ret != EGIS_OK) goto exit;
        ret = io_5xx_dispatch_write_register(SCAN_CSR_ET5XX_ADDR, 0x20);
        if (ret != EGIS_OK) goto exit;

        if (height < SENSOR_HEIGHT || width < SENSOR_FULL_WIDTH)  ////For Size Zone2
        {
            egislog_d("Getting Zone2 Image, height width=(%d,%d)", height, width);
            X1 = SCAN_Z2_COL_BEGIN;
            Y1 = SCAN_Z2_ROW_BEGIN;
            x_step = SCAN_Z2_COL_STEP;
            y_step = SCAN_Z2_ROW_STEP;
        } else  // For Size Zone1, SENSOR_FULL_WIDTH, SENSOR_HEIGHT
        {
            egislog_d("Getting Zone1 Image, height width=(%d,%d)", height, width);
            X1 = 0;
            Y1 = 0;
        }
    } else
        raw = in_frame;

    int X1Y1 = X1 + Y1 * SENSOR_FULL_WIDTH;
    int X2Y1 = X1Y1 + (width - 1) * x_step;
    int X2Y2 = X2Y1 + SENSOR_FULL_WIDTH * (height - 1) * y_step;
    int i = X1Y1, j = 0, bad_pixel_count = 0;
    // egislog_d("(%dx%d)(x,y=%d,%d) (%d,%d,%d) step(%d,%d)",width,
    // height,X1, Y1, X1Y1, X2Y1, X2Y2, x_step, y_step);
    do {
        if (cd->bad_pixel[i] == 1) {
            bad_pixel_count++;
            // egislog_e("bp skip raw[%d]=%d, %d",
            // i,raw[j],bad_pixel_count);
            raw[j] = cd->vdm_hw_mean + 1;
        } else {
            if (raw[j] < min) min = raw[j];
            if (raw[j] > max) max = raw[j];
            sum += raw[j];
        }

        if (((i - X2Y1) % SENSOR_FULL_WIDTH) == 0)
            i += SENSOR_FULL_WIDTH - (width * x_step) + (y_step - 1) * SENSOR_FULL_WIDTH + x_step;
        else
            i += x_step;
        j++;
    } while (i <= X2Y2);

    mean = (BYTE)(sum / (image_size - bad_pixel_count));
    if (pMin) *pMin = min;
    if (pMax) *pMax = max;
    if (pMean) *pMean = mean;

    // egislog_d("sum(%d,%d,%d)",sum,image_size,bad_pixel_count);
    egislog_d("pMin = 0x%x, pMax = 0x%x, mean = 0x%x", min, max, mean);

exit:
    if ((in_frame == NULL) && (raw != NULL)) plat_free(raw);
    return ret;
}

static int calculate_statistic_by_hw(BYTE* pMin, BYTE* pMax, BYTE* pMean, int scan_zone) {
    BYTE reg_buf[3];
    BYTE buf[2];
    // Count A Frame ///////////////////////////////////////////////////
    // FIFO RESET
    // if (et505_write_register(SCAN_CTRL_ET505_ADDR, 0x20) != EGIS_OK)
    // return EGIS_COMMAND_FAIL;
    // TGEN_ON | SCAN_GO | DROP_DAT | MAF_EN | STAT_EN

    if (g_Zone2_bad_pixel == TRUE) {
        if (scan_zone == SCAN_ZONE_Z2)
            calculate_statistic_by_frames(pMin, pMax, pMean, NULL, Z2_HEIGHT, Z2_WIDTH);
        else if (scan_zone == SCAN_ZONE_Z1)
            calculate_statistic_by_frames(pMin, pMax, pMean, NULL, SENSOR_HEIGHT,
                                          SENSOR_FULL_WIDTH);
    } else {
        buf[0] = 0x00;  // SCAN_FRAMES
        // buf[1] = 0x57; //Zone-2, SCAN_GO, DROP_DAT, MAF_EN, STAT_EN
        buf[1] = 0x15;                  // SCAN_GO, DROP_DAT, STAT_EN
        if (scan_zone) buf[1] |= 0x42;  // Zone-2,  MAF_EN
        if (io_5xx_dispatch_write_burst_register(SCAN_FRAMES_ET5XX_ADDR, 2, buf) != EGIS_OK)
            return EGIS_COMMAND_FAIL;
        if (polling_registry(SCAN_CSR_ET5XX_ADDR, 0x0, 0x80) != EGIS_OK) return EGIS_COMMAND_FAIL;
        // Read STAT_MIN, STAT_MAX, STAT_MEAN registers
        if (io_5xx_dispatch_read_burst_register(STAT_MIN_ET5XX_ADDR, 3, reg_buf) != EGIS_OK)
            return EGIS_COMMAND_FAIL;
        if (pMin) *pMin = reg_buf[0];
        if (pMax) *pMax = reg_buf[1];
        if (pMean) *pMean = reg_buf[2];
    }

    egislog_i("pMin = 0x%x, pMax = 0x%x, pMean= 0x%x", reg_buf[0], reg_buf[1], reg_buf[2]);

    return EGIS_OK;
}

int set_detect_threshold(BYTE detect_threshold) {
    return io_5xx_dispatch_write_register(FOD_MEAN_THH_ET5XX_ADDR, detect_threshold);
}
static int set_detect_threshold_HL(BYTE mean, BYTE threshold_offset) {
    int ret = io_5xx_dispatch_write_register(FOD_MEAN_THH_ET5XX_ADDR, mean + threshold_offset);
    return ret | io_5xx_dispatch_write_register(FOD_MEAN_THL_ET5XX_ADDR, mean - threshold_offset);
}

int isensor_get_frame(BYTE* frame, UINT height, UINT width, UINT number_of_frames) {
    int ret;
    ret = check_and_recovery(g_fp_mode);
    if (ret != EGIS_OK) {
        egislog_d("%s, check_and_recovery ret = %d", __func__, ret);
        return ret;
    }
    return et5xx_fetch_intensity(frame, width, height, &g_sensor_parameter, number_of_frames);
}

int isensor_get_dynamic_frame(BYTE* frame, UINT height, UINT width, UINT number_of_frames) {
    int ret;
    TIME_MEASURE_START(isensor_get_dynamic_frame);
    ret = check_and_recovery(g_fp_mode);
    if (ret != EGIS_OK) {
        egislog_d("%s, check_and_recovery ret = %d ,g_mode = %d ", __func__, ret, g_fp_mode);
        return ret;
    }
    set_best_available_vdm_to_ic();

    ret =
        et5xx_fetch_dynamic_intensity(frame, width, height, &g_sensor_parameter, number_of_frames);
    TIME_MEASURE_STOP(isensor_get_dynamic_frame, "isensor_get_dynamic_frame");
    return ret;
}

int isensor_get_dynamic_frame_16bit(unsigned short* frame, UINT height, UINT width,
                                    UINT number_of_frames) {
    egislog_e("%s not supported !", __func__);
    return EGIS_OK;
}

int fp_tz_secure_set_power_on(void) {
    egislog_d("%s", __func__);
    return sensor_power_control(TRUE);
}

int fp_tz_secure_set_detect_mode_exit(void) {
    int ret = EGIS_COMMAND_FAIL;
    BYTE buf[2];

    egislog_d("%s", __func__);
    sensor_power_control(TRUE);

    ret = io_5xx_dispatch_write_register(FOD_CSR_ET5XX_ADDR, 0x0);  // 0x40 FPD_MAF_EN->0, FOD_EN->0

    if (polling_registry(FOD_CSR_ET5XX_ADDR, 0x0, 0x80) != EGIS_OK) {
        egislog_e("TurnOff FOD Fail");
    }
    g_INT_CONF_value = INTERRUPT_POLARITY | FOD_TRIGGER_MODE;  // 0x02 Edge Trigger|Enable
                                                               // FOD Interrupt
                                                               // Output|active high output
    buf[0] = g_INT_CONF_value;
    buf[1] = 0x0C;  // 0x01 INT_STUS
    io_5xx_dispatch_write_reverse_register(INT_CONF_ET5XX_ADDR, 2, buf);

    return ret;
}

int isensor_set_power_off(void) {
    int ret = EGIS_COMMAND_FAIL;
    BYTE value;

    egislog_d("%s", __func__);

    check_and_recovery(FP_MODE_DONT_CARE);

    // check if sensor is in detect mode
    io_5xx_dispatch_read_register(FOD_CSR_ET5XX_ADDR, &value);
    if ((value & 0x81) == 0x81) {
        egislog_d("%s, preivious detect_mode", __func__);
        ret = fp_tz_secure_set_detect_mode_exit();
        if (ret != EGIS_OK) return ret;
    }
    g_fp_mode = FP_STANDBY_MODE;
    return sensor_power_control(FALSE);
}

int set_cds_out(BYTE cds_out) {
    return EGIS_OK;
}
int et5xx_get_vdm_target_mean(BYTE* mean, BOOL fix) {
    int ret = EGIS_COMMAND_FAIL;
    BYTE buf[11];

    buf[0] = g_LDO_enable ? g_LDO_voltage
                          : (0x80 + g_LDO_voltage);  // 0x09 PWR_CTRL0 REG28 -> ON, REG_VSET
    buf[1] = 0x24;                                   // 0x0A Turn on VREF,PGA,ADC,V2I,BG and BG-SW
    buf[2] = 0x00;                                   // 0x0B Turn on CDA Amp and FPS array
    buf[3] = 0x44;                                   // 0x0C TurnOn 40MHz

    // buf[4] = 0x03;// 0x0D VREF_SEL = 0.6V
    // Using DS mode to calibrate FOD's vdm
    buf[4] = 0x0F;  // 0x0D VREF_SEL = 1.20V
    buf[5] = 0x08;  // 0x0E DC_P for padding
    buf[6] = 0x20;  // 0x0F DC_C Vdcp-Vdcn=0V
    buf[7] = 0x20;  // 0x10 DC_F_G Vdfp-Vdfn=0V
    buf[8] = 0x00;  // 0x011 DC_F_SEL Select Global DC_F_G
    buf[9] = 0x00;  // 0x012 PGA_GAIN x1
    /*
     *	During calibrate stage, both FOD mode 0 & 1 using DS to get vdm
     *data.
     */
    if (gBoostModeOn) {
        egislog_d("%s BoostModeOn", __func__);
        buf[10] = 0x42;
    } else {
        buf[10] = g_FOD_mode == FOD_MODE0 ? 0x52 : 0x52;  // 0x13 GAIN2_EN on, DS Mode
    }
    io_5xx_dispatch_write_burst_register(PWR_CTRL0_ET5XX_ADDR, 11, buf);

    /*
     *	For DS mode ref point mean.
     */
    if (fix) {
        buf[0] = 0x01;                     // 0x54 FIX_COLROW_EN
        buf[1] = (SENSOR_WIDTH - 1) / 2;   // 0x55 FIX_COL
        buf[2] = (SENSOR_HEIGHT - 1) / 2;  // 0x56 FIX_ROW
        io_5xx_dispatch_write_burst_register(FIX_COLROW_EN_ET5XX_ADDR, 3, buf);
    }

    ret = calculate_statistic_by_hw(NULL, NULL, mean,
                                    SCAN_ZONE_Z1);  // mean from a frame
    if (fix) io_5xx_dispatch_write_register(FIX_COLROW_EN_ET5XX_ADDR,
                                            0);  // Disable FIX_COLROW
    return ret;
}

int fp_tz_secure_pre_calibrate(void) {
    BYTE buf[12];
    BYTE hw_mean = 0x0;
    int ret;
    BYTE value;
    egislog_d("%s", __func__);

    ret = check_and_recovery(g_fp_mode);
    if (ret != EGIS_OK) return ret;

    // check if sensor is in detect mode
    io_5xx_dispatch_read_register(FOD_CSR_ET5XX_ADDR, &value);
    if ((value & 0x81) == 0x81) {
        egislog_i("pre_calibration, exit detect_mode first");
        ret = fp_tz_secure_set_detect_mode_exit();
        if (ret != EGIS_OK) return ret;
    }

    et5xx_get_vdm_target_mean(&hw_mean, FALSE);
    egislog_d("vdm target mean = 0x%x", hw_mean);
    // write target mean
    buf[0] = hw_mean;  // 0x33 CALI_VDM_TM
    buf[1] = 0x10;     // 0x34 CALI_CONF VDM(VDARK Memory) Calibration Enable
    buf[2] = 0x01;     // 0x35 CALI_EN
    if (io_5xx_dispatch_write_burst_register(CALI_VDM_TM_ET5XX_ADDR, 3, buf) != EGIS_OK)
        return EGIS_COMMAND_FAIL;
    if (polling_registry(CALI_CSR_ET5XX_ADDR, 0x00, 0x80) != EGIS_OK) return EGIS_COMMAND_FAIL;

    /*
     * Till now , the vdm of ds mode was gotten.
     * VDM data array would be storage in sram.
     * Function's caller need to apply vdm by itself.
     */

    return EGIS_OK;
}

int isensor_set_detect_mode(void) {
    BYTE buf[12];
    BYTE value;
    ET5XXCalibrationData* cd = &g_sensor_parameter;
    int ret = EGIS_COMMAND_FAIL;

    check_and_recovery(FP_MODE_DONT_CARE);

    // check if sensor is in detect mode
    io_5xx_dispatch_read_register(FOD_CSR_ET5XX_ADDR, &value);
    if ((value & 0x81) == 0x81) {
        egislog_d("%s, preivious detect_mode", __func__);
        ret = fp_tz_secure_set_detect_mode_exit();
        if (ret != EGIS_OK) return ret;
    }
#ifdef HW_HAWAII_Y6
    if (!g_LDO_enable) {
        egislog_d("set_detect_mode re K ", __func__);

        io_5xx_dispatch_write_register(PWR_CTRL1_ET5XX_ADDR,
                                       0x24);  // PWR_CTRL1, Turn ON V2I. Keep-ON BG & BG-SW only
        io_5xx_dispatch_write_register(PWR_CTRL2_ET5XX_ADDR, 0x00);  // PWR_CTRL2, padding, Turn
                                                                     // OFF CDS Amp and FPS Array
        io_5xx_dispatch_write_register(OSC40M_CSR_ET5XX_ADDR,
                                       0x44);  // OSC40M_CSR

        // for test
        calibrate_detect_mode(FALSE);
        egislog_d("set_detect_mode re K end", __func__);
    }
#endif

    g_fp_mode = FP_DETECT_MODE;
#ifdef IMAGE_ENHANCE_MODE
    io_5xx_dispatch_write_register(ENHANCE_MODE_ET5XX_ADDR,
                                   0x00);  // Close 5XX enhance mode
#endif

    // set zone 2 registers
    io_5xx_dispatch_write_burst_register(SCAN_Z2_COL_BEGIN_ET5XX_ADDR, 6, &(cd->z2_col_begin));
#ifdef FOD_CALIBRATION_DCFG
    egislog_i("%s gain(%d), dc 0x%X, 0x%X. th 0x%X, 0x%X.", __func__, cd->detect_gain,
              cd->detect_dc_c, cd->detect_dc_f_g, cd->detect_thh, cd->detect_thl);
#else
    egislog_i("%s gain(%d), dc 0x%X. threshold 0x%X, 0x%X.", __func__, cd->detect_gain,
              cd->detect_dc_c, cd->detect_thh, cd->detect_thl);
#endif
    egislog_d("detect_vref_sel = 0x%x  detect_dc_p = 0x%x", cd->detect_vref_sel, cd->detect_dc_p);

    buf[0] = g_LDO_enable ? g_LDO_voltage : (0x80 + g_LDO_voltage);  // 0x09 PWR_CTRL0 REG28
                                                                     // -> ON,
                                                                     // REG_VSET=3(2.6V)
    buf[1] = 0xF4;  // 0x0A Turn off VREF,PGA,ADC,V2I,BG and BG-SW Turn ON
                    // V2I. Keep-ON BG & BG-SW only
    buf[2] = 0x03;  // 0x0B Turn off CDA Amp and FPS array
    buf[3] = 0x44;  // 0x0C TurnOn 40MHz
    if (g_FOD_mode == FOD_MODE0) {
        buf[4] = cd->detect_vref_sel;  // 0x0D VREF_SEL = 1.20V
        buf[5] = cd->detect_dc_p;      // 0x0E DC_P
        buf[6] = cd->detect_dc_c;      // 0x0F DC_C Vdcp-Vdcn=0V
#ifdef FOD_CALIBRATION_DCFG
        buf[7] = cd->detect_dc_f_g;
        buf[8] = 0x0;  // Disable VDM
#else
        buf[7] = 0x20;  // 0x10 DC_F_G padding
        buf[8] = 0x01;  // 0x011 DC_F_SEL Select VDM
#endif
        buf[10] = 0x72;  // 0x13 GAIN2_EN on, SS Mode
    } else {
        buf[4] = 0x0F;  // 0x0D VREF_SEL = 1.20V
        buf[5] = 0x08;  // 0x0E DC_P
        buf[6] = 0x20;  // 0x0F DC_C Vdcp-Vdcn=0V
#ifdef FOD_CALIBRATION_DCFG
        buf[7] = cd->detect_dc_f_g;
        buf[8] = 0x0;  // Disable VDM
#else
        buf[7] = 0x20;  // 0x10 DC_F_G padding
        buf[8] = 0x01;  // 0x011 DC_F_SEL Select VDM
#endif
        buf[10] = 0x52;  // 0x13 GAIN2_EN on, DS Mode
    }
    buf[9] = cd->detect_gain;  // 0x012 PGA_GAIN x1
    io_5xx_dispatch_write_burst_register(PWR_CTRL0_ET5XX_ADDR, 11, buf);

    buf[0] = 0x0C;  // 0x01 INT_STUS
    g_INT_CONF_value =
        0x02 | INTERRUPT_POLARITY | FOD_TRIGGER_MODE;  // 0x02 Edge Trigger|Enable FOD
                                                       // Interrupt Output|active high
                                                       // output
    buf[1] = g_INT_CONF_value;
    io_5xx_dispatch_write_burst_register(INT_STUS_ET5XX_ADDR, 2, buf);
    sensor_power_control(FALSE);

    // Enter FOD mode
    if (g_FOD_mode == FOD_MODE0) {
        buf[0] = cd->detect_thl;  // 0x45 FOD Threshold-Low
        buf[1] = cd->detect_thh;  // 0x44 FOD Threshold-High
        buf[2] = 0x87;            // 0x50; //0x43 FOD Cycles - Low Byte //50ms
        buf[3] = 0x13;            // 0x00; //0x42 FOD Cycles - High Byte
        buf[4] = g_FOD_mode;      // 0x41 FOD Operation Mode
        buf[5] = 0x03;            // 0x40 Enable Moving Average Filter for Digital
                                  // FOD|Enable FOD
        io_5xx_dispatch_write_reverse_register(FOD_MEAN_THL_ET5XX_ADDR, 6, buf);
        // io_5xx_dispatch_write_burst_register(0x49, 1 , &value0x49);
    } else {
        buf[0] = 6;           // 0x47 FOD FOD_VALLEY_ROW_THL
        buf[1] = 6;           // 0x46 FOD_VALLEY_COL_TH
        buf[2] = 0x0;         // 0x45 FOD Threshold-Low
        buf[3] = 0x0;         // 0x44 FOD Threshold-High
        buf[4] = 0x87;        // 0x43 FOD Cycles - Low Byte
        buf[5] = 0x13;        // 0x42 FOD Cycles - High Byte
        buf[6] = g_FOD_mode;  // 0x41 FOD Operation Mode
        buf[7] = 0x03;        // 0x40 Enable Moving Average Filter for Digital
                              // FOD|Enable FOD
        io_5xx_dispatch_write_reverse_register(FOD_VALLEY_ROW_TH_ET5XX_ADDR, 8, buf);
    }

    // fp_dump_data();

    return EGIS_OK;
}

int fp_tz_secure_pullup_int(BOOL polarity) {
    g_INT_CONF_value = polarity ? FOD_ACTIVE_LOW : FOD_ACTIVE_HIGH;
    return io_5xx_dispatch_write_register(INT_CONF_ET5XX_ADDR, g_INT_CONF_value);
}

int isensor_pullup_int(BOOL polarity) {
    return fp_tz_secure_pullup_int(polarity);
}

int isensor_set_Z1_area(BOOL forceSetFull) {
    int ret;
    if (forceSetFull) {
        io_5xx_dispatch_write_register(SRBA_ADDR, 0);
        io_5xx_dispatch_write_register(SREA_ADDR, SENSOR_HEIGHT - 1);
        io_5xx_dispatch_write_register(SCBA_ADDR, 0);
        ret = io_5xx_dispatch_write_register(SCEA_ADDR, SENSOR_WIDTH - 1);
    } else {
        io_5xx_dispatch_write_register(SRBA_ADDR, SENSOR_FRAME_OFFSET_Y);
        io_5xx_dispatch_write_register(SREA_ADDR, SENSOR_FRAME_OFFSET_Y + SENSOR_HEIGHT - 1);
        io_5xx_dispatch_write_register(SCBA_ADDR, SENSOR_FRAME_OFFSET_X);
        ret = io_5xx_dispatch_write_register(SCEA_ADDR, SENSOR_FRAME_OFFSET_X + SENSOR_WIDTH - 1);
    }
    return ret;
}

int isensor_set_sensor_mode(void) {
    return isensor_set_custom_sensor_mode(g_sensing_mode);
}

int isensor_set_custom_sensor_mode(int sensing_mode) {
    check_and_recovery(FP_MODE_DONT_CARE);

    egislog_i("%s sensing_mode %d", __func__, sensing_mode);
    int ret = EGIS_COMMAND_FAIL;
    BYTE value;

    // check if sensor is in detect mode
    io_5xx_dispatch_read_register(FOD_CSR_ET5XX_ADDR, &value);
    if ((value & 0x81) == 0x81) {
        egislog_d("%s, preivious detect_mode", __func__);
        ret = fp_tz_secure_set_detect_mode_exit();
        if (ret != EGIS_OK) return ret;
    }

    if (g_LDO_enable) egislog_d("LDO_voltage=%d", g_LDO_voltage);

    g_fp_mode = FP_SENSOR_MODE;

    ET5XXCalibrationData* cd = &g_sensor_parameter;

    BYTE buf[11];
    buf[0] = g_LDO_enable ? g_LDO_voltage : (0x80 + g_LDO_voltage);  // 0x09 PWR_CTRL0 REG28
                                                                     // -> ON,
                                                                     // REG_VSET=3(2.6V)
    buf[1] = 0x24;  // 0x0A PWR_CTRL1 Turn on VREF,PGA,ADC,V2I,BG and BG-SW
    buf[2] = 0x00;  // 0x0B PWR_CTRL2 Turn on CDA Amp and FPS array
    buf[3] = 0x44;  // 0x0C TurnOn 40MHz

    if (sensing_mode == DELTA_SENSING_MODE) {
        buf[4] = cd->sensor_ds_vref_sel;  // 0x0D VREF_SEL = 1.20V
        buf[5] = cd->sensor_ds_dc_p;      // 0x0E DC_P
        buf[6] = cd->sensor_ds_dc_c;      // 0x0F DC_C Vdcp-Vdcn=0V
    } else {
        buf[4] = cd->sensor_ss_vref_sel;  // 0x0D VREF_SEL = 1.20V
        buf[5] = cd->sensor_ss_dc_p;      // 0x0E DC_P
        buf[6] = cd->sensor_ss_dc_c;      // 0x0F DC_C Vdcp-Vdcn=0V
    }

    buf[7] = 0x20;  // 0x10 DC_F_G padding
    buf[8] = 0x01;  // 0x11 DC_F_SEL Select VDM
    if (sensing_mode == DELTA_SENSING_MODE) {
        buf[9] = cd->sensor_gain;  // 0x012 PGA_GAIN x1
        if (gBoostModeOn) {
            egislog_d("%s BoostModeOn(%d)", __func__, cd->sensor_gain);
            buf[10] = 0x02;
        } else {
            buf[10] = 0x12;  // 0x13 GAIN2_EN on, Delta-Sensing
                             // Mode/Single-Sensing Mode
        }
    } else {
        buf[9] = 0x1;
        buf[10] = 0x32;  // 0x13 GAIN2_EN on, Delta-Sensing
                         // Mode/Single-Sensing Mode
    }
    if (g_gain2_enable) buf[10] |= 0x40;

    io_5xx_dispatch_write_burst_register(PWR_CTRL0_ET5XX_ADDR, 11, buf);
#ifdef IMAGE_ENHANCE_MODE
    io_5xx_dispatch_write_register(ENHANCE_MODE_ET5XX_ADDR,
                                   0x80);  // Open 5XX enhance mode
#endif
    cd->realtime_dc_c = 0xFF;
    ret =
        io_5xx_dispatch_write_burst_register(SCAN_Z2_COL_BEGIN_ET5XX_ADDR, 6, &(cd->z2_col_begin));

    ret = isensor_set_Z1_area(FALSE);

    return ret;
}

inline int fp_check_write_register(BYTE address, BYTE value) {
    int ret;
    ret = check_and_recovery(g_fp_mode);
    if (ret != EGIS_OK) return ret;
    return io_5xx_dispatch_write_register(address, value);
}

int fp_check_read_register(BYTE address, BYTE* value) {
    int ret;
    ret = check_and_recovery(g_fp_mode);
    if (ret != EGIS_OK) return ret;
    return io_5xx_dispatch_read_register(address, value);
}

static BOOL g_need_recovery = FALSE;

int isensor_get_recovery_event(void) {
    return g_need_recovery;
}

#define MAX_RECOVERY_TRY 5
inline int check_and_recovery(FP_MODE fp_mode) {
    int ret = EGIS_OK;
    static int try
        = 0;

#ifdef __DEVICE_DRIVER_MEIZU_1712__
    // TODO : fix
    egislog_d("check_and_recovery is skipped. TODO:fix");
    return EGIS_OK;
#endif

    if (!g_need_recovery) {
        ret = check_reset();
        egislog_d("check_reset %d", ret);
        if (ret == EGIS_OK)
            return EGIS_OK;
        else {
            egislog_d("do reset %d", ret);
            g_need_recovery = TRUE;
#ifdef HW_HAWAII_Y6
            egis_device_reset();
#else
            return EGIS_ESD_NEED_RESET;
#endif
        }
        return EGIS_OK;
    }

    ret = isensor_recovery(fp_mode);
    egislog_d("isensor_recovery %d", ret);
    if (ret != EGIS_OK) {
        egislog_e("!! isensor_recovery fail %d", ret);
    }
    if (ret == EGIS_OK) {
        g_need_recovery = FALSE;
        try
            = 0;
    } else {
        egislog_e("!! isensor_recovery retry", ret);
        try
            ++;
        ret = EGIS_ESD_NEED_RESET;
    }

    if (try > MAX_RECOVERY_TRY) {
        egislog_e("!! isensor_recovery fail %d, try=%d", ret, try);
        ret = EGIS_COMMAND_FAIL;
    }

    return ret;
}

static int et5xx_LDO_detect_calibrate() {
    BYTE value = 0;
    g_LDO_enable = OnBnClickedVldo();
    egislog_d("%s g_LDO_enable=%d", __func__, g_LDO_enable);
    if (g_LDO_enable) {
        io_5xx_dispatch_read_register(PWR_CTRL0_ET5XX_ADDR, &value);
        g_LDO_voltage = value & 0x07;
        if (gBoostModeOn)
            g_et5XX_sensor_gain = SENSOR_GAIN_LDO_BOOST_ON;
        else
            g_et5XX_sensor_gain = SENSOR_GAIN_LDO;

        egislog_d("LDO_voltage=%d, boost=%d", g_LDO_voltage, gBoostModeOn);
    }
    egislog_d("sensor gain = %d", g_et5XX_sensor_gain);
    return io_5xx_dispatch_write_register(PWR_CTRL0_ET5XX_ADDR,
                                          (g_LDO_enable ? 0x40 : 0x80) + g_LDO_voltage);
}

static int et5xx_power_on_initialize(void) {
    BYTE buf[2];
    int ret = EGIS_COMMAND_FAIL;
    ET5XXCalibrationData* cd = &g_sensor_parameter;

    egislog_d("%s", __func__);
    // Waitting for the device Available
    egislog_d("%s", __func__);
    if (polling_registry(STUS_ET5XX_ADDR, 0xAA, 0xFE) != EGIS_OK) return EGIS_COMMAND_FAIL;

    // Waitting for POACF Calibration Done
    if (polling_registry(POACF_CSR_ET5XX_ADDR, 0xC0, 0xC0) != EGIS_OK) return EGIS_COMMAND_FAIL;
    // 0x0A Turn on BG|turn on BG switch|Turn off VREF, PGA, Analog
    // FOD,ADC,V2I and test buffer
    ret = io_5xx_dispatch_write_register(PWR_CTRL1_ET5XX_ADDR, 0xFD);
    // disable POACF-Calibration
    io_5xx_dispatch_write_register(CALI_CSR_ET5XX_ADDR, 0x02);
    // STOP POACF (Power-ON Auto Calibration & FOD)
    io_5xx_dispatch_write_register(POACF_CSR_ET5XX_ADDR, 0x00);
    if (polling_registry(POACF_CSR_ET5XX_ADDR, 0x00, 0xC0) != EGIS_OK)
        egislog_e("%s, poll poacf fail 0x00", __func__);
    ret = io_5xx_dispatch_write_register(PWR_CTRL1_ET5XX_ADDR, 0xFC);

    buf[0] = 0x0F;  // 0x01 Clear RST & EFT Event &FOD Event flag
    g_INT_CONF_value =
        0x02 | INTERRUPT_POLARITY | FOD_TRIGGER_MODE;  // 0x02 Edge Trigger|Enable FOD
                                                       // Interrupt Output|active high
                                                       // output
    buf[1] = g_INT_CONF_value;
    if (io_5xx_dispatch_write_burst_register(INT_STUS_ET5XX_ADDR, 2, buf) != EGIS_OK)
        return EGIS_COMMAND_FAIL;
    io_5xx_dispatch_write_register(OSC40M_CSR_ET5XX_ADDR, 0x22);

#ifdef IMAGE_ENHANCE_MODE
    io_5xx_dispatch_write_register(ENHANCE_MODE_ET5XX_ADDR,
                                   0x00);  // Close 5XX enhance mode
#endif

    /*
     * Change Scan zone2 Region.
     */
    ret = io_5xx_dispatch_write_burst_register(SCAN_Z2_COL_BEGIN_ET5XX_ADDR, 6, &cd->z2_col_begin);
    return ret;
}

static int check_reset(void) {
    int try
        = 0, check_fail = 0;
    BYTE buf[0x22] = {0}, value = 0;
    int ret = EGIS_COMMAND_FAIL;

    if (polling_registry(STUS_ET5XX_ADDR, 0xAA, 0xFE) != EGIS_OK) goto command_fail;

    // check CHIP id ADDR:0xFF expect 0x5;
    ret = io_5xx_dispatch_read_register(0xFF, &value);
    if (ret != EGIS_OK || (value != 0x05)) {
        egislog_e("%s, addr[0xFF] =0x%X, expect 5", __func__, value);
        goto command_fail;
    }

    ret = io_5xx_dispatch_read_burst_register(STUS_ET5XX_ADDR, 0x22, &buf[0]);

    if (buf[0x1] & 0xC)
        egislog_d("%s, trigger = 0x%X", __func__, buf[0x01]);
    else if (buf[0x1] & 0x3)
        egislog_e("%s, INT_STUS = 0x%X", __func__, buf[0x01]);

    if ((buf[0x00] & 0xFE) != 0xAA) check_fail = 1;
    if (buf[0x01] & 0x3) check_fail = 1;
#ifdef HW_HAWAII_Y6
    if ((buf[0x02]) != g_INT_CONF_value) {
        check_fail = 1;
        egislog_e("%s, INT_CONF = 0x%X, expect 0x%X", __func__, buf[0x02], g_INT_CONF_value);
    }
#endif
    if (g_LDO_enable) {
        if ((buf[0x09] & 0x07) != g_LDO_voltage) check_fail = 1;
    } else {
        if ((buf[0x09] & 0x87) != (0x80 + g_LDO_voltage)) check_fail = 1;
    }

    if (check_fail) {
        for (try = 0; try < 0x22; try ++) egislog_e("Failed(1). buf[%x] 0x%x", try, buf[try]);
        egislog_d("Failed(1). LDO_voltage 0x%x", g_LDO_voltage);
        goto command_fail;
    }

    // check POACF Calibration
    ret = io_5xx_dispatch_read_register(POACF_CSR_ET5XX_ADDR, &value);
    if (ret != EGIS_OK || (value & 0xC0) != 0x00) {
        for (try = 0; try < 0x22; try ++) egislog_e("Failed(2). buf[%x] 0x%x", try, buf[try]);
        egislog_d("Failed(2). LDO_voltage 0x%x", g_LDO_voltage);
        goto command_fail;
    }
    return ret;
command_fail:
    // fp_dump_data();
    sensor_exception_set_active();
    return EGIS_COMMAND_FAIL;
}

int isensor_recovery(FP_MODE fp_mode) {
    int ret;
    egislog_d("isensor_recovery mode = %d", fp_mode);

    ret = fp_tz_secure_sensor_init();
    if (ret != EGIS_OK) {
        egislog_e("%s, fp_tz_secure_sensor_init ret = %d", __func__, ret);
        goto exit;
    }
    ret = et5xx_set_vdm(g_sensor_parameter.vdm_bk, SENSOR_FULL_WIDTH, SENSOR_HEIGHT);
    if (ret != EGIS_OK) {
        egislog_e("%s, et5xx_set_vdm ret = %d", __func__, ret);
        goto exit;
    }

    if (fp_mode == FP_MODE_DONT_CARE) {
        // Do nothing
    } else if (fp_mode == FP_SENSOR_MODE) {
        ret = isensor_set_sensor_mode();
    } else if (fp_mode == FP_DETECT_MODE) {
        ret = isensor_set_detect_mode();
    } else if (fp_mode == FP_NAVI_DETECT_MODE) {
        ret = isensor_set_navigation_detect_mode();
    } else if (fp_mode == FP_NAVI_SENSOR_MODE) {
        ret = isensor_set_navigation_sensor_mode();
    } else {
        ret = isensor_set_power_off();
    }
    if (ret != EGIS_OK) {
        egislog_e("%s, et5xx_set_vdm ret = %d", __func__, ret);
    }
exit:
    fp_dump_data();

    return ret;
}

int et5xx_has_interrupt(BOOL clean) {
    BYTE status;

    /*
     *	Delay for FOD duty cycle after enter detect mode.
     */
    // Sleep(60);
    if (io_5xx_dispatch_read_register(INT_STUS_ET5XX_ADDR, &status) != EGIS_OK)
        return EGIS_COMMAND_FAIL;

    egislog_d("%s, INT_STUS = 0x%X", __func__, status);
    if (g_LDO_enable) egislog_d("LDO_voltage=%d", g_LDO_voltage);

    if (status & 0x0F) {
        if (status & 0x1)  // RST
            egislog_d("%s, interrupt RST", __func__);
        if (status & 0x2)  // EFT
            egislog_d("%s, interrupt EFT", __func__);
        if (status & 0x4)  // THH
            egislog_d("%s, interrupt THH", __func__);
        if (status & 0x8)  // THL
            egislog_d("%s, interrupt THL", __func__);

        egislog_d("%s,return 1", __func__);
        if (clean)
            io_5xx_dispatch_write_register(INT_STUS_ET5XX_ADDR, 0x0C);  // clear detect status
        return 1;
    }
    egislog_d("%s, return 0", __func__);
    return 0;
}

int gNVM_MEAN_FROM_DCC_MAX = NVM_MEAN_MAX_INIT_FAIL;
static int gNVM_RV = 0;
int fpsGetNVMRV() {
    return gNVM_RV;
}
int et5xx_check_nvram() {
    int i, ret;
    BYTE buf[64];
    unsigned short expSum = 0;
    unsigned short curSum = 0;
    if (gNVM_RV > 0) {
        return EGIS_OK;
    }
    ret = isensor_read_nvm(buf);
    if (ret != EGIS_OK) return EGIS_OK;
    egislog_d("@egis-NVRAM:[%x,%x,%x,%x,%x,%x,%x,%x,%x,%x,%x,%x,%x,%x,%x,%x]\n", buf[0x13],
              buf[0x14], buf[0x15], buf[0x16], buf[0x17], buf[0x18], buf[0x19], buf[0x1A],
              buf[0x1B], buf[0x1C], buf[0x1D], buf[0x1E], buf[0x1F], buf[0x20], buf[0x21],
              buf[0x22]);
    egislog_d("@egis-NVRAM:[%x,%x,%x,%x,%x,%x,%x,%x,%x,%x,%x,%x,%x,%x,%x,%x]\n", buf[0x23],
              buf[0x24], buf[0x25], buf[0x26], buf[0x27], buf[0x28], buf[0x29], buf[0x2A],
              buf[0x2B], buf[0x2C], buf[0x2D], buf[0x2E], buf[0x2F], buf[0x30], buf[0x31],
              buf[0x32]);

    expSum = ((unsigned short)buf[NVM_INDEX_CHECKSUM_HB] << 8) | buf[NVM_INDEX_CHECKSUM_LB];
    for (i = 0x13; i <= 0x30; i++) {
        curSum += buf[i];
    }
    if ((curSum & 0xFFFF) != expSum) {
        egislog_e("@egis-NVRAM[FAIL:%d]", curSum);
        return EGIS_NVRAM_CHECKSUM;
    }
    if ((buf[NVM_INDEX_HW_DC_C] >= HW_DC_C_MIN && buf[NVM_INDEX_HW_DC_C] <= HW_DC_C_MAX) &&
        (buf[NVM_INDEX_HW_TH] >= HW_TH_MIN && buf[NVM_INDEX_HW_TH] <= HW_TH_MAX))
        gNVM_MEAN_FROM_DCC_MAX =
            ((buf[NVM_INDEX_HW_DC_C] * MEAN_PER_DCC) - (buf[NVM_INDEX_HW_TH] - FOD_THH_DIFF) + 100);
    else
        egislog_e("@egis-NVRAM, over_range");
    egislog_d("@egis-NVRAM, HW_DC_C:%d, HW_threshold:%d\n", buf[NVM_INDEX_HW_DC_C],
              buf[NVM_INDEX_HW_TH]);
    gNVM_RV = buf[NVM_INDEX_RV];
    egislog_i("@egis-NVRAM[OK!] RV=%d", gNVM_RV);
    return EGIS_OK;
}

int fp_tz_secure_sensor_init(void) {
    // io_dispatch_connect(NULL);
    BYTE buf[1];

    egislog_d("%s", __func__);
    // Waitting for the device Available
    if (polling_registry(STUS_ET5XX_ADDR, 0xAA, 0xFE) != EGIS_OK) return EGIS_COMMAND_FAIL;

    io_5xx_dispatch_read_register(INT_STUS_ET5XX_ADDR, &buf[0]);
    if (buf[0] & 0x3) {
        egislog_d("%s, reset!", __func__);
        et5xx_power_on_initialize();
    }
    et5xx_check_nvram();
    if (g_LDO_voltage == 0x0) et5xx_LDO_detect_calibrate();

    isensor_set_Z1_area(FALSE);
    return EGIS_OK;
}

static int et5xx_calibrate_analog(BYTE* ana) {
    BYTE buf[3];

    buf[0] = 01;    // 0x11  DC_F_SEL = VDM
    buf[1] = 0;     // 0x12 PGA_GAIN
    buf[2] = 0x72;  // 0x13 AFE_CONF0 GAIN2_EN, SINGLE_MODE
    if (io_5xx_dispatch_write_burst_register(DC_F_SEL_ET5XX_ADDR, 3, buf) != EGIS_OK)
        return EGIS_COMMAND_FAIL;
    buf[0] = 0x07;  // CALI_CONF VREF=>DC_P=>DC_C
    buf[1] = 0x01;  // CALI_CSR (CALI_GO=1)
    if (io_5xx_dispatch_write_burst_register(CALI_CONF_ET5XX_ADDR, 2, buf) != EGIS_OK)
        return EGIS_COMMAND_FAIL;
    polling_registry(CALI_CSR_ET5XX_ADDR, 0x00, 0x80);
#ifdef FOD_CALIBRATION_DCFG
    if (io_5xx_dispatch_read_burst_register(VREF_SEL_ET5XX_ADDR, 4, ana) != EGIS_OK)
        return EGIS_COMMAND_FAIL;
#else
    if (io_5xx_dispatch_read_burst_register(VREF_SEL_ET5XX_ADDR, 3, ana) != EGIS_OK)
        return EGIS_COMMAND_FAIL;
#endif
    return EGIS_OK;
}

int et5xx_calibrate_sensor_mode(void) {
    ET5XXCalibrationData* cd = &g_sensor_parameter;
    BYTE hw_mean = 0, dc_c;

    cd->sensor_gain = g_et5XX_sensor_gain;

    /*
     * SS sensor mode stage.sensor_vref_sel
     * Get vref_sel, dc_p and dc_c for ss mode.
     */
    if (et5xx_calibrate_analog(&(cd->sensor_ss_vref_sel)) != EGIS_OK) return EGIS_COMMAND_FAIL;
    io_5xx_dispatch_write_register(PGA_GAIN_ET5XX_ADDR, g_et5XX_sensor_gain);
    dc_c = bsearch_dc_c(ET5XX_MIN_INTENSITY, &hw_mean, SCAN_ZONE_Z1);
    if (dc_c == 0xFF) return EGIS_COMMAND_FAIL;
    if ((hw_mean > ET5XX_MIN_INTENSITY) && (dc_c > 0))
        io_5xx_dispatch_write_register(DC_C_ET5XX_ADDR, --dc_c);
    cd->sensor_ss_dc_c = dc_c;

    /*
     * DS sensor mode stage.
     * Using fixed analog parameter. The Image level will dynamic adjust by
     * dc_c.
     */
    cd->sensor_ds_vref_sel = 0x0F;
    cd->sensor_ds_dc_p = 0x08;
    cd->sensor_ds_dc_c = 0x20;
    return EGIS_OK;
}

int calibrate_detect_mode(BOOL toWaitForTHL) {
    BYTE hw_mean, buf[2];
    ET5XXCalibrationData* cd = &g_sensor_parameter;
    egislog_i("++++ calibrate_detect_mode ++++%d", toWaitForTHL);
    int thl_diff = toWaitForTHL ? FOD_EXPECT_THL_DIFF : FOD_THL_DIFF;

    if (et5xx_calibrate_analog(&(cd->detect_vref_sel)) != EGIS_OK) return EGIS_COMMAND_FAIL;

    buf[0] = cd->detect_gain = DETECT_GAIN;
    io_5xx_dispatch_write_register(PGA_GAIN_ET5XX_ADDR, buf[0]);

#ifdef FOD_CALIBRATION_DCFG
    io_5xx_dispatch_write_register(DC_F_SEL_ET5XX_ADDR, 0);  // Disable VDM
#else
    int j = 0;
    egislog_d("DCC detect_dc_c = 0x%x", cd->detect_dc_c);
    do {
        j++;
        io_5xx_dispatch_write_register(DC_C_ET5XX_ADDR, cd->detect_dc_c);
        if (calculate_statistic_by_hw(NULL, NULL, &hw_mean, SCAN_ZONE_Z2) != EGIS_OK)
            return EGIS_COMMAND_FAIL;
        if (hw_mean >= 50) {
            egislog_d("detect_mean = 0x%x dc_c= 0x%x", hw_mean, cd->detect_dc_c);
            break;
        }

        egislog_d("[%d] detect_mean = 0x%x dc_c= 0x%x", j, hw_mean, cd->detect_dc_c);
        cd->detect_dc_c++;
    } while (cd->detect_dc_c < 0x3F);
#endif
    int i = 0;
    egislog_d("DCC detect_dc_c = 0x%x", cd->detect_dc_c);
    do {
        i++;
        io_5xx_dispatch_write_register(DC_C_ET5XX_ADDR, cd->detect_dc_c);
        if (calculate_statistic_by_hw(NULL, NULL, &hw_mean, SCAN_ZONE_Z2) != EGIS_OK)
            return EGIS_COMMAND_FAIL;
        if (hw_mean <= FOD_DCC_TARGET_MEAN) {
            egislog_d("detect_mean = 0x%x dc_c= 0x%x", hw_mean, cd->detect_dc_c);
            break;
        }

        egislog_d("[%d] detect_mean = 0x%x dc_c= 0x%x", i, hw_mean, cd->detect_dc_c);
        cd->detect_dc_c--;
    } while (cd->detect_dc_c > 0);
    cd->detect_thh = ((hw_mean + FOD_THH_DIFF) > 0xFF) ? 0xFF : (hw_mean + FOD_THH_DIFF);
    cd->detect_thl = hw_mean > thl_diff ? (hw_mean - thl_diff) : 0;
    egislog_i(
        "1)  detect_mean = 0x%x dc_c= 0x%x, detect_thh = 0x%x, detect_thl "
        "= 0x%x",
        hw_mean, cd->detect_dc_c, cd->detect_thh, cd->detect_thl);

#ifdef FOD_CALIBRATION_DCFG
    i = 0;
    egislog_d("DCFG  detect_dc_f_g = 0x%x", cd->detect_dc_f_g);
    do {
        i++;
        io_5xx_dispatch_write_register(DC_F_G_ET5XX_ADDR, cd->detect_dc_f_g);
        if (calculate_statistic_by_hw(NULL, NULL, &hw_mean, SCAN_ZONE_Z2) != EGIS_OK)
            return EGIS_COMMAND_FAIL;
        if (hw_mean >= FOD_DCF_TARGET_MEAN) {
            egislog_d("detect_mean = 0x%x dc_c= 0x%x", hw_mean, cd->detect_dc_c);
            break;
        }

        egislog_d("[%d] detect_mean = 0x%x detect_dc_f_g= 0x%x", i, hw_mean, cd->detect_dc_f_g);
        cd->detect_dc_f_g++;
    } while (cd->detect_dc_f_g < 0x3F);
#endif
    cd->detect_thh = ((hw_mean + FOD_THH_DIFF) > 0xFF) ? 0xFF : (hw_mean + FOD_THH_DIFF);
    cd->detect_thl = hw_mean > thl_diff ? (hw_mean - thl_diff) : 0;
    cd->detect_mean = hw_mean;
    egislog_i(
        "2)  detect_mean = 0x%x dc_c= 0x%x, detect_thh = 0x%x, detect_thl "
        "= 0x%x",
        hw_mean, cd->detect_dc_c, cd->detect_thh, cd->detect_thl);

#ifdef HW_HAWAII_Y6
    if (!g_LDO_enable) {
        egislog_i("set THL 0");
        cd->detect_thl = 0;
    }
#endif

    cd->realtime_dc_c = 0xFF;
    // fp_dump_data();
    // dump
    // egislog_d("bkg_average = 0x%x\n", cd->bkg_average);
    // egislog_d("bkg_standard_deviation = 0x%x\n",
    // cd->bkg_standard_deviation);
    // egislog_d("calibration_flag = 0x%x\n", cd->calibration_flag);

    // egislog_d("sensor_gain = 0x%x\n", cd->sensor_gain);
    // egislog_d("sensor_vref_sel = 0x%x\n", cd->sensor_vref_sel);
    // egislog_d("sensor_ss_dc_p = 0x%x\n", cd->sensor_ss_dc_p);
    // egislog_d("sensor_ss_dc_c = 0x%x\n", cd->sensor_ss_dc_c);
    // egislog_d("detect_gain = 0x%x\n", cd->detect_gain);
    egislog_d("detect_vref_sel = 0x%x\n", cd->detect_vref_sel);
    egislog_d("detect_dc_p = 0x%x\n", cd->detect_dc_p);
    // egislog_d("detect_dc_c = 0x%x\n", cd->detect_dc_c);
    // egislog_d("detect_dc_f_g = 0x%x\n", cd->detect_dc_f_g);
    // egislog_d("detect_mean = 0x%x\n", cd->detect_mean);
    // egislog_d("detect_thh = 0x%x\n", cd->detect_thh);
    // egislog_d("detect_thl = 0x%x\n", cd->detect_thl);

    // egislog_d("realtime_dc_c = 0x%x\n", cd->realtime_dc_c);

    // egislog_d("detect_threshold = 0x%x\n", cd->detect_threshold);
    ////BYTE vdm_bk[SENSOR_WIDTH * SENSOR_HEIGHT];
    // egislog_d("vdm_hw_mean = 0x%x\n", cd->vdm_hw_mean);

    ////BYTE bad_pixel[SENSOR_WIDTH * SENSOR_HEIGHT];
    // egislog_d("bad_pixel_count = 0x%x\n", cd->bad_pixel_count);
    // egislog_d("z2_col_begin = 0x%x\n", cd->z2_col_begin);
    // egislog_d("z2_col_end = 0x%x\n", cd->z2_col_end);
    // egislog_d("z2_col_step = 0x%x\n", cd->z2_col_step);
    // egislog_d("z2_row_begin = 0x%x\n", cd->z2_row_begin);
    // egislog_d("z2_row_end = 0x%x\n", cd->z2_row_end);
    // egislog_d("z2_row_step = 0x%x\n", cd->z2_row_step);

    return EGIS_OK;
}

static int gFirstIntIsTHL = -1;
int isensor_read_int_status(BYTE* IntStatus) {
    int ret = fp_check_read_register(INT_STUS_ET5XX_ADDR, IntStatus);
    if (gFirstIntIsTHL == -1) {
        if (ret == EGIS_OK && INT_STATUS_THL(*IntStatus)) {
            egislog_e("reK: first Int is THL");
            gFirstIntIsTHL = 1;
            SetNeedReKVDM();
        } else {
            egislog_d("reK: first Int=%d", *IntStatus);
            gFirstIntIsTHL = 0;
        }
    }
    return ret;
}

int isensor_read_rev_id(BYTE* read_buf, int length) {
    if (length < SENSOR_REV_ID_LENGTH) return -1;

    int ret = io_5xx_dispatch_read_burst_register(REV_ID, SENSOR_REV_ID_LENGTH, read_buf);
    egislog_d("Rev ID: %Xh %Xh %Xh", read_buf[2], read_buf[1], read_buf[0]);
    return ret;
}

void fp_dump_data(void) {
#if 1
    int i = 0;
    BYTE read_buf[0xff] = {0};
    BYTE burst_read_addr = 0x0;  // Total amount registers read for one burst read OP.
    int burst_read_count = 20;   // For start address.

    egislog_d("%s", __func__);
    egislog_d("======");
    io_5xx_dispatch_read_burst_register(burst_read_addr, burst_read_count, read_buf);
    for (i = 0; i < burst_read_count; i++)
        egislog_d("addr[0x%x] = 0x%x", i + burst_read_addr, read_buf[i]);
    egislog_d("======");
    burst_read_count = 14;
    burst_read_addr = 0x20;
    io_5xx_dispatch_read_burst_register(burst_read_addr, burst_read_count, read_buf);
    for (i = 0; i < burst_read_count; i++)
        egislog_d("addr[0x%x] = 0x%x", i + burst_read_addr, read_buf[i]);
    egislog_d("======");
    burst_read_count = 13;
    burst_read_addr = 0x33;
    io_5xx_dispatch_read_burst_register(burst_read_addr, burst_read_count, read_buf);
    for (i = 0; i < burst_read_count; i++)
        egislog_d("addr[0x%x] = 0x%x", i + burst_read_addr, read_buf[i]);
    egislog_d("======");
    burst_read_count = 9;
    burst_read_addr = 0x40;
    io_5xx_dispatch_read_burst_register(burst_read_addr, burst_read_count, read_buf);
    for (i = 0; i < burst_read_count; i++)
        egislog_d("addr[0x%x] = 0x%x", i + burst_read_addr, read_buf[i]);
    egislog_d("======");
    burst_read_count = 6;
    burst_read_addr = 0x51;
    io_5xx_dispatch_read_burst_register(burst_read_addr, burst_read_count, read_buf);
    for (i = 0; i < burst_read_count; i++)
        egislog_d("addr[0x%x] = 0x%x", i + burst_read_addr, read_buf[i]);
    egislog_d("======");
    burst_read_count = 1;
    burst_read_addr = 0x61;
    io_5xx_dispatch_read_burst_register(burst_read_addr, burst_read_count, read_buf);
    for (i = 0; i < burst_read_count; i++)
        egislog_d("addr[0x%x] = 0x%x", i + burst_read_addr, read_buf[i]);
    egislog_d("======");
    burst_read_count = 3;
    burst_read_addr = 0x67;
    io_5xx_dispatch_read_burst_register(burst_read_addr, burst_read_count, read_buf);
    for (i = 0; i < burst_read_count; i++)
        egislog_d("addr[0x%x] = 0x%x", i + burst_read_addr, read_buf[i]);
    egislog_d("======");
    burst_read_count = 1;
    burst_read_addr = 0x80;
    io_5xx_dispatch_read_burst_register(burst_read_addr, burst_read_count, read_buf);
    for (i = 0; i < burst_read_count; i++)
        egislog_d("addr[0x%x] = 0x%x", i + burst_read_addr, read_buf[i]);
#endif
}

int et5xx_fetch_intensity(BYTE* raw, unsigned int width, unsigned int height,
                          ET5XXCalibrationData* cd, unsigned int number_of_frames) {
    BYTE min = 0;
    BYTE reg_buf[2];
    int32_t image_size = width * height;

#ifdef UNDER_GLASS

#define INTEGRATE_COUNT 4
    BYTE raw_temp[image_size * INTEGRATE_COUNT];
    {
        int sum[image_size], i, j, k, m;
        memset(sum, 0x00, sizeof(sum));
        BYTE* img_ptr;
        reg_buf[0] = INTEGRATE_COUNT - 1;
        reg_buf[1] = 0x13;
        if (io_5xx_dispatch_write_burst_register(SCAN_FRAMES_ET5XX_ADDR, 2, reg_buf) != EGIS_OK)
            return EGIS_COMMAND_FAIL;
        if (polling_registry(STUS_ET5XX_ADDR, 0x01, 0x01) != EGIS_OK) return EGIS_COMMAND_FAIL;

        for (i = 0; i < number_of_frames; i++) {
            io_5xx_dispatch_get_frame(raw_temp, height, width, 0, INTEGRATE_COUNT);
            for (j = 0; j < INTEGRATE_COUNT; j++) {
                img_ptr = raw_temp + (j * image_size);
                if (EGIS_OK ==
                    calculate_statistics_of_frame(img_ptr, width, height, &min, NULL, NULL)) {
                    offset_min(img_ptr, min, height, width);
                }
                for (k = 0; k < image_size; k++) sum[k] += img_ptr[k];
            }
            img_ptr = raw + (i * image_size);
            int32_t upper_bound = 255 * (INTEGRATE_COUNT - 1);
            for (m = 0; m < image_size; m++)
                img_ptr[m] = (sum[m] > upper_bound) ? 255 : sum[m] / (INTEGRATE_COUNT - 1);
        }
    }
#else
    reg_buf[0] = number_of_frames - 1;
    reg_buf[1] = 0x13;
    if (io_5xx_dispatch_write_burst_register(SCAN_FRAMES_ET5XX_ADDR, 2, reg_buf) != EGIS_OK)
        return EGIS_COMMAND_FAIL;
    polling_registry(STUS_ET5XX_ADDR, 0x01, 0x01);
    int ret = io_5xx_dispatch_get_frame(raw, height, width, 0, number_of_frames);

    unsigned int i = 0;
    for (i = 0; i < number_of_frames; i++) {
        if (g_fp_mode != FP_NAVI_SENSOR_MODE &&
            EGIS_OK == calculate_statistics_of_frame(raw + (i * image_size), width, height, &min,
                                                     NULL, NULL)) {
            offset_min(raw + (i * image_size), min, height, width);
        }
    }

    if (ret != EGIS_OK) {
        egislog_e("io_5xx_dispatch_get_frame fail; ret = %d", ret);
    }

#endif
    return io_5xx_dispatch_write_register(SCAN_CSR_ET5XX_ADDR, 0x20);
}

int et5xx_fetch_dynamic_intensity(BYTE* raw, unsigned int width, unsigned int height,
                                  ET5XXCalibrationData* cd, unsigned int number_of_frames) {
    BYTE max, min;
    BYTE gain;
    int i, diff;
    const int GAIN_X2e10[16] = {2048,  14684, 17121, 19558, 20623, 24371, 26829, 29286,
                                31744, 34202, 36454, 38912, 41370, 43827, 46285, 48742};

    if (g_sensing_mode == DELTA_SENSING_MODE) {  // dynamic tune dc_c
        egislog_d("%s, DELTA_SENSING_MODE", __func__);
        calculate_statistic_by_hw(&min, &max, NULL, SCAN_ZONE_Z2);
        if (min == 0 && max == 255) {  // too much gain
            io_5xx_dispatch_read_register(PGA_GAIN_ET5XX_ADDR, &gain);
            if (gain > 1) io_5xx_dispatch_write_register(PGA_GAIN_ET5XX_ADDR, gain - 1);
            goto start_capture;
        }
        if (cd->realtime_dc_c == 0xFF)
            io_5xx_dispatch_read_register(DC_C_ET5XX_ADDR, &cd->realtime_dc_c);
        if (max == 255) {  // max=255, maybe overflow
            egislog_d("%s,Max == 0xff", __func__);
            diff = min * 1024;
            if (g_gain2_enable) diff /= 2;
            io_5xx_dispatch_read_register(PGA_GAIN_ET5XX_ADDR, &gain);
            for (i = cd->realtime_dc_c; i >= 0; i--) {
                if (diff < GAIN_X2e10[gain]) break;
                diff -= GAIN_X2e10[gain];
            }
            if (i != cd->realtime_dc_c) {
                io_5xx_dispatch_write_register(DC_C_ET5XX_ADDR, i);
                cd->realtime_dc_c = i;
            }
        } else if (min == 0) {  // min = 0, maybe under flow
            egislog_d("%s,Min == 0x0", __func__);
            diff = (255 - max) * 1024;
            if (g_gain2_enable) diff /= 2;
            io_5xx_dispatch_read_register(PGA_GAIN_ET5XX_ADDR, &gain);
            for (i = cd->realtime_dc_c; i <= 0x3F; i++) {
                if (diff < GAIN_X2e10[gain]) break;
                diff -= GAIN_X2e10[gain];
            }
            if (i != cd->realtime_dc_c) {
                io_5xx_dispatch_write_register(DC_C_ET5XX_ADDR, i);
                cd->realtime_dc_c = i;
            }
        }
    }
start_capture:
    return et5xx_fetch_intensity(raw, width, height, cd, number_of_frames);
}

int fp_tz_secure_sensor_prob() {
    int ret = 0;

    BYTE val, temp_val;
    int i;
    egislog_d("fp_tz_secure_sensor_prob()");

    ret = io_5xx_dispatch_read_register(ADC_DAT_SUBS_ET5XX_ADDR, &temp_val);

    if (ret != EGIS_OK) return ret;

    for (i = 0; i <= 0xFF; i++) {
        ret = io_5xx_dispatch_write_register(ADC_DAT_SUBS_ET5XX_ADDR, i);

        if (ret != EGIS_OK) {
            ret = EGIS_NO_DEVICE;
            goto end;
        }

        ret = io_5xx_dispatch_read_register(ADC_DAT_SUBS_ET5XX_ADDR, &val);

        if (ret != EGIS_OK) {
            ret = EGIS_NO_DEVICE;
            goto end;
        }

        if (val != i) {
            egislog_e("register %d, write = 0x%x, read = 0x%x", ADC_DAT_SUBS_ET5XX_ADDR, i, val);
            ret = EGIS_NO_DEVICE;
            goto end;
        }
        egislog_d("register %d, write = 0x%x, read = 0x%x", ADC_DAT_SUBS_ET5XX_ADDR, i, val);
    }

    ret = EGIS_OK;
    ret = io_5xx_dispatch_write_register(ADC_DAT_SUBS_ET5XX_ADDR, temp_val);

    if (ret != EGIS_OK) return ret;

    ret = io_5xx_dispatch_read_register(ADC_DAT_SUBS_ET5XX_ADDR, &val);

    if (ret != EGIS_OK) return ret;
end:

    return ret;
}

int et5xx_get_vdm(unsigned char* buff, int width, int height) {
    int ret = -1;

    sensor_power_control(TRUE);
    io_5xx_dispatch_write_register(VDM_CSR_ET5XX_ADDR,
                                   0x01);  // VDM_READ | VDM_GO
    polling_registry(VDM_CSR_ET5XX_ADDR, 0x80,
                     0x80);                      // mask = 0x80, exp = 0x80 (Wait VDM_CSR, STUS = 1)
    ret = io_5xx_read_vdm(buff, height, width);  // Read 103x52 VDM data
    return ret;
}

int et5xx_set_vdm(unsigned char* buff, int width, int height) {
    int ret = -1;

    sensor_power_control(TRUE);
    io_5xx_dispatch_write_register(VDM_CSR_ET5XX_ADDR,
                                   0x03);  // VDM_READ | VDM_GO
    polling_registry(VDM_CSR_ET5XX_ADDR, 0x80,
                     0x80);  // mask = 0x80, exp = 0x80 (Wait VDM_CSR, STUS = 1)
    ret = io_5xx_write_vdm(buff, height, width);  // Write 103x52 VDM data
    return ret;
}

int isensor_read_nvm(unsigned char* buff) {
    int ret;
    BYTE rxBuf[70];
    ret = io_5xx_read_nvm(rxBuf);
    if (ret != EGIS_OK) return ret;
    memcpy(buff, &rxBuf[3], 64);
    return ret;
}

int isensor_set_navigation_detect_mode(void) {
    int ret;
    ET5XXCalibrationData* cd = &g_sensor_parameter;
    BYTE buf[6];
    ret = isensor_set_detect_mode();
    if (ret != EGIS_OK) return ret;
    buf[0] = cd->detect_thl;  // 0x45 FOD Threshold-Low
    buf[1] = cd->detect_thh;  // 0x44 FOD Threshold-High
    buf[2] = 0xD0;            // 0x43 FOD Cycles - Low Byte //20ms
    buf[3] = 0x07;            // 0x42 FOD Cycles - High Byte
    // buf[2] = 0x50; //0x43 FOD Cycles - Low Byte //0.8ms
    // buf[3] = 0x00; //0x42 FOD Cycles - High Byte
    buf[4] = FOD_MODE0;  // 0x41 FOD Operation Mode
    buf[5] = 0x03;       // 0x40 Enable Moving Average Filter for Digital
                         // FOD|Enable FOD
    ret = io_5xx_dispatch_write_reverse_register(FOD_MEAN_THL_ET5XX_ADDR, 6, buf);
    // io_5xx_dispatch_write_burst_register(0x49, 1 , &value0x49);
    g_fp_mode = FP_NAVI_DETECT_MODE;
    return ret;
}

int isensor_set_navigation_sensor_mode(void) {
    int ret = EGIS_COMMAND_FAIL;
    ret = isensor_set_custom_sensor_mode(SINGLE_SENSING_MODE);
    if (ret != EGIS_OK) return ret;

    io_5xx_dispatch_write_register(SRBA_ADDR,
                                   NAV_START_ROW);  // set navi sensing area
    io_5xx_dispatch_write_register(SREA_ADDR, NAV_END_ROW);
    io_5xx_dispatch_write_register(SCBA_ADDR, NAV_START_COL);
    ret = io_5xx_dispatch_write_register(SCEA_ADDR, NAV_END_COL);

    g_fp_mode = FP_NAVI_SENSOR_MODE;
    return ret;
}

int isensor_set_feature(const char* param, int value) {
    if (param == NULL) {
        return EGIS_INCORRECT_PARAMETER;
    }
    // const char PARAM_BOOST_MODE[] = "boost_mode";
    if (strncmp(param, PARAM_BOOST_MODE, sizeof(PARAM_BOOST_MODE)) == 0) {
        egislog_d("%s - %s (%d)", __func__, PARAM_BOOST_MODE, value);
        fpsSetBoostModeOn(value > 0 ? TRUE : FALSE);
        return EGIS_OK;
    } else if (strncmp(param, PARAM_UPDATE_CONFIG, sizeof(PARAM_UPDATE_CONFIG)) == 0) {
        g_sensing_mode =
            core_config_get_int(INI_SECTION_SENSOR, KEY_SENSING_MODE, DEFAULT_SENSING_MODE);
        g_et5XX_sensor_gain = core_config_get_int(INI_SECTION_SENSOR, KEY_PGA_GAIN, 5);
        g_gain2_enable = core_config_get_int(INI_SECTION_SENSOR, KEY_GAIN2, 0);
        gBoostModeOn = core_config_get_int(INI_SECTION_SENSOR, KEY_BOOST_GAIN, 0);
        g_auto_calibrate_all =
            core_config_get_int(INI_SECTION_SENSOR, KEY_AUTO_CALIBRATE_ALL, g_auto_calibrate_all);

        egislog_d(
            "g_sensing_mode = %d, g_et5XX_sensor_gain = %d, g_gain2_enable = %d, gBoostModeOn = %d",
            g_sensing_mode, g_et5XX_sensor_gain, g_gain2_enable, gBoostModeOn);
        return EGIS_OK;
    } else if (strncmp(param, "sensing_area_width", sizeof("sensing_area_width")) == 0) {
        egislog_i("SENSOR_WIDTH = %d", value);
        SENSOR_WIDTH = value;
        return EGIS_OK;
    } else if (strncmp(param, "sensing_area_height", sizeof("sensing_area_height")) == 0) {
        egislog_i("SENSOR_HEIGHT = %d", value);
        SENSOR_HEIGHT = value;
        return EGIS_OK;
    }

    egislog_e("Failed %s - %s (%d)", __func__, param, value);
    return EGIS_COMMAND_FAIL;
}

int isensor_set_spi_power(const int option) {
    int ret = 0;
    switch (option) {
        case 0:
            ret = io_dispatch_disconnect();
            break;
        case 1:
            ret = io_dispatch_connect(NULL);
            break;
        default:
            break;
    }
    return ret;
}

int isensor_get_sensor_id(unsigned short* id) {
    egislog_d("%s not supported", __func__);
    *id = 0;
    return EGIS_OK;
}

int et5xx_get_auto_calibrate_all_flag() {
    egislog_d("%s: %d", __func__, g_auto_calibrate_all);
    return g_auto_calibrate_all;
}
