typedef enum {
    ET51x_TEST_HALF_VCC = 0x44,
    ET51x_TEST_HALF_LDO = 0x46,
    ET51x_TEST_BANDGAP = 0x47,
} ET51x_TEST_SETTING;

#define RBS_SDK_USE

#include "ET538LDOCalibrationFlow.h"
#include "egis_log.h"
#include "sdk_adapter.h"
#define LOG_TAG "RBS-IC"
BYTE ET51xGetTestPathValue(ET51x_TEST_SETTING Setting);

#define PWR_CTRL1_0A 0x0A
#define PWR_CTRL2_0B 0x0B
#define OSC40M_CSR_0C 0x0C

#define TEST_CTRL0_51 0x51

#define ET51X_MIN_LDO_SETTING 0x01

// 1/2VCC - 1/2LDO difference should less then 40mV for LDO off module
#define ET51X_LDO_ON_OFF_THRESHOLD 4

// LDO setting target voltage set to VCC - 150mV
#define ET51X_LDO_CALIBRATION_DIFF 15

// LDO setting target voltage set to 1/2LDO - Gandgap
#define ET51X_LDO_CALIBRATION_DIFF_BANDGAP 15

#if !defined(RBS_SDK_USE)
void CET505ATestToolDlg::OnBnClickedVldo()
#else
BOOL OnBnClickedVldo()
#endif
{
    BYTE nVCCHalf, nBandgap, nVLDOTestHalf;
    BYTE Reg09Value;
    BYTE diff_VCC, diff_BG;
    BOOL m_bREG28On = FALSE;
    egislog_d("OnBnClickedVldo entry");

    ET505A_REG_WRITE(PWR_CTRL1_0A, 0x20);
    ET505A_REG_WRITE(PWR_CTRL2_0B, 0x00);
    ET505A_REG_WRITE(OSC40M_CSR_0C, 0x44);

    //  1/2*VCC ADC code
    nVCCHalf = ET51xGetTestPathValue(ET51x_TEST_HALF_VCC);

    ET505A_REG_WRITE(PWR_CTRL0_09, ET51X_MIN_LDO_SETTING);

    //  1/2*VLDO ADC code
    nVLDOTestHalf = ET51xGetTestPathValue(ET51x_TEST_HALF_LDO);

    // VLDO test setting, lowest voltage.
    diff_VCC = nVCCHalf - nVLDOTestHalf;

    if (diff_VCC >= ET51X_LDO_ON_OFF_THRESHOLD) {
#if 0  // DEBUG_LDO_REG_VALUE_2
		egislog_i("DEBUG_LDO_REG_VALUE_2");
		Reg09Value = 0x02;
		ET505A_REG_WRITE(PWR_CTRL0_09  , Reg09Value);
#else
        // LDO calibration start with highest setting
        Reg09Value = 0x06;

        while (1) {
            // Set LDO28 voltage
            ET505A_REG_WRITE(PWR_CTRL0_09, Reg09Value);

            //  1/2*VCC ADC code
            nVCCHalf = ET51xGetTestPathValue(ET51x_TEST_HALF_VCC);

            //  1/2*VLDO ADC code
            nVLDOTestHalf = ET51xGetTestPathValue(ET51x_TEST_HALF_LDO);

            //  Band gap ADC code
            nBandgap = ET51xGetTestPathValue(ET51x_TEST_BANDGAP);

            diff_VCC = nVCCHalf - nVLDOTestHalf;

            diff_BG = nVLDOTestHalf - nBandgap;

            if ((Reg09Value <= ET51X_MIN_LDO_SETTING) ||
                ((diff_VCC >= ET51X_LDO_CALIBRATION_DIFF) &&
                 (diff_BG <= ET51X_LDO_CALIBRATION_DIFF_BANDGAP))) {
                break;
            } else {
                Reg09Value--;
            }
        }
#endif
        m_bREG28On = TRUE;
    } else {
        m_bREG28On = FALSE;
    }

    ET505A_REG_WRITE(TEST_CTRL0_51, 0x03);
    return m_bREG28On;
}

#if !defined(RBS_SDK_USE)
BYTE CET505ATestToolDlg::ET51xGetTestPathValue(ET51x_TEST_SETTING Setting)
#else
BYTE ET51xGetTestPathValue(ET51x_TEST_SETTING Setting)
#endif
{
    BYTE nValue;
    ET505A_REG_WRITE(TEST_CTRL0_51, Setting);

    // Count Zone2
    ET505A_REG_WRITE(SCAN_FRAME_2C, 0x00);
    ET505A_REG_WRITE(SCAN_CSR_2D, 0x57);
    ET505A_REG_POLLING(SCAN_CSR_2D, 0x00,
                       0x80);  // Waitting for SCAN_CSR.BUSY=0

    nValue = ET505A_REG_READ(STAT_MEAN_69);

    return nValue;
}
