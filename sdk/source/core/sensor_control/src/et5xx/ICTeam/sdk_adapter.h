#ifndef __SDK_ADAPTER_H__
#define __SDK_ADAPTER_H__

#include "egis_definition.h"
#include "plat_log.h"
#include "plat_spi.h"

#define ET505A_REG_POLLING(addr, expect, mask) polling_registry(addr, expect, mask)
#define ET505A_REG_READ(x) ET_5xx_dispatch_read_register(x, __func__)
#define ET505A_REG_WRITE(x, value) ET_5xx_dispatch_write_register(x, value)

#endif