#ifndef __ET538_LDO_CALIBRATION_FLOW_H__
#define __ET538_LDO_CALIBRATION_FLOW_H__

#include "type_definition.h"
BOOL OnBnClickedVldo();

#define STUS_00 0x00
#define INT_STUS_01 0x01
#define INT_CONF_02 0x02
#define ADC_DAT_SUBS_03 0x03
#define IO_DS_04 0x04
#define OSC40M_CONF_07 0x07
#define OSC100K_CSR_08 0x08
#define PWR_CTRL0_09 0x09
#define PWR_CTRL1_0A 0x0A
#define PWR_CTRL2_0B 0x0B
#define OSC40M_CSR_0C 0x0C

#define VREF_SEL_0D 0x0D
#define DC_P_0E 0x0E
#define DC_C_0F 0x0F
#define DC_F_G_10 0x10
#define DC_F_SEL_11 0x11
#define PGA_GAIN_12 0x12
#define AFE_CONF0_13 0x13
#define AFE_CONF1_14 0x14
#define Z1_COL_BEGIN_20 0x20
#define Z1_COL_END_21 0x21
#define Z1_COL_STEP_22 0x22
#define Z1_ROW_BEGIN_23 0x23
#define Z1_ROW_END_24 0x24
#define Z1_ROW_STEP_25 0x25
#define SCAN_FRAME_2C 0x2C
#define SCAN_CSR_2D 0x2D
#define CALI_DC_P_TM 0x30
#define CALI_DC_C_TM 0x31
#define CALI_VDM_TM_33 0x33
#define CALI_CONF_34 0x34
#define CALI_CSR_35 0x35

#define FOD_CSR_40 0x40
#define FOD_MODE_41 0x41
#define FOD_CTRL 0x41
#define FOD_CYCLES_H_42 0x42
#define FOD_CYCLES_L_43 0x43
#define FOD_TH 0x44
#define FOD_MEAN_THH_44 0x44
#define FOD_MEAN_THL_45 0x45
#define FOD_VALLEY_COL_TH_46 0x46
#define FOD_VALLEY_ROW_TH_47 0x47
#define VDM_CSR_50 0x50
#define TEST_CTRL0_51 0x51
#define TEST_CTRL1_52 0x52
#define FIX_COL_ROW_54 0x54
#define FIX_COLUMN_55 0x55
#define FIX_ROW_56 0x56
#define STAT_MIN_67 0x67
#define STAT_MAX_68 0x68
#define STAT_MEAN_69 0x69
#define FOD_VALLEY_COUNT_6A 0x6A

#define POACF_CSR_80 0x80

// ET600 Register
//		Name			Address		Description
#define STUS 0x00             //	Status Register
#define ACTN_01 0x01          //	Action Register
#define DSM_OFS_SCN_H 0x03    //	Delta-Sensing Offset for Image Scan High Byte
#define DSM_OFS_SCN_L 0x04    //	Delta-Sensing Offset for Image Scan Low Byte
#define DSM_OFS_VDM_H 0x05    //	Delta-Sensing Offset for VDM High Byte
#define DSM_OFS_VDM_L 0x06    //	Delta-Sensing Offset for VDM Low Byte
#define DPTH_07 0x07          //	Data Path Register
#define INT_CTRL_08 0x08      //	Interrupt Control Register
#define INT_STUS_CLR_09 0x09  //	Interrupt Status Register
#define TEST_CTRL 0x0a        //	Test Control Register
#define OSC100K_CSR_0B 0x0b   //	100KHz OSC/Clock Control & Status Register
//#define	OSC40M_CSR		0x0c	//	40MHz OSC/Clock Control &
// Status Register
//#define OSC40M_CSR_0C	0x0C
#define OSC40M_CONF 0x0d       //	OSC 40MHz Frequency Configuration Register
#define VREF_CTRL0 0x0f        //	VREF Control Register 0
#define CPDR_CSR 0x10          //	CPDR Control & Status Register
#define LDO_CTRL1 0x11         //	LDO Control & Status Register 1
#define LDO_CTRL2_12 0x12      //	LDO Control & Status Register 2
#define VREF_CTRL1_13 0x13     //	VREF Control Register 1
#define ARY_PWR_14 0x14        //	Array Power Control Register
#define ARY_ISEL_15 0x15       //	Array I-Select Register
#define ARY_FPS 0x16           //	Array FPS Select Register
#define ARY_IBUF 0x17          //	Array IBuf Configuration Register
#define ADC_GAIN_18 0x18       //	ADC Integrated Gain Register
#define ADC_CTRL0 0x19         //	ADC Control Register 0
#define ADC_CTRL1 0x1a         //	ADC Control Register 1
#define ADC_CTRL2_1B 0x1b      //	ADC Control Register 2
#define ANAOUT 0x1c            //	ANAOUT Control Register
#define ADC_SW_FPS_1E 0x1e     //	ADC FPS Switch control
#define PWR_CTRL0_20 0x20      //	Power Control Register 0
#define PWR_CTRL1_21 0x21      //	Power Control Register 1
#define PWR_CTRL2_22 0x22      //	Power Control Register 2
#define PWR_CTRL3_23 0x23      //	Power Control Register 3
#define PWR_STUS0 0x24         //	Power Control Status Register 0
#define PWR_STUS1_25 0x25      //	Power Control Status Register 1
#define PWR_STUS2 0x26         //	Power Control Status Register 2
#define SSM_OFS_SCN_H_2A 0x2a  //	Single-Sensing Offset for Image Scan MSB
#define SSM_OFS_SCN_L_2B \
    0x2b                           //	Single -Sensing Offset for Image Scan
                                   // LSB
#define CALI_CSR0_30 0x30          //	Calibration Control & Status Register-0
#define VBT_OFS_CALI_31 0x31       //	Vbt for ADC calibration
#define VBT_OFS_FW_32 0x32         //	Vbt Offset F.W. Register
#define VBT_OFS_MRGN_33 0x33       //	VBT offset Margin Register
#define CALI_CSR1_34 0x34          //	Calibration Control & Status Register-1
#define VBT_CALI_RSLT_36 0x36      //	Vbt Calibration Result
#define VDM_GAIN_37 0x37           //	VDM Integration Gain
#define VDM_SCL_38 0x38            //	VDM scaling factor
#define STAT_MEAN_H_3A 0x3a        //	Mean of Statistic Data, High Byte
#define STAT_MEAN_L_3B 0x3b        //	Mean of Statistic Data, Low Byte
#define STAT_MAX_H_3C 0x3c         //	Maximum of Statistic Data, High Byte
#define STAT_MAX_L_3D 0x3d         //	Maximum of Statistic Data, Low Byte
#define STAT_MIN_H_3E 0x3e         //	Minimum of Statistic Data, High Byte
#define STAT_MIN_L_3F 0x3f         //	Minimum of Statistic Data, Low Byte
#define FOD_CSR 0x40               //	Finger-On Detection Control & Status Register
#define FOD_CYCLE_H_41 0x41        //	FOD Period, High Byte
#define FOD_CYCLE_L_42 0x42        //	FOD Period, Low Byte
#define FOD_MEAN_THH_LS_H_43 0x43  //	FOD Mean Threshold High, Left Side, High Byte
#define FOD_MEAN_THH_LS_L_44 0x44  //	FOD Mean Threshold High, Left Side, Low Byte
#define FOD_MEAN_THL_LS_H_45 0x45  //	FOD Mean Threshold Low, Left Side, High Byte
#define FOD_MEAN_THL_LS_L_46 0x46  //	FOD Mean Threshold Low, Left Side, Low Byte
#define FOD_MEAN_THH_RS_H_47 0x47  //	FOD Mean Threshold High, Right Side, High Byte
#define FOD_MEAN_THH_RS_L_48 0x48  //	FOD Mean Threshold High, Right Side, Low Byte
#define FOD_MEAN_THL_RS_H_49 0x49  //	FOD Mean Threshold Low, Right Side, High Byte
#define FOD_MEAN_THL_RS_L_4A 0x4a  //	FOD Mean Threshold Low, Right Side, Low Byte
#define SGS_CSR_50 0x50            //	Signal Scaling Control & Status Register
#define SGS_SUB_H_51 0x51          //	Signal Scaling Subtract Value, High Byte
#define SGS_SUB_L_52 0x52          //	Signal Scaling Subtract Value, Low Byte
#define SGS_DIV_53 0x53            //	Signal Scaling Divider Value
#define SGS_OFS_54 0x54            //	Signal Scaling Offset Value
#define SGS_DIV_STUS 0x55          //	Signal Scaling Divider Status Value
#define SGS_DIV_BW_THD 0x56        //	Signal Scaling Divider Backward Threshold
#define SGS_DIV_TUNE 0x57          //	Signal Scaling Divider Fine Tune
#define SGS_MD2_OFS 0x58           //	Signal Scaling Offset Value for Mode 2
#define GPR0 0xF0                  //	General Purpose Register - 0
#define GPR1 0xF1                  //	General Purpose Register - 1
#define GPR2 0xF2                  //	General Purpose Register - 2
#define GPR3 0xF3                  //	General Purpose Register - 3
#define GPO 0xF4                   //	General Purpose Output
#define GPO_OE 0xF5                //	General Purpose Output, Output Enable
#define DARK_FILT 0xFC             //
#define REV_ID 0xFD                //	Revision ID (ver.A=0x00, ver.B=0x01)
#define DEV_ID1 0xFE               //	Device ID ¡V 1 (ETx00)
#define DEV_ID0 0xFF               //	Device ID ¡V 0 (ET6xx)

#define FOD_GAIN_43 0x43           //	FOD Mean Threshold High, Left Side, High Byte
#define FOD_MEAN_THH_LS_H_44 0x44  //	FOD Mean Threshold High, Left Side, High Byte
#define FOD_MEAN_THH_LS_M_45 0x45  //	FOD Mean Threshold High, Left Side, Low Byte
#define FOD_MEAN_THH_LS_L_46 0x46  //	FOD Mean Threshold High, Left Side, Low Byte
#define FOD_MEAN_THL_LS_H_47 0x47  //	FOD Mean Threshold Low, Left Side, High Byte
#define FOD_MEAN_THL_LS_M_48 0x48  //	FOD Mean Threshold Low, Left Side, Low Byte
#define FOD_MEAN_THL_LS_L_49 0x49  //	FOD Mean Threshold Low, Left Side, Low Byte
#define FOD_MEAN_THH_RS_H_4A 0x4A  //	FOD Mean Threshold High, Right Side, High Byte
#define FOD_MEAN_THH_RS_M_4B 0x4B  //	FOD Mean Threshold High, Right Side, High Byte
#define FOD_MEAN_THH_RS_L_4C 0x4C  //	FOD Mean Threshold High, Right Side, Low Byte
#define FOD_MEAN_THL_RS_H_4D 0x4D  //	FOD Mean Threshold Low, Right Side, High Byte
#define FOD_MEAN_THL_RS_M_4E 0x4E  //	FOD Mean Threshold Low, Right Side, High Byte
#define FOD_MEAN_THL_RS_L_4F 0x4F  //	FOD Mean Threshold Low, Right Side, Low Byte

#define FOD_SUM_LS_H 0x68
#define FOD_SUM_LS_M 0x69
#define FOD_SUM_LS_L 0x6A

#define FOD_SUM_RS_H 0x6C  // High, Middle upside down
#define FOD_SUM_RS_M 0x6B
#define FOD_SUM_RS_L 0x6D

// ET601 Register
//		Name			Address		Description
#define CLK40M_CSR_0D 0x0d  //	40MHz Clock Control & Status Register

#define ZN2_CSR 0x80
#define NAV_CYCLE_H_81 0x81
#define NAV_CYCLE_L_82 0x82
#define NAV_GAIN_83 0x83

#define NAV_ZN00_THH_H_90 0x90
#define NAV_ZN00_THH_L_91 0x91
#define NAV_ZN01_THH_H_92 0x92
#define NAV_ZN01_THH_L_93 0x93
#define NAV_ZN02_THH_H_94 0x94
#define NAV_ZN02_THH_L_95 0x95
#define NAV_ZN03_THH_H_96 0x96
#define NAV_ZN03_THH_L_97 0x97
#define NAV_ZN04_THH_H_98 0x98
#define NAV_ZN04_THH_L_99 0x99
#define NAV_ZN05_THH_H_9A 0x9A
#define NAV_ZN05_THH_L_9B 0x9B
#define NAV_ZN06_THH_H_9C 0x9C
#define NAV_ZN06_THH_L_9D 0x9D
#define NAV_ZN07_THH_H_9E 0x9E
#define NAV_ZN07_THH_L_9F 0x9F
#define NAV_ZN08_THH_H_A0 0xA0
#define NAV_ZN08_THH_L_A1 0xA1
#define NAV_ZN09_THH_H_A2 0xA2
#define NAV_ZN09_THH_L_A3 0xA3
#define NAV_ZN0A_THH_H_A4 0xA4
#define NAV_ZN0A_THH_L_A5 0xA5
#define NAV_ZN0B_THH_H_A6 0xA6
#define NAV_ZN0B_THH_L_A7 0xA7
#define NAV_ZN0C_THH_H_A8 0xA8
#define NAV_ZN0C_THH_L_A9 0xA9
#define NAV_ZN0D_THH_H_AA 0xAA
#define NAV_ZN0D_THH_L_AB 0xAB
#define NAV_ZN0E_THH_H_AC 0xAC
#define NAV_ZN0E_THH_L_AD 0xAD
#define NAV_ZN0F_THH_H_AE 0xAE
#define NAV_ZN0F_THH_L_AF 0xAF

#endif