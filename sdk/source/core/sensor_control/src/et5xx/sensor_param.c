#include "sensor_param.h"
#include "egis_log.h"
#include "egis_sensor_parameter.h"

#define LOG_TAG "RBS-EGISFP"

static SensorDetectParam gNoramlDetectParam = {.isValid = FALSE};
void BackupDetectParam(SensorParameter* pSensorParameter, BOOL forceBackup) {
    if (gNoramlDetectParam.isValid && !forceBackup) return;

    egislog_i("%s", __func__);
    gNoramlDetectParam.detect_gain = pSensorParameter->detect_gain;
    gNoramlDetectParam.detect_vref_sel = pSensorParameter->detect_vref_sel;
    gNoramlDetectParam.detect_dc_p = pSensorParameter->detect_dc_p;
    gNoramlDetectParam.detect_dc_c = pSensorParameter->detect_dc_c;
#ifdef FOD_CALIBRATION_DCFG
    gNoramlDetectParam.detect_dc_f_g = pSensorParameter->detect_dc_f_g;
#endif
    gNoramlDetectParam.detect_mean = pSensorParameter->detect_mean;
    gNoramlDetectParam.detect_thh = pSensorParameter->detect_thh;
    gNoramlDetectParam.detect_thl = pSensorParameter->detect_thl;

    gNoramlDetectParam.isValid = TRUE;
    return;
}
int RestoreDetectParam(SensorParameter* pSensorParameter) {
    egislog_i("%s", __func__);
    if (!gNoramlDetectParam.isValid) {
        egislog_e("gNoramlDetectParam is not valid.");
        return -1;
    }
    pSensorParameter->detect_gain = gNoramlDetectParam.detect_gain;
    pSensorParameter->detect_vref_sel = gNoramlDetectParam.detect_vref_sel;
    pSensorParameter->detect_dc_p = gNoramlDetectParam.detect_dc_p;
    pSensorParameter->detect_dc_c = gNoramlDetectParam.detect_dc_c;
#ifdef FOD_CALIBRATION_DCFG
    pSensorParameter->detect_dc_f_g = gNoramlDetectParam.detect_dc_f_g;
#endif
    pSensorParameter->detect_mean = gNoramlDetectParam.detect_mean;
    pSensorParameter->detect_thh = gNoramlDetectParam.detect_thh;
    pSensorParameter->detect_thl = gNoramlDetectParam.detect_thl;

    return EGIS_OK;
}

int isensor_get_int(isensor_param_type_t param_type, int* value) {
    egislog_d("%s, param_type = %d", __func__, param_type);
    switch (param_type) {
        case PARAM_INT_RAW_IMAGE_BPP:
            *value = 8;
            break;
        default:
            egislog_e("%s, not supported type = %d", __func__, param_type);
            break;
    }
    return EGIS_OK;
}

int isensor_set_int(isensor_param_type_t param_type, int value) {
    egislog_e("%s, not implemented. return OK directly", __func__);
    return EGIS_OK;
}

int isensor_get_buffer(isensor_param_type_t param_type, unsigned char* out_buffer,
                       int* out_buffer_size) {
    egislog_e("%s, not implemented. return OK directly", __func__);
    return EGIS_OK;
}

int isensor_set_buffer(isensor_param_type_t param_type, unsigned char* in_buffer,
                       int in_buffer_size) {
    egislog_e("%s, not implemented. return OK directly", __func__);
    return EGIS_OK;
}
