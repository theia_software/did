#pragma once
#include "egis_definition.h"
#include "egis_fp_calibration.h"
#include "egis_fp_get_image.h"
#include "fpsensor_definition.h"
#include "type_definition.h"

typedef enum {
    CALIBRATION_NONE = 0,
    CALIBRATION_DVR_FINGERON_LEVEL,
    CALIBRATION_INTERRUPT_LEVEL,
    CALIBRATION_ALL_LEVEL
} CALIBRATION_LEVEL;

typedef struct _SensorDetectParameter {
    BOOL isValid;
    BYTE detect_gain;
    BYTE detect_vref_sel;
    BYTE detect_dc_p;
    BYTE detect_dc_c;
#ifdef FOD_CALIBRATION_DCFG
    BYTE detect_dc_f_g;
#endif
    BYTE detect_mean;
    BYTE detect_thh;
    BYTE detect_thl;

    BYTE realtime_dc_c;

    BYTE detect_threshold;
} SensorDetectParam;

#define NO_FINGER_ADJUST_DC_OFFSET_MAX 3
#define MAX_REDUCE_DETECT_DC_OFFSET 5

// int fp_check_and_recovery(void);
