#include "fingerprint_library.h"
#include "common_definition.h"
#include "egis_fp_common_5XX.h"
#include "egis_log.h"
#include "egis_sensor_parameter.h"
#include "isensor_api.h"
#include "plat_heap.h"
#include "plat_spi.h"
#ifdef EGIS_DBG
#ifdef TEEIGP  // fisher
#include <external/fdlibm/include/fdlibm.h>
#else
#include <math.h>
#endif
#include <time.h>
#endif

#define LOG_TAG "RBS-EGISFP"

extern BOOL g_is_finger_detect;
extern int g_reduce_no_finger_count;
extern SensorParameter g_sensor_parameter;
void egis_device_reset();

#if EGIS_DBG
extern long g_open_sensor_start_time;
#endif

int isensor_close(void) {
    int ret;
    egislog_d("isensor_close enter");
    fp_dump_data();
    ret = io_dispatch_disconnect();
    return ret;
}

int isensor_open(int* timeout) {
    int ret = 0;

    egislog_i("isensor_open(), g_sensor_parameter.calibration_flag = %d",
              g_sensor_parameter.calibration_flag);

#ifdef HW_HAWAII_Y6
    // DO HW RESET, MAKE SURE SENSOR STATE
    egis_device_reset();
    egislog_d("do HW reset");
#endif

    ret = fp_tz_secure_sensor_init();
    if (ret != EGIS_OK) return ret;  // Should not run to this line if sensor OK.

    int RV = fpsGetNVMRV();
    if (!fpsGetBoostMode() && RV > 0) {
        if (RV <= RV_LOW_THRESHOLD) {
            egislog_d("%s, RV %d is low. (<= %d)", __func__, RV, RV_LOW_THRESHOLD);
            fpsSetBoostModeOn(TRUE);
        }
    }

    ret = isensor_calibrate(FPS_CALI_INIT);

    egislog_i("isensor_open() ret = %d", ret);

    return ret;
}

int fp_check_and_recovery(void) {
    return check_and_recovery(g_fp_mode);
}