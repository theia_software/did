#include "egis_fp_calibration.h"
#include "egis_fp_common_5XX.h"
#include "egis_log.h"
#include "egis_sensor_parameter.h"
#include "isensor_api.h"
#include "plat_spi.h"
#include "sensor_param.h"

#define LOG_TAG "RBS-FP-CALI"

static int isensor_check_cali_flag(void);
extern SensorParameter g_sensor_parameter;

static int _do_calibrate_all() {
    int ret = EGIS_OK;
    isensor_check_cali_flag();
    if (g_sensor_parameter.calibration_flag & DO_CALIBRATION_DVR) {
        ret = calibrate_vdm_mode();
    }
    if (g_sensor_parameter.calibration_flag & DO_CALIBRATION_INTERRUPT) {
        ret = calibrate_detect_mode(FALSE);
    }
    g_sensor_parameter.calibration_flag = DO_CALIBRATION_COMPLETE;
    return ret;
}

int isensor_calibrate(FPS_CALIBRATE_OPTION option) {
    egislog_v("isensor_calibrate:%d, sensor_is_long:%d", option, g_sensor_parameter.sensor_is_long);
    int ret = EGIS_COMMAND_FAIL;
    BOOL prevent_false_trigger = FALSE;

#if defined(_WINDOWS) || defined(__DEVICE_DRIVER_MEIZU_1712__)
    if (option != FPS_CALI_INIT) {
        egislog_d("skip calibration %d", option);
        return EGIS_COMMAND_FAIL;
    }
#endif

    if (et5xx_get_auto_calibrate_all_flag() == TRUE) {
        if (option == FPS_CALI_SDK_THL) {
            option = FPS_CALI_INIT;
            egislog_d("Do all calibration for THL");
        }
        prevent_false_trigger = g_sensor_parameter.sensor_is_long;
    }

    switch (option) {
        case FPS_CALI_SDK_THH_FALSE_TRIGGER:
            if (prevent_false_trigger) {
                ret = _do_calibrate_all();
            } else {
                isensor_set_sensor_mode();
                ret = calibrate_detect_mode(TRUE);
            }
            break;
        case FPS_CALI_SDK_THL:
            isensor_set_sensor_mode();
            ret = calibrate_detect_mode(FALSE);
            break;
        case FPS_CALI_SDK_CHECK_FINGER_REMOVE:
            if (GetNeedReKVDM()) {
                egislog_d("Continue to do FPS_CALI_INIT");
            } else {
                if (prevent_false_trigger) {
                    ret = _do_calibrate_all();
                } else {
                    isensor_set_sensor_mode();
                    ret = calibrate_detect_mode(TRUE);
                }
                break;
            }
        case FPS_CALI_INIT:
            ret = _do_calibrate_all();
            update_best_available_vdm_in_buffer();
            break;
        default:
            break;
    }
    egislog_d("isensor_calibrate ret=%d", ret);
    return ret;
}

int isensor_get_bad_pixels(BYTE** bad_pixel, int* bad_pixel_count) {
    if (bad_pixel) {
        *bad_pixel = g_sensor_parameter.bad_pixel;
    }
    if (bad_pixel_count) {
        *bad_pixel_count = g_sensor_parameter.bad_pixel_count;
    }
    return EGIS_OK;
}

static int isensor_check_cali_flag(void) {
#ifdef POLLING
    g_sensor_parameter.calibration_flag = DO_CALIBRATION_DVR;
#else
    g_sensor_parameter.calibration_flag = DO_CALIBRATION_DVR | DO_CALIBRATION_INTERRUPT;
#endif
    return EGIS_OK;
}
