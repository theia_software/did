#ifndef __EGIS_FP_COMMON_5XX__H_
#define __EGIS_FP_COMMON_5XX__H_

#include "isensor_api.h"
#include "type_definition.h"

int et5xx_get_auto_calibrate_all_flag();
int et5xx_get_vdm_target_mean(BYTE* mean, BOOL fix);
int et5xx_get_vdm(unsigned char* buff, int width, int height);
int et5xx_set_vdm(unsigned char* buff, int width, int height);

int calibrate_detect_mode(BOOL toWaitForTHL);
int et5xx_calibrate_sensor_mode(void);

void fp_dump_data(void);

int fp_tz_secure_sensor_init(void);

int check_and_recovery(FP_MODE fp_mode);

/*
 *	specific 5xx adjust functions.
 */
BOOL sensor_is_LDO_enable();

void fpsSetBoostModeOn(BOOL on);
BOOL fpsGetBoostMode();
int fps_get_scan_z2_info(void);
int CrossFix(BYTE* img, int w, int h, BOOL revert);
#define NVM_INDEX_HW_DC_C 0x1D
#define NVM_INDEX_HW_TH 0x20
#define NVM_INDEX_RV 0x21
#define NVM_INDEX_CHECKSUM_HB 0x31
#define NVM_INDEX_CHECKSUM_LB 0x32
#define NVM_MEAN_MAX_INIT_FAIL -1
#define MEAN_PER_DCC 70  // Trustonic_516, Lenovo_516, Normal_516
int fpsGetNVMRV();

#define RV_LOW_THRESHOLD 20

#endif
