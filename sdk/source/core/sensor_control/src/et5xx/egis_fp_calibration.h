#pragma once
#include "calibration_state.h"
#include "egis_definition.h"
#include "isensor_api.h"
#include "type_definition.h"

typedef struct {
    BOOL vdm_is_good;
    BYTE vdm[MAX_IMG_SIZE];
    int bad_pixel_count;
    BYTE bad_pixel[MAX_IMG_SIZE];
} sensor_bkg_data_t;

#define MAX_DETECT_THRESHOLD 0x3F              // Maximum of hardware detect threshold
#define MIN_DETECT_THRESHOLD 0x0               // Minimum of hardware detect threshold
#define MAX_DC_FOR_INTERRUPT 0x3F              // Start dc offset of interrupt calibration
#define WAIT_INTERRUPT_TIMEOUT_INFINITE -1     //	Wait inturrupt timeout
#define WAIT_INTERRUPT_TIMEOUT_CALIBRATION 10  //	Wait inturrupt timeout for calibration
#define DC_OFFSET_SEARCH_INT 10                // Interrupt calibration settings
#define CALIBRATION_FIRST_AND_LAST_ROW_THRESHOLD 240
#define CALIBRATION_MIDDLE_ROW_THRESHOLD 10
#define CALIBRATION_FINGER_DETECT_THRESHOLD 40

#define BKG_AVERAGE_THESHOLD 5              // Verify cablibration data
#define BKG_STANDARD_DEVIATION_THESHOLD 10  // Verify cablibration data
#define MAX_RECALIBRATION_FAIL 1            // Max retry calibration count

void SetNeedReKVDM();
BOOL GetNeedReKVDM();

int fp_tz_secure_pre_calibrate(void);
int has_interrupt(void);
int confirm_finger_on_and_retry(BYTE* image);
int skip_calibration_vdm_mode(void);
int sum_pixel(BYTE* img, BYTE* bad, int area_size, int* pixel_count);
unsigned int count_variance_2e10(unsigned char** frames, int count, int area, BYTE* avg_frame);
int calibrate_vdm_mode(void);
void update_best_available_vdm_in_buffer(void);
void set_best_available_vdm_to_ic(void);

void CalibrateDvrFingerTouch();
