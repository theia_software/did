#pragma once

//
//	Calibration status
//
#define DO_CALIBRATION_COMPLETE 0
#define DO_CALIBRATION_DVR 1
#define DO_CALIBRATION_INTERRUPT 2
