#ifndef _WINDOWS
#ifndef TZ_MODE
#include <unistd.h>
#endif
#endif
#include "ImageProcessingLib.h"
#include "egis_fp_calibration.h"
#include "egis_fp_common_5XX.h"
#include "egis_log.h"
#include "egis_sensor_parameter.h"
#include "fpsensor_definition.h"
#include "image_analysis.h"
#include "isensor_api.h"
#include "isensor_definition.h"
#include "plat_heap.h"
#include "plat_spi.h"
#include "qm_lib.h"

#define LOG_TAG "RBS-EGISFP"
#define ACCEPTABLE_BAD_PIXEL_COUNT (SENSOR_WIDTH * SENSOR_HEIGHT / 50)
#define UPDATE_CNT_MAX 5

//
//	The best bkg sensor settings
//
sensor_bkg_data_t g_sensor_bkg_data = {
    .vdm_is_good = FALSE,
};
//
//	Capture image status for TrustZone
//
#ifndef _WINDOWS
SensorParameter g_sensor_parameter = {

    .sensor_gain = MAX_SENSOR_GAIN,
#ifdef POLLING
    .calibration_flag = DO_CALIBRATION_DVR,
#else
    .calibration_flag = DO_CALIBRATION_DVR | DO_CALIBRATION_INTERRUPT,
#endif
};
#else
SensorParameter g_sensor_parameter;
#endif
//
//	Debug
//

//
//  Sensor Static parameters
//
int g_sensing_mode = DEFAULT_SENSING_MODE;

//
//	Interrupt calibration temp parameters
//
unsigned char g_detect_thr_array[DC_OFFSET_SEARCH_INT + 1];
int g_int_thr_array_index;
BYTE g_int_thr_high;
BYTE g_int_thr_low;
BYTE g_int_calibration_dc;
int g_int_dir;
int g_int_calibration_dc_begin;

static int g_update_best_cnt = 0;
static int g_mean_max = 0;
static BOOL g_is_need_update_vdm = FALSE;
extern int gNVM_MEAN_FROM_DCC_MAX;

static BOOL is_better_than_best();
static BOOL vdm_is_clean_qty(BYTE* vdm_img, int bad_pixel_count);
static BOOL vdm_is_clean_qm(BYTE* vdm_img, int bad_pixel_count);
static void revert_bad_pixel_from_best(void);
static double get_orignal_mean(BYTE a_Gain, BYTE a_Gain2, BYTE a_DC_P, BYTE a_DC_C, BYTE a_DC_F_G,
                               BYTE a_CurrMean);
//
// for finger on invoke
//

int et5xx_calibrate_bad_pixel(int sensorWidth, int sensorHeight, ET5XXCalibrationData* cd,
                              int* bad_count);

int g_vdm_cnt = 0;
BOOL gNeedReKVDM = FALSE;

void SetNeedReKVDM() {
    egislog_i("%s, want to reK", __func__);
    gNeedReKVDM = TRUE;
}
BOOL GetNeedReKVDM() {
    return gNeedReKVDM;
}
static int vdm_finger_check_get(BYTE* vdm_out) {
    int ret = EGIS_OK;

    BYTE* vdm_poacf = plat_alloc(SENSOR_FULL_WIDTH * SENSOR_HEIGHT);
    ret = et5xx_get_vdm(vdm_poacf, SENSOR_FULL_WIDTH, SENSOR_HEIGHT);
    if (ret != EGIS_OK) goto exit;
    if (vdm_out != NULL) memcpy(vdm_out, vdm_poacf, SENSOR_FULL_WIDTH * SENSOR_HEIGHT);
#ifdef __ET538__
    // 516 size to 538 size
    memset(vdm_poacf, 0, SENSOR_FULL_WIDTH * SENSOR_HEIGHT);
    int i = 0, j = 0, k = 0;
    for (i = 0; i < SENSOR_HEIGHT; i++) {
        for (j = 0; j < SENSOR_FULL_WIDTH; j++) {
            if (j == 0 || j >= (SENSOR_FULL_WIDTH - 2)) continue;
            vdm_poacf[k] = vdm_out[i * SENSOR_FULL_WIDTH + j];
            k++;
        }
    }
#endif

exit:
    plat_free(vdm_poacf);
    return ret;
}

int skip_calibration_vdm_mode(void) {
    egislog_e("che removecode1");
    int ret = 0;

    return ret;
}
int calibrate_vdm_mode(void) {
    int ret = EGIS_OK;
    unsigned char* vdm_temp = NULL;
    egislog_i("++++ calibrate_vdm_mode ++++");
    g_is_need_update_vdm = TRUE;
    ret = fp_tz_secure_pre_calibrate();
    if (ret != EGIS_OK) return ret;

    vdm_temp = plat_alloc(SENSOR_FULL_WIDTH * SENSOR_HEIGHT);
    if (vdm_temp == NULL) {
        egislog_e("vdm_temp memory allocate failed");
        return EGIS_CALIBRATION_DVR_FAIL;
    }
    vdm_finger_check_get(vdm_temp);
    memcpy(g_sensor_parameter.vdm_bk, vdm_temp, SENSOR_FULL_WIDTH * SENSOR_HEIGHT);
    et5xx_get_vdm_target_mean(&g_sensor_parameter.vdm_hw_mean, FALSE);
    egislog_i("vdm hw mean = %d", g_sensor_parameter.vdm_hw_mean);

    ret = et5xx_calibrate_sensor_mode();
    if (ret != EGIS_OK) goto exit;
    isensor_set_sensor_mode();
    isensor_set_Z1_area(TRUE);
    ret = et5xx_calibrate_bad_pixel(SENSOR_FULL_WIDTH, SENSOR_HEIGHT, &g_sensor_parameter,
                                    &g_sensor_parameter.bad_pixel_count);
    isensor_set_Z1_area(FALSE);
    egislog_d("%s, bad_pixel_count = %d\n", __func__, g_sensor_parameter.bad_pixel_count);
exit:
    plat_free(vdm_temp);
    return ret;
}

void set_best_available_vdm_to_ic(void) {
    if (g_is_need_update_vdm && g_sensor_bkg_data.vdm_is_good) {
        egislog_d("%s", __func__);
        g_is_need_update_vdm = FALSE;
        et5xx_set_vdm(g_sensor_bkg_data.vdm, SENSOR_FULL_WIDTH, SENSOR_HEIGHT);
        revert_bad_pixel_from_best();
        isensor_set_sensor_mode();
    }
}

void update_best_available_vdm_in_buffer(void) {
    ET5XXCalibrationData* cd = &g_sensor_parameter;

#ifdef FOD_CALIBRATION_DCFG
    BYTE gain2_en = 1;  // 0x13 AFE_CONF0 GAIN2_EN, SINGLE_MODE
    double original_hw_mean = get_orignal_mean(cd->detect_gain, gain2_en, cd->detect_dc_p,
                                               cd->detect_dc_c, cd->detect_dc_f_g, cd->detect_mean);
    egislog_d("original_hw_mean:%.2f", original_hw_mean);
#endif

    if (is_better_than_best() && g_update_best_cnt < UPDATE_CNT_MAX) {
        memcpy(g_sensor_bkg_data.vdm, cd->vdm_bk, SENSOR_FULL_WIDTH * SENSOR_HEIGHT);
        memcpy(g_sensor_bkg_data.bad_pixel, cd->bad_pixel, sizeof(cd->bad_pixel));
        g_sensor_bkg_data.bad_pixel_count = cd->bad_pixel_count;
        g_sensor_bkg_data.vdm_is_good = TRUE;
        g_update_best_cnt++;
        egislog_d("update_best_available_vdm, update_cnt:%d, max_cnt:%d", g_update_best_cnt,
                  UPDATE_CNT_MAX);
    }

    // TODO: workaround to turn power ON
    et5xx_get_vdm_target_mean(NULL, FALSE);
}

static BOOL is_better_than_best() {
    ET5XXCalibrationData* cd = &g_sensor_parameter;
    int mean_from_dcc;
    BOOL is_better = FALSE;

    if (gNVM_MEAN_FROM_DCC_MAX > 0) {
        mean_from_dcc = (cd->detect_dc_c * MEAN_PER_DCC) - cd->detect_mean;
        egislog_d("gNVM_MEAN_FROM_DCC_MAX:%d, mean_from_dcc:%d, g_mean_max:%d",
                  gNVM_MEAN_FROM_DCC_MAX, mean_from_dcc, g_mean_max);

        if (mean_from_dcc > g_mean_max) {
#ifdef USE_QM_FOR_FINGER_DETECT
            is_better = vdm_is_clean_qm(cd->vdm_bk, cd->bad_pixel_count);
#else
            is_better = vdm_is_clean_qty(cd->vdm_bk, cd->bad_pixel_count);
#endif
            if (is_better) g_mean_max = mean_from_dcc;
        }
    } else
        egislog_d("NVM_MEAN_FROM_DCC function disable");

    egislog_d("%s, is_better:%d", __func__, is_better);
    return is_better;
}

static BOOL vdm_is_clean_qty(BYTE* vdm_img, int bad_pixel_count) {
    ET5XXCalibrationData* cd = &g_sensor_parameter;
    int corner_count, cover_count, percentage, img_intensity, img_qty;
    static int best_vdm_qty = 10, best_vdm_percentage = 10;
    BOOL is_clean = FALSE;

    img_qty = resample_IsFPImage_Lite(vdm_img, SENSOR_FULL_WIDTH, SENSOR_HEIGHT, &corner_count,
                                      &cover_count, FALSE, &img_intensity);
    IPnavi_is_fp_image_percentage(vdm_img, SENSOR_FULL_WIDTH, SENSOR_HEIGHT, &percentage);

    egislog_d("%s, qty:%d,corner:%d,cover:%d,percentage:%d", __func__, img_qty, corner_count,
              cover_count, percentage);
    egislog_d("%s, bad_pixel_count:%d, acceptable_count:%d", __func__, bad_pixel_count,
              ACCEPTABLE_BAD_PIXEL_COUNT);

    if ((img_qty > CALIBRATION_FINGER_DETECT_THRESHOLD) || (percentage > 10))
        egislog_d("%s, result_is_finger", __func__);
    else if (img_qty <= best_vdm_qty && percentage <= best_vdm_percentage &&
             bad_pixel_count < ACCEPTABLE_BAD_PIXEL_COUNT) {
        egislog_d("%s, result_non_finger", __func__);
        best_vdm_qty = img_qty;
        best_vdm_percentage = percentage;
        is_clean = TRUE;
    }

    return is_clean;
}

static BOOL vdm_is_clean_qm(BYTE* vdm_img, int bad_pixel_count) {
    QMFeatures qm_feat;
    QMOption* opt = NULL;
    static int best_vdm_qty = 20, best_vdm_percentage = 20;
    int qm_min_dr_value;
    BOOL is_clean = FALSE;

    opt = qm_alloc_option();
    if (opt == NULL) {
        return FALSE;
    }

    if (SENSOR_FULL_WIDTH <= 57)
        qm_min_dr_value = 5;
    else
        qm_min_dr_value = 10;

    opt->count_percentage = TRUE;
    opt->min_dr_value = qm_min_dr_value;
    opt->return_qty = TRUE;

    egislog_d("%s, w:%d, h:%d, min_dr_value:%d", __func__, SENSOR_FULL_WIDTH, SENSOR_HEIGHT,
              opt->min_dr_value);
    qm_extract(vdm_img, SENSOR_FULL_WIDTH, SENSOR_HEIGHT, &qm_feat, opt);
    egislog_d("%s, qty %d diff %d,feat %d, percentage %d", __func__, qm_feat.qty, qm_feat.diff,
              qm_feat.size, qm_feat.percentage);

    if (qm_feat.qty > CALIBRATION_FINGER_DETECT_THRESHOLD || qm_feat.percentage > 10)
        egislog_d("%s, result_is_finger", __func__);
    else if (qm_feat.qty <= best_vdm_qty && qm_feat.percentage <= best_vdm_percentage &&
             bad_pixel_count < ACCEPTABLE_BAD_PIXEL_COUNT) {
        egislog_d("%s, result_non_finger", __func__);
        best_vdm_qty = qm_feat.qty;
        best_vdm_percentage = qm_feat.percentage;
        is_clean = TRUE;
    }

    if (opt) {
        qm_free_option(opt);
        opt = NULL;
    }

    return is_clean;
}

static void revert_bad_pixel_from_best() {
    ET5XXCalibrationData* cd = &g_sensor_parameter;
    memcpy(cd->bad_pixel, g_sensor_bkg_data.bad_pixel, sizeof(g_sensor_bkg_data.bad_pixel));
    cd->bad_pixel_count = g_sensor_bkg_data.bad_pixel_count;
    egislog_d("%s, __bad_pixel_count = %d", __func__, cd->bad_pixel_count);
}

extern int et5xx_has_interrupt(BOOL clean);
int has_interrupt(void) {
#define INTERRUPT_DELAY_TIME 10 * 1000 * 1000

#ifdef TZ_MODE
    volatile int time_cnt = 0;
    int j;
    for (j = 0; j < INTERRUPT_DELAY_TIME; j++) time_cnt++;
#elif _WINDOWS
    Sleep(10);
#else
    usleep(10000);
#endif
    return et5xx_has_interrupt(TRUE);
}

static int gFirstCalibrateDvrFail = -1;
void CalibrateDvrFingerTouch() {
    if (gFirstCalibrateDvrFail == -1) {
        egislog_d("FirstCalibrateDvr Fail");
        gFirstCalibrateDvrFail = 1;
        SetNeedReKVDM();
    } else {
        egislog_d("FirstCalibrateDvr OK");
        gFirstCalibrateDvrFail = 0;
    }
}

static double get_orignal_mean(BYTE a_Gain, BYTE a_Gain2, BYTE a_DC_P, BYTE a_DC_C, BYTE a_DC_F_G,
                               BYTE a_CurrMean) {
    double gain_tbl[] = {1.0,  7.17, 8.36, 9.55, 10.07, 11.9, 13.1, 14.3,
                         15.5, 16.7, 17.8, 19,   20.2,  21.4, 22.6, 23.8};
    double mean = 0;

    if (a_Gain > 0x0F) return 0;
    if (a_DC_P > 0x0F) return 0;
    if (a_DC_C > 0x3F) return 0;
    if (a_DC_F_G > 0x3F) return 0;

    int dc_p = -120 - (10 * a_DC_P);
    int dc_c = -64 + (2 * a_DC_C);
    double dc_f_g = -16.0 + (0.5f * a_DC_F_G);
    double gain = (gain_tbl[a_Gain] * (a_Gain2 ? 2 : 1));

    mean = ((double)a_CurrMean / gain) - dc_p - dc_c - dc_f_g;

    return mean;
}
