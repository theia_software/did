#include <stdint.h>
#include "core_config.h"
#include "egis_algomodule.h"
#include "egis_definition.h"
#include "egis_image_db.h"
#include "et0xx_default.h"
#include "finger_image_db.h"
#include "image_cut.h"
#include "ini_definition.h"
#include "isensor_api.h"
#include "isensor_definition.h"
#include "plat_file.h"
#include "plat_log.h"
#include "plat_mem.h"

#define LOG_TAG "RBS-DB"

#include "test_db/testdb_cb.h"

#define ET0XX_FEATURE_DB_FINGER_IDX "db_finger_idx"
#define ET0XX_FEATURE_DB_FINGERPIRNT_IDX "db_fingerpirnt_idx"
#define ET0XX_FEATURE_DB_GET_IMAGE "db_get_image"

#define DEFAULT_BKG_FILENAME "bkg.bin"

static int g_db_fimage_type;
static int g_db_finger_idx;
int g_db_fingerpirnt_idx = 0;
static int g_db_total_fingerprint_for_this_finger;
extern enum FIMAGE_DB_TYPE g_db_type;

int g_current_type = -1;
char g_map_storage_path[PATH_MAX] = IMAGE_DATA_FOLDER;
int g_map_db_type = DB_EVTOOL;
static char g_enroll_file_path[PATH_MAX];
static char g_verify_file_path[PATH_MAX];
extern int g_center_x, g_center_y;

int g_sensor_try_index;

uint8_t* g_0xx_default_bkg = NULL;

int isensor_get_frame(BYTE* frame, UINT height, UINT width, UINT number_of_frames) {
    egislog_d("%s, not implemented. return OK directly", __func__);
    return EGIS_OK;
}

static int _get_frame_from_db(BYTE* frame, int width, int height, uint16_t bytes_per_pixel,
                              int target_finger_idx) {
    egislog_d("%s, %d:%d, bytes_per_pixel %d", __func__, height, width, bytes_per_pixel);
    char main_path[PATH_MAX];
    char image_path[PATH_MAX];
    int load_size;
    int fingeridx;

    int try_index = g_sensor_try_index;

    // Just make sure bytes_per_pixel is reasonable
    if (bytes_per_pixel == 0) bytes_per_pixel = 1;
    if (bytes_per_pixel > 4) bytes_per_pixel = 4;

    if (DB_SIMEPLE == g_map_db_type) {
        g_db_fimage_type = FIMAGE_SIMPLEDB;
        fingeridx = core_config_get_int(INI_SECTION_ET0XX, KEY_VERIFY_FINGER_INDEX, 0);
    } else if (egis_fp_is_enroll()) {
        g_db_fimage_type = FIMAGE_ENROLL;
        fingeridx = core_config_get_int(INI_SECTION_ET0XX, KEY_ENROLL_FINGER_INDEX, 0);
    } else {
        g_db_fimage_type = FIMAGE_VERIFY;
        fingeridx = core_config_get_int(INI_SECTION_ET0XX, KEY_VERIFY_FINGER_INDEX, 0);
    }

    if (g_db_fimage_type != g_current_type) {
        g_current_type = g_db_fimage_type;
        if (try_index != 0) {
            egislog_e("1) try_index %d is not zero", try_index);
            try_index = 0;
        }
        g_db_fingerpirnt_idx = -1;
    }
    if (g_db_finger_idx != fingeridx) {
        g_db_finger_idx = fingeridx;
        if (try_index != 0) {
            egislog_e("2) try_index %d is not zero", try_index);
            try_index = 0;
        }
        g_db_fingerpirnt_idx = -1;
    }

    g_db_total_fingerprint_for_this_finger =
        fimage_db_get_num_fingerprint(g_db_fimage_type, g_db_finger_idx);
    if (g_db_total_fingerprint_for_this_finger == 0) {
        egislog_e("total fingerprint for this finger is 0");
        return EGIS_COMMAND_FAIL;
    }

    if (try_index == 0) {
        g_db_fingerpirnt_idx++;
        g_db_fingerpirnt_idx = g_db_fingerpirnt_idx % g_db_total_fingerprint_for_this_finger;
    }

    do {
        // egislog_v("%s (%d) [%d][%d]", __func__, g_current_type, g_db_finger_idx,
        // g_db_fingerpirnt_idx);
        fimage_db_get_path(g_db_fimage_type, fingeridx, g_db_fingerpirnt_idx, main_path);
        // egislog_v("%s (%d) [%d][%d] %s", __func__, g_current_type, g_db_finger_idx,
        // g_db_fingerpirnt_idx, main_path);

        if (g_db_fimage_type == FIMAGE_ENROLL && is_enroll_redundant(main_path)) {
            g_db_fingerpirnt_idx++;
            g_db_fingerpirnt_idx = g_db_fingerpirnt_idx % g_db_total_fingerprint_for_this_finger;
            continue;
        }
        break;
    } while (1);

    egislog_d("%s before calling fimage_db_get_try_image_count", __func__);
    int tryfolder_count = fimage_db_get_try_image_count(main_path);
    if (try_index == tryfolder_count) {
        char file_name[PATH_MAX] = {0};
        extract_file_name(main_path, file_name);
        egislog_i("%s take main_path %s", __func__, file_name);
        strcpy(image_path, main_path);
    } else if (try_index < tryfolder_count) {
        egislog_d("%s take try folder", __func__);
        fimage_db_get_tryfolder_image(main_path, try_index, image_path);
        egislog_d("%s image_path %s", __func__, image_path);
    } else {
        return EGIS_COMMAND_FAIL;
    }
    load_size = plat_load_raw_image(image_path, frame, width, height * bytes_per_pixel);
    if (load_size <= 0) {
        ex_log(LOG_ERROR, "Failed to load (%d) %s", load_size, image_path);
        return EGIS_COMMAND_FAIL;
    }
    if (load_size != width * height * bytes_per_pixel) {
        ex_log(LOG_WARN, "load size %d, image size not matched", load_size);
    }
    if (bytes_per_pixel == 2) {
        covert_endian16(frame, width, height);
    }

    int cx = 0, cy = 0;
    int parse_result = db_parse_cx_cy(image_path, &cx, &cy);
    if (parse_result == EGIS_OK) {
        g_center_x = cx;
        g_center_y = cy;
    }

    if (g_db_fimage_type == FIMAGE_ENROLL) {
        strncpy(g_enroll_file_path, image_path, PATH_MAX);
    } else if (g_db_fimage_type == FIMAGE_VERIFY) {
        strncpy(g_verify_file_path, image_path, PATH_MAX);
    }
    return EGIS_OK;
}

#define NORMAL_TEMP 25
int g_temperature = NORMAL_TEMP;
#ifdef __ET7XX__
static void _extract_info(uint8_t* image, int width, int height) {
    int i;
    int8_t temp2;
    int8_t temp1;
#define INFO_LENGTH 8
    egislog_d("einfo %d:%d", width, height);
    // Must sync with compress_info()
    const uint8_t VER = 0x1;
    uint8_t my_info[8] = {0xE, 'G', 'I', 'S', VER, (uint8_t)(NORMAL_TEMP * 2), (uint8_t)NORMAL_TEMP,
                          0};
    uint8_t read_info[INFO_LENGTH] = {0};
    extract_info(image, width, height, read_info, INFO_LENGTH);
    BOOL is_valid = TRUE;
    for (i = 0; i < 5; i++) {
        if (my_info[i] != read_info[i]) {
            is_valid = FALSE;
            break;
        }
    }
    if (!is_valid) return;

    temp2 = read_info[5];
    temp1 = read_info[6];
    if (temp2 / 2 == temp1) {
        g_temperature = temp1;
    }
    return;
}
#endif

int isensor_get_dynamic_frame(BYTE* frame, UINT height, UINT width, UINT number_of_frames) {
    g_temperature = NORMAL_TEMP;
    if (number_of_frames > 1) {
        egislog_e("error , only support read one frame at a time");
    }
    int target_finger_idx = -1;
    int ret = _get_frame_from_db(frame, width, height, 1, target_finger_idx);

    if (ret != EGIS_OK) return ret;

#ifdef __ET7XX__
    _extract_info(frame, width, height);
#endif
    return EGIS_OK;
}

int isensor_get_dynamic_frame_16bit(unsigned short* frame, UINT height, UINT width,
                                    UINT number_of_frames) {
    int bytes_per_pixel = 1;
    ;
    if (number_of_frames > 1) {
        egislog_e("error , only support read one frame at a time");
        number_of_frames = 1;
    }
    int db_is_raw_image = 0;
    isensor_get_int(PARAM_INT_DB_IS_RAW_IMAGE, &db_is_raw_image);
    if (db_is_raw_image) {
        bytes_per_pixel = 2;
    }
    int target_finger_idx = -1;
    return _get_frame_from_db((uint8_t*)frame, width, height, bytes_per_pixel, target_finger_idx);
}
int isensor_set_feature(const char* param, int value) {
    if (param == NULL) {
        egislog_e("isensor_set_feature illegal parameter");
        return EGIS_INCORRECT_PARAMETER;
    }

    egislog_d("%s, [%s] %d", __func__, param, value);

    if (strncmp(param, PARAM_UPDATE_CONFIG, sizeof(PARAM_UPDATE_CONFIG)) == 0) {
        core_config_get_string(INI_SECTION_ET0XX, KEY_DB_PATH, g_map_storage_path, PATH_MAX,
                               IMAGE_DATA_FOLDER);
        g_map_db_type = core_config_get_int(INI_SECTION_ET0XX, KEY_DB_TYPE, g_map_db_type);
        return EGIS_OK;
    } else {
        egislog_e("%s, not support", param);
    }
    return EGIS_OK;
}

static int _get_bkg_raw(unsigned char* out_buffer, int* out_buffer_size) {
    int width = SENSOR_WIDTH;
    int height = SENSOR_HEIGHT;
    int frame_sz = width * height * sizeof(uint16_t);
    char default_bkg_path[PATH_MAX] = {0};

    int img_buffer_size = width * height * 2;
    if (g_0xx_default_bkg != NULL) {
        *out_buffer_size = img_buffer_size;
        memcpy(out_buffer, g_0xx_default_bkg, *out_buffer_size);
        return EGIS_OK;
    }

    sprintf(default_bkg_path, "%s/%s", g_map_storage_path, DEFAULT_BKG_FILENAME);
    int read_size = plat_load_raw_image(default_bkg_path, out_buffer, width, height * 2);
    ex_log(LOG_DEBUG, "%s, default bkg %s load size %d", __func__, default_bkg_path, read_size);
    if (read_size > 0) {
        covert_endian16(out_buffer, width, height);
        g_0xx_default_bkg = plat_alloc(img_buffer_size);
        memcpy(g_0xx_default_bkg, out_buffer, *out_buffer_size);
        return EGIS_OK;
    } else {
        return EGIS_COMMAND_FAIL;
    }
}

int isensor_get_buffer(isensor_param_type_t param_type, unsigned char* out_buffer,
                       int* out_buffer_size) {
    if (out_buffer == NULL || out_buffer_size == NULL || *out_buffer_size <= 0) {
        egislog_e("%s [%d] wrong output parameter", __func__, param_type);
        return EGIS_INCORRECT_PARAMETER;
    }
    switch (param_type) {
        case PARAM_BUF_BDS_BKG_IMAGE:
        case PARAM_BUF_CALI_NORMALIZE_BKG_IMAGE:
            _get_bkg_raw(out_buffer, out_buffer_size);
            break;
        case PARAM_BUF_ENROLLED_PATH:
            strncpy((char*)out_buffer, g_enroll_file_path, PATH_MAX);
            break;
        case PARAM_BUF_VERIFIED_PATH:
            strncpy((char*)out_buffer, g_verify_file_path, PATH_MAX);
            break;
        case PARAM_BUF_DB_TOTAL_FINGERPRINT: {
            int count;
            if (out_buffer_size != NULL) {
                count = *out_buffer_size / sizeof(int);
            }
            for (int i = 0; i < count; i++)
                ((int*)out_buffer)[i] = fimage_db_get_num_fingerprint(FIMAGE_VERIFY, i);
            break;
        }
        default: { egislog_d("%s, not supported type = %d", __func__, param_type); } break;
    }

    return EGIS_OK;
}

int isensor_set_buffer(isensor_param_type_t param_type, unsigned char* in_buffer,
                       int in_buffer_size) {
    egislog_e("%s, not implemented. return OK directly", __func__);
    return EGIS_OK;
}

int isensor_get_sensor_id(uint16_t* sensor_id) {
    egislog_e("%s, not implemented. return OK directly", __func__);
    return EGIS_OK;
}
