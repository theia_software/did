LOCAL_PATH := $(call my-dir)
$(info LOCAL_PATH=$(LOCAL_PATH))
GIT_PROJECT_RELATIVE := ../..
#---------------------------------------------
COMMON_PLATFORM=linux

$(info SENSOR_TYPE=$(SENSOR_TYPE)  PLATFORM_OTG=$(PLATFORM_OTG))

include $(CLEAR_VARS)

LOCAL_MODULE := sensorc_et0xx
LOCAL_LDLIBS := -L$(SYSROOT)/usr/lib -llog
LOCAL_LDFLAGS += -nodefaultlibs -lc -lm -ldl
LOCAL_EXPORT_LDLIBS := $(LOCAL_LDLIBS)
LOCAL_CFLAGS := -D__LINUX__  -DRBS

LOCAL_CFLAGS += -D__ET0XX__
LOCAL_CFLAGS += -DEGIS_SPEED_DBG
LOCAL_CFLAGS += -DUSE_CORE_CONFIG_INI
LOCAL_CFLAGS += -DEGIS_DBG

ifeq ($(ET0XX_FLOW_ET7XX),true)
LOCAL_CFLAGS += -D__ET7XX__
LOCAL_CFLAGS += -DRBS_EVTOOL
endif

LOCAL_C_INCLUDES += \
    $(LOCAL_PATH)/../../../../common/platform/inc \
	$(LOCAL_PATH)/../../../sensor_control/src/et0xx/test_db \
	$(LOCAL_PATH)/../../../../common/utility/finger_image_db/inc \
	$(LOCAL_PATH)/../../../../common/utility/cutimage/inc \
	$(LOCAL_PATH)/../../../../common/utility/config/inc \
	$(LOCAL_PATH)/../../../../common/utility/hash_map \
	$(LOCAL_PATH)/../../../../common/utility \
	$(LOCAL_PATH)/../../../../common/definition \
	$(LOCAL_PATH)/../../../sensor_control/inc \
	$(LOCAL_PATH)/../../../fp_api/inc \
	$(LOCAL_PATH)/../../../../core/finger_detect/inc \
	$(LOCAL_PATH)/../../../fp_api/include \
	$(LOCAL_PATH) \
	$(LOCAL_PATH)/../../../../common/utility/inih 

LOCAL_SRC_FILES += \
	../../../sensor_control/src/et0xx/isensor_calibrate_et0xx.c \
	../../../sensor_control/src/et0xx/isensor_get_frame_et0xx.c \
	../../../sensor_control/src/et0xx/isensor_mode_et0xx.c \
	../../../sensor_control/src/et0xx/isensor_on_off_et0xx.c \
	../../../../common/utility/finger_image_db/src/finger_image_db.c \
	../../../../common/utility/finger_image_db/src/egis_image_db.c \
	../../../../common/utility/finger_image_db/src/try_folder_map.c \
	../../../../common/utility/finger_image_db/src/search_finger.c \
	../../../../common/utility/clibs/list/src/list.c \
	../../../../common/utility/clibs/list/src/list_helper.c \
	../../../../common/utility/clibs/list/src/list_iterator.c \
	../../../../common/utility/clibs/list/src/list_node.c \
	../../../../common/utility/hash_map/hashmap.c \
	../../../sensor_control/src/et0xx/db_config.c \
	../../../sensor_control/src/et0xx/db_ini_parser.c 

include $(BUILD_STATIC_LIBRARY)