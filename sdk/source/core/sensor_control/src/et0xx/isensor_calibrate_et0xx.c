#include "egis_definition.h"
#include "isensor_api.h"
#include "plat_file.h"
#include "plat_log.h"
#include "plat_mem.h"

#define LOG_TAG "RBS-Sensor"

int isensor_calibrate(FPS_CALIBRATE_OPTION option) {
    egislog_d("%s (%d)", __func__, option);

    return EGIS_OK;
}

int isensor_get_bad_pixels(BYTE** bad_pixel, int* bad_pixel_count) {
    egislog_d("%s, not implemented. return OK directly", __func__);

    return EGIS_OK;
}

int isensor_set_calib_update_listener(fn_on_calib_data_update calib_data_update_listener) {
    egislog_d("%s, not implemented. return OK directly", __func__);
    return EGIS_OK;
}

void isensor_remove_calib_update_listener(fn_on_calib_data_update calib_data_update_listener) {
    egislog_d("%s, not implemented. return directly", __func__);
    return;
}
