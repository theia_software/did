#include "egis_definition.h"
#include "isensor_api.h"
#include "isensor_definition.h"
#include "plat_file.h"
#include "plat_log.h"
#include "plat_mem.h"

#include "finger_image_db.h"
#include "test_db/testdb_cb.h"
extern char g_map_storage_path[PATH_MAX];
extern int g_map_db_type;
extern int g_db_fingerpirnt_idx;

#define LOG_TAG "RBS-Sensor"

extern uint8_t* g_0xx_default_bkg;
int isensor_open(int* timeout) {
    egislog_d("%s, image data folder %s", __func__, g_map_storage_path);

    fimage_db_create(g_map_storage_path, g_map_db_type);

    g_db_fingerpirnt_idx = 0;

    return EGIS_OK;
}

int isensor_close(void) {
    egislog_d("%s ", __func__);

    PLAT_FREE(g_0xx_default_bkg);
    fimage_db_destroy();

    SENSOR_WIDTH = 0;
    SENSOR_HEIGHT = 0;

    return EGIS_OK;
}

int isensor_set_power_off(void) {
    egislog_d("%s, not implemented. return OK directly", __func__);

    return EGIS_OK;
}

int isensor_clean_firmware_buffer(void) {
    egislog_d("%s, not implemented. return OK directly", __func__);
    return EGIS_OK;
}

int isensor_set_exposure_time(unsigned int value) {
    egislog_d("%s, not implemented. return OK directly", __func__);
    return EGIS_OK;
}

int isensor_set_dc_offset(BYTE value) {
    egislog_d("%s, not implemented. return OK directly", __func__);
    return EGIS_OK;
}

int isensor_dump_all_register(void) {
    egislog_d("%s, not implemented. return OK directly", __func__);
    return EGIS_OK;
}

int isensor_get_firmware_version(unsigned short* firmware_version) {
    egislog_d("%s, not implemented. return OK directly", __func__);
    return EGIS_OK;
}

int isensor_set_calibration_dc_offset(BYTE value) {
    egislog_d("%s, not implemented. return OK directly", __func__);
    return EGIS_OK;
}

int fp_tz_secure_sensor_init(void) {
    egislog_d("%s, not implemented. return OK directly", __func__);

    return EGIS_OK;
}