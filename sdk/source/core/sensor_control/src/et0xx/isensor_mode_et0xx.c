#include <limits.h>
#include "db_config.h"
#include "egis_definition.h"
#include "et0xx_default.h"
#include "finger_image_db.h"
#include "ini_definition.h"
#include "isensor_api.h"
#include "isensor_definition.h"
#include "plat_file.h"
#include "plat_log.h"
#include "plat_mem.h"
#include "testdb_cb.h"
#include "../../inc/isensor_definition.h"

#include <stdio.h>
#define LOG_TAG "RBS-Sensor"
#define SENSOR_CONFIG "db_info.ini"
#define CONFING_MAX_SIZE 5 * 1024
#define CONFIG_BUF_TYPE_INI 0

#define INI_SECTION_DB "image_db"
#define KEY_IMAGE_DB_TYPE "IMAGE_DB_TYP"
#define KEY_DB_SENSOR_SERIES "DB_SENSOR_SERIES"
#define KEY_DB_SENSOR_TYPE "DB_SENSOR_TYPE"
#define KEY_DB_IMAGE_WIDTH "DB_IMAGE_WIDTH"
#define KEY_DB_IMAGE_HEIGHT "DB_IMAGE_HEIGHT"
#define KEY_DB_IMAGE_FULL_WIDTH "DB_IMAGE_FULL_WIDTH"
#define KEY_DB_IMAGE_FULL_HEIGHT "DB_IMAGE_FULL_HEIGHT"
#define KEY_DB_IMAGE_CENTER_X "DB_IMAGE_CENTER_X"
#define KEY_DB_IMAGE_CENTER_Y "DB_IMAGE_CENTER_Y"
#define KEY_DB_IMAGE_BIN_FOLDER "DB_IMAGE_BIN_FOLDER"
#define KEY_DB_IMAGE_TRY_FOLDER "DB_IMAGE_TRY_FOLDER"
#define KEY_DB_IMAGE_RAW_FOLDER "DB_IMAGE_RAW_FOLDER"

#ifdef __ET7XX__
// ET713
#define DEFAULT_SENSOR_WIDTH 200   // et0xx default sensor parameter
#define DEFAULT_SENSOR_HEIGHT 200  // et0xx default sensor parameter
#define DEFAULT_SENSOR_SERIES 7    // et0xx default sensor parameter
#define DEFAULT_SENSOR_TYPE 13     // et0xx default sensor parameter
#else
#define DEFAULT_SENSOR_WIDTH 70                // et0xx default sensor parameter
#define DEFAULT_SENSOR_HEIGHT 57               // et0xx default sensor parameter
#define DEFAULT_SENSOR_SERIES ET5XX_SERIES_ID  // et0xx default sensor parameter
#define DEFAULT_SENSOR_TYPE ET516              // et0xx default sensor parameter
#endif

FP_MODE g_fp_mode = FP_MODE_DONT_CARE;
int g_center_x = 0, g_center_y = 0;
char g_bin_folder[PATH_MAX] = {0};
char g_try_folder[PATH_MAX] = {0};
char g_raw_folder[PATH_MAX] = {0};
static BOOL g_db_is_raw_image = FALSE;

extern char g_map_storage_path[];
extern int g_temperature;
extern int g_sensor_try_index;

int isensor_set_detect_mode(void) {
    egislog_d("%s, not implemented. return OK directly", __func__);
    return EGIS_OK;
}

int isensor_set_sensor_mode(void) {
    egislog_d("%s, not implemented. return OK directly", __func__);
    return EGIS_OK;
}

int isensor_set_Z1_area(BOOL forceSetFull) {
    egislog_d("%s, not implemented. return OK directly", __func__);
    return EGIS_OK;
}

int fp_tz_secure_pullup_int(BOOL polarity) {
    egislog_d("%s, not implemented. return OK directly", __func__);
    return EGIS_OK;
}

int isensor_set_custom_sensor_mode(int sensing_mode) {
    egislog_d("%s, not implemented. return OK directly", __func__);
    return EGIS_OK;
}

int isensor_get_sensor_roi_size(int* width, int* height) {
    egislog_d("isensor_get_sensor_roi_size enter!");

    if (width == NULL || height == NULL) {
        egislog_e("ERROR: illegal parameter");
        return EGIS_INCORRECT_PARAMETER;
    }
    if (SENSOR_WIDTH > 0 && SENSOR_HEIGHT > 0) {
        *width = SENSOR_WIDTH;
        *height = SENSOR_HEIGHT;
        egislog_d("isensor_get_sensor_roi_size 0x1 sensor_type = %d, width = %d, height = %d",
                  g_egis_sensortype.type, *width, *height);
        return EGIS_OK;
    }

    int retval;
    char config_file_path[PATH_MAX];

    memset(config_file_path, 0, PATH_MAX);
    sprintf(config_file_path, "%s/%s", g_map_storage_path, SENSOR_CONFIG);

    egislog_d("%s config_file_path = %s", __func__, config_file_path);

    uint8_t* config_data = plat_alloc(CONFING_MAX_SIZE);
    if (config_data == NULL) {
        return EGIS_OUT_OF_MEMORY;
    }

    unsigned int real_size;
    retval = plat_load_file(config_file_path, config_data, CONFING_MAX_SIZE, &real_size);

    if (retval == PLAT_FILE_NOT_EXIST || (retval <= 0 || real_size == CONFING_MAX_SIZE)) {
        egislog_d("%s config file is not exist! %d", __func__, retval);

        *width = DEFAULT_SENSOR_WIDTH;
        *height = DEFAULT_SENSOR_HEIGHT;
        g_egis_sensortype.type = DEFAULT_SENSOR_TYPE;
        g_egis_sensortype.series = DEFAULT_SENSOR_SERIES;
        goto EXIT;
    } else {
        db_config_create(CONFIG_BUF_TYPE_INI, config_data, real_size);
    }

    g_egis_sensortype.series =
        db_config_get_int(INI_SECTION_DB, KEY_DB_SENSOR_SERIES, DEFAULT_SENSOR_SERIES);
    g_egis_sensortype.type =
        db_config_get_int(INI_SECTION_DB, KEY_DB_SENSOR_TYPE, DEFAULT_SENSOR_TYPE);
    *width = db_config_get_int(INI_SECTION_DB, KEY_DB_IMAGE_WIDTH, DEFAULT_SENSOR_WIDTH);
    *height = db_config_get_int(INI_SECTION_DB, KEY_DB_IMAGE_HEIGHT, DEFAULT_SENSOR_HEIGHT);
    g_center_x = db_config_get_int(INI_SECTION_DB, KEY_DB_IMAGE_CENTER_X, 0);
    g_center_y = db_config_get_int(INI_SECTION_DB, KEY_DB_IMAGE_CENTER_Y, 0);
    db_config_get_string(INI_SECTION_DB, KEY_DB_IMAGE_BIN_FOLDER, g_bin_folder, PATH_MAX, "");
    db_config_get_string(INI_SECTION_DB, KEY_DB_IMAGE_TRY_FOLDER, g_try_folder, PATH_MAX, "");
    db_config_get_string(INI_SECTION_DB, KEY_DB_IMAGE_RAW_FOLDER, g_raw_folder, PATH_MAX, "");

    egislog_d("series = %d, type = %d", g_egis_sensortype.series, g_egis_sensortype.type);
    egislog_d("width = %d, height = %d", *width, *height);
    egislog_d("center_x = %d, center_y = %d", g_center_x, g_center_y);
    egislog_d("g_bin_folder = (%s)", g_bin_folder);
    egislog_d("g_try_folder = (%s)", g_try_folder);

    if (strlen(g_raw_folder) == 0) {
        strcpy(g_raw_folder, "image_raw");
        char raw_folder_path[PATH_MAX] = {0};
        sprintf(raw_folder_path, "%s/%s", g_map_storage_path, g_raw_folder);
        if (plat_file_is_exist(raw_folder_path)) {
            egislog_d(">> g_raw_folder = (%s)", g_raw_folder);
            strcpy(g_bin_folder, g_raw_folder);
            if (strlen(g_try_folder) == 0) {
                sprintf(g_try_folder, "image_try/image_raw");
                egislog_d(">> g_try_folder = (%s)", g_try_folder);
            }
        } else {
            egislog_d("not exist %s", raw_folder_path);
            strcpy(g_raw_folder, "");
        }
    }

    if (strlen(g_raw_folder) > 0) {
        g_db_is_raw_image = TRUE;
    }

    db_config_destroy();

EXIT:
    SENSOR_WIDTH = *width;
    SENSOR_HEIGHT = *height;

    PLAT_FREE(config_data);
    egislog_d("isensor_get_sensor_roi_size 0x1 sensor_type = %d, width = %d, height = %d",
              g_egis_sensortype.type, *width, *height);

    return EGIS_OK;
}

int isensor_get_sensor_full_size(int* width_full, int* height_full) {
    egislog_d("%s enter!", __func__);

    if (width_full == NULL || height_full == NULL) {
        egislog_e("ERROR: illegal parameter");
        return EGIS_INCORRECT_PARAMETER;
    }
    if (SENSOR_FULL_WIDTH > 0 && SENSOR_FULL_HEIGHT > 0) {
        *width_full = SENSOR_FULL_WIDTH;
        *height_full = SENSOR_FULL_HEIGHT;
        egislog_d("%s 0x1 sensor_type = %d, width_full = %d, height_full = %d", __func__,
                  g_egis_sensortype.type, *width_full, *height_full);
        return EGIS_OK;
    }

    int retval;
    char config_file_path[PATH_MAX];

    memset(config_file_path, 0, PATH_MAX);
    sprintf(config_file_path, "%s/%s", g_map_storage_path, SENSOR_CONFIG);

    egislog_d("%s config_file_path = %s", __func__, config_file_path);

    uint8_t* config_data = plat_alloc(CONFING_MAX_SIZE);
    if (config_data == NULL) {
        return EGIS_OUT_OF_MEMORY;
    }

    unsigned int real_size;
    retval = plat_load_file(config_file_path, config_data, CONFING_MAX_SIZE, &real_size);

    if (retval == PLAT_FILE_NOT_EXIST || (retval <= 0 || real_size == CONFING_MAX_SIZE)) {
        egislog_d("%s config file is not exist! %d", __func__, retval);

        *width_full = DEFAULT_SENSOR_WIDTH;
        *height_full = DEFAULT_SENSOR_HEIGHT;
        g_egis_sensortype.type = DEFAULT_SENSOR_TYPE;
        g_egis_sensortype.series = DEFAULT_SENSOR_SERIES;
        goto EXIT;
    } else {
        db_config_create(CONFIG_BUF_TYPE_INI, config_data, real_size);
    }

    g_egis_sensortype.series =
            db_config_get_int(INI_SECTION_DB, KEY_DB_SENSOR_SERIES, DEFAULT_SENSOR_SERIES);
    g_egis_sensortype.type =
            db_config_get_int(INI_SECTION_DB, KEY_DB_SENSOR_TYPE, DEFAULT_SENSOR_TYPE);
    *width_full = db_config_get_int(INI_SECTION_DB, KEY_DB_IMAGE_FULL_WIDTH, DEFAULT_SENSOR_WIDTH);
    *height_full = db_config_get_int(INI_SECTION_DB, KEY_DB_IMAGE_FULL_HEIGHT, DEFAULT_SENSOR_HEIGHT);
    g_center_x = db_config_get_int(INI_SECTION_DB, KEY_DB_IMAGE_CENTER_X, 0);
    g_center_y = db_config_get_int(INI_SECTION_DB, KEY_DB_IMAGE_CENTER_Y, 0);
    db_config_get_string(INI_SECTION_DB, KEY_DB_IMAGE_BIN_FOLDER, g_bin_folder, PATH_MAX, "");
    db_config_get_string(INI_SECTION_DB, KEY_DB_IMAGE_TRY_FOLDER, g_try_folder, PATH_MAX, "");
    db_config_get_string(INI_SECTION_DB, KEY_DB_IMAGE_RAW_FOLDER, g_raw_folder, PATH_MAX, "");

    egislog_d("series = %d, type = %d", g_egis_sensortype.series, g_egis_sensortype.type);
    egislog_d("width_full = %d, height_full = %d", *width_full, *height_full);
    egislog_d("center_x = %d, center_y = %d", g_center_x, g_center_y);
    egislog_d("g_bin_folder = (%s)", g_bin_folder);
    egislog_d("g_try_folder = (%s)", g_try_folder);

    if (strlen(g_raw_folder) == 0) {
        strcpy(g_raw_folder, "image_raw");
        char raw_folder_path[PATH_MAX] = {0};
        sprintf(raw_folder_path, "%s/%s", g_map_storage_path, g_raw_folder);
        if (plat_file_is_exist(raw_folder_path)) {
            egislog_d(">> g_raw_folder = (%s)", g_raw_folder);
            strcpy(g_bin_folder, g_raw_folder);
            if (strlen(g_try_folder) == 0) {
                sprintf(g_try_folder, "image_try/image_raw");
                egislog_d(">> g_try_folder = (%s)", g_try_folder);
            }
        } else {
            egislog_d("not exist %s", raw_folder_path);
            strcpy(g_raw_folder, "");
        }
    }

    if (strlen(g_raw_folder) > 0) {
        g_db_is_raw_image = TRUE;
    }

    db_config_destroy();

    EXIT:
    SENSOR_FULL_WIDTH= *width_full;
    SENSOR_FULL_HEIGHT = *height_full;

    PLAT_FREE(config_data);
    egislog_d("%s 0x1 sensor_type = %d, width_full = %d, height_full = %d", __func__,
              g_egis_sensortype.type, *width_full, *height_full);

    return EGIS_OK;
}

int isensor_get_int(isensor_param_type_t param_type, int* value) {
    egislog_d("%s, param_type = %d", __func__, param_type);
    switch (param_type) {
        case PARAM_INT_GET_IS_ET0XX:
            *value = 1;
            break;
        case PARAM_INT_DB_IS_RAW_IMAGE:
            *value = g_db_is_raw_image;
            break;
        case PARAM_INT_SENSOR_ID:
            *value = g_egis_sensortype.dev_id0 * 100 + g_egis_sensortype.dev_id1;
            break;
        case PARAM_INT_CALI_BKG_HW_INTEGRATE_COUNT:
        case PARAM_INT_INTEGRATE_COUNT:
        case PARAM_INT_HW_INTEGRATE_COUNT:
            *value = 1;
            break;
        case PARAM_INT_CALI_BKG_EXPOSURE_TIME_X10:
        case PARAM_INT_EXPOSURE_TIME_X10:
            // Fake value
            *value = 10;
            break;
        case PARAM_INT_TEMPERATURE:
            *value = g_temperature;
            egislog_d("debugb %d", (int)(g_temperature));
            break;
        case PARAM_INT_CALI_BKG_CX:
            *value = g_center_x;
            break;
        case PARAM_INT_CALI_BKG_CY:
            *value = g_center_y;
            break;
        case PARAM_INT_DB_TOTAL_FINGER:
            *value = fimage_db_get_num_finger();
            break;
        default:
            egislog_e("%s, not supported type = %d", __func__, param_type);
            break;
    }
    return EGIS_OK;
}

int isensor_set_int(isensor_param_type_t param_type, int value) {
    switch (param_type) {
        case PARAM_INT_TRY_INDEX:
            g_sensor_try_index = value;
            egislog_d("%s, g_sensor_try_index = %d", __func__, value);
            break;
        default:
            egislog_d("%s, not implemented. return OK directly", __func__);
            break;
    }
    return EGIS_OK;
}

FPS_SENSING_TYPE_T fps_get_sensing_mode(void) {
    egislog_d("%s, not implemented. return OK directly", __func__);
    return EGIS_OK;
}

int isensor_read_nvm(unsigned char* buff) {
    return EGIS_OK;
}

int isensor_read_int_status(BYTE* IntStatus) {
    egislog_d("%s, not implemented. return OK directly", __func__);
    return EGIS_OK;
}

#define SENSOR_REV_ID_LENGTH 3
int isensor_read_rev_id(BYTE* read_buf, int length) {
    int ret = EGIS_OK;
    if (length < SENSOR_REV_ID_LENGTH) return -1;

    egislog_d("Rev ID: %Xh %Xh %Xh", read_buf[2], read_buf[1], read_buf[0]);
    return ret;
}

int isensor_set_navigation_detect_mode(void) {
    egislog_d("%s, not implemented. return OK directly", __func__);
    return EGIS_OK;
}

int isensor_set_navigation_sensor_mode(void) {
    egislog_d("%s, not implemented. return OK directly", __func__);
    return EGIS_OK;
}

int isensor_recovery(FP_MODE fp_mode) {
    egislog_d("%s, not implemented. return OK directly", __func__);
    return EGIS_OK;
}

int isensor_pullup_int(BOOL polarity) {
    return EGIS_OK;
}

int isensor_set_spi_power(const int option) {
    egislog_d("%s, not implemented. return OK directly", __func__);
    return EGIS_OK;
}

int isensor_get_recovery_event(void) {
    egislog_d("%s, not implemented. return OK directly", __func__);
    return EGIS_OK;
}
