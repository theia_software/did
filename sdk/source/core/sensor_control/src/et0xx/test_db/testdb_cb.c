#include <limits.h>

#include "egis_fp_get_image.h"
#include "egis_sensormodule.h"
#include "fd_process.h"
#include "plat_log.h"
#include "testdb_cb.h"

#define LOG_TAG "RBS-testdb"

static enum FIMAGE_TYPE g_fingerprintType;
static int g_fingerIdx;
static int g_fingerprint_index;
static BOOL g_is_new_finger_on;

static Fn_LoadImageFromFile g_fn_load_image = NULL;
char g_image_path[PATH_MAX] = {'\0'};
void testdb_set_load_image_fn(Fn_LoadImageFromFile fn) {
    g_fn_load_image = fn;
}

int testdb_read(BYTE* frame, UINT height, UINT width, UINT number_of_frames) {
    egislog_d("%s entry", __func__);
    if (g_fn_load_image == NULL) {
        egislog_e("g_fn_load_image is NULL");
        return -1;
    }
    fd_set_fetch_single_image(TRUE);
    int ret_get_path =
        fimage_db_get_path(g_fingerprintType, g_fingerIdx, g_fingerprint_index, g_image_path);

    if (ret_get_path == FIMAGE_DB_TYPE_DIFFERENT) {
        fd_set_fetch_single_image(FALSE);
        if (g_is_new_finger_on) {
            g_is_new_finger_on = FALSE;
            fimage_db_get_path_start(g_fingerprintType, g_fingerIdx, g_fingerprint_index,
                                     g_image_path);
        } else {
            fimage_db_get_path_next(g_fingerprintType, g_fingerIdx, g_fingerprint_index,
                                    g_image_path);
        }
    }

    int ret = g_fn_load_image(g_image_path, frame, width, height);
    egislog_d("%s load %s return %d", __func__, g_image_path, ret);
    if (ret > 0) {
        return 0;
    }

    return -2;
}

void testdb_set_read_param(enum FIMAGE_TYPE fingerprintType, int fingerIdx, int fingerprint_index,
                           BOOL is_new_finger_on) {
    g_fingerprintType = fingerprintType;
    g_fingerIdx = fingerIdx;
    g_fingerprint_index = fingerprint_index;
    g_is_new_finger_on = is_new_finger_on;

    fd_set_fetch_image_fn(testdb_read, TRUE);

    return;
}

void testdb_set_param(enum FIMAGE_TYPE fingerprintType, int fingerIdx, int fingerprint_index,
                      BOOL is_new_finger_on) {
    g_fingerprintType = fingerprintType;
    g_fingerIdx = fingerIdx;
    g_fingerprint_index = fingerprint_index;
    g_is_new_finger_on = is_new_finger_on;

    fd_set_fetch_image_fn(NULL, TRUE);
    return;
}
void testdb_reset_read_param() {
    fd_reset_fetch_image_fn();
}

char* testdb_get_image_path() {
    return g_image_path;
}
