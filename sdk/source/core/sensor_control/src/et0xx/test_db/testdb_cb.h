
#ifndef __TESTDB_CB_HEADER__
#define __TESTDB_CB_HEADER__

#include "finger_image_db.h"
#include "type_definition.h"

#ifndef _WINDOWS
#define IMAGE_DATA_FOLDER "/sdcard/FP7"
#else
#define IMAGE_DATA_FOLDER "D:/Image"
#endif

#ifdef _MSC_VER
#ifndef PATH_MAX
#define PATH_MAX MAX_PATH
#endif
#endif

typedef int (*Fn_LoadImageFromFile)(char* path, unsigned char* pImage, unsigned int width,
                                    unsigned int height);

int testdb_read(BYTE* frame, UINT height, UINT width, UINT number_of_frames);

void testdb_set_read_param(enum FIMAGE_TYPE fingerprintType, int fingerIdx, int fingerprint_index,
                           BOOL is_new_finger_on);
void testdb_set_param(enum FIMAGE_TYPE fingerprintType, int fingerIdx, int fingerprint_index,
                      BOOL is_new_finger_on);

void testdb_reset_read_param();
void testdb_set_load_image_fn(Fn_LoadImageFromFile fn);

char* testdb_get_image_path();

#endif