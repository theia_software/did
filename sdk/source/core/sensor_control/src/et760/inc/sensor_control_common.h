#ifndef __SENSOR_CONTROL_COMMON_H__
#define __SENSOR_CONTROL_COMMON_H__
#include <stdint.h>

#define DEVID(id0, id1) (((uint16_t)(id0)<<8)|(id1))

#define __SENSOR_NUMBER10__
#ifdef __SENSOR_NUMBER10__
// #define SENSOR_A0 (0x8000)
// #define SENSOR_A1 (SENSOR_A0 >> 1)
// #define SENSOR_A2 (SENSOR_A1 >> 1)
// #define SENSOR_A3 (SENSOR_A2 >> 1)
// #define SENSOR_A4 (SENSOR_A3 >> 1)
// #define SENSOR_B0 (SENSOR_A4 >> 1)
// #define SENSOR_B1 (SENSOR_B0 >> 1)
// #define SENSOR_B2 (SENSOR_B1 >> 1)
// #define SENSOR_B3 (SENSOR_B2 >> 1)
// #define SENSOR_B4 (SENSOR_B3 >> 1)



#define SENSOR_B4 (0x8000)
#define SENSOR_B3 (0x4000)
#define SENSOR_B2 (0x2000)
#define SENSOR_B1 (0x1000)
#define SENSOR_B0 (0x800)
#define SENSOR_A4 (0x400)
#define SENSOR_A3 (0x200)
#define SENSOR_A2 (0x100)
#define SENSOR_A1 (0x80)
#define SENSOR_A0 (0x40)
#define SENSOR_ALL                                                                       \
    (SENSOR_A0 | SENSOR_A1 | SENSOR_A2 | SENSOR_A3 | SENSOR_A4 | SENSOR_B0 | SENSOR_B1 | \
     SENSOR_B2 | SENSOR_B3 | SENSOR_B4)
#else
#define SENSOR_A0 (0x00)
#define SENSOR_A1 (0x01)
#define SENSOR_A2 (0x02)
#define SENSOR_A3 (0x03)
#endif

#ifdef __cplusplus
extern "C" {
#endif

#define V2Code(volt) (uint16_t)(((volt)+10)*65535/20)

int et7xx_write_register(uint8_t address, uint8_t val);
int et7xx_write_register_16(uint16_t address, uint16_t val);
int et7xx_read_register(uint8_t address, uint8_t* val);
int et7xx_read_register_16(uint16_t address, uint16_t* val);
int et7xx_read_frame(uint8_t* buffer, int length);
int et7xx_io_command(int cmd, int param1, int param2, uint8_t* out_buf, int* out_buf_size);
int et7xx_polling_registry(uint8_t address, uint8_t expect, uint8_t mask);
int et7xx_sensor_select(uint16_t select);

int et901_set_spi_mux(uint8_t mux);
int et901_fpga_set_HIVDAC(uint16_t VPP1, uint16_t VPP2, uint16_t VCOM, uint16_t VGH, uint16_t VGL);

int et760_dispatch_get_id(uint8_t *dev_id0, uint8_t *dev_id1);
int et760_dispatch_init_sensor();
int et760_dispatch_set_sensor_param(int param, int value);
int et760_dispatch_get_sensor_param(int param, int *value);
int et760_dispatch_get_frame(uint8_t *buffer, int length);
int et760_dispatch_raw_spi(int param, uint8_t *buffer, int length) ;

#ifdef __cplusplus
}
#endif

#endif
