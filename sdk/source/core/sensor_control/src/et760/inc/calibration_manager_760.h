#ifndef __CALIBRATION_MANAGER_H__
#define __CALIBRATION_MANAGER_H__

#include <stdbool.h>
#include "isensor_api.h"

#define BASE_EXPOSE_TIME (16.67)
#define EXP_SPECIES (2)
#define HW_INT_SPECIES (1)
#define CALI_DATA_MAX_SIZE (600*800)
// 暫時先減小: (1320*1000)

typedef struct {
    float exp_time;
    int hw_integrate_count;
    int bkg_cx;
    int bkg_cy;
    int adc_dac;
    uint16_t data[CALI_DATA_MAX_SIZE];
} CALIBRATE_INFO;

typedef enum calib_bkg_type {
    CALIB_BKG_BK,
    CALIB_BKG_WK,
} calib_bkg_t;

int calibration_set_calib_update_fn(fn_on_calib_data_update calib_data_update_listener);
void calibration_remove_calib_update_fn(fn_on_calib_data_update calib_data_update_listener);
void calibration_set_folder_path(const char* folder_path);
int calibration_load(void);
int calibration_get_bkg_from_flash(uint8_t* bkg_flash, int* bkg_flash_sz);
int calibration_save(void);
int calibration_generate(void);
int calibration_save_to_flash(void);
int calibration_load_from_flash(void);
int calibration_remove(void);
uint16_t* calibration_get_bkg(void);
uint16_t* calibration_get_wkg(void);
int calibration_get_bkg_integer(int type);

void calibration_get_bkg_center(int* bkg_cx, int* bkg_cy);

int calibration_save_to_buffer(unsigned char* buffer, int* len);
int calibration_load_from_buffer(unsigned char* buffer, int len);
void calibration_set_bkg_type(int option);
#endif
