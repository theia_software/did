#pragma once
/**
    add for ET760
 **/
#define RISCV_IMAGE_ARRD (0x60000004)
#define RISCV_TCON_DONE_INT (0x20000004)
#define RISCV_CONFIG_START_ADDR (0x60000000)

#define RAW_IMAGE_BPP_ET760 (16)
/**
    ET760 only global 變數
 **/
#ifndef __ET760_SENSOR_CONTROL__
#define __ET760_SENSOR_CONTROL__

int et7xa_set_expo_time_sw(float expo_time);  //避免COMPILE ERROR，之後是ET760 與 ET7xa分開
int et7xa_get_expo_time_sw(float* expo_time);  //避免COMPILE ERROR，之後是ET760 與 ET7xa分開

int et760_fetch_raw_data_without_setting(uint16_t* raw_image, int width, int height, int x0,
                                         int y0);

int et760_is_spi_working();
int et760_init_sensor(void);

int et760_fetch_raw_data(uint16_t* raw_image, int width, int height, int x0, int y0);

int et760_get_sensor_id(uint8_t* dev_id0, uint8_t* dev_id1);
void et760_statistics_data(void* statistics_data);

// for et760 only
int et760_set_expo_time_sw(float expo_time);
int et760_get_expo_time_sw(float* expo_time);

int et760_set_expo_time_enable_sw(uint8_t exposure_enable, float expo_time);  // for OPCODE control
int et760_set_all_adc_dac(int adc_dac);
int et760_get_image(uint8_t* buffer, int length);

int et760_search_dac(int expo, int target_avg, int *targetDac);

int et760_ba_enable_spi();

extern int adc_gain;  // ET760 GAIN

#endif
