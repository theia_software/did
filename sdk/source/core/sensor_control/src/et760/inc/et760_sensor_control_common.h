#ifndef __ET760_SENSOR_CONTROL_COMMON_H__
#define __ET760_SENSOR_CONTROL_COMMON_H__
#include <stdint.h>

/**
 * et760 sensor control common
 *
 * 放ET760底層用的，跟PANEL無關
 *
 */

#define ET760_BASEADDR_SPI0 (0x6000000c)
#define ET760_BASEADDR_SPI1 (0x2000000c)
#define ET760_VERSION (0x30000000)
#define ET760_VERSION_RESULT (0x20190118)
#define ET760_SYSTEM_RISCV_RESET_VECTOR (0x1000005c)
#define ET760_SYSTEM_RESET_RISC_V (0x10000038)
#define ET760_CHECK_INIT_ADDRESS (0x50000168)

#define SPI_MAX_LENGTH (16384) // 目前走OTG的SPI資料上限為16K，而且LENGTH必需為512的倍數(不含前面OPCODE及OFFSET)

#ifdef __cplusplus
extern "C" {
#endif


    /*
     * 未來ET760_RAW_SPI為主，其他的USB-OTG的可能移除
     */
enum et760_param {
    ET760_PARAM_EXPO_TIME = 1000,
    ET760_PARAM_OFFSET = 1001,
    ET760_PARAM_X_START = 1002,
    ET760_PARAM_X_LENGTH = 1003,
    ET760_PARAM_Y_START = 1004,
    ET760_PARAM_Y_LENGTH = 1005,
    ET760_PARAM_ALL_ADC_DAC = 1006,
    ET760_PARAM_SAMPLE_MODE = 1007,
    ET760_RAW_SPI = 9000,
};

int et760_32bit_read(int addr, int *value);
int et760_32bit_read_value(int addr) ;
int et760_32bit_write(int addr, int value);
int et760_set_spi_default_address() ;
int et760_set_spi_base_address(int addr);
int et760_opcode(int opcode);
int et760_opcode_b3_value(int opcode, uint8_t b3, int value );
int et760_opcode_v1v2(int opcode, int v1, int v2);
int et760_opcode4(int opcode, int value) ;
int et760_burst_read(uint8_t* buffer, int length) ;
int et760_burst_write(uint8_t* buffer, int length) ;
int et760_burn_bin_code(uint8_t *buf, int buf_size) ;
void force_enable_ba_spi() ;
void et760_set_gain(int gain_value) ;
void control_spi_speed(int speed) ;
int chip_speed_flow() ;
int calibrateSysClock();
float get_chip_speed() ;


int et760_dispatch_raw_spi(int param, uint8_t *buffer, int length) ;

// OTG的未來都不會用，只會用RAW_SPI組
int et760_dispatch_get_id(uint8_t *dev_id0, uint8_t *dev_id1);
int et760_dispatch_init_sensor();
int et760_dispatch_set_sensor_param(int param, int value);
int et760_dispatch_get_sensor_param(int param, int *value);
int et760_dispatch_get_frame(uint8_t *buffer, int length);

int et760_set_all_adc_dac(int adc_dac) ;

int** et760_2d_alloc(int width, int height);
void et760_2d_free(int **array, int height);

void putPartArray(int **dest, int** src, int start_x, int start_y, int src_width, int src_height) ;
void getPartArray(int **dest, int** src, int start_x, int start_y, int width, int height) ;
void rotate180(int** src, int width, int height) ;
#ifdef __cplusplus
}
#endif

#endif
