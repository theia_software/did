#pragma once

#ifndef __AUO_OLED_PANEL__
#define __AUO_OLED_PANEL__

#include <stdbool.h>
#include "ini_definition.h"
#include "plat_log.h"
#include "plat_mem.h"
#include "plat_spi.h"
#include "plat_time.h"


// 預期被外部修改，設定extern
extern int g_auo_EXPTIME ;
extern int g_auo_CMI_TIMES ;
extern int g_auo_ZERO_EXPO_EN ;
extern int g_auo_CURRENTSOURCE;

extern int g_x_zsw_start;
extern int g_x_zsw_end; 
extern int g_y_stv_start ;
extern int g_y_stv_end ;

extern int g_roi_x ;
extern int g_roi_y ;
extern int g_roi_w ;
extern int g_roi_h ;

extern int g_auo_VGH_R ;
extern int g_auo_VGL_R ;
extern int g_auo_VGH_W ;
extern int g_auo_VGL_W ;
extern int g_auo_SVDD ;

#define CELL_WIDTH 200 
#define CELL_HEIGHT 200
#define CELL_ROW_LENGTH 4
#define CELL_COL_LENGTH 3

#define REGION_MINUS 24

void auo3x4_init_panel();
void auo3x4_update_config();
void auo3x4_set_expotime_ms(int ms);

void auo3x4_trigger_tcon() ;
int auo3x4_wait_tcon() ;
int auo3x4_empty_wait_tcon() ;
void auo3x4_empty_trig() ;

// single area
void auo3x4_change_region(int x, int y) ;

// full area
void change_region_roi_v2(int x_start, int x_num, int y_stv_start, int y_stv_end) ;
int convert_to_roi_v2(int x, int y, int w, int h) ;

int ** auo3x4_get_image_single_area() ;
int ** auo3x4_get_image(int width, int height) ; 
void auo3x4_get_full(int ** full) ;

int calc_limit() ;
int y_cross_region_num(int y, int height);
int x_cross_region_num(int x, int width);
int is_in_x_region(int x, int width, int region);


#endif
