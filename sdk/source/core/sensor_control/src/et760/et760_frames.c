#include <stdbool.h>
#include <auo_oled_panel.h>
#include "isensor_api.h"

#include "core_config.h"
#include "egis_definition.h"
#include "fp_err.h"
#include "fpsensor_7xa_definition.h"
#include "ini_definition.h"
#include "isensor_definition.h"
#include "plat_log.h"
#include "plat_mem.h"
#include "plat_spi.h"
#include "plat_time.h"
#include "sensor_control_common.h"
#include "et760_sensor_control.h"

#define LOG_TAG "RBS-Sensor"

int et760_set_position(int x, int y, int width, int height);
static bool check_frame_ready(int hw_integrate_count, float exposure_time);

extern float g_exp_time;
extern int g_all_adc_dac;
extern int g_matcher_threshold[5]; // for et760 only, different threshold
extern int g_hw_integrate_count;
extern int g_zone_average_stabled;
extern BOOL g_gain2_enable;
extern BOOL g_enable_pipeline_capture;
extern BOOL g_run_quick_capture_hw_int;
extern float g_run_quick_capture_exp_time;
extern int g_try_match_count;
extern uint8_t g_sensor_pix_id;
static void print_statistics_data(uint16_t* ptr_raw, int frame_size);
typedef struct {
    int max;
    int min;
    int mean;
} statistics_data_t;

statistics_data_t g_statistics_data = {0};

#ifdef FAKE_IMAGE_FLOW
BOOL egis_fp_is_enroll();
int g_fake_enroll_count = 0;
int g_fake_verify_count = 0;
#endif
#ifndef TZ_MODE
#include "errno.h"
int fopen_s(FILE **f, const char *name, const char *mode) 
{
	int ret = 0;
	//assert(f);
	*f = fopen(name, mode);
	/* Can't be sure about 1-to-1 mapping of errno and MS' errno_t */
	if (!*f)
		ret = errno;
	return ret;
}
void g7_write_file(char* fn, BYTE *feat, int len) 
{
	FILE *f;
	if (fopen_s(&f, fn, "wb") == 0)	{
		fwrite(feat, sizeof(BYTE), len, f);
		fclose(f);
	}
}

//Caller needs to free the returned data
BYTE* g7_read_file(char* fn, int* len) 
{
	FILE *f;
	int size;
	BYTE *ret = NULL;
	if (fopen_s(&f, fn, "rb") == 0) {
		fseek(f, 0, SEEK_END);
		size = ftell(f);
		fseek(f, 0, SEEK_SET);
		ret = plat_alloc(size);
		*len = (int)fread(ret, 1, size, f);
		fclose(f);
	}
	return ret;
}

#include "unistd.h"
#include "fcntl.h"
#include "errno.h"
void save_bin_file(char *fn, unsigned char *data, int size)
{
	int fd, ret;
	egislog(LOG_DEBUG, "save_bin_file fn = %s data = %p size = %d", fn, data, size);

	fd = open(fn, O_CREAT|O_TRUNC|O_RDWR, 0666);
	if (fd > 0) {
		ret = write(fd, data, size);

		egislog(LOG_DEBUG, "save_bin_file ret = %d", ret);
		close(fd);
	} else {
		egislog(LOG_ERROR, "open file fail fn = %s", fd);
	}
}
#endif

static int et7xa_fetch_zone_avg(unsigned char* buf, int* buf_size, int bac_exp_time) {
#ifdef FAKE_IMAGE_FLOW
	egislog_i("darkresearch fake_760_flow");
	return RESULT_OK;
#endif
    if (bac_exp_time > 0) {
        et7xa_set_expo_time_sw(bac_exp_time);
    }

    int ret = et7xx_write_register(ET7XA_ADDR_HDR_CSR, 0x00);  // Default
    et7xx_write_register(ET7XA_ADDR_SCAN_FRAMES, 0x00);        // capture one image
    et7xx_write_register(ET7XA_ADDR_SCAN_CSR, 0x21);           // start capture
    if (0 != et7xx_polling_registry(ET7XA_ADDR_SYS_STUS, 0x1, 0x1)) {
        return -1;
    }
    return et7xx_io_command(IOCMD_READ_ZONE_AVERAGE, 0, 0, buf, buf_size);
}

#define ZONE_AVG_AREA_WIDTH 6
#define ZONE_AVG_AREA_HEIGHT 6
#define ZONE_AVG_SIZE (ZONE_AVG_AREA_WIDTH * ZONE_AVG_AREA_HEIGHT) /*36*/

static BOOL is_dark_image(unsigned short* pbuf, int black_img_avg_threshold) {
#ifdef FAKE_IMAGE_FLOW
	egislog_i("darkresearch fake_760_flow");
	return FALSE;
#endif
    if (pbuf == NULL) return false;
    int i, total_block_avgs = 0;
    int all_block_avg = 0.0;
    for (i = 0; i < ZONE_AVG_SIZE; i++) {
        total_block_avgs += *pbuf++;
    }
    all_block_avg = total_block_avgs / ZONE_AVG_SIZE;
    egislog_d("%s, all block avg = %d, threshold = %d", __func__, all_block_avg,
              black_img_avg_threshold);
    return all_block_avg < black_img_avg_threshold;
}

#define _ABS(x, y) (short)((x > y) ? (x - y) : (y - x))
int et7xa_fetch_zone_avg_check_stable(int fetch_times) {
#define MAX_ZONE_AVG_TEST_COUNT 10
#define MAX_DARK_IMAGE_COUNT 10
#ifdef FAKE_IMAGE_FLOW
	egislog_i("darkresearch fake_760_flow");
	return RESULT_OK;
#endif
}

#define FAKE_ENROLL_IMAGE_COUNT (15)
#define FAKE_VERIFY_IMAGE_COUNT (30)
#define FAKE_IMAGE_PATH "/sdcard/et760_fake_image/p1/1/"
#define FAKE_ENROLL_IMAGE_PATH FAKE_IMAGE_PATH"enroll/st"
#define FAKE_VERIFY_IMAGE_PATH FAKE_IMAGE_PATH"verify/st"
uint8_t g_read_image_count = 0;

static int __fetch_raw_data(uint16_t* raw_image, int width, int height, BOOL is_need_setting, int x0, int y0) {
#ifdef FAKE_IMAGE_FLOW
	egislog_i("darkresearch fake_760_flow");
	egislog_i("darkresearch fake_760_flow __fetch_raw_data raw_image %p width %d height %d is_need_setting %d",
										raw_image, width, height, is_need_setting);
	egislog_i("darkresearch egis_fp_is_enroll() = %d", egis_fp_is_enroll());

	//__fetch_fake_raw_data(raw_image, width, height);
	//__fetch_fake_raw_data(raw_image, width, height, 0, 0);
	
#if 1

    // todo: 注意如果orientation改了，這裡的RULE也要改，這裡是UI座標，RBS之方向與WITDH會影響
	if(x0 < 0 || y0 < 0 || x0 > (SENSOR_FULL_WIDTH - width/2) || y0 > (SENSOR_FULL_HEIGHT - height/2)){
        egislog_d("%s, invalid parameters %d %d %d %d", __func__, x0, y0, width, height);
        return RESULT_INVALID_PARAMETER;
    }

	int length = width * height * sizeof(uint16_t);
	unsigned char *raw_image_buffer= plat_alloc(length);

    egislog_d("%s, g_all_adc_dac = %d", __func__, g_all_adc_dac);
    egislog_d("%s, g_exp_time = %d", __func__, (int)(g_exp_time*10));
    et760_set_all_adc_dac(g_all_adc_dac) ;

    //et760_set_expo_time_sw(g_exp_time) ;
    int cross_region = y_cross_region_num(y0, height);
    et760_set_expo_time_sw(g_exp_time - (cross_region-1)*REGION_MINUS ) ;

	et760_set_position(x0, y0, width, height);
    //et760_dispatch_get_frame(raw_image_hw, length);
    et760_get_image(raw_image_buffer, length) ;

    // 不像ET7XA 沒有分_hw 不要crop那些的
    memcpy(raw_image, raw_image_buffer,length ) ;
    print_statistics_data(raw_image, width * height);
	plat_free(raw_image_buffer);
#endif
	return RESULT_OK;
#endif
}


int et760_fetch_raw_data(uint16_t* raw_image, int width, int height, int x0, int y0) {
    int ret;
    ret = __fetch_raw_data(raw_image, width, height, TRUE, x0, y0);
    return ret;
}
int et7xa_fetch_raw_data(uint16_t* raw_image, int width, int height, int x0, int y0) {
    int ret;
    ret = __fetch_raw_data(raw_image, width, height, TRUE, x0, y0);
    return ret;
}

int et760_fetch_raw_data_without_setting(uint16_t* raw_image, int width, int height, int x0, int y0) {
    int ret;
    ret = __fetch_raw_data(raw_image, width, height, FALSE, x0, y0);
    return ret;
}
#ifdef QSEE
extern int io_et713_lense_type_uuid(unsigned char* uuid);
#endif

void et7xx_read_otp_uuid(unsigned char* uuid) {
#ifdef FAKE_IMAGE_FLOW
	egislog_i("darkresearch fake_760_flow");
	return;
#endif
    int ret = 0;
    egislog_d("et7xx_read_otp_uuid start");
#ifdef QSEE
    ret = io_et713_lense_type_uuid(uuid);
#endif
    egislog_d("et7xx_read_otp_uuid end, %d", ret);
    return;
}

void et760_statistics_data(void* statistics_data) {
    memcpy(statistics_data, &g_statistics_data, sizeof(statistics_data_t));
    egislog_i("mean=%d max=%d min=%d", ((statistics_data_t*)statistics_data)->mean,
              ((statistics_data_t*)statistics_data)->max,
              ((statistics_data_t*)statistics_data)->min);
}

// Note: statistic data must be readout before image frame
static void print_statistics_data(uint16_t* ptr_raw, int frame_size) {
    if(frame_size <= 0 || ptr_raw == NULL) {
        egislog_e("%s, invalid input parameter ptr_raw 0x%08x, frame_size %d",
        ptr_raw, frame_size);
        return;
    }
    uint16_t max, min, mean=0;
    long total = 0;
    max = ptr_raw[0];
    min = ptr_raw[0];
    for(int i=1; i<frame_size; i++) {
        if(ptr_raw[i] > max) {
            max = ptr_raw[i];
        }
        if(ptr_raw[i] < min) {
            min = ptr_raw[i];
        }
        total += ptr_raw[i];
    }
    mean = total/frame_size;
    egislog_d("mean=%d max=%d min=%d", mean, max, min);
    g_statistics_data.mean = (int)mean;
    g_statistics_data.max = (int)max;
    g_statistics_data.min = (int)min;
}

static bool check_frame_ready(int hw_integrate_count, float exposure_time) {
    int retry;
    uint8_t SYS_STUS = 0x00;
    bool success = false;
    const int max_retry = 1000;
#ifdef FAKE_IMAGE_FLOW
	egislog_i("darkresearch fake_760_flow");
	return true;
#endif

    if (!g_enable_pipeline_capture || g_try_match_count == 0) {
        plat_sleep_time((uint32_t)(exposure_time * hw_integrate_count));
    }

    for (retry = 0; retry < max_retry; retry++) {
        et7xx_read_register(ET7XA_ADDR_SYS_STUS, &SYS_STUS);
        if ((SYS_STUS & IMG_RDY) == 1) {
            success = true;
            break;
        }
        plat_sleep_time(1);
    }
    if (success) {
        egislog_d("retry = %d, SYS_STUS = 0x%x", retry, SYS_STUS);
    } else {
        egislog_e("retry = %d, SYS_STUS = 0x%x", retry, SYS_STUS);
    }
    return success;
}
