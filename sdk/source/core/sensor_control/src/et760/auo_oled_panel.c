/**

implement 各家的:
Init Sensor
opcode type & oled 
change region
get_image
*/
#include "auo_oled_panel.h"
#include "et760ba_cof_oled_3x4_full_area.h"
#include "et760_sensor_control.h"
#include "et760_sensor_control_common.h"
#include "egis_definition.h"

#define LOG_TAG "et760_AUO"

int g_auo_EXPTIME = 200 ; // for imo save time, storage real time is ms, not opcode time
int g_auo_CMI_TIMES = 1 ;
int g_auo_ZERO_EXPO_EN = 0;
int g_auo_CURRENTSOURCE = 5 ;
int g_auo_DD_EN = 0 ;



int g_y_stv_start = 0 ;
int g_y_stv_end = 49;

int g_x_zsw_start = 0 ;
int g_x_zsw_end = 49;

// for input et760_set_position
int g_roi_x ;
int g_roi_y ;
int g_roi_w ;
int g_roi_h ;

int g_auo_VGH_R = 16 ;
int g_auo_VGL_R = 17 ;
int g_auo_VGH_W = 8 ;
int g_auo_VGL_W = 8 ;
int g_auo_SVDD = 10 ;


// header for AuoOledPanel 3x4

// auo op code list:
int stv = 0xF1 ;
int zsw = 0xF2 ;
int OP_CMD_start = 0xFD ;

int OP_CMD_end = 0xFE ;
int OP_CMD_N = 0xF3 ;

int T1 = 0xF4 ;
int T5 = 0xF5 ;
int T6 = 0xF6 ;
int Tfp1 = 0xF7 ;
int Tfp2 = 0xF8 ;
int Tfp3 = 0xF9;
int Tfp4 = 0xFa ;
int Tfp5 = 0xFB ;
int Tfp6 = 0xFC ;
int Tfp7 = 0xE0 ;
int Tfp8 = 0xE1 ;
int Tfp9 = 0xE2 ;

int VGH_R = 0xD0 ;
int VGL_R = 0xD1 ;
int SVDD_0 = 0xD2 ;
int SVSS_0  = 0xD3 ;
int VB = 0xD4 ;
int SVDD_1 = 0xD5 ;
int SVSS_1 = 0xD6 ;
int SVSS_2 = 0xD7 ;
int SVSS_3 = 0xD8 ;

int OP_CMD_SW_TRIGGER = 0xE4 ; // may not use
int OP_CMD_HW_TRIG =  0xE3 ;
int OP_CMD_EXPTIME = 0xE5 ; // 4 + 16
int OP_CMD_ZERO_EXPO_EN = 0xE6 ;
int OP_CMD_CMI_TIMES = 0xE7 ;
int OP_CMD_CURRENTSOURCE = 0xE8 ;
int OP_CMD_DD_EN = 0xE9 ;

int AUO_OP_CMD_LINE_START = 0xF0 ;
int AUO_OP_CMD_LINE_END = 0xE6 ;
int ZSW_START = 0xF2 ;
int ZSW_END =  0xC2 ;

int AUO_OP_ROW_START = 0xC3 ;
int AUO_OP_ROW_END = 0xC4 ;

int image_addr = 0x00014E00 ;
int tcon_addr = 0x20000004 ;


/**
 * software project is dotline bug
 * @return
 */
int check_dotline_bug() {

	convert_to_roi_v2(200,200,200,200) ; // region 2,2
	et760_set_all_adc_dac(0) ; // 強制設定一個DEFAULT
	auo3x4_set_expotime_ms(0) ;

	auo3x4_empty_trig() ;
	auo3x4_empty_wait_tcon() ;
	auo3x4_trigger_tcon() ;
	int **frame = auo3x4_get_image(200,200) ;

	int not_zero_cnt = 0 ;
	for ( int h = 0 ; h < 200 ; h++ ) {
		for ( int w = 0 ; w < 200 ; w++ ) {
			if ( frame[h][w] >= 5000 ) {
				not_zero_cnt ++ ;
			}
		}
	}

	if ( not_zero_cnt > 20) {
		egislog_i("has dotline bug ") ;
		return 1 ;
	}

	return 0 ;
}

void auo3x4_init_panel() {

	egislog_i("Burn Firmware name = %s", "et760ba_cof_oled_3x4_full_area" ) ;
	int has_dotline_bug = 0 ;
	int check_dotline_cnt = 0 ;

	while( has_dotline_bug == 0 ) {

		et760_burn_bin_code(ET760_FIRMWARE, sizeof(ET760_FIRMWARE));

		auo3x4_update_config() ;
		et760_set_gain(0) ;
		et760_set_all_adc_dac(0) ;
		auo3x4_set_expotime_ms(100) ;

		if ( check_dotline_bug() == 0 ) {
			break ;
		}
		check_dotline_cnt ++ ;
		if ( check_dotline_cnt > 1) {
			egislog_e("dotline bug machine, please change hardware") ;
			break ;
		}
	}
}



/**
	更新所有相關參數
*/
void auo3x4_update_config() {
	egislog_i("update all config for auo3x4 single area" ) ;
	et760_opcode(OP_CMD_start) ;

	et760_opcode4(OP_CMD_N, 1);
	et760_opcode4(T1, 108);
	et760_opcode4(T5, 2);
	et760_opcode4(T6, 2);

	et760_opcode4(Tfp1, 0);
	et760_opcode4(Tfp2, 1);
	et760_opcode4(Tfp3, 1);
	et760_opcode4(Tfp4, 12);
	et760_opcode4(Tfp5, 32);
	et760_opcode4(Tfp6, 0);
	et760_opcode4(Tfp7, 0);
	et760_opcode4(Tfp8, 75);
	et760_opcode4(Tfp9, 20);

    et760_opcode4(VGH_R, g_auo_VGH_R);
    et760_opcode4(VGL_R, g_auo_VGL_R);
    et760_opcode4(SVDD_0, g_auo_VGH_W);
    et760_opcode4(SVSS_0, g_auo_VGL_W);
    et760_opcode4(VB, 6);
    et760_opcode4(SVDD_1, g_auo_SVDD);
    et760_opcode4(SVSS_1, 0);
    et760_opcode4(SVSS_2, 0);
    et760_opcode4(SVSS_3, 19);

    egislog_i("Volts: VGH_R %d, VGL_R %d, VGH_W %d, VGL_W %d, SVDD %d",
    		g_auo_VGH_R, g_auo_VGL_R, g_auo_VGH_W, g_auo_VGL_W, g_auo_SVDD) ;

	et760_opcode4(OP_CMD_CMI_TIMES, g_auo_CMI_TIMES );
	et760_opcode4(OP_CMD_ZERO_EXPO_EN, g_auo_ZERO_EXPO_EN);
	et760_opcode4(OP_CMD_CURRENTSOURCE, g_auo_CURRENTSOURCE);

	auo3x4_set_expotime_ms(200) ; 
	et760_opcode4(OP_CMD_DD_EN, 0);
	et760_opcode(OP_CMD_end) ;
}


/**
 * expo
 * input ms, send opcode value = (x-16)/4
 * 200ms: (200-16)/4 = 46 
 * 100ms: (100-16/4 = 21
 *
 * @param ms
 */
void auo3x4_set_expotime_ms(int ms) {
	int output_value = (ms-REGION_MINUS)/4 ;
	egislog_i("set expo time ms %d (%d)", ms, output_value) ;
	if ( output_value < 0) {
		output_value = 0 ;
	}
	et760_opcode4(OP_CMD_EXPTIME, output_value);
}

void auo3x4_trigger_tcon(){
	egislog_i("Trigger Tcon!") ;
	et760_32bit_write(tcon_addr, 0);
	et760_opcode(OP_CMD_HW_TRIG);
}

int auo3x4_wait_tcon() {
	egislog_i("Wait Tcon!") ;
	int retry = 100 ;
	plat_sleep_time(g_auo_EXPTIME);
	int tconDone = 0 ;
	while ( tconDone == 0 ){
		tconDone = et760_32bit_read_value(tcon_addr) ;
		retry -- ;
		if (retry == 0) {
			break ;
		}
		plat_sleep_time(10);
	}
	if ( retry == 0 ) {
		egislog_e("tcon did not done") ;
		return retry ;
	}
	else {
		egislog_i("get tcon done! retry: %d", retry);
	}
	return retry ;
}


int auo3x4_empty_wait_tcon() {
	egislog_d("empty wait tcon") ;
	int retry = 100 ;
	plat_sleep_time(g_auo_EXPTIME);
	int tconDone = 0 ;
	while ( tconDone == 0 ){
		tconDone = et760_32bit_read_value(tcon_addr) ;
		retry -- ;
		if (retry == 0) {
			break ;
		}
		plat_sleep_time(10) ;
	}
	if ( retry == 0 ) {
		egislog_e("tcon did not done") ;
		return retry ;
	}
	else {
		egislog_i("get tcon done! retry: %d", retry);
	}
	return retry ;
}

void auo3x4_empty_trig() {
	//egislog_d("Disable Empty Trig!") ;
	egislog_i("Empty Trig!") ;
	auo3x4_trigger_tcon();
	auo3x4_empty_wait_tcon();
}



/**
 * for single area first
 * @param x
 * @param y
 */
void auo3x4_change_region(int x, int y) {
	egislog_i("Change Region: %d, %d", x, y) ;
	et760_opcode(OP_CMD_start);
	et760_opcode4(stv, x);
	et760_opcode4(zsw, y);
	et760_opcode(OP_CMD_end);
}


/**
 * for full area
**/
void change_region_roi_v2(int x_zsw_start, int x_zsw_end, int y_stv_start, int y_stv_end){
	egislog_i("Change Region 2, x:%d, %d y: %d %d", x_zsw_start, x_zsw_end, y_stv_start, y_stv_end ) ;

    et760_opcode(OP_CMD_start);

    et760_opcode4(AUO_OP_CMD_LINE_START, y_stv_start);
    et760_opcode4(AUO_OP_CMD_LINE_END, y_stv_end);

    et760_opcode4(AUO_OP_ROW_START, x_zsw_start);
    et760_opcode4(AUO_OP_ROW_END, x_zsw_end);

    et760_opcode(OP_CMD_end);
}


int convert_to_roi_v2(int x, int y, int w, int h){

	g_roi_x = x ;
	g_roi_y = y ;
	g_roi_w = w ;
	g_roi_h = h ;

    y = (int) (y / 4);
    y = y * 4 ;
    g_y_stv_start = y / 4 ;
    g_y_stv_end = (g_roi_y+h)/4 - 1;

    if ((g_roi_y+h) % 4 != 0 ) {
        g_y_stv_end = g_y_stv_end + 1 ; // include the last line
    }

    x = (int) (x / 4);
    x = x * 4 ;
    g_x_zsw_start = x / 4 ;
    g_x_zsw_end = (g_roi_x+w)/4 - 1;

    if ((g_roi_x+w) % 4 != 0 ) {
        g_x_zsw_end = g_x_zsw_end + 1 ;
    }

    egislog_i("xywh:(%d, %d, %d, %d) to x_zsw/y_stv:(%d-%d, %d-%d)",
            g_roi_x, g_roi_y, g_roi_w, g_roi_h,
            g_x_zsw_start, g_x_zsw_end, g_y_stv_start, g_y_stv_end) ;
    return EGIS_OK ;
}



void auo3x4_get_full_hori(int ** full) {
	egislog_i("Get Full Area Image by hori split2") ;

	int full_width = CELL_WIDTH * CELL_COL_LENGTH ;
	int full_height = CELL_HEIGHT * CELL_ROW_LENGTH ; ;
	int half_height = full_height / 2 ;

	change_region_roi_v2(0,149,0,99) ;
	auo3x4_empty_trig();
	auo3x4_trigger_tcon() ;
	auo3x4_wait_tcon();

	int**top = auo3x4_get_image(full_width, half_height) ;

	change_region_roi_v2(0,149,100,199) ;
	auo3x4_empty_trig();
	auo3x4_trigger_tcon() ;
	auo3x4_wait_tcon();

	int** bottom = auo3x4_get_image(full_width, half_height) ;

	for ( int h = 0 ; h < half_height ; h++ ) {
		for ( int w = 0 ; w < full_width ; w++ ) {
			full[h][w] = top[h][w];
			full[h+half_height][w] = bottom[h][w] ;
		}
	}

	et760_2d_free(top, half_height) ;
	et760_2d_free(bottom, half_height) ;
}




/**
 * info by auo anita, avoid overlap
 * @param full
 */
void auo3x4_get_full_hori_overlap8(int ** full) {
	egislog_i("Get Full Area Image by hori split2") ;

	int full_width = CELL_WIDTH * CELL_COL_LENGTH ;
	int full_height = CELL_HEIGHT * CELL_ROW_LENGTH ; ;
	int half_height = full_height / 2 ;

	int overlap_pixel = 32 ;
	int overlap_val = overlap_pixel / 4 ;


	auo3x4_set_expotime_ms(g_auo_EXPTIME - 2*REGION_MINUS) ;
	change_region_roi_v2(0,149,0,99) ;
	auo3x4_empty_trig();
	auo3x4_trigger_tcon() ;
	auo3x4_wait_tcon();

	int**top = auo3x4_get_image(full_width, half_height) ;

	auo3x4_set_expotime_ms(g_auo_EXPTIME - 3*REGION_MINUS) ;
	change_region_roi_v2(0,149,100-overlap_val,199) ;
	auo3x4_empty_trig();
	auo3x4_trigger_tcon() ;
	auo3x4_wait_tcon();

	int** bottom = auo3x4_get_image(full_width, half_height+overlap_pixel) ;

	for ( int h = 0 ; h < half_height ; h++ ) {
		for ( int w = 0 ; w < full_width ; w++ ) {
			full[h][w] = top[h][w];
			full[h+half_height][w] = bottom[h+overlap_pixel][w] ;
		}
	}

	// overlap 16 pixel, move 8 pixel temp to top test
	int t ;
	for ( int h = overlap_pixel/2; h < overlap_pixel ; h++ ) {
		for (int w = 0; w < full_width; w++) {
			t = full[half_height - overlap_pixel + h][w];
			full[half_height - overlap_pixel + h][w] = bottom[h][w];
		}
	}

	et760_2d_free(top, half_height) ;
	et760_2d_free(bottom, half_height+overlap_pixel) ;
}

void auo3x4_get_full_hori_overlap_avg(int ** full) {
	egislog_i("Get Full Area Image by hori split2") ;

	int full_width = CELL_WIDTH * CELL_COL_LENGTH ;
	int full_height = CELL_HEIGHT * CELL_ROW_LENGTH ; ;
	int half_height = full_height / 2 ;

	int overlap_pixel = 32 ;
	int overlap_val = overlap_pixel / 4 ;


	auo3x4_set_expotime_ms(g_auo_EXPTIME - 2*REGION_MINUS) ;
	change_region_roi_v2(0,149,0,99) ;
	auo3x4_empty_trig();
	auo3x4_trigger_tcon() ;
	auo3x4_wait_tcon();

	int**top = auo3x4_get_image(full_width, half_height) ;

	auo3x4_set_expotime_ms(g_auo_EXPTIME - 3*REGION_MINUS) ;
	change_region_roi_v2(0,149,100-overlap_val,199) ;
	auo3x4_empty_trig();
	auo3x4_trigger_tcon() ;
	auo3x4_wait_tcon();

	int** bottom = auo3x4_get_image(full_width, half_height+overlap_pixel) ;

	for ( int h = 0 ; h < half_height ; h++ ) {
		for ( int w = 0 ; w < full_width ; w++ ) {
			full[h][w] = top[h][w];
			full[h+half_height][w] = bottom[h+overlap_pixel][w] ;
		}
	}

	// overlap 16 pixel, move 8 pixel temp to top test
	int t , b,  hh, half ;
	half = overlap_pixel/2;
	for ( int h = overlap_pixel/2; h < overlap_pixel ; h++ ) {
		for ( int w = 0 ; w < full_width ; w++ ) {
			t = full[half_height-overlap_pixel+h][w] ;
			hh = h - half ; // 0 ~ 16
			t = t * (half-hh) / half ;
			b = bottom[h][w] ;
			b = b * (hh) / half ;
			full[half_height-overlap_pixel+h][w] = t+b;
		}
	}

	et760_2d_free(top, half_height) ;
	et760_2d_free(bottom, half_height+overlap_pixel) ;
}

void auo3x4_get_full_vert(int ** full) {
	egislog_i("Get Full Area Image by vert split3") ;

	int full_width = CELL_WIDTH * CELL_COL_LENGTH ;
	int full_height = CELL_HEIGHT * CELL_ROW_LENGTH ; ;
	int part_width = full_width / 3 ;

    auo3x4_set_expotime_ms(g_auo_EXPTIME - 3*REGION_MINUS) ;

	change_region_roi_v2(0,49,0,199) ;
	auo3x4_empty_trig();
	auo3x4_trigger_tcon() ;
	auo3x4_wait_tcon();

	int**v1 = auo3x4_get_image(part_width, full_height) ;

	change_region_roi_v2(50,99,0,199) ;
	auo3x4_empty_trig();
	auo3x4_trigger_tcon() ;
	auo3x4_wait_tcon();

	int** v2 = auo3x4_get_image(part_width, full_height) ;

	change_region_roi_v2(100,149,0,199) ;
	auo3x4_empty_trig();
	auo3x4_trigger_tcon() ;
	auo3x4_wait_tcon();

	int** v3 = auo3x4_get_image(part_width, full_height) ;

	for ( int h = 0 ; h < full_height ; h++ ) {
		for ( int w = 0 ; w < part_width ; w++ ) {
			full[h][w] = v1[h][w];
			full[h][w+part_width] = v2[h][w] ;
			full[h][w+part_width+part_width] = v3[h][w] ;
		}
	}

	et760_2d_free(v1, full_height) ;
	et760_2d_free(v2, full_height) ;
	et760_2d_free(v3, full_height) ;
}

void auo3x4_get_full(int ** full) {

	// select best way to get full image
	//auo3x4_get_full_hori(full) ;
	auo3x4_get_full_vert(full) ;
}


/**
 * 只取單區200X200 image
 * @return
 */
int ** auo3x4_get_image_single_area() {

	int ** pixel2D ;
	pixel2D = plat_alloc(CELL_HEIGHT * sizeof(int *));
	for (int i = 0; i < CELL_HEIGHT; i++) {
		pixel2D[i] = plat_alloc(CELL_WIDTH * sizeof(int));
	}

	int bufferLen = SPI_MAX_LENGTH * ((CELL_HEIGHT * CELL_HEIGHT*2)/SPI_MAX_LENGTH +1); // 固定每次16384
	uint8_t *buffer = plat_alloc(bufferLen) ;
	uint8_t *line = plat_alloc(SPI_MAX_LENGTH) ;

	int currentAddr = image_addr ;

//	int bufferLen = 16384 * ((CELL_WIDTH * CELL_HEIGHT*2)/16384 +1); // 固定每次16384
//	byte[] buffer = new byte[bufferLen] ;
//	byte[] line = new byte[16384] ;

	int hasMod = bufferLen % SPI_MAX_LENGTH ;
	int read16kTimes = bufferLen / SPI_MAX_LENGTH ;

	egislog_i("read 16k need: %d times", read16kTimes) ;
	for ( int i = 0 ; i < read16kTimes ; i++ ) {
		int sum = 0 ;

		et760_set_spi_base_address( currentAddr );
		//SPIUtility.readBurst((byte) 0,line, line.length) ;
		et760_burst_read(line, SPI_MAX_LENGTH) ;

		for (int x = 0 ; x < SPI_MAX_LENGTH ; x++ ) {
			sum = sum + line[x] ;
			buffer[x+i*SPI_MAX_LENGTH] = line[x] ;
		}
		currentAddr = currentAddr + SPI_MAX_LENGTH ;
	}


	int *pixel1D = plat_alloc(sizeof(int) * CELL_HEIGHT * CELL_WIDTH) ;

	int sum = 0 ;
	for ( int i = 0 ; i < CELL_HEIGHT*CELL_WIDTH*2; i+=2 ) {
		int t =  (0xFF & buffer[i]) + ( (0xFF & buffer[i+1]) << 8)  ;
		pixel1D[i/2] = t ;
		sum = sum + t ;
	}
	for ( int h= 0 ; h < CELL_HEIGHT ; h++ ) {
		for ( int w = 0 ; w < CELL_WIDTH ; w++ ) {
			pixel2D[h][w] = pixel1D[h*CELL_WIDTH + w] ;
		}
	}

	plat_free(buffer) ;
	plat_free(line) ;
	plat_free(pixel1D) ;

	return pixel2D ;
}


/**
 * @return
 */
int ** auo3x4_get_image(int width, int height) {

	int ** pixel2D ;
	pixel2D = plat_alloc(height * sizeof(int *));
	for (int i = 0; i < height; i++) {
		pixel2D[i] = plat_alloc(width * sizeof(int));
	}

	int bufferLen = SPI_MAX_LENGTH * ((height * width*2)/SPI_MAX_LENGTH +1); // 固定每次16384
	uint8_t *buffer = plat_alloc(bufferLen) ;
	uint8_t *line = plat_alloc(SPI_MAX_LENGTH) ;

	int currentAddr = image_addr ;

	int hasMod = bufferLen % SPI_MAX_LENGTH ;
	int read16kTimes = bufferLen / SPI_MAX_LENGTH ;

	egislog_i("read 16k need: %d times, bufferLen=%d", read16kTimes, bufferLen) ;
	for ( int i = 0 ; i < read16kTimes ; i++ ) {
		et760_set_spi_base_address( currentAddr );
		et760_burst_read(line, SPI_MAX_LENGTH) ;

		for (int x = 0 ; x < SPI_MAX_LENGTH ; x++ ) {
			buffer[x+i*SPI_MAX_LENGTH] = line[x] ;
		}
		currentAddr = currentAddr + SPI_MAX_LENGTH ;
	}

	int *pixel1D = plat_alloc(sizeof(int) * height * width) ;

	int sum = 0 ;
	for ( int i = 0 ; i < height*width*2; i+=2 ) {
		int t =  (0xFF & buffer[i]) + ( (0xFF & buffer[i+1]) << 8)  ;
		pixel1D[i/2] = t ;
		sum = sum + t ;
	}

	for ( int h= 0 ; h < height ; h++ ) {
		for ( int w = 0 ; w < width ; w++ ) {
			pixel2D[h][w] = pixel1D[h*width + w] ;
		}
	}

	plat_free(buffer) ;
	plat_free(line) ;
	plat_free(pixel1D) ;

	return pixel2D ;
}

/**
 * calc auo y region num:
 * auo 3x4 full area only
 * @param y
 * @param height
 * @return region count
 */
int y_cross_region_num(int y, int height) {
	int start_region = y / CELL_HEIGHT ; // 0~3
	int end_region = (y+height-1) / CELL_HEIGHT ; // 0~3

	return end_region - start_region + 1;
}


/**
 * get x cross zsw number
 * @param x
 * @param width
 * @return region count
 */
int x_cross_region_num(int x, int width) {
	int start_region = x / CELL_WIDTH ;
	int end_region = (x+width-1) / CELL_WIDTH ;

	return end_region - start_region + 1 ;
}


/**
 * is region number in
 * @param region 0 1 2
 * @return cross zsw count
 */
int is_in_x_region(int x, int width, int region) {
	int start_region = x / CELL_WIDTH ;
	int end_region = (x+width-1) / CELL_WIDTH ;
	if ( (region >= start_region) && (region <= end_region)) {
		return 1;
	}
	else {
		return 0 ;
	}

}


/**
 * return how many time trigger will done
 * @return 1: just 1 trigger, 2: 2 or more
 */
int calc_limit(){
	int y_pixels = (g_y_stv_end - g_y_stv_start + 1 ) * 4 ;
	int x_region_cross_cnt = x_cross_region_num(g_roi_x, g_roi_w) ;
	int x_pixels = x_region_cross_cnt * 200 ;
	int total_pixel = y_pixels * x_pixels ;

	int safe_threshold = CELL_HEIGHT * CELL_WIDTH * 6 ;
	if ( total_pixel > safe_threshold ) {
		return 2 ;
	}
	else if ( total_pixel == safe_threshold ) {
		if ( (g_x_zsw_start*4) % 200 == 0) {
			return 1 ;  // only fit zsw can trigger once
		}
		return 2 ;
	}
	else {
		return 1 ;
	}
}
