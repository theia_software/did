#include <math.h>
#include <stdbool.h>

#include "egis_definition.h"
#include "fp_err.h"
#include "plat_log.h"
#include "plat_mem.h"
#include "plat_spi.h"
#include "plat_time.h"
#include "et760_sensor_control_common.h"
#include "type_definition.h"

#ifdef __OTG_SENSOR__
#include "plat_otg.h"
#endif

#define LOG_TAG "RBS-Sensor"
#include "et760_sensor_control.h"


/**
 * Note:
 * force enable 32bit spi
 * @return
 */
void force_enable_ba_spi() {

	et760_32bit_write(0x20000028, 0x21) ;
	et760_32bit_write(0x60000028, 0x21) ;
	et760_32bit_write(0x2000002c, 0x1) ;
	et760_32bit_write(0x6000002c, 0x1) ;

	et760_32bit_write(0x100000D4, 0x00010101);
}



/**
 * Note: temp add for IC Clock calibration
 * only work in MODIFYD OTG V9
 * @param speed 1~20, speed in MHZ
 * @return
 */
void control_spi_speed(int speed) {
	egislog_i("change spi speed to %d", speed) ;

	// make a UPPER CASE Text "SPEED" and check in SPI
	uint8_t b[10] ;
	b[0] = 'S' ;
	b[1] = 'P' ;
	b[2] = 'E' ;
	b[3] = 'E' ;
	b[4] = 'D' ;
	b[5] = 0 ;
	b[6] = 0 ;
	b[7] = 0 ;
	b[9] = 0 ;
	b[9] = (speed & 0xFF);

	et760_dispatch_raw_spi(ET760_RAW_SPI, b, 10);
}


int chip_speed_flow() {
	control_spi_speed(4) ;
	float default_speed = get_chip_speed() ;
	egislog_i("CHIP Default Speed= %f", default_speed) ;

	int ret = calibrateSysClock() ;
	control_spi_speed(19) ;

	int e8 = et760_32bit_read_value(0x100000e8) ;
	egislog_i("calibration result=%d, 0x100000e8= %d", ret, e8) ;
	return ret ;
}


int calibrateSysClock(){
	int target_Frq = 100 ;
	float counter = get_chip_speed() ;
	float FREQ_add = (int)(fabs(round((target_Frq - counter)/2))) ;
	int adjust = 0 ;
	float counter_new = counter ;
	if (counter < (target_Frq-1) || counter > (target_Frq + 4)){
		egislog_d("calibrateSysClock counter: %f", counter) ;
		if (counter < 85.0) {
			egislog_d("chip system clock too slow") ;
		}
		else if (counter > 140.0) {
			egislog_d("chip system clock too fast") ;
		}

		if (counter > (target_Frq + 4) && counter <= 140){
			for ( int i = 0 ; i < FREQ_add ; i++ ) {
				adjust = 162 - i ;
				int rx_data = et760_32bit_read_value(0x100000e8) ;
				int set_val = (rx_data & 0xff00ffff) | (adjust << 16) ;
				et760_32bit_write(0x100000e8, set_val);
			}
		}
		else if (counter >= 85 && counter <= (target_Frq + 4)){
			for ( int i = 0 ; i < (FREQ_add); i++ ) {
				adjust = 162 + i;
				int rx_data = et760_32bit_read_value(0x100000e8) ;
				int set_val = (rx_data & 0xff00ffff) | (adjust << 16) ;
				et760_32bit_write(0x100000e8, set_val);
			}
		}
		plat_sleep_time(10) ;

		float temp = get_chip_speed() ;
		while (temp < target_Frq) {
			adjust = adjust + 1 ;
			int rx_data = et760_32bit_read_value(0x100000e8);
			int set_val = (rx_data & 0xff00ffff) | (adjust << 16) ;
			et760_32bit_write(0x100000e8, set_val);
			plat_sleep_time(10); //  # <= slowly change

			if (adjust > 207) {
				egislog_e("chip system clock calibration failed") ;
				return RESULT_FAILED;
			}

			counter_new = get_chip_speed() ;
			if ((fabsf(target_Frq - temp)+1) <= fabsf(counter_new-target_Frq)){
				rx_data = et760_32bit_read_value(0x100000e8) ;
				set_val = (rx_data & 0xff00ffff) | (adjust << 16) ;
				et760_32bit_write(0x100000e8, set_val) ;
				plat_sleep_time(10);
				break;
			}

			if (fabsf(target_Frq - temp) > fabsf(counter_new - target_Frq)) {
				break;
			}
		}
	}

	egislog_i("calibrateSysClock counter: %f", counter_new) ;
	return RESULT_OK ;
}
float get_chip_speed() {

	int SYS_FREQ_METER_INPUT_SEL = 0x1000001c ;
	int SPI_CHANNEL = 0 ;
	int baseSpeed = 0 ;
	int SYS_FREQ_METER_CYCLE = 0x10000020 ;
	int SYS_FREQ_METER_COUNTER_DONE = 0x10000024 ;
	int SYS_FREQ_METER_TRIG = 0x10000028 ;
	int SET_SYS_FREQ_METER_TRIG = 1 ;
	int SYS_FREQ_METER_COUNTER_H = 0x1000002c ;
	float correctionVar = 0 ;

	baseSpeed = 4000000 ;
	correctionVar = 2.5f ;

	SPI_CHANNEL = 2 ;

	int commSpeed = 4000000 ; // self._comm.speed
	float M = (float) (commSpeed/baseSpeed) ;
	int freq_meter_cycle = (int) (1024*M) ;
	et760_32bit_write(SYS_FREQ_METER_INPUT_SEL, SPI_CHANNEL);
	et760_32bit_write(SYS_FREQ_METER_CYCLE, freq_meter_cycle) ;
	et760_32bit_write(SYS_FREQ_METER_TRIG, SET_SYS_FREQ_METER_TRIG) ;
	int retryCount = 0 ;
	while (et760_32bit_read_value(SYS_FREQ_METER_COUNTER_DONE) != 1) {
		plat_sleep_time(10) ;
		retryCount += 1;
		if (retryCount > 30) {
			egislog_e("system clock not responding");
			break;
		}
	}
	float count_h = (et760_32bit_read_value(SYS_FREQ_METER_COUNTER_H))/M ;
	float res = (((commSpeed / 1000000 * count_h) * 2) /  freq_meter_cycle) - correctionVar ;
	return res;
}


/**
 * ET760 32bit read
 * @param addr
 * @param value ref to value
 * @return
 */
int et760_32bit_read(int addr, int *value) {
	uint8_t b[10] ;
	int p ;
	int len = 10 ;
	
	memset(b,0,10);

	b[0] = 0x32 ;
	b[1] = (addr & 0xFF000000) >> 24 ;
	b[2] = (addr & 0x00FF0000) >> 16 ;
	b[3] = (addr & 0x0000FF00) >> 8 ;
	b[4] = (addr & 0x000000FF) ;
	b[5] = 0 ;


	et760_dispatch_raw_spi(ET760_RAW_SPI, b, len);
	*value = (b[6] << 24) | (b[7] << 16) | (b[8] << 8) | (b[9] ) ;
	
	return RESULT_OK ;
}

/**
 * Simple warpper
 * @param addr
 * @return
 */
int et760_32bit_read_value(int addr) {
	int value;
	et760_32bit_read(addr, &value) ;
	return value ;
}

/**
 * ET760 32bit write
 * @param addr
 * @param value
 * @return
 */
int et760_32bit_write(int addr, int value) {

	uint8_t b[9] ;
	b[0] = 0x33 ;
	b[1] = (addr & 0xFF000000) >> 24 ;
	b[2] = (addr & 0x00FF0000) >> 16 ;
	b[3] = (addr & 0x0000FF00) >> 8 ;
	b[4] = (addr & 0x000000FF) ;

	b[5] = (value & 0xFF000000) >> 24 ;
	b[6] = (value & 0x00FF0000) >> 16 ;
	b[7] = (value & 0x0000FF00) >> 8 ;
	b[8] = (value & 0x000000FF) ;

	//檢查這個有沒有沒寫入 QQ
	et760_dispatch_raw_spi(ET760_RAW_SPI, b, 9);
	return RESULT_OK ;
}


/**
 * ET760 opcode
 * @param opcode 只有第一個BYTE有義意的OPCODE
 * @return
 */
int et760_opcode(int opcode) {
	uint8_t b[8] ;
	b[0] = opcode ;
	b[1] = 0 ;
	b[2] = 0 ;
	b[3] = 0 ;
	b[4] = 0 ;
	b[5] = 0 ;
	b[6] = 0 ;
	b[7] = 0 ;

	et760_dispatch_raw_spi(ET760_RAW_SPI, b, 8);
	return RESULT_OK ;
}


/**
 * ET760 opcode4
 * @param opcode 
 * @param value opcode 中第四個
 * @return
 */
int et760_opcode4(int opcode, int value) {
	uint8_t b[8] ;
	b[0] = opcode ;
	b[1] = 0 ;
	b[2] = 0 ;
	b[3] = 0 ;
	b[4] = (0xFF & value);
	b[5] = 0 ;
	b[6] = 0 ;
	b[7] = 0 ;

	et760_dispatch_raw_spi(ET760_RAW_SPI, b, 8);
	return RESULT_OK ;
}

/**
 * ET760 opcode
 * @param opcode 有多個BYTE有意義的OPCODE
 * @param b3 third byte
 * @value value byte4-7
 * @return
 */
int et760_opcode_b3_value(int opcode, uint8_t b3, int value ) {
	uint8_t b[8] ;
	b[0] = opcode ;
	b[1] = 0 ;
	b[2] = 0 ;
	b[3] = b3 ;
	b[4] = (value & 0xFF000000) >> 24 ;
	b[5] = (value & 0x00FF0000) >> 16 ;
	b[6] = (value & 0x0000FF00) >> 8 ;
	b[7] = (value & 0x000000FF) ;

	et760_dispatch_raw_spi(ET760_RAW_SPI, b, 8);
	return RESULT_OK ;
}


/**
 * Opcode with Two Value (two 16 bit)
 * @param opcode
 * @param v1
 * @param v2
 * @return
 */
int et760_opcode_v1v2(int opcode, int v1, int v2) {
	uint8_t b[8] ;
	b[0] = opcode;
	b[1] = 0 ;
	b[2] = 0 ;
	b[3] = 0 ;

	b[4] = (v1 >> 8 & 0xFF) ;
	b[5] = (v1      & 0xFF) ;
	b[6] = (v2 >> 8 & 0xFF) ;
	b[7] = (v2      & 0xFF) ;

	et760_dispatch_raw_spi(ET760_RAW_SPI, b, 8);
	return RESULT_OK ;
}

/**
 * ET760 burst read
 * 用SPI寫出BUFFER的資料，組圖要在外面
 * 轉換會有資料上限，目前尚未處理此功能 故 LENGHT < 16K
 */
int et760_burst_read(uint8_t* buffer, int length){

	if ( length > SPI_MAX_LENGTH || (length % 512 != 0)) {
		egislog_e("Error OTG-SPI can only burst/write 16kb, and length must divided by 512") ;
	}
	if ( (length % 2) == 1) {
		// stm32 f7 OTG length must odd number.
		egislog_e("Error stm32f7 OTG-SPI length must odd number") ;
	}

	int add_length = 3;

	uint8_t* spi_buffer = (uint8_t*)plat_alloc(length + add_length);
	spi_buffer[0] = 0x22 ;
	spi_buffer[1] = 0 ; // start addr always zero
	spi_buffer[2] = 0 ; // dummy byte always 2

	et760_dispatch_raw_spi(ET760_RAW_SPI, spi_buffer, length + add_length);
	memcpy(buffer, &spi_buffer[add_length], length) ;
	plat_free(spi_buffer);
	return RESULT_OK ;
}

/**
 * ET760 burst write
 * 用SPI寫出BUFFER的資料，組圖要在外面
 *
 * 轉換會有資料上限，目前尚未處理此功能 故 LENGHT < 16K
 */
int et760_burst_write(uint8_t* buffer, int length){

	if ( length > SPI_MAX_LENGTH || (length % 512 != 0)) {
		egislog_e("Error OTG-SPI can only burst/write 16kb, and length must divided by 512") ;
	}
	uint8_t* spi_buffer = (uint8_t*)plat_alloc(length+2);
	memcpy( &spi_buffer[2], buffer, length);
	spi_buffer[0] = 0x26 ;
	spi_buffer[1] = 0 ; // start addr always zero
	et760_dispatch_raw_spi(ET760_RAW_SPI, spi_buffer, length + 2);
	plat_free(spi_buffer);
	return RESULT_OK ;
}


int et760_set_spi_default_address() {
	et760_32bit_write(ET760_BASEADDR_SPI0, 0) ;
	et760_32bit_write(ET760_BASEADDR_SPI1, 0) ;
	return EGIS_OK;
}

int et760_set_spi_base_address(int addr) {
	et760_32bit_write(ET760_BASEADDR_SPI0, addr) ;
	et760_32bit_write(ET760_BASEADDR_SPI1, addr) ;
	return EGIS_OK;
}

int et760_dispatch_raw_spi(int param, uint8_t *buffer, int length)
{
	// 這裡可設定要輸出到linux spi driver
	// 或是 USB-OTG
	// USB OTG:
	return io_760_dispatch_raw_spi(param, buffer, length);
}



/**
 * 設定所有的 ADC DAC 值 FOR 所有的CH
 * @param adc_dac
 * @return
 */
int et760_set_all_adc_dac(int adc_dac)
{
	egislog_i("set all adc_dac %d", adc_dac);

	// use opcode to change all dac (must be burn firmware first)
	et760_opcode4(0xB0, adc_dac) ;
	return RESULT_OK;
}


void et760_set_gain(int gain_value ) {
    int ret =et760_32bit_read_value(0x10000108) ;
    ret = ret & 0xFFF0F0FF ;
    ret = ret | (gain_value << 8) | (gain_value << 16) ;
    et760_32bit_write(0x10000108, ret) ;
}

/**
 * TODO: 燒韌體
 * @param firmware
 * @return
 */
int et760_burn_bin_code(uint8_t *buf, int buf_size) {
	int errorLimit = 20 ;
	int buf512len = 512* (( buf_size / 512 ) + 1 ) ; // Burst Write大小必需為512的倍數
	int isSuccess = 0 ;

	egislog_i("Write FIRMWARE, convert .bin into .h file!") ;

	et760_32bit_write(ET760_SYSTEM_RESET_RISC_V, 0x00010000);
	et760_set_spi_default_address() ;

	// et760_32bit_write(ET760_CHECK_INIT_ADDRESS, 0);

	uint8_t *buf512 = plat_alloc(buf512len) ;
	uint8_t *readback = plat_alloc(buf512len) ;

	while ( isSuccess == 0 ) {
		memset(buf512, 0, buf512len) ;
		memcpy(buf512, buf, buf_size) ;
		et760_burst_write(buf512, buf512len) ;
		et760_burst_read(readback, buf512len) ; //測試燒進去的有沒有正常
		isSuccess = 1 ;
		for ( int i = 0 ; i < buf_size ; i++ ) {
			if ( readback[i] != buf512[i]) {
				isSuccess = 0 ;
				egislog_e("Firmware Write Error at byte %d! Retry Limit=%d", i, errorLimit) ;
				errorLimit -- ;
				break ;
			}
		}
		if ( errorLimit <= 0 ) {
			egislog_e("Firmware Write FAIL retry Limit = 0!!") ;
			break ;
		}
	}

	force_enable_ba_spi();

	et760_set_spi_default_address() ;
	et760_32bit_write(ET760_SYSTEM_RISCV_RESET_VECTOR, 0x00000000);
	et760_32bit_write(ET760_SYSTEM_RESET_RISC_V, 0x00000000);

	plat_sleep_time(10) ;

	PLAT_FREE(buf512) ;
	PLAT_FREE(readback) ;
    return EGIS_OK;
}



void getPartArray(int **dest, int** src, int start_x, int start_y, int width, int height) {

//int [][] back = new int[height][width] ;
    int t ;
    int base_x = start_x ;
    int base_y = start_y ;
    for ( int y = 0 ; y < height ; y++ ) {
        for ( int x = 0 ; x < width ; x++ ) {
            t = src[base_y + y][base_x + x] ;
            dest[y][x] = t ;
        }
    }
}

void putPartArray(int** dest, int** src, int start_x, int start_y, int src_width, int src_height) {
    for ( int h = 0 ; h < src_height ; h++ ) {
        for ( int w = 0 ; w < src_width ; w++ ) {
            dest[start_y+h][start_x+w] = src[h][w] ;
        }
    }
}

void rotate180(int** src, int width, int height) {
	int cnt = 0 ;
    int stop = width*height/2 ;
    int t ;
    for ( int h = 0 ; h < height ; h++ ) {
        for ( int w = 0 ; w < width ; w++ ) {
            t = src[h][w] ;
            src[h][w] = src[height-h-1][width-w-1] ;
            src[height-h-1][width-w-1] = t;
            cnt++ ;
            if (cnt==stop){
            	return ;
            }
        }
    }
}




int** et760_2d_alloc(int width, int height) {
	int ** pixel2D ;
	pixel2D = plat_alloc(height * sizeof(int *));
	for (int i = 0; i < height; i++) {
		pixel2D[i] = plat_alloc(width * sizeof(int));
	}
	return pixel2D ;
}

void et760_2d_free(int **array, int height) {
	for (int i = 0; i < height; i++) {
		plat_free(array[i]);
	}
	plat_free(array);
}


/*
 * 以下為 USB-OTG專用，之後可能不會使用它
 */

int et760_dispatch_get_id(uint8_t *dev_id0, uint8_t *dev_id1)
{
	return io_760_dispatch_get_id(dev_id0, dev_id1);
}
int et760_dispatch_init_sensor()
{
	return io_760_dispatch_init_sensor();
}
int et760_dispatch_set_sensor_param(int param, int value)
{
	return io_760_dispatch_set_sensor_param(param, value);
}
int et760_dispatch_get_sensor_param(int param, int *value)
{
	return io_760_dispatch_get_sensor_param(param, value);
}
int et760_dispatch_get_frame(uint8_t *buffer, int length)
{
	return io_760_dispatch_get_frame(buffer, length);
}

