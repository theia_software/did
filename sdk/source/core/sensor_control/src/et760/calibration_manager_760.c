#include <assert.h>
#include <math.h>
#include <stdbool.h>
#include <stdio.h>
#include "InlineLib.h"
#include "core_config.h"
#include "egis_definition.h"
#include "egis_sprintf.h"
#include "et760_sensor_control.h"
#include "fp_err.h"
#include "ini_definition.h"
#include "isensor_definition.h"
#include "plat_file.h"
#include "plat_log.h"
#include "plat_mem.h"
#include "plat_time.h"
#ifdef EEPROM_USE
#include "plat_eeprom.h"
#include "plat_spi.h"
#endif
#ifdef G3_MATCHER
#include "ImageProcessingLib.h"
#elif defined(ALGO_GEN_4)
#include "faceplusutil.h"
#include "work_with_G2/ImageProcessingLib.h"
#elif defined(ALGO_GEN_5)
#include "work_with_G2/ImageProcessingLib.h"
#elif defined(ALGO_GEN_7)
#include "work_with_G2/ImageProcessingLib.h"
#include "work_with_G7/GoImageProcessingLib.h"
#else
#include "EgisAlgorithmAPI.h"
#endif

#include "calibration_manager_760.h"
#include "image_cut.h"
#if !defined(_WINDOWS) && !defined(TZ_MODE)
#include <unistd.h>
#endif
#include <limits.h>
#include <auo_oled_panel.h>
#include "sensor_control_common.h"

static char g_calib_folder_path[PATH_MAX] = {"/sdcard/RbsG5Temp/"};
static char g_calib_file_path[PATH_MAX] = {0};
static int g_calib_bkg_type = CALIB_BKG_BK;
#define FILENAME_CALIBRATION "calibration.bin"
#define FILENAME_BK_CALI_IMG "Bkg.bin"
#define FILENAME_WK_CALI_IMG "Wkg.bin"
#define CALIBRATION_VERSION (10)  // increase version when modify CALIBRATION_DATA
#define IMG_NUM 10
//#define IMG_NUM 1

#define LOG_TAG "RBS-Sensor"

typedef struct {
    int32_t version;
    CALIBRATE_INFO infos[EXP_SPECIES][HW_INT_SPECIES];
} CALIBRATION_DATA;

static CALIBRATION_DATA g_calibration = {0x0};
static BOOL g_calibration_to_be_saved = FALSE;
extern float g_exp_time;
extern int g_all_adc_dac;
extern int g_hw_integrate_count;
extern BOOL g_gain2_enable;
static int g_is_need_renew_calibration_data = FALSE;
typedef struct calib_listener_item_t {
    fn_on_calib_data_update update_listener;
    struct calib_listener_item_t* prev;
    struct calib_listener_item_t* next;
} CALIB_LISTENER_ITEM;

static CALIB_LISTENER_ITEM* g_calib_listener_head;
static CALIB_LISTENER_ITEM* g_calib_listener_tail;

static CALIBRATE_INFO* calibration_get(void) {
    if (g_calibration.version == CALIBRATION_VERSION) {
        return &g_calibration.infos[0][0];
    } else {
        egislog_e("%s found no calibration data", __func__);
        return NULL;
    }
}

static CALIBRATE_INFO* calibration_get2(void) {
    if (g_calibration.version == CALIBRATION_VERSION) {
        return &g_calibration.infos[1][0];
    } else {
        egislog_e("%s found no calibration data", __func__);
        return NULL;
    }
}

int calibration_set_calib_update_fn(fn_on_calib_data_update calib_data_update_listener) {
    RBS_CHECK_IF_NULL(calib_data_update_listener, EGIS_INCORRECT_PARAMETER);
    CALIB_LISTENER_ITEM* listener_item_temp;
    listener_item_temp = plat_alloc(sizeof(CALIB_LISTENER_ITEM));
    RBS_CHECK_IF_NULL(listener_item_temp, EGIS_OUT_OF_MEMORY);
    listener_item_temp->update_listener = calib_data_update_listener;
    listener_item_temp->next = NULL;
    if (g_calib_listener_head == NULL) {
        listener_item_temp->prev = NULL;
        g_calib_listener_head = listener_item_temp;
    } else {
        listener_item_temp->prev = g_calib_listener_tail;
        g_calib_listener_tail->next = listener_item_temp;
    }
    g_calib_listener_tail = listener_item_temp;
    return EGIS_OK;
}

void calibration_remove_calib_update_fn(fn_on_calib_data_update calib_data_update_listener) {
    if (g_calib_listener_head == NULL) {
        return;
    }
    CALIB_LISTENER_ITEM* listener_item = g_calib_listener_head;
    CALIB_LISTENER_ITEM* listener_item_temp = NULL;
    CALIB_LISTENER_ITEM* listener_item_temp_prev = NULL;
    while (listener_item != NULL) {
        if (listener_item->update_listener == calib_data_update_listener) {
            listener_item_temp = listener_item;
            listener_item_temp_prev = listener_item->prev;
            listener_item = listener_item->next;
            if (listener_item != NULL) {
                listener_item->prev = listener_item_temp_prev;
            } else {
                g_calib_listener_tail = listener_item_temp_prev;
            }
            if (listener_item_temp_prev != NULL) {
                listener_item_temp_prev->next = listener_item;
            } else {
                g_calib_listener_head = NULL;
            }
            PLAT_FREE(listener_item_temp);
        } else {
            listener_item = listener_item->next;
        }
    }
}

static void notify_calib_data_update() {
    CALIB_LISTENER_ITEM* listener_item = g_calib_listener_head;
    fn_on_calib_data_update listener = NULL;
    CALIBRATE_INFO* info = &g_calibration.infos[0][0];
    while (listener_item != NULL) {
        listener = listener_item->update_listener;
        if (listener != NULL) {
            listener(info->data, calibration_get_wkg(), info->hw_integrate_count, info->exp_time);
        }
        listener_item = listener_item->next;
    }
}

static void notify_calib_data_removed() {
    CALIB_LISTENER_ITEM* listener_item = g_calib_listener_head;
    fn_on_calib_data_update listener = NULL;
    while (listener_item != NULL) {
        listener = listener_item->update_listener;
        if (listener != NULL) {
            listener(NULL, NULL, 0, 0);
        }
        listener_item = listener_item->next;
    }
}

void calibration_set_folder_path(const char* folder_path) {
    if (folder_path != NULL) {
        egist_snprintf(g_calib_folder_path, PATH_MAX, "%s", folder_path);
    }
    return;
}

int calibration_load(void) {
    int retval = EGIS_OK, ret_size;
    uint32_t realsize;
    egislog_d("%s, g_calib_folder_path %s", __func__, g_calib_folder_path);

    egist_snprintf(g_calib_file_path, PATH_MAX, "%s/%s", g_calib_folder_path, FILENAME_CALIBRATION);
    ret_size = plat_load_file(g_calib_file_path, (uint8_t*)&g_calibration, sizeof(CALIBRATION_DATA),
                              &realsize);

    if (ret_size < 0) {
        egislog_e("calibration_load error, path = %s", g_calib_file_path);
#ifdef EEPROM_USE
        calibration_load_from_flash();
        g_is_need_renew_calibration_data = TRUE;
#else
        egislog_e("NOT SUPPORT calibration_load");
        return RESULT_FAILED;
#endif
    }

    egislog_i("the calibration version is %d (%d)", g_calibration.version, ret_size);
    if (g_calibration.version != CALIBRATION_VERSION) {
        egislog_e("the calibration data is deprecated. Current version is %d", CALIBRATION_VERSION);
        calibration_remove();
        return RESULT_FAILED;
    }

    if (g_is_need_renew_calibration_data) {
        g_is_need_renew_calibration_data = FALSE;
        ret_size =
            plat_save_file(g_calib_file_path, (uint8_t*)&g_calibration, sizeof(CALIBRATION_DATA));
        egislog_d("plat_save_file ret_size=%d", ret_size);
        if (ret_size <= 0) {
            egislog_e("calibration_save error, path = %s", g_calib_file_path);
        }
    }
    CALIBRATE_INFO* cali_info = calibration_get();

    // get setting from ini first
    g_exp_time = cali_info->exp_time;
    g_hw_integrate_count = cali_info->hw_integrate_count;
    g_all_adc_dac = cali_info->adc_dac;
    g_exp_time = core_config_get_double(INI_SECTION_SENSOR, KEY_EXPOSURE_TIME, g_exp_time);  // ms
    g_hw_integrate_count =
        core_config_get_int(INI_SECTION_SENSOR, KEY_INTEGRATE_COUNT_HW, g_hw_integrate_count);
    g_all_adc_dac = core_config_get_int(INI_SECTION_SENSOR, KEY_ET760_ALL_ADC_DAC, g_all_adc_dac);
    g_auo_VGH_R = core_config_get_int(INI_SECTION_SENSOR, KEY_ET760_VGH_R, g_auo_VGH_R);
    g_auo_VGL_R = core_config_get_int(INI_SECTION_SENSOR, KEY_ET760_VGL_R, g_auo_VGL_R);
    g_auo_VGH_W = core_config_get_int(INI_SECTION_SENSOR, KEY_ET760_VGH_W, g_auo_VGH_W);
    g_auo_VGL_W = core_config_get_int(INI_SECTION_SENSOR, KEY_ET760_VGL_W, g_auo_VGL_W);
    g_auo_SVDD = core_config_get_int(INI_SECTION_SENSOR, KEY_ET760_SVDD, g_auo_SVDD);
    ex_log(LOG_DEBUG, "calibration_load g_all_adc_dac %d g_exp_time_x10 = %d", g_all_adc_dac, (int)(g_exp_time * 10));
    if (cali_info == NULL) {
        egislog_e("calibration_get fail");
        calibration_remove();
        return RESULT_FAILED;
    }
    notify_calib_data_update();
    egislog_i("calibration is v%d, %d,%d xy=(%d,%d)", g_calibration.version, (int)(g_exp_time * 10),
              g_hw_integrate_count, cali_info->bkg_cx, cali_info->bkg_cy);
    return retval;
}

#ifndef TZ_MODE
static void genarate_background_file_path(char* result) {
    int exposure_time_x1000 = (int)(g_exp_time * 1000.0 + 0.5);
    int temperature = 0;
    isensor_get_int(PARAM_INT_TEMPERATURE, &temperature);
    sprintf(result, "%s%d_%d_%s", g_calib_folder_path, exposure_time_x1000, temperature,
           (g_calib_bkg_type == CALIB_BKG_BK)? FILENAME_BK_CALI_IMG : FILENAME_WK_CALI_IMG);
}

static void convert_img16_to_BigEndian(uint8_t* output_img, uint16_t* raw_img16, int width,
                                       int height) {
    int i;
    for (i = 0; i < (width * height); i++) {
        output_img[2 * i] = (raw_img16[i] >> 8) & 0xFF;
        output_img[2 * i + 1] = (raw_img16[i]) & 0xFF;
    }
}
#endif

int calibration_save(void) {
    int ret_size;
    if (g_calibration_to_be_saved) {
        ret_size =
            plat_save_file(g_calib_file_path, (uint8_t*)&g_calibration, sizeof(CALIBRATION_DATA));
        egislog_d("plat_save_file ret_size=%d", ret_size);

        if (ret_size <= 0) {
            egislog_e("calibration_save error, path = %s", g_calib_file_path);
            return RESULT_FAILED;
        }
#ifndef TZ_MODE
        char bkg_img_path[256] = {0};
        unsigned int img_size = SENSOR_FULL_WIDTH * SENSOR_FULL_HEIGHT * sizeof(uint16_t);
        genarate_background_file_path(bkg_img_path);
        egislog_d("%s, bkg_img_path = %s", __func__, bkg_img_path);
        uint8_t* output_bkg_img = (uint8_t*)plat_alloc(img_size);
        uint16_t* bkg = NULL;
        if (output_bkg_img != NULL) {
            switch (g_calib_bkg_type) {
                case CALIB_BKG_WK:
                    bkg = calibration_get_wkg();
                    break;
                case CALIB_BKG_BK:
                default:
                    bkg = calibration_get_bkg();
                    break;
            }
            convert_img16_to_BigEndian(output_bkg_img, bkg, SENSOR_FULL_WIDTH, SENSOR_FULL_HEIGHT);
            plat_save_file(bkg_img_path, output_bkg_img, img_size);
            plat_free(output_bkg_img);
        }
#endif
        g_calibration_to_be_saved = FALSE;
    }
    return RESULT_OK;
}

int calibration_get_bkg_from_flash(uint8_t* bkg_flash, int* bkg_flash_sz) {
    CALIBRATION_DATA calibration_data;
    uint8_t addr[3];

    addr[0] = 0x01;
    addr[1] = 0x00;
    addr[2] = 0x00;

    memset(&calibration_data, 0x00, sizeof(CALIBRATION_DATA));
#ifdef EEPROM_USE
    io_eeprom_read_anysize(addr, sizeof(CALIBRATION_DATA), (uint8_t*)&calibration_data);
#else
    egislog_e("NOT SUPPORT calibration_get_bkg_from_flash");
#endif
    *bkg_flash_sz = SENSOR_FULL_WIDTH * SENSOR_FULL_HEIGHT * sizeof(uint16_t);
    memcpy(bkg_flash, calibration_data.infos[0][0].data, *bkg_flash_sz);

    plat_sleep_time(10);
    et760_init_sensor();

    return RESULT_OK;
}

int calibration_load_from_flash(void) {
    uint8_t addr[3];
    egislog_d("calibration_load_from_flash");

    addr[0] = 0x01;
    addr[1] = 0x00;
    addr[2] = 0x00;
    memset(&g_calibration, 0x00, sizeof(CALIBRATION_DATA));
#ifdef EEPROM_USE
    io_eeprom_read_anysize(addr, sizeof(CALIBRATION_DATA), (uint8_t*)&g_calibration);
#else
    egislog_e("NOT SUPPORT calibration_load_from_flash");
#endif
    plat_sleep_time(10);
    et760_init_sensor();

    return RESULT_OK;
}

int calibration_save_to_flash(void) {
    uint8_t addr[3];

    addr[0] = 0x01;
    addr[1] = 0x00;
    addr[2] = 0x00;
#ifdef EEPROM_USE
    io_eeprom_sector_write_anysize(addr, sizeof(CALIBRATION_DATA), (uint8_t*)&g_calibration);
#else
    egislog_e("NOT SUPPORT calibration_save_to_flash");
#endif
    plat_sleep_time(10);
    et760_init_sensor();
    return RESULT_OK;
}

static void insert_sort(int* arr, int num) {
    int i, key, j;
    for (i = 1; i < num; i++) {
        key = arr[i];
        j = i - 1;

        /* Move elements of arr[0..i-1], that are
          greater than key, to one position ahead
          of their current position */
        while (j >= 0 && arr[j] > key) {
            arr[j + 1] = arr[j];
            j = j - 1;
        }
        arr[j + 1] = key;
    }
}

static void generate_median_calib(unsigned short** input_images, int image_num, int width,
                                  int height, unsigned short* output_image) {
    if(input_images == NULL || output_image == NULL){
        egislog_e("%s , param has null", __func__);
        return;
    }
    int total_pixel = width * height;
    int toSort[image_num];
    for (int i = 0; i < total_pixel; i++) {
        for (int f = 0; f < image_num; f++) {
            if (input_images[f] == NULL) {
                egislog_e("%s , param has null", __func__);
                return;
            }
            toSort[f] = input_images[f][i];
        }
        insert_sort(toSort ,image_num);
        if (image_num % 2 == 0) {
            output_image[i] = (toSort[image_num / 2] + toSort[image_num / 2 + 1]) / 2;
        } else {
            output_image[i] = toSort[image_num / 2];
        }
    }
}

int calibration_generate(void) {
    int ret;

    g_exp_time = core_config_get_double(INI_SECTION_SENSOR, KEY_EXPOSURE_TIME, g_exp_time);  // ms
    g_hw_integrate_count =
        core_config_get_int(INI_SECTION_SENSOR, KEY_INTEGRATE_COUNT_HW, g_hw_integrate_count);
    int hw_integrate_count = g_hw_integrate_count;
    int gain2_enable = core_config_get_int(INI_SECTION_SENSOR, KEY_TWO_TIMES_GAIN, g_gain2_enable);
    int width = SENSOR_FULL_WIDTH;
    int height = SENSOR_FULL_HEIGHT;
    int frame_sz = width * height * sizeof(uint16_t);
    int img_num = IMG_NUM;
    uint16_t* image_list[IMG_NUM] = {0};
    uint16_t* output_image = (uint16_t*)plat_alloc(frame_sz);
    int* bkg = (int*)plat_alloc(width * height * sizeof(int));
    int bkg_cx = 0, bkg_cy = 0, bkg_radius = 0;
#define BKG_THR (3)

    egislog_d("exp_time_x10 = %d, hw_integrate_count = %d", (int)(g_exp_time * 10),
              hw_integrate_count);
    egislog_d("gain = %dX", g_gain2_enable ? 2 : 1);

    for (int k = 0; k < img_num; k++) {
        image_list[k] = (uint16_t*)plat_alloc(frame_sz);
        if (image_list[k] == NULL) {
            ret = RESULT_MALLOC_FAIL;
            egislog_e("%s out of memory", __func__);
            goto exit;
        }
    }

    isensor_set_int(PARAM_INT_ROI_X, 0);
    isensor_set_int(PARAM_INT_ROI_Y, 0);
    for (int k = 0; k < img_num; k++) {
        ret = isensor_get_dynamic_frame_16bit(image_list[k], height, width, 1);
        if (ret != EGIS_OK) {
            ret = RESULT_FAILED;
            goto exit;
        }
        ret = RESULT_OK;
    }
    uint16_t** input_image_list = image_list;
    uint16_t dev_id = DEVID(g_egis_sensortype.dev_id0, g_egis_sensortype.dev_id1);
    if(dev_id == DEVID(9,1)) {
        img_num -= 2;
        input_image_list = image_list + 2;
    }
#if defined(G3PLUS_MATCHER) || defined(ALGO_GEN_4) || defined(ALGO_GEN_5)
    // egis_generate_calib(image_list, img_num, width, height, output_image);  // Average background
    generate_median_calib(image_list, img_num, width, height, output_image);   // Median background
#elif defined(ALGO_GEN_7)
    go_generate_calib(image_list, img_num, width, height, output_image);
#else
    generate_calib(image_list, img_num, width, height, output_image);
#endif
    for (int i = 0; i < (width * height); i++) {
        bkg[i] = output_image[i];  // uint16_t to int
    }
    find_circle_center(bkg, width, height, BKG_THR, &bkg_cx, &bkg_cy, &bkg_radius);
    if (bkg_cx <= 0 || bkg_cy <= 0) {
        bkg_cx = width / 2;
        bkg_cy = height / 2;
        egislog_e("find_circle_center Error! set(%d,%d)", bkg_cx, bkg_cy);
    } else {
        egislog_i("find_circle_center, set (%d,%d) [%d]", bkg_cx, bkg_cy, CALIBRATION_VERSION);
    }

    int roi_length = core_config_get_int(INI_SECTION_SENSOR, KEY_RAW_NORMALIZATION_ROI,
                                         INID_RAW_NORMALIZATION_ROI);
    g_calibration.version = CALIBRATION_VERSION;

    if (g_calib_bkg_type >= EXP_SPECIES) {
        egislog_e("%s type %d >= %d", __func__, g_calib_bkg_type, EXP_SPECIES);
        ret = RESULT_FAILED;
        goto exit;
    }
    
    CALIBRATE_INFO* info = &g_calibration.infos[g_calib_bkg_type][0];
    info->exp_time = g_exp_time;
    info->hw_integrate_count = hw_integrate_count;
    info->bkg_cx = bkg_cx;
    info->bkg_cy = bkg_cy;
    info->adc_dac = g_all_adc_dac;
    memcpy((void*)info->data, (void*)output_image, frame_sz);
    g_calibration_to_be_saved = TRUE;
    notify_calib_data_update();
exit:
    for (int k = 0; k < img_num; k++)
        if (image_list[k]) plat_free(image_list[k]);

    PLAT_FREE(output_image);
    PLAT_FREE(bkg);

    return ret;
}

int is_calibration_data_valid() {
    return g_calibration.version == CALIBRATION_VERSION ? 1 : 0;
}

void calibration_get_bkg_center(int* bkg_cx, int* bkg_cy) {
    if (is_calibration_data_valid()) {
        CALIBRATE_INFO* info = &g_calibration.infos[0][0];
        *bkg_cx = info->bkg_cx;
        *bkg_cy = info->bkg_cy;
    } else {
        *bkg_cx = SENSOR_FULL_WIDTH / 2;
        *bkg_cy = SENSOR_FULL_HEIGHT / 2;
    }
}

int calibration_remove(void) {
    egislog_i("%s calibration_remove (%d,%d)", __func__, g_calibration.version,
              CALIBRATION_VERSION);
    RBS_CHECK_IF_NULL(g_calib_file_path, EGIS_INCORRECT_PARAMETER);
    g_calibration.version = 0;
    plat_remove_file(g_calib_file_path);
    notify_calib_data_removed();
    return RESULT_OK;
}


uint16_t* calibration_get_bkg(void) {
    CALIBRATE_INFO* cali_info = calibration_get();
    return cali_info == NULL ? NULL : calibration_get()->data;
}

uint16_t* calibration_get_wkg(void) {
    CALIBRATE_INFO* cali_info = calibration_get2();
    return cali_info == NULL ? NULL : calibration_get2()->data;
}

int calibration_get_bkg_integer(int type) {
    int ret = -99;

    CALIBRATE_INFO* cali_info = calibration_get();
    if (cali_info == NULL) {
        egislog_e("%s, no default bkg.", __func__);
        return ret;
    }

    switch (type) {
        case PARAM_INT_CALI_BKG_EXPOSURE_TIME_X10:
            ret = (int)(cali_info->exp_time * 10);
            break;
        case PARAM_INT_CALI_BKG_HW_INTEGRATE_COUNT:
            ret = cali_info->hw_integrate_count;
            break;
        default:
            egislog_e("%s, not supported type", __func__);
            break;
    }

    return ret;
}

int calibration_save_to_buffer(unsigned char* buffer, int* len) {
    ex_log(LOG_DEBUG, "%s enter", __func__);

    if (buffer == NULL || len == NULL) {
        ex_log(LOG_ERROR, "calibration_save_to_buffer EGIS_INCORRECT_PARAMETER ");
        return EGIS_INCORRECT_PARAMETER;
    }

    if (*len < sizeof(CALIBRATION_DATA)) {
        ex_log(LOG_ERROR, "calibration_save_to_buffer len is not enough");
        ((int*)buffer)[0] = sizeof(CALIBRATION_DATA);
        *len = sizeof(int);
        return EGIS_RECEIVE_BUF_LEN_IS_NOT_ENOUGH;
    }

    *len = sizeof(CALIBRATION_DATA);
    memcpy(buffer, (uint8_t*)&g_calibration, sizeof(CALIBRATION_DATA));

    ex_log(
        LOG_DEBUG,
        "calibration_save_to_buffer exp_time = %d, hw_integrate_count = %d, bkg_cx=%d, bkg_cy=%d",
        g_calibration.infos[0][0].exp_time * 10, g_calibration.infos[0][0].hw_integrate_count,
        g_calibration.infos[0][0].bkg_cx, g_calibration.infos[0][0].bkg_cy);

    return EGIS_OK;
}

int calibration_load_from_buffer(unsigned char* buffer, int len) {
    int ret = EGIS_OK;
    ex_log(LOG_DEBUG, "%s enter, length = %d", __func__, len);
    memset(&g_calibration, 0x00, sizeof(CALIBRATION_DATA));

    if (len != 0 && len != sizeof(CALIBRATION_DATA)) {
        return EGIS_CHECKSUM_INVALID_PARAMETER;
    }

    if (buffer == NULL || len == 0) {
        ex_log(LOG_ERROR, "calibration_load_from_buffer use flash data ");
        ret = calibration_load_from_flash();
    } else {
        ex_log(LOG_DEBUG, "calibration_load_from_buffer use send data ");
        memcpy((unsigned char*)&g_calibration, buffer, sizeof(CALIBRATION_DATA));
    }

    ex_log(LOG_DEBUG,
           "calibration_load_from_buffer exp_time = %d, hw_integrate_count = "
           "%d,bkg_cx=%d,bkg_cy=%d,len=%d,sizeof(CALIBRATION_DATA)=%d",
           g_calibration.infos[0][0].exp_time * 10, g_calibration.infos[0][0].hw_integrate_count,
           g_calibration.infos[0][0].bkg_cx, g_calibration.infos[0][0].bkg_cy, len,
           sizeof(CALIBRATION_DATA));
    notify_calib_data_update();
    return ret;
}

void calibration_set_bkg_type(int option){
    g_calib_bkg_type = option;
}
