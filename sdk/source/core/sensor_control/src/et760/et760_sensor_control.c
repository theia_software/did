#include <plat_mem.h>
#include <isensor_api.h>
#include "auo_oled_panel.h"
#include "egis_definition.h"
#include "fp_err.h"
#include "fpsensor_7xa_definition.h"
#include "plat_log.h"
#include "plat_time.h"
#include "sensor_config.h"
#include "sensor_control_common.h"
#include "type_definition.h"

#include "et760_sensor_control_common.h"
#include "et760_sensor_control.h"

#define LOG_TAG "RBS-et760-Sensor"

#define NEED_CALIBRATION_IC_CLOCK 1

#define MAX(x, y) (((x) > (y)) ? (x) : (y))
#define MIN(x, y) (((x) < (y)) ? (x) : (y))

uint8_t g_pix_id = 0;

#ifdef FAKE_IMAGE_FLOW

/**
    ET760 BA SPI SPEED MODE:
    0=disable, spi speed ~= 8M
    1=enable, need control SPI Register 0x?0000028 & 0x?0000002c
    and read32 & burst read add one dummy byte after address/offset

    ※※※ !! must be force enable !! ※※ raw spi data cant be indentify spi mode
**/
int NEED_SPI_SPEED_MODE = 1 ; 

struct ET760_Rectangle {
    int x ;
    int y ;
    int width ;
    int height ;
} calcBlockTemp ;

/**
 * 變數分為 ET760專用 及 面版PANEL專用
 * 有可能改叫 ET760_PANEL_CONTROL 之類的
 */
 

extern uint8_t g_read_image_count;
extern int g_fake_enroll_count;
extern int g_fake_verify_count;

// for sensor region
int x_num = 2 ;
int y_num = 2;
int x_start = 1;
int y_start = 1;

int trim_val = 0 ;


// old i7

extern float g_exp_time ; // 這個值是SHARE isensor_et7xa之的 共用曝光時間

// for ET760
int adc_gain = 0 ; // ET760 GAIN

#endif


/**
 * 在各大INTERFACE中，放置各面版需要的FUNCTION
 *
 * 大INTERFACE:
 * et760_get_image
 * et760_get_sensor_id
 * et760_set_position
 *
 * 要OVERRIDE的:
 * BURN_BIN_CODE
 * TRIGGER_TCON
 * WAIT_TCON
 * 各種設定方法、OPCODE TRIGGER方法
 *
 * IC ONLY、跟韌體無關的設定 放到 et760_sensor_control_common
 *
 */

int et760_get_sensor_id(uint8_t* dev_id0, uint8_t* dev_id1) {
    force_enable_ba_spi() ; // 預期強制打開
    
	int ver = et760_32bit_read_value(ET760_VERSION) ;
    egislog_i("ET760_VERSION 0x%x = 0x%x", ET760_VERSION, ver) ;
	if ( ver == ET760_VERSION_RESULT || ver == 0x20000000 ) {
		*dev_id0 = 7; // 值FROM ROBERT
		*dev_id1 = 60;
		egislog_i("Get Sensor ID= %x", ver) ;
	}
	else {
		egislog_i("Get Sensor ID ERROR!! %x != %x", ver, ET760_VERSION_RESULT) ;
	}

	return RESULT_OK;
}

int et760_setGain(int et760_gain) {
    int ret = et760_32bit_read_value(0x10000108) ;
    ret = ret & 0xFFF0F0FF ;
    ret = ret | (et760_gain << 8) | (et760_gain << 16) ;
    et760_32bit_write(0x10000108, ret);
    return EGIS_OK;
}

int et760_is_spi_working() {
	int ver = et760_32bit_read_value(ET760_VERSION) ;
	if ( ver == ET760_VERSION_RESULT || ver == 0x20000000 ) {
		return RESULT_OK ;
	}
	egislog_e("Error SPI Not Working: can't read version, please check spi connection") ;
	return EGIS_COMMAND_FAIL ;
}

int et760_init_sensor(void) {
	int ret = et760_is_spi_working() ;
	if (RESULT_OK == ret) {

	    if ( NEED_CALIBRATION_IC_CLOCK ) { // if need calibration IC, may need 3-5 seconds
            chip_speed_flow() ;
	    }
		auo3x4_init_panel() ;
	} else {
        egislog_e("%s, et760 sensor init fail!!!!");
    }
	return ret ;
}

int et760_set_expo_time_sw(float expo_time){
    auo3x4_set_expotime_ms( (int) expo_time );
    return EGIS_OK;
}

int et760_get_expo_time_sw(float* expo_time) {
	int expo_time_int = 0;
	*expo_time = g_exp_time ;
    return RESULT_OK;
}


void et7xa_get_temperature(int* code_out, int* temperature) {
	egislog_d("Skip for 760");
	*temperature = 0;
}

void et7xa_set_gain(BOOL gain2_enable) {
	egislog_d("Skip for 760");
}

int et7xa_sensor_mux_select(uint16_t select) {
	egislog_d("Skip for 760");
	return RESULT_OK;
}


int et760_set_position(int x, int y, int width, int height)
{
	int ret = 0;
    convert_to_roi_v2(x, y, width, height) ; // update to g_x_start ... 
    change_region_roi_v2(g_x_zsw_start, g_x_zsw_end, g_y_stv_start, g_y_stv_end) ;
	return ret;
}

int et760_get_image(uint8_t *buffer, int length) {

    int orientation = 0 ;
    isensor_get_int(PARAM_INT_SENSOR_ORIENTATION, &orientation) ;

    // auo full area v2
    if (1) {
        int full_size = CELL_WIDTH * CELL_ROW_LENGTH * CELL_COL_LENGTH * CELL_HEIGHT ;
        //int ** full = plat_alloc( sizeof(int) * full_size) ;
        int ** full = et760_2d_alloc(CELL_COL_LENGTH * CELL_WIDTH, CELL_ROW_LENGTH * CELL_HEIGHT) ;
        // done AuoOledPanel.convert_to_roi(roi_x, roi_y, roi_w, roi_h);

        egislog_i("ET760 Auo full area v2, GET_IMAGE size=%d, full_size=%d", g_roi_h * g_roi_w, full_size) ;

        if ( g_roi_h * g_roi_w == (full_size)) {
        	int p = 0 ;
			auo3x4_get_full(full) ;
            if (orientation==180) {
                rotate180(full, g_roi_w, g_roi_h);
            }
            for ( int h = 0 ; h < g_roi_h ; h++ ) {
                for (int w = 0 ; w < g_roi_w ; w++ ) {
                    buffer[p++] = full[h][w] & 0xFF ;
                    buffer[p++] = (full[h][w] >> 8) & 0xFF;
                }
            }
        }
        else {
            // global variable about xywh already update by et760_set_position
            int v2_width = (g_x_zsw_end - g_x_zsw_start + 1) * 4 ;
            int v2_height = (g_y_stv_end - g_y_stv_start + 1) * 4 ;

            int limits = calc_limit() ;

            int **part = et760_2d_alloc(v2_width, v2_height) ;
            int p = 0 ;
            if ( limits == 2 ) {

                auo3x4_set_expotime_ms(g_auo_EXPTIME - 3*REGION_MINUS) ;
                for ( int i = 0 ; i < 3 ; i++ ) {
                    if (is_in_x_region(g_roi_x, g_roi_w, i)) {

                        change_region_roi_v2(i*50,((i+1)*50-1),0,199) ;
                        //auo3x4_empty_trig();
                        auo3x4_trigger_tcon() ;
                        auo3x4_wait_tcon();

                        int** frame = auo3x4_get_image(CELL_WIDTH, CELL_HEIGHT * CELL_ROW_LENGTH) ;
                        putPartArray(full, frame, i*CELL_WIDTH,0, CELL_WIDTH, CELL_HEIGHT * CELL_ROW_LENGTH ) ;

                        et760_2d_free(frame, CELL_HEIGHT * CELL_ROW_LENGTH) ;
                    }
                }

                getPartArray(part, full, g_roi_x, g_roi_y, g_roi_w, g_roi_h) ;
                if (orientation==180) {
                    rotate180(part, g_roi_w, g_roi_h);
                }
                for ( int h = 0 ; h < g_roi_h ; h++ ) {
                    for (int w = 0 ; w < g_roi_w ; w++ ) {
                        buffer[p++] = part[h][w] & 0xFF ;
                        buffer[p++] = (part[h][w] >> 8) & 0xFF;
                    }
                }
            }
            else {
                // only trigger once to get all image
                //auo3x4_empty_trig();
                auo3x4_trigger_tcon() ;
                auo3x4_wait_tcon() ;

                int **frame = auo3x4_get_image(v2_width, v2_height);
                putPartArray(full, frame, g_x_zsw_start*4, g_y_stv_start*4, v2_width, v2_height);
                getPartArray(part, full, g_roi_x, g_roi_y, g_roi_w, g_roi_h) ;


                if (orientation==180){
                    rotate180(part, g_roi_w, g_roi_h) ;
                }
                for ( int h = 0 ; h < g_roi_h ; h++ ) {
                    for (int w = 0 ; w < g_roi_w ; w++ ) {
                        buffer[p++] = part[h][w] & 0xFF ;
                        buffer[p++] = (part[h][w] >> 8) & 0xFF;
                    }
                }
                et760_2d_free(frame, v2_height) ;
            }
            et760_2d_free(part, v2_height) ;
        }
        et760_2d_free(full, CELL_HEIGHT * CELL_ROW_LENGTH);
        return EGIS_OK;
    }
}




/**
 * @param dac
 * @return frame avg
 */
int _doTestDac(int *map, int dac, int test_width, int test_height) {
    et760_set_all_adc_dac(dac) ;

    if ( map[dac] != -1 ) {
        return map[dac] ;
    }
    auo3x4_trigger_tcon() ;
    auo3x4_wait_tcon() ;
    int ** frame = auo3x4_get_image(test_width, test_height);

    int sum = 0 ;
    for ( int h = 0 ; h < test_height ; h++ ) {
        for ( int w = 0 ; w < test_width ; w++ ) {
            sum += frame[h][w] ;
        }
    }

    et760_2d_free(frame, test_height) ;
    int avg = sum / (test_height*test_width) ;
    map[dac] = avg ;

    egislog_i("Search dac %3d avg= %d", dac, map[dac]) ;

    return avg ;
}
/**
 * Search dac with xywh = 200,200,200,200(Region 2,2)
 * @param expo 0-1000
 * @param target_avg 0-12288
 * @param targetDac 0-127
 * @return EGIS_OK or EGIS_TEST_FAIL
 */
int et760_search_dac(int expo, int target_avg, int *targetDac) {

    int width = 200 ;
    int height = 200 ;
    int dac_count = 128 ;
    auo3x4_set_expotime_ms(expo) ;
    et760_set_position(200,200,200,200) ; // use Region 2,2

    int resultMap[dac_count] ;
    for ( int i = 0 ; i < dac_count ; i++ ) {
        resultMap[i] = -1 ;
    }

    auo3x4_empty_trig() ;
    auo3x4_empty_wait_tcon() ;

    // scan basic result
    for ( int i = 127; i >=0 ; i-=10) {
        _doTestDac(resultMap, i, width, height) ;
    }

    int up = 127 ;
    int down = 0 ;
    int middle = 0 ;

    while ( abs(up-down) > 1) {
        middle = (up + down) / 2 ;

        int avg = _doTestDac(resultMap, middle, width, height);
        if ( avg > target_avg) {
            up = middle ;
        }
        else if ( avg <= target_avg ) {
            down = middle ;
        }
    }
    egislog_i("find middle= %d avg=%d",(up+down)/2, resultMap[(up+down)/2] );
    middle = (up+down)/2 ;


    // get middle +-2 dac
    for ( int i = 0 ; i < 5 ; i++ ) {
        int search_dac = middle - (i-2) ;
        if ( search_dac >= 0 && search_dac <= dac_count ) {
            resultMap[search_dac] = -1 ;
            _doTestDac(resultMap, search_dac, width, height);
        }
    }

    int cnt = 0 ;
    int near_id = 0 ;
    int near_diff = 65536;

    for (int i = 0 ; i < dac_count; i++ ){
        if (abs(target_avg - resultMap[i]) < near_diff) {
            near_id = i ;
            near_diff = abs(target_avg - resultMap[i]) ;
        }
        cnt = cnt + 1 ;
    }

    for (int i = 0 ; i < dac_count ; i++ ) {
        if ( resultMap[i] != -1 ) {
            egislog_i("Search dac %3d avg= %d", i, resultMap[i]) ;
        }
    }

    if ( resultMap[near_id] == 0 ) {
        egislog_i("Search Dac Error: get avg=0");
        return EGIS_COMMAND_FAIL ;
    }

    *targetDac = near_id ;
    return EGIS_OK ;
}