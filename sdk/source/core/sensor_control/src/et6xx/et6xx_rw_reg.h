#ifndef __ET6XX_RW_REG_H__
#define __ET6XX_RW_REG_H__

#include "egis_definition.h"

typedef struct _SENSOR_REG {
    unsigned char RegAddr;
    unsigned char RegType;
    unsigned char RegValue;
    unsigned char RegValueDefault;
    unsigned char RegValueMask;
} SENSOR_REG;

#define REG_TYPE_NONE 0x00
#define REG_TYPE_DISCARD 0x00
#define REG_TYPE_READ 0x01
#define REG_TYPE_WRITE 0x02

int et6xx_get_vdm(unsigned char* buff);
int et6xx_set_vdm(unsigned char* buff);

int et6xx_write_register(BYTE address, BYTE value);
int et6xx_write_burst_register(BYTE start_addr, BYTE len, BYTE* pvalue);

int et6xx_read_register(BYTE address, BYTE* value);
int et6xx_read_burst_register(BYTE start_addr, BYTE len, BYTE* pdata);
SENSOR_REG et6xx_get_map_reg(BYTE addr);

void et6xx_get_reg_all_map(SENSOR_REG** map, int* size);

void et6xx_reg_map_fill(void);
void et6xx_reg_map_init(void);

#endif
