#ifndef __EGIS_FP_COMMON_6XX__H_
#define __EGIS_FP_COMMON_6XX__H_

#include "isensor_api.h"

/*int et5xx_get_vdm_target_mean(BYTE* mean, BOOL fix);
int et5xx_get_vdm(unsigned char* buff, int width, int height);
int et5xx_set_vdm(unsigned char* buff, int width, int height);

int calibrate_detect_mode(BOOL toWaitForTHL);
int et5xx_calibrate_sensor_mode(void);*/

int isensor_set_custom_et6xx_sensor_mode(int sensing_mode);
int isensor_et6xx_set_feature(const char* param, int value);
int isensor_et6xx_open(int* timeout);
int isensor_get_et6xx_sensor_size(int* width, int* height);
int isensor_et6xx_calibrate(FPS_CALIBRATE_OPTION option);
int et6xx_calibrate_sensor_mode(void);
int isensor_et6xx_set_sensor_mode();
int isensor_et6xx_set_detect_mode();
void fpsET6XXSetBoostModeOn(BOOL on);
BOOL fpsET6XXGetBoostMode();
void isensor_et6xx_dump_data(void);
FPS_SENSING_TYPE_T isensor_et6xx_get_sensing_mode(void);
int isensor_et6xx_set_power_off(void);
int isensor_et6xx_get_frame(BYTE* frame, UINT height, UINT width, UINT number_of_frames);
int isensor_et6xx_read_int_status(BYTE* IntStatus);
int isensor_et6xx_get_dynamic_frame(BYTE* frame, UINT height, UINT width, UINT number_of_frames);
int isensor_et6xx_set_Z1_area(BOOL forceSetFull);
int isensor_et6xx_set_power_off(void);
int isensor_et6xx_pullup_int(BOOL polarity);
int isensor_et6xx_read_nvm(unsigned char* buff);
int isensor_et6xx_read_rev_id(BYTE* read_buf, int length);
int isensor_et6xx_set_navigation_detect_mode(void);
int isensor_et6xx_set_navigation_sensor_mode(void);
int isensor_et6xx_recovery(FP_MODE fp_mode);
int isensor_et6xx_get_recovery_event(void);
int isensor_et6xx_set_spi_power(const int option);

#define NVM_INDEX_VBT 0x1B
#define NVM_INDEX_RV_L 0x2D
#define NVM_INDEX_RV_H 0x2E
#define NVM_INDEX_CHECKSUM_HB 0x31
#define NVM_INDEX_CHECKSUM_LB 0x32
int fpsGetNVMRV();

/*int check_and_recovery(FP_MODE fp_mode);*/

/*
 *	specific 5xx adjust functions.
 */
/*BOOL sensor_is_LDO_enable();

void fpsSetBoostModeOn(BOOL on);
BOOL fpsGetBoostMode();
int fps_get_scan_z2_info(void);
int CrossFix(BYTE* img, int w, int h, BOOL revert);
#define NVM_INDEX_HW_DC_C 0x1D
#define NVM_INDEX_HW_TH 0x20
#define NVM_INDEX_RV 0x21
#define NVM_INDEX_CHECKSUM_HB 0x31
#define NVM_INDEX_CHECKSUM_LB 0x32
#define NVM_MEAN_MAX_INIT_FAIL -1
#define NVM_MEAN_MAX_ERROR -2
#define MEAN_PER_DCC 70  // Trustonic_516, Lenovo_516, Normal_516
int fpsGetNVMRV();

#define RV_LOW_THRESHOLD 20*/

#endif
