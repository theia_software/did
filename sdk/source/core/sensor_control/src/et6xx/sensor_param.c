#include "sensor_param.h"
#include "egis_sensor_parameter.h"
#include "isensor_api.h"
#include "plat_log.h"

#define LOG_TAG "RBS-EGISFP"

extern unsigned short g_et6xx_background[128 * 128];
extern int g_et600_adc_gain;
extern BOOL g_refline_bad;
extern SensorParameter g_sensor_parameter;

int isensor_get_int(isensor_param_type_t param_type, int* value) {
    egislog_d("%s, param_type = %d", __func__, param_type);
    switch (param_type) {
        case PARAM_INT_RAW_IMAGE_BPP:
            *value = 16;
            break;
        case PARAM_INT_ADC_GAIN:
            *value = g_et600_adc_gain;
            break;
        case PARAM_INT_REF_LINE_IS_BAD:
            *value = g_refline_bad;
            break;
        default:
            egislog_e("%s, not supported type = %d", __func__, param_type);
            break;
    }
    return EGIS_OK;
}

int isensor_set_int(isensor_param_type_t param_type, int value) {
    egislog_e("%s, not implemented. return OK directly", __func__);
    return EGIS_OK;
}

int isensor_get_buffer(isensor_param_type_t param_type, unsigned char* out_buffer,
                       int* out_buffer_size) {
    unsigned short **p_bkg_image, **p_vdm_image;
    SensorParameter** p_dst_cd_buf;
    egislog_d("%s, param_type = %d", __func__, param_type);
    switch (param_type) {
        case PARAM_BUF_BKG_IMAGE_POINTER:
            p_bkg_image = (unsigned short**)out_buffer;
            *p_bkg_image = g_et6xx_background;
            break;
        case PARAM_BUF_CALIBRATION_DATA_POINTER:
            p_dst_cd_buf = (SensorParameter**)out_buffer;
            *p_dst_cd_buf = &g_sensor_parameter;
            *out_buffer_size = sizeof(g_sensor_parameter);
            break;
        case PARAM_BUF_VDM_DATA_POINTER:
            p_vdm_image = (unsigned short**)out_buffer;
            *p_vdm_image = g_sensor_parameter.vdm_bk;
            *out_buffer_size = sizeof(g_sensor_parameter.vdm_bk);
            break;

        default:
            egislog_e("%s, not supported type = %d", __func__, param_type);
            break;
    }
    return EGIS_OK;
}

int isensor_set_buffer(isensor_param_type_t param_type, unsigned char* in_buffer,
                       int in_buffer_size) {
    int ret = EGIS_OK;
    SensorParameter* cd;
    switch (param_type) {
        case PARAM_BUF_CALIBRATION_DATA_POINTER:
            if (in_buffer_size != sizeof(SensorParameter) || in_buffer == NULL) {
                egislog_e("%s, CALIBRATION_DATA wrong", __func__);
                egislog_e("size %d expect size %d", in_buffer_size, sizeof(SensorParameter));
                ret = EGIS_INCORRECT_PARAMETER;
            }
            cd = (SensorParameter*)in_buffer;
            /*
             * Add version Check.
             */
            /*
            if ( cd->cd_version != g_sensor_parameter.version)
            {
                egislog_e("%s, CALIBRATION_DATA version was wrong", __func__);
                ret = EGIS_INCORRECT_PARAMETER;
            }
            */
            if (ret == EGIS_OK) {
                memcpy(&g_sensor_parameter, cd, in_buffer_size);
            }
            break;

        default:
            egislog_e("%s, not implemented. return OK directly", __func__);
            break;
    }
    return ret;
    ;
}
