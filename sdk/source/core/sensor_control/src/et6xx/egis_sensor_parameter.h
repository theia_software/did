#ifndef __EGIS_SENSOR_PARAMETER__H_
#define __EGIS_SENSOR_PARAMETER__H_

#include "egis_definition.h"

typedef struct _statistic_value_601_t {
    unsigned int r;
    unsigned int l;
} statistic_value_601_t;

typedef struct _fod_param_t {
    statistic_value_601_t calibrated_mean;
    statistic_value_601_t calibrated_thh;
    statistic_value_601_t calibrated_thl;
    unsigned int threshold_delta_value;
} fod_param_t;

typedef struct _SensorParameter {
    BYTE bkg_average;
    BYTE bkg_standard_deviation;
    BYTE calibration_flag;
    BYTE detect_gain;
    BYTE sensor_gain;

    BYTE WBK_VBT_OFS_Value;
    BYTE Ori_VBT_OFS_Value;
    BYTE VBT_OPTION;

    BOOL sensor_is_long;

    unsigned short vdm_bk[MAX_IMG_SIZE];
    unsigned short clb_bk[MAX_SENSOR_WIDTH];
    int is_already_get_vdm_clb;
    fod_param_t fod_th;
    int clb_size;

    BYTE bad_pixel[MAX_IMG_SIZE];
    int bad_pixel_count;

} SensorParameter;

typedef SensorParameter ET6XXCalibrationData;

#endif
