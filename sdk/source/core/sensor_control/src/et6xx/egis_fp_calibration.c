#ifndef _WINDOWS
#ifndef TZ_MODE
#include <unistd.h>
#endif
#endif
//#include "VFSys.h"
//#include "FPMatchLib.h"
//#include "EgisImageAlgorithm.h"
#include "egis_fp_calibration.h"
#include "egis_fp_common_6XX.h"
#include "egis_log.h"
#include "egis_sensor_parameter.h"
#include "fpsensor_definition.h"
#include "isensor_api.h"
#include "plat_heap.h"
#include "plat_spi.h"

#define LOG_TAG "RBS-EGISFP"

//
//	Capture image status for TrustZone
//
#ifndef _WINDOWS
SensorParameter g_sensor_parameter = {

    .sensor_gain = MAX_SENSOR_GAIN,
#ifdef POLLING
    .calibration_flag = DO_CALIBRATION_DVR,
#else
    .calibration_flag = DO_CALIBRATION_DVR | DO_CALIBRATION_INTERRUPT,
#endif
};
#else
SensorParameter g_sensor_parameter;
#endif
//
//	Debug
//

//
//	Interrupt calibration temp parameters
//
int g_int_dir;
//
// for finger on invoke
//

int g_vdm_cnt = 0;
BOOL gNeedReKVDM = FALSE;

void SetNeedReKVDM() {
    egislog_i("%s, want to reK", __func__);
    gNeedReKVDM = TRUE;
}
BOOL GetNeedReKVDM() {
    return gNeedReKVDM;
}

int skip_calibration_vdm_mode(void) {
    egislog_e("removecode1");
    int ret = 0;

    return ret;
}
int calibrate_vdm_mode(void) {
    return 0;
}

static BYTE g_best_vdm[MAX_IMG_SIZE];
static BOOL g_use_best_vdm = FALSE;
static int g_bad_pixel_count_temp = 0;
static BYTE g_bad_pixel_temp[MAX_IMG_SIZE] = {0};

int calibrate_vdm_check(BYTE flag) {
    return 0;
}

int has_interrupt(void) {
#define INTERRUPT_DELAY_TIME 10 * 1000 * 1000

#ifdef TZ_MODE
    volatile int time_cnt = 0;
    int j;
    for (j = 0; j < INTERRUPT_DELAY_TIME; j++) time_cnt++;
#elif _WINDOWS
    Sleep(10);
#else
    usleep(10000);
#endif
    // return et5xx_has_interrupt(TRUE);
    return 0;
}

static int gFirstCalibrateDvrFail = -1;
void CalibrateDvrFingerTouch() {
    if (gFirstCalibrateDvrFail == -1) {
        egislog_d("FirstCalibrateDvr Fail");
        gFirstCalibrateDvrFail = 1;
        SetNeedReKVDM();
    } else {
        egislog_d("FirstCalibrateDvr OK");
        gFirstCalibrateDvrFail = 0;
    }
}
