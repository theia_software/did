
/******************************************************************************\
|*
*|
|*  calibrate_et510                                                           *|
|*  Version: 0.9.5.12                                                         *|
|*  Date: 2015/12/16                                                          *|
|*  Revise Date: 2016/06/27                                                   *|
|*  Copyright (C) 2007-2016 Egis Technology Inc.                              *|
|*                                                                            *|
\******************************************************************************/
#include "egis_log.h"
#include "fpsensor_6XX_definition.h"
#include "isensor_api.h"
#include "plat_spi.h"

#define LOG_TAG "RBS-EGISFP-DUMP"

void isensor_dump_data(void) {
    egislog_d("%s", __func__);
#define burst_read_count (read_end_register - burst_read_addr + 1)
    int i = 0;
    BYTE read_buf[0xff] = {0};
#if 0
	BYTE burst_read_addr =
	    0x0;  // Total amount registers read for one burst read OP.
	BYTE read_end_register = 0x0;
	
	//int burst_read_count = 1;  // For start address.

	egislog_d("%s", __func__);
	egislog_d("======");
	io_5xx_dispatch_read_burst_register(burst_read_addr, burst_read_count,
					    read_buf);
	for (i = 0; i < burst_read_count; i++)
		egislog_d("addr[0x%x] = 0x%x", i + burst_read_addr,
			  read_buf[i]);
	egislog_d("======");

	burst_read_addr = 0x3;  // Total amount registers read for one burst read OP.
	read_end_register = 0x28;

	egislog_d("%s", __func__);
	egislog_d("======");
	io_5xx_dispatch_read_burst_register(burst_read_addr, burst_read_count,
					    read_buf);
	for (i = 0; i < burst_read_count; i++)
		egislog_d("addr[0x%x] = 0x%x", i + burst_read_addr,
			  read_buf[i]);
	egislog_d("======");

	burst_read_addr = 0x2a;  // Total amount registers read for one burst read OP.
	read_end_register = 0x2b;

	egislog_d("%s", __func__);
	egislog_d("======");
	io_5xx_dispatch_read_burst_register(burst_read_addr, burst_read_count,
					    read_buf);
	for (i = 0; i < burst_read_count; i++)
		egislog_d("addr[0x%x] = 0x%x", i + burst_read_addr,
			  read_buf[i]);
	egislog_d("======");

	burst_read_addr = 0x30;  // Total amount registers read for one burst read OP.
	read_end_register = 0x38;

	egislog_d("%s", __func__);
	egislog_d("======");
	io_5xx_dispatch_read_burst_register(burst_read_addr, burst_read_count,
					    read_buf);
	for (i = 0; i < burst_read_count; i++)
		egislog_d("addr[0x%x] = 0x%x", i + burst_read_addr,
			  read_buf[i]);
	egislog_d("======");

	burst_read_addr = 0x3a;  // Total amount registers read for one burst read OP.
	read_end_register = 0x43;

	egislog_d("%s", __func__);
	egislog_d("======");
	io_5xx_dispatch_read_burst_register(burst_read_addr, burst_read_count,
					    read_buf);
	for (i = 0; i < burst_read_count; i++)
		egislog_d("addr[0x%x] = 0x%x", i + burst_read_addr,
			  read_buf[i]);
	egislog_d("======");

#endif

    BYTE burst_read_addr = 0x7;  // Total amount registers read for one burst read OP.
    int read_end_register = 0x8;

    io_5xx_dispatch_read_burst_register(burst_read_addr, burst_read_count, read_buf);
    for (i = 0; i < burst_read_count; i++)
        egislog_d("addr[0x%x] = 0x%x", i + burst_read_addr, read_buf[i]);
    egislog_d("======");

    burst_read_addr = 0x14;  // Total amount registers read for one burst read OP.
    read_end_register = 0x14;

    io_5xx_dispatch_read_burst_register(burst_read_addr, burst_read_count, read_buf);
    for (i = 0; i < burst_read_count; i++)
        egislog_d("addr[0x%x] = 0x%x", i + burst_read_addr, read_buf[i]);
    egislog_d("======");

    burst_read_addr = 0x40;  // Total amount registers read for one burst read OP.
    read_end_register = 0x40;

    io_5xx_dispatch_read_burst_register(burst_read_addr, burst_read_count, read_buf);
    for (i = 0; i < burst_read_count; i++)
        egislog_d("addr[0x%x] = 0x%x", i + burst_read_addr, read_buf[i]);
    egislog_d("======");
}
