#include <stdint.h>
#include <stdlib.h>

#include "et6xx_rw_reg.h"
#include "fpsensor_6XX_definition.h"
#include "plat_heap.h"
#include "plat_spi.h"

// static BYTE g_reg_mapping = {0};
static SENSOR_REG g_reg_mapping[TOTAL_REG_COUNT] = {{0}};
int et6xx_write_burst_register(BYTE start_addr, BYTE len, BYTE* pvalue) {
    int i = 0;
    for (i = start_addr; i < start_addr + len; i++)
        g_reg_mapping[i].RegValue = *(pvalue + i) & g_reg_mapping[i].RegValueMask;
    return io_5xx_dispatch_write_burst_register(start_addr, len, pvalue);
}

int et6xx_write_reverse_register(BYTE start_addr, BYTE len, BYTE* pvalue) {
    int i = 0;
    for (i = start_addr; i < start_addr + len; i--)
        g_reg_mapping[i].RegValue = *(pvalue + i) & g_reg_mapping[i].RegValueMask;
    return io_5xx_dispatch_write_reverse_register(start_addr, len, pvalue);
}

int et6xx_write_register(BYTE address, BYTE value) {
    return et6xx_write_burst_register(address, 1, &value);
}

int et6xx_read_register(BYTE address, BYTE* value) {
    return io_5xx_dispatch_read_register(address, value);
}

int et6xx_read_burst_register(BYTE start_addr, BYTE len, BYTE* pdata) {
    return io_5xx_dispatch_read_burst_register(start_addr, len, pdata);
}

SENSOR_REG et6xx_get_map_reg(BYTE addr) {
    return g_reg_mapping[addr];
}

void et6xx_get_reg_all_map(SENSOR_REG** map, int* size) {
#if 1
#include "egis_log.h"
#define LOG_TAG "EGISFP_REG"
    int i = 0;
    for (i = 0; i < 0x40; i++) egislog_d("reg[%x] = 0x%x", i, g_reg_mapping[i]);
#endif
    if (map != NULL) *map = g_reg_mapping;
    if (size != NULL) *size = TOTAL_REG_COUNT;
}
void et6xx_reg_map_fill(void) {
    int i;
    BYTE values[TOTAL_REG_COUNT] = {0};
    et6xx_read_burst_register(0, 0xff, values);

    for (i = 0; i < 0xff; i++) {
        if (g_reg_mapping[i].RegType == (REG_TYPE_DISCARD & REG_TYPE_NONE)) continue;
        g_reg_mapping[i].RegValue = values[i] & g_reg_mapping[i].RegValueMask;
    }
}

void et6xx_reg_map_init(void) {
#ifdef __SENSOR_ET601__
    SENSOR_REG* reg = (SENSOR_REG*)g_reg_mapping;
    unsigned char i;
    for (i = 0; i < TOTAL_REG_COUNT; i++) {
        reg[i].RegAddr = i;
        reg[i].RegType = REG_TYPE_NONE;
        reg[i].RegValue = 0x00;
        reg[i].RegValueDefault = 0x00;
        reg[i].RegValueMask = 0xFF;
    }

    // STUS 0x0
    reg[STUS_ET600_ADDR].RegType = REG_TYPE_READ;
    reg[STUS_ET600_ADDR].RegValueDefault = 0xA8;
    reg[STUS_ET600_ADDR].RegValueMask = 0xFF;

    // ACTN_ET600_ADDR 0x1
    reg[ACTN_ET600_ADDR].RegType = REG_TYPE_DISCARD;
    reg[ACTN_ET600_ADDR].RegValueDefault = 0x0;
    reg[ACTN_ET600_ADDR].RegValueMask = 0x0F;

    // FIX_COL_ADR_ADDR 0x2
    reg[FIX_COL_ADR_ADDR].RegType = REG_TYPE_DISCARD;
    reg[FIX_COL_ADR_ADDR].RegValueDefault = 0x38;
    reg[FIX_COL_ADR_ADDR].RegValueMask = 0xFF;

    // DSM_OFS_SCN_H_ET600_ADDR 0x3
    reg[DSM_OFS_SCN_H_ET600_ADDR].RegType = REG_TYPE_READ | REG_TYPE_WRITE;
    reg[DSM_OFS_SCN_H_ET600_ADDR].RegValueDefault = 0x40;
    reg[DSM_OFS_SCN_H_ET600_ADDR].RegValueMask = 0xFF;

    // DSM_OFS_SCN_L_ET600_ADDR 0x4
    reg[DSM_OFS_SCN_L_ET600_ADDR].RegType = REG_TYPE_READ | REG_TYPE_WRITE;
    reg[DSM_OFS_SCN_L_ET600_ADDR].RegValueDefault = 0x00;
    reg[DSM_OFS_SCN_L_ET600_ADDR].RegValueMask = 0xFF;

    // DSM_OFS_VDM_H_ET600_ADDR 0x5
    reg[DSM_OFS_VDM_H_ET600_ADDR].RegType = REG_TYPE_READ | REG_TYPE_WRITE;
    reg[DSM_OFS_VDM_H_ET600_ADDR].RegValueDefault = 0x0a;
    reg[DSM_OFS_VDM_H_ET600_ADDR].RegValueMask = 0xFF;

    // DSM_OFS_VDM_L_ET600_ADDR 0x6
    reg[DSM_OFS_VDM_L_ET600_ADDR].RegType = REG_TYPE_READ | REG_TYPE_WRITE;
    reg[DSM_OFS_VDM_L_ET600_ADDR].RegValueDefault = 0x0;
    reg[DSM_OFS_VDM_L_ET600_ADDR].RegValueMask = 0xFF;

    // DPTH_ET600_ADDR 0x7
    reg[DPTH_ET600_ADDR].RegType = REG_TYPE_READ | REG_TYPE_WRITE;
    reg[DPTH_ET600_ADDR].RegValueDefault = 0x80;
    reg[DPTH_ET600_ADDR].RegValueMask = 0x8F;

    // INT_CTRL_ET600_ADDR 0x8
    reg[INT_CTRL_ET600_ADDR].RegType = REG_TYPE_READ | REG_TYPE_WRITE;
    reg[INT_CTRL_ET600_ADDR].RegValueDefault = 0x2;
    reg[INT_CTRL_ET600_ADDR].RegValueMask = 0x3F;

    // INT_STUS_CLR_ET600_ADDR 0x9
    reg[INT_STUS_CLR_ET600_ADDR].RegType = REG_TYPE_READ | REG_TYPE_WRITE;
    reg[INT_STUS_CLR_ET600_ADDR].RegValueDefault = 0x00;
    reg[INT_STUS_CLR_ET600_ADDR].RegValueMask = 0xFF;

    // TEST_CTRL_ET600_ADDR 0xa
    reg[TEST_CTRL_ET600_ADDR].RegType = REG_TYPE_READ | REG_TYPE_WRITE;
    reg[TEST_CTRL_ET600_ADDR].RegValueDefault = 0x02;
    reg[TEST_CTRL_ET600_ADDR].RegValueMask = 0x6F;

    // OSC100K_CSR_ET600_ADDR 0xb
    reg[OSC100K_CSR_ET600_ADDR].RegType = REG_TYPE_READ | REG_TYPE_WRITE;
    reg[OSC100K_CSR_ET600_ADDR].RegValueDefault = 0x88;
    reg[OSC100K_CSR_ET600_ADDR].RegValueMask = 0x66;

    // OSC40M_CSR_ET600_ADDR 0xc
    reg[OSC40M_CSR_ET600_ADDR].RegType = REG_TYPE_READ | REG_TYPE_WRITE;

    reg[OSC40M_CSR_ET600_ADDR].RegValueDefault = 0x0;
    reg[OSC40M_CSR_ET600_ADDR].RegValueMask = 0x60;

    // CLK40M_CSR_ET600_ADDR 0xD
    reg[CLK40M_CSR_ET600_ADDR].RegType = REG_TYPE_READ | REG_TYPE_WRITE;

    reg[CLK40M_CSR_ET600_ADDR].RegValueDefault = 0;
    reg[CLK40M_CSR_ET600_ADDR].RegValueMask = 0x66;

    // OSC40M_CONF_ET600_ADDR 0xe
    reg[OSC40M_CONF_ET600_ADDR].RegType = REG_TYPE_READ | REG_TYPE_WRITE;
    reg[OSC40M_CONF_ET600_ADDR].RegValueDefault = 0x15;
    reg[OSC40M_CONF_ET600_ADDR].RegValueMask = 0xBF;

    // VREF_CTRL0_ET600_ADDR 0xf
    reg[VREF_CTRL0_ET600_ADDR].RegType = REG_TYPE_READ | REG_TYPE_WRITE;
    reg[VREF_CTRL0_ET600_ADDR].RegValueDefault = 0x84;
    reg[VREF_CTRL0_ET600_ADDR].RegValueMask = 0xFF;

    // CPDR_CSR_ET600_ADDR 0x10
    // reg[CPDR_CSR_ET600_ADDR].RegType = REG_TYPE_DISCARD;	// lei

    // LDO_CTRL1_ET600_ADDR 0x11
    reg[LDO_CTRL1_ET600_ADDR].RegType = REG_TYPE_READ | REG_TYPE_WRITE;
    reg[LDO_CTRL1_ET600_ADDR].RegValueDefault = 0x4;
    reg[LDO_CTRL1_ET600_ADDR].RegValueMask = 0x16;

    // LDO_CTRL2_ET600_ADDR 0x12
    // reg[LDO_CTRL2_ET600_ADDR].RegType = REG_TYPE_DISCARD;	// lei

    // VREF_CTRL1_ET600_ADDR 0x13
    reg[VREF_CTRL1_ET600_ADDR].RegType = REG_TYPE_READ | REG_TYPE_WRITE;
    reg[VREF_CTRL1_ET600_ADDR].RegValueDefault = 0x60;
    reg[VREF_CTRL1_ET600_ADDR].RegValueMask = 0xF2;

    // ARY_PWR_ET600_ADDR 0x14
    reg[ARY_PWR_ET600_ADDR].RegType = REG_TYPE_READ | REG_TYPE_WRITE;
    reg[ARY_PWR_ET600_ADDR].RegValueDefault = 0x40;
    reg[ARY_PWR_ET600_ADDR].RegValueMask = 0xFF;

    // ARY_ISEL_ET600_ADDR 0x15
    reg[ARY_ISEL_ET600_ADDR].RegType = REG_TYPE_READ | REG_TYPE_WRITE;
    reg[ARY_ISEL_ET600_ADDR].RegValueDefault = 0x1;
    reg[ARY_ISEL_ET600_ADDR].RegValueMask = 0x7F;

    // ARY_FPS_ET600_ADDR 0x16
    reg[ARY_FPS_ET600_ADDR].RegType = REG_TYPE_READ | REG_TYPE_WRITE;
    reg[ARY_FPS_ET600_ADDR].RegValueDefault = 0x42;
    reg[ARY_FPS_ET600_ADDR].RegValueMask = 0xFF;

    // ARY_IBUF_ET600_ADDR 0x17
    reg[ARY_IBUF_ET600_ADDR].RegType = REG_TYPE_READ | REG_TYPE_WRITE;
    reg[ARY_IBUF_ET600_ADDR].RegValueDefault = 0x7;
    reg[ARY_IBUF_ET600_ADDR].RegValueMask = 0x7F;

    // ADC_GAIN_ET600_ADDR
    reg[ADC_GAIN_ET600_ADDR].RegType = REG_TYPE_READ | REG_TYPE_WRITE;
    reg[ADC_GAIN_ET600_ADDR].RegValueDefault = 0x1;
    reg[ADC_GAIN_ET600_ADDR].RegValueMask = 0xFF;

    // ADC_CTRL0_ET600_ADDR
    reg[ADC_CTRL0_ET600_ADDR].RegType = REG_TYPE_READ | REG_TYPE_WRITE;
    reg[ADC_CTRL0_ET600_ADDR].RegValueDefault = 0;
    reg[ADC_CTRL0_ET600_ADDR].RegValueMask = 0xFF;

    // ADC_CTRL1_ET600_ADDR
    reg[ADC_CTRL1_ET600_ADDR].RegType = REG_TYPE_READ | REG_TYPE_WRITE;
    reg[ADC_CTRL1_ET600_ADDR].RegValueDefault = 0;
    reg[ADC_CTRL1_ET600_ADDR].RegValueMask = 0xFF;

    // ADC_CTRL2_ET600_ADDR
    reg[ADC_CTRL2_ET600_ADDR].RegType = REG_TYPE_READ | REG_TYPE_WRITE;
    reg[ADC_CTRL2_ET600_ADDR].RegValueDefault = 0x85;
    reg[ADC_CTRL2_ET600_ADDR].RegValueMask = 0x8F;

    // ANAOUT_ET600_ADDR 0x1c
    reg[ANAOUT_ET600_ADDR].RegType = REG_TYPE_READ | REG_TYPE_WRITE;
    reg[ANAOUT_ET600_ADDR].RegValueDefault = 0;
    reg[ANAOUT_ET600_ADDR].RegValueMask = 0xF;

    // ARY_CTRL 0x1d
    // reg[0x1D].RegType =  REG_TYPE_DISCARD;

    // ADC_SW_FPS_ET600_ADDR 0x1e
    reg[ADC_SW_FPS_ET600_ADDR].RegType = REG_TYPE_READ | REG_TYPE_WRITE;
    reg[ADC_SW_FPS_ET600_ADDR].RegValueDefault = 0x0;
    reg[ADC_SW_FPS_ET600_ADDR].RegValueMask = 0x3;

    // VSIG_CTRL_ET600_ADDR 0x1F
    reg[VSIG_CTRL_ET600_ADDR].RegType = REG_TYPE_READ | REG_TYPE_WRITE;
    reg[VSIG_CTRL_ET600_ADDR].RegValueDefault = 0xb;
    reg[VSIG_CTRL_ET600_ADDR].RegValueMask = 0x1F;

    // PWR_CTRL0_ET600_ADDR
    reg[PWR_CTRL0_ET600_ADDR].RegType = REG_TYPE_READ | REG_TYPE_WRITE;
    reg[PWR_CTRL0_ET600_ADDR].RegValueDefault = 0x80;
    reg[PWR_CTRL0_ET600_ADDR].RegValueMask = 0xF1;

    // PWR_CTRL1_ET600_ADDR
    reg[PWR_CTRL1_ET600_ADDR].RegType = REG_TYPE_READ | REG_TYPE_WRITE;
    reg[PWR_CTRL1_ET600_ADDR].RegValueDefault = 0xFF;
    reg[PWR_CTRL1_ET600_ADDR].RegValueMask = 0xFF;

    // PWR_CTRL2_ET600_ADDR
    reg[PWR_CTRL2_ET600_ADDR].RegType = REG_TYPE_READ | REG_TYPE_WRITE;
    reg[PWR_CTRL2_ET600_ADDR].RegValueDefault = 0xA8;
    reg[PWR_CTRL2_ET600_ADDR].RegValueMask = 0xEF;

    // PWR_CTRL3_ET600_ADDR
    reg[PWR_CTRL3_ET600_ADDR].RegType = REG_TYPE_READ | REG_TYPE_WRITE;
    reg[PWR_CTRL3_ET600_ADDR].RegValueDefault = 0x0;
    reg[PWR_CTRL3_ET600_ADDR].RegValueMask = 0x3;

    // PWR_STUS0_ET600_ADDR 0x24
    reg[PWR_STUS0_ET600_ADDR].RegType = REG_TYPE_READ;
    reg[PWR_STUS0_ET600_ADDR].RegValueDefault = 0xC0;
    reg[PWR_STUS0_ET600_ADDR].RegValueMask = 0xF1;

    // PWR_STUS1_ET600_ADDR 0x25
    reg[PWR_STUS1_ET600_ADDR].RegType = REG_TYPE_READ;
    reg[PWR_STUS1_ET600_ADDR].RegValueDefault = 0xFF;
    reg[PWR_STUS1_ET600_ADDR].RegValueMask = 0xFF;

    // PWR_STUS2_ET600_ADDR 0x26
    reg[PWR_STUS2_ET600_ADDR].RegType = REG_TYPE_READ;
    reg[PWR_STUS2_ET600_ADDR].RegValueDefault = 0x40;
    reg[PWR_STUS2_ET600_ADDR].RegValueMask = 0xC0;

    // PWR_CONFIG0_ET600_ADDR 0x27
    reg[PWR_CONFIG0_ET600_ADDR].RegType = REG_TYPE_READ | REG_TYPE_WRITE;
    reg[PWR_CONFIG0_ET600_ADDR].RegValueDefault = 0x13;
    reg[PWR_CONFIG0_ET600_ADDR].RegValueMask = 0xFF;

    // PWR_CONFIG1_ET600_ADDR 0x28
    reg[PWR_CONFIG1_ET600_ADDR].RegType = REG_TYPE_READ | REG_TYPE_WRITE;
    reg[PWR_CONFIG1_ET600_ADDR].RegValueDefault = 0x10;
    reg[PWR_CONFIG1_ET600_ADDR].RegValueMask = 0xFF;

    //  0x29

    // SSM_OFS_SCN_H_ET600_ADDR 0x2a
    reg[SSM_OFS_SCN_H_ET600_ADDR].RegType = REG_TYPE_READ | REG_TYPE_WRITE;
    reg[SSM_OFS_SCN_H_ET600_ADDR].RegValueDefault = 0x00;
    reg[SSM_OFS_SCN_H_ET600_ADDR].RegValueMask = 0xFF;

    // SSM_OFS_SCN_L_ET600_ADDR 0x2b
    reg[SSM_OFS_SCN_L_ET600_ADDR].RegType = REG_TYPE_READ | REG_TYPE_WRITE;
    reg[SSM_OFS_SCN_L_ET600_ADDR].RegValueDefault = 0x00;
    reg[SSM_OFS_SCN_L_ET600_ADDR].RegValueMask = 0xFF;

    // CALI_CSR0_ET600_ADDR 0x30
    reg[CALI_CSR0_ET600_ADDR].RegType = REG_TYPE_DISCARD;
    reg[CALI_CSR0_ET600_ADDR].RegValueDefault = 0x00;
    reg[CALI_CSR0_ET600_ADDR].RegValueMask = 0xF0;

    // VBT_OFS_CALI_ET600_ADDR 0x31
    reg[VBT_OFS_CALI_ET600_ADDR].RegType = REG_TYPE_READ | REG_TYPE_WRITE;
    reg[VBT_OFS_CALI_ET600_ADDR].RegValueDefault = 0x0;
    reg[VBT_OFS_CALI_ET600_ADDR].RegValueMask = 0xFF;

    // VBT_OFS_FW_ET600_ADD 0x32
    reg[VBT_OFS_FW_ET600_ADDR].RegType = REG_TYPE_READ | REG_TYPE_WRITE;
    reg[VBT_OFS_FW_ET600_ADDR].RegValueDefault = 0x0;
    reg[VBT_OFS_FW_ET600_ADDR].RegValueMask = 0xFF;

    // VBT_OFS_MRGN_ET600_ADDR 0x33
    reg[VBT_OFS_MRGN_ET600_ADDR].RegType = REG_TYPE_READ | REG_TYPE_WRITE;
    reg[VBT_OFS_MRGN_ET600_ADDR].RegValueDefault = 0x3c;
    reg[VBT_OFS_MRGN_ET600_ADDR].RegValueMask = 0xFF;

    // CALI_CSR1_ET600_ADDR 0x34
    reg[CALI_CSR1_ET600_ADDR].RegType = REG_TYPE_DISCARD;
    reg[CALI_CSR1_ET600_ADDR].RegValueDefault = 0x00;
    reg[CALI_CSR1_ET600_ADDR].RegValueMask = 0x7F;

    // VBT_OFS_COL_ADR_ET600_ADDR 0x35
    // reg[VBT_OFS_COL_ADR_ET600_ADDR].RegType = REG_TYPE_READ | REG_TYPE_WRITE;;
    // reg[VBT_OFS_COL_ADR_ET600_ADDR].RegValueDefault = 0x5a;
    // reg[VBT_OFS_COL_ADR_ET600_ADDR].RegValueMask = 0x7F;
    reg[VBT_OFS_COL_ET600_ADDR].RegType = REG_TYPE_READ | REG_TYPE_WRITE;  // lei
    reg[VBT_OFS_COL_ET600_ADDR].RegValueDefault = 0x5a;
    reg[VBT_OFS_COL_ET600_ADDR].RegValueMask = 0x7F;

    // VBT_CALI_RSLT_ET600_ADDR 0x36
    reg[VBT_CALI_RSLT_ET600_ADDR].RegType = REG_TYPE_READ;
    reg[VBT_CALI_RSLT_ET600_ADDR].RegValueDefault = 0x0;
    reg[VBT_CALI_RSLT_ET600_ADDR].RegValueMask = 0xFF;

    // VDM_GAIN_ET600_ADDR 0x37
    reg[VDM_GAIN_ET600_ADDR].RegType = REG_TYPE_READ | REG_TYPE_WRITE;
    reg[VDM_GAIN_ET600_ADDR].RegValueDefault = 0x3F;
    reg[VDM_GAIN_ET600_ADDR].RegValueMask = 0xFF;

    // VDM_SCL_ET600_ADDR 0x38
    reg[VDM_SCL_ET600_ADDR].RegType = REG_TYPE_READ | REG_TYPE_WRITE;
    reg[VDM_SCL_ET600_ADDR].RegValueDefault = 0x2;
    reg[VDM_SCL_ET600_ADDR].RegValueMask = 0xFF;

    // STAT_MEAN_H_ET600_ADDR to STAT_MEAN_L_ET600_ADDR
    for (i = STAT_MEAN_H_ET600_ADDR; i < STAT_MIN_L_ET600_ADDR + 1; i++) {
        reg[i].RegType = REG_TYPE_READ;
        reg[i].RegValueDefault = 0x0;
        reg[i].RegValueMask = 0xFF;
    }

    // FOD_CSR_ET600_ADDR 0x40
    reg[FOD_CSR_ET600_ADDR].RegType = REG_TYPE_READ | REG_TYPE_WRITE;
    reg[FOD_CSR_ET600_ADDR].RegValueDefault = 0x00;
    reg[FOD_CSR_ET600_ADDR].RegValueMask = 0x05;

    // FOD_CYCLE_H_ET600_ADDR
    reg[FOD_CYCLE_H_ET600_ADDR].RegType = REG_TYPE_READ | REG_TYPE_WRITE;
    reg[FOD_CYCLE_H_ET600_ADDR].RegValueDefault = 0x13;
    reg[FOD_CYCLE_H_ET600_ADDR].RegValueMask = 0xFF;

    // FOD_CYCLE_L_ET600_ADDR
    reg[FOD_CYCLE_L_ET600_ADDR].RegType = REG_TYPE_READ | REG_TYPE_WRITE;
    reg[FOD_CYCLE_L_ET600_ADDR].RegValueDefault = 0x87;
    reg[FOD_CYCLE_L_ET600_ADDR].RegValueMask = 0xFF;

    // FOD_GAIN_ET600_ADDR
    reg[FOD_GAIN_ET600_ADDR].RegType = REG_TYPE_READ | REG_TYPE_WRITE;
    reg[FOD_GAIN_ET600_ADDR].RegValueDefault = 0x0;
    reg[FOD_GAIN_ET600_ADDR].RegValueMask = 0xFF;

    // FOD_SUM_THH_LS_H_ET600_ADDR to FOD_BBND_THL_L_ET600_ADDR
    for (i = FOD_SUM_THH_LS_H_ET600_ADDR; i < FOD_BBND_THL_L_ET600_ADDR + 1; i++) {
        reg[i].RegType = REG_TYPE_READ | REG_TYPE_WRITE;
        reg[i].RegValueDefault = 0x0;
        reg[i].RegValueMask = 0xFF;
    }

    // FOD_SUM_LS_H_ET600_ADDR to FOD_SUM_BND_STUS_ET600_ADDR
    for (i = FOD_SUM_LS_H_ET600_ADDR; i < FOD_SUM_BND_STUS_ET600_ADDR + 1; i++) {
        reg[i].RegType = REG_TYPE_READ;
        reg[i].RegValueDefault = 0x0;
        reg[i].RegValueMask = 0xFF;
    }
// Todo
// add 0x80 to 0xaf
// add 0xf0 to 0xff
#endif
}
