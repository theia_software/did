#include <stdint.h>
#include <stdlib.h>

#include "egis_log.h"
#include "egis_sensor_parameter.h"
#include "et6xx_rw_reg.h"
#include "fpsensor_6XX_definition.h"
#include "isensor_api.h"
#include "plat_spi.h"

#define LOG_TAG "EGISFP-RECOVERY"

extern int g_adc_gain;
extern int fp_tz_secure_et6xx_sensor_init(void);

static int read_compare_reg_map(BYTE addr) {
    BYTE value = 0, ret;
    ret = et6xx_read_register(addr, &value);
    if (ret != EGIS_OK) {
        egislog_e("%s, read addr: 0x%x  fail ,ret = %d", __func__, addr, ret);
        return EGIS_COMMAND_FAIL;
    }
    SENSOR_REG map_reg = et6xx_get_map_reg(addr);
    if (map_reg.RegType == REG_TYPE_NONE) return EGIS_OK;
    if (map_reg.RegValue != (value & map_reg.RegValueMask)) {
        egislog_e("%s, read addr: 0x%x  value = 0x%x map = 0x%x", __func__, addr, value,
                  map_reg.RegValue);
        egislog_e("%s, read addr: 0x%x  RegType: %x", __func__, addr, map_reg.RegType);
        return EGIS_COMMAND_FAIL;
    }
    return EGIS_OK;
}

int et6xx_recovery(void);
int isensor_recovery(FP_MODE fp_mode) {
    int ret = EGIS_OK;
    // isensor_open(NULL);

    fp_tz_secure_et6xx_sensor_init();

    et6xx_recovery();
    switch (fp_mode) {
        case FP_MODE_DONT_CARE:
            // Do nothing
            break;

        case FP_SENSOR_MODE:
            ret = isensor_set_sensor_mode();
            break;

        case FP_DETECT_MODE:
            ret = isensor_set_detect_mode();
            break;

        case FP_NAVI_DETECT_MODE:
            ret = isensor_set_navigation_detect_mode();
            break;

        case FP_NAVI_SENSOR_MODE:
            ret = isensor_set_navigation_sensor_mode();
            break;
        default:
            ret = isensor_set_power_off();
            break;
    }
    if (ret != EGIS_OK) {
        egislog_e("%s, et5xx_set_vdm ret = %d", __func__, ret);
    }

    return ret;
}

int et6xx_check_reset(void) {
    BYTE value = 0;
    int ret = EGIS_COMMAND_FAIL;

    BYTE buf[0x22] = {0};

    if (polling_registry(STUS_ET600_ADDR, 0xA8, 0xFC) != EGIS_OK) {
        egislog_e("%s, polling_registry fail", __func__);
        goto command_fail;
    }
    if (polling_registry(DEV_ID0_ET600_ADDR, 0x6, 0xFF) != EGIS_OK) {
        goto command_fail;
    }
    ret = et6xx_read_register(DEV_ID0_ET600_ADDR, &value);
    if (ret != EGIS_OK || (value != 0x06)) {
        egislog_e("%s, addr[DEV_ID0] =0x%X, expect 6", __func__, value);
        goto command_fail;
    }
    ret = et6xx_read_burst_register(STUS_ET600_ADDR, 0x22, buf);

    if (buf[INT_STUS_CLR_ET600_ADDR] & 0x3)  // bit0|bit1  RST_STUS and EFT_STUS
    {
        egislog_e("%s, INT_STUS_CLR_ET600_ADDR = 0x%X", __func__, buf[INT_STUS_CLR_ET600_ADDR]);
        goto command_fail;
    }

    return EGIS_OK;

command_fail:
    return EGIS_COMMAND_FAIL;
}

static BOOL g_need_recovery = FALSE;

#define MAX_RECOVERY_TRY 5
int et6xx_check_and_recovery(FP_MODE fp_mode) {
    int ret = EGIS_COMMAND_FAIL;
    static int try
        = 0;

    if (!g_need_recovery) {
        if (EGIS_COMMAND_FAIL == et6xx_check_reset()) {
            g_need_recovery = TRUE;
#ifdef HW_HAWAII_Y6
            egis_device_reset();
#else
            return EGIS_ESD_NEED_RESET;
#endif
        }
        return EGIS_OK;
    }

    ret = isensor_recovery(fp_mode);
    if (ret == EGIS_OK) {
        g_need_recovery = FALSE;
        try
            = 0;
    } else {
        egislog_e("!! isensor_recovery retry", ret);
        try
            ++;
        ret = EGIS_ESD_NEED_RESET;
    }

    if (try > MAX_RECOVERY_TRY) {
        egislog_e("!! isensor_recovery fail %d, try=%d", ret, try);
        ret = EGIS_COMMAND_FAIL;
    }

    return ret;
}
int isensor_get_recovery_event(void) {
    return g_need_recovery;
}
