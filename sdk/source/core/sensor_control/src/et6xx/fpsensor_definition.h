#pragma once

/* Sensor Sensing type_definition
 * For ET 3 series sening type is single sening.
 * For ET 5 series both single and delta supports.
 */
#define DELTA_SENSING_MODE 0
#define SINGLE_SENSING_MODE 1

#define DEFAULT_SENSING_MODE DELTA_SENSING_MODE

#include "egis_definition.h"
#include "fpsensor_6XX_definition.h"
#define MAX_SENSOR_GAIN 32
