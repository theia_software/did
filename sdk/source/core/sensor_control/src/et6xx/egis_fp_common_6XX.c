/******************************************************************************\
|*                                                                            *|
|*  calibrate_et510                                                           *|
|*  Version: 0.9.5.12                                                         *|
|*  Date: 2015/12/16                                                          *|
|*  Revise Date: 2016/06/27                                                   *|
|*  Copyright (C) 2007-2016 Egis Technology Inc.                              *|
|*                                                                            *|
\******************************************************************************/
#include <stdint.h>
#include <stdlib.h>

#include "EgisAlgorithmAPI.h"
#include "common_definition.h"
#include "core_config.h"
#include "egis_fp_common_6XX.h"
#include "egis_sensor_parameter.h"
#include "et6xx_recovery.h"
#include "et6xx_rw_reg.h"
#include "fpsensor_6XX_definition.h"
#include "isensor_api.h"
#include "isensor_definition.h"
#include "plat_heap.h"
#include "plat_log.h"
#include "plat_spi.h"
#include "plat_time.h"
#include "sensor_param.h"

#ifndef _WINDOWS
#ifndef TZ_MODE
#include <unistd.h>
#endif
#endif

#define LOG_TAG "RBS-EGISFP"

#define EDGE_TRIGGER 0
#define LEVEL_TRIGGER 4

#define FOD_ACTIVE_HIGH 0
#define FOD_ACTIVE_LOW 1

#define ET6_BIG_ENDIAN 0x80
#define ET6_LITTLE_ENDIAN 0x00

#define ENDIAN_TYPE ET6_LITTLE_ENDIAN

#ifdef HW_HAWAII_Y6
#define FOD_TRIGGER_MODE EDGE_TRIGGER
#define INTERRUPT_POLARITY FOD_ACTIVE_HIGH
#else
#define FOD_TRIGGER_MODE LEVEL_TRIGGER
#define INTERRUPT_POLARITY FOD_ACTIVE_LOW
#endif

#ifdef _WINDOWS
#define TRIGGER_CASE LEVEL_TRIGGER
#else
#define TRIGGER_CASE LEVEL_TRIGGER
#endif

extern SensorParameter g_sensor_parameter;
SensorParameter cali_cd, *pCurrent_cd = &g_sensor_parameter;

static int g_sensing_mode = SINGLE_SENSING_MODE;
static unsigned short g_et6xx_16bits_raw_image[128 * 128];  // change this
FP_MODE g_fp_mode = FP_MODE_DONT_CARE;

#ifdef PROJECT_CS1_ET613
#define DEFAULT_SENSOR_ADC_GAIN 8
#define DEFAULT_SENSOR_OSC40M 27
#define DEFAULT_SENSOR_SPI_CLOCK 13
static BYTE g_VBT_OFS_Value = 0x08;
static BYTE g_VDM_Bypass = 0x00;
#else
#define DEFAULT_SENSOR_ADC_GAIN 32
#ifdef MTK_EVB
#define DEFAULT_SENSOR_OSC40M 43
#else
#define DEFAULT_SENSOR_OSC40M 38
#endif
#define DEFAULT_SENSOR_SPI_CLOCK 7
static BYTE g_VBT_OFS_Value = 0x00;
static BYTE g_VDM_Bypass = 0x00;
#endif

int g_spiclock = DEFAULT_SENSOR_SPI_CLOCK;
int g_osc40m = DEFAULT_SENSOR_OSC40M;
int g_crop = 0;  // for ET602 40 * 40
static int g_keep_T_dummy = 0;
BOOL g_refline_bad = FALSE;

int need_bypass_vbt = 0;

#define DATA_PATH_BYPASS_VBT (need_bypass_vbt ? 2 : 0)

unsigned short g_et6xx_background[128 * 128] = {0};

enum _PDN_ENTRY {
    PDN_ENTER,
    PDN_EXIT,
};

#ifdef __SENSOR_ET601__
//#define INT_TRIG	6
#define INT_TRIG(pol) (pol | TRIGGER_CASE)
static int g_et600_adc_gain = DEFAULT_SENSOR_ADC_GAIN;
#elif defined(__SENSOR_ET602__)
//#define INT_TRIG	4	// 0 : edge, 4 : level trigger
#define INT_TRIG(pol) (pol | TRIGGER_CASE)
int g_enable_ldo28 = 1;
int g_enable_ldo18 = 1;
int g_et600_adc_gain = DEFAULT_SENSOR_ADC_GAIN;
#elif defined(__SENSOR_ET603A__)
//#define INT_TRIG	4	// 0 : edge, 4 : level trigger
#define INT_TRIG(pol) (pol | TRIGGER_CASE)
int g_enable_ldo28 = 1;
int g_enable_ldo18 = 0;
static const int g_detect_mode_delay_def = 60;
static const int g_detect_mode_delay_64 = 60;
static const int g_detect_mode_delay_128 = 100;
static const int g_detect_mode_delay_256 = 100;
int g_et600_adc_gain = DEFAULT_SENSOR_ADC_GAIN;
#endif

static int gNVM_RV = 0;
static BYTE gNVM_VBT = 0;
static BYTE g_nvm_vbt_ref_tolerance = 2;
int fpsGetNVMRV() {
    return gNVM_RV;
}
int et6xx_check_nvram() {
    int i, ret;
    BYTE buf[64];
    unsigned short expSum = 0;
    unsigned short curSum = 0;
    if (gNVM_RV > 0) {
        return EGIS_OK;
    }
    ret = isensor_read_nvm(buf);
    if (ret != EGIS_OK) return EGIS_OK;
    egislog_d("@egis-NVRAM:[%x,%x,%x,%x,%x,%x,%x,%x,%x,%x,%x,%x,%x,%x,%x,%x]\n", buf[0x13],
              buf[0x14], buf[0x15], buf[0x16], buf[0x17], buf[0x18], buf[0x19], buf[0x1A],
              buf[0x1B], buf[0x1C], buf[0x1D], buf[0x1E], buf[0x1F], buf[0x20], buf[0x21],
              buf[0x22]);
    egislog_d("@egis-NVRAM:[%x,%x,%x,%x,%x,%x,%x,%x,%x,%x,%x,%x,%x,%x,%x,%x]\n", buf[0x23],
              buf[0x24], buf[0x25], buf[0x26], buf[0x27], buf[0x28], buf[0x29], buf[0x2A],
              buf[0x2B], buf[0x2C], buf[0x2D], buf[0x2E], buf[0x2F], buf[0x30], buf[0x31],
              buf[0x32]);

    expSum = ((unsigned short)buf[NVM_INDEX_CHECKSUM_HB] << 8) | buf[NVM_INDEX_CHECKSUM_LB];
    for (i = 0x13; i <= 0x30; i++) {
        curSum += buf[i];
    }
    if ((curSum & 0xFFFF) != expSum) {
        egislog_e("@egis-NVRAM[FAIL:%d]", curSum);
        return EGIS_NVRAM_CHECKSUM;
    }
    // if ((buf[NVM_INDEX_HW_DC_C] >= HW_DC_C_MIN &&
    //      buf[NVM_INDEX_HW_DC_C] <= HW_DC_C_MAX) &&
    //     (buf[NVM_INDEX_HW_TH] >= HW_TH_MIN &&
    //      buf[NVM_INDEX_HW_TH] <= HW_TH_MAX))
    // 	gNVM_MEAN_FROM_DCC_MAX =
    // 	    ((buf[NVM_INDEX_HW_DC_C] * MEAN_PER_DCC) -
    // 	     (buf[NVM_INDEX_HW_TH] - FOD_THH_DIFF) + 100);
    // else
    // 	egislog_e("@egis-NVRAM, over_range");
    // egislog_d("@egis-NVRAM, HW_DC_C:%d, HW_threshold:%d\n",
    // 	  buf[NVM_INDEX_HW_DC_C], buf[NVM_INDEX_HW_TH]);
    gNVM_RV = ((buf[NVM_INDEX_RV_H] << 8) | buf[NVM_INDEX_RV_L]);
    gNVM_VBT = (buf[NVM_INDEX_VBT]);
    egislog_i("@egis-NVRAM[OK!] RV=%d, CALI_VBT_RST=0x%x", gNVM_RV, gNVM_VBT);
    return EGIS_OK;
}
static int et600_stop_detect_mode(int hdev);

static void power_down(BOOL enter_exit) {
    BYTE value = 0;

    if (g_fp_mode == FP_DETECT_MODE) {
        egislog_d("%s, preivious detect_mode", __func__);
        et600_stop_detect_mode(0);
    }

    isensor_read_int_status(NULL);
    egislog_i("power %s, mode:%d", enter_exit ? "on" : "off", g_fp_mode);
    if (PDN_ENTER == enter_exit) {
        if (g_fp_mode != FP_STANDBY_MODE) {
            // Set Power Manager to PwerDown State
            et6xx_write_register(PWR_CTRL3_ET600_ADDR, 0x2);
            // Turn off osc 100K, CLK 100K
            et6xx_write_register(OSC100K_CSR_ET600_ADDR, 0x22);
            plat_wait_time(250);
            // trun off LDO28
            et6xx_read_register(PWR_CTRL0_ET600_ADDR, &value);
            value = value | 0x01;
            et6xx_write_register(PWR_CTRL0_ET600_ADDR, value);
            if (g_enable_ldo18) {
                // Enable LDO 18 output
                et6xx_write_register(PWR_LDO18_ET600_ADDR, 0x2);
            } else {
                // Disable LDO 18 output
                et6xx_write_register(PWR_LDO18_ET600_ADDR, 0x3);
            }
        }
        g_fp_mode = FP_STANDBY_MODE;
    } else if (PDN_EXIT == enter_exit) {
        if (g_fp_mode == FP_STANDBY_MODE) {
            if (g_enable_ldo18) {
                // Enable LDO18 output
                et6xx_write_register(PWR_LDO18_ET600_ADDR, 0x02);
            } else {
                // Disable LDO18 output
                et6xx_write_register(PWR_LDO18_ET600_ADDR, 0x3);
            }
            // Turn On LDO28
            et6xx_read_register(PWR_CTRL0_ET600_ADDR, &value);
            value = value & 0xFE;
            et6xx_write_register(PWR_CTRL0_ET600_ADDR, value);

            // Turn On OSC 100K, CLK 100K
            et6xx_write_register(OSC100K_CSR_ET600_ADDR, 0x44);
            plat_wait_time(250);
            // Set Power Manager to Power Up State
            et6xx_write_register(PWR_CTRL3_ET600_ADDR, 0x01);
        }
        g_fp_mode = FP_MODE_DONT_CARE;
    }
}

static void set_fod_static_regs(void) {
    // Set Single-sensing mode, bypass VDM and ADC calibration
    et6xx_write_register(DPTH_ET600_ADDR, 0x14 | (g_VDM_Bypass ? 0x08 : 0x00) | ENDIAN_TYPE);
    et6xx_write_register(FOD_GAIN_ET600_ADDR, g_sensor_parameter.detect_gain);
    // Set FOD suspend cycle
    et6xx_write_register(FOD_CYCLE_H_ET600_ADDR, 0x13);  // FOD Cycles High Bytes
    et6xx_write_register(FOD_CYCLE_L_ET600_ADDR, 0x87);  // FOD Cycles Low Bytes
}

static void set_frame_static_regs(void) {
    // Set Array Power Control Register to Single-Sensing
    et6xx_write_register(ARY_PWR_ET600_ADDR, 0x18);

    // ADC calibration for VDM
    et6xx_write_register(ADC_GAIN_ET600_ADDR, g_et600_adc_gain - 1);
    et6xx_write_register(VDM_GAIN_ET600_ADDR, g_et600_adc_gain - 1);
}

static int et6xx_set_clb(unsigned char* buff) {
    return io_6xx_set_clb(buff, SENSOR_HEIGHT + 1);
}
static int et6xx_get_clb(unsigned char* buff) {
    int ret = -1;
    BYTE reg_value = 0;

    // force datapath to Bigendian to recieve Big-endian Data.
    et6xx_read_register(DPTH_ET600_ADDR, &reg_value);
    et6xx_write_register(DPTH_ET600_ADDR, 0x1 << 7 | reg_value);

    et6xx_write_register(ACTN_ET600_ADDR, 0x10);
    ret = io_6xx_get_clb(buff, SENSOR_HEIGHT + 1);

    // rollback datapath.
    et6xx_write_register(DPTH_ET600_ADDR, reg_value);
    // no need ACTN for CLB (Sounder)
    // et6xx_write_register(ACTN_ET600_ADDR,  0x04);
    return ret;
}

int et6xx_get_vdm(unsigned char* buff) {
    int ret = -1;
    BYTE reg_value = 0;

    /*
     * No-need do below ,since we already K ADC (Sounder)
     */

    // kick ADC calibration FSM to Done stage.
    et6xx_read_register(CALI_CSR1_ET600_ADDR, &reg_value);
    et6xx_write_register(CALI_CSR1_ET600_ADDR, reg_value | 0x08);

    // kick VDM FSM to Done stage.
    et6xx_read_register(CALI_CSR0_ET600_ADDR, &reg_value);
    et6xx_write_register(CALI_CSR0_ET600_ADDR, reg_value | 0x08);
    //

    // force datapath to Bigendian to recieve Big-endian Data.
    et6xx_read_register(DPTH_ET600_ADDR, &reg_value);
    et6xx_write_register(DPTH_ET600_ADDR, 0x1 << 7 | reg_value);

    et6xx_write_register(ACTN_ET600_ADDR, 0x08);
    // mask = 0x80, exp = 0x80 (Wait VDM_CSR, STUS = 1)

    polling_registry(STUS_ET600_ADDR, 0x01, 0x01);

    ret = io_5xx_read_vdm(buff, 1,
                          ((SENSOR_WIDTH + 1) * (SENSOR_HEIGHT + 1) * 2));  // Read 103x52 VDM data

    et6xx_write_register(ACTN_ET600_ADDR, 0x04);

    // rollback datapath.
    et6xx_write_register(DPTH_ET600_ADDR, reg_value);
    return ret;
}

int et6xx_set_vdm(unsigned char* buff) {
    egislog_d("%s, set_vdm w:%d, h:%d", __func__, SENSOR_WIDTH + 1, SENSOR_HEIGHT + 1);

    return io_5xx_write_vdm(
        buff, 1, ((SENSOR_WIDTH + 1) * (SENSOR_HEIGHT + 1) * 2));  // Write 103x52 VDM data
}

int et6xx_recovery(void) {
    egislog_d("%s ", __func__);
    set_frame_static_regs();
    set_fod_static_regs();
    et6xx_set_clb((unsigned char*)g_sensor_parameter.clb_bk);
    et6xx_set_vdm((unsigned char*)g_sensor_parameter.vdm_bk);
    if (g_VBT_OFS_Value)
        et6xx_write_register(VBT_OFS_FW_ET600_ADDR, g_sensor_parameter.Ori_VBT_OFS_Value);
    else
        et6xx_write_register(VBT_OFS_FW_ET600_ADDR, g_sensor_parameter.WBK_VBT_OFS_Value);
    need_bypass_vbt = 1;

    BYTE reg_value = 0;
    et6xx_read_register(DPTH_ET600_ADDR, &reg_value);
    et6xx_write_register(DPTH_ET600_ADDR, reg_value | DATA_PATH_BYPASS_VBT);
    et6xx_write_register(VDM_SCL_ET600_ADDR, 0x00);

    // isensor_dump_data();
    return 0;
}

int fp_tz_secure_et6xx_sensor_init(void) {
    io_dispatch_connect(NULL);

    // Waiting for the device Available
    if (polling_registry(STUS_ET600_ADDR, 0xA8, 0xFC) != EGIS_OK) return EGIS_COMMAND_FAIL;

#ifdef __SENSOR_ET601__
#elif defined(__SENSOR_ET602__)
#elif defined(__SENSOR_ET603A__)
    // Set LDO output voltage
    et6xx_write_register(LDO_CTRL1_ET600_ADDR, 0x04);

    // Enable LDO28 output
    if (g_enable_ldo28)
        et6xx_write_register(VREF_CTRL1_ET600_ADDR, 0x63);
    else
        et6xx_write_register(VREF_CTRL1_ET600_ADDR, 0x60);
    if (g_enable_ldo18)
        et6xx_write_register(PWR_LDO18_ET600_ADDR, 0x02);
    else
        et6xx_write_register(PWR_LDO18_ET600_ADDR, 0x03);

    // clear RESET flag
    et6xx_write_register(INT_STUS_CLR_ET600_ADDR, 0x05);

    // Set Power Manager go to Power Up State
    et6xx_write_register(PWR_CTRL3_ET600_ADDR, 0x01);
    // Row Amp bias setting
    et6xx_write_register(ARY_ISEL_ET600_ADDR, 0x01);

    // VBT offset value from External SPI setting
    et6xx_write_register(VBT_OFS_FW_ET600_ADDR, 0xD6);

    // ADC control 2 disable bus26
    et6xx_write_register(ADC_CTRL2_ET600_ADDR, 0x05);

    // Set target OSC clock for SPI vs gain
    et6xx_write_register(OSC40M_CONF_ET600_ADDR, g_osc40m | 0x80);

    et6xx_check_nvram();
#endif

    return EGIS_OK;
}

int fp_tz_secure_sensor_init(void) {
    return fp_tz_secure_et6xx_sensor_init();
}

static int et600_stop_detect_mode(int hdev) {
#ifdef __SENSOR_ET601__
#elif defined(__SENSOR_ET602__) || defined(__SENSOR_ET603A__)
    //  Exit FOD Mode.

    BYTE value;

    et6xx_read_register(FOD_CSR_ET600_ADDR, &value);
    if ((value & 0x01) == 0x01) {
        et6xx_write_register(INT_STUS_CLR_ET600_ADDR, 0x4);
        et6xx_write_register(FOD_CSR_ET600_ADDR, 0x00);

        // Stop Scan Image.
        et6xx_write_register(ACTN_ET600_ADDR, 0x04);
    }
#endif

    return EGIS_OK;
}

int isensor_open(int* timeout) {
    int ret = EGIS_COMMAND_FAIL;

    egislog_i("isensor_open(), g_sensor_parameter.calibration_flag = %d",
              g_sensor_parameter.calibration_flag);
    ret = fp_tz_secure_et6xx_sensor_init();
    if (ret != EGIS_OK) return ret;  // Should not run to this line if sensor OK.
    ret = isensor_calibrate(FPS_CALI_INIT);
    egislog_i("isensor_open() ret = %d", ret);

#ifdef __SENSOR_ET601__
    et6xx_reg_map_init();
    et6xx_reg_map_fill();
#endif

    /* Use below code  before validate first.
        int RV = fpsGetNVMRV();
        egislog_d("%s, RV %d.", __func__, RV);
        */

    power_down(PDN_ENTER);

    return ret;
}

int isensor_set_feature(const char* param, int value) {
    if (param == NULL) {
        return EGIS_INCORRECT_PARAMETER;
    }

    if (strncmp(param, PARAM_UPDATE_CONFIG, sizeof(PARAM_UPDATE_CONFIG)) == 0) {
        g_spiclock = core_config_get_int(INI_SECTION_SENSOR, "SPICLOCK", DEFAULT_SENSOR_SPI_CLOCK);
        g_osc40m = core_config_get_int(INI_SECTION_SENSOR, "OSC40M", DEFAULT_SENSOR_OSC40M);
        g_et600_adc_gain =
            core_config_get_int(INI_SECTION_SENSOR, "ADC_GAIN", DEFAULT_SENSOR_ADC_GAIN);
        g_crop = core_config_get_int(INI_SECTION_SENSOR, "CROP", 0);
        g_sensing_mode =
            core_config_get_int(INI_SECTION_SENSOR, "SENSING_MODE", DEFAULT_SENSING_MODE);
        g_VBT_OFS_Value =
            core_config_get_int(INI_SECTION_SENSOR, KEY_VBT_OFS_VALUE, g_VBT_OFS_Value);
        g_nvm_vbt_ref_tolerance = core_config_get_int(INI_SECTION_SENSOR, KEY_NVM_VBT_REF_TOLERANCE,
                                                      g_nvm_vbt_ref_tolerance);
        int keep_T_dummy = core_config_get_int(INI_SECTION_SENSOR, KEY_KEEP_T_DUMMY, 0);
        if (g_keep_T_dummy != keep_T_dummy) {
            SENSOR_WIDTH = 0;
            SENSOR_HEIGHT = 0;
            g_keep_T_dummy = keep_T_dummy;
        }
#ifdef _WINDOWS
        set_spi_config(3, g_spiclock);
#endif
        return EGIS_OK;
    }

    return EGIS_OK;
}

int isensor_get_et6xx_sensor_size(int* width, int* height) {
    if (SENSOR_WIDTH != 0 && SENSOR_HEIGHT != 0) {
        *width = SENSOR_WIDTH;
        *height = SENSOR_HEIGHT;
        return EGIS_OK;
    }
    g_keep_T_dummy = core_config_get_int(INI_SECTION_SENSOR, KEY_KEEP_T_DUMMY, 0);

    io_dispatch_connect(NULL);
    et6xx_read_register(REV_ID_ET600_ADDR, &g_egis_sensortype.rev_id);
    et6xx_read_register(DEV_ID0_ET600_ADDR, &g_egis_sensortype.dev_id0);
    et6xx_read_register(DEV_ID1_ET600_ADDR, &g_egis_sensortype.dev_id1);
    egislog_i("%s, Sensor ID=%d %d-%d", __func__, g_egis_sensortype.series, g_egis_sensortype.type,
              g_egis_sensortype.rev_id);

    if (g_egis_sensortype.series != 0x06) {
        egislog_e("%s, Read_register FAIL, Sensor ID=%d %d-%d", __func__, g_egis_sensortype.series,
                  g_egis_sensortype.type, g_egis_sensortype.rev_id);
        return EGIS_NO_DEVICE;
    }

    switch (g_egis_sensortype.type) {
        case ET601:
            SENSOR_WIDTH = 114;
            SENSOR_HEIGHT = 74;
            NAV_WINDOW_W = 96;
            NAV_WINDOW_H = 40;
            NAV_REF_W = 72;
            NAV_REF_H = 32;
            break;
        case ET602:
            if (g_crop) {
                SENSOR_WIDTH = 40;
                SENSOR_HEIGHT = 40;
                NAV_WINDOW_W = 40;
                NAV_WINDOW_H = 40;
                NAV_REF_W = 40;
                NAV_REF_H = 40;
            } else {
                SENSOR_WIDTH = 57;
                SENSOR_HEIGHT = 46;
                NAV_WINDOW_W = 57;
                NAV_WINDOW_H = 46;
                NAV_REF_W = 57;
                NAV_REF_H = 46;
            }
            break;
        case ET613:
            SENSOR_WIDTH = 120;
            SENSOR_HEIGHT = 28;
            if (g_keep_T_dummy) {
                SENSOR_WIDTH = 121;
                SENSOR_HEIGHT = 29;
            }
            NAV_WINDOW_W = 120;
            NAV_WINDOW_H = 28;
            NAV_REF_W = 120;
            NAV_REF_H = 28;
            break;
        default:
            egislog_e("%s, Unknow Sensor type", __func__);
            return EGIS_NO_DEVICE;
            break;
    }
    *width = SENSOR_WIDTH;
    *height = SENSOR_HEIGHT;
    egislog_d("%s, Sensor type = %d, (%d*%d)", __func__, g_egis_sensortype.type, SENSOR_WIDTH,
              SENSOR_HEIGHT);
    io_dispatch_disconnect();

    return EGIS_OK;
}

static int et6xx_check_cali_flag(void) {
#ifdef POLLING
    g_sensor_parameter.calibration_flag = DO_CALIBRATION_DVR;
#else
    g_sensor_parameter.calibration_flag = DO_CALIBRATION_DVR | DO_CALIBRATION_INTERRUPT;
#endif
    return EGIS_OK;
}

int et600_fetch_statistic(unsigned char* buf) {
    et6xx_write_register(CALI_CSR0_ET600_ADDR, 1 << 2);
    if (polling_registry(CALI_CSR0_ET600_ADDR, 1 << 6, 1 << 6) != EGIS_OK) return EGIS_COMMAND_FAIL;
    return et6xx_read_burst_register(STAT_MEAN_H_ET600_ADDR, 6, buf);
}

int et6xx_get_frame_statistic(unsigned short* max, unsigned short* min, unsigned short* mean) {
    unsigned char buf[6];
    // isensor_set_custom_et6xx_sensor_mode(g_sensing_mode);
    int ret = et600_fetch_statistic((unsigned char*)buf);
    for (int i = 0; i < 6; i++) egislog_d("%s,buf[] 0x%x", __func__, buf[i]);

    *mean = (unsigned short)((buf[0] << 8) + buf[1]);
    *max = (unsigned short)((buf[2] << 8) + buf[3]);
    *min = (unsigned short)((buf[4] << 8) + buf[5]);
    egislog_d("%s, mean:%d, max: %d, min: %d", __func__, *mean, *max, *min);
    return ret;
}

int et6xx_calibrate_sensor_mode(void) {
    int value, ret;
    BYTE hi, low;
    ET6XXCalibrationData* cd = &g_sensor_parameter;

    if (g_fp_mode == FP_DETECT_MODE) {
        egislog_d("%s, preivious detect_mode", __func__);
        ret = et600_stop_detect_mode(0);
        if (ret != EGIS_OK) return ret;
    }
    g_fp_mode = FP_MODE_DONT_CARE;

    if (g_sensing_mode == DELTA_SENSING_MODE) {
        et6xx_write_register(ARY_PWR_ET600_ADDR, 0x50);
        et6xx_write_register(DPTH_ET600_ADDR, 0x00 | ENDIAN_TYPE);
        et6xx_write_register(ADC_GAIN_ET600_ADDR, g_et600_adc_gain - 1);
        et6xx_write_register(VDM_GAIN_ET600_ADDR, g_et600_adc_gain - 1);
        et6xx_write_register(VDM_SCL_ET600_ADDR, 0x00);

        // Start Calibration ADC for VDM
        et6xx_write_register(CALI_CSR0_ET600_ADDR, 0x01);
        if (polling_registry(STUS_ET600_ADDR, 0x02, 0x02) != EGIS_OK) return EGIS_COMMAND_FAIL;

        // Update VDM
        et6xx_write_register(CALI_CSR0_ET600_ADDR, 0x02);
        if (polling_registry(CALI_CSR0_ET600_ADDR, 0x20, 0x20) != EGIS_OK) return EGIS_COMMAND_FAIL;
    } else {
        et6xx_write_register(ARY_PWR_ET600_ADDR, 0x18);
        et6xx_write_register(DPTH_ET600_ADDR, 0x14 | ENDIAN_TYPE);
        et6xx_write_register(ADC_GAIN_ET600_ADDR, g_et600_adc_gain - 1);
        et6xx_write_register(VDM_GAIN_ET600_ADDR, g_et600_adc_gain - 1);
        et6xx_write_register(VDM_SCL_ET600_ADDR, 0x00);

        // Start Calibration ADC for VDM
        et6xx_write_register(CALI_CSR0_ET600_ADDR, 0x01);
        if (polling_registry(STUS_ET600_ADDR, 0x02, 0x02) != EGIS_OK) return EGIS_COMMAND_FAIL;

        // Kick VDM to done state
        et6xx_write_register(CALI_CSR0_ET600_ADDR, 0x08);

        // Bypass VDm
        et6xx_write_register(DPTH_ET600_ADDR, 0x1C | ENDIAN_TYPE);  // little endian

        // Do statistic
        et6xx_write_register(CALI_CSR0_ET600_ADDR, 0x04);
        if (polling_registry(CALI_CSR0_ET600_ADDR, 0x40, 0x40) != EGIS_OK) return EGIS_COMMAND_FAIL;

        // Read statistic minimun value
        et6xx_read_register(STAT_MIN_H_ET600_ADDR, &hi);
        et6xx_read_register(STAT_MIN_L_ET600_ADDR, &low);
        value = ((hi << 8) + low) * 3 / 4;

        // Set Single-Sensing mode offset to Min*3/4
        et6xx_write_register(SSM_OFS_SCN_H_ET600_ADDR, value >> 8);
        et6xx_write_register(SSM_OFS_SCN_L_ET600_ADDR, value & 0x00FF);

        // Update VDM
        et6xx_write_register(CALI_CSR0_ET600_ADDR, 0x02);
        if (polling_registry(CALI_CSR0_ET600_ADDR, 0x20, 0x20) != EGIS_OK) return EGIS_COMMAND_FAIL;

        // Rollback SS mode offset
        et6xx_write_register(SSM_OFS_SCN_H_ET600_ADDR, 0x00);
        et6xx_write_register(SSM_OFS_SCN_L_ET600_ADDR, 0x00);
    }

    // if (!cd->is_already_get_vdm_clb) {
    memset(cd->vdm_bk, 0x0, sizeof(cd->vdm_bk));
    memset(cd->clb_bk, 0x0, sizeof(cd->clb_bk));
    et6xx_get_clb((unsigned char*)cd->clb_bk);
    et6xx_get_vdm((unsigned char*)cd->vdm_bk);  // get original one.
    cd->is_already_get_vdm_clb = 1;
    cd->VBT_OPTION = 0;
    //}

    cd->sensor_gain = g_et600_adc_gain;

    return EGIS_OK;
}

typedef enum _fod_th_gen_type {
    GEN_THH,
    GEN_THL,
    GEN_REK_THH,
    GEN_REK_THL,
} fod_th_gen_type;

statistic_value_601_t fod_gen_th(statistic_value_601_t mean, fod_th_gen_type gen_type,
                                 unsigned int threshold_delta) {
    switch (gen_type) {
        case GEN_THH:
            mean.r += threshold_delta;
            mean.l += threshold_delta;
            break;
        case GEN_THL:
            mean.r -= threshold_delta;
            mean.l -= threshold_delta;
            break;
        case GEN_REK_THH:
            if (mean.r <= g_sensor_parameter.fod_th.calibrated_thh.r) {
                egislog_d("skip adjust thh.r");
                mean.r = g_sensor_parameter.fod_th.calibrated_thh.r;
            } else
                mean.r += threshold_delta;

            if (mean.l <= g_sensor_parameter.fod_th.calibrated_thh.l) {
                egislog_d("skip adjust thh.l");
                mean.l = g_sensor_parameter.fod_th.calibrated_thh.l;
            } else
                mean.l += threshold_delta;

            break;
        case GEN_REK_THL:
            if (mean.r <= g_sensor_parameter.fod_th.calibrated_thl.r + 1000) {
                egislog_d("skip adjust thl.r");
                mean.r = g_sensor_parameter.fod_th.calibrated_thl.r;
            } else
                mean.r -= threshold_delta;
            if (mean.l <= g_sensor_parameter.fod_th.calibrated_thl.l + 1000) {
                egislog_d("skip adjust thl.l");
                mean.l = g_sensor_parameter.fod_th.calibrated_thl.l;
            } else
                mean.l -= threshold_delta;
            break;
    }
    return mean;
}

static void isensor_get_detect_mean(unsigned int* pmean_left, unsigned int* pmean_right) {
    BYTE buf[6] = {0};
    uint32_t mean_left = 0, mean_right = 0;

    // // Do FOD HW Statistic.
#if TRIGGER_CASE == EDGE_TRIGGER
    et6xx_write_register(FOD_CSR_ET600_ADDR, 0x2);     // Start FOD HW Statistic.
    polling_registry(FOD_CSR_ET600_ADDR, 0x80, 0x80);  // Polling FOD HW Statistic Ready.
#endif

    // Update Statistic value.
    et6xx_read_burst_register(FOD_SUM_LS_H_ET600_ADDR, 6, buf);

    mean_left = (buf[0] << 0x10) + (buf[1] << 0x8) + buf[2];
    mean_right = (buf[3] << 0x10) + (buf[4] << 0x8) + buf[5];
    egislog_d("%s, mean_left = %d", __func__, mean_left);
    egislog_d("%s, mean_right = %d", __func__, mean_right);

    if ((pmean_left != NULL) && (pmean_right != NULL)) {
        *pmean_left = mean_left;
        *pmean_right = mean_right;
    }
}

static int et6xx_write_fod_threshold(statistic_value_601_t thh, statistic_value_601_t thl) {
    BYTE buf[12] = {0};
    buf[0] = thh.l >> 0x10;
    buf[1] = thh.l >> 0x08;
    buf[2] = thh.l & 0xFF;
    buf[3] = thl.l >> 0x10;
    buf[4] = thl.l >> 0x08;
    buf[5] = thl.l & 0xFF;
    buf[6] = thh.r >> 0x10;
    buf[7] = thh.r >> 0x08;
    buf[8] = thh.r & 0xFF;
    buf[9] = thl.r >> 0x10;
    buf[10] = thl.r >> 0x08;
    buf[11] = thl.r & 0xFF;
    return et6xx_write_burst_register(FOD_SUM_THH_LS_H_ET600_ADDR, 12, buf);
}
int et6xx_calibrate_detect_mode(BOOL toWaitForTHL) {
#ifdef __SENSOR_ET601__
#elif defined(__SENSOR_ET602__)
#elif defined(__SENSOR_ET603A__)
    int thh_offset = 4000, thl_offset = 1000;

    statistic_value_601_t mean, thh, thl;

    ET6XXCalibrationData* cd = toWaitForTHL ? &cali_cd : &g_sensor_parameter;
    fod_param_t* fod_th = &cd->fod_th;

    cd->detect_gain = DETECT_GAIN;

    et6xx_write_register(ARY_PWR_ET600_ADDR, 0x18);
    if (g_VBT_OFS_Value == 0) {
        et6xx_write_register(DPTH_ET600_ADDR, 0x14 | (g_VDM_Bypass ? 0x08 : 0x00) | ENDIAN_TYPE |
                                                  DATA_PATH_BYPASS_VBT);
    } else if (cd->VBT_OPTION) {
        // Change Vbt setting to original value
        et6xx_write_register(VBT_OFS_FW_ET600_ADDR, cd->Ori_VBT_OFS_Value);
        et6xx_write_register(DPTH_ET600_ADDR, 0x16 | (g_VDM_Bypass ? 0x08 : 0x00) | ENDIAN_TYPE |
                                                  DATA_PATH_BYPASS_VBT);
    } else {
        et6xx_write_register(VBT_OFS_FW_ET600_ADDR, cd->Ori_VBT_OFS_Value);
        et6xx_write_register(DPTH_ET600_ADDR, 0x16 | (g_VDM_Bypass ? 0x08 : 0x00) | ENDIAN_TYPE |
                                                  DATA_PATH_BYPASS_VBT);
    }

    // Finger-On Detection
    et6xx_write_register(INT_STUS_CLR_ET600_ADDR, 0x04);
    et6xx_write_register(INT_CTRL_ET600_ADDR, 0x02 | INT_TRIG(1));
    et6xx_write_register(FOD_GAIN_ET600_ADDR, cd->detect_gain);

#if 1
    et6xx_write_register(FOD_CYCLE_H_ET600_ADDR, 0x00);  // FOD Cycles High Bytes
    et6xx_write_register(FOD_CYCLE_L_ET600_ADDR, 0xC8);  // FOD Cycles Low Bytes

    et6xx_write_register(FOD_CSR_ET600_ADDR, 0x01);
    plat_wait_time(10);
    et6xx_write_register(FOD_CSR_ET600_ADDR, 0x00);
    et6xx_write_register(ACTN_ET600_ADDR, 0x04);  // reset scan state
#else
    // Do FOD statistic
    et6xx_write_register(FOD_CSR_ET600_ADDR, 0x02);
    if (polling_registry(FOD_CSR_ET600_ADDR, 0x80, 0x80) != EGIS_OK) return EGIS_COMMAND_FAIL;

#endif

    et6xx_write_register(FOD_CYCLE_H_ET600_ADDR, 0x13);  // FOD Cycles High Bytes
    et6xx_write_register(FOD_CYCLE_L_ET600_ADDR, 0x87);  // FOD Cycles Low Bytes

#if 1
    isensor_get_detect_mean(&mean.l, &mean.r);

    thh = fod_gen_th(mean, toWaitForTHL ? GEN_REK_THH : GEN_THH, toWaitForTHL ? 10000 : thh_offset);
    thl = fod_gen_th(mean, toWaitForTHL ? GEN_REK_THL : GEN_THL, thl_offset);

    if (toWaitForTHL) {
        egislog_d("[6xx] RE-k new threshold");
        egislog_d("[6xx] RE-k, THH r = %d", thh.r);
        egislog_d("[6xx] RE-k, THH l = %d", thh.l);
        egislog_d("[6xx] RE-k, THL r = %d", thl.r);
        egislog_d("[6xx] RE-k, THL l = %d", thl.l);
    } else {
        egislog_d("[6xx] threshold");
        egislog_d("[6xx] Original, THH r = %d", thh.r);
        egislog_d("[6xx] Original, THH l = %d", thh.l);
        egislog_d("[6xx] ");
        egislog_d("[6xx] Original, THL r = %d", thl.r);
        egislog_d("[6xx] Original, THL l = %d", thl.l);
    }

    fod_th->calibrated_mean = mean;
    fod_th->calibrated_thh = thh;
    fod_th->calibrated_thl = thl;
    fod_th->threshold_delta_value = thh_offset;

    et6xx_write_fod_threshold(fod_th->calibrated_thh, fod_th->calibrated_thl);

#else
    int thh_left, thl_left, thh_right, thl_right;
    BYTE high, medium, low;
    int mean_left, mean_right;

    et6xx_read_register(FOD_SUM_LS_H_ET600_ADDR, &high);
    et6xx_read_register(FOD_SUM_LS_M_ET600_ADDR, &medium);
    et6xx_read_register(FOD_SUM_LS_L_ET600_ADDR, &low);
    mean_left = (high << 0x10) | (medium << 0x08) | (low & 0xFF);

    et6xx_read_register(FOD_SUM_RS_H_ET600_ADDR, &high);
    et6xx_read_register(FOD_SUM_RS_M_ET600_ADDR, &medium);
    et6xx_read_register(FOD_SUM_RS_L_ET600_ADDR, &low);
    mean_right = (high << 0x10) | (medium << 0x08) | (low & 0xFF);

    thh_left = mean_left + thh_offset;
    thl_left = mean_left - thl_offset;
    thh_right = mean_right + thh_offset;
    thl_right = mean_right - thl_offset;

    et6xx_write_register(FOD_SUM_THH_LS_H_ET600_ADDR, (BYTE)(thh_left >> 0x10));
    et6xx_write_register(FOD_SUM_THH_LS_M_ET600_ADDR, (BYTE)(thh_left >> 0x08));
    et6xx_write_register(FOD_SUM_THH_LS_L_ET600_ADDR, (BYTE)(thh_left & 0xFF));
    et6xx_write_register(FOD_SUM_THL_LS_H_ET600_ADDR, (BYTE)(thl_left >> 0x10));
    et6xx_write_register(FOD_SUM_THL_LS_M_ET600_ADDR, (BYTE)(thl_left >> 0x08));
    et6xx_write_register(FOD_SUM_THL_LS_L_ET600_ADDR, (BYTE)(thl_left & 0xFF));

    et6xx_write_register(FOD_SUM_THH_RS_H_ET600_ADDR, (BYTE)(thh_right >> 0x10));
    et6xx_write_register(FOD_SUM_THH_RS_M_ET600_ADDR, (BYTE)(thh_right >> 0x08));
    et6xx_write_register(FOD_SUM_THH_RS_L_ET600_ADDR, (BYTE)(thh_right & 0xFF));
    et6xx_write_register(FOD_SUM_THL_RS_H_ET600_ADDR, (BYTE)(thl_right >> 0x10));
    et6xx_write_register(FOD_SUM_THL_RS_M_ET600_ADDR, (BYTE)(thl_right >> 0x08));
    et6xx_write_register(FOD_SUM_THL_RS_L_ET600_ADDR, (BYTE)(thl_right & 0xFF));
#endif

#endif

    return EGIS_OK;
}

static int et6xx_calc_background() {
    const int NUMBER_OF_FRAME = 10;
    unsigned short tmp_raw_image[128 * 128];
    int i, j;
    int image_size = SENSOR_WIDTH * SENSOR_HEIGHT;
    unsigned short* p_src;
    unsigned short* p_dest;

    memset(g_et6xx_background, 0, sizeof(g_et6xx_background));
    for (i = 0; i < NUMBER_OF_FRAME; i++) {
        if (isensor_get_dynamic_frame_16bit(tmp_raw_image, SENSOR_HEIGHT, SENSOR_WIDTH, 1) ==
            EGIS_COMMAND_FAIL) {
            egislog_e("%s failed [%d]", __func__, i);
        }
        p_src = tmp_raw_image;
        p_dest = g_et6xx_background;
        for (j = 0; j < image_size; j++) {
            *p_dest++ += *p_src++;
        }
    }

    p_dest = g_et6xx_background;
    for (j = 0; j < image_size; j++) {
        *p_dest = *p_dest / NUMBER_OF_FRAME;
        p_dest++;
    }

    return EGIS_OK;
}
static void update_current_cd(SensorParameter* cd) {
    if (cd == &g_sensor_parameter)
        egislog_d("g_sensor_parameter -> cd", __func__);
    else if (cd == &cali_cd)
        egislog_d("cali_cd -> cd", __func__);
    pCurrent_cd = cd;
}

#define VBT_OFS_NO_FINGER ((gNVM_VBT == 0) ? 0xFF : (gNVM_VBT + g_nvm_vbt_ref_tolerance))

int isensor_calibrate(FPS_CALIBRATE_OPTION option) {
    egislog_d("isensor_calibrate %d", option);
    int ret = EGIS_COMMAND_FAIL;

    switch (option) {
        case FPS_CALI_SDK_THH_FALSE_TRIGGER:
            memcpy(&cali_cd, &g_sensor_parameter, sizeof(SensorParameter));
            ret = et6xx_calibrate_detect_mode(TRUE);
            update_current_cd(&cali_cd);
            break;

        case FPS_CALI_SDK_CHECK_FINGER_REMOVE:
            // if (GetNeedReKVDM())
            //	et6xx_check_cali_flag();
            // else if ((g_sensor_parameter.calibration_flag & 0x03) ==
            //	DO_CALIBRATION_COMPLETE)
#if 1
            memcpy(&cali_cd, &g_sensor_parameter, sizeof(SensorParameter));
            ret = et6xx_calibrate_detect_mode(TRUE);
            update_current_cd(&cali_cd);
#endif

            break;  // Nothing to do
                    // Continue to do FPS_CALI_INIT
        case FPS_CALI_SDK_THL:
        case FPS_CALI_INIT:
            if (pCurrent_cd == &cali_cd) {
                update_current_cd(&g_sensor_parameter);
                break;
            }
            et6xx_check_cali_flag();
            if (g_sensor_parameter.calibration_flag & DO_CALIBRATION_DVR) {
                ret = et6xx_calibrate_sensor_mode();
                // Get Vbt value
                BYTE value;
                et6xx_read_register(VBT_CALI_RSLT_ET600_ADDR, &value);
                egislog_d("VBT value = 0x%x", value);
                g_sensor_parameter.Ori_VBT_OFS_Value = value;
                g_sensor_parameter.WBK_VBT_OFS_Value = value + g_VBT_OFS_Value;

#if 0
			egislog_d("statistic mean calibration");
			et6xx_get_frame_statistic(&max, &min, &mean);

			egislog_d("statistic mean calibration skip vdm");
			et6xx_write_register(DPTH_ET600_ADDR, 0x0 |  /*vdm_by_pass*/ 0x08  | ENDIAN_TYPE | /* no SKIP VBT*/  0);
			et6xx_get_frame_statistic(&max, &min, &mean);
#endif
#if 1

                if (g_sensor_parameter.Ori_VBT_OFS_Value > VBT_OFS_NO_FINGER) {
                    egislog_i("Apply NV VBT: 0x%x", gNVM_VBT);
                    g_sensor_parameter.Ori_VBT_OFS_Value = gNVM_VBT;
                    g_sensor_parameter.WBK_VBT_OFS_Value = gNVM_VBT + g_VBT_OFS_Value;
                    g_sensor_parameter.VBT_OPTION = 1;
                } else {
                    g_sensor_parameter.VBT_OPTION = 0;
                }
#endif
#if 0
			egislog_d("statistic mean calibration skip vdm| VBt");
			et6xx_write_register(VBT_OFS_FW_ET600_ADDR, 0x60);
			et6xx_write_register(DPTH_ET600_ADDR, 0x0 |  /*vdm_by_pass*/ 0x08  | ENDIAN_TYPE | /* no SKIP VBT*/  2);
			et6xx_get_frame_statistic(&max, &min, &mean);
#endif
                // isensor_set_custom_et6xx_sensor_mode(g_sensing_mode);
                // et6xx_calc_background();
            }
            if (g_sensor_parameter.calibration_flag & DO_CALIBRATION_INTERRUPT) {
                ret = et6xx_calibrate_detect_mode(FALSE);
            }
            g_sensor_parameter.calibration_flag = DO_CALIBRATION_COMPLETE;
            update_current_cd(&g_sensor_parameter);
            /*
                    if compare_cd_clean(original_cd)
                    {
                        memcpy(&original_cd, &g_sensor_parameter, sizeof(SensorParameter));
                        swap_cd();
                    }
            */

            break;
        default:
            break;
    }

    return ret;
}

int isensor_set_detect_mode() {
    // ET6XXCalibrationData *cd = &g_sensor_parameter;

    egislog_d("%s", __func__);
    int ret = 0;

    ret = et6xx_check_and_recovery(FP_MODE_DONT_CARE);
    if (EGIS_OK != ret) {
        egislog_e("%s, do reset", __func__);
        return ret;
    }

    if (g_fp_mode == FP_STANDBY_MODE) {
        power_down(PDN_EXIT);
    }

    if (g_fp_mode == FP_DETECT_MODE) {
        egislog_d("%s, preivious detect_mode", __func__);
        ret = et600_stop_detect_mode(0);
        if (ret != EGIS_OK) return ret;
    }

    g_fp_mode = FP_DETECT_MODE;

    // set osc40m to default for power consumption
    et6xx_write_register(OSC40M_CONF_ET600_ADDR, 0x95);

    et6xx_write_register(ARY_PWR_ET600_ADDR, 0x18);

    if (g_VBT_OFS_Value == 0) {
        et6xx_write_register(DPTH_ET600_ADDR, 0x14 | (g_VDM_Bypass ? 0x08 : 0x00) | ENDIAN_TYPE |
                                                  DATA_PATH_BYPASS_VBT);
    } else if (pCurrent_cd->VBT_OPTION) {
        egislog_d("go through VBT_OPTION, Ori_VBT_OFS_Value = 0x%x",
                  pCurrent_cd->Ori_VBT_OFS_Value);
        // Change Vbt setting to original value
        et6xx_write_register(VBT_OFS_FW_ET600_ADDR, pCurrent_cd->Ori_VBT_OFS_Value);
        et6xx_write_register(DPTH_ET600_ADDR, 0x16 | (g_VDM_Bypass ? 0x08 : 0x00) | ENDIAN_TYPE |
                                                  DATA_PATH_BYPASS_VBT);
    } else {
        // Change Vbt setting to original value
        et6xx_write_register(VBT_OFS_FW_ET600_ADDR, pCurrent_cd->Ori_VBT_OFS_Value);
        et6xx_write_register(DPTH_ET600_ADDR, 0x16 | (g_VDM_Bypass ? 0x08 : 0x00) | ENDIAN_TYPE |
                                                  DATA_PATH_BYPASS_VBT);
    }

    et6xx_write_register(INT_CTRL_ET600_ADDR, 0x02 | INT_TRIG(1));
    et6xx_write_register(INT_STUS_CLR_ET600_ADDR, 0x4);

#if 0
	BYTE buf[12] = {0};

	buf[0] = g_fod_param.calibrated_thh.l >> 0x10;
	buf[1] = g_fod_param.calibrated_thh.l >> 0x08;
	buf[2] = g_fod_param.calibrated_thh.l & 0xFF;
	buf[3] = g_fod_param.calibrated_thl.l >> 0x10;
	buf[4] = g_fod_param.calibrated_thl.l >> 0x08;
	buf[5] = g_fod_param.calibrated_thl.l & 0xFF;
	buf[6] = g_fod_param.calibrated_thh.r >> 0x10;
	buf[7] = g_fod_param.calibrated_thh.r >> 0x08;
	buf[8] = g_fod_param.calibrated_thh.r & 0xFF;
	buf[9] = g_fod_param.calibrated_thl.r >> 0x10;
	buf[10]= g_fod_param.calibrated_thl.r >> 0x08;
	buf[11]= g_fod_param.calibrated_thl.r & 0xFF;

	et6xx_write_burst_register(FOD_SUM_THH_LS_H_ET600_ADDR, 12, buf);
  	et6xx_read_burst_register(FOD_SUM_THH_LS_H_ET600_ADDR, 12, buf);

	statistic_value_601_t th_thh ,th_thl;
	th_thh.l = (buf[0] << 0x10) + (buf[1] << 0x8) + buf[2];
	th_thl.l = (buf[3] << 0x10) + (buf[4] << 0x8) + buf[5];
	th_thh.r = (buf[6] << 0x10) + (buf[7] << 0x8) + buf[8];
	th_thl.r = (buf[9] << 0x10) + (buf[10] << 0x8) + buf[11];
	//egislog_d("%s, th_thh.l = %d th_thh.r = %d", __func__, th_thh.l,th_thh.r);
	//egislog_d("%s, th_thl.l = %d th_thl.r = %d", __func__, th_thl.l,th_thl.r);

	isensor_get_detect_mean(NULL,NULL);
#endif

    // unsigned short max, mean, min;
    // et6xx_get_frame_statistic(&max, &min, &mean);
    isensor_get_detect_mean(NULL, NULL);
    et6xx_write_fod_threshold(pCurrent_cd->fod_th.calibrated_thh,
                              pCurrent_cd->fod_th.calibrated_thl);
    // Enter Fod Mode.
    et6xx_write_register(FOD_CSR_ET600_ADDR, 0x01);

#ifdef _WINDOWS
    // Waiting for INT. (workaround for finger off check fail issue)
    plat_wait_time(110);
#endif
    // isensor_dump_data();

    return EGIS_OK;
}

int isensor_set_custom_et6xx_sensor_mode(int sensing_mode) {
    egislog_d("%s", __func__);
    //	BYTE value;
    //	BYTE buffer[2];
    int ret = EGIS_COMMAND_FAIL;

    update_current_cd(&g_sensor_parameter);

    ret = et6xx_check_and_recovery(FP_MODE_DONT_CARE);
    if (EGIS_OK != ret) {
        egislog_e("%s, do reset", __func__);
        return ret;
    }

    if (g_fp_mode == FP_STANDBY_MODE) {
        power_down(PDN_EXIT);
    }

    // check if sensor is in detect mode
    if (g_fp_mode == FP_DETECT_MODE) {
        egislog_d("%s, preivious detect_mode", __func__);
        ret = et600_stop_detect_mode(0);
        if (ret != EGIS_OK) return ret;
    }

    g_fp_mode = FP_SENSOR_MODE;

    // Clear FOD event flag & disable FOD interrupt output.
    et6xx_write_register(INT_CTRL_ET600_ADDR, 0x0 | INT_TRIG(1));
    et6xx_write_register(INT_STUS_CLR_ET600_ADDR, 0x4);

    // Set OSC 40M for image scan
    et6xx_write_register(OSC40M_CONF_ET600_ADDR, g_osc40m | 0x80);

    if (sensing_mode == DELTA_SENSING_MODE) {
        et6xx_write_register(ARY_PWR_ET600_ADDR, 0x50);
        if (g_VBT_OFS_Value == 0) {
            et6xx_write_register(DPTH_ET600_ADDR,
                                 0x00 | (g_VDM_Bypass ? 0x08 : 0x00) | ENDIAN_TYPE);
        } else if (pCurrent_cd->VBT_OPTION) {
            // Change Vbt setting to original value
            et6xx_write_register(VBT_OFS_FW_ET600_ADDR, pCurrent_cd->Ori_VBT_OFS_Value);
            et6xx_write_register(DPTH_ET600_ADDR,
                                 0x02 | (g_VDM_Bypass ? 0x08 : 0x00) | ENDIAN_TYPE);
        } else {
            et6xx_write_register(VBT_OFS_FW_ET600_ADDR, pCurrent_cd->WBK_VBT_OFS_Value);
            et6xx_write_register(DPTH_ET600_ADDR,
                                 0x02 | (g_VDM_Bypass ? 0x08 : 0x00) | ENDIAN_TYPE);
        }
    } else {
        et6xx_write_register(ARY_PWR_ET600_ADDR, 0x18);
        if (g_VBT_OFS_Value == 0) {
            et6xx_write_register(DPTH_ET600_ADDR, 0x14 | (g_VDM_Bypass ? 0x08 : 0x00) |
                                                      ENDIAN_TYPE | DATA_PATH_BYPASS_VBT);
        } else if (pCurrent_cd->VBT_OPTION) {
            // Change Vbt setting to original value
            et6xx_write_register(VBT_OFS_FW_ET600_ADDR, pCurrent_cd->Ori_VBT_OFS_Value);
            et6xx_write_register(DPTH_ET600_ADDR, 0x16 | (g_VDM_Bypass ? 0x08 : 0x00) |
                                                      ENDIAN_TYPE | DATA_PATH_BYPASS_VBT);
        } else {
            et6xx_write_register(VBT_OFS_FW_ET600_ADDR, pCurrent_cd->WBK_VBT_OFS_Value);
            et6xx_write_register(DPTH_ET600_ADDR, 0x16 | (g_VDM_Bypass ? 0x08 : 0x00) |
                                                      ENDIAN_TYPE | DATA_PATH_BYPASS_VBT);
        }
    }

    return EGIS_OK;
}

static void _transpose_remove_T_dummy(unsigned short* src_frame, int width, int height,
                                      unsigned short* out_frame, unsigned short* ref_line,
                                      BOOL keep_T_dummy) {
    // Remove T dummy pixel and transfer Col-major to Row-major
    int i, j, h_plus = height, middle = width / 2;
    unsigned short* data;
    int count = 0;
    for (i = 0; i < h_plus; i++) {
        data = src_frame + i;
        for (j = 0; j < width; j++, data += h_plus) {
            if (i == 0) {
                *ref_line++ = *data;
            }

            if (keep_T_dummy || !(j == middle || i == 0)) {
                *out_frame++ = *data;
                count++;
            }
        }
    }
    egislog_d("%s (%d) count=%d", __func__, keep_T_dummy, count);
    return;
}

int et600_fetch_intensity_16(unsigned short* raw, int width, int height, int number_of_frames) {
    et6xx_write_register(ACTN_ET600_ADDR, 0x02);
    if (polling_registry(STUS_ET600_ADDR, 0x01, 0x01) != EGIS_OK) return EGIS_COMMAND_FAIL;
    if (io_5xx_dispatch_get_frame_16bits(raw, width, height, 0, number_of_frames) != EGIS_OK)
        return EGIS_COMMAND_FAIL;
    return et6xx_write_register(ACTN_ET600_ADDR, 0x04);  // can be moved to stop fetch //return OK;
}

int isensor_get_dynamic_frame(BYTE* frame, UINT height, UINT width, UINT number_of_frames) {
    egislog_e("%s, not supported!", __func__);
    return EGIS_OK;
}

static int calMaxMinDiff(unsigned short* line, int idx_start, int idx_end) {
    int i;
    unsigned short max, min;
    line += idx_start;
    max = *line++;
    min = max;
    for (i = idx_start + 1; i < idx_end; i++) {
        if (*line < min) {
            min = *line;
        } else if (*line > max) {
            max = *line;
        }
        line++;
    }
    return max - min;
}

int isensor_get_dynamic_frame_16bit(unsigned short* frame_16bits, UINT height, UINT width,
                                    UINT number_of_frames) {
#define MAX_WIDTH 130

    unsigned short tmp_raw_image[MAX_WIDTH * MAX_WIDTH];
    unsigned short ref_line[MAX_WIDTH];
    BOOL keep_T_dummy = g_keep_T_dummy;

    if (width + 1 > MAX_WIDTH) {
        egislog_e("width+1 (%d) > MAX_WIDTH (%d)", width, MAX_WIDTH);
    }
    if (!keep_T_dummy) {
        width += 1;
        height += 1;
    }

    g_refline_bad = FALSE;
    if (et600_fetch_intensity_16(tmp_raw_image, width, height, number_of_frames) != EGIS_OK)
        return EGIS_COMMAND_FAIL;

    _transpose_remove_T_dummy(tmp_raw_image, width, height, frame_16bits, ref_line, keep_T_dummy);
    int diffLeft = calMaxMinDiff(ref_line, 3, width / 2 - 3);
    int diffRight = calMaxMinDiff(ref_line, width / 2 + 3, width - 3);
    egislog_d("refline diff %d %d", diffLeft, diffRight);
#define THRESHOLD_REFLINE_DIFF 600
    if (diffLeft > THRESHOLD_REFLINE_DIFF || diffRight > THRESHOLD_REFLINE_DIFF) {
        egislog_d("refline is bad!!");
        g_refline_bad = TRUE;
    }

    return EGIS_OK;
}

FPS_SENSING_TYPE_T isensor_get_sensing_mode(void) {
    return g_sensing_mode;
}

int isensor_set_power_off(void) {
    power_down(PDN_ENTER);
    return EGIS_OK;
}

int isensor_get_frame(BYTE* frame, UINT height, UINT width, UINT number_of_frames) {
    return isensor_get_dynamic_frame(frame, height, width, number_of_frames);
}

struct trigger_case_t {
    BOOL right_high;
    BOOL right_low;
    BOOL left_high;
    BOOL left_low;
};

#define DEBUG_COMPARE_STATISTIC
static void compare_statistic_mean_and_th(struct trigger_case_t* trigger) {
    // pCurrent_cd
    statistic_value_601_t mean;
    int debug = 1;
    isensor_get_detect_mean(&mean.l, &mean.r);
    trigger->right_high = mean.r >= pCurrent_cd->fod_th.calibrated_thh.r ? 1 : 0;
    trigger->right_low = mean.r <= pCurrent_cd->fod_th.calibrated_thl.r ? 1 : 0;
    trigger->left_high = mean.l >= pCurrent_cd->fod_th.calibrated_thh.l ? 1 : 0;
    trigger->left_low = mean.l <= pCurrent_cd->fod_th.calibrated_thl.l ? 1 : 0;
#ifdef DEBUG_COMPARE_STATISTIC
    if (trigger->right_high)
        egislog_d("%s, [et6xx] TRIGGER %s, threshold = %d ", __func__, "thh.r",
                  pCurrent_cd->fod_th.calibrated_thh.r);
    if (trigger->right_low)
        egislog_d("%s, [et6xx] TRIGGER %s, threshold = %d ", __func__, "thl.r",
                  pCurrent_cd->fod_th.calibrated_thl.r);
    if (trigger->left_high)
        egislog_d("%s, [et6xx] TRIGGER %s, threshold = %d ", __func__, "thh.l",
                  pCurrent_cd->fod_th.calibrated_thh.l);
    if (trigger->left_low)
        egislog_d("%s, [et6xx] TRIGGER %s, threshold = %d ", __func__, "thl.l",
                  pCurrent_cd->fod_th.calibrated_thl.l);
#endif
}

int isensor_read_int_status(BYTE* IntStatus) {
    BYTE value;

    et6xx_read_register(INT_STUS_CLR_ET600_ADDR, &value);
    egislog_d("chedebug 0x%x", value);

    if (IntStatus != NULL) {
        *IntStatus = 0;

        if (value & 0x28) {
            *IntStatus |= 0x04;
        }
        if (value & 0x14) {
            *IntStatus |= 0x08;
        }
    }

    return EGIS_OK;
}

int isensor_set_Z1_area(BOOL forceSetFull) {
    return EGIS_OK;
}

int isensor_pullup_int(BOOL polarity) {
    return EGIS_OK;
}

int isensor_read_nvm(unsigned char* buff) {
    int ret;
    BYTE rxBuf[70];
    memset(rxBuf, 0, 70);
    ret = io_5xx_read_nvm(rxBuf);
    if (ret != EGIS_OK) return ret;
    memcpy(buff, &rxBuf[3], 64);
    return ret;
}

int isensor_read_rev_id(BYTE* read_buf, int length) {
    return EGIS_OK;
}

int isensor_set_navigation_detect_mode(void) {
    return EGIS_OK;
}

int isensor_set_navigation_sensor_mode(void) {
    return EGIS_OK;
}

int isensor_set_spi_power(const int option) {
    int ret = 0;
    switch (option) {
        case 0:
            ret = io_dispatch_disconnect();
            break;
        case 1:
            ret = io_dispatch_connect(NULL);
            break;
        default:
            break;
    }
    return ret;
}

int isensor_get_bad_pixels(BYTE** bad_pixel, int* bad_pixel_count) {
    if (bad_pixel) {
        *bad_pixel = g_sensor_parameter.bad_pixel;
    }
    if (bad_pixel_count) {
        *bad_pixel_count = g_sensor_parameter.bad_pixel_count;
    }
    return EGIS_OK;
}

int isensor_set_sensor_mode(void) {
    return isensor_set_custom_et6xx_sensor_mode(g_sensing_mode);
}

int isensor_close(void) {
    egislog_d("isensor_close enter");
    isensor_dump_data();
    return io_dispatch_disconnect();
}

int isensor_get_sensor_roi_size(int* width, int* height) {
    return isensor_get_et6xx_sensor_size(width, height);
}

int isensor_get_sensor_full_size(int* full_width, int* full_height) {
    return isensor_get_et6xx_sensor_size(full_width, full_height);
}

int isensor_get_sensor_id(unsigned short* id) {
    unsigned short buf = 0;
    egislog_d("%s", __func__);
    et6xx_read_burst_register(DEV_ID1_ET600_ADDR, 2, (void*)&buf);
    *id = buf;
    egislog_d("%s done, id = 0x%x", __func__, *id);
    return 0;
}
