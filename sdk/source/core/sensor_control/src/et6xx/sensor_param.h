
#ifndef __SENSOR_PARAM_HEADER__
#define __SENSOR_PARAM_HEADER__

#include "egis_definition.h"
#include "egis_sensor_parameter.h"
#include "fingerprint_library.h"
#include "type_definition.h"

void BackupDetectParam(SensorParameter* pSensorParameter, BOOL forceBackup);
int RestoreDetectParam(SensorParameter* g_sensor_parameter);

#endif
