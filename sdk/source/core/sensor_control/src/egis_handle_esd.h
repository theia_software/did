#include "type_definition.h"

void sensor_exception_set_active();
void sensor_exception_reset();
BOOL sensor_exception_is_active();
BOOL sensor_exception_is_over_duration();
