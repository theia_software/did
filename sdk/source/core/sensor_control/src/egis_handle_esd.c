#include "egis_handle_esd.h"
#include "egis_log.h"
#include "plat_time.h"

#define LOG_TAG "RBS-EGISFP"

#define DURATION_10_MIN (1000 * 60 * 10)

BOOL is_sensor_exception = FALSE;
unsigned long long sensor_last_exception_time = 0;

void sensor_exception_set_active() {
    sensor_last_exception_time = plat_get_time();
    is_sensor_exception = TRUE;
}

void sensor_exception_reset() {
    if (is_sensor_exception) egislog_e("is_sensor_exception (%d) is reset", is_sensor_exception);
    is_sensor_exception = FALSE;
}

BOOL sensor_exception_is_active() {
    return is_sensor_exception;
}
BOOL sensor_exception_is_over_duration() {
    unsigned long exception_duration = plat_get_diff_time(sensor_last_exception_time);
    return (exception_duration > DURATION_10_MIN) ? TRUE : FALSE;
}