#include "fp_capture_count.h"
#include <stdint.h>

#include "egis_log.h"
#include "fp_capture_count.h"
#include "isensor_api.h"

#define LOG_TAG "RBS-Sensor"

#define INTERVAL_FALSE_TRIGGER 500
static uint16_t gCountBadFrame = 0;
int AddCountSequentialBadFrame(uint64_t timeCurrent) {
    static uint64_t timeLastBadFrame = 0;
    if (timeLastBadFrame == 0 || (timeCurrent - timeLastBadFrame) > INTERVAL_FALSE_TRIGGER) {
        gCountBadFrame = 1;
    } else {
        gCountBadFrame++;
    }
    egislog_d("%s return %d (%d)", __func__, gCountBadFrame, (int)(timeCurrent - timeLastBadFrame));
    timeLastBadFrame = timeCurrent;
    return gCountBadFrame;
}
void ResetCountSequentialBadFrame() {
    egislog_d("%s ", __func__);
    gCountBadFrame = 0;
}
int GetCountSequentialBadFrame() {
    return gCountBadFrame;
}
//
#define INTERVAL_NEW_FINGER 1200
static int gValidCount = 0;
BOOL SetCaptureImageStart(uint64_t timeCapture) {
    static uint64_t timeLastCaptureImage = 0;
    uint64_t diffTime = timeCapture - timeLastCaptureImage;
    BOOL isNewFingerStart = FALSE;
    if (timeLastCaptureImage == 0 || diffTime > INTERVAL_NEW_FINGER) {
        isNewFingerStart = TRUE;
    }
    if (isNewFingerStart) {
        gValidCount = 0;
    }
    egislog_d("%s (%d) (%d)", __func__, gValidCount, (int)diffTime);
    timeLastCaptureImage = timeCapture;
    return isNewFingerStart;
}

int SetCaptureImageFingerTouch() {
    gValidCount++;
    egislog_d("%s, count=%d, bad=%d", __func__, gValidCount, gCountBadFrame);
    return EGIS_OK;
}

#define ENROLL_COUNT 12
BOOL IsCaptureImageForEnroll() {
    if (gValidCount < ENROLL_COUNT)
        return TRUE;
    else
        return FALSE;
}
