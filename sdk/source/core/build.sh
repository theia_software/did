#!/bin/bash

APP_ABI=arm64-v8a

# run NDK build
ndk-build \
    NDK_DEBUG=1 \
    NDK_PROJECT_PATH=. \
    NDK_APPLICATION_MK=Application.mk \
    APP_BUILD_SCRIPT=Android_core.mk \
    APP_OPTIM=$OPTIM \
    APP_ABI=$APP_ABI
