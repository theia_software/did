#ifndef __EGIS_FP_SENSOR_TEST_H__
#define __EGIS_FP_SENSOR_TEST_H__
#include <stdint.h>

/*!
*******************************************************************************
**  Initialize Egis Fingerprint Module and Device.
**
**  @param[in]  scriptId            Identifier of the script to execute.
**  @param[in]  options             This value is used to set timeout , when
testing snr with rubber on sensor.
                                    Timeout
minimum is 5 sec, and default is 30 sec. ( Unit timeout in seconds ).
**  @param[in]  dataLogOpt          Not Used.
**  @param[in]  unitId              Not Used.
**  @param[in]  pfnCustomRequest    Pointer to function provided by caller
**                                  to invoke the egisFpCustomRequest function.
**  @param[in]  pContext			Context.
**  @param[in]  pfnCallback         Not Used.
**  @param[in]  callbackData        Not Used.
**
**  @return
**      EGIS_OK
**      EGIS_INCORRECT_PARAMETER
**      EGIS_COMMAND_FAIL
**      EGIS_NO_DEVICE
**      EGIS_WAIT_INTERRUPT_TIMEOUT
**      Other - unexpected fatal error.
*/

/* ************************************************************************* */
/* ****************** Start Of Test Script Definitions ********************* */

typedef UINT egisFpSnsrTestScriptId_t;

#define EGIS_FP_DIRTYDOTS_TEST_SCRIPT_ID ((egisFpSnsrTestScriptId_t)1000)

#define EGIS_FP_REGISTER_RW_TEST_SCRIPT_ID ((egisFpSnsrTestScriptId_t)1001)

#define EGIS_FP_PRE_FINGER_ON_TEST_SCRIPT_ID ((egisFpSnsrTestScriptId_t)1002)

#define EGIS_FP_FINGER_ON_TEST_SCRIPT_ID ((egisFpSnsrTestScriptId_t)1003)

#define EGIS_FP_READ_REV_TEST_SCRIPT_ID ((egisFpSnsrTestScriptId_t)1015)
/* ****************** End Of Test Script Definitions *********************** */
/* ************************************************************************* */

#endif /*__EGIS_FP_SENSOR_TEST_H__*/
