#ifndef __EGIS_SENSOR_TEST_H__
#define __EGIS_SENSOR_TEST_H__

#include "common_definition.h"
#include "egis_definition.h"
#include "liver_image.h"

typedef struct _BufferIn {
    int scriptId;
    int options;
    int status;
    int touch;
} BufferIn;

typedef struct _BufferOut {
    int result;
    unsigned char picture_put[MAX_IMG_SIZE];
} BufferOut;

typedef struct egisBlobData_s {
    BYTE* pData;
    UINT length;
} egisBlobData_t;

typedef struct egisConstBlobData_s {
    const BYTE* pData;
    UINT length;
} egisConstBlobData_t;

int ReadWriteTest(void);
int DarkdotsTest(const int sensor_width, const int sensor_height, unsigned char* dirtydots_map);
int WhitedotsTest(const int sensor_width, const int sensor_height, unsigned char* dirtydots_map);
int AbnormalTest_Use_Avg_Frame(float* avgStdDev, const int sensor_width, const int sensor_height,
                               unsigned char* dirtydots_map);
int AbnormalTest_Use_Fixed_Frame(const int sensor_width, const int sensor_height);
int check_cali_data(int* state, BOOL interrupt);
int interrupt_calibration(int* state, BOOL interrupt);
int egisFpSnsrTest(const egisConstBlobData_t* pIn, egisBlobData_t* pOut);
BOOL egis_checkIsFinger(uint8_t* img, int img_width, int img_height, int check_mode,
                        liver_image_out_header_t* pheader);
int egis_mmiGetNVMUid(unsigned char* buf, int* buf_size);
void egis_mmiGetStatisticData(liver_image_out_header_t* pheader);

#define INDEX_BAD_PIXEL 10

#endif  // __EGIS_SENSOR_TEST_H__
