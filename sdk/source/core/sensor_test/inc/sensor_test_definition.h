#pragma once

/*
 * 	Sensor Control
 */
#define WHITE_DOTS_TH 240
#define DARK_DOTS_TH 10
#define CALIBRATED_WHITE_TH 240
#define CALIBRATED_DARK_TH 1
#define CDSBadDotsTh 40
#define DirtyDotsTh16x16 3
#define ConsecutiveDirtyDotsTh 6

#define FingerOnTh 6
#define FINGER_ON_TIMEOUT 5
#define AverageDifferTh 10
#define BadDeviationTh 40.0

typedef enum { TR_PASS = 0, TR_FAIL = 1, TR_NA = 2 } TestResult;

typedef enum {
    SKIP_CALIBRATION_CHECK = 0, /*0*/
    SKIP_INTERRUPT_CHECK_STEP1,
    SKIP_INTERRUPT_CHECK_STEP2,
    CALIBRATION_DVR,
    CALIBRATION_INTERRUPT_STEP1,
    CALIBRATION_INTERRUPT_STEP2,
    CALIBRATION_INTERRUPT_STEP3,
    CALIBRATION_INTERRUPT_STEP4
} CheckCaliState;

typedef enum {
    BACKUP_REG = 0, /*0*/
    READWRITE_TEST
} RegTestState;
