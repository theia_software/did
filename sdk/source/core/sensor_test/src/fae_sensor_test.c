#include "egis_definition.h"
#include "egis_sensormodule.h"
#include "fp_sensormodule.h"
#include "plat_log.h"

#define LOG_TAG "RBS-FAE"

int command_handler(unsigned char* msg_data, int msg_size, unsigned char* out_data,
                    int* out_data_size) {
    egislog_e("command_handler is empty");
    return -1;
}
int egis_mmiDoTest(fp_mmi_info* pMmiInfo) {
    if (pMmiInfo == NULL) {
        egislog_e("wrong pMmiInfo");
        return EGIS_INCORRECT_PARAMETER;
    }
    egislog_d("pMmiInfo->testType %d", pMmiInfo->testType);
    return EGIS_OK;
}