#if defined(WIN32)

#include <ctype.h>
#include <windows.h>

#include <ctype.h>
#include <windows.h>
#include "../inc/func.h"
#elif defined(RBS_SDK_USE)
#include "egis_sprintf.h"
#include "func.h"
#include "plat_log.h"
#include "plat_mem.h"
#include "string.h"
#include "type_definition.h"

#define OutputDebugString(X) egislog_d(X)
#define LOG_TAG "RBS-INLINE"
#define malloc plat_alloc
#define free plat_free
#else

// PLEASE DEFINE PLATFORM

#endif
#include <math.h>
#include <stdio.h>

unsigned char ucFindOtsuTh(unsigned char* pPicture, int width, int height, int* pnB, int* pnF,
                           unsigned char* pmB, unsigned char* pmF, unsigned char* pDirty) {
    float otsu;

    float Wb;
    float ub;

    float Wf;
    float uf;

    int th;

    int max_th;
    float max_otsu;

    unsigned char bg_avg;
    unsigned char fg_avg;

    int bg_num;
    int fg_num;

    max_th = 0;
    max_otsu = 0;

    bg_avg = 0;
    fg_avg = 0;
    bg_num = 0;
    fg_num = 0;

    for (th = 0; th < 256; th++) {
        int i;

        int nB = 0;
        int sumB = 0;

        int nF = 0;
        int sumF = 0;

        for (i = 0; i < width * height; i++) {
            if (pDirty) {
                if (!pDirty[i]) {
                    if (pPicture[i] > th) {
                        nF++;
                        sumF += pPicture[i];
                    } else {
                        nB++;
                        sumB += pPicture[i];
                    }
                }
            } else {
                if (pPicture[i] > th) {
                    nF++;
                    sumF += pPicture[i];
                } else {
                    nB++;
                    sumB += pPicture[i];
                }
            }
        }

        if ((nB == 0) || (nF == 0)) {
            continue;
        }

        Wb = (float)nB / ((float)width * height);
        Wf = (float)nF / ((float)width * height);

        ub = (float)sumB / (float)nB;  // avg of background
        uf = (float)sumF / (float)nF;  // avg of foreground

        otsu = Wb * Wf * (ub - uf) * (ub - uf);

        if (otsu > max_otsu) {
            max_otsu = otsu;
            max_th = th;

            bg_avg = (unsigned char)ub;
            fg_avg = (unsigned char)uf;
            bg_num = nB;
            fg_num = nF;
        }
    }

    *pnB = bg_num;
    *pnF = fg_num;
    *pmB = bg_avg;
    *pmF = fg_avg;

    return max_th;
}

void vCharacterizeImage(unsigned char char_point, double char_value, int width, int height,
                        unsigned char* pImage) {
    int i;

    for (i = 0; i < width * height; i++) {
        int intensity;

        intensity = pImage[i];

        if (intensity > char_point) {
            intensity += (int)((intensity - char_point) * char_value + 0.5);
            if (intensity > 0xFF) {
                intensity = 0xFF;
            }
        } else {
            intensity -= (int)((char_point - intensity) * char_value + 0.5);
            if (intensity < 0) {
                intensity = 0;
            }
            // intensity=0;
        }

        pImage[i] = intensity;
    }
}

#define CONSECUTIVE_PXLS 4

void vCalculateSoftStatistics(unsigned char* pMean, unsigned char* pMax, unsigned char* pMin,
                              int width, int height, unsigned char* pImage,
                              unsigned char* pDitryDots) {
    unsigned int i;
    int row;
    int col;
    unsigned int pxl_sum;

    unsigned char pxl_avg;
    unsigned char max;
    unsigned char min;
    unsigned char mean;

    max = 0x00;
    min = 0xFF;
    mean = 0x00;

    unsigned int pxl_cnt;

    unsigned char* row_temp;
    row_temp = (unsigned char*)malloc(width);

    unsigned char* col_temp;
    col_temp = (unsigned char*)malloc(height);

    // calculate minimum avg of 4 consecutive pxl in row
    for (row = 0; row < height; row++) {
        pxl_cnt = 0;
        memset(row_temp, 0x00, width);
        for (i = 0; i < (unsigned int)width; i++) {
            if (pDitryDots) {
                if (!pDitryDots[row * width + i]) {
                    row_temp[pxl_cnt++] = pImage[row * width + i];
                }
            } else {
                row_temp[pxl_cnt++] = pImage[row * width + i];
            }
        }

        pxl_sum = 0;
        for (i = 0; i < CONSECUTIVE_PXLS - 1; i++) {
            pxl_sum += row_temp[i];
        }

        for (i = CONSECUTIVE_PXLS - 1; i < pxl_cnt; i++) {
            pxl_sum += row_temp[i];
            pxl_avg = pxl_sum / CONSECUTIVE_PXLS;

            if (pxl_avg < min) {
                min = pxl_avg;
            }

            if (pxl_avg > max) {
                max = pxl_avg;
            }

            pxl_sum -= row_temp[i - (CONSECUTIVE_PXLS - 1)];
        }
    }

    // calculate minimum avg of 4 consecutive pxl in col
    for (col = 0; col < width; col++) {
        pxl_cnt = 0;
        memset(col_temp, 0x00, height);
        for (i = 0; i < (unsigned int)height; i++) {
            if (pDitryDots) {
                if (!pDitryDots[i * width + col]) {
                    col_temp[pxl_cnt++] = pImage[i * width + col];
                }
            } else {
                col_temp[pxl_cnt++] = pImage[i * width + col];
            }
        }

        pxl_sum = 0;
        for (i = 0; i < CONSECUTIVE_PXLS - 1; i++) {
            pxl_sum += col_temp[i];
        }

        for (i = CONSECUTIVE_PXLS - 1; i < pxl_cnt; i++) {
            pxl_sum += col_temp[i];
            pxl_avg = pxl_sum / CONSECUTIVE_PXLS;
            if (pxl_avg < min) {
                min = pxl_avg;
            }
            if (pxl_avg > max) {
                max = pxl_avg;
            }
            pxl_sum -= col_temp[i - (CONSECUTIVE_PXLS - 1)];
        }
    }

    pxl_sum = 0x0000;
    pxl_cnt = 0;
    for (i = 0; i < (unsigned int)width * height; i++) {
        if (pDitryDots) {
            if (!pDitryDots[i]) {
                pxl_sum += pImage[i];
                pxl_cnt++;
            }
        } else {
            pxl_sum += pImage[i];
            pxl_cnt++;
        }
    }
    if (pxl_cnt > 0) {
        mean = pxl_sum / pxl_cnt;
    } else {
        mean = 0;
    }

    *pMean = mean;
    *pMax = max;
    *pMin = min;

    free(row_temp);
    free(col_temp);
}

void vCalculateSoftStatistics16(unsigned short* pMean, unsigned short* pMax, unsigned short* pMin,
                                int width, int height, unsigned short* pImage,
                                unsigned char* pDitryDots) {
    unsigned int i;
    int row;
    int col;
    unsigned int pxl_sum;

    unsigned short pxl_avg;
    unsigned short max;
    unsigned short min;
    unsigned short mean;

    max = 0x0000;
    min = 0xFFFF;
    mean = 0x0000;

    unsigned int pxl_cnt;

    unsigned short* row_temp;
    row_temp = (unsigned short*)malloc(width * 2);

    unsigned short* col_temp;
    col_temp = (unsigned short*)malloc(height * 2);

    // calculate minimum avg of 4 consecutive pxl in row
    for (row = 0; row < height; row++) {
        pxl_cnt = 0;
        memset(row_temp, 0x00, width * 2);
        for (i = 0; i < (unsigned int)width; i++) {
            if (pDitryDots) {
                if (!pDitryDots[row * width + i]) {
                    row_temp[pxl_cnt++] = pImage[row * width + i];
                }
            } else {
                row_temp[pxl_cnt++] = pImage[row * width + i];
            }
        }

        pxl_sum = 0;
        for (i = 0; i < CONSECUTIVE_PXLS - 1; i++) {
            pxl_sum += row_temp[i];
        }

        for (i = CONSECUTIVE_PXLS - 1; i < pxl_cnt; i++) {
            pxl_sum += row_temp[i];
            pxl_avg = pxl_sum / CONSECUTIVE_PXLS;

            if (pxl_avg < min) {
                min = pxl_avg;
            }

            if (pxl_avg > max) {
                max = pxl_avg;
            }

            pxl_sum -= row_temp[i - (CONSECUTIVE_PXLS - 1)];
        }
    }

    // calculate minimum avg of 4 consecutive pxl in col
    for (col = 0; col < width; col++) {
        pxl_cnt = 0;
        memset(col_temp, 0x00, height * 2);
        for (i = 0; i < (unsigned int)height; i++) {
            if (pDitryDots) {
                if (!pDitryDots[i * width + col]) {
                    col_temp[pxl_cnt++] = pImage[i * width + col];
                }
            } else {
                col_temp[pxl_cnt++] = pImage[i * width + col];
            }
        }

        pxl_sum = 0;
        for (i = 0; i < CONSECUTIVE_PXLS - 1; i++) {
            pxl_sum += col_temp[i];
        }

        for (i = CONSECUTIVE_PXLS - 1; i < pxl_cnt; i++) {
            pxl_sum += col_temp[i];
            pxl_avg = pxl_sum / CONSECUTIVE_PXLS;
            if (pxl_avg < min) {
                min = pxl_avg;
            }
            if (pxl_avg > max) {
                max = pxl_avg;
            }
            pxl_sum -= col_temp[i - (CONSECUTIVE_PXLS - 1)];
        }
    }

    pxl_sum = 0x0000;
    pxl_cnt = 0;
    for (i = 0; i < (unsigned int)width * height; i++) {
        if (pDitryDots) {
            if (!pDitryDots[i]) {
                pxl_sum += pImage[i];
                pxl_cnt++;
            }
        } else {
            pxl_sum += pImage[i];
            pxl_cnt++;
        }
    }
    if (pxl_cnt > 0) {
        mean = (unsigned short)((pxl_sum / pxl_cnt) + 0.5);
    } else {
        mean = 0;
    }

    if (pMean) {
        *pMean = mean;
    }

    if (pMax) {
        *pMax = max;
    }

    if (pMin) {
        *pMin = min;
    }

    free(row_temp);
    free(col_temp);
}

void vCalculateCenterAverage16(unsigned short* pMean, int width, int height, unsigned short* pImage,
                               unsigned char* pDitryDots, int CenterX, int CenterY,
                               int WindowSize) {
    int row;
    int col;

    unsigned int pxl_sum;
    unsigned int pxl_cnt;

    *pMean = 0;
    pxl_sum = 0;
    pxl_cnt = 0;

    for (row = (CenterY > (WindowSize / 2)) ? (CenterY - (WindowSize / 2)) : 0;
         (row < ((CenterY > (WindowSize / 2)) ? (CenterY - (WindowSize / 2)) : 0) + WindowSize) &&
         (row < height);
         row++) {
        for (col = (CenterX > (WindowSize / 2)) ? (CenterX - (WindowSize / 2)) : 0;
             (col <
              ((CenterX > (WindowSize / 2)) ? (CenterX - (WindowSize / 2)) : 0) + WindowSize) &&
             (col < width);
             col++) {
            if ((!pDitryDots) || (!pDitryDots[row * width + col])) {
                pxl_sum += pImage[row * width + col];
                pxl_cnt++;
            }
        }
    }

    if (pxl_cnt > 0) {
        *pMean = pxl_sum / pxl_cnt;
    }
}

void vCalculateScaleRange(int img_size, unsigned short sw_max, unsigned short* pMax,
                          unsigned short* pMin, unsigned short* pMiddle, int* pAccumulatedPxl,
                          int min_pxl_th, unsigned int* pHistogram8, unsigned short* pImage) {
    int i;
    unsigned short pxl;

    int AccumulatedPxl;

    unsigned short MaxPxl = 0xFFFF;
    unsigned short MinPxl = 0x0000;

    memset(pHistogram8, 0x00, 256 * sizeof(unsigned int));

    // scale to 8bit histogram;
    for (i = 0; i < img_size; i++) {
        pxl = pImage[i];
        if (pxl >= sw_max) {
            pHistogram8[0xFF]++;
        } else {
            pHistogram8[pxl * 256 / (sw_max + 1)]++;
        }
    }

    unsigned short LocalPeak = 0x0000;

    BOOL bMaxFound = FALSE;
    BOOL bLocalPeakFound = FALSE;

    *pMiddle = 0;

    AccumulatedPxl = 0;

    for (i = 0xFF; i > 1; i--) {
        AccumulatedPxl += pHistogram8[i];

        if ((AccumulatedPxl >= (img_size / 2)) && (*pMiddle == 0)) {
            *pMiddle = i * sw_max / 256;
        }

        if (pHistogram8[i] > LocalPeak) {
            LocalPeak = pHistogram8[i];
        }

        if (bLocalPeakFound) {
            if ((pHistogram8[i] < (unsigned int)min_pxl_th) &&
                (pHistogram8[i - 1] < (unsigned int)min_pxl_th)) {
                MinPxl = i * sw_max / 256;
                break;
            }
        } else {
            if (!bMaxFound) {
                if ((pHistogram8[i] >= (unsigned int)min_pxl_th) &&
                    (pHistogram8[i - 1] >= (unsigned int)min_pxl_th)) {
                    MaxPxl = i * sw_max / 256;
                    bMaxFound = TRUE;
                }
            }

            if (pHistogram8[i] >= (unsigned int)min_pxl_th * 2) {
                bLocalPeakFound = TRUE;
            }
        }
    }

    *pMax = MaxPxl;
    *pMin = MinPxl;
    *pAccumulatedPxl = AccumulatedPxl;
}

void vScaleImage16ToImage8(int width, int height, unsigned char* pDirtyDots,
                           unsigned short* pImage16, unsigned char* pImage8,
                           BOOL bImageIsIntegrated, unsigned char* pSignal8,
                           unsigned short* pSignal16) {
    int img_size = width * height;
    double scale_factor = 0.0;

#if (EGIS_SENSOR == 711)
    // 10 bit ADC, cut 2 lsb bits.
    int i;
    for (i = 0; i < img_size; i++) {
        pImage8[i] = (unsigned char)(pImage16[i] >> 2);
    }
    scale_factor = 4.0;

#elif (EGIS_SENSOR == 713)
    // 11 bit ADC, cut 3 lsb bits.
    int i;
    for (i = 0; i < img_size; i++) {
        pImage8[i] = (unsigned char)(pImage16[i] >> 3);
    }
    scale_factor = 8.0;

#endif

    if (bImageIsIntegrated || (scale_factor == 0.0)) {
        char buf[MAX_PATH];

        // get software calculated statistics
        unsigned short sw_mean, sw_max, sw_min;

        vCalculateSoftStatistics16(&sw_mean, &sw_max, &sw_min, width, height, pImage16, pDirtyDots);

        egist_snprintf(buf, MAX_PATH, "sw_mean=%4d, sw_max=%4d, sw_min=%4d, sw_max-sw_min=%4d.\n",
                       sw_mean, sw_max, sw_min, sw_max - sw_min);
        OutputDebugString(buf);

        int i;
        int AccumulatedPxl;
        unsigned int histogram8[256];
        unsigned short pxl_max, pxl_middle, pxl_min;
        unsigned short range;

        if ((sw_max - sw_min) < 256) {
            for (i = 0; i < img_size; i++) {
                int pxl;
                if (sw_max < 256) {
                    pxl = pImage16[i];
                } else {
                    pxl = 128 + pImage16[i] - sw_mean;
                }

                if (pxl > 0xFF) {
                    pImage8[i] = 0xFF;
                } else if (pxl < 0) {
                    pImage8[i] = 0x00;
                } else {
                    pImage8[i] = (unsigned char)pxl;
                }
            }
            scale_factor = 1.0;
        } else {
            vCalculateScaleRange(img_size, sw_max, &pxl_max, &pxl_min, &pxl_middle, &AccumulatedPxl,
                                 10, histogram8, pImage16);

            range = (pxl_max + 1) - pxl_min;
            if (range <= 256) {
                pxl_max = pxl_middle + 128;
                pxl_min = pxl_max - 255;
                range = 256;
            }

            egist_snprintf(buf, MAX_PATH,
                           "sw_max=%5d, sw_mean=%5d, sw_min=%5d, pxl_max=%5d, pxl_middle=%5d, "
                           "pxl_min=%5d, range=%5d.\n",
                           sw_max, sw_mean, sw_min, pxl_max, pxl_middle, pxl_min, range);
            OutputDebugString(buf);

            for (i = 0; i < img_size; i++) {
                int pxl = ((int)pImage16[i] - (int)pxl_min) * 256 / range;
                if (pxl > 0xFF) {
                    pImage8[i] = 0xFF;
                } else if (pxl < 0x00) {
                    pImage8[i] = 0x00;
                } else {
                    pImage8[i] = (unsigned char)pxl;
                }
            }

            scale_factor = range / 256;
        }
    }

    if (pSignal8) {
        int nB;
        int nF;
        unsigned char mB;
        unsigned char mF;
        unsigned char otsu;

        otsu = ucFindOtsuTh(pImage8, width, height, &nB, &nF, &mB, &mF, pDirtyDots);
        *pSignal8 = mF - mB;

        if (pSignal16) {
            unsigned short otsu16;
            otsu16 = (unsigned short)(otsu * scale_factor + 0.5);

            int nB16;
            int nF16;
            int SumB16;
            int SumF16;
            unsigned short mB16;
            unsigned short mF16;

            SumB16 = 0;
            SumF16 = 0;
            nB16 = 0;
            nF16 = 0;

            for (i = 0; i < img_size; i++) {
                if (pDirtyDots && pDirtyDots[i]) {
                    continue;
                }

                if (pImage16[i] > otsu16) {
                    SumF16 += pImage16[i];
                    nF16++;
                } else {
                    SumB16 += pImage16[i];
                    nB16++;
                }
            }

            if (nF16 > 0) {
                mF16 = SumF16 / nF16;
            } else {
                mF16 = 0;
            }

            if (nB16 > 0) {
                mB16 = SumB16 / nB16;
            } else {
                mB16 = 0;
            }

            *pSignal16 = mF16 - mB16;
        }
    }
}

int compare_char_decrease(const void* a, const void* b) {
    unsigned char c;
    unsigned char d;

    c = *((unsigned char*)a);
    d = *((unsigned char*)b);

    if (c < d) {
        return 1;
    } else if (c == d) {
        return 0;
    } else {
        return -1;
    }
}

void vPixelBinning16(unsigned short* pImgIn, unsigned short* pImgOut, int width, int height) {
    int i, j;
    unsigned short B, GB, GR, R;

    for (j = 0; j < height / 2; j++) {
        for (i = 0; i < width / 2; i++) {
            int pxl_sum = 0;

            B = pImgIn[j * 2 * width + i * 2];
            GB = pImgIn[j * 2 * width + i * 2 + 1];
            GR = pImgIn[j * 2 * width + width + i * 2];
            R = pImgIn[j * 2 * width + width + i * 2 + 1];

            pxl_sum = B + GB + GR + R;

            // pImgOut[j * (width / 2) + i] = (unsigned short) (pxl_sum / 4);
            pImgOut[j * (width / 2) + i] = (unsigned short)pxl_sum;
        }
    }
}

#if defined(RBS_SDK_USE)

int isdigit(int c) {
    if (c <= 9 && c >= 0)
        return 1;
    else
        return 0;
}

#endif

int ishex(char c) {
    if (isdigit(c)) {
        return 1;
    }
    if ((c > 'a' - 1) && (c < 'g')) {
        return 1;
    }
    if ((c > 'A' - 1) && (c < 'G')) {
        return 1;
    }
    return 0;
}

unsigned char h2toc(char* hex) {
    unsigned char c;

    c = 0x00;

    if (ishex(hex[0])) {
        if (isdigit(hex[0])) {
            c += hex[0] - 48;
        } else {
            if ((hex[0] >= 'a') && (hex[0] <= 'f')) {
                c += hex[0] - 'a' + 10;
            } else {
                c += hex[0] - 'A' + 10;
            }
        }
    } else {
        return 0x00;
    }

    c *= 16;

    if (ishex(hex[1])) {
        if (isdigit(hex[1])) {
            c += hex[1] - 48;
        } else {
            if ((hex[1] >= 'a') && (hex[1] <= 'f')) {
                c += hex[1] - 'a' + 10;
            } else {
                c += hex[1] - 'A' + 10;
            }
        }
    } else {
        return 0x00;
    }

    return c;
}

unsigned int htoi(char* hex) {
    unsigned int i = 0;
    unsigned int j = 0;

    for (i = 0; i < strlen(hex); i++) {
        if (ishex(hex[i])) {
            j *= 16;
            if (isdigit(hex[i])) {
                j += hex[i] - 48;
            } else {
                if ((hex[i] >= 'a') && (hex[i] <= 'f')) {
                    j += hex[i] - 'a' + 10;
                } else {
                    j += hex[i] - 'A' + 10;
                }
            }
        } else {
            return 0;
        }
    }
    return j;
}

USHORT
h4tous(char* hex) {
    int i = 0;
    USHORT j = 0;
    for (i = 0; i < 4; i++) {
        if (ishex(hex[i])) {
            j *= 16;
            if (isdigit(hex[i])) {
                j += hex[i] - 48;
            } else {
                if ((hex[i] >= 'a') && (hex[i] <= 'f')) {
                    j += hex[i] - 'a' + 10;
                } else {
                    j += hex[i] - 'A' + 10;
                }
            }
        } else {
            return 0;
        }
    }
    return j;
}

char* pPath2FileName(char* pPath) {
    char* p;
    int i;

    p = NULL;
    for (i = (strlen(pPath) - 1); i >= 0; i--) {
        if (pPath[i] == '\\') {
            p = pPath + i + 1;
            break;
        }
    }
    if (p == NULL) {
        p = pPath;
    }
    return p;
}

void vPath2Path(char* pPath) {
    char* p;
    int i;

    p = NULL;
    for (i = (strlen(pPath) - 1); i >= 0; i--) {
        if (pPath[i] == '\\') {
            pPath[i] = 0;
            break;
        }
    }
}

//////////////////////////////////////////////////////////////////////////////////
#define AT_PIXEL(img, w, h, x, y) img[y * w + x]

int find_8con_min_label(int x, int y, int width, int height, int* plabel_table) {
    int min_label = width * height;

    int i, j;

    // find min label
    for (i = (x - 1); i <= (x + 1); i++) {
        for (j = (y - 1); j <= (y + 1); j++) {
            if ((i >= 0) && (j >= 0) && (i < width) && (j < height)) {
                if (AT_PIXEL(plabel_table, width, height, i, j)) {
                    if (AT_PIXEL(plabel_table, width, height, i, j) < min_label) {
                        min_label = AT_PIXEL(plabel_table, width, height, i, j);
                    }
                }
            }
        }
    }

    if (min_label == width * height) {
        min_label = 0;
    }

    return min_label;
}

typedef struct _LABEL_STRUCT {
    int min_x;
    int max_x;
    int min_y;
    int max_y;
    int pxl_cnt;
} LABEL_STRUCT;

int two_pass_algo(unsigned char* pdirty_dots, int width, int height, unsigned char option,
                  BOOL* pIsStraitLine, BOOL* pIsAwkwardShape, int fail_th) {
    int label_table_size = width * height * sizeof(int);
    int* label_table = (int*)malloc(label_table_size);
    if (!label_table) {
        return width * height;
    }

    LABEL_STRUCT* all_labels = (LABEL_STRUCT*)malloc((width * height / 2) * sizeof(LABEL_STRUCT));
    if (!all_labels) {
        return width * height;
    }

    int max_label_pxl_cnt;

    int i, j;
    int current_label;
    int label_temp;

    current_label = 1;
    memset(label_table, 0x00, label_table_size);
    max_label_pxl_cnt = 0;

    for (i = 0; i < width * height / 2; i++) {
        all_labels[i].max_x = 0;
        all_labels[i].min_x = width - 1;
        all_labels[i].max_y = 0;
        all_labels[i].min_y = height - 1;
        all_labels[i].pxl_cnt = 0;
    }

    // pass 1
    for (j = 0; j < height; j++) {
        for (i = 0; i < width; i++) {
            if (option & (TEST_WHITE_DOT | TEST_DARK_DOT | TEST_DEFECT_DOT) &
                AT_PIXEL(pdirty_dots, width, height, i, j)) {
                label_temp = find_8con_min_label(i, j, width, height, label_table);
                if (label_temp == 0) {
                    // new label
                    AT_PIXEL(label_table, width, height, i, j) = current_label++;
                } else {
                    AT_PIXEL(label_table, width, height, i, j) = label_temp;
                }
            }
        }
    }

    // pass 2
    int* label_table_tmp = (int*)malloc(width * height * sizeof(int));
    if (!label_table_tmp) {
        return width * height;
    }

    do {
        memcpy(label_table_tmp, label_table, label_table_size);
        for (j = 0; j < height; j++) {
            for (i = 0; i < width; i++) {
                if (AT_PIXEL(label_table, width, height, i, j) > 0) {
                    label_temp = find_8con_min_label(i, j, width, height, label_table);
                    AT_PIXEL(label_table, width, height, i, j) = label_temp;
                }
            }
        }
    } while (memcmp(label_table_tmp, label_table, label_table_size));

    // calculate label range
    for (j = 0; j < height; j++) {
        for (i = 0; i < width; i++) {
            label_temp = AT_PIXEL(label_table, width, height, i, j);
            if (label_temp > 0) {
                if (i > all_labels[label_temp].max_x) {
                    all_labels[label_temp].max_x = i;
                }
                if (i < all_labels[label_temp].min_x) {
                    all_labels[label_temp].min_x = i;
                }

                if (j > all_labels[label_temp].max_y) {
                    all_labels[label_temp].max_y = j;
                }
                if (j < all_labels[label_temp].min_y) {
                    all_labels[label_temp].min_y = j;
                }

                all_labels[label_temp].pxl_cnt++;
                if (all_labels[label_temp].pxl_cnt > max_label_pxl_cnt) {
                    max_label_pxl_cnt = all_labels[label_temp].pxl_cnt;
                }
            }
        }
    }

    for (i = 0; i < (width * height) / 2; i++) {
        if (all_labels[i].pxl_cnt > 0) {
            BOOL AwkwardShape = FALSE;
            BOOL StraitLine = FALSE;
            if (all_labels[i].pxl_cnt >= fail_th) {
                if ((all_labels[i].max_x != all_labels[i].min_x) &&
                    (all_labels[i].max_y != all_labels[i].min_y)) {
                    *pIsAwkwardShape = TRUE;
                    AwkwardShape = TRUE;
                } else {
                    *pIsStraitLine = TRUE;
                    StraitLine = TRUE;
                }
            }
        }
    }
    free(all_labels);
    free(label_table);
    free(label_table_tmp);
    return max_label_pxl_cnt;
}

void vEvaluateImage16(ImgTestCriteria* pImgTestCriteria, ImgTestResult* pImgTestResult,
                      BOOL* pTooManyDirtyDots, unsigned char* pDirtyDots,
                      Image_Valid_Area* pScanArea, unsigned short* pImage, int width, int height,
                      unsigned char option) {
    int i, j;

    unsigned int pxl_sum = 0;
    unsigned short pxl_avg = 0;

    double pxl_avg_double = 0.0;
    double std_dev;

    BOOL bTooManyDirtyDots;
    int WhiteDots;
    int DarkDots;
    int DefectDots;

    int AllFailDots;
    int MaxConsecutiveDirtyDots;
    int MaxConnectedDirtyDots;

    ImgTestCriteria img_test_criteria;

    unsigned char* DirtyDots;
    DirtyDots = (unsigned char*)malloc(width * height);
    if (!DirtyDots) {
        return;
    }

    int test_width;
    int test_height;
    int test_total;

    test_width = pScanArea->col_end - pScanArea->col_start + 1;
    test_height = pScanArea->row_end - pScanArea->row_start + 1;
    test_total = test_width * test_height;

    WhiteDots = 0;
    DarkDots = 0;
    DefectDots = 0;
    MaxConsecutiveDirtyDots = 0;
    MaxConnectedDirtyDots = 0;
    AllFailDots = 0;
    bTooManyDirtyDots = FALSE;

    memset(DirtyDots, 0x00, width * height);

    memset(&img_test_criteria, 0x00, sizeof(img_test_criteria));
    if (pImgTestCriteria) {
        memcpy(&img_test_criteria, pImgTestCriteria, sizeof(img_test_criteria));
    } else {
        img_test_criteria.dark_dot_th = 0;
        img_test_criteria.white_dot_th = 255;
        img_test_criteria.cds_bad_dot_th = 255;
        img_test_criteria.consecutive_fail_th = width * height;
        img_test_criteria.max_allowed_fail_pxl = width * height;
    }

    // mark white and dark pixels
    for (i = pScanArea->row_start; i <= pScanArea->row_end; i++) {
        for (j = pScanArea->col_start; j <= pScanArea->col_end; j++) {
            pxl_sum += pImage[i * width + j];
            if (option & TEST_WHITE_DOT) {
                if (pImage[i * width + j] > img_test_criteria.white_dot_th) {
                    WhiteDots++;
                    DirtyDots[i * width + j] |= TEST_WHITE_DOT;
                }
            }

            if (option & TEST_DARK_DOT) {
                if (pImage[i * width + j] < img_test_criteria.dark_dot_th) {
                    DarkDots++;
                    DirtyDots[i * width + j] |= TEST_DARK_DOT;
                }
            }
        }
    }
    // whole image average
    pxl_avg = (unsigned short)((pxl_sum / test_total) + 0.5);
    pxl_avg_double = (double)pxl_sum / (double)test_total;

    // calculate whole image stddev
    pxl_sum = 0;
    for (i = pScanArea->row_start; i <= pScanArea->row_end; i++) {
        for (j = pScanArea->col_start; j <= pScanArea->col_end; j++) {
            pxl_sum += (pImage[i * width + j] - pxl_avg) * (pImage[i * width + j] - pxl_avg);
        }
    }
    std_dev = sqrt((double)pxl_sum / test_total);

    // Test Defect Dots
    if (option & TEST_DEFECT_DOT) {
        for (i = pScanArea->row_start; i <= pScanArea->row_end; i++) {
            for (j = pScanArea->col_start; j <= pScanArea->col_end; j++) {
                if (abs(pImage[i * width + j] - pxl_avg) > img_test_criteria.cds_bad_dot_th) {
                    DefectDots++;
                    DirtyDots[i * width + j] |= TEST_DEFECT_DOT;
                }
            }
        }
    }

    for (i = 0; i < height; i++) {
        for (j = 0; j < width; j++) {
            if (DirtyDots[i * width + j]) {
                int consective_test;

                // '|' dots
                consective_test = 1;
                while ((i + consective_test) < height) {
                    if (DirtyDots[(i + consective_test) * width + j]) {
                        consective_test++;
                    } else {
                        break;
                    }
                }
                if (consective_test > MaxConsecutiveDirtyDots) {
                    MaxConsecutiveDirtyDots = consective_test;
                }

                // '--' dots
                consective_test = 1;
                while ((j + consective_test) < width) {
                    if (DirtyDots[i * width + j + consective_test]) {
                        consective_test++;
                    } else {
                        break;
                    }
                }
                if (consective_test > MaxConsecutiveDirtyDots) {
                    MaxConsecutiveDirtyDots = consective_test;
                }

                // '/' dots
                consective_test = 1;
                while (((i + consective_test) < height) && ((j + consective_test) < width)) {
                    if (DirtyDots[(i + consective_test) * width + (j + consective_test)]) {
                        consective_test++;
                    } else {
                        break;
                    }
                }
                if (consective_test > MaxConsecutiveDirtyDots) {
                    MaxConsecutiveDirtyDots = consective_test;
                }

                // '\' dots
                consective_test = 1;
                while (((i + consective_test) < height) && (j >= consective_test)) {
                    if (DirtyDots[(i + consective_test) * width + (j - consective_test)]) {
                        consective_test++;
                    } else {
                        break;
                    }
                }
                if (consective_test > MaxConsecutiveDirtyDots) {
                    MaxConsecutiveDirtyDots = consective_test;
                }
            }
        }
    }

    if (option & TEST_CONSECUTIVE_DOT) {
        if (MaxConsecutiveDirtyDots >= pImgTestCriteria->consecutive_fail_th) {
            bTooManyDirtyDots = TRUE;
        }
    }

    if (option & TEST_FOUR_LEAF) {
        for (i = 0; i < height - 1; i++) {
            for (j = 0; j < width - 1; j++) {
                if (DirtyDots[i * width + j]) {
                    // '\A4f'
                    if (DirtyDots[(i + 1) * width + j] && DirtyDots[(i + 1) * width + j + 1] &&
                        DirtyDots[i * width + j + 1]) {
                        bTooManyDirtyDots = TRUE;

                        if (4 > MaxConsecutiveDirtyDots) {
                            MaxConsecutiveDirtyDots = 4;
                        }
                    }
                }
            }
        }
    }

    // Maximum Connected Fail Pixels
    BOOL IsHasStraitLine = FALSE;
    BOOL IsHasAwkwardShape = FALSE;

    if (option & TEST_CONNECTED_FAIL) {
        MaxConnectedDirtyDots =
            two_pass_algo(DirtyDots, width, height, option, &IsHasStraitLine, &IsHasAwkwardShape,
                          pImgTestCriteria->consecutive_fail_th);
        if (MaxConnectedDirtyDots >= pImgTestCriteria->consecutive_fail_th) {
            bTooManyDirtyDots = TRUE;
        }
    }

    // count all fail pxls/////////////////
    AllFailDots = 0;
    if (option & TEST_DARK_DOT) {
        AllFailDots += DarkDots;
    }
    if (option & TEST_WHITE_DOT) {
        AllFailDots += WhiteDots;
    }

    if (option & TEST_DEFECT_DOT) {
        AllFailDots += DefectDots;
    }
    if (AllFailDots > img_test_criteria.max_allowed_fail_pxl) {
        bTooManyDirtyDots = TRUE;
    }

    if (pImgTestResult) {
        pImgTestResult->AvergeInDouble = pxl_avg_double;
        pImgTestResult->Average = pxl_avg;
        pImgTestResult->Deviation = std_dev;

        if (option & TEST_DARK_DOT) {
            pImgTestResult->DarkDots = DarkDots;
        }

        if (option & TEST_WHITE_DOT) {
            pImgTestResult->WhiteDots = WhiteDots;
        }

        if (option & TEST_DEFECT_DOT) {
            pImgTestResult->DefectDots = DefectDots;
        }

        if (option & (TEST_CONSECUTIVE_DOT | TEST_FOUR_LEAF)) {
            pImgTestResult->MaxConsecutiveDirtyDots = MaxConsecutiveDirtyDots;
        }

        if (option & TEST_CONNECTED_FAIL) {
            pImgTestResult->MaxConnectedDirtyDots = MaxConnectedDirtyDots;
            pImgTestResult->HasStraitLine = IsHasStraitLine;
            pImgTestResult->HasAwkwardShape = IsHasAwkwardShape;
        }
    }

    if (pTooManyDirtyDots) {
        *pTooManyDirtyDots = bTooManyDirtyDots;
    }

    // copy dirty dots
    if (pDirtyDots) {
        for (i = 0; i < width * height; i++) {
            if (DirtyDots[i]) {
                pDirtyDots[i] |= DirtyDots[i];
            }
        }
    }

    free(DirtyDots);
}

void vEvaluateImage8(ImgTestCriteria* pImgTestCriteria, ImgTestResult* pImgTestResult,
                     BOOL* pTooManyDirtyDots, unsigned char* pDirtyDots,
                     Image_Valid_Area* pScanArea, unsigned char* pImage, int width, int height,
                     unsigned char option) {
    int i, j;

    unsigned int pxl_sum = 0;
    unsigned char pxl_avg = 0;

    double pxl_avg_double = 0.0;
    double std_dev;

    BOOL bTooManyDirtyDots;
    int WhiteDots;
    int DarkDots;
    int DefectDots;

    int AllFailDots;
    int MaxConsecutiveDirtyDots;
    int MaxConnectedDirtyDots;

    ImgTestCriteria img_test_criteria;

    unsigned char* DirtyDots;
    DirtyDots = (unsigned char*)malloc(width * height);
    if (!DirtyDots) {
        return;
    }

    int test_width;
    int test_height;
    int test_total;

    test_width = pScanArea->col_end - pScanArea->col_start + 1;
    test_height = pScanArea->row_end - pScanArea->row_start + 1;
    test_total = test_width * test_height;

    WhiteDots = 0;
    DarkDots = 0;
    DefectDots = 0;
    MaxConsecutiveDirtyDots = 0;
    MaxConnectedDirtyDots = 0;
    AllFailDots = 0;
    bTooManyDirtyDots = FALSE;

    memset(DirtyDots, 0x00, width * height);

    memset(&img_test_criteria, 0x00, sizeof(img_test_criteria));
    if (pImgTestCriteria) {
        memcpy(&img_test_criteria, pImgTestCriteria, sizeof(img_test_criteria));
    } else {
        img_test_criteria.dark_dot_th = 0;
        img_test_criteria.white_dot_th = 255;
        img_test_criteria.cds_bad_dot_th = 255;
        img_test_criteria.consecutive_fail_th = width * height;
        img_test_criteria.max_allowed_fail_pxl = width * height;
    }

    // mark white and dark pixels
    for (i = pScanArea->row_start; i <= pScanArea->row_end; i++) {
        for (j = pScanArea->col_start; j <= pScanArea->col_end; j++) {
            pxl_sum += pImage[i * width + j];
            if (option & TEST_WHITE_DOT) {
                if (pImage[i * width + j] > img_test_criteria.white_dot_th) {
                    WhiteDots++;
                    DirtyDots[i * width + j] |= TEST_WHITE_DOT;
                }
            }

            if (option & TEST_DARK_DOT) {
                if (pImage[i * width + j] < img_test_criteria.dark_dot_th) {
                    DarkDots++;
                    DirtyDots[i * width + j] |= TEST_DARK_DOT;
                }
            }
        }
    }
    // whole image average
    pxl_avg = (unsigned char)((pxl_sum / test_total) + 0.5);
    pxl_avg_double = (double)pxl_sum / (double)test_total;

    // calculate whole image stddev
    pxl_sum = 0;
    for (i = pScanArea->row_start; i <= pScanArea->row_end; i++) {
        for (j = pScanArea->col_start; j <= pScanArea->col_end; j++) {
            pxl_sum += (pImage[i * width + j] - pxl_avg) * (pImage[i * width + j] - pxl_avg);
        }
    }
    std_dev = sqrt((double)pxl_sum / test_total);

    // Test Defect Dots
    if (option & TEST_DEFECT_DOT) {
        for (i = pScanArea->row_start; i <= pScanArea->row_end; i++) {
            for (j = pScanArea->col_start; j <= pScanArea->col_end; j++) {
                if (abs(pImage[i * width + j] - pxl_avg) > img_test_criteria.cds_bad_dot_th) {
                    DefectDots++;
                    DirtyDots[i * width + j] |= TEST_DEFECT_DOT;
                }
            }
        }
    }

    for (i = 0; i < height; i++) {
        for (j = 0; j < width; j++) {
            if (DirtyDots[i * width + j]) {
                int consective_test;

                // '|' dots
                consective_test = 1;
                while ((i + consective_test) < height) {
                    if (DirtyDots[(i + consective_test) * width + j]) {
                        consective_test++;
                    } else {
                        break;
                    }
                }
                if (consective_test > MaxConsecutiveDirtyDots) {
                    MaxConsecutiveDirtyDots = consective_test;
                }

                // '--' dots
                consective_test = 1;
                while ((j + consective_test) < width) {
                    if (DirtyDots[i * width + j + consective_test]) {
                        consective_test++;
                    } else {
                        break;
                    }
                }
                if (consective_test > MaxConsecutiveDirtyDots) {
                    MaxConsecutiveDirtyDots = consective_test;
                }

                // '/' dots
                consective_test = 1;
                while (((i + consective_test) < height) && ((j + consective_test) < width)) {
                    if (DirtyDots[(i + consective_test) * width + (j + consective_test)]) {
                        consective_test++;
                    } else {
                        break;
                    }
                }
                if (consective_test > MaxConsecutiveDirtyDots) {
                    MaxConsecutiveDirtyDots = consective_test;
                }

                // '\' dots
                consective_test = 1;
                while (((i + consective_test) < height) && (j >= consective_test)) {
                    if (DirtyDots[(i + consective_test) * width + (j - consective_test)]) {
                        consective_test++;
                    } else {
                        break;
                    }
                }
                if (consective_test > MaxConsecutiveDirtyDots) {
                    MaxConsecutiveDirtyDots = consective_test;
                }
            }
        }
    }

    if (option & TEST_CONSECUTIVE_DOT) {
        if (MaxConsecutiveDirtyDots >= pImgTestCriteria->consecutive_fail_th) {
            bTooManyDirtyDots = TRUE;
        }
    }

    if (option & TEST_FOUR_LEAF) {
        for (i = 0; i < height - 1; i++) {
            for (j = 0; j < width - 1; j++) {
                if (DirtyDots[i * width + j]) {
                    if (DirtyDots[(i + 1) * width + j] && DirtyDots[(i + 1) * width + j + 1] &&
                        DirtyDots[i * width + j + 1]) {
                        bTooManyDirtyDots = TRUE;

                        if (4 > MaxConsecutiveDirtyDots) {
                            MaxConsecutiveDirtyDots = 4;
                        }
                    }
                }
            }
        }
    }

    // Maximum Connected Fail Pixels
    BOOL IsHasStraitLine = FALSE;
    BOOL IsHasAwkwardShape = FALSE;

    if (option & TEST_CONNECTED_FAIL) {
        MaxConnectedDirtyDots =
            two_pass_algo(DirtyDots, width, height, option, &IsHasStraitLine, &IsHasAwkwardShape,
                          pImgTestCriteria->consecutive_fail_th);
        if (MaxConnectedDirtyDots >= pImgTestCriteria->consecutive_fail_th) {
            bTooManyDirtyDots = TRUE;
        }
    }

    // count all fail pxls/////////////////
    AllFailDots = 0;
    if (option & TEST_DARK_DOT) {
        AllFailDots += DarkDots;
    }
    if (option & TEST_WHITE_DOT) {
        AllFailDots += WhiteDots;
    }

    if (option & TEST_DEFECT_DOT) {
        AllFailDots += DefectDots;
    }
    if (AllFailDots > img_test_criteria.max_allowed_fail_pxl) {
        bTooManyDirtyDots = TRUE;
    }

    if (pImgTestResult) {
        pImgTestResult->AvergeInDouble = pxl_avg_double;
        pImgTestResult->Average = pxl_avg;
        pImgTestResult->Deviation = std_dev;

        if (option & TEST_DARK_DOT) {
            pImgTestResult->DarkDots = DarkDots;
        }

        if (option & TEST_WHITE_DOT) {
            pImgTestResult->WhiteDots = WhiteDots;
        }

        if (option & TEST_DEFECT_DOT) {
            pImgTestResult->DefectDots = DefectDots;
        }

        if (option & (TEST_CONSECUTIVE_DOT | TEST_FOUR_LEAF)) {
            pImgTestResult->MaxConsecutiveDirtyDots = MaxConsecutiveDirtyDots;
        }

        if (option & TEST_CONNECTED_FAIL) {
            pImgTestResult->MaxConnectedDirtyDots = MaxConnectedDirtyDots;
            pImgTestResult->HasStraitLine = IsHasStraitLine;
            pImgTestResult->HasAwkwardShape = IsHasAwkwardShape;
        }
    }

    if (pTooManyDirtyDots) {
        *pTooManyDirtyDots = bTooManyDirtyDots;
    }

    // copy dirty dots
    if (pDirtyDots) {
        for (i = 0; i < width * height; i++) {
            if (DirtyDots[i]) {
                pDirtyDots[i] |= DirtyDots[i];
            }
        }
    }

    free(DirtyDots);
}

void LensCentroid(UCHAR* a_Image, USHORT a_Width, USHORT a_Height, UINT* x, UINT* y) {
#define QUANTIZE_FACTOR 1000

    UINT Sum_x[1024] = {0};
    UINT Sum_y[1024] = {0};
    UINT index_x1 = 0, index_y1 = 0;
    UINT index_x2 = 0, index_y2 = 0;
    UINT max_x1 = 0, max_y1 = 0;
    UINT max_x2 = 0, max_y2 = 0;
    UINT crop = 0;
    UINT i, j;

    UINT value = 0;
    UINT index = 0;

    for (i = 0; i < a_Width; i++) {
        for (j = 0; j < a_Height; j++) {
            Sum_x[i] += a_Image[j * a_Width + i];
        }
        Sum_x[i] /= QUANTIZE_FACTOR;
    }
    for (i = 0; i < a_Height; i++) {
        for (j = 0; j < a_Width; j++) {
            Sum_y[i] += a_Image[i * a_Width + j];
        }
        Sum_y[i] /= QUANTIZE_FACTOR;
    }

    for (i = 0; i < a_Width; i++) {
        if (Sum_x[i] > max_x1) {
            max_x1 = Sum_x[i];
            index_x1 = i;
        }
        index = (a_Width - 1 - i);
        if (Sum_x[index] > max_x2) {
            max_x2 = Sum_x[index];
            index_x2 = index;
        }
    }

    for (i = 0; i < a_Height; i++) {
        if (Sum_y[i] > max_y1) {
            max_y1 = Sum_y[i];
            index_y1 = i;
        }
        index = (a_Height - 1 - i);
        if (Sum_y[index] > max_y2) {
            max_y2 = Sum_y[index];
            index_y2 = index;
        }
    }

    *x = (index_x1 + index_x2) / 2 + 1;  // +1 for display start with 1
    *y = (index_y1 + index_y2) / 2 + 1;
}

#if defined(WIN32)
LARGE_INTEGER cpu_clock;
LARGE_INTEGER time_last;
#endif

void QueryExecutionTimeInit() {
#if defined(WIN32)
    QueryPerformanceFrequency(&cpu_clock);
    QueryPerformanceCounter(&time_last);
#endif
}

int QueryExecutionTimeUs() {
#if defined(WIN32)
    LARGE_INTEGER time_now;
    LARGE_INTEGER time_elapsed;

    QueryPerformanceCounter(&time_now);

    time_elapsed.QuadPart = time_now.QuadPart - time_last.QuadPart;
    time_elapsed.QuadPart *= 1000000;
    time_elapsed.QuadPart /= cpu_clock.QuadPart;

    time_last = time_now;

    return (int)time_elapsed.QuadPart;
#else
    return 0;
#endif
}

double QueryExecutionTimeMs() {
    return ((double)QueryExecutionTimeUs() / 1000);
}

#ifndef TZ_MODE
void sprintf_round_down(char* outbuf, double value, int dec_point_num) {
    int i;
    char buf[MAX_PATH];
    float chop_value;

    egist_snprintf(outbuf, MAX_PATH, "%d.", (int)value);

    chop_value = (float)(value - (int)value);

    for (i = 0; i < dec_point_num; i++) {
        chop_value *= 10;
        sprintf(buf, "%d", (int)chop_value);
        strcat(outbuf, buf);
        chop_value -= (int)chop_value;
    }
}
#endif
