#ifndef _EGIS_FP_CONFIG_H
#define _EGIS_FP_CONFIG_H

#include "egis_fp_sensor_define.h"
#include "type_definition.h"

#ifdef EgisFPSensorTestDll_EXPORTS
#define EgisFPSensorTestDll_API __declspec(dllexport)
#else
#define EgisFPSensorTestDll_API  //__declspec(dllimport)
#endif

#ifdef RBS_SDK_USE

#endif

#ifndef MAX_PATH
#define MAX_PATH 260
#endif

#define MAX_DEVICE_NUM 16

typedef struct _BOARD_INSTANCE {
    int PortIndex;
    int TestSequence;
    int ComPortNum;
    HANDLE DeviceHandle;
    unsigned char bIsMaster;
    unsigned char bIsFound;
    unsigned char TestStatus;
    unsigned char unique_id[17];
    int major_item_tested;
    double CurrentOffset33;
    double CurrentOffset18;
} BOARD_INSTANCE, *pBOARD_INSTANCE;

typedef struct _EGFPS_GLOBAL_CONFIG {
    // test program name
    char TestProgramName[MAX_PATH];

    // test config file
    char TestConfigFile[MAX_PATH];

    // test log folder
    char TestLogFolder[MAX_PATH];
    int TestSequence;
    BOOL ShowTestDetail;
    int SaveImage;

    // test mode
    int TestMode;

    // Backlight LED
    int backlight_led_always_on;
    int backlight_led_control_board_type;  // 0:old(PWM), 1:new(DC)
    int backlight_led_check_valid;
    unsigned char backlight_led_gain[MAX_DEVICE_NUM];
    unsigned short backlight_led_lightness[MAX_DEVICE_NUM];
    unsigned short backlight_led_lightness_max;
    unsigned short backlight_led_lightness_min;

    int backlight_check_range;
    int backlight_range_max;
    int backlight_range_min;

    // number of test sites;
    int NumberOfSites;

    // board instance
    BOARD_INSTANCE BoardInstances[MAX_DEVICE_NUM];

} EGFPS_GLOBAL_CONFIG;

enum {
    SAVE_IMAGE_TESTPATTERN_INDEX = 0,
    SAVE_IMAGE_DARK_LEVEL_INDEX = 1,
    SAVE_IMAGE_CENTROID_INDEX = 2,
    SAVE_IMAGE_MAX_EXPO_INDEX = 3,
    SAVE_IMAGE_CALIBRATION_INDEX = 4,
    SAVE_IMAGE_BAD_PXL_INDEX = 5,
    SAVE_IMAGE_BAD_BLOCK_PXL_INDEX = 6,
    SAVE_IMAGE_MTF_INDEX = 7,
    SAVE_IMAGE_AVG_RV_INDEX = 8,
    SAVE_IMAGE_INT_RV_INDEX = 9,
    SAVE_IMAGE_MAX = 10
};

#define SAVE_IMAGE_TESTPATTERN (0x00000001 << SAVE_IMAGE_TESTPATTERN_INDEX)
#define SAVE_IMAGE_DARK_LEVEL (0x00000001 << SAVE_IMAGE_DARK_LEVEL_INDEX)
#define SAVE_IMAGE_CENTROID (0x00000001 << SAVE_IMAGE_CENTROID_INDEX)
#define SAVE_IMAGE_MAX_EXPO (0x00000001 << SAVE_IMAGE_MAX_EXPO_INDEX)
#define SAVE_IMAGE_CALIBRATION (0x00000001 << SAVE_IMAGE_CALIBRATION_INDEX)
#define SAVE_IMAGE_BAD_PXL (0x00000001 << SAVE_IMAGE_BAD_PXL_INDEX)
#define SAVE_IMAGE_BAD_BLOCK_PXL (0x00000001 << SAVE_IMAGE_BAD_BLOCK_PXL_INDEX)
#define SAVE_IMAGE_MTF (0x00000001 << SAVE_IMAGE_MTF_INDEX)
#define SAVE_IMAGE_AVG_RV (0x00000001 << SAVE_IMAGE_AVG_RV_INDEX)
#define SAVE_IMAGE_INT_RV (0x00000001 << SAVE_IMAGE_INT_RV_INDEX)

typedef struct _EGFPS_TEST_CONFIG {
    // Tester
    int ConfigVersion;
    int bin2_is_pass_bin;

    // sensor setting
    char SensorSettingFile[MAX_PATH];
    int ResetHold;
    int do_load_setting_file;

    // test pattern
    int check_test_pattern;

    // dark level test
    int do_dark_img_test;
    double dark_expo_time;
    int dark_level_th;
    int white_pixel_th;
    int max_white_pixel;

    // Max Exposure Time Test
    int do_max_expo_test;
    int max_expo_gain;
    double max_expo_time;
    int dark_pixel_th;
    int dark_pixel_th2;
    int max_dark_pixel;

    // Img Calibration Test
    int do_img_calibration_test;
    int img_calibration_method;
    int img_gain;
    int ImageAverageCount;
    double img_expo_time;
    unsigned short img_target_mean;
    int img_background_cancellation;
    int check_background_avg;
    int bkg_avg_min;
    int bkg_avg_max;
    double img_max_noise;

    // abnormal pixel test
    int do_bad_pxl_test;
    double bad_pxl_percentage;
    int max_bad_pxl;
    double bad_rowcol_percentage;
    int max_bad_rowcol;
    int do_consecutive_fail_test;
    int consecutive_fail_pxl;

    // abnormal pixel test2
    double bad_pxl_percentage2;
    int max_bad_pxl2;
    double bad_rowcol_percentage2;
    int max_bad_rowcol2;
    int do_consecutive_fail_test2;
    int consecutive_fail_pxl2;

    // block bad pixel test
    int do_bad_block_test;
    double bad_block_pixel_percentage;
    int block_size_width;
    int block_size_height;
    int block_scan_step;
    int max_bad_block;
    int max_bad_pixel_block;
    int max_bad_pixel_block_all;

    int do_consecutive_fail_test_block;
    int consecutive_fail_pxl_block;

    // Dynamic Range Test
    int DynamicRangeTestOption;  // 1: average, 2: integration 3: both
    int ImageIntegrationCount;
    int ImageIntegrationRV16Min;
    int ImageAverageRV16Min;

    // Power Consumption Test
    int PowerConsumptionTest;
    double MaxPowerConsumption33;
    double MinPowerConsumption33;
    double MaxPowerConsumption18;

    int PowerConsumptionTestPowerDown;
    double MaxPowerConsumption33Pd;
    double MinPowerConsumption33Pd;
    double MaxPowerConsumption18Pd;

    int PowerConsumptionTest18;

    // LENS_TEST
    int do_lens_centeroid_test;
    double centeroid_expo_time;
    int lens_design_center_x;
    int lens_design_center_y;
    int lens_center_tolerance_x;
    int lens_center_tolerance_y;
    int lens_roi_x;
    int lens_roi_y;
    int lens_center_tolerance_line;

    int do_lens_mtf_test;
    int lens_mtf_scan_window;
    int lens_mtf_test_case;
    double lens_mtf_center_floor;
    double lens_mtf_center_floor2;
    double lens_mtf_corner_floor;
    double lens_mtf_corner_floor2;

    // MP INFO
    int ProductionPlant;
    int ProjectStep;

    // NVM
    int package_has_flash;
    int check_flash_data_valid;
    int write_flash_data;
    int check_otp_data_valid;
    int write_otp_data;

} EGFPS_TEST_CONFIG;

#ifdef __cplusplus
extern "C" {
#endif

void EgisFPSensorTestDll_API vLoadGlobalTestConfig(EGFPS_GLOBAL_CONFIG* pEgfpsGlobalConfig);
void EgisFPSensorTestDll_API vSaveGlobalTestConfig(EGFPS_GLOBAL_CONFIG* pEgfpsGlobalConfig);

BOOL EgisFPSensorTestDll_API bLoadTestConfig(EGFPS_TEST_CONFIG* pEgfpsTestConfig,
                                             char* pConfigFile);
void EgisFPSensorTestDll_API vSaveTestConfig(EGFPS_TEST_CONFIG* pEgfpsTestConfig,
                                             char* pConfigFile);

void EgisFPSensorTestDll_API vBuildConfigHash(EGFPS_TEST_CONFIG* pEgfpsTestConfig,
                                              unsigned char* pDigest);

#ifdef __cplusplus
}
#endif

#endif
