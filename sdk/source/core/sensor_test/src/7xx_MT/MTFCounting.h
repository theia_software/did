#ifndef __MTFCOUNTING_H__
#define __MTFCOUNTING_H__

#ifdef __cplusplus
extern "C" {
#endif

typedef enum {
    MTFMiddle = 1,
    MTFUp,
    MTFLeft,
    MTFRight,
    MTFDown,
    MTFTop,
    MTFLeft1,
    TFRight1,
    MTFBottom,
} MTFBlockOption;

double MTFCounting(unsigned char* a_Image, unsigned int a_Width, unsigned int a_Height,
                   unsigned int a_centroid_x, unsigned int a_centroid_y, MTFBlockOption a_Option,
                   unsigned char a_test_case, unsigned int a_MTF_Scan_window, double* a_MTFValue,
                   unsigned int* a_Mean, unsigned char* a_RV);

double MTFCounting713(unsigned char* a_Image, unsigned int a_Width, unsigned int a_Height,
                      unsigned int a_centroid_x, unsigned int a_centroid_y, MTFBlockOption a_Option,
                      unsigned char a_test_case, unsigned int a_MTF_Scan_window, double* a_MTFValue,
                      unsigned int* a_Mean, unsigned char* a_RV);

#ifdef __cplusplus
};
#endif

#endif
