#if defined(WIN32)
#include <memory.h>
#include <stdlib.h>

#include "MTFCounting.h"
#elif defined(RBS_SDK_USE)
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "MTFCounting.h"
#include "plat_mem.h"
#endif

#define MTF_BLOCK_SIZE_X 40
#define MTF_BLOCK_SIZE_Y 40

#define FALSE 0
#define TRUE 1

#ifndef NULL
#define NULL ((void*)0)
#endif

unsigned char ucFindOtsuRV(unsigned char* pPicture, int width, int height) {
    float otsu;

    float Wb;
    float ub;

    float Wf;
    float uf;

    int th;

    int max_th;
    float max_otsu;

    unsigned char bg_avg;
    unsigned char fg_avg;

    int bg_num;
    int fg_num;

    max_th = 0;
    max_otsu = 0;

    bg_avg = 0;
    fg_avg = 0;
    bg_num = 0;
    fg_num = 0;

    for (th = 0; th < 256; th++) {
        int i;

        int nB = 0;
        int sumB = 0;

        int nF = 0;
        int sumF = 0;

        for (i = 0; i < width * height; i++) {
            if (pPicture[i] > th) {
                nF++;
                sumF += pPicture[i];
            } else {
                nB++;
                sumB += pPicture[i];
            }
        }

        if ((nB == 0) || (nF == 0)) {
            continue;
        }

        Wb = (float)nB / ((float)width * height);
        Wf = (float)nF / ((float)width * height);

        ub = (float)sumB / (float)nB;  // avg of background
        uf = (float)sumF / (float)nF;  // avg of foreground

        otsu = Wb * Wf * (ub - uf) * (ub - uf);

        if (otsu > max_otsu) {
            max_otsu = otsu;
            max_th = th;

            bg_avg = (unsigned char)ub;
            fg_avg = (unsigned char)uf;
            bg_num = nB;
            fg_num = nF;
        }
    }

    return (fg_avg - bg_avg);
}

#define MAX_MTF_COUNTING_ARRAY_SIZE 2048
double MTFCalculatingSvalue(unsigned char* DataBuffer, unsigned short block_x,
                            unsigned short block_y, unsigned short scan_window, unsigned int* MTFA,
                            unsigned int* MTFB) {
    unsigned int m, i, j;
    unsigned char bFindingMax = TRUE;
    unsigned char data_array[512];
    unsigned char local_ridge_array[MAX_MTF_COUNTING_ARRAY_SIZE];
    unsigned char local_valley_array[MAX_MTF_COUNTING_ARRAY_SIZE];
    unsigned char LocalPeak = 0;
    unsigned int ridge_count = 0;
    unsigned int valley_count = 0;

    memset(local_ridge_array, 0x00, MAX_MTF_COUNTING_ARRAY_SIZE);
    memset(local_valley_array, 0xFF, MAX_MTF_COUNTING_ARRAY_SIZE);

    for (m = 0; m < block_y; m++) {
        // copy data to data array
        memcpy(data_array, (DataBuffer + (m * block_x)), block_x);

        bFindingMax = TRUE;
        LocalPeak = 0;

        for (i = 0; i < (unsigned int)(block_x - scan_window); i++) {
            if (bFindingMax) {
                if (data_array[i] > LocalPeak) {
                    LocalPeak = data_array[i];
                }
                for (j = i; j < i + scan_window; j++) {
                    if (data_array[j] > LocalPeak) {
                        break;
                    }
                }
                if (j == i + scan_window) {
                    bFindingMax = FALSE;
                    local_ridge_array[ridge_count++] = LocalPeak;
                    LocalPeak = 0xFF;
                }
            } else {
                if (data_array[i] < LocalPeak) {
                    LocalPeak = data_array[i];
                }
                for (j = i; j < i + scan_window; j++) {
                    if (data_array[j] < LocalPeak) {
                        break;
                    }
                }
                if (j == i + scan_window) {
                    bFindingMax = TRUE;
                    local_valley_array[valley_count++] = LocalPeak;
                    LocalPeak = 0x00;
                }
            }
        }
    }

    // valley_count = valley_count;

    unsigned int sum = 0;

    for (i = 0; i < ridge_count; i++) {
        sum += local_ridge_array[i];
    }

    if (ridge_count == 0) {
        *MTFA = 0;
    } else {
        *MTFA = sum / ridge_count;
    }

    sum = 0;

    for (i = 0; i < valley_count; i++) {
        sum += local_valley_array[i];
    }

    if (valley_count == 0) {
        *MTFB = 0;
    } else {
        *MTFB = sum / valley_count;
    }

    if ((ridge_count == 0) || (valley_count == 0)) {
        *MTFA = 0;
        *MTFB = 0;
    }
    return TRUE;
}

double MTFCalculatingTvalue(unsigned char* DataBuffer, unsigned short block_x,
                            unsigned short block_y, unsigned short scan_window, unsigned int* MTFA,
                            unsigned int* MTFB) {
    unsigned int m, n, i, j;
    unsigned char bFindingMax = TRUE;
    unsigned char data_array[512];
    unsigned char local_ridge_array[MAX_MTF_COUNTING_ARRAY_SIZE];
    unsigned char local_valley_array[MAX_MTF_COUNTING_ARRAY_SIZE];
    unsigned char LocalPeak = 0;
    unsigned int ridge_count = 0;
    unsigned int valley_count = 0;

    memset(local_ridge_array, 0x00, MAX_MTF_COUNTING_ARRAY_SIZE);
    memset(local_valley_array, 0xFF, MAX_MTF_COUNTING_ARRAY_SIZE);

    for (m = 0; m < block_x; m++) {
        // copy data to data array
        for (n = 0; n < block_y; n++) {
            data_array[n] = DataBuffer[n * block_x + m];
        }

        bFindingMax = TRUE;
        LocalPeak = 0;

        for (i = 0; i < (unsigned int)(block_y - scan_window); i++) {
            if (bFindingMax) {
                if (data_array[i] > LocalPeak) {
                    LocalPeak = data_array[i];
                }
                for (j = i; j < i + scan_window; j++) {
                    if (data_array[j] > LocalPeak) {
                        break;
                    }
                }
                if (j == i + scan_window) {
                    bFindingMax = FALSE;
                    local_ridge_array[ridge_count++] = LocalPeak;
                    LocalPeak = 0xFF;
                }
            } else {
                if (data_array[i] < LocalPeak) {
                    LocalPeak = data_array[i];
                }
                for (j = i; j < i + scan_window; j++) {
                    if (data_array[j] < LocalPeak) {
                        break;
                    }
                }
                if (j == i + scan_window) {
                    bFindingMax = TRUE;
                    local_valley_array[valley_count++] = LocalPeak;
                    LocalPeak = 0x00;
                }
            }
        }
    }

    unsigned int sum = 0;

    for (i = 0; i < ridge_count; i++) {
        sum += local_ridge_array[i];
    }

    if (ridge_count == 0) {
        *MTFA = 0;
    } else {
        *MTFA = sum / ridge_count;
    }

    sum = 0;

    for (i = 0; i < valley_count; i++) {
        sum += local_valley_array[i];
    }

    if (valley_count == 0) {
        *MTFB = 0;
    } else {
        *MTFB = sum / valley_count;
    }

    if ((ridge_count == 0) || (valley_count == 0)) {
        *MTFA = 0;
        *MTFB = 0;
    }

    return TRUE;
}

double MTFCalculatingValue(unsigned char* DataBuffer, unsigned short block_x,
                           unsigned short block_y, unsigned char direction,
                           unsigned short scan_window, unsigned int* MTFA, unsigned int* MTFB,
                           unsigned int* Ridge, unsigned int* Valley) {
    unsigned int m, n, i, j;
    unsigned char bFindingMax = TRUE;
    unsigned char data_array[512];
    unsigned char local_ridge_array[MAX_MTF_COUNTING_ARRAY_SIZE];
    unsigned char local_valley_array[MAX_MTF_COUNTING_ARRAY_SIZE];
    unsigned char LocalRidge = 0;
    unsigned char LocalRidgeIndex = 0;
    unsigned char LocalValley = 0;
    unsigned char LocalValleyIndex = 0;
    unsigned char LocalPeak = 0;
    unsigned char LocalPeakIndex = 0;
    unsigned int ridge_count = 0;
    unsigned int valley_count = 0;
    unsigned int block_size;

    memset(local_ridge_array, 0x00, MAX_MTF_COUNTING_ARRAY_SIZE);
    memset(local_valley_array, 0xFF, MAX_MTF_COUNTING_ARRAY_SIZE);

    if ((scan_window % 2) == 0) {
        scan_window++;
    }

    unsigned short offset = scan_window / 2;

    for (m = 0; m < block_y; m++) {
        if (direction == 2)  // vertical
        {
            for (n = 0; n < block_y; n++) {
                data_array[n] = DataBuffer[n * block_x + m];
            }
            block_size = block_y;
        } else  // horizontal
        {
            // copy data to data array
            memcpy(data_array, (DataBuffer + (m * block_x)), block_x);
            block_size = block_x;
        }

        for (i = offset; i < (unsigned int)(block_size - offset); i++) {
            LocalRidge = 0;
            LocalRidgeIndex = 0;
            LocalValley = 0xFF;
            LocalValleyIndex = 0;
            for (j = (i - offset); j <= (i + offset); j++) {
                if (data_array[j] > LocalRidge) {
                    LocalRidgeIndex = j;
                    LocalRidge = data_array[j];
                }
            }
            for (j = (i - offset); j <= (i + offset); j++) {
                if (data_array[j] < LocalValley) {
                    LocalValleyIndex = j;
                    LocalValley = data_array[j];
                }
            }
            if (LocalRidgeIndex == i) {
                local_ridge_array[ridge_count++] = data_array[i];
            }
            if (LocalValleyIndex == i) {
                local_valley_array[valley_count++] = data_array[i];
            }
        }
    }

    unsigned int sum = 0;

    for (i = 0; i < ridge_count; i++) {
        sum += local_ridge_array[i];
    }

    if (ridge_count == 0) {
        *MTFA = 0;
    } else {
        *MTFA = sum / ridge_count;
    }

    sum = 0;

    for (i = 0; i < valley_count; i++) {
        sum += local_valley_array[i];
    }

    if (valley_count == 0) {
        *MTFB = 0;
    } else {
        *MTFB = sum / valley_count;
    }

    if ((ridge_count == 0) || (valley_count == 0)) {
        *MTFA = 0;
        *MTFB = 0;
    } else {
        *Ridge = ridge_count;
        *Valley = valley_count;
    }
    return TRUE;
}

double MTFCounting(unsigned char* a_Image, unsigned int a_Width, unsigned int a_Height,
                   unsigned int a_centroid_x, unsigned int a_centroid_y, MTFBlockOption a_Option,
                   unsigned char a_test_case, unsigned int a_MTF_Scan_window, double* a_MTFValue,
                   unsigned int* a_Mean, unsigned char* a_RV) {
    double SMTFValue = 0.0;
    double TMTFValue = 0.0;
    int x1, x2;
    int y1, y2;
    unsigned int pxl_count = 0;
    int i, j;

    unsigned int average = 0;
    unsigned char RV = 0;

    // prepare counting buffer
    unsigned char* DataBuffer = NULL;
    unsigned short block_x = 0;
    unsigned short block_y = 0;

    int offset_x, offset_y;

    if (a_Option == 8 || a_Option == 9) {
        if (a_Mean != NULL) {
            *a_Mean = 0;
        }
        if (a_RV != NULL) {
            *a_RV = 0;
        }
        *a_MTFValue = 0;
        return *a_MTFValue;
    }

    block_x = MTF_BLOCK_SIZE_X;
    block_y = MTF_BLOCK_SIZE_Y;

    DataBuffer = (unsigned char*)plat_alloc(block_x * block_y);

    //                       {0 , 1   2   3   4   5   6   7   8  9}
    unsigned int x_ary[10] = {0, 92, 52, 132, 12, 172, 92, 92, 0, 0};
    unsigned int y_ary[10] = {0, 53, 53, 53, 53, 53, 26, 93, 0, 0};

    // Prepare x,y index
    x1 = x_ary[a_Option];
    y1 = y_ary[a_Option];
    x2 = x1 + block_x;
    y2 = y1 + block_y;

    // Prepare offset value for align to centroid
    offset_x = a_centroid_x - (a_Width / 2);
    offset_y = a_centroid_y - (a_Height / 2);

    // Limited the offset x and y under one block_x or block_y
    if (offset_x >= block_x) offset_x = block_x;
    if (offset_x <= -block_x) offset_x = -block_x;
    if (offset_y >= block_y) offset_y = block_y;
    if (offset_y <= -block_y) offset_y = -block_y;

    x1 += offset_x;
    x2 += offset_x;

    y1 += offset_y;
    y2 += offset_y;

    if (x1 < 0) x1 = 0;
    if (x1 > (int)a_Width) x1 = (int)a_Width;

    if (x2 < 0) x2 = 0;
    if (x2 > (int)a_Width) x2 = (int)a_Width;

    if (y1 < 0) y1 = 0;
    if (y1 > (int)a_Height) y1 = (int)a_Height;

    if (y2 < 0) y2 = 0;
    if (y2 > (int)a_Height) y2 = (int)a_Height;

    // copy target data to buffer
    for (j = y1; j < y2; j++) {
        for (i = x1; i < x2; i++) {
            DataBuffer[pxl_count++] = a_Image[j * a_Width + i];
            average += a_Image[j * a_Width + i];
        }
    }
    // pxl_count = pxl_count;

    if (average > 0) {
        average /= pxl_count;
    }

    unsigned int SMTFA = 0, SMTFB = 0;
    unsigned int TMTFA = 0, TMTFB = 0;

    switch (a_test_case) {
        case 1:
            MTFCalculatingSvalue(DataBuffer, block_x, block_y, a_MTF_Scan_window, &SMTFA, &SMTFB);
            break;
        case 2:
            MTFCalculatingTvalue(DataBuffer, block_x, block_y, a_MTF_Scan_window, &TMTFA, &TMTFB);
            break;
        case 3:
            MTFCalculatingSvalue(DataBuffer, block_x, block_y, a_MTF_Scan_window, &SMTFA, &SMTFB);
            MTFCalculatingTvalue(DataBuffer, block_x, block_y, a_MTF_Scan_window, &TMTFA, &TMTFB);
            break;
    }

    if (((SMTFA + SMTFB) == 0) || (SMTFA < SMTFB)) {
        SMTFValue = 0;
    } else {
        SMTFValue = (double)(SMTFA - SMTFB) / (SMTFA + SMTFB);
    }

    if (((TMTFA + TMTFB) == 0) || (TMTFA < TMTFB)) {
        TMTFValue = 0;
    } else {
        TMTFValue = (double)(TMTFA - TMTFB) / (TMTFA + TMTFB);
    }

    // RV counting
    RV = ucFindOtsuRV(DataBuffer, block_x, block_y);

    // plat_free data
    if (DataBuffer != NULL) {
        plat_free(DataBuffer);
    }

    if (a_MTFValue != NULL) {
        switch (a_test_case) {
            case 1:
                *a_MTFValue = SMTFValue;
                break;
            case 2:
                *a_MTFValue = TMTFValue;
                break;
            case 3:
                *a_MTFValue = (SMTFValue > TMTFValue) ? SMTFValue : TMTFValue;
                break;
        }
    }
    if (a_Mean != NULL) {
        *a_Mean = average;
    }
    if (a_RV != NULL) {
        *a_RV = RV;
    }

    return *a_MTFValue;
}

double MTFCounting713(unsigned char* a_Image, unsigned int a_Width, unsigned int a_Height,
                      unsigned int a_centroid_x, unsigned int a_centroid_y, MTFBlockOption a_Option,
                      unsigned char a_test_case, unsigned int a_MTF_Scan_window, double* a_MTFValue,
                      unsigned int* a_Mean, unsigned char* a_RV) {
    double SMTFValue = 0.0;
    double TMTFValue = 0.0;
    int x1, x2;
    int y1, y2;
    unsigned int pxl_count = 0;
    int i, j;

    unsigned int average = 0;
    unsigned char RV = 0;

    // prepare counting buffer
    unsigned char* DataBuffer = NULL;
    unsigned short block_x = 0;
    unsigned short block_y = 0;

    int offset_x, offset_y;

    block_x = MTF_BLOCK_SIZE_X;
    block_y = MTF_BLOCK_SIZE_Y;

    DataBuffer = (unsigned char*)plat_alloc(block_x * block_y);

    //               {0 , 1   2   3   4   5   6   7   8   9}
#if defined(WIN32)
    char x_ary[10] = {0, 0, -1, 1, -2, 2, 0, 0, 0, 0};
    char y_ary[10] = {0, 0, 0, 0, 0, 0, -1, 1, -2, 2};
#elif defined(RBS_SDK_USE)
    char x_ary[10] = {0, 0, (char)-1, 1, (char)-2, 2, 0, 0, 0, 0};
    char y_ary[10] = {0, 0, 0, 0, 0, 0, (char)-1, 1, (char)-2, 2};
#endif

    // Prepare x,y index
    x1 = (a_Width - MTF_BLOCK_SIZE_X) / 2 + (MTF_BLOCK_SIZE_X * x_ary[a_Option]);
    y1 = (a_Height - MTF_BLOCK_SIZE_Y) / 2 + (MTF_BLOCK_SIZE_Y * y_ary[a_Option]);
    x2 = x1 + block_x;
    y2 = y1 + block_y;

    // Prepare offset value for align to centroid
    offset_x = a_centroid_x - (a_Width / 2);
    offset_y = a_centroid_y - (a_Height / 2);

    // Limited the offset x and y under one block_x or block_y
    if (offset_x >= block_x) offset_x = block_x;
    if (offset_x <= -block_x) offset_x = -block_x;
    if (offset_y >= block_y) offset_y = block_y;
    if (offset_y <= -block_y) offset_y = -block_y;

    x1 += offset_x;
    x2 += offset_x;

    y1 += offset_y;
    y2 += offset_y;

    if (x1 < 0) x1 = 0;
    if (x1 > (int)a_Width) x1 = (int)a_Width;

    if (x2 < 0) x2 = 0;
    if (x2 > (int)a_Width) x2 = (int)a_Width;

    if (y1 < 0) y1 = 0;
    if (y1 > (int)a_Height) y1 = (int)a_Height;

    if (y2 < 0) y2 = 0;
    if (y2 > (int)a_Height) y2 = (int)a_Height;

    // copy target data to buffer
    for (j = y1; j < y2; j++) {
        for (i = x1; i < x2; i++) {
            DataBuffer[pxl_count++] = a_Image[j * a_Width + i];
            average += a_Image[j * a_Width + i];
        }
    }
    // pxl_count = pxl_count;

    if (average > 0) {
        average /= pxl_count;
    }

    unsigned int SMTFA = 0, SMTFB = 0, RidgeA = 0, ValleyA = 0;
    unsigned int TMTFA = 0, TMTFB = 0, RidgeB = 0, ValleyB = 0;

    switch (a_test_case) {
        case 1:
            MTFCalculatingValue(DataBuffer, block_x, block_y, 1, a_MTF_Scan_window, &SMTFA, &SMTFB,
                                &RidgeA, &ValleyA);
            break;
        case 2:
            MTFCalculatingValue(DataBuffer, block_x, block_y, 2, a_MTF_Scan_window, &TMTFA, &TMTFB,
                                &RidgeB, &ValleyB);
            break;
        case 3:
            MTFCalculatingValue(DataBuffer, block_x, block_y, 1, a_MTF_Scan_window, &SMTFA, &SMTFB,
                                &RidgeA, &ValleyA);
            MTFCalculatingValue(DataBuffer, block_x, block_y, 2, a_MTF_Scan_window, &TMTFA, &TMTFB,
                                &RidgeB, &ValleyB);
            break;
    }

    if (((SMTFA + SMTFB) == 0) || (SMTFA < SMTFB)) {
        SMTFValue = 0;
    } else {
        SMTFValue = (double)(SMTFA - SMTFB) / (SMTFA + SMTFB);
    }

    if (((TMTFA + TMTFB) == 0) || (TMTFA < TMTFB)) {
        TMTFValue = 0;
    } else {
        TMTFValue = (double)(TMTFA - TMTFB) / (TMTFA + TMTFB);
    }

    // RV counting
    RV = ucFindOtsuRV(DataBuffer, block_x, block_y);

    // plat_free data
    if (DataBuffer != NULL) {
        plat_free(DataBuffer);
    }

    if (a_MTFValue != NULL) {
        switch (a_test_case) {
            case 1:
                *a_MTFValue = SMTFValue;
                break;
            case 2:
                *a_MTFValue = TMTFValue;
                break;
            case 3:
                *a_MTFValue = (SMTFValue > TMTFValue) ? SMTFValue : TMTFValue;
                break;
        }
    }
    if (a_Mean != NULL) {
        *a_Mean = average;
    }
    if (a_RV != NULL) {
        *a_RV = RV;
    }

    return *a_MTFValue;
}
