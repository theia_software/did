
#include <stdio.h>
#include <string.h>

#include "egis_definition.h"
#include "egis_sprintf.h"
#include "ini.h"
#include "plat_file.h"
#include "plat_heap.h"
#include "plat_log.h"
#include "plat_std.h"
#include "type_definition.h"

#define LOG_TAG "ini_help"

typedef struct _IniHelper {
    int (*SetPathName)(const char* path);
    int (*GetString)(const char* section, const char* name, char* value, int size,
                     const char* default_value);
    int (*GetInt)(const char* section, const char* name, int default_value);
    double (*GetDouble)(const char* section, const char* name, double default_value);

} IniHelper;

static IniHelper g_ini_helper;
static char* g_ini_string = NULL;

static int _SetPathName(const char* path) {
    // TODO: check real file size
    const unsigned int len = 10 * 1024;
    int retval;
    if (path == NULL) {
        egislog_d("%s, bad path.", __func__);
        return EGIS_INCORRECT_PARAMETER;
    }
    egislog_d("%s %s\n", __func__, path);

    plat_free(g_ini_string);
    g_ini_string = (char*)plat_alloc(len);
    if (g_ini_string == NULL) {
        return EGIS_OUT_OF_MEMORY;
    }
#ifndef TZ_MODE
    retval = plat_load_raw_image((char*)path, (unsigned char*)g_ini_string, len, 1);
#endif
    if (retval <= 0) {
        egislog_e("!! Error. Failed to load %s", path);
        return EGIS_COMMAND_FAIL;
    }
    egislog_d("%s %s. real_size=%d", __func__, path, retval);
    return EGIS_OK;
}

#define VALUE_TYPE_STRING 0
#define VALUE_TYPE_INT 1
#define VALUE_TYPE_DOUBLE 2
typedef struct _IniHelperValue {
    const char* in_setction;
    const char* in_name;
    int in_type;
    BOOL found;

    void* out_value;
    int out_string_max;
} IniHelperValue;

static void _InitIniHelperValue(IniHelperValue* iniHelperValue, const char* section,
                                const char* name, int data_type, void* out_value,
                                int out_string_max) {
    // egislog_d("%s [%s] %s - type(%d)\n", __func__, section, name, data_type);
    iniHelperValue->in_setction = section;
    iniHelperValue->in_name = name;
    iniHelperValue->in_type = data_type;
    iniHelperValue->found = FALSE;

    iniHelperValue->out_value = out_value;
    iniHelperValue->out_string_max = out_string_max;
}

static int _ini_handler(void* user, const char* section, const char* name, const char* value) {
    IniHelperValue* pconfig = (IniHelperValue*)user;
    if (pconfig->found) {
        return 1;
    }
    // egislog_d("    %s [%s] %s entry\n", __func__, section, name);

#define MATCH(s, n) strcmp(section, s) == 0 && strcmp(name, n) == 0

    if (MATCH(pconfig->in_setction, pconfig->in_name)) {
        pconfig->found = TRUE;
        switch (pconfig->in_type) {
            case VALUE_TYPE_STRING:
#if defined(QSEE) && defined(TZ_MODE)
                plat_strncpy((char*)(pconfig->out_value), value, pconfig->out_string_max);
#else
                strncpy((char*)(pconfig->out_value), value, pconfig->out_string_max);
#endif
                break;
#ifndef TZ_MODE
            case VALUE_TYPE_INT:
                *((int*)pconfig->out_value) = atoi(value);
                break;
            case VALUE_TYPE_DOUBLE:
                *((double*)pconfig->out_value) = atof(value);
                break;
#endif
            default:
                egislog_e("!! Error. bad value type %d\n", pconfig->in_type);
                break;
        }
    }
    return 1;
}

static int _GetString(const char* section, const char* name, char* value, int size,
                      const char* default_value) {
    IniHelperValue helperValue;
    _InitIniHelperValue(&helperValue, section, name, VALUE_TYPE_STRING, (void*)value, size);

    ini_parse_string(g_ini_string, _ini_handler, &helperValue);
    if (helperValue.found) {
        egislog_d("%s [%s] %s => %s", __func__, section, name, value);
        return 1;
    } else {
        egislog_d("%s [%s] %s (default)=> %s", __func__, section, name, value);
#if defined(QSEE) && defined(TZ_MODE)
        plat_strncpy(value, default_value, size);
#else
        strncpy(value, default_value, size);
#endif
        return 0;
    }
}
static int _GetInt(const char* section, const char* name, int default_value) {
    int out_int;
    IniHelperValue helperValue;
    _InitIniHelperValue(&helperValue, section, name, VALUE_TYPE_INT, (void*)&out_int, 0);

    ini_parse_string(g_ini_string, _ini_handler, &helperValue);
    if (helperValue.found) {
        egislog_d("%s [%s] %s => %d", __func__, section, name, out_int);
        return out_int;
    } else {
        egislog_d("%s [%s] %s (default)=> %d", __func__, section, name, default_value);
        return default_value;
    }
}
static double _GetDouble(const char* section, const char* name, double default_value) {
    double out_double;
    IniHelperValue helperValue;
    _InitIniHelperValue(&helperValue, section, name, VALUE_TYPE_DOUBLE, (void*)&out_double, 0);

    ini_parse_string(g_ini_string, _ini_handler, &helperValue);
    if (helperValue.found) {
        egislog_d("%s [%s] %s => %d.%d", __func__, section, name, (int)out_double,
                  (int)(out_double * 1000) % 1000);
        return out_double;
    } else {
        egislog_d("%s [%s] %s (default)=> %d.%d", __func__, section, name, (int)default_value,
                  (int)(default_value * 1000) % 1000);
        return default_value;
    }
}

int IniHelperCreate(IniHelper** myIniHelper) {
    if (myIniHelper == NULL) {
        return -1;
    }
    g_ini_helper.SetPathName = _SetPathName;
    g_ini_helper.GetString = _GetString;
    g_ini_helper.GetInt = _GetInt;
    g_ini_helper.GetDouble = _GetDouble;

    *myIniHelper = &g_ini_helper;

    return EGIS_OK;
}

void IniHelperDestroy(IniHelper* pIniHelper) {
    if (pIniHelper == NULL) {
        return;
    }
    plat_free(g_ini_string);
}
