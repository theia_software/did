// Sensor.h: interface for the CSensor class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_SENSOR_H__0CE55C81_E772_4816_AF4C_9F9ABDB9BAC8__INCLUDED_)
#define AFX_SENSOR_H__0CE55C81_E772_4816_AF4C_9F9ABDB9BAC8__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif  // _MSC_VER > 1000

#include "egis_fp_sensor_define.h"

#define MAX_DEVICE 8
#define DEVICE_NAME_LEN 36
#define DROP_FRAMES 2

#if defined(WIN32)
#if (EGIS_SENSOR == 711)
#include "../../inc/et711_reg.h"
#elif (EGIS_SENSOR == 701)
#include "../../inc/et701_reg.h"
#elif (EGIS_SENSOR == 713)
#include "../../inc/et713_reg.h"
#endif
#elif defined(RBS_SDK_USE)
#include "et713_reg.h"
#include "type_definition.h"
#endif

#define READ_TIMEOUT 8000

#define REG_TYPE_NONE 0x00
#define REG_TYPE_DISCARD 0x00
#define REG_TYPE_READ 0x01
#define REG_TYPE_WRITE 0x02

// for Nor-Flash controlling
#define FLASH_COMMAND_LENGTH 10
#define SST_NORFLASH_CTRl 0xC0
#define SUB_OPCODE_SST_NNORFLASH_READ_ID 0x01
#define SUB_OPCODE_SST_NNORFLASH_CHIP_ERASE 0x02
#define SUB_OPCODE_SST_NNORFLASH_SECTOR_ERASE 0x03
#define SUB_OPCODE_SST_NNORFLASH_BLOCK_ERASE 0x04
#define SUB_OPCODE_SST_NNORFLASH_READ_DATA 0x05
#define SUB_OPCODE_SST_NNORFLASH_PAGE_PROGRAM 0x06
#define SUB_OPCODE_SST_NNORFLASH_WRITE_ENABLE 0x07    // Write Enable (WREN)
#define SUB_OPCODE_SST_NNORFLASH_WRITE_DSISABLE 0x08  // Write Disable (WRDI)
#define SUB_OPCODE_SST_NNORFLASH_READ_STATUS 0x09     // Read Status Register (RDSR)
#define SUB_OPCODE_SST_NNORFLASH_WRITE_STATUS 0x10    // RWrite Status Register (WRSR)

typedef struct _SENSOR_REG {
    unsigned short RegAddr;
    unsigned char RegType;
    unsigned char RegValue;
    unsigned char RegValueDefault;
    unsigned char RegValueDefaultAlt;
    unsigned char RegValueMask;
} SENSOR_REG;

typedef struct _SENSOR_ID {
    unsigned char major_id;
    unsigned char minor_id;
    unsigned char revision;
    unsigned char pixel_id;
} SENSOR_ID;

extern unsigned short reg_addr[REG_NUM];
extern char reg_name[REG_NUM][50];

enum {
    TESTER_BOARD_ARDUINO = 0,
};

enum {
    TEST_MODE_MANUAL = 0,
    TEST_MODE_AUTO_WINSOCK = 1,
    TEST_MODE_AUTO_IPC = 2,
    TEST_MODE_AUTO_GPIO = 3,
};

#define RESET_ARDUINO_PIN 4
#define LDO_ARDUINO_PIN 5
#define LDO30_ARDUINO_PIN 40
#define INT_ARDUINO_PIN 7
#define CE_ARDUINO_PIN 40
#define ARDUINO_BOARD_LED 43
#define LED_POWER_SOURCE_PIN 28

// For Auto Test Handler
#define GPIO_EMERGENCE_PIN 23
#define GPIO_READ_MANUAL_AUTO_PIN 26
#define GPIO_START_PIN 25
#define GPIO_SITE1_LEDR_PIN 34
#define GPIO_SITE1_LEDG_PIN 35
#define GPIO_SITE2_LEDR_PIN 36
#define GPIO_SITE2_LEDG_PIN 37
#define GPIO_SITE3_LEDR_PIN 30
#define GPIO_SITE3_LEDG_PIN 31
#define GPIO_SITE4_LEDR_PIN 32
#define GPIO_SITE4_LEDG_PIN 33

#define FRAME_DROP 4

// Win32 CSensor Class ///////////////////////////////////////////////////////////////////////
#if defined(WIN32)

#ifdef EgisSensorsDll_EXPORTS
#define EgisSensorsDll_API __declspec(dllexport)
#else
#define EgisSensorsDll_API  //__declspec(dllimport)
#endif

class EgisSensorsDll_API CSensor {
   public:
    CSensor();
    virtual ~CSensor();

    // parameters /////////////////////////////////////////////////////
    int target_board_type;
    int ndevice;
    int PortIndex;

    unsigned short firmware_version;

    // software image size
    int sensor_width_show;
    int sensor_height_show;
    int sensor_total_pixels_show;

    // hardware image size
    int sensor_width;
    int sensor_height;
    int sensor_total_pixels;

    // raw data size
    int sensor_width_raw;
    int sensor_height_raw;
    int sensor_total_pixels_raw;

    unsigned char* raw_image;
    unsigned short* fullframe16;

    // current meter
    int meter_select;
    unsigned char meter_id[2];

    // Arduino board
    SENSOR_REG sensor_reg_all[MAX_REG_ADDR + 1];
    //////////////////////////////////////////////////////////////////////

    // Functions /////////////////////////////////////////////////////////
    int iSearchDevices(int tester_board_type);

    BOOL bOpenSensor(int device_index);
    HANDLE hGetDeviceHandle();
    BOOL bReOpenSensor(HANDLE device_handle);
    BOOL bCloseSensor();

    void vRegInit();

    BOOL bRegisterReadWriteTest(BOOL bDoReadWriteTest);
    BOOL bDumpRegister(char* pReason, BOOL bShowAll);
    BOOL bDumpRegister(unsigned short addr);

    ////////////////////////////////////////////////////////////////////
    // addr 2 byte, value 1 byte
    BOOL bReadRegister(unsigned short addr, unsigned char* pValue);

    BOOL bWriteRegister(unsigned short addr, unsigned char value);

    BOOL bReadRegisterUs(unsigned short addr, unsigned short* pValue);

    BOOL bWriteRegisterUs(unsigned short addr, unsigned short us_value);

    // LED Control
    // addr 1 byte, value 1 byte
    BOOL bWriteRegisterSingleByte(unsigned char addr, unsigned char value, unsigned char I2CAddr);
    // Photo Diode
    BOOL bReadI2CDataWord(unsigned short* pValue, unsigned char I2CAddr);

    // DAC
    BOOL bWriteDAC(unsigned short value, unsigned char I2CAddr);

    // Current Meter
    // INA230 ///////////////////////////////////////////////////////////
    BOOL bSetI2CClock(unsigned int i2c_clock);
    BOOL bSetMeterI2CID(unsigned char ucMeter, unsigned char ucI2CID);
    BOOL bGetMeterI2CID(unsigned char ucMeter, unsigned char* pucI2CID);
    BOOL bSetSelectedMeter(unsigned char ucMeter);
    BOOL bGetSelectedMeter(unsigned char* pucMeter);
    BOOL bReadMeterRegister(unsigned char reg, unsigned short* pValue);
    BOOL bWriteMeterRegister(unsigned char reg, unsigned short value);
    /////////////////////////////////////////////////////////////////////

    // Photo Diode
    // BH1750 ///////////////////////////////////////////////////////////
    BOOL bReadPhotoDiode(unsigned short* pLumi);
    BOOL bReadPhotoDiodeAverage(unsigned short* pLumi, int count);
    /////////////////////////////////////////////////////////////////////

    // ET711 ET701 Flash
    BOOL bOTPReadData(unsigned char* pData);
    BOOL bOTPWriteData(unsigned char* pData);

    // ET711 ET701 Flash
    BOOL bFlashGetID(unsigned char* pFlashID);
    BOOL bFlashReadStatus(unsigned char* value);
    BOOL bFlashReadData(int address, int len_k, unsigned char* pData);  // unit of 1KB
    BOOL bFlashWriteEnable();
    BOOL bFlashWriteData_256(int address, unsigned char* pData);  // unit of 256Bytes
    BOOL bFlashEraseAll();
    BOOL bFlashEraseSector(int address);

    BOOL bFlashWaitWriteEnableLatch();
    BOOL bFlashWaitWriteDone();

    BOOL bGetSensorID(SENSOR_ID* pSensorID);

    BOOL bPowerOn(BOOL bOn);
    BOOL bHardwareReset(int reset_hold);
    BOOL bSoftwareReset(int reset_hold);

    BOOL bSetExposureTime(double* pExposureTime);

    BOOL bSetPGASetting(unsigned char data);
    BOOL bGetPGASetting(unsigned char* pdata);

    BOOL bInitSensor(char* pSettingFile, BOOL bLoadSettingFile);

    static float fPGAControltoPGAGain(unsigned char reg_value);

    BOOL bLEDBackLightOn(BOOL bOn, unsigned char led_gain, unsigned short* pLightness,
                         int led_control_board_type, int led_ext_comport = 0);

    // 16bits mode /////////////////////////////////////////////////////
    BOOL bGetFrame16(unsigned short* pFrame, int drops);
    BOOL bGetAverageFrameAndStdDev16(unsigned short* pData, double* pStdDev, int nFrame,
                                     int* pBackground);
    BOOL bGetIntegratedFrameAndStdDev16(unsigned short* pData, double* pStdDev, int nFrame,
                                        int* pBackground);
    ////////////////////////////////////////////////////////////////////

    BOOL bGetFrameRaw(unsigned char* pRaw, int frame_size);

    // Arduino Board Only ///////////////////////////////////////////////
    BOOL bSetGPIOMode(unsigned char Pin, unsigned char Mode);
    BOOL bGetGPIOData(unsigned char Pin, unsigned char* pData);
    BOOL bSetGPIOData(unsigned char Pin, unsigned char Data);
    BOOL bGetInterruptPin(unsigned char* pStatus);

    BOOL bET711_GetImage(int imagesize, unsigned char* pData);

    void vGetComports(int ndevice, int* pcomports);
    BOOL bGetUID(unsigned char* pUID);

    BOOL bInitAllGPIO();

    ////////////////////////////////////////////////////////////////////

    // INA230 //////////////////////////////////////////////////////////
    BOOL bReadMeters(double* pCurrent33, double* pCurrent18, int* pVoltage33, int* pVoltage18);
    ////////////////////////////////////////////////////////////////////
   private:
    SENSOR_ID sensor_id;

    BOOL bLoadFile(char* pfile);

    // Arduino board
    BOOL bOpenArduino(int com);
    int com_ports[MAX_DEVICE];
    HANDLE m_hCom;
    BOOL bIsDeviceReopen;
    BOOL bWritComPort(unsigned char* pWriteBuf, int iWriteBufLen);
    BOOL bReadComPort(unsigned char* pReadBuf, int iReadBufLen, int timeout);
    BOOL bGetGPIO(unsigned char* pgpio);
    BOOL bSetGPIO(unsigned char gpio);
    BOOL bInitSPI(int mode, int spi_clk);
};

// Linux CSensor structure
#elif defined(RBS_SDK_USE)

#define MAX_REG_ADDR 0xFC
#define REG_NUM 101

typedef struct CSensor_t {
    int target_board_type;
    int ndevice;
    int PortIndex;
    unsigned short firmware_version;
    int sensor_width_show;
    int sensor_height_show;
    int sensor_total_pixels_show;
    int sensor_width;
    int sensor_height;
    int sensor_total_pixels;
    int sensor_width_raw;
    int sensor_height_raw;
    int sensor_total_pixels_raw;
    int m_hCom;
    int meter_select;
    int com_ports[MAX_DEVICE];
    unsigned char* raw_image;
    unsigned short* fullframe16;
    unsigned char meter_id[2];
    SENSOR_REG sensor_reg_all[MAX_REG_ADDR + 1];
    SENSOR_ID sensor_id;
    BOOL bIsDeviceReopen;

    BOOL (*bRegisterReadWriteTest)(BOOL bDoReadWriteTest);
    BOOL (*bDumpRegister)(char* pReason, BOOL bShowAll);
    BOOL (*bReadRegister)(unsigned short addr, unsigned char* pValue);
    BOOL (*bWriteRegister)(unsigned short addr, unsigned char value);
    BOOL (*bReadRegisterUs)(unsigned short addr, unsigned short* pValue);
    BOOL (*bWriteRegisterUs)(unsigned short addr, unsigned short us_value);
    BOOL (*bGetSensorID)(SENSOR_ID* pSensorID);
    BOOL (*bOpenSensor)(int device_index);
    BOOL (*bHardwareReset)(int reset_hold);
    BOOL (*bSetExposureTime)(double* pExposureTime);
    BOOL (*bSetPGASetting)(unsigned char data);
    BOOL (*bInitSensor)(char* pSettingFile, BOOL bLoadSettingFile);
    BOOL (*bGetFrame16)(unsigned short* pFrame, int drops);
    BOOL(*bGetAverageFrameAndStdDev16)
    (unsigned short* pData, double* pStdDev, int nFrame, int* pBackground);
    BOOL(*bGetIntegratedFrameAndStdDev16)
    (unsigned short* pData, double* pStdDev, int nFrame, int* pBackground);
    BOOL (*bReadMeters)(double* pCurrent33, double* pCurrent18, int* pVoltage33, int* pVoltage18);
    BOOL (*bReadEfuseData)(unsigned char* buf);
} CSensor;

#ifdef __cplusplus
extern "C" {
#endif

BOOL InitCSensor(CSensor* sensor);
BOOL bWriteRegisterSingleByte(unsigned char addr, unsigned char value, unsigned char I2CAddr);
BOOL bOpenSensor(int device_index);
BOOL bReOpenSensor(HANDLE device_handle);
BOOL bCloseSensor();
BOOL bSetI2CClock(unsigned int i2c_clock);
BOOL bSetMeterI2CID(unsigned char ucMeter, unsigned char ucI2CID);
BOOL bGetMeterI2CID(unsigned char ucMeter, unsigned char* pucI2CID);
BOOL bSetSelectedMeter(unsigned char ucMeter);
BOOL bGetSelectedMeter(unsigned char* pucMeter);
BOOL bReadMeterRegister(unsigned char reg, unsigned short* pValue);
BOOL bWriteMeterRegister(unsigned char reg, unsigned short value);
BOOL bReadI2CDataWord(unsigned short* pValue, unsigned char I2CAddr);
BOOL bWriteDAC(unsigned short value, unsigned char I2CAddr);
BOOL bReadPhotoDiode(unsigned short* pLumi);
BOOL bReadPhotoDiodeAverage(unsigned short* pLumi, int count);
BOOL bOTPReadData(unsigned char* pData);
BOOL bOTPWriteData(unsigned char* pData);
BOOL bFlashGetID(unsigned char* pFlashID);
BOOL bFlashReadStatus(unsigned char* value);
BOOL bFlashReadData(int address, int len_k, unsigned char* pData);
BOOL bFlashWriteEnable();
BOOL bFlashWriteData_256(int address, unsigned char* pData);
BOOL bFlashEraseAll();
BOOL bFlashEraseSector(int address);
BOOL bFlashWaitWriteEnableLatch();
BOOL bFlashWaitWriteDone();
BOOL bGetFrameRaw(unsigned char* pRaw, int frame_size);
BOOL bSetGPIOMode(unsigned char Pin, unsigned char Mode);
BOOL bGetGPIOData(unsigned char Pin, unsigned char* pData);
BOOL bSetGPIOData(unsigned char Pin, unsigned char Data);
BOOL bGetInterruptPin(unsigned char* pStatus);
BOOL bET711_GetImage(int imagesize, unsigned char* pData);
BOOL bGetUID(unsigned char* pUID);
BOOL bInitAllGPIO();
BOOL bPowerOn(BOOL bOn);
BOOL bSoftwareReset(int reset_hold);
BOOL bGetPGASetting(unsigned char* pdata);
BOOL bLEDBackLightOn(BOOL bOn, unsigned char led_gain, unsigned short* pLightness,
                     int led_control_board_type, int led_ext_comport);
BOOL bWritComPort(unsigned char* pWriteBuf, int iWriteBufLen);
BOOL bReadComPort(unsigned char* pReadBuf, int iReadBufLen, int timeout);
BOOL bLoadFile(char* pfile);
BOOL bOpenArduino(int com);
BOOL bGetGPIO(unsigned char* pgpio);
BOOL bSetGPIO(unsigned char gpio);
BOOL bInitSPI(int mode, int spi_clk);
int iSearchDevices(int tester_board_type);
HANDLE hGetDeviceHandle();
float fPGAControltoPGAGain(unsigned char reg_value);
void vRegInit();
void vGetComports(int ndevice, int* pcomports);

#ifdef __cplusplus
}
#endif

#endif

#endif
