#include "egisfp_et7xx_normal_scan.h"
#include "7xx_sensor_test_definition.h"
#include "ImageProcessingLib.h"
#include "Sensor.h"
#include "egis_definition.h"
#include "egis_eeprom_definition.h"
#include "egis_fp_sensor_test.h"
#include "egis_sprintf.h"
#include "egisfp_test_et7xx.h"
#include "fp_definition.h"
#include "plat_log.h"
#include "plat_mem.h"

#define LOG_TAG "RBS-7XX_inline_normal_scan"
#define CONFIG_FILE "/sdcard/temp2/default_INLINE.ini"

static normal_scan_state_t g_ns_state = NS_STATE_RESET;
static CSensor* g_sensor = NULL;
static EGFPS_TEST_INST* g_pTestInst = NULL;

void otp_check_csum(BYTE const* const buf, int len, BYTE* result) {
    int i = 0;
    if (!buf || len <= 0 || !result) return;
    for (i = 0; i < len; i++) *result += buf[i];
}

int otp_csum_cmp(BYTE const* const buf, int len, BYTE const csum2) {
    BYTE csum1 = 0x0;

    if (!buf) return EGIS_NULL_POINTER;
    if (len <= 0) return EGIS_TEST_FAIL;
    otp_check_csum(buf, len, &csum1);
    egislog(LOG_DEBUG, "%x, %x", csum1, csum2);
    if (csum1 != csum2)
        return EGIS_TEST_FAIL;
    else
        return EGIS_OK;
}

BOOL check_otp_checksum(struct egfps_otp_data* otp_data) {
    int ret;
    // unsigned char csum = 0x0;

    ret = otp_csum_cmp((BYTE*)otp_data,
                       sizeof(struct egfps_otp_data) - OTP_CHECKSUM_SIZE -
                           sizeof(otp_data->reserved) - sizeof(otp_data->uid),
                       otp_data->check_sum);
    /*
    csum+= otp_data->mt_result;
    csum+= otp_data->software_bin;
    csum+= otp_data->production_plant;
    csum+= otp_data->program_version;
    csum+= otp_data->config_version;
    csum+= otp_data->test_data[0];
    csum+= otp_data->test_data[1];
    csum+= otp_data->test_data[2];
    csum+= otp_data->test_data[3];
    csum+= otp_data->exposure_time_h;
    csum+= otp_data->exposure_time_l;
    csum+= otp_data->centroid_x1;
    csum+= otp_data->centroid_y1;
    csum+= otp_data->centroid_x2;
    csum+= otp_data->centroid_y2;
    csum+= otp_data->centroid_x3;
    csum+= otp_data->centroid_y3;
    csum+= otp_data->centroid_x4;
    csum+= otp_data->centroid_y4;

    egislog(LOG_ERROR,"csuma=%x,checksuma=%d,ret=%d",csum,otp_data->check_sum,ret);
    */
    return (ret == EGIS_OK) ? TRUE : FALSE;
}

int egfps_otp_test(EGFPS_TEST_INST* hTestInst, CSensor* pEgisSensor, BOOL bDoReadWriteTest) {
    struct egfps_otp_data* otp_data;
    EGFPS_TEST_INST* ptest_inst = hTestInst;
    BOOL r_value;

    ptest_inst->egfps_test_result.otp_test = 0x1;
    r_value = pEgisSensor->bReadEfuseData(ptest_inst->egfps_test_result.mt_result_buf);
    otp_data = (struct egfps_otp_data*)ptest_inst->egfps_test_result.mt_result_buf;

    if (!r_value) return SW_BIN_REG_FAIL;

    if (!check_otp_checksum(otp_data)) /*return SW_BIN_NVM_CHECK_FAIL;*/
        ;

    ptest_inst->egfps_test_result.otp_test = 0x0;

    memcpy(ptest_inst->egfps_test_result.uid, otp_data->uid, 9);

    egislog_d("UID %d %d %d %d %d %d %d %d %d", ptest_inst->egfps_test_result.uid[0],
              ptest_inst->egfps_test_result.uid[1], ptest_inst->egfps_test_result.uid[2],
              ptest_inst->egfps_test_result.uid[3], ptest_inst->egfps_test_result.uid[4],
              ptest_inst->egfps_test_result.uid[5], ptest_inst->egfps_test_result.uid[6],
              ptest_inst->egfps_test_result.uid[7], ptest_inst->egfps_test_result.uid[8]);

    return SW_BIN_PASS;
}

int et713_flash_rw_test(EGFPS_TEST_INST* hTestInst, CSensor* pEgisSensor) {
#if 0
#define FLASH_DTAT_LEN_V1 0xFA1
#define FLASH_DTAT_LEN_V2 0xFA1
	EGFPS_TEST_INST *ptest_inst = hTestInst;
	BOOL r_value;
	struct eeprom_v1 *p_eeprom_v1;
	struct eeprom_v2 *p_eeprom_v2;
	unsigned char flash_data_v1[FLASH_DTAT_LEN_V1];
	unsigned char flash_data_v2[FLASH_DTAT_LEN_V2];

	ptest_inst->egfps_test_result.flash_test = 0x01;

	r_value = pEgisSensor->bReadFlash(0x00, flash_data_v2, FLASH_DTAT_LEN_V2);
	if (r_value) {
		egislog_i("Fail in bReadFlash");
		ptest_inst->egfps_test_result.flash_test = 0x01;
		return SW_BIN_NVM_CHECK_FAIL;
	}

	memcpy(flash_data_v1, flash_data_v2, FLASH_DTAT_LEN_V1);
	p_eeprom_v1 = (struct eeprom_v1 *)flash_data_v1;
	p_eeprom_v2 = (struct eeprom_v2 *)flash_data_v2;

	if (check_v1_all_checksum(p_eeprom_v1) && check_v2_all_checksum(p_eeprom_v2))
		return SW_BIN_NVM_CHECK_FAIL;

	ptest_inst->egfps_test_result.flash_test = 0x0;
#endif
    egislog_i("SKIP et713_flash_rw_test, Todo");
    return SW_BIN_PASS;
}

static int egfps_normal_scan_test_init(void) {
    egislog_d("egfps_normal_scan_test_init in");
    int ret = -1;
    g_sensor = (CSensor*)plat_alloc(sizeof(CSensor));
    ret = InitCSensor(g_sensor);
    egislog_d("InitCSensor ret = %d", ret);

    g_pTestInst = egfps_new_test_instance(g_sensor, CONFIG_FILE);
    ret = egfps_init_sensor(g_pTestInst, g_sensor);
    egislog_d("egfps_init_sensor ret = %d", ret);

    if (ret != SW_BIN_PASS) {
        egislog_e("egfps_init_sensor fail");
        return EGIS_7XX_TEST_SENSOR_TEST_FAIL;
    }
    return FP_LIB_OK;
}

void egisfp_test_et7xx_normal_scan_uninit(void) {
    egislog_d("egisfp_test_et7xx_normal_scan_uninit in");
    if (g_sensor == NULL) {
        egislog_e("egisfp_test_et7xx_normal_scan_uninit: g_sensor is NULL!");
        return;
    }
    egfps_free_test_instance(g_pTestInst, g_sensor);
    plat_free(g_sensor->raw_image);    //  allocated at bInitSensor()
    plat_free(g_sensor->fullframe16);  //  allocated at bInitSensor()
    plat_free(g_sensor);
    g_sensor = NULL;
}

#ifdef QSEE
extern int io_et713_lense_type_uuid(unsigned char* uuid);
#endif

unsigned int et713_do_normal_scan(void* test) {
    int ret = -1;
    char name[INLINE_IMAGE_NAME_LENGH] = {0};
    struct sensor_test_output* out_data = ((struct egisfp_test_session*)test)->out_data;
    struct sensor_test_input* in_data = ((struct egisfp_test_session*)test)->in_data;
    int buffer_out_length = IMG_MAX_BUFFER_SIZE * 2;

    egislog_d("et713_set_normal_scan state: %d %d", g_ns_state, in_data->script_id);

    switch (g_ns_state) {
        // case NS_STATE_READ_FLASE_START:
        // 	ret = et713_flash_rw_test(g_pTestInst, g_sensor);
        // 	egislog_d("et713_flash_rw_test ret = %d", ret);

        // 	if (ret != SW_BIN_PASS) {
        // 		egislog_e("et713_flash_rw_test fail");
        // 		ret = EGIS_7XX_TEST_SENSOR_TEST_FAIL;
        // 		out_data->result = EGIS_7XX_TEST_SENSOR_TEST_FAIL;
        // 		break;
        // 	}

        // 	out_data->result = EGIS_7XX_TEST_READ_SENSOR_INFO_END;
        // 	ret = FP_LIB_OK;
        // 	g_ns_state = NS_STATE_RESET;

        // break;
        case NS_STATE_RESET:
            out_data->result = EGIS_7XX_TEST_RESET;
            ret = FP_LIB_OK;
            g_ns_state = NS_STATE_READ_FLASE_END;

            break;
        case NS_STATE_READ_FLASE_END:

            egislog_d("egfps_register_test start");
            ret = egfps_register_test(g_pTestInst, g_sensor, FALSE);
            egislog_d("egfps_register_test ret = %d", ret);

            if (ret != SW_BIN_PASS) {
                egislog_e("egfps_register_test fail");
                ret = EGIS_7XX_TEST_REGISTER_TEST_FAIL;
                out_data->result = EGIS_7XX_TEST_REGISTER_TEST_FAIL;
                break;
            }

            egislog_d("egfps_otp_test start");
#ifdef QSEE
            unsigned char et713_uuid[10] = {0};
            ret = io_et713_lense_type_uuid(et713_uuid);
            ret = SW_BIN_PASS;
#else
            ret = egfps_otp_test(g_pTestInst, g_sensor, FALSE);
#endif
            egislog_d("egfps_otp_test ret = %d", ret);

            if (ret != SW_BIN_PASS) {
                egislog_e("egfps_otp_test fail");
                ret = EGIS_7XX_TEST_OTP_TEST_FAIL;
                out_data->result = EGIS_7XX_TEST_OTP_TEST_FAIL;
                break;
            }

            egislog_d("egfps_testpattern_test start");
            ret = egfps_testpattern_test(g_pTestInst, g_sensor, g_pTestInst->picture_raw);
            egislog_d("egfps_testpattern_test ret = %d", ret);

            if (ret != SW_BIN_PASS) {
                egislog_e("egfps_testpattern_test fail");
                ret = EGIS_7XX_TEST_TESTPATTERN_TEST_FAIL;
                out_data->result = EGIS_7XX_TEST_TESTPATTERN_TEST_FAIL;
                break;
            }

            g_ns_state = NS_STATE_RESET;
            out_data->result = EGIS_OK;
            ret = FP_LIB_OK;
            break;

        case NS_STATE_GET_IMAGE:
            egislog_d("[NS_STATE_GET_IMAGE]");
            memcpy(out_data->picture_buffer_16, (unsigned char*)g_pTestInst->picture_raw16,
                   buffer_out_length);
            egist_snprintf(name, INLINE_IMAGE_NAME_LENGH, "picture_testpattern_ct16.bin");
            memcpy(out_data->name, name, INLINE_IMAGE_NAME_LENGH);
            egislog_d("[%d] [%s]end", out_data->picture_remaining_count, out_data->name);
            g_ns_state = NS_STATE_RESET;
            out_data->result = EGIS_OK;
            ret = FP_LIB_OK;
            break;

        default:
            egislog_d("et713_set_normal_scan unknow state");
            break;
    }

    return ret;
}

void egisfp_test_et7xx_normal_scan_init(struct egisfp_test_session* egisfp_test) {
    int ret;
    egisfp_test->do_normal_scan = et713_do_normal_scan;
    ret = egfps_normal_scan_test_init();
    if (ret != FP_LIB_OK) {
        egislog_e("egfps_normal_scan_test_init fail");
    }
}

void egisfp_test_et7xx_normal_scan_set_state(normal_scan_state_t state) {
    g_ns_state = state;
}
