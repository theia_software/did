#if defined(WIN32)

#include <windows.h>

#include "../../ini.h"
#include "../inc/egis_fp_config.h"
#include "../inc/egis_fp_sensor_test.h"
#include "../inc/func.h"
#include "../lib/crypto/SHA1.h"
#include "../lib/crypto/rijndael-api.h"
#define snprintf _snprintf

#define ENCRYPT_KEY "hakunamatata"

#elif defined(RBS_SDK_USE)

// #include "util/inih/ini.h"
#include <string.h>

#include "egis_definition.h"
#include "egis_fp_config.h"
#include "egis_fp_sensor_test.h"
#include "egis_sprintf.h"
#include "ini_helper.h"
#define snprintf egist_snprintf

#endif

BOOL bLoadTestConfig(EGFPS_TEST_CONFIG* pEgfpsTestConfig, char* pConfigFile) {
#if defined(RBS_SDK_USE)
    int retval;
    IniHelper ini;
    IniHelper* myIni = NULL;
#endif

    // [MISC]
    pEgfpsTestConfig->ConfigVersion = 0x00;
    pEgfpsTestConfig->bin2_is_pass_bin = 0x00;

    // [SENSOR_SETTING]
    memset(pEgfpsTestConfig->SensorSettingFile, 0x00, sizeof(pEgfpsTestConfig->SensorSettingFile));
    pEgfpsTestConfig->ResetHold = 50;

    // [TEST_PATTERN]
    pEgfpsTestConfig->check_test_pattern = 1;

    // [DARK_LEVEL]
    pEgfpsTestConfig->do_dark_img_test = 0;
    pEgfpsTestConfig->dark_expo_time = 16.670000;
    pEgfpsTestConfig->dark_level_th = 10;
    pEgfpsTestConfig->white_pixel_th = 64;
    pEgfpsTestConfig->max_white_pixel = 300;

    // [MAX_EXPO_TEST]
    pEgfpsTestConfig->max_expo_gain = 0x00;
    pEgfpsTestConfig->do_max_expo_test = 0;
    pEgfpsTestConfig->max_expo_time = 100.0;
    pEgfpsTestConfig->dark_pixel_th = 64;
    pEgfpsTestConfig->dark_pixel_th2 = 64;
    pEgfpsTestConfig->max_dark_pixel = 300;

    // [IMG_MODE]
    pEgfpsTestConfig->do_img_calibration_test = 1;
    pEgfpsTestConfig->img_calibration_method = 0;
    pEgfpsTestConfig->img_gain = 0x00;
    pEgfpsTestConfig->img_expo_time = 16.67;
    pEgfpsTestConfig->img_target_mean = 512;
    pEgfpsTestConfig->DynamicRangeTestOption = 2;
    pEgfpsTestConfig->ImageAverageCount = 10;
    pEgfpsTestConfig->ImageIntegrationCount = 16;
    pEgfpsTestConfig->ImageIntegrationRV16Min = 200;
    pEgfpsTestConfig->ImageAverageRV16Min = 200;
    pEgfpsTestConfig->img_background_cancellation = 1;
    pEgfpsTestConfig->check_background_avg = 0;
    pEgfpsTestConfig->bkg_avg_min = 0;
    pEgfpsTestConfig->bkg_avg_max = 65535;
    pEgfpsTestConfig->img_max_noise = 10.0;

    // [DIRTY_DOTS]
    pEgfpsTestConfig->do_bad_pxl_test = 0;
    pEgfpsTestConfig->bad_pxl_percentage = 20.0;  // 25%(MT2 20%)
    pEgfpsTestConfig->max_bad_pxl = 300;
    pEgfpsTestConfig->bad_rowcol_percentage = 8.0;  // 3%(MT2 4%)
    pEgfpsTestConfig->max_bad_rowcol = 0;
    pEgfpsTestConfig->do_consecutive_fail_test = 0;
    pEgfpsTestConfig->consecutive_fail_pxl = 6;

    //[BLOCK_TEST]
    pEgfpsTestConfig->do_bad_block_test = 0;
    pEgfpsTestConfig->bad_block_pixel_percentage = 10.0;
    pEgfpsTestConfig->block_size_width = 16;
    pEgfpsTestConfig->block_size_height = 16;
    pEgfpsTestConfig->block_scan_step = 16;
    pEgfpsTestConfig->max_bad_block = 4;
    pEgfpsTestConfig->max_bad_pixel_block = 4;
    pEgfpsTestConfig->max_bad_pixel_block_all = 300;
    pEgfpsTestConfig->do_consecutive_fail_test = 0;
    pEgfpsTestConfig->consecutive_fail_pxl = 3;
    pEgfpsTestConfig->consecutive_fail_pxl_block = 0;

    // [DIRTY_DOTS2]
    pEgfpsTestConfig->bad_pxl_percentage2 = 20.0;  // 25%(MT2 20%)
    pEgfpsTestConfig->max_bad_pxl2 = 300;
    pEgfpsTestConfig->bad_rowcol_percentage2 = 8.0;  // 3%(MT2 4%)
    pEgfpsTestConfig->max_bad_rowcol2 = 0;
    pEgfpsTestConfig->do_consecutive_fail_test2 = 0;
    pEgfpsTestConfig->consecutive_fail_pxl2 = 6;

    // [POWER]
    pEgfpsTestConfig->PowerConsumptionTest = 0;
    pEgfpsTestConfig->MaxPowerConsumption33 = 10.0;
    pEgfpsTestConfig->MinPowerConsumption33 = 5.0;

    pEgfpsTestConfig->MaxPowerConsumption18 = 20.0;

    pEgfpsTestConfig->PowerConsumptionTestPowerDown = 0;
    pEgfpsTestConfig->MaxPowerConsumption33Pd = 1.0;
    pEgfpsTestConfig->MinPowerConsumption33Pd = 0.0;
    pEgfpsTestConfig->MaxPowerConsumption18Pd = 1.0;

    pEgfpsTestConfig->PowerConsumptionTest18 = 0;

#if (IS_EGIS_SENSOR_LENS_TYPE)
    // LENS_TEST
    pEgfpsTestConfig->do_lens_centeroid_test = 0;
    pEgfpsTestConfig->centeroid_expo_time = 16.67;

#if (EGIS_SENSOR == 711)
    pEgfpsTestConfig->lens_design_center_x = 224 / 2;
    pEgfpsTestConfig->lens_design_center_y = 146 / 2;
    pEgfpsTestConfig->lens_roi_x = 140;
    pEgfpsTestConfig->lens_roi_y = 120;
#elif (EGIS_SENSOR == 713)
    pEgfpsTestConfig->lens_design_center_x = 200 / 2;
    pEgfpsTestConfig->lens_design_center_y = 200 / 2;
    pEgfpsTestConfig->lens_roi_x = 180;
    pEgfpsTestConfig->lens_roi_y = 180;
#else
    pEgfpsTestConfig->lens_design_center_x = 161;
    pEgfpsTestConfig->lens_design_center_y = 161;
    pEgfpsTestConfig->lens_roi_x = 140;
    pEgfpsTestConfig->lens_roi_y = 140;
#endif

    pEgfpsTestConfig->lens_center_tolerance_x = 40;
    pEgfpsTestConfig->lens_center_tolerance_y = 40;
    pEgfpsTestConfig->lens_center_tolerance_line = 40;

    pEgfpsTestConfig->do_lens_mtf_test = 0;
#if (EGIS_SENSOR == 711)
    pEgfpsTestConfig->lens_mtf_scan_window = 5;
#elif (EGIS_SENSOR == 713)
    pEgfpsTestConfig->lens_mtf_scan_window = 4;
#else
    pEgfpsTestConfig->lens_mtf_scan_window = 5;
#endif
    pEgfpsTestConfig->lens_mtf_test_case = 1;
    pEgfpsTestConfig->lens_mtf_center_floor = 0.85;
    pEgfpsTestConfig->lens_mtf_center_floor2 = 0.75;
    pEgfpsTestConfig->lens_mtf_corner_floor = 0.75;
    pEgfpsTestConfig->lens_mtf_corner_floor2 = 0.65;
#endif

    // MP_INFO
    pEgfpsTestConfig->ProductionPlant = 0x00;
    pEgfpsTestConfig->ProjectStep = 0x00;

    // NVM
    pEgfpsTestConfig->package_has_flash = 0;
    pEgfpsTestConfig->check_flash_data_valid = 0;
    pEgfpsTestConfig->write_flash_data = 0;
    pEgfpsTestConfig->check_otp_data_valid = 0;
    pEgfpsTestConfig->write_otp_data = 0;

#if defined(WIN32)
    CIni ini;
#elif defined(RBS_SDK_USE)
    retval = IniHelperCreate(&myIni);

    if (retval != EGIS_OK) {
        return FALSE;
    }

    memcpy(&ini, myIni, sizeof(IniHelper));
#endif

    char ini_file[MAX_PATH];

#if defined(WIN32)
    if ((strcmp(pConfigFile + strlen(pConfigFile) - 4, ".cfg") == 0) ||
        (strcmp(pConfigFile + strlen(pConfigFile) - 4, ".CFG") == 0)) {
        char temp[MAX_PATH];
        GetTempPath(MAX_PATH, temp);

        sprintf(ini_file, "%s\\%s.ini", temp, pPath2FileName(pConfigFile));

        unsigned char cfgkey_str[16];
        memset(cfgkey_str, 0x00, 16);
        memcpy(cfgkey_str, ENCRYPT_KEY, strlen(ENCRYPT_KEY));

        rijndael_file_decrypt(MODE_ECB, NULL, cfgkey_str, 128, pConfigFile, ini_file);
    } else {
        strcpy(ini_file, pConfigFile);
    }
#endif

    if (ini.SetPathName(ini_file) != 0) {
        return TRUE;
    }

    // config version
    pEgfpsTestConfig->ConfigVersion =
        ini.GetInt("MISC", "CONFIG_VERSION", pEgfpsTestConfig->ConfigVersion);
    pEgfpsTestConfig->bin2_is_pass_bin =
        ini.GetInt("MISC", "BIN2_IS_PASS_BIN", pEgfpsTestConfig->bin2_is_pass_bin);

    // Sensor Setting
    pEgfpsTestConfig->do_load_setting_file = ini.GetInt("SENSOR_SETTING", "LOAD_SETTING_FILE", 1);
    ini.GetString("SENSOR_SETTING", "SETTING_FILE", pEgfpsTestConfig->SensorSettingFile, MAX_PATH,
                  "");
    if ((strlen(pEgfpsTestConfig->SensorSettingFile) == 0) &&
        (pEgfpsTestConfig->do_load_setting_file)) {
        return FALSE;
    }
    pEgfpsTestConfig->ResetHold =
        ini.GetInt("SENSOR_SETTING", "RESET_HOLD", pEgfpsTestConfig->ResetHold);

    // test pattern
    pEgfpsTestConfig->check_test_pattern =
        ini.GetInt("TEST_PATTERN", "CHECK_TEST_PATTERN", pEgfpsTestConfig->check_test_pattern);

    // Dark Level Test
    pEgfpsTestConfig->do_dark_img_test =
        ini.GetInt("DARK_LEVEL", "DO_DARK_IMG_TEST", pEgfpsTestConfig->do_dark_img_test);
    pEgfpsTestConfig->dark_expo_time =
        ini.GetDouble("DARK_LEVEL", "DARK_EXPO_TIME", pEgfpsTestConfig->dark_expo_time);
    pEgfpsTestConfig->dark_level_th =
        ini.GetInt("DARK_LEVEL", "DARK_LEVEL_TH", pEgfpsTestConfig->dark_level_th);
    pEgfpsTestConfig->white_pixel_th =
        ini.GetInt("DARK_LEVEL", "WHITE_PIXEL_TH", pEgfpsTestConfig->white_pixel_th);
    pEgfpsTestConfig->max_white_pixel =
        ini.GetInt("DARK_LEVEL", "MAX_WHITE_PIXEL", pEgfpsTestConfig->max_white_pixel);

    // Max Exposure Time
    pEgfpsTestConfig->max_expo_gain =
        ini.GetInt("MAX_EXPO_TEST", "MAX_EXPO_GAIN", pEgfpsTestConfig->max_expo_gain);
    pEgfpsTestConfig->do_max_expo_test =
        ini.GetInt("MAX_EXPO_TEST", "DO_MAX_EXPO_TEST", pEgfpsTestConfig->do_max_expo_test);
    pEgfpsTestConfig->max_expo_time =
        ini.GetDouble("MAX_EXPO_TEST", "MAX_EXPO_TIME", pEgfpsTestConfig->max_expo_time);
    pEgfpsTestConfig->dark_pixel_th =
        ini.GetInt("MAX_EXPO_TEST", "DARK_PIXEL_TH", pEgfpsTestConfig->dark_pixel_th);
    pEgfpsTestConfig->dark_pixel_th2 =
        ini.GetInt("MAX_EXPO_TEST", "DARK_PIXEL_TH2", pEgfpsTestConfig->dark_pixel_th2);
    pEgfpsTestConfig->max_dark_pixel =
        ini.GetInt("MAX_EXPO_TEST", "MAX_DARK_PIXEL", pEgfpsTestConfig->max_dark_pixel);

    // Img Calibration
    pEgfpsTestConfig->do_img_calibration_test = ini.GetInt(
        "IMG_MODE", "DO_IMG_CALIBRATION_TEST", pEgfpsTestConfig->do_img_calibration_test);
    pEgfpsTestConfig->img_calibration_method =
        ini.GetInt("IMG_MODE", "IMG_CALIBRATION_METHOD", pEgfpsTestConfig->img_calibration_method);
    pEgfpsTestConfig->img_gain = ini.GetInt("IMG_MODE", "IMG_GAIN", pEgfpsTestConfig->img_gain);
    pEgfpsTestConfig->ImageAverageCount =
        ini.GetInt("IMG_MODE", "AVERAGE_COUNT", pEgfpsTestConfig->ImageAverageCount);
    pEgfpsTestConfig->img_expo_time =
        ini.GetDouble("IMG_MODE", "IMG_EXPO_TIME", pEgfpsTestConfig->img_expo_time);
    pEgfpsTestConfig->img_target_mean =
        ini.GetInt("IMG_MODE", "IMG_TARGET_MEAN", pEgfpsTestConfig->img_target_mean);
    pEgfpsTestConfig->img_background_cancellation = ini.GetInt(
        "IMG_MODE", "IMG_BACKGROUND_CANCELLATION", pEgfpsTestConfig->img_background_cancellation);
    pEgfpsTestConfig->check_background_avg =
        ini.GetInt("IMG_MODE", "CHECK_BACKGROUND_AVG", pEgfpsTestConfig->check_background_avg);
    pEgfpsTestConfig->bkg_avg_min =
        ini.GetInt("IMG_MODE", "BKG_AVG_MIN", pEgfpsTestConfig->bkg_avg_min);
    pEgfpsTestConfig->bkg_avg_max =
        ini.GetInt("IMG_MODE", "BKG_AVG_MAX", pEgfpsTestConfig->bkg_avg_max);
    pEgfpsTestConfig->img_max_noise =
        ini.GetDouble("IMG_MODE", "IMG_MAX_NOISE", pEgfpsTestConfig->img_max_noise);

    // Dynamic Range
    pEgfpsTestConfig->DynamicRangeTestOption = ini.GetInt("IMG_MODE", "DYNAMIC_RANGE_TEST_OPTION",
                                                          pEgfpsTestConfig->DynamicRangeTestOption);
    pEgfpsTestConfig->ImageIntegrationCount =
        ini.GetInt("IMG_MODE", "IMG_INTEGRATION_COUNT", pEgfpsTestConfig->ImageIntegrationCount);
    pEgfpsTestConfig->ImageIntegrationRV16Min = ini.GetInt(
        "IMG_MODE", "IMG_INTEGRATION_RV16_MIN", pEgfpsTestConfig->ImageIntegrationRV16Min);
    pEgfpsTestConfig->ImageAverageRV16Min =
        ini.GetInt("IMG_MODE", "IMG_AVERAGE_RV16_MIN", pEgfpsTestConfig->ImageAverageRV16Min);

    // Bad Pixel Test
    pEgfpsTestConfig->do_bad_pxl_test =
        ini.GetInt("DIRTY_DOTS", "DO_BAD_PXL_TEST", pEgfpsTestConfig->do_bad_pxl_test);
    pEgfpsTestConfig->bad_pxl_percentage =
        ini.GetDouble("DIRTY_DOTS", "BAD_PIXEL_PERCENTAGE", pEgfpsTestConfig->bad_pxl_percentage);
    pEgfpsTestConfig->max_bad_pxl =
        ini.GetInt("DIRTY_DOTS", "MAX_BAD_PIXEL", pEgfpsTestConfig->max_bad_pxl);
    pEgfpsTestConfig->bad_rowcol_percentage = ini.GetDouble(
        "DIRTY_DOTS", "BAD_ROWCOL_PERCENTAGE", pEgfpsTestConfig->bad_rowcol_percentage);
    pEgfpsTestConfig->max_bad_rowcol =
        ini.GetInt("DIRTY_DOTS", "MAX_BAD_ROWCOL", pEgfpsTestConfig->max_bad_rowcol);
    pEgfpsTestConfig->do_consecutive_fail_test = ini.GetInt(
        "DIRTY_DOTS", "DO_CONSECUTIVE_FAIL_TEST", pEgfpsTestConfig->do_consecutive_fail_test);
    pEgfpsTestConfig->consecutive_fail_pxl =
        ini.GetInt("DIRTY_DOTS", "CONSECUTIVE_FAIL_PXL", pEgfpsTestConfig->consecutive_fail_pxl);

    // Bad Pixel Test2
    pEgfpsTestConfig->bad_pxl_percentage2 = ini.GetDouble("DIRTY_DOTS2", "BAD_PIXEL_PERCENTAGE2",
                                                          pEgfpsTestConfig->bad_pxl_percentage2);
    pEgfpsTestConfig->max_bad_pxl2 =
        ini.GetInt("DIRTY_DOTS2", "MAX_BAD_PIXEL2", pEgfpsTestConfig->max_bad_pxl2);
    pEgfpsTestConfig->bad_rowcol_percentage2 = ini.GetDouble(
        "DIRTY_DOTS2", "BAD_ROWCOL_PERCENTAGE2", pEgfpsTestConfig->bad_rowcol_percentage2);
    pEgfpsTestConfig->max_bad_rowcol2 =
        ini.GetInt("DIRTY_DOTS2", "MAX_BAD_ROWCOL2", pEgfpsTestConfig->max_bad_rowcol2);
    pEgfpsTestConfig->do_consecutive_fail_test2 = ini.GetInt(
        "DIRTY_DOTS2", "DO_CONSECUTIVE_FAIL_TEST2", pEgfpsTestConfig->do_consecutive_fail_test2);
    pEgfpsTestConfig->consecutive_fail_pxl2 =
        ini.GetInt("DIRTY_DOTS2", "CONSECUTIVE_FAIL_PXL2", pEgfpsTestConfig->consecutive_fail_pxl2);

    // Block Test
    pEgfpsTestConfig->do_bad_block_test =
        ini.GetInt("BLOCK_TEST", "DO_BAD_BLOCK_TEST", pEgfpsTestConfig->do_bad_block_test);
    pEgfpsTestConfig->bad_block_pixel_percentage = ini.GetDouble(
        "BLOCK_TEST", "BAD_BLOCK_PIXEL_PERCENTAGE", pEgfpsTestConfig->bad_block_pixel_percentage);
    pEgfpsTestConfig->block_size_width =
        ini.GetInt("BLOCK_TEST", "BLOCK_SIZE_WIDTH", pEgfpsTestConfig->block_size_width);
    pEgfpsTestConfig->block_size_height =
        ini.GetInt("BLOCK_TEST", "BLOCK_SIZE_HEIGHT", pEgfpsTestConfig->block_size_height);
    pEgfpsTestConfig->block_scan_step =
        ini.GetInt("BLOCK_TEST", "BLOCK_SCAN_STEP", pEgfpsTestConfig->block_scan_step);
    pEgfpsTestConfig->max_bad_block =
        ini.GetInt("BLOCK_TEST", "MAX_BAD_BLOCK", pEgfpsTestConfig->max_bad_block);
    pEgfpsTestConfig->max_bad_pixel_block =
        ini.GetInt("BLOCK_TEST", "MAX_BAD_PIXEL_BLOCK", pEgfpsTestConfig->max_bad_pixel_block);
    pEgfpsTestConfig->max_bad_pixel_block_all = ini.GetInt(
        "BLOCK_TEST", "MAX_BAD_PIXEL_BLOCK_ALL", pEgfpsTestConfig->max_bad_pixel_block_all);
    pEgfpsTestConfig->do_consecutive_fail_test_block =
        ini.GetInt("BLOCK_TEST", "DO_CONSECUTIVE_FAIL_TEST_BLOCK",
                   pEgfpsTestConfig->do_consecutive_fail_test_block);
    pEgfpsTestConfig->consecutive_fail_pxl_block = ini.GetInt(
        "BLOCK_TEST", "CONSECUTIVE_FAIL_PXL_BLOCK", pEgfpsTestConfig->consecutive_fail_pxl_block);

    // Power Consumption Test
    pEgfpsTestConfig->PowerConsumptionTest =
        ini.GetInt("POWER", "POWER_CONSUMPTION_TEST", pEgfpsTestConfig->PowerConsumptionTest);
    pEgfpsTestConfig->MaxPowerConsumption33 =
        ini.GetDouble("POWER", "MAX_POWER_CONSUMPTION33", pEgfpsTestConfig->MaxPowerConsumption33);
    pEgfpsTestConfig->MinPowerConsumption33 =
        ini.GetDouble("POWER", "MIN_POWER_CONSUMPTION33", pEgfpsTestConfig->MinPowerConsumption33);
    pEgfpsTestConfig->MaxPowerConsumption18 =
        ini.GetDouble("POWER", "MAX_POWER_CONSUMPTION18", pEgfpsTestConfig->MaxPowerConsumption18);

    pEgfpsTestConfig->PowerConsumptionTestPowerDown =
        ini.GetInt("POWER", "POWER_CONSUMPTION_TEST_POWERDOWN",
                   pEgfpsTestConfig->PowerConsumptionTestPowerDown);
    pEgfpsTestConfig->MaxPowerConsumption33Pd = ini.GetDouble(
        "POWER", "MAX_POWER_CONSUMPTION33_PD", pEgfpsTestConfig->MaxPowerConsumption33Pd);
    pEgfpsTestConfig->MinPowerConsumption33Pd = ini.GetDouble(
        "POWER", "MIN_POWER_CONSUMPTION33_PD", pEgfpsTestConfig->MinPowerConsumption33Pd);
    pEgfpsTestConfig->MaxPowerConsumption18Pd = ini.GetDouble(
        "POWER", "MAX_POWER_CONSUMPTION18_PD", pEgfpsTestConfig->MaxPowerConsumption18Pd);

    pEgfpsTestConfig->PowerConsumptionTest18 =
        ini.GetInt("POWER", "POWER_CONSUMPTION_TEST18", pEgfpsTestConfig->PowerConsumptionTest18);

#if (IS_EGIS_SENSOR_LENS_TYPE)
    // LENS_TEST
    pEgfpsTestConfig->lens_roi_x =
        ini.GetInt("LENS_TEST", "LENS_ROI_X", pEgfpsTestConfig->lens_roi_x);
    pEgfpsTestConfig->lens_roi_y =
        ini.GetInt("LENS_TEST", "LENS_ROI_Y", pEgfpsTestConfig->lens_roi_y);
    pEgfpsTestConfig->do_lens_centeroid_test =
        ini.GetInt("LENS_TEST", "DO_LENS_CENTEROID_TEST", pEgfpsTestConfig->do_lens_centeroid_test);
    pEgfpsTestConfig->centeroid_expo_time =
        ini.GetDouble("LENS_TEST", "CENTEROID_EXPO_TIME", pEgfpsTestConfig->centeroid_expo_time);
    pEgfpsTestConfig->lens_design_center_x =
        ini.GetInt("LENS_TEST", "LENS_DESIGN_CENTER_X", pEgfpsTestConfig->lens_design_center_x);
    pEgfpsTestConfig->lens_design_center_y =
        ini.GetInt("LENS_TEST", "LENS_DESIGN_CENTER_Y", pEgfpsTestConfig->lens_design_center_y);
    pEgfpsTestConfig->lens_center_tolerance_line = ini.GetInt(
        "LENS_TEST", "LENS_CENTER_TOLERANCE", pEgfpsTestConfig->lens_center_tolerance_line);
    pEgfpsTestConfig->do_lens_mtf_test =
        ini.GetInt("LENS_TEST", "DO_LENS_MTF_TEST", pEgfpsTestConfig->do_lens_mtf_test);
    pEgfpsTestConfig->lens_mtf_scan_window =
        ini.GetInt("LENS_TEST", "LENS_MTF_SCAN_WINDOW", pEgfpsTestConfig->lens_mtf_scan_window);
    pEgfpsTestConfig->lens_mtf_test_case =
        ini.GetInt("LENS_TEST", "LENS_MTF_TEST_CASE", pEgfpsTestConfig->lens_mtf_test_case);
    pEgfpsTestConfig->lens_mtf_center_floor = ini.GetDouble(
        "LENS_TEST", "LENS_MTF_CENTER_FLOOR", pEgfpsTestConfig->lens_mtf_center_floor);
    pEgfpsTestConfig->lens_mtf_center_floor2 = ini.GetDouble(
        "LENS_TEST", "LENS_MTF_CENTER_FLOOR2", pEgfpsTestConfig->lens_mtf_center_floor2);
    pEgfpsTestConfig->lens_mtf_corner_floor = ini.GetDouble(
        "LENS_TEST", "LENS_MTF_CORNER_FLOOR", pEgfpsTestConfig->lens_mtf_corner_floor);
    pEgfpsTestConfig->lens_mtf_corner_floor2 = ini.GetDouble(
        "LENS_TEST", "LENS_MTF_CORNER_FLOOR2", pEgfpsTestConfig->lens_mtf_corner_floor2);

    // pEgfpsTestConfig->lens_center_tolerance_x  = ini.GetInt("LENS_TEST",
    // "LENS_CENTER_TOLERANCE_X", pEgfpsTestConfig->lens_center_tolerance_x);
    // pEgfpsTestConfig->lens_center_tolerance_y  = ini.GetInt("LENS_TEST",
    // "LENS_CENTER_TOLERANCE_Y", pEgfpsTestConfig->lens_center_tolerance_y);

#endif

    // MPINFO
    pEgfpsTestConfig->ProductionPlant =
        ini.GetInt("MP_INFO", "PRODUCTION_PLANT", pEgfpsTestConfig->ProductionPlant);
    pEgfpsTestConfig->ProjectStep =
        ini.GetInt("MP_INFO", "PROJECT_STEP", pEgfpsTestConfig->ProjectStep);

    // NVM
    pEgfpsTestConfig->package_has_flash =
        ini.GetInt("NVM", "PACKAGE_HAS_FLASH", pEgfpsTestConfig->package_has_flash);
    pEgfpsTestConfig->check_flash_data_valid =
        ini.GetInt("NVM", "CHECK_FLASH_DATA_VALID", pEgfpsTestConfig->check_flash_data_valid);
    pEgfpsTestConfig->write_flash_data =
        ini.GetInt("NVM", "WRITE_FLASH_DATA", pEgfpsTestConfig->write_flash_data);
    pEgfpsTestConfig->check_otp_data_valid =
        ini.GetInt("NVM", "CHECK_OTP_DATA_VALID", pEgfpsTestConfig->check_otp_data_valid);
    pEgfpsTestConfig->write_otp_data =
        ini.GetInt("NVM", "WRITE_OTP_DATA", pEgfpsTestConfig->write_otp_data);

    return TRUE;
}

#ifdef WIN32

void vLoadGlobalTestConfig(EGFPS_GLOBAL_CONFIG* pEgfpsGlobalConfig) {
    CIni ini;
    char inifile[MAX_PATH];
    char testconfig[MAX_PATH];

    sprintf(inifile, ".\\GlobalConfig.ini");
    ini.SetPathName(inifile);

    memset(pEgfpsGlobalConfig, 0x00, sizeof(EGFPS_GLOBAL_CONFIG));
    sprintf(testconfig, ".\\default.ini");

    // test program name
#if (EGIS_SENSOR == 711)
    ini.GetString("GLOBAL", "TEST_PROGRAM_NAME", pEgfpsGlobalConfig->TestProgramName, MAX_PATH,
                  "ET711_FT_TOOL");
#elif (EGIS_SENSOR == 713)
    ini.GetString("GLOBAL", "TEST_PROGRAM_NAME", pEgfpsGlobalConfig->TestProgramName, MAX_PATH,
                  "ET713_FT_TOOL");
#elif (EGIS_SENSOR == 701)
    ini.GetString("GLOBAL", "TEST_PROGRAM_NAME", pEgfpsGlobalConfig->TestProgramName, MAX_PATH,
                  "ET701_FT_TOOL");
#endif

    // test config file
    ini.GetString("GLOBAL", "CONFIG_FILE", pEgfpsGlobalConfig->TestConfigFile, MAX_PATH,
                  testconfig);

    // test log folder
    ini.GetString("GLOBAL", "TEST_FOLDER", pEgfpsGlobalConfig->TestLogFolder, MAX_PATH, "");
    pEgfpsGlobalConfig->TestSequence = ini.GetInt("GLOBAL", "TEST_SEQUENCE", 1);

    pEgfpsGlobalConfig->ShowTestDetail = ini.GetInt("GLOBAL", "SHOW_TEST_DETAIL", 0);
    pEgfpsGlobalConfig->SaveImage = ini.GetInt("GLOBAL", "SAVE_TEST_IMAGE", 0);

    // tester board
    pEgfpsGlobalConfig->TestMode = ini.GetInt("TESTER", "TEST_MODE", 0);
    pEgfpsGlobalConfig->NumberOfSites = ini.GetInt("TESTER", "NUM_OF_SITES", 1);

    // LED
    pEgfpsGlobalConfig->backlight_led_always_on = ini.GetInt("LED", "BACKLIGHT_LED_ALWAYS_ON", 0);
    pEgfpsGlobalConfig->backlight_led_control_board_type =
        ini.GetInt("LED", "BACKLIGHT_LED_CONTROL_BOARD_TYPE", 1);

    pEgfpsGlobalConfig->backlight_check_range = ini.GetInt("LED", "BACKLIGHT_CHECK_RANGE", 0);
    pEgfpsGlobalConfig->backlight_range_max = ini.GetInt("LED", "BACKLIGHT_RANGE_MAX", 0x03FF);
    pEgfpsGlobalConfig->backlight_range_min = ini.GetInt("LED", "BACKLIGHT_RANGE_MIN", 0x0000);

    pEgfpsGlobalConfig->backlight_led_lightness_max =
        ini.GetInt("LED", "BACKLIGHT_LED_LIGHTNESS_MAX", 2000);
    pEgfpsGlobalConfig->backlight_led_lightness_min =
        ini.GetInt("LED", "BACKLIGHT_LED_LIGHTNESS_MIN", 0);

    int i;
    char buf[MAX_PATH];
    char uid_temp[MAX_PATH];

    for (i = 0; i < MAX_DEVICE_NUM; i++) {
        pEgfpsGlobalConfig->BoardInstances[i].PortIndex = i;

        // UID
        sprintf(buf, "SITE%d", i + 1);

        ini.GetString("TESTER", buf, uid_temp, 17, "                ");
        strcpy((char*)pEgfpsGlobalConfig->BoardInstances[i].unique_id, uid_temp);

        // Backlight LED
        sprintf(buf, "BACKLIGHT_LED_LIGHTNESS%d", i + 1);
        pEgfpsGlobalConfig->backlight_led_lightness[i] = ini.GetInt("LED", buf, 0);

        sprintf(buf, "BACKLIGHT_LED_GAIN%d", i + 1);
        pEgfpsGlobalConfig->backlight_led_gain[i] = ini.GetInt("LED", buf, 0);

        // Current Offset
        sprintf(buf, "SITE%d_V33_OFFSET", i + 1);
        pEgfpsGlobalConfig->BoardInstances[i].CurrentOffset33 = ini.GetDouble("TESTER", buf, 0);

        sprintf(buf, "SITE%d_V18_OFFSET", i + 1);
        pEgfpsGlobalConfig->BoardInstances[i].CurrentOffset18 = ini.GetDouble("TESTER", buf, 0);
    }

    pEgfpsGlobalConfig->backlight_led_check_valid =
        ini.GetInt("LED", "BACKLIGHT_LED_CHECK_VALID", 1);
}

void vSaveGlobalTestConfig(EGFPS_GLOBAL_CONFIG* pEgfpsGlobalConfig) {
    CIni ini;
    char inifile[MAX_PATH];

    sprintf(inifile, ".\\GlobalConfig.ini");
    ini.SetPathName(inifile);

    // test program name
    ini.WriteString("GLOBAL", "TEST_PROGRAM_NAME", pEgfpsGlobalConfig->TestProgramName);

    // test config file
    ini.WriteString("GLOBAL", "CONFIG_FILE", pEgfpsGlobalConfig->TestConfigFile);

    // test log folder
    ini.WriteString("GLOBAL", "TEST_FOLDER", pEgfpsGlobalConfig->TestLogFolder);
    ini.WriteInt("GLOBAL", "TEST_SEQUENCE", pEgfpsGlobalConfig->TestSequence);
    ini.WriteInt("GLOBAL", "SHOW_TEST_DETAIL", pEgfpsGlobalConfig->ShowTestDetail);
    ini.WriteInt("GLOBAL", "SAVE_TEST_IMAGE", pEgfpsGlobalConfig->SaveImage);

    ini.WriteInt("TESTER", "TEST_MODE", pEgfpsGlobalConfig->TestMode);
    ini.WriteInt("TESTER", "NUM_OF_SITES", pEgfpsGlobalConfig->NumberOfSites);

    int i;
    char buf[MAX_PATH];

    for (i = 0; i < MAX_DEVICE_NUM; i++) {
        // UID
        sprintf(buf, "SITE%d", i + 1);
        ini.WriteString("TESTER", buf, (char*)pEgfpsGlobalConfig->BoardInstances[i].unique_id);
    }

    for (i = 0; i < MAX_DEVICE_NUM; i++) {
        // Current Offset
        sprintf(buf, "SITE%d_V33_OFFSET", i + 1);
        ini.WriteDouble("TESTER", buf, pEgfpsGlobalConfig->BoardInstances[i].CurrentOffset33);
    }

    for (i = 0; i < MAX_DEVICE_NUM; i++) {
        // Current Offset
        sprintf(buf, "SITE%d_V18_OFFSET", i + 1);
        ini.WriteDouble("TESTER", buf, pEgfpsGlobalConfig->BoardInstances[i].CurrentOffset18);
    }

    // LED
    ini.WriteInt("LED", "BACKLIGHT_LED_ALWAYS_ON", pEgfpsGlobalConfig->backlight_led_always_on);
    ini.WriteInt("LED", "BACKLIGHT_LED_CONTROL_BOARD_TYPE",
                 pEgfpsGlobalConfig->backlight_led_control_board_type);

    ini.WriteInt("LED", "BACKLIGHT_LED_CHECK_VALID", pEgfpsGlobalConfig->backlight_led_check_valid);

    ini.WriteInt("LED", "BACKLIGHT_CHECK_RANGE", pEgfpsGlobalConfig->backlight_check_range);
    ini.WriteInt("LED", "BACKLIGHT_RANGE_MAX", pEgfpsGlobalConfig->backlight_range_max);
    ini.WriteInt("LED", "BACKLIGHT_RANGE_MIN", pEgfpsGlobalConfig->backlight_range_min);

    ini.WriteInt("LED", "BACKLIGHT_LED_LIGHTNESS_MAX",
                 pEgfpsGlobalConfig->backlight_led_lightness_max);
    ini.WriteInt("LED", "BACKLIGHT_LED_LIGHTNESS_MIN",
                 pEgfpsGlobalConfig->backlight_led_lightness_min);

    for (i = 0; i < MAX_DEVICE_NUM; i++) {
        sprintf(buf, "BACKLIGHT_LED_GAIN%d", i + 1);
        ini.WriteInt("LED", buf, pEgfpsGlobalConfig->backlight_led_gain[i]);
    }

    for (i = 0; i < MAX_DEVICE_NUM; i++) {
        // Backlight LED
        sprintf(buf, "BACKLIGHT_LED_LIGHTNESS%d", i + 1);
        ini.WriteInt("LED", buf, pEgfpsGlobalConfig->backlight_led_lightness[i]);
    }
}

void vSaveTestConfig(EGFPS_TEST_CONFIG* pEgfpsTestConfig, char* pConfigFile) {
    CIni ini;
    char ini_file[MAX_PATH];

    BOOL bIsConfigEncrypted;

    if ((strcmp(pConfigFile + strlen(pConfigFile) - 4, ".cfg") == 0) ||
        (strcmp(pConfigFile + strlen(pConfigFile) - 4, ".CFG") == 0)) {
        bIsConfigEncrypted = TRUE;

        char temp[MAX_PATH];
        GetTempPath(MAX_PATH, temp);

        sprintf(ini_file, "%s\\%s.ini", temp, pPath2FileName(pConfigFile));
    } else {
        bIsConfigEncrypted = FALSE;

        strcpy(ini_file, pConfigFile);
    }

    ini.SetPathName(ini_file);

    // Tester
    ini.WriteInt("MISC", "CONFIG_VERSION", pEgfpsTestConfig->ConfigVersion);
    ini.WriteInt("MISC", "BIN2_IS_PASS_BIN", pEgfpsTestConfig->bin2_is_pass_bin);

    // Sensor Setting
    ini.WriteInt("SENSOR_SETTING", "LOAD_SETTING_FILE", pEgfpsTestConfig->do_load_setting_file);
    ini.WriteString("SENSOR_SETTING", "SETTING_FILE", pEgfpsTestConfig->SensorSettingFile);
    ini.WriteInt("SENSOR_SETTING", "RESET_HOLD", pEgfpsTestConfig->ResetHold);

    // test pattern
    ini.WriteInt("TEST_PATTERN", "CHECK_TEST_PATTERN", pEgfpsTestConfig->check_test_pattern);

    // Dark Level Test
    ini.WriteInt("DARK_LEVEL", "DO_DARK_IMG_TEST", pEgfpsTestConfig->do_dark_img_test);
    ini.WriteDouble("DARK_LEVEL", "DARK_EXPO_TIME", pEgfpsTestConfig->dark_expo_time);
    ini.WriteInt("DARK_LEVEL", "DARK_LEVEL_TH", pEgfpsTestConfig->dark_level_th);
    ini.WriteInt("DARK_LEVEL", "WHITE_PIXEL_TH", pEgfpsTestConfig->white_pixel_th);
    ini.WriteInt("DARK_LEVEL", "MAX_WHITE_PIXEL", pEgfpsTestConfig->max_white_pixel);

    // Max Exposure Time
    ini.WriteInt("MAX_EXPO_TEST", "MAX_EXPO_GAIN", pEgfpsTestConfig->max_expo_gain);
    ini.WriteInt("MAX_EXPO_TEST", "DO_MAX_EXPO_TEST", pEgfpsTestConfig->do_max_expo_test);
    ini.WriteDouble("MAX_EXPO_TEST", "MAX_EXPO_TIME", pEgfpsTestConfig->max_expo_time);
    ini.WriteInt("MAX_EXPO_TEST", "DARK_PIXEL_TH", pEgfpsTestConfig->dark_pixel_th);
    ini.WriteInt("MAX_EXPO_TEST", "DARK_PIXEL_TH2", pEgfpsTestConfig->dark_pixel_th2);
    ini.WriteInt("MAX_EXPO_TEST", "MAX_DARK_PIXEL", pEgfpsTestConfig->max_dark_pixel);

    // Img Calibration
    ini.WriteInt("IMG_MODE", "DO_IMG_CALIBRATION_TEST", pEgfpsTestConfig->do_img_calibration_test);
    ini.WriteInt("IMG_MODE", "IMG_CALIBRATION_METHOD", pEgfpsTestConfig->img_calibration_method);
    ini.WriteInt("IMG_MODE", "IMG_GAIN", pEgfpsTestConfig->img_gain);
    ini.WriteInt("IMG_MODE", "AVERAGE_COUNT", pEgfpsTestConfig->ImageAverageCount);
    ini.WriteDouble("IMG_MODE", "IMG_EXPO_TIME", pEgfpsTestConfig->img_expo_time);
    ini.WriteInt("IMG_MODE", "IMG_TARGET_MEAN", pEgfpsTestConfig->img_target_mean);
    ini.WriteInt("IMG_MODE", "IMG_BACKGROUND_CANCELLATION",
                 pEgfpsTestConfig->img_background_cancellation);
    ini.WriteInt("IMG_MODE", "CHECK_BACKGROUND_AVG", pEgfpsTestConfig->check_background_avg);
    ini.WriteInt("IMG_MODE", "BKG_AVG_MIN", pEgfpsTestConfig->bkg_avg_min);
    ini.WriteInt("IMG_MODE", "BKG_AVG_MAX", pEgfpsTestConfig->bkg_avg_max);
    ini.WriteDouble("IMG_MODE", "IMG_MAX_NOISE", pEgfpsTestConfig->img_max_noise);

    // Dynamic Range
    ini.WriteInt("IMG_MODE", "DYNAMIC_RANGE_TEST_OPTION", pEgfpsTestConfig->DynamicRangeTestOption);
    ini.WriteInt("IMG_MODE", "IMG_INTEGRATION_COUNT", pEgfpsTestConfig->ImageIntegrationCount);
    ini.WriteInt("IMG_MODE", "IMG_INTEGRATION_RV16_MIN", pEgfpsTestConfig->ImageIntegrationRV16Min);
    ini.WriteInt("IMG_MODE", "IMG_AVERAGE_RV16_MIN", pEgfpsTestConfig->ImageAverageRV16Min);

    // Bad Pixel Test
    ini.WriteInt("DIRTY_DOTS", "DO_BAD_PXL_TEST", pEgfpsTestConfig->do_bad_pxl_test);
    ini.WriteDouble("DIRTY_DOTS", "BAD_PIXEL_PERCENTAGE", pEgfpsTestConfig->bad_pxl_percentage);
    ini.WriteInt("DIRTY_DOTS", "MAX_BAD_PIXEL", pEgfpsTestConfig->max_bad_pxl);
    ini.WriteDouble("DIRTY_DOTS", "BAD_ROWCOL_PERCENTAGE", pEgfpsTestConfig->bad_rowcol_percentage);
    ini.WriteInt("DIRTY_DOTS", "MAX_BAD_ROWCOL", pEgfpsTestConfig->max_bad_rowcol);
    ini.WriteInt("DIRTY_DOTS", "DO_CONSECUTIVE_FAIL_TEST",
                 pEgfpsTestConfig->do_consecutive_fail_test);
    ini.WriteInt("DIRTY_DOTS", "CONSECUTIVE_FAIL_PXL", pEgfpsTestConfig->consecutive_fail_pxl);

    // Bad Pixel Test2
    ini.WriteDouble("DIRTY_DOTS2", "BAD_PIXEL_PERCENTAGE2", pEgfpsTestConfig->bad_pxl_percentage2);
    ini.WriteInt("DIRTY_DOTS2", "MAX_BAD_PIXEL2", pEgfpsTestConfig->max_bad_pxl2);
    ini.WriteDouble("DIRTY_DOTS2", "BAD_ROWCOL_PERCENTAGE2",
                    pEgfpsTestConfig->bad_rowcol_percentage2);
    ini.WriteInt("DIRTY_DOTS2", "MAX_BAD_ROWCOL2", pEgfpsTestConfig->max_bad_rowcol2);
    ini.WriteInt("DIRTY_DOTS2", "DO_CONSECUTIVE_FAIL_TEST2",
                 pEgfpsTestConfig->do_consecutive_fail_test2);
    ini.WriteInt("DIRTY_DOTS2", "CONSECUTIVE_FAIL_PXL2", pEgfpsTestConfig->consecutive_fail_pxl2);

    // block test
    ini.WriteInt("BLOCK_TEST", "DO_BAD_BLOCK_TEST", pEgfpsTestConfig->do_bad_block_test);
    ini.WriteDouble("BLOCK_TEST", "BAD_BLOCK_PIXEL_PERCENTAGE",
                    pEgfpsTestConfig->bad_block_pixel_percentage);
    ini.WriteInt("BLOCK_TEST", "BLOCK_SIZE_WIDTH", pEgfpsTestConfig->block_size_width);
    ini.WriteInt("BLOCK_TEST", "BLOCK_SIZE_HEIGHT", pEgfpsTestConfig->block_size_height);
    ini.WriteInt("BLOCK_TEST", "BLOCK_SCAN_STEP", pEgfpsTestConfig->block_scan_step);
    ini.WriteInt("BLOCK_TEST", "MAX_BAD_BLOCK", pEgfpsTestConfig->max_bad_block);
    ini.WriteInt("BLOCK_TEST", "MAX_BAD_PIXEL_BLOCK", pEgfpsTestConfig->max_bad_pixel_block);
    ini.WriteInt("BLOCK_TEST", "MAX_BAD_PIXEL_BLOCK_ALL",
                 pEgfpsTestConfig->max_bad_pixel_block_all);
    ini.WriteInt("BLOCK_TEST", "DO_CONSECUTIVE_FAIL_TEST_BLOCK",
                 pEgfpsTestConfig->do_consecutive_fail_test_block);
    ini.WriteInt("BLOCK_TEST", "CONSECUTIVE_FAIL_PXL_BLOCK",
                 pEgfpsTestConfig->consecutive_fail_pxl_block);

    // Power
    ini.WriteInt("POWER", "POWER_CONSUMPTION_TEST", pEgfpsTestConfig->PowerConsumptionTest);
    ini.WriteDouble("POWER", "MAX_POWER_CONSUMPTION33", pEgfpsTestConfig->MaxPowerConsumption33);
    ini.WriteDouble("POWER", "MIN_POWER_CONSUMPTION33", pEgfpsTestConfig->MinPowerConsumption33);
    ini.WriteDouble("POWER", "MAX_POWER_CONSUMPTION18", pEgfpsTestConfig->MaxPowerConsumption18);

    ini.WriteInt("POWER", "POWER_CONSUMPTION_TEST_POWERDOWN",
                 pEgfpsTestConfig->PowerConsumptionTestPowerDown);
    ini.WriteDouble("POWER", "MAX_POWER_CONSUMPTION33_PD",
                    pEgfpsTestConfig->MaxPowerConsumption33Pd);
    ini.WriteDouble("POWER", "MIN_POWER_CONSUMPTION33_PD",
                    pEgfpsTestConfig->MinPowerConsumption33Pd);
    ini.WriteDouble("POWER", "MAX_POWER_CONSUMPTION18_PD",
                    pEgfpsTestConfig->MaxPowerConsumption18Pd);

    ini.WriteInt("POWER", "POWER_CONSUMPTION_TEST18", pEgfpsTestConfig->PowerConsumptionTest18);

#if (IS_EGIS_SENSOR_LENS_TYPE)
    // LENS_TEST
    ini.WriteInt("LENS_TEST", "LENS_ROI_X", pEgfpsTestConfig->lens_roi_x);
    ini.WriteInt("LENS_TEST", "LENS_ROI_Y", pEgfpsTestConfig->lens_roi_y);
    ini.WriteInt("LENS_TEST", "DO_LENS_CENTEROID_TEST", pEgfpsTestConfig->do_lens_centeroid_test);
    ini.WriteDouble("LENS_TEST", "CENTEROID_EXPO_TIME", pEgfpsTestConfig->centeroid_expo_time);
    ini.WriteInt("LENS_TEST", "LENS_DESIGN_CENTER_X", pEgfpsTestConfig->lens_design_center_x);
    ini.WriteInt("LENS_TEST", "LENS_DESIGN_CENTER_Y", pEgfpsTestConfig->lens_design_center_y);
    // ini.WriteInt("LENS_TEST",  "LENS_CENTER_TOLERANCE_X",
    // pEgfpsTestConfig->lens_center_tolerance_x);  ini.WriteInt("LENS_TEST",
    // "LENS_CENTER_TOLERANCE_Y", pEgfpsTestConfig->lens_center_tolerance_y);
    ini.WriteInt("LENS_TEST", "LENS_CENTER_TOLERANCE",
                 pEgfpsTestConfig->lens_center_tolerance_line);
    ini.WriteInt("LENS_TEST", "DO_LENS_MTF_TEST", pEgfpsTestConfig->do_lens_mtf_test);
    ini.WriteInt("LENS_TEST", "LENS_MTF_SCAN_WINDOW", pEgfpsTestConfig->lens_mtf_scan_window);
    ini.WriteInt("LENS_TEST", "LENS_MTF_TEST_CASE", pEgfpsTestConfig->lens_mtf_test_case);
    ini.WriteDouble("LENS_TEST", "LENS_MTF_CENTER_FLOOR", pEgfpsTestConfig->lens_mtf_center_floor);
    ini.WriteDouble("LENS_TEST", "LENS_MTF_CENTER_FLOOR2",
                    pEgfpsTestConfig->lens_mtf_center_floor2);
    ini.WriteDouble("LENS_TEST", "LENS_MTF_CORNER_FLOOR", pEgfpsTestConfig->lens_mtf_corner_floor);
    ini.WriteDouble("LENS_TEST", "LENS_MTF_CORNER_FLOOR2",
                    pEgfpsTestConfig->lens_mtf_corner_floor2);
#endif

    // MPINFO
    ini.WriteInt("MP_INFO", "PRODUCTION_PLANT", pEgfpsTestConfig->ProductionPlant);
    ini.WriteInt("MP_INFO", "PROJECT_STEP", pEgfpsTestConfig->ProjectStep);

    // NVM
    ini.WriteInt("NVM", "PACKAGE_HAS_FLASH", pEgfpsTestConfig->package_has_flash);
    ini.WriteInt("NVM", "CHECK_FLASH_DATA_VALID", pEgfpsTestConfig->check_flash_data_valid);
    ini.WriteInt("NVM", "WRITE_FLASH_DATA", pEgfpsTestConfig->write_flash_data);
    ini.WriteInt("NVM", "CHECK_OTP_DATA_VALID", pEgfpsTestConfig->check_otp_data_valid);
    ini.WriteInt("NVM", "WRITE_OTP_DATA", pEgfpsTestConfig->write_otp_data);

    if (bIsConfigEncrypted) {
        unsigned char cfgkey_str[16];
        memset(cfgkey_str, 0x00, 16);
        memcpy(cfgkey_str, ENCRYPT_KEY, strlen(ENCRYPT_KEY));

        rijndael_file_encrypt(MODE_ECB, NULL, cfgkey_str, 128, ini_file, pConfigFile);
    }
}

void vBuildConfigHash(EGFPS_TEST_CONFIG* pEgfpsTestConfig, unsigned char* pDigest) {
    CSHA1 ini_sha1;

    ini_sha1.Reset();

    // Misc
    ini_sha1.Update((BYTE*)&pEgfpsTestConfig->ConfigVersion, sizeof(int));
    ini_sha1.Update((BYTE*)&pEgfpsTestConfig->bin2_is_pass_bin, sizeof(int));

    // Sensor Setting
    ini_sha1.Update((BYTE*)&pEgfpsTestConfig->SensorSettingFile,
                    strlen(pEgfpsTestConfig->SensorSettingFile));
    ini_sha1.Update((BYTE*)&pEgfpsTestConfig->ResetHold, sizeof(int));

    // test pattern
    ini_sha1.Update((BYTE*)&pEgfpsTestConfig->check_test_pattern, sizeof(int));

    // Dark Level Test
    ini_sha1.Update((BYTE*)&pEgfpsTestConfig->do_dark_img_test, sizeof(int));
    ini_sha1.Update((BYTE*)&pEgfpsTestConfig->dark_expo_time, sizeof(double));
    ini_sha1.Update((BYTE*)&pEgfpsTestConfig->dark_level_th, sizeof(int));
    ini_sha1.Update((BYTE*)&pEgfpsTestConfig->white_pixel_th, sizeof(int));
    ini_sha1.Update((BYTE*)&pEgfpsTestConfig->max_white_pixel, sizeof(int));

    // Max Exposure Time
    ini_sha1.Update((BYTE*)&pEgfpsTestConfig->max_expo_gain, sizeof(int));
    ini_sha1.Update((BYTE*)&pEgfpsTestConfig->do_max_expo_test, sizeof(int));
    ini_sha1.Update((BYTE*)&pEgfpsTestConfig->max_expo_time, sizeof(double));
    ini_sha1.Update((BYTE*)&pEgfpsTestConfig->dark_pixel_th, sizeof(int));
    ini_sha1.Update((BYTE*)&pEgfpsTestConfig->dark_pixel_th2, sizeof(int));
    ini_sha1.Update((BYTE*)&pEgfpsTestConfig->max_dark_pixel, sizeof(int));

    // Img Calibration
    ini_sha1.Update((BYTE*)&pEgfpsTestConfig->do_img_calibration_test, sizeof(int));
    ini_sha1.Update((BYTE*)&pEgfpsTestConfig->img_calibration_method, sizeof(int));
    ini_sha1.Update((BYTE*)&pEgfpsTestConfig->img_gain, sizeof(int));
    ini_sha1.Update((BYTE*)&pEgfpsTestConfig->ImageAverageCount, sizeof(int));
    ini_sha1.Update((BYTE*)&pEgfpsTestConfig->img_expo_time, sizeof(double));
    ini_sha1.Update((BYTE*)&pEgfpsTestConfig->img_target_mean, sizeof(unsigned short));
    ini_sha1.Update((BYTE*)&pEgfpsTestConfig->img_background_cancellation, sizeof(int));
    ini_sha1.Update((BYTE*)&pEgfpsTestConfig->check_background_avg, sizeof(int));
    ini_sha1.Update((BYTE*)&pEgfpsTestConfig->bkg_avg_min, sizeof(int));
    ini_sha1.Update((BYTE*)&pEgfpsTestConfig->bkg_avg_max, sizeof(int));
    ini_sha1.Update((BYTE*)&pEgfpsTestConfig->img_max_noise, sizeof(double));

    // Dynamic Range
    ini_sha1.Update((BYTE*)&pEgfpsTestConfig->DynamicRangeTestOption, sizeof(int));
    ini_sha1.Update((BYTE*)&pEgfpsTestConfig->ImageIntegrationCount, sizeof(int));
    ini_sha1.Update((BYTE*)&pEgfpsTestConfig->ImageIntegrationRV16Min, sizeof(int));
    ini_sha1.Update((BYTE*)&pEgfpsTestConfig->ImageAverageRV16Min, sizeof(int));

    // Bad Pixel Test
    ini_sha1.Update((BYTE*)&pEgfpsTestConfig->do_bad_pxl_test, sizeof(int));
    ini_sha1.Update((BYTE*)&pEgfpsTestConfig->bad_pxl_percentage, sizeof(double));
    ini_sha1.Update((BYTE*)&pEgfpsTestConfig->max_bad_pxl, sizeof(int));
    ini_sha1.Update((BYTE*)&pEgfpsTestConfig->bad_rowcol_percentage, sizeof(double));
    ini_sha1.Update((BYTE*)&pEgfpsTestConfig->max_bad_rowcol, sizeof(int));
    ini_sha1.Update((BYTE*)&pEgfpsTestConfig->do_consecutive_fail_test, sizeof(int));
    ini_sha1.Update((BYTE*)&pEgfpsTestConfig->consecutive_fail_pxl, sizeof(int));

    // Bad Pixel Test2
    ini_sha1.Update((BYTE*)&pEgfpsTestConfig->bad_pxl_percentage2, sizeof(double));
    ini_sha1.Update((BYTE*)&pEgfpsTestConfig->max_bad_pxl2, sizeof(int));
    ini_sha1.Update((BYTE*)&pEgfpsTestConfig->bad_rowcol_percentage2, sizeof(double));
    ini_sha1.Update((BYTE*)&pEgfpsTestConfig->max_bad_rowcol2, sizeof(int));
    ini_sha1.Update((BYTE*)&pEgfpsTestConfig->do_consecutive_fail_test2, sizeof(int));
    ini_sha1.Update((BYTE*)&pEgfpsTestConfig->consecutive_fail_pxl2, sizeof(int));

    // block test
    ini_sha1.Update((BYTE*)&pEgfpsTestConfig->do_bad_block_test, sizeof(int));
    ini_sha1.Update((BYTE*)&pEgfpsTestConfig->bad_block_pixel_percentage, sizeof(double));
    ini_sha1.Update((BYTE*)&pEgfpsTestConfig->block_size_width, sizeof(int));
    ini_sha1.Update((BYTE*)&pEgfpsTestConfig->block_size_height, sizeof(int));
    ini_sha1.Update((BYTE*)&pEgfpsTestConfig->block_scan_step, sizeof(int));
    ini_sha1.Update((BYTE*)&pEgfpsTestConfig->max_bad_block, sizeof(int));
    ini_sha1.Update((BYTE*)&pEgfpsTestConfig->max_bad_pixel_block, sizeof(int));
    ini_sha1.Update((BYTE*)&pEgfpsTestConfig->max_bad_pixel_block_all, sizeof(int));
    ini_sha1.Update((BYTE*)&pEgfpsTestConfig->do_consecutive_fail_test_block, sizeof(int));
    ini_sha1.Update((BYTE*)&pEgfpsTestConfig->consecutive_fail_pxl_block, sizeof(int));

    // Power
    ini_sha1.Update((BYTE*)&pEgfpsTestConfig->PowerConsumptionTest, sizeof(int));
    ini_sha1.Update((BYTE*)&pEgfpsTestConfig->MaxPowerConsumption33, sizeof(double));
    ini_sha1.Update((BYTE*)&pEgfpsTestConfig->MinPowerConsumption33, sizeof(double));
    ini_sha1.Update((BYTE*)&pEgfpsTestConfig->MaxPowerConsumption18, sizeof(double));

    ini_sha1.Update((BYTE*)&pEgfpsTestConfig->PowerConsumptionTestPowerDown, sizeof(int));
    ini_sha1.Update((BYTE*)&pEgfpsTestConfig->MaxPowerConsumption33Pd, sizeof(double));
    ini_sha1.Update((BYTE*)&pEgfpsTestConfig->MinPowerConsumption33Pd, sizeof(double));
    ini_sha1.Update((BYTE*)&pEgfpsTestConfig->MaxPowerConsumption18Pd, sizeof(double));

    ini_sha1.Update((BYTE*)&pEgfpsTestConfig->PowerConsumptionTest18, sizeof(int));

#if (IS_EGIS_SENSOR_LENS_TYPE)
    ini_sha1.Update((BYTE*)&pEgfpsTestConfig->do_lens_centeroid_test, sizeof(int));
    ini_sha1.Update((BYTE*)&pEgfpsTestConfig->centeroid_expo_time, sizeof(double));
    ini_sha1.Update((BYTE*)&pEgfpsTestConfig->lens_design_center_x, sizeof(int));
    ini_sha1.Update((BYTE*)&pEgfpsTestConfig->lens_design_center_y, sizeof(int));
    // ini_sha1.Update((BYTE *) &pEgfpsTestConfig->lens_center_tolerance_x , sizeof(int));
    // ini_sha1.Update((BYTE *) &pEgfpsTestConfig->lens_center_tolerance_y , sizeof(int));
    ini_sha1.Update((BYTE*)&pEgfpsTestConfig->lens_center_tolerance_line, sizeof(int));
    ini_sha1.Update((BYTE*)&pEgfpsTestConfig->do_lens_mtf_test, sizeof(int));
    ini_sha1.Update((BYTE*)&pEgfpsTestConfig->lens_mtf_scan_window, sizeof(int));
    ini_sha1.Update((BYTE*)&pEgfpsTestConfig->lens_mtf_test_case, sizeof(int));
    ini_sha1.Update((BYTE*)&pEgfpsTestConfig->lens_mtf_center_floor, sizeof(double));
    ini_sha1.Update((BYTE*)&pEgfpsTestConfig->lens_mtf_center_floor2, sizeof(double));
    ini_sha1.Update((BYTE*)&pEgfpsTestConfig->lens_mtf_corner_floor, sizeof(double));
    ini_sha1.Update((BYTE*)&pEgfpsTestConfig->lens_mtf_corner_floor2, sizeof(double));
#endif

    ini_sha1.Update((BYTE*)&pEgfpsTestConfig->ProductionPlant, sizeof(int));
    ini_sha1.Update((BYTE*)&pEgfpsTestConfig->ProjectStep, sizeof(int));

    ini_sha1.Update((BYTE*)&pEgfpsTestConfig->package_has_flash, sizeof(int));
    ini_sha1.Update((BYTE*)&pEgfpsTestConfig->check_flash_data_valid, sizeof(int));
    ini_sha1.Update((BYTE*)&pEgfpsTestConfig->write_flash_data, sizeof(int));
    ini_sha1.Update((BYTE*)&pEgfpsTestConfig->check_otp_data_valid, sizeof(int));
    ini_sha1.Update((BYTE*)&pEgfpsTestConfig->write_otp_data, sizeof(int));

    ini_sha1.Final();

    BYTE uDigest[20];
    ini_sha1.GetHash(uDigest);

    memcpy(pDigest, uDigest, 20);
}

#endif
