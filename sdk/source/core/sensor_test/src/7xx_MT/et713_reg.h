#define DEV_ID_0 0x00
#define DEV_ID_1 0x01
#define REV_ID 0x02
#define FID 0x03
#define SYS_STUS 0x04
#define INT_CTRL 0x05
#define CONF 0x06
#define OSC40M_CONF 0x07
#define IO_DS 0x08
#define BOOST_CP 0x0C
#define SCAN_CSR 0x0D
#define SCAN_FRAMES 0x0E
#define FB_CSR 0x0F
#define CMD_NOV_H 0x10
#define CMD_NOV_L 0x11
#define CMD_RRST_UP_H 0x12
#define CMD_RRST_UP_L 0x13
#define CMD_RRST_DN_H 0x14
#define CMD_RRST_DN_L 0x15
#define CMD_TG_UP_H 0x16
#define CMD_TG_UP_L 0x17
#define CMD_TG_DN_H 0x18
#define CMD_TG_DN_L 0x19
#define CMD_OFFNE_UP_H 0x1A
#define CMD_OFFNE_UP_L 0x1B
#define CMD_OFFNE_DN_H 0x1C
#define CMD_OFFNE_DN_L 0x1D
#define CMD_RRST_DN2_H 0x1E
#define CMD_RRST_DN2_L 0x1F
#define CMD_SHR_UP_H 0x20
#define CMD_SHR_UP_L 0x21
#define CMD_SHR_DN_H 0x22
#define CMD_SHR_DN_L 0x23
#define CMD_SHS_UP_H 0x24
#define CMD_SHS_UP_L 0x25
#define CMD_SHS_DN_H 0x26
#define CMD_SHS_DN_L 0x27
#define CMD_READOUT_H 0x28
#define CMD_READOUT_L 0x29
#define EXP_TIME_SW_H 0x2C
#define EXP_TIME_SW_L 0x2D
#define EXP_TIME_MTY_H 0x2E
#define EXP_TIME_MTY_L 0x2F
#define AE_TARGET_H 0x30
#define AE_TARGET_L 0x31
#define AE_V0_H 0x32
#define AE_V0_L 0x33
#define AE_RESULT_H 0x34
#define AE_RESULT_L 0x35
#define AE_AVG_H 0x36
#define AE_AVG_L 0x37
#define PGA_CSR 0x38
#define DARK_ADJ_L 0x39
#define DARK_ADJ_H 0x3A
#define PXL_CTRL0 0x3B
#define PXL_CTRL1 0x3C
#define HDR_CSR 0x3D
#define HDR_GAIN_H 0x3E
#define HDR_GAIN_L 0x3F
#define PWR_CTRL0 0x40
#define PWR_CTRL1 0x41
#define PWR_CTRL2 0x42
#define PWR_CTRL3 0x43
#define VCM_CTRL 0x44
#define FOD_CYCLE_H 0x45
#define FOD_CYCLE_L 0x46
#define FOD_CSR 0x47
#define DAC_CSR 0x48
#define DAC_CLK_DIV 0x49
#define DAC_FOD_H 0x4A
#define DAC_FOD_L 0x4B
#define DAC_OUT 0x4C
#define TS_DOUT 0x4D
#define STAT_MEAN_H 0x60
#define STAT_MEAN_L 0x61
#define STAT_MAX_H 0x62
#define STAT_MAX_L 0x63
#define STAT_MIN_H 0x64
#define STAT_MIN_L 0x65
#define HSTG_COL_STR 0x66
#define HSTG_COL_END 0x67
#define HSTG_ROW_STR 0x68
#define HSTG_ROW_END 0x69
#define EF_CSR 0xD0
#define REG_KEY 0xDF
#define RST_CTRL 0xE0
#define TEST_ADC 0xE1
#define TEST_PAT_H 0xE2
#define TEST_PAT_L 0xE3
#define FB_RST 0xE4
#define MBIST_SET 0xE5
#define MBIST_DAT 0xE6
#define TEST_CTRL 0xF4
#define CADDR_PHS 0xF5
#define TEST_MODE 0xF6
#define DAC_DATA_H 0xF7
#define DAC_DATA_L 0xF8
#define DEBUG0 0xF9
#define DEBUG1 0xFA
#define PAD_TEST_CSR 0xFB
#define PIX_ID 0xFC

#define SW_RESET 0xE0

#define MAX_REG_ADDR 0xFC
#define REG_NUM 101

#if defined(WIN32)

unsigned short reg_addr[REG_NUM] = {
    0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x0C, 0x0D, 0x0E, 0x0F, 0x10, 0x11,
    0x12, 0x13, 0x14, 0x15, 0x16, 0x17, 0x18, 0x19, 0x1A, 0x1B, 0x1C, 0x1D, 0x1E, 0x1F, 0x20,
    0x21, 0x22, 0x23, 0x24, 0x25, 0x26, 0x27, 0x28, 0x29, 0x2C, 0x2D, 0x2E, 0x2F, 0x30, 0x31,
    0x32, 0x33, 0x34, 0x35, 0x36, 0x37, 0x38, 0x39, 0x3A, 0x3B, 0x3C, 0x3D, 0x3E, 0x3f, 0x40,
    0x41, 0x42, 0x43, 0x44, 0x45, 0x46, 0x47, 0x48, 0x49, 0x4A, 0x4B, 0x4C, 0x4D, 0x60, 0x61,
    0x62, 0x63, 0x64, 0x65, 0x66, 0x67, 0x68, 0x69, 0xD0, 0xDF, 0xE0, 0xE1, 0xE2, 0xE3, 0xE4,
    0xE5, 0xE6, 0xF4, 0xF5, 0xF6, 0xF7, 0xF8, 0xF9, 0xFA, 0xFB, 0xFC};

char reg_name[REG_NUM][50] = {
    "DEV_ID0       ", "DEV_ID1       ", "REV_ID        ", "FID           ", "SYS_STUS      ",
    "INT_CTRL      ", "CONF          ", "OSC40M_CONF   ", "IO_DS         ", "BOOST_CP      ",
    "SCAN_CSR      ", "SCAN_FRAMES   ", "FB_CSR        ", "CMD_NOV_H     ", "CMD_NOV_L     ",
    "CMD_RRST_UP_H ", "CMD_RRST_UP_L ", "CMD_RRST_DN_H ", "CMD_RRST_DN_L ", "CMD_TG_UP_H   ",
    "CMD_TG_UP_L   ", "CMD_TG_DN_H   ", "CMD_TG_DN_L   ", "CMD_OFFNE_UP_H", "CMD_OFFNE_UP_L",
    "CMD_OFFNE_DN_H", "CMD_OFFNE_DN_L", "CMD_RRST_DN2_H", "CMD_RRST_DN2_L", "CMD_SHR_UP_H  ",
    "CMD_SHR_UP_L  ", "CMD_SHR_DN_H  ", "CMD_SHR_DN_L  ", "CMD_SHS_UP_H  ", "CMD_SHS_UP_L  ",
    "CMD_SHS_DN_H  ", "CMD_SHS_DN_L  ", "CMD_READOUT_H ", "CMD_READOUT_L ", "EXP_TIME_SW_H ",
    "EXP_TIME_SW_L ", "EXP_TIME_MTY_H", "EXP_TIME_MTY_L", "AE_TARGET_H   ", "AE_TARGET_L   ",
    "AE_V0_H       ", "AE_V0_L       ", "AE_RESULT_H   ", "AE_RESULT_L   ", "AE_AVG_H      ",
    "AE_AVG_L      ", "PGA_CSR       ", "DARK_ADJ_L    ", "DARK_ADJ_H    ", "PXL_CTRL0     ",
    "PXL_CTRL1     ", "HDR_CSR       ", "HDR_GAIN_H    ", "HDR_GAIN_L    ", "PWR_CTRL0     ",
    "PWR_CTRL1     ", "PWR_CTRL2     ", "PWR_CTRL3     ", "VCM_CTRL      ", "FOD_CYCLE_H   ",
    "FOD_CYCLE_L   ", "FOD_CSR       ", "DAC_CSR       ", "DAC_CLK_DIV   ", "DAC_FOD_H     ",
    "DAC_FOD_L     ", "DAC_OUT       ", "TS_DOUT       ", "STAT_MEAN_H   ", "STAT_MEAN_L   ",
    "STAT_MAX_H    ", "STAT_MAX_L    ", "STAT_MIN_H    ", "STAT_MIN_L    ", "HSTG_COL_STR  ",
    "HSTG_COL_END  ", "HSTG_ROW_STR  ", "HSTG_ROW_END  ", "EF_CSR        ", "REG_KEY       ",
    "RST_CTRL      ", "TEST_ADC      ", "TEST_PAT_H    ", "TEST_PAT_L    ", "FB_RST        ",
    "MBIST_SET     ", "MBIST_DAT     ", "TEST_CTRL     ", "CADDR_PHS     ", "TEST_MODE     ",
    "DAC_DATA_H    ", "DAC_DATA_L    ", "DEBUG0        ", "DEBUG1        ", "PAD_TEST_CSR  ",
    "PIX_ID        "};
#endif
