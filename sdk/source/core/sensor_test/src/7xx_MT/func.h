#ifndef _FUNC_H
#define _FUNC_H

#include "egis_fp_sensor_test.h"

unsigned int htoi(char* hex);
unsigned char h2toc(char* hex);
unsigned short h4tous(char* hex);

char* pPath2FileName(char* pPath);
void vPath2Path(char* pPath);

void sprintf_round_down(char* outbuf, double value, int dec_point_num);

unsigned char ucFindOtsuTh(unsigned char* pPicture, int width, int height, int* pnB, int* pnF,
                           unsigned char* pmB, unsigned char* pmF, unsigned char* pDirty);

void vCharacterizeImage(unsigned char char_point, double char_value, int width, int height,
                        unsigned char* pImage);

void vCalculateSoftStatistics(unsigned char* pMean, unsigned char* pMax, unsigned char* pMin,
                              int width, int height, unsigned char* pImage,
                              unsigned char* pDitryDots);

void vCalculateSoftStatistics16(unsigned short* pMean, unsigned short* pMax, unsigned short* pMin,
                                int width, int height, unsigned short* pImage,
                                unsigned char* pDitryDots);

void vCalculateCenterAverage16(unsigned short* pMean, int width, int height, unsigned short* pImage,
                               unsigned char* pDitryDots, int CenterX, int CenterY, int WindowSize);

void vCalculateScaleRange(int img_size, unsigned short sw_max, unsigned short* pMax,
                          unsigned short* pMin, unsigned short* pMiddle, int* pAccumulatedPxl,
                          int min_pxl_th, unsigned int* pHistogram8, unsigned short* pImage);

#if defined(WIN32)
void vScaleImage16ToImage8(int width, int height, unsigned char* pDirtyDots,
                           unsigned short* pImage16, unsigned char* pImage8,
                           BOOL bImageIsIntegrated = FALSE, unsigned char* pSignal8 = NULL,
                           unsigned short* pSignal16 = NULL);
#elif defined(RBS_SDK_USE)
void vScaleImage16ToImage8(int width, int height, unsigned char* pDirtyDots,
                           unsigned short* pImage16, unsigned char* pImage8,
                           BOOL bImageIsIntegrated, unsigned char* pSignal8,
                           unsigned short* pSignal16);
#endif

void vPixelBinning16(unsigned short* pImgIn, unsigned short* pImgOut, int width, int height);

void vEvaluateImage16(ImgTestCriteria* pImgTestCriteria, ImgTestResult* pImgTestResult,
                      BOOL* pTooManyDirtyDots, unsigned char* pDirtyDots,
                      Image_Valid_Area* pScanArea, unsigned short* pImage, int width, int height,
                      unsigned char option);

void vEvaluateImage8(ImgTestCriteria* pImgTestCriteria, ImgTestResult* pImgTestResult,
                     BOOL* pTooManyDirtyDots, unsigned char* pDirtyDots,
                     Image_Valid_Area* pScanArea, unsigned char* pImage, int width, int height,
                     unsigned char option);

void LensCentroid(UCHAR* a_Image, USHORT a_Width, USHORT a_Height, UINT* x, UINT* y);

void QueryExecutionTimeInit();
int QueryExecutionTimeUs();
double QueryExecutionTimeMs();

#endif