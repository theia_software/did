#ifndef __EGIS_EEPROM_DEFINITION_H__
#define __EGIS_EEPROM_DEFINITION_H__

#include "type_definition.h"

#define EEPROM_VERSION_V1
#define CHECKSUM_SIZE 2
#define OTP_CHECKSUM_SIZE 1
#define EEPROM_MAIN_ADDR 0x0
#define EEPROM_PGA_GAIN_ADDR 0x14
#define EEPROM_PHYSICAL_SIZE 0x5E
#define EEPROM_BLOCK_SIZE EEPROM_PHYSICAL_SIZE
#define EEPROM_CALIBRATION_VERSION_INFO_VALUE 1
#define EEPROM_RETRY_COUNTV1_LATEST_DATE_SIZE 4

//
// EEPROM UID
//
#define UID_SIZE 9

struct eeprom_uid_t {
    BYTE eeprom_version;
    BYTE uid[UID_SIZE];
    BYTE check_sum[CHECKSUM_SIZE];
};

/*
 * EEPROM v1
 */
/*
0x00 EEPROM Version
0x01 product_id[0] = 0x45; //E
0x02 product_id[1] = 0x54; //T
0x03 product_id[2] = 0x33; //3
0x04 product_id[3] = 0x32; //2
0x05 product_id[4] = 0x30; //0
0x06 Checksum[15..8] for data in address 0x00 to 0x05
0x07 Checksum[7..0] for data in address 0x00 to 0x05
0x08 Production Plant + Project Step
0x09 Calibration Date
0x0A Calibration Date
0x0B Calibration Date
0x0C Calibration Date
0x0D Program Version
0x0E Library Version
0x0F Configuration Version
0x10 MT Test Result
0x11 Checksum[15..8] for data in address 0x08 to 0x10
0x12 Checksum[7..0] for data in address 0x08 to 0x10
0x13 Calibration Version
0x14 HW sensor Gain
0x15 HW sensor DC offset
0x16 HW Sensor CDS out offset
0x17 HW detection gain
0x18 HW detection DC offset
0x19 HW detection CDS out offset
0x1A HW detection threshold
0x1B Finger-On threshold
0x1C Finger-On loose threshold
0x1D Finger-On stable threshold
0x1E Finger-Off threshold
0x1F Background average
0x20 Delta Row[0]
0x21 Delta Row[1]
0x22 Delta Row[2]
0x23 Delta Row[54]
0x24 Delta Row[55]
0x25 Delta Row[56]
0x26 Reserved
0x27 Reserved
0x28 Reserved
0x29 Reserved
0x2A Reserved
0x2B Reserved
0x2C Reserved
0x2D Reserved
0x2E Reserved
0x2F Reserved
0x30 Reserved
0x31 Checksum[15..8] for data in 0x13 to 0x30
0x32 Checksum[7..0] for data in 0x13 to 0x30
0x33 Unused
0x34 Unused
0x35 Unused
0x36 Unused
0x37 Unused
0x38 Checksum[15..8] for data in 0x3A to 0x3F
0x39 Checksum[7..0] for data in 0x3A to 0x3F
0x3A Pre-programmed UID
0x3B Pre-programmed UID
0x3C Pre-programmed UID
0x3D Pre-programmed UID
0x3E Pre-programmed UID
0x3F Pre-programmed UID
*/
#define V2_PRODUCT_ID_SIZE 5
#define V2_CALIBRATION_DATE_SIZE 8
#define V2_LATEST_TEST_DATE_SIZE 4
#define V2_EXPOSURE_TIME_IN_MT_SIZE 4
#define V2_MTF_BLOCK_SIZE 18
#define V2_CENTROID_X_SIZE 2
#define V2_CENTROID_Y_SIZE 2
#define V2_RESERVED_SIZE 3940
#define V2_RANDOM_NUMBER_SIZE 2
#define V2_BAD_PIXEL_SIZE 2

#define V1_TEST_DATA_SIZE 15
#define V1_RESERVE_SIZZE 997

//#define INLINE_BKG_IMG_SIZE 63656
/* FIXME : Need to support 711 and 713 */
#define INLINE_BKG_IMG_SIZE 80000
#define INLINE_EXPOSURE_TIME_SIZE 2
#define INLINE_RESERVE_SIZE 3992

struct eeprom_v2_id {
    BYTE product_id[V2_PRODUCT_ID_SIZE];
    BYTE check_sum[CHECKSUM_SIZE];
};

struct eeprom_v2_calibration {
    BYTE calibration_method;
    BYTE exposure_time_in_mt[V2_EXPOSURE_TIME_IN_MT_SIZE];
    BYTE centroid_x[V2_CENTROID_X_SIZE];
    BYTE centroid_y[V2_CENTROID_Y_SIZE];
    BYTE mtf_block[V2_MTF_BLOCK_SIZE];
    BYTE bad_pixel[V2_BAD_PIXEL_SIZE];
    BYTE reserved[V2_RESERVED_SIZE];
    BYTE check_sum[CHECKSUM_SIZE];
};

struct eeprom_v2_bin_info {
    BYTE pp_ps;
    BYTE latest_test_date[V2_LATEST_TEST_DATE_SIZE];
    BYTE program_version;
    BYTE library_version;
    BYTE configuration_version;
    BYTE mt_test_result;
    BYTE check_sum[CHECKSUM_SIZE];
};

struct eeprom_v2 {
    struct eeprom_uid_t uid;
    struct eeprom_v2_id id;
    struct eeprom_v2_bin_info bin_info;
    struct eeprom_v2_calibration calibration_data;
};
/*
 * MT EEPROM v2 END
 */

struct eeprom_v1_data {
    BYTE uid[UID_SIZE];
    BYTE version;
    BYTE mt_test_data[V1_TEST_DATA_SIZE];
    BYTE reserve[V1_RESERVE_SIZZE];
    BYTE check_sum[CHECKSUM_SIZE];
};

struct eeprom_v1 {
    struct eeprom_v1_data mt_data;
};
/*
 * MT EEPROM v1 END
 */

struct eeprom_inline_data {
    BYTE exposure_time_in_inline[INLINE_EXPOSURE_TIME_SIZE];
    BYTE finger_effective_area;
    BYTE lens_m_factor;
    BYTE hw_int;
    BYTE gain_value;
    BYTE reserve[INLINE_RESERVE_SIZE];
    BYTE check_sum[CHECKSUM_SIZE];
};

struct eeprom_inline {
    struct eeprom_inline_data inline_data;
};

struct egfps_otp_data {
    unsigned char mt_result;
    unsigned char software_bin;
    unsigned char production_plant;
    unsigned char program_version;
    unsigned char config_version;
    unsigned char test_data[4];
    unsigned char exposure_time_h;
    unsigned char exposure_time_l;
    unsigned char centroid_x1;
    unsigned char centroid_y1;
    unsigned char centroid_x2;
    unsigned char centroid_y2;
    unsigned char centroid_x3;
    unsigned char centroid_y3;
    unsigned char centroid_x4;
    unsigned char centroid_y4;

    unsigned char reserved[3];
    unsigned char check_sum;
    unsigned char uid[9];
};
// #pragma pack(1)
// struct inner_field {
//         UINT length;
//         UINT offset;
//         USHORT check_sum;
// };

// struct egis_metadata_header_struct {
//         BYTE version;
//         UINT size;
// };

// struct egis_metadata_v1_struct {
//         BYTE version;
//         UINT size;
//         USHORT check_sum;
//         BYTE uid[20];
//         FLOAT exposure_time_1;
// 		FLOAT exposure_time_2;
//         BYTE hw_integ_count_1;
// 		BYTE hw_integ_count_2;
//         BYTE gain_value_1;
//         BYTE gain_value_2;
//         FLOAT finger_effective_area_x1;
//         FLOAT finger_effective_area_x2;
//         FLOAT finger_effective_area_y1;
//         FLOAT finger_effective_area_y2;
//         FLOAT lens_m_factor;
//         struct inner_field inline_result;
//         struct inner_field algorithm_parameter;
//         struct inner_field bkg1;
//         struct inner_field bkg2;
//         struct inner_field bkg3;
//         struct inner_field bds_db_offset;
// };

// struct inline_data_result {
// 	BYTE inline_version;
// 	FLOAT exposure_time_1;
// 	FLOAT exposure_time_2;
// 	FLOAT finger_effective_area_x1;
//     FLOAT finger_effective_area_x2;
//     FLOAT finger_effective_area_y1;
//     FLOAT finger_effective_area_y2;
//     FLOAT lens_m_factor;
// 	BYTE hw_integ_count_1;
// 	BYTE hw_integ_count_2;
//     BYTE gain_value_1;
//     BYTE gain_value_2;
// };

// struct inline_data {
// 	struct inline_data_result inline_result;
// 	BYTE bkg_img1[INLINE_BKG_IMG_SIZE];
// 	BYTE bkg_img2[INLINE_BKG_IMG_SIZE];
// };

// #pragma pack()

#endif /*__SENSOR_TEST_CONTROL_H__*/
