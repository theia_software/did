#if defined(WIN32)
#define FUNCTION_PREFIX CSensor::
#elif defined(RBS_SDK_USE)
#include "Sensor.h"
#include "et713_reg.h"
#define FUNCTION_PREFIX
extern CSensor* g_sensor_ptr;
#define this g_sensor_ptr
#endif

void FUNCTION_PREFIX vRegInit() {
    int i;
    for (i = 0; i <= MAX_REG_ADDR; i++) {
        this->sensor_reg_all[i].RegAddr = i;
        this->sensor_reg_all[i].RegType = REG_TYPE_NONE;
        this->sensor_reg_all[i].RegValue = 0x00;
        this->sensor_reg_all[i].RegValueDefault = 0x00;
        this->sensor_reg_all[i].RegValueDefaultAlt = 0x00;
        this->sensor_reg_all[i].RegValueMask = 0xFF;
    }

    // ET713 Register Value Init
    if (this->target_board_type == TESTER_BOARD_ARDUINO) {
        this->sensor_reg_all[0x00].RegAddr = 0x00;
        this->sensor_reg_all[0x00].RegType = REG_TYPE_READ;
        this->sensor_reg_all[0x00].RegValueDefault = 0x07;
        this->sensor_reg_all[0x00].RegValueMask = 0xFF;

        this->sensor_reg_all[0x01].RegAddr = 0x01;
        this->sensor_reg_all[0x01].RegType = REG_TYPE_READ;
        this->sensor_reg_all[0x01].RegValueDefault = 0x0D;
        this->sensor_reg_all[0x01].RegValueMask = 0xFF;

        // Discard Sensor Revision
        this->sensor_reg_all[0x02].RegAddr = 0x02;
        this->sensor_reg_all[0x02].RegType = REG_TYPE_DISCARD;
        this->sensor_reg_all[0x02].RegValueDefault = 0x00;
        this->sensor_reg_all[0x02].RegValueMask = 0xFF;

        this->sensor_reg_all[0x03].RegAddr = 0x03;
        this->sensor_reg_all[0x03].RegType = REG_TYPE_READ;
        this->sensor_reg_all[0x03].RegValueDefault = 0x00;
        this->sensor_reg_all[0x03].RegValueMask = 0xFF;

        this->sensor_reg_all[0x04].RegAddr = 0x04;
        this->sensor_reg_all[0x04].RegType = REG_TYPE_READ;
        this->sensor_reg_all[0x04].RegValueDefault = 0xC2;
        this->sensor_reg_all[0x04].RegValueMask = 0xFF;

        this->sensor_reg_all[0x05].RegAddr = 0x05;
        this->sensor_reg_all[0x05].RegType = REG_TYPE_READ;
        this->sensor_reg_all[0x05].RegValueDefault = 0x00;
        this->sensor_reg_all[0x05].RegValueMask = 0xFF;

        this->sensor_reg_all[0x06].RegAddr = 0x06;
        this->sensor_reg_all[0x06].RegType = REG_TYPE_READ | REG_TYPE_WRITE;
        this->sensor_reg_all[0x06].RegValueDefault = 0x01;
        this->sensor_reg_all[0x06].RegValueMask = 0xFF;

        this->sensor_reg_all[0x07].RegAddr = 0x07;
        this->sensor_reg_all[0x07].RegType = REG_TYPE_READ | REG_TYPE_WRITE;
        this->sensor_reg_all[0x07].RegValueDefault = 0x0C;
        this->sensor_reg_all[0x07].RegValueMask = 0xFF;

        this->sensor_reg_all[0x08].RegAddr = 0x08;
        this->sensor_reg_all[0x08].RegType = REG_TYPE_READ | REG_TYPE_WRITE;
        this->sensor_reg_all[0x08].RegValueDefault = 0x00;
        this->sensor_reg_all[0x08].RegValueMask = 0x01;

        this->sensor_reg_all[0x0C].RegAddr = 0x0C;
        this->sensor_reg_all[0x0C].RegType = REG_TYPE_READ | REG_TYPE_WRITE;
        this->sensor_reg_all[0x0C].RegValueDefault = 0xC1;
        this->sensor_reg_all[0x0C].RegValueMask = 0xF1;

        this->sensor_reg_all[0x0D].RegAddr = 0x0D;
        this->sensor_reg_all[0x0D].RegType = REG_TYPE_READ | REG_TYPE_WRITE;
        this->sensor_reg_all[0x0D].RegValueDefault = 0x00;
        this->sensor_reg_all[0x0D].RegValueMask = 0xFF;

        this->sensor_reg_all[0x0E].RegAddr = 0x0E;
        this->sensor_reg_all[0x0E].RegType = REG_TYPE_READ | REG_TYPE_WRITE;
        this->sensor_reg_all[0x0E].RegValueDefault = 0x00;
        this->sensor_reg_all[0x0E].RegValueMask = 0xFF;

        this->sensor_reg_all[0x0F].RegAddr = 0x0F;
        this->sensor_reg_all[0x0F].RegType = REG_TYPE_READ;
        this->sensor_reg_all[0x0F].RegValueDefault = 0x00;
        this->sensor_reg_all[0x0F].RegValueMask = 0xFF;

        this->sensor_reg_all[0x10].RegAddr = 0x10;
        this->sensor_reg_all[0x10].RegType = REG_TYPE_READ | REG_TYPE_WRITE;
        this->sensor_reg_all[0x10].RegValueDefault = 0x01;
        this->sensor_reg_all[0x10].RegValueMask = 0xFF;

        this->sensor_reg_all[0x11].RegAddr = 0x11;
        this->sensor_reg_all[0x11].RegType = REG_TYPE_READ | REG_TYPE_WRITE;
        this->sensor_reg_all[0x11].RegValueDefault = 0x77;
        this->sensor_reg_all[0x11].RegValueMask = 0xFF;

        this->sensor_reg_all[0x12].RegAddr = 0x12;
        this->sensor_reg_all[0x12].RegType = REG_TYPE_READ | REG_TYPE_WRITE;
        this->sensor_reg_all[0x12].RegValueDefault = 0x01;
        this->sensor_reg_all[0x12].RegValueMask = 0xFF;

        this->sensor_reg_all[0x13].RegAddr = 0x13;
        this->sensor_reg_all[0x13].RegType = REG_TYPE_READ | REG_TYPE_WRITE;
        this->sensor_reg_all[0x13].RegValueDefault = 0x75;
        this->sensor_reg_all[0x13].RegValueMask = 0xFF;

        this->sensor_reg_all[0x14].RegAddr = 0x14;
        this->sensor_reg_all[0x14].RegType = REG_TYPE_READ | REG_TYPE_WRITE;
        this->sensor_reg_all[0x14].RegValueDefault = 0x00;
        this->sensor_reg_all[0x14].RegValueMask = 0xFF;

        this->sensor_reg_all[0x15].RegAddr = 0x15;
        this->sensor_reg_all[0x15].RegType = REG_TYPE_READ | REG_TYPE_WRITE;
        this->sensor_reg_all[0x15].RegValueDefault = 0x01;
        this->sensor_reg_all[0x15].RegValueMask = 0xFF;

        this->sensor_reg_all[0x16].RegAddr = 0x16;
        this->sensor_reg_all[0x16].RegType = REG_TYPE_READ | REG_TYPE_WRITE;
        this->sensor_reg_all[0x16].RegValueDefault = 0x00;
        this->sensor_reg_all[0x16].RegValueMask = 0xFF;

        this->sensor_reg_all[0x17].RegAddr = 0x17;
        this->sensor_reg_all[0x17].RegType = REG_TYPE_READ | REG_TYPE_WRITE;
        this->sensor_reg_all[0x17].RegValueDefault = 0x7B;
        this->sensor_reg_all[0x17].RegValueMask = 0xFF;

        this->sensor_reg_all[0x18].RegAddr = 0x18;
        this->sensor_reg_all[0x18].RegType = REG_TYPE_READ | REG_TYPE_WRITE;
        this->sensor_reg_all[0x18].RegValueDefault = 0x00;
        this->sensor_reg_all[0x18].RegValueMask = 0xFF;

        this->sensor_reg_all[0x19].RegAddr = 0x19;
        this->sensor_reg_all[0x19].RegType = REG_TYPE_READ | REG_TYPE_WRITE;
        this->sensor_reg_all[0x19].RegValueDefault = 0xEA;
        this->sensor_reg_all[0x19].RegValueMask = 0xFF;

        this->sensor_reg_all[0x1A].RegAddr = 0x1A;
        this->sensor_reg_all[0x1A].RegType = REG_TYPE_READ | REG_TYPE_WRITE;
        this->sensor_reg_all[0x1A].RegValueDefault = 0x01;
        this->sensor_reg_all[0x1A].RegValueMask = 0xFF;

        this->sensor_reg_all[0x1B].RegAddr = 0x1B;
        this->sensor_reg_all[0x1B].RegType = REG_TYPE_READ | REG_TYPE_WRITE;
        this->sensor_reg_all[0x1B].RegValueDefault = 0x8F;
        this->sensor_reg_all[0x1B].RegValueMask = 0xFF;

        this->sensor_reg_all[0x1C].RegAddr = 0x1C;
        this->sensor_reg_all[0x1C].RegType = REG_TYPE_READ | REG_TYPE_WRITE;
        this->sensor_reg_all[0x1C].RegValueDefault = 0x01;
        this->sensor_reg_all[0x1C].RegValueMask = 0xFF;

        this->sensor_reg_all[0x1D].RegAddr = 0x1D;
        this->sensor_reg_all[0x1D].RegType = REG_TYPE_READ | REG_TYPE_WRITE;
        this->sensor_reg_all[0x1D].RegValueDefault = 0xA8;
        this->sensor_reg_all[0x1D].RegValueMask = 0xFF;

        this->sensor_reg_all[0x1E].RegAddr = 0x1E;
        this->sensor_reg_all[0x1E].RegType = REG_TYPE_READ | REG_TYPE_WRITE;
        this->sensor_reg_all[0x1E].RegValueDefault = 0x02;
        this->sensor_reg_all[0x1E].RegValueMask = 0xFF;

        this->sensor_reg_all[0x1F].RegAddr = 0x1F;
        this->sensor_reg_all[0x1F].RegType = REG_TYPE_READ | REG_TYPE_WRITE;
        this->sensor_reg_all[0x1F].RegValueDefault = 0xC3;
        this->sensor_reg_all[0x1F].RegValueMask = 0xFF;

        this->sensor_reg_all[0x20].RegAddr = 0x20;
        this->sensor_reg_all[0x20].RegType = REG_TYPE_READ | REG_TYPE_WRITE;
        this->sensor_reg_all[0x20].RegValueDefault = 0x00;
        this->sensor_reg_all[0x20].RegValueMask = 0xFF;

        this->sensor_reg_all[0x21].RegAddr = 0x21;
        this->sensor_reg_all[0x21].RegType = REG_TYPE_READ | REG_TYPE_WRITE;
        this->sensor_reg_all[0x21].RegValueDefault = 0x02;
        this->sensor_reg_all[0x21].RegValueMask = 0xFF;

        this->sensor_reg_all[0x22].RegAddr = 0x22;
        this->sensor_reg_all[0x22].RegType = REG_TYPE_READ | REG_TYPE_WRITE;
        this->sensor_reg_all[0x22].RegValueDefault = 0x00;
        this->sensor_reg_all[0x22].RegValueMask = 0xFF;

        this->sensor_reg_all[0x23].RegAddr = 0x23;
        this->sensor_reg_all[0x23].RegType = REG_TYPE_READ | REG_TYPE_WRITE;
        this->sensor_reg_all[0x23].RegValueDefault = 0x7A;
        this->sensor_reg_all[0x23].RegValueMask = 0xFF;

        this->sensor_reg_all[0x24].RegAddr = 0x24;
        this->sensor_reg_all[0x24].RegType = REG_TYPE_READ | REG_TYPE_WRITE;
        this->sensor_reg_all[0x24].RegValueDefault = 0x00;
        this->sensor_reg_all[0x24].RegValueMask = 0xFF;

        this->sensor_reg_all[0x25].RegAddr = 0x25;
        this->sensor_reg_all[0x25].RegType = REG_TYPE_READ | REG_TYPE_WRITE;
        this->sensor_reg_all[0x25].RegValueDefault = 0x7B;
        this->sensor_reg_all[0x25].RegValueMask = 0xFF;

        this->sensor_reg_all[0x26].RegAddr = 0x26;
        this->sensor_reg_all[0x26].RegType = REG_TYPE_READ | REG_TYPE_WRITE;
        this->sensor_reg_all[0x26].RegValueDefault = 0x01;
        this->sensor_reg_all[0x26].RegValueMask = 0xFF;

        this->sensor_reg_all[0x27].RegAddr = 0x27;
        this->sensor_reg_all[0x27].RegType = REG_TYPE_READ | REG_TYPE_WRITE;
        this->sensor_reg_all[0x27].RegValueDefault = 0x6B;
        this->sensor_reg_all[0x27].RegValueMask = 0xFF;

        this->sensor_reg_all[0x28].RegAddr = 0x28;
        this->sensor_reg_all[0x28].RegType = REG_TYPE_READ | REG_TYPE_WRITE;
        this->sensor_reg_all[0x28].RegValueDefault = 0x03;
        this->sensor_reg_all[0x28].RegValueMask = 0xFF;

        this->sensor_reg_all[0x29].RegAddr = 0x29;
        this->sensor_reg_all[0x29].RegType = REG_TYPE_READ | REG_TYPE_WRITE;
        this->sensor_reg_all[0x29].RegValueDefault = 0xCA;
        this->sensor_reg_all[0x29].RegValueMask = 0xFF;

        this->sensor_reg_all[0x2C].RegAddr = 0x2C;
        this->sensor_reg_all[0x2C].RegType = REG_TYPE_READ | REG_TYPE_WRITE;
        this->sensor_reg_all[0x2C].RegValueDefault = 0x8A;
        this->sensor_reg_all[0x2C].RegValueMask = 0xFF;

        this->sensor_reg_all[0x2D].RegAddr = 0x2D;
        this->sensor_reg_all[0x2D].RegType = REG_TYPE_READ | REG_TYPE_WRITE;
        this->sensor_reg_all[0x2D].RegValueDefault = 0x00;
        this->sensor_reg_all[0x2D].RegValueMask = 0xFF;

        this->sensor_reg_all[0x2E].RegAddr = 0x2E;
        this->sensor_reg_all[0x2E].RegType = REG_TYPE_READ | REG_TYPE_WRITE;
        this->sensor_reg_all[0x2E].RegValueDefault = 0x1A;
        this->sensor_reg_all[0x2E].RegValueMask = 0xFF;

        this->sensor_reg_all[0x2F].RegAddr = 0x2F;
        this->sensor_reg_all[0x2F].RegType = REG_TYPE_READ | REG_TYPE_WRITE;
        this->sensor_reg_all[0x2F].RegValueDefault = 0x80;
        this->sensor_reg_all[0x2F].RegValueMask = 0xFF;

        this->sensor_reg_all[0x30].RegAddr = 0x30;
        this->sensor_reg_all[0x30].RegType = REG_TYPE_READ | REG_TYPE_WRITE;
        this->sensor_reg_all[0x30].RegValueDefault = 0x03;
        this->sensor_reg_all[0x30].RegValueMask = 0xFF;

        this->sensor_reg_all[0x31].RegAddr = 0x31;
        this->sensor_reg_all[0x31].RegType = REG_TYPE_READ | REG_TYPE_WRITE;
        this->sensor_reg_all[0x31].RegValueDefault = 0x00;
        this->sensor_reg_all[0x31].RegValueMask = 0xFF;

        this->sensor_reg_all[0x32].RegAddr = 0x32;
        this->sensor_reg_all[0x32].RegType = REG_TYPE_READ | REG_TYPE_WRITE;
        this->sensor_reg_all[0x32].RegValueDefault = 0x00;
        this->sensor_reg_all[0x32].RegValueMask = 0xFF;

        this->sensor_reg_all[0x33].RegAddr = 0x33;
        this->sensor_reg_all[0x33].RegType = REG_TYPE_READ | REG_TYPE_WRITE;
        this->sensor_reg_all[0x33].RegValueDefault = 0x00;
        this->sensor_reg_all[0x33].RegValueMask = 0xFF;

        this->sensor_reg_all[0x34].RegAddr = 0x34;
        this->sensor_reg_all[0x34].RegType = REG_TYPE_READ;
        this->sensor_reg_all[0x34].RegValueDefault = 0x00;
        this->sensor_reg_all[0x34].RegValueMask = 0xFF;

        this->sensor_reg_all[0x35].RegAddr = 0x35;
        this->sensor_reg_all[0x35].RegType = REG_TYPE_READ;
        this->sensor_reg_all[0x35].RegValueDefault = 0x00;
        this->sensor_reg_all[0x35].RegValueMask = 0xFF;

        this->sensor_reg_all[0x36].RegAddr = 0x36;
        this->sensor_reg_all[0x36].RegType = REG_TYPE_READ;
        this->sensor_reg_all[0x36].RegValueDefault = 0x00;
        this->sensor_reg_all[0x36].RegValueMask = 0xFF;

        this->sensor_reg_all[0x37].RegAddr = 0x37;
        this->sensor_reg_all[0x37].RegType = REG_TYPE_READ;
        this->sensor_reg_all[0x37].RegValueDefault = 0x00;
        this->sensor_reg_all[0x37].RegValueMask = 0xFF;

        this->sensor_reg_all[0x38].RegAddr = 0x38;
        this->sensor_reg_all[0x38].RegType = REG_TYPE_READ | REG_TYPE_WRITE;
        this->sensor_reg_all[0x38].RegValueDefault = 0x00;
        this->sensor_reg_all[0x38].RegValueMask = 0xFF;

        this->sensor_reg_all[0x39].RegAddr = 0x39;
        this->sensor_reg_all[0x39].RegType = REG_TYPE_READ | REG_TYPE_WRITE;
        this->sensor_reg_all[0x39].RegValueDefault = 0x00;
        this->sensor_reg_all[0x39].RegValueMask = 0xFF;

        this->sensor_reg_all[0x3A].RegAddr = 0x3A;
        this->sensor_reg_all[0x3A].RegType = REG_TYPE_READ | REG_TYPE_WRITE;
        this->sensor_reg_all[0x3A].RegValueDefault = 0x00;
        this->sensor_reg_all[0x3A].RegValueMask = 0xFF;

        this->sensor_reg_all[0x3B].RegAddr = 0x3B;
        this->sensor_reg_all[0x3B].RegType = REG_TYPE_READ | REG_TYPE_WRITE;
        this->sensor_reg_all[0x3B].RegValueDefault = 0x48;
        this->sensor_reg_all[0x3B].RegValueMask = 0xFF;

        this->sensor_reg_all[0x3C].RegAddr = 0x3C;
        this->sensor_reg_all[0x3C].RegType = REG_TYPE_READ | REG_TYPE_WRITE;
        this->sensor_reg_all[0x3C].RegValueDefault = 0x04;
        this->sensor_reg_all[0x3C].RegValueMask = 0xFF;

        this->sensor_reg_all[0x3D].RegAddr = 0x3D;
        this->sensor_reg_all[0x3D].RegType = REG_TYPE_READ | REG_TYPE_WRITE;
        this->sensor_reg_all[0x3D].RegValueDefault = 0x00;
        this->sensor_reg_all[0x3D].RegValueMask = 0xFF;

        this->sensor_reg_all[0x3E].RegAddr = 0x3E;
        this->sensor_reg_all[0x3E].RegType = REG_TYPE_READ | REG_TYPE_WRITE;
        this->sensor_reg_all[0x3E].RegValueDefault = 0x01;
        this->sensor_reg_all[0x3E].RegValueMask = 0xFF;

        this->sensor_reg_all[0x3F].RegAddr = 0x3F;
        this->sensor_reg_all[0x3F].RegType = REG_TYPE_READ | REG_TYPE_WRITE;
        this->sensor_reg_all[0x3F].RegValueDefault = 0x00;
        this->sensor_reg_all[0x3F].RegValueMask = 0xFF;

        this->sensor_reg_all[0x40].RegAddr = 0x40;
        this->sensor_reg_all[0x40].RegType = REG_TYPE_READ;
        this->sensor_reg_all[0x40].RegValueDefault = 0x40;
        this->sensor_reg_all[0x40].RegValueMask = 0xFF;

        this->sensor_reg_all[0x41].RegAddr = 0x41;
        this->sensor_reg_all[0x41].RegType = REG_TYPE_READ;
        this->sensor_reg_all[0x41].RegValueDefault = 0x20;
        this->sensor_reg_all[0x41].RegValueMask = 0xFF;

        this->sensor_reg_all[0x42].RegAddr = 0x42;
        this->sensor_reg_all[0x42].RegType = REG_TYPE_READ;
        this->sensor_reg_all[0x42].RegValueDefault = 0xF6;
        this->sensor_reg_all[0x42].RegValueMask = 0xFF;

        this->sensor_reg_all[0x43].RegAddr = 0x43;
        this->sensor_reg_all[0x43].RegType = REG_TYPE_READ;
        this->sensor_reg_all[0x43].RegValueDefault = 0x13;
        this->sensor_reg_all[0x43].RegValueMask = 0xFF;

        this->sensor_reg_all[0x44].RegAddr = 0x44;
        this->sensor_reg_all[0x44].RegType = REG_TYPE_READ | REG_TYPE_WRITE;
        this->sensor_reg_all[0x44].RegValueDefault = 0x60;
        this->sensor_reg_all[0x44].RegValueMask = 0xFF;

        this->sensor_reg_all[0x45].RegAddr = 0x45;
        this->sensor_reg_all[0x45].RegType = REG_TYPE_READ | REG_TYPE_WRITE;
        this->sensor_reg_all[0x45].RegValueDefault = 0x13;
        this->sensor_reg_all[0x45].RegValueMask = 0xFF;

        this->sensor_reg_all[0x46].RegAddr = 0x46;
        this->sensor_reg_all[0x46].RegType = REG_TYPE_READ | REG_TYPE_WRITE;
        this->sensor_reg_all[0x46].RegValueDefault = 0x88;
        this->sensor_reg_all[0x46].RegValueMask = 0xFF;

        this->sensor_reg_all[0x47].RegAddr = 0x47;
        this->sensor_reg_all[0x47].RegType = REG_TYPE_READ | REG_TYPE_WRITE;
        this->sensor_reg_all[0x47].RegValueDefault = 0x00;
        this->sensor_reg_all[0x47].RegValueMask = 0xFF;

        this->sensor_reg_all[0x48].RegAddr = 0x48;
        this->sensor_reg_all[0x48].RegType = REG_TYPE_READ | REG_TYPE_WRITE;
        this->sensor_reg_all[0x48].RegValueDefault = 0x00;
        this->sensor_reg_all[0x48].RegValueMask = 0xFF;

        this->sensor_reg_all[0x49].RegAddr = 0x49;
        this->sensor_reg_all[0x49].RegType = REG_TYPE_READ | REG_TYPE_WRITE;
        this->sensor_reg_all[0x49].RegValueDefault = 0x3F;
        this->sensor_reg_all[0x49].RegValueMask = 0xFF;

        this->sensor_reg_all[0x4A].RegAddr = 0x4A;
        this->sensor_reg_all[0x4A].RegType = REG_TYPE_READ | REG_TYPE_WRITE;
        this->sensor_reg_all[0x4A].RegValueDefault = 0x02;
        this->sensor_reg_all[0x4A].RegValueMask = 0xFF;

        this->sensor_reg_all[0x4B].RegAddr = 0x4B;
        this->sensor_reg_all[0x4B].RegType = REG_TYPE_READ | REG_TYPE_WRITE;
        this->sensor_reg_all[0x4B].RegValueDefault = 0x00;
        this->sensor_reg_all[0x4B].RegValueMask = 0xFF;

        this->sensor_reg_all[0x4C].RegAddr = 0x4C;
        this->sensor_reg_all[0x4C].RegType = REG_TYPE_READ;
        this->sensor_reg_all[0x4C].RegValueDefault = 0x00;
        this->sensor_reg_all[0x4C].RegValueMask = 0xFF;

        this->sensor_reg_all[0x4D].RegAddr = 0x4D;
        this->sensor_reg_all[0x4D].RegType = REG_TYPE_READ;
        this->sensor_reg_all[0x4D].RegValueDefault = 0x00;
        this->sensor_reg_all[0x4D].RegValueMask = 0xFF;

        this->sensor_reg_all[0x60].RegAddr = 0x60;
        this->sensor_reg_all[0x60].RegType = REG_TYPE_READ;
        this->sensor_reg_all[0x60].RegValueDefault = 0x00;
        this->sensor_reg_all[0x60].RegValueMask = 0xFF;

        this->sensor_reg_all[0x61].RegAddr = 0x61;
        this->sensor_reg_all[0x61].RegType = REG_TYPE_READ;
        this->sensor_reg_all[0x61].RegValueDefault = 0x00;
        this->sensor_reg_all[0x61].RegValueMask = 0xFF;

        this->sensor_reg_all[0x62].RegAddr = 0x62;
        this->sensor_reg_all[0x62].RegType = REG_TYPE_READ;
        this->sensor_reg_all[0x62].RegValueDefault = 0x00;
        this->sensor_reg_all[0x62].RegValueMask = 0xFF;

        this->sensor_reg_all[0x63].RegAddr = 0x63;
        this->sensor_reg_all[0x63].RegType = REG_TYPE_READ;
        this->sensor_reg_all[0x63].RegValueDefault = 0x00;
        this->sensor_reg_all[0x63].RegValueMask = 0xFF;

        this->sensor_reg_all[0x64].RegAddr = 0x64;
        this->sensor_reg_all[0x64].RegType = REG_TYPE_READ;
        this->sensor_reg_all[0x64].RegValueDefault = 0xFF;
        this->sensor_reg_all[0x64].RegValueMask = 0xFF;

        this->sensor_reg_all[0x65].RegAddr = 0x65;
        this->sensor_reg_all[0x65].RegType = REG_TYPE_READ;
        this->sensor_reg_all[0x65].RegValueDefault = 0xFF;
        this->sensor_reg_all[0x65].RegValueMask = 0xFF;

        this->sensor_reg_all[0x66].RegAddr = 0x66;
        this->sensor_reg_all[0x66].RegType = REG_TYPE_READ | REG_TYPE_WRITE;
        this->sensor_reg_all[0x66].RegValueDefault = 0x01;
        this->sensor_reg_all[0x66].RegValueMask = 0xFF;

        this->sensor_reg_all[0x67].RegAddr = 0x67;
        this->sensor_reg_all[0x67].RegType = REG_TYPE_READ;
        this->sensor_reg_all[0x67].RegValueDefault = 0xC8;
        this->sensor_reg_all[0x67].RegValueMask = 0xFF;

        this->sensor_reg_all[0x68].RegAddr = 0x68;
        this->sensor_reg_all[0x68].RegType = REG_TYPE_READ | REG_TYPE_WRITE;
        this->sensor_reg_all[0x68].RegValueDefault = 0x00;
        this->sensor_reg_all[0x68].RegValueMask = 0xFF;

        this->sensor_reg_all[0x69].RegAddr = 0x69;
        this->sensor_reg_all[0x69].RegType = REG_TYPE_READ;
        this->sensor_reg_all[0x69].RegValueDefault = 0xC7;
        this->sensor_reg_all[0x69].RegValueMask = 0xFF;

        this->sensor_reg_all[0xD0].RegAddr = 0xD0;
        this->sensor_reg_all[0xD0].RegType = REG_TYPE_READ;
        this->sensor_reg_all[0xD0].RegValueDefault = 0x00;
        this->sensor_reg_all[0xD0].RegValueMask = 0xFF;

        this->sensor_reg_all[0xDF].RegAddr = 0xDF;
        this->sensor_reg_all[0xDF].RegType = REG_TYPE_READ | REG_TYPE_WRITE;
        this->sensor_reg_all[0xDF].RegValueDefault = 0x00;
        this->sensor_reg_all[0xDF].RegValueMask = 0xFF;

        this->sensor_reg_all[0xE0].RegAddr = 0xE0;
        this->sensor_reg_all[0xE0].RegType = REG_TYPE_READ;
        this->sensor_reg_all[0xE0].RegValueDefault = 0x00;
        this->sensor_reg_all[0xE0].RegValueMask = 0xFF;

        this->sensor_reg_all[0xE1].RegAddr = 0xE1;
        this->sensor_reg_all[0xE1].RegType = REG_TYPE_READ;
        this->sensor_reg_all[0xE1].RegValueDefault = 0x00;
        this->sensor_reg_all[0xE1].RegValueMask = 0xFF;

        this->sensor_reg_all[0xE2].RegAddr = 0xE2;
        this->sensor_reg_all[0xE2].RegType = REG_TYPE_READ;
        this->sensor_reg_all[0xE2].RegValueDefault = 0x00;
        this->sensor_reg_all[0xE2].RegValueMask = 0xFF;

        this->sensor_reg_all[0xE3].RegAddr = 0xE3;
        this->sensor_reg_all[0xE3].RegType = REG_TYPE_READ;
        this->sensor_reg_all[0xE3].RegValueDefault = 0x00;
        this->sensor_reg_all[0xE3].RegValueMask = 0xFF;

        this->sensor_reg_all[0xE4].RegAddr = 0xE4;
        this->sensor_reg_all[0xE4].RegType = REG_TYPE_READ;
        this->sensor_reg_all[0xE4].RegValueDefault = 0x00;
        this->sensor_reg_all[0xE4].RegValueMask = 0xFF;

        this->sensor_reg_all[0xE5].RegAddr = 0xE5;
        this->sensor_reg_all[0xE5].RegType = REG_TYPE_READ;
        this->sensor_reg_all[0xE5].RegValueDefault = 0x00;
        this->sensor_reg_all[0xE5].RegValueMask = 0xFF;

        this->sensor_reg_all[0xE6].RegAddr = 0xE6;
        this->sensor_reg_all[0xE6].RegType = REG_TYPE_READ;
        this->sensor_reg_all[0xE6].RegValueDefault = 0x00;
        this->sensor_reg_all[0xE6].RegValueMask = 0xFF;

        this->sensor_reg_all[0xF4].RegAddr = 0xF4;
        this->sensor_reg_all[0xF4].RegType = REG_TYPE_READ;
        this->sensor_reg_all[0xF4].RegValueDefault = 0x00;
        this->sensor_reg_all[0xF4].RegValueMask = 0xFF;

        this->sensor_reg_all[0xF5].RegAddr = 0xF5;
        this->sensor_reg_all[0xF5].RegType = REG_TYPE_READ;
        this->sensor_reg_all[0xF5].RegValueDefault = 0x00;
        this->sensor_reg_all[0xF5].RegValueMask = 0xFF;

        this->sensor_reg_all[0xF6].RegAddr = 0xF6;
        this->sensor_reg_all[0xF6].RegType = REG_TYPE_READ;
        this->sensor_reg_all[0xF6].RegValueDefault = 0x00;
        this->sensor_reg_all[0xF6].RegValueMask = 0xFF;

        this->sensor_reg_all[0xF7].RegAddr = 0xF7;
        this->sensor_reg_all[0xF7].RegType = REG_TYPE_READ;
        this->sensor_reg_all[0xF7].RegValueDefault = 0x00;
        this->sensor_reg_all[0xF7].RegValueMask = 0xFF;

        this->sensor_reg_all[0xF8].RegAddr = 0xF8;
        this->sensor_reg_all[0xF8].RegType = REG_TYPE_READ;
        this->sensor_reg_all[0xF8].RegValueDefault = 0x00;
        this->sensor_reg_all[0xF8].RegValueMask = 0xFF;

        this->sensor_reg_all[0xF9].RegAddr = 0xF9;
        this->sensor_reg_all[0xF9].RegType = REG_TYPE_READ;
        this->sensor_reg_all[0xF9].RegValueDefault = 0x00;
        this->sensor_reg_all[0xF9].RegValueMask = 0xFF;

        this->sensor_reg_all[0xFA].RegAddr = 0xFA;
        this->sensor_reg_all[0xFA].RegType = REG_TYPE_READ;
        this->sensor_reg_all[0xFA].RegValueDefault = 0x00;
        this->sensor_reg_all[0xFA].RegValueMask = 0xFF;

        this->sensor_reg_all[0xFB].RegAddr = 0xFB;
        this->sensor_reg_all[0xFB].RegType = REG_TYPE_READ;
        this->sensor_reg_all[0xFB].RegValueDefault = 0x00;
        this->sensor_reg_all[0xFB].RegValueMask = 0xFF;

        this->sensor_reg_all[0xFC].RegAddr = 0xFC;
        this->sensor_reg_all[0xFC].RegType = REG_TYPE_READ;
        this->sensor_reg_all[0xFC].RegValueDefault = 0x00;
        this->sensor_reg_all[0xFC].RegValueMask = 0xFF;
    }
}