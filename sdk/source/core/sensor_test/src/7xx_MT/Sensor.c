// Sensor.cpp: implementation of the CSensor class.
//
//////////////////////////////////////////////////////////////////////

#if defined(WIN32)

#include <share.h>
#include <windows.h>

#include "../../inc/func.h"
#include "../../src/windows/com_helper.h"
#include "LedExt.h"
#define egist_snprintf _snprintf
#define FUNCTION_PREFIX CSensor::

#define ARDUINO_COMMAND_LENGTH 8
#define ARDUINO_RESPONSE_LENGTH 7

#elif defined(RBS_SDK_USE)

#include "7xx_sensor_test_definition.h"
#include "egis_definition.h"
#include "egis_log.h"
#include "egis_sprintf.h"
#include "et713_reg_init.h"
#include "plat_heap.h"
#include "plat_mem.h"
#include "plat_spi.h"
#include "plat_time.h"
#include "sensor_control_common.h"
#include "type_definition.h"

#define malloc plat_alloc
#define free plat_free
#define Sleep plat_sleep_time
#define GetTickCount plat_get_time
#define LOG_TAG "RBS-INLINE"
#define OutputDebugString(X) egislog_d(X)
#define FUNCTION_PREFIX
#define MAX_PATH 256
#define ARDUINO_COMMAND_LENGTH 8
#define this g_sensor_ptr
CSensor* g_sensor_ptr = NULL;
extern int et7xa_fetch_raw_data_without_setting(uint16_t* raw_image, int width, int height);

unsigned short reg_addr[REG_NUM] = {
    0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x0C, 0x0D, 0x0E, 0x0F, 0x10, 0x11,
    0x12, 0x13, 0x14, 0x15, 0x16, 0x17, 0x18, 0x19, 0x1A, 0x1B, 0x1C, 0x1D, 0x1E, 0x1F, 0x20,
    0x21, 0x22, 0x23, 0x24, 0x25, 0x26, 0x27, 0x28, 0x29, 0x2C, 0x2D, 0x2E, 0x2F, 0x30, 0x31,
    0x32, 0x33, 0x34, 0x35, 0x36, 0x37, 0x38, 0x39, 0x3A, 0x3B, 0x3C, 0x3D, 0x3E, 0x3f, 0x40,
    0x41, 0x42, 0x43, 0x44, 0x45, 0x46, 0x47, 0x48, 0x49, 0x4A, 0x4B, 0x4C, 0x4D, 0x60, 0x61,
    0x62, 0x63, 0x64, 0x65, 0x66, 0x67, 0x68, 0x69, 0xD0, 0xDF, 0xE0, 0xE1, 0xE2, 0xE3, 0xE4,
    0xE5, 0xE6, 0xF4, 0xF5, 0xF6, 0xF7, 0xF8, 0xF9, 0xFA, 0xFB, 0xFC};

char reg_name[REG_NUM][50] = {
    "DEV_ID0       ", "DEV_ID1       ", "REV_ID        ", "FID           ", "SYS_STUS      ",
    "INT_CTRL      ", "CONF          ", "OSC40M_CONF   ", "IO_DS         ", "BOOST_CP      ",
    "SCAN_CSR      ", "SCAN_FRAMES   ", "FB_CSR        ", "CMD_NOV_H     ", "CMD_NOV_L     ",
    "CMD_RRST_UP_H ", "CMD_RRST_UP_L ", "CMD_RRST_DN_H ", "CMD_RRST_DN_L ", "CMD_TG_UP_H   ",
    "CMD_TG_UP_L   ", "CMD_TG_DN_H   ", "CMD_TG_DN_L   ", "CMD_OFFNE_UP_H", "CMD_OFFNE_UP_L",
    "CMD_OFFNE_DN_H", "CMD_OFFNE_DN_L", "CMD_RRST_DN2_H", "CMD_RRST_DN2_L", "CMD_SHR_UP_H  ",
    "CMD_SHR_UP_L  ", "CMD_SHR_DN_H  ", "CMD_SHR_DN_L  ", "CMD_SHS_UP_H  ", "CMD_SHS_UP_L  ",
    "CMD_SHS_DN_H  ", "CMD_SHS_DN_L  ", "CMD_READOUT_H ", "CMD_READOUT_L ", "EXP_TIME_SW_H ",
    "EXP_TIME_SW_L ", "EXP_TIME_MTY_H", "EXP_TIME_MTY_L", "AE_TARGET_H   ", "AE_TARGET_L   ",
    "AE_V0_H       ", "AE_V0_L       ", "AE_RESULT_H   ", "AE_RESULT_L   ", "AE_AVG_H      ",
    "AE_AVG_L      ", "PGA_CSR       ", "DARK_ADJ_L    ", "DARK_ADJ_H    ", "PXL_CTRL0     ",
    "PXL_CTRL1     ", "HDR_CSR       ", "HDR_GAIN_H    ", "HDR_GAIN_L    ", "PWR_CTRL0     ",
    "PWR_CTRL1     ", "PWR_CTRL2     ", "PWR_CTRL3     ", "VCM_CTRL      ", "FOD_CYCLE_H   ",
    "FOD_CYCLE_L   ", "FOD_CSR       ", "DAC_CSR       ", "DAC_CLK_DIV   ", "DAC_FOD_H     ",
    "DAC_FOD_L     ", "DAC_OUT       ", "TS_DOUT       ", "STAT_MEAN_H   ", "STAT_MEAN_L   ",
    "STAT_MAX_H    ", "STAT_MAX_L    ", "STAT_MIN_H    ", "STAT_MIN_L    ", "HSTG_COL_STR  ",
    "HSTG_COL_END  ", "HSTG_ROW_STR  ", "HSTG_ROW_END  ", "EF_CSR        ", "REG_KEY       ",
    "RST_CTRL      ", "TEST_ADC      ", "TEST_PAT_H    ", "TEST_PAT_L    ", "FB_RST        ",
    "MBIST_SET     ", "MBIST_DAT     ", "TEST_CTRL     ", "CADDR_PHS     ", "TEST_MODE     ",
    "DAC_DATA_H    ", "DAC_DATA_L    ", "DEBUG0        ", "DEBUG1        ", "PAD_TEST_CSR  ",
    "PIX_ID        "};

#endif

#include "Sensor.h"

#define SHOW_REG_TEST_FAIL 1
#define GET_IMAGE_TIMEOUT 2000

#define ET711_GET_IMAGE_TIMEOUT 1000

#include <math.h>
#include <stdio.h>
#include <string.h>

int FUNCTION_PREFIX iSearchDevices(int tester_board_type) {
#if defined(WIN32)
    ndevice = 0;
    target_board_type = tester_board_type;

    if (target_board_type == TESTER_BOARD_ARDUINO) {
        ndevice = iFindArduinoCDC(com_ports);
    }
#else
    this->ndevice = 1;
#endif
    return this->ndevice;
}

BOOL FUNCTION_PREFIX bOpenSensor(int device_index) {
#if defined(WIN32)
    vRegInit();

    if (target_board_type == TESTER_BOARD_ARDUINO) {
        return bOpenArduino(device_index);
    }

    return FALSE;
#else
    int ret = -1;
#ifndef __OTG_SENSOR__
    ret = io_dispatch_connect(NULL);
    return ret ? ret : 1;
#else
    return TRUE;
#endif

#endif
}

BOOL FUNCTION_PREFIX bReOpenSensor(HANDLE device_handle) {
#if defined(WIN32)
    m_hCom = device_handle;
    bIsDeviceReopen = TRUE;
    return TRUE;
#else
    return TRUE;
#endif
}

HANDLE
FUNCTION_PREFIX
hGetDeviceHandle() {
#if defined(WIN32)
    return m_hCom;
#else
    return 0;
#endif
}

BOOL FUNCTION_PREFIX bCloseSensor() {
#if defined(WIN32)
    if (raw_image) {
        free(raw_image);
        raw_image = NULL;
    }
    if (fullframe16) {
        free(fullframe16);
        fullframe16 = NULL;
    }

    if (target_board_type == TESTER_BOARD_ARDUINO) {
        if (m_hCom != INVALID_HANDLE_VALUE) {
            if (bIsDeviceReopen) {
                bIsDeviceReopen = FALSE;
            } else {
                CloseHandle(m_hCom);
                m_hCom = INVALID_HANDLE_VALUE;
            }
        }
    }
#else
    /*io_dispatch_disconnect*/
#endif
    return TRUE;
}

BOOL FUNCTION_PREFIX bWriteRegisterSingleByte(unsigned char addr, unsigned char value,
                                              unsigned char I2CAddr) {
#if defined(WIN32)
    if (target_board_type == TESTER_BOARD_ARDUINO) {
        BOOL r_value;
        unsigned char command[512];
        unsigned char buf[512];

        memset(command, 0x00, sizeof(command));
        memset(buf, 0x00, sizeof(buf));

        // Signature
        command[0] = 0x45;  // 'E'
        command[1] = 0x47;  // 'G'
        command[2] = 0x49;  // 'I'
        command[3] = 0x53;  // 'S'

        // Operation code
        command[4] = 0x58;  // Write LED data

        // Parameter
        command[5] = I2CAddr;

        command[6] = addr;

        command[7] = value;

        // Write Command
        r_value = bWritComPort(command, ARDUINO_COMMAND_LENGTH);
        if (!r_value) {
            return FALSE;
        }

        // Read Response
        r_value = bReadComPort(buf, ARDUINO_RESPONSE_LENGTH, READ_TIMEOUT);
        if (!r_value) {
            return FALSE;
        }

        if ((buf[0] != 'S') || (buf[1] != 'I') || (buf[2] != 'G') || (buf[3] != 'E')) {
            return FALSE;
        }

        if (buf[6] != 0x01) {
            return FALSE;
        }

        return TRUE;
    } else {
        return TRUE;
    }
#else
    return TRUE;
#endif
}

// Photo Diode
BOOL FUNCTION_PREFIX bReadI2CDataWord(unsigned short* pValue, unsigned char I2CAddr) {
#if defined(WIN32)
    if (target_board_type == TESTER_BOARD_ARDUINO) {
        BOOL r_value;
        unsigned char command[512];
        unsigned char buf[512];

        memset(command, 0x00, sizeof(command));
        memset(buf, 0x00, sizeof(buf));

        // Signature
        command[0] = 0x45;  // 'E'
        command[1] = 0x47;  // 'G'
        command[2] = 0x49;  // 'I'
        command[3] = 0x53;  // 'S'

        // Operation code
        command[4] = 0x59;  // Read 2 bytes from I2C

        // Parameter
        command[5] = I2CAddr;

        command[6] = 0x00;

        command[7] = 0x00;

        // Write Command
        r_value = bWritComPort(command, ARDUINO_COMMAND_LENGTH);
        if (!r_value) {
            return FALSE;
        }

        // Read Response
        r_value = bReadComPort(buf, ARDUINO_RESPONSE_LENGTH, READ_TIMEOUT);
        if (!r_value) {
            return FALSE;
        }

        if ((buf[0] != 'S') || (buf[1] != 'I') || (buf[2] != 'G') || (buf[3] != 'E')) {
            return FALSE;
        }

        if (buf[6] != 0x01) {
            return FALSE;
        }

        *pValue = (buf[4] << 8) + buf[5];

        return TRUE;
    } else {
        return TRUE;
    }
#else
    return TRUE;
#endif
}

// DAC
BOOL FUNCTION_PREFIX bWriteDAC(unsigned short value, unsigned char I2CAddr) {
#if defined(WIN32)
    if (target_board_type == TESTER_BOARD_ARDUINO) {
        BOOL r_value;
        unsigned char command[512];
        unsigned char buf[512];

        memset(command, 0x00, sizeof(command));
        memset(buf, 0x00, sizeof(buf));

        // Signature
        command[0] = 0x45;  // 'E'
        command[1] = 0x47;  // 'G'
        command[2] = 0x49;  // 'I'
        command[3] = 0x53;  // 'S'

        // Operation code
        command[4] = 0x5A;  // Write 2 bytes of DAC setting

        // Parameter
        command[5] = I2CAddr;

        // Control Byte
        command[6] = 0x10;

        // Stores the 8 most significant bits of unsigned binary data sent by the master.
        command[7] = (value & 0x0FF0) >> 4;

        // Stores the 4 least significant bits of unsigned binary data sent by the master
        command[8] = (value & 0x000F) << 4;

        command[9] = 0x00;

        // Write Command
        r_value = bWritComPort(command, 10);
        if (!r_value) {
            return FALSE;
        }

        // Read Response
        r_value = bReadComPort(buf, ARDUINO_RESPONSE_LENGTH, READ_TIMEOUT);
        if (!r_value) {
            return FALSE;
        }

        if ((buf[0] != 'S') || (buf[1] != 'I') || (buf[2] != 'G') || (buf[3] != 'E')) {
            return FALSE;
        }

        if (buf[6] != 0x01) {
            return FALSE;
        }

        return TRUE;
    } else {
        return TRUE;
    }
#else
    return TRUE;
#endif
}

BOOL FUNCTION_PREFIX bLEDBackLightOn(BOOL bOn, unsigned char led_gain, unsigned short* pLightness,
                                     int led_control_board_type, int led_ext_comport) {
#if defined(WIN32)

    if (led_control_board_type == 1) {
        // new lec control board
        BOOL r_value;
        r_value = bSetGPIOMode(LED_POWER_SOURCE_PIN, 0x01);
        if (!r_value) {
            return FALSE;
        }

        if (bOn) {
            r_value = bSetGPIOData(LED_POWER_SOURCE_PIN, 0x00);
            if (!r_value) {
                return FALSE;
            }

            if (pLightness) {
                bWriteDAC(*pLightness, 0x4F);
            } else {
                bWriteDAC(0x0FFF, 0x4F);
            }

            // Wait some time for LED Stable
            Sleep(200);
        } else {
            r_value = bSetGPIOData(LED_POWER_SOURCE_PIN, 0x01);
            if (!r_value) {
                return FALSE;
            }
        }
    } else if (led_control_board_type == 2) {
        // external led control
        BOOL r_value;
        CLedExt led_ext;

        r_value = led_ext.bOpenDevice(led_ext_comport);
        if (!r_value) {
            return FALSE;
        }

        r_value = led_ext.bLedControl256(0x01, 0x01, 128);
        if (!r_value) {
            return FALSE;
        }

        led_ext.bCloseDevice();
    } else {
        bWriteRegisterSingleByte(0xA5, 0x5A, 0x6B);

        if (bOn) {
            bWriteRegisterSingleByte(0x1C, led_gain, 0x69);

            bWriteRegisterSingleByte(0x00, 0x08, 0x68);
            bWriteRegisterSingleByte(0x01, 0x00, 0x69);

            if (pLightness) {
                int i;
                for (i = 0; i < 16; i++) {
                    bWriteRegisterSingleByte(0x02 + i, (unsigned char)(*pLightness & 0x00FF), 0x69);
                }
            } else {
                bWriteRegisterSingleByte(0x02, 0xA0, 0x69);
                bWriteRegisterSingleByte(0x03, 0xA0, 0x69);
                bWriteRegisterSingleByte(0x04, 0xA0, 0x69);
                bWriteRegisterSingleByte(0x05, 0xA0, 0x69);
                bWriteRegisterSingleByte(0x06, 0xA0, 0x69);
                bWriteRegisterSingleByte(0x07, 0xA0, 0x69);
                bWriteRegisterSingleByte(0x08, 0xA0, 0x69);
                bWriteRegisterSingleByte(0x09, 0xA0, 0x69);
                bWriteRegisterSingleByte(0x0A, 0xA0, 0x69);
                bWriteRegisterSingleByte(0x0B, 0xA0, 0x69);
                bWriteRegisterSingleByte(0x0C, 0xA0, 0x69);
                bWriteRegisterSingleByte(0x0D, 0xA0, 0x69);
                bWriteRegisterSingleByte(0x0E, 0xA0, 0x69);
                bWriteRegisterSingleByte(0x0F, 0xA0, 0x69);
                bWriteRegisterSingleByte(0x10, 0xA0, 0x69);
                bWriteRegisterSingleByte(0x11, 0xA0, 0x69);
            }

            bWriteRegisterSingleByte(0x14, 0xFF, 0x69);
            bWriteRegisterSingleByte(0x15, 0xFF, 0x69);
            bWriteRegisterSingleByte(0x16, 0xFF, 0x69);
            bWriteRegisterSingleByte(0x17, 0xFF, 0x69);

            // Wait some time for LED Stable
            Sleep(200);
        }
    }
#endif
    return TRUE;
}

BOOL FUNCTION_PREFIX bLoadFile(char* pfile) {
#if defined(WIN32)
    FILE* fp = _fsopen(pfile, "r", _SH_DENYWR);
    if (!fp) {
        return FALSE;
    }

    char buf[4096];
    char* p;

    BOOL r_value;
    unsigned short addr;
    unsigned char value;

    char setting_file[MAX_PATH];
    strcpy(setting_file, pfile);

    while (fgets(buf, 4096, fp)) {
        if (buf[0] == 'W') {
            p = strtok(buf + 1, " ");
            if (!p) {
                continue;
            }

            p = strtok(NULL, " ");
            if (!p) {
                continue;
            }

            addr = h4tous(p);

            p = strtok(NULL, " ");
            if (!p) {
                continue;
            }

            value = h2toc(p);
            r_value = bWriteRegister(addr, value);
            if (!r_value) {
                return FALSE;
            }
        } else if (buf[0] == 'S') {
            p = strtok(buf + 1, " ");
            if (p) {
                Sleep(atoi(p));
            }
        } else if (buf[0] == 'L') {
            p = strtok(buf + 1, " \n");

            char include_file[MAX_PATH];

            vPath2Path(setting_file);
            egist_snprintf(include_file, MAX_PATH, "%s\\%s", setting_file, p);

            if (!bLoadFile(include_file)) {
                return FALSE;
            }
        }
    }
#endif
    return TRUE;
}

BOOL FUNCTION_PREFIX bInitSensor(char* pSettingFile, BOOL bLoadSettingFile) {
#if (EGIS_SENSOR == 711)
    // ET729 146x(218+6) 16 bit
    // ET711 146x218 16 bit

    this->sensor_width_raw = 146 * 2;
    this->sensor_height_raw = (218 + 6);

    this->sensor_width = 146;
    this->sensor_height = 218;

    this->sensor_width_show = 218;
    this->sensor_height_show = 146;

#elif (EGIS_SENSOR == 713)
    // ET713 (1+200+1)x(200+2) 16 bit

    this->sensor_width_raw = 202 * 2;
    this->sensor_height_raw = 202;

    this->sensor_width = 200;
    this->sensor_height = 200;

    this->sensor_width_show = 200;
    this->sensor_height_show = 200;

#elif (EGIS_SENSOR == 701)
    this->sensor_width_raw = 139;
    this->sensor_height_raw = 116;

    this->sensor_width = 139;
    this->sensor_height = 116;

    this->sensor_width_show = 139;
    this->sensor_height_show = 116;

#endif

    this->sensor_total_pixels_raw = this->sensor_width_raw * this->sensor_height_raw;
    this->sensor_total_pixels = this->sensor_width * this->sensor_height;
    this->sensor_total_pixels_show = this->sensor_width_show * this->sensor_height_show;

    this->raw_image = (unsigned char*)malloc(this->sensor_total_pixels_raw);
    if (!this->raw_image) {
        OutputDebugString("Memory Allocate Fail\n");
        bCloseSensor();
        return FALSE;
    }

    this->fullframe16 = (unsigned short*)malloc(this->sensor_total_pixels * sizeof(unsigned short));
    if (!this->fullframe16) {
        OutputDebugString("Memory Allocate Fail\n");
        bCloseSensor();
        free(this->raw_image);
        return FALSE;
    }

#ifdef WIN32

#if ((EGIS_SENSOR == 711) || (EGIS_SENSOR == 713))
    bWriteRegister(BOOST_CP, 0xC1);
    bWriteRegister(PWR_CTRL2, 0xF6);
    bWriteRegister(PWR_CTRL3, 0x13);
    bWriteRegister(VCM_CTRL, 0x62);
#elif (EGIS_SENSOR == 713)
    bWriteRegister(PXL_CTRL1, 0x02);
    bWriteRegister(VCM_CTRL, 0x61);
#endif

    if (bLoadSettingFile) {
        if (!bLoadFile(pSettingFile)) {
            // return FALSE;
            OutputDebugString("bLoadSettingFile Fail\n");
        }
    }
#else
    /*et725 FW will help initialize sensor*/
    egislog_d("finished", __func__);
#endif

    return TRUE;
}

BOOL FUNCTION_PREFIX bReadRegister(unsigned short addr, unsigned char* pValue) {
#ifdef WIN32
    if (target_board_type == TESTER_BOARD_ARDUINO) {
        // To Sensor

        BOOL r_value;
        unsigned char command[512];
        int command_len;
        unsigned char buf[512];

        memset(command, 0x00, sizeof(command));
        memset(buf, 0x00, sizeof(buf));

        // Signature
        command[0] = 0x45;  // 'E'
        command[1] = 0x47;  // 'G'
        command[2] = 0x49;  // 'I'
        command[3] = 0x53;  // 'S'

        command_len = ARDUINO_COMMAND_LENGTH;

        // Operation code
        command[4] = 0x60;

        // Parameter
        command[5] = (unsigned char)(addr & 0x00FF);

        // Write Command
        r_value = bWritComPort(command, command_len);
        if (!r_value) {
            return FALSE;
        }

        // Read Response
        r_value = bReadComPort(buf, ARDUINO_RESPONSE_LENGTH, READ_TIMEOUT);
        if (!r_value) {
            return FALSE;
        }

        if ((buf[0] != 'S') || (buf[1] != 'I') || (buf[2] != 'G') || (buf[3] != 'E')) {
            return FALSE;
        }

        // buf[4]: param
        *pValue = buf[5];

        if (buf[6] != 0x01) {
            return FALSE;
        }

        return TRUE;
    } else {
        return TRUE;
    }
#else
    int result;
    result = et7xx_read_register(addr, pValue);
    egislog_d("read : addr = %d  pValue = %d  ret = %d", addr, *pValue, result);
    return result ? result : 1;
#endif
}

BOOL FUNCTION_PREFIX bWriteRegister(unsigned short addr, unsigned char value) {
#ifdef WIN32
    if (target_board_type == TESTER_BOARD_ARDUINO) {
        // To Sensor
        BOOL r_value;
        unsigned char command[512];
        int command_len;
        unsigned char buf[512];

        memset(command, 0x00, sizeof(command));
        memset(buf, 0x00, sizeof(buf));

        // Signature
        command[0] = 0x45;  // 'E'
        command[1] = 0x47;  // 'G'
        command[2] = 0x49;  // 'I'
        command[3] = 0x53;  // 'S'

        command_len = ARDUINO_COMMAND_LENGTH;

        // Operation code
        command[4] = 0x61;  //

        // Parameter
        command[5] = (unsigned char)(addr & 0x00FF);  // Register Address

        // Value
        command[6] = value;  // Register Value to write

        // Write Command
        r_value = bWritComPort(command, command_len);
        if (!r_value) {
            return FALSE;
        }

        // Read Response
        r_value = bReadComPort(buf, ARDUINO_RESPONSE_LENGTH, READ_TIMEOUT);
        if (!r_value) {
            return FALSE;
        }

        if ((buf[0] != 'S') || (buf[1] != 'I') || (buf[2] != 'G') || (buf[3] != 'E')) {
            return FALSE;
        }

        if (buf[6] != 0x01) {
            return FALSE;
        }
    }

    return TRUE;
#else
    int result;
    result = et7xx_write_register(addr, value);
    egislog_d("write : addr = %d  value = %d  ret = %d", addr, value, result);
    return result ? result : 1;
#endif
}

BOOL FUNCTION_PREFIX bReadRegisterUs(unsigned short addr, unsigned short* pValue) {
    BOOL r_value;
    unsigned char value;

    r_value = bReadRegister(addr, &value);
    if (!r_value) {
        return FALSE;
    }
    *pValue = value << 8;

    r_value = bReadRegister(addr + 1, &value);
    if (!r_value) {
        return FALSE;
    }
    *pValue |= value;

    return TRUE;
}

BOOL FUNCTION_PREFIX bWriteRegisterUs(unsigned short addr, unsigned short us_value) {
    BOOL r_value;
    unsigned char value;

    value = (us_value & 0xFF00) >> 8;
    r_value = bWriteRegister(addr, value);
    if (!r_value) {
        return FALSE;
    }

    value = (us_value & 0x00FF);
    r_value = bWriteRegister(addr + 1, value);
    if (!r_value) {
        return FALSE;
    }

    return TRUE;
}

BOOL FUNCTION_PREFIX bPowerOn(BOOL bOn) {
#ifdef WIN32
    if (target_board_type == TESTER_BOARD_ARDUINO) {
        BOOL r_value;

        if (bOn) {
            // LDO30 for OTP Write
            r_value = bSetGPIOMode(LDO30_ARDUINO_PIN, 0x01);
            if (!r_value) {
                return FALSE;
            }
            r_value = bSetGPIOData(LDO30_ARDUINO_PIN, 0x01);
            if (!r_value) {
                return FALSE;
            }

            // LDO pin high
            r_value = bSetGPIOData(LDO_ARDUINO_PIN, 0x01);
            if (!r_value) {
                return FALSE;
            }
            Sleep(50);

            // Reset Pin High
            r_value = bSetGPIOData(RESET_ARDUINO_PIN, 0x01);
            if (!r_value) {
                return FALSE;
            }
            Sleep(50);
        } else {
            r_value = bSetGPIOData(LDO30_ARDUINO_PIN, 0x00);
            if (!r_value) {
                return FALSE;
            }

            // LDO pin low
            r_value = bSetGPIOData(LDO_ARDUINO_PIN, 0x00);
            if (!r_value) {
                return FALSE;
            }
            Sleep(50);

            // Reset Pin Low
            r_value = bSetGPIOData(RESET_ARDUINO_PIN, 0x00);
            if (!r_value) {
                return FALSE;
            }
            Sleep(50);
        }

        return TRUE;
    } else {
        return FALSE;
    }
#else
    return TRUE;
#endif
}

BOOL FUNCTION_PREFIX bHardwareReset(int reset_hold) {
#ifdef WIN32
    if (target_board_type == TESTER_BOARD_ARDUINO) {
        BOOL r_value;

        // Reset Pin Low
        r_value = bSetGPIOData(RESET_ARDUINO_PIN, 0x00);
        if (!r_value) {
            return FALSE;
        }

        Sleep(reset_hold);

        // Reset Pin High
        r_value = bSetGPIOData(RESET_ARDUINO_PIN, 0x01);
        if (!r_value) {
            return FALSE;
        }

        Sleep(reset_hold);
    }

    return TRUE;
#else
    return TRUE;
#endif
}

#ifdef _DEBUG
#define SIMULATE_OTP 1
#else
#define SIMULATE_OTP 0
#endif

BOOL FUNCTION_PREFIX bOTPReadData(unsigned char* pData) {
#if (SIMULATE_OTP)
    char filename[MAX_PATH];
    egist_snprintf(filename, MAX_PATH, "otp_site%d.txt", PortIndex);
    FILE* fp = fopen(filename, "r");
    if (fp) {
        memset(pData, 0x00, 32);

        int i;
        char buf[MAX_PATH];
        char* p;
        fread(buf, 1, MAX_PATH, fp);

        p = strtok(buf, " ");
        if (p) {
            for (i = 0; i < 32; i++) {
                pData[i] = h2toc(p);
                p = strtok(NULL, " ");
                if (!p) {
                    break;
                }
            }
        }

        fclose(fp);
        return TRUE;
    } else {
        memset(pData, 0x00, 32);
        return TRUE;
    }
#else
    BOOL r_value;
    unsigned char command[512];
    unsigned char buf[512];
    unsigned char value;
    char log[MAX_PATH];

    int retry;
    for (retry = 0; retry < 3; retry++) {
        // Enable EFuse controller for read
        r_value = bWriteRegister(EF_CSR, 0x01);
        if (!r_value) {
            return FALSE;
        }

        // delay 5ms for OTP data prepare
        Sleep(10);

        r_value = bReadRegister(EF_CSR, &value);
        if (!r_value) {
            return FALSE;
        }

        if ((value & 0x80) == 0x00) {
            break;
        }
        egist_snprintf(log, MAX_PATH, "OTP Not Ready(%02X)...\n", value);
        OutputDebugString(log);
    }

    if (retry == 3) {
        return FALSE;
    }

    memset(command, 0x00, sizeof(command));
    memset(buf, 0x00, sizeof(buf));

    // Signature
    command[0] = 0x45;  // 'E'
    command[1] = 0x47;  // 'G'
    command[2] = 0x49;  // 'I'
    command[3] = 0x53;  // 'S'

    // Operation code
    command[4] = 0x75;

    command[5] = 0x00;
    command[6] = 0x20;  // 32 bytes OTP data

#if defined(WIN32)
    // Write Command
    r_value = bWritComPort(command, ARDUINO_COMMAND_LENGTH);
    if (!r_value) {
        // Disable EFuse controller for read
        bWriteRegister(EF_CSR, 0x00);
        return FALSE;
    }

    // Read Response
    r_value = bReadComPort(buf, 0x20, READ_TIMEOUT);
    if (!r_value) {
        // Disable EFuse controller for read
        bWriteRegister(EF_CSR, 0x00);
        return FALSE;
    }
#endif

    memcpy(pData, buf, 0x20);

    // Disable EFuse controller for read
    bWriteRegister(EF_CSR, 0x00);

    return TRUE;
#endif
}

BOOL FUNCTION_PREFIX bOTPWriteData(unsigned char* pData) {
#if (SIMULATE_OTP)
    unsigned char otp_data_read[32];
    if (!bOTPReadData(otp_data_read)) {
        return FALSE;
    }

    char filename[MAX_PATH];
    egist_snprintf(filename, MAX_PATH, "otp_site%d.txt", PortIndex);
    FILE* fp = fopen(filename, "w");
    if (fp) {
        int i;
        for (i = 0; i < 32; i++) {
            fprintf(fp, "%02X ", pData[i] | otp_data_read[i]);
        }
        fclose(fp);
        return TRUE;
    } else {
        return FALSE;
    }
#else
    BOOL r_value;
    unsigned char command[512];
    unsigned char buf[512];
    unsigned char value;
    char log[MAX_PATH];

    int retry;
    for (retry = 0; retry < 3; retry++) {
        // Enable EFuse controller for write
        r_value = bWriteRegister(EF_CSR, 0x03);
        if (!r_value) {
            return FALSE;
        }

        // delay 5ms for OTP data prepare
        Sleep(10);

        r_value = bReadRegister(EF_CSR, &value);
        if (!r_value) {
            return FALSE;
        }

        if ((value & 0x83) == 0x83) {
            // busy == 1 and mode ==WR
            break;
        }
        egist_snprintf(log, MAX_PATH, "OTP Not Ready(%02X)...\n", value);
        OutputDebugString(log);
    }

    if (retry == 3) {
        return FALSE;
    }

    // delay 5ms for OTP data prepare
    Sleep(10);

    memset(command, 0x00, sizeof(command));
    memset(buf, 0x00, sizeof(buf));

    // Signature
    command[0] = 0x45;  // 'E'
    command[1] = 0x47;  // 'G'
    command[2] = 0x49;  // 'I'
    command[3] = 0x53;  // 'S'

    // Operation code
    command[4] = 0x76;

    command[5] = 0x00;
    command[6] = 0x20;  // 32 bytes OTP data

#if defined(WIN32)
    // Write Command
    r_value = bWritComPort(command, ARDUINO_COMMAND_LENGTH);
    if (!r_value) {
        // Disable EFuse controller for write
        r_value = bWriteRegister(EF_CSR, 0x00);
        return FALSE;
    }

    // Write Command
    r_value = bWritComPort(pData, 32);
    if (!r_value) {
        // Disable EFuse controller for write
        r_value = bWriteRegister(EF_CSR, 0x00);
        return FALSE;
    }

    // Read Response
    r_value = bReadComPort(buf, 7, READ_TIMEOUT);
#endif

    if (buf[6] != 0x01) {
        // Disable EFuse controller for write
        r_value = bWriteRegister(EF_CSR, 0x00);
        return FALSE;
    }

    // delay 10ms for FIFO -> eFuse memory
    Sleep(50);

    r_value = bReadRegister(EF_CSR, &value);
    if ((value & 0x80) != 0x00) {
        return FALSE;
    }
    // Disable EFuse controller for write
    r_value = bWriteRegister(EF_CSR, 0x00);

    return TRUE;
#endif
}

BOOL FUNCTION_PREFIX bFlashGetID(unsigned char* pFlashID) {
#if defined(WIN32)
    BOOL r_value;
#endif
    unsigned char command[512];
    unsigned char buf[512];

    memset(command, 0x00, sizeof(command));
    memset(buf, 0x00, sizeof(buf));

    // Signature
    command[0] = 0x45;  // 'E'
    command[1] = 0x47;  // 'G'
    command[2] = 0x49;  // 'I'
    command[3] = 0x53;  // 'S'

    // Operation code
    command[4] = SST_NORFLASH_CTRl;

    // Parameter
    command[5] = SUB_OPCODE_SST_NNORFLASH_READ_ID;

#if defined(WIN32)
    // Write Command
    r_value = bWritComPort(command, FLASH_COMMAND_LENGTH);
    if (!r_value) {
        return FALSE;
    }

    // Read Response
    r_value = bReadComPort(buf, 7, READ_TIMEOUT);
    if (!r_value) {
        return FALSE;
    }
#endif

    if (buf[0] != 'S') {
        return FALSE;
    }
    if (buf[1] != 'I') {
        return FALSE;
    }
    if (buf[2] != 'G') {
        return FALSE;
    }
    if (buf[3] != 'E') {
        return FALSE;
    }

    pFlashID[0] = buf[5];
    if (buf[6] != 0x01) {
        return FALSE;
    }
    return TRUE;
}

BOOL FUNCTION_PREFIX bFlashReadStatus(unsigned char* value) {
#if defined(WIN32)
    BOOL r_value;
#endif
    unsigned char command[512];
    unsigned char buf[512];

    memset(command, 0x00, sizeof(command));
    memset(buf, 0x00, sizeof(buf));

    // Signature
    command[0] = 0x45;  // 'E'
    command[1] = 0x47;  // 'G'
    command[2] = 0x49;  // 'I'
    command[3] = 0x53;  // 'S'

    // Operation code
    command[4] = SST_NORFLASH_CTRl;

    // Parameter
    command[5] = SUB_OPCODE_SST_NNORFLASH_READ_STATUS;

#if defined(WIN32)
    // Write Command
    r_value = bWritComPort(command, FLASH_COMMAND_LENGTH);
    if (!r_value) {
        return FALSE;
    }

    // Read Response
    r_value = bReadComPort(buf, 7, READ_TIMEOUT);
    if (!r_value) {
        return FALSE;
    }
#endif

    if (buf[0] != 'S') {
        return FALSE;
    }
    if (buf[1] != 'I') {
        return FALSE;
    }
    if (buf[2] != 'G') {
        return FALSE;
    }
    if (buf[3] != 'E') {
        return FALSE;
    }

    // buf[4]: param
    value[0] = buf[5];
    if (buf[6] != 0x01) {
        return FALSE;
    }
    return TRUE;
}

BOOL FUNCTION_PREFIX bFlashReadData(int address, int len_k, unsigned char* pData) {
#if defined(WIN32)
    BOOL r_value;
#endif
    unsigned char command[512];
    unsigned char start_addr_1;
    unsigned char start_addr_2;
    unsigned char start_addr_3;

    memset(command, 0x00, sizeof(command));
    int transferlength;
    transferlength = len_k * 1024;

    // Signature
    command[0] = 0x45;  // 'E'
    command[1] = 0x47;  // 'G'
    command[2] = 0x49;  // 'I'
    command[3] = 0x53;  // 'S'

    // Operation code
    command[4] = SST_NORFLASH_CTRl;

    start_addr_3 = (address & 0x0000ff);
    start_addr_2 = (address & 0x00ff00) >> 8;
    start_addr_1 = (address & 0xff0000) >> 16;

    // len high byte
    command[5] = SUB_OPCODE_SST_NNORFLASH_READ_DATA;
    command[6] = start_addr_1;
    command[7] = start_addr_2;
    command[8] = start_addr_3;
    command[9] = len_k;

#if defined(WIN32)
    // Write Command
    r_value = bWritComPort(command, FLASH_COMMAND_LENGTH);
    if (!r_value) {
        return FALSE;
    }

    // Read Image
    r_value = bReadComPort(pData, transferlength, READ_TIMEOUT);
    if (!r_value) {
        return FALSE;
    }
#endif

    return TRUE;
}

BOOL FUNCTION_PREFIX bFlashWriteEnable() {
#if defined(WIN32)
    BOOL r_value;
#endif
    unsigned char command[512];
    unsigned char buf[512];

    memset(command, 0x00, sizeof(command));
    memset(buf, 0x00, sizeof(buf));

    // Signature
    command[0] = 0x45;  // 'E'
    command[1] = 0x47;  // 'G'
    command[2] = 0x49;  // 'I'
    command[3] = 0x53;  // 'S'

    // Operation code
    command[4] = SST_NORFLASH_CTRl;

    // Parameter
    command[5] = SUB_OPCODE_SST_NNORFLASH_WRITE_ENABLE;

#if defined(WIN32)
    // Write Command
    r_value = bWritComPort(command, FLASH_COMMAND_LENGTH);
    if (!r_value) {
        return FALSE;
    }

    // Read Response
    r_value = bReadComPort(buf, 7, READ_TIMEOUT);
    if (!r_value) {
        return FALSE;
    }
#endif

    if (buf[0] != 'S') {
        return FALSE;
    }
    if (buf[1] != 'I') {
        return FALSE;
    }
    if (buf[2] != 'G') {
        return FALSE;
    }
    if (buf[3] != 'E') {
        return FALSE;
    }

    // buf[4]: param
    // value[0] = buf[5];
    if (buf[6] != 0x01) {
        return FALSE;
    }
    return TRUE;
}

BOOL FUNCTION_PREFIX bFlashWaitWriteEnableLatch() {
    BOOL r_value;
    DWORD t_begin;
    unsigned char flash_status;

    t_begin = GetTickCount();
    do {
        r_value = bFlashReadStatus(&flash_status);
        if (!r_value) {
            return FALSE;
        }

        // WEL(Write Enable Latch) bit
        if (flash_status & 0x02) {
            break;
        }

        if ((GetTickCount() - t_begin) > 3000) {
            OutputDebugString("bFlashWaitWriteEnableLatch Timeout!\n");
            return FALSE;
        }

        Sleep(10);
    } while (1);

    return TRUE;
}

BOOL FUNCTION_PREFIX bFlashWaitWriteDone() {
    BOOL r_value;
    DWORD t_begin;
    unsigned char flash_status;

    t_begin = GetTickCount();
    do {
        r_value = bFlashReadStatus(&flash_status);
        if (!r_value) {
            return FALSE;
        }

        // WIP(Write In Progress) bit
        if (!(flash_status & 0x01)) {
            break;
        }

        if ((GetTickCount() - t_begin) > 3000) {
            OutputDebugString("bFlashWaitWriteEnableLatch Timeout!\n");
            return FALSE;
        }

        Sleep(10);
    } while (1);

    return TRUE;
}

BOOL FUNCTION_PREFIX bFlashWriteData_256(int address, unsigned char* pData) {
#if defined(WIN32)
    BOOL r_value;
#endif
    unsigned char command[512];
    unsigned char buf[512];
    unsigned char Address_1;
    unsigned char Address_2;
    unsigned char Address_3;

    Address_3 = (address & 0x0000ff);
    Address_2 = (address & 0x00ff00) >> 8;
    Address_1 = (address & 0xff0000) >> 16;

    memset(command, 0x00, sizeof(command));
    memset(buf, 0x00, sizeof(buf));

    // Signature
    command[0] = 0x45;  // 'E'
    command[1] = 0x47;  // 'G'
    command[2] = 0x49;  // 'I'
    command[3] = 0x53;  // 'S'

    command[4] = SST_NORFLASH_CTRl;  // opcode

    // Parameter
    command[5] = SUB_OPCODE_SST_NNORFLASH_PAGE_PROGRAM;
    command[6] = Address_1;
    command[7] = Address_2;
    command[8] = Address_3;

#if defined(WIN32)
    // Write Command
    r_value = bWritComPort(command, FLASH_COMMAND_LENGTH);
    if (!r_value) {
        return FALSE;
    }

    // Write Data
    r_value = bWritComPort(pData, 256);  // write 256 bytes
    if (!r_value) {
        return FALSE;
    }

    // Read Response
    r_value = bReadComPort(buf, 7, READ_TIMEOUT);
    if (!r_value) {
        return FALSE;
    }
#endif

    if (buf[0] != 'S') {
        return FALSE;
    }
    if (buf[1] != 'I') {
        return FALSE;
    }
    if (buf[2] != 'G') {
        return FALSE;
    }
    if (buf[3] != 'E') {
        return FALSE;
    }

    // buf[4]: param
    //*pData = buf[5];

    if (buf[6] != 0x01) {
        return FALSE;
    }

    return TRUE;
}

BOOL FUNCTION_PREFIX bFlashEraseAll() {
#if defined(WIN32)
    BOOL r_value;
#endif
    unsigned char command[512];
    unsigned char buf[512];

    memset(command, 0x00, sizeof(command));
    memset(buf, 0x00, sizeof(buf));

    // Signature
    command[0] = 0x45;  // 'E'
    command[1] = 0x47;  // 'G'
    command[2] = 0x49;  // 'I'
    command[3] = 0x53;  // 'S'

    // Operation code
    command[4] = SST_NORFLASH_CTRl;

    // Parameter
    command[5] = SUB_OPCODE_SST_NNORFLASH_CHIP_ERASE;

#if defined(WIN32)
    // Write Command
    r_value = bWritComPort(command, FLASH_COMMAND_LENGTH);
    if (!r_value) {
        return FALSE;
    }

    // Read Response
    r_value = bReadComPort(buf, 7, READ_TIMEOUT);
    if (!r_value) {
        return FALSE;
    }
#endif

    if (buf[0] != 'S') {
        return FALSE;
    }
    if (buf[1] != 'I') {
        return FALSE;
    }
    if (buf[2] != 'G') {
        return FALSE;
    }
    if (buf[3] != 'E') {
        return FALSE;
    }
    return TRUE;

    // buf[4]: param
    // value[0] = buf[5];
    if (buf[6] != 0x01) {
        return FALSE;
    }
    return TRUE;
}

BOOL FUNCTION_PREFIX bFlashEraseSector(int address) {
#if defined(WIN32)
    BOOL r_value;
#endif
    unsigned char command[512];
    unsigned char buf[512];
    unsigned char start_addr_1;
    unsigned char start_addr_2;
    unsigned char start_addr_3;
    //  int     transferlength;
    //  transferlength = address*1024;

    memset(command, 0x00, sizeof(command));
    memset(buf, 0x00, sizeof(buf));

    // Signature
    command[0] = 0x45;  // 'E'
    command[1] = 0x47;  // 'G'
    command[2] = 0x49;  // 'I'
    command[3] = 0x53;  // 'S'

    // Operation code
    command[4] = SST_NORFLASH_CTRl;

    start_addr_3 = (address & 0x0000ff);
    start_addr_2 = (address & 0x00ff00) >> 8;
    start_addr_1 = (address & 0xff0000) >> 16;

    // Parameter
    command[5] = SUB_OPCODE_SST_NNORFLASH_SECTOR_ERASE;
    command[6] = start_addr_1;
    command[7] = start_addr_2;
    command[8] = start_addr_3;
    //  command[9] = len_k;

#if defined(WIN32)
    // Write Command
    r_value = bWritComPort(command, FLASH_COMMAND_LENGTH);
    if (!r_value) {
        return FALSE;
    }

    // Read Response
    r_value = bReadComPort(buf, 7, READ_TIMEOUT);
    if (!r_value) {
        return FALSE;
    }
#endif

    if (buf[0] != 'S') {
        return FALSE;
    }
    if (buf[1] != 'I') {
        return FALSE;
    }
    if (buf[2] != 'G') {
        return FALSE;
    }
    if (buf[3] != 'E') {
        return FALSE;
    }

    return TRUE;

    // buf[4]: param
    // value[0] = buf[5];
    if (buf[6] != 0x01) {
        return FALSE;
    }
    return TRUE;
}

BOOL FUNCTION_PREFIX bGetSensorID(SENSOR_ID* pSensorID) {
    BOOL r_value;
    unsigned char ID_0, ID_1, VER, PID;

    r_value = bReadRegister(DEV_ID_0, &ID_0);
    if (!r_value) {
        return FALSE;
    }
    pSensorID->major_id = ID_0;

    r_value = bReadRegister(DEV_ID_1, &ID_1);
    if (!r_value) {
        return FALSE;
    }
    pSensorID->minor_id = ID_1;

    r_value = bReadRegister(REV_ID, &VER);
    if (!r_value) {
        return FALSE;
    }
    pSensorID->revision = VER;

    r_value = bWriteRegister(REG_KEY, 0x88);
    if (!r_value) {
        return FALSE;
    }

    r_value = bReadRegister(PIX_ID, &PID);
    if (!r_value) {
        return FALSE;
    }
    pSensorID->pixel_id = PID;

    r_value = bWriteRegister(REG_KEY, 0x00);
    if (!r_value) {
        return FALSE;
    }

#if (EGIS_SENSOR == 711)

    if ((ID_0 == 0x07) && (ID_1 == 0x0B)) {
        this->sensor_width_raw = 146 * 2;
        this->sensor_height_raw = 218;
        this->sensor_total_pixels_raw = this->sensor_width_raw * this->sensor_height_raw;
    }
#endif

#if defined(WIN32)
    memcpy(&sensor_id, pSensorID, sizeof(SENSOR_ID));
#else
    memcpy(&this->sensor_id, pSensorID, sizeof(SENSOR_ID));
#endif
    return TRUE;
}

BOOL FUNCTION_PREFIX bSoftwareReset(int reset_hold) {
    BOOL r_value;

    // Softeware Reset
    r_value = bWriteRegister(SW_RESET, 0x00);
    if (!r_value) {
        return FALSE;
    }

    Sleep(reset_hold);

    return TRUE;
}

BOOL FUNCTION_PREFIX bSetPGASetting(unsigned char data) {
    unsigned char addr;
    unsigned char value;

    addr = PGA_CSR;
    value = data;

    if (!bWriteRegister(addr, value)) {
        return FALSE;
    }

    return TRUE;
}

BOOL FUNCTION_PREFIX bGetPGASetting(unsigned char* pdata) {
    unsigned char addr;

    addr = PGA_CSR;

    if (!bReadRegister(addr, pdata)) {
        return FALSE;
    }

    return TRUE;
}

BOOL FUNCTION_PREFIX bGetFrameRaw(unsigned char* pRaw, int frame_size) {
#if defined(WIN32)
    if (target_board_type == TESTER_BOARD_ARDUINO) {
#elif defined(RBS_SDK_USE)
    if (this->target_board_type == TESTER_BOARD_ARDUINO) {
#endif
        // Arduino Tester Board
        // QueryExecutionTimeInit();

        BOOL r_value;
        unsigned char value;
        unsigned char retry = 0;

    ET711_GETIMAGE_WORKAROUND:
        r_value = bWriteRegister(SCAN_CSR, 0x21);
        if (!r_value) {
            return FALSE;
        }

        DWORD t_begin = GetTickCount();
        while (1) {
            value = 0x00;

            r_value = bReadRegister(SYS_STUS, &value);
            if (!r_value) {
                return FALSE;
            }

            if (value & 0x01) {
                break;
            }

            Sleep(10);

            if ((GetTickCount() - t_begin) > ET711_GET_IMAGE_TIMEOUT) {
                OutputDebugString("Timeout!\n");
                return FALSE;
            }
        }

#if defined(WIN32)
        r_value = bET711_GetImage(frame_size, pRaw);
#elif defined(RBS_SDK_USE)
        r_value = et7xx_read_frame((uint8_t*)pRaw, this->sensor_total_pixels_raw);
        egislog_d("r_value = %d", r_value);
        r_value = r_value ? r_value : 1;
#endif

        if (!r_value) {
            // retry 3 times for Arduino USB 1~3 packets loss issue
            if (retry++ < 2) {
                char buf[MAX_PATH];
                egist_snprintf(buf, MAX_PATH, "!! ET711 retry count = %d\n", retry);
                OutputDebugString(buf);

                goto ET711_GETIMAGE_WORKAROUND;
            }
            return FALSE;
        }

        /*
         double getimage_time = QueryExecutionTimeMs();
         char buf[MAX_PATH];
         egist_snprintf(buf, "getframe time=%.3f\n", getimage_time);
         OutputDebugString(buf);
         */
    }

    return TRUE;
}

BOOL FUNCTION_PREFIX bGetFrame16(unsigned short* pFrame, int drops) {
    BOOL r_value;

    for (; drops > 0; drops--) {
        r_value = bGetFrameRaw(this->raw_image, this->sensor_total_pixels_raw);
        if (!r_value) {
            // bDumpRegister("bGetFrameRaw Fail", FALSE);
            return FALSE;
        }
    }

    r_value = bGetFrameRaw(this->raw_image, this->sensor_total_pixels_raw);
    if (!r_value) {
        // bDumpRegister("bGetFrameRaw Fail", FALSE);
        return FALSE;
    }

    int i, j;
    unsigned short pixel;

    for (j = 0; j < this->sensor_height; j++) {
        for (i = 0; i < this->sensor_width; i++) {
            if (this->sensor_id.minor_id == 29) {
                // et729
                // shift 6 dark rows
                pixel = this->raw_image[(j + 6) * this->sensor_width_raw + i * 2] << 8;
                pixel += this->raw_image[(j + 6) * this->sensor_width_raw + i * 2 + 1];
            } else if (this->sensor_id.minor_id == 13) {
                // et713
                pixel = this->raw_image[j * this->sensor_width_raw + (i + 1) * 2] << 8;
                pixel += this->raw_image[j * this->sensor_width_raw + (i + 1) * 2 + 1];
            } else {
                // et711
                pixel = this->raw_image[j * this->sensor_width_raw + i * 2] << 8;
                pixel += this->raw_image[j * this->sensor_width_raw + i * 2 + 1];
            }

            // rotate to right (90 degree)
            pFrame[this->sensor_height * i + j] = pixel;
        }
    }

#if (0)
    FILE* fp = fopen("raw.csv", "w");
    if (fp) {
        for (i = 0; i < this->sensor_width_raw / 2; i++) {
            fprintf(fp, ",%d,", i);
        }
        fprintf(fp, "\n");

        for (j = 0; j < this->sensor_height_raw; j++) {
            fprintf(fp, "%d,", j);
            for (i = 0; i < this->sensor_width_raw; i++) {
                fprintf(fp, "%d,", this->raw_image[j * this->sensor_width_raw + i]);
            }
            fprintf(fp, "\n");
        }
        fclose(fp);
    }
#endif

    return TRUE;
}

BOOL FUNCTION_PREFIX bGetAverageFrameAndStdDev16(unsigned short* pData, double* pStdDev, int nFrame,
                                                 int* pBackground) {
    int total_pixels;
    total_pixels = this->sensor_total_pixels;

    int i, j;
    unsigned short* pAllFrames;

    pAllFrames = (unsigned short*)malloc(total_pixels * sizeof(unsigned short) * nFrame);

    if (!pAllFrames) {
        return FALSE;
    }
    memset(pData, 0x00, total_pixels * sizeof(unsigned short));

    BOOL r_value;

    for (i = 0; i < nFrame; i++) {
        r_value = bGetFrame16(pAllFrames + i * total_pixels, 0);
        if (!r_value) {
            free(pAllFrames);
            return FALSE;
        }
    }

    // Average every pixel into output buffer
    for (i = 0; i < total_pixels; i++) {
        int pxl_sum = 0;
        for (j = 0; j < nFrame; j++) {
            pxl_sum += pAllFrames[total_pixels * j + i];
            if (pBackground) {
                pxl_sum -= pBackground[i];
            }
        }

        if ((pxl_sum / nFrame) > 0xFFFF) {
            pData[i] = 0xFFFF;
        } else if ((pxl_sum / nFrame) < 0) {
            pData[i] = 0;
        } else {
            pData[i] = pxl_sum / nFrame;  // 10 frames average.
        }

        if (pStdDev) {
            pxl_sum = 0;
            for (j = 0; j < nFrame; j++) {
                pxl_sum += pAllFrames[total_pixels * j + i];
            }
            double pxl_avg = 0;
            pxl_avg = (double)pxl_sum / (double)nFrame;

            double sum2 = 0;
            for (j = 0; j < nFrame; j++) {
                sum2 += ((double)pAllFrames[total_pixels * j + i] - (double)pxl_avg) *
                        ((double)pAllFrames[total_pixels * j + i] - (double)pxl_avg);
            }

            pStdDev[i] = sqrt(sum2 / nFrame);
        }
    }

    free(pAllFrames);
    return TRUE;
}

BOOL FUNCTION_PREFIX bGetIntegratedFrameAndStdDev16(unsigned short* pData, double* pStdDev,
                                                    int nFrame, int* pBackground) {
    int total_pixels;
    total_pixels = this->sensor_total_pixels;

    int i, j;
    unsigned short* pAllFrames;

    pAllFrames = (unsigned short*)malloc(total_pixels * sizeof(unsigned short) * nFrame);

    if (!pAllFrames) {
        return FALSE;
    }
    memset(pData, 0x00, total_pixels * sizeof(unsigned short));

    BOOL r_value;

    for (i = 0; i < nFrame; i++) {
        r_value = bGetFrame16(pAllFrames + i * total_pixels, 0);
        if (!r_value) {
            free(pAllFrames);
            return FALSE;
        }
    }

    // add every pixel into output buffer
    for (i = 0; i < total_pixels; i++) {
        int pxl_sum = 0;

        for (j = 0; j < nFrame; j++) {
            pxl_sum += pAllFrames[total_pixels * j + i];
        }

        if (pBackground) {
            pxl_sum -= pBackground[i] * nFrame;
            if (pxl_sum > 0xFFFF) {
                pData[i] = 0xFFFF;
            } else if (pxl_sum < 0) {
                pData[i] = 0;
            } else {
                pData[i] = pxl_sum;
            }
        } else {
            pData[i] = pxl_sum;
        }

        if (pStdDev) {
            pxl_sum = 0;
            for (j = 0; j < nFrame; j++) {
                pxl_sum += pAllFrames[total_pixels * j + i];
            }

            double pxl_avg = 0;
            pxl_avg = (double)pxl_sum / (double)nFrame;

            double sum2 = 0;
            for (j = 0; j < nFrame; j++) {
                sum2 += ((double)pAllFrames[total_pixels * j + i] - (double)pxl_avg) *
                        ((double)pAllFrames[total_pixels * j + i] - (double)pxl_avg);
            }

            pStdDev[i] = sqrt(sum2 / nFrame);
        }
    }

    free(pAllFrames);
    return TRUE;
}

float FUNCTION_PREFIX fPGAControltoPGAGain(unsigned char reg_value) {
    if (reg_value & 0x02) {
        return 1.0;
    } else if (reg_value & 0x04) {
        return 2.0;
    } else {
        return 0.0;
    }
}

BOOL FUNCTION_PREFIX bReadMeters(double* pCurrent33, double* pCurrent18, int* pVoltage33,
                                 int* pVoltage18) {
#ifdef WIN32
    BOOL r_value;

    double meter_resistor;
    meter_resistor = 0.1;

    unsigned char addr;
    unsigned short usvalue;

    double sensor_current;
    int sensor_voltage;

    // reset current measurement //////////////////////////////////////
    // Meter1. Analog 3.3V
    // Set Average and Conversion time.
    r_value = bSetSelectedMeter(0x01);
    if (!r_value) {
        return FALSE;
    }

    addr = 0x00;
    usvalue = 0x0000;
    r_value = bReadMeterRegister(addr, &usvalue);
    if (!r_value) {
        return FALSE;
    }

    usvalue &= 0xF1C7;
    usvalue |= METER_AVERAGE << 9;
    usvalue |= METER_CONVERSION_TIME << 3;
    r_value = bWriteMeterRegister(addr, usvalue);
    if (!r_value) {
        return FALSE;
    }

    // Meter2. Digital 1.8V
    // Set Average and Conversion time.
    r_value = bSetSelectedMeter(0x02);
    if (!r_value) {
        return FALSE;
    }

    addr = 0x00;
    usvalue = 0x0000;
    r_value = bReadMeterRegister(addr, &usvalue);
    if (!r_value) {
        return FALSE;
    }
    usvalue &= 0xF1C7;
    usvalue |= METER_AVERAGE << 9;
    usvalue |= METER_CONVERSION_TIME << 3;
    r_value = bWriteMeterRegister(addr, usvalue);
    if (!r_value) {
        return FALSE;
    }

    // Wait meter to complete conversion
    Sleep(METER_PAUSE);

    // Meter1. Analog 3.3V ///////////////////////////////////////
    r_value = bSetSelectedMeter(0x01);
    if (!r_value) {
        return FALSE;
    }

    // current
    addr = 0x01;
    usvalue = 0x0000;
    r_value = bReadMeterRegister(addr, &usvalue);
    if (!r_value) {
        return FALSE;
    }
    if ((short)usvalue < 0) {
        usvalue = 0;
    }
    sensor_current = usvalue * 2.5 / (meter_resistor * 1000);

    // voltage
    addr = 0x02;
    usvalue = 0x0000;
    r_value = bReadMeterRegister(addr, &usvalue);
    if (!r_value) {
        return FALSE;
    }
    if ((short)usvalue < 0) {
        usvalue = 0;
    }
    sensor_voltage = (int)(usvalue * 1.25);

    if (pCurrent33) {
        *pCurrent33 = sensor_current;
    }
    if (pVoltage33) {
        *pVoltage33 = sensor_voltage;
    }

    // Meter2. Digital 1.8V ///////////////////////////////////////
    r_value = bSetSelectedMeter(0x02);
    if (!r_value) {
        return FALSE;
    }

    // current
    addr = 0x01;
    usvalue = 0x0000;
    r_value = bReadMeterRegister(addr, &usvalue);
    if (!r_value) {
        return FALSE;
    }
    if ((short)usvalue < 0) {
        usvalue = 0;
    }
    sensor_current = usvalue * 2.5 / (meter_resistor * 1000);

    // voltage
    addr = 0x02;
    usvalue = 0x0000;
    r_value = bReadMeterRegister(addr, &usvalue);
    if (!r_value) {
        return FALSE;
    }
    if ((short)usvalue < 0) {
        usvalue = 0;
    }
    sensor_voltage = (int)(usvalue * 1.25);

    if (pCurrent18) {
        *pCurrent18 = sensor_current;
    }
    if (pVoltage18) {
        *pVoltage18 = sensor_voltage;
    }
#endif
    return TRUE;
}

BOOL FUNCTION_PREFIX bSetExposureTime(double* pExposureTime) {
#if (EGIS_SENSOR == 711)
    int exp_time_unit = 44;  // 44us
#elif (EGIS_SENSOR == 713)
    int exp_time_unit = 54;  // 54us
#else
    int exp_time_unit = 104;  // 104us
#endif

    BOOL r_value;

    unsigned short target_exp_time_sw;
    double target_expo_time;

#if defined(WIN32)
    if (sensor_id.pixel_id == 1) {
        target_expo_time = *pExposureTime * 0.8;
    } else if (sensor_id.pixel_id == 2) {
        target_expo_time = *pExposureTime * 0.9;
    } else {
        target_expo_time = *pExposureTime * 1.0;
    }
#else
    if (this->sensor_id.pixel_id == 1) {
        target_expo_time = *pExposureTime * 0.8;
    } else if (this->sensor_id.pixel_id == 2) {
        target_expo_time = *pExposureTime * 0.9;
    } else {
        target_expo_time = *pExposureTime * 1.0;
    }
#endif

    // Exposure Time = (E+1) * 44us
    target_exp_time_sw = (unsigned short)((target_expo_time * 1000) / exp_time_unit) - 1;

    r_value = bWriteRegisterUs(EXP_TIME_SW_H, target_exp_time_sw);
    if (!r_value) {
        return FALSE;
    }

    return TRUE;
}

BOOL FUNCTION_PREFIX bRegisterReadWriteTest(BOOL bDoReadWriteTest) {
#if (SHOW_REG_TEST_FAIL)
    char buf[MAX_PATH];
#endif

    // Register Test
    int i;
    BOOL r_value;
    unsigned char write_value;

    for (i = 0; i <= MAX_REG_ADDR; i++) {
        if (this->sensor_reg_all[i].RegType & REG_TYPE_READ) {
            r_value = bReadRegister(i, &this->sensor_reg_all[i].RegValue);
            if (!r_value) {
                return FALSE;
            }
            egislog_d("SHOW_REG_TEST[%d] 0x0%x %d", __LINE__, i, this->sensor_reg_all[i].RegValue);
            if ((this->sensor_reg_all[i].RegValue & this->sensor_reg_all[i].RegValueMask) !=
                this->sensor_reg_all[i].RegValueDefault) {
#if (EGIS_SENSOR == 711)
                // workaround for et711 and et729
                if ((this->sensor_reg_all[i].RegValue & this->sensor_reg_all[i].RegValueMask) ==
                    this->sensor_reg_all[i].RegValueDefaultAlt) {
                    continue;
                }
#endif

#if (SHOW_REG_TEST_FAIL)
                egist_snprintf(buf, MAX_PATH, "Default Register Value Test Fail!\n");
                OutputDebugString(buf);

                egist_snprintf(buf, MAX_PATH, "ADDR:%04X Read:%02X Default:%02X Mask:%02X\n", i,
                               this->sensor_reg_all[i].RegValue,
                               this->sensor_reg_all[i].RegValueDefault,
                               this->sensor_reg_all[i].RegValueMask);
                OutputDebugString(buf);
#endif
                return FALSE;
            }
        }
    }

    egist_snprintf(buf, MAX_PATH, "Default Register Value Test pass!\n");
    OutputDebugString(buf);

    if (!bDoReadWriteTest) {
        return TRUE;
    }

    // Write 0x00 Register Value Test
    write_value = 0x00;
    for (i = 0; i <= MAX_REG_ADDR; i++) {
        if (this->sensor_reg_all[i].RegType & REG_TYPE_WRITE) {
            egislog_d("i = %d  write_value = %d", i, write_value);
            r_value = bWriteRegister(i, write_value);
            if (!r_value) {
                return FALSE;
            }
            r_value = bReadRegister(i, &this->sensor_reg_all[i].RegValue);
            if (!r_value) {
                return FALSE;
            }
            if (this->sensor_reg_all[i].RegValue !=
                (write_value & this->sensor_reg_all[i].RegValueMask)) {
#if (SHOW_REG_TEST_FAIL)
                egist_snprintf(buf, MAX_PATH, "0x00 Write Register Value Test Fail!\n");
                OutputDebugString(buf);

                egist_snprintf(buf, MAX_PATH, "ADDR:%04X Read:%02X Mask:%02X\n", i,
                               this->sensor_reg_all[i].RegValue,
                               this->sensor_reg_all[i].RegValueMask);
                OutputDebugString(buf);
#endif
                return FALSE;
            }
        }
    }

    egist_snprintf(buf, MAX_PATH, "Write 0x00 Register Value Test pass!\n");
    OutputDebugString(buf);

    // 0xFF Register Value Test
    for (i = 0; i <= MAX_REG_ADDR; i++) {
        if (this->sensor_reg_all[i].RegType & REG_TYPE_WRITE) {
            write_value = 0xFF & this->sensor_reg_all[i].RegValueMask;
            r_value = bWriteRegister(i, write_value);
            if (!r_value) {
                return FALSE;
            }
            r_value = bReadRegister(i, &this->sensor_reg_all[i].RegValue);
            if (!r_value) {
                return FALSE;
            }
            if (this->sensor_reg_all[i].RegValue !=
                (write_value & this->sensor_reg_all[i].RegValueMask)) {
#if (SHOW_REG_TEST_FAIL)
                egist_snprintf(buf, MAX_PATH, "0xFF Write Register Value Test Fail!\n");
                OutputDebugString(buf);

                egist_snprintf(buf, MAX_PATH, "ADDR:%04X Read:%02X Mask:%02X\n", i,
                               this->sensor_reg_all[i].RegValue,
                               this->sensor_reg_all[i].RegValueMask);
                OutputDebugString(buf);
#endif
                return FALSE;
            }
        }
    }

    egist_snprintf(buf, MAX_PATH, "Write 0xFF Register Value Test pass!\n");
    OutputDebugString(buf);

    // 0x5A Register Value Test
    for (i = 0; i <= MAX_REG_ADDR; i++) {
        if (this->sensor_reg_all[i].RegType & REG_TYPE_WRITE) {
            write_value = 0x5A & this->sensor_reg_all[i].RegValueMask;
            r_value = bWriteRegister(i, write_value);
            if (!r_value) {
                return FALSE;
            }
            r_value = bReadRegister(i, &this->sensor_reg_all[i].RegValue);
            if (!r_value) {
                return FALSE;
            }
            if (this->sensor_reg_all[i].RegValue !=
                (write_value & this->sensor_reg_all[i].RegValueMask)) {
#if (SHOW_REG_TEST_FAIL)
                egist_snprintf(buf, MAX_PATH, "0x5A Write Register Value Test Fail!\n");
                OutputDebugString(buf);

                egist_snprintf(buf, MAX_PATH, "ADDR:%04X Read:%02X Mask:%02X\n", i,
                               this->sensor_reg_all[i].RegValue,
                               this->sensor_reg_all[i].RegValueMask);
                OutputDebugString(buf);
#endif
                return FALSE;
            }
        }
    }

    egist_snprintf(buf, MAX_PATH, "Write 0x5A Register Value Test pass!\n");
    OutputDebugString(buf);

    // 0xA5 Register Value Test
    for (i = 0; i <= MAX_REG_ADDR; i++) {
        if (this->sensor_reg_all[i].RegType & REG_TYPE_WRITE) {
            write_value = 0xA5 & this->sensor_reg_all[i].RegValueMask;
            r_value = bWriteRegister(i, write_value);
            if (!r_value) {
                return FALSE;
            }
            r_value = bReadRegister(i, &this->sensor_reg_all[i].RegValue);
            if (!r_value) {
                return FALSE;
            }
            if (this->sensor_reg_all[i].RegValue !=
                (write_value & this->sensor_reg_all[i].RegValueMask)) {
#if (SHOW_REG_TEST_FAIL)
                egist_snprintf(buf, MAX_PATH, "0xA5 Write Register Value Test Fail!\n");
                OutputDebugString(buf);

                egist_snprintf(buf, MAX_PATH, "ADDR:%04X Read:%02X Mask:%02X\n", i,
                               this->sensor_reg_all[i].RegValue,
                               this->sensor_reg_all[i].RegValueMask);
                OutputDebugString(buf);
#endif
                return FALSE;
            }
        }
    }

    egist_snprintf(buf, MAX_PATH, "Write 0xA5 Register Value Test pass!\n");
    OutputDebugString(buf);

    return TRUE;
}

#if defined(WIN32)
#if (EGIS_SENSOR == 711)
#include "../../inc/et711_reg_init.h"
#elif (EGIS_SENSOR == 713)
#include "../../inc/et713_reg_init.h"
#elif (EGIS_SENSOR == 701)
#include "../../inc/et701_reg_init.h"
#endif
#endif

BOOL FUNCTION_PREFIX bDumpRegister(char* pReason, BOOL bShowAll) {
    char buf[MAX_PATH];

    egist_snprintf(buf, MAX_PATH, "DrumpRegister: %s\n", pReason);
    OutputDebugString(buf);

    int i;
    BOOL r_value;
    unsigned short addr;
    unsigned char value;

    if (bShowAll) {
        for (i = 0; i <= MAX_REG_ADDR; i++) {
            addr = i;
            value = 0x00;

            r_value = bReadRegister(addr, &value);
            if (!r_value) {
                OutputDebugString("bReadRegister Fail.\n");
                return FALSE;
            }

            egist_snprintf(buf, MAX_PATH, "0x%04X 0x%02X\n", addr, value);
            OutputDebugString(buf);
        }
    } else {
        for (i = 0; i < REG_NUM; i++) {
            addr = reg_addr[i];
            value = 0x00;

            r_value = bReadRegister(addr, &value);
            if (!r_value) {
                OutputDebugString("bReadRegister Fail.\n");
                return FALSE;
            }

            egist_snprintf(buf, MAX_PATH, "%s 0x%04X 0x%02X\n", reg_name[i], addr, value);
            OutputDebugString(buf);
        }
    }

    return TRUE;
}

#ifdef WIN32
BOOL CSensor::bDumpRegister(unsigned short addr) {
    char buf[MAX_PATH];
    BOOL r_value;
    unsigned char value;

    r_value = bReadRegister(addr, &value);
    if (!r_value) {
        OutputDebugString("bReadRegister Fail.\n");
        return FALSE;
    }

    int i;
    for (i = 0; i < REG_NUM; i++) {
        if (addr == reg_addr[i]) {
            break;
        }
    }

    if (i == REG_NUM) {
        egist_snprintf(buf, MAX_PATH, "Register Addr:0x%04X Value:0x%02X\n", addr, value);
    } else {
        egist_snprintf(buf, MAX_PATH, "Register %s\tAddr:0x%04X Value:0x%02X\n", reg_name[i], addr,
                       value);
    }

    OutputDebugString(buf);
    return TRUE;
}
#endif

// Win32 only functions
// /////////////////////////////////////////////////////////////////////////////
#ifdef WIN32
BOOL CSensor::bWritComPort(unsigned char* pWriteBuf, int iWriteBufLen) {
    if (m_hCom == INVALID_HANDLE_VALUE) {
        return FALSE;
    }

    COMSTAT ComStat;
    DWORD dwErrorFlags;
    DWORD dwDataWritten;
    BOOL bWriteStat;

    ClearCommError(m_hCom, &dwErrorFlags, &ComStat);

    bWriteStat = WriteFile(m_hCom, pWriteBuf, iWriteBufLen, &dwDataWritten, NULL);
    if (!bWriteStat) {
        return FALSE;
    }

    return TRUE;
}

BOOL CSensor::bReadComPort(unsigned char* pReadBuf, int iReadBufLen, int timeout) {
    if (m_hCom == INVALID_HANDLE_VALUE) {
        return FALSE;
    }

    DWORD st_begin;
    DWORD st_end;

    st_begin = GetTickCount();

    BOOL bTimeOut = FALSE;

    BYTE read_data[65536 * 2];
    DWORD read_count;

    BYTE DataQueue[65536 * 2];
    int DataQueueLen;

    BOOL r_value;

    memset(DataQueue, 0x00, sizeof(DataQueue));
    DataQueueLen = 0;

    while (!bTimeOut) {
        memset(read_data, 0x00, sizeof(read_data));
        read_count = 65536;

        r_value = ReadFile(m_hCom, read_data, read_count, &read_count, NULL);
        if ((r_value) && (read_count > 0)) {
            char buf[MAX_PATH];
            egist_snprintf(buf, MAX_PATH, "%d data received.\n", read_count);
            // OutputDebugString(buf);

            memcpy(DataQueue + DataQueueLen, read_data, read_count);
            DataQueueLen += read_count;

            // Reset Timer
            st_begin = GetTickCount();

            if (DataQueueLen >= iReadBufLen) {
                break;
            }
        }

        st_end = GetTickCount();

        int mille_time_passed = st_end - st_begin;

        if (mille_time_passed > timeout) {
            bTimeOut = TRUE;
        }
    }

    PurgeComm(m_hCom, PURGE_TXABORT | PURGE_RXABORT | PURGE_TXCLEAR | PURGE_RXCLEAR);

    if (bTimeOut) {
        if (DataQueueLen) {
            OutputDebugString("????\n");
        }
        return FALSE;
    }

    memcpy(pReadBuf, DataQueue, iReadBufLen);

    return TRUE;
}

BOOL CSensor::bSetGPIOMode(unsigned char Pin, unsigned char Mode) {
    if (target_board_type == TESTER_BOARD_ARDUINO) {
        BOOL r_value;
        unsigned char command[512];
        unsigned char buf[512];

        memset(command, 0x00, sizeof(command));

        // Signature
        command[0] = 0x45;  // 'E'
        command[1] = 0x47;  // 'G'
        command[2] = 0x49;  // 'I'
        command[3] = 0x53;  // 'S'

        // Operation code
        command[4] = 0x20;  // Set Pin Mode

        // Parameter
        command[5] = Pin;

        // Value
        command[6] = Mode;  // 0:input 1:output

#if defined(WIN32)
        // Write Command
        r_value = bWritComPort(command, ARDUINO_COMMAND_LENGTH);
        if (!r_value) {
            return FALSE;
        }

        // Read Response
        r_value = bReadComPort(buf, ARDUINO_RESPONSE_LENGTH, READ_TIMEOUT);
        if (!r_value) {
            return FALSE;
        }
#endif

        if ((buf[0] != 'S') || (buf[1] != 'I') || (buf[2] != 'G') || (buf[3] != 'E')) {
            return FALSE;
        }

        if (buf[6] != 0x01) {
            return FALSE;
        }

        return TRUE;
    }
    return TRUE;
}

BOOL CSensor::bGetGPIOData(unsigned char Pin, unsigned char* pData) {
    if (target_board_type == TESTER_BOARD_ARDUINO) {
        BOOL r_value;
        unsigned char command[512];
        unsigned char buf[512];

        memset(command, 0x00, sizeof(command));

        // Signature
        command[0] = 0x45;  // 'E'
        command[1] = 0x47;  // 'G'
        command[2] = 0x49;  // 'I'
        command[3] = 0x53;  // 'S'

        // Operation code
        command[4] = 0x22;  // Get GPIO Data

        // Parameter
        command[5] = Pin;

        // Value
        command[6] = 0x00;  // Don't Care

#if defined(WIN32)
        // Write Command
        r_value = bWritComPort(command, ARDUINO_COMMAND_LENGTH);
        if (!r_value) {
            return FALSE;
        }

        // Read Response
        r_value = bReadComPort(buf, ARDUINO_RESPONSE_LENGTH, READ_TIMEOUT);
        if (!r_value) {
            return FALSE;
        }
#endif

        if ((buf[0] != 'S') || (buf[1] != 'I') || (buf[2] != 'G') || (buf[3] != 'E')) {
            return FALSE;
        }

        if (buf[6] != 0x01) {
            return FALSE;
        }

        *pData = buf[5];

        return TRUE;
    }
    return TRUE;
}

BOOL CSensor::bSetGPIOData(unsigned char Pin, unsigned char Data) {
    if (target_board_type == TESTER_BOARD_ARDUINO) {
        BOOL r_value;
        unsigned char command[512];
        unsigned char buf[512];

        memset(command, 0x00, sizeof(command));

        // Signature
        command[0] = 0x45;  // 'E'
        command[1] = 0x47;  // 'G'
        command[2] = 0x49;  // 'I'
        command[3] = 0x53;  // 'S'

        // Operation code
        command[4] = 0x21;  // Get GPIO Data

        // Parameter
        command[5] = Pin;

        // Value
        command[6] = Data;

#if defined(WIN32)
        // Write Command
        r_value = bWritComPort(command, ARDUINO_COMMAND_LENGTH);
        if (!r_value) {
            return FALSE;
        }

        // Read Response
        r_value = bReadComPort(buf, ARDUINO_RESPONSE_LENGTH, READ_TIMEOUT);
        if (!r_value) {
            return FALSE;
        }
#endif

        if ((buf[0] != 'S') || (buf[1] != 'I') || (buf[2] != 'G') || (buf[3] != 'E')) {
            return FALSE;
        }

        if (buf[6] != 0x01) {
            return FALSE;
        }

        return TRUE;
    }
    return TRUE;
}

BOOL CSensor::bInitAllGPIO() {
    BOOL r_value;

    // input
    r_value = bSetGPIOMode(GPIO_READ_MANUAL_AUTO_PIN, 0x00);
    if (!r_value) {
        return FALSE;
    }
    r_value = bSetGPIOData(GPIO_READ_MANUAL_AUTO_PIN, 0x01);
    if (!r_value) {
        return FALSE;
    }

    r_value = bSetGPIOMode(GPIO_START_PIN, 0x00);
    if (!r_value) {
        return FALSE;
    }
    r_value = bSetGPIOData(GPIO_START_PIN, 0x01);
    if (!r_value) {
        return FALSE;
    }

    r_value = bSetGPIOMode(GPIO_EMERGENCE_PIN, 0x00);
    if (!r_value) {
        return FALSE;
    }
    r_value = bSetGPIOData(GPIO_EMERGENCE_PIN, 0x01);
    if (!r_value) {
        return FALSE;
    }

    // output
    r_value = bSetGPIOMode(GPIO_SITE1_LEDR_PIN, 0x01);
    if (!r_value) {
        return FALSE;
    }
    r_value = bSetGPIOData(GPIO_SITE1_LEDR_PIN, 0x01);
    if (!r_value) {
        return FALSE;
    }
    r_value = bSetGPIOMode(GPIO_SITE1_LEDG_PIN, 0x01);
    if (!r_value) {
        return FALSE;
    }
    r_value = bSetGPIOData(GPIO_SITE1_LEDG_PIN, 0x01);
    if (!r_value) {
        return FALSE;
    }

    r_value = bSetGPIOMode(GPIO_SITE2_LEDR_PIN, 0x01);
    if (!r_value) {
        return FALSE;
    }
    r_value = bSetGPIOData(GPIO_SITE2_LEDR_PIN, 0x01);
    if (!r_value) {
        return FALSE;
    }
    r_value = bSetGPIOMode(GPIO_SITE2_LEDG_PIN, 0x01);
    if (!r_value) {
        return FALSE;
    }
    r_value = bSetGPIOData(GPIO_SITE2_LEDG_PIN, 0x01);
    if (!r_value) {
        return FALSE;
    }

    r_value = bSetGPIOMode(GPIO_SITE3_LEDR_PIN, 0x01);
    if (!r_value) {
        return FALSE;
    }
    r_value = bSetGPIOData(GPIO_SITE3_LEDR_PIN, 0x01);
    if (!r_value) {
        return FALSE;
    }
    r_value = bSetGPIOMode(GPIO_SITE3_LEDG_PIN, 0x01);
    if (!r_value) {
        return FALSE;
    }
    r_value = bSetGPIOData(GPIO_SITE3_LEDG_PIN, 0x01);
    if (!r_value) {
        return FALSE;
    }

    r_value = bSetGPIOMode(GPIO_SITE4_LEDR_PIN, 0x01);
    if (!r_value) {
        return FALSE;
    }
    r_value = bSetGPIOData(GPIO_SITE4_LEDR_PIN, 0x01);
    if (!r_value) {
        return FALSE;
    }
    r_value = bSetGPIOMode(GPIO_SITE4_LEDG_PIN, 0x01);
    if (!r_value) {
        return FALSE;
    }
    r_value = bSetGPIOData(GPIO_SITE4_LEDG_PIN, 0x01);
    if (!r_value) {
        return FALSE;
    }

    return TRUE;
}

BOOL CSensor::bGetInterruptPin(unsigned char* pStatus) {
    if (target_board_type == TESTER_BOARD_ARDUINO) {
        BOOL r_value;
        unsigned char GPIO;

        r_value = bGetGPIO(&GPIO);
        if (!r_value) {
            return FALSE;
        }

        if (GPIO & 0x08) {
            *pStatus = 0x01;
        } else {
            *pStatus = 0x00;
        }

        return TRUE;
    }
    return TRUE;
}

BOOL CSensor::bET711_GetImage(int imagesize, unsigned char* pData) {
    BOOL r_value;
    unsigned char command[512];
    memset(command, 0x00, sizeof(command));

    // Signature
    command[0] = 0x45;  // 'E'
    command[1] = 0x47;  // 'G'
    command[2] = 0x49;  // 'I'
    command[3] = 0x53;  // 'S'

    // Operation code
    command[4] = 0x64;  // get image opcode

    // len high byte
    command[5] = (imagesize & 0xFF00) >> 8;
    command[6] = (imagesize & 0xFF);
    command[7] = (imagesize & 0xFF0000) >> 16;  // for et713

#if defined(WIN32)
    // Write Command
    r_value = bWritComPort(command, ARDUINO_COMMAND_LENGTH);
    if (!r_value) {
        return FALSE;
    }

    // Read Image
    r_value = bReadComPort(pData, imagesize, READ_TIMEOUT);
    if (!r_value) {
        return FALSE;
    }
#endif

    return TRUE;
}

BOOL CSensor::bGetGPIO(unsigned char* pgpio) {
    if (target_board_type == TESTER_BOARD_ARDUINO) {
        BOOL r_value;
        unsigned char command[512];
        unsigned char buf[512];

        memset(command, 0x00, sizeof(command));

        // Signature
        command[0] = 0x45;  // 'E'
        command[1] = 0x47;  // 'G'
        command[2] = 0x49;  // 'I'
        command[3] = 0x53;  // 'S'

        // Operation code
        command[4] = 0x03;  // Get GPIO

        // Parameter
        command[5] = 0x00;  // Don't care

        // Value
        command[6] = 0x00;  // Don't care

#if defined(WIN32)
        // Write Command
        r_value = bWritComPort(command, ARDUINO_COMMAND_LENGTH);
        if (!r_value) {
            return FALSE;
        }

        // Read Response
        r_value = bReadComPort(buf, ARDUINO_RESPONSE_LENGTH, READ_TIMEOUT);
        if (!r_value) {
            return FALSE;
        }
#endif

        if (buf[0] != 'S') {
            return FALSE;
        }
        if (buf[1] != 'I') {
            return FALSE;
        }
        if (buf[2] != 'G') {
            return FALSE;
        }
        if (buf[3] != 'E') {
            return FALSE;
        }

        // buf[4]: param
        *pgpio = buf[5];

        if (buf[6] != 0x01) {
            return FALSE;
        }

        return TRUE;
    } else {
        return FALSE;
    }
}

BOOL CSensor::bInitSPI(int mode, int spi_clk) {
    if (target_board_type == TESTER_BOARD_ARDUINO) {
        BOOL r_value;
        unsigned char command[512];
        unsigned char buf[512];

        memset(command, 0x00, sizeof(command));
        memset(buf, 0x00, sizeof(buf));

        // Signature
        command[0] = 0x45;  // 'E'
        command[1] = 0x47;  // 'G'
        command[2] = 0x49;  // 'I'
        command[3] = 0x53;  // 'S'

        // Operation code
        command[4] = 0x07;  // OP Code: Set SPI Speed

        command[5] = mode;     // SPI_MODE: 0, 1, 2, 3
        command[6] = spi_clk;  // SPI Speed. 0x01=1MB.....

#if defined(WIN32)
        // Write Command
        r_value = bWritComPort(command, ARDUINO_COMMAND_LENGTH);
        if (!r_value) {
            return FALSE;
        }

        // Read Response
        r_value = bReadComPort(buf, ARDUINO_RESPONSE_LENGTH, READ_TIMEOUT);
        if (!r_value) {
            return FALSE;
        }
#endif

        if (buf[0] != 'S') {
            return FALSE;
        }
        if (buf[1] != 'I') {
            return FALSE;
        }
        if (buf[2] != 'G') {
            return FALSE;
        }
        if (buf[3] != 'E') {
            return FALSE;
        }

        if (buf[6] != 0x01) {
            return FALSE;
        }

        return TRUE;
    } else {
        return FALSE;
    }
}

BOOL CSensor::bSetGPIO(unsigned char gpio) {
    if (target_board_type == TESTER_BOARD_ARDUINO) {
        BOOL r_value;
        unsigned char command[512];
        unsigned char buf[512];

        memset(command, 0x00, sizeof(command));

        // Signature
        command[0] = 0x45;  // 'E'
        command[1] = 0x47;  // 'G'
        command[2] = 0x49;  // 'I'
        command[3] = 0x53;  // 'S'

        // Operation code
        command[4] = 0x04;  // Set GPIO P1

        // Parameter
        command[5] = 0x00;  // Don't care

        // Value
        command[6] = gpio;  // GPIO

#if defined(WIN32)
        // Write Command
        r_value = bWritComPort(command, ARDUINO_COMMAND_LENGTH);
        if (!r_value) {
            return FALSE;
        }

        // Read Response
        r_value = bReadComPort(buf, ARDUINO_RESPONSE_LENGTH, READ_TIMEOUT);
        if (!r_value) {
            return FALSE;
        }
#endif

        if (buf[0] != 'S') {
            return FALSE;
        }
        if (buf[1] != 'I') {
            return FALSE;
        }
        if (buf[2] != 'G') {
            return FALSE;
        }
        if (buf[3] != 'E') {
            return FALSE;
        }

        if (buf[6] != 0x01) {
            return FALSE;
        }

        return TRUE;
    } else {
        return FALSE;
    }
}

// INA230 ///////////////////////////////////////////////////////////
BOOL CSensor::bSetI2CClock(unsigned int i2c_clock) {
    if (target_board_type == TESTER_BOARD_ARDUINO) {
        BOOL r_value;
        unsigned char command[512];
        unsigned char buf[512];

        memset(command, 0x00, sizeof(command));

        unsigned short i2c_clock_div_k;
        i2c_clock_div_k = i2c_clock / 1000;

        // Signature
        command[0] = 0x45;  // 'E'
        command[1] = 0x47;  // 'G'
        command[2] = 0x49;  // 'I'
        command[3] = 0x53;  // 'S'

        // Operation code
        command[4] = 0x36;  // Set I2C Clock

        // i2c clock. unit is 1000Hz
        command[5] = (i2c_clock_div_k & 0xFF00) >> 8;
        command[6] = i2c_clock_div_k & 0x00FF;

#if defined(WIN32)
        // Write Command
        r_value = bWritComPort(command, ARDUINO_COMMAND_LENGTH);
        if (!r_value) {
            return FALSE;
        }

        // Read Response
        r_value = bReadComPort(buf, ARDUINO_RESPONSE_LENGTH, READ_TIMEOUT);
        if (!r_value) {
            return FALSE;
        }
#endif

        if (buf[0] != 'S') {
            return FALSE;
        }
        if (buf[1] != 'I') {
            return FALSE;
        }
        if (buf[2] != 'G') {
            return FALSE;
        }
        if (buf[3] != 'E') {
            return FALSE;
        }

        if (buf[6] != 0x01) {
            return FALSE;
        }

        return TRUE;
    } else {
        return TRUE;
    }
}

BOOL CSensor::bSetMeterI2CID(unsigned char ucMeter, unsigned char ucI2CID) {
    meter_id[ucMeter - 1] = ucI2CID;

    if (target_board_type == TESTER_BOARD_ARDUINO) {
        BOOL r_value;
        unsigned char command[512];
        unsigned char buf[512];

        memset(command, 0x00, sizeof(command));

        // Signature
        command[0] = 0x45;  // 'E'
        command[1] = 0x47;  // 'G'
        command[2] = 0x49;  // 'I'
        command[3] = 0x53;  // 'S'

        // Operation code
        command[4] = 0x30;  // Set Meter I2C ID

        // Parameter
        command[5] = ucMeter;

        // Value
        command[6] = ucI2CID;

#if defined(WIN32)
        // Write Command
        r_value = bWritComPort(command, ARDUINO_COMMAND_LENGTH);
        if (!r_value) {
            return FALSE;
        }

        // Read Response
        r_value = bReadComPort(buf, ARDUINO_RESPONSE_LENGTH, READ_TIMEOUT);
        if (!r_value) {
            return FALSE;
        }
#endif

        if (buf[0] != 'S') {
            return FALSE;
        }
        if (buf[1] != 'I') {
            return FALSE;
        }
        if (buf[2] != 'G') {
            return FALSE;
        }
        if (buf[3] != 'E') {
            return FALSE;
        }

        if (buf[5] != 0x00) {
            return FALSE;
        }

        if (buf[6] != 0x01) {
            return FALSE;
        }

        return TRUE;
    } else {
        return TRUE;
    }
}

BOOL CSensor::bGetMeterI2CID(unsigned char ucMeter, unsigned char* pucI2CID) {
    if (target_board_type == TESTER_BOARD_ARDUINO) {
        BOOL r_value;
        unsigned char command[512];
        unsigned char buf[512];

        memset(command, 0x00, sizeof(command));

        // Signature
        command[0] = 0x45;  // 'E'
        command[1] = 0x47;  // 'G'
        command[2] = 0x49;  // 'I'
        command[3] = 0x53;  // 'S'

        // Operation code
        command[4] = 0x31;  // Get Meter I2C ID

        // Parameter
        command[5] = ucMeter;

        // Value
        command[6] = 0x00;  // Don't Care

#if defined(WIN32)
        // Write Command
        r_value = bWritComPort(command, ARDUINO_COMMAND_LENGTH);
        if (!r_value) {
            return FALSE;
        }

        // Read Response
        r_value = bReadComPort(buf, ARDUINO_RESPONSE_LENGTH, READ_TIMEOUT);
        if (!r_value) {
            return FALSE;
        }
#endif

        if (buf[0] != 'S') {
            return FALSE;
        }
        if (buf[1] != 'I') {
            return FALSE;
        }
        if (buf[2] != 'G') {
            return FALSE;
        }
        if (buf[3] != 'E') {
            return FALSE;
        }

        if (buf[6] != 0x01) {
            return FALSE;
        }

        *pucI2CID = buf[5];

        return TRUE;
    } else {
        *pucI2CID = meter_id[ucMeter - 1];
        return TRUE;
    }
}

BOOL CSensor::bSetSelectedMeter(unsigned char ucMeter) {
    meter_select = ucMeter - 1;
    if (target_board_type == TESTER_BOARD_ARDUINO) {
        BOOL r_value;
        unsigned char command[512];
        unsigned char buf[512];

        memset(command, 0x00, sizeof(command));

        // Signature
        command[0] = 0x45;  // 'E'
        command[1] = 0x47;  // 'G'
        command[2] = 0x49;  // 'I'
        command[3] = 0x53;  // 'S'

        // Operation code
        command[4] = 0x32;  // Set Selected Meter

        // Parameter
        command[5] = 0x00;  // Don't Care

        // Value
        command[6] = ucMeter;

#if defined(WIN32)
        // Write Command
        r_value = bWritComPort(command, ARDUINO_COMMAND_LENGTH);
        if (!r_value) {
            return FALSE;
        }

        // Read Response
        r_value = bReadComPort(buf, ARDUINO_RESPONSE_LENGTH, READ_TIMEOUT);
        if (!r_value) {
            return FALSE;
        }
#endif

        if (buf[0] != 'S') {
            return FALSE;
        }
        if (buf[1] != 'I') {
            return FALSE;
        }
        if (buf[2] != 'G') {
            return FALSE;
        }
        if (buf[3] != 'E') {
            return FALSE;
        }

        if (buf[6] != 0x01) {
            return FALSE;
        }

        return TRUE;
    } else {
        return TRUE;
    }
}

BOOL CSensor::bGetSelectedMeter(unsigned char* pucMeter) {
    if (target_board_type == TESTER_BOARD_ARDUINO) {
        BOOL r_value;
        unsigned char command[512];
        unsigned char buf[512];

        memset(command, 0x00, sizeof(command));

        // Signature
        command[0] = 0x45;  // 'E'
        command[1] = 0x47;  // 'G'
        command[2] = 0x49;  // 'I'
        command[3] = 0x53;  // 'S'

        // Operation code
        command[4] = 0x33;  // Get Selected Meter

        // Parameter
        command[5] = 0x00;  // Don't Care

        // Value
        command[6] = 0x00;  // Don't Care

#if defined(WIN32)
        // Write Command
        r_value = bWritComPort(command, ARDUINO_COMMAND_LENGTH);
        if (!r_value) {
            return FALSE;
        }

        // Read Response
        r_value = bReadComPort(buf, ARDUINO_RESPONSE_LENGTH, READ_TIMEOUT);
        if (!r_value) {
            return FALSE;
        }
#endif

        if (buf[0] != 'S') {
            return FALSE;
        }
        if (buf[1] != 'I') {
            return FALSE;
        }
        if (buf[2] != 'G') {
            return FALSE;
        }
        if (buf[3] != 'E') {
            return FALSE;
        }

        if (buf[6] != 0x01) {
            return FALSE;
        }

        *pucMeter = buf[5];
        meter_select = buf[5];

        return TRUE;
    } else {
        *pucMeter = meter_select + 1;
        return FALSE;
    }
}

BOOL CSensor::bReadMeterRegister(unsigned char reg, unsigned short* pValue) {
    if (target_board_type == TESTER_BOARD_ARDUINO) {
        BOOL r_value;
        unsigned char command[512];
        unsigned char buf[512];

        memset(command, 0x00, sizeof(command));

        // Signature
        command[0] = 0x45;  // 'E'
        command[1] = 0x47;  // 'G'
        command[2] = 0x49;  // 'I'
        command[3] = 0x53;  // 'S'

        // Operation code
        command[4] = 0x34;  // Read Meter Register

        // Parameter
        command[5] = reg;  // register address

        // Value
        command[6] = 0x00;  // Don't Care

#if defined(WIN32)
        // Write Command
        r_value = bWritComPort(command, ARDUINO_COMMAND_LENGTH);
        if (!r_value) {
            return FALSE;
        }

        // Read Response
        r_value = bReadComPort(buf, ARDUINO_RESPONSE_LENGTH, READ_TIMEOUT);
        if (!r_value) {
            return FALSE;
        }
#endif

        if (buf[0] != 'S') {
            return FALSE;
        }
        if (buf[1] != 'I') {
            return FALSE;
        }
        if (buf[2] != 'G') {
            return FALSE;
        }
        if (buf[3] != 'E') {
            return FALSE;
        }

        if (buf[6] != 0x01) {
            return FALSE;
        }

        *pValue = buf[4] << 8;
        *pValue += buf[5];

        return TRUE;
    } else {
        return FALSE;
    }
}

BOOL CSensor::bWriteMeterRegister(unsigned char reg, unsigned short value) {
    if (target_board_type == TESTER_BOARD_ARDUINO) {
        BOOL r_value;
        unsigned char command[512];
        unsigned char buf[512];

        memset(command, 0x00, sizeof(command));

        // Signature
        command[0] = 0x45;  // 'E'
        command[1] = 0x47;  // 'G'
        command[2] = 0x49;  // 'I'
        command[3] = 0x53;  // 'S'

        // Operation code
        command[4] = 0x35;  // Write Meter Register

        // Parameter
        command[5] = reg;  // register address

        // Value H
        command[6] = (value & 0xFF00) >> 8;  // High Byte

        // Value L
        command[7] = (value & 0x00FF);  // Low Byte

#if defined(WIN32)
        // Write Command
        r_value = bWritComPort(command, ARDUINO_COMMAND_LENGTH);
        if (!r_value) {
            return FALSE;
        }

        // Read Response
        r_value = bReadComPort(buf, ARDUINO_RESPONSE_LENGTH, READ_TIMEOUT);
        if (!r_value) {
            return FALSE;
        }
#endif

        if (buf[0] != 'S') {
            return FALSE;
        }
        if (buf[1] != 'I') {
            return FALSE;
        }
        if (buf[2] != 'G') {
            return FALSE;
        }
        if (buf[3] != 'E') {
            return FALSE;
        }

        if (buf[6] != 0x01) {
            return FALSE;
        }

        return TRUE;
    } else {
        return FALSE;
    }
}

BOOL CSensor::bReadPhotoDiode(unsigned short* pLumi) {
    if (target_board_type == TESTER_BOARD_ARDUINO) {
        BOOL r_value;
        unsigned short data;

        // Set Mode
        r_value = bWriteRegisterSingleByte(0x10, 0x00, 0x23);
        if (!r_value) {
            return FALSE;
        }

        // Sleep at least 120 ms
        Sleep(200);

        // Read Data 16-bit
        r_value = bReadI2CDataWord(&data, 0x23);
        if (!r_value) {
            return FALSE;
        }

        // print str(float(int(data)/1.2))

        // Reset clear data
        r_value = bWriteRegisterSingleByte(0x07, 0x00, 0x23);
        if (!r_value) {
            return FALSE;
        }

        *pLumi = data;

        return TRUE;
    } else {
        return FALSE;
    }
}

BOOL CSensor::bReadPhotoDiodeAverage(unsigned short* pLumi, int count) {
    if (target_board_type == TESTER_BOARD_ARDUINO) {
        BOOL r_value;
        int sum;
        unsigned short data;
        int i;

        sum = 0;
        for (i = 0; i < count; i++) {
            r_value = bReadPhotoDiode(&data);
            if (!r_value) {
                return FALSE;
            }

            sum += data;
        }

        *pLumi = sum / count;

        return TRUE;
    } else {
        return FALSE;
    }
}

void CSensor::vGetComports(int ndevice, int* pcomports) {
    int i;
    for (i = 0; i < ndevice; i++) {
        *(pcomports + i) = com_ports[i];
    }
}

BOOL CSensor::bGetUID(unsigned char* pUID) {
    if (target_board_type == TESTER_BOARD_ARDUINO) {
        BOOL r_value;
        unsigned char command[512];

        memset(command, 0x00, sizeof(command));

        // Signature
        command[0] = 0x45;  // 'E'
        command[1] = 0x47;  // 'G'
        command[2] = 0x49;  // 'I'
        command[3] = 0x53;  // 'S'

        // Operation code
        command[4] = 0xF1;  // Get UID

        // Parameter
        command[5] = 0x00;  // Don't care

        // Value
        command[6] = 0x00;  // Don't care

#if defined(WIN32)
        // Write Command
        r_value = bWritComPort(command, ARDUINO_COMMAND_LENGTH);
        if (!r_value) {
            return FALSE;
        }

        // Read Build Date
        r_value = bReadComPort(pUID, 16, READ_TIMEOUT);
        if (!r_value) {
            return FALSE;
        }
#endif

        // Round to 0~9
        int i;
        for (i = 0; i < 16; i++) {
            if ((pUID[i] < 48) || (pUID[i] > 90)) {
                pUID[i] = (pUID[i] % 10) + 48;
            }
        }

        return TRUE;
    } else {
        return FALSE;
    }
}

#endif

#if defined(WIN32)
CSensor::CSensor() {
    target_board_type = TESTER_BOARD_ARDUINO;
    ndevice = 0;

    // arduino
    m_hCom = INVALID_HANDLE_VALUE;
    bIsDeviceReopen = FALSE;
    PortIndex = 0;

    raw_image = NULL;
    fullframe16 = NULL;

    firmware_version = 0x0000;

    meter_select = 0x00;
    meter_id[0] = 0x00;
    meter_id[1] = 0x00;
}

CSensor::~CSensor() {}

BOOL CSensor::bOpenArduino(int com) {
    char port_name[MAX_PATH];
    egist_snprintf(port_name, MAX_PATH, "\\\\.\\COM%d", com);

    m_hCom = CreateFile(port_name, GENERIC_READ | GENERIC_WRITE, 0, NULL, OPEN_EXISTING, 0, NULL);

    if (m_hCom == INVALID_HANDLE_VALUE) {
        return FALSE;
    }

    PurgeComm(m_hCom, PURGE_TXABORT | PURGE_RXABORT | PURGE_TXCLEAR | PURGE_RXCLEAR);

    // Set COM Port Queue Size
    SetupComm(m_hCom, 1024, 1024);

    COMMTIMEOUTS TimeOuts;

    // set read timeout
    TimeOuts.ReadIntervalTimeout = MAXDWORD;
    TimeOuts.ReadTotalTimeoutMultiplier = 0;
    TimeOuts.ReadTotalTimeoutConstant = 0;

    // set write timeout
    TimeOuts.WriteTotalTimeoutMultiplier = 100;
    TimeOuts.WriteTotalTimeoutConstant = 500;
    SetCommTimeouts(m_hCom, &TimeOuts);

    DCB dcb;
    GetCommState(m_hCom, &dcb);
    dcb.BaudRate = 9600;
    dcb.ByteSize = 8;
    dcb.Parity = NOPARITY;
    dcb.StopBits = ONESTOPBIT;
    dcb.fDtrControl = 1;

    SetCommState(m_hCom, &dcb);

    return TRUE;
}

#elif defined(RBS_SDK_USE)

BOOL bReadEfuseData(unsigned char* buf) {
    int r_value;
    unsigned char value;

    bWriteRegister(EF_CSR, 0x01);
    plat_sleep_time(10);  // Sleep(10 ms);
    bReadRegister(EF_CSR, &value);

    if (value & 0x80) return FALSE;

#ifndef __OTG_SENSOR__
    r_value = io_dispatch_read_eFuse(buf, EFUSE_BUF_SIZE);
    if (r_value) return FALSE;
#endif

    bWriteRegister(EF_CSR, 0x00);
    return TRUE;
}

BOOL InitCSensor(CSensor* sensor) {
    egislog_d("%s start(0x%x)", __func__, sensor);

    if (sensor == NULL) return -1;

    this = sensor;
    vRegInit();

    sensor->target_board_type = TESTER_BOARD_ARDUINO;
    sensor->raw_image = NULL;
    sensor->bOpenSensor = bOpenSensor;
    sensor->bDumpRegister = bDumpRegister;
    sensor->bHardwareReset = bHardwareReset;
    sensor->bRegisterReadWriteTest = bRegisterReadWriteTest;
    sensor->bGetSensorID = bGetSensorID;
    sensor->bInitSensor = bInitSensor;
    sensor->bWriteRegister = bWriteRegister;
    sensor->bGetFrame16 = bGetFrame16;
    sensor->bSetPGASetting = bSetPGASetting;
    sensor->bSetExposureTime = bSetExposureTime;
    sensor->bGetIntegratedFrameAndStdDev16 = bGetIntegratedFrameAndStdDev16;
    sensor->bGetAverageFrameAndStdDev16 = bGetAverageFrameAndStdDev16;
    sensor->bReadEfuseData = bReadEfuseData;

    return 0;
}

#endif
