#ifndef __EGISFP_ET7XX_NORMAL_SCAN_H__
#define __EGISFP_ET7XX_NORMAL_SCAN_H__

#include "egisfp_test_et7xx.h"

typedef enum normal_scan_state {
    NS_STATE_READ_FLASE_START = 0,
    NS_STATE_RESET,
    NS_STATE_READ_FLASE_END,
    NS_STATE_GET_IMAGE,
    NS_STATE_UNKNOWN = 0x7FFFFFFF
} normal_scan_state_t;

void egisfp_test_et7xx_normal_scan_uninit(void);
void egisfp_test_et7xx_normal_scan_init(struct egisfp_test_session* egisfp_test);
void egisfp_test_et7xx_normal_scan_set_state(normal_scan_state_t state);

#endif
