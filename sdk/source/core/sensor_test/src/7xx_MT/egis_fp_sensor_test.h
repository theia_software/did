#ifndef EGFPS_FP_SENSOR_TEST_H
#define EGFPS_FP_SENSOR_TEST_H

#if defined(WIN32)
#ifdef EgisFPSensorTestDll_EXPORTS
#define EgisFPSensorTestDll_API __declspec(dllexport)
#else
#define EgisFPSensorTestDll_API  //__declspec(dllimport)
#endif
#include "../src/windows/sensor.h"
#elif defined(RBS_SDK_USE)
#include "7xx_sensor_test_definition.h"
#include "Sensor.h"
#include "egis_fp_config.h"
#include "egis_fp_sensor_define.h"
#include "type_definition.h"

#define USHORT unsigned short
#define UCHAR unsigned char
#else
// PLEASE DEFINE PLATFORM
#endif

#include "egis_fp_config.h"
#include "egis_fp_sensor_define.h"

#define METER_AVERAGE 0x04
#define METER_CONVERSION_TIME 0x04
#define METER_PAUSE 100
#define METER_ADDRESS_V33 0x41
#define METER_ADDRESS_V18 0x44

typedef struct _EGFPS_POWER_RESULT {
    double current33;
    double current18;
    int voltage33;
    int voltage18;
} EGFPS_POWER_RESULT;

typedef struct _EGFPS_TEST_RESULT {
    int PortIndex;
    unsigned int test_sequence;
    unsigned int test_items;
    unsigned int test_start_time;
    unsigned int test_time;

    int hw_result;
    int sw_result;

    // Register R/W test
    int register_rw_test;
    SENSOR_ID sensor_id;

    // OTP test
    int otp_test;
    unsigned char mt_result_buf[EFUSE_BUF_SIZE];

    // Flash test
    int flash_test;

    // Module QR Code
    char QRCode[200];

    // MCU Module ID
    unsigned char module_id[12];

    // IC UID in OTP
    unsigned char uid[9];

    // Firmware Version
    unsigned short firmware_version;

    // Test Pattern Test
    int test_pattern_match;

    // Dark Level Test
    unsigned short dark_level_avg;
    double dark_level_noise;
    double dark_level_stddev;
    int white_pixel_count;

    // Max Exposure Time Test
    double max_expo_time;
    unsigned short max_expo_avg;
    int dark_pixel_count;
    int dark_pixel_count2;

    // Img Calibration Test
    double img_expo_time;
    unsigned short img_target_mean;
    unsigned short img_avg;
    unsigned short img_center_avg;
    double img_noise;
    double img_stddev;

    // bad pxl test
    int total_fail;
    int total_rowcol_fail;
    int max_consecutive_fail;
    int max_connected_fail;

    // bad pxl test2
    int total_fail2;
    int total_rowcol_fail2;
    int max_consecutive_fail2;
    int max_connected_fail2;

    int total_bad_block;
    int total_bad_pixel_block;
    int max_consecutive_fail_block;

    // Dynamic Range Test
    // image integration
    unsigned short img_int_rubberon_avg;
    double img_int_rubberon_noise;
    unsigned char img_int_signal;
    unsigned short img_int_signal16;
    double img_int_snr;

    // image average
    unsigned short img_avg_rubberon_avg;
    double img_avg_rubberon_noise;
    unsigned char img_avg_signal;
    unsigned short img_avg_signal16;
    double img_avg_snr;

    // power consumption for image mode
    EGFPS_POWER_RESULT power_consumption_img;

    // power consumption for powerdown mode
    EGFPS_POWER_RESULT power_consumption_pd;

    // LENS_TEST
    int lens_center_x;
    int lens_center_y;
    int lens_center_offset;

    int lens_mtf_test;
    double lens_mtf_result[9];
    unsigned short lens_mtf_mean[9];

    unsigned short pd_value_read;

} EGFPS_TEST_RESULT;

typedef struct _ImgTestResult {
    unsigned short Average;
    double AvergeInDouble;
    double Deviation;
    int WhiteDots;
    int DarkDots;
    int DefectDots;
    int MaxConsecutiveDirtyDots;
    int MaxConnectedDirtyDots;
    BOOL HasStraitLine;
    BOOL HasAwkwardShape;
} ImgTestResult;

typedef struct _ImgTestCriteria {
    unsigned short white_dot_th;
    unsigned short dark_dot_th;
    unsigned short cds_bad_dot_th;
    int consecutive_fail_th;
    int max_allowed_fail_pxl;
} ImgTestCriteria;

enum {
    TEST_NONE = 0x00,
    TEST_WHITE_DOT = 0x01,
    TEST_DARK_DOT = 0x02,
    TEST_DEFECT_DOT = 0x04,
    TEST_CONSECUTIVE_DOT = 0x10,
    TEST_FOUR_LEAF = 0x20,
    TEST_CONNECTED_FAIL = 0x40,
    TEST_BLOCK_BAD_PXL = 0x80,
};

enum {
    STATUS_NONE = 0x00,
    STATUS_READY = 0x01,
    STATUS_START = 0x02,
    STATUS_REG = 0x03,
    STATUS_TEST_PATTERN = 0x04,
    STATUS_DARK_LEVEL = 0x05,
    STATUS_CENTROID_TEST = 0x06,
    STATUS_MAX_EXPO_TEST = 0x07,
    STATUS_CALIBRATION = 0x08,
    STATUS_DIRTY_DOTS = 0x09,
    STATUS_BLOCK_TEST = 0x0A,
    STATUS_WAIT_CHART = 0x0B,
    STATUS_CHART_SENSED = 0x0C,
    STATUS_MTF_TEST = 0x0D,
    STATUS_SNR_TEST = 0x0E,
    STATUS_POWER_CONSUMPTION_TEST = 0x0F,
    STATUS_END = 0x10
};

typedef struct _Image_Valid_Area {
    unsigned short row_start;
    unsigned short row_end;
    unsigned short col_start;
    unsigned short col_end;
} Image_Valid_Area;

typedef struct _EGFPS_TEST_INST {
    int PortIndex;
    Image_Valid_Area iva;

    unsigned char* picture_raw;
    unsigned char* dirty_dots;
    unsigned char* picture_bkg;

    unsigned short* picture_raw16;
    unsigned short* picture_bkg16;
    int* picture_offset;

    EGFPS_TEST_CONFIG egfps_test_config;
    EGFPS_TEST_RESULT egfps_test_result;
} EGFPS_TEST_INST;

#if defined(RBS_SDK_USE)
#undef HANDLE
#define HANDLE EGFPS_TEST_INST*
#endif

// 32 bytes
typedef struct _EGFPS_OTP_DATA {  // addr  value
    // MP Info
    unsigned char mt_result;         // 0x00  mt test count
    unsigned char software_bin;      // 0x01  software bin
    unsigned char production_plant;  // 0x02  Production Plant[3..0]
    unsigned char program_version;   // 0x03
    unsigned char config_version;    // 0x04
    unsigned char test_date[4];      // 0x05  Seconds since 1970/01/01 00:00:00
                                     // 0x06
                                     // 0x07
                                     // 0x08
    // Calibration Information
    unsigned char exposure_time_h;  // 0x09
    unsigned char exposure_time_l;  // 0x0A
    unsigned char centroid_x1;      // 0x0B  Valid if x!=0x00 and y!=0x00
    unsigned char centroid_y1;      // 0x0C
    unsigned char centroid_x2;      // 0x0D
    unsigned char centroid_y2;      // 0x0E
    unsigned char centroid_x3;      // 0x0F
    unsigned char centroid_y3;      // 0x00
    unsigned char centroid_x4;      // 0x11
    unsigned char centroid_y4;      // 0x12

    // Unused storage
    unsigned char reserved[3];  // 0x13
                                // 0x14
                                // 0x15

    // Otp check sum
    unsigned char check_sum;  // 0x16
    unsigned char uid[9];     // 0x17 ~ 0x1F
} EGFPS_OTP_DATA;

#define FLASH_UID_VALID_BIT (0x01 << 0)
#define FLASH_PID_VALID_BIT (0x01 << 1)
#define FLASH_MPINFO_VALID_BIT (0x01 << 2)
#define FLASH_CALIBRATION_DATA_VALID_BIT (0x01 << 3)
#define FLASH_ALL_DATA_VALID_BIT (0x01 << 6)
#define FLASH_ECC_BIT (0x01 << 7)

typedef struct _EGFPS_FLASH_DATA_V2 {  // addr  value
    // UID              (12 bytes)
    unsigned char uid[9];              // 0x00
    unsigned char flash_data_version;  // 0x09  0x02
    unsigned char uid_chk_sum_h;       // 0x0A
    unsigned char uid_chk_sum_l;       // 0x0B

    // Product ID       (7 bytes)
    unsigned char product_id[5];  // 0x0C  "ET711" "ET713"
    unsigned char pid_chk_sum_h;  // 0x11  0x01    0x01
    unsigned char pid_chk_sum_l;  // 0x12  0x32    0x34

    // MP Info          (11 bytes)
    unsigned char mp_info;           // 0x13  ProjectStep[5..4] ProductionPlant[3..0]
    unsigned char test_date[4];      // 0x14  Seconds since 1970/01/01 00:00:00
    unsigned char program_version;   // 0x18
    unsigned char library_version;   // 0x19
    unsigned char config_version;    // 0x1A
    unsigned char mt_result;         // 0x1B  mt2[3..2] mt1[1..0]
    unsigned char mpinfo_chk_sum_h;  // 0x1C
    unsigned char mpinfo_chk_sum_l;  // 0x1D

    // Calibration data (31 bytes)
    unsigned char calibration_method;   // 0x1E  0:Fix Expo Time 1:K Target Mean
    unsigned char mt1_expo_time_h;      // 0x1F
    unsigned char mt1_expo_time_l;      // 0x20
    unsigned char mt2_expo_time_h;      // 0x21
    unsigned char mt2_expo_time_l;      // 0x22
    unsigned char lens_centroid_x_h;    // 0x23
    unsigned char lens_centroid_x_l;    // 0x24
    unsigned char lens_centroid_y_h;    // 0x25
    unsigned char lens_centroid_y_l;    // 0x26
    unsigned char mtf_value[2 * 9];     // 0x27  Block1..9 mtf value x10000
    unsigned char bad_pxl_mt1_h;        // 0x39
    unsigned char bad_pxl_mt1_l;        // 0x3A
    unsigned char cali_data_chk_sum_h;  // 0x3B
    unsigned char cali_data_chk_sum_l;  // 0x3C

    // ECC Parity Data  (16 bytes)
    unsigned char ecc_parity[16];  // 0x3D

    unsigned char reserved[913];  // 0x4D  (1024 - 61 - 16 - 32 - 2 ) Padding to 1024 (4 pages)

    unsigned char module_otp[32];

    unsigned char sector_chk_sum_h;  // 0x03FE (Byte0+Byte1+...+Byte1021) & 0xFFFF
    unsigned char sector_chk_sum_l;  // 0x03FF

} EGFPS_FLASH_DATA_V2;

#define DO_FLASH_CHIP_ERASE 0

enum {
    SW_BIN_UNKNOW = 0,
    SW_BIN_PASS = 1,
    SW_BIN_SNR_BAD = 2,
    SW_BIN_REG_FAIL = 3,
    SW_BIN_GETIMAGE_FAIL = 4,
    SW_BIN_CALIBRATION_FAIL = 5,
    SW_BIN_BAD_BKG = 6,
    SW_BIN_DIRTY_DOTS = 7,
    SW_BIN_DETECTMODE_FAIL = 8,
    SW_BIN_SNR_TEST_FAIL = 9,
    SW_BIN_RESETN_FAIL = 10,
    SW_BIN_FLASH_FAIL = 11,
    SW_BIN_INT_DISCONNECT = 12,
    SW_BIN_POWER_FAIL = 14,
    SW_BIN_DEFECT_DOTS_FAIL = 15,
    SW_BIN_DARK_DOTS_FAIL = 16,
    SW_BIN_WHITE_DOTS_FAIL = 17,
    SW_BIN_POOR_DR_DOTS_FAIL = 18,
    SW_BIN_OTP_FAIL = 19,
    SW_BIN_OTP_CHECK_FAIL = 20,
    SW_BIN_CONVERTER_BD_VOLTAGE_FAIL = 24,
    SW_BIN_INT_COUNT_LOW = 25,
    SW_BIN_STRESS_TEST_FAIL = 26,
    SW_BIN_NOISE_TEST_FAIL = 27,
    SW_BIN_CLEAR_BACKGROUND_TEST_FAIL = 28,
    SW_BIN_FLASH_CHECK_FAIL = 32,
    SW_BIN_SNR_LOW = 33,
    SW_BIN_FOD_CURRENT_FAIL = 34,
    SW_BIN_POWERDOWN_CURRENT_FAIL = 35,
    SW_BIN_TESTPATTERN_FAIL = 40,
    SW_BIN_CHARTER_FAIL = 41,
    SW_BIN_BAD_COL_ROW = 42,
    SW_BIN_DARK_LEVEL_FAIL = 43,
    SW_BIN_LENS_CENTEROID_FAIL = 44,
    SW_BIN_LENS_MTF_FAIL = 45,
    SW_BIN_LENS_MTF_FAIL2 = 46,
    SW_BIN_LENS_MTF_CORNER_FAIL = 47,
    SW_BIN_LENS_MTF_CORNER_FAIL2 = 48,
    SW_BIN_BLOCK_TEST_FAIL = 49,
    SW_BIN_GENERAL_FAIL = 98,
    SW_BIN_USB_FAIL = 99,
    SW_BIN_MAX = 100
};

// test indication bits
#define STATE_REGISTRY_TESTED (0x00000001 << 0)
#define STATE_TESTPATTERN_TESTED (0x00000001 << 1)
#define STATE_DARK_LEVEL_TESTED (0x00000001 << 2)
#define STATE_MAX_EXPO_TESTED (0x00000001 << 3)
#define STATE_CALIBRATION_TESTED (0x00000001 << 4)
#define STATE_CENTEROID_TESTED (0x00000001 << 5)
#define STATE_MTF_TESTED (0x00000001 << 6)
#define STATE_DIRTY_DOTS_TESTED (0x00000001 << 7)
#define STATE_DIRTY_DOTS2_TESTED (0x00000001 << 8)
#define STATE_BAD_BLOCK_TESTED (0x00000001 << 9)
#define STATE_AVERAGE_DYNAMIC_RANGE_TESTED (0x00000001 << 10)
#define STATE_INTEGRATION_DYNAMIC_RANGE_TESTED (0x00000001 << 11)
#define STATE_CURRENT_TESTED (0x00000001 << 12)
#define STATE_PD_CURRENT_TESTED (0x00000001 << 13)

typedef void (*DRAW_CALLBACK)(int, unsigned char*, int, int);

#ifdef __cplusplus
extern "C" {
#endif
/**
 * The function to init main test instance.
 * @param pEgisSensor The ref of sensor instance for test.
 * @param pConfigFile The path for specific test confic file from Egis.
 * @param pBoardInstance The ref of board information instance for test.
 * @return The ref of test instance, EGFPS_TEST_INST.
 * @see EGFPS_TEST_INST
 */
HANDLE EgisFPSensorTestDll_API egfps_new_test_instance(CSensor* pEgisSensor, char* pConfigFile);

/**
 * The function to release test instance.
 * @param hTestInst The handle of created test instance.
 */
void EgisFPSensorTestDll_API egfps_free_test_instance(HANDLE hTestInst, CSensor* pEgisSensor);

/**
 * The function to process register test
 * @param hTestInst The handle of created test instance.
 * @param pEgisSensor The opened sensor for test.
 * @return OK with SW_BIN_PASS.
 * @return Fail with SW_BIN_REG_FAIL or SW_BIN_USB_FAIL.
 */
DWORD EgisFPSensorTestDll_API egfps_register_test(HANDLE hTestInst, CSensor* pEgisSensor,
                                                  BOOL bDoReadWriteTest);

/**
 * The function to init sensor for image tests
 * @param hTestInst The handle of created test instance.
 * @param pEgisSensor The opened sensor for test.
 * @return OK with SW_BIN_PASS.
 * @return Fail with SW_BIN_REG_FAIL.
 */
DWORD EgisFPSensorTestDll_API egfps_init_sensor(HANDLE hTestInst, CSensor* pEgisSensor);

DWORD EgisFPSensorTestDll_API egfps_dark_img_test(HANDLE hTestInst, CSensor* pEgisSensor,
                                                  unsigned char* pResultImg);

DWORD EgisFPSensorTestDll_API egfps_testpattern_test(HANDLE hTestInst, CSensor* pEgisSensor,
                                                     unsigned char* pResultImg);

DWORD EgisFPSensorTestDll_API egfps_max_exposure_test(HANDLE hTestInst, CSensor* pEgisSensor,
                                                      unsigned char* pResultImg);

DWORD EgisFPSensorTestDll_API egfps_img_calibration_test(HANDLE hTestInst, CSensor* pEgisSensor,
                                                         unsigned char* pResultImg);

DWORD EgisFPSensorTestDll_API egfps_average_dynamic_range_test(HANDLE hTestInst,
                                                               CSensor* pEgisSensor,
                                                               unsigned char* pResultImg);

DWORD EgisFPSensorTestDll_API egfps_integration_dynamic_range_test(HANDLE hTestInst,
                                                                   CSensor* pEgisSensor,
                                                                   unsigned char* pResultImg);

DWORD EgisFPSensorTestDll_API egfps_init_current_meter(HANDLE hTestInst, CSensor* pEgisSensor);

DWORD EgisFPSensorTestDll_API egfps_power_consumption_test(HANDLE hTestInst, CSensor* pEgisSensor,
                                                           EGFPS_POWER_RESULT* pCurrentOffset);

DWORD EgisFPSensorTestDll_API egfps_power_consumption_test_pd(HANDLE hTestInst,
                                                              CSensor* pEgisSensor,
                                                              EGFPS_POWER_RESULT* pCurrentOffset);

DWORD EgisFPSensorTestDll_API egfps_bad_pxl_test(HANDLE hTestInst, CSensor* pEgisSensor,
                                                 unsigned char* pResultImg);

DWORD EgisFPSensorTestDll_API egfps_bad_pxl_test2(HANDLE hTestInst, CSensor* pEgisSensor,
                                                  unsigned char* pResultImg);

DWORD EgisFPSensorTestDll_API egfps_bad_col_row_test(HANDLE hTestInst, CSensor* pEgisSensor,
                                                     unsigned char* pResultImg);

DWORD EgisFPSensorTestDll_API egfps_bad_col_row_test2(HANDLE hTestInst, CSensor* pEgisSensor,
                                                      unsigned char* pResultImg);

DWORD EgisFPSensorTestDll_API egfps_bad_block_test(HANDLE hTestInst, CSensor* pEgisSensor,
                                                   unsigned char* pResultImg);

// for lens type sensors
DWORD EgisFPSensorTestDll_API egfps_lens_centroid_test(HANDLE hTestInst, CSensor* pEgisSensor,
                                                       unsigned char* pResultImg);

DWORD EgisFPSensorTestDll_API egfps_lens_centroid_test_from_nvm(HANDLE hTestInst,
                                                                CSensor* pEgisSensor,
                                                                unsigned int CenterX,
                                                                unsigned int CenterY);

DWORD EgisFPSensorTestDll_API egfps_lens_mtf_test(HANDLE hTestInst, CSensor* pEgisSensor,
                                                  double* pMtfResult, unsigned short* pBlockMean,
                                                  unsigned char* pResultImg);

DWORD EgisFPSensorTestDll_API egfps_read_nvm(HANDLE hTestInst, CSensor* pEgisSensor,
                                             unsigned char* uid,
                                             EGFPS_FLASH_DATA_V2* pFlashDataPlan,
                                             unsigned char* pFlashDataValid,
                                             EGFPS_OTP_DATA* pOtpDataPlan);

DWORD EgisFPSensorTestDll_API egfps_update_flash(HANDLE hTestInst, CSensor* pEgisSensor,
                                                 EGFPS_FLASH_DATA_V2* pFlashDataPlan);

DWORD EgisFPSensorTestDll_API egfps_update_otp(HANDLE hTestInst, CSensor* pEgisSensor,
                                               EGFPS_OTP_DATA* pOtpDataPlan);

DWORD EgisFPSensorTestDll_API egfps_check_otp(HANDLE hTestInst, CSensor* pEgisSensor,
                                              EGFPS_OTP_DATA* pOtpDataPlan,
                                              unsigned char* pOtpDataValid);

#ifdef __cplusplus
}
#endif

#endif
