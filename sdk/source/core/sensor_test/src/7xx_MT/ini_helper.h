
#ifndef __INI_HELPER_H__
#define __INI_HELPER_H__

#ifdef __cplusplus
extern "C" {
#endif
typedef struct _IniHelper {
    int (*SetPathName)(const char* path);
    int (*GetString)(const char* section, const char* name, char* value, int size,
                     const char* default_value);
    int (*GetInt)(const char* section, const char* name, int default_value);
    double (*GetDouble)(const char* section, const char* name, double default_value);

} IniHelper;

int IniHelperCreate(IniHelper** myIniHelper);
void IniHelperDestroy(IniHelper* pIniHelper);

#ifdef __cplusplus
}
#endif
#endif  // #ifndef __INI_HELPER_H__
