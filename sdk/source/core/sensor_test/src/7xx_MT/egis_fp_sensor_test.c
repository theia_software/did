#if defined(WIN32)

#include <stdio.h>
#include <time.h>
#include <windows.h>

#include "../inc/color_bar.h"
#include "../inc/egis_fp_config.h"
#include "../inc/egis_fp_nvm.h"
#include "../inc/egis_fp_sensor_test.h"
#include "../inc/func.h"
#include "../lib/crypto/rs.h"
#include "MTFCounting.h"
#define snprintf _snprintf

#elif defined(RBS_SDK_USE)
#include "MTFCounting.h"
#include "egis_fp_config.h"
#include "egis_fp_sensor_test.h"
#include "egis_sprintf.h"
#include "et713_reg.h"
#include "func.h"
#include "plat_log.h"
#include "plat_mem.h"

#define sprintf(str, format, ...) egist_snprintf(str, MAX_PATH * 2, format, ##__VA_ARGS__)
#define snprintf egist_snprintf
#define OutputDebugString(X) egislog_d(X)
#define LOG_TAG "RBS-INLINE"
#define malloc plat_alloc
#define free plat_free
#define INVALID_HANDLE_VALUE NULL

#else

PLEASE DEFINE PLATFORM

#endif

#include <math.h>

HANDLE
egfps_new_test_instance(CSensor* pEgisSensor, char* pConfigFile) {
    OutputDebugString("-egfps_new_test_instance-\n");

    EGFPS_TEST_INST* ptest_inst;
    BOOL r_value;

    ptest_inst = (EGFPS_TEST_INST*)malloc(sizeof(EGFPS_TEST_INST));
    if (!ptest_inst) {
        return NULL;
    }
    memset(ptest_inst, 0x00, sizeof(EGFPS_TEST_INST));

    r_value = bLoadTestConfig(&ptest_inst->egfps_test_config, pConfigFile);
    if (!r_value) {
        free(ptest_inst);
        return NULL;
    }

    return (HANDLE)ptest_inst;
}

void egfps_free_test_instance(HANDLE hTestInst, CSensor* pEgisSensor) {
    OutputDebugString("-egfps_free_test_instance-\n");

    EGFPS_TEST_INST* ptest_inst = (EGFPS_TEST_INST*)hTestInst;

    if (hTestInst) {
        // free image buffers
        free(ptest_inst->picture_raw);
        free(ptest_inst->dirty_dots);
        free(ptest_inst->picture_bkg);
        free(ptest_inst->picture_raw16);
        free(ptest_inst->picture_bkg16);
        free(ptest_inst->picture_offset);

        free((EGFPS_TEST_INST*)hTestInst);
        hTestInst = INVALID_HANDLE_VALUE;
    }
    // pEgisSensor->bCloseSensor();
}

DWORD
egfps_register_test(HANDLE hTestInst, CSensor* pEgisSensor, BOOL bDoReadWriteTest) {
    OutputDebugString("-egfps_register_test-\n");

    EGFPS_TEST_INST* ptest_inst = (EGFPS_TEST_INST*)hTestInst;

    ptest_inst->egfps_test_result.test_items |= STATE_REGISTRY_TESTED;

    BOOL r_value;

    // apply hardware reset pin reset
    r_value = pEgisSensor->bHardwareReset(ptest_inst->egfps_test_config.ResetHold);
    if (!r_value) {
        ptest_inst->egfps_test_result.hw_result = 5;
        ptest_inst->egfps_test_result.sw_result = SW_BIN_USB_FAIL;
        return SW_BIN_USB_FAIL;
    }

    // Register Read/Write Test ////////////////
    r_value = pEgisSensor->bRegisterReadWriteTest(bDoReadWriteTest);
    if (!r_value) {
        ptest_inst->egfps_test_result.hw_result = 5;
        ptest_inst->egfps_test_result.sw_result = SW_BIN_REG_FAIL;
        return SW_BIN_REG_FAIL;
    }

    // apply hardware reset pin reset
    r_value = pEgisSensor->bHardwareReset(ptest_inst->egfps_test_config.ResetHold);
    if (!r_value) {
        ptest_inst->egfps_test_result.hw_result = 5;
        ptest_inst->egfps_test_result.sw_result = SW_BIN_USB_FAIL;
        return SW_BIN_USB_FAIL;
    }

    ptest_inst->egfps_test_result.register_rw_test = 0x01;

    return SW_BIN_PASS;
}

DWORD
egfps_init_sensor(HANDLE hTestInst, CSensor* pEgisSensor) {
    OutputDebugString("-egfps_init_sensor-\n");

    EGFPS_TEST_INST* ptest_inst = (EGFPS_TEST_INST*)hTestInst;

    BOOL r_value;
    char buf[MAX_PATH];

    // Get Sensor ID
    r_value = pEgisSensor->bGetSensorID(&ptest_inst->egfps_test_result.sensor_id);
    if (!r_value) {
        ptest_inst->egfps_test_result.hw_result = 5;
        ptest_inst->egfps_test_result.sw_result = SW_BIN_REG_FAIL;
        return SW_BIN_REG_FAIL;
    }

    sprintf(buf, "SensorID=%02X%02X%02X PxlID=%02X\n",
            ptest_inst->egfps_test_result.sensor_id.major_id,
            ptest_inst->egfps_test_result.sensor_id.minor_id,
            ptest_inst->egfps_test_result.sensor_id.revision,
            ptest_inst->egfps_test_result.sensor_id.pixel_id);
    OutputDebugString(buf);

    // Fullframe
    r_value = pEgisSensor->bInitSensor(ptest_inst->egfps_test_config.SensorSettingFile,
                                       ptest_inst->egfps_test_config.do_load_setting_file);
    if (!r_value) {
        ptest_inst->egfps_test_result.hw_result = 5;
        ptest_inst->egfps_test_result.sw_result = SW_BIN_REG_FAIL;
        return SW_BIN_REG_FAIL;
    }

    // allocate image buffers
    ptest_inst->picture_raw = (unsigned char*)malloc(pEgisSensor->sensor_total_pixels_show);
    ptest_inst->dirty_dots = (unsigned char*)malloc(pEgisSensor->sensor_total_pixels_show);
    ptest_inst->picture_bkg = (unsigned char*)malloc(pEgisSensor->sensor_total_pixels_show);

    // 16 bits buffers
    ptest_inst->picture_raw16 =
        (unsigned short*)malloc(pEgisSensor->sensor_total_pixels_show * sizeof(unsigned short));
    ptest_inst->picture_bkg16 =
        (unsigned short*)malloc(pEgisSensor->sensor_total_pixels_show * sizeof(unsigned short));

    // background offset
    ptest_inst->picture_offset = (int*)malloc(pEgisSensor->sensor_total_pixels_show * sizeof(int));

    memset(ptest_inst->dirty_dots, 0x00, pEgisSensor->sensor_total_pixels_show);

#if (EGIS_SENSOR == 701)
    ptest_inst->iva.row_start = 0;
    ptest_inst->iva.row_end = pEgisSensor->sensor_height_show - 3;
    ptest_inst->iva.col_start = 0;
    ptest_inst->iva.col_end = pEgisSensor->sensor_width_show - 3;
#else
    ptest_inst->iva.row_start = 0;
    ptest_inst->iva.row_end = pEgisSensor->sensor_height_show - 1;
    ptest_inst->iva.col_start = 0;
    ptest_inst->iva.col_end = pEgisSensor->sensor_width_show - 1;
#endif

    return SW_BIN_PASS;
}

DWORD
egfps_dark_img_test(HANDLE hTestInst, CSensor* pEgisSensor, unsigned char* pResultImg) {
    OutputDebugString("-egfps_dark_img_test-\n");

    EGFPS_TEST_INST* ptest_inst = (EGFPS_TEST_INST*)hTestInst;

    BOOL r_value;

    int i;
    double double_temp;
    double* DeviationOf10Frame;
    double image_noise;

    DeviationOf10Frame = (double*)malloc(pEgisSensor->sensor_total_pixels_show * sizeof(double));
    if (!DeviationOf10Frame) {
        ptest_inst->egfps_test_result.hw_result = 5;
        ptest_inst->egfps_test_result.sw_result = SW_BIN_GENERAL_FAIL;
        return SW_BIN_REG_FAIL;
    }

    // 1X gain
    r_value = pEgisSensor->bSetPGASetting(0x00);
    if (!r_value) {
        free(DeviationOf10Frame);
        ptest_inst->egfps_test_result.hw_result = 5;
        ptest_inst->egfps_test_result.sw_result = SW_BIN_REG_FAIL;
        return SW_BIN_REG_FAIL;
    }

    // set dark level test exposure time
    r_value = pEgisSensor->bSetExposureTime(&ptest_inst->egfps_test_config.dark_expo_time);
    if (!r_value) {
        free(DeviationOf10Frame);
        ptest_inst->egfps_test_result.hw_result = 5;
        ptest_inst->egfps_test_result.sw_result = SW_BIN_REG_FAIL;
        return SW_BIN_REG_FAIL;
    }

    // drop frames
    r_value = pEgisSensor->bGetFrame16(ptest_inst->picture_raw16, FRAME_DROP);
    if (!r_value) {
        free(DeviationOf10Frame);
        ptest_inst->egfps_test_result.hw_result = 5;
        ptest_inst->egfps_test_result.sw_result = SW_BIN_GETIMAGE_FAIL;
        return SW_BIN_REG_FAIL;
    }

    // get couple frames and calculate noise
    memset(DeviationOf10Frame, 0x00, pEgisSensor->sensor_total_pixels_show * sizeof(double));
    r_value = pEgisSensor->bGetAverageFrameAndStdDev16(
        ptest_inst->picture_raw16, DeviationOf10Frame,
        ptest_inst->egfps_test_config.ImageAverageCount, NULL);

    if (!r_value) {
        free(DeviationOf10Frame);
        ptest_inst->egfps_test_result.hw_result = 5;
        ptest_inst->egfps_test_result.sw_result = SW_BIN_GETIMAGE_FAIL;
        return SW_BIN_GETIMAGE_FAIL;
    }

    double_temp = 0.0;
    for (i = 0; i < pEgisSensor->sensor_total_pixels_show; i++) {
        double_temp += DeviationOf10Frame[i];
    }
    image_noise = double_temp / (pEgisSensor->sensor_total_pixels_show);
    free(DeviationOf10Frame);

    ptest_inst->egfps_test_result.dark_level_noise = image_noise;

    ImgTestResult img_test_result;
    ImgTestCriteria img_test_criteria;
    unsigned char test_option;
    BOOL TooManyDirtyDots;

    memset(&img_test_criteria, 0x00, sizeof(img_test_criteria));
    img_test_criteria.white_dot_th = ptest_inst->egfps_test_config.white_pixel_th;

    memset(&img_test_result, 0x00, sizeof(img_test_result));
    test_option = TEST_WHITE_DOT;

    img_test_criteria.max_allowed_fail_pxl = ptest_inst->egfps_test_config.max_white_pixel;

    vEvaluateImage16(&img_test_criteria, &img_test_result, &TooManyDirtyDots,
                     ptest_inst->dirty_dots, &ptest_inst->iva, ptest_inst->picture_raw16,
                     pEgisSensor->sensor_width_show, pEgisSensor->sensor_height_show, test_option);

    ptest_inst->egfps_test_result.dark_level_avg = img_test_result.Average;
    ptest_inst->egfps_test_result.dark_level_stddev = img_test_result.Deviation;
    ptest_inst->egfps_test_result.white_pixel_count = img_test_result.WhiteDots;

#if defined(WIN32)
    // Scale 16 bit image to 8 bit image
    vScaleImage16ToImage8(pEgisSensor->sensor_width_show, pEgisSensor->sensor_height_show, NULL,
                          ptest_inst->picture_raw16, pResultImg);
#elif defined(RBS_SDK_USE)
    // Scale 16 bit image to 8 bit image
    vScaleImage16ToImage8(pEgisSensor->sensor_width_show, pEgisSensor->sensor_height_show, NULL,
                          ptest_inst->picture_raw16, pResultImg, FALSE, NULL, NULL);
#endif

    ptest_inst->egfps_test_result.test_items |= STATE_DARK_LEVEL_TESTED;

    if (TooManyDirtyDots) {
        ptest_inst->egfps_test_result.hw_result = 3;
        ptest_inst->egfps_test_result.sw_result = SW_BIN_WHITE_DOTS_FAIL;
        return SW_BIN_WHITE_DOTS_FAIL;
    }

    if (ptest_inst->egfps_test_result.dark_level_avg <
        ptest_inst->egfps_test_config.dark_level_th) {
        ptest_inst->egfps_test_result.hw_result = 3;
        ptest_inst->egfps_test_result.sw_result = SW_BIN_DARK_LEVEL_FAIL;
        return SW_BIN_WHITE_DOTS_FAIL;
    }

    return SW_BIN_PASS;
}

DWORD
egfps_testpattern_test(HANDLE hTestInst, CSensor* pEgisSensor, unsigned char* pResultImg) {
    OutputDebugString("-egfps_testpattern_test-\n");

    EGFPS_TEST_INST* ptest_inst = (EGFPS_TEST_INST*)hTestInst;

    ptest_inst->egfps_test_result.test_pattern_match = 0x01;

#if (EGIS_SENSOR == 701)
    OutputDebugString("ET701 doesn't support test pattern mode\n");

    ptest_inst->egfps_test_result.test_pattern_match = 0x00;
    ptest_inst->egfps_test_result.test_items |= STATE_TESTPATTERN_TESTED;
    ptest_inst->egfps_test_result.hw_result = 5;
    ptest_inst->egfps_test_result.sw_result = SW_BIN_TESTPATTERN_FAIL;
    return SW_BIN_TESTPATTERN_FAIL;

#elif ((EGIS_SENSOR == 711) || (EGIS_SENSOR == 713))
    BOOL r_value;
    int i, j;
    unsigned short addr;
    unsigned char value;

    addr = REG_KEY;
    value = 0x88;

    r_value = pEgisSensor->bWriteRegister(addr, value);
    if (!r_value) {
        ptest_inst->egfps_test_result.hw_result = 5;
        ptest_inst->egfps_test_result.sw_result = SW_BIN_REG_FAIL;
        return SW_BIN_REG_FAIL;
    }

    addr = TEST_ADC;
    value = 0x02;

    r_value = pEgisSensor->bWriteRegister(addr, value);
    if (!r_value) {
        ptest_inst->egfps_test_result.hw_result = 5;
        ptest_inst->egfps_test_result.sw_result = SW_BIN_REG_FAIL;
        return SW_BIN_REG_FAIL;
    }

    addr = EXP_TIME_SW_H;
    value = 0x00;

    r_value = pEgisSensor->bWriteRegister(addr, value);
    if (!r_value) {
        ptest_inst->egfps_test_result.hw_result = 5;
        ptest_inst->egfps_test_result.sw_result = SW_BIN_REG_FAIL;
        return SW_BIN_REG_FAIL;
    }

    addr = EXP_TIME_SW_L;
    value = 0x00;

    r_value = pEgisSensor->bWriteRegister(addr, value);
    if (!r_value) {
        ptest_inst->egfps_test_result.hw_result = 5;
        ptest_inst->egfps_test_result.sw_result = SW_BIN_REG_FAIL;
        return SW_BIN_REG_FAIL;
    }

    r_value = pEgisSensor->bGetFrame16(ptest_inst->picture_raw16, 0);
    if (!r_value) {
        ptest_inst->egfps_test_result.hw_result = 5;
        ptest_inst->egfps_test_result.sw_result = SW_BIN_GETIMAGE_FAIL;
        return SW_BIN_GETIMAGE_FAIL;
    }

    if (ptest_inst->egfps_test_result.sensor_id.minor_id == 29) {
        for (j = 0; j < pEgisSensor->sensor_height_show; j++) {
            for (i = 0; i < pEgisSensor->sensor_width_show; i++) {
                if (ptest_inst->picture_raw16[j * pEgisSensor->sensor_width_show + i] !=
                    ((144 + i * pEgisSensor->sensor_height_show + j) % 1024)) {
                    ptest_inst->egfps_test_result.test_pattern_match = 0x00;
                }
            }
        }
    }

#if defined(RBS_SDK_USE)
    if (EGIS_SENSOR == 713) {
        for (j = 0; j < pEgisSensor->sensor_height_show; j++) {
            for (i = 0; i < pEgisSensor->sensor_width_show; i++) {
                if (ptest_inst->picture_raw16[j * pEgisSensor->sensor_width_show + i] !=
                    ((1617 + i * (pEgisSensor->sensor_height_show + 2) + j) % 2048)) {
                    OutputDebugString("set test_pattern_match to 0");
                    ptest_inst->egfps_test_result.test_pattern_match = 0x00;
                }
            }
        }
    }
#endif

#if defined(WIN32)
    // Scale 16 bit image to 8 bit image
    vScaleImage16ToImage8(pEgisSensor->sensor_width_show, pEgisSensor->sensor_height_show, NULL,
                          ptest_inst->picture_raw16, pResultImg);
#elif defined(RBS_SDK_USE)
    // Scale 16 bit image to 8 bit image
    vScaleImage16ToImage8(pEgisSensor->sensor_width_show, pEgisSensor->sensor_height_show, NULL,
                          ptest_inst->picture_raw16, pResultImg, FALSE, NULL, NULL);
#endif

    addr = TEST_ADC;
    value = 0x00;

    r_value = pEgisSensor->bWriteRegister(addr, value);
    if (!r_value) {
        ptest_inst->egfps_test_result.hw_result = 5;
        ptest_inst->egfps_test_result.sw_result = SW_BIN_REG_FAIL;
        return SW_BIN_REG_FAIL;
    }

    addr = REG_KEY;
    value = 0x00;

    r_value = pEgisSensor->bWriteRegister(addr, value);
    if (!r_value) {
        ptest_inst->egfps_test_result.hw_result = 5;
        ptest_inst->egfps_test_result.sw_result = SW_BIN_REG_FAIL;
        return SW_BIN_REG_FAIL;
    }

    ptest_inst->egfps_test_result.test_items |= STATE_TESTPATTERN_TESTED;

    if (ptest_inst->egfps_test_config.check_test_pattern &&
        (ptest_inst->egfps_test_result.test_pattern_match == 0x00)) {
        ptest_inst->egfps_test_result.hw_result = 5;
        ptest_inst->egfps_test_result.sw_result = SW_BIN_TESTPATTERN_FAIL;
        return SW_BIN_TESTPATTERN_FAIL;
    }

    return SW_BIN_PASS;
#else
    return SW_BIN_PASS;
#endif
}

DWORD
egfps_max_exposure_test(HANDLE hTestInst, CSensor* pEgisSensor, unsigned char* pResultImg) {
    OutputDebugString("-egfps_max_exposure_test-\n");

    EGFPS_TEST_INST* ptest_inst = (EGFPS_TEST_INST*)hTestInst;

    BOOL r_value;

    // set max expo gain
    r_value = pEgisSensor->bSetPGASetting(ptest_inst->egfps_test_config.max_expo_gain);
    if (!r_value) {
        ptest_inst->egfps_test_result.hw_result = 5;
        ptest_inst->egfps_test_result.sw_result = SW_BIN_REG_FAIL;
        return SW_BIN_REG_FAIL;
    }

    double exposure_time = ptest_inst->egfps_test_config.max_expo_time;
    r_value = pEgisSensor->bSetExposureTime(&exposure_time);
    if (!r_value) {
        ptest_inst->egfps_test_result.hw_result = 5;
        ptest_inst->egfps_test_result.sw_result = SW_BIN_REG_FAIL;
        return SW_BIN_REG_FAIL;
    }

    r_value = pEgisSensor->bGetFrame16(ptest_inst->picture_raw16, 0);
    if (!r_value) {
        ptest_inst->egfps_test_result.hw_result = 5;
        ptest_inst->egfps_test_result.sw_result = SW_BIN_GETIMAGE_FAIL;
        return SW_BIN_GETIMAGE_FAIL;
    }

    ptest_inst->egfps_test_result.max_expo_time = exposure_time;

    ImgTestResult img_test_result;
    ImgTestCriteria img_test_criteria;
    unsigned char test_option;
    BOOL TooManyDirtyDots;

    memset(&img_test_criteria, 0x00, sizeof(img_test_criteria));
    memset(&img_test_result, 0x00, sizeof(img_test_result));

    // A
    img_test_criteria.dark_dot_th = ptest_inst->egfps_test_config.dark_pixel_th;
    img_test_criteria.max_allowed_fail_pxl = ptest_inst->egfps_test_config.max_dark_pixel;
    test_option = TEST_DARK_DOT;

    vEvaluateImage16(&img_test_criteria, &img_test_result, &TooManyDirtyDots,
                     ptest_inst->dirty_dots, &ptest_inst->iva, ptest_inst->picture_raw16,
                     pEgisSensor->sensor_width_show, pEgisSensor->sensor_height_show, test_option);

    ptest_inst->egfps_test_result.max_expo_avg = img_test_result.Average;
    ptest_inst->egfps_test_result.dark_pixel_count = img_test_result.DarkDots;

    // B
    img_test_criteria.dark_dot_th = ptest_inst->egfps_test_config.dark_pixel_th2;
    img_test_criteria.max_allowed_fail_pxl = pEgisSensor->sensor_total_pixels_show;

    vEvaluateImage16(&img_test_criteria, &img_test_result, NULL, NULL, &ptest_inst->iva,
                     ptest_inst->picture_raw16, pEgisSensor->sensor_width_show,
                     pEgisSensor->sensor_height_show, test_option);

    ptest_inst->egfps_test_result.dark_pixel_count2 = img_test_result.DarkDots;

#if defined(WIN32)
    // Scale 16 bit image to 8 bit image
    vScaleImage16ToImage8(pEgisSensor->sensor_width_show, pEgisSensor->sensor_height_show, NULL,
                          ptest_inst->picture_raw16, pResultImg);
#elif defined(RBS_SDK_USE)
    // Scale 16 bit image to 8 bit image
    vScaleImage16ToImage8(pEgisSensor->sensor_width_show, pEgisSensor->sensor_height_show, NULL,
                          ptest_inst->picture_raw16, pResultImg, FALSE, NULL, NULL);
#endif

    ptest_inst->egfps_test_result.test_items |= STATE_MAX_EXPO_TESTED;

    if (TooManyDirtyDots) {
        ptest_inst->egfps_test_result.hw_result = 3;
        ptest_inst->egfps_test_result.sw_result = SW_BIN_DARK_DOTS_FAIL;
        return SW_BIN_DARK_DOTS_FAIL;
    }

    return SW_BIN_PASS;
}

DWORD
egfps_img_calibration_test(HANDLE hTestInst, CSensor* pEgisSensor, unsigned char* pResultImg) {
    OutputDebugString("-egfps_img_calibration_test-\n");

    EGFPS_TEST_INST* ptest_inst = (EGFPS_TEST_INST*)hTestInst;

    BOOL r_value;
    char buf[MAX_PATH];

    int i;
    double double_temp;
    double* DeviationOf10Frame;
    double image_noise;
    int drop_frame = 0;

    DeviationOf10Frame = (double*)malloc(pEgisSensor->sensor_total_pixels_show * sizeof(double));
    if (!DeviationOf10Frame) {
        ptest_inst->egfps_test_result.hw_result = 5;
        ptest_inst->egfps_test_result.sw_result = SW_BIN_GENERAL_FAIL;
        return SW_BIN_REG_FAIL;
    }

    // Set imge gain
    r_value = pEgisSensor->bSetPGASetting(ptest_inst->egfps_test_config.img_gain);
    if (!r_value) {
        free(DeviationOf10Frame);
        ptest_inst->egfps_test_result.hw_result = 5;
        ptest_inst->egfps_test_result.sw_result = SW_BIN_REG_FAIL;
        return SW_BIN_REG_FAIL;
    }

    if (ptest_inst->egfps_test_config.img_calibration_method == 1) {
        // calibrate to target mean
        unsigned short mean;
        double exposure_time;
        double exposure_step;

        exposure_time = 10.0;
        exposure_step = 16.0;

        BOOL bOverFlow = FALSE;

        BOOL r_value;

        while (1) {
            r_value = pEgisSensor->bSetExposureTime(&exposure_time);
            if (!r_value) {
                free(DeviationOf10Frame);
                ptest_inst->egfps_test_result.hw_result = 5;
                ptest_inst->egfps_test_result.sw_result = SW_BIN_REG_FAIL;
                return SW_BIN_REG_FAIL;
            }

            // Drop 5 Frames and get 1
            r_value = pEgisSensor->bGetFrame16(ptest_inst->picture_raw16, drop_frame);
            if (!r_value) {
                free(DeviationOf10Frame);
                ptest_inst->egfps_test_result.hw_result = 5;
                ptest_inst->egfps_test_result.sw_result = SW_BIN_GETIMAGE_FAIL;
                return SW_BIN_REG_FAIL;
            }

#if (IS_EGIS_SENSOR_LENS_TYPE)
            if (ptest_inst->egfps_test_config.do_lens_centeroid_test) {
                vCalculateCenterAverage16(
                    &mean, pEgisSensor->sensor_width_show, pEgisSensor->sensor_height_show,
                    ptest_inst->picture_raw16, NULL, ptest_inst->egfps_test_result.lens_center_x,
                    ptest_inst->egfps_test_result.lens_center_y, 50);
            } else {
                vCalculateCenterAverage16(
                    &mean, pEgisSensor->sensor_width_show, pEgisSensor->sensor_height_show,
                    ptest_inst->picture_raw16, NULL, pEgisSensor->sensor_width_show / 2,
                    pEgisSensor->sensor_height_show / 2, 50);
            }
            snprintf(buf, MAX_PATH, "exposure_time=%2.2f, exposure_step=%.2f, mean=%d, ",
                     exposure_time, exposure_step, mean);
            OutputDebugString(buf);

            if (exposure_step == 0.5) {
                ptest_inst->egfps_test_result.img_expo_time = exposure_time;
                OutputDebugString("done...\n");
                break;
            }

            if (mean >= ptest_inst->egfps_test_config.img_target_mean) {
                if (!bOverFlow) {
                    exposure_step /= 2;
                }
                exposure_time -= exposure_step;

                bOverFlow = TRUE;

                snprintf(buf, MAX_PATH, "next exposure_step=-%.2f\n", exposure_step);
            } else {
                if (exposure_time >= ptest_inst->egfps_test_config.max_expo_time) {
                    ptest_inst->egfps_test_result.img_expo_time = exposure_time;
                    break;
                }

                if (bOverFlow) {
                    exposure_step /= 2;
                }
                bOverFlow = FALSE;

                exposure_time += exposure_step;

                snprintf(buf, MAX_PATH, "next exposure_step=+%.2f\n", exposure_step);
            }

            OutputDebugString(buf);
#else
            unsigned short max, min;
            vCalculateSoftStatistics16(&mean, &max, &min, pEgisSensor->sensor_width_show,
                                       pEgisSensor->sensor_height_show, ptest_inst->picture_raw16,
                                       NULL);

            snprintf(buf, MAX_PATH,
                     "exposure_time=%2.2f, exposure_step=%.2f, max=%d, min=%d, mean=%d, ",
                     exposure_time, exposure_step, max, min, mean);
            OutputDebugString(buf);

            if (exposure_step == 0.5) {
                ptest_inst->egfps_test_result.img_expo_time = exposure_time;
                OutputDebugString("done...\n");
                break;
            }

            if (max >= ptest_inst->egfps_test_config.img_target_mean) {
                if (!bOverFlow) {
                    exposure_step /= 2;
                }
                exposure_time -= exposure_step;

                bOverFlow = TRUE;

                snprintf(buf, MAX_PATH, "next exposure_step=-%.2f\n", exposure_step);
            } else {
                if (exposure_time >= ptest_inst->egfps_test_config.max_expo_time) {
                    ptest_inst->egfps_test_result.img_expo_time = exposure_time;
                    break;
                }

                if (bOverFlow) {
                    exposure_step /= 2;
                }
                bOverFlow = FALSE;

                exposure_time += exposure_step;

                snprintf(buf, MAX_PATH, "next exposure_step=+%.2f\n", exposure_step);
            }

            OutputDebugString(buf);
#endif
        }
    } else {
        // fix exposure time

        // Set Target Exposure Time
        r_value = pEgisSensor->bSetExposureTime(&ptest_inst->egfps_test_config.img_expo_time);
        if (!r_value) {
            free(DeviationOf10Frame);
            ptest_inst->egfps_test_result.hw_result = 5;
            ptest_inst->egfps_test_result.sw_result = SW_BIN_REG_FAIL;
            return SW_BIN_REG_FAIL;
        }

        ptest_inst->egfps_test_result.img_expo_time = ptest_inst->egfps_test_config.img_expo_time;

        // drop frames
        r_value = pEgisSensor->bGetFrame16(ptest_inst->picture_raw16, drop_frame);
        if (!r_value) {
            free(DeviationOf10Frame);
            ptest_inst->egfps_test_result.hw_result = 5;
            ptest_inst->egfps_test_result.sw_result = SW_BIN_GETIMAGE_FAIL;
            return SW_BIN_REG_FAIL;
        }
    }

    // get couple frames and calculate noise
    memset(DeviationOf10Frame, 0x00, pEgisSensor->sensor_total_pixels_show * sizeof(double));
    r_value = pEgisSensor->bGetAverageFrameAndStdDev16(
        ptest_inst->picture_raw16, DeviationOf10Frame,
        ptest_inst->egfps_test_config.ImageAverageCount, NULL);
    if (!r_value) {
        free(DeviationOf10Frame);
        ptest_inst->egfps_test_result.hw_result = 5;
        ptest_inst->egfps_test_result.sw_result = SW_BIN_GETIMAGE_FAIL;
        return SW_BIN_GETIMAGE_FAIL;
    }

    // get software calculated statistics
    unsigned short sw_mean, sw_max, sw_min;

    vCalculateSoftStatistics16(&sw_mean, &sw_max, &sw_min, pEgisSensor->sensor_width_show,
                               pEgisSensor->sensor_height_show, ptest_inst->picture_raw16, NULL);

    ptest_inst->egfps_test_result.img_avg = sw_mean;

    sprintf(buf, "sw_mean=%4d, sw_max=%4d, sw_min=%4d, sw_max-sw_min=%4d.\n", sw_mean, sw_max,
            sw_min, sw_max - sw_min);
    OutputDebugString(buf);

    double_temp = 0.0;
    for (i = 0; i < pEgisSensor->sensor_total_pixels_show; i++) {
        double_temp += DeviationOf10Frame[i];
    }
    image_noise = double_temp / (pEgisSensor->sensor_total_pixels_show);
    free(DeviationOf10Frame);
    ptest_inst->egfps_test_result.img_noise = image_noise;

    double_temp = 0.0;
    for (i = 0; i < pEgisSensor->sensor_total_pixels_show; i++) {
        double_temp += (double)(ptest_inst->picture_raw16[i] - sw_mean) *
                       (double)(ptest_inst->picture_raw16[i] - sw_mean);
    }

    ptest_inst->egfps_test_result.img_stddev =
        sqrt(double_temp / pEgisSensor->sensor_total_pixels_show);

    memcpy(ptest_inst->picture_bkg16, ptest_inst->picture_raw16,
           pEgisSensor->sensor_total_pixels_show * sizeof(unsigned short));
    memcpy(ptest_inst->picture_bkg, pResultImg,
           pEgisSensor->sensor_total_pixels_show * sizeof(unsigned char));

    for (i = 0; i < pEgisSensor->sensor_total_pixels_show; i++) {
        ptest_inst->picture_offset[i] = (int)ptest_inst->picture_raw16[i] - (int)sw_mean;
    }

    // center average (100x100 block)
    vCalculateCenterAverage16(&sw_mean, pEgisSensor->sensor_width_show,
                              pEgisSensor->sensor_height_show, ptest_inst->picture_raw16, NULL,
                              pEgisSensor->sensor_width_show / 2,
                              pEgisSensor->sensor_height_show / 2, 100);
    ptest_inst->egfps_test_result.img_center_avg = sw_mean;

#if defined(WIN32)
    // Scale 16 bit image to 8 bit image
    vScaleImage16ToImage8(pEgisSensor->sensor_width_show, pEgisSensor->sensor_height_show,
                          ptest_inst->dirty_dots, ptest_inst->picture_raw16, pResultImg);
#elif defined(RBS_SDK_USE)
    // Scale 16 bit image to 8 bit image
    vScaleImage16ToImage8(pEgisSensor->sensor_width_show, pEgisSensor->sensor_height_show,
                          ptest_inst->dirty_dots, ptest_inst->picture_raw16, pResultImg, FALSE,
                          NULL, NULL);
#endif

    ptest_inst->egfps_test_result.test_items |= STATE_CALIBRATION_TESTED;

#if defined(WIN32)

#if (EGIS_SENSOR == 711)
    pEgisSensor->bDumpRegister(BOOST_CP);
    pEgisSensor->bDumpRegister(PWR_CTRL2);
    pEgisSensor->bDumpRegister(PWR_CTRL3);
    pEgisSensor->bDumpRegister(VCM_CTRL);
    pEgisSensor->bDumpRegister(EXP_TIME_SW_H);
    pEgisSensor->bDumpRegister(EXP_TIME_SW_L);
#endif

#if (EGIS_SENSOR == 713)
    pEgisSensor->bDumpRegister(BOOST_CP);
    pEgisSensor->bDumpRegister(PWR_CTRL1);
    pEgisSensor->bDumpRegister(PWR_CTRL2);
    pEgisSensor->bDumpRegister(PWR_CTRL3);
    pEgisSensor->bDumpRegister(VCM_CTRL);
    pEgisSensor->bDumpRegister(EXP_TIME_SW_H);
    pEgisSensor->bDumpRegister(EXP_TIME_SW_L);
#endif
#elif defined(RBS_SDK_USE)
    char str;
#if (EGIS_SENSOR == 711)
    str = BOOST_CP;
    pEgisSensor->bDumpRegister(&str, 0);
    str = PWR_CTRL2;
    pEgisSensor->bDumpRegister(&str, 0);
    str = PWR_CTRL3;
    pEgisSensor->bDumpRegister(&str, 0);
    str = VCM_CTRL;
    pEgisSensor->bDumpRegister(&str, 0);
    str = EXP_TIME_SW_H;
    pEgisSensor->bDumpRegister(&str, 0);
    str = EXP_TIME_SW_L;
    pEgisSensor->bDumpRegister(&str, 0);
#endif

#if (EGIS_SENSOR == 713)
    str = BOOST_CP;
    pEgisSensor->bDumpRegister(&str, 0);
    str = PWR_CTRL1;
    pEgisSensor->bDumpRegister(&str, 0);
    str = PWR_CTRL2;
    pEgisSensor->bDumpRegister(&str, 0);
    str = PWR_CTRL3;
    pEgisSensor->bDumpRegister(&str, 0);
    str = VCM_CTRL;
    pEgisSensor->bDumpRegister(&str, 0);
    str = EXP_TIME_SW_H;
    pEgisSensor->bDumpRegister(&str, 0);
    str = EXP_TIME_SW_L;
    pEgisSensor->bDumpRegister(&str, 0);
#endif

#endif

    if (ptest_inst->egfps_test_result.img_noise > ptest_inst->egfps_test_config.img_max_noise) {
        ptest_inst->egfps_test_result.hw_result = 5;
        ptest_inst->egfps_test_result.sw_result = SW_BIN_NOISE_TEST_FAIL;
        return SW_BIN_NOISE_TEST_FAIL;
    }

    // avg of image calibration must be 5 times larger than dark level.
    if (ptest_inst->egfps_test_config.check_background_avg) {
        if (ptest_inst->egfps_test_result.img_avg <
            (ptest_inst->egfps_test_result.dark_level_avg * 5)) {
            ptest_inst->egfps_test_result.hw_result = 5;
            ptest_inst->egfps_test_result.sw_result = SW_BIN_USB_FAIL;
            return SW_BIN_GETIMAGE_FAIL;
        }

        // check min and max of avg
        if (ptest_inst->egfps_test_result.img_avg < (ptest_inst->egfps_test_config.bkg_avg_min)) {
            ptest_inst->egfps_test_result.hw_result = 5;
            ptest_inst->egfps_test_result.sw_result = SW_BIN_BAD_BKG;
            return SW_BIN_BAD_BKG;
        }
        if (ptest_inst->egfps_test_result.img_avg > (ptest_inst->egfps_test_config.bkg_avg_max)) {
            ptest_inst->egfps_test_result.hw_result = 2;
            ptest_inst->egfps_test_result.sw_result = SW_BIN_BAD_BKG;
            return SW_BIN_BAD_BKG;
        }
    }

    return SW_BIN_PASS;
}

DWORD
egfps_average_dynamic_range_test(HANDLE hTestInst, CSensor* pEgisSensor,
                                 unsigned char* pResultImg) {
    OutputDebugString("-egfps_average_dynamic_range_test-\n");

    EGFPS_TEST_INST* ptest_inst = (EGFPS_TEST_INST*)hTestInst;

    BOOL r_value;

    int i;
    double double_temp;
    double* DeviationOf10Frame;
    double image_noise;

    DeviationOf10Frame = (double*)malloc(pEgisSensor->sensor_total_pixels_show * sizeof(double));
    if (!DeviationOf10Frame) {
        ptest_inst->egfps_test_result.hw_result = 5;
        ptest_inst->egfps_test_result.sw_result = SW_BIN_GENERAL_FAIL;
        return SW_BIN_REG_FAIL;
    }

    // Set imge gain
    r_value = pEgisSensor->bSetPGASetting(ptest_inst->egfps_test_config.img_gain);
    if (!r_value) {
        free(DeviationOf10Frame);
        ptest_inst->egfps_test_result.hw_result = 5;
        ptest_inst->egfps_test_result.sw_result = SW_BIN_REG_FAIL;
        return SW_BIN_REG_FAIL;
    }

    // Set Target Exposure Time
    r_value = pEgisSensor->bSetExposureTime(&ptest_inst->egfps_test_result.img_expo_time);
    if (!r_value) {
        free(DeviationOf10Frame);
        ptest_inst->egfps_test_result.hw_result = 5;
        ptest_inst->egfps_test_result.sw_result = SW_BIN_REG_FAIL;
        return SW_BIN_REG_FAIL;
    }

    // drop frames
    r_value = pEgisSensor->bGetFrame16(ptest_inst->picture_raw16, FRAME_DROP);
    if (!r_value) {
        free(DeviationOf10Frame);
        ptest_inst->egfps_test_result.hw_result = 5;
        ptest_inst->egfps_test_result.sw_result = SW_BIN_GETIMAGE_FAIL;
        return SW_BIN_REG_FAIL;
    }

    if (ptest_inst->egfps_test_config.img_background_cancellation) {
        r_value = pEgisSensor->bGetAverageFrameAndStdDev16(
            ptest_inst->picture_raw16, DeviationOf10Frame,
            ptest_inst->egfps_test_config.ImageAverageCount, ptest_inst->picture_offset);
    } else {
        r_value = pEgisSensor->bGetAverageFrameAndStdDev16(
            ptest_inst->picture_raw16, DeviationOf10Frame,
            ptest_inst->egfps_test_config.ImageAverageCount, NULL);
    }
    if (!r_value) {
        free(DeviationOf10Frame);
        ptest_inst->egfps_test_result.hw_result = 5;
        ptest_inst->egfps_test_result.sw_result = SW_BIN_GETIMAGE_FAIL;
        return SW_BIN_GETIMAGE_FAIL;
    }

    if (!r_value) {
        free(DeviationOf10Frame);
        ptest_inst->egfps_test_result.hw_result = 5;
        ptest_inst->egfps_test_result.sw_result = SW_BIN_GETIMAGE_FAIL;
        return SW_BIN_REG_FAIL;
    }

    // get software calculated statistics
    unsigned short sw_mean, sw_max, sw_min;

    vCalculateSoftStatistics16(&sw_mean, &sw_max, &sw_min, pEgisSensor->sensor_width_show,
                               pEgisSensor->sensor_height_show, ptest_inst->picture_raw16, NULL);
    ptest_inst->egfps_test_result.img_avg_rubberon_avg = sw_mean;

    double_temp = 0.0;
    for (i = 0; i < pEgisSensor->sensor_total_pixels_show; i++) {
        double_temp += DeviationOf10Frame[i];
    }
    image_noise = double_temp / (pEgisSensor->sensor_total_pixels_show);
    free(DeviationOf10Frame);
    ptest_inst->egfps_test_result.img_avg_rubberon_noise = image_noise;

    // Scale 16 bit image to 8 bit image
    vScaleImage16ToImage8(pEgisSensor->sensor_width_show, pEgisSensor->sensor_height_show,
                          ptest_inst->dirty_dots, ptest_inst->picture_raw16, pResultImg, FALSE,
                          &ptest_inst->egfps_test_result.img_avg_signal,
                          &ptest_inst->egfps_test_result.img_avg_signal16);

    if (ptest_inst->egfps_test_result.img_avg_rubberon_noise != 0) {
        ptest_inst->egfps_test_result.img_avg_snr =
            ptest_inst->egfps_test_result.img_avg_signal16 /
            ptest_inst->egfps_test_result.img_avg_rubberon_noise;
    }

    ptest_inst->egfps_test_result.test_items |= STATE_AVERAGE_DYNAMIC_RANGE_TESTED;

    // check 16 bit RV
    if (ptest_inst->egfps_test_result.img_avg_signal16 <
        ptest_inst->egfps_test_config.ImageAverageRV16Min) {
        ptest_inst->egfps_test_result.hw_result = 2;
        ptest_inst->egfps_test_result.sw_result = SW_BIN_SNR_BAD;
        return SW_BIN_SNR_BAD;
    }

    return SW_BIN_PASS;
}

DWORD
egfps_integration_dynamic_range_test(HANDLE hTestInst, CSensor* pEgisSensor,
                                     unsigned char* pResultImg) {
    OutputDebugString("-egfps_integration_dynamic_range_test-\n");

    EGFPS_TEST_INST* ptest_inst = (EGFPS_TEST_INST*)hTestInst;

    BOOL r_value;
    int i;
    double double_temp;
    double* DeviationOf10Frame;
    double image_noise;

    DeviationOf10Frame = (double*)malloc(pEgisSensor->sensor_total_pixels_show * sizeof(double));
    if (!DeviationOf10Frame) {
        ptest_inst->egfps_test_result.hw_result = 5;
        ptest_inst->egfps_test_result.sw_result = SW_BIN_GENERAL_FAIL;
        return SW_BIN_REG_FAIL;
    }

    // Set imge gain
    r_value = pEgisSensor->bSetPGASetting(ptest_inst->egfps_test_config.img_gain);
    if (!r_value) {
        free(DeviationOf10Frame);
        ptest_inst->egfps_test_result.hw_result = 5;
        ptest_inst->egfps_test_result.sw_result = SW_BIN_REG_FAIL;
        return SW_BIN_REG_FAIL;
    }

    // Set Target Exposure Time
    r_value = pEgisSensor->bSetExposureTime(&ptest_inst->egfps_test_result.img_expo_time);
    if (!r_value) {
        free(DeviationOf10Frame);
        ptest_inst->egfps_test_result.hw_result = 5;
        ptest_inst->egfps_test_result.sw_result = SW_BIN_REG_FAIL;
        return SW_BIN_REG_FAIL;
    }

    // drop frames
    r_value = pEgisSensor->bGetFrame16(ptest_inst->picture_raw16, FRAME_DROP);
    if (!r_value) {
        free(DeviationOf10Frame);
        ptest_inst->egfps_test_result.hw_result = 5;
        ptest_inst->egfps_test_result.sw_result = SW_BIN_GETIMAGE_FAIL;
        return SW_BIN_REG_FAIL;
    }

    if (ptest_inst->egfps_test_config.img_background_cancellation) {
        r_value = pEgisSensor->bGetIntegratedFrameAndStdDev16(
            ptest_inst->picture_raw16, DeviationOf10Frame,
            ptest_inst->egfps_test_config.ImageIntegrationCount, ptest_inst->picture_offset);
    } else {
        r_value = pEgisSensor->bGetIntegratedFrameAndStdDev16(
            ptest_inst->picture_raw16, DeviationOf10Frame,
            ptest_inst->egfps_test_config.ImageIntegrationCount, NULL);
    }

    if (!r_value) {
        free(DeviationOf10Frame);
        ptest_inst->egfps_test_result.hw_result = 5;
        ptest_inst->egfps_test_result.sw_result = SW_BIN_GETIMAGE_FAIL;
        return SW_BIN_REG_FAIL;
    }

    // get software calculated statistics
    unsigned short sw_mean, sw_max, sw_min;

    vCalculateSoftStatistics16(&sw_mean, &sw_max, &sw_min, pEgisSensor->sensor_width_show,
                               pEgisSensor->sensor_height_show, ptest_inst->picture_raw16, NULL);
    ptest_inst->egfps_test_result.img_int_rubberon_avg = sw_mean;

    double_temp = 0.0;
    for (i = 0; i < pEgisSensor->sensor_total_pixels_show; i++) {
        double_temp += DeviationOf10Frame[i];
    }
    image_noise = double_temp / (pEgisSensor->sensor_total_pixels_show);
    free(DeviationOf10Frame);
    ptest_inst->egfps_test_result.img_int_rubberon_noise = image_noise;

    // Scale 16 bit image to 8 bit image
    vScaleImage16ToImage8(pEgisSensor->sensor_width_show, pEgisSensor->sensor_height_show,
                          ptest_inst->dirty_dots, ptest_inst->picture_raw16, pResultImg, TRUE,
                          &ptest_inst->egfps_test_result.img_int_signal,
                          &ptest_inst->egfps_test_result.img_int_signal16);

    if (ptest_inst->egfps_test_result.img_int_rubberon_noise != 0) {
        ptest_inst->egfps_test_result.img_int_snr =
            ptest_inst->egfps_test_result.img_int_signal16 /
            ptest_inst->egfps_test_result.img_int_rubberon_noise;
    }

    ptest_inst->egfps_test_result.test_items |= STATE_INTEGRATION_DYNAMIC_RANGE_TESTED;

    // check 16 bit RV
    if (ptest_inst->egfps_test_result.img_int_signal16 <
        ptest_inst->egfps_test_config.ImageIntegrationRV16Min) {
        ptest_inst->egfps_test_result.hw_result = 2;
        ptest_inst->egfps_test_result.sw_result = SW_BIN_SNR_BAD;
        return SW_BIN_SNR_BAD;
    }

    return SW_BIN_PASS;
}

DWORD
egfps_power_consumption_test(HANDLE hTestInst, CSensor* pEgisSensor,
                             EGFPS_POWER_RESULT* pCurrentOffset) {
    OutputDebugString("-egfps_power_consumption_test-\n");

    EGFPS_TEST_INST* ptest_inst = (EGFPS_TEST_INST*)hTestInst;

    BOOL r_value;

    r_value =
        pEgisSensor->bReadMeters(&ptest_inst->egfps_test_result.power_consumption_img.current33,
                                 &ptest_inst->egfps_test_result.power_consumption_img.current18,
                                 &ptest_inst->egfps_test_result.power_consumption_img.voltage33,
                                 &ptest_inst->egfps_test_result.power_consumption_img.voltage18);

    if (!r_value) {
        ptest_inst->egfps_test_result.hw_result = 5;
        ptest_inst->egfps_test_result.sw_result = SW_BIN_USB_FAIL;
        return SW_BIN_USB_FAIL;
    }

    ptest_inst->egfps_test_result.test_items |= STATE_CURRENT_TESTED;

    if (pCurrentOffset && (ptest_inst->egfps_test_result.power_consumption_img.current33 >
                           pCurrentOffset->current33)) {
        ptest_inst->egfps_test_result.power_consumption_img.current33 -= pCurrentOffset->current33;
    } else {
        ptest_inst->egfps_test_result.power_consumption_img.current33 = 0;
    }

    if (pCurrentOffset && (ptest_inst->egfps_test_result.power_consumption_img.current18 >
                           pCurrentOffset->current18)) {
        ptest_inst->egfps_test_result.power_consumption_img.current18 -= pCurrentOffset->current18;
    } else {
        ptest_inst->egfps_test_result.power_consumption_img.current18 = 0.0;
    }

    if (ptest_inst->egfps_test_result.power_consumption_img.current33 >
        ptest_inst->egfps_test_config.MaxPowerConsumption33) {
        ptest_inst->egfps_test_result.hw_result = 5;
        ptest_inst->egfps_test_result.sw_result = SW_BIN_POWER_FAIL;
        return SW_BIN_POWER_FAIL;
    }

    if (ptest_inst->egfps_test_result.power_consumption_img.current33 <
        ptest_inst->egfps_test_config.MinPowerConsumption33) {
        ptest_inst->egfps_test_result.hw_result = 5;
        ptest_inst->egfps_test_result.sw_result = SW_BIN_POWER_FAIL;
        return SW_BIN_POWER_FAIL;
    }

    if (ptest_inst->egfps_test_config.PowerConsumptionTest18) {
        if (ptest_inst->egfps_test_result.power_consumption_img.current18 >
            ptest_inst->egfps_test_config.MaxPowerConsumption18) {
            ptest_inst->egfps_test_result.hw_result = 5;
            ptest_inst->egfps_test_result.sw_result = SW_BIN_POWER_FAIL;
            return SW_BIN_POWER_FAIL;
        }
    }
    return SW_BIN_PASS;
}

DWORD
egfps_power_consumption_test_pd(HANDLE hTestInst, CSensor* pEgisSensor,
                                EGFPS_POWER_RESULT* pCurrentOffset) {
    OutputDebugString("-egfps_power_consumption_test_pd-\n");

    EGFPS_TEST_INST* ptest_inst = (EGFPS_TEST_INST*)hTestInst;

#if ((EGIS_SENSOR == 701) || (EGIS_SENSOR == 711) || (EGIS_SENSOR == 713))

    BOOL r_value;

    unsigned short addr;
    unsigned char value;

    addr = PWR_CTRL1;
    value = 0x80;

    r_value = pEgisSensor->bWriteRegister(addr, value);
    if (!r_value) {
        ptest_inst->egfps_test_result.hw_result = 5;
        ptest_inst->egfps_test_result.sw_result = SW_BIN_REG_FAIL;
        return SW_BIN_REG_FAIL;
    }

    r_value =
        pEgisSensor->bReadMeters(&ptest_inst->egfps_test_result.power_consumption_pd.current33,
                                 &ptest_inst->egfps_test_result.power_consumption_pd.current18,
                                 &ptest_inst->egfps_test_result.power_consumption_pd.voltage33,
                                 &ptest_inst->egfps_test_result.power_consumption_pd.voltage18);

    if (!r_value) {
        ptest_inst->egfps_test_result.hw_result = 5;
        ptest_inst->egfps_test_result.sw_result = SW_BIN_USB_FAIL;
        return SW_BIN_USB_FAIL;
    }

    ptest_inst->egfps_test_result.test_items |= STATE_PD_CURRENT_TESTED;

    if (pCurrentOffset && (ptest_inst->egfps_test_result.power_consumption_pd.current33 >
                           pCurrentOffset->current33)) {
        ptest_inst->egfps_test_result.power_consumption_pd.current33 -= pCurrentOffset->current33;
    } else {
        ptest_inst->egfps_test_result.power_consumption_pd.current33 = 0.0;
    }

    if (pCurrentOffset && (ptest_inst->egfps_test_result.power_consumption_pd.current18 >
                           pCurrentOffset->current18)) {
        ptest_inst->egfps_test_result.power_consumption_pd.current18 -= pCurrentOffset->current18;
    } else {
        ptest_inst->egfps_test_result.power_consumption_pd.current18 = 0.0;
    }

    if (ptest_inst->egfps_test_result.power_consumption_pd.current33 >
        ptest_inst->egfps_test_config.MaxPowerConsumption33Pd) {
        ptest_inst->egfps_test_result.hw_result = 5;
        ptest_inst->egfps_test_result.sw_result = SW_BIN_POWERDOWN_CURRENT_FAIL;
        return SW_BIN_POWER_FAIL;
    }

    if (ptest_inst->egfps_test_result.power_consumption_pd.current33 <
        ptest_inst->egfps_test_config.MinPowerConsumption33Pd) {
        ptest_inst->egfps_test_result.hw_result = 5;
        ptest_inst->egfps_test_result.sw_result = SW_BIN_POWERDOWN_CURRENT_FAIL;
        return SW_BIN_POWER_FAIL;
    }

    if (ptest_inst->egfps_test_config.PowerConsumptionTest18) {
        if (ptest_inst->egfps_test_result.power_consumption_pd.current18 >
            ptest_inst->egfps_test_config.MaxPowerConsumption18Pd) {
            ptest_inst->egfps_test_result.hw_result = 5;
            ptest_inst->egfps_test_result.sw_result = SW_BIN_POWERDOWN_CURRENT_FAIL;
            return SW_BIN_POWER_FAIL;
        }
    }

    return SW_BIN_PASS;
#else
    OutputDebugString("PowerDown Mode Not Supported.\n");
    return SW_BIN_PASS;
#endif
}

DWORD
egfps_bad_block_test(HANDLE hTestInst, CSensor* pEgisSensor, unsigned char* pResultImg) {
    OutputDebugString("-egfps_bad_block_test-\n");

    EGFPS_TEST_INST* ptest_inst = (EGFPS_TEST_INST*)hTestInst;

    int i, j;
    int block_width = ptest_inst->egfps_test_config.block_size_width;
    int block_height = ptest_inst->egfps_test_config.block_size_height;
    int block_scan_step = ptest_inst->egfps_test_config.block_scan_step;

    int block_x, block_y;

    // phase 1: scan all block bad pixels
    for (block_y = ptest_inst->iva.row_start;
         (block_y + block_height) < (ptest_inst->iva.row_end + 1); block_y += block_scan_step) {
        for (block_x = ptest_inst->iva.col_start;
             (block_x + block_width) < (ptest_inst->iva.col_end + 1); block_x += block_scan_step) {
            unsigned int sum;
            unsigned short avg;
            unsigned short bad_pxl_offset;

            sum = 0;
            for (i = block_x; i < block_x + block_width; i++) {
                for (j = block_y; j < block_y + block_height; j++) {
                    sum += ptest_inst->picture_bkg16[j * pEgisSensor->sensor_width_show + i];
                }
            }
            avg = sum / (block_width * block_height);
            bad_pxl_offset =
                (unsigned short)(avg * ptest_inst->egfps_test_config.bad_block_pixel_percentage /
                                 100);

            for (i = block_x; i < block_x + block_width; i++) {
                for (j = block_y; j < block_y + block_height; j++) {
                    if (abs(ptest_inst->picture_bkg16[j * pEgisSensor->sensor_width_show + i] -
                            avg) > bad_pxl_offset) {
                        ptest_inst->dirty_dots[j * pEgisSensor->sensor_width_show + i] |=
                            TEST_BLOCK_BAD_PXL;
                    }
                }
            }
        }
    }

    // phase 2: check bad pixel table
    // check bad block count
    int bad_block_cnt = 0;
    for (block_y = ptest_inst->iva.row_start;
         (block_y + block_height) < (ptest_inst->iva.row_end + 1); block_y += block_scan_step) {
        for (block_x = ptest_inst->iva.col_start;
             (block_x + block_width) < (ptest_inst->iva.col_end + 1); block_x += block_scan_step) {
            int bad_pxl_cnt = 0;
            for (i = block_x; i < block_x + block_width; i++) {
                for (j = block_y; j < block_y + block_height; j++) {
                    if (ptest_inst->dirty_dots[j * pEgisSensor->sensor_width_show + i] &
                        TEST_BLOCK_BAD_PXL) {
                        bad_pxl_cnt++;
                    }
                }
            }

            if (bad_pxl_cnt > ptest_inst->egfps_test_config.max_bad_pixel_block) {
                bad_block_cnt++;
            }
        }
    }

    // check overall bad pixel
    int bad_pxl_all = 0;
    memset(pResultImg, 0xFF, pEgisSensor->sensor_total_pixels_show);
    for (j = ptest_inst->iva.row_start; j <= ptest_inst->iva.row_end; j++) {
        for (i = ptest_inst->iva.col_start; i <= ptest_inst->iva.col_end; i++) {
            if (ptest_inst->dirty_dots[j * pEgisSensor->sensor_width_show + i] &
                TEST_BLOCK_BAD_PXL) {
                pResultImg[j * pEgisSensor->sensor_width_show + i] = 0x00;
                bad_pxl_all++;
            }
        }
    }

    // consecutive bad pixel
    int MaxConsecutiveDirtyDots = 0;

    for (i = 0; i < pEgisSensor->sensor_height_show; i++) {
        for (j = 0; j < pEgisSensor->sensor_width_show; j++) {
            if (ptest_inst->dirty_dots[i * pEgisSensor->sensor_width_show + j] &
                TEST_BLOCK_BAD_PXL) {
                int consective_test;

                // '|' dots
                consective_test = 1;
                while ((i + consective_test) < pEgisSensor->sensor_height_show) {
                    if (ptest_inst
                            ->dirty_dots[(i + consective_test) * pEgisSensor->sensor_width_show +
                                         j] &
                        TEST_BLOCK_BAD_PXL) {
                        consective_test++;
                    } else {
                        break;
                    }
                }
                if (consective_test > MaxConsecutiveDirtyDots) {
                    MaxConsecutiveDirtyDots = consective_test;
                }

                // '--' dots
                consective_test = 1;
                while ((j + consective_test) < pEgisSensor->sensor_width_show) {
                    if (ptest_inst
                            ->dirty_dots[i * pEgisSensor->sensor_width_show + j + consective_test] &
                        TEST_BLOCK_BAD_PXL) {
                        consective_test++;
                    } else {
                        break;
                    }
                }
                if (consective_test > MaxConsecutiveDirtyDots) {
                    MaxConsecutiveDirtyDots = consective_test;
                }

                // '/' dots
                consective_test = 1;
                while (((i + consective_test) < pEgisSensor->sensor_height_show) &&
                       ((j + consective_test) < pEgisSensor->sensor_width_show)) {
                    if (ptest_inst
                            ->dirty_dots[(i + consective_test) * pEgisSensor->sensor_width_show +
                                         (j + consective_test)] &
                        TEST_BLOCK_BAD_PXL) {
                        consective_test++;
                    } else {
                        break;
                    }
                }
                if (consective_test > MaxConsecutiveDirtyDots) {
                    MaxConsecutiveDirtyDots = consective_test;
                }

                // '\' dots
                consective_test = 1;
                while (((i + consective_test) < pEgisSensor->sensor_height_show) &&
                       (j >= consective_test)) {
                    if (ptest_inst
                            ->dirty_dots[(i + consective_test) * pEgisSensor->sensor_width_show +
                                         (j - consective_test)] &
                        TEST_BLOCK_BAD_PXL) {
                        consective_test++;
                    } else {
                        break;
                    }
                }
                if (consective_test > MaxConsecutiveDirtyDots) {
                    MaxConsecutiveDirtyDots = consective_test;
                }
            }
        }
    }

    ptest_inst->egfps_test_result.test_items |= STATE_BAD_BLOCK_TESTED;

    ptest_inst->egfps_test_result.total_bad_block = bad_block_cnt;
    ptest_inst->egfps_test_result.total_bad_pixel_block = bad_pxl_all;
    ptest_inst->egfps_test_result.max_consecutive_fail_block = MaxConsecutiveDirtyDots;

    if (bad_block_cnt > ptest_inst->egfps_test_config.max_bad_block) {
        ptest_inst->egfps_test_result.hw_result = 3;
        ptest_inst->egfps_test_result.sw_result = SW_BIN_BLOCK_TEST_FAIL;
        return SW_BIN_BLOCK_TEST_FAIL;
    }

    if (bad_pxl_all > ptest_inst->egfps_test_config.max_bad_pixel_block_all) {
        ptest_inst->egfps_test_result.hw_result = 3;
        ptest_inst->egfps_test_result.sw_result = SW_BIN_DIRTY_DOTS;
        return SW_BIN_BLOCK_TEST_FAIL;
    }

    if (ptest_inst->egfps_test_config.do_consecutive_fail_test_block == 1) {
        if (MaxConsecutiveDirtyDots > ptest_inst->egfps_test_config.consecutive_fail_pxl_block) {
            ptest_inst->egfps_test_result.hw_result = 3;
            ptest_inst->egfps_test_result.sw_result = SW_BIN_DIRTY_DOTS;
            return SW_BIN_BLOCK_TEST_FAIL;
        }
    }

    return SW_BIN_PASS;
}

DWORD
egfps_bad_pxl_test(HANDLE hTestInst, CSensor* pEgisSensor, unsigned char* pResultImg) {
    OutputDebugString("-egfps_bad_pxl_test-\n");

    EGFPS_TEST_INST* ptest_inst = (EGFPS_TEST_INST*)hTestInst;

    int i;
    double bad_pixel_offset;
    bad_pixel_offset = (double)ptest_inst->egfps_test_result.img_avg *
                       ptest_inst->egfps_test_config.bad_pxl_percentage / 100;

    ImgTestResult img_test_result;
    ImgTestCriteria img_test_criteria;
    unsigned char test_option;
    BOOL TooManyDirtyDots;

    memset(&img_test_criteria, 0x00, sizeof(img_test_criteria));
    img_test_criteria.cds_bad_dot_th = (unsigned short)bad_pixel_offset;

    memset(&img_test_result, 0x00, sizeof(img_test_result));
    test_option = TEST_DEFECT_DOT;

    img_test_criteria.max_allowed_fail_pxl = ptest_inst->egfps_test_config.max_bad_pxl;

    if (ptest_inst->egfps_test_config.do_consecutive_fail_test == 1) {
        test_option |= TEST_CONSECUTIVE_DOT;
        img_test_criteria.consecutive_fail_th = ptest_inst->egfps_test_config.consecutive_fail_pxl;
    }

    if (ptest_inst->egfps_test_config.do_consecutive_fail_test == 2) {
        test_option |= TEST_CONNECTED_FAIL;
        img_test_criteria.consecutive_fail_th = ptest_inst->egfps_test_config.consecutive_fail_pxl;
    }

    vEvaluateImage16(&img_test_criteria, &img_test_result, &TooManyDirtyDots,
                     ptest_inst->dirty_dots, &ptest_inst->iva, ptest_inst->picture_bkg16,
                     pEgisSensor->sensor_width_show, pEgisSensor->sensor_height_show, test_option);

    ptest_inst->egfps_test_result.total_fail = img_test_result.DefectDots;
    ptest_inst->egfps_test_result.max_consecutive_fail = img_test_result.MaxConsecutiveDirtyDots;
    ptest_inst->egfps_test_result.max_connected_fail = img_test_result.MaxConnectedDirtyDots;

    memset(pResultImg, 0xFF, pEgisSensor->sensor_total_pixels_show);
    for (i = 0; i < pEgisSensor->sensor_total_pixels_show; i++) {
        if (ptest_inst->dirty_dots[i]) {
            pResultImg[i] = 0x00;
        }
    }

    ptest_inst->egfps_test_result.test_items |= STATE_DIRTY_DOTS_TESTED;

    if ((img_test_result.DefectDots > ptest_inst->egfps_test_config.max_bad_pxl) ||
        TooManyDirtyDots) {
        ptest_inst->egfps_test_result.hw_result = 3;
        ptest_inst->egfps_test_result.sw_result = SW_BIN_DIRTY_DOTS;
        return SW_BIN_DIRTY_DOTS;
    }

    return SW_BIN_PASS;
}

DWORD
egfps_bad_col_row_test(HANDLE hTestInst, CSensor* pEgisSensor, unsigned char* pResultImg) {
    OutputDebugString("-egfps_bad_col_row_test-\n");

    EGFPS_TEST_INST* ptest_inst = (EGFPS_TEST_INST*)hTestInst;

    int* single_row_sum = (int*)malloc(pEgisSensor->sensor_height_show * sizeof(int));
    if (!single_row_sum) {
        ptest_inst->egfps_test_result.hw_result = 5;
        ptest_inst->egfps_test_result.sw_result = SW_BIN_GENERAL_FAIL;
        return SW_BIN_GENERAL_FAIL;
    }

    int* single_col_sum = (int*)malloc(pEgisSensor->sensor_width_show * sizeof(int));
    if (!single_col_sum) {
        free(single_row_sum);
        ptest_inst->egfps_test_result.hw_result = 5;
        ptest_inst->egfps_test_result.sw_result = SW_BIN_GENERAL_FAIL;
        return SW_BIN_GENERAL_FAIL;
    }

    int i, j;
    double bad_col_row_offset;
    int bad_col_row_cnt;

    bad_col_row_cnt = 0;
    memset(pResultImg, 0xFF, pEgisSensor->sensor_total_pixels_show);

    // calculate row average ///////////////////////////////////////////
#if (EGIS_SENSOR == 701)
    for (i = 0; i < pEgisSensor->sensor_height_show - 2; i++) {
#else
    for (i = 0; i < pEgisSensor->sensor_height_show; i++) {
#endif
        single_row_sum[i] = 0;
        for (j = 0; j < pEgisSensor->sensor_width_show; j++) {
            single_row_sum[i] += ptest_inst->picture_bkg16[i * pEgisSensor->sensor_width_show + j];
        }
    }

#if (EGIS_SENSOR == 701)
    for (i = 1; i < pEgisSensor->sensor_height_show - 2; i++) {
#else
    for (i = 1; i < pEgisSensor->sensor_height_show; i++) {
#endif
        // start with second row
        bad_col_row_offset = (double)single_row_sum[i - 1] *
                             ptest_inst->egfps_test_config.bad_rowcol_percentage / 100;

        if (fabs((double)single_row_sum[i] - (double)single_row_sum[i - 1]) > bad_col_row_offset) {
            for (j = 0; j < pEgisSensor->sensor_width_show; j++) {
                pResultImg[i * pEgisSensor->sensor_width_show + j] = 0x00;
            }
            bad_col_row_cnt++;
        }
    }
    //////////////////////////////////////////////////////////////////////

    // calculate col average /////////////////////////////////////////////
    for (j = 0; j < pEgisSensor->sensor_width_show; j++) {
        single_col_sum[j] = 0;
#if (EGIS_SENSOR == 701)
        for (i = 0; i < pEgisSensor->sensor_height_show - 2; i++) {
#else
        for (i = 0; i < pEgisSensor->sensor_height_show; i++) {
#endif
            single_col_sum[j] += ptest_inst->picture_bkg16[i * pEgisSensor->sensor_width_show + j];
        }
    }

    for (j = 1; j < pEgisSensor->sensor_width_show; j++) {
        // start with second col
        bad_col_row_offset =
            single_col_sum[j - 1] * ptest_inst->egfps_test_config.bad_rowcol_percentage / 100;

        if (fabs((double)single_col_sum[j] - (double)single_col_sum[j - 1]) > bad_col_row_offset) {
#if (EGIS_SENSOR == 701)
            for (i = 0; i < pEgisSensor->sensor_height_show - 2; i++) {
#else
            for (i = 0; i < pEgisSensor->sensor_height_show; i++) {
#endif
                pResultImg[i * pEgisSensor->sensor_width_show + j] = 0x00;
            }
            bad_col_row_cnt++;
        }
    }
    //////////////////////////////////////////////////////////////////////

    ptest_inst->egfps_test_result.total_rowcol_fail = bad_col_row_cnt;

    free(single_row_sum);
    free(single_col_sum);

    ptest_inst->egfps_test_result.test_items |= STATE_DIRTY_DOTS_TESTED;

    if (bad_col_row_cnt > ptest_inst->egfps_test_config.max_bad_rowcol) {
        ptest_inst->egfps_test_result.hw_result = 3;
        ptest_inst->egfps_test_result.sw_result = SW_BIN_BAD_COL_ROW;
        return SW_BIN_BAD_COL_ROW;
    }

    return SW_BIN_PASS;
}

DWORD
egfps_bad_pxl_test2(HANDLE hTestInst, CSensor* pEgisSensor, unsigned char* pResultImg) {
    OutputDebugString("-egfps_bad_pxl_test2-\n");

    EGFPS_TEST_INST* ptest_inst = (EGFPS_TEST_INST*)hTestInst;

    int i;
    double bad_pixel_offset;
    bad_pixel_offset = (double)ptest_inst->egfps_test_result.img_avg *
                       ptest_inst->egfps_test_config.bad_pxl_percentage2 / 100;

    ImgTestResult img_test_result;
    ImgTestCriteria img_test_criteria;
    unsigned char test_option;
    BOOL TooManyDirtyDots;

    memset(&img_test_criteria, 0x00, sizeof(img_test_criteria));
    img_test_criteria.cds_bad_dot_th = (unsigned short)bad_pixel_offset;

    memset(&img_test_result, 0x00, sizeof(img_test_result));
    test_option = TEST_DEFECT_DOT;

    img_test_criteria.max_allowed_fail_pxl = ptest_inst->egfps_test_config.max_bad_pxl2;

    if (ptest_inst->egfps_test_config.do_consecutive_fail_test2 == 1) {
        test_option |= TEST_CONSECUTIVE_DOT;
        img_test_criteria.consecutive_fail_th = ptest_inst->egfps_test_config.consecutive_fail_pxl2;
    }

    if (ptest_inst->egfps_test_config.do_consecutive_fail_test2 == 2) {
        test_option |= TEST_CONNECTED_FAIL;
        img_test_criteria.consecutive_fail_th = ptest_inst->egfps_test_config.consecutive_fail_pxl2;
    }
    vEvaluateImage16(&img_test_criteria, &img_test_result, &TooManyDirtyDots,
                     ptest_inst->dirty_dots, &ptest_inst->iva, ptest_inst->picture_bkg16,
                     pEgisSensor->sensor_width_show, pEgisSensor->sensor_height_show, test_option);
    ptest_inst->egfps_test_result.total_fail2 = img_test_result.DefectDots;
    ptest_inst->egfps_test_result.max_consecutive_fail2 = img_test_result.MaxConsecutiveDirtyDots;
    ptest_inst->egfps_test_result.max_connected_fail2 = img_test_result.MaxConnectedDirtyDots;

    memset(pResultImg, 0xFF, pEgisSensor->sensor_total_pixels_show);
    for (i = 0; i < pEgisSensor->sensor_total_pixels_show; i++) {
        if (ptest_inst->dirty_dots[i]) {
            pResultImg[i] = 0x00;
        }
    }

    ptest_inst->egfps_test_result.test_items |= STATE_DIRTY_DOTS2_TESTED;

    if ((img_test_result.DefectDots > ptest_inst->egfps_test_config.max_bad_pxl2) ||
        TooManyDirtyDots) {
        ptest_inst->egfps_test_result.hw_result = 3;
        ptest_inst->egfps_test_result.sw_result = SW_BIN_DIRTY_DOTS;
        return SW_BIN_DIRTY_DOTS;
    }

    return SW_BIN_PASS;
}

DWORD
egfps_bad_col_row_test2(HANDLE hTestInst, CSensor* pEgisSensor, unsigned char* pResultImg) {
    OutputDebugString("-egfps_bad_col_row_test2-\n");

    EGFPS_TEST_INST* ptest_inst = (EGFPS_TEST_INST*)hTestInst;

    int* single_row_sum = (int*)malloc(pEgisSensor->sensor_height_show * sizeof(int));
    if (!single_row_sum) {
        ptest_inst->egfps_test_result.hw_result = 5;
        ptest_inst->egfps_test_result.sw_result = SW_BIN_GENERAL_FAIL;
        return SW_BIN_GENERAL_FAIL;
    }

    int* single_col_sum = (int*)malloc(pEgisSensor->sensor_width_show * sizeof(int));
    if (!single_col_sum) {
        free(single_row_sum);
        ptest_inst->egfps_test_result.hw_result = 5;
        ptest_inst->egfps_test_result.sw_result = SW_BIN_GENERAL_FAIL;
        return SW_BIN_GENERAL_FAIL;
    }

    int i, j;
    double bad_col_row_offset;
    int bad_col_row_cnt;

    bad_col_row_cnt = 0;
    memset(pResultImg, 0xFF, pEgisSensor->sensor_total_pixels_show);

    // calculate row average ///////////////////////////////////////////
#if (EGIS_SENSOR == 701)
    for (i = 0; i < pEgisSensor->sensor_height_show - 2; i++) {
#else
    for (i = 0; i < pEgisSensor->sensor_height_show; i++) {
#endif
        single_row_sum[i] = 0;
        for (j = 0; j < pEgisSensor->sensor_width_show; j++) {
            single_row_sum[i] += ptest_inst->picture_bkg16[i * pEgisSensor->sensor_width_show + j];
        }
    }

#if (EGIS_SENSOR == 701)
    for (i = 1; i < pEgisSensor->sensor_height_show - 2; i++) {
#else
    for (i = 1; i < pEgisSensor->sensor_height_show; i++) {
#endif
        // start with second row
        bad_col_row_offset = (double)single_row_sum[i - 1] *
                             ptest_inst->egfps_test_config.bad_rowcol_percentage2 / 100;

        if (fabs((double)single_row_sum[i] - (double)single_row_sum[i - 1]) > bad_col_row_offset) {
            for (j = 0; j < pEgisSensor->sensor_width_show; j++) {
                pResultImg[i * pEgisSensor->sensor_width_show + j] = 0x00;
            }
            bad_col_row_cnt++;
        }
    }
    //////////////////////////////////////////////////////////////////////

    // calculate col average /////////////////////////////////////////////
    for (j = 0; j < pEgisSensor->sensor_width_show; j++) {
        single_col_sum[j] = 0;
#if (EGIS_SENSOR == 701)
        for (i = 0; i < pEgisSensor->sensor_height_show - 2; i++) {
#else
        for (i = 0; i < pEgisSensor->sensor_height_show; i++) {
#endif
            single_col_sum[j] += ptest_inst->picture_bkg16[i * pEgisSensor->sensor_width_show + j];
        }
    }

    for (j = 1; j < pEgisSensor->sensor_width_show; j++) {
        // start with second col
        bad_col_row_offset =
            single_col_sum[j - 1] * ptest_inst->egfps_test_config.bad_rowcol_percentage2 / 100;

        if (fabs((double)single_col_sum[j] - (double)single_col_sum[j - 1]) > bad_col_row_offset) {
#if (EGIS_SENSOR == 701)
            for (i = 0; i < pEgisSensor->sensor_height_show - 2; i++) {
#else
            for (i = 0; i < pEgisSensor->sensor_height_show; i++) {
#endif
                pResultImg[i * pEgisSensor->sensor_width_show + j] = 0x00;
            }
            bad_col_row_cnt++;
        }
    }
    //////////////////////////////////////////////////////////////////////

    ptest_inst->egfps_test_result.total_rowcol_fail2 = bad_col_row_cnt;

    free(single_row_sum);
    free(single_col_sum);

    ptest_inst->egfps_test_result.test_items |= STATE_DIRTY_DOTS2_TESTED;

    if (bad_col_row_cnt > ptest_inst->egfps_test_config.max_bad_rowcol2) {
        ptest_inst->egfps_test_result.hw_result = 3;
        ptest_inst->egfps_test_result.sw_result = SW_BIN_BAD_COL_ROW;
        return SW_BIN_BAD_COL_ROW;
    }

    return SW_BIN_PASS;
}

// for lens type sensors
#define SHOW_CENTROID 1

DWORD
egfps_lens_centroid_test(HANDLE hTestInst, CSensor* pEgisSensor, unsigned char* pResultImg) {
    OutputDebugString("-egfps_lens_centroid_test-\n");

    EGFPS_TEST_INST* ptest_inst = (EGFPS_TEST_INST*)hTestInst;

    BOOL r_value;
    char buf[MAX_PATH];

    // Set imge gain
    r_value = pEgisSensor->bSetPGASetting(ptest_inst->egfps_test_config.img_gain);
    if (!r_value) {
        ptest_inst->egfps_test_result.hw_result = 5;
        ptest_inst->egfps_test_result.sw_result = SW_BIN_REG_FAIL;
        return SW_BIN_REG_FAIL;
    }

    // Set Target Exposure Time
    r_value = pEgisSensor->bSetExposureTime(&ptest_inst->egfps_test_config.centeroid_expo_time);
    if (!r_value) {
        ptest_inst->egfps_test_result.hw_result = 5;
        ptest_inst->egfps_test_result.sw_result = SW_BIN_REG_FAIL;
        return SW_BIN_REG_FAIL;
    }

#if ((EGIS_SENSOR == 701) || (EGIS_SENSOR == 711) || (EGIS_SENSOR == 713))
    r_value = pEgisSensor->bGetFrame16(ptest_inst->picture_raw16, 0);
#else
    // drop frames
    r_value = pEgisSensor->bGetFrame16(ptest_inst->picture_raw16, FRAME_DROP);
#endif

    if (!r_value) {
        ptest_inst->egfps_test_result.hw_result = 5;
        ptest_inst->egfps_test_result.sw_result = SW_BIN_GETIMAGE_FAIL;
        return SW_BIN_REG_FAIL;
    }

    // get couple frames and average it
    r_value = pEgisSensor->bGetAverageFrameAndStdDev16(
        ptest_inst->picture_raw16, NULL, ptest_inst->egfps_test_config.ImageAverageCount, NULL);
    if (!r_value) {
        ptest_inst->egfps_test_result.hw_result = 5;
        ptest_inst->egfps_test_result.sw_result = SW_BIN_GETIMAGE_FAIL;
        return SW_BIN_GETIMAGE_FAIL;
    }

#if defined(WIN32)
    // Scale 16 bit image to 8 bit image
    vScaleImage16ToImage8(pEgisSensor->sensor_width_show, pEgisSensor->sensor_height_show, NULL,
                          ptest_inst->picture_raw16, pResultImg);
#elif defined(RBS_SDK_USE)
    // Scale 16 bit image to 8 bit image
    vScaleImage16ToImage8(pEgisSensor->sensor_width_show, pEgisSensor->sensor_height_show, NULL,
                          ptest_inst->picture_raw16, pResultImg, FALSE, NULL, NULL);
#endif

    ptest_inst->egfps_test_result.test_items |= STATE_CENTEROID_TESTED;

    unsigned int centerX;
    unsigned int centerY;

    LensCentroid(pResultImg, pEgisSensor->sensor_width_show, pEgisSensor->sensor_height_show,
                 &centerX, &centerY);

#if (IS_EGIS_SENSOR_LENS_TYPE)
    int x_offset, y_offset;

    ptest_inst->egfps_test_result.lens_center_x = centerX;
    ptest_inst->egfps_test_result.lens_center_y = centerY;

    x_offset = ptest_inst->egfps_test_result.lens_center_x -
               ptest_inst->egfps_test_config.lens_design_center_x;
    y_offset = ptest_inst->egfps_test_result.lens_center_y -
               ptest_inst->egfps_test_config.lens_design_center_y;

    ptest_inst->egfps_test_result.lens_center_offset =
        (int)sqrt((double)x_offset * x_offset + y_offset * y_offset);

    snprintf(buf, MAX_PATH, "lens_center_x (%d,%d), lens design (%d,%d), dist = %d\n", centerX,
             centerY, ptest_inst->egfps_test_config.lens_design_center_x,
             ptest_inst->egfps_test_config.lens_design_center_y,
             ptest_inst->egfps_test_result.lens_center_offset);
    OutputDebugString(buf);

    int i;
    unsigned short sw_mean;
    int pxl_sum = 0;
    for (i = 0; i < pEgisSensor->sensor_total_pixels_show; i++) {
        pxl_sum += ptest_inst->picture_raw16[i];
    }
    sw_mean = (unsigned short)(pxl_sum / pEgisSensor->sensor_total_pixels_show);

    // check min with bkg min, workaround for LED board malfunction cause average min too low
    if (sw_mean < (ptest_inst->egfps_test_config.bkg_avg_min)) {
        ptest_inst->egfps_test_result.hw_result = 5;
        ptest_inst->egfps_test_result.sw_result = SW_BIN_USB_FAIL;

        // ptest_inst->egfps_test_result.lens_center_x = 0;
        // ptest_inst->egfps_test_result.lens_center_y = 0;

        x_offset = ptest_inst->egfps_test_result.lens_center_x -
                   ptest_inst->egfps_test_config.lens_design_center_x;
        y_offset = ptest_inst->egfps_test_result.lens_center_y -
                   ptest_inst->egfps_test_config.lens_design_center_y;
        ptest_inst->egfps_test_result.lens_center_offset =
            (int)sqrt((double)x_offset * x_offset + y_offset * y_offset);

        snprintf(buf, MAX_PATH,
                 "!!bkg too dark, change lens_center_x (%d,%d), lens design (%d,%d), dist = %d\n",
                 centerX, centerY, ptest_inst->egfps_test_config.lens_design_center_x,
                 ptest_inst->egfps_test_config.lens_design_center_y,
                 ptest_inst->egfps_test_result.lens_center_offset);
        OutputDebugString(buf);

        return SW_BIN_USB_FAIL;
    }

#endif

#if (SHOW_CENTROID)
#define MARK_LEN 10
    int j;

    // tested
    for (i = (centerX > MARK_LEN) ? ((int)centerX - MARK_LEN) : 0;
         (i < (int)centerX + MARK_LEN) && (i < pEgisSensor->sensor_width_show); i++) {
        pResultImg[centerY * pEgisSensor->sensor_width_show + i] = 128;
    }

    for (j = (centerY > MARK_LEN) ? ((int)centerY - MARK_LEN) : 0;
         (j < (int)centerY + MARK_LEN) && (j < pEgisSensor->sensor_height_show); j++) {
        pResultImg[j * pEgisSensor->sensor_width_show + centerX] = 128;
    }

    // design
    for (i = ptest_inst->egfps_test_config.lens_design_center_x - MARK_LEN;
         i < ptest_inst->egfps_test_config.lens_design_center_x + MARK_LEN; i++) {
        pResultImg[ptest_inst->egfps_test_config.lens_design_center_y *
                       pEgisSensor->sensor_width_show +
                   i] = 0;
    }

    for (j = ptest_inst->egfps_test_config.lens_design_center_y - MARK_LEN;
         j < ptest_inst->egfps_test_config.lens_design_center_y + MARK_LEN; j++) {
        pResultImg[j * pEgisSensor->sensor_width_show +
                   ptest_inst->egfps_test_config.lens_design_center_x] = 0;
    }

#endif

    if (ptest_inst->egfps_test_result.lens_center_offset >
        ptest_inst->egfps_test_config.lens_center_tolerance_line) {
        ptest_inst->egfps_test_result.hw_result = 5;
        ptest_inst->egfps_test_result.sw_result = SW_BIN_LENS_CENTEROID_FAIL;
        return SW_BIN_LENS_CENTEROID_FAIL;
    }

    // Check ROI (Region of Interest)
    if (((int)centerX < (ptest_inst->egfps_test_config.lens_roi_x / 2)) ||
        ((((int)centerX - (ptest_inst->egfps_test_config.lens_roi_x / 2)) +
          ptest_inst->egfps_test_config.lens_roi_x) > pEgisSensor->sensor_width_show) ||
        ((int)centerY < (ptest_inst->egfps_test_config.lens_roi_y / 2)) ||
        ((((int)centerY - (ptest_inst->egfps_test_config.lens_roi_y / 2)) +
          ptest_inst->egfps_test_config.lens_roi_y) > pEgisSensor->sensor_height_show)) {
        ptest_inst->egfps_test_result.hw_result = 5;
        ptest_inst->egfps_test_result.sw_result = SW_BIN_LENS_CENTEROID_FAIL;
        return SW_BIN_LENS_CENTEROID_FAIL;
    }

    ptest_inst->iva.col_start = centerX - (ptest_inst->egfps_test_config.lens_roi_x / 2);
    ptest_inst->iva.col_end =
        ptest_inst->iva.col_start + ptest_inst->egfps_test_config.lens_roi_x - 1;
    ptest_inst->iva.row_start = centerY - (ptest_inst->egfps_test_config.lens_roi_y / 2);
    ptest_inst->iva.row_end =
        ptest_inst->iva.row_start + ptest_inst->egfps_test_config.lens_roi_y - 1;

    return SW_BIN_PASS;
}

DWORD
egfps_lens_centroid_test_from_nvm(HANDLE hTestInst, CSensor* pEgisSensor, unsigned int CenterX,
                                  unsigned int CenterY) {
    OutputDebugString("-egfps_lens_centroid_test_from_nvm-\n");

    EGFPS_TEST_INST* ptest_inst = (EGFPS_TEST_INST*)hTestInst;

    ptest_inst->egfps_test_result.test_items |= STATE_CENTEROID_TESTED;

#if (IS_EGIS_SENSOR_LENS_TYPE)
    int x_offset, y_offset;

    ptest_inst->egfps_test_result.lens_center_x = CenterX;
    ptest_inst->egfps_test_result.lens_center_y = CenterY;

    x_offset = ptest_inst->egfps_test_result.lens_center_x -
               ptest_inst->egfps_test_config.lens_design_center_x;
    y_offset = ptest_inst->egfps_test_result.lens_center_y -
               ptest_inst->egfps_test_config.lens_design_center_y;

    ptest_inst->egfps_test_result.lens_center_offset =
        (int)sqrt((double)x_offset * x_offset + y_offset * y_offset);

    char buf[MAX_PATH];

    snprintf(buf, MAX_PATH, "lens_center_x (%d,%d), lens design (%d,%d), dist = %d\n", CenterX,
             CenterY, ptest_inst->egfps_test_config.lens_design_center_x,
             ptest_inst->egfps_test_config.lens_design_center_y,
             ptest_inst->egfps_test_result.lens_center_offset);
    OutputDebugString(buf);

#endif

    if (ptest_inst->egfps_test_result.lens_center_offset >
        ptest_inst->egfps_test_config.lens_center_tolerance_line) {
        ptest_inst->egfps_test_result.hw_result = 5;
        ptest_inst->egfps_test_result.sw_result = SW_BIN_LENS_CENTEROID_FAIL;
        return SW_BIN_LENS_CENTEROID_FAIL;
    }

    // Check ROI (Region of Interest)
    if (((int)CenterX < (ptest_inst->egfps_test_config.lens_roi_x / 2)) ||
        ((((int)CenterX - (ptest_inst->egfps_test_config.lens_roi_x / 2)) +
          ptest_inst->egfps_test_config.lens_roi_x) > pEgisSensor->sensor_width_show) ||
        ((int)CenterY < (ptest_inst->egfps_test_config.lens_roi_y / 2)) ||
        ((((int)CenterY - (ptest_inst->egfps_test_config.lens_roi_y / 2)) +
          ptest_inst->egfps_test_config.lens_roi_y) > pEgisSensor->sensor_height_show)) {
        ptest_inst->egfps_test_result.hw_result = 5;
        ptest_inst->egfps_test_result.sw_result = SW_BIN_LENS_CENTEROID_FAIL;
        return SW_BIN_LENS_CENTEROID_FAIL;
    }

    ptest_inst->iva.col_start = CenterX - (ptest_inst->egfps_test_config.lens_roi_x / 2);
    ptest_inst->iva.col_end =
        ptest_inst->iva.col_start + ptest_inst->egfps_test_config.lens_roi_x - 1;
    ptest_inst->iva.row_start = CenterY - (ptest_inst->egfps_test_config.lens_roi_y / 2);
    ptest_inst->iva.row_end =
        ptest_inst->iva.row_start + ptest_inst->egfps_test_config.lens_roi_y - 1;

    return SW_BIN_PASS;
}

DWORD
egfps_lens_mtf_test(HANDLE hTestInst, CSensor* pEgisSensor, double* pMtfResult,
                    unsigned short* pBlockMean, unsigned char* pResultImg) {
    OutputDebugString("-egfps_lens_mtf_test-\n");

    EGFPS_TEST_INST* ptest_inst = (EGFPS_TEST_INST*)hTestInst;

    BOOL r_value;

    // Set imge gain
    r_value = pEgisSensor->bSetPGASetting(ptest_inst->egfps_test_config.img_gain);
    if (!r_value) {
        ptest_inst->egfps_test_result.hw_result = 5;
        ptest_inst->egfps_test_result.sw_result = SW_BIN_REG_FAIL;
        return SW_BIN_REG_FAIL;
    }

    // Set Target Exposure Time
    r_value = pEgisSensor->bSetExposureTime(&ptest_inst->egfps_test_config.img_expo_time);
    if (!r_value) {
        ptest_inst->egfps_test_result.hw_result = 5;
        ptest_inst->egfps_test_result.sw_result = SW_BIN_REG_FAIL;
        return SW_BIN_REG_FAIL;
    }

#if ((EGIS_SENSOR == 701) || (EGIS_SENSOR == 711) || (EGIS_SENSOR == 713))
    r_value = pEgisSensor->bGetFrame16(ptest_inst->picture_raw16, 0);
#else
    // drop frames
    r_value = pEgisSensor->bGetFrame16(ptest_inst->picture_raw16, FRAME_DROP);
#endif

    if (!r_value) {
        ptest_inst->egfps_test_result.hw_result = 5;
        ptest_inst->egfps_test_result.sw_result = SW_BIN_GETIMAGE_FAIL;
        return SW_BIN_REG_FAIL;
    }

    // get couple frames and average it
    r_value = pEgisSensor->bGetAverageFrameAndStdDev16(
        ptest_inst->picture_raw16, NULL, ptest_inst->egfps_test_config.ImageAverageCount, NULL);
    if (!r_value) {
        ptest_inst->egfps_test_result.hw_result = 5;
        ptest_inst->egfps_test_result.sw_result = SW_BIN_GETIMAGE_FAIL;
        return SW_BIN_GETIMAGE_FAIL;
    }

#if defined(WIN32)
    // Scale 16 bit image to 8 bit image
    vScaleImage16ToImage8(pEgisSensor->sensor_width_show, pEgisSensor->sensor_height_show, NULL,
                          ptest_inst->picture_raw16, pResultImg);
#elif defined(RBS_SDK_USE)
    // Scale 16 bit image to 8 bit image
    vScaleImage16ToImage8(pEgisSensor->sensor_width_show, pEgisSensor->sensor_height_show, NULL,
                          ptest_inst->picture_raw16, pResultImg, FALSE, NULL, NULL);
#endif

    ptest_inst->egfps_test_result.test_items |= STATE_MTF_TESTED;

    for (int i = 1; i < 10; i++) {
        double tmp_mtf;
        unsigned int tmp_mean;
        unsigned char tmp_rv;

#if (EGIS_SENSOR == 713)
        MTFCounting713(pResultImg, pEgisSensor->sensor_width_show, pEgisSensor->sensor_height_show,
                       ptest_inst->egfps_test_result.lens_center_x,
                       ptest_inst->egfps_test_result.lens_center_y, (MTFBlockOption)i,
                       ptest_inst->egfps_test_config.lens_mtf_test_case,
                       ptest_inst->egfps_test_config.lens_mtf_scan_window, &tmp_mtf, &tmp_mean,
                       &tmp_rv);
#else
        MTFCounting(pResultImg, pEgisSensor->sensor_width_show, pEgisSensor->sensor_height_show,
                    ptest_inst->egfps_test_result.lens_center_x,
                    ptest_inst->egfps_test_result.lens_center_y, (MTFBlockOption)i,
                    ptest_inst->egfps_test_config.lens_mtf_test_case,
                    ptest_inst->egfps_test_config.lens_mtf_scan_window, &tmp_mtf, &tmp_mean,
                    &tmp_rv);
#endif
        pBlockMean[i - 1] = tmp_mean;
        pMtfResult[i - 1] = tmp_mtf;
    }

    // Center    Corner  Result
    // Bin1      Bin1    Bin1
    // Bin1      Bin2    Bin2 47   SW_BIN_LENS_MTF_CORNER_FAIL
    // Bin1      Bin4    Bin4 46   SW_BIN_LENS_MTF_FAIL2
    // Bin2      Bin1    Bin2 45   SW_BIN_LENS_MTF_FAIL
    // Bin2      Bin2    Bin2 48   SW_BIN_LENS_MTF_CORNER_FAIL2
    // Bin2      Bin4    Bin4 46   SW_BIN_LENS_MTF_FAIL2
    // Bin4      Bin1    Bin4 46   SW_BIN_LENS_MTF_FAIL2
    // Bin4      Bin2    Bin4 46   SW_BIN_LENS_MTF_FAIL2
    // Bin4      Bin4    Bin4 46   SW_BIN_LENS_MTF_FAIL2

    int center_bin_level;
    int corner_bin_level;

    if (pMtfResult[0] < ptest_inst->egfps_test_config.lens_mtf_center_floor) {
        if (pMtfResult[0] < ptest_inst->egfps_test_config.lens_mtf_center_floor2) {
            center_bin_level = 4;
        } else {
            center_bin_level = 2;
        }
    } else {
        center_bin_level = 1;
    }

    if ((pMtfResult[1] < ptest_inst->egfps_test_config.lens_mtf_corner_floor) ||
        (pMtfResult[2] < ptest_inst->egfps_test_config.lens_mtf_corner_floor) ||
        (pMtfResult[5] < ptest_inst->egfps_test_config.lens_mtf_corner_floor) ||
        (pMtfResult[6] < ptest_inst->egfps_test_config.lens_mtf_corner_floor)) {
        if ((pMtfResult[1] < ptest_inst->egfps_test_config.lens_mtf_corner_floor2) ||
            (pMtfResult[2] < ptest_inst->egfps_test_config.lens_mtf_corner_floor2) ||
            (pMtfResult[5] < ptest_inst->egfps_test_config.lens_mtf_corner_floor2) ||
            (pMtfResult[6] < ptest_inst->egfps_test_config.lens_mtf_corner_floor2)) {
            corner_bin_level = 4;
        } else {
            corner_bin_level = 2;
        }
    } else {
        corner_bin_level = 1;
    }

    ptest_inst->egfps_test_result.test_items |= STATE_MTF_TESTED;

    if (center_bin_level == 1) {
        if (corner_bin_level == 2) {
            ptest_inst->egfps_test_result.lens_mtf_test = 0x02;
            ptest_inst->egfps_test_result.hw_result = 2;
            ptest_inst->egfps_test_result.sw_result = SW_BIN_LENS_MTF_CORNER_FAIL;
            return SW_BIN_LENS_MTF_CORNER_FAIL;
        }

        if (corner_bin_level == 4) {
            ptest_inst->egfps_test_result.lens_mtf_test = 0x04;
            ptest_inst->egfps_test_result.hw_result = 4;
            ptest_inst->egfps_test_result.sw_result = SW_BIN_LENS_MTF_FAIL2;
            return SW_BIN_LENS_MTF_FAIL2;
        }
    }

    if (center_bin_level == 2) {
        if (corner_bin_level == 1) {
            ptest_inst->egfps_test_result.lens_mtf_test = 0x02;
            ptest_inst->egfps_test_result.hw_result = 2;
            ptest_inst->egfps_test_result.sw_result = SW_BIN_LENS_MTF_FAIL;
            return SW_BIN_LENS_MTF_FAIL;
        }

        if (corner_bin_level == 2) {
            ptest_inst->egfps_test_result.lens_mtf_test = 0x02;
            ptest_inst->egfps_test_result.hw_result = 2;
            ptest_inst->egfps_test_result.sw_result = SW_BIN_LENS_MTF_CORNER_FAIL2;
            return SW_BIN_LENS_MTF_CORNER_FAIL2;
        }

        if (corner_bin_level == 4) {
            ptest_inst->egfps_test_result.lens_mtf_test = 0x04;
            ptest_inst->egfps_test_result.hw_result = 4;
            ptest_inst->egfps_test_result.sw_result = SW_BIN_LENS_MTF_FAIL2;
            return SW_BIN_LENS_MTF_FAIL2;
        }
    }

    if (center_bin_level == 4) {
        ptest_inst->egfps_test_result.lens_mtf_test = 0x04;
        if (corner_bin_level == 1) {
            ptest_inst->egfps_test_result.hw_result = 4;
            ptest_inst->egfps_test_result.sw_result = SW_BIN_LENS_MTF_FAIL2;
            return SW_BIN_LENS_MTF_FAIL2;
        }

        if (corner_bin_level == 2) {
            ptest_inst->egfps_test_result.hw_result = 4;
            ptest_inst->egfps_test_result.sw_result = SW_BIN_LENS_MTF_FAIL2;
            return SW_BIN_LENS_MTF_FAIL2;
        }

        if (corner_bin_level == 4) {
            ptest_inst->egfps_test_result.hw_result = 4;
            ptest_inst->egfps_test_result.sw_result = SW_BIN_LENS_MTF_FAIL2;
            return SW_BIN_LENS_MTF_FAIL2;
        }
    }

    ptest_inst->egfps_test_result.lens_mtf_test = 0x01;
    return SW_BIN_PASS;
}

#if defined(WIN32)

DWORD
egfps_init_current_meter(HANDLE hTestInst, CSensor* pEgisSensor) {
    OutputDebugString("-egfps_init_current_meter-\n");

    EGFPS_TEST_INST* ptest_inst = (EGFPS_TEST_INST*)hTestInst;

    BOOL r_value;
    unsigned short value;

    // I2C Current Meter
    // Meter1 3.3V
    r_value = pEgisSensor->bSetMeterI2CID(0x01, METER_ADDRESS_V33);
    if (!r_value) {
        ptest_inst->egfps_test_result.hw_result = 5;
        ptest_inst->egfps_test_result.sw_result = SW_BIN_USB_FAIL;
        return SW_BIN_USB_FAIL;
    }

    // Select meter
    r_value = pEgisSensor->bSetSelectedMeter(0x01);
    if (!r_value) {
        ptest_inst->egfps_test_result.hw_result = 5;
        ptest_inst->egfps_test_result.sw_result = SW_BIN_USB_FAIL;
        return SW_BIN_USB_FAIL;
    }

    // Read Meter ID
    r_value = pEgisSensor->bReadMeterRegister(0xFF, &value);
    if ((!r_value) || (value != 0x2260)) {
        ptest_inst->egfps_test_result.hw_result = 5;
        ptest_inst->egfps_test_result.sw_result = SW_BIN_USB_FAIL;
        return SW_BIN_USB_FAIL;
    }

    // reset configuration
    r_value = pEgisSensor->bWriteMeterRegister(0x00, 0x4127);
    if (!r_value) {
        ptest_inst->egfps_test_result.hw_result = 5;
        ptest_inst->egfps_test_result.sw_result = SW_BIN_USB_FAIL;
        return SW_BIN_USB_FAIL;
    }

    // Meter2 3.3V
    r_value = pEgisSensor->bSetMeterI2CID(0x02, METER_ADDRESS_V18);
    if (!r_value) {
        ptest_inst->egfps_test_result.hw_result = 5;
        ptest_inst->egfps_test_result.sw_result = SW_BIN_USB_FAIL;
        return SW_BIN_USB_FAIL;
    }

    // Select meter
    r_value = pEgisSensor->bSetSelectedMeter(0x02);
    if (!r_value) {
        ptest_inst->egfps_test_result.hw_result = 5;
        ptest_inst->egfps_test_result.sw_result = SW_BIN_USB_FAIL;
        return SW_BIN_USB_FAIL;
    }

    // Read Meter ID
    r_value = pEgisSensor->bReadMeterRegister(0xFF, &value);
    if ((!r_value) || (value != 0x2260)) {
        ptest_inst->egfps_test_result.hw_result = 5;
        ptest_inst->egfps_test_result.sw_result = SW_BIN_USB_FAIL;
        return SW_BIN_USB_FAIL;
    }

    // reset configuration
    r_value = pEgisSensor->bWriteMeterRegister(0x00, 0x4127);
    if (!r_value) {
        ptest_inst->egfps_test_result.hw_result = 5;
        ptest_inst->egfps_test_result.sw_result = SW_BIN_USB_FAIL;
        return SW_BIN_USB_FAIL;
    }

    return SW_BIN_PASS;
}

DWORD
egfps_read_nvm(HANDLE hTestInst, CSensor* pEgisSensor, unsigned char* uid,
               EGFPS_FLASH_DATA_V2* pFlashDataPlan, unsigned char* pFlashDataValid,
               EGFPS_OTP_DATA* pOtpDataPlan) {
    OutputDebugString("-egfps_read_nvm-\n");

    EGFPS_TEST_INST* ptest_inst = (EGFPS_TEST_INST*)hTestInst;
    BOOL r_value;

    int i;
    char buf[MAX_PATH];
    unsigned char otp_data[32];

    BOOL bIsOTPUIDValid = FALSE;
    unsigned char uid_gen[9];

    // Get Sensor ID
    r_value = pEgisSensor->bGetSensorID(&ptest_inst->egfps_test_result.sensor_id);
    if (!r_value) {
        ptest_inst->egfps_test_result.hw_result = 5;
        ptest_inst->egfps_test_result.sw_result = SW_BIN_REG_FAIL;
        return SW_BIN_REG_FAIL;
    }

    sprintf(buf, "SensorID=%02X%02X%02X PxlID=%02X\n",
            ptest_inst->egfps_test_result.sensor_id.major_id,
            ptest_inst->egfps_test_result.sensor_id.minor_id,
            ptest_inst->egfps_test_result.sensor_id.revision,
            ptest_inst->egfps_test_result.sensor_id.pixel_id);
    OutputDebugString(buf);

    if (ptest_inst->egfps_test_result.sensor_id.major_id != 0x07) {
        ptest_inst->egfps_test_result.hw_result = 5;
        ptest_inst->egfps_test_result.sw_result = SW_BIN_OTP_FAIL;
        return SW_BIN_OTP_FAIL;
    }

    // Read UID from OTP
    r_value = pEgisSensor->bOTPReadData(otp_data);
    if (!r_value) {
        OutputDebugString("bOTPReadData Fail\n");
        ptest_inst->egfps_test_result.hw_result = 5;
        ptest_inst->egfps_test_result.sw_result = SW_BIN_OTP_FAIL;
        return SW_BIN_OTP_FAIL;
    }

    OutputDebugString("<=Read OTP Data\n");
    for (i = 0; i < 2; i++) {
        sprintf(buf,
                "%02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X\n",
                otp_data[16 * i], otp_data[16 * i + 1], otp_data[16 * i + 2], otp_data[16 * i + 3],
                otp_data[16 * i + 4], otp_data[16 * i + 5], otp_data[16 * i + 6],
                otp_data[16 * i + 7], otp_data[16 * i + 8], otp_data[16 * i + 9],
                otp_data[16 * i + 10], otp_data[16 * i + 11], otp_data[16 * i + 12],
                otp_data[16 * i + 13], otp_data[16 * i + 14], otp_data[16 * i + 15]);
        OutputDebugString(buf);
    }
    OutputDebugString("\n");

    // If the uid in otp_data is wrong in sepcial format, consider as invalid uid.
    int dUIDSpecialbyteNum = 0;
    for (i = 0; i < 9; i++) {
        if (otp_data[i + 0x17] == 0x00 || otp_data[i + 0x17] == 0x80 ||
            otp_data[i + 0x17] == 0x08 || otp_data[i + 0x17] == 0x88) {
            dUIDSpecialbyteNum++;
        }
    }

    if (((otp_data[0x17] == 0xFF) && (otp_data[0x18] == 0xFF) && (otp_data[0x19] == 0xFF) &&
         (otp_data[0x1A] == 0xFF) && (otp_data[0x1B] == 0xFF) && (otp_data[0x1C] == 0xFF) &&
         (otp_data[0x1D] == 0xFF) && (otp_data[0x1E] == 0xFF) && (otp_data[0x1F] == 0xFF)) ||
        ((otp_data[0x17] == 0x00) && (otp_data[0x18] == 0x00) && (otp_data[0x19] == 0x00) &&
         (otp_data[0x1A] == 0x00) && (otp_data[0x1B] == 0x00) && (otp_data[0x1C] == 0x00) &&
         (otp_data[0x1D] == 0x00) && (otp_data[0x1E] == 0x00) && (otp_data[0x1F] == 0x00)) ||
        (dUIDSpecialbyteNum == 9)) {
        // Generate UID from Arduino device uid
        unsigned char arduino_uid[16];

        r_value = pEgisSensor->bGetUID(arduino_uid);
        if (!r_value) {
            ptest_inst->egfps_test_result.hw_result = 5;
            ptest_inst->egfps_test_result.sw_result = SW_BIN_USB_FAIL;
            return SW_BIN_USB_FAIL;
        }

        vGenUID(uid_gen, arduino_uid);

        // Write Generated UID to sensor
        memcpy(otp_data + 0x17, uid_gen, 9);

        OutputDebugString("Writed OTP Data=>\n");
        for (i = 0; i < 2; i++) {
            sprintf(
                buf,
                "%02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X\n",
                otp_data[16 * i], otp_data[16 * i + 1], otp_data[16 * i + 2], otp_data[16 * i + 3],
                otp_data[16 * i + 4], otp_data[16 * i + 5], otp_data[16 * i + 6],
                otp_data[16 * i + 7], otp_data[16 * i + 8], otp_data[16 * i + 9],
                otp_data[16 * i + 10], otp_data[16 * i + 11], otp_data[16 * i + 12],
                otp_data[16 * i + 13], otp_data[16 * i + 14], otp_data[16 * i + 15]);
            OutputDebugString(buf);
        }
        OutputDebugString("\n");

        r_value = pEgisSensor->bOTPWriteData(otp_data);
        if (!r_value) {
            OutputDebugString("bOTPReadData Fail\n");
            ptest_inst->egfps_test_result.hw_result = 5;
            ptest_inst->egfps_test_result.sw_result = SW_BIN_OTP_FAIL;
            return SW_BIN_OTP_FAIL;
        }

        // Read UID from OTP
        r_value = pEgisSensor->bOTPReadData(otp_data);
        if (!r_value) {
            OutputDebugString("bOTPReadData Fail\n");
            ptest_inst->egfps_test_result.hw_result = 5;
            ptest_inst->egfps_test_result.sw_result = SW_BIN_OTP_FAIL;
            return SW_BIN_OTP_FAIL;
        }

        OutputDebugString("<=Read OTP Data\n");
        for (i = 0; i < 2; i++) {
            sprintf(
                buf,
                "%02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X\n",
                otp_data[16 * i], otp_data[16 * i + 1], otp_data[16 * i + 2], otp_data[16 * i + 3],
                otp_data[16 * i + 4], otp_data[16 * i + 5], otp_data[16 * i + 6],
                otp_data[16 * i + 7], otp_data[16 * i + 8], otp_data[16 * i + 9],
                otp_data[16 * i + 10], otp_data[16 * i + 11], otp_data[16 * i + 12],
                otp_data[16 * i + 13], otp_data[16 * i + 14], otp_data[16 * i + 15]);
            OutputDebugString(buf);
        }
        OutputDebugString("\n");

        if (memcmp(otp_data + 0x17, uid_gen, 9)) {
            bIsOTPUIDValid = FALSE;
        } else {
            bIsOTPUIDValid = TRUE;
        }
    } else {
        bIsOTPUIDValid = TRUE;
#if (CHECK_DUPLICATE_UID & OVERWRITE_DUPLICATE_UID)
        vCheckDuplicateUID(otp_data + 0x17);
#endif
    }

#if (CHECK_DUPLICATE_UID)
    vUpdateUIDTable(otp_data + 0x17);
#endif

    memcpy(pOtpDataPlan, otp_data, 32);

    if (ptest_inst->egfps_test_config.check_otp_data_valid) {
        if (pOtpDataPlan->software_bin != 0x01) {
            ptest_inst->egfps_test_result.hw_result = 5;
            ptest_inst->egfps_test_result.sw_result = SW_BIN_OTP_CHECK_FAIL;
            return SW_BIN_OTP_CHECK_FAIL;
        }
    }

    if (ptest_inst->egfps_test_config.package_has_flash) {
        unsigned short chk_sum;
        unsigned short chk_sum_from_flash;

        unsigned char FlashDataValid = 0x00;

        // Read Calibration Data from Flash
        unsigned char flash_data[1024];
        char buf[MAX_PATH];

        int retry_cnt = FLASH_RW_RETRY;

        while (retry_cnt-- > 0) {
            r_value = pEgisSensor->bFlashReadData(0, 1, flash_data);
            if (!r_value) {
                OutputDebugString("bFlashReadData Fail\n");
                ptest_inst->egfps_test_result.hw_result = 5;
                ptest_inst->egfps_test_result.sw_result = SW_BIN_FLASH_FAIL;
                return SW_BIN_FLASH_FAIL;
            }

            OutputDebugString("<=Read Flash Data\n");
            for (i = 0; i < 4; i++) {
                sprintf(buf,
                        "%02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X "
                        "%02X %02X\n",
                        flash_data[16 * i], flash_data[16 * i + 1], flash_data[16 * i + 2],
                        flash_data[16 * i + 3], flash_data[16 * i + 4], flash_data[16 * i + 5],
                        flash_data[16 * i + 6], flash_data[16 * i + 7], flash_data[16 * i + 8],
                        flash_data[16 * i + 9], flash_data[16 * i + 10], flash_data[16 * i + 11],
                        flash_data[16 * i + 12], flash_data[16 * i + 13], flash_data[16 * i + 14],
                        flash_data[16 * i + 15]);
                OutputDebugString(buf);
            }
            OutputDebugString("\n");

            chk_sum = 0x0000;
            for (i = 0; i < 1024 - 2; i++) {
                chk_sum += flash_data[i];
            }

            chk_sum_from_flash = (flash_data[1022] << 8) + flash_data[1023];

            if ((chk_sum == chk_sum_from_flash) && (chk_sum != 0x0000)) {
                FlashDataValid |= FLASH_ALL_DATA_VALID_BIT;
                break;
            }
        }

        vCheckFlashDataPlan((EGFPS_FLASH_DATA_V2*)flash_data, &FlashDataValid);

        if (bIsOTPUIDValid) {
            // Update Flash's uid if uid from OTP is valid.
            memcpy(((EGFPS_FLASH_DATA_V2*)flash_data)->uid, pOtpDataPlan->uid, 9);
            memcpy(uid, pOtpDataPlan->uid, 9);
        } else {
            // If Flash uid is valid, use it
            if (FlashDataValid & FLASH_UID_VALID_BIT) {
                memcpy(uid, ((EGFPS_FLASH_DATA_V2*)flash_data)->uid, 9);
            } else {
                // use generated uid
                memcpy(((EGFPS_FLASH_DATA_V2*)flash_data)->uid, uid_gen, 9);
                memcpy(uid, uid_gen, 9);
            }
        }

        memcpy(pFlashDataPlan, flash_data, 1024);
        *pFlashDataValid = FlashDataValid;

        if (ptest_inst->egfps_test_config.check_flash_data_valid) {
            if ((!(FlashDataValid & FLASH_UID_VALID_BIT)) ||
                (!(FlashDataValid & FLASH_PID_VALID_BIT)) ||
                (!(FlashDataValid & FLASH_MPINFO_VALID_BIT)) ||
                (!(FlashDataValid & FLASH_CALIBRATION_DATA_VALID_BIT)) ||
                (!(FlashDataValid & FLASH_ALL_DATA_VALID_BIT)) ||
                (((EGFPS_FLASH_DATA_V2*)flash_data)->mt_result != 0x0F)) {
                ptest_inst->egfps_test_result.hw_result = 5;
                ptest_inst->egfps_test_result.sw_result = SW_BIN_FLASH_CHECK_FAIL;

                OutputDebugString("<=Check Flash Valid Fail\n");
                for (i = 0; i < (1024 / 16); i++) {
                    sprintf(buf,
                            "%02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X "
                            "%02X %02X\n",
                            flash_data[16 * i], flash_data[16 * i + 1], flash_data[16 * i + 2],
                            flash_data[16 * i + 3], flash_data[16 * i + 4], flash_data[16 * i + 5],
                            flash_data[16 * i + 6], flash_data[16 * i + 7], flash_data[16 * i + 8],
                            flash_data[16 * i + 9], flash_data[16 * i + 10],
                            flash_data[16 * i + 11], flash_data[16 * i + 12],
                            flash_data[16 * i + 13], flash_data[16 * i + 14],
                            flash_data[16 * i + 15]);
                    OutputDebugString(buf);
                }
                OutputDebugString("\n");

                return SW_BIN_FLASH_CHECK_FAIL;
            }
        }
    } else {
        if (bIsOTPUIDValid) {
            memcpy(uid, pOtpDataPlan->uid, 9);
        } else {
            memset(uid, 0x00, 9);
        }
    }

    return SW_BIN_PASS;
}

DWORD
egfps_update_flash(HANDLE hTestInst, CSensor* pEgisSensor, EGFPS_FLASH_DATA_V2* pFlashDataPlan) {
    OutputDebugString("-egfps_update_flash-\n");

    EGFPS_TEST_INST* ptest_inst = (EGFPS_TEST_INST*)hTestInst;
#if ((EGIS_SENSOR == 701) || (EGIS_SENSOR == 711) || (EGIS_SENSOR == 713))

    BOOL r_value;
    int i;
    char buf[MAX_PATH];
    unsigned char* pData = (unsigned char*)pFlashDataPlan;
    unsigned char flash_data[1024];

    int retry_cnt = FLASH_RW_RETRY;

    while (retry_cnt-- > 0) {
        // Set WE before CE
        r_value = pEgisSensor->bFlashWriteEnable();
        if (!r_value) {
            ptest_inst->egfps_test_result.hw_result = 5;
            ptest_inst->egfps_test_result.sw_result = SW_BIN_FLASH_FAIL;
            return SW_BIN_FLASH_FAIL;
        }

        // Polling WEL(Write Enable Latch) bit
        r_value = pEgisSensor->bFlashWaitWriteEnableLatch();
        if (!r_value) {
            ptest_inst->egfps_test_result.hw_result = 5;
            ptest_inst->egfps_test_result.sw_result = SW_BIN_FLASH_FAIL;
            return SW_BIN_FLASH_FAIL;
        }

#if (DO_FLASH_CHIP_ERASE)
        // Send CE command
        r_value = pEgisSensor->bFlashEraseAll();
        if (!r_value) {
            ptest_inst->egfps_test_result.hw_result = 5;
            ptest_inst->egfps_test_result.sw_result = SW_BIN_FLASH_FAIL;
            return SW_BIN_FLASH_FAIL;
        }
#else
        // Sector(4K) 0 Erase
        r_value = pEgisSensor->bFlashEraseSector(0x000000);
        if (!r_value) {
            ptest_inst->egfps_test_result.hw_result = 5;
            ptest_inst->egfps_test_result.sw_result = SW_BIN_FLASH_FAIL;
            return SW_BIN_FLASH_FAIL;
        }
#endif

        // Wait WIP bit
        r_value = pEgisSensor->bFlashWaitWriteDone();
        if (!r_value) {
            ptest_inst->egfps_test_result.hw_result = 5;
            ptest_inst->egfps_test_result.sw_result = SW_BIN_FLASH_FAIL;
            return SW_BIN_FLASH_FAIL;
        }

        // calculate chk sum
        unsigned short chk_sum = 0x0000;
        for (i = 0; i < 1022; i++) {
            chk_sum += pData[i];
        }

        pData[1022] = (chk_sum & 0xFF00) >> 8;
        pData[1023] = (chk_sum & 0x00FF);

        OutputDebugString("Write Flash Data=>\n");
        for (i = 0; i < 4; i++) {
            sprintf(
                buf,
                "%02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X\n",
                pData[16 * i], pData[16 * i + 1], pData[16 * i + 2], pData[16 * i + 3],
                pData[16 * i + 4], pData[16 * i + 5], pData[16 * i + 6], pData[16 * i + 7],
                pData[16 * i + 8], pData[16 * i + 9], pData[16 * i + 10], pData[16 * i + 11],
                pData[16 * i + 12], pData[16 * i + 13], pData[16 * i + 14], pData[16 * i + 15]);
            OutputDebugString(buf);
        }

        OutputDebugString("\n");

        for (i = 0; i < 4; i++) {
            // Set WE before PP
            r_value = pEgisSensor->bFlashWriteEnable();
            if (!r_value) {
                ptest_inst->egfps_test_result.hw_result = 5;
                ptest_inst->egfps_test_result.sw_result = SW_BIN_FLASH_FAIL;
                return SW_BIN_FLASH_FAIL;
            }

            // Polling WEL(Write Enable Latch) bit
            r_value = pEgisSensor->bFlashWaitWriteEnableLatch();
            if (!r_value) {
                ptest_inst->egfps_test_result.hw_result = 5;
                ptest_inst->egfps_test_result.sw_result = SW_BIN_FLASH_FAIL;
                return SW_BIN_FLASH_FAIL;
            }

            // PP Command
            r_value = pEgisSensor->bFlashWriteData_256(i * 256, pData + i * 256);
            if (!r_value) {
                ptest_inst->egfps_test_result.hw_result = 5;
                ptest_inst->egfps_test_result.sw_result = SW_BIN_FLASH_FAIL;
                return SW_BIN_FLASH_FAIL;
            }

            // Wait WIP bit
            r_value = pEgisSensor->bFlashWaitWriteDone();
            if (!r_value) {
                ptest_inst->egfps_test_result.hw_result = 5;
                ptest_inst->egfps_test_result.sw_result = SW_BIN_FLASH_FAIL;
                return SW_BIN_FLASH_FAIL;
            }
        }

        r_value = pEgisSensor->bFlashReadData(0, 1, flash_data);
        if (!r_value) {
            ptest_inst->egfps_test_result.hw_result = 5;
            ptest_inst->egfps_test_result.sw_result = SW_BIN_FLASH_FAIL;
            return SW_BIN_FLASH_FAIL;
        }

        OutputDebugString("<=Read Flash Data\n");
        for (i = 0; i < 4; i++) {
            sprintf(
                buf,
                "%02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X\n",
                flash_data[16 * i], flash_data[16 * i + 1], flash_data[16 * i + 2],
                flash_data[16 * i + 3], flash_data[16 * i + 4], flash_data[16 * i + 5],
                flash_data[16 * i + 6], flash_data[16 * i + 7], flash_data[16 * i + 8],
                flash_data[16 * i + 9], flash_data[16 * i + 10], flash_data[16 * i + 11],
                flash_data[16 * i + 12], flash_data[16 * i + 13], flash_data[16 * i + 14],
                flash_data[16 * i + 15]);
            OutputDebugString(buf);
        }
        OutputDebugString("\n");

        if (memcmp(flash_data, pData, 1024) == 0) {
            break;
        } else {
            for (i = 0; i < (1024 / 16); i++) {
                OutputDebugString("Flash Read/Write Fail\n");
                if (memcmp(flash_data + i * 16, pData + i * 16, 16)) {
                    sprintf(buf,
                            "Write(%d):%02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X "
                            "%02X %02X %02X %02X\n",
                            i * 16, pData[16 * i], pData[16 * i + 1], pData[16 * i + 2],
                            pData[16 * i + 3], pData[16 * i + 4], pData[16 * i + 5],
                            pData[16 * i + 6], pData[16 * i + 7], pData[16 * i + 8],
                            pData[16 * i + 9], pData[16 * i + 10], pData[16 * i + 11],
                            pData[16 * i + 12], pData[16 * i + 13], pData[16 * i + 14],
                            pData[16 * i + 15]);
                    OutputDebugString(buf);

                    sprintf(buf,
                            "Read(%d): %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X "
                            "%02X %02X %02X %02X\n",
                            i * 16, flash_data[16 * i], flash_data[16 * i + 1],
                            flash_data[16 * i + 2], flash_data[16 * i + 3], flash_data[16 * i + 4],
                            flash_data[16 * i + 5], flash_data[16 * i + 6], flash_data[16 * i + 7],
                            flash_data[16 * i + 8], flash_data[16 * i + 9], flash_data[16 * i + 10],
                            flash_data[16 * i + 11], flash_data[16 * i + 12],
                            flash_data[16 * i + 13], flash_data[16 * i + 14],
                            flash_data[16 * i + 15]);
                    OutputDebugString(buf);

                    OutputDebugString("\n");
                    break;
                }
            }
        }
    }

    if (retry_cnt == 0) {
        ptest_inst->egfps_test_result.hw_result = 5;
        ptest_inst->egfps_test_result.sw_result = SW_BIN_FLASH_FAIL;
        return SW_BIN_FLASH_FAIL;
    }

#endif

    return SW_BIN_PASS;
}

DWORD
egfps_update_otp(HANDLE hTestInst, CSensor* pEgisSensor, EGFPS_OTP_DATA* pOtpDataPlan) {
    OutputDebugString("-egfps_update_otp-\n");

    EGFPS_TEST_INST* ptest_inst = (EGFPS_TEST_INST*)hTestInst;

    BOOL r_value;
    int i;
    char buf[MAX_PATH];

    unsigned char* otp_data = (unsigned char*)pOtpDataPlan;
    unsigned char otp_read[32];

    OutputDebugString("Write OTP Data=>\n");
    for (i = 0; i < 2; i++) {
        sprintf(buf,
                "%02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X\n",
                otp_data[16 * i], otp_data[16 * i + 1], otp_data[16 * i + 2], otp_data[16 * i + 3],
                otp_data[16 * i + 4], otp_data[16 * i + 5], otp_data[16 * i + 6],
                otp_data[16 * i + 7], otp_data[16 * i + 8], otp_data[16 * i + 9],
                otp_data[16 * i + 10], otp_data[16 * i + 11], otp_data[16 * i + 12],
                otp_data[16 * i + 13], otp_data[16 * i + 14], otp_data[16 * i + 15]);
        OutputDebugString(buf);
    }
    OutputDebugString("\n");

    r_value = pEgisSensor->bOTPWriteData(otp_data);
    if (!r_value) {
        OutputDebugString("bOTPWriteData Fail\n");
        ptest_inst->egfps_test_result.hw_result = 5;
        ptest_inst->egfps_test_result.sw_result = SW_BIN_OTP_FAIL;
        return SW_BIN_OTP_FAIL;
    }

    r_value = pEgisSensor->bOTPReadData(otp_read);
    if (!r_value) {
        OutputDebugString("bOTPWriteData Fail\n");
        ptest_inst->egfps_test_result.hw_result = 5;
        ptest_inst->egfps_test_result.sw_result = SW_BIN_OTP_FAIL;
        return SW_BIN_OTP_FAIL;
    }

    OutputDebugString("<=Read OTP Data\n");
    for (i = 0; i < 2; i++) {
        sprintf(buf,
                "%02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X\n",
                otp_read[16 * i], otp_read[16 * i + 1], otp_read[16 * i + 2], otp_read[16 * i + 3],
                otp_read[16 * i + 4], otp_read[16 * i + 5], otp_read[16 * i + 6],
                otp_read[16 * i + 7], otp_read[16 * i + 8], otp_read[16 * i + 9],
                otp_read[16 * i + 10], otp_read[16 * i + 11], otp_read[16 * i + 12],
                otp_read[16 * i + 13], otp_read[16 * i + 14], otp_read[16 * i + 15]);
        OutputDebugString(buf);
    }
    OutputDebugString("\n");

    if (memcmp(otp_data, otp_read, 32)) {
        OutputDebugString("OTP Write/Read Mismatch\n");
        ptest_inst->egfps_test_result.hw_result = 5;
        ptest_inst->egfps_test_result.sw_result = SW_BIN_OTP_FAIL;
        return SW_BIN_OTP_FAIL;
    }

    return SW_BIN_PASS;
}

DWORD
egfps_check_otp(HANDLE hTestInst, CSensor* pEgisSensor, EGFPS_OTP_DATA* pOtpDataPlan,
                unsigned char* pOtpDataValid) {
    OutputDebugString("-egfps_check_otp-\n");

    EGFPS_TEST_INST* ptest_inst = (EGFPS_TEST_INST*)hTestInst;

    BOOL r_value;
    int i;
    char buf[MAX_PATH];

    unsigned char otp_data[32];

    r_value = pEgisSensor->bOTPReadData(otp_data);
    if (!r_value) {
        OutputDebugString("bOTPReadData Fail\n");
        ptest_inst->egfps_test_result.hw_result = 5;
        ptest_inst->egfps_test_result.sw_result = SW_BIN_OTP_FAIL;
        return SW_BIN_OTP_FAIL;
    }

    OutputDebugString("<=Read OTP Data\n");
    for (i = 0; i < 2; i++) {
        sprintf(buf,
                "%02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X\n",
                otp_data[16 * i], otp_data[16 * i + 1], otp_data[16 * i + 2], otp_data[16 * i + 3],
                otp_data[16 * i + 4], otp_data[16 * i + 5], otp_data[16 * i + 6],
                otp_data[16 * i + 7], otp_data[16 * i + 8], otp_data[16 * i + 9],
                otp_data[16 * i + 10], otp_data[16 * i + 11], otp_data[16 * i + 12],
                otp_data[16 * i + 13], otp_data[16 * i + 14], otp_data[16 * i + 15]);
        OutputDebugString(buf);
    }
    OutputDebugString("\n");

    memcpy(pOtpDataPlan, otp_data, 32);

    unsigned char chksum = 0x00;
    for (i = 0; i <= 0x12; i++) {
        chksum += otp_data[i];
    }

    if (chksum != pOtpDataPlan->check_sum) {
        *pOtpDataValid = 0x00;
    } else {
        *pOtpDataValid = 0x01;
    }
    return SW_BIN_PASS;
}

#endif
