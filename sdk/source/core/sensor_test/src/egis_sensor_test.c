#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "ImageProcessingLib.h"
#include "egis_definition.h"
#include "egis_log.h"
#include "egis_sensor_test.h"
#include "egis_sensor_test_et5xx.h"
#include "ipsys.h"
#include "isensor_api.h"
#include "plat_heap.h"
#include "plat_spi.h"
#include "qm_lib.h"
#include "sensor_function.h"
#include "sensor_test_definition.h"
#include "type_definition.h"

#include "../../sensor_control/src/et5xx/calibration_state.h"
#include "egis_fp_get_image.h"

#define BUILD_N 1;
#define LOG_TAG "RBS-EGISFP-SENSOR_TEST"

extern int fp_tz_secure_sensor_init(void);
static int get_bad_pixel_count_threshold(const int frame_size);
static int ITPL_Fix_Bad_Pixel(const int sensor_width, const int sensor_height,
                              unsigned char* dirtydots_map);

SENSOR_REG sensor_reg_all[MAX_REG_ADDR + 1];
unsigned char g_picture_off[MAX_IMG_SIZE];
BYTE g_test_detect_gain;
BYTE g_test_detect_dc_offset;
BYTE g_test_detect_threshold;

CheckCaliState g_check_cali_state = SKIP_CALIBRATION_CHECK;

BufferIn buffer_in;
BufferOut buffer_out;

SENSOR_REG g_reg_original[MAX_REG_ADDR + 1];
int g_rw_test_state = BACKUP_REG;
int ReadWriteTest(void) {
    unsigned char write_value, i;
    int ret = 1, rw_fail_count = 0;

    // Register Test
    egislog_d("ReadWriteTest");

    switch (g_rw_test_state) {
        case BACKUP_REG:  // backup stage.
            egislog_d("ReadWriteTest, bk start");
            sensor_reg_init((SENSOR_REG*)&g_reg_original);
            for (i = 0; i <= MAX_REG_ADDR; i++) {
                if (g_reg_original[i].RegType & REG_TYPE_READ) {
                    if (ReadRegister(i, &g_reg_original[i].RegValue)) {
                        egislog_e(
                            "ReadRegister addr: 0x%x "
                            "fail",
                            i);
                        return EGIS_NO_DEVICE;
                    }
                }
            }
            if (rw_fail_count) {
                return EGIS_COMMAND_FAIL;
            }

            g_rw_test_state = READWRITE_TEST;     // to the test stage.
            return EGIS_INLINETOOL_SENSOR_RESET;  // for reset TEST;

        case READWRITE_TEST:
            // Default Register Value Test
            egislog_d("ReadWriteTest 1");
            sensor_reg_init((SENSOR_REG*)&sensor_reg_all);

            for (i = 0; i <= MAX_REG_ADDR; i++) {
                if (sensor_reg_all[i].RegType & REG_TYPE_READ) {
                    if (ReadRegister(i, &sensor_reg_all[i].RegValue)) {
                        egislog_e("ReadRegister fail");
                        goto fail;
                    }
                    if ((sensor_reg_all[i].RegValue & sensor_reg_all[i].RegValueMask) !=
                        sensor_reg_all[i].RegValueDefault) {
                        egislog_e(
                            "addr[0x%x] value = 0x%x, "
                            "default should be = 0x%x",
                            i, sensor_reg_all[i].RegValue & sensor_reg_all[i].RegValueMask,
                            sensor_reg_all[i].RegValueDefault);
                        goto fail;
                    }
                }
            }

            egislog_d("ReadWriteTest 2");
            // need to exit POACF mode after reset
            fp_tz_secure_sensor_init();
            // 0x00 Register Value Test
            write_value = 0x00;
            for (i = 0; i <= MAX_REG_ADDR; i++) {
                if (sensor_reg_all[i].RegType & REG_TYPE_WRITE) {
                    if (WriteRegister(i, write_value & sensor_reg_all[i].RegValueMask)) goto fail;
                    if (ReadRegister(i, &sensor_reg_all[i].RegValue)) goto fail;
                    if (sensor_reg_all[i].RegValue !=
                        (write_value & sensor_reg_all[i].RegValueMask)) {
                        egislog_e(
                            "addr[0x%x] write value = "
                            "0x%x, read back value = "
                            "0x%x",
                            i, write_value & sensor_reg_all[i].RegValueMask,
                            sensor_reg_all[i].RegValue & sensor_reg_all[i].RegValueMask);
                        goto fail;
                    }
                }
            }

            egislog_d("ReadWriteTest 3");
            // 0xFF Register Value Test
            write_value = 0xFF;
            for (i = 0; i <= MAX_REG_ADDR; i++) {
                if (sensor_reg_all[i].RegType & REG_TYPE_WRITE) {
                    if (WriteRegister(i, write_value & sensor_reg_all[i].RegValueMask)) goto fail;
                    if (ReadRegister(i, &sensor_reg_all[i].RegValue)) goto fail;
                    if (sensor_reg_all[i].RegValue !=
                        (write_value & sensor_reg_all[i].RegValueMask)) {
                        egislog_e(
                            "addr[0x%x] write value = "
                            "0x%x, read back value = "
                            "0x%x",
                            i, write_value & sensor_reg_all[i].RegValueMask,
                            sensor_reg_all[i].RegValue & sensor_reg_all[i].RegValueMask);
                        goto fail;
                    }
                }
            }

            egislog_d("ReadWriteTest 4");
            // 0x5A Register Value Test
            write_value = 0x5A;
            for (i = 0; i <= MAX_REG_ADDR; i++) {
                if (sensor_reg_all[i].RegType & REG_TYPE_WRITE) {
                    if (WriteRegister(i, write_value & sensor_reg_all[i].RegValueMask)) goto fail;
                    if (ReadRegister(i, &sensor_reg_all[i].RegValue)) goto fail;

                    if (sensor_reg_all[i].RegValue !=
                        (write_value & sensor_reg_all[i].RegValueMask)) {
                        egislog_e(
                            "addr[0x%x] write value = "
                            "0x%x, read back value = "
                            "0x%x",
                            i, write_value & sensor_reg_all[i].RegValueMask,
                            sensor_reg_all[i].RegValue & sensor_reg_all[i].RegValueMask);
                        goto fail;
                    }
                }
            }

            egislog_d("ReadWriteTest 5");
            // 0xA5 Register Value Test
            write_value = 0xA5;
            for (i = 0; i <= MAX_REG_ADDR; i++) {
                if (sensor_reg_all[i].RegType & REG_TYPE_WRITE) {
                    if (WriteRegister(i, write_value & sensor_reg_all[i].RegValueMask)) goto fail;
                    if (ReadRegister(i, &sensor_reg_all[i].RegValue)) goto fail;
                    if (sensor_reg_all[i].RegValue !=
                        (write_value & sensor_reg_all[i].RegValueMask)) {
                        egislog_e(
                            "addr[0x%x] write value = "
                            "0x%x, read back value = "
                            "0x%x",
                            i, write_value & sensor_reg_all[i].RegValueMask,
                            sensor_reg_all[i].RegValue & sensor_reg_all[i].RegValueMask);
                        goto fail;
                    }
                }
            }

            egislog_d("ReadWriteTest 6");
            // Reset Register Value Test
            for (i = 0; i <= MAX_REG_ADDR; i++) {
                if (sensor_reg_all[i].RegType & REG_TYPE_WRITE) {
                    if (WriteRegister(i, sensor_reg_all[i].RegValueDefault)) goto fail;
                    if (ReadRegister(i, &sensor_reg_all[i].RegValue)) goto fail;
                    if (sensor_reg_all[i].RegValue != sensor_reg_all[i].RegValueDefault) {
                        egislog_e(
                            "addr[0x%x] write value = "
                            "0x%x, read back value = "
                            "0x%x",
                            i, sensor_reg_all[i].RegValueDefault, sensor_reg_all[i].RegValueMask);
                        goto fail;
                    }
                }
            }

            egislog_d("ReadWriteEnd");
            goto pass;
    }  // for switch case.

fail:
    ret = EGIS_NO_DEVICE;
    rw_fail_count++;
pass:
    ret = EGIS_OK;
    /*
     * Restore stage.
     */
    for (i = 0; i <= MAX_REG_ADDR; i++) {
        if (sensor_reg_all[i].RegType & REG_TYPE_WRITE) {
            if (WriteRegister(i, g_reg_original[i].RegValue)) rw_fail_count++;
            // egislog_d("ReadWriteTest show,  addr[0x%x] value =
            // %d", i, g_reg_original[i].RegValue);
        }
    }
    g_rw_test_state = BACKUP_REG;
    if (rw_fail_count) {
        egislog_e("ReadWriteTest restore Fail, count = %d", rw_fail_count);
        return EGIS_NO_DEVICE;
    }
    return ret;
}

int DarkdotsTest(const int sensor_width, const int sensor_height, unsigned char* dirtydots_map) {
    unsigned char picture_black_pixel[MAX_IMG_SIZE];
    int DarkDotsTemp = 0;
    BOOL TooManyDirtyDotsTemp = TRUE;
    int MaxConsecutive_dots = 0;
    Image_Valid_Area image_valid_area;

    image_valid_area.row_start = 0;
    image_valid_area.row_end = sensor_height - 1;
    image_valid_area.col_start = 0;
    image_valid_area.col_end = sensor_width - 1;

    egislog_d("DarkdotsTest start");
    if (set_white_reg() != EGIS_OK) return EGIS_COMMAND_FAIL;
    // Get One Frame
    if (GetFrame(picture_black_pixel) != EGIS_OK) return EGIS_COMMAND_FAIL;

    // We only test dark dots here
    vEvaluateImage(dirtydots_map, NULL, &DarkDotsTemp, &TooManyDirtyDotsTemp, &MaxConsecutive_dots,
                   &image_valid_area, picture_black_pixel, TEST_DARK_DOT | TEST_CONSECUTIVE_DOT,
                   WHITE_DOTS_TH, DARK_DOTS_TH);

    egislog_i("DarkdotsTest, total = %d, max = %d", DarkDotsTemp, MaxConsecutive_dots);

    if (isensor_set_sensor_mode() != EGIS_OK) {
        return EGIS_COMMAND_FAIL;
    }

    return EGIS_OK;
}

int WhitedotsTest(const int sensor_width, const int sensor_height, unsigned char* dirtydots_map) {
    unsigned char picture_white_pixel[MAX_IMG_SIZE];
    int WhiteDotsTemp = 0;
    BOOL TooManyDirtyDotsTemp = TRUE;
    int MaxConsecutive_dots = 0;
    Image_Valid_Area image_valid_area;

    image_valid_area.row_start = 0;
    image_valid_area.row_end = sensor_height - 1;
    image_valid_area.col_start = 0;
    image_valid_area.col_end = sensor_width - 1;

    egislog_d("WhitedotsTest start");
    if (set_black_reg() != EGIS_OK) return EGIS_COMMAND_FAIL;
    // Get One Frame
    if (GetFrame(picture_white_pixel) != EGIS_OK) return EGIS_COMMAND_FAIL;

    vEvaluateImage(dirtydots_map, &WhiteDotsTemp, NULL, &TooManyDirtyDotsTemp, &MaxConsecutive_dots,
                   &image_valid_area, picture_white_pixel, TEST_WHITE_DOT | TEST_CONSECUTIVE_DOT,
                   WHITE_DOTS_TH, DARK_DOTS_TH);

    egislog_i("WhiteDotsTest, total = %d, max = %d", WhiteDotsTemp, MaxConsecutive_dots);

    if (isensor_set_sensor_mode() != EGIS_OK) {
        return EGIS_COMMAND_FAIL;
    }

    return EGIS_OK;
}

int AbnormalTest_Use_Avg_Frame(float* avgStdDev, const int sensor_width, const int sensor_height,
                               unsigned char* dirtydots_map) {
    int DarkDotsTemp = 0;
    int WhiteDotsTemp = 0;
    BOOL TooManyDirtyDotsTemp = TRUE;
    int MaxConsecutive_dots = 0, bad_pixel_count = 0;
    int ret;
    Image_Valid_Area image_valid_area;

    image_valid_area.row_start = 0;
    image_valid_area.row_end = sensor_height - 1;
    image_valid_area.col_start = 0;
    image_valid_area.col_end = sensor_width - 1;

    if (isensor_calibrate(FPS_CALI_INIT) != EGIS_OK) return FAIL;

    ret = isensor_set_sensor_mode();
    if (ret != EGIS_OK) {
        return FAIL;
    }

    unsigned char** ppAllFrames = alloc_multi_sensor_frames(ImageAverageCount);
    if (ppAllFrames == NULL) {
        egislog_e("ppAllFrames NULL. Out of memory");
        return FAIL;
    }
    /****** get 10 frames and count dirty dots *****/
    ret = get_multi_sensor_frames(ppAllFrames, ImageAverageCount, (UINT)sensor_height,
                                  (UINT)sensor_width);
    if (ret == EGIS_OK) {
        ret = calc_multi_frames_avg(ppAllFrames, ImageAverageCount, g_picture_off);
        ret =
            calc_multi_frames_stddev_avg(ppAllFrames, ImageAverageCount, g_picture_off, avgStdDev);
        ret = adjust_multi_frames_avg(ppAllFrames, ImageAverageCount, g_picture_off,
                                      CALIBRATED_WHITE_TH, CALIBRATED_DARK_TH);
    }
    free_multi_sensor_frames(ppAllFrames, ImageAverageCount);
    if (ret != EGIS_OK) {
        return FAIL;
    }

    vEvaluateImage(dirtydots_map, &WhiteDotsTemp, &DarkDotsTemp, &TooManyDirtyDotsTemp,
                   &MaxConsecutive_dots, &image_valid_area, g_picture_off,
                   TEST_WHITE_DOT | TEST_DARK_DOT | TEST_CONSECUTIVE_DOT, CALIBRATED_WHITE_TH,
                   CALIBRATED_DARK_TH);

    egislog_i("AbnormalTest_Use_Avg_Frame, total = %d, max = %d, std = %d",
              WhiteDotsTemp + DarkDotsTemp, MaxConsecutive_dots, (int)(*avgStdDev));

    return EGIS_OK;
}

int AbnormalTest_Use_Fixed_Frame(const int sensor_width, const int sensor_height) {
    int DarkDotsTemp = 0;
    int WhiteDotsTemp = 0;
    BOOL TooManyDirtyDotsTemp = TRUE;
    int MaxConsecutive_dots = 0;
    Image_Valid_Area image_valid_area;

    image_valid_area.row_start = 0;
    image_valid_area.row_end = sensor_height - 1;
    image_valid_area.col_start = 0;
    image_valid_area.col_end = sensor_width - 1;

    vEvaluateImage(NULL, &WhiteDotsTemp, &DarkDotsTemp, &TooManyDirtyDotsTemp, &MaxConsecutive_dots,
                   &image_valid_area, g_picture_off,
                   TEST_WHITE_DOT | TEST_DARK_DOT | TEST_CONSECUTIVE_DOT, CALIBRATED_WHITE_TH,
                   CALIBRATED_DARK_TH);

    egislog_i("AbnormalTest, total = %d, max = %d", WhiteDotsTemp + DarkDotsTemp,
              MaxConsecutive_dots);

    return TooManyDirtyDotsTemp ? FAIL : PASS;
}

extern BOOL calibration_finished(void);
extern int fp_calibration_interrupt_step1(void);
extern int fp_calibration_interrupt_step2(BOOL interrupt);
extern int fp_calibration_interrupt_step3(BOOL interrupt);
extern int fp_calibration_interrupt_step4(BOOL interrupt, BYTE* gain, BYTE* dc_offset,
                                          BYTE* threshold);

int interrupt_calibration(int* state, BOOL interrupt) {
    return isensor_calibrate(FPS_CALI_SDK_THL);
}

int egisFpSnsrTest(const egisConstBlobData_t* pIn, egisBlobData_t* pOut) {
    int ret = EGIS_COMMAND_FAIL;
    BYTE TestFailItem = 0;
    float avgStdDev = -1;
    const BYTE DIRTYDOTS_TEST_RESULT_VER = 0x12;

    if (pIn == NULL || pOut == NULL) return EGIS_INCORRECT_PARAMETER;

    IPCopyMem(&buffer_in, pIn->pData, pIn->length);

    egislog_d("==== Sensor Test Version 5.8 ====");  // Version (X.Y)
                                                     // X:Normal Word
                                                     // version, Y:Secure
                                                     // Word version
    egislog_d("scriptId == %d, options == %d  touch = %d, TimeOut = %d", buffer_in.scriptId,
              buffer_in.options, buffer_in.touch, buffer_in.options);

    switch (buffer_in.scriptId) {
        case EGIS_FP_DIRTYDOTS_TEST_SCRIPT_ID: {
            int sensor_width, sensor_height;
            buffer_out.result = EGIS_OK;
            egislog_d("tz_secure_set_sensor_mode");
            isensor_set_sensor_mode();
            isensor_get_sensor_roi_size(&sensor_width, &sensor_height);
            unsigned char* dirtydots_map = NULL;
            dirtydots_map = plat_alloc(sensor_width * sensor_height);
            memset(dirtydots_map, 0x00, sensor_width * sensor_height);

            if (DarkdotsTest(sensor_width, sensor_height, dirtydots_map) != EGIS_OK) {
                egislog_e("DarkdotsTest EGIS_COMMAND_FAIL");
                TestFailItem |= EGIS_TEST_FAIL_DARKDOT;
            } else
                egislog_d("DarkdotsTest DONE");

            if (WhitedotsTest(sensor_width, sensor_height, dirtydots_map) != EGIS_OK) {
                egislog_e("WhitedotsTest EGIS_COMMAND_FAIL");
                TestFailItem |= EGIS_TEST_FAIL_WHITEDOT;
            } else
                egislog_d("WhitedotsTest DONE");

            if (AbnormalTest_Use_Avg_Frame(&avgStdDev, sensor_width, sensor_height,
                                           dirtydots_map) != EGIS_OK) {
                egislog_e("AbnormalTest_Use_Avg_Frame EGIS_COMMAND_FAIL");
                TestFailItem |= EGIS_TEST_FAIL_ABNORMAL_AVG_FRAME;
            } else
                egislog_d("AbnormalTest_Use_Avg_Frame DONE");

            if (ITPL_Fix_Bad_Pixel(sensor_width, sensor_height, dirtydots_map) != EGIS_OK) {
                egislog_e("ITPL_Fix_Bad_Pixel FAIL");
                buffer_out.result = EGIS_TEST_FAIL;
                TestFailItem |= EGIS_TEST_FAIL_MANY_BAD_DOT;
            } else {
                egislog_d("ITPL_Fix_Bad_Pixel DONE");
                if (AbnormalTest_Use_Fixed_Frame(sensor_width, sensor_height) != PASS) {
                    egislog_e("AbnormalTest FAIL");
                    buffer_out.result = EGIS_TEST_FAIL;
                    TestFailItem |= EGIS_TEST_FAIL_ABNORMAL_FIXED_FRAME;
                } else
                    egislog_d("AbnormalTest PASS");
            }

            if (dirtydots_map != NULL) plat_free(dirtydots_map);

            if (avgStdDev > 65535) {
                egislog_e("image average stddev is big. %d", (int)avgStdDev);
            }
            int bad_pixel_count = 0;
            isensor_get_bad_pixels(NULL, &bad_pixel_count);
            buffer_out.picture_put[0] = DIRTYDOTS_TEST_RESULT_VER;
            buffer_out.picture_put[1] = TestFailItem;
            memcpy(buffer_out.picture_put + 2, &avgStdDev, sizeof(avgStdDev));
            memcpy(buffer_out.picture_put + INDEX_BAD_PIXEL, &bad_pixel_count,
                   sizeof(bad_pixel_count));
            if (buffer_out.result != EGIS_OK) {
                egislog_e("TestFailItem 0x%X", TestFailItem);
                egislog_e("buffer_out.result 0x%X", buffer_out.result);
                goto TESTFAIL;
            }

            buffer_out.result = EGIS_OK;
            IPCopyMem(pOut->pData, &buffer_out, sizeof(buffer_out));
            pOut->length = sizeof(buffer_out);
            egislog_d("return EVT_EGIS_NORMALSCAN_END %d 0x%x", sizeof(buffer_out),
                      sizeof(pOut->pData));
            return EGIS_OK;
        }
        case EGIS_FP_REGISTER_RW_TEST_SCRIPT_ID:
            egislog_d("case RW_TEST:");
            buffer_out.result = ReadWriteTest();
            IPCopyMem(pOut->pData, &buffer_out, sizeof(buffer_out));
            pOut->length = sizeof(buffer_out);
            return EGIS_OK;

        case EGIS_FP_FINGER_ON_TEST_SCRIPT_ID:
            buffer_out.result = EGIS_TEST_FAIL;
            egislog_e("FingerOnTest not supported");
            return EGIS_COMMAND_FAIL;
        case EGIS_FP_READ_REV_TEST_SCRIPT_ID:
            buffer_out.result = isensor_read_rev_id(buffer_out.picture_put, SENSOR_REV_ID_LENGTH);
            IPCopyMem(pOut->pData, &buffer_out, sizeof(buffer_out));
            pOut->length = sizeof(buffer_out);
            return EGIS_OK;
        default:
            egislog_d("return FP_RESULT_GEN_BAD_PARAM, scriptId = %d", buffer_in.scriptId);
            return EGIS_INCORRECT_PARAMETER;
    }

TESTFAIL:
    egislog_e("%s TESTFAIL", __func__);

    IPCopyMem(pOut->pData, &buffer_out, sizeof(buffer_out));
    pOut->length = sizeof(buffer_out);
    g_check_cali_state = SKIP_CALIBRATION_CHECK;
    return EGIS_OK;
}

static int get_bad_pixel_count_threshold(const int frame_size) {
    int value;
    float a_hundredth_area;

    a_hundredth_area = ((float)frame_size / 100);
    egislog_d("%s, a_hundredth_area:%.2f", __func__, a_hundredth_area);
    value = (int)(a_hundredth_area + 0.5);  // ROUND
    // value = ((((int)(a_hundredth_area * 10))) - (((int)a_hundredth_area) * 10)) > 0 ?
    // 	(((int)a_hundredth_area) + 1) : ((int)a_hundredth_area); // ROUNDUP

    egislog_d("%s, value:%d", __func__, value);
    return value;
}

static int ITPL_Fix_Bad_Pixel(const int sensor_width, const int sensor_height,
                              unsigned char* dirtydots_map) {
    int i, frame_size = sensor_width * sensor_height;
    int total_bad_dot_cnt = 0, threshold_cnt;

    threshold_cnt = get_bad_pixel_count_threshold(frame_size);
    for (i = 0; i < frame_size; i++) {
        if (dirtydots_map[i]) ++total_bad_dot_cnt;
    }
    egislog_i("%s, total_bad_dot_cnt=%d, threshold_cnt=%d", __func__, total_bad_dot_cnt,
              threshold_cnt);
    if (total_bad_dot_cnt <= threshold_cnt) {
        IPbadpointfix(g_picture_off, sensor_width, sensor_height, dirtydots_map, 2);
        return EGIS_OK;
    } else
        return FAIL;
}
