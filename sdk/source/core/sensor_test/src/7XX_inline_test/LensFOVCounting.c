#include "LensFOVCounting.h"
#include <stdlib.h>
#include <string.h>

#include "LensFOVCounting.h"
#include "plat_mem.h"

#define MTF_BLOCK_SIZE 40

#define FALSE 0
#define TRUE 1

#ifndef NULL
#define NULL ((void*)0)
#endif

#define LENS_STRIPE_COUNTING_ARRAY_SIZE 512

unsigned char LensFOVCounting(unsigned char* a_Image, unsigned int a_Width, unsigned int a_Height,
                              unsigned int a_centroid_x, unsigned int a_centroid_y,
                              unsigned int* a_FOV_X1, unsigned int* a_FOV_X2,
                              unsigned int* a_FOV_Y1, unsigned int* a_FOV_Y2) {
    unsigned int i;

    unsigned char FOV_result = TRUE;
    unsigned char data_array_x[LENS_STRIPE_COUNTING_ARRAY_SIZE] = {0};
    unsigned char data_array_y[LENS_STRIPE_COUNTING_ARRAY_SIZE] = {0};
    short verify_array[LENS_STRIPE_COUNTING_ARRAY_SIZE] = {0};
    unsigned short start, end, max;
    //	unsigned short FOV_X,FOV_Y;
    unsigned short FOV_X1, FOV_Y1;
    unsigned short FOV_X2, FOV_Y2;
    short value;

    // copy data to data array
    memcpy(data_array_x, (a_Image + (a_centroid_y * a_Width)), a_Width);

    for (i = 0; i < a_Height; i++) {
        data_array_y[i] = *(a_Image + (i * a_Width) + a_centroid_x);
    }

    // check with verify array for horizontal
    for (i = 1; i < a_Width; i++) {
        value = data_array_x[i];
        value -= data_array_x[i - 1];

        if (value < 0) value = -value;

        verify_array[i] = value;
    }

    max = 0;
    start = 0;
    end = (a_Width - 1);

    max = 0;
    for (i = 0; i < (a_Width / 2); i++) {
        if (max < verify_array[i]) {
            max = verify_array[i];
            start = i;
        }
    }
    max = 0;
    for (i = (a_Width - 1); i > (a_Width / 2); i--) {
        if (max < verify_array[i]) {
            max = verify_array[i];
            end = i;
        }
    }

    FOV_X1 = (a_centroid_x > start) ? (a_centroid_x - start) : 0;
    FOV_X2 = (end > a_centroid_x) ? (end - a_centroid_x) : 0;

    // check with verify array for horizontal
    for (i = 1; i < a_Height; i++) {
        value = data_array_y[i];
        value -= data_array_y[i - 1];

        if (value < 0) value = -value;

        verify_array[i] = value;
    }

    max = 0;
    start = 0;
    end = (a_Height - 1);

    for (i = 0; i < (a_Height / 2); i++) {
        if (max < verify_array[i]) {
            max = verify_array[i];
            start = i;
        }
    }
    max = 0;
    for (i = (a_Height - 1); i > (a_Height / 2); i--) {
        if (max < verify_array[i]) {
            max = verify_array[i];
            end = i;
        }
    }

    FOV_Y1 = (a_centroid_y > start) ? (a_centroid_y - start) : 0;
    FOV_Y2 = (end > a_centroid_y) ? (end - a_centroid_y) : 0;

    *a_FOV_X1 = FOV_X1;
    *a_FOV_X2 = FOV_X2;
    *a_FOV_Y1 = FOV_Y1;
    *a_FOV_Y2 = FOV_Y2;

    return FOV_result;
}
