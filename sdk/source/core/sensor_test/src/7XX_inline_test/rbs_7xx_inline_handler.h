
#pragma once

#include "common_definition.h"
#include "egisfp_test_et7xx.h"

void et7xx_inline_init(void);
void et7xx_inline_uninit(void);
UINT32 et7xx_inline_test_handler(UINT32 script_id, UINT8* in_data, UINT32 in_data_len,
                                 UINT8* out_data, UINT32* out_data_len);