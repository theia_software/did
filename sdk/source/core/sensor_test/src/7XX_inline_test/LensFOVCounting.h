#ifndef __LENSFOVCOUNTING_H__
#define __LENSFOVCOUNTING_H__

#ifdef __cplusplus
extern "C" {
#endif

unsigned char LensFOVCounting(unsigned char* a_Image, unsigned int a_Width, unsigned int a_Height,
                              unsigned int a_centroid_x, unsigned int a_centroid_y,
                              unsigned int* a_FOV_X1, unsigned int* a_FOV_X2,
                              unsigned int* a_FOV_Y1, unsigned int* a_FOV_Y2);

#ifdef __cplusplus
};
#endif

#endif
