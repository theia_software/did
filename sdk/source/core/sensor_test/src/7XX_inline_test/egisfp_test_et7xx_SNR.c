#include <stdint.h>
#include <stdio.h>

#include "7xx_sensor_test_definition.h"
#include "InlineLib.h"
#include "LensFOVCounting.h"
#include "LensMFactorCounting.h"
#include "common_definition.h"
#include "core_config.h"
#include "egis_definition.h"
#include "egis_sprintf.h"
#include "egisfp_test_et7xx_SNR.h"
#ifdef LARGE_AREA
#include "et760_sensor_control.h"
#else
#include "et7xa_sensor_control.h"
#endif
#include "fp_definition.h"
#include "fpsensor_7xa_definition.h"
#include "image_cut.h"
#include "inline_obj_et7xx.h"
#include "isensor_api.h"
#include "isensor_definition.h"
#include "plat_file.h"
#include "plat_log.h"
#include "plat_mem.h"
#include "plat_spi.h"
#include "sensor_control_common.h"
#include "work_with_G2/ImageProcessingLib.h"

#define LOG_TAG "RBS-7XX_inline_SNR"
#define X1000(a) ((int)((a)*1000))

typedef enum snr_state {
    SNR_STATE_INIT = 0,
    SNR_STATE_WKBOX_ON,
    SNR_STATE_BKBOX_ON,
    SNR_STATE_CHART_ON,
    SNR_STATE_GET_LOG,
    SNR_STATE_UNKNOWN = 0x7FFFFFFF
} snr_state_t;

static final_MT_cali_data_t g_final_MT_cali_data;
static snr_state_t g_snr_state = SNR_STATE_INIT;
static inline_picture_buf_t* g_inline_img;

//=== 713 setting ===
#define MAX_CAPTURING_TIME_MS 150
float g_expo_start_time_wk;
float g_expo_end_time_wk;
float g_expo_step_wk;
int g_expo_img_cnt_wk;
int g_ang_try_start;
int g_ang_try_end;
static int g_inline_save_log_enable_extra_info = FALSE;

static const snr_config_t snr_config = {
    // WKbox test
    .hw_int_cnt = 1,
    .hw_int_shift = 3,
    .target_percentage = 55,  // default value of KEY_CALIBRATE_625AVG_TARGET_PERCENTAGE
                              // MAG_TEST
    .mag_hw_int_shift = 6,
    .mag_centroid_x = 109,
    .mag_centroid_y = 73,
    .mag_roi_w = 120,
    .mag_roi_h = 120,
    .mag_scan_window = 8,
    .mag_scan_direct = 0,
    .mag_roi_chart_pich = 200,
    .mag_roi_sensor_pitch = 5,
    // FEA_TEST
    .fea_hw_int_shift = 3,
    // module setting
    .pc_cell_width = 6,
};
//--------------------------

static int egfps_snr_test_init(void) {
    uint16_t** raw_image16 = NULL;
    int i = 0, image_num = sizeof(inline_picture_buf_t) / sizeof(void*);
    int frame_size = SENSOR_WIDTH * SENSOR_HEIGHT;
    egislog_d("egfps_snr_test_init image_num=%d", image_num);

    int sensor_id = get_sensor_id();
    g_expo_start_time_wk = core_config_get_int(
        INI_SECTION_CALIBRATE, KEY_CALIBRATE_EXPOSURE_START_WK, (float)DBG_EXPO_START_WK);
    g_expo_end_time_wk = core_config_get_int(INI_SECTION_CALIBRATE, KEY_CALIBRATE_EXPOSURE_END_WK,
                                             (float)DBG_EXPO_END_WK);
    g_expo_step_wk = core_config_get_int(INI_SECTION_CALIBRATE, KEY_CALIBRATE_EXPOSURE_STEP_WK,
                                         (float)DBG_EXPO_STEP_WK);
    g_expo_img_cnt_wk =
        ((int)(g_expo_end_time_wk - g_expo_start_time_wk) / (int)g_expo_step_wk + 1);

    g_ang_try_start = core_config_get_int(INI_SECTION_CALIBRATE, KEY_CALIBRATE_ANG_TRY_START,
                                          (int)DBG_ANG_TRY_START);
    g_ang_try_end =
        core_config_get_int(INI_SECTION_CALIBRATE, KEY_CALIBRATE_ANG_TRY_END, (int)DBG_ANG_TRY_END);

    if (raw_image16 == NULL) {
        raw_image16 = plat_alloc(sizeof(inline_picture_buf_t));
        if (raw_image16 == NULL) egislog_e("raw_image16 alloc error");
    } else
        egislog_e("raw_image16 not NULL, Do you alloc beforce");

    g_inline_img = (inline_picture_buf_t*)raw_image16;
    g_inline_img->picture_temp8 = (uint8_t*)plat_alloc(frame_size);
    g_inline_img->picture_snr_dbg8 = (uint8_t*)plat_alloc(frame_size);
    g_inline_img->picture_snr_bk16 = (uint16_t*)plat_alloc(frame_size * sizeof(uint16_t));
    g_inline_img->picture_snr_ct16 =
        (uint16_t*)plat_alloc(frame_size * sizeof(uint16_t) * DBG_IMG_CNT_SNR);
    g_inline_img->picture_mf_bk16 = (uint16_t*)plat_alloc(frame_size * sizeof(uint16_t));
    g_inline_img->picture_mf_ct16 = (uint16_t*)plat_alloc(frame_size * sizeof(uint16_t));
    g_inline_img->picture_fea_bk16 = (uint16_t*)plat_alloc(frame_size * sizeof(uint16_t));
    g_inline_img->picture_sp_bk16 = (uint16_t*)plat_alloc(frame_size * sizeof(uint16_t));
    g_inline_img->picture_moire_bkg16 = (uint16_t*)plat_alloc(frame_size * sizeof(uint16_t));
    g_inline_img->picture_wk_bk16 =
        (uint16_t*)plat_alloc(frame_size * sizeof(uint16_t) * g_expo_img_cnt_wk);
    g_inline_img->picture_bp_bk16 =
        (uint16_t*)plat_alloc(frame_size * sizeof(uint16_t) * DBG_IMG_CNT_BP);
    g_inline_img->picture_wk_bk16_step2 =
        (uint16_t*)plat_alloc(frame_size * sizeof(uint16_t) * DBG_IMG_CNT_STEP2_WK);
    g_inline_img->picture_intensity16 = (uint16_t*)plat_alloc(frame_size * sizeof(uint16_t));

    egislog_d("%p, %p", raw_image16[0], g_inline_img->picture_temp8);
    egislog_d("%p, %p", raw_image16[1], g_inline_img->picture_snr_bk16);

    int inline_obj_create_8bit_size = 2;  // save picture_snr_dbg8 & picture_temp8
    int inline_obj_create_16bit_size = image_num - inline_obj_create_8bit_size + DBG_IMG_CNT_SNR -
                                       1 + g_expo_img_cnt_wk - 1 + DBG_IMG_CNT_BP - 1 +
                                       DBG_IMG_CNT_STEP2_WK - 1;
    inline_obj_create(inline_obj_create_8bit_size, inline_obj_create_16bit_size);
    egislog_d("inline_obj_create number 8bit=%d, 16bit=%d", inline_obj_create_8bit_size,
              inline_obj_create_16bit_size);

    for (i = 0; i < image_num; i++) {
        if (raw_image16[i] == NULL) {
            egislog_e("FP_LIB_ERROR_MEMORY, i=%d", i);
            return FP_LIB_ERROR_MEMORY;
        }
    }
    return FP_LIB_OK;
}

static void __EnableGain2(BOOL bEnable) {
    BOOL r_value;
    uint16_t addr;
    unsigned char value;

    egislog_d("bEnableGain2: %d", bEnable);
    if (bEnable) {
        // Enable DAC (DC Offset )
        addr = ET7XA_ADDR_PGA_CSR;
        if (get_sensor_id() == SENSOR_ID_ET716)
            value = 0x06;
        else
            value = 0x02;
        egislog_d("bEnableGain2 ET7XA_ADDR_PGA_CSR : %d", value);
        r_value = et7xx_write_register(addr, value);
    } else {
        // Disable DAC (DC Offset )
        addr = ET7XA_ADDR_PGA_CSR;
        if (get_sensor_id() == SENSOR_ID_ET716)
            value = 0x04;
        else
            value = 0x00;
        egislog_d("bEnableGain2 ET7XA_ADDR_PGA_CSR : %d", value);
        r_value = et7xx_write_register(addr, value);
    }
}

static void __GetFrame16NoRotate(uint16_t* pFrame) {
    int ret = 0;
    egislog_v("WxH=%d*%d", SENSOR_WIDTH, SENSOR_HEIGHT);
#ifdef LARGE_AREA
    ret = et760_fetch_raw_data_without_setting(pFrame, SENSOR_FULL_WIDTH, SENSOR_FULL_HEIGHT, 0, 0);
#else
    ret = et7xa_fetch_raw_data_without_setting(pFrame, SENSOR_FULL_WIDTH, SENSOR_FULL_HEIGHT, 0, 0);
#endif
}

static void __GetFrame16(uint16_t* pFrame) {
    int ret = 0;
    int i, j, x0, y0;
    unsigned short pixel;
    unsigned short* pFrame_temp;
    egislog_v("WxH=%d*%d", SENSOR_WIDTH, SENSOR_HEIGHT);
    pFrame_temp =
        (unsigned short*)plat_alloc(SENSOR_WIDTH * SENSOR_HEIGHT * sizeof(unsigned short));
    isensor_get_int(PARAM_INT_ROI_X, &x0);
    isensor_get_int(PARAM_INT_ROI_Y, &y0);
#ifdef LARGE_AREA
    ret = et760_fetch_raw_data_without_setting(pFrame_temp, SENSOR_FULL_WIDTH, SENSOR_FULL_HEIGHT, 0, 0);
#else
    ret = et7xa_fetch_raw_data_without_setting(pFrame_temp, SENSOR_FULL_WIDTH, SENSOR_FULL_HEIGHT, 0, 0);
#endif

    for (j = 0; j < SENSOR_HEIGHT; j++) {
        for (i = 0; i < SENSOR_WIDTH; i++) {
            pixel = pFrame_temp[j * SENSOR_WIDTH + i];
            pFrame[SENSOR_HEIGHT * (SENSOR_HEIGHT - i - 1) + j] = pixel;
        }
    }
    plat_free(pFrame_temp);
    egislog_d("ret %d", ret);
}

static UINT32 egfps_get_wkbox_image_test(void) {
#ifdef LARGE_AREA
    return FP_LIB_OK;
#else
    float expo_start_time = 0;
    float expo_tmp_time = 0;
    float expo_ret = 0, expo_ret2 = 0;
    int i = 0, centroid_x = 0, centroid_y = 0;
    int frame_size = SENSOR_WIDTH * SENSOR_HEIGHT;
    int real_raw_bpp = 0;
    uint16_t* raw_image16 = NULL;
    int target_percentage =
        core_config_get_int(INI_SECTION_CALIBRATE, KEY_CALIBRATE_625AVG_TARGET_PERCENTAGE,
                            snr_config.target_percentage);
    int target_capture_time = core_config_get_int(
        INI_SECTION_CALIBRATE, KEY_CALIBRATE_TARGET_CAPTURE_TIME, MAX_CAPTURING_TIME_MS);
    int gain2_enable = core_config_get_int(INI_SECTION_SENSOR, KEY_TWO_TIMES_GAIN, DBG_GAIN_WK);
    char name[INLINE_IMAGE_NAME_LENGH] = {0};

    egislog_d("====egfps_get_wkbox_image_test START====");
    int original_exp_x10;
    isensor_get_int(PARAM_INT_EXPOSURE_TIME_X10, &original_exp_x10);

    if (get_sensor_id() != SENSOR_ID_ET701) {
        __EnableGain2(gain2_enable);
    }
    raw_image16 = g_inline_img->picture_wk_bk16;
    isensor_get_int(PARAM_INT_GET_REAL_RAW_BPP, &real_raw_bpp);

    if (DBG_HW_INT_WK > 1) {
        et7xx_write_register(ET7XA_ADDR_SCAN_FRAMES, DBG_HW_INT_WK - 1);
        et7xx_write_register(ET7XA_ADDR_HDR_CSR, 0x01);
    } else {
        et7xx_write_register(ET7XA_ADDR_SCAN_FRAMES, DBG_HW_INT_WK - 1);
        et7xx_write_register(ET7XA_ADDR_HDR_CSR, 0x00);
    }
    /* Step 1. */
    for (i = 0, expo_start_time = g_expo_start_time_wk; expo_start_time <= g_expo_end_time_wk;
         i++, expo_start_time += g_expo_step_wk) {
        egislog_d("test %d, addr=0x%x, expo X1000=%d", i, raw_image16 + (i * frame_size),
                  X1000(expo_tmp_time));
        expo_tmp_time = expo_start_time;
        isensor_set_int(PARAM_INT_EXPOSURE_TIME_X10, (int)(expo_tmp_time * 10));
        et7xa_set_expo_time_sw(expo_tmp_time);
        __GetFrame16NoRotate(raw_image16 + (i * frame_size));
        if (g_inline_save_log_enable_extra_info) {
            egist_snprintf(name, INLINE_IMAGE_NAME_LENGH, "picture_wk_bk16_setp1_%d.bin",
                           X1000(expo_tmp_time));
            inline_obj_add(raw_image16 + (i * frame_size), name, TRUE);
        }
    }
    egislog_d(
        "WBOX step1: expo range_X1000=(%d-%d), step_X1000=%d, target_percentage=%d, "
        "raw_real_bpp=%d, img_cnt=%d",
        X1000(g_expo_start_time_wk), X1000(g_expo_end_time_wk), X1000(g_expo_step_wk),
        target_percentage, real_raw_bpp, g_expo_img_cnt_wk);

    IPexposure_estimate(g_expo_start_time_wk, target_percentage, DBG_HW_INT_WK, g_expo_step_wk,
                        g_expo_end_time_wk, real_raw_bpp, &centroid_x, &centroid_y, raw_image16,
                        SENSOR_WIDTH, SENSOR_HEIGHT, (int)g_expo_img_cnt_wk, &expo_ret);

    if (expo_ret <= g_expo_start_time_wk || expo_ret >= g_expo_end_time_wk) {
        egislog_e("WBOX step 1. Unexpected exposure result %d", X1000(expo_ret));
    } else {
        egislog_i("WBOX step 1. expo_result_X1000:%d [%d,%d]", X1000(expo_ret), centroid_x,
                  centroid_y);
    }
    if (expo_ret < 0) {
        egislog_e("IPexposure_estimate error, expo_result_X1000:%d", X1000(expo_ret));
        return FP_LIB_ERROR_GENERAL;
    }

    raw_image16 = g_inline_img->picture_wk_bk16_step2;
    memset(name, 0, sizeof(name));
    /* Step 2. */
    for (i = 0; i < DBG_IMG_CNT_STEP2_WK; i++) {
        expo_tmp_time = expo_ret - 1 + (0.1 * i);
        egislog_d("WBOX step 2. expoX1000:%d", X1000(expo_tmp_time));
        et7xa_set_expo_time_sw(expo_tmp_time);
        __GetFrame16NoRotate(raw_image16 + (i * frame_size));
        if (g_inline_save_log_enable_extra_info) {
            egist_snprintf(name, INLINE_IMAGE_NAME_LENGH, "picture_wk_bk16_setp2_%d.bin",
                           X1000(expo_tmp_time));
            inline_obj_add(raw_image16 + (i * frame_size), name, TRUE);
        }
    }

    egislog_i("WBOX step2: expo range_X1000=(%d-%d), target_percentage=%d, raw_real_bpp=%d",
              X1000(expo_ret - 1), X1000(expo_ret + 1), target_percentage, real_raw_bpp);

    IPexposure_estimate(expo_ret - 1, target_percentage, 1, 0.1f, expo_ret + 1, real_raw_bpp,
                        &centroid_x, &centroid_y, raw_image16, SENSOR_WIDTH, SENSOR_HEIGHT,
                        DBG_IMG_CNT_STEP2_WK, &expo_ret2);
#ifndef TZ_MODE
    et7xa_set_expo_time_sw(expo_ret2);
    uint16_t* wk_image = plat_alloc(frame_size * sizeof(uint16_t));
    __GetFrame16NoRotate(wk_image);
    covert_endian16((uint8_t*)wk_image, SENSOR_WIDTH, SENSOR_HEIGHT);
    char wk_path[PATH_MAX] = {0};
    egist_snprintf(wk_path, PATH_MAX, "/sdcard/RbsG5Temp/wk_image_%d.bin",
                   (int)(expo_ret2 * 1000 + 0.5));
    plat_save_file(wk_path, (uint8_t*)wk_image, frame_size * sizeof(uint16_t));
    plat_free(wk_image);
#endif
    g_final_MT_cali_data.expo = expo_ret2;
    if (get_sensor_id() == SENSOR_ID_ET701 || get_sensor_id() == SENSOR_ID_ET702) {
        egislog_e("set hw int to 1 for %d", get_sensor_id());
        g_final_MT_cali_data.hw_int = 1;
    } else {
        g_final_MT_cali_data.hw_int = target_capture_time / expo_ret2 /* + 0.5 */;
    }
    g_final_MT_cali_data.centroid_x = centroid_x;
    g_final_MT_cali_data.centroid_y = centroid_y;

    egislog_i("WBOX [SNR_RESULT] expo=X1000=%d(%d), hw_int=%d, [%d,%d]",
              X1000(g_final_MT_cali_data.expo), X1000(expo_ret2), g_final_MT_cali_data.hw_int,
              g_final_MT_cali_data.centroid_x, g_final_MT_cali_data.centroid_y);

    isensor_set_int(PARAM_INT_EXPOSURE_TIME_X10, original_exp_x10);
    if (expo_ret2 < 0) {
        egislog_e("IPexposure_estimate error, expo_ret2_X1000=%d", X1000(expo_ret2));
        return FP_LIB_ERROR_GENERAL;
    }

    return FP_LIB_OK;
#endif
}

static int egfps_get_bkbox_image_test(void) {
#ifdef LARGE_AREA
    return FP_LIB_OK;
#else
    int i = 0;
    float expo_tmp = 0;
    uint16_t* raw_image16 = NULL;
    int frame_size = SENSOR_WIDTH * SENSOR_HEIGHT;
    char name[INLINE_IMAGE_NAME_LENGH] = {0};

    egislog_d("====egfps_get_bkbox_image_test START====");

    // SNR bk img
    egislog_d("====SNR bk====");
    raw_image16 = g_inline_img->picture_snr_bk16;
    __EnableGain2(DBG_GAIN_SNR);
    expo_tmp = g_final_MT_cali_data.expo;
    et760_set_expo_time_sw(expo_tmp);  // Will modify input value.

    if (g_final_MT_cali_data.hw_int > 1) {
        et7xx_write_register(ET7XA_ADDR_SCAN_FRAMES, g_final_MT_cali_data.hw_int - 1);
        et7xx_write_register(ET7XA_ADDR_HDR_CSR, 0x01);
    } else {
        et7xx_write_register(ET7XA_ADDR_SCAN_FRAMES, g_final_MT_cali_data.hw_int - 1);
        et7xx_write_register(ET7XA_ADDR_HDR_CSR, 0x00);
    }
    __GetFrame16NoRotate(raw_image16);
    egist_snprintf(name, INLINE_IMAGE_NAME_LENGH, "picture_snr_bk16_%d.bin", X1000(expo_tmp));
    egislog_d("====SNR bk [name]%s====", name);
    inline_obj_add(raw_image16, name, TRUE);

    // Moire img
    egislog_d("====Moire====");
    raw_image16 = g_inline_img->picture_moire_bkg16;
    memset(name, 0, sizeof(name));
    __EnableGain2(DBG_GAIN_MOIRE);
    expo_tmp = g_final_MT_cali_data.expo;
    et7xa_set_expo_time_sw(expo_tmp);  // Will modify input value.
    if (g_final_MT_cali_data.hw_int > 1) {
        et7xx_write_register(ET7XA_ADDR_SCAN_FRAMES, g_final_MT_cali_data.hw_int - 1);
        et7xx_write_register(ET7XA_ADDR_HDR_CSR, 0x01);
    } else {
        et7xx_write_register(ET7XA_ADDR_SCAN_FRAMES, g_final_MT_cali_data.hw_int - 1);
        et7xx_write_register(ET7XA_ADDR_HDR_CSR, 0x00);
    }
    __GetFrame16NoRotate(raw_image16);
    if (g_inline_save_log_enable_extra_info) {
        egist_snprintf(name, INLINE_IMAGE_NAME_LENGH, "picture_moire_bkg16_%d.bin",
                       X1000(expo_tmp));
        inline_obj_add(raw_image16, name, TRUE);
    }

    // MF bk img
    egislog_d("====MF====");
    raw_image16 = g_inline_img->picture_mf_bk16;
    memset(name, 0, sizeof(name));
    __EnableGain2(DBG_GAIN_MF);
    expo_tmp = g_final_MT_cali_data.expo;
    et7xa_set_expo_time_sw(expo_tmp);  // Will modify input value.
    if (g_final_MT_cali_data.hw_int > 1) {
        et7xx_write_register(ET7XA_ADDR_SCAN_FRAMES, g_final_MT_cali_data.hw_int - 1);
        et7xx_write_register(ET7XA_ADDR_HDR_CSR, 0x01);
    } else {
        et7xx_write_register(ET7XA_ADDR_SCAN_FRAMES, g_final_MT_cali_data.hw_int - 1);
        et7xx_write_register(ET7XA_ADDR_HDR_CSR, 0x00);
    }
    __GetFrame16NoRotate(raw_image16);
    if (g_inline_save_log_enable_extra_info) {
        egist_snprintf(name, INLINE_IMAGE_NAME_LENGH, "picture_mf_bk16_%d.bin", X1000(expo_tmp));
        inline_obj_add(raw_image16, name, TRUE);
    }

    // for (i = 0; i < frame_size; i++) {
    //	ptest_inst->calib_img_dbg[i] =
    //		g_inline_img->picture_mf_for_fea_bk16[i] >> snr_config.mag_hw_int_shift;
    //}

    // BP img
    egislog_d("====BP====");
    raw_image16 = g_inline_img->picture_bp_bk16;
    memset(name, 0, sizeof(name));
    __EnableGain2(DBG_GAIN_BP);
    if (g_final_MT_cali_data.hw_int > 1) {
        et7xx_write_register(ET7XA_ADDR_SCAN_FRAMES, g_final_MT_cali_data.hw_int - 1);
        et7xx_write_register(ET7XA_ADDR_HDR_CSR, 0x01);
    } else {
        et7xx_write_register(ET7XA_ADDR_SCAN_FRAMES, g_final_MT_cali_data.hw_int - 1);
        et7xx_write_register(ET7XA_ADDR_HDR_CSR, 0x00);
    }
    for (i = 0; i < DBG_IMG_CNT_BP; i++) {
        expo_tmp = DBG_EXPO_TIME_BP + i;
        et7xa_set_expo_time_sw(expo_tmp);  // Will modify input value.
        __GetFrame16NoRotate(raw_image16 + (i * frame_size));
        if (g_inline_save_log_enable_extra_info) {
            egist_snprintf(name, INLINE_IMAGE_NAME_LENGH, "picture_bp_bk16_%d.bin",
                           X1000(expo_tmp));
            inline_obj_add(raw_image16 + (i * frame_size), name, TRUE);
        }
    }

    // FEA bk img
    egislog_d("====FEA====");
    raw_image16 = g_inline_img->picture_fea_bk16;
    memset(name, 0, sizeof(name));
    __EnableGain2(TRUE);  // 0x38
    expo_tmp = DBG_EXPO_TIME_FEA;
    et7xa_set_expo_time_sw(expo_tmp);
    et7xx_write_register(ET7XA_ADDR_SCAN_FRAMES, 0);
    et7xx_write_register(ET7XA_ADDR_HDR_CSR, 0x00);
    __GetFrame16(raw_image16);  // 200*200
    if (g_inline_save_log_enable_extra_info) {
        egist_snprintf(name, INLINE_IMAGE_NAME_LENGH, "picture_fea_bk16_%d.bin", X1000(expo_tmp));
        inline_obj_add(raw_image16, name, TRUE);
    }
    // SP bk img
    egislog_d("egfps_get_bkbox_image_test start gencalib: expX1000=%d, integ:%d",
              X1000(g_final_MT_cali_data.expo), g_final_MT_cali_data.hw_int);

    isensor_set_int(PARAM_INT_EXPOSURE_TIME_X10, g_final_MT_cali_data.expo * 10);
    isensor_set_int(PARAM_INT_HW_INTEGRATE_COUNT, g_final_MT_cali_data.hw_int);
    isensor_calibrate(FPS_CALI_ET7XX_BKG);
    isensor_calibrate(FPS_SAVE_ET7XX_CALI_DATA);
    isensor_calibrate(FPS_LOAD_ET7XX_CALI_DATA);

    int test_expo_x10 = 0;
    isensor_get_int(PARAM_INT_EXPOSURE_TIME_X10, &test_expo_x10);
    isensor_get_int(PARAM_INT_HW_INTEGRATE_COUNT, &g_final_MT_cali_data.hw_int);
    isensor_get_int(PARAM_INT_CALI_BKG_CX, &g_final_MT_cali_data.bkg_cx);
    isensor_get_int(PARAM_INT_CALI_BKG_CY, &g_final_MT_cali_data.bkg_cy);

    egislog_d("after calibration: expX1000=%d, integ:%d, test_expo_x10=%d",
              X1000(g_final_MT_cali_data.expo), g_final_MT_cali_data.hw_int, test_expo_x10);
    egislog_d("[SNR_RESULT] calibration bkg_cx,y=(%d,%d)", g_final_MT_cali_data.bkg_cx,
              g_final_MT_cali_data.bkg_cy);

    int buffer_out_length = IMG_MAX_BUFFER_SIZE * 2;
    isensor_get_buffer(PARAM_BUF_CALI_BKG_IMAGE, (unsigned char*)g_inline_img->picture_sp_bk16,
                       &buffer_out_length);
    memset(name, 0, sizeof(name));
    egist_snprintf(name, INLINE_IMAGE_NAME_LENGH, "picture_sp_bk16_%d.bin",
                   X1000(g_final_MT_cali_data.expo));
    inline_obj_add(g_inline_img->picture_sp_bk16, name, TRUE);

    return FP_LIB_OK;
#endif
}

static int egfps_get_chart_image_test(void) {
#ifdef LARGE_AREA
    return FP_LIB_OK;
#else
    int i;
    float expo_tmp;
    uint16_t* raw_image16 = NULL;
    int frame_size = SENSOR_WIDTH * SENSOR_HEIGHT;
    char name[INLINE_IMAGE_NAME_LENGH] = {0};

    egislog_d("====egfps_get_chart_image_test START====");
    // SNR chart img
    egislog_d("====SNR chart img====");
    __EnableGain2(DBG_GAIN_SNR);
    expo_tmp = g_final_MT_cali_data.expo;
    raw_image16 = g_inline_img->picture_snr_ct16;
    et7xa_set_expo_time_sw(expo_tmp);  // Will modify input value.
    if (g_final_MT_cali_data.hw_int > 1) {
        et7xx_write_register(ET7XA_ADDR_SCAN_FRAMES, g_final_MT_cali_data.hw_int - 1);
        et7xx_write_register(ET7XA_ADDR_HDR_CSR, 0x01);
    } else {
        et7xx_write_register(ET7XA_ADDR_SCAN_FRAMES, g_final_MT_cali_data.hw_int - 1);
        et7xx_write_register(ET7XA_ADDR_HDR_CSR, 0x00);
    }
    for (i = 0; i < DBG_IMG_CNT_SNR; i++) {
        __GetFrame16NoRotate(raw_image16 + (i * frame_size));
        egist_snprintf(name, INLINE_IMAGE_NAME_LENGH, "picture_snr_ct16_%d_%d.bin", i + 1,
                       X1000(expo_tmp));
        inline_obj_add(raw_image16 + (i * frame_size), name, TRUE);
    }

    // MF chart img
    egislog_d("====MF chart img====");
    __EnableGain2(DBG_GAIN_MF);
    raw_image16 = g_inline_img->picture_mf_ct16;
    memset(name, 0, sizeof(name));
    expo_tmp = g_final_MT_cali_data.expo;
    et7xa_set_expo_time_sw(expo_tmp);  // Will modify input value.
    if (g_final_MT_cali_data.hw_int > 1) {
        et7xx_write_register(ET7XA_ADDR_SCAN_FRAMES, g_final_MT_cali_data.hw_int - 1);
        et7xx_write_register(ET7XA_ADDR_HDR_CSR, 0x01);
    } else {
        et7xx_write_register(ET7XA_ADDR_SCAN_FRAMES, g_final_MT_cali_data.hw_int - 1);
        et7xx_write_register(ET7XA_ADDR_HDR_CSR, 0x00);
    }
    __GetFrame16NoRotate(raw_image16);
    if (g_inline_save_log_enable_extra_info) {
        egist_snprintf(name, INLINE_IMAGE_NAME_LENGH, "picture_mf_ct16_%d.bin", X1000(expo_tmp));
        inline_obj_add(raw_image16, name, TRUE);
    }

    return FP_LIB_OK;
#endif
}

static int egfps_lens_fea_counting_test() {
    int i = 0;
    int frame_size = SENSOR_WIDTH * SENSOR_HEIGHT;
    uint8_t* pResultImg = g_inline_img->picture_temp8;

    egislog_d("====egfps_lens_fea_counting_test Start====");
    unsigned int fov_x1 = 0, fov_y1 = 0, fov_x2 = 0, fov_y2 = 0;

    for (i = 0; i < frame_size; i++) {
        pResultImg[i] = g_inline_img->picture_fea_bk16[i] >> snr_config.fea_hw_int_shift;
    }

    LensFOVCounting(pResultImg, SENSOR_HEIGHT, SENSOR_WIDTH, g_final_MT_cali_data.centroid_y,
                    g_final_MT_cali_data.centroid_x, &fov_x1, &fov_x2, &fov_y1, &fov_y2);

    g_final_MT_cali_data.fov_x1_result =
        (float)(fov_x1 * snr_config.pc_cell_width * g_final_MT_cali_data.period * 1000) / 1000;
    g_final_MT_cali_data.fov_x2_result =
        (float)(fov_x2 * snr_config.pc_cell_width * g_final_MT_cali_data.period * 1000) / 1000;
    g_final_MT_cali_data.fov_y1_result =
        (float)(fov_y1 * snr_config.pc_cell_width * g_final_MT_cali_data.period * 1000) / 1000;
    g_final_MT_cali_data.fov_y2_result =
        (float)(fov_y2 * snr_config.pc_cell_width * g_final_MT_cali_data.period * 1000) / 1000;

    egislog_d("FEA X1000, x1x2 = %d,%d", X1000(fov_x1), X1000(fov_x2));
    egislog_d("FEA X1000, y1y2 = %d,%d", X1000(fov_y1), X1000(fov_y2));
    egislog_i("[SNR_RESULT] FEA X1000, x1x2 = (%d,%d), y1y2=(%d,%d)",
              X1000(g_final_MT_cali_data.fov_x1_result), X1000(g_final_MT_cali_data.fov_x2_result),
              X1000(g_final_MT_cali_data.fov_y1_result), X1000(g_final_MT_cali_data.fov_y2_result));
    return FP_LIB_OK;
}

static int egfps_lens_period_counting_test(void) {
    float period_ret;
    int best_angle;

    egislog_d("====egfps_lens_period_counting_test Start====");

    period_ret = cal_period_api(g_inline_img->picture_mf_ct16, g_inline_img->picture_mf_bk16,
                                SENSOR_WIDTH, SENSOR_HEIGHT, g_final_MT_cali_data.centroid_x,
                                g_final_MT_cali_data.centroid_y, DBG_ROI_HALF_WIDTH,
                                DBG_ROI_HALF_HEIGHT, g_ang_try_start, g_ang_try_end,
                                DBG_PERIOD_TRY_START, DBG_PERIOD_TRY_END, &best_angle);

    g_final_MT_cali_data.period = (float)DBG_PITCH_WIDTH / (period_ret * snr_config.pc_cell_width);

    egislog_i(
        "[SNR_RESULT] PERIOD X1000 =%d, ret=%d, pc_cell_width=%d, ang_try_start=%d, ang_try_end=%d",
        X1000(g_final_MT_cali_data.period), X1000(period_ret), snr_config.pc_cell_width,
        g_ang_try_start, g_ang_try_end);

    return FP_LIB_OK;
}

static int egfps_average_snr_test_inline(void) {
    float signal;
    float noise;
    float snr;
    float best_angle;
    char name[INLINE_IMAGE_NAME_LENGH] = {0};

    egislog_d("====egfps_average_snr_test_inline Start====");
    snr = cal_snr_level_api(g_inline_img->picture_snr_ct16, g_inline_img->picture_snr_bk16,
                            SENSOR_WIDTH, SENSOR_HEIGHT, DBG_IMG_CNT_SNR, SENSOR_WIDTH / 2,
                            SENSOR_HEIGHT / 2, DBG_ROI_HALF_WIDTH, DBG_ROI_HALF_HEIGHT,
                            g_ang_try_start, g_ang_try_end, DBG_PERIOD_TRY_START,
                            DBG_PERIOD_TRY_END, &signal, &noise, &best_angle);
    egislog_d("====egfps_average_snr_test_inline end====");
    g_final_MT_cali_data.signal = signal;
    g_final_MT_cali_data.noise = noise;
    g_final_MT_cali_data.snr = snr;

    cal_show_snr_image_api(g_inline_img->picture_snr_ct16, g_inline_img->picture_snr_bk16,
                           g_inline_img->picture_snr_dbg8, SENSOR_WIDTH, SENSOR_HEIGHT,
                           DBG_ROI_HALF_WIDTH, DBG_ROI_HALF_HEIGHT, 5, 100, best_angle);
    egist_snprintf(name, INLINE_IMAGE_NAME_LENGH, "picture_snr_ct8_dbg.bin");
    inline_obj_add((uint16_t*)g_inline_img->picture_snr_dbg8, name, FALSE);
    egislog(LOG_INFO, "[SNR_RESULT], X1000 signal=%d, noise=%d, snr=%d, best_angle=%d",
            X1000(signal), X1000(noise), X1000(snr), X1000(best_angle));

    /*	cal_show_snr_image_api(
        g_inline_img->picture_snr_ct16,
        g_inline_img->picture_snr_bk16,
        ptest_inst->snr_img_dbg,
        SENSOR_WIDTH,
        SENSOR_HEIGHT,
        DBG_ROI_HALF_WIDTH,
        DBG_ROI_HALF_HEIGHT,
        5,
        100,
        63);
*/
    return FP_LIB_OK;
}
#if 0
egfps_moire_test(HANDLE hTestInst,
                   CSensor *pEgisSensor,
                   unsigned char *pResultImg) {
    EGFPS_TEST_INST     *ptest_inst     = (EGFPS_TEST_INST *) hTestInst;

	IPcal_moire_strength_number(g_inline_img->picture_moire_bkg16,
	                            SENSOR_WIDTH,
	                            SENSOR_HEIGHT,
	                            snr_config.moire_hw_int_cnt,
	                            80, &g_final_MT_cali_data.center_col,
	                            &g_final_MT_cali_data.center_row,
	                            &g_final_MT_cali_data.moire_strength,
	                            &g_final_MT_cali_data.moire_number);

    egislog_d("Morie test, expo time = %.3f, HW = %d",
	          snr_config.moire_expo_time,
	          (int)snr_config.moire_hw_int_cnt);
	egislog_d("Morie test, (center_col, center_row) = (%d, %d)",
	          g_final_MT_cali_data.center_col,
	          g_final_MT_cali_data.center_row);
    egislog_d("Morie test, moire_strength = %.3f, moire_number = %d",
	          g_final_MT_cali_data.moire_strength,
	          g_final_MT_cali_data.moire_number);

#if !defined(TZ_MODE) && defined(SAVE_INLINE_DBG_IMG)
	memset(g_dbg_save_file, 0x0, 260);
    sprintf(g_dbg_save_file,
			"%s/img_mor_%f_%d_%d_%d.bin",
			g_dbg_save_path,
			g_final_MT_cali_data.moire_strength,
            g_final_MT_cali_data.moire_number,
		    g_final_MT_cali_data.center_col,
		    g_final_MT_cali_data.center_row);

	plat_save_raw_image(g_dbg_save_file,
						(unsigned char*)g_inline_img->picture_moire_bkg16,
	                    frame_size, 2);
#endif

	return FP_LIB_OK;
}
#endif
static int egfps_bad_pxl_test(void) {
    unsigned char bp_map[DBG_ROI_H_BP * DBG_ROI_W_BP] = {0};
    int cx, cy;

    egislog_d("egfps_bad_pxl_test x,y=(%d, %d)", g_final_MT_cali_data.centroid_x,
              g_final_MT_cali_data.centroid_y);

    // Please check this, why need to init? these result should get from "BPdetection".
    // Does "BPdetection" needs we init these?
    g_final_MT_cali_data.bad_block_cnt = 200;
    g_final_MT_cali_data.bad_pxl_max_continu_cnt = 200;
    g_final_MT_cali_data.bad_pxl_cnt = 200;

    BPgravity(g_inline_img->picture_bp_bk16 + ((DBG_IMG_CNT_BP - 1) * IMG_MAX_BUFFER_SIZE),
              SENSOR_HEIGHT, SENSOR_WIDTH, &cx, &cy);
    egislog_d("BPgravity (cx,cy)=(%d,%d)", cx, cy);

    BPdetection(g_inline_img->picture_bp_bk16, bp_map, DBG_IMG_CNT_BP, SENSOR_HEIGHT, SENSOR_WIDTH,
                cx, cy, DBG_ROI_H_BP, DBG_ROI_W_BP, DBG_STAR_IDX_BP, DBG_STEP_BP, 8,
                DBG_THRESHOLD_1_BP, DBG_THRESHOLD_2_BP, DBG_THRESHOLD_3_BP, 1,
                &g_final_MT_cali_data.bad_block_cnt, &g_final_MT_cali_data.bad_pxl_max_continu_cnt,
                &g_final_MT_cali_data.bad_pxl_cnt);

    egislog_d("[SNR_RESULT] BadBlockNum = %d, BadPixelMaxContinu = %d, BadPixelNum = %d",
              g_final_MT_cali_data.bad_block_cnt, g_final_MT_cali_data.bad_pxl_max_continu_cnt,
              g_final_MT_cali_data.bad_pxl_cnt);

    return FP_LIB_OK;
}

static int egfps_check_order_test(void) {
    int order_test;
    order_test = cal_et713_check_wb_box_order_api(g_inline_img->picture_sp_bk16,
                                                  g_inline_img->picture_intensity16, SENSOR_WIDTH,
                                                  SENSOR_HEIGHT);
    if (order_test) egislog_e("egfps_check_order_test = %d, Put White Box first.", order_test);
    return FP_LIB_OK;
}

static int egfps_img_avg_intensity_test(void) {
    char name[INLINE_IMAGE_NAME_LENGH] = {0};
    egislog_d("egfps_img_avg_intensity");

    // bHardwareReset(ptest_inst->egfps_test_config.ResetHold);
    // et7xx_write_register(PXL_CTRL1, 0x02, 0x0);
    // et7xx_write_register(VCM_CTRL, 0x61, 0x0);
    // et7xx_write_register(CONF, 0x00, 0x0);

    // isensor_set_int(PARAM_INT_EXPOSURE_TIME_X10, (int)(inline_expo_time * 10));
    // isensor_set_int(PARAM_INT_HW_INTEGRATE_COUNT, ptest_inst->egfps_test_result.inline_hw_int);

    __GetFrame16NoRotate(g_inline_img->picture_intensity16);
    if (g_inline_save_log_enable_extra_info) {
        egist_snprintf(name, INLINE_IMAGE_NAME_LENGH, "picture_intensity16.bin");
        inline_obj_add(g_inline_img->picture_intensity16, name, TRUE);
    }

    // bHardwareReset(ptest_inst->egfps_test_config.ResetHold);
    // et7xx_write_register(PXL_CTRL1, 0x02, 0x0);
    // et7xx_write_register(VCM_CTRL, 0x61, 0x0);

    egislog_d("calculate avg_intensity");
    g_final_MT_cali_data.avg_intensity = cal_img_avg_intensity_api(
        g_inline_img->picture_intensity16, SENSOR_WIDTH, SENSOR_HEIGHT, SENSOR_WIDTH / 2,
        SENSOR_HEIGHT / 2, DBG_ROI_HALF_WIDTH, DBG_ROI_HALF_HEIGHT);

    egislog_d("calculate bkg_avg_intensity");
    g_final_MT_cali_data.bkg_avg_intensity = cal_img_avg_intensity_api(
        g_inline_img->picture_sp_bk16, SENSOR_WIDTH, SENSOR_HEIGHT, SENSOR_WIDTH / 2,
        SENSOR_HEIGHT / 2, DBG_ROI_HALF_WIDTH, DBG_ROI_HALF_HEIGHT);
    egislog(LOG_DEBUG, "avg_intensity:%d, bkg_avg_intensity=%d", g_final_MT_cali_data.avg_intensity,
            g_final_MT_cali_data.bkg_avg_intensity);

    return FP_LIB_OK;
}

static int egfps_verify_flash_test(void) {
    int i;
    int ret = FP_LIB_OK;
    int bkg_image_16bit_size_flash = IMG_MAX_BUFFER_SIZE * sizeof(unsigned short);
    unsigned char* picture_sp_bkg16_flash = (unsigned char*)plat_alloc(bkg_image_16bit_size_flash);
    unsigned char* picture_sp_bkg16 = (unsigned char*)(g_inline_img->picture_sp_bk16);

    isensor_get_buffer(PARAM_BUF_CALI_BKG_IMAGE_FROM_FLASH, picture_sp_bkg16_flash,
                       &bkg_image_16bit_size_flash);

    for (i = 0; i < bkg_image_16bit_size_flash; i++) {
        if (picture_sp_bkg16_flash[i] != picture_sp_bkg16[i]) {
#ifdef EEPROM_USE
            ret = FP_LIB_ERROR_GENERAL;
#else
            ret = FP_LIB_OK;
#endif
            break;
        }
    }

    egislog(LOG_DEBUG, "egfps_verify_flash_test Result: %d(0 pass)", ret);
    plat_free(picture_sp_bkg16_flash);
    return ret;
}

UINT32 et713_do_snr(void* test) {
    UINT32 ret = FP_LIB_ERROR_GENERAL;
    UINT32 egfps_ret = FP_LIB_OK;
    static int image_number = 1;
    char temp_path[260];
    int buffer_out_length = IMG_MAX_BUFFER_SIZE * 2;

    egislog_i("et713_do_snr Start");
    struct sensor_test_output* out_data = ((struct egisfp_test_session*)test)->out_data;
    struct sensor_test_input* in_data = ((struct egisfp_test_session*)test)->in_data;
    egislog_d("et713_do_snr script_id: %d", in_data->script_id);

    switch (in_data->script_id) {
        case FP_INLINE_7XX_SNR_INIT: {
            egislog_d("[SNR_STATE_INIT]");
            if (egfps_ret != FP_LIB_OK) {
                egislog_e("egfps_get_wkbox_image_test fail");
                out_data->result = FP_LIB_ERROR_GENERAL;
                break;
            }
            out_data->result = FP_LIB_OK;
        } break;

            egislog_d("[SNR_STATE_WKBOX_ON]");
        case FP_INLINE_7XX_SNR_WKBOX_ON: {
            egislog_d("egfps_get_wkbox_image_test >>>");
            egfps_ret = egfps_get_wkbox_image_test();
            egislog_d("egfps_get_wkbox_image_test <<< result(%d)", egfps_ret);
            if (egfps_ret != FP_LIB_OK) {
                egislog_e("egfps_get_wkbox_image_test fail");
                out_data->result = FP_LIB_ERROR_GENERAL;
                break;
            }
            ret = FP_LIB_OK;
            out_data->result = FP_LIB_OK;
        } break;

        case FP_INLINE_7XX_SNR_BKBOX_ON: {
            egislog_d("[SNR_STATE_BKBOX_ON]");

            egislog_d("egfps_get_bkbox_image_test >>>");
            egfps_ret = egfps_get_bkbox_image_test();
            egislog_d("egfps_get_bkbox_image_test <<< result(%d)", egfps_ret);
            if (egfps_ret != FP_LIB_OK) {
                egislog_e("egfps_get_bkbox_image_test fail");
                out_data->result = FP_LIB_ERROR_GENERAL;
                break;
            }
            ret = FP_LIB_OK;
            out_data->result = FP_LIB_OK;
        } break;

        case FP_INLINE_7XX_SNR_CHART_ON: {
            egislog_d("[SNR_STATE_CHART_ON]");

            egislog_d("egfps_get_chart_image_test >>>");
            egfps_ret = egfps_get_chart_image_test();
            egislog_d("egfps_get_chart_image_test <<< result(%d)", egfps_ret);
            if (egfps_ret != FP_LIB_OK) {
                egislog_e("egfps_get_chart_image_test fail");
                ret = FP_LIB_ERROR_GENERAL;
                out_data->result = FP_LIB_ERROR_GENERAL;
                break;
            }

            egislog_d("egfps_average_snr_test_inline >>>");
            egfps_ret = egfps_average_snr_test_inline();
            egislog_d("egfps_average_snr_test_inline <<< result(%d)", egfps_ret);
            if (egfps_ret != FP_LIB_OK) {
                egislog_e("egfps_average_snr_test_inline fail");
                ret = FP_LIB_ERROR_GENERAL;
                out_data->result = FP_LIB_ERROR_GENERAL;
                break;
            }

            egislog_d("egfps_lens_period_counting_test >>>");
            egfps_ret = egfps_lens_period_counting_test();
            egislog_d("egfps_lens_period_counting_test <<< result(%d)", egfps_ret);
            if (egfps_ret != FP_LIB_OK) {
                egislog_e("egfps_lens_period_counting_test fail");
                ret = FP_LIB_ERROR_GENERAL;
                out_data->result = FP_LIB_ERROR_GENERAL;
                break;
            }

            egislog_d("egfps_lens_fea_counting_test >>>");
            egfps_ret = egfps_lens_fea_counting_test();
            egislog_d("egfps_lens_fea_counting_test <<< result(%d)", egfps_ret);
            if (egfps_ret != FP_LIB_OK) {
                egislog_e("egfps_lens_fea_counting_test fail");
                ret = FP_LIB_ERROR_GENERAL;
                out_data->result = FP_LIB_ERROR_GENERAL;
                break;
            }
            /*
                    egislog_d("egfps_moire_test >>>");
                    egfps_ret = egfps_moire_test((HANDLE)g_pTestInst, g_sensor,
                                                   g_pTestInst->picture_raw);
                    egislog_d("egfps_moire_test <<< result(%d)", egfps_ret);
                    if (egfps_ret != FP_LIB_OK) {
                        egislog_e("egfps_moire_test fail");
                        ret = FP_LIB_ERROR_GENERAL;
                        out_data->result = FP_LIB_ERROR_GENERAL;
                        break;
                    }
            */
            egislog_d("egfps_bad_pxl_test >>>");
            egfps_ret = egfps_bad_pxl_test();
            egislog_d("egfps_bad_pxl_test <<< result(%d)", egfps_ret);
            if (egfps_ret != FP_LIB_OK) {
                egislog_e("egfps_bad_pxl_test fail");
                ret = FP_LIB_ERROR_GENERAL;
                out_data->result = FP_LIB_ERROR_GENERAL;
                break;
            }

            egislog_d("egfps_img_avg_intensity_test >>>");
            egfps_ret = egfps_img_avg_intensity_test();
            egislog_d("egfps_img_avg_intensity_test <<< result(%d)", egfps_ret);
            if (egfps_ret != FP_LIB_OK) {
                egislog_e("egfps_img_avg_intensity_test fail");
                ret = FP_LIB_ERROR_GENERAL;
                out_data->result = FP_LIB_ERROR_GENERAL;
                break;
            }

            egislog_d("egfps_check_order_test >>>");
            egfps_ret = egfps_check_order_test();
            egislog_d("egfps_check_order_test <<< result(%d)", egfps_ret);
            if (egfps_ret != FP_LIB_OK) {
                egislog_e("egfps_check_order_test fail");
                ret = FP_LIB_ERROR_GENERAL;
                out_data->result = FP_LIB_ERROR_GENERAL;
                break;
            }
#if 0
		g_pTestInst->egfps_test_result.calib_data_save = 1;
		if (in_data->script_id == EGIS_FP_SNSR_TEST_SNR_CHART_ON_SCRIPT_ID) {
			egislog_d("egfps_save_efs_data >>>");
			egfps_ret = egfps_save_efs_data((HANDLE)g_pTestInst, g_sensor);
			egislog_d("egfps_save_efs_data <<< result(%d)", egfps_ret);
			if (egfps_ret != FP_LIB_OK) {
				egislog_e("egfps_save_efs_data fail");
				ret = FP_LIB_ERROR_GENERAL;
				out_data->result = FP_LIB_ERROR_GENERAL;
				break;
			}
		}
#endif
            ret = FP_LIB_OK;
            out_data->result = FP_LIB_OK;
        } break;
        case FP_INLINE_7XX_SNR_GET_DATA: {
            egislog_d("[SNR_STATE_GET_DATA], size=%d", sizeof(final_MT_cali_data_t));
            memcpy((void*)&out_data->data, &g_final_MT_cali_data, sizeof(final_MT_cali_data_t));
            out_data->result = FP_LIB_OK;
            ret = FP_LIB_OK;
        } break;
        case FP_INLINE_7XX_FLASH_TEST: {
            egislog_d("egfps_verify_flash_test >>>");
            isensor_calibrate(FPS_SAVE_ET7XX_CALI_DATA_TO_FLASH);
            egfps_ret = egfps_verify_flash_test();
            egislog_d("egfps_verify_flash_test <<< result(%d)", egfps_ret);
            if (egfps_ret != FP_LIB_OK) {
                egislog_e("egfps_verify_flash_test fail");
                ret = FP_LIB_ERROR_GENERAL;
                out_data->result = FP_LIB_ERROR_GENERAL;
                break;
            }
            ret = FP_LIB_OK;
            out_data->result = FP_LIB_OK;
        } break;

        case FP_INLINE_7XX_SNR_GET_ALL_IMAGE_16_BIT:
            egislog_d("[FP_INLINE_7XX_SNR_GET_ALL_IMAGE_16_BIT]");
            inline_obj_get(out_data->picture_buffer_16, out_data->name, TRUE);
            egislog_d("[%d] [%s]end", out_data->picture_remaining_count, out_data->name);
        case FP_INLINE_7XX_SNR_GET_ALL_IMAGE_16_BIT_INFO: {
            egislog_d("[FP_INLINE_7XX_SNR_GET_ALL_IMAGE_16_BIT_INFO]");
            out_data->picture_remaining_count = inline_obj_get_picture16_count();

            ret = FP_LIB_OK;
            out_data->result = FP_LIB_OK;
        } break;

        case FP_INLINE_7XX_SNR_GET_ALL_IMAGE_8_BIT:
            egislog_d("[FP_INLINE_7XX_SNR_GET_ALL_IMAGE_8_BIT]");
            inline_obj_get((uint16_t*)out_data->picture_buffer_8, out_data->name, FALSE);
            egislog_d("[%d] [%s]end", out_data->picture_remaining_count, out_data->name);
        case FP_INLINE_7XX_SNR_GET_ALL_IMAGE_8_BIT_INFO: {
            egislog_d("[FP_INLINE_7XX_SNR_GET_ALL_IMAGE_8_BIT_INFO]");
            out_data->picture_remaining_count = inline_obj_get_picture8_count();
            ;

            ret = FP_LIB_OK;
            out_data->result = FP_LIB_OK;
        } break;
        case FP_INLINE_7XX_SNR_SAVE_LOG_ENABLE_EXTRA_INFO: {
            egislog_d("FP_INLINE_7XX_SNR_SAVE_LOG_ENABLE_EXTRA_INFO");
            g_inline_save_log_enable_extra_info = TRUE;
            ret = FP_LIB_OK;
            out_data->result = FP_LIB_OK;
        } break;
        default:
            egislog(LOG_ERROR, "et713_do_snr unknow state");
            break;
    }

    egislog_i("et713_do_snr end, result: %d", out_data->result);
    return ret;
}

void egisfp_test_et7xx_do_snr_init(struct egisfp_test_session* egisfp_test) {
    egisfp_test->do_snr = et713_do_snr;
    egfps_snr_test_init();
}

void egisfp_test_et7xx_do_snr_uninit(void) {
    if (g_inline_img == NULL) {
        egislog_e("egisfp_test_et7xx_do_snr_uninit: g_inline_img is NULL!");
        return;
    }
    uint16_t** raw_image16 = NULL;
    int i = 0, image_num = sizeof(inline_picture_buf_t) / sizeof(void*);
    raw_image16 = (uint16_t**)g_inline_img;
    inline_obj_free();
    egislog_d("Free %d images", image_num);
    for (i = 0; i < image_num; i++) {
        if (raw_image16[i] != NULL) {
            plat_free(raw_image16[i]);
            raw_image16[i] = NULL;
        } else
            egislog_e("raw_image16 is NULL, check number %d images", i);
    }
    plat_free(raw_image16);
    g_inline_img = NULL;
}

int get_sensor_id() {
    int sensor_id = g_egis_sensortype.type;
    if (g_egis_sensortype.series >= 7) {
        sensor_id += 700;
    }
    return sensor_id;
};
