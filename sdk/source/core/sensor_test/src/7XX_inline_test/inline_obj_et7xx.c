#include "inline_obj_et7xx.h"
#include <stdlib.h>

#include "egis_sprintf.h"
#include "inline_obj_et7xx.h"
#include "plat_log.h"
#include "plat_mem.h"

#define LOG_TAG "RBS-7XX_inline_SNR"
static out_inline_picture_buf_t g_out_inline_img;
static int g_picture16_count_max = 0, g_picture8_count_max = 0;

void inline_obj_add(uint16_t* img, char* name, BOOL is_16bit) {
    out_inline_picture_buf_t* obj = &g_out_inline_img;
    if (is_16bit == TRUE) {
        if (obj->picture16_count < g_picture16_count_max) {
            obj->picture16_buf[obj->picture16_count].img = img;
            memcpy(obj->picture16_buf[obj->picture16_count].name, name, INLINE_IMAGE_NAME_LENGH);
            egislog_d("add name=[%s]", obj->picture16_buf[obj->picture16_count].name);
            obj->picture16_count++;
        } else {
            // Todo: auto realloc
            egislog_e("add fail, inline 16bit buffer is FULL, please alloc larger");
        }
    } else {
        if (obj->picture8_count < g_picture8_count_max) {
            obj->picture8_buf[obj->picture8_count].img = img;
            memcpy(obj->picture8_buf[obj->picture8_count].name, name, INLINE_IMAGE_NAME_LENGH);
            egislog_d("add name=[%s]", obj->picture8_buf[obj->picture8_count].name);
            obj->picture8_count++;
        } else {
            egislog_e("add fail, inline 8bit buffer is FULL, please alloc larger");
        }
    }
    egislog_d("_add 8:[%d/%d], 16:[%d/%d]", obj->picture8_count, g_picture8_count_max,
              obj->picture16_count, g_picture16_count_max);
}
void inline_obj_get(uint16_t* img, uint8_t* name, BOOL is_16bit) {
    out_inline_picture_buf_t* obj = &g_out_inline_img;

    egislog_d("_get start 8:[%d], 16:[%d] is_16bit=%d", obj->picture8_count, obj->picture16_count,
              is_16bit);
    if (is_16bit == TRUE) {
        if (obj->picture16_count) {
            obj->picture16_count--;
            memcpy(img, obj->picture16_buf[obj->picture16_count].img, IMG_MAX_BUFFER_SIZE * 2);
            memcpy(name, obj->picture16_buf[obj->picture16_count].name, INLINE_IMAGE_NAME_LENGH);
            egislog_d("get name=[%s]", name);
        }
    } else {
        if (obj->picture8_count) {
            obj->picture8_count--;
            memcpy(img, obj->picture8_buf[obj->picture8_count].img, IMG_MAX_BUFFER_SIZE);
            memcpy(name, obj->picture8_buf[obj->picture8_count].name, INLINE_IMAGE_NAME_LENGH);
            egislog_d("get name=[%s]", name);
        }
    }
    egislog_d("_get end 8:[%d/%d], 16:[%d/%d] is_16bit=%d", obj->picture8_count,
              g_picture8_count_max, obj->picture16_count, g_picture16_count_max, is_16bit);
}

int inline_obj_get_picture16_count(void) {
    egislog_d("inline_obj_get_picture16_count %d", g_out_inline_img.picture16_count);
    return g_out_inline_img.picture16_count;
}

int inline_obj_get_picture8_count(void) {
    egislog_d("inline_obj_get_picture8_count %d", g_out_inline_img.picture8_count);
    return g_out_inline_img.picture8_count;
}

void inline_obj_create(int picture8_count, int picture16_count) {
    g_out_inline_img.picture8_buf =
        (inline_img_t*)plat_alloc(picture8_count * sizeof(inline_img_t));
    g_out_inline_img.picture16_buf =
        (inline_img_t*)plat_alloc(picture16_count * sizeof(inline_img_t));
    memset(g_out_inline_img.picture8_buf, 0, picture8_count * sizeof(inline_img_t));
    memset(g_out_inline_img.picture16_buf, 0, picture16_count * sizeof(inline_img_t));
    egislog_d("create 8:[%d], 16:[%d] End", picture8_count, picture16_count);
    g_picture8_count_max = picture8_count;
    g_picture16_count_max = picture16_count;
}
void inline_obj_free(void) {
    g_out_inline_img.picture8_count = 0;
    g_out_inline_img.picture16_count = 0;
    plat_free(g_out_inline_img.picture8_buf);
    plat_free(g_out_inline_img.picture16_buf);
    egislog_d("free End");
}