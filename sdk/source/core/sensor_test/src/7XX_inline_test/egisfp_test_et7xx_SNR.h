#include "7xx_sensor_test_definition.h"
#include "egisfp_test_et7xx.h"
#include "type_definition.h"

#pragma once

typedef struct _SNR_config {
    // WKbox test
    int hw_int_cnt;
    int hw_int_shift;
    int target_percentage;
    // MAG_TEST
    int mag_hw_int_shift;
    int mag_centroid_x;
    int mag_centroid_y;
    int mag_roi_w;
    int mag_roi_h;
    int mag_scan_window;
    int mag_scan_direct;
    int mag_roi_chart_pich;
    int mag_roi_sensor_pitch;
    // FEA_TEST
    int fea_hw_int_shift;
    // module setting
    int pc_cell_width;
} snr_config_t;

typedef struct _inline_picture_buf {
    uint8_t* picture_temp8;
    uint8_t* picture_snr_dbg8;

    uint16_t* picture_snr_bk16;
    uint16_t* picture_mf_bk16;
    uint16_t* picture_mf_ct16;
    uint16_t* picture_fea_bk16;
    uint16_t* picture_sp_bk16;
    uint16_t* picture_intensity16;
    uint16_t* picture_wk_bk16;
    uint16_t* picture_wk_bk16_step2;
    uint16_t* picture_moire_bkg16;
    uint16_t* picture_snr_ct16;
    uint16_t* picture_bp_bk16;
} inline_picture_buf_t;

typedef struct _inline_picture {
    uint8_t name[INLINE_IMAGE_NAME_LENGH];
    uint16_t* img;
} inline_img_t;

typedef struct _out_inline_picture_buf {
    int picture8_count;
    int picture16_count;
    inline_img_t* picture8_buf;
    inline_img_t* picture16_buf;
} out_inline_picture_buf_t;

void egisfp_test_et7xx_do_snr_uninit(void);
void egisfp_test_et7xx_do_snr_init(struct egisfp_test_session* egisfp_test);
int get_sensor_id();