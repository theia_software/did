#ifndef __EGFPS_TEST_ET7XX_H__
#define __EGFPS_TEST_ET7XX_H__

#include "7xx_sensor_test_definition.h"
#include "type_definition.h"

typedef enum fp_type {
    FP_NONE = 0,
    FP_ET711,
    FP_ET713,
} fp_type;

typedef UINT32 (*DO_NORMAL_SCAN)(void* egis_test_session);
typedef UINT32 (*DO_SNR)(void* egis_test_session);

struct egisfp_test_session {
    struct sensor_test_input* in_data;
    struct sensor_test_output* out_data;
    DO_NORMAL_SCAN do_normal_scan;
    DO_SNR do_snr;
};

#endif
