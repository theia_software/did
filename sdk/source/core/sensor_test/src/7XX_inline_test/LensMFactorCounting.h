#ifndef __LENSMFACTORCOUNTING_H__
#define __LENSMFACTORCOUNTING_H__

double LensMFactorCounting(unsigned char* a_Image, unsigned int a_Width, unsigned int a_Height,
                           unsigned int a_centroid_x, unsigned int a_centroid_y,
                           unsigned int a_ROI_Width, unsigned int a_ROI_Height,
                           unsigned int a_Scan_window, unsigned char a_Scan_direct,
                           unsigned int a_ROI_ChartPitch, unsigned int a_ROI_SensorPitch,
                           double* a_Ridge, double* a_Valley);

#endif
