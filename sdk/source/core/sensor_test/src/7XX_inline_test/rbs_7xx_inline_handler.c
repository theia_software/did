#include "rbs_7xx_inline_handler.h"
#include "7xx_sensor_test_definition.h"
#include "common_definition.h"
//#include "egisfp_et7xx_normal_scan.h"
#include "egisfp_test_et7xx.h"
#include "egisfp_test_et7xx_SNR.h"
#include "plat_log.h"
#include "plat_mem.h"

#define LOG_TAG "RBS-7xxinline"
#define TEST_PROJECT_SENSOR_TYPE FP_ET713
//"enum fp_type" copy from
// CS1 \fingerprint\sensor\jni\public\include\hw_definition.h
static struct egisfp_test_session* g_p_7xx_test = NULL;

static struct egisfp_test_session* egisfp_test_alloc(enum fp_type type) {
    struct egisfp_test_session* egisfp_test;

    egislog_d("egisfp_test_alloc type: %d", type);
    egisfp_test = (struct egisfp_test_session*)plat_alloc(sizeof(struct egisfp_test_session));
    if (!egisfp_test) {
        egisfp_test = NULL;
        egislog_e("egisfp_test_alloc ERROR");
        return NULL;
    }
    egislog_d("egisfp_test_alloc =%p", egisfp_test);

    switch (type) {
        case FP_ET711:
        case FP_ET713:
            //egisfp_test_et7xx_normal_scan_init(egisfp_test);
            //egisfp_test_et7xx_do_snr_init(egisfp_test);
            break;
        // case FP_ET7XX:
        default:
            egislog_e("egisfp_test_alloc type ERROR");
            egisfp_test = NULL;
            break;
    }
    egislog_d("egisfp_test_alloc =%p", egisfp_test);
    return egisfp_test;
}

void et7xx_inline_init() {
    egislog_d("7xx_inline_init in");
    egislog_d("sizeof(struct egisfp_test_session)=%d", sizeof(struct egisfp_test_session));

    if (g_p_7xx_test == NULL) {
        g_p_7xx_test = egisfp_test_alloc(TEST_PROJECT_SENSOR_TYPE);
        if (g_p_7xx_test == NULL) egislog_e("egisfp_test_alloc = fail");
    } else
        egislog_e("7xx_inline_init not NULL, Do you free before?");
    egislog_d("7xx_inline_init g_p_7xx_test=%p", g_p_7xx_test);
}

void et7xx_inline_uninit() {
    egislog_d("7xx_inline_uninit in");
    //egisfp_test_et7xx_do_snr_uninit();
    //egisfp_test_et7xx_normal_scan_uninit();
    if (g_p_7xx_test) plat_free(g_p_7xx_test);
    g_p_7xx_test = NULL;
}

UINT32 et7xx_inline_test_handler(UINT32 script_id, UINT8* in_data, UINT32 in_data_len,
                                 UINT8* out_data, UINT32* out_data_len) {
    UINT32 ret = 0;

    egislog_d("7xx_inline_test_handler in. script_id=%d", script_id);
    if (g_p_7xx_test == NULL) {
        egislog_e("please 7xx_inline_init first, g_p_7xx_test=NULL");
        return ret;
    }

    g_p_7xx_test->in_data = (struct sensor_test_input*)in_data;
    g_p_7xx_test->out_data = (struct sensor_test_output*)out_data;
    g_p_7xx_test->in_data->script_id = script_id;

    switch (script_id) {
        case FP_INLINE_7XX_NORMALSCAN:
            ret = g_p_7xx_test->do_normal_scan(g_p_7xx_test);
            break;

        case FP_INLINE_7XX_NORMAL_GET_IMAGE:
            //egisfp_test_et7xx_normal_scan_set_state(NS_STATE_GET_IMAGE);
            ret = g_p_7xx_test->do_normal_scan(g_p_7xx_test);
            break;
        case FP_INLINE_7XX_SNR_INIT:
        case FP_INLINE_7XX_SNR_WKBOX_ON:
        case FP_INLINE_7XX_SNR_BKBOX_ON:
        case FP_INLINE_7XX_SNR_CHART_ON:
        case FP_INLINE_7XX_SNR_GET_ALL_IMAGE_16_BIT_INFO:
        case FP_INLINE_7XX_SNR_GET_ALL_IMAGE_16_BIT:
        case FP_INLINE_7XX_SNR_GET_ALL_IMAGE_8_BIT_INFO:
        case FP_INLINE_7XX_SNR_GET_ALL_IMAGE_8_BIT:
        case FP_INLINE_7XX_SNR_GET_DATA:
        case FP_INLINE_7XX_FLASH_TEST:
        case FP_INLINE_7XX_SNR_SAVE_LOG_ENABLE_EXTRA_INFO:
            ret = g_p_7xx_test->do_snr(g_p_7xx_test);
            break;
        default:
            egislog_e("no ID script_id=%d", script_id);
            break;
    }

    egislog_i("7xx_inline_test_handler ret=%d, out_data_len=%d", ret, *out_data_len);
    return ret;
}
