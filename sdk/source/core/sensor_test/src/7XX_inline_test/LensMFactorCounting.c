#include <stdlib.h>
//#include <memory.h>
#include <string.h>
#include "LensMFactorCounting.h"
#include "plat_mem.h"

#define FALSE 0
#define TRUE 1

#ifndef NULL
#define NULL ((void*)0)
#endif

#define LENS_STRIPE_COUNTING_ARRAY_SIZE 512

static unsigned char MTFCalculatingRidgeValleyCount(
    unsigned char* a_Image, unsigned int a_ImageWidth, unsigned int a_ImageHeight,
    unsigned int a_centroid_x, unsigned int a_centroid_y, unsigned short a_scan_window,
    unsigned char a_Scan_direct, unsigned short a_ROI_Width, unsigned short a_ROI_height,
    unsigned short* a_RidgeCnt, unsigned short* a_ValleyCnt, unsigned short* a_RidgeDist,
    unsigned short* a_ValleDist) {
    int i, j;
    unsigned char bFindingMax = TRUE;
    unsigned char data_array[LENS_STRIPE_COUNTING_ARRAY_SIZE];
    unsigned char ridge_valley_array[LENS_STRIPE_COUNTING_ARRAY_SIZE];
    unsigned char LocalPeak = 0;
    unsigned int ridge_count = 0;
    unsigned int valley_count = 0;
    int start = 0;
    int end = 0;
    unsigned short r_start = 0, v_start = 0;
    double r_dist = 0, v_dist = 0;
    double tmp;
    unsigned int r_curr = 0, v_curr = 0;

    memset(ridge_valley_array, 0x00, LENS_STRIPE_COUNTING_ARRAY_SIZE);

    if (a_Scan_direct == 2) {
        start = a_centroid_y - (a_ROI_height / 2);

        if (start < 0) start = 0;

        end = start + a_ROI_height;
        if (end > a_ImageHeight) {
            end = a_ROI_height;
            start = end - a_ROI_height;
        }
        for (i = 0; i < a_ImageHeight; i++) {
            data_array[i] = a_Image[i * a_ImageWidth + a_centroid_x];
        }
    } else {
        // copy data to data array
        memcpy(data_array, (a_Image + (a_centroid_y * a_ImageWidth)), a_ImageWidth);

        start = a_centroid_x - (a_ROI_Width / 2);

        if (start < 0) start = 0;

        end = start + a_ROI_Width;
        if (end > a_ImageWidth) {
            end = a_ImageWidth;
            start = end - a_ROI_Width;
        }
    }

    bFindingMax = TRUE;
    LocalPeak = 0;

    for (i = start; i < end; i++) {
        if (bFindingMax) {
            if (data_array[i] > LocalPeak) {
                LocalPeak = data_array[i];
            }
            for (j = i; j < i + a_scan_window; j++) {
                if (data_array[j] > LocalPeak) {
                    break;
                }
            }
            if (j == i + a_scan_window) {
                bFindingMax = FALSE;
                ridge_valley_array[i] |= 0x01;
                ridge_count++;
                LocalPeak = 0xFF;
            }
        } else {
            if (data_array[i] < LocalPeak) {
                LocalPeak = data_array[i];
            }
            for (j = i; j < i + a_scan_window; j++) {
                if (data_array[j] < LocalPeak) {
                    break;
                }
            }
            if (j == i + a_scan_window) {
                bFindingMax = TRUE;
                ridge_valley_array[i] |= 0x02;
                valley_count++;
                LocalPeak = 0x00;
            }
        }
    }

    // Search ridge and valley
    for (i = start; i < end; i++) {
        if (ridge_valley_array[i] == 0x01) {
            if (r_start == 0) {
                r_start = i;
            } else {
                if (r_dist == 0) {
                    r_dist = i - r_start;
                } else {
                    tmp = i - r_curr;

                    r_dist = (r_dist + tmp) / 2;
                }
            }
            r_curr = i;
        }
        if (ridge_valley_array[i] == 0x02) {
            if (v_start == 0) {
                v_start = i;
            } else {
                if (v_dist == 0) {
                    v_dist = i - v_start;
                } else {
                    tmp = i - v_curr;
                    v_dist = (v_dist + tmp) / 2;
                }
            }
            v_curr = i;
        }
    }

    // counting result
    r_dist = r_curr - r_start;
    v_dist = v_curr - v_start;

    *a_RidgeCnt = ridge_count;
    *a_ValleyCnt = valley_count;
    *a_RidgeDist = (r_curr - r_start);
    *a_ValleDist = (v_curr - v_start);

    return TRUE;
}

double LensMFactorCounting(unsigned char* a_Image, unsigned int a_Width, unsigned int a_Height,
                           unsigned int a_centroid_x, unsigned int a_centroid_y,
                           unsigned int a_ROI_Width, unsigned int a_ROI_Height,
                           unsigned int a_Scan_window, unsigned char a_Scan_direct,
                           unsigned int a_ROI_ChartPitch, unsigned int a_ROI_SensorPitch,
                           double* a_Ridge, double* a_Valley) {
    double sensor_area, M_Factor;
    unsigned short Ridge;
    unsigned short Valley;
    unsigned short RidgeDist;
    unsigned short ValleyDist;

    double M_Factor_ridge, M_Factor_valley;

    MTFCalculatingRidgeValleyCount(a_Image, a_Width, a_Height, a_centroid_x, a_centroid_y,
                                   a_Scan_window, a_Scan_direct, a_ROI_Width, a_ROI_Height, &Ridge,
                                   &Valley, &RidgeDist, &ValleyDist);

    sensor_area = a_ROI_Width * a_ROI_SensorPitch;

    if (RidgeDist == 0) {
        M_Factor_ridge = 0;
    } else {
        M_Factor_ridge =
            ((double)(Ridge - 1) * (a_ROI_ChartPitch * 2) / RidgeDist) / a_ROI_SensorPitch;
    }
    if (ValleyDist == 0) {
        M_Factor_valley = 0;
    } else {
        M_Factor_valley =
            ((double)(Valley - 1) * (a_ROI_ChartPitch * 2) / ValleyDist) / a_ROI_SensorPitch;
    }

    if ((M_Factor_ridge == 0) || (M_Factor_valley == 0)) {
        M_Factor = (M_Factor_ridge > M_Factor_valley) ? M_Factor_ridge : M_Factor_valley;
    } else {
        M_Factor = (M_Factor_ridge + M_Factor_valley) / 2;
    }

    if (a_Ridge != NULL) {
        *a_Ridge = Ridge;
    }
    if (a_Valley != NULL) {
        *a_Valley = Valley;
    }

    return M_Factor;
}
