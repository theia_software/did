#include "egisfp_test_et7xx_SNR.h"
#include "type_definition.h"

void inline_obj_add(uint16_t* img, char* name, BOOL is_16bit);

void inline_obj_get(uint16_t* img, uint8_t* name, BOOL is_16bit);

int inline_obj_get_picture16_count(void);

int inline_obj_get_picture8_count(void);

void inline_obj_create(int picture8_count, int picture16_count);

void inline_obj_free(void);
