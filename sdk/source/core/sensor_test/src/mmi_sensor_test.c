#include "../../sensor_control/src/egis_handle_esd.h"
#include "algomodule.h"
#include "common_definition.h"
#include "core_config.h"
#include "egis_definition.h"
#include "egis_fp_get_image.h"
#include "egis_log.h"
#include "egis_sensor_test.h"
#include "egis_sensor_test_et5xx.h"
#include "egis_sensormodule.h"
#include "fp_algomodule.h"
#include "fp_sensormodule.h"
#include "image_analysis.h"
#include "isensor_api.h"
#include "plat_heap.h"
#include "qm_lib.h"

#define CHECKFINGER_FPIMAGE_LITE 2000
#define CHECKFINGER_QM_EXTRACT 2001
#define CHECKFINGER_STATISTICS 3
#define QM_MIN_DR_VALUR 30
#define IMG_QTY_THRESHOLD 120
#define QM_IMG_QTY_THRESHOLD 100
#define IMG_LEVEL_THRESHOLD 3
#define IMG_PERCENTAGE_THRESHOLD 30
#define IMG_BSD2_THRESHOLD 400
#define NVM_UID_SIZE 6
#define NVM_UID_BASE_ADDR 0x3A
#define FCHECK_NON_FINGER FALSE
#define FCHECK_IS_FINGER TRUE
#define LOG_TAG "RBS-SENSOR"

static QMFeatures g_base_feat;
static int gNumBadPixel = 0;
// The function implemetation is on OpenSourceB
int fp_runSnrTest(uint8_t type, uint32_t* result);
void egis_device_reset();

int egis_sensorTest(egisFpSnsrTestScriptId_t script_id) {
#ifdef __ET5XX__
    BufferIn buffer_in;
    BufferOut buffer_out;
    egisBlobData_t in_blob_data;
    egisBlobData_t out_blob_data;
    int result = -1;
    int ret = -1;
    gNumBadPixel = 0;

    in_blob_data.length = sizeof(buffer_in);
    in_blob_data.pData = (uint8_t*)&buffer_in;
    out_blob_data.pData = (uint8_t*)&buffer_out;

    buffer_in.scriptId = script_id;

    result = egisFpSnsrTest((egisConstBlobData_t*)&in_blob_data, &out_blob_data);
    egislog_d("result= %d ,Script ID= %d Done, test result= %d", result, buffer_in.scriptId,
              buffer_out.result);

    if (EGIS_FP_DIRTYDOTS_TEST_SCRIPT_ID == script_id) {
        egislog_d("DIRTYDOTS_TEST_RESULT_VER 0x%X TestFailItem 0x%X", buffer_out.picture_put[0],
                  buffer_out.picture_put[1]);
        memcpy(&gNumBadPixel, buffer_out.picture_put + INDEX_BAD_PIXEL, sizeof(int));
        egislog_d("gNumBadPixel=%d", gNumBadPixel);
    }

    sensor_exception_reset();
    return buffer_out.result;
#else
    return EGIS_TEST_FAIL;
#endif
}

#define WHITE_SNR_IMAGE_TYPE 0

int egis_mmiDoTest(fp_mmi_info* pMmiInfo) {
    int retval = FP_LIB_OK;
    uint32_t result;
    int egisRet;
    int width, height;
    egis_image_t test_image;
    fd_img_analysis_info_t bestImgInfo;
    FINGER_QUALITY finger_on_status;

    egislog_d("%s entry", __func__);

    if (pMmiInfo == NULL) {
        egislog_e("pMmiInfo = NULL!");
        retval = FP_LIB_ERROR_PARAMETER;
        goto out;
    }

    egislog_d("pMmiInfo->testType[%d]", pMmiInfo->testType);
    switch (pMmiInfo->testType) {
        case FP_MMI_AUTO_TEST:
            egisRet = egis_sensorTest(EGIS_FP_DIRTYDOTS_TEST_SCRIPT_ID);
            egisRet = egis_sensorTest(EGIS_FP_READ_REV_TEST_SCRIPT_ID);
            egisRet = egis_sensorTest(EGIS_FP_REGISTER_RW_TEST_SCRIPT_ID);
            if (EGIS_INLINETOOL_SENSOR_RESET == egisRet) {
                egislog_d("Need reset sensor");
                egis_device_reset();
                egisRet = egis_sensorTest(EGIS_FP_REGISTER_RW_TEST_SCRIPT_ID);
            }
            // do reset and recover after sensor test
            egis_device_reset();

            result = isensor_recovery(g_fp_mode);
            egislog_d("isensor_recovery %d", result);

            if (EGIS_OK == egisRet) {
                pMmiInfo->testResult = MMI_TEST_SUCCESS;
            } else {
                pMmiInfo->testResult = MMI_TEST_FAIL;
                retval = FP_LIB_ERROR_GENERAL;
            }
            break;
        case FP_MMI_TYPE_INTERRUPT_TEST:
            pMmiInfo->testResult = MMI_TEST_SUCCESS;
            break;
        case FP_MMI_FAKE_FINGER_TEST:
            pMmiInfo->testResult = MMI_TEST_SUCCESS;
            break;
        case FP_MMI_SNR_SINGAL_IMAGE_TEST:
            isensor_set_sensor_mode();
            fp_runSnrTest(1, &result);
            egislog_d("fp_runSnrTest result[%d] %d", pMmiInfo->testType, result);
            pMmiInfo->testResult = result;
            break;
        case FP_MMI_SNR_WHITE_IMAGE_TEST:
            isensor_set_sensor_mode();
            fp_runSnrTest(0, &result);
            egislog_d("fp_runSnrTest result[%d] %d", pMmiInfo->testType, result);
            pMmiInfo->testResult = result;
            break;
        case FP_MMI_DIRTYDOTS_TEST:
            egisRet = egis_sensorTest(EGIS_FP_DIRTYDOTS_TEST_SCRIPT_ID);
            if (EGIS_OK == egisRet) {
                pMmiInfo->testResult = MMI_TEST_SUCCESS;
            } else {
                pMmiInfo->testResult = MMI_TEST_FAIL;
                retval = FP_LIB_ERROR_GENERAL;
            }
            isensor_set_power_off();
            break;
        case FP_MMI_READ_REV_TEST:
            egisRet = egis_sensorTest(EGIS_FP_READ_REV_TEST_SCRIPT_ID);
            if (EGIS_OK == egisRet) {
                pMmiInfo->testResult = MMI_TEST_SUCCESS;
            } else {
                pMmiInfo->testResult = MMI_TEST_FAIL;
                retval = FP_LIB_ERROR_GENERAL;
            }
            break;
        case FP_MMI_REGISTER_RW_TEST:
            egisRet = egis_sensorTest(EGIS_FP_REGISTER_RW_TEST_SCRIPT_ID);
            if (EGIS_OK == egisRet) {
                pMmiInfo->testResult = MMI_TEST_SUCCESS;
            } else if (EGIS_INLINETOOL_SENSOR_RESET == egisRet) {
                egislog_d("Need reset sensor");
                retval = egisRet;
                goto out;
            } else {
                pMmiInfo->testResult = MMI_TEST_FAIL;
                retval = FP_LIB_ERROR_GENERAL;
            }
            break;
        case FP_MMI_REGISTER_RECOVERY:
            egislog_d("egis FP_MMI_REGISTER_RECOVERY enter!");
            egisRet = egis_recovery();
            if (EGIS_OK == egisRet) {
                pMmiInfo->testResult = MMI_TEST_SUCCESS;
            } else {
                pMmiInfo->testResult = MMI_TEST_FAIL;
                retval = FP_LIB_ERROR_GENERAL;
            }
            break;
        case FP_MMI_FOD_TEST:
            egisRet = isensor_set_detect_mode();
            if (EGIS_OK == egisRet) {
                pMmiInfo->testResult = MMI_TEST_SUCCESS;
            } else {
                pMmiInfo->testResult = MMI_TEST_FAIL;
            }
            break;
        case FP_MMI_GET_FINGER_IMAGE:
            egisRet = isensor_set_sensor_mode();
            if (egisRet != EGIS_OK) {
                pMmiInfo->testResult = MMI_TEST_FAIL;
                break;
            }

            egisRet = isensor_get_sensor_roi_size(&width, &height);
            if (egisRet != EGIS_OK) {
                pMmiInfo->testResult = MMI_TEST_FAIL;
                break;
            }

            EGIS_IMAGE_CREATE(&test_image, width, height, 1);
            fd_finger_detect(FCHECK_FLOW_VERIFY_NO_STABLE, &test_image, &bestImgInfo,
                             &finger_on_status, FD_PROCESS_OPTION_NORMAL);
            pMmiInfo->testResult =
                finger_on_status == FINGER_QUALITY_GOOD ? MMI_TEST_SUCCESS : MMI_TEST_FAIL;
            EGIS_IMAGE_FREE(&test_image);
            isensor_set_power_off();
            break;
        default:
            break;
    }
    egislog_d("testType[%d] testResult %d (TEST_SUCCESS is 1)", pMmiInfo->testType,
              pMmiInfo->testResult);

out:
    egislog_d("%s end, retval = %d", __func__, retval);

    return retval;
}

int egis_mmiCoatingResult(fp_Coatingcheck* data) {
    int retval = FP_LIB_OK;

    egislog_d("%s entry", __func__);

    if (data == NULL) {
        egislog_e("data = NULL!");
        retval = FP_LIB_ERROR_PARAMETER;
        goto out;
    }
/*
 * Add implement Code here.
 */
out:
    egislog_d("%s end, retval = %d", __func__, retval);

    return retval;
}

int egis_mmiDeadPixelResult(uint32_t* data) {
    int retval = FP_LIB_OK;

    egislog_d("%s entry", __func__);

    if (data == NULL) {
        egislog_e("data = NULL!");
        retval = FP_LIB_ERROR_PARAMETER;
        goto out;
    }
    *data = gNumBadPixel;
out:
    egislog_d("%s end, retval = %d, data is %d", __func__, retval, *data);

    return retval;
}

int egis_getCoatingImage(unsigned char* buffer, unsigned int length) {
    int retval = FP_LIB_OK;

    egislog_d("%s entry", __func__);

    if (buffer == NULL) {
        egislog_e("buffer = NULL!");
        retval = FP_LIB_ERROR_PARAMETER;
        goto out;
    }
/*
 * Add implement Code here.
 */
out:
    egislog_d("%s end, retval = %d", __func__, retval);

    return retval;
}

void egis_setSnrParameter(fp_snr_parameter_t* snr_parameter) {
    egislog_d("%s entry", __func__);
    int width, height;

    if (snr_parameter == NULL) {
        egislog_e("snr_parameter = NULL!");
        goto out;
    }

    if (isensor_get_sensor_roi_size(&width, &height) != EGIS_OK) {
        egislog_e("isensor_get_sensor_roi_size fail!");
        goto out;
    }

    snr_parameter->width = width;
    snr_parameter->height = height;
    snr_parameter->image_counts = 30;
    snr_parameter->cut_white_pixels = 0;
    snr_parameter->cut_signal_pixels = 0;
    snr_parameter->calibration = 0;
    snr_parameter->cut_black_percent = (float)0.05;
    snr_parameter->cut_white_percent = (float)0.1;
    snr_parameter->snr_threshold = 15;

out:
    egislog_d("%s end", __func__);
}

#define ABNORMAL_THRES 30
#define CALC_ABS(x, y) ((x > y) ? (x - y) : (y - x))
static int replace_abnormal_pixel(uint8_t* pBaseImage, uint8_t* pImage, uint32_t image_size) {
    int i;
    int diff;
    int countAbnormal = 0;
    uint8_t* pBaseTemp = pBaseImage;
    uint8_t* pTemp = pImage;
    for (i = 0; i < (int)image_size; i++) {
        diff = 128 + *pTemp - *pBaseTemp;
        if (diff > 255 || diff < 0) {
            countAbnormal++;
            *pTemp = *pBaseTemp;
        }
        pBaseTemp++;
        pTemp++;
        if (countAbnormal > ABNORMAL_THRES) {
            egislog_e("Warning: countAbnormal=%d.", countAbnormal);
            return -1;
        }
    }
    egislog_d("done. countAbnormal=%d.", countAbnormal);
    return EGIS_OK;
}

int egis_captureSnrImage(uint8_t* buffer, uint32_t image_counts, uint32_t image_size,
                         uint8_t type) {
    int i, j;
    int retval = FP_LIB_OK;
    int width, height;
    uint8_t* pImage = NULL;

    egislog_d("%s entry type=%d image_counts %d.", __func__, type, image_counts);
    egislog_d("%s entry type=%d image_size %d.", __func__, image_size);

    retval = isensor_get_sensor_roi_size(&width, &height);
    if (retval != EGIS_OK) {
        goto out;
    }

    if (buffer == NULL) {
        egislog_e("buffer = NULL!");
        retval = FP_LIB_ERROR_PARAMETER;
        goto out;
    }

    if (image_size != width * height) {
        egislog_e("image size not matched. %d, %d:%d.", image_size, width, height);
        retval = FP_LIB_ERROR_PARAMETER;
        goto out;
    }

    pImage = (uint8_t*)plat_alloc(image_size);
    if (pImage == NULL) {
        egislog_e("Failed to allocate pBaseImage");
        retval = FP_LIB_ERROR_MEMORY;
        goto out;
    }

    int ret = isensor_set_sensor_mode();
    if (ret != EGIS_OK) {
        egislog_e("captureSnrImage set_sensor_mode, ret = %d", ret);
        return -1;
    }

    for (j = 0; j < (int)image_counts; j++) {
        isensor_get_dynamic_frame(pImage, height, width, 1);
        for (i = 0; i < (int)image_size; i++) buffer[j * image_size + i] = pImage[i];
    }

out:

    if (pImage != NULL) {
        plat_free(pImage);
        pImage = NULL;
    }

    egislog_d("%s end, retval = %d", __func__, retval);
    return retval;
}

BOOL egis_checkIsFinger(uint8_t* img, int img_width, int img_height, int check_mode,
                        liver_image_out_header_t* pheader) {
    egislog_d("%s, check mode = %d end!", __func__, check_mode);
    BOOL isFinger = FCHECK_NON_FINGER;
    if (check_mode == CHECKFINGER_STATISTICS) {
        egis_mmiGetStatisticData(pheader);
        return isFinger;
    }
#ifdef G3_MATCHER

#ifdef __ET6XX__
    int use_vgg = core_config_get_int(INI_SECTION_SENSOR, KEY_FIMAGE_CHECK_VGG, 0);
    if (use_vgg) {
        int ipp_sensor_type =
            core_config_get_int(INI_SECTION_SENSOR, KEY_IPP_SENSOR_TYPE, DEFAULT_IPP);
        VggResult vgg_result;
        int vgg_value =
            IPvgg_finger_detect(ipp_sensor_type, img, img_width, img_height, &vgg_result);
        egislog_d("IPvgg [%d] finger %d, percentage %d", ipp_sensor_type, vgg_value,
                  vgg_result.percentage);
    }
#endif

    if (check_mode == CHECKFINGER_FPIMAGE_LITE) {
        int bsd2 = 0, corner_count = 0, percentage = 0, cover_count = 0, img_intensity = 0,
            img_qty = 0, img_level = 0;
        img_qty = resample_IsFPImage_Lite(img, img_width, img_height, &corner_count, &cover_count,
                                          0, &img_intensity);
        img_level = IPis_fp_image_raw_info(img, img_width, img_height, NULL);
        IPnavi_is_fp_image_percentage(img, img_width, img_height, &percentage);
        bsd2 = IPcount_percentageBSD2(img, img_width, img_height);
        egislog_d(
            "CHECKFINGER_FPIMAGE_LITE img_qty = %d img_level = %d, percentage = %d , bsd = %d end.",
            img_qty, img_level, percentage, bsd2);
        if (img_qty >= IMG_QTY_THRESHOLD && img_level >= IMG_LEVEL_THRESHOLD &&
            percentage >= IMG_PERCENTAGE_THRESHOLD && bsd2 < IMG_BSD2_THRESHOLD)
            isFinger = FCHECK_IS_FINGER;

        pheader->live_image_type = LIVIMG_IMAGETYPE_FPIMAGELITE;
        pheader->image_par_t.process_parameter.isFinger = isFinger;
        pheader->image_par_t.process_parameter.percentage = percentage;
        pheader->image_par_t.process_parameter.corner_count = corner_count;
        pheader->image_par_t.process_parameter.img_qty = img_qty;
        pheader->image_par_t.process_parameter.cover_count = cover_count;
        pheader->image_par_t.process_parameter.img_level = img_level;
        pheader->image_par_t.process_parameter.bsd2 = bsd2;

    } else if (check_mode == CHECKFINGER_QM_EXTRACT) {
        QMFeatures qm_feat;
        QMOption* qm_opt;
        QMMatchDetail qm_md;
        int qm_score;
        int img_level;
        unsigned char* raw = plat_alloc(img_width * img_height);
        if (raw == NULL) goto EXIT;

        qm_opt = qm_alloc_option();
        if (qm_opt == NULL) {
            PLAT_FREE(raw);
            goto EXIT;
        }

        memcpy(raw, img, img_width * img_height);
        qm_opt->count_percentage = TRUE;
        qm_opt->min_dr_value = QM_MIN_DR_VALUR;
        qm_opt->return_qty = TRUE;

        img_level = IPis_fp_image_raw_info(img, img_width, img_height, NULL);

        qm_extract(raw, img_width, img_height, &qm_feat, qm_opt);
        egislog_d("CHECKFINGER_QM_EXTRACT qty %d percentage %d !", qm_feat.qty, qm_feat.percentage);
        if (qm_feat.qty >= QM_IMG_QTY_THRESHOLD && qm_feat.percentage >= IMG_PERCENTAGE_THRESHOLD) {
            isFinger = FCHECK_IS_FINGER;
        }

        qm_score = qm_match(&g_base_feat, &qm_feat, &qm_md, qm_opt);
        memcpy(&g_base_feat, &qm_feat, sizeof(QMFeatures));
        egislog_d("CHECKFINGER_QM_EXTRACT qm_score %d dx %d dy %d !", qm_score, qm_md.dx, qm_md.dy);

        pheader->live_image_type = LIVING_IMAGETYPE_QMEXTRACT;
        pheader->image_par_t.qm_parameter.isFinger = isFinger;
        pheader->image_par_t.qm_parameter.qty = qm_feat.qty;
        pheader->image_par_t.qm_parameter.size = qm_feat.size;
        pheader->image_par_t.qm_parameter.percentage = qm_feat.percentage;
        pheader->image_par_t.qm_parameter.img_level = img_level;
        pheader->image_par_t.qm_parameter.gap = IPotsu_gap_w(img, img_width, img_height);
        pheader->image_par_t.qm_parameter.qm_score = qm_score;
        pheader->image_par_t.qm_parameter.dx = qm_md.dx;
        pheader->image_par_t.qm_parameter.dy = qm_md.dy;

        qm_free_option(qm_opt);

        PLAT_FREE(raw);
    }
EXIT:
    egislog_d("%s, isFinger = %d !", __func__, isFinger);
#endif
    return isFinger;
}

int egis_mmiGetNVMUid(unsigned char* buf, int* buf_size) {
    egislog_d("egis_mmiGetNVMUid enter!");
    if (NULL == buf) {
        return EGIS_INCORRECT_PARAMETER;
    }

    int retval = EGIS_COMMAND_FAIL;
    unsigned char temp[70];

    memset(temp, 0, 70);
    retval = isensor_read_nvm(temp);
    if (retval != EGIS_OK) {
        egislog_d("isensor_get_nvm_uid isensor_read_nvm fail!");
        return retval;
    }

    memcpy(buf, &temp[NVM_UID_BASE_ADDR], NVM_UID_SIZE);
    *buf_size = NVM_UID_SIZE;

    return retval;
}

void egis_mmiGetStatisticData(liver_image_out_header_t* pheader) {
    egislog_d("egis_mmiGetStatisticData enter!");
    pheader->live_image_type = LIVING_IMAGETYPE_STATISTICSDATA;
    int statistics_data_size = sizeof(pheader->image_par_t.image_statistics_data);
    isensor_get_buffer(PARAM_BUF_GET_RAWFRAME_STATISTICS,
                       (unsigned char*)&pheader->image_par_t.image_statistics_data,
                       &statistics_data_size);
    egislog_i("CHECKFINGER_STATISTICS mean=%d max=%d min=%d",
              pheader->image_par_t.image_statistics_data.mean,
              pheader->image_par_t.image_statistics_data.max,
              pheader->image_par_t.image_statistics_data.min);
}
