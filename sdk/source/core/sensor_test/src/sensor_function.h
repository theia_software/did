#include <stdint.h>

#include "type_definition.h"

//#define Sleep(ms)   usleep(ms*1000)
#define PASS 0
#define FAIL -1

#define MAX_REG_ADDR 0x58
#define REG_TYPE_NONE 0x00
#define REG_TYPE_DISCARD 0x00
#define REG_TYPE_READ 0x01
#define REG_TYPE_WRITE 0x02

#define TEST_NONE 0x00
#define TEST_WHITE_DOT 0x01
#define TEST_DARK_DOT 0x02
#define TEST_DEFECT_DOT 0x04
#define TEST_MARK_BAD_BLOCK 0x08
#define TEST_CONSECUTIVE_DOT 0x10

#define ImageAverageCount 10

typedef struct _SENSOR_REG {
    unsigned char RegAddr;
    unsigned char RegType;
    unsigned char RegValue;
    unsigned char RegValueDefault;
    unsigned char RegValueMask;
} SENSOR_REG;

typedef struct _Image_Valid_Area {
    unsigned char row_start;
    unsigned char row_end;
    unsigned char col_start;
    unsigned char col_end;
} Image_Valid_Area;

int ReadRegister(unsigned char addr, unsigned char* pValue);
int WriteRegister(unsigned char addr, unsigned char value);
int GetFrame(unsigned char* pData);

void sensor_reg_init(SENSOR_REG* pSENSOR_REG);

void vEvaluateImage(unsigned char* dirtydots_map, int* pWhiteDots, int* pDarkDots,
                    BOOL* pTooManyDirtyDots, int* pMaxDirtyDotsInBlock, Image_Valid_Area* pScanArea,
                    unsigned char* pImage, unsigned char option, unsigned char white_pxl_th,
                    unsigned char dark_pxl_th);

unsigned char** alloc_multi_sensor_frames(int nFrame);
void free_multi_sensor_frames(unsigned char** ppAllFrames, int nFrame);
int get_multi_sensor_frames(unsigned char** ppAllFrames, int nFrame, UINT height, UINT width);

int calc_multi_frames_avg(unsigned char** ppAllFrames, int nFrame, unsigned char* pAvgFrame);
int calc_multi_frames_stddev_avg(unsigned char** ppAllFrames, int nFrame, unsigned char* pAvgFrame,
                                 float* avgStdDev);
int adjust_multi_frames_avg(unsigned char** ppAllFrames, int nFrame, unsigned char* pData,
                            unsigned char white_pxl_th, unsigned char dark_pxl_th);
int img_range(int data);

int set_black_reg(void);
int set_white_reg(void);
