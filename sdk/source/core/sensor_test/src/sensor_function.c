#include <stdio.h>
#include <string.h>

#include "../../sensor_control/src/et5xx/fpsensor_5XX_definition.h"
#include "egis_definition.h"
#include "egis_log.h"
#include "isensor_api.h"
#include "isensor_definition.h"
#include "plat_heap.h"
#include "plat_spi.h"
#include "sensor_function.h"
#include "sensor_test_definition.h"
#include "type_definition.h"

static void combine_bad_map(BYTE* map1, BYTE* map2, int frame_size);

#ifndef TEEIGP
#include <math.h>
#endif
#define LOG_TAG "RBS-EGISFP-SENSOR_TestFunc"

int ReadRegister(unsigned char addr, unsigned char* pValue) {
    return io_5xx_dispatch_read_register(addr, pValue);
}

int WriteRegister(unsigned char addr, unsigned char value) {
    return io_5xx_dispatch_write_register(addr, value);
}

int GetFrame(unsigned char* pData) {
    int ret = -1;
    int width, height;

    ret = isensor_get_sensor_roi_size(&width, &height);
    if (ret != EGIS_OK) {
        return ret;
    }

    ret = io_5xx_dispatch_write_register(SCAN_CSR_ET5XX_ADDR, 0x13);
    polling_registry(STUS_ET5XX_ADDR, 0x01, 0x01);
    io_5xx_dispatch_get_frame(pData, height, width, 0, 1);
    ret = io_5xx_dispatch_write_register(SCAN_CSR_ET5XX_ADDR, 0x20);
    return ret;
}

void sensor_reg_init(SENSOR_REG* pSENSOR_REG) {
    int width, height;
    unsigned char i;

    isensor_get_sensor_roi_size(&width, &height);

    for (i = 0; i <= MAX_REG_ADDR; i++) {
        pSENSOR_REG[i].RegAddr = i;
        pSENSOR_REG[i].RegType = REG_TYPE_NONE;
        pSENSOR_REG[i].RegValue = 0x00;
        pSENSOR_REG[i].RegValueDefault = 0x00;
        pSENSOR_REG[i].RegValueMask = 0xFF;
    }

    // STUS
    pSENSOR_REG[0x00].RegType = REG_TYPE_READ;
    pSENSOR_REG[0x00].RegValueDefault = 0xAA;
    pSENSOR_REG[0x00].RegValueMask = 0xFF;

    // INT_STUS
    pSENSOR_REG[0x01].RegType = REG_TYPE_READ;
    pSENSOR_REG[0x01].RegValueDefault = 0x01;
    pSENSOR_REG[0x01].RegValueMask = 0xFF;

    // INT_CONF
    pSENSOR_REG[0x02].RegType = REG_TYPE_READ | REG_TYPE_WRITE;
    pSENSOR_REG[0x02].RegValueDefault = 0x02;
    pSENSOR_REG[0x02].RegValueMask = 0x07;

    // ADC_DAT_SUBS
    pSENSOR_REG[0x03].RegType = REG_TYPE_READ | REG_TYPE_WRITE;
    pSENSOR_REG[0x03].RegValueDefault = 0x00;
    pSENSOR_REG[0x03].RegValueMask = 0xFF;

    // IO_DS
    pSENSOR_REG[0x04].RegType = REG_TYPE_READ | REG_TYPE_WRITE;
    pSENSOR_REG[0x04].RegValueDefault = 0x01;
    pSENSOR_REG[0x04].RegValueMask = 0x01;

    // OSC40M_CONF
    pSENSOR_REG[0x07].RegType = REG_TYPE_DISCARD;
    pSENSOR_REG[0x07].RegValueDefault = 0x15;
    pSENSOR_REG[0x07].RegValueMask = 0x3F;

    // OSC100K_CSR
    pSENSOR_REG[0x08].RegType = REG_TYPE_DISCARD;
    pSENSOR_REG[0x08].RegValueDefault = 0x88;
    pSENSOR_REG[0x08].RegValueMask = 0xEE;

    // PWR_CTRL0
    pSENSOR_REG[0x09].RegType = REG_TYPE_DISCARD;  // REG_TYPE_READ | REG_TYPE_WRITE;
    pSENSOR_REG[0x09].RegValueDefault = 0x04;
    pSENSOR_REG[0x09].RegValueMask = 0xC7;

    // PWR_CTRL1
    pSENSOR_REG[0x0A].RegType = REG_TYPE_DISCARD;
    pSENSOR_REG[0x0A].RegValueDefault = 0xFF;  // ET510ABC
    pSENSOR_REG[0x0A].RegValueDefault = 0xFC;  // ET510D
    pSENSOR_REG[0x0A].RegValueMask = 0xFF;

    // PWR_CTRL2
    pSENSOR_REG[0x0B].RegType = REG_TYPE_READ | REG_TYPE_WRITE;
    pSENSOR_REG[0x0B].RegValueDefault = 0x03;
    pSENSOR_REG[0x0B].RegValueMask = 0x0B;

    // OSC40M_CSR
    pSENSOR_REG[0x0C].RegType = REG_TYPE_DISCARD;
    pSENSOR_REG[0x0C].RegValueDefault = 0x00;
    pSENSOR_REG[0x0C].RegValueMask = 0xFF;

    // VREF_SEL
    pSENSOR_REG[0x0D].RegType = REG_TYPE_DISCARD;
    pSENSOR_REG[0x0D].RegValueDefault = 0x03;
    pSENSOR_REG[0x0D].RegValueMask = 0xFF;

    // DC_P
    pSENSOR_REG[0x0E].RegType = REG_TYPE_DISCARD;
    pSENSOR_REG[0x0E].RegValueDefault = 0x0B;
    pSENSOR_REG[0x0E].RegValueMask = 0xFF;

    // DC_C
    pSENSOR_REG[0x0F].RegType = REG_TYPE_DISCARD;
    pSENSOR_REG[0x0F].RegValueDefault = 0x15;
    pSENSOR_REG[0x0F].RegValueMask = 0xFF;

    // DC_F_G
    pSENSOR_REG[0x10].RegType = REG_TYPE_DISCARD;
    pSENSOR_REG[0x10].RegValueDefault = 0x15;
    pSENSOR_REG[0x10].RegValueMask = 0xFF;

    // DC_F_SEL
    pSENSOR_REG[0x11].RegType = REG_TYPE_DISCARD;
    pSENSOR_REG[0x11].RegValueDefault = 0x00;
    pSENSOR_REG[0x11].RegValueMask = 0xFF;

    // PGA_GAIN
    pSENSOR_REG[0x12].RegType = REG_TYPE_DISCARD;
    pSENSOR_REG[0x12].RegValueDefault = 0x04;
    pSENSOR_REG[0x12].RegValueMask = 0x0F;

    // AFE_CONF0
    pSENSOR_REG[0x13].RegType = REG_TYPE_DISCARD;
    pSENSOR_REG[0x13].RegValueDefault = 0x52;
    pSENSOR_REG[0x13].RegValueMask = 0xFF;

    // SCAN_Z1_COL_BEGIN
    pSENSOR_REG[0x20].RegType = REG_TYPE_READ | REG_TYPE_WRITE;
    pSENSOR_REG[0x20].RegValueDefault = 0x00;
    pSENSOR_REG[0x20].RegValueMask = 0x3F;

    // SCAN_Z1_COL_END
    pSENSOR_REG[0x21].RegType = REG_TYPE_READ | REG_TYPE_WRITE;
#ifdef __ET538__
    pSENSOR_REG[0x21].RegValueDefault = SENSOR_FULL_WIDTH - 1;

    int mask;
    mask = 0x01;
    do {
        mask <<= 1;
    } while (mask <= (SENSOR_FULL_WIDTH - 1));
    pSENSOR_REG[0x21].RegValueMask = mask - 1;
#else
    pSENSOR_REG[0x21].RegValueDefault = width - 1;

    int mask;
    mask = 0x01;
    do {
        mask <<= 1;
    } while (mask <= (width - 1));
    pSENSOR_REG[0x21].RegValueMask = mask - 1;
#endif
    // SCAN_Z1_COL_STEP
    pSENSOR_REG[0x22].RegType = REG_TYPE_READ | REG_TYPE_WRITE;
    pSENSOR_REG[0x22].RegValueDefault = 0x01;
    pSENSOR_REG[0x22].RegValueMask = 0x3F;

    // SCAN_Z1_ROW_BEGIN
    pSENSOR_REG[0x23].RegType = REG_TYPE_READ | REG_TYPE_WRITE;
    pSENSOR_REG[0x23].RegValueDefault = 0x00;
    pSENSOR_REG[0x23].RegValueMask = 0x3F;

    // SCAN_Z1_ROW_END
    pSENSOR_REG[0x24].RegType = REG_TYPE_READ | REG_TYPE_WRITE;
    pSENSOR_REG[0x24].RegValueDefault = height - 1;

    mask = 0x01;
    do {
        mask <<= 1;
    } while (mask <= (height - 1));
    pSENSOR_REG[0x24].RegValueMask = mask - 1;

    // SCAN_Z1_ROW_STEP
    pSENSOR_REG[0x25].RegType = REG_TYPE_READ | REG_TYPE_WRITE;
    pSENSOR_REG[0x25].RegValueDefault = 0x01;
    pSENSOR_REG[0x25].RegValueMask = 0x3F;

    // SCAN_Z2_COL_BEGIN
    pSENSOR_REG[0x26].RegType = REG_TYPE_DISCARD;
    pSENSOR_REG[0x26].RegValueDefault = SCAN_Z2_COL_BEGIN;
    pSENSOR_REG[0x26].RegValueMask = 0x7F;

    // SCAN_Z2_COL_END
    pSENSOR_REG[0x27].RegType = REG_TYPE_DISCARD;
    pSENSOR_REG[0x27].RegValueDefault = SCAN_Z2_COL_END;
    mask = 0x01;
    do {
        mask <<= 1;
    } while (mask <= SCAN_Z2_COL_END);
    pSENSOR_REG[0x27].RegValueMask = mask - 1;

    // SCAN_Z2_COL_STEP
    pSENSOR_REG[0x28].RegType = REG_TYPE_DISCARD;
    pSENSOR_REG[0x28].RegValueDefault = SCAN_Z2_COL_STEP;
    pSENSOR_REG[0x28].RegValueMask = 0x7F;

    // SCAN_Z2_ROW_BEGIN
    pSENSOR_REG[0x29].RegType = REG_TYPE_DISCARD;
    pSENSOR_REG[0x29].RegValueDefault = SCAN_Z2_ROW_BEGIN;
    pSENSOR_REG[0x29].RegValueMask = 0x3F;

    // SCAN_Z2_ROW_END
    pSENSOR_REG[0x2A].RegType = REG_TYPE_DISCARD;
    pSENSOR_REG[0x2A].RegValueDefault = SCAN_Z2_ROW_END;
    pSENSOR_REG[0x2A].RegValueMask = 0x3F;

    // SCAN_Z2_ROW_STEP
    pSENSOR_REG[0x2B].RegType = REG_TYPE_DISCARD;
    pSENSOR_REG[0x2B].RegValueDefault = SCAN_Z2_ROW_STEP;
    pSENSOR_REG[0x2B].RegValueMask = 0x3F;

    // SCAN_FRAMES
    pSENSOR_REG[0x2C].RegType = REG_TYPE_READ | REG_TYPE_WRITE;
    pSENSOR_REG[0x2C].RegValueDefault = 0x00;
    pSENSOR_REG[0x2C].RegValueMask = 0xFF;

    // SCAN_CSR
    pSENSOR_REG[0x2D].RegType = REG_TYPE_DISCARD;
    pSENSOR_REG[0x2D].RegValueDefault = 0x00;
    pSENSOR_REG[0x2D].RegValueMask = 0xFF;

    // CALI_VDM_TM
    pSENSOR_REG[0x33].RegType = REG_TYPE_DISCARD;
    pSENSOR_REG[0x33].RegValueDefault = 0x00;
    pSENSOR_REG[0x33].RegValueMask = 0xFF;

    // CALI_CONF
    pSENSOR_REG[0x34].RegType = REG_TYPE_DISCARD;
    pSENSOR_REG[0x34].RegValueDefault = 0x0F;
    pSENSOR_REG[0x34].RegValueMask = 0xFF;

    // CALI_CSR
    pSENSOR_REG[0x35].RegType = REG_TYPE_DISCARD;
    pSENSOR_REG[0x35].RegValueDefault = 0x00;
    pSENSOR_REG[0x35].RegValueMask = 0xFF;

    // FOD_CSR
    pSENSOR_REG[0x40].RegType = REG_TYPE_DISCARD;
    pSENSOR_REG[0x40].RegValueDefault = 0x80;
    pSENSOR_REG[0x40].RegValueMask = 0x83;

    // FOD_MODE
    pSENSOR_REG[0x41].RegType = REG_TYPE_READ | REG_TYPE_WRITE;
    pSENSOR_REG[0x41].RegValueDefault = 0x00;
    pSENSOR_REG[0x41].RegValueMask = 0x01;

    // FOD_CYCLES_H
    // sensor_reg_all[0x42].RegType = REG_TYPE_READ | REG_TYPE_WRITE;
    pSENSOR_REG[0x42].RegType = REG_TYPE_DISCARD;
    pSENSOR_REG[0x42].RegValueDefault = 0x09;
    pSENSOR_REG[0x42].RegValueMask = 0xFF;

    // FOD_CYCLES_L
    // sensor_reg_all[0x42].RegType = REG_TYPE_READ | REG_TYPE_WRITE;
    pSENSOR_REG[0x42].RegType = REG_TYPE_DISCARD;
    pSENSOR_REG[0x43].RegValueMask = 0xFF;

    // FOD_MEAN_THH
    pSENSOR_REG[0x44].RegType = REG_TYPE_DISCARD;
    pSENSOR_REG[0x44].RegValueDefault = 0xB0;
    pSENSOR_REG[0x44].RegValueMask = 0xFF;

    // FOD_MEAN_THL
    pSENSOR_REG[0x45].RegType = REG_TYPE_DISCARD;
    pSENSOR_REG[0x45].RegValueDefault = 0x10;
    pSENSOR_REG[0x45].RegValueMask = 0xFF;

    // FOD_VALLEY_COL_TH
    pSENSOR_REG[0x46].RegType = REG_TYPE_DISCARD;
    pSENSOR_REG[0x46].RegValueDefault = 0x06;
    pSENSOR_REG[0x46].RegValueMask = 0xFF;

    // FOD_VALLEY_ROW_TH
    pSENSOR_REG[0x47].RegType = REG_TYPE_DISCARD;
    pSENSOR_REG[0x47].RegValueDefault = 0x06;
    pSENSOR_REG[0x47].RegValueMask = 0xFF;

    // FOD_TP2_SEL
    // sensor_reg_all[0x49].RegType = REG_TYPE_READ | REG_TYPE_WRITE;
    pSENSOR_REG[0x49].RegType = REG_TYPE_DISCARD;
    pSENSOR_REG[0x49].RegValueDefault = 0x18;
    pSENSOR_REG[0x49].RegValueMask = 0xFF;

    // FIX_COLROW_EN
    pSENSOR_REG[0x54].RegType = REG_TYPE_READ | REG_TYPE_WRITE;
    pSENSOR_REG[0x54].RegValueDefault = 0x00;
    pSENSOR_REG[0x54].RegValueMask = 0x01;

    // FIX_COL
    // sensor_reg_all[0x55].RegType = REG_TYPE_READ | REG_TYPE_WRITE;
    pSENSOR_REG[0x55].RegType = REG_TYPE_DISCARD;
    pSENSOR_REG[0x55].RegValueDefault = 0x33;
    pSENSOR_REG[0x55].RegValueMask = 0x7F;

    // FIX_ROW
    // sensor_reg_all[0x56].RegType =REG_TYPE_READ | REG_TYPE_WRITE;
    pSENSOR_REG[0x56].RegType = REG_TYPE_DISCARD;
    pSENSOR_REG[0x56].RegValueDefault = 0x1A;
    pSENSOR_REG[0x56].RegValueMask = 0x3F;

    // STAT_VAL_SLOPE
    // sensor_reg_all[0x61].RegType = REG_TYPE_READ | REG_TYPE_WRITE;
    pSENSOR_REG[0x61].RegType = REG_TYPE_DISCARD;
    pSENSOR_REG[0x61].RegValueDefault = 0x27;
    pSENSOR_REG[0x61].RegValueMask = 0xFF;

    // STAT_MIN
    pSENSOR_REG[0x67].RegType = REG_TYPE_DISCARD;
    pSENSOR_REG[0x67].RegValueDefault = 0x00;
    pSENSOR_REG[0x67].RegValueMask = 0xFF;

    // STAT_MAX
    pSENSOR_REG[0x68].RegType = REG_TYPE_DISCARD;
    pSENSOR_REG[0x68].RegValueDefault = 0x87;
    pSENSOR_REG[0x68].RegValueMask = 0xFF;

    // STAT_MEAN
    pSENSOR_REG[0x69].RegType = REG_TYPE_DISCARD;
    pSENSOR_REG[0x69].RegValueDefault = 0x40;
    pSENSOR_REG[0x69].RegValueMask = 0xFF;

    // POACF_CSR
    pSENSOR_REG[0x80].RegType = REG_TYPE_DISCARD;
    pSENSOR_REG[0x80].RegValueDefault = 0xC3;
    pSENSOR_REG[0x80].RegValueMask = 0xFF;
}

void vEvaluateImage(unsigned char* dirtydots_map, int* pWhiteDots, int* pDarkDots,
                    BOOL* pTooManyDirtyDots, int* pMaxConsecutive_dots, Image_Valid_Area* pScanArea,
                    unsigned char* pImage, unsigned char option, unsigned char white_pxl_th,
                    unsigned char dark_pxl_th) {
    int i, j;

    BOOL bTooManyDirtyDots;
    int WhiteDots;
    int DarkDots;
    int MaxConsecutiveDirtyDots;
    unsigned char* DirtyDots = NULL;
    int width, height;

    isensor_get_sensor_roi_size(&width, &height);

    DirtyDots = plat_alloc(width * height);
    WhiteDots = 0;
    DarkDots = 0;
    MaxConsecutiveDirtyDots = 0;
    bTooManyDirtyDots = FALSE;

    memset(DirtyDots, 0x00, width * height);

    for (i = pScanArea->row_start; i <= pScanArea->row_end; i++) {
        for (j = pScanArea->col_start; j <= pScanArea->col_end; j++) {
            if (option & TEST_WHITE_DOT) {
                if (pImage[i * width + j] > white_pxl_th) {
                    WhiteDots++;
                    DirtyDots[i * width + j] |= TEST_WHITE_DOT;
                }
            }

            if (option & TEST_DARK_DOT) {
                if (pImage[i * width + j] < dark_pxl_th) {
                    DarkDots++;
                    DirtyDots[i * width + j] |= TEST_DARK_DOT;
                }
            }
        }
    }

    // check consective dirty dots
    if (option & TEST_CONSECUTIVE_DOT) {
        for (i = 0; i < height; i++) {
            for (j = 0; j < width; j++) {
                if (DirtyDots[i * width + j]) {
                    int consective_test;

                    // '\' dots
                    consective_test = 1;
                    while (((i + consective_test) < height) && (j >= consective_test)) {
                        if (DirtyDots[(i + consective_test) * width + (j - consective_test)])
                            consective_test++;
                        else
                            break;
                    }
                    if (consective_test > MaxConsecutiveDirtyDots) {
                        MaxConsecutiveDirtyDots = consective_test;
                    }

                    // '|' dots
                    consective_test = 1;
                    while ((i + consective_test) < height) {
                        if (DirtyDots[(i + consective_test) * width + j]) {
                            consective_test++;
                        } else {
                            break;
                        }
                    }
                    if (consective_test > MaxConsecutiveDirtyDots) {
                        MaxConsecutiveDirtyDots = consective_test;
                    }

                    // '/' dots
                    consective_test = 1;
                    while (((i + consective_test) < height) && ((j + consective_test) < width)) {
                        if (DirtyDots[(i + consective_test) * width + (j + consective_test)]) {
                            consective_test++;
                        } else {
                            break;
                        }
                    }
                    if (consective_test > MaxConsecutiveDirtyDots) {
                        MaxConsecutiveDirtyDots = consective_test;
                    }

                    // '--' dots
                    consective_test = 1;
                    while ((j + consective_test) < width) {
                        if (DirtyDots[i * width + j + consective_test]) {
                            consective_test++;
                        } else {
                            break;
                        }
                    }
                    if (consective_test > MaxConsecutiveDirtyDots) {
                        MaxConsecutiveDirtyDots = consective_test;
                    }
                }
            }
        }

        if (MaxConsecutiveDirtyDots >= ConsecutiveDirtyDotsTh) {
            bTooManyDirtyDots = TRUE;
        }
    }

    if (option & TEST_DARK_DOT) {
        if (pDarkDots) {
            *pDarkDots = DarkDots;
        }
    }

    if (option & TEST_WHITE_DOT) {
        if (pWhiteDots) {
            *pWhiteDots = WhiteDots;
        }
    }

    if (option) {
        if (pTooManyDirtyDots) {
            *pTooManyDirtyDots = bTooManyDirtyDots;
        }
        if (pMaxConsecutive_dots) {
            *pMaxConsecutive_dots = MaxConsecutiveDirtyDots;
        }
    }

    if (dirtydots_map != NULL) combine_bad_map(dirtydots_map, DirtyDots, width * height);
    if (DirtyDots != NULL) {
        plat_free(DirtyDots);
    }
}

unsigned char** alloc_multi_sensor_frames(int nFrame) {
    int i, j;
    int width, height;
    unsigned char** ppAllFrames;

    ppAllFrames = (unsigned char**)plat_alloc(sizeof(unsigned char*) * nFrame);
    if (!ppAllFrames) {
        return NULL;
    }

    if (isensor_get_sensor_roi_size(&width, &height) != EGIS_OK) {
        plat_free(ppAllFrames);
        return NULL;
    }

    for (i = 0; i < nFrame; i++) {
        ppAllFrames[i] = (unsigned char*)plat_alloc(width * height);
        if (!ppAllFrames[i]) {
            for (j = 0; j < i; j++) {
                plat_free(ppAllFrames[j]);
            }
            plat_free(ppAllFrames);
            return NULL;
        }
    }
    return ppAllFrames;
}

void free_multi_sensor_frames(unsigned char** ppAllFrames, int nFrame) {
    int j;
    if (ppAllFrames == NULL) return;

    for (j = 0; j < nFrame; j++) {
        if (ppAllFrames[j] != NULL) plat_free(ppAllFrames[j]);
    }
    plat_free(ppAllFrames);
}

int get_multi_sensor_frames(unsigned char** ppAllFrames, int nFrame, UINT height, UINT width) {
    int i;
    if (ppAllFrames == NULL) return -1;

    for (i = 0; i < nFrame; i++) {
        if (isensor_get_dynamic_frame(ppAllFrames[i], height, width, 1) != EGIS_OK) {
            egislog_e("GetFrame fail, i=%d\n", i);
            return -2;
        }
    }
    return EGIS_OK;
}

//  Calculate average of multiple frames
//  Parameter
//    ppAllFrames: [input] The multiple sensor frames
//    nFrame: [input] Number of sensor frame
//    pAvgFrame: [input/output] The buffer should be allocated outside.
//  Return
//    EGIS_OK: Success
int calc_multi_frames_avg(unsigned char** ppAllFrames, int nFrame, unsigned char* pAvgFrame) {
    int i, j;
    int width, height;

    if (ppAllFrames == NULL) return -1;

    if (isensor_get_sensor_roi_size(&width, &height) != EGIS_OK) return EGIS_COMMAND_FAIL;

    // Average every pixel into output buffer
    for (i = 0; i < width * height; i++) {
        unsigned int pxl_sum = 0;
        for (j = 0; j < nFrame; j++) {
            pxl_sum += ppAllFrames[j][i];
        }
        pAvgFrame[i] = pxl_sum / nFrame;
    }
    return EGIS_OK;
}

//  Calculate standard deviation of multiple frames
//  Parameter
//    ppAllFrames: [input] The multiple sensor frames
//    nFrame: [input] Number of sensor frame
//    pAvgFrame: [input]
//    pStdDev: [out] The buffer should be allocated outside.
//  Return
//    EGIS_OK: Success
int calc_multi_frames_stddev_avg(unsigned char** ppAllFrames, int nFrame, unsigned char* pAvgFrame,
                                 float* avgStdDev) {
    int i, j;
    int width, height;
    if (ppAllFrames == NULL) return -1;

    if (isensor_get_sensor_roi_size(&width, &height) != EGIS_OK) {
        return EGIS_COMMAND_FAIL;
    }

    int totalPixel = width * height;
    double* pStdDev = plat_alloc(sizeof(double) * totalPixel);
    if (pStdDev == NULL) return EGIS_OUT_OF_MEMORY;
    // Average every pixel into output buffer
    for (i = 0; i < totalPixel; i++) {
        double pxl_diff;
        double sumDiff = 0;
        for (j = 0; j < nFrame; j++) {
            pxl_diff = (double)ppAllFrames[j][i] - (double)pAvgFrame[i];
            sumDiff += pxl_diff * pxl_diff;
        }
        pStdDev[i] = sqrt(sumDiff / nFrame);
    }

    // Calculate average
    float sum = 0.f;
    for (i = 0; i < totalPixel; i++) {
        sum += (float)pStdDev[i];
    }
    *avgStdDev = sum / totalPixel;

    plat_free(pStdDev);
    return EGIS_OK;
}

//  Adjust average of multiple frames according to white_pxl_th and dark_pxl_th
//  Parameter
//    ppAllFrames: [input] The multiple sensor frames
//    nFrame: [input] Number of sensor frame
//    pData: [input/output] The buffer should be allocated outside. The frame
//    for adjustment
//  Return
//    EGIS_OK: Success
int adjust_multi_frames_avg(unsigned char** ppAllFrames, int nFrame, unsigned char* pData,
                            unsigned char white_pxl_th, unsigned char dark_pxl_th) {
    int i, j;
    int width, height;
    if (ppAllFrames == NULL) return -1;

    if (isensor_get_sensor_roi_size(&width, &height) != EGIS_OK) {
        return EGIS_COMMAND_FAIL;
    }

    for (i = 0; i < width * height; i++) {
        unsigned char pxl_avg = pData[i];

        if (pxl_avg > white_pxl_th) {
            unsigned char pxl_min = 0xFF;

            for (j = 0; j < nFrame; j++) {
                if (ppAllFrames[j][i] < pxl_min) {
                    pxl_min = ppAllFrames[j][i];
                }
            }
            pData[i] = pxl_min;
        }

        if (pxl_avg < dark_pxl_th) {
            unsigned char pxl_max = 0x00;
            for (j = 0; j < nFrame; j++) {
                if (ppAllFrames[j][i] > pxl_max) {
                    pxl_max = ppAllFrames[j][i];
                }
            }
            pData[i] = pxl_max;
        }
    }
    return EGIS_OK;
}

int img_range(int data) {
    return data < 0 ? 0 : (data > 0xff ? 0xff : data);
}
int set_black_reg() {
    unsigned char addr;
    unsigned char buf[256];

    // Start Addr = VREF_SEL
    addr = VREF_SEL_ET5XX_ADDR;

    memset(buf, 0x00, sizeof(256));

    buf[0] = 0x00;  // min vref selection
    buf[1] = 0x0F;  // max dc_offset
    buf[2] = 0x00;  // min cds_offset
    buf[3] = 0x00;  // max dc_f_g
    buf[4] = 0x00;  // select dc_f from dc_f_g
    buf[5] = 0x00;  // min pga gain
    buf[6] = 0x72;  // 0X13 =GAIN2_EN on, SS Mode
    egislog_i("set_black_reg GAIN2 enable");

    return io_5xx_dispatch_write_burst_register(addr, 7, buf);
}

int set_white_reg() {
    unsigned char addr;
    unsigned char buf[256];

    // Start Addr = VREF_SEL
    addr = VREF_SEL_ET5XX_ADDR;

    memset(buf, 0x00, sizeof(256));

    buf[0] = 0x0F;  // max vref selection
    buf[1] = 0x00;  // min dc_offset
    buf[2] = 0x3F;  // max cds_offset
    buf[3] = 0x3F;  // max dc_f_g
    buf[4] = 0x00;  // select dc_f from dc_f_g
    buf[5] = 0x0F;  // max pga gain
    buf[6] = 0x72;  // 0X13 =GAIN2_EN on, SS Mode
    egislog_i("set_white_reg GAIN2 enable");
    return io_5xx_dispatch_write_burst_register(addr, 7, buf);
}

static void combine_bad_map(BYTE* map1, BYTE* map2, int frame_size) {
    int i;
    for (i = 0; i < frame_size; i++) map1[i] |= map2[i];
}
