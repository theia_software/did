#include <limits.h>

#include "common_definition.h"
#include "algomodule.h"
#include "egis_definition.h"
#include "fp_algomodule.h"
#include "plat_log.h"
#include "plat_mem.h"
#include "template_file.h"
#include "template_manager.h"

#define LOG_TAG "RBS-TEMPL"

//#define TEMPL_DEFAULT_MAX_COUNT 5
#define TEMPL_DEFAULT_MAX_COUNT SUPPORT_MAX_ENROLL_COUNT
static int g_max_count = 0;
static int g_enrolled_count = 0;
uint32_t g_group_id = 0;

static uint32_t* g_index_to_fid = NULL;
#define DEFAULT_INVALID_FID 0

static int* g_index_to_file_idx = NULL;
#define DEFAULT_FILE_IDX -1

static BOOL g_keep_idx_array_value_after_deleteall = FALSE;

int _set_new_file_index(int index) {
    int file_index;
    int i;
    BOOL file_index_availabe;

    for (file_index = 0; file_index < g_max_count; file_index++) {
        file_index_availabe = TRUE;
        for (i = 0; i < g_max_count; i++) {
            if (g_index_to_file_idx[i] == file_index) {
                // file_index has existed
                file_index_availabe = FALSE;
                break;
            }
        }

        if (file_index_availabe) {
            g_index_to_file_idx[index] = file_index;
            return file_index;
        }
    }

    return DEFAULT_FILE_IDX;
}

static int g_templ_algo_version = 0;
uint32_t template_get_version() {
#if defined(ALGO_GEN_4)
    int templ_algo_version;
    algo_get_int(ALGO_PARAM_TYPE_TEMPLATE_VERSION, &templ_algo_version);
    if (g_templ_algo_version != templ_algo_version) {
        // Not a real error. Just highlight the log
        ex_log(LOG_ERROR, "First - templ_algo_version %d -> %d", g_templ_algo_version,
               templ_algo_version);
    }
    g_templ_algo_version = templ_algo_version;
    return (TEMPL_VER_G4_00 + g_templ_algo_version);
#else
    return (TEMPL_VER_01 + g_templ_algo_version);
#endif
}
int template_set_active_group(uint32_t group_id, const char* path, const char* prefix) {
    int ret = _template_set_file_path_prefix(path, prefix);
    if (ret != EGIS_OK) {
        return ret;
    }

    g_group_id = group_id;
    return EGIS_OK;
}

int template_set_max_count(int max_count) {
    int i;

    if (g_max_count != 0) {
        egislog_e("%s g_max_count is changed. %d -> %d", __func__, g_max_count, max_count);
    }

    if (max_count > 0 && max_count < 20) {
        g_max_count = max_count;
        CREAT_ARRAY(g_index_to_fid, uint32_t, DEFAULT_INVALID_FID, max_count);
        CREAT_ARRAY(g_index_to_file_idx, int, DEFAULT_FILE_IDX, max_count);
        return EGIS_OK;
    } else {
        return TEMPL_ERROR_INVALID_MAX_COUNT;
    }
}

int template_add(uint32_t fid, unsigned char* buf, unsigned int size) {
    if (g_enrolled_count >= g_max_count) {
        egislog_e("%s db is full (%d)", __func__, g_enrolled_count);
        return TEMPL_ERROR_DB_FULL;
    }

    int index = g_enrolled_count;
    if (fid == 0) {
        egislog_e("%s fid is 0", __func__);
        return TEMPL_ERROR_PARAM;
    } else if (g_index_to_fid[index] != 0) {
        egislog_e("%s index_to_fid[%d] is %d (not zero)", __func__, index, g_index_to_fid[index]);
        return TEMPL_ERROR_PARAM;
    }

    int retSize = _template_save_file(_set_new_file_index(index), fid, buf, size);
    if (retSize > 0) {
        g_index_to_fid[index] = fid;
        g_enrolled_count++;
        egislog_d("%s total enrolled=%d", __func__, g_enrolled_count);
        return EGIS_OK;
    }

    return TEMPL_ERROR_SAVE;
}

int template_update(int index, unsigned char* buf, unsigned int size) {
    if (index < 0 || index >= g_enrolled_count) {
        egislog_e("%s wrong index %d", __func__, index);
        return TEMPL_ERROR_INVALID_INDEX;
    }

    uint32_t fid = g_index_to_fid[index];
    if (fid == 0) {
        egislog_e("%s fid is 0", __func__);
        return TEMPL_ERROR_INVALID_INDEX;
    }

    int retSize = _template_save_file(g_index_to_file_idx[index], fid, buf, size);
    if (retSize > 0) {
        return EGIS_OK;
    }

    egislog_e("%s [%d](%d) failed", __func__, index, size);
    return TEMPL_ERROR_SAVE;
}

static int _delete_all(BOOL also_delete_file) {
    int i;
    int ret = EGIS_OK;
    BOOL reset_idx_array = also_delete_file;

    for (i = 0; i < g_max_count; i++) {
        ret = fp_deleteTemplate(i);
        if (ret != FP_LIB_OK) {
            egislog_e("%s [%d] failed", __func__, i);
        }

        if (also_delete_file) {
            egislog_d("%s delete [%d]", __func__, i);
            _template_delete_file(i);
        }

        if (reset_idx_array) {
            g_index_to_fid[i] = DEFAULT_INVALID_FID;
            g_index_to_file_idx[i] = DEFAULT_FILE_IDX;
        }
    }

    if (reset_idx_array) {
        g_enrolled_count = 0;
    } else {
        g_keep_idx_array_value_after_deleteall = TRUE;
    }
    return EGIS_OK;
}

int template_delete(uint32_t fid) {
    int i;
    int ret = EGIS_OK;

    for (i = 0; i < g_enrolled_count; i++) {
        if (g_index_to_fid[i] == fid) {
            egislog_d("%s delete [%d] fid %d", __func__, i, fid);
            ret = fp_deleteTemplate(i);

            _template_delete_file(g_index_to_file_idx[i]);
            g_index_to_file_idx[i] = DEFAULT_FILE_IDX;

            return EGIS_OK;
        }
    }

    egislog_e("%s no such fid %d", __func__, fid);
    return TEMPL_ERROR_PARAM;
}

int template_delete_all(BOOL also_delete_file) {
    egislog_d("%s, delete file (%d)", __func__, also_delete_file);
    return _delete_all(also_delete_file);
}

int template_load_all(uint32_t* fid_list, int* enrolled_count) {
    int i, ret;
    unsigned int real_size;
    fp_lib_template_t templ;
    template_file_data* file_data;

    if (g_max_count == 0) {
        template_set_max_count(TEMPL_DEFAULT_MAX_COUNT);
    }
    file_data = plat_alloc(sizeof(template_file_data));
    RBS_CHECK_IF_NULL(file_data, EGIS_OUT_OF_MEMORY);

    _delete_all(FALSE);
    g_enrolled_count = 0;
    g_keep_idx_array_value_after_deleteall = FALSE;

    for (i = 0; i < g_max_count; i++) {
        real_size = 0;
        g_index_to_fid[i] = DEFAULT_INVALID_FID;
        g_index_to_file_idx[i] = DEFAULT_FILE_IDX;

        ret = _template_load_file(i, (unsigned char*)file_data, sizeof(template_file_data),
                                  &real_size);
        if (ret < 0) continue;
        if (real_size > 4 * sizeof(uint32_t)) {
            if (file_data->version == TEMPL_VER_CURRENT) {
                templ.tpl = file_data->templ_data;
                templ.size = file_data->templ_data_size;
                g_index_to_fid[g_enrolled_count] = file_data->fingerprint_id;
                g_index_to_file_idx[g_enrolled_count] = i;

                egislog_d("%s [%d] %d, size=%d", __func__, g_enrolled_count,
                          g_index_to_fid[g_enrolled_count], file_data->templ_data_size);

                if (fid_list) {
                    fid_list[g_enrolled_count] = file_data->fingerprint_id;
                }
                // g_enrolled_count will be increased after
                // fp_unpackTemplate()
                fp_unpackTemplate(&templ, &g_enrolled_count);
            } else {
                egislog_e("%s unsupported version 0x%X (0x%X)", __func__, file_data->version,
                          TEMPL_VER_CURRENT);
            }
        }
    }

    egislog_d("%s total enrolled=%d", __func__, g_enrolled_count);
    if (enrolled_count) {
        *enrolled_count = g_enrolled_count;
    }
    PLAT_FREE(file_data);
    return EGIS_OK;
}

uint32_t template_get_fid(int index) {
    if (index < 0 || index >= g_enrolled_count) {
        egislog_e("%s wrong index %d", __func__, index);
        return TEMPL_ERROR_INVALID_INDEX;
    }

    return g_index_to_fid[index];
}

int template_get_enrolled_count() {
    return g_enrolled_count;
}

int template_uninit() {
    egislog_d("%s ", __func__);

    int ret = _delete_all(FALSE);
    if (ret == EGIS_OK) {
        PLAT_FREE(g_index_to_fid);
        PLAT_FREE(g_index_to_file_idx);
    }

    return ret;
}

int fplib_template_create(fp_lib_template_t* fplib_template) {
    RBS_CHECK_IF_NULL(fplib_template, EGIS_INCORRECT_PARAMETER);

    egislog_d("%s ", __func__);
    PLAT_FREE(fplib_template->tpl);
    fplib_template->tpl = plat_alloc(MAX_ENROLL_TEMPLATE_LEN);
    fplib_template->size = MAX_ENROLL_TEMPLATE_LEN;

    return EGIS_OK;
}

void fplib_template_destroy(fp_lib_template_t* fplib_template) {
    if (fplib_template == NULL) {
        return;
    }
    egislog_d("%s ", __func__);

    PLAT_FREE(fplib_template->tpl);
    fplib_template->size = 0;
}
