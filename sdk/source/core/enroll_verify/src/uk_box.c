#include "uk_box.h"
#include "core_config.h"
#include "egis_definition.h"
#include "egis_log.h"
#include "ini_definition.h"
#include "isensor_definition.h"
#include "plat_mem.h"
#include "type_definition.h"

#define LOG_TAG "RBS UK"

int uk_box_create(int width, int height, int bpp, int max_bkg_num, uk_box_info_t* uk_info) {
    egislog_d("create_uk_box width %d. height %d, bpp %d", width, height, bpp);
    RBS_CHECK_IF_NULL(uk_info, EGIS_INCORRECT_PARAMETER);
    if (width <= 0 || height <= 0 || bpp <= 0) {
        egislog_e("invalid parameters %d %d %d", width, height, bpp);
        return EGIS_INCORRECT_PARAMETER;
    }

    if (uk_info->data != NULL) {
        uk_box_destroy(uk_info);
    }

    int total_data_size = width * height * 4 + max_bkg_num * width * height * bpp / 8;

    uk_info->width = width;
    uk_info->height = height;
    uk_info->bpp = bpp;
    uk_info->cur_bkg_num = 0;
    uk_info->max_bkg_num = max_bkg_num;
    uk_info->gen_bkg_complete = FALSE;
    uk_info->data = plat_alloc(total_data_size);
    return EGIS_OK;
}

static void uk_box_generate_bkg(uk_box_info_t* uk_info) {
    egislog_d("uk_box_generate_bkg");
    int width = uk_info->width;
    int height = uk_info->height;
    int bpp = uk_info->bpp;
    uint32_t* ptr_avg_data_32 = uk_info->data;
    for (int i = 0; i < width * height; i++) {
        *(ptr_avg_data_32 + i) = (*(ptr_avg_data_32 + i) / uk_info->cur_bkg_num);
    }
    uk_info->gen_bkg_complete = TRUE;
}

int uk_box_add_image(uk_box_info_t* uk_info, void* data) {
    egislog_d("uk_box_add_image");
    RBS_CHECK_IF_NULL(data, EGIS_INCORRECT_PARAMETER);
    RBS_CHECK_IF_NULL(uk_info, EGIS_INCORRECT_PARAMETER);

    unsigned char* ptr_raw = uk_box_get_raw_image_ptr(uk_info, uk_info->cur_bkg_num);
    RBS_CHECK_IF_NULL(ptr_raw, EGIS_NULL_POINTER);
    int img_size = uk_box_get_bkg_size(uk_info);
    memcpy(ptr_raw, data, img_size);
    uk_info->cur_bkg_num++;

    uint32_t* ptr_avg_data_32 = uk_info->data;
    int pixel_number = uk_info->width * uk_info->height;
    switch (uk_info->bpp) {
        case 8: {
            uint8_t* tempdata = (uint8_t*)data;
            for (int i = 0; i < pixel_number; i++) {
                *(ptr_avg_data_32 + i) += *(tempdata + i);
            }
            break;
        }
        case 16: {
            uint16_t* tempdata = (uint16_t*)data;
            for (int i = 0; i < pixel_number; i++) {
                *(ptr_avg_data_32 + i) += *(tempdata + i);
            }
            break;
        }
        default:
            egislog_e("bkg_box_add_image bpp %d not support", uk_info->bpp);
            break;
    }
    if (uk_info->cur_bkg_num == uk_info->max_bkg_num) {
        uk_box_generate_bkg(uk_info);
    }
    return EGIS_OK;
}

int uk_box_get_bkg(uk_box_info_t* uk_info, void* out_buffer, int out_buffer_len) {
    RBS_CHECK_IF_NULL(out_buffer, EGIS_INCORRECT_PARAMETER);
    RBS_CHECK_IF_NULL(uk_info, EGIS_INCORRECT_PARAMETER);
    if (!uk_info->gen_bkg_complete) {
        return EGIS_INCORRECT_STATUS;
    }
    int pixel_number = uk_info->width * uk_info->height;
    int bpp = uk_info->bpp;
    int bkg_num = uk_info->cur_bkg_num;
    if (out_buffer_len < pixel_number * bpp / 8) {
        egislog_e("out_buffer size < bkg size", out_buffer_len);
        return EGIS_INCORRECT_PARAMETER;
    }
    egislog_d("uk_box_get_bkg cur_bkg_num = %d", bkg_num);
    uint32_t* ptr_avg_data_32 = uk_info->data;
    int ret;
    switch (bpp) {
        case 8: {
            uint8_t* tempdata = (uint8_t*)out_buffer;
            for (int i = 0; i < pixel_number; i++) {
                *(tempdata + i) = *(ptr_avg_data_32 + i);
            }
            ret = EGIS_OK;
            break;
        }
        case 16: {
            uint16_t* tempdata = (uint16_t*)out_buffer;
            for (int i = 0; i < pixel_number; i++) {
                *(tempdata + i) = *(ptr_avg_data_32 + i);
            }
            ret = EGIS_OK;
            break;
        }
        default:
            egislog_e("uk_box_get_bkg bpp %d not support", bpp);
            return EGIS_INCORRECT_PARAMETER;
    }
    return ret;
}

int uk_box_get_bkg_size(uk_box_info_t* uk_box_info) {
    RBS_CHECK_IF_NULL(uk_box_info, -1);
    return uk_box_info->width * uk_box_info->height * uk_box_info->bpp / 8;
}

int uk_box_destroy(uk_box_info_t* uk_box_info) {
    RBS_CHECK_IF_NULL(uk_box_info, EGIS_INCORRECT_PARAMETER);
    plat_free(uk_box_info->data);
    memset(uk_box_info, 0, sizeof(uk_box_info_t));
    return EGIS_OK;
}

unsigned char* uk_box_get_raw_image_ptr(uk_box_info_t* uk_box_info, int index) {
    if (uk_box_info == NULL) {
        return NULL;
    }

    if (index >= uk_box_info->max_bkg_num) {
        egislog_e("index out of range max_bkg_num = %d, index = %d", uk_box_info->max_bkg_num,
                  index);
        return NULL;
    }

    unsigned char* temp_ptr = (unsigned char*)uk_box_info->data;
    int uk_data_size = uk_box_info->width * uk_box_info->height * 4;
    int img_size = uk_box_info->width * uk_box_info->height * uk_box_info->bpp / 8;
    return temp_ptr + uk_data_size + index * img_size;
}
