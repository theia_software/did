#ifdef ENROLL_POLICY_Y6
#include "EgisAlgorithmAPI.h"
#include "common_definition.h"
#include "egis_log.h"

#define LOG_TAG "RBS"

static int g_redundant_enroll = 0;
static int g_redundant_total = 0;

#define REDUNDANT_CHECK_BEGIN 3
#define REDUNDANT_CHECK_END 8
#define REDUNDANT_CHECK_RETRY 2
#define REDUNDANT_CHECK_TOTAL \
    ((REDUNDANT_CHECK_END - REDUNDANT_CHECK_BEGIN + 1) * REDUNDANT_CHECK_RETRY)

void enroll_policy_init(int enroll_mode, void* enroll_context) {
    switch (enroll_mode) {
        case ENROLL_METHOD_TOUCH: {
            set_enroll_context(enroll_context, ENROLL_CTX_REDUNDANT_IMAGE_POLICY,
                               FP_REDUNDANT_POLICY_REJECT_IMAGE);
        } break;
        default:
            break;
    }

    g_redundant_total = 0;
    g_redundant_enroll = 0;
}

void enroll_policy_update(void* enroll_context, int enroll_result, int enrolled_cnt,
                          int max_enroll_cnt) {
    int effect_enroll_input = 0;
    int policy = 0;
    int continue_reject = -1;

    if (enroll_result == FP_MERGE_ENROLL_REDUNDANT_INPUT) {
        ++g_redundant_enroll;
        ++g_redundant_total;
        egislog_d("egis_alg_enroll ENROLL_REDUNDANT_INPUT (%d) (%d)", g_redundant_enroll,
                  g_redundant_total);
    } else if (enroll_result == FP_MERGE_ENROLL_REDUNDANT_ACCEPT) {
        egislog_d("egis_alg_enroll ENROLL_REDUNDANT_ACCEPT");
        g_redundant_enroll = 0;
    } else {
        g_redundant_enroll = 0;
    }

    if ((enrolled_cnt + 1 < REDUNDANT_CHECK_BEGIN) || (enrolled_cnt + 1 > REDUNDANT_CHECK_END)) {
        egislog_v("polcy debug 0");
        policy = FP_REDUNDANT_POLICY_ACCPET_IMAGE;
    } else {
        if ((g_redundant_enroll + 1) > REDUNDANT_CHECK_RETRY) {
            policy = FP_REDUNDANT_POLICY_ACCPET_IMAGE;
            g_redundant_enroll = 0;
            egislog_v("polcy debug 1");
        } else {
            policy = FP_REDUNDANT_POLICY_REJECT_IMAGE;
            egislog_v("polcy debug 2");

            if ((g_redundant_total + 1) > 6 /*REDUNDANT_CHECK_TOTAL*/) {
                egislog_v("polcy debug 4");
                policy = FP_REDUNDANT_POLICY_ACCPET_IMAGE;
            }
        }
    }

    egislog_d("next enroll policy = %s", policy == FP_REDUNDANT_POLICY_ACCPET_IMAGE
                                             ? "FP_REDUNDANT_POLICY_ACCPET_IMAGE"
                                             : "FP_REDUNDANT_POLICY_REJECT_IMAGE");

    set_enroll_context(enroll_context, ENROLL_CTX_REDUNDANT_IMAGE_POLICY, policy);

    /*
     *	For debug
     */
    effect_enroll_input = enrolled_cnt + g_redundant_enroll;
    egislog_d("effect_enroll_input = %d", effect_enroll_input);
}

#endif
