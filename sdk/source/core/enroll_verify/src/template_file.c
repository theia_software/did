#include <limits.h>
#include <string.h>

#include "egis_definition.h"
#include "egis_sprintf.h"
#include "plat_file.h"
#include "plat_log.h"
#include "plat_mem.h"
#include "template_file.h"
#include "template_manager.h"

#define LOG_TAG "RBS-TEMPL"

#ifdef _MSC_VER
#ifndef PATH_MAX
#define PATH_MAX MAX_PATH
#endif
#endif
static char g_template_path_prefix[PATH_MAX] = {"/data/misc/vendor/egistec/template"};

int _template_set_file_path_prefix(const char* path, const char* prefix) {
    if (path != NULL && prefix != NULL) {
        // TODO: Check permission on the path
        if (strlen(path) + strlen(prefix) > PATH_MAX - 4) {
            return TEMPL_ERROR_INVALID_PATH;
        } else {
            plat_set_template_path_prefix(g_template_path_prefix, PATH_MAX, path, prefix);
            egislog_d("%s path_prefix=%s", __func__, g_template_path_prefix);
        }
        return EGIS_OK;
    }

    return TEMPL_ERROR_INVALID_PATH;
}

static template_custom_save_file g_fn_save = NULL;
static template_custom_load_file g_fn_load = NULL;
static template_custom_remove_file g_fn_remove = NULL;

void template_set_custom_file_routin(template_custom_save_file fn_save,
                                     template_custom_load_file fn_load,
                                     template_custom_remove_file fn_remove) {
    egislog_d("%s", __func__);
    g_fn_save = fn_save;
    g_fn_load = fn_load;
    g_fn_remove = fn_remove;
}

int _template_load_file(int file_index, unsigned char* buf, unsigned int size,
                        unsigned int* real_size) {
    char path[PATH_MAX] = {'\0'};
    if (file_index < 0 || !buf || size == 0) {
        egislog_e("%s [%d] buf %d  (%p)", __func__, file_index, size, buf);
        return TEMPL_ERROR_PARAM;
    }

    int ret = 0;
    if (g_fn_load) {
        ret = g_fn_load(file_index, buf, size, real_size);
    } else {
        get_app_ctx_path(path, PATH_MAX, g_template_path_prefix, file_index);
        ret = plat_load_file(path, buf, size, real_size);
    }

    egislog_d("%s ret=%d", __func__, ret);
    return ret;
}

int _template_save_file(int file_index, uint32_t fid, unsigned char* buf, unsigned int size) {
    char path[PATH_MAX] = {'\0'};

    if (file_index < 0 || !buf || size == 0) {
        egislog_e("%s [%d] buf %d  (%p)", __func__, file_index, size, buf);
        return TEMPL_ERROR_PARAM;
    }

    if (size > MAX_ENROLL_TEMPLATE_LEN) {
        egislog_e("%s buf size is too big. %d > %d", __func__, size, MAX_ENROLL_TEMPLATE_LEN);
        return TEMPL_ERROR_PARAM;
    }
    egislog_d("%s [%d] %d, size=%d", __func__, file_index, fid, size);

    template_file_data* file_data = plat_alloc(sizeof(template_file_data));
    RBS_CHECK_IF_NULL(file_data, EGIS_OUT_OF_MEMORY);

    file_data->version = TEMPL_VER_CURRENT;
    file_data->user_id = 0;
    file_data->fingerprint_id = fid;
    file_data->templ_data_size = size;
    memcpy(file_data->templ_data, buf, size);

    int ret = 0;
    if (g_fn_save) {
        ret = g_fn_save(file_index, (unsigned char*)file_data, sizeof(template_file_data));
    } else {
        get_app_ctx_path(path, PATH_MAX, g_template_path_prefix, file_index);
        ret = plat_save_file(path, (unsigned char*)file_data, sizeof(template_file_data));
    }

    if (ret > 0) {
        egislog_d("%s ret=%d", __func__, ret);
    } else {
        egislog_e("%s failed !!! ret=%d", __func__, ret);
    }
    PLAT_FREE(file_data);
    return ret;
}

int _template_delete_file(int file_index) {
    char path[PATH_MAX] = {'\0'};
    if (file_index < 0) {
        egislog_e("%s file_index is wrong %d]", __func__, file_index);
        return TEMPL_ERROR_PARAM;
    }

    int ret = 0;
    if (g_fn_remove) {
        ret = g_fn_remove(file_index);
    } else {
        get_app_ctx_path(path, PATH_MAX, g_template_path_prefix, file_index);
        ret = plat_remove_file(path);
    }

    egislog_d("%s ret=%d", __func__, ret);
    return ret;
}
