
#include "matcher_feature.h"
#include "EgisAlgorithmAPI.h"
#include "egis_definition.h"
#include "plat_log.h"
#include "plat_mem.h"
#include "plat_time.h"

#define LOG_TAG "RBS-MFEAT"

static int g_eqty = 0;
static matcher_feature_t g_mfeat = {0, NULL, 0, 0, 0, 0};
static BOOL g_feat_is_valid;
int mfeature_create() {
    mfeature_destroy();

#ifdef ALGO_GEN_5
    return EGIS_OK;
#else
    g_mfeat.feat_buf = plat_alloc(MAX_FEATURE_LEN);
    RBS_CHECK_IF_NULL(g_mfeat.feat_buf, EGIS_OUT_OF_MEMORY);
    egislog_d("%s, done", __func__);
    return EGIS_OK;
#endif
}

void mfeature_destroy() {
    PLAT_FREE(g_mfeat.feat_buf);
    memset(&g_mfeat, 0, sizeof(g_mfeat));
    g_feat_is_valid = FALSE;
}

static BOOL g_has_run_check_residue = FALSE;
#ifdef ALGO_GEN_5
int mfeature_extract(int try_index, unsigned char* img, int width, int height) {
    int retval =
        extract_feature(img, width, height, g_mfeat.feat_buf, &g_mfeat.feat_length, &g_mfeat.eqty);
    g_mfeat.extract_ret = exOK;
    g_mfeat.eqty = 20;
    g_mfeat.is_fake = 0;
    g_feat_is_valid = TRUE;
    return exOK;
}
#else
int mfeature_extract(int try_index, unsigned char* img, int width, int height) {
    int retval;
    RBS_CHECK_IF_NULL(g_mfeat.feat_buf, EGIS_INCORRECT_STATUS);
    if (g_feat_is_valid) {
        egislog_e("%s [%d], again", __func__, try_index);
    }

    if (try_index == 0) {
        g_has_run_check_residue = FALSE;
    }

    egislog_d("%s, entry", __func__);
    g_mfeat.feat_length = MAX_FEATURE_LEN;
    TIME_MEASURE_START(extract_feature);
    retval =
        extract_feature(img, width, height, g_mfeat.feat_buf, &g_mfeat.feat_length, &g_mfeat.eqty);
    TIME_MEASURE_STOP(extract_feature, "extract_feature");
    g_mfeat.extract_ret = retval;

    g_feat_is_valid = (retval == exOK) ? TRUE : FALSE;
#ifdef ALGO_GEN_4
    g_mfeat.eqty = 20;
    g_mfeat.is_fake = 0;
    g_feat_is_valid = TRUE;
    switch (g_mfeat.extract_ret) {
        case exOK:
            break;
        case exFakeFinger:
            g_mfeat.is_fake = 1;
            ex_log(LOG_DEBUG, "%s, exFakeFinger", __func__);
            break;
        case exParital:
            ex_log(LOG_DEBUG, "%s, exParital", __func__);
            break;
        case exFail:
            g_feat_is_valid = FALSE;
            ex_log(LOG_ERROR, "%s, exFail", __func__);
            break;
        default:
            ex_log(LOG_ERROR, "%s, result is unknown.", __func__);
            break;
    }
    if (g_mfeat.extract_ret != exFail) {
        g_mfeat.extract_ret = exOK;
    }
#endif

#if defined(ALGO_GEN_7)
    g_mfeat.eqty = 20;
#endif

#ifdef G3PLUS_MATCHER
    if (!g_has_run_check_residue && retval == exOK) {
        g_mfeat.residual = api_check_residue(g_mfeat.feat_buf, g_mfeat.feat_length);
        g_has_run_check_residue = TRUE;
        egislog_d("check_residual ret= %d", g_mfeat.residual);
    }
#endif
    return retval;
}
#endif

int mfeature_get_result(int try_index, enum mfeature_result_id result_id) {
    switch (result_id) {
        case MFEAT_FEATURE_LENGTH:
            return g_mfeat.feat_length;
        case MFEAT_FEATURE_EXTRACT_RET:
            return g_mfeat.extract_ret;
        case MFEAT_RESULT_EQTY:
            return g_mfeat.eqty;
        case MFEAT_RESULT_RESIDUAL:
            return g_mfeat.residual;
        case MFEAT_RESULT_IS_FAKE:
            return g_mfeat.is_fake;
        default:
            return 0;
    }
}

unsigned char* mfeature_get_feature_buf(int try_indx) {
    if (!g_feat_is_valid) {
        egislog_e("%s, feat buf is invalid", __func__);
        memset(g_mfeat.feat_buf, 0, MAX_FEATURE_LEN);
        g_mfeat.feat_length = 0;
    }
    g_feat_is_valid = FALSE;
    return g_mfeat.feat_buf;
}
