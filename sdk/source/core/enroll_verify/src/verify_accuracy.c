#include <stdint.h>
#include "plat_log.h"
#include "verify_accuracy.h"
#include "plat_time.h"



#if defined(VENDOR_LGE)
#define ALGO_EASY_MODE
#endif

#if defined(VENDOR_HW)
#define ALGO_EASY_MODE
#define EASY_MODE_1_MIN_TIME_INTERVAL (10 * 1000) // 10 sec
#define EASY_MODE_1_MAX_TIME_INTERVAL (60000 * 2)  // 2 mins
#define EASY_MODE_MAX_FINGER_ON_COUNT 200
#define EASY_MODE_MIN_FINGER_ON_COUNT 0
#define EASY_MODE_VERIFY_COUNT_PER_FINGER 50
#define TRIGGER_EASY_MODE_1_MIN_INTEVAL (6 * 1000)  // 6 sec
#define TRIGGER_EASY_MODE_1_MAX_INTEVAL (1000 *1000) // 1000 sec
#define FAR_RATIO_EASY_MODE_1 (1 * 1000)  // 1K
#else
#define EASY_MODE_1_MIN_TIME_INTERVAL 6 * 1000 // 6 sec
#define EASY_MODE_1_MAX_TIME_INTERVAL (60000 * 2)  // 2 mins
#define EASY_MODE_MAX_FINGER_ON_COUNT 900
#define EASY_MODE_MIN_FINGER_ON_COUNT 400
#define EASY_MODE_VERIFY_COUNT_PER_FINGER 100
#define TRIGGER_EASY_MODE_1_MIN_INTEVAL (60000 * 2)  // 120 sec
#define TRIGGER_EASY_MODE_1_MAX_INTEVAL (1000 *1000) // 1000 sec
#define FAR_RATIO_EASY_MODE_1 (0.5 * 1000) // 0.5k
#endif

#define EASY_MODE_1_TIME_OUT (60000 * 10)  // 10 mins
#define EASY_MODE_2_TIME_INTERVAL (60000 * 1)  // 1 mins

#define TIME_1_HOUR_IN_MS (1 * 60 * 60 * 1000)  // 1 hr
#define FAR_RATIO_EASY_MODE_1_STAGE_2 (10 * 1000) // 10k
#define FAR_RATIO_EASY_MODE_2  (10 * 1000)  // 10k



static uint32_t g_app_level = 0;
BOOL g_is_wash_hand = FALSE;
static int g_finger_on_count = 0;
static int g_easy_mode_count = 0;
static int g_max_finger_on_count = EASY_MODE_MAX_FINGER_ON_COUNT;

static unsigned long long g_previous_finger_on_timestamp = 0;
static unsigned long long g_current_finger_on_timestamp = (unsigned long long)-1; /*Maximum value*/

static unsigned long long g_last_enroll_timestamp = 0;
static easy_mode_t g_cur_easy_mode = EASY_MODE_DONE;

static BOOL accept_turn_on_easy_mode(easy_mode_t easyMode) {
    unsigned long long diff_finger_on_timestamp =
            g_current_finger_on_timestamp - g_previous_finger_on_timestamp;
    if (easyMode == EASY_MODE_OFF && diff_finger_on_timestamp > TRIGGER_EASY_MODE_1_MIN_INTEVAL
        && diff_finger_on_timestamp <
           TRIGGER_EASY_MODE_1_MAX_INTEVAL) { // (HW)if EASY_MODE_OFF & > 6 sec & < 1000 sec
        return TRUE;
    } else if ((easyMode == EASY_MODE_ON || easyMode == EASY_MODE_ON_STAGE_2) &&
               // if EASY_MODE_ON & 10~120 sec
               (diff_finger_on_timestamp >= EASY_MODE_1_MIN_TIME_INTERVAL &&
                diff_finger_on_timestamp <= EASY_MODE_1_MAX_TIME_INTERVAL)) {
        return TRUE;
    } else {
        return FALSE;
    }
}


void easy_mode_reset() {
    ex_log(LOG_DEBUG, "reset em");
    g_easy_mode_count = 0;
    g_cur_easy_mode = EASY_MODE_OFF;
    g_finger_on_count = 0;
    g_last_enroll_timestamp = plat_get_time();
#if defined(VENDOR_HW)
    g_current_finger_on_timestamp = plat_get_time(); // fix HW restart failed
#endif
}

int easy_mode_do_update(int try_count,BOOL is_FA_attack , int verify_enroll_number, int temperature) {
    if (try_count != 0) {
        return EASY_MODE_UPDATE_OK;
    }

    g_previous_finger_on_timestamp = g_current_finger_on_timestamp;
    g_current_finger_on_timestamp = plat_get_time();

    g_finger_on_count++;

#if defined(VENDOR_HW)
    ex_log(LOG_DEBUG, "g_is_wash_hand = %d", g_is_wash_hand);
    if (g_is_wash_hand) {
        g_max_finger_on_count = EASY_MODE_VERIFY_COUNT_PER_FINGER * verify_enroll_number;
    } else {
        g_cur_easy_mode = EASY_MODE_OFF;
        return EASY_MODE_UPDATE_OK;
    }
#else
    g_max_finger_on_count = EASY_MODE_MAX_FINGER_ON_COUNT;
#endif

#ifdef ALGO_EASY_MODE
    if (g_last_enroll_timestamp == 0 || plat_get_diff_time(g_last_enroll_timestamp) > TIME_1_HOUR_IN_MS) { // if > 1 hours, set EASY_MODE_DONE
        g_cur_easy_mode = EASY_MODE_DONE;
        ex_log(LOG_DEBUG, "em Timestamp = %d", plat_get_diff_time(g_last_enroll_timestamp));
        return EASY_MODE_UPDATE_OK;
    }

    unsigned long long diff_finger_on = g_current_finger_on_timestamp - g_previous_finger_on_timestamp;
    g_cur_easy_mode = is_FA_attack ? EASY_MODE_DONE: g_cur_easy_mode;  // if Fa_attack, set EASY_MODE_DONE

    if (g_finger_on_count <= g_max_finger_on_count &&
        g_finger_on_count > EASY_MODE_MIN_FINGER_ON_COUNT) {
        switch (g_cur_easy_mode) {
            case EASY_MODE_DONE:
                g_finger_on_count = 0;
                break;
            case EASY_MODE_OFF:
                if (accept_turn_on_easy_mode(g_cur_easy_mode)) { // if satisfied accept_turn_on_easy_mode
                    g_cur_easy_mode = EASY_MODE_ON;
                    g_easy_mode_count = 0;
                }
                break;
            case EASY_MODE_ON:
                g_easy_mode_count++;
                if (diff_finger_on > EASY_MODE_1_TIME_OUT) {
                    g_cur_easy_mode = EASY_MODE_DONE;
                } else if (g_easy_mode_count >= 4) {  // easy_mode >= 4
                    g_cur_easy_mode = EASY_MODE_ON_STAGE_2;
                }
                break;
            case EASY_MODE_ON_STAGE_2:
                if (accept_turn_on_easy_mode(g_cur_easy_mode)) { // if satisfied accept_turn_on_easy_mode
                    g_cur_easy_mode = EASY_MODE_ON;
                    g_easy_mode_count = 0;
                } else if (diff_finger_on > EASY_MODE_1_TIME_OUT) {
                    g_cur_easy_mode = EASY_MODE_DONE;
                }
                break;
            default:
                ex_log(LOG_DEBUG, "EASY_MODE ERROR");
                break;
        }
    } else if (g_finger_on_count > g_max_finger_on_count) {
        g_cur_easy_mode = EASY_MODE_DONE;
    }
    ex_log(LOG_DEBUG, "em = %d ct - pt = %d fnc = %d emc = %d", g_cur_easy_mode,
           g_current_finger_on_timestamp - g_previous_finger_on_timestamp, g_finger_on_count,
           g_easy_mode_count);

    ex_log(LOG_DEBUG, "g_max_finger_on_count = %d", g_max_finger_on_count);
#endif
    return EASY_MODE_UPDATE_OK;
}


BOOL easy_mode_is_on() {
    return (g_cur_easy_mode == EASY_MODE_ON || g_cur_easy_mode == EASY_MODE_ON_STAGE_2);
}

int easy_mode_get_level(int far_ratio){
    if (g_cur_easy_mode == EASY_MODE_ON) {
        ex_log(LOG_DEBUG, "%s, arr em1 %d", __func__, FAR_RATIO_EASY_MODE_1);
        return FAR_RATIO_EASY_MODE_1;
    } else if (g_cur_easy_mode == EASY_MODE_ON_STAGE_2) {
        ex_log(LOG_DEBUG, "%s, arr em1_2 %d", __func__, FAR_RATIO_EASY_MODE_1_STAGE_2);
        return FAR_RATIO_EASY_MODE_1_STAGE_2;
    } else if (plat_get_diff_time(g_previous_finger_on_timestamp) > EASY_MODE_2_TIME_INTERVAL) {
        ex_log(LOG_DEBUG, "%s, arr em2 %d", __func__, FAR_RATIO_EASY_MODE_2);
        return FAR_RATIO_EASY_MODE_2;
    }else {
        return far_ratio;
    }
}


void verify_accuracy_set_app_level(uint32_t level){
	g_app_level = level;
	ex_log(LOG_DEBUG, "%s level  %d", __func__, level);
}
uint32_t verify_accuracy_get_app_level(){
	ex_log(LOG_DEBUG, "%s g_app_level  %d", __func__, g_app_level);
	return g_app_level;
}
