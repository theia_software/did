#include <stdint.h>
#include <stdio.h>
#include "EgisAlgorithmAPI.h"
#include "algomodule.h"
#include "common_definition.h"
#include "core_config.h"
#include "egisImageQuality.h"
#include "egis_algomodule.h"
#include "egis_definition.h"
#include "egis_log.h"
#include "egis_sprintf.h"
#include "fp_algomodule.h"
#include "image_cut.h"
#include "matcher_feature.h"
#include "plat_heap.h"
#include "plat_mem.h"
#include "plat_time.h"
#include "tl_lib_type.h"
#include "verify_accuracy.h"

#if defined(ALGO_GEN_2) || defined(ALGO_GEN_4)
// PASS
#elif defined(G3PLUS_MATCHER)
#include "detect_scratch.h"
#include "isensor_api.h"
#include "update_sensor.h"
#else
// For getting center position
#include "isensor_api.h"
#endif

#ifdef ALGO_GEN_4
#include "faceplusutil.h"
#endif

#define G3PLUS_ENROLL_REDUNDANT_METHOD 3

#ifdef ENROLL_POLICY_Y6
#include "enroll_policy.h"
#endif
#define LOG_TAG "RBS"

#define LIB_VERSION "1"
#define EGISLIB_VERSION "9"
#define EGISLIB_PATCH "0"

#define FP_SCRATCH_DETECT -111
#ifdef G3_MATCHER
#define DECISION_DATA_LEN 64
#elif defined(ALGO_GEN_5)
#define DECISION_DATA_LEN 512 * 1024
#elif defined(ALGO_GEN_7)
#define DECISION_DATA_LEN MAX_DECISION_DATA_LEN
#else
#define DECISION_DATA_LEN 170 * 1024
#endif

#define DECREASE_REJECT_LEVEL 1
#define RESET_REJECT_LEVEL 2
#define SET_MUST_ACCEPT_REJECT_LEVEL -1

#define VER_STRING_SIZE 1024
#define FP_MERGE_ENROLL_INIT 0

static enroll_config_t g_enroll_config;
static verify_config_t g_verify_config;

static const BOOL g_option_skip_verify = TRUE;

static int g_verify_partial_threshold = 70;
static int g_enroll_partial_threshold = 80;
static int g_image_qty_threshold = -1;
#ifdef ALGO_GEN_5
static int g_decision_data_length = -1;
#elif ALGO_GEN_7
static int g_decision_data_length = -1;
#endif

static int g_matcher_threshold[5] ; // for et760 local branch only

#define FPM_FINGER_MAX BAUTH_MAX_FINGER_IDENTIFY_SUPPORT
struct alg_info {
    void* enroll_ctx;
    unsigned int max_enroll_cnt;
    int enroll_result;
    int percentage;
    unsigned int redundant_threshold;
    unsigned int enrolled_cnt;
    int image_qty;
    int matched_score;
    int matched_bioidx;
    int isLearningUpdate;
    int has_been_duplicate_check;
    int fp_enroll_result;
    int identify_session;
    algo_swipe_info_t swipe_info;
};

#define LEARN_UPDATE_UPDATE_TEMPLATE 11
#define LEARN_UPDATE_SDK_REJECT 10

static struct alg_info g_sdk;
static struct alg_info* pSdk = &g_sdk;
static unsigned char g_has_enroll_set_db;

static int16_t g_matched_index;
static uint32_t g_matched_img_checksum;
static unsigned long long g_matched_timing;
#define APPEND_IMAGE_TIME_OUT 500

static void img_array_checksum_reset() {
    g_matched_img_checksum = -1;
    g_matched_index = -1;
    return;
}

static void img_array_checksum_update(unsigned char* data, int data_size, int matched_idx) {
    g_matched_img_checksum = checksum_calculation(data, data_size);
    g_matched_index = matched_idx;
    g_matched_timing = plat_get_time();
    return;
}

static BOOL img_array_checksum_verify(unsigned char* data, int data_size, unsigned long timeout) {
    int checksum;
    if (plat_get_diff_time(g_matched_timing) > timeout) {
        egislog_e("append image time out, reject!");
        return FALSE;
    }

    if (data == NULL || data_size <= 0 || g_matched_img_checksum == -1) {
        return FALSE;
    }

    checksum = checksum_calculation(data, data_size);

    return checksum == g_matched_img_checksum ? TRUE : FALSE;
}

static void _set_circle_center() {
#if defined(ALGO_GEN_2) || defined(ALGO_GEN_4) || defined(__ET6XX__)
    return;
#else
    int bkg_cx = 0, bkg_cy = 0;
#ifdef LARGE_AREA
    //TODO
    bkg_cx = 100;
    bkg_cy = 100;
#endif
    bkg_cx = core_config_get_int(INI_SECTION_SENSOR, KEY_CIRCLE_CENTER_X, bkg_cx);
    bkg_cy = core_config_get_int(INI_SECTION_SENSOR, KEY_CIRCLE_CENTER_Y, bkg_cy);
    if (bkg_cx <= 0 || bkg_cy <= 0) {
        isensor_get_int(PARAM_INT_CALI_BKG_CX, &bkg_cx);
        isensor_get_int(PARAM_INT_CALI_BKG_CY, &bkg_cy);
    }
    egislog_d("bkg_cx = %d, bkg_cy = %d", bkg_cx, bkg_cy);
//#ifdef LARGE_AREA
//    //TODO
//    bkg_cx = 100;
//    bkg_cy = 100;
//#endif
    set_algo_config(ENROLL_CTX_SET_CIRCLE_CENTRE_CX, bkg_cx);
    set_algo_config(ENROLL_CTX_SET_CIRCLE_CENTRE_CY, bkg_cy);
#endif
}

void egis_fp_appendImgIntoTemp(const egis_image_t* egis_image, unsigned char* enroll_templ,
                               int* enroll_temp_size) {
#ifdef DISABLE_TRY_MATCH_LEARNING
    egislog_d("%s, Disable Try match learning", __func__);
    return;
#endif
#ifdef G3_MATCHER
    int image_size = egis_image->img_data.format.width * egis_image->img_data.format.height;
    int frame_count = egis_image->img_data.frame_count;
    if (!g_verify_config.append_img_into_template) {
        egislog_d("append image disabled");
        return;
    }
    // Workaround: only check the first image, in case the image buffer is not continous
    if (!img_array_checksum_verify(egis_image_get_pointer(egis_image, 0), image_size * 1,
                                   APPEND_IMAGE_TIME_OUT)) {
        egislog_e("append image checksum not match, reject!");
        return;
    }

    if (frame_count > 1 && g_matched_index >= 0 && g_matched_index < frame_count) {
        egislog_d("[append_img_template][%d] frame_count=%d, old temp_size=%d", g_matched_index,
                  frame_count, *enroll_temp_size);

        unsigned char* buffer = NULL;
        unsigned char* img_buf;
        int buffer_size, i;

        buffer = (unsigned char*)plat_alloc(MAX_ENROLL_TEMPLATE_LEN);
        img_buf = plat_alloc(image_size * (frame_count - 1));
        if (buffer == NULL || img_buf == NULL)
            egislog_e("buffer memory allocate failed");
        else {
            unsigned char *img_src, *img_dest = img_buf;
            int img_count = 0, ret;
            for (i = 0; i < frame_count; i++) {
                if (i == g_matched_index) continue;

                img_src = egis_image_get_pointer(egis_image, i);
                if (!img_src) continue;

                memcpy(img_dest, img_src, image_size);
                img_count++;
                img_dest += image_size;
            }

            buffer_size = *enroll_temp_size;
            memcpy(buffer, enroll_templ, buffer_size);
            ret = g3api_try_match_learning(img_buf, egis_image->img_data.format.width,
                                           egis_image->img_data.format.height, img_count, buffer,
                                           &buffer_size);
            if (ret == FP_LIB_OK && buffer_size <= MAX_ENROLL_TEMPLATE_LEN) {
                egislog_d("[append_img_template] new temp_size=%d", buffer_size);

                enroll_templ = plat_realloc(enroll_templ, buffer_size);
                if (enroll_templ != NULL) {
                    memcpy(enroll_templ, buffer, buffer_size);
                    *enroll_temp_size = buffer_size;
                } else {
                    egislog_e("append image failed : no space left!");
                }

            } else {
                egislog_e("[append_img_template] buffer_size=%d, ret=%d", buffer_size, ret);
            }
        }
        PLAT_FREE(img_buf);
        PLAT_FREE(buffer);
    }
#endif
}

static int do_verify_uninit(void) {
    if (pSdk->identify_session) verify_uninit();
    pSdk->identify_session = FALSE;
    return 0;
}

static int do_verify_init(int enroll_temp_number, unsigned char** pEnroll_temp,
                          int* enroll_temp_size) {
    int i, template_number, ret = FALSE;
    VERIFY_INIT verify_init_data;

    if (pSdk->identify_session) {
        do_verify_uninit();
    }

    egislog_d("do_verify_init start. %d", enroll_temp_number);
    egislog_d("debug_check do_verify_init start. %d", enroll_temp_number);

    template_number = enroll_temp_number;
    verify_init_data.pEnroll_temp_array = (BYTE**)plat_alloc(template_number * sizeof(BYTE*));
    verify_init_data.enroll_temp_size_array = (int*)plat_alloc(template_number * sizeof(int));

    if (verify_init_data.pEnroll_temp_array == NULL ||
        verify_init_data.enroll_temp_size_array == NULL) {
        egislog_e("do_verify_init, Feat memory allocate failed");
        ret = FALSE;
        goto fail;
    }

    verify_init_data.enroll_temp_number = template_number;
    for (i = 0; i < template_number; i++) {
        verify_init_data.pEnroll_temp_array[i] = pEnroll_temp[i];
        verify_init_data.enroll_temp_size_array[i] = enroll_temp_size[i];
        egislog_d(
            "do_verify_init, Finger_num = %d, Enrollment Temp_size =[ "
            "%d ] ",
            i, enroll_temp_size[i]);
        egislog_d("do_verify_init, Enrollment Temp tpl  = %p ", pEnroll_temp[i]);
    }
    TIME_MEASURE_START(do_verify_init);
    ret = verify_init(&verify_init_data);
    TIME_MEASURE_STOP(do_verify_init, "verify_init");
    egislog_d("do_verify_init verify_init ret = %d", ret);
    if (ret >= 0) ret = TRUE;

    pSdk->identify_session = TRUE;

    g_verify_config.enable_learning_update =
        core_config_get_int(INI_SECTION_VERIFY, KEY_ENABLE_LEARNING_UPDATE, TRUE);
    g_verify_config.append_img_into_template =
        core_config_get_int(INI_SECTION_VERIFY, KEY_APPEND_IMG_INTO_TEMPLATE, TRUE);
    g_verify_config.enable_delay_learning = TRUE;  // TODO: use ini config
    g_verify_partial_threshold = core_config_get_int(
        INI_SECTION_VERIFY, KEY_VERIFY_PARTIAL_THRESHOLD, INID_VERIFY_PARTIAL_THRESHOLD);
    g_image_qty_threshold =
        core_config_get_int(INI_SECTION_VERIFY, KEY_IMAGE_QTY_THRESHOLD, g_image_qty_threshold);

    if (core_config_get_int(INI_SECTION_VERIFY, KEY_FLOW_TRY_MATCH, INID_FLOW_TRY_MATCH) > 0)
        g_verify_config.enable_try_match = FALSE;
    else
        g_verify_config.enable_try_match =
            core_config_get_int(INI_SECTION_VERIFY, KEY_ENABLE_TRY_MATCH, TRUE);
    g_verify_config.skip_identiy_ext_failure = 1;

    g_verify_config.flow_trymatch_count =
        core_config_get_int(INI_SECTION_VERIFY, KEY_FLOW_TRY_MATCH, INID_FLOW_TRY_MATCH);
    g_verify_config.ext_feat_quality_trymatch_th =
        core_config_get_int(INI_SECTION_VERIFY, KEY_EXT_FEAT_QUALITY_TRYMATCH_THRESHOLD,
                            INID_EXT_FEAT_QUALITY_TRYMATCH_THRESHOLD);
    g_verify_config.ext_feat_quality_lqmatch_th =
        core_config_get_int(INI_SECTION_VERIFY, KEY_EXT_FEAT_QUALITY_LQMATCH_THRESHOLD,
                            INID_EXT_FEAT_QUALITY_LQMATCH_THRESHOLD);
    g_verify_config.enable_sratch_mask =
        core_config_get_int(INI_SECTION_VERIFY, KEY_DETECT_SCRATCH_MASK, INID_DETECT_SCRATCH_MASK);
fail:
    if (verify_init_data.pEnroll_temp_array != NULL) plat_free(verify_init_data.pEnroll_temp_array);
    if (verify_init_data.enroll_temp_size_array != NULL)
        plat_free(verify_init_data.enroll_temp_size_array);

    if (ret == FALSE) egislog_e("do_verify_init ret = %d", ret);

    return ret;
}
static int g_duplicate_enroll = 0;

typedef enum {
    SET_ALGO_FLAG_ENROLL,
    SET_ALGO_FLAG_ENROLL_DQE,

    SET_ALGO_FLAG_VERIFY = 10
} set_algo_flag_type_t;

static void _enroll_result_to_algo_flag(set_algo_flag_type_t type, int algo_result) {
    int algo_flag = ALGO_FLAG_DROP;
    switch (type) {
        case SET_ALGO_FLAG_ENROLL: {
            switch (algo_result) {
                case FP_MERGE_ENROLL_IAMGE_OK:
                case FP_MERGE_ENROLL_REDUNDANT_ACCEPT:
                case FP_MERGE_ENROLL_REDUNDANT_INPUT:
                    algo_flag = ALGO_FLAG_KEEP;
                    break;
                case FP_MERGE_ENROLL_FINISH:
                    if (g_enroll_config.enroll_method == ENROLL_METHOD_TOUCH) {
                        algo_flag = ALGO_FLAG_KEEP;
                    } else {
                        algo_flag = ALGO_FLAG_DROP;
                    }
                    break;
                default:
                    algo_flag = ALGO_FLAG_DROP;
                    break;
            }
            EGIS_IMAGE_KEEP_PARAM(try_match_result, (algo_flag == ALGO_FLAG_KEEP) ? 1 : 0);
            EGIS_IMAGE_KEEP_PARAM(algo_flag, algo_result);
            break;
        }
        case SET_ALGO_FLAG_ENROLL_DQE:
            EGIS_IMAGE_KEEP_PARAM(try_match_result, 0);
            EGIS_IMAGE_KEEP_PARAM(algo_flag, algo_result);
            break;
        default:
            egislog_e("not supported %d", type);
            break;
    }
    return;
}

#ifdef __ET7XX__
#ifdef G3_MATCHER
#define ENROLL_REJ_LEVEL 40
#else
#ifdef LARGE_AREA
#define ENROLL_REJ_LEVEL 10
#else
#define ENROLL_REJ_LEVEL 20
#endif
#endif
static int GetDynamicEnrollRejectLevel(int enroll_bad_image_count) {
    int reject_level = 1;
    int enroll_quality_threshold =
        core_config_get_int(INI_SECTION_ENROLL, KEY_ENROLL_QUALITY_THRESHOLD, ENROLL_REJ_LEVEL);
#ifdef LARGE_AREA
    int enroll_qty_decrease =
        core_config_get_int(INI_SECTION_ENROLL, KEY_ENROLL_QUALITY_THRESHOLD_DEC, 5);
#else
    int enroll_qty_decrease =
        core_config_get_int(INI_SECTION_ENROLL, KEY_ENROLL_QUALITY_THRESHOLD_DEC, 4);
#endif

    if (g_enroll_config.enroll_extra_1st_enable) {
        if (enroll_bad_image_count <= 0) {
            // Workaround: Make first image always be rejected
            reject_level = 500;
            return reject_level;
        } else {
            enroll_bad_image_count--;
        }
    }

    reject_level = enroll_quality_threshold - enroll_qty_decrease * enroll_bad_image_count;
    if (reject_level < 1) {
        reject_level = 1;
    }
    return reject_level;
}

static void update_enroll_qty_reject_level(int param, void* ctx) {
    static int enroll_bad_image_count = 0;
    int show_reject_level = 0;
    int current_rej_level = 0;
#ifndef RBS_EVTOOL
    switch (param) {
        case SET_MUST_ACCEPT_REJECT_LEVEL:
            enroll_bad_image_count = -1;  // the value won't matter
            current_rej_level = 1;
            break;
        case DECREASE_REJECT_LEVEL:
        case RESET_REJECT_LEVEL:
        default: {
            if (param == DECREASE_REJECT_LEVEL) {
                enroll_bad_image_count++;
            } else {
                enroll_bad_image_count = 0;
            }
            current_rej_level = GetDynamicEnrollRejectLevel(enroll_bad_image_count);
        }
    }
#endif
    set_enroll_context(ctx, ENROLL_CTX_QUALITY_REJECT_LEVEL, current_rej_level);
    egislog_d("(%d) enroll_bad_image_count = %d, current_rej_level = %d", param,
              enroll_bad_image_count, current_rej_level);
}
#endif

static int do_enroll(struct alg_info* p_sdk, egis_image_t* egis_image, int img_index, BOOL useDQE) {
    fp_image_t* image = (fp_image_t*)egis_image;
    unsigned char* best_img = NULL;
    egislog_d("%s enter, img_index [%d]", __func__, img_index);

    best_img = egis_image_get_pointer(egis_image, img_index);
    if (best_img == NULL) {
        egislog_e("%s, bad img_index %d (frame count %d)", __func__, img_index,
                  egis_image->img_data.frame_count);
        img_index = egis_image->img_data.frame_count - 1;
        best_img = egis_image_get_pointer(egis_image, img_index);
    }
#ifdef MATCHER_TAKE_RAW_IMAGE
    best_img = egis_image_get_pointer_raw(egis_image, img_index);
    uint16_t* rawimg = (uint16_t*)best_img;
#endif

    int has_black_edge = -1;  // < 0 is OK
#ifdef EGIS_IMAGE_V2
    has_black_edge = EGIS_IMAGE_OBTAIN_PARAM(black_edge);
    egis_image_set_keep_param_index(img_index);
#endif

    egislog_d("%s, has_black_edge %d. DQE (%d)", __func__, has_black_edge, useDQE);
#if defined(G3PLUS_MATCHER)
    if (detect_scratch_is_detected()) {
        useDQE = FALSE;
        p_sdk->enroll_result = FP_SCRATCH_DETECT;
    } else if (has_black_edge > 0) {
        if (useDQE) {
            p_sdk->enroll_result = FP_MERGE_ENROLL_IAMGE_OK;
        } else {
            p_sdk->enroll_result = FP_MERGE_ENROLL_FEATURE_LOW;
        }
    } else {
        if (useDQE) {
            p_sdk->enroll_result =
                DQE_enroll(best_img, image->format.width, image->format.height, p_sdk->enroll_ctx);
            if (p_sdk->enroll_result == 0) {
                egislog_v("assume 0 is OK");
                p_sdk->enroll_result = FP_MERGE_ENROLL_IAMGE_OK;
            }
        } else {
            p_sdk->enroll_result = enroll(best_img, image->format.width, image->format.height,
                                          p_sdk->enroll_ctx, &p_sdk->percentage);
        }
    }
#elif defined(G3_MATCHER)
    p_sdk->enroll_result = enroll_ex(
        best_img, image->format.width, image->format.height, p_sdk->enroll_ctx, &p_sdk->percentage,
        MERGE_MAP_WIDTH, MERGE_MAP_HEIGHT, &p_sdk->swipe_info.dx, &p_sdk->swipe_info.dy,
        &p_sdk->swipe_info.similarity_score, &p_sdk->swipe_info.swipe_dir);
#else
    if (has_black_edge > 0) {
        p_sdk->enroll_result = FP_MERGE_ENROLL_FEATURE_LOW;
    } else {
        p_sdk->enroll_result = enroll(best_img, image->format.width, image->format.height,
                                      p_sdk->enroll_ctx, &p_sdk->percentage);
    }
#endif

#ifdef ALGO_GEN_4
    int image_len = 0;
    unsigned char* debug_img = megvii_get_debug_image(&image_len);
    egis_image_set_buffer(egis_image, img_index, IMGTYPE_BIN, debug_img,
                          image->format.width * image->format.height * 4);
#endif
    {
        egislog_v("do_enroll before adjust percentage %d enroll_result_debug = %d", p_sdk->percentage,
                  p_sdk->enroll_result);
        egislog_v("do_enroll before adjust score = %d dx = %d dy = %d", p_sdk->swipe_info.similarity_score,
                  p_sdk->swipe_info.dx, p_sdk->swipe_info.dy);
        int pre_enrolled_cnt = p_sdk->enrolled_cnt;
        get_enroll_context(p_sdk->enroll_ctx, ENROLL_CTX_ENROLLED_COUNT, (INT*)&(p_sdk->enrolled_cnt));
        egislog_d("debug_check_enrolled_cnt new %d pre %d", p_sdk->enrolled_cnt, pre_enrolled_cnt);

        if (pre_enrolled_cnt >= p_sdk->enrolled_cnt) {
            egislog_e(
                    "!!!!!!!!!! debug_check_enrolled_cnt new enrolled_cnt %d < pre enrolled_cnt %d", 
                    p_sdk->enrolled_cnt, pre_enrolled_cnt);
            p_sdk->enrolled_cnt = pre_enrolled_cnt;
            if (p_sdk->enroll_result == FP_MERGE_ENROLL_IAMGE_OK)
                p_sdk->enroll_result = FP_FEATURELOW;
        }
        egislog_v("do_enroll after adjust percentage %d enroll_result_debug = %d", p_sdk->percentage,
                  p_sdk->enroll_result);
        egislog_v("do_enroll after adjust score = %d dx = %d dy = %d", p_sdk->swipe_info.similarity_score,
                  p_sdk->swipe_info.dx, p_sdk->swipe_info.dy);
    }

    if (useDQE) {
        _enroll_result_to_algo_flag(SET_ALGO_FLAG_ENROLL_DQE, p_sdk->enroll_result);
    } else {
        _enroll_result_to_algo_flag(SET_ALGO_FLAG_ENROLL, p_sdk->enroll_result);
    }

    egislog_v("do_enroll percent(0) = %d enroll_result_debug = %d", p_sdk->percentage,
              p_sdk->enroll_result);
    egislog_v("do_enroll score = %d dx = %d dy = %d", p_sdk->swipe_info.similarity_score,
              p_sdk->swipe_info.dx, p_sdk->swipe_info.dy);
    get_enroll_context(p_sdk->enroll_ctx, ENROLL_CTX_MAX_ENROLL_COUNT,
                       (INT*)&(p_sdk->max_enroll_cnt));

    egislog_d("enrolled_cnt = %d (max %d)", p_sdk->enrolled_cnt, p_sdk->max_enroll_cnt);
    if (p_sdk->enroll_result == FP_DUPLICATE) g_duplicate_enroll++;
#ifdef ENROLL_POLICY_Y6
    enroll_policy_update(p_sdk->enroll_ctx, p_sdk->enroll_result, p_sdk->enrolled_cnt,
                         p_sdk->max_enroll_cnt);
#endif  // end of ENROLL_POLICY_Y6

    if (g_enroll_config.enroll_method == ENROLL_METHOD_TOUCH) {
#if defined(G3_MATCHER) || defined(ALGO_GEN_5) || defined(ALGO_GEN_7)
#else
        p_sdk->enrolled_cnt -= g_duplicate_enroll;
        egislog_d("workaround: decrease g_duplicate_enroll(%d)", g_duplicate_enroll);
#endif
        if (p_sdk->max_enroll_cnt > 0) {
            p_sdk->percentage = p_sdk->enrolled_cnt * 100 / p_sdk->max_enroll_cnt;
            if (p_sdk->percentage > 100) p_sdk->percentage = 100;
        }
    }

    egislog_d(
        "do_enroll percent(1) = %d, enroll_result = %d enrolled_cnt = %d "
        "max_enroll_cnt = %d",
        p_sdk->percentage, p_sdk->enroll_result, p_sdk->enrolled_cnt, p_sdk->max_enroll_cnt);
    egislog_d(
        "do_enroll percent(1) = %d, image_qty = %d fp_enroll_result = %d ",
        p_sdk->image_qty, p_sdk->fp_enroll_result);

    return FP_LIB_OK;
}

static VERIFY_INFO g_verify_info;
static int _verify_info_init(uint8_t* feat, int feat_size, int ext_feat_retval) {
#ifndef ALGO_GEN_5
    // The buffer is controlled by G5
    PLAT_FREE(g_verify_info.pEnroll_temp);
#endif
    memset(&g_verify_info, 0, sizeof(VERIFY_INFO));
    g_verify_info.matchindex = -1;
    g_verify_info.pFeat = feat;
    g_verify_info.feat_size = feat_size;
    g_verify_info.extract_ret = ext_feat_retval;
    g_verify_info.enroll_temp_size = 0;
#ifdef G3_MATCHER
    g_verify_info.target_template = VERIFY_ALL_FINGERS;
#endif
#if defined(ALGO_GEN_4) || defined(ALGO_GEN_5)
    g_verify_info.pEnroll_temp = NULL;
#else
    g_verify_info.pEnroll_temp = (BYTE*)plat_alloc(MAX_ENROLL_TEMPLATE_LEN);
    if (g_verify_info.pEnroll_temp == NULL) {
        egislog_e("verify_info.pEnroll_temp alloc fail!");
        return EGIS_OUT_OF_MEMORY;
    }
#endif
    g_verify_info.matchScore = 0;
    g_verify_info.matchindex = -1;
    g_verify_info.isLearningUpdate = FALSE;
#ifdef G3PLUS_MATCHER
    if (g_verify_config.enable_delay_learning) {
        g_verify_info.learning_mode = 0;
    } else {
        g_verify_info.learning_mode = 2;
    }
#endif
#ifdef G3_MATCHER
    g_verify_info.target_template = VERIFY_ALL_FINGERS;
#elif defined(ALGO_GEN_2) || defined(ALGO_GEN_5) || defined(ALGO_GEN_7)
    g_verify_info.match_score_array = (int*)plat_alloc(MAX_ENROLL_FINGERS * sizeof(int));
    memset(g_verify_info.match_score_array, 0, MAX_ENROLL_FINGERS * sizeof(int));
#ifdef ALGO_GEN_2
    set_algorithm_config(ALGO_COF_RESOLUTION,
                         core_config_get_int(INI_SECTION_VERIFY, KEY_VERIFY_IMAGE_DPI, 500));
#endif
#endif
    return EGIS_OK;
}

static void _verify_info_uninit() {
#ifndef ALGO_GEN_5
    // The buffer is controlled by G5
    PLAT_FREE(g_verify_info.pEnroll_temp);
#endif
#if defined(ALGO_GEN_2) || defined(ALGO_GEN_5) || defined(ALGO_GEN_7)
    PLAT_FREE(g_verify_info.match_score_array)
#endif
    g_verify_info.pFeat = NULL;
    g_verify_info.feat_size = 0;
    g_verify_info.matchindex = -1;
}

#if defined(__ET7XX__)
extern BOOL g_this_image_use_quick_capture;
#endif
enum {
    VERIFY_TYPE_NA = 0,
    VERIFY_TYPE_NORMAL_MATCH,
    VERIFY_TYPE_QUICK_MATCH,
    VERIFY_TYPE_LQM_MATCH,
};
static int _do_verify(struct alg_info* data, feature_extract_result_t extract, int try_index,
                      unsigned char* mfeature_buf, int mfeature_size) {
    int ret, temp_match_idx = -1;

    if (extract.ext_feat_retval != exOK) {
        egislog_e("%s, extract ret = %d", __func__, extract.ext_feat_retval);
        if (g_verify_config.skip_identiy_ext_failure) {
            egislog_e("%s, FP_FEATURELOW", __func__);
            return FP_FEATURELOW;
        }
    }
    _verify_info_init(mfeature_buf, mfeature_size, extract.ext_feat_retval);

    egislog_d("%s:try %d / %d", __func__, try_index, g_verify_config.flow_trymatch_count);
#if defined(G3PLUS_MATCHER)
    egislog_d("%s:try %d v.s %d", __func__, extract.ext_feat_quality,
              g_verify_config.ext_feat_quality_trymatch_th);
    int final_try = (try_index >= g_verify_config.flow_trymatch_count ||
                     extract.ext_feat_quality > g_verify_config.ext_feat_quality_trymatch_th)
                        ? 1
                        : 0;
    egislog_d("%s:try final %d", __func__, final_try);
    g_verify_info.try_count = try_index;
    g_verify_info.final_try = final_try;
#elif defined(ALGO_GEN_2)
    int g2partial = EGIS_IMAGE_OBTAIN_PARAM(g2_partial);
    int g2qty = EGIS_IMAGE_OBTAIN_PARAM(qty);
    int mlpartial = EGIS_IMAGE_OBTAIN_PARAM(partial);

    egislog_d("%s:g2_partial %d, qty = %d, partial = %d", __func__, g2partial, g2qty, mlpartial);
    egislog_d("%s:g_verify_partial_threshold = %d, g_image_qty_threshold = %d", __func__,
              g_verify_partial_threshold, g_image_qty_threshold);

    egislog_d("%s:ext qty %d v.s %d", __func__, extract.ext_feat_quality,
              g_verify_config.ext_feat_quality_lqmatch_th);
    int bUsingLQMatchTry = extract.ext_feat_quality < g_verify_config.ext_feat_quality_lqmatch_th;
    if (g_this_image_use_quick_capture) {
        EGIS_IMAGE_KEEP_PARAM(match_type, VERIFY_TYPE_QUICK_MATCH);
        ret = quick_verify(&g_verify_info);
        egislog_d("egis_alg_identify quick match verify ret = %d", ret);
    } else if (bUsingLQMatchTry) {
        EGIS_IMAGE_KEEP_PARAM(match_type, VERIFY_TYPE_LQM_MATCH);
        ret = lowQty_verify(&g_verify_info);  // G2 only
        egislog_d("egis_alg_identify lowQty verify ret = %d", ret);
    } else
#elif defined(ALGO_GEN_4)
    egislog_d("Backup g_verify_info.pEnroll_temp=%p", g_verify_info.pEnroll_temp);
    uint8_t* backup_pointer = g_verify_info.pEnroll_temp;
#elif defined(ALGO_GEN_5) || defined(ALGO_GEN_7)
    int final_try = (try_index >= g_verify_config.flow_trymatch_count ||
                     extract.ext_feat_quality > g_verify_config.ext_feat_quality_trymatch_th)
                        ? 1
                        : 0;
    egislog_d("%s:try final %d", __func__, final_try);
    g_verify_info.try_count = try_index;
    g_verify_info.final_try = final_try;
#endif
    {
        //EGIS_IMAGE_KEEP_PARAM(match_type, VERIFY_TYPE_NORMAL_MATCH);
        ret = verify(&g_verify_info);
        egislog_d("egis_alg_identify verify ret = %d", ret);
    }
    if (ret == FP_MATCHFAIL) {
        ret = FP_MATCHFAIL;
    } else if (ret >= 0) {
        ret = EGIS_OK;
    } else {
        egislog_e("verify error! ret = %d", ret);
        ret = EGIS_INCORRECT_PARAMETER;
    }

    data->isLearningUpdate = g_verify_info.isLearningUpdate;
    data->matched_bioidx = g_verify_info.matchindex;
    data->matched_score = g_verify_info.matchScore;

    temp_match_idx = data->matched_bioidx;
    egislog_d("STATE_VERIFY isLearningUpdate = %d, verify_info.matchindex = %d, score=%d",
              g_verify_info.isLearningUpdate, temp_match_idx, g_verify_info.matchScore);

#if defined(G3_MATCHER) || defined(RBS_EVTOOL)
    if (data->isLearningUpdate) {
        data->isLearningUpdate = LEARN_UPDATE_UPDATE_TEMPLATE;
    }
#elif defined(ALGO_GEN_2)
    if (data->isLearningUpdate) {
        if (g2partial <= -1 || mlpartial < g_enroll_partial_threshold ||
            g2qty < g_image_qty_threshold) {
            data->isLearningUpdate = LEARN_UPDATE_SDK_REJECT;
            egislog_d("isLearningUpdate = %d (sdk reject)", LEARN_UPDATE_SDK_REJECT);
        } else {
            data->isLearningUpdate = LEARN_UPDATE_UPDATE_TEMPLATE;
        }
    }
#elif defined(ALGO_GEN_4)
    egislog_d("Check g_verify_info.pEnroll_temp=%p", g_verify_info.pEnroll_temp);
    g_verify_info.pEnroll_temp = backup_pointer;
    if (data->isLearningUpdate) {
        data->isLearningUpdate = LEARN_UPDATE_UPDATE_TEMPLATE;
        // Workaround to different learning buffer update
        g_verify_info.enroll_temp_size = 0;
    }
#endif
    g_verify_info.isLearningUpdate = data->isLearningUpdate;

#if defined(ALGO_GEN_2) || defined(ALGO_GEN_5) || defined(ALGO_GEN_7)
    egislog_d("%s [%d, %d, %d, %d, %d]", __func__, g_verify_info.match_score_array[0],
              g_verify_info.match_score_array[1], g_verify_info.match_score_array[2],
              g_verify_info.match_score_array[3], g_verify_info.match_score_array[4]);
    EGIS_IMAGE_KEEP_PARAM(match_score_0, g_verify_info.match_score_array[0]);
    EGIS_IMAGE_KEEP_PARAM(match_score_1, g_verify_info.match_score_array[1]);
    EGIS_IMAGE_KEEP_PARAM(match_score_2, g_verify_info.match_score_array[2]);
    EGIS_IMAGE_KEEP_PARAM(match_score_3, g_verify_info.match_score_array[3]);
    EGIS_IMAGE_KEEP_PARAM(match_score_4, g_verify_info.match_score_array[4]);
    PLAT_FREE(g_verify_info.match_score_array);
#endif
    egislog_d("%s ret = %d", __func__, ret);
    return ret;
}
BYTE* g_decision_data = NULL;

int egis_fp_initAlgAndPPLib(int snsr_type) {
    UINT ret = FP_LIB_OK;
    BOOL reset_flag = FALSE;
    int sensor_type = snsr_type;
    int enable_cut_image;
    int image_width, image_height, image_dpi;
    ALGO_API_INFO algo_api_version;

    g_enroll_partial_threshold = core_config_get_int(
        INI_SECTION_ENROLL, KEY_ENROLL_PARTIAL_THRESHOLD, INID_ENROLL_PARTIAL_THRESHOLD);
    g_verify_partial_threshold = core_config_get_int(
        INI_SECTION_VERIFY, KEY_VERIFY_PARTIAL_THRESHOLD, INID_VERIFY_PARTIAL_THRESHOLD);

    g_matcher_threshold[0] = core_config_get_int(INI_SECTION_SENSOR, KEY_ET760_MATCHER_THRESHOLD_1, 100000);
    g_matcher_threshold[1] = core_config_get_int(INI_SECTION_SENSOR, KEY_ET760_MATCHER_THRESHOLD_2, 100000);
    g_matcher_threshold[2] = core_config_get_int(INI_SECTION_SENSOR, KEY_ET760_MATCHER_THRESHOLD_3, 100000);
    g_matcher_threshold[3] = core_config_get_int(INI_SECTION_SENSOR, KEY_ET760_MATCHER_THRESHOLD_4, 100000);
    g_matcher_threshold[4] = core_config_get_int(INI_SECTION_SENSOR, KEY_ET760_MATCHER_THRESHOLD_5, 100000);

    for ( int i = 0 ; i < 5 ; i++ ) {
        egislog_d("multi_threshold ET760 Get matcher %d threshold = %d", i, g_matcher_threshold[i]) ;
    }

    memset(pSdk, 0, sizeof(struct alg_info));

    egislog_d("GIT_SHA1 = UNKNOWN");
    egislog_d("FP TA build at %s %s", __DATE__, __TIME__);

    fp_getImageSize(&image_width, &image_height, &image_dpi);

    enable_cut_image = core_config_get_int(INI_SECTION_SENSOR, KEY_CUT_IMAGE, 0);
    if (enable_cut_image) {
        sensor_type = core_config_get_int(INI_SECTION_SENSOR, KEY_ALGO_INIT_SENSOR_TYPE,
                                          FP_ALGOAPI_MODE_UNKNOWN);
        egislog_i("crop: sensor type: %d", sensor_type);
    }

    int sensor_id = g_egis_sensortype.type;
    egislog_d("sensor_id = %d, series = %d, sensor_type = %d", g_egis_sensortype.type,
              g_egis_sensortype.series, sensor_type);
    if (g_egis_sensortype.series == 9){
        sensor_id += 900;
    } else if (g_egis_sensortype.series >= 7) {
        sensor_id += 700;
    }
    if (FP_ALGOAPI_MODE_UNKNOWN == sensor_type) {
        switch (sensor_id) {
#ifndef __ET7XX__
            case ET510:
                sensor_type = FP_ALGOAPI_MODE_EGIS_ET510;
                break;
            case ET511:
                sensor_type = FP_ALGOAPI_MODE_EGIS_ET511;
                break;
            case ET512:
                sensor_type = FP_ALGOAPI_MODE_EGIS_ET512;
                break;
            case ET516:
                sensor_type = FP_ALGOAPI_MODE_EGIS_ET516;
                break;
            case ET520:
                sensor_type = FP_ALGOAPI_MODE_EGIS_ET520;
                break;
            case ET522:
                sensor_type = FP_ALGOAPI_MODE_EGIS_ET522;
                break;
            case ET523:
                sensor_type = FP_ALGOAPI_MODE_EGIS_ET523;
                break;
            case ET613:
                sensor_type = FP_ALGOAPI_MODE_EGIS_ET613;
                break;
            case ET601:
                sensor_type = FP_ALGOAPI_MODE_EGIS_ET601;
                break;
            case ET605:
                sensor_type = FP_ALGOAPI_MODE_EGIS_ET616;
                break;
#endif
            case SENSOR_ID_ET711:
            case SENSOR_ID_ET729:
                sensor_type = FP_ALGOAPI_MODE_EGIS_ET711;
                break;
            case SENSOR_ID_ET713:
                sensor_type = FP_ALGOAPI_MODE_EGIS_ET713;
                break;
#ifdef G3PLUS_MATCHER
            case SENSOR_ID_ET701:
                sensor_type = FP_ALGOAPI_MODE_EGIS_ET701;
                break;
#endif
#ifdef ALGO_GEN_5
            case SENSOR_ID_ET701:
                sensor_type = FP_ALGOAPI_MODE_EGIS_ET701_;
                break;
            case SENSOR_ID_ET901:
                sensor_type = FP_ALGOAPI_MODE_EGIS_ET901_;
                break;
            case SENSOR_ID_ET702:
                sensor_type = FP_ALGOAPI_MODE_EGIS_ET702_;
                break;
            case SENSOR_ID_ET716:
                sensor_type = FP_ALGOAPI_MODE_EGIS_ET715;
                break;
#endif
#if defined(ALGO_GEN_5) || defined(ALGO_GEN_7)
			case SENSOR_ID_ET760:
                sensor_type = FP_ALGOAPI_MODE_EGIS_ET760_;
                break;
#endif
            default:
                if (67 == image_width) {
                    sensor_type = FP_ALGOAPI_MODE_EGIS_ET538;
                }
                break;
        }
    }
    if (FP_ALGOAPI_MODE_UNKNOWN == sensor_type) {
        egislog_e("Unknown sensor type 0x%x", sensor_type);
    }

    if (g_decision_data == NULL) {
#if defined(ALGO_GEN_5)
        if(image_width <= 0 || image_height <= 0)
            g_decision_data_length = DECISION_DATA_LEN;
        else
            g_decision_data_length = image_width * image_height * 10 + 10000;
        g_decision_data = plat_alloc(g_decision_data_length);
        memset(g_decision_data, 0, g_decision_data_length);
#elif defined(ALGO_GEN_7)
        g_decision_data_length = DECISION_DATA_LEN;
        g_decision_data = plat_alloc(g_decision_data_length);
        memset(g_decision_data, 0, g_decision_data_length);
#else
        g_decision_data = plat_alloc(DECISION_DATA_LEN);
        memset(g_decision_data, 0, DECISION_DATA_LEN);
#endif
        reset_flag = TRUE;
        egislog_d("reset_flag = %x", reset_flag);
    }

    egislog_i("sensor type: %d", sensor_type);

#ifdef ALGO_GEN_4
    algorithm_initialization_for_faceplus();
#else
    #if defined(ALGO_GEN_5) || defined(ALGO_GEN_7)
    ret = algorithm_initialization_by_sensor(g_decision_data, g_decision_data_length, sensor_type,
                                             reset_flag);
    #else
    ret = algorithm_initialization_by_sensor(g_decision_data, DECISION_DATA_LEN, sensor_type,
                                             reset_flag);
    #endif
    if (ret != FP_OK) {
        egislog_e(
            "algorithm_initialization_by_sensor, "
            "algorithm_initialization fail ,ret = %d",
            ret);
        return FP_LIB_ERROR_GENERAL;
    }
    egislog_d("egisalg:%s - %s", EGISLIB_VERSION, EGISLIB_PATCH);

    algorithm_do_other(FP_ALGOAPI_GET_VERSION, NULL, (BYTE*)&algo_api_version);
    egislog_d("algo_api_version:%s,matchlib_version:%s", algo_api_version.algo_api_version,
              algo_api_version.matchlib_version);
	#if defined(ALGO_GEN_7)
	//TODO
	//Enable SQ only for ET760
    //set_algo_config(ALGO_CONFIG_SET_MATCHER_MASK, 1);
    set_algo_config(ALGO_CONFIG_SET_MATCHER_MASK, 2);
	#endif
#endif
    ret = mfeature_create();
    if (ret != EGIS_OK) {
        return ret;
    }
#ifdef G3PLUS_MATCHER
    detect_scratch_init(image_width, image_height);
#endif
    egislog_d("egis_fp_initAlgAndPPLib ret = %d", ret);

#ifdef EGIS_DEBUG_MEMORY
    sys_memory_init();
#endif

    return ret;
}

int egis_fp_initCheckTemplateVersion(uint32_t* templateVersion) {
    egislog_d("egis_fp_initCheckTemplateVersion %d %d", *templateVersion, EGIS_TEMPLATE_VERSION);
    // check template version with algo version  -1:uncheck   0:check ok
    // 1:unmatch  2:error

    if (templateVersion == NULL) {
        egislog_e("egis_fp_initCheckTemplateVersion null pointer");
        return 2;  // error
    }

    if (*templateVersion == EGIS_TEMPLATE_VERSION)
        return 0;  // check ok
    else
        return 1;  // unmatch
}

void egis_fp_deInitAlg(void) {
    mfeature_destroy();
    _verify_info_uninit();

    if (pSdk->enroll_ctx != NULL) {
        egislog_d("calling enroll_uninit");
        enroll_uninit(pSdk->enroll_ctx);
        pSdk->enroll_ctx = NULL;
    }
#ifdef G3PLUS_MATCHER
    detect_scratch_uninit();
#endif
#ifdef ALGO_GEN_4
    algorithm_uninitialization_for_faceplus(g_decision_data, DECISION_DATA_LEN);
#else
    #if defined(ALGO_GEN_5) || defined(ALGO_GEN_7)
    algorithm_uninitialization(g_decision_data, g_decision_data_length);
    #else
    algorithm_uninitialization(g_decision_data, DECISION_DATA_LEN);
    #endif
#endif
    // TODO: Save g_decision_data

    PLAT_FREE(g_decision_data);
}

static BOOL g_enroll_flag = FALSE;

int egis_fp_identifyStart(uint32_t need_liveness_authentication,
                          fp_lib_template_t** candidate_templates, int size) {
    int retval = FP_LIB_OK;
    memset(&g_verify_info, 0, sizeof(VERIFY_INFO));

    egislog_d("GIT_SHA1 = UNKNOWN");
    egislog_d("egis_fp_identifyStart need_liveness = %d", need_liveness_authentication);

    egislog_d("debug_check egis_fp_identifyStart size = %d MAX_ENROLL_FINGERS %d", 
                    size, MAX_ENROLL_FINGERS);
    if (size > MAX_ENROLL_FINGERS)
        size = MAX_ENROLL_FINGERS;
    egislog_d("debug_check egis_fp_identifyStart size = %d MAX_ENROLL_FINGERS %d", 
                    size, MAX_ENROLL_FINGERS);

#ifdef ALGO_GEN_4
    int ret = algorithm_initialization_for_faceplus();
    if (ret != EGIS_OK) {
        egislog_e("%s algorithm_initialization_for_faceplus failed", __func__);
        return FP_LIB_ERROR_GENERAL;
    }
#endif

    BYTE* pEnroll_temp_array[MAX_ENROLL_FINGERS];
    int enroll_temp_size[MAX_ENROLL_FINGERS];
    int i;

    if (candidate_templates == NULL) {
        egislog_e("%s, null pointer", __func__);
        return FP_LIB_ERROR_MEMORY;
    }

    if (size < 1 || size > MAX_ENROLL_FINGERS) {
        egislog_e("%s, wrong size: %d", __func__, size);
        return FP_LIB_ERROR_MEMORY;
    }

    // In case the SDK found Enroll Duplicate and did not call egis_fp_enrollFinish()
    if (pSdk->enroll_ctx != NULL) {
        egislog_d("calling enroll_uninit");
        enroll_uninit(pSdk->enroll_ctx);
        pSdk->enroll_ctx = NULL;
    }
    do_verify_uninit();

    _set_circle_center();
    TIME_MEASURE_START(egis_fp_identifyStart);
    for (i = 0; i < size; i++) {
        enroll_temp_size[i] = candidate_templates[i]->size;
        pEnroll_temp_array[i] = candidate_templates[i]->tpl;
    }

    if (do_verify_init(size, pEnroll_temp_array, enroll_temp_size) != TRUE) {
        egislog_e("egis_alg_begin_identify, Failed End");
        return FP_LIB_ERROR_GENERAL;
    }
    TIME_MEASURE_STOP(egis_fp_identifyStart, "do_verify_init");
#ifdef G3PLUS_MATCHER
    if (g_enroll_config.enroll_redundant_method == G3PLUS_ENROLL_REDUNDANT_METHOD) {
        egislog_d("redundant method %d", G3PLUS_ENROLL_REDUNDANT_METHOD);
        set_algo_config(ALGO_CONFIG_REDUNDANT_METHOD, G3PLUS_ENROLL_REDUNDANT_METHOD);
    }
#endif
    egislog_d("egis_fp_identifyStart retval = %d", retval);
    if (FP_LIB_OK == retval) {
        g_enroll_flag = FALSE;
    }
    return retval;
}

void egis_fp_identifyFinish(void) {
    int retval = FP_LIB_OK;

    _verify_info_uninit();

    egislog_d("calling verify_uninit");
    do_verify_uninit();
    egislog_d("egis_fp_identifyFinish retval = %d", retval);

    return;
}

int egis_fp_identifyImage_enroll(egis_image_t* image, fp_lib_enroll_data_t* enroll_data_t,
                                 fp_lib_template_t** candidate_templates, int size,
                                 algo_swipe_info_t* swipe_info) {
    int retval, i, template_number = 0;
    VERIFY_INIT set_enroll_db_data;

    fp_image_quality_t* capture_quality = &(image->quality);

    egislog_d("%s (%d) size=%d", __func__, enroll_data_t->enroll_option, size);
    if (image == NULL) {
        egislog_e("%s null pointer", __func__);
        return FP_LIB_ERROR_PARAMETER;
    } else if (image->img_data.frame_count < 1) {
        egislog_e("%s input frame count < 1", __func__);
        return FP_LIB_ERROR_PARAMETER;
    }

    if (g_enroll_config.enroll_method == ENROLL_METHOD_TOUCH) {
        if (FP_LIB_ENROLL_SUCCESS != capture_quality->reject_reason) {
            enroll_data_t->result = capture_quality->reject_reason;
            egislog_d("%s, reject reason = %d", __func__, capture_quality->reject_reason);
            return FP_LIB_OK;
        }
    }

    retval = FP_LIB_OK;
    if (g_has_enroll_set_db == 0) {
        template_number = size;
    }
    pSdk->fp_enroll_result = FP_LIB_ENROLL_FAIL_NONE;

    if (template_number == 0 || candidate_templates == NULL) {
        set_enroll_db_data.pEnroll_temp_array = NULL;
        set_enroll_db_data.enroll_temp_size_array = NULL;
        set_enroll_db_data.enroll_temp_number = 0;
    } else {
        set_enroll_db_data.pEnroll_temp_array = (BYTE**)plat_alloc(template_number * sizeof(BYTE*));
        set_enroll_db_data.enroll_temp_size_array = (int*)plat_alloc(template_number * sizeof(int));

        if (set_enroll_db_data.pEnroll_temp_array == NULL ||
            set_enroll_db_data.enroll_temp_size_array == NULL) {
            egislog_e(
                "egis_fp_identifyImage_enroll, Feat memory "
                "allocate failed");
            return FP_LIB_ERROR_MEMORY;
        }
        set_enroll_db_data.enroll_temp_number = template_number;
        for (i = 0; i < template_number; i++) {
            set_enroll_db_data.pEnroll_temp_array[i] = candidate_templates[i]->tpl;
            set_enroll_db_data.enroll_temp_size_array[i] = candidate_templates[i]->size;
        }
        egislog_d("enroll_setDB: enroll_temp_number=%d", set_enroll_db_data.enroll_temp_number);
        for (i = 0; i < template_number; i++) {
            egislog_v("enroll_setDB: pEnroll_temp_array[%d]=%p", i,
                      set_enroll_db_data.pEnroll_temp_array[i]);
            egislog_v("enroll_setDB: enroll_temp_size_array[%d]=%p", i,
                      set_enroll_db_data.enroll_temp_size_array[i]);
            egislog_v("enroll_setDB: candidate_templates[%i]=%p", i, candidate_templates[i]->tpl);
            egislog_v("enroll_setDB: size[%d]=%d", i, candidate_templates[i]->size);
        }
        retval = enroll_setDB(pSdk->enroll_ctx, &set_enroll_db_data);
        for (i = 0; i < template_number; i++) {
            egislog_v("enroll_setDB: pEnroll_temp_array[%d]=%p", i,
                      set_enroll_db_data.pEnroll_temp_array[i]);
            egislog_v("enroll_setDB: enroll_temp_size_array[%d]=%p", i,
                      set_enroll_db_data.enroll_temp_size_array[i]);
            egislog_v("enroll_setDB: candidate_templates[%i]=%p", i, candidate_templates[i]->tpl);
            egislog_v("enroll_setDB: size[%d]=%d", i, candidate_templates[i]->size);
        }
        if (retval != FP_OK) {
            egislog_e(
                "egis_fp_identifyImage_enroll, enroll_setDB fail "
                "ret = %d",
                retval);
            goto clean_up;
        }
        g_has_enroll_set_db = 1;
    }

    int img_index = image->img_data.frame_count - 1;
    BOOL useDQE = FALSE;
#ifdef __ET7XX__
    if (enroll_data_t->enroll_option == ENROLL_OPTION_FORCE_ACCEPT) {
        update_enroll_qty_reject_level(SET_MUST_ACCEPT_REJECT_LEVEL, pSdk->enroll_ctx);
    } else if (enroll_data_t->enroll_option == ENROLL_OPTION_REJECT_RETRY) {
        update_enroll_qty_reject_level(DECREASE_REJECT_LEVEL, pSdk->enroll_ctx);
    } else if (enroll_data_t->enroll_option == ENROLL_OPTION_ENROLL_THE_FIRST) {
        useDQE = TRUE;
        //img_index = 0;
        update_enroll_qty_reject_level(SET_MUST_ACCEPT_REJECT_LEVEL, pSdk->enroll_ctx);
    } else {
        update_enroll_qty_reject_level(RESET_REJECT_LEVEL, pSdk->enroll_ctx);
    }
#ifdef RBS_EVTOOL
    _set_circle_center();
#endif
#endif

    retval = do_enroll(pSdk, image, img_index, useDQE);

    if (retval != FP_LIB_OK) {
        egislog_e("egis_fp_identifyImage_enroll, do_enroll fail ret = %d", retval);
        goto clean_up;
    }

    if (pSdk->enroll_result == FP_DUPLICATE) {
        egislog_d("egis_fp_identifyImage_enroll, FP_DUPLICATE");
        pSdk->fp_enroll_result = FP_LIB_ENROLL_HELP_ALREADY_EXIST;
        pSdk->has_been_duplicate_check = 0;
        goto clean_up;
    } else if (pSdk->enroll_result == FP_SCRATCH_DETECT) {
        egislog_d("egis_fp_identifyImage_enroll, FP_SCRATCH_DETECT");
        pSdk->fp_enroll_result = FP_LIB_ENROLL_HELP_SCRATCH_DETECTED;
        pSdk->has_been_duplicate_check = 1;
        goto clean_up;
    } else if (pSdk->enroll_result == FP_MERGE_ENROLL_REDUNDANT_INPUT) {
        egislog_d("egis_fp_identifyImage_enroll, FP_MERGE_ENROLL_REDUNDANT_INPUT");
        pSdk->fp_enroll_result = FP_LIB_ENROLL_HELP_SAME_AREA;
        pSdk->has_been_duplicate_check = 1;
        goto clean_up;
    } else if (pSdk->enroll_result == FP_MERGE_ENROLL_FEATURE_LOW) {
        egislog_d("egis_fp_identifyImage_enroll, FP_MERGE_ENROLL_FEATURE_LOW");
        pSdk->fp_enroll_result = FP_LIB_ENROLL_FAIL_LOW_COVERAGE;
        pSdk->has_been_duplicate_check = 1;
        goto clean_up;
#ifdef ALGO_GEN_4
    } else if (pSdk->enroll_result == FP_MERGE_ENROLL_REJECT_PARTIAL ||
               pSdk->enroll_result == FP_MERGE_ENROLL_LOW_QTY) {
        ex_log(LOG_DEBUG, "egis_fp_identifyImage_enroll, FP_MERGE_ENROLL_REJECT_PARTIAL");
        pSdk->fp_enroll_result = FP_LIB_ENROLL_FAIL_LOW_COVERAGE;
        pSdk->has_been_duplicate_check = 1;
        EGIS_IMAGE_KEEP_PARAM(black_edge, 1);
        goto clean_up;
    } else if (pSdk->enroll_result == FP_MERGE_ENROLL_REJECT_FAKE) {
        ex_log(LOG_ERROR, "egis_fp_identifyImage_enroll, FP_MERGE_ENROLL_REJECT_FAKE");
        if (core_config_get_int(INI_SECTION_VERIFY, KEY_FAKE_FINGER_DETECTION,
                                INID_FAKE_FINGER_DETECTION)) {
            pSdk->fp_enroll_result = FP_LIB_ENROLL_FAIL_SPOOF_FINGER;
        } else {
            pSdk->fp_enroll_result = FP_LIB_ENROLL_FAIL_LOW_COVERAGE;
        }
        pSdk->has_been_duplicate_check = 1;
        EGIS_IMAGE_KEEP_PARAM(fake_score, 1);
        goto clean_up;
#endif
#if defined(G3PLUS_MATCHER) || defined(ALGO_GEN_5) || defined(ALGO_GEN_7)
    } else if (pSdk->enroll_result == FP_MERGE_ENROLL_LOW_QTY ||
               pSdk->enroll_result == FP_FEATURELOW) {
        pSdk->fp_enroll_result = FP_LIB_ENROLL_FAIL_LOW_QUALITY_DYNAMIC_REJECT_LEVEL;
#endif
    } else if (pSdk->enroll_result == FP_MERGE_ENROLL_BAD_IMAGE) {
        egislog_d("egis_fp_identifyImage_enroll, FP_MERGE_ENROLL_BAD_IMAGE");
#ifdef __ET7XX__
        pSdk->fp_enroll_result = FP_LIB_ENROLL_FAIL_LOW_QUALITY_DYNAMIC_REJECT_LEVEL;
#else
        pSdk->fp_enroll_result = FP_LIB_ENROLL_FAIL_LOW_QUALITY;
#endif
        pSdk->has_been_duplicate_check = 1;
        goto clean_up;
    } else if (pSdk->enroll_result == FP_MERGE_ENROLL_TOO_FAST) {
        egislog_d("egis_fp_identifyImage_enroll FP_MERGE_ENROLL_TOO_FAST");
        pSdk->fp_enroll_result = FP_LIB_ENROLL_TOO_FAST;
        pSdk->has_been_duplicate_check = 1;
    } else if (pSdk->enroll_result <= 0) {
        egislog_e("egis_fp_identifyImage_enroll failed result %d", pSdk->enroll_result);
        pSdk->fp_enroll_result = FP_LIB_ENROLL_FAIL_NONE;
        pSdk->has_been_duplicate_check = 0;
        goto clean_up;
    } else {
        pSdk->fp_enroll_result = FP_LIB_ENROLL_SUCCESS;
        pSdk->has_been_duplicate_check = 1;
        if (swipe_info != NULL && g_enroll_config.enroll_method != ENROLL_METHOD_TOUCH) {
            swipe_info->dx = pSdk->swipe_info.dx;
            swipe_info->dy = pSdk->swipe_info.dy;
            swipe_info->swipe_dir = pSdk->swipe_info.swipe_dir;
            swipe_info->similarity_score = pSdk->swipe_info.similarity_score;
            swipe_info->mergemap_hw_width = image->img_data.format.width;
            swipe_info->mergemap_hw_height = image->img_data.format.height;
        }
    }
clean_up:
    set_enroll_db_data.enroll_temp_number = 0;
    for (i = 0; i < template_number; i++) {
        set_enroll_db_data.pEnroll_temp_array[i] = NULL;
        set_enroll_db_data.enroll_temp_size_array[i] = 0;
    }
    // int retTmp = enroll_setDB(pSdk->enroll_ctx, &set_enroll_db_data);
    // egislog_d("enroll_setDB retTTTT = %d", retTmp);
    if (set_enroll_db_data.pEnroll_temp_array != NULL) {
        plat_free(set_enroll_db_data.pEnroll_temp_array);
        set_enroll_db_data.pEnroll_temp_array = NULL;
    }
    if (set_enroll_db_data.enroll_temp_size_array != NULL) {
        plat_free(set_enroll_db_data.enroll_temp_size_array);
        set_enroll_db_data.enroll_temp_size_array = NULL;
    }

    enroll_data_t->result = pSdk->fp_enroll_result;
    enroll_data_t->progress = pSdk->percentage;

    egislog_d("egis_fp_identifyImage_enroll image (%p) result %d,progress %d ret = %d", image,
              enroll_data_t->result, enroll_data_t->progress, retval);

    return retval;
}

int egis_fp_identifyImage_identify(egis_image_t* egis_image, fp_lib_identify_data_t* identify_data,
                                   int* candidate_index) {
    TIME_MEASURE_START(identify_total);

    int retval = FP_LIB_OK;
    int i = 0, try_index;

    egislog_d("%s entry", __func__);

    if (egis_image == NULL || identify_data == NULL || candidate_index == NULL) {
        egislog_e("%s null pointer", __func__);
        return FP_LIB_ERROR_MEMORY;
    }
    fp_image_t* image = (fp_image_t*)egis_image;
    fp_image_quality_t* capture_quality = &((egis_image_t*)image)->quality;
    identify_data->result = FP_LIB_IDENTIFY_NO_MATCH;
    identify_data->score = 0;
    identify_data->try_match_count = 0;
    identify_data->last_tried_index = -1;
    pSdk->isLearningUpdate = FALSE;
    pSdk->matched_bioidx = -1;
    pSdk->matched_score = 0;
    g_matched_timing = 0;
    img_array_checksum_reset();
    if (image->frame_count <= 0) {
        egislog_e("frame_count %d is wrong", image->frame_count);
        return FP_LIB_ERROR_PARAMETER;
    }

#if G3PLUS_MATCHER
    int useMode2 = core_config_get_int(INI_SECTION_VERIFY, KEY_CONFIG_MATCH_MODE2, 1);
    int temperature;
    isensor_get_int(PARAM_INT_TEMPERATURE, &temperature);
    BOOL is_low_temp = (temperature < 10 ? TRUE : FALSE);
    if (useMode2) {
        if (!is_low_temp) {
            egislog_d("Mode2 off");
            set_algo_config(ALGO_CONFIG_MATCH_LOW_TEMP, 0);
        } else {
            egislog_d("Mode2 on");
            set_algo_config(ALGO_CONFIG_MATCH_LOW_TEMP, 1);
        }
    }
#endif
#ifdef RBS_EVTOOL
    _set_circle_center();
#endif

    // extract
    feature_extract_result_t ext_result = {
        .ext_feat_retval = exNotImplement,
        .ext_feat_quality = -1,
    };
    unsigned char* mfeature_buf;
    int mfeature_size;

#ifdef RBS_EVTOOL
    for (try_index = 0; try_index < image->frame_count; try_index++) {
        if (try_index <= 1) {
            g_this_image_use_quick_capture = TRUE;
        } else {
            g_this_image_use_quick_capture = FALSE;
        }
#else
    for (try_index = image->frame_count - 1; try_index >= 0; try_index--) {
#endif
#ifdef EGIS_IMAGE_V2
        egis_image_set_keep_param_index(try_index);
#endif
        TIME_MEASURE_START(identify_part);
        uint8_t* img_ptr = egis_image_get_pointer(egis_image, try_index);
#ifdef MATCHER_TAKE_RAW_IMAGE
        img_ptr = egis_image_get_pointer_raw(egis_image, try_index);
        uint16_t* rawimg = (uint16_t*)img_ptr;
#endif
        mfeature_size = MAX_FEATURE_LEN;
        egislog_v("try_match %d, [%d](%p)", i++, try_index, img_ptr);
#if defined(RBS_EVTOOL) || !defined(__ET7XX__)
        ext_result.ext_feat_retval =
            mfeature_extract(try_index, img_ptr, image->format.width, image->format.height);
#else
        ext_result.ext_feat_retval = mfeature_get_result(try_index, MFEAT_FEATURE_EXTRACT_RET);
#endif
        ext_result.ext_feat_quality = mfeature_get_result(try_index, MFEAT_RESULT_EQTY);
        mfeature_size = mfeature_get_result(try_index, MFEAT_FEATURE_LENGTH);
        mfeature_buf = mfeature_get_feature_buf(try_index);

        egislog_d("extract_feature size = %d ext_feat_quality = %d exret = %d", mfeature_size,
                  ext_result.ext_feat_quality, ext_result.ext_feat_retval);

#ifdef G3PLUS_MATCHER
        if (g_verify_config.enable_sratch_mask && ext_result.ext_feat_retval == exOK) {
            detect_scratch_do_mask(mfeature_buf, &mfeature_size, image->format.width,
                                   image->format.height);
        }
#endif
        pSdk->image_qty = EGIS_IMAGE_QUALITY_GOOD;
        if (ext_result.ext_feat_retval == exOK) {
            pSdk->image_qty |= EGIS_IMAGE_QUALITY_GOOD;
            //pSdk->enrolled_cnt++;
        } else {
            pSdk->image_qty |= EGIS_IMAGE_QUALITY_PROCESS_FAILED;
        }
        egislog_d("egis_alg_extract image_qty = 0x%x enrolled_cnt = %d", pSdk->image_qty,
                  pSdk->enrolled_cnt);

        retval = _do_verify(pSdk, ext_result, try_index, mfeature_buf, mfeature_size);
        TIME_MEASURE_STOP(identify_part, "do_verify");

        *candidate_index = pSdk->matched_bioidx;
        identify_data->extract_result = ext_result;
        identify_data->index = pSdk->matched_bioidx;
        identify_data->score = pSdk->matched_score;

        identify_data->quality = capture_quality->quality;
        identify_data->coverage = capture_quality->coverage;

        identify_data->try_match_count++;
        identify_data->last_tried_index = try_index;

        EGIS_IMAGE_KEEP_PARAM(match_score, identify_data->score);
        EGIS_IMAGE_KEEP_PARAM(is_learning, pSdk->isLearningUpdate);
        EGIS_IMAGE_KEEP_PARAM(algo_flag, pSdk->matched_bioidx);
        if (retval == FP_MATCHFAIL) {
            identify_data->result = FP_LIB_IDENTIFY_NO_MATCH;
            EGIS_IMAGE_KEEP_PARAM(try_match_result, VERIFY_TRY_MATCH_RESULT_LAST_NOT_MATCH);
#ifdef EGIS_IMAGE_V2
            if (try_index > 0) {
                egis_image_set_keep_param_index(try_index - 1);
                EGIS_IMAGE_KEEP_PARAM(try_match_result, VERIFY_TRY_MATCH_RESULT_NOT_MATCH);
                egis_image_set_keep_param_index(try_index);
            }
#endif
            retval = FP_LIB_OK;
        } else if (retval == EGIS_OK) {
            identify_data->result = FP_LIB_IDENTIFY_MATCH;
            EGIS_IMAGE_KEEP_PARAM(try_match_result, FP_LIB_IDENTIFY_MATCH);
            // Workaround: only check the first image, in case the image buffer is not continous
            img_array_checksum_update(egis_image_get_pointer(egis_image, 0),
                                      image->format.width * image->format.height * 1, try_index);
            retval = FP_LIB_OK;
            break;
        } else {
#ifdef G3PLUS_MATCHER
            if (ext_result.ext_feat_retval == FP_RESIDUAL_DETECT) {
                identify_data->result = FP_LIB_IDENTIFY_RESIDUAL;
                retval = FP_LIB_OK;
            } else
#endif
            {
                egislog_e("FP_LIB_FAIL_IDENTIFY_IMAGE");
                identify_data->result = FP_LIB_IDENTIFY_NO_MATCH;
                retval = FP_LIB_FAIL_IDENTIFY_IMAGE;
            }
        }

        if (!g_verify_config.enable_try_match) {
            break;
        }
    }

    TIME_MEASURE_STOP(identify_total, "identify total");
    egislog_d(
        "egis_fp_identifyImage_identify matched_bioidx = %d matched_score "
        "= %d is_updated = %d retval = %d",
        pSdk->matched_bioidx, pSdk->matched_score, pSdk->isLearningUpdate, retval);

    return retval;
}
// candidate_templates
// updated_template

int egis_fp_identifyUpdateTemplate(void** unpack_tplt, uint8_t* isTemplateUpdate) {
    int retval = EGIS_OK;

    if (isTemplateUpdate == NULL) {
        egislog_e("isTemplateUpdate : null pointer");
        return FP_LIB_ERROR_PARAMETER;
    }

    if (g_verify_info.matchindex < 0) {
        *isTemplateUpdate = 0;
        egislog_d("%s, Update=%d", __func__, *isTemplateUpdate);
        return FP_LIB_OK;
    }

#if defined(G3PLUS_MATCHER) || defined(ALGO_GEN_5) || defined(ALGO_GEN_7)
    egislog_d("%s, matchindex %d", __func__, g_verify_info.matchindex);
    if (g_verify_config.enable_delay_learning && g_verify_info.matchindex >= 0) {
        egislog_d("delay learning matchindex = %d", g_verify_info.matchindex);
        retval = learning(&g_verify_info, g_verify_info.matchindex);
        egislog_d("delay learning (%d) size=%d ret=%d", g_verify_info.isLearningUpdate,
                  g_verify_info.enroll_temp_size, retval);
        if (retval == EGIS_OK && g_verify_info.isLearningUpdate) {
            g_verify_info.isLearningUpdate = LEARN_UPDATE_UPDATE_TEMPLATE;
#ifndef ALGO_GEN_7
            int has_black_edge = EGIS_IMAGE_OBTAIN_PARAM(black_edge);
            egislog_d("has_black_edge %d", has_black_edge);
            if (has_black_edge > 0) {
                g_verify_info.isLearningUpdate = LEARN_UPDATE_SDK_REJECT;
            }
#endif
        } else {
            g_verify_info.isLearningUpdate = 0;
        }
    }
#endif
    if (g_verify_info.isLearningUpdate == 0 ||
        g_verify_info.isLearningUpdate == LEARN_UPDATE_SDK_REJECT) {
        *isTemplateUpdate = 0;
    } else {
        *isTemplateUpdate = 1;
    }
    egislog_d("%s, (%d) matchindex %d", __func__, *isTemplateUpdate, g_verify_info.matchindex);
    if (retval == EGIS_OK && *isTemplateUpdate && g_verify_info.enroll_temp_size > 0) {
        int matchindex = g_verify_info.matchindex;
        fp_lib_template_t* templ = *unpack_tplt;
        templ->size = g_verify_info.enroll_temp_size;
        templ->tpl = plat_realloc(templ->tpl, templ->size);
        memcpy(templ->tpl, g_verify_info.pEnroll_temp, templ->size);
        egislog_d("templ size = %d", templ->size);
    }
#ifdef ALGO_GEN_5
    // you do not need to free G5's updated template buffer
    g_verify_info.pEnroll_temp = NULL;
#endif
    EGIS_IMAGE_KEEP_PARAM(is_learning, g_verify_info.isLearningUpdate);
    return FP_LIB_OK;
}

//#if defined(ALGO_GEN_7)
//int egis_fp_identifyUpdateNotMatchTemplate(void** unpack_tplt, uint8_t* isTemplateUpdate,
//                                           int index) {
//    int retval = EGIS_OK;
//
//    if (isTemplateUpdate == NULL) {
//        egislog_e("isTemplateUpdate : null pointer");
//        return FP_LIB_ERROR_PARAMETER;
//    }
//
//    if (index < 0 || index > MAX_ENROLL_FINGERS) {
//        egislog_e("invalid index = %d", index);
//    }
//
//    VERIFY_INFO verify_info;
//    memset(&verify_info, 0, sizeof(VERIFY_INFO));
//    verify_info.matchindex = index;
//    verify_info.pEnroll_temp = plat_alloc(MAX_ENROLL_TEMPLATE_LEN);
//
//    egislog_d("%s, g_verify_info.matchindex %d verify_info.matchindex %d", __func__,
//              g_verify_info.matchindex, verify_info.matchindex);
//    if (g_verify_config.enable_delay_learning) {
//        egislog_d("matchindex %d", verify_info.matchindex);
//        learning(&verify_info, -1);
//        egislog_d("verify_info isLearningUpdate = %d enroll_temp_size = %d",
//                  verify_info.isLearningUpdate, verify_info.enroll_temp_size);
//        // if (verify_info.isLearningUpdate == 0)
//        //	EGIS_IMAGE_KEEP_PARAM(is_learning, 0);
//    }
//    egislog_v("isLearningUpdate = %d", verify_info.isLearningUpdate);
//    if (retval == EGIS_OK && verify_info.isLearningUpdate && verify_info.enroll_temp_size > 0) {
//        // EGIS_IMAGE_KEEP_PARAM(is_learning, 11);
//        // int matchindex = verify_info.matchindex;
//        fp_lib_template_t* templ = *unpack_tplt;
//        templ->size = verify_info.enroll_temp_size;
//        templ->tpl = plat_realloc(templ->tpl, templ->size);
//        memcpy(templ->tpl, verify_info.pEnroll_temp, templ->size);
//        egislog_d("templ size = %d", templ->size);
//    }
//
//    *isTemplateUpdate = verify_info.isLearningUpdate ? 1 : 0;
//
//    if (verify_info.pEnroll_temp) plat_free(verify_info.pEnroll_temp);
//    egislog_d("%s, Update=%d", __func__, *isTemplateUpdate);
//    return FP_LIB_OK;
//}
//#endif
// updated_template

int egis_fp_startEnroll(void) {
    g_has_enroll_set_db = 0;
    int retval = FP_LIB_OK;
    void* enroll_context = NULL;
    egislog_d("GIT_SHA1 = UNKNOWN");

    if (pSdk->enroll_ctx != NULL) {
        egislog_d("calling enroll_uninit");
        enroll_uninit(pSdk->enroll_ctx);
        pSdk->enroll_ctx = NULL;
    }
    if (pSdk->identify_session == TRUE) {
        do_verify_uninit();
    }
    g_enroll_partial_threshold = core_config_get_int(
        INI_SECTION_ENROLL, KEY_ENROLL_PARTIAL_THRESHOLD, INID_ENROLL_PARTIAL_THRESHOLD);
    g_duplicate_enroll = 0;

    //
    //	Init enroll context
    //
    g_enroll_config.enroll_method =
        core_config_get_int(INI_SECTION_ENROLL, KEY_ENROLL_METHOD, ENROLL_METHOD_TOUCH);
    switch (g_enroll_config.enroll_method) {
        case ENROLL_METHOD_SWIPE:
            g_enroll_config.enroll_max_count =
                core_config_get_int(INI_SECTION_ENROLL, KEY_MAX_ENROLL_COUNT, 50);
            break;
        case ENROLL_METHOD_PAINT:
            g_enroll_config.enroll_max_count =
                core_config_get_int(INI_SECTION_ENROLL, KEY_MAX_ENROLL_COUNT, 80);
            break;
        case ENROLL_METHOD_TOUCH:
#ifdef __ET7XX__
#ifdef G3PLUS_MATCHER
            g_enroll_config.enroll_max_count =
                core_config_get_int(INI_SECTION_ENROLL, KEY_MAX_ENROLL_COUNT, 15);
#else //G3PLUS_MATCHER
#ifdef LARGE_AREA
            g_enroll_config.enroll_max_count =
                core_config_get_int(INI_SECTION_ENROLL, KEY_MAX_ENROLL_COUNT, INID_MAX_ENROLL_COUNT);
#else //LARGE_AREA
            g_enroll_config.enroll_max_count =
                core_config_get_int(INI_SECTION_ENROLL, KEY_MAX_ENROLL_COUNT, 17);
#endif //LARGE_AREA
#endif //G3PLUS_MATCHER
#else //__ET7XX__
            g_enroll_config.enroll_max_count =
                core_config_get_int(INI_SECTION_ENROLL, KEY_MAX_ENROLL_COUNT, 12);
#endif //__ET7XX__
            break;
        default:
            g_enroll_config.enroll_max_count = 20;
            break;
    }
    _set_circle_center();
#ifdef G3_MATCHER
    set_algo_config(ALGO_CONFIG_INIT_MAX_ENROLL, g_enroll_config.enroll_max_count);

    enroll_context = enroll_init();
    if (enroll_context == NULL) {
        egislog_e("egis_alg_begin_enroll, enroll_context init Failed");
        retval = FP_LIB_ERROR_GENERAL;
        goto exit;
    }

    egislog_d("egis_alg_begin_enroll,enroll_context init");

    g_enroll_config.enroll_method =
        core_config_get_int(INI_SECTION_ENROLL, KEY_ENROLL_METHOD, ENROLL_METHOD_TOUCH);

    g_enroll_config.enroll_select_threshold =
        core_config_get_int(INI_SECTION_ENROLL, KEY_ENROLLEX_SELECT_THRESHOLED, 5);
    g_enroll_config.switch_horizontal =
        core_config_get_int(INI_SECTION_ENROLL, KEY_SWITCH_HORIZONTAL, 0);
    g_enroll_config.switch_vertical =
        core_config_get_int(INI_SECTION_ENROLL, KEY_SWITCH_VERTICAL, 0);
    g_enroll_config.switch_x_y = core_config_get_int(INI_SECTION_ENROLL, KEY_SWITCH_XY, 0);

    if (g_enroll_config.enroll_method == ENROLL_METHOD_SWIPE) {
        egislog_d("egis_alg_begin_enroll,enroll_context init ENROLL_MODE_SWIPE");
        g_enroll_config.enroll_too_fast_rollback = core_config_get_int(
            INI_SECTION_ENROLL, KEY_ENROLLEX_TOO_FAST_ROLLBACK, FP_ENOLLEX_TOO_FAST_RET_MSG);
        g_enroll_config.enroll_too_fast_level =
            core_config_get_int(INI_SECTION_ENROLL, KEY_ENROLLEX_TOO_FAST_LEVEL, 7);
        g_enroll_config.swipe_dir =
            core_config_get_int(INI_SECTION_ENROLL, KEY_SWIPE_DIRECTION_MODE, SWIPE_DIRECTION_AUTO);
        g_enroll_config.swipe_count_y =
            core_config_get_int(INI_SECTION_ENROLL, KEY_SWIPE_COUNT_Y, 1);
        g_enroll_config.swipe_count_x =
            core_config_get_int(INI_SECTION_ENROLL, KEY_SWIPE_COUNT_X, DEFAULT_SWIPE_COUNT_X);

        set_enroll_context(enroll_context, ENROLL_CTX_ENROLL_POLICY,
                           FP_ENROLL_POLICY_SWIPE_ENROLL_V1);
        set_enroll_context(enroll_context, ENROLL_CTX_MAX_ENROLL_COUNT,
                           g_enroll_config.enroll_max_count);
        set_enroll_context(enroll_context, ENROLL_CTX_ENROLLEX_SWIPE_COUNT_Y,
                           g_enroll_config.swipe_count_y);
        set_enroll_context(enroll_context, ENROLL_CTX_ENROLLEX_SWIPE_COUNT_X,
                           g_enroll_config.swipe_count_x);
        set_enroll_context(enroll_context, ENROLL_CTX_ENROLLEX_METHOD,
                           FP_ENOLLEX_METHOD_ADD_ENROLL);
        set_enroll_context(enroll_context, ENROLL_CTX_ENROLLEX_SWITCH_VERTICAL,
                           g_enroll_config.switch_vertical);
        set_enroll_context(enroll_context, ENROLL_CTX_ENROLLEX_SWITCH_HORIZONTAL,
                           g_enroll_config.switch_horizontal);
        set_enroll_context(enroll_context, ENROLL_CTX_ENROLLEX_SWITCH_XY,
                           g_enroll_config.switch_x_y);
        set_enroll_context(enroll_context, ENROLL_CTX_ENROLLEX_SELECT_THRESHOLD,
                           g_enroll_config.enroll_select_threshold);
        set_enroll_context(enroll_context, ENROLL_CTX_ENROLLEX_TOO_FAST_LEVEL,
                           g_enroll_config.enroll_too_fast_level);
        set_enroll_context(enroll_context, ENROLL_CTX_ENROLLEX_TOO_FAST_METHOD,
                           g_enroll_config.enroll_too_fast_rollback);

        if (SWIPE_DIRECTION_X == g_enroll_config.swipe_dir) {
            pSdk->swipe_info.swipe_dir = 1;  // 1: paint enroll x direction
        } else if (SWIPE_DIRECTION_Y == g_enroll_config.swipe_dir) {
            pSdk->swipe_info.swipe_dir = 2;  // 2: paint enroll y direction
        } else {
            pSdk->swipe_info.swipe_dir = 0;  // 0: paint enroll auto direction
        }
    } else if (g_enroll_config.enroll_method == ENROLL_METHOD_PAINT) {
        egislog_d("egis_alg_begin_enroll,enroll_context init ENROLL_MODE_PAINT");
        g_enroll_config.enroll_too_fast_level =
            core_config_get_int(INI_SECTION_ENROLL, "ENROLLEX_TOO_FAST_LEVEL", 50);

        set_enroll_context(enroll_context, ENROLL_CTX_ENROLL_POLICY,
                           FP_ENROLL_POLICY_SWIPE_ENROLL_V2);
        set_enroll_context(enroll_context, ENROLL_CTX_MAX_ENROLL_COUNT,
                           g_enroll_config.enroll_max_count);
        set_enroll_context(enroll_context, ENROLL_CTX_ENROLLEX_METHOD,
                           FP_ENOLLEX_METHOD_EXTRACT_EARLY);
        set_enroll_context(enroll_context, ENROLL_CTX_ENROLLEX_SWITCH_VERTICAL,
                           g_enroll_config.switch_vertical);
        set_enroll_context(enroll_context, ENROLL_CTX_ENROLLEX_SWITCH_HORIZONTAL,
                           g_enroll_config.switch_horizontal);
        set_enroll_context(enroll_context, ENROLL_CTX_ENROLLEX_SWITCH_XY,
                           g_enroll_config.switch_x_y);
        set_enroll_context(enroll_context, ENROLL_CTX_ENROLLEX_SELECT_THRESHOLD,
                           g_enroll_config.enroll_select_threshold);
        set_enroll_context(enroll_context, ENROLL_CTX_ENROLLEX_TOO_FAST_LEVEL,
                           g_enroll_config.enroll_too_fast_level);
        set_enroll_context(enroll_context, ENROLL_CTX_ENROLLEX_TOO_FAST_METHOD,
                           FP_ENOLLEX_TOO_FAST_RET_MSG);

    } else if (g_enroll_config.enroll_method == ENROLL_METHOD_TOUCH) {  // NORMAL_ENROLL
        egislog_d("egis_alg_begin_enroll,enroll_context init ENROLL_METHOD_TOUCH");
        g_enroll_config.enroll_redundant_image_policy = core_config_get_int(
            INI_SECTION_ENROLL, KEY_REDUNDANT_IMAGE_POLICY, FP_REDUNDANT_POLICY_REJECT_IMAGE);
        //g_enroll_config.enroll_extra_1st_enable =
        //    core_config_get_int(INI_SECTION_ENROLL, KEY_ENROLL_EXTRA_THE_FIRST_ENABLE,
        //                        INID_ENROLL_EXTRA_THE_FIRST_ENABLE);
        g_enroll_config.enroll_extra_1st_enable = 0;
#ifdef LARGE_AREA
        g_enroll_config.enroll_extra_1st_before_progress =
            core_config_get_int(INI_SECTION_ENROLL, KEY_ENROLL_EXTRA_THE_FIRST_BEFORE_PROGRESS, 90);
#else
        g_enroll_config.enroll_extra_1st_before_progress =
            core_config_get_int(INI_SECTION_ENROLL, KEY_ENROLL_EXTRA_THE_FIRST_BEFORE_PROGRESS, 40);
#endif

        // Redundant settings
        g_enroll_config.enroll_redundant_level =
            core_config_get_int(INI_SECTION_ENROLL, KEY_REDUNDANT_LEVEL, 10);
        g_enroll_config.enroll_redundant_method = 1;
#ifdef G3PLUS_MATCHER
        g_enroll_config.enroll_redundant_method = core_config_get_int(
            INI_SECTION_ENROLL, KEY_REDUNDANT_METHOD, G3PLUS_ENROLL_REDUNDANT_METHOD);
#endif
        if (g_enroll_config.enroll_redundant_method == G3PLUS_ENROLL_REDUNDANT_METHOD &&
            g_enroll_config.enroll_redundant_image_policy == FP_REDUNDANT_POLICY_ACCPET_IMAGE) {
            egislog_d("redundant method -1 to make Enroll accept");
            set_enroll_context(enroll_context, ENROLL_CTX_REDUNDANT_METHOD, -1);
        } else {
            set_enroll_context(enroll_context, ENROLL_CTX_REDUNDANT_METHOD,
                               g_enroll_config.enroll_redundant_method);
        }
        if (g_enroll_config.enroll_redundant_method == G3PLUS_ENROLL_REDUNDANT_METHOD) {
#ifdef G3PLUS_MATCHER
            // REDUNDANT_SINGLE_LEVEL 1st~9th enroll (value: 0~19)
            set_enroll_context(enroll_context, ENROLL_CTX_REDUNDANT_SINGLE_LEVEL,
                               (g_enroll_config.enroll_redundant_level >= 19)
                                   ? 19
                                   : g_enroll_config.enroll_redundant_level);
            // REDUNDANT_PERCENT_LEVEL 10th~  enroll (value: 0~19)
            set_enroll_context(enroll_context, ENROLL_CTX_REDUNDANT_PERCENT_LEVEL,
                               (g_enroll_config.enroll_redundant_level * 2 >= 19)
                                   ? 19
                                   : g_enroll_config.enroll_redundant_level * 2);
#else
            egislog_e("enroll redundant method %d ! not supported", G3PLUS_ENROLL_REDUNDANT_METHOD);
#endif
        } else {
            set_enroll_context(enroll_context, ENROLL_CTX_REDUNDANT_LEVEL,
                               g_enroll_config.enroll_redundant_level);
        }
        set_enroll_context(enroll_context, ENROLL_CTX_ENROLL_POLICY, FP_ENROLL_POLICY_TOUCH_ENROLL);
        set_enroll_context(
            enroll_context, ENROLL_CTX_MAX_ENROLL_COUNT,
            g_enroll_config.enroll_max_count >= 8 ? g_enroll_config.enroll_max_count : 8);
        set_enroll_context(enroll_context, ENROLL_CTX_REDUNDANT_IMAGE_POLICY,
                           g_enroll_config.enroll_redundant_image_policy);
    } else {
        retval = FP_LIB_ERROR_PARAMETER;
        goto exit;
    }

    pSdk->swipe_info.similarity_score = 0;
#ifdef ENROLL_POLICY_Y6
    enroll_policy_init(g_enroll_config.enroll_method, enroll_context);
#endif

#else

#ifdef ALGO_GEN_4
    retval = algorithm_initialization_for_faceplus();
    if (retval != EGIS_OK) {
        egislog_e("%s algorithm_initialization_for_faceplus failed", __func__);
        retval = FP_LIB_ERROR_GENERAL;
        goto exit;
    }
#endif
    enroll_context = enroll_init();
    if (enroll_context == NULL) {
        egislog_e("egis_alg_begin_enroll, enroll_context init Failed");
        retval = FP_LIB_ERROR_GENERAL;
        goto exit;
    }
    egislog_d("egis_alg_begin_enroll,enroll_context init");

#ifdef __ET7XX__
    g_enroll_config.enroll_redundant_image_policy = core_config_get_int(
        INI_SECTION_ENROLL, KEY_REDUNDANT_IMAGE_POLICY, FP_REDUNDANT_POLICY_REJECT_IMAGE);
#ifdef LARGE_AREA
    g_enroll_config.enroll_redundant_level =
        core_config_get_int(INI_SECTION_ENROLL, KEY_REDUNDANT_LEVEL, INID_REDUNDANT_LEVEL);
#else
    g_enroll_config.enroll_redundant_level =
        core_config_get_int(INI_SECTION_ENROLL, KEY_REDUNDANT_LEVEL, 7);
#endif
    set_enroll_context(enroll_context, ENROLL_CTX_REDUNDANT_IMAGE_POLICY,
                       g_enroll_config.enroll_redundant_image_policy);
    set_enroll_context(enroll_context, ENROLL_CTX_REDUNDANT_LEVEL,
                       g_enroll_config.enroll_redundant_level);
    set_enroll_context(enroll_context, ENROLL_CTX_MAX_ENROLL_COUNT,
                       g_enroll_config.enroll_max_count);
#if defined(ALGO_GEN_7)
    set_enroll_context(enroll_context, ENROLL_CTX_MAX_ENROLL_COUNT,
                       g_enroll_config.enroll_max_count);
    set_enroll_context(enroll_context, ENROLL_CTX_REDUNDANT_CHECK_NUMBER,
                       0);
    set_enroll_context(enroll_context, ENROLL_CTX_REDUNDANT_LEVEL,
                       100);
#endif
#endif

#endif

    pSdk->enroll_ctx = enroll_context;
    pSdk->max_enroll_cnt = 0;
    pSdk->enroll_result = 0;
    pSdk->percentage = 0;

    egislog_d("egis_alg_begin_enroll, end");

    g_enroll_flag = TRUE;

    return FP_LIB_OK;
exit:
    egislog_d("egis_fp_startEnroll retval = %d", retval);

    _verify_info_uninit();

    if (enroll_context) {
        enroll_uninit(enroll_context);
    }

    return retval;
}

int egis_fp_updateEnrolData(fp_lib_enroll_data_t* fp_enrol_data, egis_image_t* image) {
    int retval = FP_LIB_OK;

    egislog_d("William v0, egis_fp_updateEnrolData");

    if (fp_enrol_data == NULL || image == NULL) {
        egislog_e("egis_fp_updateEnrolData null pointer");
        return FP_LIB_ERROR_PARAMETER;
    }

    fp_image_quality_t* capture_quality = &((egis_image_t*)image)->quality;

    if (g_enroll_config.enroll_method == ENROLL_METHOD_TOUCH) {
        if (FP_LIB_ENROLL_SUCCESS != capture_quality->reject_reason) {
            fp_enrol_data->result = capture_quality->reject_reason;
            egislog_d("%s, reject reason = %d", __func__, capture_quality->reject_reason);
            retval = FP_LIB_OK;
            goto reject;
        }
    }

    if (pSdk->has_been_duplicate_check == 0) {
        retval = egis_fp_identifyImage_enroll(image, fp_enrol_data, NULL, 0, NULL);
    }

    pSdk->has_been_duplicate_check = 0;
    if (pSdk->percentage == 100) {
        // pSdk->enroll_result = FP_MERGE_ENROLL_FINISH;
        egislog_d("egis_fp_updateEnrolData,!!! Enroll_Finish !!!");

        // pSdk->final_output_template =
        // get_enroll_template(pSdk->enroll_ctx,
        // &(pSdk->final_output_size));
        // pSdk->final_output_size = final_size;

        // egislog_d("fp_device_state_handler,Enrollment Temp_size =[ %d
        // ] ", pSdk->final_output_size);
    }

    egislog_d("egis_fp_updateEnrolData retval = %d", retval);
    egislog_d("egis_fp_updateEnrolData fp_enrol_data->result = %d", pSdk->fp_enroll_result);

    fp_enrol_data->result = pSdk->fp_enroll_result;
    fp_enrol_data->guide_direction = ENROLL_DIRECTION_NA;
    if (pSdk->enroll_result >= 0)
        fp_enrol_data->nr_successful++;
    else
        fp_enrol_data->nr_failed++;
    fp_enrol_data->progress = pSdk->percentage;

reject:
    fp_enrol_data->quality = capture_quality->quality;
    fp_enrol_data->coverage = capture_quality->coverage;

    // convert_retval(&retval, retval);
    egislog_d("egis_fp_updateEnrolData retval = %d", retval);
    egislog_d("fp_enrol_data->quality : %u", fp_enrol_data->quality);
    egislog_d("fp_enrol_data->coverage : %u", fp_enrol_data->coverage);

    return retval;
}

int egis_fp_mergeEnrollTemp(fp_lib_enroll_data_t* enrol_data, egis_image_t* image) {
#ifdef G3_MATCHER
    enrol_data->result = pSdk->fp_enroll_result;
    enrol_data->progress = pSdk->percentage;

    if (pSdk->enroll_result != FP_MERGE_ENROLL_FINISH &&
        g_enroll_config.enroll_method != ENROLL_METHOD_TOUCH) {
        egislog_d("pSdk->percentage = %d, pSdk->enroll_result = %d", pSdk->percentage,
                  pSdk->enroll_result);
#ifdef G3PLUS_MATCHER
        pSdk->enroll_result =
            enroll(ENROLL_EX_FINGER_OFF, image->img_data.format.width,
                   image->img_data.format.height, pSdk->enroll_ctx, &pSdk->percentage);
#else
        pSdk->enroll_result = enroll_ex(
            ENROLL_EX_FINGER_OFF, image->img_data.format.width, image->img_data.format.height,
            pSdk->enroll_ctx, &pSdk->percentage, MERGE_MAP_WIDTH, MERGE_MAP_HEIGHT, NULL, NULL,
            &pSdk->swipe_info.similarity_score, &pSdk->swipe_info.swipe_dir);
#endif

        egislog_d("paint_enroll NULL retval = %d , percentage %d", pSdk->enroll_result,
                  pSdk->percentage);
        if (pSdk->enroll_result == FP_DUPLICATE)
            enrol_data->result = FP_LIB_ENROLL_HELP_ALREADY_EXIST;
        else if (pSdk->enroll_result == FP_MERGE_ENROLL_TOO_FAST)
            enrol_data->result = FP_LIB_ENROLL_TOO_FAST;

        enrol_data->progress = pSdk->percentage;
    }
#endif

    return FP_LIB_OK;
}

int egis_fp_setFingerState(unsigned int finger_on) {
#ifdef G3_MATCHER
    if (finger_on && g_enroll_config.enroll_method != ENROLL_METHOD_TOUCH) {
        set_finger_on(ENROLL_OPTION_FINGERON);
        egislog_d("egis_fp_setFingerState set finger on");
    }
#endif
    return FP_LIB_OK;
}
// int egis_fp_finishEnroll(fp_libtemplate_t* template_t, void* unpack_tplt);
int egis_fp_finishEnroll(fp_lib_template_t* template_t) {
    int retval = FP_LIB_OK;
    unsigned char* template_algo = NULL;
    int template_algo_len = 0;
    g_enroll_flag = FALSE;
    egislog_i("egis_fp_finishEnroll start");

    if (template_t == NULL && pSdk->enroll_result == FP_MERGE_ENROLL_FINISH) {
        egislog_e("egis_fp_finishEnroll null pointer");
        return FP_LIB_ERROR_PARAMETER;
    }

    egislog_d("egis_alg_end_enroll pSdk = %p enroll_result = %d", pSdk, pSdk->enroll_result);

    switch (pSdk->enroll_result) {
        // case	FP_ENROLL_PARCTICE_FINISH:
        case FP_MERGE_ENROLL_FINISH:
            template_algo = get_enroll_template(pSdk->enroll_ctx, &template_algo_len);
            /*the default value of template_t->size is
             * MAX_ENROLL_TEMPLATE_LEN at present */
            if (template_algo == NULL || template_algo_len <= 0 ||
                template_algo_len > MAX_ENROLL_TEMPLATE_LEN) {
                egislog_e(
                    "egis_fp_finishEnroll, get_enroll_template "
                    "failed, template_algo_len = %d",
                    template_algo_len);
                retval = FP_TA_ERROR_EROLL_GET_TEMPLATE_FAIL;
                break;
            }

            template_t->size = (unsigned int)template_algo_len;
            if (template_t->tpl == NULL) {
                egislog_e(
                    "egis_fp_finishEnroll, template_t->tpl == "
                    "NULL");
            } else {
                egislog_d(
                    "egis_fp_finishEnroll, template_t->tpl = "
                    "%p",
                    template_t->tpl);
                memcpy(template_t->tpl, template_algo, template_algo_len);
            }
            break;
        case FP_DUPLICATE:
            egislog_d("enroll_result: FP_DUPLICATE");
        case FP_MERGE_ENROLL_IAMGE_OK:
            break;
        case FP_MERGE_ENROLL_FAIL:
        case FP_MERGE_ENROLL_OUT_OF_MEMORY:
        case FP_MERGE_ENROLL_UNKNOWN_FAIL:
        case FP_MERGE_ENROLL_IRREGULAR_CONTEXT:
        default:
            if (pSdk->enroll_result != FP_MERGE_ENROLL_INIT) {
                retval = FP_TA_ERROR_EROLL_NOT_COMPLETED;
                egislog_e("enroll failed: Context  need to be re-initialize: %d",
                          pSdk->enroll_result);
            }
            break;
    }

    _verify_info_uninit();

    if (pSdk->enroll_ctx != NULL) {
        egislog_d("calling enroll_uninit");
        enroll_uninit(pSdk->enroll_ctx);
        pSdk->enroll_ctx = NULL;
    }

    egislog_d("egis_fp_finishEnroll end template_algo_len = %d retval = %d", template_algo_len,
              retval);

    pSdk->enrolled_cnt = 0;
    pSdk->max_enroll_cnt = 0;
    pSdk->fp_enroll_result = FP_LIB_ENROLL_FAIL_NONE;
    return retval;
}
// gettemplate
// fp_packTemplate

int egis_fp_packTemplate(void* unpack_tplt, uint8_t* template_buf) {
    fp_lib_template_t* fp_template_t = (fp_lib_template_t*)unpack_tplt;

    egislog_d("egis_fp_packTemplate start");

    if (template_buf == NULL || unpack_tplt == NULL) {
        egislog_e("egis_fp_packTemplate null pointer");
        return FP_LIB_ERROR_PARAMETER;
    }
    egislog_d("egis_fp_packTemplate fp_template_t = %p size = %d", fp_template_t->tpl,
              fp_template_t->size);

#ifdef PACK_TEMPLATE_BUFFER
#error NotImplemented
#else
    memcpy(template_buf, fp_template_t->tpl, fp_template_t->size);
#endif
    return FP_LIB_OK;
}
int egis_fp_unpackTemplate(fp_lib_template_t* tplt, fp_lib_template_t** candidate_templates,
                           int candidate_index) {
    int retval = FP_LIB_OK;
    fp_lib_template_t* fp_template_t = NULL;

    if (tplt == NULL || candidate_templates == NULL) {
        egislog_e("egis_fp_unpackTemplate null pointer");
        retval = FP_LIB_ERROR_PARAMETER;
        goto exit;
    }
    egislog_d("egis_fp_unpackTemplate size=%d", tplt->size);

    fp_template_t = plat_alloc(sizeof(fp_lib_template_t));
    if (fp_template_t == NULL) {
        egislog_d("egis_fp_unpackTemplate alloc fail");
        retval = FP_LIB_ERROR_PARAMETER;
        goto exit;
    }

    egislog_d("egis_fp_unpackTemplate candidate_index = %d", candidate_index);

    fp_template_t->tpl = plat_alloc(tplt->size);
    if (fp_template_t->tpl == NULL) {
        fp_template_t->size = 0;
        egislog_d("egis_fp_unpackTemplate alloc fail");
        retval = FP_LIB_ERROR_MEMORY;
        goto exit;
    }

#ifdef PACK_TEMPLATE_BUFFER
#error NotImplemented
#else
    memcpy(fp_template_t->tpl, tplt->tpl, tplt->size);
#endif
    fp_template_t->size = tplt->size;
    candidate_templates[candidate_index] = fp_template_t;

    return FP_LIB_OK;
exit:
    if (fp_template_t) {
        if (fp_template_t->tpl) plat_free(fp_template_t->tpl);
        plat_free(fp_template_t);
    }
    candidate_templates[candidate_index] = NULL;
    return retval;
}

int egis_fp_deleteTemplate(fp_lib_template_t** candidate_templates, int candidate_index) {
    // int retval;
    fp_lib_template_t* fp_template_t;

    egislog_d("egis_fp_deleteTemplate");

    if (candidate_templates == NULL) {
        egislog_e("egis_fp_deleteTemplate null pointer");
        return FP_LIB_ERROR_PARAMETER;
    }
    if (candidate_templates[candidate_index] == NULL) {
        egislog_e("candidate_templates[%d] is NULL. Skip delete.", candidate_index);
        return FP_LIB_OK;
    }

    egislog_d("egis_fp_deleteTemplate candidate_index = %d", candidate_index);

    fp_template_t = candidate_templates[candidate_index];
    if (fp_template_t) {
        if (fp_template_t->tpl) plat_free(fp_template_t->tpl);
        plat_free(fp_template_t);
    }
    candidate_templates[candidate_index] = NULL;

    return FP_LIB_OK;
}

void egis_fp_preprocessor_cleanup(void) {
    egislog_d("egis_fp_preprocessor_cleanup");
}

int egis_fp_preprocessor(void* image) {
    int retval = FP_LIB_OK;

    egislog_d("egis_fp_preprocessor void image (%p), retval = %d", image, retval);

    return retval;
}

void egis_fp_getAlgVersion(uint8_t* out_version_buf, uint32_t* out_version_len) {
    ALGO_API_INFO algo_api_version;
    char version[VER_STRING_SIZE] = {0};
    unsigned int api_version_len = 0, matchlib_version_len = 0, version_len = 0;

    egislog_d("%s entry", __func__);
    if (out_version_buf == NULL || out_version_len == NULL) {
        egislog_e("get_version receive NULL Buffer");
        return;
    }
#ifdef ALGO_GEN_4
    algorithm_do_other_for_faceplus(FP_ALGOAPI_GET_VERSION, NULL, (BYTE*)&algo_api_version);
#else
    algorithm_do_other(FP_ALGOAPI_GET_VERSION, NULL, (BYTE*)&algo_api_version);
#endif
    api_version_len = strlen(algo_api_version.algo_api_version);
    matchlib_version_len = strlen(algo_api_version.matchlib_version);

    memcpy(version, algo_api_version.algo_api_version, api_version_len);
    version[api_version_len] = ',';
    memcpy(version + api_version_len + 1, algo_api_version.matchlib_version,
           matchlib_version_len + 1);

    version_len = strlen(version);
    egislog_d("algorithm version : %s", version);

    if (*out_version_len >= (version_len + 1)) {
        memcpy(out_version_buf, version, version_len + 1);
        *out_version_len = version_len + 1;
    } else if (*out_version_len >= sizeof(algo_api_version.algo_api_version)) {
        egislog_e("%s, len %d < %d. only use api ver", __func__, *out_version_len, version_len + 1);
        memcpy(out_version_buf, algo_api_version.algo_api_version, api_version_len);
        *out_version_len = api_version_len + 1;
    } else {
        egislog_e("%s, len %d < %d", __func__, *out_version_len, version_len + 1);
        memset(out_version_buf, 0, *out_version_len);
        *out_version_len = 0;
    }
}

int egis_fp_get_enroll_config_max_count() {
    return g_enroll_config.enroll_max_count;
}

BOOL egis_fp_is_enroll() {
    return g_enroll_flag;
}

int egis_fp_getAlgSecurityThr(uint32_t* data) {
    int retval = FP_LIB_OK;

    // convert_retval(&retval, retval);
    egislog_d("egis_fp_getAlgSecurityThr retval = %d", retval);

    return retval;
}
void egis_fp_getTemplateSize(uint32_t* size) {
    *size = MAX_ENROLL_TEMPLATE_LEN;
}
int egis_fp_getAlgoBpp() {
#ifdef ALGO_GEN_4
    return 32;
#endif
    return 8;
}
int egis_fp_set_verify_finger_info(int verify_count, int verified_count)
{
    egislog_d("multi_threshold egis_fp_set_verify_finger_info %d %d",
                            verify_count, verified_count);
    egislog_d("multi_threshold g_matcher_threshold[%d] %d",
                            verified_count, g_matcher_threshold[verified_count]);
    verify_accuracy_set_app_level(g_matcher_threshold[verified_count]);
    return FP_LIB_OK;
}
