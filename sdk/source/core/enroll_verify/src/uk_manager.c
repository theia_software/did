#include "uk_manager.h"
#include "algomodule.h"
#include "core_config.h"
#include "egis_algomodule.h"
#include "egis_fp_get_image.h"
#include "egis_log.h"
#include "egis_sensormodule.h"
#include "fd_process.h"
#include "ini_definition.h"
#include "plat_mem.h"
#include "uk_box.h"
#if defined(ALGO_GEN_7)
#include "GoImageProcessingLib.h"
#endif
#include "object_def_image.h"

#define LOG_TAG "RBS UK"

static uk_box_info_t g_uk_box;
static unsigned char* g_ptr_uk_bkg;
static int g_current_enroll_index;
static int g_max_total_enroll_count;
static BOOL g_ukm_inited = FALSE;

static int fetch_uk_bkg();
static void remove_the_lastest_frame_of_egist_image(egis_image_t* egis_image);

int ukm_init() {
    egislog_d("ukm_init");
    ukm_deinit();
    int img_width, img_height, img_dpi, ret, img_bpp = 16;
    g_ukm_inited = TRUE;
    g_ptr_uk_bkg = NULL;
    g_current_enroll_index = 0;
    g_max_total_enroll_count = egis_fp_get_enroll_config_max_count();
    fp_getImageSize(&img_width, &img_height, &img_dpi);
    ret = uk_box_create(img_width, img_height, img_bpp, UK_BOX_FRAME_COUNT, &g_uk_box);
    return ret;
}

void ukm_deinit() {
    egislog_d("ukm_deinit");
    if (g_ukm_inited == TRUE) {
        g_ukm_inited = FALSE;
        uk_box_destroy(&g_uk_box);
        if (g_ptr_uk_bkg != NULL) {
            plat_free(g_ptr_uk_bkg);
        }
    }
}

BOOL ukm_is_enabled() {
    return core_config_get_int(INI_SECTION_ENROLL, KEY_ENROLL_ENABLE_UK, INID_ENROLL_ENABLE_UK);
}

BOOL ukm_is_inited() {
    return g_ukm_inited;
}

BOOL ukm_is_bkg_ready() {
    return g_uk_box.gen_bkg_complete;
}

int ukm_add_image(unsigned char* img) {
    if (g_uk_box.gen_bkg_complete == TRUE) {
        egislog_e("add_image uk_bkg has been generated. skip this operation");
        return EGIS_INCORRECT_STATUS;
    }
    uk_box_add_image(&g_uk_box, img);
    if (g_uk_box.cur_bkg_num >= UK_BOX_FRAME_COUNT) {
        egislog_d("fetch_uk_bkg");
        fetch_uk_bkg();
#if defined(ALGO_GEN_7)
        go_IPBDS_set_uk(g_ptr_uk_bkg, g_uk_box.width, g_uk_box.height, g_uk_box.bpp);
#endif
    }
    return EGIS_OK;
}

int ukm_get_uk_progress(int real_percentage) {
    int ratio = g_uk_box.max_bkg_num * 100 / g_max_total_enroll_count;
    int ret_percentage =
        ratio * g_uk_box.cur_bkg_num / g_uk_box.max_bkg_num + (100 - ratio) * real_percentage / 100;
    egislog_d(
        "ukm_get_uk_progress ret_percentage = %d, cur_bkg_num %d, max_bkg_num %d, real_percentage "
        "= %d ratio = %d",
        ret_percentage, g_uk_box.cur_bkg_num, g_uk_box.max_bkg_num, real_percentage, ratio);
    return ret_percentage;
}

// this function will return the number of extra delayed_enroll should be executed in each step
int get_delayed_enroll_count(int real_percentage) {
    int ret_count;
    if (ukm_get_uk_progress(real_percentage) >= 100) {
        ret_count = g_uk_box.max_bkg_num - g_current_enroll_index - 1;
    } else {
        ret_count = g_uk_box.max_bkg_num / (g_max_total_enroll_count - g_uk_box.max_bkg_num);
    }
    return ret_count;
}

static void remove_the_lastest_frame_of_egist_image(egis_image_t* egis_image) {
    if (egis_image->img_data.frame_count < 1) {
        return;
    }
#if defined(EGIS_IMAGE_V2)
    rbs_obj_image_v1_0_t* current_param = EGIS_IMAGE_get_param_keeper();
    RBSOBJ_copy_IMAGE_params(current_param, NULL);
    egis_image->img_data.frame_count--;
    int index = egis_image->img_data.frame_count - 1;
    if (index >= 0) {
        egis_image_set_keep_param_index(index);
        EGIS_IMAGE_KEEP_PARAM(is_last_image, 1);
    }
#endif
}

int ukm_do_delayed_enroll(egis_image_t* egis_image, fp_lib_enroll_data_t* enroll_data,
                          int real_percentage, int enrolled_count) {
    egislog_d("ukm_do_delayed_enroll real_percentage %d, g_max_total_enroll_count %d",
              real_percentage, g_max_total_enroll_count);
    if (g_current_enroll_index >= UK_BOX_FRAME_COUNT) {
        egislog_d("ukm_do_delayed_enroll delayed enroll finish");
        return UK_DELAYED_ENROLL_FINISH;
    }

    if (!ukm_is_bkg_ready()) {
        egislog_d("ukm_do_delayed_enroll uk bkg is not ready");
        return UK_BKG_NOT_READY;
    }

    int cur_enroll_index = g_current_enroll_index;
    int raw_image_size = uk_box_get_bkg_size(&g_uk_box);
    int delayed_enroll_count = get_delayed_enroll_count(real_percentage);
    for (int i = 0; i < delayed_enroll_count && cur_enroll_index < (g_uk_box.max_bkg_num - 1);
         i++) {
        // do not need to enroll the latest image again
        // because the uk bkg has been generated after that frame is captured, and then the enroll
        // process executed.  check command_handler EX_CMD_GET_IMAGE_IPP AND EX_CMD_ENROLL for
        // details
        unsigned char* ptr_raw = uk_box_get_raw_image_ptr(&g_uk_box, cur_enroll_index);
        if (ptr_raw != NULL) {
            int retval = egis_captureImage_only_raw((void*)egis_image, ptr_raw, raw_image_size);
            if (retval == EGIS_OK) {
                EGIS_IMAGE_KEEP_PARAM(is_uk_image, 1);
                retval = fp_captureImage(egis_image, 0, FD_PROCESS_OPTION_USE_OLD_RAW);
                egislog_d("ukm_do_delayed_enroll fp_captureImage retval %d frame count = %d",
                          retval, egis_image->img_data.frame_count);
            }
            BOOL skip_img_qty_checking = TRUE;
            if (retval == EGIS_OK || skip_img_qty_checking) {
                enroll_data->enroll_option = ENROLL_OPTION_FORCE_ACCEPT;
                retval = egis_fp_identifyImage_enroll(egis_image, enroll_data, NULL, 0, NULL);
                egislog_d("egis_fp_identifyImage_enroll retval %d", retval);
            }

            // remove the frame from g_img_holder and reset corresponding parameters after delayed
            // enroll
            remove_the_lastest_frame_of_egist_image(egis_image);

            cur_enroll_index++;
            switch (enroll_data->result) {
                case FP_LIB_ENROLL_SUCCESS:
                    egislog_d("ukm_do_delayed_enroll %d delayed enroll finish", cur_enroll_index);
                    break;
                default:
                    egislog_e("ukm_do_delayed_enroll failed to enroll");
                    break;
            }
        }
    }

    g_current_enroll_index = cur_enroll_index;
    return EGIS_OK;
}

int ukm_get_uk_bkg(unsigned char* buffer, int buffer_size) {
    egislog_d("ukm_get_uk_bkg");
    if (!ukm_is_bkg_ready() || g_ptr_uk_bkg == NULL) {
        return EGIS_COMMAND_FAIL;
    }
    int bkg_size = uk_box_get_bkg_size(&g_uk_box);
    if (buffer_size < bkg_size) {
        egislog_e("ukm_get_uk_bkg buffer_size < bkg_size");
        return EGIS_INCORRECT_PARAMETER;
    }
    memcpy(buffer, g_ptr_uk_bkg, bkg_size);
    return EGIS_OK;
}

static int fetch_uk_bkg() {
    int uk_bkg_size, ret;
    if (g_ptr_uk_bkg != NULL) {
        egislog_e("fetch_uk_bkg bkg has been loaded.");
        return EGIS_OK;
    }
    uk_bkg_size = uk_box_get_bkg_size(&g_uk_box);
    g_ptr_uk_bkg = plat_alloc(uk_bkg_size);
    RBS_CHECK_IF_NULL(g_ptr_uk_bkg, EGIS_OUT_OF_MEMORY);
    ret = uk_box_get_bkg(&g_uk_box, g_ptr_uk_bkg, uk_bkg_size);
    return ret;
}
