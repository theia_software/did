#ifndef _UK_BOX_H_
#define _UK_BOX_H_
#include <stdint.h>
#include <stdlib.h>
#include "type_definition.h"

typedef struct {
    int max_bkg_num;
    int cur_bkg_num;
    int width;
    int height;
    int bpp;
    uint32_t* data;
    BOOL gen_bkg_complete;
} uk_box_info_t;

int uk_box_create(int width, int height, int bpp, int max_bkg_num, uk_box_info_t* uk_info);
int uk_box_add_image(uk_box_info_t* bkg_info, void* data);
int uk_box_get_bkg(uk_box_info_t* bkg_info, void* out_buffer, int out_buffer_len);
int uk_box_get_bkg_size(uk_box_info_t* uk_box_info);
int uk_box_destroy(uk_box_info_t* uk_box_info);
unsigned char* uk_box_get_raw_image_ptr(uk_box_info_t* uk_box_info, int index);
#endif
