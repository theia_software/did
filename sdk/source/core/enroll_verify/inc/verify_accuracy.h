#ifndef __VERIFY_ACCURACY_H__
#define __VERIFY_ACCURACY_H__

void verify_accuracy_set_app_level(uint32_t level);
uint32_t verify_accuracy_get_app_level();

#define BOOL int
#ifndef FALSE
#define FALSE 0
#endif
#ifndef TRUE
#define TRUE 1
#endif

/* Far ratio */
#ifdef VENDOR_CS1
#define FAR_RATIO (100 * 1000)
#define FAR_RATIO_10_TIMES (FAR_RATIO * 10)
#define FAR_RATIO_SUNLIGHT_MODE (FAR_RATIO / 10)
#define FAR_RATIO_EASY_MODE (FAR_RATIO / 100)
#define FAR_RATIO_ABNORMAL_TEMP (FAR_RATIO / 100)
#else
#define FAR_RATIO (50 * 1000)
#define FAR_RATIO_FA_PROTECTION (10000000)  // 10M

#define FAR_RATIO_LOW_TEMP_LEVEL1 (5 * 1000)
#define FAR_RATIO_LOW_TEMP_LEVEL2 (10 * 1000)
#define FAR_RATIO_LOW_TEMP_LEVEL3 (50 * 1000)
#endif

#ifdef VENDOR_CS1
//set set_accuracy_level
#define VERIFY_CTX_ACCURACY_LEVEL 1300
typedef enum easy_mode {
    EASY_MODE_OFF = 0,
    EASY_MODE_ON,
    EASY_MODE_DONE
} easy_mode_t;
#else
typedef enum easy_mode {
    EASY_MODE_OFF = 0,
    EASY_MODE_ON,
    EASY_MODE_ON_STAGE_2,
    EASY_MODE_DONE
} easy_mode_t;
#endif

#define EASY_MODE_UPDATE_OK 0
#define EASY_MODE_UPDATE_SKIP_MATCHER -6

#ifdef VENDOR_CS1
enum system_solution_type fcheck_type_of_system_solution(int try_count, int temperature);
int fcheck_get_changing_far_ratio();
void fcheck_clear_system_solution();
void fcheck_reset_special_mode();
void fcheck_set_verify_fail_count();
void _clear_sys_sol_flag();
#endif
void easy_mode_reset();
int  easy_mode_do_update(int try_count, BOOL is_FA_attack, int verify_enroll_number, int temperature);
BOOL easy_mode_is_on();
void verify_accuracy_set_app_level(uint32_t level);
uint32_t verify_accuracy_get_app_level();
unsigned long long easy_mode_get_previous_timestamp();
int easy_mode_get_level(int far_ratio);

#endif
