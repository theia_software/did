#ifndef _ENROLL_POLICY_H_
#define _ENROLL_POLICY_H_

void enroll_policy_init(int enroll_mode, void* enroll_context);
void enroll_policy_update(void* enroll_context, int enroll_result, int enrolled_cnt,
                          int max_enroll_cnt);

#endif
