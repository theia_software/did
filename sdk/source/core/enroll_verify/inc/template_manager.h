
#ifndef __TEMPLATE_MANAGER_H__
#define __TEMPLATE_MANAGER_H__

#include <stdint.h>

#include "fp_algomodule.h"
#include "type_definition.h"

#define TEMPL_ERROR_PARAM -1
#define TEMPL_ERROR_LOAD -2
#define TEMPL_ERROR_SAVE -3
#define TEMPL_ERROR_INVALID_INDEX -4
#define TEMPL_ERROR_DB_FULL -5
#define TEMPL_ERROR_DELETE -6
#define TEMPL_ERROR_RENAME -7

#define TEMPL_ERROR_INVALID_PATH -10
#define TEMPL_ERROR_INVALID_MAX_COUNT -11

/**
 * @brief Set a group id and a file system path for a set of template files
 *
 * @param group_id Group ID
 * @param path The path of the folder storing template files. For example
 * "/data/system/users/0/fpdata"
 * @param prefix The prefix of template file "template"
 * @return EGIS_OK SUCCESS
 */
int template_set_active_group(uint32_t group_id, const char* path, const char* prefix);

/**
 * @brief Set the maximum count of template files
 *
 * @param max_count The maximum count
 * @return EGIS_OK SUCCESS
 */
int template_set_max_count(int max_count);

/**
 * @brief Add a new template. This should be called right after finish Enroll
 *
 * @param fid Fingerprint ID
 * @param buf The buffer pointer
 * @param size The size of the buffer
 * @return EGIS_OK SUCCESS
 */
int template_add(uint32_t fid, unsigned char* buf, unsigned int size);

/**
 * @brief Update a existing template. This should be called after Verify gets
 * MATCH result and
 *  has a new learning template.
 *
 * @param index The index of templates in memory
 * @param buf The buffer pointer
 * @param size The size of the buffer
 * @return EGIS_OK SUCCESS
 */
int template_update(int index, unsigned char* buf, unsigned int size);

/**
 * @brief Delete a existing template with specified fid
 *
 * @param fid Fingerprint ID
 * @return EGIS_OK SUCCESS
 */
int template_delete(uint32_t fid);

/**
 * @brief Delete all templates in current group
 *
 * @return EGIS_OK SUCCESS
 */
int template_delete_all(BOOL also_delete_file);

/**
 * @brief
 *
 * @param [out] fid_list
 * @param [out] enrolled_count
 * @return EGIS_OK SUCCESS
 */
int template_load_all(uint32_t* fid_list, int* enrolled_count);

/**
 * @brief Get the Fingerprint ID from the index of template in memory.
 *     This will be needed when Verify has matched index and wants to return fid
 *
 * @param index The index of templates in memory
 * @return uint32_t Fingerprint ID
 */
uint32_t template_get_fid(int index);

/**
 * @brief Get current enrolled count
 *
 * @return int current enrolled count
 */
int template_get_enrolled_count();

/**
 * @brief free template buffer
 *
 * @return EGIS_OK SUCCESS
 */
int template_uninit();

int fplib_template_create(fp_lib_template_t* fplib_template);
void fplib_template_destroy(fp_lib_template_t* fplib_template);

#endif
