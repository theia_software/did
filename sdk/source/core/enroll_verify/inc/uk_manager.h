#ifndef _UK_MANAGER_H_
#define _UK_MANAGER_H_
#include "algomodule.h"
#include "fp_algomodule.h"
#include "type_definition.h"

#define UK_BOX_FRAME_COUNT 10

// define uk return code
#define UK_BKG_NOT_READY -1
#define UK_DELAYED_ENROLL_FINISH 1000

int ukm_init();
void ukm_deinit();
BOOL ukm_is_enabled();
BOOL ukm_is_inited();
BOOL ukm_is_bkg_ready();
int ukm_add_image(unsigned char* img);
int ukm_do_delayed_enroll(egis_image_t* egis_image, fp_lib_enroll_data_t* enroll_data,
                          int real_percentage, int enrolled_count);
int ukm_get_uk_progress(int real_percentage);
int ukm_get_uk_bkg(unsigned char* buffer, int buffer_size);
#endif
