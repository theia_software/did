#ifndef __TEMPLATE_FILE_H__
#define __TEMPLATE_FILE_H__

#include <stdint.h>

#include "EgisAlgorithmAPI.h"

#define CREAT_ARRAY(pointer, p_type, init_value, count)    \
    if (pointer) {                                         \
        plat_free(pointer);                                \
    }                                                      \
    pointer = (p_type*)plat_alloc(count * sizeof(p_type)); \
    for (i = 0; i < count; i++) {                          \
        pointer[i] = init_value;                           \
    }

#define TEMPL_VER_TEST 0xFF
#define TEMPL_VER_01 0x1
#define TEMPL_VER_G4_00 0x400

uint32_t template_get_version();
#define TEMPL_VER_CURRENT template_get_version()

typedef struct _templeate_file_data {
    uint32_t version;
    uint32_t user_id;
    uint32_t fingerprint_id;
    uint32_t templ_data_size;
    uint8_t templ_data[MAX_ENROLL_TEMPLATE_LEN];
} template_file_data;
#define TEMPLATE_FILE_DATA_HEADER 16

int _template_set_file_path_prefix(const char* path, const char* prefix);

int _template_load_file(int file_index, unsigned char* buf, unsigned int size,
                        unsigned int* real_size);
int _template_save_file(int file_index, uint32_t fid, unsigned char* buf, unsigned int size);
int _template_delete_file(int file_index);

typedef int (*template_custom_save_file)(int file_index, unsigned char* buf, unsigned int len);
typedef int (*template_custom_load_file)(int file_index, unsigned char* buf, unsigned int len,
                                         unsigned int* real_size);
typedef int (*template_custom_remove_file)(int file_index);

void template_set_custom_file_routin(template_custom_save_file fn_save,
                                     template_custom_load_file fn_load,
                                     template_custom_remove_file fn_remove);

#endif
