
typedef struct _matcher_feature_t {
    int feat_length;
    unsigned char* feat_buf;
    int extract_ret;
    int eqty;
    int residual;
    int is_fake;
} matcher_feature_t;

enum mfeature_result_id {
    MFEAT_FEATURE_LENGTH = 1,
    MFEAT_FEATURE_EXTRACT_RET,
    MFEAT_RESULT_EQTY,
    MFEAT_RESULT_RESIDUAL,
    MFEAT_RESULT_IS_FAKE
};

int mfeature_create();
void mfeature_destroy();

int mfeature_extract(int try_index, unsigned char* img, int width, int height);

int mfeature_get_result(int try_index, enum mfeature_result_id result_id);
unsigned char* mfeature_get_feature_buf(int try_indx);