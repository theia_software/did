#include <string.h>

#include "partial_hist.h"
//#include "fileio.h"

#ifdef OUTPUT_MASK
static unsigned char* mask;
#endif

void normalize_raw(int* img, int w, int h, int roi_window, int target, int integrate_count) {
    int i, j, min, max;
    int y0 = (h - roi_window) / 2, x0 = (w - roi_window) / 2, y1 = (h + roi_window) / 2,
        x1 = (w + roi_window) / 2;
    unsigned int ratio_2e16;

    // IPcount_max_min_ROI(src_img, w, h, &max, &min, 1, x0, y0, roi_window, roi_window);
    max = -0xFFFF;
    min = 0xFFFF;
    for (i = y0; i < y1; i++) {
        for (j = x0; j < x1; j++) {
            max = max > img[i * w + j] ? max : img[i * w + j];
            min = min < img[i * w + j] ? min : img[i * w + j];
        }
    }

    max /= integrate_count;
    ratio_2e16 = (target * 65536) / max;
    for (i = 0; i < w * h; i++) {
        int v = ((unsigned int)img[i] * ratio_2e16 + 32768) >> 16;
        img[i] = v > 0xFFFF ? 0xFFFF : v;
    }
}

static void determine_center(int* img, int width, int height, int* cx, int* cy) {
    int i, j;
    int sum = 0, thres, x = 0, y = 0, count = 0;

    for (i = 0; i < 10; i++) {
        for (j = 0; j < 10; j++) {
            sum += img[i * width + j];
        }
    }

    thres = (sum * 5 + 50) / 100;

    for (i = 0; i < width; i++) {
        for (j = 0; j < height; j++) {
            if (img[i * width + j] >= thres) {
                x += j;
                y += i;
                count++;
            }
        }
    }

    if (count == 0) {
        *cx = width / 2;
        *cy = height / 2;
    } else {
        *cx = (x + (count >> 1)) / count;
        *cy = (y + (count >> 1)) / count;
    }
}

static int* pad_array(int* img, int height, int width, int p_y_size, int p_x_size) {
    int *p_img, *p1, *p2;
    int p_height = (height + 2 * p_y_size);
    int p_width = (width + 2 * p_x_size);
    int i, j;
    p_img = (int*)malloc(sizeof(int) * p_height * p_width);

    if (img == NULL || p_img == NULL) return NULL;

    for (i = 0; i < p_height; i++) {
        p1 = p_img + i * p_width + p_x_size;
        if (i < p_y_size) {
            p2 = img;
        } else if (i >= p_height - p_y_size) {
            p2 = img + (height - 1) * width;
        } else {
            p2 = img + (i - p_y_size) * width;
        }
        memcpy(p1, p2, sizeof(int) * width);
    }

    for (i = 0; i < p_height; i++) {
        p1 = p_img + i * p_width;
        if (i < p_y_size) {
            p2 = img;
        } else if (i >= p_height - p_y_size) {
            p2 = img + (height - 1) * width;
        } else {
            p2 = img + (i - p_y_size) * width;
        }

        for (j = 0; j < p_x_size; j++) {
            p1[j] = *p2;
        }
    }

    for (i = 0; i < p_height; i++) {
        p1 = p_img + i * p_width + width + p_x_size;
        if (i < p_y_size) {
            p2 = img + width - 1;
        } else if (i >= p_height - p_y_size) {
            p2 = img + (height - 1) * width + width - 1;
        } else {
            p2 = img + (i - p_y_size) * width + width - 1;
        }
        for (j = 0; j < p_x_size; j++) {
            p1[j] = *p2;
        }
    }

    return p_img;
}

static void cal_integral_img(int* img, int height, int width) {
    int i, j, index;
    int sum;

    for (i = 0; i < height; i++) {
        sum = 0;
        for (j = 0; j < width; j++) {
            index = i * width + j;
            sum += img[index];
            if (i == 0) {
                img[index] = sum;
            } else {
                img[index] = img[index - width] + sum;
            }
        }
    }
}

static void mean_filter_integral(int* img, int width, int height, int k_size) {
    int i, j;
    int f_radius = (k_size - 1) / 2;
    int p_width = width + 2 * f_radius;
    int* int_img = NULL;
    int x1, y1, x2, y2;
    int v1, v2, v3, v4;
    int r = 65536 / ((f_radius * 2 + 1) * (f_radius * 2 + 1));

    int_img = pad_array(img, height, width, f_radius, f_radius);

    if (int_img == NULL) return;

    cal_integral_img(int_img, height + f_radius * 2, width + f_radius * 2);

    for (i = f_radius; i < height + f_radius; i++) {
        y1 = i - f_radius - 1;
        y2 = i + f_radius;

        for (j = f_radius; j < width + f_radius; j++) {
            x1 = j - f_radius - 1;
            x2 = j + f_radius;

            v1 = int_img[y2 * p_width + x2];
            if (y1 < 0)
                v2 = 0;
            else
                v2 = int_img[y1 * p_width + x2];

            if (x1 < 0)
                v3 = 0;
            else
                v3 = int_img[y2 * p_width + x1];

            if (v2 == 0 || v3 == 0)
                v4 = 0;
            else
                v4 = int_img[y1 * p_width + x1];

            *img = ((v1 - v2 - v3 + v4) * r) >> 16;
            img++;
        }
    }
    FREE(int_img);
}

static unsigned int int_sqrt(unsigned int x) {
    unsigned int op, res, one;

    op = x;
    res = 0;

    one = 1 << 30;
    while (one > op) one >>= 2;

    while (one != 0) {
        if (op >= res + one) {
            op -= res + one;
            res += one << 1;
        }
        res >>= 1;
        one >>= 2;
    }
    return res;
}

int* hist_maxmin(int* img, int width, int height, float fEt_HwInt, int hist_num_thres,
                 int* hist_org_min, int* hist_size, int* hist5_max, int* hist5_min,
                 int band_width) {
    int i, j, k;
    int max = -0xFFFF;
    int min = 0xFFFF;

    if (img == NULL) {
        return NULL;
    }

    for (k = 0; k < width * height; k++) {
        if (img[k] == MASK_NUM) {
            continue;
        }
        max = max > img[k] ? max : img[k];
        min = min < img[k] ? min : img[k];
    }

    int _hist_size = (max - min) / fEt_HwInt / band_width + 1;
    int* _hist = malloc(_hist_size * sizeof(int));

    if (_hist == NULL) {
        return NULL;
    }
    memset(_hist, 0, sizeof(int) * _hist_size);

    if (_hist_size < 1) {
        return 0;
    }

    // get histogram
    for (i = 0; i < width * height; i++) {
        if (img[i] == MASK_NUM) {
            continue;
        }
        int index = (img[i] - min) / fEt_HwInt / band_width;
        _hist[index]++;
    }

    // find _index_max & _index_min and get _hist_index
    int _index_max = -0xFFFF;
    int _index_min = 0xFFFF;
    for (i = 0; i < _hist_size; i++) {
        if (_hist[i] < hist_num_thres) {
            continue;
        }
        _index_max = _index_max > i ? _index_max : i;
        _index_min = _index_min < i ? _index_min : i;
    }

    *hist_size = _hist_size;
    *hist_org_min = min;
    *hist5_max = _index_max * band_width * fEt_HwInt + min;
    *hist5_min = _index_min * band_width * fEt_HwInt + min;

#ifdef DEBUG
    FILE* file;
    if (fopen_s(&file, "hist.csv", "a") == 0) {
        fprintf(file, "hist_maxmin\n");

        int need_comma = 0;
        for (i = 0; i < _hist_size; i++) {
            if (need_comma)
                fprintf(file, ", ");
            else
                need_comma = 1;
            int index = min + i * BAND_WIDTH * fEt_HwInt;
            fprintf(file, "%i", index);
        }
        fprintf(file, "\n");

        need_comma = 0;
        for (i = 0; i < _hist_size; i++) {
            if (need_comma)
                fprintf(file, ", ");
            else
                need_comma = 1;
            fprintf(file, "%i", _hist[i]);
        }
        fprintf(file, "\n");

        fclose(file);
    }
#endif
    return _hist;
}

static void analyze_hist(int* hist_input, int* _phist_index, int hist_size, int kernel_size,
                         float img_bkg_ratio_pos, float img_bkg_ratio_neg,
                         enum model_type _modeltype, enum lens_type _lenstype,
                         struct HistogramInfoPeak* histogramInfoPeak, int* histogramInfoPeak_index,
                         struct HistogramInfoValley* histogramInfoValley,
                         int* histogramInfoValley_index, struct HistogramInfo* histogramInfo) {
    if (hist_input == NULL || _phist_index == NULL) return;

    int i, j, k;

    float* kernel = malloc(kernel_size * sizeof(float));
    if (kernel == NULL) return;

    for (i = 0; i < kernel_size; i++) {
        kernel[i] = (double)1 / kernel_size;
    }

    int kernel_half = kernel_size >> 1;

    int src_pos = kernel_half;

    float* _hist_conv = malloc(hist_size * sizeof(float));

    if (_hist_conv == NULL) {
        FREE(kernel);
        return;
    }
    memset(_hist_conv, 0, sizeof(float) * hist_size);

    int sum_kernel = SUM_KERNEL_SIZE;

    float _hist_max = 0.0;
    int _hist_max_position = 0.0;

    for (i = kernel_half; i < hist_size - kernel_half; ++i) {
        float convolve = 0.0;

        int conv_pos = src_pos - kernel_half;
        int k = 0;
        for (k = 0; k < kernel_size; k++) {
            convolve += hist_input[conv_pos] * kernel[k];
            ++conv_pos;
        }
        _hist_conv[src_pos] = convolve;

        if (convolve > _hist_max) {
            _hist_max = convolve;
            _hist_max_position = src_pos;
        }

        ++src_pos;
    }

    // get local peaks and valley
    int iHistogramInfoPeak_index = 0;
    int iHistogramInfoValley_index = 0;
    float local_max = 0;
    float local_min = 0;
    float thres = 20;
    float thres_default = MIN10(_hist_max / 15);
    int sun_index = 2000;
    int finger_index = 2000;
    float waive_light_thres = 0.6f;
    float waive_light_thres1 = 0.15f;

    switch (_lenstype) {
        case LENS_3PA:
            if (img_bkg_ratio_neg < 0.2) {
                thres_default = 10;
            }
            break;
        case LENS_3PF:
            thres_default = MIN10(_hist_max / 30);
            break;
        case LENS_UNKNOW:
        case LENS_2PA:
        case LENS_3PC:
        case LENS_3PD:
        case LENS_3PE:
        case LENS_END:
        default:
            break;
    }

    float thres_p = thres_default;
    float thres_v = thres_default;
    for (i = kernel_half; i < hist_size - kernel_half; ++i) {
        int debug = _hist_conv[i];
        if (_hist_conv[i] > local_max && _hist_conv[i] > thres) {
            local_max = _hist_conv[i];
        } else if (_hist_conv[i] < local_min) {
            local_min = _hist_conv[i];
        }

        int local_peak = 1;
        for (j = i - kernel_half; j < i + kernel_half; j++) {
            if (_hist_conv[i] < _hist_conv[j]) {
                local_peak = 0;
                break;
            }
        }

        int local_valley = 1;
        for (j = i - kernel_half; j < i + kernel_half; j++) {
            if (_hist_conv[i] > _hist_conv[j]) {
                local_valley = 0;
                break;
            }
        }

        if (local_peak == 1 && _hist_conv[i] >= local_max && (local_max - local_min) > thres_p) {
            // last index is peak not valley, replace it
            if (iHistogramInfoPeak_index > 0 && iHistogramInfoValley_index > 0 &&
                histogramInfoValley[iHistogramInfoValley_index - 1].index <
                    histogramInfoPeak[iHistogramInfoPeak_index - 1].index) {
                histogramInfoPeak[iHistogramInfoPeak_index - 1].index = i;
                histogramInfoPeak[iHistogramInfoPeak_index - 1].intensity = _hist_conv[i];
            } else if (iHistogramInfoPeak_index > 0 && iHistogramInfoValley_index == 0) {
                histogramInfoPeak[iHistogramInfoPeak_index - 1].index = i;
                histogramInfoPeak[iHistogramInfoPeak_index - 1].intensity = _hist_conv[i];
            } else {
                histogramInfoPeak[iHistogramInfoPeak_index].index = i;
                histogramInfoPeak[iHistogramInfoPeak_index].intensity = _hist_conv[i];
                iHistogramInfoPeak_index++;
                thres_v = thres_default;
            }
            local_min = local_max;
            thres_p = 1;
        }

        if (local_valley == 1 && _hist_conv[i] <= local_min)  // no valley on the peak
        {
            // last index is valley not peak, replace it
            if (iHistogramInfoPeak_index == 0) {
                continue;
            } else if (_hist_conv[i] > _hist_max * 0.7 &&
                       (local_max - local_min) < _hist_max * 0.1)  // no valley on the peak
            {
                continue;
            }
            // valley deep should low than thres_v or little low and little far from peak
            else if ((local_max - local_min) > thres_v ||
                     ((i - histogramInfoPeak[iHistogramInfoPeak_index - 1].index) >= 5 &&
                      (local_max - local_min) > (thres_v * 0.7)) ||
                     ((i - histogramInfoPeak[iHistogramInfoPeak_index - 1].index) >= 13 &&
                      (local_max - local_min) > (thres_v * 0.4))) {
                if (iHistogramInfoPeak_index > 0 && iHistogramInfoValley_index > 0 &&
                    histogramInfoPeak[iHistogramInfoPeak_index - 1].index <
                        histogramInfoValley[iHistogramInfoValley_index - 1].index) {
                    histogramInfoValley[iHistogramInfoValley_index - 1].index = i;
                    histogramInfoValley[iHistogramInfoValley_index - 1].intensity = _hist_conv[i];
                } else {
                    histogramInfoValley[iHistogramInfoValley_index].index = i;
                    histogramInfoValley[iHistogramInfoValley_index].intensity = _hist_conv[i];
                    iHistogramInfoValley_index++;
                    thres_p = thres_default;
                }
                local_max = local_min;
                thres_v = 1;
            }
        }

        if (iHistogramInfoPeak_index >= 40) {
            break;
        }
        if (iHistogramInfoValley_index >= 40) {
            break;
        }
    }

    // waive little w type valley
    if (iHistogramInfoPeak_index >= 3 && iHistogramInfoValley_index >= 2) {
        for (i = 1; i < iHistogramInfoPeak_index - 1; i++) {
            if (iHistogramInfoValley_index <= i) {
                break;
            }

            if (histogramInfoPeak[i].intensity / MIN(histogramInfoPeak[i - 1].intensity,
                                                     histogramInfoPeak[i + 1].intensity) <
                    0.9 &&  // two side peaks are higher
                (histogramInfoPeak[i].intensity -
                 MAX(histogramInfoValley[i - 1].intensity, histogramInfoValley[i].intensity)) /
                        (MAX(histogramInfoPeak[i - 1].intensity,
                             histogramInfoPeak[i + 1].intensity) -
                         MIN(histogramInfoValley[i - 1].intensity,
                             histogramInfoValley[i].intensity)) <
                    0.5)  // center peak is small
            {
                // avoid two big valley
                float valley_deep = (double)(MIN(histogramInfoPeak[i - 1].intensity,
                                                 histogramInfoPeak[i].intensity) -
                                             histogramInfoValley[i - 1].intensity) /
                                    _hist_max;
                float valley_deep1 = (double)(MIN(histogramInfoPeak[i + 1].intensity,
                                                  histogramInfoPeak[i].intensity) -
                                              histogramInfoValley[i].intensity) /
                                     _hist_max;

                if (valley_deep >= 0.25 && valley_deep1 >= 0.25) {
                    continue;
                }

                if (histogramInfoValley[i - 1].intensity >
                    histogramInfoValley[i].intensity)  // delete valley i-1 and peak i
                {
                    for (j = i - 1; j < iHistogramInfoValley_index - 1; j++) {
                        histogramInfoValley[j].index = histogramInfoValley[j + 1].index;
                        histogramInfoValley[j].intensity = histogramInfoValley[j + 1].intensity;
                    }
                    iHistogramInfoValley_index--;

                    for (k = i; k < iHistogramInfoPeak_index - 1; k++) {
                        histogramInfoPeak[k].index = histogramInfoPeak[k + 1].index;
                        histogramInfoPeak[k].intensity = histogramInfoPeak[k + 1].intensity;
                    }
                    iHistogramInfoPeak_index--;
                } else  // delete i and peak i
                {
                    // delete
                    for (j = i; j < iHistogramInfoValley_index - 1; j++) {
                        histogramInfoValley[j].index = histogramInfoValley[j + 1].index;
                        histogramInfoValley[j].intensity = histogramInfoValley[j + 1].intensity;
                    }
                    iHistogramInfoValley_index--;

                    for (k = i; k < iHistogramInfoPeak_index - 1; k++) {
                        histogramInfoPeak[k].index = histogramInfoPeak[k + 1].index;
                        histogramInfoPeak[k].intensity = histogramInfoPeak[k + 1].intensity;
                    }
                    iHistogramInfoPeak_index--;

                    ////fine tune the valley index
                    // float valley_deep = 0;
                    // float peaks_wide = 0;
                    // for (i = 0; i < iHistogramInfoPeak_index - 1; i++)
                    //{
                    //	if (iHistogramInfoValley_index > i)
                    //	{
                    //		valley_deep = (double)(MIN(histogramInfoPeak[i + 1].intensity,
                    // histogramInfoPeak[i].intensity) - histogramInfoValley[i].intensity) /
                    //_hist_max; 		peaks_wide = (double)MAX(ABS(_hist_max_position -
                    // histogramInfoPeak[i].index), ABS(_hist_max_position - histogramInfoPeak[i +
                    // 1].index)) / _hist_HalfWidth;

                    //		if (valley_deep > 0.25 ||
                    //			(valley_deep > 0.15 &&
                    //				peaks_wide > 1))
                    //		{
                    //			//get fake valley index
                    //			int fake_index = histogramInfoValley[i].index;
                    //			int thres = (MIN(histogramInfoPeak[i].intensity, histogramInfoPeak[i
                    //+  1].intensity) - histogramInfoValley[i].intensity) * 0.4 +
                    // histogramInfoValley[i].intensity; 			for (j =
                    // histogramInfoValley[i].index; j < hist_size - kernel_half; ++j)
                    //			{
                    //				if (_hist_conv[j] > thres)
                    //				{
                    //					fake_index = j;
                    //					break;
                    //				}
                    //			}
                    //			histogramInfoValley[i].index = fake_index;
                    //		}
                    //	}
                    //}
                }
            }
        }
    }

    // delete last valley, peak-valley-peak-"valley"
    if (iHistogramInfoPeak_index > 0 && iHistogramInfoValley_index > 0 &&
        histogramInfoPeak[iHistogramInfoPeak_index - 1].index <
            histogramInfoValley[iHistogramInfoValley_index - 1].index) {
        iHistogramInfoValley_index--;
    }

    *histogramInfoPeak_index = iHistogramInfoPeak_index;
    *histogramInfoValley_index = iHistogramInfoValley_index;

    // check two large peak, waive dark peak histogramInfo
    int waive_dark_peak_index = 0;
    // int waive_light_peak_index = hist_size - 1;
    if (iHistogramInfoPeak_index >= 2 && iHistogramInfoValley_index >= 1) {
        for (i = 0; i < iHistogramInfoPeak_index - 1; i++) {
            if (i < iHistogramInfoValley_index) {
                for (j = i + 1; j < iHistogramInfoPeak_index; j++) {
                    if (j > i + 1 &&
                        histogramInfoPeak[j].index - histogramInfoPeak[i].index > 150) {
                        break;
                    } else if ((double)(histogramInfoValley[i].intensity / _hist_max) >
                               0.2)  // valley is not deep
                    {
                        continue;
                    }
                    // dark peak is higher than others and valley is very deep
                    if (_phist_index[histogramInfoPeak[j].index] <
                            sun_index &&  // avoid sun condition
                        ABS(_phist_index[histogramInfoPeak[j].index] - finger_index) <
                            ABS(_phist_index[histogramInfoPeak[i].index] -
                                finger_index) &&  // peak close to 2000
                        histogramInfoPeak[i].intensity > histogramInfoPeak[j].intensity &&
                        histogramInfoPeak[i].intensity - histogramInfoValley[i].intensity >
                            _hist_max * 0.6 &&
                        (histogramInfoPeak[j].intensity - histogramInfoValley[i].intensity >
                             _hist_max * 0.1 ||
                         _phist_index[histogramInfoPeak[i].index] < 500)) {
                        waive_dark_peak_index = histogramInfoValley[i].index;
                    }

                    ////light peak [j] is _hist_max, and valley is very deep
                    // if (histogramInfoPeak[j].index == _hist_max_position &&
                    //	_phist_index[histogramInfoPeak[i].index] < sun_index && //avoid sun
                    // condition 	ABS(_phist_index[histogramInfoPeak[j].index] - finger_index) >
                    // ABS(_phist_index[histogramInfoPeak[i].index] - finger_index) && //[i] peak
                    // close to 2000 	histogramInfoPeak[i].intensity <
                    // histogramInfoPeak[j].intensity && 	histogramInfoPeak[j].intensity -
                    // histogramInfoValley[i].intensity > _hist_max * waive_light_thres &&
                    //	histogramInfoPeak[i].intensity - histogramInfoValley[i].intensity >
                    //_hist_max * waive_light_thres1)
                    //{
                    //	waive_light_peak_index = histogramInfoValley[j - 1].index;
                    //}
                    ////avoid get_room_light_ratio wrong, waive next light peaks
                    // else if (_phist_index[histogramInfoPeak[i].index] < sun_index && //avoid sun
                    // condition 	ABS(_phist_index[histogramInfoPeak[j].index] - finger_index) >
                    // ABS(_phist_index[histogramInfoPeak[i].index] - finger_index) && //[i] peak
                    // close to 2000 	histogramInfoPeak[i].intensity / 5 <
                    // histogramInfoPeak[j].intensity && //avoid get_room_light_ratio wrong
                    //	histogramInfoPeak[j].intensity - histogramInfoValley[i].intensity >
                    //_hist_max * waive_light_thres && 	histogramInfoPeak[i].intensity -
                    // histogramInfoValley[i].intensity > _hist_max * waive_light_thres1)
                    //{
                    //	waive_light_peak_index = histogramInfoValley[j - 1].index;
                    //}
                }
            }
        }
    }

    ////check two large peak, waive light peak histogramInfo
    // int waive_light_peak_index = 0;
    // if (iHistogramInfoPeak_index >= 2 && iHistogramInfoValley_index >= 1)
    //{
    //	for (i = iHistogramInfoPeak_index - 1; i < 1; i++)
    //	{
    //		for (j = i - 1; j < 0; j++)
    //		{
    //			//dark peak is higher than others and valley is very deep
    //			if (_phist_index[histogramInfoPeak[j].index] < sun_index && //avoid sun condition
    //				ABS(_phist_index[histogramInfoPeak[j].index] - 2000) <
    // ABS(_phist_index[histogramInfoPeak[i].index] - 2000) && //peak close to 2000
    //				histogramInfoPeak[i].intensity > histogramInfoPeak[j].intensity &&
    //				histogramInfoPeak[i].intensity - histogramInfoValley[i].intensity > _hist_max *
    // 0.6
    //&&
    //				(histogramInfoPeak[j].intensity - histogramInfoValley[i].intensity > _hist_max *
    // 0.15
    //|| 					_phist_index[histogramInfoPeak[i].index] < 500))
    //			{
    //				waive_dark_peak_index = histogramInfoValley[i].index;
    //			}
    //		}
    //	}
    //}

    if (waive_dark_peak_index > 0) {
        histogramInfo->hist_main_dark_peak = 1;
    } else {
        histogramInfo->hist_main_dark_peak = 0;
    }

    histogramInfo->waive_dark_peak_index = waive_dark_peak_index;
    // histogramInfo->waive_light_peak_index = waive_light_peak_index;

#ifdef DEBUG
    FILE* file;
    if (fopen_s(&file, "hist_log.txt", "a") == 0) {
        fprintf(file, "waive_dark_peak_index %i\n", waive_dark_peak_index);
        // fprintf(file, "waive_light_peak_index %i\n", waive_light_peak_index);
        fclose(file);
    }
#endif

    if (waive_dark_peak_index > 0 /*|| waive_light_peak_index < hist_size - 1*/) {
        _hist_max = 0.0;
        _hist_max_position = 0;

        for (i = kernel_half; i < hist_size - kernel_half; ++i) {
            if (i < waive_dark_peak_index) {
                continue;
            }
            // else if (i > waive_light_peak_index)
            //{
            //	continue;
            //}

            if (_hist_max < _hist_conv[i]) {
                _hist_max = _hist_conv[i];
                _hist_max_position = i;
            }
        }
    }

    int _hist_HalfWidth = 0;
    int _hist_HalfWidth_noreset = 0;
    int _hist_HlafDozenWidth = 0;
    int _hist_HlafDozenWidth_noreset = 0;
    // find hist median
    {
        // Half Width
        int _sum = 0;
        int get_peak = 0;
        for (i = kernel_half; i < hist_size - kernel_half; ++i) {
            if (i < waive_dark_peak_index) {
                continue;
            }
            // else if (i > waive_light_peak_index)
            //{
            //	continue;
            //}

            if (_hist_conv[i] == _hist_max) {
                get_peak = 1;
            }

            if (_hist_conv[i] > _hist_max / 2) {
                _sum += i;
                _hist_HalfWidth++;
            }

            if (_sum > 0 && get_peak == 0 && _hist_conv[i] < _hist_max / 2) {
                _sum = 0;
                _hist_HalfWidth = 0;
            }

            if (get_peak == 1 && _hist_conv[i] < _hist_max / 2) {
                break;
            }
        }

        // Quarter Width
        int _sum_q = 0;
        get_peak = 0;
        for (i = kernel_half; i < hist_size - kernel_half; ++i) {
            if (_hist_conv[i] == _hist_max) {
                get_peak = 1;
            }

            if (_hist_conv[i] > _hist_max / 6) {
                _sum_q += i;
                _hist_HlafDozenWidth++;
            }

            if (_sum_q > 0 && get_peak == 0 && _hist_conv[i] < _hist_max / 6) {
                _sum_q = 0;
                _hist_HlafDozenWidth = 0;
            }

            if (get_peak == 1 && _hist_conv[i] < _hist_max / 6) {
                break;
            }
        }

        // get no reset
        for (i = kernel_half; i < hist_size - kernel_half; ++i) {
            if (_hist_conv[i] > _hist_max / 2) {
                _hist_HalfWidth_noreset++;
            }

            if (_hist_conv[i] > _hist_max / 6) {
                _hist_HlafDozenWidth_noreset++;
            }
        }

        if (_hist_HalfWidth == 0) {
            histogramInfo->hist_HalfWidth_MedianPosition = 0;
        } else {
            histogramInfo->hist_HalfWidth_MedianPosition = _sum / _hist_HalfWidth;
        }
        histogramInfo->hist_HalfWidth = _hist_HalfWidth;
        histogramInfo->hist_HalfWidth_noreset = _hist_HalfWidth_noreset;

        if (_hist_HalfWidth == 0) {
            histogramInfo->hist_HlafDozenWidth_MedianPosition = 0;
        } else {
            histogramInfo->hist_HlafDozenWidth_MedianPosition = _sum_q / _hist_HlafDozenWidth;
        }
        histogramInfo->hist_HlafDozenWidth = _hist_HlafDozenWidth;
        histogramInfo->hist_HlafDozenWidth_noreset = _hist_HlafDozenWidth_noreset;

        histogramInfo->hist_max = _hist_max;
        histogramInfo->hist_max_position = _hist_max_position;
    }

    ////fine tune the valley index
    // float valley_deep = 0;
    // float peaks_wide = 0;
    // for (i = 0; i < iHistogramInfoPeak_index - 1; i++)
    //{
    //	if (iHistogramInfoValley_index > i)
    //	{
    //		valley_deep = (double)(MIN(histogramInfoPeak[i + 1].intensity,
    // histogramInfoPeak[i].intensity) - histogramInfoValley[i].intensity) / _hist_max;
    //		peaks_wide = (double)MAX(ABS(_hist_max_position - histogramInfoPeak[i].index),
    // ABS(_hist_max_position - histogramInfoPeak[i + 1].index)) / _hist_HalfWidth;

    //		if (valley_deep > 0.25 ||
    //			(valley_deep > 0.15 &&
    //				peaks_wide > 1))
    //		{
    //			//get fake valley index
    //			int fake_index = histogramInfoValley[i].index;
    //			int thres = (MIN(histogramInfoPeak[i].intensity, histogramInfoPeak[i + 1].intensity)
    //-  histogramInfoValley[i].intensity) * 0.4 + histogramInfoValley[i].intensity; 			for
    // (j =  histogramInfoValley[i].index; j < hist_size - kernel_half; ++j)
    //			{
    //				if (_hist_conv[j] > thres)
    //				{
    //					fake_index = j;
    //					break;
    //				}
    //			}
    //			histogramInfoValley[i].index = fake_index;
    //		}
    //	}
    //}

#ifdef DEBUG
    if (fopen_s(&file, "hist.csv", "a") == 0) {
        int need_comma = 0;
        for (i = 0; i < hist_size; ++i) {
            if (need_comma)
                fprintf(file, ", ");
            else
                need_comma = 1;
            fprintf(file, "%i", hist_input[i]);
        }
        fprintf(file, "\n");

        need_comma = 0;
        for (i = 0; i < hist_size; ++i) {
            if (need_comma)
                fprintf(file, ", ");
            else
                need_comma = 1;
            fprintf(file, "%f", _hist_conv[i]);
        }
        fprintf(file, "\n");

        fclose(file);
    }
#endif

    FREE(kernel);
    FREE(_hist_conv);

    return;
}

static void get_boundarys(
    struct HistogramInfo _histogramInfo,
    // struct HistogramInfoPeak* histogramInfoPeak, int histogramInfoPeak_index,
    // struct HistogramInfoValley* histogramInfoValley, int histogramInfoValley_index,
    int* _pHist_index, float _img_bkg_ratio_pos, float _img_bkg_ratio_neg, int _room_light_index,
    int _room_light_score, int _bkg_max, enum model_type _modeltype, enum lens_type _lenstype,
    int* _light_boundary, int* _dark_boundary, int* _break_boundary) {
    //*_light_boundary = MAX(5000, MIN(8000, _pHist_index[_histogramInfo.hist_max_position]));
    *_light_boundary = MAX(_bkg_max, 4000);
    *_dark_boundary = 1000;
    int hist_min_width = _histogramInfo.hist_HalfWidth * 1;
    float pass_thres = 0.2f;

    switch (_lenstype) {
        case LENS_3PA:
            if (_img_bkg_ratio_neg < 0) {
                hist_min_width = MIN(20, _histogramInfo.hist_HalfWidth);
            } else if (_room_light_score < 40 && _img_bkg_ratio_neg > pass_thres) {
                hist_min_width = 100;
            } else if (_histogramInfo.hist_HalfWidth > 60) {
                hist_min_width = 30;
            } else if (_histogramInfo.hist_HalfWidth > 50) {
                hist_min_width = 50;
            } else {
                hist_min_width = MAX(50, _histogramInfo.hist_HalfWidth);
            }

            if (_room_light_index > hist_min_width) {
                *_dark_boundary = _pHist_index[_room_light_index - hist_min_width];
            }

            if (*_dark_boundary < 500) {
                *_dark_boundary = 500;
            } else if (*_dark_boundary > 2100) {
                *_dark_boundary = 2100;
            }

            *_break_boundary = 1200;

            break;

        case LENS_3PC:
            if (_img_bkg_ratio_neg < -0.1) {
                hist_min_width = 10;
            } else if (_img_bkg_ratio_neg < 0) {
                hist_min_width = MIN(30, _histogramInfo.hist_HalfWidth);
            } else if (_room_light_score < 40 && _img_bkg_ratio_neg > pass_thres) {
                hist_min_width = 100;
            } else if (_room_light_score > 80) {
                hist_min_width = 20;
            } else if (_room_light_score > 70) {
                hist_min_width = 30;
            } else if (_histogramInfo.hist_HalfWidth > 65) {
                hist_min_width = 30;
            } else if (_histogramInfo.hist_HalfWidth > 40) {
                hist_min_width = 40;
            } else {
                hist_min_width = MAX(50, _histogramInfo.hist_HalfWidth);
            }

            if (_room_light_index > hist_min_width) {
                *_dark_boundary = _pHist_index[_room_light_index - hist_min_width];
            }

            if (*_dark_boundary < 500) {
                *_dark_boundary = 500;
            }
            // else if (dark_boundary > 2100)
            //{
            //	dark_boundary = 2100;
            //}

            *_break_boundary = 1300;
            break;

        case LENS_3PF:
            if (_img_bkg_ratio_neg < 0) {
                hist_min_width = MIN(40, _histogramInfo.hist_HalfWidth);
            } else if (_room_light_score < 40 && _img_bkg_ratio_neg > pass_thres) {
                hist_min_width = 300;
            } else if (_histogramInfo.hist_HalfWidth > 240) {
                hist_min_width = 100;
            } else if (_histogramInfo.hist_HalfWidth > 130) {
                hist_min_width = 130;
            } else {
                hist_min_width = MAX(60, _histogramInfo.hist_HalfWidth);
            }

            if (_room_light_index > hist_min_width) {
                *_dark_boundary = _pHist_index[_room_light_index - hist_min_width];
            }

            if (*_dark_boundary < 500) {
                *_dark_boundary = 500;
            } else if (*_dark_boundary > 2600) {
                *_dark_boundary = 2600;
            }

            *_break_boundary = 1500;

            break;
        case LENS_UNKNOW:
        case LENS_2PA:
        case LENS_3PD:
        case LENS_3PE:
        case LENS_END:
        default:
            if (_img_bkg_ratio_pos > 0.4) {
                hist_min_width = 90;
                if (_room_light_index > hist_min_width) {
                    *_dark_boundary = _pHist_index[_room_light_index - hist_min_width];
                } else {
                    *_dark_boundary = 1000;
                }
            } else if (_img_bkg_ratio_pos > 0.3) {
                hist_min_width = 70;
                if (_room_light_index > hist_min_width) {
                    *_dark_boundary = _pHist_index[_room_light_index - hist_min_width];
                } else {
                    *_dark_boundary = 1000;
                }
            } else if (_img_bkg_ratio_pos > 0.2) {
                hist_min_width = 50;
                if (_room_light_index > hist_min_width) {
                    *_dark_boundary = _pHist_index[_room_light_index - hist_min_width];
                } else {
                    *_dark_boundary = 1000;
                }
            } else {
                *_dark_boundary = 1000;
            }

            if (*_dark_boundary < 500) {
                *_dark_boundary = 500;
            } else if (*_dark_boundary > 1000) {
                *_dark_boundary = 1000;
            }
            *_break_boundary = 1500;
            break;
    }

    switch (_modeltype) {
        case MODEL_A91:
            pass_thres = 0.5;
            if (_img_bkg_ratio_neg < -0.1) {
                hist_min_width = 10;
            } else if (_img_bkg_ratio_neg < 0.1) {
                hist_min_width = MIN(25, _histogramInfo.hist_HalfWidth);
            } else if (_histogramInfo.hist_HlafDozenWidth > _histogramInfo.hist_HalfWidth * 1.7) {
                hist_min_width = MIN(25, _histogramInfo.hist_HalfWidth);
            } else if (_room_light_score < 40 && _img_bkg_ratio_neg > pass_thres) {
                hist_min_width = 100;
            } else if (_room_light_score > 80) {
                hist_min_width = 25;
            } else if (_room_light_score > 70) {
                hist_min_width = 25;
            }
            // else if (_histogramInfo.hist_HalfWidth > 50)
            //{
            //	hist_min_width = 25;
            //}
            else if (_histogramInfo.hist_HalfWidth > 40) {
                hist_min_width = 40;
            } else {
                hist_min_width = MAX(50, _histogramInfo.hist_HalfWidth);
            }

            if (_room_light_index > hist_min_width) {
                *_dark_boundary = _pHist_index[_room_light_index - hist_min_width];
            }

            if (*_dark_boundary < 500) {
                *_dark_boundary = 500;
            }
            if (*_dark_boundary > 1600) {
                *_dark_boundary = 1600;
            }

            *_break_boundary = 1300;
            break;
        default:
            break;
    }

#ifdef DEBUG
    FILE* file;
    if (fopen_s(&file, "hist_log.txt", "a") == 0) {
        fprintf(file, "hist_min_width %i\n", hist_min_width);
        fprintf(file, "room_light_index %i\n", _room_light_index);
        fprintf(file, "_light_boundary %i\n", *_light_boundary);
        fprintf(file, "_dark_boundary %i\n", *_dark_boundary);
        fprintf(file, "_break_boundary %i\n", *_break_boundary);
        fclose(file);
    }
#endif
}

static void get_multi_peak_thres(
    struct HistogramInfo histogramInfo, struct HistogramInfoPeak* histogramInfoPeak,
    int histogramInfoPeak_index, struct HistogramInfoValley* histogramInfoValley,
    int histogramInfoValley_index, int* phist_index, float img_bkg_ratio_pos,
    float img_bkg_ratio_neg, int room_light_index, int room_light_score, enum model_type _modeltype,
    enum lens_type _lenstype, int _light_boundary, int _dark_boundary, int _break_boundary,
    int* light_index, int* dark_index) {
    int is_pass_thres = 0;

    int i;
    for (i = 0; i < histogramInfoValley_index; i++) {
        float valley_deep = 0;
        float valley_relate_deep = 0;
        float peaks_wide = 0;
        float peaks_wide_q = 0;
        float main_peak_distance = 0;
        float main_peak_distance_q = 0;
        if (histogramInfoPeak_index > i + 1) {
            valley_deep =
                (double)(MIN(histogramInfoPeak[i + 1].intensity, histogramInfoPeak[i].intensity) -
                         histogramInfoValley[i].intensity) /
                histogramInfo.hist_max;
            valley_relate_deep =
                (double)(MIN(histogramInfoPeak[i + 1].intensity, histogramInfoPeak[i].intensity) -
                         histogramInfoValley[i].intensity) /
                (histogramInfo.hist_max - histogramInfoValley[i].intensity);
            peaks_wide =
                (double)MAX(ABS(histogramInfo.hist_max_position - histogramInfoPeak[i].index),
                            ABS(histogramInfo.hist_max_position - histogramInfoPeak[i + 1].index)) /
                histogramInfo.hist_HalfWidth;
            peaks_wide_q = (double)MAX(ABS(histogramInfo.hist_HlafDozenWidth_MedianPosition -
                                           histogramInfoPeak[i].index),
                                       ABS(histogramInfo.hist_HlafDozenWidth_MedianPosition -
                                           histogramInfoPeak[i + 1].index)) /
                           histogramInfo.hist_HlafDozenWidth;
            main_peak_distance = (double)ABS(histogramInfo.hist_HalfWidth_MedianPosition -
                                             histogramInfoValley[i].index) /
                                 histogramInfo.hist_HalfWidth;
            main_peak_distance_q = (double)ABS(histogramInfo.hist_HlafDozenWidth_MedianPosition -
                                               histogramInfoValley[i].index) /
                                   histogramInfo.hist_HlafDozenWidth;
        }

#ifdef DEBUG
        FILE* file;
        if (fopen_s(&file, "hist_log.txt", "a") == 0) {
            fprintf(file, "valley value %i\n", phist_index[histogramInfoValley[i].index]);
            fprintf(file, "valley index %i\n", histogramInfoValley[i].index);
            fprintf(file, "valley_deep %f\n", valley_deep);
            fprintf(file, "valley_relate_deep %f\n", valley_relate_deep);
            fprintf(file, "peaks_wide %f\n", peaks_wide);
            fprintf(file, "peaks_wide_q %f\n", peaks_wide_q);
            fprintf(file, "main_peak_distance %f\n", main_peak_distance);
            fprintf(file, "main_peak_distance_q %f\n", main_peak_distance_q);
            fclose(file);
        }
#endif

        if (phist_index[histogramInfoValley[i].index] < -100) {
            *dark_index = histogramInfoValley[i].index;
        }
        // big valley
        else if (valley_deep > 0.25 || (valley_deep > 0.15 && peaks_wide > 1)) {
#ifdef DEBUG
            FILE* file;
            if (fopen_s(&file, "hist_log.txt", "a") == 0) {
                fprintf(file, "big valley\n");
                fclose(file);
            }
#endif
            if (phist_index[histogramInfoValley[i].index] > _light_boundary) {
                //*light_index = histogramInfoValley[i].index;

                // if (phist_index[histogramInfoValley[i].index] > 10000)
                //{
                *light_index = SUN_NUM;
                break;
                //}
            } else if (valley_deep > 0.33) {
                if (phist_index[histogramInfoValley[i].index] > 2000) {
                    *light_index = histogramInfoValley[i].index;
                } else {
                    *dark_index = histogramInfoValley[i].index;
                }
            } else if (phist_index[histogramInfoValley[i].index] >
                       _dark_boundary + 100)  // stop detect dark
            {
            }
            //			else if (img_bkg_ratio_neg > 0.4 &&
            //				histogramInfoValley[i].index > MIN(histogramInfo.hist_max_position,
            // histogramInfo.hist_HalfWidth_MedianPosition))
            //			{
            //#ifdef DEBUG
            //				printf("img_bkg_ratio_neg > 0.4 -> waive\n");
            //
            //				if (fopen_s(&file, "hist_log.txt", "a") == 0)
            //				{
            //					fprintf(file, "img_bkg_ratio_neg > 0.4 -> waive\n");
            //					fclose(file);
            //				}
            //#endif
            //			}
            else if (phist_index[histogramInfoValley[i].index] < _dark_boundary &&
                     is_pass_thres == 0) {
                *dark_index = histogramInfoValley[i].index;
            }

            if (phist_index[histogramInfoValley[i].index] > _break_boundary) {
                is_pass_thres = 1;
            } else if (histogramInfoPeak[i].intensity > histogramInfo.hist_max) {
                is_pass_thres = 1;
            }

        } else  // if(valley_deep > 0.06)
        {
            if (phist_index[histogramInfoValley[i].index] > _light_boundary) {
                //*light_index = histogramInfoValley[i].index;

                // if (phist_index[histogramInfoValley[i].index] > 10000)
                //{
                *light_index = SUN_NUM;
                break;
                //}
            } else if (phist_index[histogramInfoValley[i].index] >
                       _dark_boundary)  // stop detect dark
            {
            }
            //			else if (img_bkg_ratio_neg > 0.4 &&
            //				histogramInfoValley[i].index > MIN(histogramInfo.hist_max_position,
            // histogramInfo.hist_HalfWidth_MedianPosition))
            //			{
            //#ifdef DEBUG
            //				printf("img_bkg_ratio_neg > 0.4 -> waive\n");
            //
            //				if (fopen_s(&file, "hist_log.txt", "a") == 0)
            //				{
            //					fprintf(file, "img_bkg_ratio_neg > 0.4 -> waive\n");
            //					fclose(file);
            //				}
            //#endif
            //			}
            else if (phist_index[histogramInfoValley[i].index] < _dark_boundary &&
                     is_pass_thres == 0) {
                if (histogramInfo.hist_HalfWidth_MedianPosition < histogramInfoValley[i].index &&
                    img_bkg_ratio_neg > 0) {
                    *dark_index = histogramInfo.hist_HalfWidth_MedianPosition;
                } else {
                    *dark_index = histogramInfoValley[i].index;
                }
            }
        }
    }
}

static int get_room_light_ratio(int* phist, int hist_size, struct HistogramInfo histogramInfo,
                                int* light_index) {
    int i;
    int room_light_ratio = 0;
    // for (i = histogramInfo.waive_light_peak_index; i >= 0; i--)
    for (i = hist_size - 1; i >= 0; i--) {
        if (phist[i] > MAX(histogramInfo.hist_max / 10, 50)) {
            room_light_ratio = 100 - (double)i / hist_size * 100;
            *light_index = i;
            break;
        }
    }

    if (room_light_ratio >= 90) {
        // for (i = histogramInfo.waive_light_peak_index; i >= 0; i--)
        for (i = hist_size - 1; i >= 0; i--) {
            if (phist[i] > MAX(histogramInfo.hist_max / 2, 50)) {
                *light_index = i;
                break;
            }
        }
    } else if (room_light_ratio >= 70) {
        // for (i = histogramInfo.waive_light_peak_index; i >= 0; i--)
        for (i = hist_size - 1; i >= 0; i--) {
            if (phist[i] > MAX(histogramInfo.hist_max / 3, 50)) {
                *light_index = i;
                break;
            }
        }
    }
    return room_light_ratio;
}

static int get_room_light_ratio_simple(int* _pHist, int _hist_size, int _hist_max,
                                       int* _light_index) {
    int i;
    int room_light_ratio = 0;
    for (i = _hist_size - 1; i >= 0; i--) {
        if (_pHist[i] > MAX(_hist_max / 10, 50)) {
            room_light_ratio = 100 - (double)i / _hist_size * 100;
            *_light_index = i;
            break;
        }
    }

    if (room_light_ratio >= 90) {
        for (i = _hist_size - 1; i >= 0; i--) {
            if (_pHist[i] > MAX(_hist_max / 2, 50)) {
                *_light_index = i;
                break;
            }
        }
    } else if (room_light_ratio >= 70) {
        for (i = _hist_size - 1; i >= 0; i--) {
            if (_pHist[i] > MAX(_hist_max / 3, 50)) {
                *_light_index = i;
                break;
            }
        }
    }
    return room_light_ratio;
}

static int get_sun_light_ratio(int* phist, int hist_size, float _hist_max, int* sun_index) {
    int i;
    int sun_light_ratio = 0;
    for (i = hist_size - 1; i >= 0; i--) {
        if (phist[i] > _hist_max / 2) {
            sun_light_ratio = 100 - (double)i / hist_size * 100;
            *sun_index = i;
            break;
        }
    }
    return sun_light_ratio;
}

static int parse_hist_sunlight(int* phist, int* phist_index, int hist_size, float img_bkg_ratio_pos,
                               float img_bkg_ratio_neg, int _bkg_max, enum model_type _modeltype,
                               enum lens_type _lenstype, struct PartialResult* _partialResult) {
    int i, j, k;

    int kernel_size = 13;
    float sunlight_thres = 1.5;

    switch (_lenstype) {
        case LENS_UNKNOW:
            break;
        case LENS_2PA:
            break;
        case LENS_3PA:
            break;
        case LENS_3PC:
            break;
        case LENS_3PD:
            break;
        case LENS_3PE:
            break;
        case LENS_3PF:
            kernel_size = 17;
            break;
        case LENS_END:
            break;
        default:
            break;
    }

#ifdef DEBUG
    FILE* file;
    if (fopen_s(&file, "hist.csv", "a") == 0) {
        fprintf(file, "parse_hist_sunlight\n");
        int need_comma = 0;
        for (i = 0; i < hist_size; ++i) {
            if (need_comma)
                fprintf(file, ", ");
            else
                need_comma = 1;
            fprintf(file, "%i", phist_index[i]);
        }
        fprintf(file, "\n");

        fclose(file);
    }
#endif

    struct HistogramInfoPeak histogramInfoPeak[50];
    int histogramInfoPeak_index = 0;
    struct HistogramInfoValley histogramInfoValley[50];
    int histogramInfoValley_index = 0;

    struct HistogramInfo histogramInfo;
    histogramInfo.hist_HalfWidth_MedianPosition = 0;
    histogramInfo.hist_HalfWidth = 0;
    histogramInfo.hist_HalfWidth_noreset = 0;
    histogramInfo.hist_HlafDozenWidth_MedianPosition = 0;
    histogramInfo.hist_HlafDozenWidth = 0;
    histogramInfo.hist_HlafDozenWidth_noreset = 0;
    histogramInfo.hist_max = 0.0;
    histogramInfo.hist_max_position = 0;
    histogramInfo.hist_main_dark_peak = 0;
    histogramInfo.waive_dark_peak_index = 0;
    // histogramInfo.waive_light_peak_index = hist_size - 1;

    analyze_hist(phist, phist_index, hist_size, kernel_size, img_bkg_ratio_pos, img_bkg_ratio_neg,
                 _modeltype, _lenstype, histogramInfoPeak, &histogramInfoPeak_index,
                 histogramInfoValley, &histogramInfoValley_index, &histogramInfo);

    _partialResult->main_dark_peak = histogramInfo.hist_main_dark_peak;
    // if (histogramInfo.waive_light_peak_index != hist_size - 1)
    //{
    //	_partialResult->main_light_peak = 1;
    //}
    // else
    //{
    //	_partialResult->main_light_peak = 0;
    //}

    int hist_HalfWidthMedian_ratio = histogramInfo.hist_HalfWidth_MedianPosition * 100 / hist_size;
    int hist_width_ratio = (double)histogramInfo.hist_HalfWidth * 100 / hist_size;
    int hist_half_quarter_pos_ratio = (double)(histogramInfo.hist_HalfWidth_MedianPosition -
                                               histogramInfo.hist_HlafDozenWidth_MedianPosition +
                                               histogramInfo.hist_HlafDozenWidth / 2) *
                                      100 / histogramInfo.hist_HlafDozenWidth;
    int hist_half_quarter_width_ratio =
        (double)histogramInfo.hist_HalfWidth * 100 / histogramInfo.hist_HlafDozenWidth;
    int hist_left_pos_ratio =
        (double)(histogramInfo.hist_HalfWidth_MedianPosition - histogramInfo.hist_HalfWidth) * 100 /
        hist_size;
    int hist_max_ratio =
        (double)(histogramInfo.hist_HalfWidth_MedianPosition - histogramInfo.hist_HalfWidth) * 100 /
        hist_size;

#ifdef DEBUG
    if (fopen_s(&file, "hist_log.txt", "a") == 0) {
        fprintf(file, "ratio %i pos %i width %i no_reset %i\n", hist_HalfWidthMedian_ratio,
                histogramInfo.hist_HalfWidth_MedianPosition, histogramInfo.hist_HalfWidth,
                histogramInfo.hist_HalfWidth_noreset);
        fprintf(file, "Quarter pos %i width %i no_reset %i\n",
                histogramInfo.hist_HlafDozenWidth_MedianPosition, histogramInfo.hist_HlafDozenWidth,
                histogramInfo.hist_HlafDozenWidth_noreset);
        fprintf(file, "hq_pos_ratio %i hq_width_ratio %i\n", hist_half_quarter_pos_ratio,
                hist_half_quarter_width_ratio);
        fprintf(file, "max %f max_pos %i\n", histogramInfo.hist_max,
                histogramInfo.hist_max_position);
        fprintf(file, "hist_size %i\n", hist_size);
        fprintf(file, "hist_left_pos_ratio %i\n", hist_left_pos_ratio);
        fclose(file);
    }
#endif

    int _maxThres = hist_size - 1;
    int _minThres = 0;
    int _peakCenter = histogramInfo.hist_HalfWidth_MedianPosition;

    // int sun_light_index = 0;
    // int sun_light_score = get_sun_light_ratio(phist, hist_size, histogramInfo.hist_max,
    // &sun_light_index);
    int room_light_index = 0;
    _partialResult->room_light_score =
        get_room_light_ratio_simple(phist, hist_size, histogramInfo.hist_max, &room_light_index);

    //#ifdef DEBUG
    //	printf("sun_light_score %i sun_light_index %i\n", _partialResult->room_light_score,
    // sun_light_index);
    //
    //	if (fopen_s(&file, "hist_log.txt", "a") == 0)
    //	{
    //		fprintf(file, "sun_light_score %i sun_light_index %i\n",
    //_partialResult->room_light_score,  sun_light_index); 		fclose(file);
    //	}
    //#endif

    int light_boundary;
    int dark_boundary;
    int break_boundary;

    get_boundarys(histogramInfo, phist_index, img_bkg_ratio_pos, img_bkg_ratio_neg,
                  room_light_index, _partialResult->room_light_score, _bkg_max, _modeltype,
                  _lenstype, &light_boundary, &dark_boundary, &break_boundary);

    if (histogramInfoValley_index > 0) {
        int dark_index = 0;
        int light_index = hist_size - 1;

        get_multi_peak_thres(
            histogramInfo, histogramInfoPeak, histogramInfoPeak_index, histogramInfoValley,
            histogramInfoValley_index, phist_index, img_bkg_ratio_pos, img_bkg_ratio_neg,
            room_light_index, _partialResult->room_light_score, _modeltype, _lenstype,
            light_boundary, dark_boundary, break_boundary, &light_index, &dark_index);

        if (light_index != hist_size - 1) {
            _maxThres = light_index;
        }

        if (dark_index != 0 && histogramInfo.hist_HalfWidth_MedianPosition - dark_index < 100) {
            _minThres = dark_index;
        }
    }

    // decide _minThres
    if (_minThres == 0 && phist_index[histogramInfo.hist_HalfWidth_MedianPosition] < _bkg_max &&
        _maxThres > histogramInfo.hist_HalfWidth_MedianPosition) {
        switch (_lenstype) {
            case LENS_3PA:
                if (_partialResult->room_light_score >= 80) {
                    if (histogramInfo.hist_HalfWidth > 50) {
                        _minThres = histogramInfo.hist_HalfWidth_MedianPosition - 50;
                    } else {
                        _minThres = histogramInfo.hist_HalfWidth_MedianPosition -
                                    histogramInfo.hist_HalfWidth;
                    }
                } else {
                    if (histogramInfo.hist_HalfWidth > 50) {
                        _minThres = histogramInfo.hist_HalfWidth_MedianPosition - 50;
                    } else {
                        _minThres = histogramInfo.hist_HalfWidth_MedianPosition -
                                    histogramInfo.hist_HalfWidth;
                    }
                }
                break;
            case LENS_3PC:
                if (_partialResult->room_light_score >= 80) {
                    if (histogramInfo.hist_HalfWidth > 40) {
                        _minThres = histogramInfo.hist_HalfWidth_MedianPosition - 40;
                    } else {
                        _minThres = histogramInfo.hist_HalfWidth_MedianPosition -
                                    histogramInfo.hist_HalfWidth;
                    }
                } else {
                    if (histogramInfo.hist_HalfWidth > 40) {
                        _minThres = histogramInfo.hist_HalfWidth_MedianPosition - 40;
                    } else {
                        _minThres = histogramInfo.hist_HalfWidth_MedianPosition -
                                    histogramInfo.hist_HalfWidth;
                    }
                }
                break;
            case LENS_UNKNOW:
            case LENS_2PA:
            case LENS_3PD:
            case LENS_3PE:
            case LENS_3PF:
                if (_partialResult->room_light_score >= 80) {
                    if (histogramInfo.hist_HalfWidth > 50) {
                        _minThres = histogramInfo.hist_HalfWidth_MedianPosition - 50;
                    } else {
                        _minThres = histogramInfo.hist_HalfWidth_MedianPosition -
                                    histogramInfo.hist_HalfWidth;
                    }
                } else {
                    if (histogramInfo.hist_HalfWidth > 50) {
                        _minThres = histogramInfo.hist_HalfWidth_MedianPosition - 50;
                    } else {
                        _minThres = histogramInfo.hist_HalfWidth_MedianPosition -
                                    histogramInfo.hist_HalfWidth;
                    }
                }
                break;
            case LENS_END:
            default:
                break;
        }

        if (_minThres > 0 && phist_index[_minThres] > dark_boundary) {
            _minThres = _minThres * 0.5;
        }
    }

    //// decide _minThres
    // if (_minThres == 0 &&
    //	phist_index[histogramInfo.hist_HalfWidth_MedianPosition] < _bkg_max &&
    //	_maxThres > histogramInfo.hist_HalfWidth_MedianPosition)
    //{

    //	if (_partialResult->room_light_score >= 80)
    //	{
    //		if (histogramInfo.hist_HalfWidth > 50)
    //		{
    //			_minThres = histogramInfo.hist_HalfWidth_MedianPosition - 50;
    //		}
    //		else
    //		{
    //			_minThres = histogramInfo.hist_HalfWidth_MedianPosition -
    // histogramInfo.hist_HalfWidth;
    //		}
    //	}
    //	else
    //	{
    //		if (histogramInfo.hist_HalfWidth > 50)
    //		{
    //			_minThres = histogramInfo.hist_HalfWidth_MedianPosition - 50;
    //		}
    //		else
    //		{
    //			_minThres = histogramInfo.hist_HalfWidth_MedianPosition -
    // histogramInfo.hist_HalfWidth;
    //		}
    //	}
    //	//if (histogramInfo.hist_HlafDozenWidth > histogramInfo.hist_HalfWidth * 1.7)
    //	//{
    //	//	_minThres = histogramInfo.hist_HlafDozenWidth_MedianPosition -
    // histogramInfo.hist_HalfWidth;
    //	//}
    //	//else
    //	//{
    //	//	_minThres = histogramInfo.hist_HlafDozenWidth_MedianPosition -
    // histogramInfo.hist_HlafDozenWidth * 0.75;
    //	//}

    //	if (_minThres > 0 &&
    //		phist_index[_minThres] > dark_boundary)
    //	{
    //		_minThres = _minThres * 0.5;
    //	}
    //}

    // decide _maxThres
    if (_maxThres == hist_size - 1) {
        // check the peak is sun light peak
        if (phist_index[histogramInfo.hist_HalfWidth_MedianPosition] > _bkg_max) {
            k = histogramInfo.hist_HalfWidth_noreset * 100 / histogramInfo.hist_HalfWidth;
            if (k > 200) {
                _maxThres = histogramInfo.hist_HalfWidth_MedianPosition -
                            histogramInfo.hist_HalfWidth_noreset;
            } else {
                _maxThres =
                    histogramInfo.hist_HalfWidth_MedianPosition - histogramInfo.hist_HalfWidth;
            }
        } else {
            //_maxThres = histogramInfo.hist_HlafDozenWidth_MedianPosition +
            // histogramInfo.hist_HlafDozenWidth;
            _maxThres = SUN_NUM;
        }
    }

    //// decide _maxThres
    // if (_maxThres == SUN_NUM &&
    //	_bkg_max > phist_index[sun_light_index])
    //{
    //	_maxThres = sun_light_index;
    //}
    // else if (_maxThres != SUN_NUM && _maxThres > phist_index[sun_light_index])
    //{
    //	_maxThres = sun_light_index;
    //}

    if (_maxThres > hist_size - 1 && _maxThres != SUN_NUM) {
        _maxThres = hist_size - 1;
    } else if (_maxThres < 0) {
        _maxThres = 0;
    }

    if (_minThres > hist_size - 1) {
        _minThres = hist_size - 1;
    } else if (_minThres < 0) {
        _minThres = 0;
    }

    _peakCenter = (double)(_maxThres + _minThres) / 2;

    _partialResult->minThres = phist_index[_minThres];

    if (_maxThres == SUN_NUM) {
        _partialResult->maxThres = light_boundary;
        _partialResult->peakCenter = phist_index[_minThres + histogramInfo.hist_HalfWidth / 2];
    } else {
        _partialResult->maxThres = phist_index[_maxThres];
        _partialResult->peakCenter = phist_index[_peakCenter];
    }

#ifdef DEBUG
    if (fopen_s(&file, "hist_log.txt", "a") == 0) {
        fprintf(file, "_minThres %i _maxThres %i _peakCenter %i\n", _minThres, _maxThres,
                _peakCenter);
        fprintf(file, "_minThres %i _maxThres %i _peakCenter %i\n", _partialResult->minThres,
                _partialResult->maxThres, _partialResult->peakCenter);
        fprintf(file, "sun end\n");
        fclose(file);
    }

    // FILE* file;
    if (fopen_s(&file, "hist_data.csv", "a") == 0) {
        int need_comma = 0;
        fprintf(file, ",%i,%i,%i,%i,%f,%i,%i,sun", hist_HalfWidthMedian_ratio,
                histogramInfo.hist_HalfWidth_MedianPosition, histogramInfo.hist_HalfWidth,
                histogramInfo.hist_HalfWidth_noreset, histogramInfo.hist_max,
                histogramInfo.hist_max_position, hist_size);
        fprintf(file, ",%i,%i,%i", histogramInfo.hist_HlafDozenWidth_MedianPosition,
                histogramInfo.hist_HlafDozenWidth, histogramInfo.hist_HlafDozenWidth_noreset);
        fprintf(file, ",%i,%i,%i,%i", hist_left_pos_ratio, hist_width_ratio,
                hist_half_quarter_width_ratio, hist_half_quarter_pos_ratio);
        fclose(file);
    }
#endif

    if (img_bkg_ratio_pos > sunlight_thres) {
        return 1;
    } else {
        return 0;
    }
}

static int parse_hist_roomlight(int* phist, int* phist_index, int hist_size,
                                float img_bkg_ratio_pos, float img_bkg_ratio_neg, int _bkg_max,
                                enum model_type _modeltype, enum lens_type _lenstype,
                                struct PartialResult* _partialResult) {
    int i, j, k;

    int kernel_size = 13;
    // switch (_lenstype)
    //{
    // case LENS_UNKNOW:
    //	break;
    // case LENS_2PA:
    //	break;
    // case LENS_3PA:
    //	break;
    // case LENS_3PC:
    //	break;
    // case LENS_3PD:
    //	break;
    // case LENS_3PE:
    //	break;
    // case LENS_3PF:
    //	kernel_size = 15;
    //	break;
    // case LENS_END:
    //	break;
    // default:
    //	break;
    //}

    // switch (_modeltype)
    //{
    // case MODEL_UNKNOW:
    //	break;
    // case MODEL_A50:
    //	break;
    // case MODEL_A70:
    //	break;
    // case MODEL_A80:
    //	break;
    // case MODEL_TAB_S6:
    //	break;
    // case MODEL_A90:
    //	break;
    // case MODEL_A30S:
    //	break;
    // case MODEL_A50S:
    //	break;
    // case MODEL_A70S:
    //	break;
    // case MODEL_A51:
    //	break;
    // case MODEL_A71:
    //	break;
    // case MODEL_A91:
    //	kernel_size = 7;
    //	break;
    // case MODEL_A_NOTE:
    //	break;
    // case MODEL_702:
    //	break;
    // case MODEL_LG:
    //	break;
    // case MODEL_END:
    //	break;
    // default:
    //	break;
    //}

#ifdef DEBUG
    FILE* file;
    if (fopen_s(&file, "hist.csv", "a") == 0) {
        fprintf(file, "parse_hist_roomlight\n");
        int need_comma = 0;
        for (i = 0; i < hist_size; ++i) {
            if (need_comma)
                fprintf(file, ", ");
            else
                need_comma = 1;
            fprintf(file, "%i", phist_index[i]);
        }
        fprintf(file, "\n");

        fclose(file);
    }
#endif

    struct HistogramInfoPeak histogramInfoPeak[50];
    int histogramInfoPeak_index = 0;
    struct HistogramInfoValley histogramInfoValley[50];
    int histogramInfoValley_index = 0;

    struct HistogramInfo histogramInfo;
    histogramInfo.hist_HalfWidth_MedianPosition = 0;
    histogramInfo.hist_HalfWidth = 0;
    histogramInfo.hist_HalfWidth_noreset = 0;
    histogramInfo.hist_HlafDozenWidth_MedianPosition = 0;
    histogramInfo.hist_HlafDozenWidth = 0;
    histogramInfo.hist_HlafDozenWidth_noreset = 0;
    histogramInfo.hist_max = 0.0;
    histogramInfo.hist_max_position = 0;
    histogramInfo.hist_main_dark_peak = 0;
    histogramInfo.waive_dark_peak_index = 0;
    // histogramInfo.waive_light_peak_index = hist_size - 1;

    analyze_hist(phist, phist_index, hist_size, kernel_size, img_bkg_ratio_pos, img_bkg_ratio_neg,
                 _modeltype, _lenstype, histogramInfoPeak, &histogramInfoPeak_index,
                 histogramInfoValley, &histogramInfoValley_index, &histogramInfo);

    _partialResult->main_dark_peak = histogramInfo.hist_main_dark_peak;
    // if (histogramInfo.waive_light_peak_index != hist_size - 1)
    //{
    //	_partialResult->main_light_peak = 1;
    //}
    // else
    //{
    //	_partialResult->main_light_peak = 0;
    //}

    int hist_HalfWidthMedian_ratio = histogramInfo.hist_HalfWidth_MedianPosition * 100 / hist_size;
    int hist_width_ratio = (double)histogramInfo.hist_HalfWidth * 100 / hist_size;
    int hist_half_quarter_pos_ratio = (double)(histogramInfo.hist_HalfWidth_MedianPosition -
                                               histogramInfo.hist_HlafDozenWidth_MedianPosition +
                                               histogramInfo.hist_HlafDozenWidth / 2) *
                                      100 / histogramInfo.hist_HlafDozenWidth;
    int hist_half_quarter_width_ratio =
        (double)histogramInfo.hist_HalfWidth * 100 / histogramInfo.hist_HlafDozenWidth;

#ifdef DEBUG
    if (fopen_s(&file, "hist_log.txt", "a") == 0) {
        fprintf(file, "ratio %i pos %i width %i no_reset %i\n", hist_HalfWidthMedian_ratio,
                histogramInfo.hist_HalfWidth_MedianPosition, histogramInfo.hist_HalfWidth,
                histogramInfo.hist_HalfWidth_noreset);
        fprintf(file, "Quarter pos %i width %i no_reset %i\n",
                histogramInfo.hist_HlafDozenWidth_MedianPosition, histogramInfo.hist_HlafDozenWidth,
                histogramInfo.hist_HlafDozenWidth_noreset);
        fprintf(file, "hq_pos_ratio %i hq_width_ratio %i\n", hist_half_quarter_pos_ratio,
                hist_half_quarter_width_ratio);
        fprintf(file, "max %f max_pos %i\n", histogramInfo.hist_max,
                histogramInfo.hist_max_position);
        fprintf(file, "hist_size %i\n", hist_size);
        fclose(file);
    }
#endif

    int _maxThres = hist_size - 1;
    int _minThres = 0;
    int _peakCenter = histogramInfo.hist_HalfWidth_MedianPosition;

    int room_light_index = hist_size - 1;
    _partialResult->room_light_score =
        get_room_light_ratio(phist, hist_size, histogramInfo, &room_light_index);

    int light_boundary;
    int dark_boundary;
    int break_boundary;

    get_boundarys(histogramInfo, phist_index, img_bkg_ratio_pos, img_bkg_ratio_neg,
                  room_light_index, _partialResult->room_light_score, _bkg_max, _modeltype,
                  _lenstype, &light_boundary, &dark_boundary, &break_boundary);

    if (histogramInfoValley_index > 0) {
        int dark_index = 0;
        int light_index = hist_size - 1;

        get_multi_peak_thres(
            histogramInfo, histogramInfoPeak, histogramInfoPeak_index, histogramInfoValley,
            histogramInfoValley_index, phist_index, img_bkg_ratio_pos, img_bkg_ratio_neg,
            room_light_index, _partialResult->room_light_score, _modeltype, _lenstype,
            light_boundary, dark_boundary, break_boundary, &light_index, &dark_index);

        if (dark_index != 0 && histogramInfo.hist_HalfWidth_MedianPosition - dark_index < 100) {
            _minThres = dark_index;
        }

        if (light_index != hist_size - 1) {
            _maxThres = light_index;
        }
    }

    // decide _minThres
    if (_minThres == 0) {
        switch (_lenstype) {
            case LENS_3PA:
                // if (_partialResult->room_light_score >= 80)
                //{
                //	if (histogramInfo.hist_HalfWidth > 50)
                //	{
                //		_minThres = histogramInfo.hist_HalfWidth_MedianPosition - 50;
                //	}
                //	else
                //	{
                //		_minThres = histogramInfo.hist_HalfWidth_MedianPosition -
                // histogramInfo.hist_HalfWidth;
                //	}
                //}
                // else
                //{
                if (histogramInfo.hist_HalfWidth > 50) {
                    _minThres = histogramInfo.hist_HalfWidth_MedianPosition - 50;
                } else {
                    _minThres =
                        histogramInfo.hist_HalfWidth_MedianPosition - histogramInfo.hist_HalfWidth;
                }
                //}
                break;
            case LENS_3PC:
                // if (_partialResult->room_light_score >= 80)
                //{
                //	if (histogramInfo.hist_HalfWidth > 40)
                //	{
                //		_minThres = histogramInfo.hist_HalfWidth_MedianPosition - 40;
                //	}
                //	else
                //	{
                //		_minThres = histogramInfo.hist_HalfWidth_MedianPosition -
                // histogramInfo.hist_HalfWidth;
                //	}
                //}
                // else
                //{
                if (histogramInfo.hist_HalfWidth > 40) {
                    _minThres = histogramInfo.hist_HalfWidth_MedianPosition - 40;
                } else {
                    _minThres =
                        histogramInfo.hist_HalfWidth_MedianPosition - histogramInfo.hist_HalfWidth;
                }
                //}
                break;
            case LENS_3PF:
                // if (_partialResult->room_light_score >= 80)
                //{
                //	if (histogramInfo.hist_HalfWidth > 50)
                //	{
                //		_minThres = histogramInfo.hist_HalfWidth_MedianPosition - 50;
                //	}
                //	else
                //	{
                //		_minThres = histogramInfo.hist_HalfWidth_MedianPosition -
                // histogramInfo.hist_HalfWidth;
                //	}
                //}
                // else
                //{
                if (histogramInfo.hist_HalfWidth > 50) {
                    _minThres = histogramInfo.hist_HalfWidth_MedianPosition - 50;
                } else {
                    _minThres =
                        histogramInfo.hist_HalfWidth_MedianPosition - histogramInfo.hist_HalfWidth;
                }
                //}
                break;
            case LENS_UNKNOW:
            case LENS_2PA:
            case LENS_3PD:
            case LENS_3PE:
            case LENS_END:
            default:
                break;
        }

        switch (_modeltype) {
            case MODEL_A91:

                if (_partialResult->room_light_score >= 80) {
                    if (histogramInfo.hist_HalfWidth > 40) {
                        _minThres = histogramInfo.hist_HalfWidth_MedianPosition - 40;
                    } else {
                        _minThres = histogramInfo.hist_HalfWidth_MedianPosition -
                                    histogramInfo.hist_HalfWidth;
                    }
                } else if (img_bkg_ratio_neg < 0.1) {
                    if (histogramInfo.hist_HalfWidth > 40) {
                        _minThres = histogramInfo.hist_HalfWidth_MedianPosition - 40;
                    } else {
                        _minThres = histogramInfo.hist_HalfWidth_MedianPosition -
                                    histogramInfo.hist_HalfWidth;
                    }
                } else {
                    if (histogramInfo.hist_HalfWidth > 50) {
                        _minThres = histogramInfo.hist_HalfWidth_MedianPosition - 50;
                    } else {
                        _minThres = histogramInfo.hist_HalfWidth_MedianPosition -
                                    histogramInfo.hist_HalfWidth;
                    }
                }
                break;
            default:
                break;
        }

        if (_minThres > 0 && phist_index[_minThres] > dark_boundary) {
            _minThres = _minThres * 0.5;
        }
    }

    // decide _maxThres
    if (_maxThres == hist_size - 1) {
        _maxThres = room_light_index;
    }

    if (_maxThres > hist_size - 1) {
        _maxThres = hist_size - 1;
    } else if (_maxThres < 0) {
        _maxThres = 0;
    }

    if (_minThres > hist_size - 1) {
        _minThres = hist_size - 1;
    } else if (_minThres < 0) {
        _minThres = 0;
    }

    _peakCenter = (double)(_maxThres + _minThres) / 2;

    _partialResult->minThres = phist_index[_minThres];
    _partialResult->maxThres = phist_index[_maxThres];
    _partialResult->peakCenter = phist_index[_peakCenter];

#ifdef DEBUG
    if (fopen_s(&file, "hist_log.txt", "a") == 0) {
        fprintf(file, "_minThres %i _maxThres %i _peakCenter %i\n", _minThres, _maxThres,
                _peakCenter);
        fprintf(file, "_minThres %i _maxThres %i _peakCenter %i\n", _partialResult->minThres,
                _partialResult->maxThres, _partialResult->peakCenter);
        fclose(file);
    }
#endif

#ifdef DEBUG
    // FILE* file;
    if (fopen_s(&file, "hist_data.csv", "a") == 0) {
        int need_comma = 0;
        fprintf(file, ",%i,%i,%i,%i,%f,%i,%i,room", hist_HalfWidthMedian_ratio,
                histogramInfo.hist_HalfWidth_MedianPosition, histogramInfo.hist_HalfWidth,
                histogramInfo.hist_HalfWidth_noreset, histogramInfo.hist_max,
                histogramInfo.hist_max_position, hist_size);
        fprintf(file, ",%i,%i,%i", histogramInfo.hist_HlafDozenWidth_MedianPosition,
                histogramInfo.hist_HlafDozenWidth, histogramInfo.hist_HlafDozenWidth_noreset);
        fprintf(file, ",%i,%i,%i", hist_width_ratio, hist_half_quarter_width_ratio,
                hist_half_quarter_pos_ratio);
        fclose(file);
    }

    // FILE* file;
    if (fopen_s(&file, "hist_data.csv", "a") == 0) {
        fprintf(file, "\n");
        fclose(file);
    }

    if (fopen_s(&file, "hist_log.txt", "a") == 0) {
        fprintf(file, "room_light_score %i\n", _partialResult->room_light_score);
        fprintf(file, "room_light end\n");
        fclose(file);
    }
#endif

    if (_partialResult->room_light_score >= 30) {
        return 1;
    } else {
        return 0;
    }
}

static void parse_hist(int* phist, int* phist_index, int hist_size, float img_bkg_ratio_pos,
                       float img_bkg_ratio_neg, int _bkg_max, enum model_type _modeltype,
                       enum lens_type _lenstype, struct PartialResult* _partialResult) {
    int i, j, k;

    int kernel_size = 13;
    // switch (_lenstype)
    //{
    // case LENS_UNKNOW:
    //	break;
    // case LENS_2PA:
    //	break;
    // case LENS_3PA:
    //	break;
    // case LENS_3PC:
    //	break;
    // case LENS_3PD:
    //	break;
    // case LENS_3PE:
    //	break;
    // case LENS_3PF:
    //	kernel_size = 15;
    //	break;
    // case LENS_END:
    //	break;
    // default:
    //	break;
    //}

#ifdef DEBUG
    FILE* file;
    if (fopen_s(&file, "hist.csv", "a") == 0) {
        fprintf(file, "parse_hist\n");
        int need_comma = 0;
        for (i = 0; i < hist_size; ++i) {
            if (need_comma)
                fprintf(file, ", ");
            else
                need_comma = 1;
            fprintf(file, "%i", phist_index[i]);
        }
        fprintf(file, "\n");

        fclose(file);
    }
#endif

    struct HistogramInfoPeak histogramInfoPeak[50];
    int histogramInfoPeak_index = 0;
    struct HistogramInfoValley histogramInfoValley[50];
    int histogramInfoValley_index = 0;

    struct HistogramInfo histogramInfo;
    histogramInfo.hist_HalfWidth_MedianPosition = 0;
    histogramInfo.hist_HalfWidth = 0;
    histogramInfo.hist_HalfWidth_noreset = 0;
    histogramInfo.hist_HlafDozenWidth_MedianPosition = 0;
    histogramInfo.hist_HlafDozenWidth = 0;
    histogramInfo.hist_HlafDozenWidth_noreset = 0;
    histogramInfo.hist_max = 0.0;
    histogramInfo.hist_max_position = 0;
    histogramInfo.hist_main_dark_peak = 0;
    histogramInfo.waive_dark_peak_index = 0;
    // histogramInfo.waive_light_peak_index = hist_size - 1;

    analyze_hist(phist, phist_index, hist_size, kernel_size, img_bkg_ratio_pos, img_bkg_ratio_neg,
                 _modeltype, _lenstype, histogramInfoPeak, &histogramInfoPeak_index,
                 histogramInfoValley, &histogramInfoValley_index, &histogramInfo);

    _partialResult->main_dark_peak = histogramInfo.hist_main_dark_peak;
    // if (histogramInfo.waive_light_peak_index != hist_size - 1)
    //{
    //	_partialResult->main_light_peak = 1;
    //}
    // else
    //{
    //	_partialResult->main_light_peak = 0;
    //}

    int hist_HalfWidthMedian_ratio = histogramInfo.hist_HalfWidth_MedianPosition * 100 / hist_size;
    int hist_width_ratio = (double)histogramInfo.hist_HalfWidth * 100 / hist_size;
    int hist_half_quarter_pos_ratio = (double)(histogramInfo.hist_HalfWidth_MedianPosition -
                                               histogramInfo.hist_HlafDozenWidth_MedianPosition +
                                               histogramInfo.hist_HlafDozenWidth / 2) *
                                      100 / histogramInfo.hist_HlafDozenWidth;
    int hist_half_quarter_width_ratio =
        (double)histogramInfo.hist_HalfWidth * 100 / histogramInfo.hist_HlafDozenWidth;

#ifdef DEBUG
    if (fopen_s(&file, "hist_log.txt", "a") == 0) {
        fprintf(file, "ratio %i pos %i width %i no_reset %i\n", hist_HalfWidthMedian_ratio,
                histogramInfo.hist_HalfWidth_MedianPosition, histogramInfo.hist_HalfWidth,
                histogramInfo.hist_HalfWidth_noreset);
        fprintf(file, "Quarter pos %i width %i no_reset %i\n",
                histogramInfo.hist_HlafDozenWidth_MedianPosition, histogramInfo.hist_HlafDozenWidth,
                histogramInfo.hist_HlafDozenWidth_noreset);
        fprintf(file, "hq_pos_ratio %i hq_width_ratio %i\n", hist_half_quarter_pos_ratio,
                hist_half_quarter_width_ratio);
        fprintf(file, "max %f max_pos %i\n", histogramInfo.hist_max,
                histogramInfo.hist_max_position);
        fprintf(file, "hist_size %i\n", hist_size);
        fclose(file);
    }
#endif

    int _maxThres = hist_size - 1;
    int _minThres = 0;
    int _peakCenter = histogramInfo.hist_HalfWidth_MedianPosition;

    int room_light_index = hist_size - 1;
    _partialResult->room_light_score =
        get_room_light_ratio(phist, hist_size, histogramInfo, &room_light_index);

    int light_boundary;
    int dark_boundary;
    int break_boundary;

    get_boundarys(histogramInfo, phist_index, img_bkg_ratio_pos, img_bkg_ratio_neg,
                  room_light_index, _partialResult->room_light_score, _bkg_max, _modeltype,
                  _lenstype, &light_boundary, &dark_boundary, &break_boundary);

    if (histogramInfoValley_index > 0) {
        int dark_index = 0;
        int light_index = hist_size - 1;

        get_multi_peak_thres(
            histogramInfo, histogramInfoPeak, histogramInfoPeak_index, histogramInfoValley,
            histogramInfoValley_index, phist_index, img_bkg_ratio_pos, img_bkg_ratio_neg,
            room_light_index, _partialResult->room_light_score, _modeltype, _lenstype,
            light_boundary, dark_boundary, break_boundary, &light_index, &dark_index);

        if (dark_index != 0 && histogramInfo.hist_HalfWidth_MedianPosition - dark_index < 100) {
            _minThres = dark_index;
        }
    }

#ifdef DEBUG
    // FILE* file;
    if (fopen_s(&file, "hist_data.csv", "a") == 0 && _minThres == 0) {
        fprintf(file, ",%s", "no peaks");
        fclose(file);
    } else {
        fprintf(file, ",%s", "multi peak");
        fclose(file);
    }
#endif

    // decide _minThres
    if (_minThres == 0) {
        switch (_lenstype) {
            case LENS_3PA:
                if (img_bkg_ratio_neg > 0.2 && room_light_index - 120 > 0 &&
                    phist_index[room_light_index - 120] < 1500) {
                    _minThres = room_light_index - 120;
                } else if (img_bkg_ratio_neg > 0.1 && room_light_index - 100 > 0 &&
                           phist_index[room_light_index - 100] < 2000) {
                    _minThres = room_light_index - 100;
                } else if (img_bkg_ratio_neg > 0.5) {
                    _minThres = 0;
                } else if (img_bkg_ratio_neg < 0) {
                    _minThres = histogramInfo.hist_HalfWidth_MedianPosition -
                                histogramInfo.hist_HalfWidth * 0.8;
                } else if (img_bkg_ratio_neg < 0.1 && histogramInfo.hist_HalfWidth < 40 &&
                           histogramInfo.hist_HalfWidth ==
                               histogramInfo.hist_HalfWidth_noreset)  // normal
                {
                    _minThres = histogramInfo.hist_HlafDozenWidth_MedianPosition -
                                histogramInfo.hist_HlafDozenWidth;
                } else if (img_bkg_ratio_neg < 0.1) {
                    _minThres =
                        histogramInfo.hist_HalfWidth_MedianPosition - histogramInfo.hist_HalfWidth;
                } else if (img_bkg_ratio_neg > 0.1 && histogramInfo.hist_HalfWidth < 40 &&
                           histogramInfo.hist_HalfWidth ==
                               histogramInfo.hist_HalfWidth_noreset)  // normal, no need _minThres
                {
                } else {
                    _minThres =
                        histogramInfo.hist_HalfWidth_MedianPosition - histogramInfo.hist_HalfWidth -
                        (histogramInfo.hist_HlafDozenWidth - histogramInfo.hist_HalfWidth) / 3;
                }
                break;
            case LENS_3PC:
                if (_partialResult->room_light_score >= 80) {
                    if (histogramInfo.hist_HalfWidth > 40) {
                        _minThres = histogramInfo.hist_HalfWidth_MedianPosition - 40;
                    } else {
                        _minThres = histogramInfo.hist_HalfWidth_MedianPosition -
                                    histogramInfo.hist_HalfWidth;
                    }
                } else {
                    if (histogramInfo.hist_HalfWidth > 40) {
                        _minThres = histogramInfo.hist_HalfWidth_MedianPosition - 40;
                    } else {
                        _minThres = histogramInfo.hist_HalfWidth_MedianPosition -
                                    histogramInfo.hist_HalfWidth;
                    }
                }
                break;
            case LENS_3PF:
                // finger is too dark
                if (img_bkg_ratio_pos < 0.3 && room_light_index - 150 > 0 &&
                    phist_index[room_light_index - 150] < 1000) {
                    _minThres = room_light_index - 150;
                } else if (img_bkg_ratio_neg > 0.5) {
                    _minThres = 0;
                } else if (img_bkg_ratio_neg < 0) {
                    _minThres = histogramInfo.hist_HalfWidth_MedianPosition -
                                histogramInfo.hist_HalfWidth * 0.8;
                } else if (img_bkg_ratio_neg < 0.1 && histogramInfo.hist_HalfWidth < 40 &&
                           histogramInfo.hist_HalfWidth ==
                               histogramInfo.hist_HalfWidth_noreset)  // normal
                {
                    _minThres = histogramInfo.hist_HlafDozenWidth_MedianPosition -
                                histogramInfo.hist_HlafDozenWidth;
                } else if (img_bkg_ratio_neg < 0.1) {
                    _minThres =
                        histogramInfo.hist_HalfWidth_MedianPosition - histogramInfo.hist_HalfWidth;
                } else if (histogramInfo.hist_HlafDozenWidth > histogramInfo.hist_HalfWidth * 1.7) {
                    _minThres =
                        histogramInfo.hist_HalfWidth_MedianPosition - histogramInfo.hist_HalfWidth;
                } else {
                    _minThres =
                        histogramInfo.hist_HalfWidth_MedianPosition - histogramInfo.hist_HalfWidth -
                        (histogramInfo.hist_HlafDozenWidth - histogramInfo.hist_HalfWidth) / 3;
                }
                break;
            case LENS_UNKNOW:
            case LENS_2PA:
            case LENS_3PD:
            case LENS_3PE:
            case LENS_END:
            default:
                // finger is too dark
                if (img_bkg_ratio_pos < 0.3 && room_light_index - 150 > 0 &&
                    phist_index[room_light_index - 150] < 1000) {
                    _minThres = room_light_index - 150;
                } else if (img_bkg_ratio_neg > 0.5) {
                    _minThres = 0;
                } else if (img_bkg_ratio_neg < 0) {
                    _minThres = histogramInfo.hist_HalfWidth_MedianPosition -
                                histogramInfo.hist_HalfWidth * 0.8;
                } else if (img_bkg_ratio_neg < 0.1 && histogramInfo.hist_HalfWidth < 40 &&
                           histogramInfo.hist_HalfWidth ==
                               histogramInfo.hist_HalfWidth_noreset)  // normal
                {
                    _minThres = histogramInfo.hist_HlafDozenWidth_MedianPosition -
                                histogramInfo.hist_HlafDozenWidth;
                } else if (img_bkg_ratio_neg < 0.1) {
                    _minThres =
                        histogramInfo.hist_HalfWidth_MedianPosition - histogramInfo.hist_HalfWidth;
                } else if (histogramInfo.hist_HlafDozenWidth > histogramInfo.hist_HalfWidth * 1.7) {
                    _minThres =
                        histogramInfo.hist_HalfWidth_MedianPosition - histogramInfo.hist_HalfWidth;
                } else {
                    _minThres =
                        histogramInfo.hist_HalfWidth_MedianPosition - histogramInfo.hist_HalfWidth -
                        (histogramInfo.hist_HlafDozenWidth - histogramInfo.hist_HalfWidth) / 3;
                }
                break;
        }

        switch (_modeltype) {
            case MODEL_A91:

                // finger is too dark
                if (img_bkg_ratio_neg > 0.5) {
                    _minThres = 0;
                } else if (img_bkg_ratio_neg < 0.1) {
                    _minThres = histogramInfo.hist_HalfWidth_MedianPosition -
                                histogramInfo.hist_HalfWidth * 0.8;
                } else if (histogramInfo.hist_HlafDozenWidth > histogramInfo.hist_HalfWidth * 1.7) {
                    _minThres =
                        histogramInfo.hist_HalfWidth_MedianPosition - histogramInfo.hist_HalfWidth;
                } else {
                    _minThres =
                        histogramInfo.hist_HalfWidth_MedianPosition - histogramInfo.hist_HalfWidth -
                        (histogramInfo.hist_HlafDozenWidth - histogramInfo.hist_HalfWidth) / 3;
                }
                break;
            default:
                break;
        }

        if (_minThres > 0 && phist_index[_minThres] > dark_boundary) {
            _minThres = _minThres * 0.5;
        }
    }

    // decide _maxThres
    if (_maxThres == hist_size - 1) {
        _maxThres = room_light_index;
    }

    if (_maxThres > hist_size - 1) {
        _maxThres = hist_size - 1;
    } else if (_maxThres < 0) {
        _maxThres = 0;
    }

    if (_minThres > hist_size - 1) {
        _minThres = hist_size - 1;
    } else if (_minThres < 0) {
        _minThres = 0;
    }

    _peakCenter = (double)(_maxThres + _minThres) / 2;

    _partialResult->minThres = phist_index[_minThres];
    _partialResult->maxThres = phist_index[_maxThres];
    _partialResult->peakCenter = phist_index[_peakCenter];

#ifdef DEBUG
    if (fopen_s(&file, "hist_log.txt", "a") == 0) {
        fprintf(file, "_minThres %i _maxThres %i _peakCenter %i\n", _minThres, _maxThres,
                _peakCenter);
        fprintf(file, "_minThres %i _maxThres %i _peakCenter %i\n", _partialResult->minThres,
                _partialResult->maxThres, _partialResult->peakCenter);
        fclose(file);
    }
#endif

#ifdef DEBUG
    // FILE* file;
    if (fopen_s(&file, "hist_data.csv", "a") == 0) {
        int need_comma = 0;
        fprintf(file, ",%i,%i,%i,%i,%f,%i,%i,normal", hist_HalfWidthMedian_ratio,
                histogramInfo.hist_HalfWidth_MedianPosition, histogramInfo.hist_HalfWidth,
                histogramInfo.hist_HalfWidth_noreset, histogramInfo.hist_max,
                histogramInfo.hist_max_position, hist_size);
        fprintf(file, ",%i,%i,%i", histogramInfo.hist_HlafDozenWidth_MedianPosition,
                histogramInfo.hist_HlafDozenWidth, histogramInfo.hist_HlafDozenWidth_noreset);
        fprintf(file, ",%i,%i,%i", hist_width_ratio, hist_half_quarter_width_ratio,
                hist_half_quarter_pos_ratio);
        fprintf(file, "\n");
        fclose(file);
    }
#endif
}

static unsigned char* get_img_bkg_ratio_mask(
    int* _pImg, int _width, int _height, int _cx, int _cy, int _maxThres, int _minThres,
    int _peakCenter, float _max_thres_ratio, float _min_thres_ratio, int _radius_center_boundary,
    int _radius_boundary, float _room_light_area_normalize, float _room_light_inner_area_normalize,
    struct PartialDetectorResult* _sPartialDetectorResult) {
    int i, j;
    int sum_finger = 0;
    int sum_dark = 0;
    int sum_light = 0;
    int sum_finger_inner = 0;
    int sum_dark_inner = 0;
    int sum_light_inner = 0;
    unsigned char* mask = malloc(_width * _height * sizeof(unsigned char));

    if (_pImg == NULL || mask == NULL) return NULL;

    int maxThres, minThres;
    for (i = 0; i < _height; i++) {
        for (j = 0; j < _width; j++) {
            if (_pImg[i * _width + j] == MASK_NUM) {
                mask[i * _width + j] = 0;
                continue;
            }

            int dist = int_sqrt((_cx - j) * (_cx - j) + (_cy - i) * (_cy - i));
            if (dist < _radius_center_boundary) {
                maxThres = _maxThres + ABS(_maxThres - _peakCenter) * _max_thres_ratio;
                minThres = _minThres - ABS(_minThres - _peakCenter) * _min_thres_ratio;
            } else {
                maxThres = _maxThres;
                minThres = _minThres;
            }

            if (_pImg[i * _width + j] <= maxThres && _pImg[i * _width + j] >= minThres) {
                mask[i * _width + j] = 150;
                sum_finger++;
            } else if (_pImg[i * _width + j] < minThres) {
                mask[i * _width + j] = 50;
                sum_dark++;
            } else {
                mask[i * _width + j] = 255;
                sum_light++;
            }

            if (dist < _radius_center_boundary) {
                if (_pImg[i * _width + j] <= maxThres && _pImg[i * _width + j] >= minThres) {
                    sum_finger_inner++;
                } else if (_pImg[i * _width + j] < minThres) {
                    sum_dark_inner++;
                } else {
                    sum_light_inner++;
                }
            } else if (dist == _radius_center_boundary) {
                mask[i * _width + j] = mask[i * _width + j] - 5;
            } else if (dist == _radius_boundary) {
                mask[i * _width + j] = mask[i * _width + j] - 5;
            }
        }
    }

    _sPartialDetectorResult->partial_ratio =
        (double)(sum_finger * 100) / (sum_finger + sum_light + sum_dark);
    _sPartialDetectorResult->room_light_partial_ratio = (double)(sum_light * 100) /
                                                        (sum_finger + sum_light + sum_dark) *
                                                        _room_light_area_normalize;
    _sPartialDetectorResult->inner_partial_ratio =
        (double)(sum_finger_inner * 100) / (sum_finger_inner + sum_light_inner + sum_dark_inner);
    _sPartialDetectorResult->inner_room_light_partial_ratio =
        (double)(sum_light_inner * 100) / (sum_finger_inner + sum_light_inner + sum_dark_inner) *
        _room_light_inner_area_normalize;
    return mask;
}

static void apply_mask_inout(int* img, int width, int height, int cx, int cy, float radius,
                             int* masked_out, int* masked_in, int radius_boundary) {
    int i, j;
    for (i = 0; i < height; i++) {
        for (j = 0; j < width; j++) {
            int dist = int_sqrt((cx - j) * (cx - j) + (cy - i) * (cy - i));
            if (dist < radius) {
                masked_out[i * width + j] = MASK_NUM;
                masked_in[i * width + j] = img[i * width + j];
            } else if (dist > radius_boundary) {
                masked_out[i * width + j] = MASK_NUM;
                masked_in[i * width + j] = MASK_NUM;
            } else {
                masked_out[i * width + j] = img[i * width + j];
                masked_in[i * width + j] = MASK_NUM;
            }
        }
    }
}

static void apply_mask(int* img, int width, int height, int cx, int cy, int radius_boundary) {
    int i, j, k;
    for (i = 0; i < height; i++) {
        for (j = 0; j < width; j++) {
            int dist = int_sqrt((cx - j) * (cx - j) + (cy - i) * (cy - i));
            if (dist > radius_boundary) {
                img[i * width + j] = MASK_NUM;
            }
        }
    }
}

static void get_img_bkg_ratio(int* _pImg, int _width, int _height, int _cx, int _cy, int _maxThres,
                              int _minThres, int _peakCenter, float _max_thres_ratio,
                              float _min_thres_ratio, int _radius_center_boundary,
                              int _radius_boundary, float _room_light_area_normalize,
                              float _room_light_inner_area_normalize,
                              struct PartialDetectorResult* _sPartialDetectorResult) {
    int i, j;
    int sum_finger = 0;
    int sum_dark = 0;
    int sum_light = 0;
    int sum_finger_inner = 0;
    int sum_dark_inner = 0;
    int sum_light_inner = 0;
    int maxThres, minThres;
    for (i = 0; i < _height; i++) {
        for (j = 0; j < _width; j++) {
            if (_pImg[i * _width + j] == MASK_NUM) {
                continue;
            }

            int dist = int_sqrt((_cx - j) * (_cx - j) + (_cy - i) * (_cy - i));
            if (dist < _radius_center_boundary) {
                maxThres = _maxThres + ABS(_maxThres - _peakCenter) * _max_thres_ratio;
                minThres = _minThres - ABS(_minThres - _peakCenter) * _min_thres_ratio;
            } else {
                maxThres = _maxThres;
                minThres = _minThres;
            }

            if (_pImg[i * _width + j] <= maxThres && _pImg[i * _width + j] >= minThres) {
                sum_finger++;
            } else if (_pImg[i * _width + j] < minThres) {
                sum_dark++;
            } else {
                sum_light++;
            }

            if (dist < _radius_center_boundary) {
                if (_pImg[i * _width + j] <= maxThres && _pImg[i * _width + j] >= minThres) {
                    sum_finger_inner++;
                } else if (_pImg[i * _width + j] < minThres) {
                    sum_dark_inner++;
                } else {
                    sum_light_inner++;
                }
            }
        }
    }

    _sPartialDetectorResult->partial_ratio =
        (double)(sum_finger * 100) / (sum_finger + sum_light + sum_dark);
    _sPartialDetectorResult->room_light_partial_ratio = (double)(sum_light * 100) /
                                                        (sum_finger + sum_light + sum_dark) *
                                                        _room_light_area_normalize;
    _sPartialDetectorResult->inner_partial_ratio =
        (double)(sum_finger_inner * 100) / (sum_finger_inner + sum_light_inner + sum_dark_inner);
    _sPartialDetectorResult->inner_room_light_partial_ratio =
        (double)(sum_light_inner * 100) / (sum_finger_inner + sum_light_inner + sum_dark_inner) *
        _room_light_inner_area_normalize;
}

unsigned char* detect_partial(unsigned short* usimg, unsigned short* usbkg, int width, int height,
                              int _cx, int _cy,  // int _radius_boundary,
                              float _Et, int _HwInt, enum fp_type _fptype,
                              enum model_type _modeltype, enum lens_type _lenstype,
                              struct PartialDetectorResult* partialDetectorResult) {
    int i, j, k;
#ifdef DEBUG

    int bill_image_16bit_sum = 0;
    int bill_bkg_16bit_sum = 0;
    for (i = 0; i < width * height; i++) {
        bill_image_16bit_sum += usimg[i];
        bill_bkg_16bit_sum += usbkg[i];
    }
    printf("Egis_debug partial bill_image_16bit_sum %i\n", bill_image_16bit_sum);
    printf("Egis_debug partial bill_bkg_16bit_sum %i\n", bill_bkg_16bit_sum);

#endif  // DEBUG

#ifdef DEBUG
    FILE* file;
#endif

    int radius_center_boundary = 50;
    int radius_boundary = 85;
    float fEt_HwInt = 1;
    float sensor_weighting = 1;
    int shading_ratio = 1;
    float noisy_finger_thres = 0;
    float noisy_finger_ratio = 1;
    float room_light_area_normalize = 1;
    float room_light_inner_area_normalize = 1;
    switch (_fptype) {
        case FP_ET711:
            sensor_weighting = 1;
            break;
        case FP_ET713:
            sensor_weighting = 1;
            break;
        case FP_ET715:
            sensor_weighting = 1;
            break;
        default:
            break;
    }

    switch (_modeltype) {
        case MODEL_A50:
            break;
        case MODEL_A70:
            break;
        case MODEL_A80:
            break;
        case MODEL_TAB_S6:
            break;
        case MODEL_A90:
            break;
        case MODEL_A30S:
            shading_ratio = 10;
            break;
        case MODEL_A50S:
            shading_ratio = 5;
            break;
        case MODEL_A70S:
            break;
        case MODEL_A51:
            break;
        case MODEL_A71:
            break;
        case MODEL_A91:
            shading_ratio = 10;
            _Et = 50;
            _HwInt = 1;
            noisy_finger_thres = 0.1f;
            noisy_finger_ratio = 0.1f;
            break;
        case MODEL_A_NOTE:
            break;
        case MODEL_702:
            break;
        case MODEL_LG:
            noisy_finger_ratio = 5;
            break;
        default:
            break;
    }

    switch (_lenstype) {
        case LENS_2PA:
            radius_center_boundary = 50;
            radius_boundary = 85;
            fEt_HwInt = _Et * _HwInt / 150;
            break;
        case LENS_3PA:
            radius_center_boundary = 70;
            radius_boundary = 100;
            fEt_HwInt = _Et * _HwInt / 60;
            shading_ratio += 1;
            room_light_area_normalize =
                (double)radius_center_boundary * radius_center_boundary / 50 / 50;
            room_light_inner_area_normalize = (double)radius_boundary * radius_boundary / 85 / 85;
            break;
        case LENS_3PC:
            radius_center_boundary = 50;
            radius_boundary = 85;
            fEt_HwInt = _Et * _HwInt / 90;
            break;
        case LENS_3PD:
            radius_center_boundary = 50;
            radius_boundary = 85;
            fEt_HwInt = _Et * _HwInt / 90;
            break;
        case LENS_3PE:
            radius_center_boundary = 50;
            radius_boundary = 85;
            fEt_HwInt = _Et * _HwInt / 90;
            break;
        case LENS_3PF:
            radius_center_boundary = 75;
            radius_boundary = 105;
            fEt_HwInt = _Et * _HwInt / 60;
            room_light_area_normalize =
                (double)radius_center_boundary * radius_center_boundary / 50 / 50;
            room_light_inner_area_normalize = (double)radius_boundary * radius_boundary / 85 / 85;
            break;
        default:
            break;
    }

    int* img = (int*)malloc(width * height * sizeof(int));
    int* bkg = (int*)malloc(width * height * sizeof(int));
    if (img != NULL && bkg != NULL) {
        for (i = 0; i < width * height; i++) {
            img[i] = usimg[i];
            bkg[i] = usbkg[i];
        }

        mean_filter_integral(img, width, height, 5);
        mean_filter_integral(bkg, width, height, 5);
    }
#ifdef DEBUG
    int max;
    int min;
    {
        max = -0xFFFF;
        min = 0xFFFF;
        for (k = 0; k < width * height; k++) {
            if (img[k] == MASK_NUM) {
                continue;
            }
            max = max > img[k] ? max : img[k];
            min = min < img[k] ? min : img[k];
        }
        printf("Egis_debug partial img mean max %i\n", max);
        printf("Egis_debug partial img mean min %i\n", min);
    }
    {
        max = -0xFFFF;
        min = 0xFFFF;
        for (k = 0; k < width * height; k++) {
            if (bkg[k] == MASK_NUM) {
                continue;
            }
            max = max > bkg[k] ? max : bkg[k];
            min = min < bkg[k] ? min : bkg[k];
        }
        printf("Egis_debug partial bkg mean max %i\n", max);
        printf("Egis_debug partial bkg mean min %i\n", min);
    }

#endif  // DEBUG

    // int cx, cy;
    // determine_center(bkg, width, height, &cx, &cy);

    int* diff = (int*)malloc(width * height * sizeof(int));
    int dist = 0;
    if (img != NULL && bkg != NULL && diff != NULL) {
        for (i = 0; i < height; i++) {
            for (j = 0; j < width; j++) {
                dist = int_sqrt((_cx - j) * (_cx - j) + (_cy - i) * (_cy - i));
                diff[i * width + j] =
                    img[i * width + j] - bkg[i * width + j] + dist * shading_ratio;
            }
        }

        apply_mask(img, width, height, _cx, _cy, radius_boundary);
        apply_mask(diff, width, height, _cx, _cy, radius_boundary);
        apply_mask(bkg, width, height, _cx, _cy, radius_boundary);
    }

#ifdef DEBUG
    {
        max = -0xFFFF;
        min = 0xFFFF;
        for (k = 0; k < width * height; k++) {
            if (diff[k] == MASK_NUM) {
                continue;
            }

            if (max < diff[k]) {
                max = diff[k];
                printf("Egis_debug partial diff max %i %i\n", k, diff[k]);
            }

            if (min > diff[k]) {
                min = diff[k];
                printf("Egis_debug partial diff min %i %i\n", k, diff[k]);
            }

            max = max > diff[k] ? max : diff[k];
            min = min < diff[k] ? min : diff[k];
        }
        printf("Egis_debug partial diff max %i\n", max);
        printf("Egis_debug partial diff min %i\n", min);
    }

#endif  // DEBUG

    // int2CSV("diff.csv", diff, width, height);

    // analyze diff bkg ratio
    int hist_threshold = 20;
    float diff_bkg_ratio_pos_thres = 0;
    float diffmax_bkg_ratio_pos_thres = 0;
    float diff_bkg_ratio_neg_thres = 0;
    int* hist_diff = NULL;
    int* hist_diff_index = NULL;
    int hist_diff_size = 0;
    int hist_diff_min = 0, hist5_diff_max = 0, hist5_diff_min = 0;
    int bkg_org_min = 0, bkg_size = 0, bkg_max = 0, bkg_min = 0;

    {
        hist_diff = hist_maxmin(diff, width, height, fEt_HwInt, hist_threshold, &hist_diff_min,
                                &hist_diff_size, &hist5_diff_max, &hist5_diff_min, BAND_WIDTH);

        hist_diff_index = malloc(hist_diff_size * sizeof(int));

        if (hist_diff != NULL && hist_diff_index != NULL) {
            for (i = 0; i < hist_diff_size; i++) {
                hist_diff_index[i] = hist_diff_min + i * BAND_WIDTH * fEt_HwInt;
            }

            int* hist_bkg = NULL;
            hist_bkg = hist_maxmin(bkg, width, height, fEt_HwInt, hist_threshold, &bkg_org_min,
                                   &bkg_size, &bkg_max, &bkg_min, BAND_WIDTH);
            if (hist_bkg != NULL) {
                diffmax_bkg_ratio_pos_thres = (double)hist_diff_index[hist_diff_size - 1] /
                                              (bkg_max - bkg_min) * sensor_weighting;
                diff_bkg_ratio_pos_thres =
                    (double)hist5_diff_max / (bkg_max - bkg_min) * sensor_weighting;
                diff_bkg_ratio_neg_thres =
                    (double)hist5_diff_min / (bkg_max - bkg_min) * sensor_weighting;
                FREE(hist_bkg);
            }
        }
#ifdef DEBUG
        FILE* file;
        if (fopen_s(&file, "hist_log.txt", "a") == 0) {
            fprintf(file, "hist5_diff_max %i hist5_diff_min %i bkg_max %i bkg_min %i\n",
                    hist5_diff_max, hist5_diff_min, bkg_max, bkg_min);
            fprintf(file, "diffmax_bkg_ratio_pos_thres %f \n", diffmax_bkg_ratio_pos_thres);
            fprintf(file, "diff_bkg_ratio_pos_thres %f diff_bkg_ratio_neg_thres %f\n",
                    diff_bkg_ratio_pos_thres, diff_bkg_ratio_neg_thres);
            fclose(file);
        }

        if (fopen_s(&file, "hist_data.csv", "a") == 0) {
            int need_comma = 0;
            fprintf(file, ",%i,%i,%i,%i,%f,%f", hist5_diff_max, hist5_diff_min, bkg_max, bkg_min,
                    diff_bkg_ratio_pos_thres, diff_bkg_ratio_neg_thres);
            fclose(file);
        }
#endif
    }

    // masked diff
    int* masked_out = (int*)malloc(width * height * sizeof(int));
    int* masked_in = (int*)malloc(width * height * sizeof(int));
    apply_mask_inout(diff, width, height, _cx, _cy, 50, masked_out, masked_in, radius_boundary);

    // masked bkg
    int* masked_out_bkg = (int*)malloc(width * height * sizeof(int));
    int* masked_in_bkg = (int*)malloc(width * height * sizeof(int));
    apply_mask_inout(bkg, width, height, _cx, _cy, 50, masked_out_bkg, masked_in_bkg,
                     radius_boundary);

    float _mask_in_bkg_ratio_pos_thres = 0;
    float _mask_in_bkg_ratio_neg_thres = 0;
    int _mask_in_room_light_index = 0;
    int _mask_in_room_light_score = 0;
    {
        hist_threshold = 10;
        int hist_mask_in_min, hist_mask_in_size, hist5_mask_in_max, hist5_mask_in_min;

        int* _hist_masked_in =
            hist_maxmin(masked_in, width, height, fEt_HwInt, hist_threshold, &hist_mask_in_min,
                        &hist_mask_in_size, &hist5_mask_in_max, &hist5_mask_in_min, BAND_WIDTH);

        int* hist_mask_in_index = malloc(hist_mask_in_size * sizeof(int));

        if (_hist_masked_in != NULL && hist_mask_in_index != NULL) {
            for (i = 0; i < hist_mask_in_size; i++) {
                hist_mask_in_index[i] = hist_mask_in_min + i * BAND_WIDTH * fEt_HwInt;
            }

            int _masked_in_bkg_org_min, _masked_in_bkg_size, _masked_in_bkg_max, _masked_in_bkg_min;
            int* _hist_masked_in_bkg = hist_maxmin(
                bkg, width, height, fEt_HwInt, hist_threshold, &_masked_in_bkg_org_min,
                &_masked_in_bkg_size, &_masked_in_bkg_max, &_masked_in_bkg_min, BAND_WIDTH);

            // get max of _hist_masked_in
            if (_hist_masked_in_bkg != NULL) {
                int hist_masked_in_max = 0;
                for (i = 0; i < hist_mask_in_size; i++) {
                    if (hist_masked_in_max < _hist_masked_in[i]) {
                        hist_masked_in_max = _hist_masked_in[i];
                    }
                }

                _mask_in_room_light_score =
                    get_room_light_ratio_simple(_hist_masked_in, hist_mask_in_size,
                                                hist_masked_in_max, &_mask_in_room_light_index);
                _mask_in_bkg_ratio_pos_thres =
                    (double)hist_mask_in_index[_mask_in_room_light_index] /
                    (_masked_in_bkg_max - _masked_in_bkg_min) * sensor_weighting;
                _mask_in_bkg_ratio_neg_thres = (double)hist5_mask_in_min /
                                               (_masked_in_bkg_max - _masked_in_bkg_min) *
                                               sensor_weighting;

                FREE(_hist_masked_in_bkg);
            }
            FREE(_hist_masked_in);
            FREE(hist_mask_in_index);
        }
    }
#ifdef DEBUG
    if (fopen_s(&file, "hist_log.txt", "a") == 0) {
        fprintf(file, "_mask_in_bkg_ratio_pos_thres %f _mask_in_bkg_ratio_neg_thres %f\n",
                _mask_in_bkg_ratio_pos_thres, _mask_in_bkg_ratio_neg_thres);
        fprintf(file, "_mask_in_room_light_score %i\n", _mask_in_room_light_score);
        fclose(file);
    }

    if (fopen_s(&file, "hist_data.csv", "a") == 0) {
        int need_comma = 0;
        fprintf(file, ",%f,%f", _mask_in_bkg_ratio_pos_thres, _mask_in_bkg_ratio_neg_thres);
        fclose(file);
    }
#endif

    // split sunlight, roomlight and normal
    unsigned char* pucMask = NULL;

    partialDetectorResult->sun_light_score = 0;
    partialDetectorResult->room_light_score = 0;

    if (diff_bkg_ratio_pos_thres > 0) {
        partialDetectorResult->sun_light_score += (int)diff_bkg_ratio_pos_thres * 45;
    }

    float sun_diff_min_thres = (float)bkg_min;
    switch (_modeltype) {
        case MODEL_UNKNOW:
            break;
        case MODEL_A50:
            break;
        case MODEL_A70:
            break;
        case MODEL_A80:
            break;
        case MODEL_TAB_S6:
            break;
        case MODEL_A90:
            break;
        case MODEL_A30S:
            break;
        case MODEL_A50S:
            break;
        case MODEL_A70S:
            break;
        case MODEL_A51:
            break;
        case MODEL_A71:
            break;
        case MODEL_A91:
            sun_diff_min_thres = 900;
            break;
        case MODEL_A_NOTE:
            break;
        case MODEL_702:
            break;
        case MODEL_LG:
            break;
        case MODEL_END:
            break;
        default:
            break;
    }

    struct PartialResult partialResult;
    if (parse_hist_sunlight(hist_diff, hist_diff_index, hist_diff_size, diff_bkg_ratio_pos_thres,
                            diff_bkg_ratio_neg_thres, bkg_max, _modeltype, _lenstype,
                            &partialResult) == 1 ||
        (diffmax_bkg_ratio_pos_thres > 3 &&
         diff_bkg_ratio_pos_thres > 1))  // over exposure, use img not diff
    {
        if (hist5_diff_min > sun_diff_min_thres) {
            partialResult.minThres = hist_diff_index[0];
        }
        if (hist5_diff_max < 0) {
            partialResult.maxThres = hist_diff_index[hist_diff_size - 1];
        }

        float light_spec = 0;  // large is relax spec
        float dark_spec = 0;   // large is relax spec

        switch (_lenstype) {
            case LENS_3PA:
            case LENS_3PC:
            case LENS_3PF:
            case LENS_UNKNOW:
            case LENS_2PA:
            case LENS_3PD:
            case LENS_3PE:
            case LENS_END:
            default:
                if (partialResult.main_dark_peak > 0 || _mask_in_room_light_score >= 65 ||
                    _mask_in_bkg_ratio_neg_thres < -0.5 ||
                    _mask_in_bkg_ratio_pos_thres < -0.1)  // too dark
                {
                    dark_spec = 0;
                } else if (MAX(_mask_in_bkg_ratio_neg_thres, diff_bkg_ratio_neg_thres) >
                           noisy_finger_thres)  // avoid mask_in darker than diff
                {
                    dark_spec = 50;
                } else if (_mask_in_bkg_ratio_neg_thres < noisy_finger_thres) {
                    dark_spec = noisy_finger_ratio;
                } else {
                    dark_spec = 50;
                }
                break;
        }

#ifdef DEBUG
        if (fopen_s(&file, "hist_log.txt", "a") == 0) {
            fprintf(file, "dark_spec %f\n", dark_spec);
            fprintf(file, "light_spec %f\n", light_spec);
            fclose(file);
        }
#endif

        pucMask =
            get_img_bkg_ratio_mask(diff, width, height, _cx, _cy, partialResult.maxThres,
                                   partialResult.minThres, partialResult.peakCenter, light_spec,
                                   dark_spec, radius_center_boundary, radius_boundary,
                                   1,  // no need to enhance in normal case
                                   1,  // no need to enhance in normal case
                                   partialDetectorResult);
    } else if (parse_hist_roomlight(hist_diff, hist_diff_index, hist_diff_size,
                                    diff_bkg_ratio_pos_thres, diff_bkg_ratio_neg_thres, bkg_max,
                                    _modeltype, _lenstype, &partialResult) == 1) {
        if (hist5_diff_min > bkg_min) {
            partialResult.minThres = 0;
        }
        if (hist5_diff_max < 0) {
            partialResult.maxThres = hist_diff_index[hist_diff_size - 1];
        }

        float light_spec = 0;  // large is relax spec
        float dark_spec = 0;   // large is relax spec

        switch (_lenstype) {
            case LENS_3PA:
            case LENS_3PC:
            case LENS_3PF:
            case LENS_UNKNOW:
            case LENS_2PA:
            case LENS_3PD:
            case LENS_3PE:
            case LENS_END:
            default:
                if (partialResult.main_dark_peak > 0 || _mask_in_room_light_score >= 65 ||
                    _mask_in_bkg_ratio_neg_thres < -0.5 ||
                    _mask_in_bkg_ratio_pos_thres < -0.1)  // too dark
                {
                    dark_spec = 0;
                } else if (MAX(_mask_in_bkg_ratio_neg_thres, diff_bkg_ratio_neg_thres) >
                           noisy_finger_thres)  // avoid mask_in darker than diff
                {
                    dark_spec = 50;
                } else if (_mask_in_bkg_ratio_neg_thres < noisy_finger_thres) {
                    dark_spec = noisy_finger_ratio;
                } else {
                    dark_spec = 50;
                }
                break;
        }

        if (_mask_in_room_light_score >=
            90 /* || partialResult.main_light_peak == 1*/)  // too light
        {
            light_spec = 0.01f;
        } else if (_mask_in_room_light_score >= 80) {
            light_spec = 1.2f;
        } else if (_mask_in_room_light_score >= 40) {
            light_spec = 1.5f;
        } else {
            light_spec = 2;
        }

#ifdef DEBUG
        if (fopen_s(&file, "hist_log.txt", "a") == 0) {
            fprintf(file, "dark_spec %f\n", dark_spec);
            fprintf(file, "light_spec %f\n", light_spec);
            fclose(file);
        }
#endif
        pucMask = get_img_bkg_ratio_mask(diff, width, height, _cx, _cy, partialResult.maxThres,
                                         partialResult.minThres, partialResult.peakCenter,
                                         light_spec, dark_spec, radius_center_boundary,
                                         radius_boundary, room_light_area_normalize,
                                         room_light_inner_area_normalize, partialDetectorResult);
    } else  // normal case
    {
        // hist_threshold = 25;
        int* _hist_diff =
            hist_maxmin(masked_out, width, height, fEt_HwInt, hist_threshold, &hist_diff_min,
                        &hist_diff_size, &hist5_diff_max, &hist5_diff_min, BAND_WIDTH);
        int* _hist_diff_index = malloc(hist_diff_size * sizeof(int));
        for (i = 0; i < hist_diff_size; i++) {
            _hist_diff_index[i] = hist_diff_min + i * BAND_WIDTH * fEt_HwInt;
        }

        int* _hist_bkg;
        int _bkg_org_min, _bkg_size, _bkg_max, _bkg_min;

        _hist_bkg = hist_maxmin(bkg, width, height, fEt_HwInt, hist_threshold, &_bkg_org_min,
                                &_bkg_size, &_bkg_max, &_bkg_min, BAND_WIDTH);

        diff_bkg_ratio_pos_thres =
            (double)hist5_diff_max / (_bkg_max - _bkg_min) * sensor_weighting;
        diff_bkg_ratio_neg_thres =
            (double)hist5_diff_min / (_bkg_max - _bkg_min) * sensor_weighting;

#ifdef DEBUG
        if (fopen_s(&file, "hist_log.txt", "a") == 0) {
            fprintf(file, "%i %i %i %i %f %f\n", hist5_diff_max, hist5_diff_min, _bkg_max, _bkg_min,
                    diff_bkg_ratio_pos_thres, diff_bkg_ratio_neg_thres);
            fprintf(file, "diff_bkg_ratio_pos_thres %f diff_bkg_ratio_neg_thres %f\n",
                    diff_bkg_ratio_pos_thres, diff_bkg_ratio_neg_thres);
            fclose(file);
        }
#endif

        parse_hist(_hist_diff, _hist_diff_index, hist_diff_size, diff_bkg_ratio_pos_thres,
                   diff_bkg_ratio_neg_thres, bkg_max, _modeltype, _lenstype, &partialResult);

        if (hist5_diff_min > bkg_min) {
            partialResult.minThres = 0;
        }
        if (hist5_diff_max < 0) {
            partialResult.maxThres = hist_diff_index[hist_diff_size - 1];
        }

        float light_spec = 0;  // large is relax spec
        float dark_spec = 0;   // large is relax spec

        switch (_lenstype) {
            case LENS_3PA:
            case LENS_3PC:
            case LENS_3PF:
            case LENS_UNKNOW:
            case LENS_2PA:
            case LENS_3PD:
            case LENS_3PE:
            case LENS_END:
            default:
                if (partialResult.main_dark_peak > 0 || _mask_in_room_light_score >= 65 ||
                    _mask_in_bkg_ratio_neg_thres < -0.5 ||
                    _mask_in_bkg_ratio_pos_thres < -0.1)  // too dark
                {
                    dark_spec = 0.0f;
                } else if (MAX(_mask_in_bkg_ratio_neg_thres, diff_bkg_ratio_neg_thres) >
                           noisy_finger_thres)  // avoid mask_in darker than mask_out
                {
                    dark_spec = 50.0f;
                } else if (_mask_in_bkg_ratio_neg_thres < noisy_finger_thres) {
                    dark_spec = noisy_finger_ratio;
                } else {
                    dark_spec = 50.0f;
                }
                break;
        }

        if (_mask_in_room_light_score >=
            90 /* || partialResult.main_light_peak == 1*/)  // too light
        {
            light_spec = 0.01f;
        } else if (_mask_in_room_light_score >= 80) {
            light_spec = 0.5f;
        } else if (_mask_in_room_light_score >= 40) {
            light_spec = 1.0f;
        } else {
            light_spec = 1.5f;
        }

        if (_mask_in_bkg_ratio_neg_thres > 0.1 &&
            _mask_in_bkg_ratio_pos_thres < 0.5)  // center no light or dark
        {
            dark_spec = 100.0f;
            light_spec = 100.0f;
        }

#ifdef DEBUG
        if (fopen_s(&file, "hist_log.txt", "a") == 0) {
            fprintf(file, "dark_spec %f\n", dark_spec);
            fprintf(file, "light_spec %f\n", light_spec);
            fclose(file);
        }
#endif

        pucMask =
            get_img_bkg_ratio_mask(diff, width, height, _cx, _cy, partialResult.maxThres,
                                   partialResult.minThres, partialResult.peakCenter, light_spec,
                                   dark_spec, radius_center_boundary, radius_boundary,
                                   1,  // no need to enhance in normal case
                                   1,  // no need to enhance in normal case
                                   partialDetectorResult);

        FREE(_hist_diff);
        FREE(_hist_diff_index);
        FREE(_hist_bkg);
    }

    if (usimg != NULL && diff != NULL) {
        for (k = 0; k < width * height; k++) {
            if (diff[k] == MASK_NUM) {
                usimg[k] = 0;
            } else {
                usimg[k] = diff[k];
            }
        }
    }

    partialDetectorResult->room_light_score = partialResult.room_light_score;

    FREE(img);
    FREE(bkg);
    FREE(diff);
    FREE(hist_diff);
    FREE(hist_diff_index);
    // FREE(hist_img);
    // FREE(hist_img_index);
    FREE(masked_out);
    FREE(masked_in);
    FREE(masked_out_bkg);
    FREE(masked_in_bkg);
    return pucMask;
}

void detect_partial_SDK(unsigned short* usimg, unsigned short* usbkg, int width, int height,
                        int _cx, int _cy,  // int _radius_boundary,
                        float _Et, int _HwInt, enum fp_type _fptype, enum model_type _modeltype,
                        enum lens_type _lenstype,
                        struct PartialDetectorResult* partialDetectorResult) {
    int i, j, k;
#ifdef DEBUG

    int bill_image_16bit_sum = 0;
    int bill_bkg_16bit_sum = 0;
    for (i = 0; i < width * height; i++) {
        bill_image_16bit_sum += usimg[i];
        bill_bkg_16bit_sum += usbkg[i];
    }
    printf("Egis_debug partial bill_image_16bit_sum %i\n", bill_image_16bit_sum);
    printf("Egis_debug partial bill_bkg_16bit_sum %i\n", bill_bkg_16bit_sum);

#endif  // DEBUG

    int radius_center_boundary = 50;
    int radius_boundary = 85;
    float fEt_HwInt = 1;
    float sensor_weighting = 1;
    int shading_ratio = 1;
    float noisy_finger_thres = 0;
    float noisy_finger_ratio = 1;
    float room_light_area_normalize = 1;
    float room_light_inner_area_normalize = 1;
    switch (_fptype) {
        case FP_ET711:
            sensor_weighting = 1;
            break;
        case FP_ET713:
            sensor_weighting = 1;
            break;
        case FP_ET715:
            sensor_weighting = 1;
            break;
        default:
            break;
    }

    switch (_modeltype) {
        case MODEL_A50:
            break;
        case MODEL_A70:
            break;
        case MODEL_A80:
            break;
        case MODEL_TAB_S6:
            break;
        case MODEL_A90:
            break;
        case MODEL_A30S:
            shading_ratio = 10;
            break;
        case MODEL_A50S:
            shading_ratio = 5;
            break;
        case MODEL_A70S:
            break;
        case MODEL_A51:
            break;
        case MODEL_A71:
            break;
        case MODEL_A91:
            shading_ratio = 10;
            _Et = 50;
            _HwInt = 1;
            noisy_finger_thres = 0.1f;
            noisy_finger_ratio = 0.1f;
            break;
        case MODEL_A_NOTE:
            break;
        case MODEL_702:
            break;
        case MODEL_LG:
            noisy_finger_ratio = 5;
            break;
        default:
            break;
    }

    switch (_lenstype) {
        case LENS_2PA:
            radius_center_boundary = 50;
            radius_boundary = 85;
            fEt_HwInt = _Et * _HwInt / 150;
            break;
        case LENS_3PA:
            radius_center_boundary = 70;
            radius_boundary = 100;
            fEt_HwInt = _Et * _HwInt / 60;
            shading_ratio += 1;
            room_light_area_normalize =
                (double)radius_center_boundary * radius_center_boundary / 50 / 50;
            room_light_inner_area_normalize = (double)radius_boundary * radius_boundary / 85 / 85;
            break;
        case LENS_3PC:
            radius_center_boundary = 50;
            radius_boundary = 85;
            fEt_HwInt = _Et * _HwInt / 90;
            break;
        case LENS_3PD:
            radius_center_boundary = 50;
            radius_boundary = 85;
            fEt_HwInt = _Et * _HwInt / 90;
            break;
        case LENS_3PE:
            radius_center_boundary = 50;
            radius_boundary = 85;
            fEt_HwInt = _Et * _HwInt / 90;
            break;
        case LENS_3PF:
            radius_center_boundary = 75;
            radius_boundary = 105;
            fEt_HwInt = _Et * _HwInt / 60;
            room_light_area_normalize =
                (double)radius_center_boundary * radius_center_boundary / 50 / 50;
            room_light_inner_area_normalize = (double)radius_boundary * radius_boundary / 85 / 85;
            break;
        default:
            break;
    }

    int* img = (int*)malloc(width * height * sizeof(int));
    int* bkg = (int*)malloc(width * height * sizeof(int));
    if (img != NULL && bkg != NULL) {
        for (i = 0; i < width * height; i++) {
            img[i] = usimg[i];
            bkg[i] = usbkg[i];
        }

        mean_filter_integral(img, width, height, 5);
        mean_filter_integral(bkg, width, height, 5);
    }

    // int cx, cy;
    // determine_center(bkg, width, height, &cx, &cy);

    int* diff = (int*)malloc(width * height * sizeof(int));
    int dist = 0;
    if (img != NULL && bkg != NULL && diff != NULL) {
        for (i = 0; i < height; i++) {
            for (j = 0; j < width; j++) {
                dist = int_sqrt((_cx - j) * (_cx - j) + (_cy - i) * (_cy - i));
                diff[i * width + j] =
                    img[i * width + j] - bkg[i * width + j] + dist * shading_ratio;
            }
        }

        apply_mask(img, width, height, _cx, _cy, radius_boundary);
        apply_mask(diff, width, height, _cx, _cy, radius_boundary);
        apply_mask(bkg, width, height, _cx, _cy, radius_boundary);
    }

    // analyze diff bkg ratio
    int hist_threshold = 20;
    float diff_bkg_ratio_pos_thres = 0;
    float diffmax_bkg_ratio_pos_thres = 0;
    float diff_bkg_ratio_neg_thres = 0;
    int* hist_diff = NULL;
    int* hist_diff_index = NULL;
    int hist_diff_size = 0;
    int hist_diff_min = 0, hist5_diff_max = 0, hist5_diff_min = 0;
    int bkg_org_min = 0, bkg_size = 0, bkg_max = 0, bkg_min = 0;

    {
        hist_diff = hist_maxmin(diff, width, height, fEt_HwInt, hist_threshold, &hist_diff_min,
                                &hist_diff_size, &hist5_diff_max, &hist5_diff_min, BAND_WIDTH);

        hist_diff_index = malloc(hist_diff_size * sizeof(int));

        if (hist_diff != NULL && hist_diff_index != NULL) {
            for (i = 0; i < hist_diff_size; i++) {
                hist_diff_index[i] = hist_diff_min + i * BAND_WIDTH * fEt_HwInt;
            }

            int* hist_bkg = NULL;
            hist_bkg = hist_maxmin(bkg, width, height, fEt_HwInt, hist_threshold, &bkg_org_min,
                                   &bkg_size, &bkg_max, &bkg_min, BAND_WIDTH);
            if (hist_bkg != NULL) {
                diffmax_bkg_ratio_pos_thres = (double)hist_diff_index[hist_diff_size - 1] /
                                              (bkg_max - bkg_min) * sensor_weighting;
                diff_bkg_ratio_pos_thres =
                    (double)hist5_diff_max / (bkg_max - bkg_min) * sensor_weighting;
                diff_bkg_ratio_neg_thres =
                    (double)hist5_diff_min / (bkg_max - bkg_min) * sensor_weighting;
                FREE(hist_bkg);
            }
        }
    }

    // masked diff
    int* masked_out = (int*)malloc(width * height * sizeof(int));
    int* masked_in = (int*)malloc(width * height * sizeof(int));
    apply_mask_inout(diff, width, height, _cx, _cy, 50, masked_out, masked_in, radius_boundary);

    // masked bkg
    int* masked_out_bkg = (int*)malloc(width * height * sizeof(int));
    int* masked_in_bkg = (int*)malloc(width * height * sizeof(int));
    apply_mask_inout(bkg, width, height, _cx, _cy, 50, masked_out_bkg, masked_in_bkg,
                     radius_boundary);

    float _mask_in_bkg_ratio_pos_thres = 0;
    float _mask_in_bkg_ratio_neg_thres = 0;
    int _mask_in_room_light_index = 0;
    int _mask_in_room_light_score = 0;
    {
        hist_threshold = 10;
        int hist_mask_in_min, hist_mask_in_size, hist5_mask_in_max, hist5_mask_in_min;

        int* _hist_masked_in =
            hist_maxmin(masked_in, width, height, fEt_HwInt, hist_threshold, &hist_mask_in_min,
                        &hist_mask_in_size, &hist5_mask_in_max, &hist5_mask_in_min, BAND_WIDTH);

        int* hist_mask_in_index = malloc(hist_mask_in_size * sizeof(int));

        if (_hist_masked_in != NULL && hist_mask_in_index != NULL) {
            for (i = 0; i < hist_mask_in_size; i++) {
                hist_mask_in_index[i] = hist_mask_in_min + i * BAND_WIDTH * fEt_HwInt;
            }

            int _masked_in_bkg_org_min, _masked_in_bkg_size, _masked_in_bkg_max, _masked_in_bkg_min;
            int* _hist_masked_in_bkg = hist_maxmin(
                bkg, width, height, fEt_HwInt, hist_threshold, &_masked_in_bkg_org_min,
                &_masked_in_bkg_size, &_masked_in_bkg_max, &_masked_in_bkg_min, BAND_WIDTH);

            // get max of _hist_masked_in
            if (_hist_masked_in_bkg != NULL) {
                int hist_masked_in_max = 0;
                for (i = 0; i < hist_mask_in_size; i++) {
                    if (hist_masked_in_max < _hist_masked_in[i]) {
                        hist_masked_in_max = _hist_masked_in[i];
                    }
                }

                _mask_in_room_light_score =
                    get_room_light_ratio_simple(_hist_masked_in, hist_mask_in_size,
                                                hist_masked_in_max, &_mask_in_room_light_index);
                _mask_in_bkg_ratio_pos_thres =
                    (double)hist_mask_in_index[_mask_in_room_light_index] /
                    (_masked_in_bkg_max - _masked_in_bkg_min) * sensor_weighting;
                _mask_in_bkg_ratio_neg_thres = (double)hist5_mask_in_min /
                                               (_masked_in_bkg_max - _masked_in_bkg_min) *
                                               sensor_weighting;

                FREE(_hist_masked_in_bkg);
            }
            FREE(_hist_masked_in);
            FREE(hist_mask_in_index);
        }
    }

    // split sunlight, roomlight and normal

    partialDetectorResult->sun_light_score = 0;
    partialDetectorResult->room_light_score = 0;

    if (diff_bkg_ratio_pos_thres > 0) {
        partialDetectorResult->sun_light_score += (int)diff_bkg_ratio_pos_thres * 45;
    }

    float sun_diff_min_thres = bkg_min;
    switch (_modeltype) {
        case MODEL_UNKNOW:
            break;
        case MODEL_A50:
            break;
        case MODEL_A70:
            break;
        case MODEL_A80:
            break;
        case MODEL_TAB_S6:
            break;
        case MODEL_A90:
            break;
        case MODEL_A30S:
            break;
        case MODEL_A50S:
            break;
        case MODEL_A70S:
            break;
        case MODEL_A51:
            break;
        case MODEL_A71:
            break;
        case MODEL_A91:
            sun_diff_min_thres = 900;
            break;
        case MODEL_A_NOTE:
            break;
        case MODEL_702:
            break;
        case MODEL_LG:
            break;
        case MODEL_END:
            break;
        default:
            break;
    }

    struct PartialResult partialResult;
    if (parse_hist_sunlight(hist_diff, hist_diff_index, hist_diff_size, diff_bkg_ratio_pos_thres,
                            diff_bkg_ratio_neg_thres, bkg_max, _modeltype, _lenstype,
                            &partialResult) == 1 ||
        (diffmax_bkg_ratio_pos_thres > 3 &&
         diff_bkg_ratio_pos_thres > 1))  // over exposure, use img not diff
    {
        if (hist5_diff_min > sun_diff_min_thres) {
            partialResult.minThres = hist_diff_index[0];
        }
        if (hist5_diff_max < 0) {
            partialResult.maxThres = hist_diff_index[hist_diff_size - 1];
        }

        float light_spec = 0;  // large is relax spec
        float dark_spec = 0;   // large is relax spec

        switch (_lenstype) {
            case LENS_3PA:
            case LENS_3PC:
            case LENS_3PF:
            case LENS_UNKNOW:
            case LENS_2PA:
            case LENS_3PD:
            case LENS_3PE:
            case LENS_END:
            default:
                if (partialResult.main_dark_peak > 0 || _mask_in_room_light_score >= 65 ||
                    _mask_in_bkg_ratio_neg_thres < -0.5 ||
                    _mask_in_bkg_ratio_pos_thres < -0.1)  // too dark
                {
                    dark_spec = 0;
                } else if (MAX(_mask_in_bkg_ratio_neg_thres, diff_bkg_ratio_neg_thres) >
                           noisy_finger_thres)  // avoid mask_in darker than diff
                {
                    dark_spec = 50;
                } else if (_mask_in_bkg_ratio_neg_thres < noisy_finger_thres) {
                    dark_spec = noisy_finger_ratio;
                } else {
                    dark_spec = 50;
                }
                break;
        }

        get_img_bkg_ratio(diff, width, height, _cx, _cy, partialResult.maxThres,
                          partialResult.minThres, partialResult.peakCenter, light_spec, dark_spec,
                          radius_center_boundary, radius_boundary,
                          1,  // no need to enhance in normal case
                          1,  // no need to enhance in normal case
                          partialDetectorResult);
    } else if (parse_hist_roomlight(hist_diff, hist_diff_index, hist_diff_size,
                                    diff_bkg_ratio_pos_thres, diff_bkg_ratio_neg_thres, bkg_max,
                                    _modeltype, _lenstype, &partialResult) == 1) {
        if (hist5_diff_min > bkg_min) {
            partialResult.minThres = 0;
        }
        if (hist5_diff_max < 0) {
            partialResult.maxThres = hist_diff_index[hist_diff_size - 1];
        }

        float light_spec = 0;  // large is relax spec
        float dark_spec = 0;   // large is relax spec

        switch (_lenstype) {
            case LENS_3PA:
            case LENS_3PC:
            case LENS_3PF:
            case LENS_UNKNOW:
            case LENS_2PA:
            case LENS_3PD:
            case LENS_3PE:
            case LENS_END:
            default:
                if (partialResult.main_dark_peak > 0 || _mask_in_room_light_score >= 65 ||
                    _mask_in_bkg_ratio_neg_thres < -0.5 ||
                    _mask_in_bkg_ratio_pos_thres < -0.1)  // too dark
                {
                    dark_spec = 0;
                } else if (MAX(_mask_in_bkg_ratio_neg_thres, diff_bkg_ratio_neg_thres) >
                           noisy_finger_thres)  // avoid mask_in darker than diff
                {
                    dark_spec = 50;
                } else if (_mask_in_bkg_ratio_neg_thres < noisy_finger_thres) {
                    dark_spec = noisy_finger_ratio;
                } else {
                    dark_spec = 50;
                }
                break;
        }

        if (_mask_in_room_light_score >=
            90 /* || partialResult.main_light_peak == 1*/)  // too light
        {
            light_spec = 0.01f;
        } else if (_mask_in_room_light_score >= 80) {
            light_spec = 1.2f;
        } else if (_mask_in_room_light_score >= 40) {
            light_spec = 1.5f;
        } else {
            light_spec = 2;
        }

        get_img_bkg_ratio(diff, width, height, _cx, _cy, partialResult.maxThres,
                          partialResult.minThres, partialResult.peakCenter, light_spec, dark_spec,
                          radius_center_boundary, radius_boundary, room_light_area_normalize,
                          room_light_inner_area_normalize, partialDetectorResult);
    } else  // normal case
    {
        // hist_threshold = 25;
        int* _hist_diff =
            hist_maxmin(masked_out, width, height, fEt_HwInt, hist_threshold, &hist_diff_min,
                        &hist_diff_size, &hist5_diff_max, &hist5_diff_min, BAND_WIDTH);
        int* _hist_diff_index = malloc(hist_diff_size * sizeof(int));
        for (i = 0; i < hist_diff_size; i++) {
            _hist_diff_index[i] = hist_diff_min + i * BAND_WIDTH * fEt_HwInt;
        }

        int* _hist_bkg;
        int _bkg_org_min, _bkg_size, _bkg_max, _bkg_min;

        _hist_bkg = hist_maxmin(bkg, width, height, fEt_HwInt, hist_threshold, &_bkg_org_min,
                                &_bkg_size, &_bkg_max, &_bkg_min, BAND_WIDTH);

        diff_bkg_ratio_pos_thres =
            (double)hist5_diff_max / (_bkg_max - _bkg_min) * sensor_weighting;
        diff_bkg_ratio_neg_thres =
            (double)hist5_diff_min / (_bkg_max - _bkg_min) * sensor_weighting;

        parse_hist(_hist_diff, _hist_diff_index, hist_diff_size, diff_bkg_ratio_pos_thres,
                   diff_bkg_ratio_neg_thres, bkg_max, _modeltype, _lenstype, &partialResult);

        if (hist5_diff_min > bkg_min) {
            partialResult.minThres = 0;
        }
        if (hist5_diff_max < 0) {
            partialResult.maxThres = hist_diff_index[hist_diff_size - 1];
        }

        float light_spec = 0;  // large is relax spec
        float dark_spec = 0;   // large is relax spec

        switch (_lenstype) {
            case LENS_3PA:
            case LENS_3PC:
            case LENS_3PF:
            case LENS_UNKNOW:
            case LENS_2PA:
            case LENS_3PD:
            case LENS_3PE:
            case LENS_END:
            default:
                if (partialResult.main_dark_peak > 0 || _mask_in_room_light_score >= 65 ||
                    _mask_in_bkg_ratio_neg_thres < -0.5 ||
                    _mask_in_bkg_ratio_pos_thres < -0.1)  // too dark
                {
                    dark_spec = 0.0f;
                } else if (MAX(_mask_in_bkg_ratio_neg_thres, diff_bkg_ratio_neg_thres) >
                           noisy_finger_thres)  // avoid mask_in darker than mask_out
                {
                    dark_spec = 50.0f;
                } else if (_mask_in_bkg_ratio_neg_thres < noisy_finger_thres) {
                    dark_spec = noisy_finger_ratio;
                } else {
                    dark_spec = 50.0f;
                }
                break;
        }

        if (_mask_in_room_light_score >=
            90 /* || partialResult.main_light_peak == 1*/)  // too light
        {
            light_spec = 0.01f;
        } else if (_mask_in_room_light_score >= 80) {
            light_spec = 0.5f;
        } else if (_mask_in_room_light_score >= 40) {
            light_spec = 1.0f;
        } else {
            light_spec = 1.5f;
        }

        if (_mask_in_bkg_ratio_neg_thres > 0.1 &&
            _mask_in_bkg_ratio_pos_thres < 0.5)  // center no light or dark
        {
            dark_spec = 100.0f;
            light_spec = 100.0f;
        }

        get_img_bkg_ratio(diff, width, height, _cx, _cy, partialResult.maxThres,
                          partialResult.minThres, partialResult.peakCenter, light_spec, dark_spec,
                          radius_center_boundary, radius_boundary,
                          1,  // no need to enhance in normal case
                          1,  // no need to enhance in normal case
                          partialDetectorResult);

        FREE(_hist_diff);
        FREE(_hist_diff_index);
        FREE(_hist_bkg);
    }

    partialDetectorResult->room_light_score = partialResult.room_light_score;

    FREE(img);
    FREE(bkg);
    FREE(diff);
    FREE(hist_diff);
    FREE(hist_diff_index);
    FREE(masked_out);
    FREE(masked_in);
    FREE(masked_out_bkg);
    FREE(masked_in_bkg);
}

unsigned char* detect_partial_light(unsigned short* usimg, unsigned short* usbkg, int width,
                                    int height, int _cx, int _cy, int _radius_boundary, float _Et,
                                    int _HwInt, enum fp_type _fptype, enum model_type _modeltype,
                                    enum lens_type _lenstype,
                                    struct PartialDetectorResult* partialDetectorResult) {
    int i, j, k;
    //#ifdef DEBUG
    //
    //	int bill_image_16bit_sum = 0;
    //	int bill_bkg_16bit_sum = 0;
    //	for (i = 0; i < width * height; i++) {
    //		bill_image_16bit_sum += usimg[i];
    //		bill_bkg_16bit_sum += usbkg[i];
    //	}
    //	printf("Egis_debug partial bill_image_16bit_sum %i\n", bill_image_16bit_sum);
    //	printf("Egis_debug partial bill_bkg_16bit_sum %i\n", bill_bkg_16bit_sum);
    //
    //#endif // DEBUG

    int radius_center_boundary = 50;
    int radius_boundary = 85;
    float fEt_HwInt = 1;
    float sensor_weighting = 1;
    int shading_ratio = 1;
    float noisy_finger_thres = 0;
    float noisy_finger_ratio = 1;
    float room_light_area_normalize = 1;
    float room_light_inner_area_normalize = 1;

    switch (_fptype) {
        case FP_ET711:
            sensor_weighting = 1;
            break;
        case FP_ET713:
            sensor_weighting = 1;
            break;
        case FP_ET715:
            sensor_weighting = 1;
            break;
        default:
            break;
    }

    switch (_modeltype) {
        case MODEL_A50:
            break;
        case MODEL_A70:
            break;
        case MODEL_A80:
            break;
        case MODEL_TAB_S6:
            break;
        case MODEL_A90:
            break;
        case MODEL_A30S:
            shading_ratio = 10;
            break;
        case MODEL_A50S:
            shading_ratio = 5;
            break;
        case MODEL_A70S:
            break;
        case MODEL_A51:
            break;
        case MODEL_A71:
            break;
        case MODEL_A91:
            shading_ratio = 10;
            _Et = 50;
            _HwInt = 1;
            noisy_finger_thres = 0.1f;
            noisy_finger_ratio = 0.1f;
            break;
        case MODEL_A_NOTE:
            break;
        case MODEL_702:
            break;
        case MODEL_LG:
            noisy_finger_ratio = 5;
            break;
        case MODEL_HW:
            radius_center_boundary = 75;
            radius_boundary = 110;
            _Et = 55;
            _HwInt = 1;
            break;
        default:
            break;
    }

    switch (_lenstype) {
        case LENS_2PA:
            radius_center_boundary = 50;
            radius_boundary = 85;
            fEt_HwInt = _Et * _HwInt / 150;
            break;
        case LENS_3PA:
            radius_center_boundary = 70;
            radius_boundary = 100;
            fEt_HwInt = _Et * _HwInt / 60;
            shading_ratio += 1;
            room_light_area_normalize =
                (double)radius_center_boundary * radius_center_boundary / 50 / 50;
            room_light_inner_area_normalize = (double)radius_boundary * radius_boundary / 85 / 85;
            break;
        case LENS_3PC:
            radius_center_boundary = 50;
            radius_boundary = 85;
            fEt_HwInt = _Et * _HwInt / 90;
            break;
        case LENS_3PD:
            radius_center_boundary = 50;
            radius_boundary = 85;
            fEt_HwInt = _Et * _HwInt / 90;
            break;
        case LENS_3PE:
            radius_center_boundary = 50;
            radius_boundary = 85;
            fEt_HwInt = _Et * _HwInt / 90;
            break;
        case LENS_3PF:
            radius_center_boundary = 75;
            radius_boundary = 105;
            fEt_HwInt = _Et * _HwInt / 60;
            room_light_area_normalize =
                (double)radius_center_boundary * radius_center_boundary / 50 / 50;
            room_light_inner_area_normalize = (double)radius_boundary * radius_boundary / 85 / 85;
            break;
        default:
            break;
    }

    if (_radius_boundary < 200 && _radius_boundary > 0) {
        radius_boundary = _radius_boundary;
    }

    int* img = (int*)malloc(width * height * sizeof(int));
    int* bkg = (int*)malloc(width * height * sizeof(int));
    if (img != NULL && bkg != NULL) {
        for (i = 0; i < width * height; i++) {
            img[i] = usimg[i];
            bkg[i] = usbkg[i];
        }

        mean_filter_integral(img, width, height, 5);
        mean_filter_integral(bkg, width, height, 5);
    }
    //#ifdef DEBUG
    //	int max;
    //	int min;
    //	{
    //		max = -0xFFFF;
    //		min = 0xFFFF;
    //		for (k = 0; k < width * height; k++) {
    //			if (img[k] == MASK_NUM)
    //			{
    //				continue;
    //			}
    //			max = max > img[k] ? max : img[k];
    //			min = min < img[k] ? min : img[k];
    //		}
    //		printf("Egis_debug partial img mean max %i\n", max);
    //		printf("Egis_debug partial img mean min %i\n", min);
    //	}
    //	{
    //		max = -0xFFFF;
    //		min = 0xFFFF;
    //		for (k = 0; k < width * height; k++) {
    //			if (bkg[k] == MASK_NUM)
    //			{
    //				continue;
    //			}
    //			max = max > bkg[k] ? max : bkg[k];
    //			min = min < bkg[k] ? min : bkg[k];
    //		}
    //		printf("Egis_debug partial bkg mean max %i\n", max);
    //		printf("Egis_debug partial bkg mean min %i\n", min);
    //	}
    //
    //#endif // DEBUG

    // int cx, cy;
    // determine_center(bkg, width, height, &cx, &cy);

    int* diff = (int*)malloc(width * height * sizeof(int));
    int dist;
    if (img != NULL && bkg != NULL && diff != NULL) {
        for (i = 0; i < height; i++) {
            for (j = 0; j < width; j++) {
                dist = int_sqrt((_cx - j) * (_cx - j) + (_cy - i) * (_cy - i));
                diff[i * width + j] =
                    img[i * width + j] - bkg[i * width + j] + dist * shading_ratio;
            }
        }

        apply_mask(img, width, height, _cx, _cy, radius_boundary);
        apply_mask(diff, width, height, _cx, _cy, radius_boundary);
        apply_mask(bkg, width, height, _cx, _cy, radius_boundary);
    }

    //#ifdef DEBUG
    //	{
    //		max = -0xFFFF;
    //		min = 0xFFFF;
    //		for (k = 0; k < width * height; k++) {
    //			if (diff[k] == MASK_NUM)
    //			{
    //				continue;
    //			}
    //
    //			if (max < diff[k])
    //			{
    //				max = diff[k];
    //				//printf("Egis_debug partial diff max %i %i\n", k, diff[k]);
    //			}
    //
    //			if (min > diff[k])
    //			{
    //				min = diff[k];
    //				//printf("Egis_debug partial diff min %i %i\n", k, diff[k]);
    //			}
    //
    //			max = max > diff[k] ? max : diff[k];
    //			min = min < diff[k] ? min : diff[k];
    //		}
    //		printf("Egis_debug partial diff max %i\n", max);
    //		printf("Egis_debug partial diff min %i\n", min);
    //	}
    //
    //#endif // DEBUG

    // analyze diff bkg ratio
    int hist_threshold = 20;
    float diff_bkg_ratio_pos_thres = 0;
    float diffmax_bkg_ratio_pos_thres = 0;
    float diff_bkg_ratio_neg_thres = 0;
    int* hist_diff = NULL;
    int* hist_diff_index = NULL;
    int hist_diff_size = 0;
    int hist_diff_min, hist5_diff_max, hist5_diff_min;
    int bkg_org_min, bkg_size, bkg_max, bkg_min;
    int room_light_score = 0;
    int room_light_index = 0;
    int hist_diff_intensity_max = 0;

    {
        if (diff != NULL)
            hist_diff = hist_maxmin(diff, width, height, fEt_HwInt, hist_threshold, &hist_diff_min,
                                    &hist_diff_size, &hist5_diff_max, &hist5_diff_min, BAND_WIDTH);

        hist_diff_index = malloc(hist_diff_size * sizeof(int));

        if (hist_diff != NULL && hist_diff_index != NULL) {
            for (i = 0; i < hist_diff_size; i++) {
                hist_diff_index[i] = hist_diff_min + i * BAND_WIDTH * fEt_HwInt;
                hist_diff_intensity_max =
                    hist_diff_intensity_max > hist_diff[i] ? hist_diff_intensity_max : hist_diff[i];
            }

            int* hist_bkg;
            hist_bkg = hist_maxmin(bkg, width, height, fEt_HwInt, hist_threshold, &bkg_org_min,
                                   &bkg_size, &bkg_max, &bkg_min, BAND_WIDTH);

            diffmax_bkg_ratio_pos_thres = (double)hist_diff_index[hist_diff_size - 1] /
                                          (bkg_max - bkg_min) * sensor_weighting;
            diff_bkg_ratio_pos_thres =
                (double)hist5_diff_max / (bkg_max - bkg_min) * sensor_weighting;
            diff_bkg_ratio_neg_thres =
                (double)hist5_diff_min / (bkg_max - bkg_min) * sensor_weighting;

            // room_light_score = get_room_light_ratio_simple(hist_diff, hist_diff_size,
            // hist5_diff_max, &room_light_index);
            room_light_score = 0;
            for (i = hist_diff_size - 1; i >= 0; i--) {
                if (hist_diff[i] > MAX(hist_diff_intensity_max / 10, 50)) {
                    room_light_score = 100 - (double)i / hist_diff_size * 100;
                    room_light_index = i;
                    break;
                }
            }

#ifdef DEBUG
            FILE* file;
            if (fopen_s(&file, "hist_log.txt", "a") == 0) {
                fprintf(file, "hist5_diff_max %i hist5_diff_min %i bkg_max %i bkg_min %i\n",
                        hist5_diff_max, hist5_diff_min, bkg_max, bkg_min);
                fprintf(file, "diffmax_bkg_ratio_pos_thres %f \n", diffmax_bkg_ratio_pos_thres);
                fprintf(file, "diff_bkg_ratio_pos_thres %f diff_bkg_ratio_neg_thres %f\n",
                        diff_bkg_ratio_pos_thres, diff_bkg_ratio_neg_thres);
                fprintf(file, "room_light_score %i room_light_index %i %i\n", room_light_score,
                        room_light_index, hist_diff_index[room_light_index]);
                fclose(file);
            }

            if (fopen_s(&file, "hist_data.csv", "a") == 0) {
                int need_comma = 0;
                fprintf(file, ",%i,%i,%i,%i,%f,%f", hist5_diff_max, hist5_diff_min, bkg_max,
                        bkg_min, diff_bkg_ratio_pos_thres, diff_bkg_ratio_neg_thres);
                fclose(file);
            }
#endif

            FREE(hist_bkg);
        }
    }

    // split sunlight, roomlight and normal
#ifdef OUTPUT_MASK
    unsigned char* pucMask = NULL;
#endif

    partialDetectorResult->sun_light_score = 0;
    partialDetectorResult->room_light_score = 0;

    if (diff_bkg_ratio_pos_thres > 0) {
        partialDetectorResult->sun_light_score += (int)diff_bkg_ratio_pos_thres * 45;
    }

    if (room_light_score > 50 || diff_bkg_ratio_pos_thres > 1.5 ||
        (diffmax_bkg_ratio_pos_thres > 3 && diff_bkg_ratio_pos_thres > 1)) {
        if (hist_diff_index != NULL && diff != NULL) {
            int maxThres = hist_diff_index[room_light_index];
            int minThres = hist_diff_index[0];
            int peakCenter = (maxThres + minThres) >> 1;

            // if ((diff_bkg_ratio_pos_thres > 1.5 || (diffmax_bkg_ratio_pos_thres > 3 &&
            // diff_bkg_ratio_pos_thres > 1)) && 	maxThres > bkg_max)
            //{
            //	maxThres = 4000;
            //}
            // if (maxThres < 2000)
            //{
            //	maxThres = 2000;
            //}
            maxThres = 4000;

#ifdef DEBUG
            FILE* file;
            if (fopen_s(&file, "hist_log.txt", "a") == 0) {
                fprintf(file, "maxThres %i\n", maxThres);
                fclose(file);
            }
#endif

#ifdef OUTPUT_MASK
            pucMask =
                get_img_bkg_ratio_mask(diff, width, height, _cx, _cy, maxThres, minThres,
                                       peakCenter, 0, 0, radius_center_boundary, radius_boundary,
                                       1,  // no need to enhance in normal case
                                       1,  // no need to enhance in normal case
                                       partialDetectorResult);
#endif
        }
    } else {
#ifdef OUTPUT_MASK
        pucMask = malloc(width * height * sizeof(unsigned char));
        if (pucMask != NULL) memset(pucMask, 150, width * height * sizeof(unsigned char));
#endif
        partialDetectorResult->partial_ratio = 100;
        partialDetectorResult->room_light_partial_ratio = 100;
        partialDetectorResult->inner_partial_ratio = 100;
        partialDetectorResult->inner_room_light_partial_ratio = 100;
    }

#ifdef OUTPUT_MASK
    if (diff != NULL && img != NULL) {
        for (k = 0; k < width * height; k++) {
            // assert(k < width * height);
            if (diff[k] == MASK_NUM) {
                diff[k] = 0;
            }
        }
        memcpy(img, diff, width * height * sizeof(int));
    }
#endif

    partialDetectorResult->room_light_score = room_light_score;

    FREE(img);
    FREE(bkg);
    FREE(diff);
    FREE(hist_diff);
    FREE(hist_diff_index);
#ifdef OUTPUT_MASK
    return pucMask;
#else
    return NULL;
#endif
}

void detect_partial_light_SDK(unsigned short* usimg, unsigned short* usbkg, int width, int height,
                              int _cx, int _cy, int _radius_boundary, float _Et, int _HwInt,
                              enum fp_type _fptype, enum model_type _modeltype,
                              enum lens_type _lenstype,
                              struct PartialDetectorResult* partialDetectorResult) {
    if (usimg == NULL || usbkg == NULL) return;

    int i, j, k;
    //#ifdef DEBUG
    //
    //	int bill_image_16bit_sum = 0;
    //	int bill_bkg_16bit_sum = 0;
    //	for (i = 0; i < width * height; i++) {
    //		bill_image_16bit_sum += usimg[i];
    //		bill_bkg_16bit_sum += usbkg[i];
    //	}
    //	printf("Egis_debug partial bill_image_16bit_sum %i\n", bill_image_16bit_sum);
    //	printf("Egis_debug partial bill_bkg_16bit_sum %i\n", bill_bkg_16bit_sum);
    //
    //#endif // DEBUG

    int radius_center_boundary = 50;
    int radius_boundary = 85;
    float fEt_HwInt = 1;
    float sensor_weighting = 1;
    int shading_ratio = 1;
    float noisy_finger_thres = 0;
    float noisy_finger_ratio = 1;
    float room_light_area_normalize = 1;
    float room_light_inner_area_normalize = 1;

    switch (_fptype) {
        case FP_ET711:
            sensor_weighting = 1;
            break;
        case FP_ET713:
            sensor_weighting = 1;
            break;
        case FP_ET715:
            sensor_weighting = 1;
            break;
        default:
            break;
    }

    switch (_modeltype) {
        case MODEL_A50:
            break;
        case MODEL_A70:
            break;
        case MODEL_A80:
            break;
        case MODEL_TAB_S6:
            break;
        case MODEL_A90:
            break;
        case MODEL_A30S:
            shading_ratio = 10;
            break;
        case MODEL_A50S:
            shading_ratio = 5;
            break;
        case MODEL_A70S:
            break;
        case MODEL_A51:
            break;
        case MODEL_A71:
            break;
        case MODEL_A91:
            shading_ratio = 10;
            _Et = 50;
            _HwInt = 1;
            noisy_finger_thres = 0.1f;
            noisy_finger_ratio = 0.1f;
            break;
        case MODEL_A_NOTE:
            break;
        case MODEL_702:
            break;
        case MODEL_LG:
            noisy_finger_ratio = 5;
            break;
        case MODEL_HW:
            radius_center_boundary = 75;
            radius_boundary = 110;
            _Et = 55;
            _HwInt = 1;
            break;
        default:
            break;
    }

    switch (_lenstype) {
        case LENS_2PA:
            radius_center_boundary = 50;
            radius_boundary = 85;
            fEt_HwInt = _Et * _HwInt / 150;
            break;
        case LENS_3PA:
            radius_center_boundary = 70;
            radius_boundary = 100;
            fEt_HwInt = _Et * _HwInt / 60;
            shading_ratio += 1;
            room_light_area_normalize =
                (double)radius_center_boundary * radius_center_boundary / 50 / 50;
            room_light_inner_area_normalize = (double)radius_boundary * radius_boundary / 85 / 85;
            break;
        case LENS_3PC:
            radius_center_boundary = 50;
            radius_boundary = 85;
            fEt_HwInt = _Et * _HwInt / 90;
            break;
        case LENS_3PD:
            radius_center_boundary = 50;
            radius_boundary = 85;
            fEt_HwInt = _Et * _HwInt / 90;
            break;
        case LENS_3PE:
            radius_center_boundary = 50;
            radius_boundary = 85;
            fEt_HwInt = _Et * _HwInt / 90;
            break;
        case LENS_3PF:
            radius_center_boundary = 75;
            radius_boundary = 105;
            fEt_HwInt = _Et * _HwInt / 60;
            room_light_area_normalize =
                (double)radius_center_boundary * radius_center_boundary / 50 / 50;
            room_light_inner_area_normalize = (double)radius_boundary * radius_boundary / 85 / 85;
            break;
        default:
            break;
    }

    if (_radius_boundary < 200 && _radius_boundary > 0) {
        radius_boundary = _radius_boundary;
    }

    int* img = (int*)malloc(width * height * sizeof(int));
    int* bkg = (int*)malloc(width * height * sizeof(int));
    if (img != NULL && bkg != NULL) {
        for (i = 0; i < width * height; i++) {
            img[i] = usimg[i];
            bkg[i] = usbkg[i];
        }

        mean_filter_integral(img, width, height, 5);
        mean_filter_integral(bkg, width, height, 5);
    }

    //#ifdef DEBUG
    //	int max;
    //	int min;
    //	{
    //		max = -0xFFFF;
    //		min = 0xFFFF;
    //		for (k = 0; k < width * height; k++) {
    //			if (img[k] == MASK_NUM)
    //			{
    //				continue;
    //			}
    //			max = max > img[k] ? max : img[k];
    //			min = min < img[k] ? min : img[k];
    //		}
    //		printf("Egis_debug partial img mean max %i\n", max);
    //		printf("Egis_debug partial img mean min %i\n", min);
    //	}
    //	{
    //		max = -0xFFFF;
    //		min = 0xFFFF;
    //		for (k = 0; k < width * height; k++) {
    //			if (bkg[k] == MASK_NUM)
    //			{
    //				continue;
    //			}
    //			max = max > bkg[k] ? max : bkg[k];
    //			min = min < bkg[k] ? min : bkg[k];
    //		}
    //		printf("Egis_debug partial bkg mean max %i\n", max);
    //		printf("Egis_debug partial bkg mean min %i\n", min);
    //	}
    //
    //#endif // DEBUG

    // int cx, cy;
    // determine_center(bkg, width, height, &cx, &cy);

    int* diff = (int*)malloc(width * height * sizeof(int));
    int dist = 0;
    if (diff != NULL && img != NULL && bkg != NULL) {
        for (i = 0; i < height; i++) {
            for (j = 0; j < width; j++) {
                dist = int_sqrt((_cx - j) * (_cx - j) + (_cy - i) * (_cy - i));
                diff[i * width + j] =
                    img[i * width + j] - bkg[i * width + j] + dist * shading_ratio;
            }
        }

        apply_mask(img, width, height, _cx, _cy, radius_boundary);
        apply_mask(diff, width, height, _cx, _cy, radius_boundary);
        apply_mask(bkg, width, height, _cx, _cy, radius_boundary);
    }

    //#ifdef DEBUG
    //	{
    //		max = -0xFFFF;
    //		min = 0xFFFF;
    //		for (k = 0; k < width * height; k++) {
    //			if (diff[k] == MASK_NUM)
    //			{
    //				continue;
    //			}
    //
    //			if (max < diff[k])
    //			{
    //				max = diff[k];
    //				//printf("Egis_debug partial diff max %i %i\n", k, diff[k]);
    //			}
    //
    //			if (min > diff[k])
    //			{
    //				min = diff[k];
    //				//printf("Egis_debug partial diff min %i %i\n", k, diff[k]);
    //			}
    //
    //			max = max > diff[k] ? max : diff[k];
    //			min = min < diff[k] ? min : diff[k];
    //		}
    //		printf("Egis_debug partial diff max %i\n", max);
    //		printf("Egis_debug partial diff min %i\n", min);
    //	}
    //
    //#endif // DEBUG

    // analyze diff bkg ratio
    int hist_threshold = 20;
    float diff_bkg_ratio_pos_thres = 0;
    float diffmax_bkg_ratio_pos_thres = 0;
    float diff_bkg_ratio_neg_thres = 0;
    int* hist_diff = NULL;
    int* hist_diff_index = NULL;
    int hist_diff_size = 0;
    int hist_diff_min, hist5_diff_max, hist5_diff_min;
    int bkg_org_min, bkg_size, bkg_max, bkg_min;
    int room_light_score = 0;
    int room_light_index = 0;
    int hist_diff_intensity_max = 0;

    {
        hist_diff = hist_maxmin(diff, width, height, fEt_HwInt, hist_threshold, &hist_diff_min,
                                &hist_diff_size, &hist5_diff_max, &hist5_diff_min, BAND_WIDTH);

        hist_diff_index = malloc(hist_diff_size * sizeof(int));

        if (hist_diff != NULL && hist_diff_index != NULL) {
            for (i = 0; i < hist_diff_size; i++) {
                hist_diff_index[i] = hist_diff_min + i * BAND_WIDTH * fEt_HwInt;
                hist_diff_intensity_max =
                    hist_diff_intensity_max > hist_diff[i] ? hist_diff_intensity_max : hist_diff[i];
            }

            int* hist_bkg;
            hist_bkg = hist_maxmin(bkg, width, height, fEt_HwInt, hist_threshold, &bkg_org_min,
                                   &bkg_size, &bkg_max, &bkg_min, BAND_WIDTH);

            diffmax_bkg_ratio_pos_thres = (double)hist_diff_index[hist_diff_size - 1] /
                                          (bkg_max - bkg_min) * sensor_weighting;
            diff_bkg_ratio_pos_thres =
                (double)hist5_diff_max / (bkg_max - bkg_min) * sensor_weighting;
            diff_bkg_ratio_neg_thres =
                (double)hist5_diff_min / (bkg_max - bkg_min) * sensor_weighting;

            // room_light_score = get_room_light_ratio_simple(hist_diff, hist_diff_size,
            // hist5_diff_max, &room_light_index);
            room_light_score = 0;
            for (i = hist_diff_size - 1; i >= 0; i--) {
                if (hist_diff[i] > MAX(hist_diff_intensity_max / 10, 50)) {
                    room_light_score = 100 - (double)i / hist_diff_size * 100;
                    room_light_index = i;
                    break;
                }
            }

#ifdef DEBUG
            FILE* file;
            if (fopen_s(&file, "hist_log.txt", "a") == 0) {
                fprintf(file, "hist5_diff_max %i hist5_diff_min %i bkg_max %i bkg_min %i\n",
                        hist5_diff_max, hist5_diff_min, bkg_max, bkg_min);
                fprintf(file, "diffmax_bkg_ratio_pos_thres %f \n", diffmax_bkg_ratio_pos_thres);
                fprintf(file, "diff_bkg_ratio_pos_thres %f diff_bkg_ratio_neg_thres %f\n",
                        diff_bkg_ratio_pos_thres, diff_bkg_ratio_neg_thres);
                fprintf(file, "room_light_score %i room_light_index %i %i\n", room_light_score,
                        room_light_index, hist_diff_index[room_light_index]);
                fclose(file);
            }

            if (fopen_s(&file, "hist_data.csv", "a") == 0) {
                int need_comma = 0;
                fprintf(file, ",%i,%i,%i,%i,%f,%f", hist5_diff_max, hist5_diff_min, bkg_max,
                        bkg_min, diff_bkg_ratio_pos_thres, diff_bkg_ratio_neg_thres);
                fclose(file);
            }
#endif

            FREE(hist_bkg);
        }
    }

    partialDetectorResult->sun_light_score = 0;
    partialDetectorResult->room_light_score = 0;

    if (diff_bkg_ratio_pos_thres > 0) {
        partialDetectorResult->sun_light_score += (int)diff_bkg_ratio_pos_thres * 45;
    }

    if (room_light_score > 50 || diff_bkg_ratio_pos_thres > 1.5 ||
        (diffmax_bkg_ratio_pos_thres > 3 && diff_bkg_ratio_pos_thres > 1)) {
        if (hist_diff_index != NULL && diff != NULL) {
            int maxThres = hist_diff_index[room_light_index];
            int minThres = hist_diff_index[0];
            int peakCenter = (maxThres + minThres) >> 1;

            // if ((diff_bkg_ratio_pos_thres > 1.5 || (diffmax_bkg_ratio_pos_thres > 3 &&
            // diff_bkg_ratio_pos_thres > 1)) && 	maxThres > bkg_max)
            //{
            //	maxThres = 4000;
            //}
            // if (maxThres < 2000)
            //{
            //	maxThres = 2000;
            //}
            maxThres = 4000;

            get_img_bkg_ratio(diff, width, height, _cx, _cy, maxThres, minThres, peakCenter, 0, 0,
                              radius_center_boundary, radius_boundary,
                              1,  // no need to enhance in normal case
                              1,  // no need to enhance in normal case
                              partialDetectorResult);
        }
    } else {
        partialDetectorResult->partial_ratio = 100;
        partialDetectorResult->room_light_partial_ratio = 100;
        partialDetectorResult->inner_partial_ratio = 100;
        partialDetectorResult->inner_room_light_partial_ratio = 100;
    }

    partialDetectorResult->room_light_score = room_light_score;

    FREE(img);
    FREE(bkg);
    FREE(diff);
    FREE(hist_diff);
    FREE(hist_diff_index);
}
