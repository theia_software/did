#ifdef __cplusplus
extern "C"
{
#endif

#include <stdlib.h>
#include <stdio.h>

//Error Code
#define IMAGE_POOL_OK	0
#define IMAGE_POOL_UPDATE_NOT_READY	1
#define IMAGE_POOL_INVALID_PARAMETER			-1000
#define IMAGE_POOL_ALLOC_MEM_FAIL				-1001
#define IMAGE_POOL_VERSION_NON_SYNC				-1002
#define IMAGE_POOL_NOT_READY					-1003
#define IMAGE_POOL_IMAGE_VARIANT				-1004
#define IMAGE_POOL_QTY_SIZE						-2001
#define IMAGE_POOL_ET516_SIZE					-2002
#define IMAGE_POOL_IPpalm_SIZE					-2003

#define SCALE_1	2048

#ifndef ISP_UTILITY_H
#define ISP_UTILITY_H

	// used by rolling ball
    //int IPsqrt_2e12(int x);

	// used by gaussian blur
	int *build_kernel_idx_760(int size, int ksize);
	//int *IPconvolution_symmetric_760(int *img16, int width, int height, int *kernel_2e15, int ksize);
	//void IPconvolution_symmetric_buf_760(int *img16_src, int *img16_dst, int width, int height, int *kernel_2e15, int ksize);

	// used by local contrast enhance
	int *matrix_dot_760(int *matrix_a, int *matrix_b, int m1, int n1);
	int *IPaverage_to_v2_760(int *img16, int width, int height, int window);
	int IPsqrt_2e10_760(int x);

	// used by multiband
	//void IPnormalize_to(int *img16, int size, int max, int min);
	void IPnormalize_to_v5(int *img16, int size, int max, int min);
	//int* IPclone(int *src, int size);
	void IPmax_min_normalize_to_760(int *img16, int size, int max, int min);

	// used by tmp roi
	unsigned int isqrt_760(unsigned int x);
	//unsigned int isqrt_v2(long long x);

	// used by local laplacian
	//unsigned int *IPaverage_to_v3(unsigned int *img16, int width, int height, int window);
	unsigned int *matrix_dot_v2_760(unsigned int *matrix_a, unsigned int *matrix_b, int m1, int n1);

	// used by IPP6.0
	//void bilateral_filter(int *src, int *dst, int width, int height, int filter_size, int max_val, const unsigned int*sigma_tbl);
	void IPcast16_to8_760(unsigned char *dst, int *src, int size);

	// used by IPP6.5
	int max_min_analysis_760(int *img16_src_2e11, int w, int h, int roi_win, int thOverExpo, int *maxGobal, int *minGobal, int *max_roi, int *min_roi, int *maxExpoLimit);

#endif

#ifdef __cplusplus
}
#endif