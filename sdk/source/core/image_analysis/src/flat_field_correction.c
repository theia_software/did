//
//  flat_field_correction.c
//  AUO_BDS_ISP
//
//  Created by Luan Liu on 2020/8/11.
//  Copyright © 2020 Theia. All rights reserved.
//

#include "flat_field_correction.h"
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#ifndef min
#define min(a,b)            (((a) < (b)) ? (a) : (b))
#endif

#ifndef max
#define max(a,b)            (((a) > (b)) ? (a) : (b))
#endif

static float ISPcal_mean(float *data, int size)
{
    float sum = 0.0f;
    int i;
    
    for (i = 0; i < size; i++) {
        sum += data[i];
    }
    
    return sum / size;
}


static void ISPcal_mean_std(float *data, int size, float *mean, float *std)
{
    float diff, sum_sqrt_diff = 0.0f;
    int i;
    
    *mean = ISPcal_mean(data, size);
    
    for (i = 0; i < size; i++) {
        diff = data[i] - *mean;
        sum_sqrt_diff  += diff * diff;
    }

    *std = sqrt(sum_sqrt_diff / (size - 1));
}


static void ffc(int *img, int *bkg, int *w_bkg, float *result, int width, int height)
{
    int i;
    float mean, std;
    int size = width * height;
    float thr1, thr2;
    float *diff = (float*)malloc(sizeof(float) * size);
    
    for (i = 0; i < size; i++) {
        result[i] = (float)(img[i] - bkg[i]);
    }
    
    
    for (i = 0; i < size; i++) {
        diff[i] = (float)(w_bkg[i] - bkg[i]);
    }
    
    

    ISPcal_mean_std(diff, size, &mean, &std);
    thr1 = mean - 3 * std;
    thr2 = mean + 3 * std;
    
   
    for (i = 0; i < size; i++) {
        if (diff[i] < thr1) {
            diff[i] = thr1;
        }
        
        if (diff[i] > thr2) {
            diff[i] = thr2;
        }
        
    }
    
    
    
    //remove_impulse_noise_r1(diff, width, height, 0.8);
    
    if (thr1 > 0) {
        for (i = 0; i < size; i++) {
            result[i] = result[i] /diff[i];
        }
    }
    
    free(diff);
    
    
}


static float array_meanf(float* arr, int length){
    float result = 0.0;
    for(int i = 0; i < length; i++){
        result += arr[i];
    }
    return result / length;
}


static float array_mean(int* arr, int length){
    float result = 0.0;
    for(int i = 0; i < length; i++){
        result += arr[i];
    }
    return result / length;
}


void et760_FlatFieldCorrection(int *img, int *bkg, int *w_bkg, int *result, int width, int height)
{
    float* middle_result = (float*)malloc(width * height * sizeof(float));
    float* horizontal_mean = (float*)malloc(height * sizeof(float));
    float* vertical_mean_upper = (float*)malloc(width * sizeof(float));
    float* vertical_mean_lower = (float*)malloc(width * sizeof(float));
    
    ffc(img, bkg, w_bkg, middle_result, width, height);
    
    // mapping to 16-bit
    int bkg_mean = (int)roundf(array_mean(bkg, width * height));
    int w_bkg_mean = (int)roundf(array_mean(w_bkg, width * height));
    for(int i = 0; i < width * height; i++) {
        result[i] = (int)roundf(middle_result[i] * (w_bkg_mean - bkg_mean) + bkg_mean);
        result[i] = max(result[i], 0);
        result[i] = min(result[i], 65535);
    }
    
    free(middle_result);
    free(horizontal_mean);
    free(vertical_mean_upper);
    free(vertical_mean_lower);
}

