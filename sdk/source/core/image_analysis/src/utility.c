#include "ipsys.h"

const int MUL_TBL_2E16_760[5] = { 65536, 92686, 65536 * 2, 185364, 65536 * 4 }; //1, sqrt(2), sqrt(4), sqrt(8), sqrt(16)
static unsigned char IPsqrt_760(int x) {

	if (x < IP_SQRT_8_MAX) return IPSqrt8_760[IP_SQRT_8_INDEX(x)];
	unsigned char result = (x) < IP_SQRT_10_MAX ? IPSqrt10_760[IP_SQRT_10_INDEX(x)]
		: (x) < IP_SQRT_12_MAX ? IPSqrt12_760[IP_SQRT_12_INDEX(x)]
		: (x) < IP_SQRT_14_MAX ? IPSqrt14_760[IP_SQRT_14_INDEX(x)]
		: IPSqrt16_760[IP_SQRT_16_INDEX(x)];
	if (x > (result*result + result)) result++;
	return result;
}
#ifdef WIN32
#define __builtin_clz __lzcnt
#endif
int IPsqrt_2e10_760(int x) {
	if (x <= 255 * 255) return IPsqrt_760(x);
	int d, xx, i = 16 - __builtin_clz(x);
	int ret = (IPsqrt_760(x >> i) * MUL_TBL_2E16_760[i] + 32768) / 65536;
search_near:
	xx = IPSqr(ret);
	if ((d = xx - x) > 0) {
		if (2 * ret - 1 >= 2 * d) return ret;
		ret--;
	}
	else {
		if (2 * ret + 1 >= -2 * d) return ret;
		ret++;
	}
	goto search_near;
}
//int IPsqrt_2e12(int x) {
//	if (x <= 255 * 255) return IPsqrt(x);
//	if (x <= 0x3FF * 0x3FF) return IPsqrt_2e10_760(x);
//	int d, xx, i = 12 - __builtin_clz(x);
//	int ret = (IPsqrt_2e10_760(x >> i) * MUL_TBL_2E16_760[i] + 32768) / 65536;
//search_near:
//	xx = IPSqr(ret);
//	if ((d = xx - x) > 0) {
//		if (2 * ret - 1 >= 2 * d) return ret;
//		ret--;
//	}
//	else {
//		if (2 * ret + 1 >= -2 * d) return ret;
//		ret++;
//	}
//	goto search_near;
//}


int *build_kernel_idx_760(int size, int ksize) {
	int i, k = ksize / 2, *idx = IPAllocIntRow_760(size + k * 2);
	if (idx == NULL) return NULL;
	for (i = -k; i < size + k; i++) {
		if (i < 0) idx[i + k] = -i;
		else if (i >= size) idx[i + k] = 2 * size - i - 1;
		else idx[i + k] = i;
	}
	return idx;
}

//void IPconvolution_symmetric_buf_760(int *img16_src, int *img16_dst, int width, int height, int *kernel_2e15, int ksize) {
//	int i, j, *idx, k, *row = img16_src, *p, sum, pos = 0;
//	idx = build_kernel_idx_760(width, ksize);
//
//	for (i = 0; i < height; i++, row += width) {
//		for (j = 0, p = idx; j < width; j++, p++) {
//			for (k = 0, sum = 0; k < ksize; k++)
//				sum += row[p[k]] * kernel_2e15[k];
//			img16_dst[pos++] = (sum + (1 << 14)) >> 15;
//		}
//	}
//	IPFree((void*)idx);
//	IPCopyMem(img16_src, img16_dst, width*height*sizeof(int));
//	idx = build_kernel_idx_760(height, ksize);
//	for (i = 0; i < height + (ksize & 0xFE); i++) idx[i] *= width; //convert idx to offset
//	for (j = 0; j < width; j++, img16_src++) {
//		for (i = 0, p = idx; i < height; i++, p++) {
//			for (k = 0, sum = 0; k < ksize; k++)
//				sum += img16_src[p[k]] * kernel_2e15[k];
//			img16_dst[j + i*width] = (sum + (1 << 14)) >> 15;
//		}
//	}
//	IPFree((void*)idx);
//}

//int *IPconvolution_symmetric_760(int *img16, int width, int height, int *kernel_2e15, int ksize) {
//	int *img16_dst = IPAllocIntRow_760(width*height);
//	IPconvolution_symmetric_buf_760(img16, img16_dst, width, height, kernel_2e15, ksize);
//	return img16_dst;
//}

int *matrix_dot_760(int *matrix_a, int *matrix_b, int m1, int n1){
	int i;
	int *multiply = (int*)IPAlloc_760(m1 * n1 * sizeof(int));

	for (i = 0; i < n1*m1; i++)
		multiply[i] = matrix_a[i] * matrix_b[i];

	return multiply;
}

int *IPaverage_to_v2_760(int *img16, int width, int height, int window) {
	int **image = IPAllocIntImage_760(width, height);
	IPCopyMem(image[0], img16, width * height * sizeof(int));

	const int fullWindow = IPFullWindow(window);
	const int memHeight = fullWindow < height ? fullWindow : height;

	int mi;
	int iW, i, iFW;
	int lineCount = 0;
	int * * theImage = IPAllocRow_760(int *, height);
	int * sumLine = IPCAllocIntRow_760(width);
	for (mi = 0; mi < memHeight; mi++)
		theImage[mi] = IPAllocIntRow_760(width);

	for (iW = 0, i = -window, iFW = -fullWindow; i < height; iW++, i++, iFW++)
	{
		if (iFW >= 0)
		{
			int j;
			int *img_row = theImage[iFW];
			for (j = 0; j < width; j++)
				sumLine[j] -= img_row[j];
			lineCount--;
		}
		if (iW < height)
		{
			int j;
			int *img_row;
			if (iFW >= 0) theImage[iW] = theImage[iFW];
			IPCopyIntRow(theImage[iW], image[iW], width);
			img_row = theImage[iW];
			for (j = 0; j < width; j++)
				sumLine[j] += img_row[j];
			lineCount++;
		}
		if (i >= 0)
		{
			int jW, j, jFW;
			int sum = 0;
			int pixelCount = 0;
			int *img_row = image[i];
			for (jW = 0, j = -window, jFW = -fullWindow; j < width; jW++, j++, jFW++)
			{
				if (jFW >= 0)
				{
					sum -= sumLine[jFW];
					pixelCount -= lineCount;
				}
				if (jW < width)
				{
					sum += sumLine[jW];
					pixelCount += lineCount;
				}
				if (j >= 0)
				{
					img_row[j] = sum / pixelCount;
				}
			}
		}
	}
	for (mi = height - memHeight; mi < height; mi++)
		IPFreeRow(theImage[mi]);
	IPFreeRow(theImage);
	IPFreeRow(sumLine);

	int *ret = (int*)IPAlloc_760(width * height * sizeof(int));
	IPCopyMem(ret, image[0], width * height * sizeof(int));
	IPFree2DImage_760((void **)image);
	return ret;
}

//void IPnormalize_to(int *img16, int size, int max, int min) {
//	int i, diff;
//	diff = max - min;
//	if (diff > 0) {
//		int ratio_2e16 = 0xFFFF / diff;
//		for (i = 0; i < size; i++) {
//			img16[i] = (img16[i] < min ? 0 : img16[i] > max ? 0xFFFF : (img16[i] - min) * ratio_2e16);
//		}
//	}
//}
void IPnormalize_to_v5(int *img16, int size, int max, int min) {

	int i;
	int diffSrc, diffDst;
	diffDst = max - min;
	int src_max = 0, src_min = 65535 << 8;
	if (diffDst > 0) {
		for (i = 0; i < size; i++) {
			if (img16[i] > src_max) src_max = (int)img16[i];
			if (img16[i] < src_min) src_min = (int)img16[i];
		}
		diffSrc = (src_max - src_min);
		for (i = 0; i < size; i++) {
			img16[i] = (int)(((long long)(img16[i] - src_min)*diffDst) / diffSrc + min);
		}
	}
}

unsigned int isqrt_760(unsigned int x) {
	unsigned int op, res, one;

	op = x;
	res = 0;
	one = 1 << 30;
	while (one > op) one >>= 2;
	while (one != 0) {
		if (op >= res + one) {
			op -= res + one;
			res += one << 1;
		}
		res >>= 1;
		one >>= 2;
	}
	return res;
}
//unsigned int isqrt_v2(long long x) {
//	long long op, res, one;
//
//	op = x;
//	res = 0;
//	one = 1 << 30;
//	while (one > op) one >>= 2;
//	while (one != 0) {
//		if (op >= res + one) {
//			op -= res + one;
//			res += one << 1;
//		}
//		res >>= 1;
//		one >>= 2;
//	}
//	return (int)res;
//}

//unsigned int *IPaverage_to_v3(unsigned int *img16, int width, int height, int window) {
//	unsigned int **image = (unsigned int **)IPAllocIntImage(width, height);
//	IPCopyMem(image[0], img16, width * height * sizeof(unsigned int));
//
//	const int fullWindow = IPFullWindow(window);
//	const int memHeight = fullWindow < height ? fullWindow : height;
//
//	int mi;
//	int iW, i, iFW;
//	int lineCount = 0;
//	unsigned int * * theImage = IPAllocRow(unsigned int *, height);
//	unsigned int * sumLine = (unsigned int *)IPCAllocIntRow(width);
//	for (mi = 0; mi < memHeight; mi++)
//		theImage[mi] = (unsigned int *)IPAllocIntRow(width);
//
//	for (iW = 0, i = -window, iFW = -fullWindow; i < height; iW++, i++, iFW++)
//	{
//		if (iFW >= 0)
//		{
//			int j;
//			int *img_row = (int*)theImage[iFW];
//			for (j = 0; j < width; j++)
//				sumLine[j] -= img_row[j];
//			lineCount--;
//		}
//		if (iW < height)
//		{
//			int j;
//			int *img_row;
//			if (iFW >= 0) theImage[iW] = theImage[iFW];
//			IPCopyIntRow(theImage[iW], image[iW], width);
//			img_row = (int*)theImage[iW];
//			for (j = 0; j < width; j++)
//				sumLine[j] += img_row[j];
//			lineCount++;
//		}
//		if (i >= 0)
//		{
//			int jW, j, jFW;
//			int sum = 0;
//			int pixelCount = 0;
//			int *img_row = (int*)image[i];
//			for (jW = 0, j = -window, jFW = -fullWindow; j < width; jW++, j++, jFW++)
//			{
//				if (jFW >= 0)
//				{
//					sum -= sumLine[jFW];
//					pixelCount -= lineCount;
//				}
//				if (jW < width)
//				{
//					sum += sumLine[jW];
//					pixelCount += lineCount;
//				}
//				if (j >= 0)
//				{
//					img_row[j] = sum / pixelCount;
//				}
//			}
//		}
//	}
//	for (mi = height - memHeight; mi < height; mi++)
//		IPFreeRow(theImage[mi]);
//	IPFreeRow(theImage);
//	IPFreeRow(sumLine);
//
//	unsigned int *ret = (unsigned int*)IPAlloc_760(width * height * sizeof(unsigned int));
//	IPCopyMem(ret, image[0], width * height * sizeof(unsigned int));
//	IPFree2DImage((void **)image);
//	return ret;
//}
unsigned int *matrix_dot_v2_760(unsigned int *matrix_a, unsigned int *matrix_b, int m1, int n1){
	int i;
	unsigned int *multiply = (unsigned int*)IPAlloc_760(m1 * n1 * sizeof(unsigned int));

	for (i = 0; i < n1*m1; i++)
		multiply[i] = matrix_a[i] * matrix_b[i];

	return multiply;
}

static unsigned int space_color_factor(int dist_diff, int color_diff, const unsigned int *sigmal_tbl){
	color_diff = IPAbs(color_diff);
	if (color_diff >= 20) return 0;
	return sigmal_tbl[color_diff] * sigmal_tbl[IPAbs(dist_diff)];
}
//void bilateral_filter(int *src, int *dst, int width, int height, int filter_size, int max_val, const unsigned int*sigma_tbl) {
//	int i, j, r, c, len = filter_size / 2;
//	int *row = src;
//	for (j = 0; j < height; j++, row += width) {
//		for (i = 0; i < width; i++) {
//			unsigned long long k_2e32 = 0, f_2e32 = 0, factor_2e32;
//
//			for (r = i - len; r <= i + len; r++) {
//				if (r < 0 || r >= width) continue;
//				//factor_2e32 = space_factor(i - r) * color_factor((int)row[i] - (int)row[r]);
//				factor_2e32 = space_color_factor(i - r, row[i] - row[r], sigma_tbl);
//				f_2e32 += row[r] * factor_2e32;
//				k_2e32 += factor_2e32;
//			}
//			int *row2 = row - len*width;
//			for (c = j - len; c <= j + len; c++, row2 += width) {
//				if (c < 0 || c >= height) continue;
//				//factor_2e32 = space_factor(j - c) * color_factor((int)row[i] - (int)row2[i]);
//				factor_2e32 = space_color_factor(j - c, row[i] - row2[i], sigma_tbl);
//				f_2e32 += row2[i] * factor_2e32;
//				k_2e32 += factor_2e32;
//			}
//			*dst++ = k_2e32>0 ? (int)(f_2e32 / k_2e32) : max_val;
//		}
//	}
//}
void IPcast16_to8_760(unsigned char *dst, int *src, int size) {
	int i;
	for (i = 0; i < size; i++) {
		dst[i] = src[i];
	}
}

int max_min_analysis_760(int *img16_src_2e11, int w, int h, int roi_win, int thOverExpo, int *maxGobal, int *minGobal, int *max_roi, int *min_roi, int *maxExpoLimit) {
	int i, j, *img16;
	//	int maxGobal = 0, minGobal = SCALE_1 * SCALE_1, max_roi = 0, min_roi = SCALE_1 * SCALE_1;
	//	int maxExpoLimit = 0;
	int roi_left_col = (w - roi_win) / 2;
	int roi_top_row = (h - roi_win) / 2;
	int roi_right_col = roi_left_col + roi_win - 1;
	int roi_bottom_row = roi_top_row + roi_win - 1;

	//	int avgSrc=0;
	int cntOverExpo = 0;
	for (i = 0, img16 = img16_src_2e11; i < h; i++, img16 += w) {
		for (j = 0; j < w; j++) {
			//            avgSrc+=img16[j];
			if (img16[j] > *maxGobal) *maxGobal = img16[j];
			if (img16[j] < *minGobal) *minGobal = img16[j];
			if (img16[j]<thOverExpo){
				if (img16[j] > *maxExpoLimit) *maxExpoLimit = img16[j];
				if (i >= roi_top_row && i <= roi_bottom_row && j >= roi_left_col && j <= roi_right_col) {
					if (img16[j] > *max_roi) *max_roi = img16[j];
					if (img16[j] < *min_roi) *min_roi = img16[j];
				}
			}
			else
				cntOverExpo++;
		}
	}
	return cntOverExpo;
}

//int* IPclone(int *src, int size) {
//	int *ret = IPAllocIntRow(size);
//	if (ret) IPCopyMem(ret, src, size*sizeof(int));
//	return ret;
//}
void IPmax_min_normalize_to_760(int *img16, int size, int max, int min) {
	int i;
	int diff_src, diff_dst;
	diff_dst = (int)((max - min) << 15);
	int trans = 0;

	int src_max = 0, src_min = 65535;
	if (diff_dst > 0) {
		for (i = 0; i < size; i++) {
			if (img16[i] > src_max) src_max = (int)img16[i];
			if (img16[i] < src_min) src_min = (int)img16[i];
		}
		diff_src = (src_max - src_min);
		trans = (int)(diff_dst / diff_src);
		for (i = 0; i < size; i++) {
			img16[i] = (int)((((img16[i] - src_min)*trans) >> 15) + min);
		}
	}
}
