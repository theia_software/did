#include <string.h>

#include "partial_hist.h"
//#include "fileio.h"

#define EGIS_DEBUG
#include "plat_log.h"
#define LOG_TAG "RBS-Sensor"

#ifdef OUTPUT_MASK
static unsigned char* mask;
#endif

struct SplitResult {
    float sharp_score;
    int contrast_score;

    int max_peaks_num;
    int max_valleys_num;
    int max_profile_valley;

    int second_peaks_num;
    int second_valleys_num;
    int second_profile_valley;

    int weak_max_peaks_num;
    int weak_second_peaks_num;
    float mean;
    int sun_factor;
    int is_period;
    int is_light;
    int is_out_boundary;
};

static unsigned int int_sqrt(unsigned int x) {
    unsigned int op, res, one;

    op = x;
    res = 0;

    one = 1 << 30;
    while (one > op) one >>= 2;

    while (one != 0) {
        if (op >= res + one) {
            op -= res + one;
            res += one << 1;
        }
        res >>= 1;
        one >>= 2;
    }
    return res;
}

static void analyze_hist_simple(int* hist_input,
                                // int* _phist_index,
                                int hist_size,
                                // int kernel_size,
                                // float img_bkg_ratio_pos,
                                // float img_bkg_ratio_neg,
                                // enum model_type _modeltype,
                                // enum lens_type _lenstype,
                                float thres, float peak_peak_dist_thres,
                                struct HistogramInfoPeak* histogramInfoPeak,
                                int* histogramInfoPeak_index,
                                struct HistogramInfoValley* histogramInfoValley,
                                int* histogramInfoValley_index,
                                struct HistogramInfo* histogramInfo) {
    int i, j, k;

    int max = -0xFFFF;
    int min = 0xFFFF;
    for (i = 0; i < hist_size; ++i) {
        max = max > hist_input[i] ? max : hist_input[i];
        min = min < hist_input[i] ? min : hist_input[i];
    }

#ifdef EGIS_DEBUG
    egislog_d("Egis_debug partial 520");
#endif

    // get local peaks and valley
    int _hist_max = 0;
    int _hist_max_position = 0;
    int iHistogramInfoPeak_index = 0;
    int iHistogramInfoValley_index = 0;
    float local_max = 0;
    float local_min = 0;
    // float thres = 10;
    float thres_default = thres;
    int sun_index = 2000;
    float thres_p = thres_default;
    float thres_v = thres_default;
    int kernel_half = 0;
    int search = 5;
    for (i = 0; i < hist_size; ++i) {
        if (i == 0)  // initial
        {
            local_max = hist_input[i];
            local_min = hist_input[i];

            if (hist_input[i] > (max + min) * 0.5) {
                histogramInfoPeak[iHistogramInfoPeak_index].index = i;
                histogramInfoPeak[iHistogramInfoPeak_index].intensity = hist_input[i];
                iHistogramInfoPeak_index++;
            } else {
                histogramInfoValley[iHistogramInfoValley_index].index = i;
                histogramInfoValley[iHistogramInfoValley_index].intensity = hist_input[i];
                iHistogramInfoValley_index++;
            }
        }

        if (hist_input[i] > _hist_max) {
            _hist_max = hist_input[i];
            _hist_max_position = i;
        }

        int debug = hist_input[i];
        if (hist_input[i] > local_max && hist_input[i] >= thres) {
            local_max = hist_input[i];
        } else if (hist_input[i] < local_min) {
            local_min = hist_input[i];
        }

        int local_peak = 1;
        for (j = i - search; j < i + search; j++) {
            if (j < 0 || j >= hist_size) {
                continue;
            }
            if (hist_input[i] < hist_input[j]) {
                local_peak = 0;
                break;
            }
        }

        int local_valley = 1;
        for (j = i - search; j < i + search; j++) {
            if (j < 0 || j >= hist_size) {
                continue;
            }
            if (hist_input[i] > hist_input[j]) {
                local_valley = 0;
                break;
            }
        }

#ifdef DEBUG
        if (local_peak == 1 || local_valley == 1) {
            printf("");
        }
#endif  // DEBUG

        if (local_peak == 1 && hist_input[i] >= local_max && (local_max - local_min) >= thres_p) {
            // last index is peak not valley, replace it
            if (iHistogramInfoPeak_index > 0 && iHistogramInfoValley_index > 0 &&
                histogramInfoValley[iHistogramInfoValley_index - 1].index <
                    histogramInfoPeak[iHistogramInfoPeak_index - 1].index) {
                histogramInfoPeak[iHistogramInfoPeak_index - 1].index = i;
                histogramInfoPeak[iHistogramInfoPeak_index - 1].intensity = hist_input[i];
            } else if (iHistogramInfoPeak_index > 0 && iHistogramInfoValley_index == 0) {
                histogramInfoPeak[iHistogramInfoPeak_index - 1].index = i;
                histogramInfoPeak[iHistogramInfoPeak_index - 1].intensity = hist_input[i];
            } else {
                // check finger peak-peak distance
                if (iHistogramInfoPeak_index > 0 &&
                    i - histogramInfoPeak[iHistogramInfoPeak_index - 1].index >
                        peak_peak_dist_thres) {
                    break;
                }

                histogramInfoPeak[iHistogramInfoPeak_index].index = i;
                histogramInfoPeak[iHistogramInfoPeak_index].intensity = hist_input[i];
                iHistogramInfoPeak_index++;
                thres_v = thres_default;
            }
            local_min = local_max;
            thres_p = 1;
        }

        if (local_valley == 1 && hist_input[i] <= local_min)  // no valley on the peak
        {
            if (local_max - local_min >= thres_v) {
                if (iHistogramInfoPeak_index > 0 && iHistogramInfoValley_index > 0 &&
                    histogramInfoPeak[iHistogramInfoPeak_index - 1].index <
                        histogramInfoValley[iHistogramInfoValley_index - 1].index) {
                    histogramInfoValley[iHistogramInfoValley_index - 1].index = i;
                    histogramInfoValley[iHistogramInfoValley_index - 1].intensity = hist_input[i];
                } else {
                    // check finger valley-valley distance
                    if (iHistogramInfoValley_index > 0 &&
                        i - histogramInfoValley[iHistogramInfoValley_index - 1].index >
                            peak_peak_dist_thres) {
                        break;
                    }

                    histogramInfoValley[iHistogramInfoValley_index].index = i;
                    histogramInfoValley[iHistogramInfoValley_index].intensity = hist_input[i];
                    iHistogramInfoValley_index++;
                    thres_p = thres_default;
                }
                local_max = local_min;
                thres_v = 1;
            }
        }

        if (iHistogramInfoPeak_index >= 50) {
            break;
        }
        if (iHistogramInfoValley_index >= 50) {
            break;
        }
    }

#ifdef EGIS_DEBUG
    egislog_d("Egis_debug partial 521");
#endif

    *histogramInfoPeak_index = iHistogramInfoPeak_index;
    *histogramInfoValley_index = iHistogramInfoValley_index;

    int _hist_HalfWidth = 0;
    int _hist_HalfWidth_noreset = 0;
    int _hist_HlafDozenWidth = 0;
    int _hist_HlafDozenWidth_noreset = 0;
    // find hist median
    {
        // Half Width
        int _sum = 0;
        int get_peak = 0;
        for (i = kernel_half; i < hist_size - kernel_half; ++i) {
            // if (i < waive_dark_peak_index)
            //{
            //	continue;
            //}

            if (hist_input[i] == _hist_max) {
                get_peak = 1;
            }

            if (hist_input[i] > _hist_max / 2) {
                _sum += i;
                _hist_HalfWidth++;
            }

            if (_sum > 0 && get_peak == 0 && hist_input[i] < _hist_max / 2) {
                _sum = 0;
                _hist_HalfWidth = 0;
            }

            if (get_peak == 1 && hist_input[i] < _hist_max / 2) {
                break;
            }
        }

        // Quarter Width
        int _sum_q = 0;
        get_peak = 0;
        for (i = kernel_half; i < hist_size - kernel_half; ++i) {
            if (hist_input[i] == _hist_max) {
                get_peak = 1;
            }

            if (hist_input[i] > _hist_max / 4) {
                _sum_q += i;
                _hist_HlafDozenWidth++;
            }

            if (_sum_q > 0 && get_peak == 0 && hist_input[i] < _hist_max / 4) {
                _sum_q = 0;
                _hist_HlafDozenWidth = 0;
            }

            if (get_peak == 1 && hist_input[i] < _hist_max / 4) {
                break;
            }
        }

        // get no reset
        for (i = kernel_half; i < hist_size - kernel_half; ++i) {
            if (hist_input[i] > _hist_max / 2) {
                _hist_HalfWidth_noreset++;
            }

            if (hist_input[i] > _hist_max / 4) {
                _hist_HlafDozenWidth_noreset++;
            }
        }

        if (_hist_HalfWidth == 0) {
            histogramInfo->hist_HalfWidth_MedianPosition = 0;
        } else {
            histogramInfo->hist_HalfWidth_MedianPosition = _sum / _hist_HalfWidth;
        }
        histogramInfo->hist_HalfWidth = _hist_HalfWidth;
        histogramInfo->hist_HalfWidth_noreset = _hist_HalfWidth_noreset;

        if (_hist_HalfWidth == 0) {
            histogramInfo->hist_HlafDozenWidth_MedianPosition = 0;
        } else {
            histogramInfo->hist_HlafDozenWidth_MedianPosition = _sum_q / _hist_HlafDozenWidth;
        }
        histogramInfo->hist_HlafDozenWidth = _hist_HlafDozenWidth;
        histogramInfo->hist_HlafDozenWidth_noreset = _hist_HlafDozenWidth_noreset;

        histogramInfo->hist_max = _hist_max;
        histogramInfo->hist_max_position = _hist_max_position;
    }

#ifdef EGIS_DEBUG
    egislog_d("Egis_debug partial 522");
#endif
    return;
}

static void get_small_area_result_avg(int* _pImg, int* _p8bit, int _width, int _height,
                                      int is_boundary, int _light_thres,
                                      int _sun_factor_total_thres, enum fp_type _fptype,
                                      enum model_type _modeltype, enum lens_type _lenstype,
                                      struct SplitResult* _splitResult) {
    int i, j, k;

    int peak_valley_intensity_thres = 5;
    int peak_peak_dist_thres = 25;
    switch (_modeltype) {
        case MODEL_UNKNOW:
            break;
        case MODEL_A50:
            break;
        case MODEL_A70:
            break;
        case MODEL_A80:
            break;
        case MODEL_TAB_S6:
            break;
        case MODEL_A90:
            break;
        case MODEL_A30S:
            break;
        case MODEL_A50S:
            break;
        case MODEL_A70S:
            break;
        case MODEL_A51:
            break;
        case MODEL_A71:
            break;
        case MODEL_A91:
            break;
        case MODEL_A_NOTE:
            break;
        case MODEL_702:
            break;
        case MODEL_LG:
            break;
        case MODEL_HW:
            break;
        case MODEL_END:
            break;
        default:
            break;
    }

#ifdef EGIS_DEBUG
    egislog_d("Egis_debug partial 419");
#endif
#ifdef DEBUG
    FILE* file;
#endif

    // get mean and sun factor
    int sum = 0;
    _splitResult->sun_factor = 0;
    for (i = 0; i < _height; i++) {
        for (j = 0; j < _width; j++) {
            sum += _pImg[i * _width + j];

            if (_pImg[i * _width + j] > _light_thres) {
                _splitResult->sun_factor = _splitResult->sun_factor + 1;
            }
        }
    }
    _splitResult->mean = (double)sum / (_height * _width);

#ifdef EGIS_DEBUG
    egislog_d("Egis_debug partial 420");
#endif

    if (_splitResult->sun_factor > _sun_factor_total_thres) {
        _splitResult->sharp_score = 0;
        _splitResult->contrast_score = 0;
        _splitResult->max_peaks_num = 0;
        _splitResult->max_profile_valley = 0;
        _splitResult->second_profile_valley = 0;
        _splitResult->second_peaks_num = 0;
        return;
    }

#ifdef EGIS_DEBUG
    egislog_d("Egis_debug partial 421");
#endif

    int band_width = BAND_WIDTH;

    int hist_size = (255 - 0) / band_width + 1;
    int* hist = malloc(hist_size * sizeof(int));
    memset(hist, 0, sizeof(int) * hist_size);

    // get histogram
    for (i = 0; i < _width * _height; i++) {
        int index = (_p8bit[i] - 0) / band_width;
        hist[index]++;
    }

    int* hist_index = malloc(hist_size * sizeof(int));
    for (i = 0; i < hist_size; i++) {
        hist_index[i] = 0 + i * band_width;
    }

#ifdef EGIS_DEBUG
    egislog_d("Egis_debug partial 422");
#endif

    // get * type profile
    int profile_size = _width;
    int* profile = malloc(profile_size * 4 * sizeof(int));
    memset(profile, 0, profile_size * 4 * sizeof(int));
    int* profile_index = malloc(profile_size * sizeof(int));

    for (i = 0; i < _height; i++) {
        for (j = 0; j < _width; j++) {
            if (i == _height / 2 || i == _height / 2 + 2 || i == _height / 2 + 4 ||
                i == _height / 2 + 6 || i == _height / 2 + 8 || i == _height / 2 + 10 ||
                i == _height / 2 - 2 || i == _height / 2 - 4 || i == _height / 2 - 6 ||
                i == _height / 2 - 8 || i == _height / 2 - 10) {
                profile[j] += _p8bit[i * _width + j];
            }

            if (j == _width / 2 || j == _width / 2 + 2 || j == _width / 2 + 4 ||
                j == _width / 2 + 6 || j == _width / 2 + 8 || j == _width / 2 + 10 ||
                j == _width / 2 - 2 || j == _width / 2 - 4 || j == _width / 2 - 6 ||
                j == _width / 2 - 8 || j == _width / 2 - 10) {
                profile[profile_size + i] += _p8bit[i * _width + j];
            }

            if (i == j || i == j + 2 || i == j + 4 || i == j + 6 || i == j + 8 || i == j + 10 ||
                i == j - 2 || i == j - 4 || i == j - 6 || i == j - 8 || i == j - 10) {
                k = (i + j) >> 1;
                profile[profile_size * 2 + k] += _p8bit[i * _width + j];
                if (k >= profile_size) {
                    continue;
                }
            }

            if (i == _width - j - 1 || i == _width - j - 1 + 2 || i == _width - j - 1 + 4 ||
                i == _width - j - 1 + 6 || i == _width - j - 1 + 8 || i == _width - j - 1 + 10 ||
                i == _width - j - 1 - 2 || i == _width - j - 1 - 4 || i == _width - j - 1 - 6 ||
                i == _width - j - 1 - 8 || i == _width - j - 1 - 10) {
                k = (i + _width - j - 1) >> 1;
                if (k >= profile_size) {
                    continue;
                }
                profile[profile_size * 3 + k] += _p8bit[i * _width + j];
            }
        }
    }

    // average
    for (i = 0; i < profile_size; i++) {
        profile[i] = profile[i] / 11;
        profile[profile_size + i] = profile[profile_size + i] / 11;

        if (i == 0 || i == profile_size - 1) {
        } else if (i == 1 || i == profile_size - 2) {
            profile[profile_size * 2 + i] = profile[profile_size * 2 + i] / 3;
            profile[profile_size * 3 + i] = profile[profile_size * 3 + i] / 3;
        } else if (i == 2 || i == profile_size - 3) {
            profile[profile_size * 2 + i] = profile[profile_size * 2 + i] / 5;
            profile[profile_size * 3 + i] = profile[profile_size * 3 + i] / 5;
        } else if (i == 3 || i == profile_size - 4) {
            profile[profile_size * 2 + i] = profile[profile_size * 2 + i] / 7;
            profile[profile_size * 3 + i] = profile[profile_size * 3 + i] / 7;
        } else if (i == 4 || i == profile_size - 5) {
            profile[profile_size * 2 + i] = profile[profile_size * 2 + i] / 9;
            profile[profile_size * 3 + i] = profile[profile_size * 3 + i] / 9;
        } else {
            profile[profile_size * 2 + i] = profile[profile_size * 2 + i] / 11;
            profile[profile_size * 3 + i] = profile[profile_size * 3 + i] / 11;
        }
    }

#ifdef DEBUG
    if (fopen_s(&file, "hist.csv", "a") == 0) {
        int need_comma = 0;
        for (i = 0; i < 4; i++) {
            need_comma = 0;
            for (j = 0; j < profile_size; j++) {
                if (need_comma)
                    fprintf(file, ", ");
                else
                    need_comma = 1;
                fprintf(file, "%i", profile[i * profile_size + j]);
            }
            fprintf(file, "\n");
        }

        fclose(file);
    }
#endif

#ifdef EGIS_DEBUG
    egislog_d("Egis_debug partial 423");
#endif

    for (i = 0; i < profile_size; i++) {
        profile_index[i] = i;
    }

    struct HistogramInfoPeak* pHistogramInfoPeak =
        (struct HistogramInfoPeak*)malloc(5 * 50 * sizeof(struct HistogramInfoPeak));
    int* pHistogramInfoPeak_index = (int*)malloc(5 * sizeof(int));
    struct HistogramInfoValley* pHistogramInfoValley =
        (struct HistogramInfoValley*)malloc(5 * 50 * sizeof(struct HistogramInfoValley));
    int* pHistogramInfoValley_index = (int*)malloc(5 * sizeof(int));
    struct HistogramInfo* pHistogramInfo =
        (struct HistogramInfo*)malloc(5 * sizeof(struct HistogramInfo));

    int kernel_size = 5;

#ifdef EGIS_DEBUG
    egislog_d("Egis_debug partial 424");
#endif

    // get contrast
    int contrast_thres = 50;
    int str = 0;
    int end = 0;
    for (i = 0; i < hist_size; ++i) {
        if (hist[i] > contrast_thres) {
            if (str == 0) {
                str = i;
            }
            end = i;
        }
    }
    _splitResult->contrast_score = end - str + 1;

    if (_splitResult->contrast_score >= 9) {
        peak_valley_intensity_thres = 10;
    } else if (_splitResult->contrast_score >= 3) {
        peak_valley_intensity_thres = 5;
    } else if (_splitResult->contrast_score >= 3) {
        peak_valley_intensity_thres = 5;
    } else if (_splitResult->contrast_score == 2 && is_boundary == 1) {
        peak_valley_intensity_thres = 3;
    } else {
        peak_valley_intensity_thres = 4;
    }

#ifdef EGIS_DEBUG
    egislog_d("Egis_debug partial 425");
#endif

    for (i = 1; i < 5; i++) {
        analyze_hist_simple(profile + profile_size * (i - 1),
                            // profile_index,
                            profile_size,
                            // kernel_size,
                            // 0.0,
                            // 0.0,
                            // MODEL_END,
                            // LENS_END,
                            peak_valley_intensity_thres, peak_peak_dist_thres,
                            pHistogramInfoPeak + 50 * i, pHistogramInfoPeak_index + i,
                            pHistogramInfoValley + 50 * i, pHistogramInfoValley_index + i,
                            pHistogramInfo + i);
    }

#ifdef EGIS_DEBUG
    egislog_d("Egis_debug partial 426");
#endif

    // max peaks
    _splitResult->max_peaks_num = 0;
    _splitResult->second_peaks_num = 0;
    int max_peaks_group = 0;
    int second_peaks_group = 0;
    for (i = 1; i < 5; i++) {
        if (_splitResult->max_peaks_num < pHistogramInfoPeak_index[i]) {
            _splitResult->second_peaks_num = _splitResult->max_peaks_num;
            second_peaks_group = max_peaks_group;
            _splitResult->max_peaks_num = pHistogramInfoPeak_index[i];
            max_peaks_group = i;
        } else if (_splitResult->second_peaks_num < pHistogramInfoPeak_index[i]) {
            _splitResult->second_peaks_num = pHistogramInfoPeak_index[i];
            second_peaks_group = i;
        }
    }

    // max valleys
    _splitResult->max_valleys_num = 0;
    _splitResult->second_valleys_num = 0;
    int max_valleys_group = 0;
    int second_valleys_group = 0;
    for (i = 1; i < 5; i++) {
        if (_splitResult->max_valleys_num < pHistogramInfoValley_index[i]) {
            _splitResult->second_valleys_num = _splitResult->max_valleys_num;
            second_valleys_group = max_valleys_group;
            _splitResult->max_valleys_num = pHistogramInfoValley_index[i];
            max_valleys_group = i;
        } else if (_splitResult->second_valleys_num < pHistogramInfoValley_index[i]) {
            _splitResult->second_valleys_num = pHistogramInfoValley_index[i];
            second_valleys_group = i;
        }
    }

#ifdef EGIS_DEBUG
    egislog_d("Egis_debug partial 429");
#endif

    // get lowest_group
    _splitResult->max_profile_valley = 0;
    int lowest_group = 0;
    int max_deep_mean = -1;
    for (i = 1; i < 5; i++) {
        int deep_mean = 0;
        int deep_count = 0;
        // get max valley in max peaks number group
        for (j = 0; j < pHistogramInfoValley_index[i]; j++) {
            if (j < pHistogramInfoPeak_index[i] - 1 && j < pHistogramInfoValley_index[i]) {
                int valley_deep = MAX(pHistogramInfoPeak[i * 50 + j].intensity,
                                      pHistogramInfoPeak[i * 50 + j + 1].intensity) -
                                  pHistogramInfoValley[i * 50 + j].intensity;

                if (_splitResult->max_profile_valley < valley_deep) {
                    _splitResult->max_profile_valley = valley_deep;
                }
                deep_mean += valley_deep;
                deep_count++;
            }
        }

        if (deep_count == 0) {
            deep_mean = 0;
        } else {
            deep_mean = deep_mean / deep_count;
        }

        if (deep_mean > max_deep_mean) {
            max_deep_mean = deep_mean;
            lowest_group = i;
        }
    }

    // check is_period
    _splitResult->is_period = 0;
    _splitResult->is_light = 0;
    if (pHistogramInfoPeak_index[lowest_group] >= 3 &&
        pHistogramInfoValley_index[lowest_group] >= 3) {
        ////check M type (is light)?
        // int sum = 0;
        // for (i = 0; i < pHistogramInfoPeak_index[lowest_group] - 1; i++)
        //{
        //	if (pHistogramInfoValley[lowest_group * 50 + i].index > pHistogramInfoPeak[lowest_group
        //* 50 + i].index) //peak first
        //	{
        //		if (pHistogramInfoValley_index[lowest_group] > i)
        //		{
        //			if (pHistogramInfoPeak[lowest_group * 50 + i].intensity -
        // pHistogramInfoValley[lowest_group * 50 + i].intensity > _splitResult->max_profile_valley
        // *  0.5)
        //			{
        //				sum++;
        //			}
        //		}
        //		else if (i > 1 && pHistogramInfoValley_index[lowest_group] > i - 1)
        //		{
        //			if (pHistogramInfoPeak[lowest_group * 50 + i].intensity -
        // pHistogramInfoValley[lowest_group * 50 + i - 1].intensity >
        //_splitResult->max_profile_valley * 0.5)
        //			{
        //				sum++;
        //			}
        //		}
        //	}
        //	else //valley first
        //	{
        //		if (pHistogramInfoValley_index[lowest_group] > i)
        //		{
        //			if (pHistogramInfoPeak[lowest_group * 50 + i].intensity -
        // pHistogramInfoValley[lowest_group * 50 + i].intensity > _splitResult->max_profile_valley
        // *  0.5)
        //			{
        //				sum++;
        //			}
        //		}
        //		else if (i > 0 && pHistogramInfoValley_index[lowest_group] > i + 1)
        //		{
        //			if (pHistogramInfoPeak[lowest_group * 50 + i].intensity -
        // pHistogramInfoValley[lowest_group * 50 + i + 1].intensity >
        //_splitResult->max_profile_valley * 0.5)
        //			{
        //				sum++;
        //			}
        //		}
        //	}

        //	if (sum >= 3)
        //	{
        //		_splitResult->is_light = 0;
        //		break;
        //	}

        //	if (pHistogramInfoValley_index[lowest_group] > i + 2 &&
        //		pHistogramInfoValley[lowest_group * 50 + i + 0].index <
        // pHistogramInfoPeak[lowest_group * 50 + i].index &&
        //		pHistogramInfoValley[lowest_group * 50 + i + 1].index >
        // pHistogramInfoPeak[lowest_group * 50 + i].index &&
        //		pHistogramInfoValley[lowest_group * 50 + i + 1].index <
        // pHistogramInfoPeak[lowest_group * 50 + i + 1].index &&
        //		pHistogramInfoValley[lowest_group * 50 + i + 2].index >
        // pHistogramInfoPeak[lowest_group * 50 + i + 1].index)
        //	{
        //		if (MAX(pHistogramInfoPeak[lowest_group * 50 + i].intensity -
        // pHistogramInfoValley[lowest_group * 50 + i + 1].intensity,
        //			pHistogramInfoPeak[lowest_group * 50 + i + 1].intensity -
        // pHistogramInfoValley[lowest_group * 50 + i + 1].intensity) 			<
        // MIN(pHistogramInfoPeak[lowest_group * 50 + i].intensity -
        // pHistogramInfoValley[lowest_group * 50 + i + 0].intensity,
        //			pHistogramInfoPeak[lowest_group * 50 + i + 1].intensity -
        // pHistogramInfoValley[lowest_group * 50 + i + 2].intensity) * 0.5)
        //		{
        //			_splitResult->is_light = 1;
        //		}
        //	}
        //	else if (i > 0 && pHistogramInfoValley_index[lowest_group] > i + 1 &&
        //		pHistogramInfoValley[lowest_group * 50 + i - 1].index <
        // pHistogramInfoPeak[lowest_group * 50 + i].index &&
        //		pHistogramInfoValley[lowest_group * 50 + i + 0].index >
        // pHistogramInfoPeak[lowest_group * 50 + i].index &&
        //		pHistogramInfoValley[lowest_group * 50 + i + 0].index <
        // pHistogramInfoPeak[lowest_group * 50 + i + 1].index &&
        //		pHistogramInfoValley[lowest_group * 50 + i + 1].index >
        // pHistogramInfoPeak[lowest_group * 50 + i + 1].index)
        //	{
        //		if (MAX(pHistogramInfoPeak[lowest_group * 50 + i].intensity -
        // pHistogramInfoValley[lowest_group * 50 + i + 0].intensity,
        //			pHistogramInfoPeak[lowest_group * 50 + i + 1].intensity -
        // pHistogramInfoValley[lowest_group * 50 + i + 0].intensity) 			<
        // MIN(pHistogramInfoPeak[lowest_group * 50 + i].intensity -
        // pHistogramInfoValley[lowest_group * 50 + i - 1].intensity,
        //				pHistogramInfoPeak[lowest_group * 50 + i + 1].intensity -
        // pHistogramInfoValley[lowest_group * 50 + i + 1].intensity) * 0.5)
        //		{
        //			_splitResult->is_light = 1;
        //		}
        //	}
        //}

        if (_splitResult->is_light == 0) {  // check period
            int diff = 0;
            int pDiff[50];
            int pDiff_size = 0;
            int diff_mean = 0;

            for (i = 0; i < pHistogramInfoPeak_index[lowest_group] - 1; i++) {
                if (pHistogramInfoPeak[lowest_group * 50 + i].index == 0 ||
                    pHistogramInfoPeak[lowest_group * 50 + i + 1].index == profile_size - 1) {
                    continue;
                }
                if (pDiff_size >= 50) {
                    break;
                }
                if (i + 1 >= pHistogramInfoPeak_index[lowest_group]) {
                    continue;
                }
                diff = pHistogramInfoPeak[lowest_group * 50 + i + 1].index -
                       pHistogramInfoPeak[lowest_group * 50 + i].index;
#ifdef EGIS_DEBUG
                egislog_d("Egis_debug partial 429 %i", diff);
#endif
                pDiff[pDiff_size] = diff;
                pDiff_size++;
                diff_mean += diff;
                //#ifdef DEBUG
                //			if (fopen_s(&file, "hist_log.txt", "a") == 0)
                //			{
                //				fprintf(file, "%i\n", diff);
                //				fclose(file);
                //			}
                //#endif
            }
            //#ifdef DEBUG
            //		if (fopen_s(&file, "hist_log.txt", "a") == 0)
            //		{
            //			fprintf(file, "\n", diff);
            //			fclose(file);
            //		}
            //#endif

            for (i = 0; i < pHistogramInfoValley_index[lowest_group] - 1; i++) {
                if (pHistogramInfoValley[lowest_group * 50 + i].index == 0 ||
                    pHistogramInfoValley[lowest_group * 50 + i + 1].index == profile_size - 1) {
                    continue;
                }
                if (pDiff_size >= 50) {
                    break;
                }
                if (i + 1 >= pHistogramInfoValley_index[lowest_group]) {
                    continue;
                }
                diff = pHistogramInfoValley[lowest_group * 50 + i + 1].index -
                       pHistogramInfoValley[lowest_group * 50 + i].index;
#ifdef EGIS_DEBUG
                egislog_d("Egis_debug partial 429 %i", diff);
#endif
                pDiff[pDiff_size] = diff;
                pDiff_size++;
                diff_mean += diff;
                //#ifdef DEBUG
                //			if (fopen_s(&file, "hist_log.txt", "a") == 0)
                //			{
                //				fprintf(file, "%i\n", diff);
                //				fclose(file);
                //			}
                //#endif
            }

            if (pDiff_size != 0) {
                diff_mean = diff_mean / pDiff_size;
                _splitResult->is_period = 1;
                for (i = 0; i < pDiff_size; i++) {
                    if (pDiff[i] < diff_mean - 4 || pDiff[i] > diff_mean + 4) {
                        _splitResult->is_period = 0;
                    }
                }
            }
        }
    }

#ifdef EGIS_DEBUG
    egislog_d("Egis_debug partial 430");
#endif

#ifdef DEBUG
    if (fopen_s(&file, "hist_log.txt", "a") == 0) {
        fprintf(file, "contrast_score %i peak_valley_intensity_thres %i is_period %i is_light %i\n",
                _splitResult->contrast_score, peak_valley_intensity_thres, _splitResult->is_period,
                _splitResult->is_light);
        fprintf(file,
                "pHistogramInfoPeak_index %i %i %i %i pHistogramInfoValley_index %i %i %i %i "
                "lowest_group %i max_profile_valley %i max_peaks_num %i group %i second_peaks_num "
                "%i group %i mean %f sun_factor %i\n",
                pHistogramInfoPeak_index[1], pHistogramInfoPeak_index[2],
                pHistogramInfoPeak_index[3], pHistogramInfoPeak_index[4],
                pHistogramInfoValley_index[1], pHistogramInfoValley_index[2],
                pHistogramInfoValley_index[3], pHistogramInfoValley_index[4], lowest_group,
                _splitResult->max_profile_valley, _splitResult->max_peaks_num, max_peaks_group,
                _splitResult->second_peaks_num, second_peaks_group, _splitResult->mean,
                _splitResult->sun_factor);

        // for (i = 0; i < pHistogramInfoPeak_index[max_peaks_group]; i++)
        //{
        //	fprintf(file, "%i ", pHistogramInfoPeak[max_peaks_group * 50 + i].index);
        //}
        // fprintf(file, "\n");
        // for (i = 0; i < pHistogramInfoPeak_index[second_peaks_group]; i++)
        //{
        //	fprintf(file, "%i ", pHistogramInfoPeak[second_peaks_group * 50 + i].index);
        //}
        // fprintf(file, "\n");

        fclose(file);
    }
#endif

    FREE(hist);
    FREE(hist_index);
    FREE(profile);
    FREE(profile_index);
    FREE(pHistogramInfoPeak);
    FREE(pHistogramInfoPeak_index);
    FREE(pHistogramInfoValley);
    FREE(pHistogramInfoValley_index);
    FREE(pHistogramInfo);

#ifdef EGIS_DEBUG
    egislog_d("Egis_debug partial 431");
#endif
}

static void get_cell_result(unsigned char* _p8bit, int _width, int _height, int _cx, int _cy,
                            int _radius_boundary, int _stride, int _cell_width, int row_cell_size,
                            int col_cell_size, int cell_xshift, int cell_yshift,
                            int inner_width_offset, int inner_height_offset,
                            struct SplitResult* splitResult, int total_area, int total_inner_area,
                            int max_contrast_score, int Q1_contrast_score, int Q2_contrast_score,
                            // int max_contrast_minus_count,
                            // int contrast_4_count,
                            // int sum,
                            float mean_thres, int _light_index,
                            // int sun_condition,
                            // float avg_max_profile_valley,
                            // int dry_constrast_count,
                            float mean_thres_0, float mean_thres_1, float mean_thres_2,
                            float mean_thres_3, float mean_thres_4, float mean_thres_5,
                            float mean_thres_boundary_max, float mean_thres_boundary_min,
                            // float valley_ratio_thres,
                            int sun_factor_total_thres,
                            // int max_profile_valley_thres,
                            int noisy_finger_max_contrast_score, int second_peaks_num_thres,
                            float LT_RB_a0, float LT_RB_b0, float LT_RB_a1, float LT_RB_b1,
                            struct PartialDetectorResult* _pPartialDetectorResult) {
#ifdef DEBUG
    FILE* file;
#endif

    int i, j, k, l;
    int is_LT_RB_corner = 0;    // is left-top or right-bottom corner, for contrast
    int is_shading_corner = 0;  // for mean thres
    int dist = 0;
    int crop_x = 0;
    int crop_y = 0;
    float partial_score = 0.0;
    float inner_partial_score = 0.0;
    float light_score = 0.0;
    float inner_light_score = 0.0;
    for (i = 0; i < row_cell_size; i++) {
        for (j = 0; j < col_cell_size; j++) {
            crop_x = cell_xshift + j * _stride + (_cell_width - _stride) / 2;
            crop_y = cell_yshift + i * _stride + (_cell_width - _stride) / 2;

            int top, bottom, left, right;
#ifdef DEBUG
            top = crop_y;
            bottom = crop_y + _cell_width - 1;
            left = crop_x;
            right = crop_x + _cell_width - 1;
#else
            if (i == 0) {
                top = 0;
            } else {
                top = crop_y;
            }

            if (i == row_cell_size - 1) {
                bottom = _height - 1;
            } else {
                bottom = crop_y + _cell_width - 1;
            }

            if (j == 0) {
                left = 0;
            } else {
                left = crop_x;
            }

            if (j == col_cell_size - 1) {
                right = _width - 1;
            } else {
                right = crop_x + _cell_width - 1;
            }
#endif  // DEBUG

            // check is_LT_RB_corner
            if ((crop_y + _cell_width * 0.5) < (crop_x + _cell_width * 0.5) * LT_RB_b0 + LT_RB_a0 ||
                (crop_y + _cell_width * 0.5) > (crop_x + _cell_width * 0.5) * LT_RB_b1 + LT_RB_a1) {
                is_LT_RB_corner = 1;
            } else {
                is_LT_RB_corner = 0;
            }

            dist = int_sqrt(
                (_cx - (crop_x + _cell_width * 0.5)) * (_cx - (crop_x + _cell_width * 0.5)) +
                (_cy - (crop_y + _cell_width * 0.5)) * (_cy - (crop_y + _cell_width * 0.5)));
            if (dist >= _radius_boundary - 25) {
                is_shading_corner = 1;
            } else {
                is_shading_corner = 0;
            }

            if (splitResult[i * col_cell_size + j].is_out_boundary == 1) {
#ifdef OUTPUT_MASK
                for (k = 0; k < _height; k++) {
                    for (l = 0; l < _width; l++) {
                        if (k >= top && k <= bottom && l >= left && l <= right) {
                            mask[k * _width + l] = 0;
                        }
                    }
                }
#endif  // OUTPUT_MASK
            } else if (splitResult[i * col_cell_size + j].sun_factor > sun_factor_total_thres) {
#ifdef DEBUG
                if (fopen_s(&file, "hist_log.txt", "a") == 0) {
                    fprintf(file, "%i %i sun_factor %i\n", i, j,
                            splitResult[i * col_cell_size + j].sun_factor);
                    fclose(file);
                }
#endif

#ifdef OUTPUT_MASK
                for (k = 0; k < _height; k++) {
                    for (l = 0; l < _width; l++) {
                        if (k >= top && k <= bottom && l >= left && l <= right) {
                            mask[k * _width + l] = 255;
                        }
                    }
                }
#endif  // OUTPUT_MASK
                light_score += (double)100 / total_area;

                if (j >= inner_width_offset && j < col_cell_size - inner_width_offset &&
                    i >= inner_height_offset && i < row_cell_size - inner_height_offset) {
                    inner_light_score += (double)100 / total_inner_area;
                }

            } else if (splitResult[i * col_cell_size + j].contrast_score <= 1) {
#ifdef DEBUG
                if (fopen_s(&file, "hist_log.txt", "a") == 0) {
                    fprintf(file, "%i %i contrast_score %i <= 1\n", i, j,
                            splitResult[i * col_cell_size + j].contrast_score);
                    fclose(file);
                }
#endif
            } else if (Q1_contrast_score >= 15) {
                if (is_LT_RB_corner == 0 && is_shading_corner == 0 &&
                    max_contrast_score - Q2_contrast_score > 6 &&
                    splitResult[i * col_cell_size + j].contrast_score < Q1_contrast_score - 6) {
#ifdef DEBUG
                    if (fopen_s(&file, "hist_log.txt", "a") == 0) {
                        fprintf(file, "%i %i contrast_score %i < %i\n", i, j,
                                splitResult[i * col_cell_size + j].contrast_score,
                                Q1_contrast_score - 6);
                        fclose(file);
                    }
#endif
                } else if (is_LT_RB_corner == 0 && is_shading_corner == 0 &&
                           Q2_contrast_score >= 15 &&
                           splitResult[i * col_cell_size + j].contrast_score <
                               Q1_contrast_score - 10) {
#ifdef DEBUG
                    if (fopen_s(&file, "hist_log.txt", "a") == 0) {
                        fprintf(file, "%i %i contrast_score %i < %i\n", i, j,
                                splitResult[i * col_cell_size + j].contrast_score,
                                Q1_contrast_score - 10);
                        fclose(file);
                    }
#endif
                } else if (is_LT_RB_corner == 0 && is_shading_corner == 0 &&
                           Q2_contrast_score >= 10 &&
                           splitResult[i * col_cell_size + j].contrast_score <
                               Q1_contrast_score - 8) {
#ifdef DEBUG
                    if (fopen_s(&file, "hist_log.txt", "a") == 0) {
                        fprintf(file, "%i %i contrast_score %i < %i\n", i, j,
                                splitResult[i * col_cell_size + j].contrast_score,
                                Q1_contrast_score - 8);
                        fclose(file);
                    }
#endif
                } else if (is_LT_RB_corner == 0 && is_shading_corner == 0 &&
                           Q2_contrast_score < 10 &&
                           splitResult[i * col_cell_size + j].contrast_score <
                               Q1_contrast_score - 6) {
#ifdef DEBUG
                    if (fopen_s(&file, "hist_log.txt", "a") == 0) {
                        fprintf(file, "%i %i contrast_score %i < %i\n", i, j,
                                splitResult[i * col_cell_size + j].contrast_score,
                                Q1_contrast_score - 6);
                        fclose(file);
                    }
#endif
                } else if (is_shading_corner == 0 &&
                           splitResult[i * col_cell_size + j].sun_factor <= 50 &&
                           (splitResult[i * col_cell_size + j].mean <
                                mean_thres + (_light_index - mean_thres) * mean_thres_0 ||
                            splitResult[i * col_cell_size + j].mean >
                                MIN(mean_thres + (_light_index - mean_thres) * mean_thres_1,
                                    2000))) {
#ifdef DEBUG
                    if (fopen_s(&file, "hist_log.txt", "a") == 0) {
                        fprintf(file,
                                "%i %i max_contrast_score %i mean %f < %f > %f sun_condition %i\n",
                                i, j, max_contrast_score, splitResult[i * col_cell_size + j].mean,
                                mean_thres + (_light_index - mean_thres) * mean_thres_0,
                                MIN(mean_thres + (_light_index - mean_thres) * mean_thres_1, 2000),
                                splitResult[i * col_cell_size + j].sun_factor);
                        fclose(file);
                    }
#endif
                } else if (is_shading_corner == 1 &&
                           splitResult[i * col_cell_size + j].sun_factor <= 50 &&
                           (splitResult[i * col_cell_size + j].mean <
                                mean_thres +
                                    (_light_index - mean_thres) * mean_thres_boundary_min ||
                            splitResult[i * col_cell_size + j].mean >
                                mean_thres +
                                    (_light_index - mean_thres) * mean_thres_boundary_max)) {
#ifdef DEBUG
                    if (fopen_s(&file, "hist_log.txt", "a") == 0) {
                        fprintf(file,
                                "%i %i max_contrast_score %i mean %f < %f > %f sun_condition %i\n",
                                i, j, max_contrast_score, splitResult[i * col_cell_size + j].mean,
                                mean_thres + (_light_index - mean_thres) * mean_thres_boundary_min,
                                mean_thres + (_light_index - mean_thres) * mean_thres_boundary_max,
                                splitResult[i * col_cell_size + j].sun_factor);
                        fclose(file);
                    }
#endif
                } else if (splitResult[i * col_cell_size + j].is_period == 1) {
#ifdef OUTPUT_MASK
                    for (k = 0; k < _height; k++) {
                        for (l = 0; l < _width; l++) {
                            if (k >= top && k <= bottom && l >= left && l <= right) {
                                mask[k * _width + l] = _p8bit[k * _width + l];
                            }
                        }
                    }
#endif  // OUTPUT_MASK
                    partial_score += (double)100 / total_area;

                    if (j >= inner_width_offset && j < col_cell_size - inner_width_offset &&
                        i >= inner_height_offset && i < row_cell_size - inner_height_offset) {
                        inner_partial_score += (double)100 / total_inner_area;
                    }
                } else if ((splitResult[i * col_cell_size + j].max_peaks_num >= 4 &&
                            splitResult[i * col_cell_size + j].second_peaks_num >= 4) ||
                           (splitResult[i * col_cell_size + j].max_valleys_num >= 4 &&
                            splitResult[i * col_cell_size + j].second_valleys_num >= 4) ||
                           (splitResult[i * col_cell_size + j].max_peaks_num >= 4 &&
                            splitResult[i * col_cell_size + j].max_valleys_num >= 4)) {
#ifdef OUTPUT_MASK
                    for (k = 0; k < _height; k++) {
                        for (l = 0; l < _width; l++) {
                            if (k >= top && k <= bottom && l >= left && l <= right) {
                                mask[k * _width + l] = _p8bit[k * _width + l];
                            }
                        }
                    }
#endif  // OUTPUT_MASK
                    partial_score += (double)100 / total_area;

                    if (j >= inner_width_offset && j < col_cell_size - inner_width_offset &&
                        i >= inner_height_offset && i < row_cell_size - inner_height_offset) {
                        inner_partial_score += (double)100 / total_inner_area;
                    }
                } else if (is_shading_corner == 0 &&
                           (((splitResult[i * col_cell_size + j].max_peaks_num >= 4 &&
                              splitResult[i * col_cell_size + j].second_peaks_num >= 3) ||
                             (splitResult[i * col_cell_size + j].max_valleys_num >= 4 &&
                              splitResult[i * col_cell_size + j].second_valleys_num >= 3)))) {
#ifdef OUTPUT_MASK
                    for (k = 0; k < _height; k++) {
                        for (l = 0; l < _width; l++) {
                            if (k >= top && k <= bottom && l >= left && l <= right) {
                                mask[k * _width + l] = _p8bit[k * _width + l];
                            }
                        }
                    }
#endif  // OUTPUT_MASK
                    partial_score += (double)100 / total_area;

                    if (j >= inner_width_offset && j < col_cell_size - inner_width_offset &&
                        i >= inner_height_offset && i < row_cell_size - inner_height_offset) {
                        inner_partial_score += (double)100 / total_inner_area;
                    }
                } else {
#ifdef DEBUG
                    if (fopen_s(&file, "hist_log.txt", "a") == 0) {
                        fprintf(file, "%i %i\n", i, j);
                        fclose(file);
                    }
#endif
                }
            } else if (max_contrast_score >= 15 && Q1_contrast_score >= 10)  // few finger print
            {
                if (is_LT_RB_corner == 0 && is_shading_corner == 0 &&
                    max_contrast_score - Q2_contrast_score > 6 && Q2_contrast_score < 9 &&
                    splitResult[i * col_cell_size + j].contrast_score < 8) {
#ifdef DEBUG
                    if (fopen_s(&file, "hist_log.txt", "a") == 0) {
                        fprintf(file, "%i %i contrast_score %i < %i\n", i, j,
                                splitResult[i * col_cell_size + j].contrast_score, 8);
                        fclose(file);
                    }
#endif
                } else if (is_LT_RB_corner == 0 && is_shading_corner == 0 &&
                           max_contrast_score - Q2_contrast_score <= 6 && Q2_contrast_score >= 9 &&
                           splitResult[i * col_cell_size + j].contrast_score < 8) {
#ifdef DEBUG
                    if (fopen_s(&file, "hist_log.txt", "a") == 0) {
                        fprintf(file, "%i %i contrast_score %i < %i\n", i, j,
                                splitResult[i * col_cell_size + j].contrast_score, 8);
                        fclose(file);
                    }
#endif
                } else if (is_LT_RB_corner == 0 && is_shading_corner == 0 &&
                           max_contrast_score - Q2_contrast_score > 6 && Q2_contrast_score >= 9 &&
                           splitResult[i * col_cell_size + j].contrast_score < 9) {
#ifdef DEBUG
                    if (fopen_s(&file, "hist_log.txt", "a") == 0) {
                        fprintf(file, "%i %i contrast_score %i < %i\n", i, j,
                                splitResult[i * col_cell_size + j].contrast_score, 9);
                        fclose(file);
                    }
#endif
                } else if (is_LT_RB_corner == 0 && is_shading_corner == 0 &&
                           Q2_contrast_score < 9 &&
                           splitResult[i * col_cell_size + j].contrast_score < 8) {
#ifdef DEBUG
                    if (fopen_s(&file, "hist_log.txt", "a") == 0) {
                        fprintf(file, "%i %i contrast_score %i < %i\n", i, j,
                                splitResult[i * col_cell_size + j].contrast_score, 8);
                        fclose(file);
                    }
#endif
                } else if (is_shading_corner == 0 &&
                           splitResult[i * col_cell_size + j].sun_factor <= 50 &&
                           (splitResult[i * col_cell_size + j].mean <
                                mean_thres + (_light_index - mean_thres) * mean_thres_2 ||
                            splitResult[i * col_cell_size + j].mean >
                                MIN(mean_thres + (_light_index - mean_thres) * mean_thres_3,
                                    2000))) {
#ifdef DEBUG
                    if (fopen_s(&file, "hist_log.txt", "a") == 0) {
                        fprintf(file,
                                "%i %i max_contrast_score %i mean %f < %f > %f sun_condition %i\n",
                                i, j, max_contrast_score, splitResult[i * col_cell_size + j].mean,
                                mean_thres + (_light_index - mean_thres) * mean_thres_2,
                                MIN(mean_thres + (_light_index - mean_thres) * mean_thres_3, 2000),
                                splitResult[i * col_cell_size + j].sun_factor);
                        fclose(file);
                    }
#endif
                } else if (is_shading_corner == 1 &&
                           splitResult[i * col_cell_size + j].sun_factor <= 50 &&
                           (splitResult[i * col_cell_size + j].mean <
                                mean_thres +
                                    (_light_index - mean_thres) * mean_thres_boundary_min ||
                            splitResult[i * col_cell_size + j].mean >
                                mean_thres +
                                    (_light_index - mean_thres) * mean_thres_boundary_max)) {
#ifdef DEBUG
                    if (fopen_s(&file, "hist_log.txt", "a") == 0) {
                        fprintf(file,
                                "%i %i max_contrast_score %i mean %f < %f > %f sun_condition %i\n",
                                i, j, max_contrast_score, splitResult[i * col_cell_size + j].mean,
                                mean_thres + (_light_index - mean_thres) * mean_thres_boundary_min,
                                mean_thres + (_light_index - mean_thres) * mean_thres_boundary_max,
                                splitResult[i * col_cell_size + j].sun_factor);
                        fclose(file);
                    }
#endif
                } else if ((splitResult[i * col_cell_size + j].max_peaks_num >= 4 &&
                            splitResult[i * col_cell_size + j].second_peaks_num >= 4) ||
                           (splitResult[i * col_cell_size + j].max_valleys_num >= 4 &&
                            splitResult[i * col_cell_size + j].second_valleys_num >= 4) ||
                           (splitResult[i * col_cell_size + j].max_peaks_num >= 4 &&
                            splitResult[i * col_cell_size + j].max_valleys_num >= 4)) {
#ifdef OUTPUT_MASK
                    for (k = 0; k < _height; k++) {
                        for (l = 0; l < _width; l++) {
                            if (k >= top && k <= bottom && l >= left && l <= right) {
                                mask[k * _width + l] = _p8bit[k * _width + l];
                            }
                        }
                    }
#endif  // OUTPUT_MASK
                    partial_score += (double)100 / total_area;

                    if (j >= inner_width_offset && j < col_cell_size - inner_width_offset &&
                        i >= inner_height_offset && i < row_cell_size - inner_height_offset) {
                        inner_partial_score += (double)100 / total_inner_area;
                    }
                } else if (is_shading_corner == 0 &&
                           (((splitResult[i * col_cell_size + j].max_peaks_num >= 4 &&
                              splitResult[i * col_cell_size + j].second_peaks_num >= 3) ||
                             (splitResult[i * col_cell_size + j].max_valleys_num >= 4 &&
                              splitResult[i * col_cell_size + j].second_valleys_num >= 3)))) {
#ifdef OUTPUT_MASK
                    for (k = 0; k < _height; k++) {
                        for (l = 0; l < _width; l++) {
                            if (k > crop_y && k < crop_y + _stride - 1 && l > crop_x &&
                                l < crop_x + _stride - 1) {
                                mask[k * _width + l] = _p8bit[k * _width + l];
                            }
                        }
                    }
#endif  // OUTPUT_MASK
                    partial_score += (double)100 / total_area;

                    if (j >= inner_width_offset && j < col_cell_size - inner_width_offset &&
                        i >= inner_height_offset && i < row_cell_size - inner_height_offset) {
                        inner_partial_score += (double)100 / total_inner_area;
                    }
                } else if (splitResult[i * col_cell_size + j].is_period == 1) {
#ifdef OUTPUT_MASK
                    for (k = 0; k < _height; k++) {
                        for (l = 0; l < _width; l++) {
                            if (k >= top && k <= bottom && l >= left && l <= right) {
                                mask[k * _width + l] = _p8bit[k * _width + l];
                            }
                        }
                    }
#endif  // OUTPUT_MASK
                    partial_score += (double)100 / total_area;

                    if (j >= inner_width_offset && j < col_cell_size - inner_width_offset &&
                        i >= inner_height_offset && i < row_cell_size - inner_height_offset) {
                        inner_partial_score += (double)100 / total_inner_area;
                    }
                } else {
#ifdef DEBUG
                    if (fopen_s(&file, "hist_log.txt", "a") == 0) {
                        fprintf(file, "%i %i\n", i, j);
                        fclose(file);
                    }
#endif
                }
            } else if (max_contrast_score < 15 && Q1_contrast_score >= 10) {
                if (is_LT_RB_corner == 0 && is_shading_corner == 0 &&
                    max_contrast_score - Q1_contrast_score > 5 &&
                    splitResult[i * col_cell_size + j].contrast_score < Q1_contrast_score) {
#ifdef DEBUG
                    if (fopen_s(&file, "hist_log.txt", "a") == 0) {
                        fprintf(file, "%i %i contrast_score %i < %i\n", i, j,
                                splitResult[i * col_cell_size + j].contrast_score,
                                Q1_contrast_score);
                        fclose(file);
                    }
#endif
                } else if (is_LT_RB_corner == 0 && is_shading_corner == 0 &&
                           Q2_contrast_score >= 11 &&
                           splitResult[i * col_cell_size + j].contrast_score <
                               MIN(max_contrast_score - 5, 10)) {
#ifdef DEBUG
                    if (fopen_s(&file, "hist_log.txt", "a") == 0) {
                        fprintf(file, "%i %i contrast_score %i < %i\n", i, j,
                                splitResult[i * col_cell_size + j].contrast_score,
                                MIN(max_contrast_score - 5, 10));
                        fclose(file);
                    }
#endif
                } else if (is_LT_RB_corner == 0 && is_shading_corner == 0 &&
                           Q2_contrast_score >= 10 &&
                           splitResult[i * col_cell_size + j].contrast_score <
                               MIN(max_contrast_score - 6, 7)) {
#ifdef DEBUG
                    if (fopen_s(&file, "hist_log.txt", "a") == 0) {
                        fprintf(file, "%i %i contrast_score %i < %i\n", i, j,
                                splitResult[i * col_cell_size + j].contrast_score,
                                MIN(max_contrast_score - 6, 7));
                        fclose(file);
                    }
#endif
                } else if (is_shading_corner == 0 &&
                           splitResult[i * col_cell_size + j].sun_factor <= 50 &&
                           (splitResult[i * col_cell_size + j].mean <
                                mean_thres + (_light_index - mean_thres) * mean_thres_2 ||
                            splitResult[i * col_cell_size + j].mean >
                                MIN(mean_thres + (_light_index - mean_thres) * mean_thres_3,
                                    2000))) {
#ifdef DEBUG
                    if (fopen_s(&file, "hist_log.txt", "a") == 0) {
                        fprintf(file,
                                "%i %i max_contrast_score %i mean %f < %f > %f sun_condition %i\n",
                                i, j, max_contrast_score, splitResult[i * col_cell_size + j].mean,
                                mean_thres * mean_thres_2,
                                MIN(mean_thres + (_light_index - mean_thres) * mean_thres_3, 2000),
                                splitResult[i * col_cell_size + j].sun_factor);
                        fclose(file);
                    }
#endif
                } else if (is_shading_corner == 1 &&
                           splitResult[i * col_cell_size + j].sun_factor <= 50 &&
                           (splitResult[i * col_cell_size + j].mean <
                                mean_thres +
                                    (_light_index - mean_thres) * mean_thres_boundary_min ||
                            splitResult[i * col_cell_size + j].mean >
                                mean_thres +
                                    (_light_index - mean_thres) * mean_thres_boundary_max)) {
#ifdef DEBUG
                    if (fopen_s(&file, "hist_log.txt", "a") == 0) {
                        fprintf(file,
                                "%i %i max_contrast_score %i mean %f < %f > %f sun_condition %i\n",
                                i, j, max_contrast_score, splitResult[i * col_cell_size + j].mean,
                                mean_thres * mean_thres_boundary_min,
                                mean_thres + (_light_index - mean_thres) * mean_thres_boundary_max,
                                splitResult[i * col_cell_size + j].sun_factor);
                        fclose(file);
                    }
#endif
                } else if ((splitResult[i * col_cell_size + j].max_peaks_num >= 4 &&
                            splitResult[i * col_cell_size + j].second_peaks_num >= 4) ||
                           (splitResult[i * col_cell_size + j].max_valleys_num >= 4 &&
                            splitResult[i * col_cell_size + j].second_valleys_num >= 4) ||
                           (splitResult[i * col_cell_size + j].max_peaks_num >= 4 &&
                            splitResult[i * col_cell_size + j].max_valleys_num >= 4)) {
#ifdef OUTPUT_MASK
                    for (k = 0; k < _height; k++) {
                        for (l = 0; l < _width; l++) {
                            if (k >= top && k <= bottom && l >= left && l <= right) {
                                mask[k * _width + l] = _p8bit[k * _width + l];
                            }
                        }
                    }
#endif  // OUTPUT_MASK
                    partial_score += (double)100 / total_area;

                    if (j >= inner_width_offset && j < col_cell_size - inner_width_offset &&
                        i >= inner_height_offset && i < row_cell_size - inner_height_offset) {
                        inner_partial_score += (double)100 / total_inner_area;
                    }
                } else if (is_shading_corner == 0 &&
                           ((splitResult[i * col_cell_size + j].max_peaks_num >= 4 &&
                             splitResult[i * col_cell_size + j].second_peaks_num >= 3) ||
                            (splitResult[i * col_cell_size + j].max_valleys_num >= 4 &&
                             splitResult[i * col_cell_size + j].second_valleys_num >= 3))) {
#ifdef OUTPUT_MASK
                    for (k = 0; k < _height; k++) {
                        for (l = 0; l < _width; l++) {
                            if (k > crop_y && k < crop_y + _stride - 1 && l > crop_x &&
                                l < crop_x + _stride - 1) {
                                mask[k * _width + l] = _p8bit[k * _width + l];
                            }
                        }
                    }
#endif  // OUTPUT_MASK
                    partial_score += (double)100 / total_area;

                    if (j >= inner_width_offset && j < col_cell_size - inner_width_offset &&
                        i >= inner_height_offset && i < row_cell_size - inner_height_offset) {
                        inner_partial_score += (double)100 / total_inner_area;
                    }
                } else if (splitResult[i * col_cell_size + j].is_period == 1) {
#ifdef OUTPUT_MASK
                    for (k = 0; k < _height; k++) {
                        for (l = 0; l < _width; l++) {
                            if (k >= top && k <= bottom && l >= left && l <= right) {
                                mask[k * _width + l] = _p8bit[k * _width + l];
                            }
                        }
                    }
#endif  // OUTPUT_MASK
                    partial_score += (double)100 / total_area;

                    if (j >= inner_width_offset && j < col_cell_size - inner_width_offset &&
                        i >= inner_height_offset && i < row_cell_size - inner_height_offset) {
                        inner_partial_score += (double)100 / total_inner_area;
                    }
                } else {
#ifdef DEBUG
                    if (fopen_s(&file, "hist_log.txt", "a") == 0) {
                        fprintf(file, "%i %i\n", i, j);
                        fclose(file);
                    }
#endif
                }
            } else  // noisy finger
            {
                if (is_LT_RB_corner == 0 && is_shading_corner == 0 &&
                    splitResult[i * col_cell_size + j].contrast_score <
                        MIN(Q1_contrast_score - 4, noisy_finger_max_contrast_score)) {
#ifdef DEBUG
                    if (fopen_s(&file, "hist_log.txt", "a") == 0) {
                        fprintf(file, "%i %i contrast_score %i < %i\n", i, j,
                                splitResult[i * col_cell_size + j].contrast_score,
                                MIN(Q1_contrast_score, noisy_finger_max_contrast_score));
                        fclose(file);
                    }
#endif
                } else if (is_shading_corner == 0 &&
                           (splitResult[i * col_cell_size + j].mean <
                                mean_thres + (_light_index - mean_thres) * mean_thres_4 ||
                            (splitResult[i * col_cell_size + j].mean >
                             MIN(mean_thres + (_light_index - mean_thres) * mean_thres_5, 2000)))) {
#ifdef DEBUG
                    if (fopen_s(&file, "hist_log.txt", "a") == 0) {
                        fprintf(file,
                                "max_contrast_score >= 5: %i %i max_contrast_score %i mean %f < %f "
                                "or > %f\n",
                                i, j, max_contrast_score, splitResult[i * col_cell_size + j].mean,
                                mean_thres + (_light_index - mean_thres) * mean_thres_4,
                                MIN(mean_thres + (_light_index - mean_thres) * mean_thres_5, 2000));
                        fclose(file);
                    }
#endif
                } else if (is_shading_corner == 1 &&
                           splitResult[i * col_cell_size + j].sun_factor <= 50 &&
                           (splitResult[i * col_cell_size + j].mean <
                                mean_thres +
                                    (_light_index - mean_thres) * mean_thres_boundary_min ||
                            splitResult[i * col_cell_size + j].mean >
                                mean_thres +
                                    (_light_index - mean_thres) * mean_thres_boundary_max)) {
#ifdef DEBUG
                    if (fopen_s(&file, "hist_log.txt", "a") == 0) {
                        fprintf(file,
                                "%i %i max_contrast_score %i mean %f < %f > %f sun_condition %i\n",
                                i, j, max_contrast_score, splitResult[i * col_cell_size + j].mean,
                                mean_thres + (_light_index - mean_thres) * mean_thres_boundary_min,
                                mean_thres + (_light_index - mean_thres) * mean_thres_boundary_max,
                                splitResult[i * col_cell_size + j].sun_factor);
                        fclose(file);
                    }
#endif
                } else if ((splitResult[i * col_cell_size + j].max_peaks_num >= 4 &&
                            splitResult[i * col_cell_size + j].second_peaks_num >= 4) ||
                           (splitResult[i * col_cell_size + j].max_valleys_num >= 4 &&
                            splitResult[i * col_cell_size + j].second_valleys_num >= 4) ||
                           (splitResult[i * col_cell_size + j].max_peaks_num >= 4 &&
                            splitResult[i * col_cell_size + j].max_valleys_num >= 4)) {
#ifdef OUTPUT_MASK
                    for (k = 0; k < _height; k++) {
                        for (l = 0; l < _width; l++) {
                            if (k >= top && k <= bottom && l >= left && l <= right) {
                                mask[k * _width + l] = _p8bit[k * _width + l];
                            }
                        }
                    }
#endif  // OUTPUT_MASK
                    partial_score += (double)100 / total_area;

                    if (j >= inner_width_offset && j < col_cell_size - inner_width_offset &&
                        i >= inner_height_offset && i < row_cell_size - inner_height_offset) {
                        inner_partial_score += (double)100 / total_inner_area;
                    }
                } else if (is_shading_corner == 0 &&
                           ((splitResult[i * col_cell_size + j].max_peaks_num >= 4 &&
                             splitResult[i * col_cell_size + j].second_peaks_num >= 3) ||
                            (splitResult[i * col_cell_size + j].max_valleys_num >= 4 &&
                             splitResult[i * col_cell_size + j].second_valleys_num >= 3))) {
#ifdef OUTPUT_MASK
                    for (k = 0; k < _height; k++) {
                        for (l = 0; l < _width; l++) {
                            if (k > crop_y && k < crop_y + _stride - 1 && l > crop_x &&
                                l < crop_x + _stride - 1) {
                                mask[k * _width + l] = _p8bit[k * _width + l];
                            }
                        }
                    }
#endif  // OUTPUT_MASK
                    partial_score += (double)100 / total_area;

                    if (j >= inner_width_offset && j < col_cell_size - inner_width_offset &&
                        i >= inner_height_offset && i < row_cell_size - inner_height_offset) {
                        inner_partial_score += (double)100 / total_inner_area;
                    }
                } else if (splitResult[i * col_cell_size + j].is_period == 1) {
#ifdef OUTPUT_MASK
                    for (k = 0; k < _height; k++) {
                        for (l = 0; l < _width; l++) {
                            if (k >= top && k <= bottom && l >= left && l <= right) {
                                mask[k * _width + l] = _p8bit[k * _width + l];
                            }
                        }
                    }
#endif  // OUTPUT_MASK
                    partial_score += (double)100 / total_area;

                    if (j >= inner_width_offset && j < col_cell_size - inner_width_offset &&
                        i >= inner_height_offset && i < row_cell_size - inner_height_offset) {
                        inner_partial_score += (double)100 / total_inner_area;
                    }
                } else {
#ifdef DEBUG
                    if (fopen_s(&file, "hist_log.txt", "a") == 0) {
                        fprintf(file, "%i %i\n", i, j);
                        fclose(file);
                    }
#endif
                }
            }

            //			if (is_LT_RB_corner == 1)
            //			{
            //#ifdef OUTPUT_MASK
            //				for (k = 0; k < _height; k++)
            //				{
            //					for (l = 0; l < _width; l++)
            //					{
            //						if (k > crop_y&& k < crop_y + _stride - 1 && l > crop_x&& l <
            // crop_x
            //+ _stride - 1)
            //						{
            //							mask[k * _width + l] = 255;
            //						}
            //					}
            //				}
            //#endif // OUTPUT_MASK
            //			}
        }
    }

    _pPartialDetectorResult->partial_ratio = partial_score;
    _pPartialDetectorResult->inner_partial_ratio = inner_partial_score;
    _pPartialDetectorResult->room_light_partial_ratio = light_score;
    _pPartialDetectorResult->inner_room_light_partial_ratio = inner_light_score;
}

static void split_cells_a91(int* _pImg, unsigned char* _p8bit, int _width, int _height, int _cx,
                            int _cy, int _radius_boundary, enum fp_type _fptype,
                            enum model_type _modeltype, enum lens_type _lenstype, int _cell_width,
                            int _stride, int _light_thres,
                            struct PartialDetectorResult* _pPartialDetectorResult) {
    int dark_thres = 100;
    switch (_modeltype) {
        case MODEL_702:
            dark_thres = 7000;
            break;
        default:
            break;
    }

#ifdef EGIS_DEBUG
    egislog_d("Egis_debug partial 301 %i", dark_thres);
#endif

    int i, j, k, l;
    // int _cell_width = 20;
    // int _stride = 20;
    int col_cell_size = (_width - _cell_width) / _stride + 1;
    int row_cell_size = (_height - _cell_width) / _stride + 1;
    int cell_xshift = (_width - _stride * (col_cell_size - 1) - _cell_width) / 2;
    int cell_yshift = (_height - _stride * (row_cell_size - 1) - _cell_width) / 2;

    struct SplitResult* splitResult =
        (struct SplitResult*)malloc(col_cell_size * row_cell_size * sizeof(struct SplitResult));

    int crop_x = 0;
    int crop_y = 0;
    int* img_crop = (int*)malloc(_cell_width * _cell_width * sizeof(int));
    int* bit8_crop = (int*)malloc(_cell_width * _cell_width * sizeof(int));

#ifdef OUTPUT_MASK
    mask = (unsigned char*)malloc(_width * _height * sizeof(unsigned char));
    for (i = 0; i < _height * _width; i++) {
        mask[i] = _p8bit[i] * 0.5;
    }
#endif  // DEBUG

#ifdef EGIS_DEBUG
    egislog_d("Egis_debug partial 302");
#endif

#ifdef DEBUG
    FILE* file;
#endif

    // get mean from max contrast_score
    int max_contrast_score = 0;
    int max_contrast_minus_count = 0;
    int contrast_4_count = 0;
    int sum = 0;
    float mean_thres = 0;
    int sun_condition = 0;
    float avg_max_profile_valley = 0;
    int dry_constrast_count = 0;
    float mean_thres_0 = 0.85f;
    float mean_thres_1 = 1.15f;
    float mean_thres_2 = 0.5f;
    float mean_thres_3 = 1.3f;
    float mean_thres_4 = 0.7f;
    float mean_thres_5 = 1.3f;
    float mean_thres_boundary_min = 0.4f;
    float mean_thres_boundary_max = 2.5f;
    float valley_ratio_thres = 0.4f;
    int sun_factor_total_thres = 100;
    int max_profile_valley_thres = 40;
    int noisy_finger_max_contrast_score = 5;
    int second_peaks_num_thres = 4;
    int corner_thres_LT = 110;
    int corner_thres_RB = 310;

    switch (_modeltype) {
        case MODEL_UNKNOW:
            break;
        case MODEL_A50:
            break;
        case MODEL_A70:
            break;
        case MODEL_A80:
            break;
        case MODEL_TAB_S6:
            break;
        case MODEL_A90:
            break;
        case MODEL_A30S:
            break;
        case MODEL_A50S:
            break;
        case MODEL_A70S:
            break;
        case MODEL_A51:
            break;
        case MODEL_A71:
            break;
        case MODEL_A91:
            max_profile_valley_thres = 7;
            //_light_thres = 4000;
            sun_factor_total_thres = 100;
            mean_thres_0 = 0.5f;
            mean_thres_1 = 100;
            mean_thres_2 = 0.5f;
            mean_thres_3 = 100;
            mean_thres_4 = 0.5f;
            mean_thres_5 = 100;
            mean_thres_boundary_min = 0.4f;
            mean_thres_boundary_max = 100;
            noisy_finger_max_contrast_score = 6;
            second_peaks_num_thres = 3;
            // max_contrast_score = Q1_contrast_score;
            // max_contrast_minus_count = Q1_contrast_count;
            break;
        case MODEL_A_NOTE:
            break;
        case MODEL_702:
            break;
        case MODEL_LG:
            break;
        case MODEL_HW:
            break;
        case MODEL_END:
            break;
        default:
            break;
    }

    int is_boundary = 0;
    int dist = 0;
    int total_area = 0;
    int total_inner_area = 0;
    int inner_width_offset = col_cell_size / 4;
    int inner_height_offset = row_cell_size / 4;
    for (i = 0; i < row_cell_size; i++) {
        for (j = 0; j < col_cell_size; j++) {
            crop_x = cell_xshift + j * _stride;
            crop_y = cell_yshift + i * _stride;

            for (k = 0; k < _height; k++) {
                for (l = 0; l < _width; l++) {
                    if (k >= crop_y && k < crop_y + _cell_width && l >= crop_x &&
                        l < crop_x + _cell_width) {
                        img_crop[(k - crop_y) * _cell_width + (l - crop_x)] = _pImg[k * _width + l];
                        bit8_crop[(k - crop_y) * _cell_width + (l - crop_x)] =
                            _p8bit[k * _width + l];
                    }
                }
            }

#ifdef DEBUG
            if (fopen_s(&file, "hist_log.txt", "a") == 0) {
                fprintf(file, "i %i j %i\n", i, j);
                fclose(file);
            }

            if (fopen_s(&file, "hist.csv", "a") == 0) {
                fprintf(file, "i %i j %i\n", i, j);
                fclose(file);
            }

            if (i == 7 && j == 4) {
                printf("");
            }
#endif

            if (i == 0 || j == 0 || i == row_cell_size - 1 || j == col_cell_size - 1) {
                is_boundary = 1;
            } else {
                is_boundary = 0;
            }

            splitResult[i * col_cell_size + j].sharp_score = 0;
            splitResult[i * col_cell_size + j].contrast_score = 0;
            splitResult[i * col_cell_size + j].max_peaks_num = 0;
            splitResult[i * col_cell_size + j].max_valleys_num = 0;
            splitResult[i * col_cell_size + j].max_profile_valley = 0;
            splitResult[i * col_cell_size + j].second_peaks_num = 0;
            splitResult[i * col_cell_size + j].second_valleys_num = 0;
            splitResult[i * col_cell_size + j].second_profile_valley = 0;
            splitResult[i * col_cell_size + j].weak_max_peaks_num = 0;
            splitResult[i * col_cell_size + j].weak_second_peaks_num = 0;
            splitResult[i * col_cell_size + j].mean = 0;
            splitResult[i * col_cell_size + j].sun_factor = 0;
            splitResult[i * col_cell_size + j].is_period = 0;
            splitResult[i * col_cell_size + j].is_light = 0;
            splitResult[i * col_cell_size + j].is_out_boundary = 0;

            dist = int_sqrt(
                (_cx - (crop_x + _cell_width * 0.5)) * (_cx - (crop_x + _cell_width * 0.5)) +
                (_cy - (crop_y + _cell_width * 0.5)) * (_cy - (crop_y + _cell_width * 0.5)));
            if (dist > _radius_boundary) {
                splitResult[i * col_cell_size + j].is_out_boundary = 1;
            } else {
                get_small_area_result_avg(img_crop, bit8_crop, _cell_width, _cell_width,
                                          is_boundary, _light_thres, sun_factor_total_thres,
                                          _fptype, _modeltype, _lenstype,
                                          &splitResult[i * col_cell_size + j]);

                splitResult[i * col_cell_size + j].is_out_boundary = 0;
                total_area++;
                if (j >= inner_width_offset && j < col_cell_size - inner_width_offset &&
                    i >= inner_height_offset && i < row_cell_size - inner_height_offset) {
                    total_inner_area++;
                }
            }
        }
    }
    FREE(img_crop);
    FREE(bit8_crop);

#ifdef EGIS_DEBUG
    egislog_d("Egis_debug partial 303");
#endif

    float partial_score = 0.0;
    float inner_partial_score = 0.0;
    float light_score = 0.0;
    float inner_light_score = 0.0;

    for (i = 0; i < row_cell_size; i++) {
        for (j = 0; j < col_cell_size; j++) {
            if (max_contrast_score < splitResult[i * col_cell_size + j].contrast_score) {
                max_contrast_score = splitResult[i * col_cell_size + j].contrast_score;
            }

            avg_max_profile_valley += splitResult[i * col_cell_size + j].max_profile_valley;

            sun_condition += splitResult[i * col_cell_size + j].sun_factor;
        }
    }

    avg_max_profile_valley = (double)avg_max_profile_valley / (row_cell_size * col_cell_size);

#ifdef EGIS_DEBUG
    egislog_d("Egis_debug partial 304");
#endif

    for (i = 0; i < row_cell_size; i++) {
        for (j = 0; j < col_cell_size; j++) {
            if (splitResult[i * col_cell_size + j].contrast_score >= max_contrast_score - 1) {
                sum += splitResult[i * col_cell_size + j].mean;
                max_contrast_minus_count++;
            }
            if (splitResult[i * col_cell_size + j].contrast_score >= 4) {
                contrast_4_count++;
            }
            if (splitResult[i * col_cell_size + j].contrast_score >=
                noisy_finger_max_contrast_score) {
                dry_constrast_count++;
            }
        }
    }
    mean_thres = (double)sum / max_contrast_minus_count;

#ifdef EGIS_DEBUG
    egislog_d("Egis_debug partial 305");
#endif

    // sort contrast
    int* sort_contrast = (int*)malloc(row_cell_size * col_cell_size * sizeof(int));

    // copy
    for (i = 0; i < row_cell_size; i++) {
        for (j = 0; j < col_cell_size; j++) {
            sort_contrast[i * col_cell_size + j] =
                splitResult[i * col_cell_size + j].contrast_score;
        }
    }

    // sort
    int temp = 0;
    for (i = 0; i < row_cell_size * col_cell_size - 1; i++) {
        for (j = i + 1; j < row_cell_size * col_cell_size; j++) {
            if (sort_contrast[j] > sort_contrast[i]) {
                temp = sort_contrast[i];
                sort_contrast[i] = sort_contrast[j];
                sort_contrast[j] = temp;
            }
        }
    }

    int q1 = (int)row_cell_size * col_cell_size * 0.25;
    int q2 = (int)row_cell_size * col_cell_size * 0.5;
    int Q1_contrast_score = sort_contrast[q1];
    int Q1_contrast_count = q1;
    for (i = q1 + 1; i < row_cell_size * col_cell_size; i++) {
        if (sort_contrast[i] == Q1_contrast_score) {
            Q1_contrast_count++;
        } else {
            break;
        }
    }
    int Q2_contrast_score = sort_contrast[q2];
    int Q2_contrast_count = q2;
    for (i = q2 + 1; i < row_cell_size * col_cell_size; i++) {
        if (sort_contrast[i] == Q2_contrast_score) {
            Q2_contrast_score++;
        } else {
            break;
        }
    }
    FREE(sort_contrast);

#ifdef DEBUG
    if (fopen_s(&file, "hist_log.txt", "a") == 0) {
        fprintf(file, "max_contrast_score %i\n", max_contrast_score);
        fprintf(file, "max_contrast_minus_count %i\n", max_contrast_minus_count);
        fprintf(file, "mean_thres %f\n", mean_thres);
        fprintf(file, "sun_condition %i\n", sun_condition);
        fprintf(file, "avg_max_profile_valley %f\n", avg_max_profile_valley);
        fprintf(file, "Q1_contrast_score %i Q1_contrast_count %i\n", Q1_contrast_score,
                Q1_contrast_count);
        fprintf(file, "Q2_contrast_score %i Q2_contrast_count %i\n", Q2_contrast_score,
                Q2_contrast_count);
        fclose(file);
    }
#endif

#ifdef EGIS_DEBUG
    egislog_d("Egis_debug partial 306");
#endif

    int is_LT_RB_corner = 0;  // is left-top or right-bottom corner
    for (i = 0; i < row_cell_size; i++) {
        for (j = 0; j < col_cell_size; j++) {
            crop_x = cell_xshift + j * _stride + (_cell_width - _stride) / 2;
            crop_y = cell_yshift + i * _stride + (_cell_width - _stride) / 2;
            // if (splitResult[i * col_cell_size + j].contrast_score >= MIN(min, 4))
            // float valley_ratio = (double)splitResult[i * col_cell_size + j].second_profile_valley
            // / splitResult[i * col_cell_size + j].max_profile_valley;

            // check is_LT_RB_corner
            if (crop_x + _cell_width * 0.5 + crop_y + _cell_width * 0.5 <=
                    corner_thres_LT + cell_xshift + cell_yshift ||
                crop_x + _cell_width * 0.5 + crop_y + _cell_width * 0.5 > corner_thres_RB) {
                is_LT_RB_corner = 1;
            } else {
                is_LT_RB_corner = 0;
            }

            if (splitResult[i * col_cell_size + j].is_out_boundary == 1) {
#ifdef OUTPUT_MASK
                for (k = 0; k < _height; k++) {
                    for (l = 0; l < _width; l++) {
                        if (k > crop_y && k < crop_y + _stride - 1 && l > crop_x &&
                            l < crop_x + _stride - 1) {
                            mask[k * _width + l] = 0;
                        }
                    }
                }
#endif  // OUTPUT_MASK
            } else if (
                splitResult[i * col_cell_size + j].sun_factor >
                sun_factor_total_thres /* || splitResult[i * col_cell_size + j].is_light == 1*/) {
#ifdef DEBUG
                if (fopen_s(&file, "hist_log.txt", "a") == 0) {
                    fprintf(file, "%i %i sun_factor %i\n", i, j,
                            splitResult[i * col_cell_size + j].sun_factor);
                    fclose(file);
                }
#endif

#ifdef OUTPUT_MASK
                for (k = 0; k < _height; k++) {
                    for (l = 0; l < _width; l++) {
                        if (k > crop_y && k < crop_y + _stride - 1 && l > crop_x &&
                            l < crop_x + _stride - 1) {
                            mask[k * _width + l] = 255;
                        }
                    }
                }
#endif  // OUTPUT_MASK
                light_score += (double)100 / total_area;

                if (j >= inner_width_offset && j < col_cell_size - inner_width_offset &&
                    i >= inner_height_offset && i < row_cell_size - inner_height_offset) {
                    inner_light_score += (double)100 / total_inner_area;
                }

            } else if (splitResult[i * col_cell_size + j].contrast_score <= 1) {
#ifdef DEBUG
                FILE* file;
                if (fopen_s(&file, "hist_log.txt", "a") == 0) {
                    fprintf(file, "%i %i contrast_score %i <= 1\n", i, j,
                            splitResult[i * col_cell_size + j].contrast_score);
                    fclose(file);
                }
#endif
            } else if (Q1_contrast_score >= 15) {
                /*if (is_LT_RB_corner == 1 &&
                    max_contrast_score - Q2_contrast_score > 6 &&
                    splitResult[i * col_cell_size + j].contrast_score < Q1_contrast_score - 8)
                {
#ifdef DEBUG
                    FILE* file;
                    if (fopen_s(&file, "hist_log.txt", "a") == 0)
                    {
                        fprintf(file, "%i %i contrast_score %i < %i\n", i, j, splitResult[i *
col_cell_size + j].contrast_score, Q1_contrast_score - 5); fclose(file);
                    }
#endif
                }
                else */
                if (is_LT_RB_corner == 0 && max_contrast_score - Q2_contrast_score > 6 &&
                    splitResult[i * col_cell_size + j].contrast_score < Q1_contrast_score - 6) {
#ifdef DEBUG
                    FILE* file;
                    if (fopen_s(&file, "hist_log.txt", "a") == 0) {
                        fprintf(file, "%i %i contrast_score %i < %i\n", i, j,
                                splitResult[i * col_cell_size + j].contrast_score,
                                Q1_contrast_score - 6);
                        fclose(file);
                    }
#endif
                } else if (is_LT_RB_corner == 0 && Q2_contrast_score >= 15 &&
                           splitResult[i * col_cell_size + j].contrast_score <
                               Q1_contrast_score - 10) {
#ifdef DEBUG
                    FILE* file;
                    if (fopen_s(&file, "hist_log.txt", "a") == 0) {
                        fprintf(file, "%i %i contrast_score %i < %i\n", i, j,
                                splitResult[i * col_cell_size + j].contrast_score,
                                Q1_contrast_score - 10);
                        fclose(file);
                    }
#endif
                } else if (is_LT_RB_corner == 0 && Q2_contrast_score >= 10 &&
                           splitResult[i * col_cell_size + j].contrast_score <
                               Q1_contrast_score - 8) {
#ifdef DEBUG
                    FILE* file;
                    if (fopen_s(&file, "hist_log.txt", "a") == 0) {
                        fprintf(file, "%i %i contrast_score %i < %i\n", i, j,
                                splitResult[i * col_cell_size + j].contrast_score,
                                Q1_contrast_score - 8);
                        fclose(file);
                    }
#endif
                } else if (is_LT_RB_corner == 0 && Q2_contrast_score < 10 &&
                           splitResult[i * col_cell_size + j].contrast_score <
                               Q1_contrast_score - 6) {
#ifdef DEBUG
                    FILE* file;
                    if (fopen_s(&file, "hist_log.txt", "a") == 0) {
                        fprintf(file, "%i %i contrast_score %i < %i\n", i, j,
                                splitResult[i * col_cell_size + j].contrast_score,
                                Q1_contrast_score - 6);
                        fclose(file);
                    }
#endif
                } else if ((i != 0 && i != row_cell_size - 1 && j != 0 && j != col_cell_size - 1) &&
                           splitResult[i * col_cell_size + j].sun_factor <= 50 &&
                           (splitResult[i * col_cell_size + j].mean < mean_thres * mean_thres_0 ||
                            splitResult[i * col_cell_size + j].mean >
                                MIN(mean_thres * mean_thres_1, 2000))) {
#ifdef DEBUG
                    if (fopen_s(&file, "hist_log.txt", "a") == 0) {
                        fprintf(file,
                                "%i %i max_contrast_score %i mean %f < %f > %f sun_condition %i\n",
                                i, j, max_contrast_score, splitResult[i * col_cell_size + j].mean,
                                mean_thres * mean_thres_0, MIN(mean_thres * mean_thres_1, 2000),
                                splitResult[i * col_cell_size + j].sun_factor);
                        fclose(file);
                    }
#endif
                } else if ((i == 0 || i == row_cell_size - 1 || j == 0 || j == col_cell_size - 1) &&
                           splitResult[i * col_cell_size + j].sun_factor <= 50 &&
                           (splitResult[i * col_cell_size + j].mean <
                                mean_thres * mean_thres_boundary_min ||
                            splitResult[i * col_cell_size + j].mean >
                                mean_thres * mean_thres_boundary_max)) {
#ifdef DEBUG
                    if (fopen_s(&file, "hist_log.txt", "a") == 0) {
                        fprintf(file,
                                "%i %i max_contrast_score %i mean %f < %f > %f sun_condition %i\n",
                                i, j, max_contrast_score, splitResult[i * col_cell_size + j].mean,
                                mean_thres * mean_thres_boundary_min,
                                mean_thres * mean_thres_boundary_max,
                                splitResult[i * col_cell_size + j].sun_factor);
                        fclose(file);
                    }
#endif
                } else if (splitResult[i * col_cell_size + j].is_period == 1) {
#ifdef OUTPUT_MASK
                    for (k = 0; k < _height; k++) {
                        for (l = 0; l < _width; l++) {
                            if (k > crop_y && k < crop_y + _stride - 1 && l > crop_x &&
                                l < crop_x + _stride - 1) {
                                mask[k * _width + l] = _p8bit[k * _width + l];
                            }
                        }
                    }
#endif  // OUTPUT_MASK
                    partial_score += (double)100 / total_area;

                    if (j >= inner_width_offset && j < col_cell_size - inner_width_offset &&
                        i >= inner_height_offset && i < row_cell_size - inner_height_offset) {
                        inner_partial_score += (double)100 / total_inner_area;
                    }
                }
				else if ((splitResult[i * col_cell_size + j].max_peaks_num >= 4 &&
					splitResult[i * col_cell_size + j].second_peaks_num >= 4) ||
					(splitResult[i * col_cell_size + j].max_valleys_num >= 4 &&
						splitResult[i * col_cell_size + j].second_valleys_num >= 4) ||
						(splitResult[i * col_cell_size + j].max_peaks_num >= 4 &&
							splitResult[i * col_cell_size + j].max_valleys_num >= 4)/* ||
					splitResult[i * col_cell_size + j].max_peaks_num >= 5 ||
					splitResult[i * col_cell_size + j].max_valleys_num >= 5*/)
				{
#ifdef OUTPUT_MASK
                    for (k = 0; k < _height; k++) {
                        for (l = 0; l < _width; l++) {
                            if (k > crop_y && k < crop_y + _stride - 1 && l > crop_x &&
                                l < crop_x + _stride - 1) {
                                mask[k * _width + l] = _p8bit[k * _width + l];
                            }
                        }
                    }
#endif  // OUTPUT_MASK
                    partial_score += (double)100 / total_area;

                    if (j >= inner_width_offset && j < col_cell_size - inner_width_offset &&
                        i >= inner_height_offset && i < row_cell_size - inner_height_offset) {
                        inner_partial_score += (double)100 / total_inner_area;
                    }
                } else {
#ifdef DEBUG
                    if (fopen_s(&file, "hist_log.txt", "a") == 0) {
                        fprintf(file, "%i %i\n", i, j);
                        fclose(file);
                    }
#endif
                }
            } else if (max_contrast_score >= 15 && Q1_contrast_score >= 10)  // few finger print
            {
                if (is_LT_RB_corner == 0 && max_contrast_score - Q2_contrast_score > 6 &&
                    Q2_contrast_score < 9 &&
                    splitResult[i * col_cell_size + j].contrast_score < 10) {
#ifdef DEBUG
                    FILE* file;
                    if (fopen_s(&file, "hist_log.txt", "a") == 0) {
                        fprintf(file, "%i %i contrast_score %i < %i\n", i, j,
                                splitResult[i * col_cell_size + j].contrast_score, 10);
                        fclose(file);
                    }
#endif
                } else if (is_LT_RB_corner == 0 && max_contrast_score - Q2_contrast_score <= 6 &&
                           Q2_contrast_score >= 9 &&
                           splitResult[i * col_cell_size + j].contrast_score < 8) {
#ifdef DEBUG
                    FILE* file;
                    if (fopen_s(&file, "hist_log.txt", "a") == 0) {
                        fprintf(file, "%i %i contrast_score %i < %i\n", i, j,
                                splitResult[i * col_cell_size + j].contrast_score, 8);
                        fclose(file);
                    }
#endif
                } else if (is_LT_RB_corner == 0 && max_contrast_score - Q2_contrast_score > 6 &&
                           Q2_contrast_score >= 9 &&
                           splitResult[i * col_cell_size + j].contrast_score < 9) {
#ifdef DEBUG
                    FILE* file;
                    if (fopen_s(&file, "hist_log.txt", "a") == 0) {
                        fprintf(file, "%i %i contrast_score %i < %i\n", i, j,
                                splitResult[i * col_cell_size + j].contrast_score, 9);
                        fclose(file);
                    }
#endif
                } else if (is_LT_RB_corner == 0 && Q2_contrast_score < 9 &&
                           splitResult[i * col_cell_size + j].contrast_score < 10) {
#ifdef DEBUG
                    FILE* file;
                    if (fopen_s(&file, "hist_log.txt", "a") == 0) {
                        fprintf(file, "%i %i contrast_score %i < %i\n", i, j,
                                splitResult[i * col_cell_size + j].contrast_score, 10);
                        fclose(file);
                    }
#endif
                } else if ((i != 0 && i != row_cell_size - 1 && j != 0 && j != col_cell_size - 1) &&
                           splitResult[i * col_cell_size + j].sun_factor <= 50 &&
                           (splitResult[i * col_cell_size + j].mean < mean_thres * mean_thres_2 ||
                            splitResult[i * col_cell_size + j].mean >
                                MIN(mean_thres * mean_thres_3, 2000))) {
#ifdef DEBUG
                    if (fopen_s(&file, "hist_log.txt", "a") == 0) {
                        fprintf(file,
                                "%i %i max_contrast_score %i mean %f < %f > %f sun_condition %i\n",
                                i, j, max_contrast_score, splitResult[i * col_cell_size + j].mean,
                                mean_thres * mean_thres_2, MIN(mean_thres * mean_thres_3, 2000),
                                splitResult[i * col_cell_size + j].sun_factor);
                        fclose(file);
                    }
#endif
                } else if ((i == 0 || i == row_cell_size - 1 || j == 0 || j == col_cell_size - 1) &&
                           splitResult[i * col_cell_size + j].sun_factor <= 50 &&
                           (splitResult[i * col_cell_size + j].mean <
                                mean_thres * mean_thres_boundary_min ||
                            splitResult[i * col_cell_size + j].mean >
                                mean_thres * mean_thres_boundary_max)) {
#ifdef DEBUG
                    if (fopen_s(&file, "hist_log.txt", "a") == 0) {
                        fprintf(file,
                                "%i %i max_contrast_score %i mean %f < %f > %f sun_condition %i\n",
                                i, j, max_contrast_score, splitResult[i * col_cell_size + j].mean,
                                mean_thres * mean_thres_boundary_min,
                                mean_thres * mean_thres_boundary_max,
                                splitResult[i * col_cell_size + j].sun_factor);
                        fclose(file);
                    }
#endif
                }
				else if ((splitResult[i * col_cell_size + j].max_peaks_num >= 4 &&
					splitResult[i * col_cell_size + j].second_peaks_num >= 4) ||
					(splitResult[i * col_cell_size + j].max_valleys_num >= 4 &&
						splitResult[i * col_cell_size + j].second_valleys_num >= 4) ||
						(splitResult[i * col_cell_size + j].max_peaks_num >= 4 &&
							splitResult[i * col_cell_size + j].max_valleys_num >= 4)/* ||
					splitResult[i * col_cell_size + j].max_peaks_num >= 5 ||
					splitResult[i * col_cell_size + j].max_valleys_num >= 5*/)
				{
#ifdef OUTPUT_MASK
                    for (k = 0; k < _height; k++) {
                        for (l = 0; l < _width; l++) {
                            if (k > crop_y && k < crop_y + _stride - 1 && l > crop_x &&
                                l < crop_x + _stride - 1) {
                                mask[k * _width + l] = _p8bit[k * _width + l];
                            }
                        }
                    }
#endif  // OUTPUT_MASK
                    partial_score += (double)100 / total_area;

                    if (j >= inner_width_offset && j < col_cell_size - inner_width_offset &&
                        i >= inner_height_offset && i < row_cell_size - inner_height_offset) {
                        inner_partial_score += (double)100 / total_inner_area;
                    }
                } else if (splitResult[i * col_cell_size + j].is_period == 1) {
#ifdef OUTPUT_MASK
                    for (k = 0; k < _height; k++) {
                        for (l = 0; l < _width; l++) {
                            if (k > crop_y && k < crop_y + _stride - 1 && l > crop_x &&
                                l < crop_x + _stride - 1) {
                                mask[k * _width + l] = _p8bit[k * _width + l];
                            }
                        }
                    }
#endif  // OUTPUT_MASK
                    partial_score += (double)100 / total_area;

                    if (j >= inner_width_offset && j < col_cell_size - inner_width_offset &&
                        i >= inner_height_offset && i < row_cell_size - inner_height_offset) {
                        inner_partial_score += (double)100 / total_inner_area;
                    }
                } else {
#ifdef DEBUG
                    if (fopen_s(&file, "hist_log.txt", "a") == 0) {
                        fprintf(file, "%i %i\n", i, j);
                        fclose(file);
                    }
#endif
                }
            } else if (max_contrast_score < 15 && Q1_contrast_score >= 10) {
                if (is_LT_RB_corner == 0 && max_contrast_score - Q1_contrast_score > 5 &&
                    splitResult[i * col_cell_size + j].contrast_score < Q1_contrast_score) {
#ifdef DEBUG
                    FILE* file;
                    if (fopen_s(&file, "hist_log.txt", "a") == 0) {
                        fprintf(file, "%i %i contrast_score %i < %i\n", i, j,
                                splitResult[i * col_cell_size + j].contrast_score,
                                Q1_contrast_score);
                        fclose(file);
                    }
#endif
                } else if (is_LT_RB_corner == 0 && Q2_contrast_score >= 11 &&
                           splitResult[i * col_cell_size + j].contrast_score <
                               MIN(max_contrast_score - 5, 10)) {
#ifdef DEBUG
                    FILE* file;
                    if (fopen_s(&file, "hist_log.txt", "a") == 0) {
                        fprintf(file, "%i %i contrast_score %i < %i\n", i, j,
                                splitResult[i * col_cell_size + j].contrast_score,
                                MIN(max_contrast_score - 5, 10));
                        fclose(file);
                    }
#endif
                } else if (is_LT_RB_corner == 0 && Q2_contrast_score >= 10 &&
                           splitResult[i * col_cell_size + j].contrast_score <
                               MIN(max_contrast_score - 6, 7)) {
#ifdef DEBUG
                    FILE* file;
                    if (fopen_s(&file, "hist_log.txt", "a") == 0) {
                        fprintf(file, "%i %i contrast_score %i < %i\n", i, j,
                                splitResult[i * col_cell_size + j].contrast_score,
                                MIN(max_contrast_score - 6, 7));
                        fclose(file);
                    }
#endif
                } else if ((i != 0 && i != row_cell_size - 1 && j != 0 && j != col_cell_size - 1) &&
                           splitResult[i * col_cell_size + j].sun_factor <= 50 &&
                           (splitResult[i * col_cell_size + j].mean < mean_thres * mean_thres_2 ||
                            splitResult[i * col_cell_size + j].mean >
                                MIN(mean_thres * mean_thres_3, 2000))) {
#ifdef DEBUG
                    if (fopen_s(&file, "hist_log.txt", "a") == 0) {
                        fprintf(file,
                                "%i %i max_contrast_score %i mean %f < %f > %f sun_condition %i\n",
                                i, j, max_contrast_score, splitResult[i * col_cell_size + j].mean,
                                mean_thres * mean_thres_2, MIN(mean_thres * mean_thres_3, 2000),
                                splitResult[i * col_cell_size + j].sun_factor);
                        fclose(file);
                    }
#endif
                } else if ((i == 0 || i == row_cell_size - 1 || j == 0 || j == col_cell_size - 1) &&
                           splitResult[i * col_cell_size + j].sun_factor <= 50 &&
                           (splitResult[i * col_cell_size + j].mean <
                                mean_thres * mean_thres_boundary_min ||
                            splitResult[i * col_cell_size + j].mean >
                                mean_thres * mean_thres_boundary_max)) {
#ifdef DEBUG
                    if (fopen_s(&file, "hist_log.txt", "a") == 0) {
                        fprintf(file,
                                "%i %i max_contrast_score %i mean %f < %f > %f sun_condition %i\n",
                                i, j, max_contrast_score, splitResult[i * col_cell_size + j].mean,
                                mean_thres * mean_thres_boundary_min,
                                mean_thres * mean_thres_boundary_max,
                                splitResult[i * col_cell_size + j].sun_factor);
                        fclose(file);
                    }
#endif
                }
				else if ((splitResult[i * col_cell_size + j].max_peaks_num >= 4 &&
					splitResult[i * col_cell_size + j].second_peaks_num >= 4) ||
					(splitResult[i * col_cell_size + j].max_valleys_num >= 4 &&
						splitResult[i * col_cell_size + j].second_valleys_num >= 4) ||
						(splitResult[i * col_cell_size + j].max_peaks_num >= 4 &&
							splitResult[i * col_cell_size + j].max_valleys_num >= 4)/* ||
					splitResult[i * col_cell_size + j].max_peaks_num >= 5 ||
					splitResult[i * col_cell_size + j].max_valleys_num >= 5*/)
				{
#ifdef OUTPUT_MASK
                    for (k = 0; k < _height; k++) {
                        for (l = 0; l < _width; l++) {
                            if (k > crop_y && k < crop_y + _stride - 1 && l > crop_x &&
                                l < crop_x + _stride - 1) {
                                mask[k * _width + l] = _p8bit[k * _width + l];
                            }
                        }
                    }
#endif  // OUTPUT_MASK
                    partial_score += (double)100 / total_area;

                    if (j >= inner_width_offset && j < col_cell_size - inner_width_offset &&
                        i >= inner_height_offset && i < row_cell_size - inner_height_offset) {
                        inner_partial_score += (double)100 / total_inner_area;
                    }
                } else if (splitResult[i * col_cell_size + j].is_period == 1) {
#ifdef OUTPUT_MASK
                    for (k = 0; k < _height; k++) {
                        for (l = 0; l < _width; l++) {
                            if (k > crop_y && k < crop_y + _stride - 1 && l > crop_x &&
                                l < crop_x + _stride - 1) {
                                mask[k * _width + l] = _p8bit[k * _width + l];
                            }
                        }
                    }
#endif  // OUTPUT_MASK
                    partial_score += (double)100 / total_area;

                    if (j >= inner_width_offset && j < col_cell_size - inner_width_offset &&
                        i >= inner_height_offset && i < row_cell_size - inner_height_offset) {
                        inner_partial_score += (double)100 / total_inner_area;
                    }
                } else {
#ifdef DEBUG
                    if (fopen_s(&file, "hist_log.txt", "a") == 0) {
                        fprintf(file, "%i %i\n", i, j);
                        fclose(file);
                    }
#endif
                }
            } else  // noisy finger
            {
                if (splitResult[i * col_cell_size + j].contrast_score <
                    MIN(Q1_contrast_score - 4, noisy_finger_max_contrast_score)) {
#ifdef DEBUG
                    FILE* file;
                    if (fopen_s(&file, "hist_log.txt", "a") == 0) {
                        fprintf(file, "%i %i contrast_score %i < %i\n", i, j,
                                splitResult[i * col_cell_size + j].contrast_score,
                                MIN(Q1_contrast_score, noisy_finger_max_contrast_score));
                        fclose(file);
                    }
#endif
                } else if ((i != 0 && i != row_cell_size - 1 && j != 0 && j != col_cell_size - 1) &&
                           (splitResult[i * col_cell_size + j].mean < mean_thres * mean_thres_4 ||
                            (splitResult[i * col_cell_size + j].mean >
                             MIN(mean_thres * mean_thres_5, 2000)))) {
#ifdef DEBUG
                    if (fopen_s(&file, "hist_log.txt", "a") == 0) {
                        fprintf(file,
                                "max_contrast_score >= 5: %i %i max_contrast_score %i mean %f < %f "
                                "or > %f\n",
                                i, j, max_contrast_score, splitResult[i * col_cell_size + j].mean,
                                mean_thres * mean_thres_4, MIN(mean_thres * mean_thres_5, 2000));
                        fclose(file);
                    }
#endif
                } else if ((i == 0 || i == row_cell_size - 1 || j == 0 || j == col_cell_size - 1) &&
                           splitResult[i * col_cell_size + j].sun_factor <= 50 &&
                           (splitResult[i * col_cell_size + j].mean <
                                mean_thres * mean_thres_boundary_min ||
                            splitResult[i * col_cell_size + j].mean >
                                mean_thres * mean_thres_boundary_max)) {
#ifdef DEBUG
                    if (fopen_s(&file, "hist_log.txt", "a") == 0) {
                        fprintf(file,
                                "%i %i max_contrast_score %i mean %f < %f > %f sun_condition %i\n",
                                i, j, max_contrast_score, splitResult[i * col_cell_size + j].mean,
                                mean_thres * mean_thres_boundary_min,
                                mean_thres * mean_thres_boundary_max,
                                splitResult[i * col_cell_size + j].sun_factor);
                        fclose(file);
                    }
#endif
                } else if ((splitResult[i * col_cell_size + j].max_peaks_num >= 4 &&
                            splitResult[i * col_cell_size + j].second_peaks_num >= 4) ||
                           (splitResult[i * col_cell_size + j].max_valleys_num >= 4 &&
                            splitResult[i * col_cell_size + j].second_valleys_num >= 4) ||
                           (splitResult[i * col_cell_size + j].max_peaks_num >= 4 &&
                            splitResult[i * col_cell_size + j].max_valleys_num >= 4)) {
#ifdef OUTPUT_MASK
                    for (k = 0; k < _height; k++) {
                        for (l = 0; l < _width; l++) {
                            if (k > crop_y && k < crop_y + _stride - 1 && l > crop_x &&
                                l < crop_x + _stride - 1) {
                                mask[k * _width + l] = _p8bit[k * _width + l];
                            }
                        }
                    }
#endif  // OUTPUT_MASK
                    partial_score += (double)100 / total_area;

                    if (j >= inner_width_offset && j < col_cell_size - inner_width_offset &&
                        i >= inner_height_offset && i < row_cell_size - inner_height_offset) {
                        inner_partial_score += (double)100 / total_inner_area;
                    }
                } else if (splitResult[i * col_cell_size + j].is_period == 1) {
#ifdef OUTPUT_MASK
                    for (k = 0; k < _height; k++) {
                        for (l = 0; l < _width; l++) {
                            if (k > crop_y && k < crop_y + _stride - 1 && l > crop_x &&
                                l < crop_x + _stride - 1) {
                                mask[k * _width + l] = _p8bit[k * _width + l];
                            }
                        }
                    }
#endif  // OUTPUT_MASK
                    partial_score += (double)100 / total_area;

                    if (j >= inner_width_offset && j < col_cell_size - inner_width_offset &&
                        i >= inner_height_offset && i < row_cell_size - inner_height_offset) {
                        inner_partial_score += (double)100 / total_inner_area;
                    }
                } else {
#ifdef DEBUG
                    if (fopen_s(&file, "hist_log.txt", "a") == 0) {
                        fprintf(file, "%i %i\n", i, j);
                        fclose(file);
                    }
#endif
                }
            }
        }
    }

#ifdef EGIS_DEBUG
    egislog_d("Egis_debug partial 307");
#endif

    _pPartialDetectorResult->partial_ratio = partial_score;
    _pPartialDetectorResult->inner_partial_ratio = inner_partial_score;
    _pPartialDetectorResult->room_light_partial_ratio = light_score;
    _pPartialDetectorResult->inner_room_light_partial_ratio = inner_light_score;

    FREE(splitResult);
}

static void split_cells(int* _pImg, unsigned char* _p8bit, int _width, int _height, int _cx,
                        int _cy, int _radius_boundary, enum fp_type _fptype,
                        enum model_type _modeltype, enum lens_type _lenstype, int _cell_width,
                        int _stride, int _light_thres, int _light_index,
                        struct PartialDetectorResult* _pPartialDetectorResult) {
    int dark_thres = 100;
    switch (_modeltype) {
        case MODEL_702:
            dark_thres = 7000;
            break;
        default:
            break;
    }

#ifdef EGIS_DEBUG
    egislog_d("Egis_debug partial 301 %i", dark_thres);
#endif

    int i, j, k, l;
    // int _cell_width = 20;
    // int _stride = 20;
    int col_cell_size = (_width - _cell_width) / _stride + 1;
    int row_cell_size = (_height - _cell_width) / _stride + 1;
    int cell_xshift = (_width - _stride * (col_cell_size - 1) - _cell_width) / 2;
    int cell_yshift = (_height - _stride * (row_cell_size - 1) - _cell_width) / 2;

    struct SplitResult* splitResult =
        (struct SplitResult*)malloc(col_cell_size * row_cell_size * sizeof(struct SplitResult));

    int crop_x = 0;
    int crop_y = 0;
    int* img_crop = (int*)malloc(_cell_width * _cell_width * sizeof(int));
    int* bit8_crop = (int*)malloc(_cell_width * _cell_width * sizeof(int));

#ifdef OUTPUT_MASK
    mask = (unsigned char*)malloc(_width * _height * sizeof(unsigned char));
    for (i = 0; i < _height * _width; i++) {
        mask[i] = _p8bit[i] * 0.5;
    }
#endif  // DEBUG

#ifdef EGIS_DEBUG
    egislog_d("Egis_debug partial 302");
#endif

#ifdef DEBUG
    FILE* file;
#endif

    int max_contrast_score = 0;
    int max_contrast_minus_count = 0;
    int contrast_4_count = 0;
    int sum = 0;
    float mean_thres = 0;
    int sun_condition = 0;
    float avg_max_profile_valley = 0;
    int dry_constrast_count = 0;
    float mean_thres_0 = 0.85f;
    float mean_thres_1 = 1.15f;
    float mean_thres_2 = 0.5f;
    float mean_thres_3 = 1.3f;
    float mean_thres_4 = 0.7f;
    float mean_thres_5 = 1.3f;
    float mean_thres_boundary_min = 0.4f;
    float mean_thres_boundary_max = 2.5f;
    float valley_ratio_thres = 0.4f;
    int sun_factor_total_thres = 50;
    int max_profile_valley_thres = 40;
    int noisy_finger_max_contrast_score = 5;
    int second_peaks_num_thres = 4;
    float LT_RB_a0 = 0.0f;
    float LT_RB_b0 = 0.0f;
    float LT_RB_a1 = 0.0f;
    float LT_RB_b1 = 0.0f;

    switch (_modeltype) {
        case MODEL_A50:
            break;
        case MODEL_A70:
            break;
        case MODEL_A80:
            break;
        case MODEL_TAB_S6:
            break;
        case MODEL_A90:
            break;
        case MODEL_A30S:
            break;
        case MODEL_A50S:
            break;
        case MODEL_A70S:
            break;
        case MODEL_A51:
            break;
        case MODEL_A71:
            break;
        case MODEL_A91:
            max_profile_valley_thres = 7;
            //_light_thres = 4000;
            sun_factor_total_thres = 100;
            mean_thres_0 = 0.5f;
            mean_thres_1 = 100;
            mean_thres_2 = 0.5f;
            mean_thres_3 = 100;
            mean_thres_4 = 0.5f;
            mean_thres_5 = 100;
            mean_thres_boundary_min = 0.4f;
            mean_thres_boundary_max = 100;
            noisy_finger_max_contrast_score = 6;
            second_peaks_num_thres = 3;
            // max_contrast_score = Q1_contrast_score;
            // max_contrast_minus_count = Q1_contrast_count;
            break;
        case MODEL_A_NOTE:
            break;
        case MODEL_702:
            break;
        case MODEL_LG:
            max_profile_valley_thres = 7;
            sun_factor_total_thres = 100;
            mean_thres_0 = -1;
            mean_thres_1 = 100;
            mean_thres_2 = -1;
            mean_thres_3 = 100;
            mean_thres_4 = -1;
            mean_thres_5 = 100;
            mean_thres_boundary_min = -1;
            mean_thres_boundary_max = 100;
            noisy_finger_max_contrast_score = 6;
            second_peaks_num_thres = 3;
            LT_RB_a0 = 140;
            LT_RB_b0 = -0.55f;
            LT_RB_a1 = 200;
            LT_RB_b1 = -0.55f;
            break;
        case MODEL_HW:
            break;
        default:
            break;
    }

    int is_boundary = 0;
    int dist = 0;
    int total_area = 0;
    int total_inner_area = 0;
    int inner_width_offset = col_cell_size / 4;
    int inner_height_offset = row_cell_size / 4;
    for (i = 0; i < row_cell_size; i++) {
        for (j = 0; j < col_cell_size; j++) {
            crop_x = cell_xshift + j * _stride;
            crop_y = cell_yshift + i * _stride;

            for (k = 0; k < _height; k++) {
                for (l = 0; l < _width; l++) {
                    if (k >= crop_y && k < crop_y + _cell_width && l >= crop_x &&
                        l < crop_x + _cell_width) {
                        img_crop[(k - crop_y) * _cell_width + (l - crop_x)] = _pImg[k * _width + l];
                        bit8_crop[(k - crop_y) * _cell_width + (l - crop_x)] =
                            _p8bit[k * _width + l];
                    }
                }
            }

#ifdef DEBUG
            if (fopen_s(&file, "hist_log.txt", "a") == 0) {
                fprintf(file, "i %i j %i\n", i, j);
                fclose(file);
            }

            if (fopen_s(&file, "hist.csv", "a") == 0) {
                fprintf(file, "i %i j %i\n", i, j);
                fclose(file);
            }

            if (i == 5 && j == 5) {
                printf("");
            }
#endif

            if (i == 0 || j == 0 || i == row_cell_size - 1 || j == col_cell_size - 1) {
                is_boundary = 1;
            } else {
                is_boundary = 0;
            }

            splitResult[i * col_cell_size + j].sharp_score = 0;
            splitResult[i * col_cell_size + j].contrast_score = 0;
            splitResult[i * col_cell_size + j].max_peaks_num = 0;
            splitResult[i * col_cell_size + j].max_valleys_num = 0;
            splitResult[i * col_cell_size + j].max_profile_valley = 0;
            splitResult[i * col_cell_size + j].second_peaks_num = 0;
            splitResult[i * col_cell_size + j].second_valleys_num = 0;
            splitResult[i * col_cell_size + j].second_profile_valley = 0;
            splitResult[i * col_cell_size + j].weak_max_peaks_num = 0;
            splitResult[i * col_cell_size + j].weak_second_peaks_num = 0;
            splitResult[i * col_cell_size + j].mean = 0;
            splitResult[i * col_cell_size + j].sun_factor = 0;
            splitResult[i * col_cell_size + j].is_period = 0;
            splitResult[i * col_cell_size + j].is_light = 0;
            splitResult[i * col_cell_size + j].is_out_boundary = 0;

            dist = int_sqrt(
                (_cx - (crop_x + _cell_width * 0.5)) * (_cx - (crop_x + _cell_width * 0.5)) +
                (_cy - (crop_y + _cell_width * 0.5)) * (_cy - (crop_y + _cell_width * 0.5)));
            if (dist > _radius_boundary) {
                splitResult[i * col_cell_size + j].is_out_boundary = 1;
            } else {
                get_small_area_result_avg(img_crop, bit8_crop, _cell_width, _cell_width,
                                          is_boundary, _light_thres, sun_factor_total_thres,
                                          _fptype, _modeltype, _lenstype,
                                          splitResult + i * col_cell_size + j);

                splitResult[i * col_cell_size + j].is_out_boundary = 0;
                total_area++;
                if (j >= inner_width_offset && j < col_cell_size - inner_width_offset &&
                    i >= inner_height_offset && i < row_cell_size - inner_height_offset) {
                    total_inner_area++;
                }
            }
        }
    }
    FREE(img_crop);
    FREE(bit8_crop);

#ifdef EGIS_DEBUG
    egislog_d("Egis_debug partial 303");
#endif

    float partial_score = 0.0;
    float inner_partial_score = 0.0;
    float light_score = 0.0;
    float inner_light_score = 0.0;

    // get mean from max contrast_score
    for (i = 0; i < row_cell_size; i++) {
        for (j = 0; j < col_cell_size; j++) {
            if (max_contrast_score < splitResult[i * col_cell_size + j].contrast_score) {
                max_contrast_score = splitResult[i * col_cell_size + j].contrast_score;
            }

            avg_max_profile_valley += splitResult[i * col_cell_size + j].max_profile_valley;

            sun_condition += splitResult[i * col_cell_size + j].sun_factor;
        }
    }

    avg_max_profile_valley = (double)avg_max_profile_valley / (row_cell_size * col_cell_size);

#ifdef EGIS_DEBUG
    egislog_d("Egis_debug partial 304");
#endif

    for (i = 0; i < row_cell_size; i++) {
        for (j = 0; j < col_cell_size; j++) {
            if (splitResult[i * col_cell_size + j].contrast_score >= max_contrast_score - 1) {
                sum += splitResult[i * col_cell_size + j].mean;
                max_contrast_minus_count++;
            }
            if (splitResult[i * col_cell_size + j].contrast_score >= 4) {
                contrast_4_count++;
            }
            if (splitResult[i * col_cell_size + j].contrast_score >=
                noisy_finger_max_contrast_score) {
                dry_constrast_count++;
            }
        }
    }
    mean_thres = (double)sum / max_contrast_minus_count;

#ifdef EGIS_DEBUG
    egislog_d("Egis_debug partial 305");
#endif

    // sort contrast
    int* sort_contrast = (int*)malloc(row_cell_size * col_cell_size * sizeof(int));

    // copy
    for (i = 0; i < row_cell_size; i++) {
        for (j = 0; j < col_cell_size; j++) {
            sort_contrast[i * col_cell_size + j] =
                splitResult[i * col_cell_size + j].contrast_score;
        }
    }

    // sort
    int temp = 0;
    for (i = 0; i < row_cell_size * col_cell_size - 1; i++) {
        for (j = i + 1; j < row_cell_size * col_cell_size; j++) {
            if (sort_contrast[j] > sort_contrast[i]) {
                temp = sort_contrast[i];
                sort_contrast[i] = sort_contrast[j];
                sort_contrast[j] = temp;
            }
        }
    }

    int q1 = (int)row_cell_size * col_cell_size * 0.25;
    int q2 = (int)row_cell_size * col_cell_size * 0.5;
    int Q1_contrast_score = sort_contrast[q1];
    int Q1_contrast_count = q1;
    for (i = q1 + 1; i < row_cell_size * col_cell_size; i++) {
        if (sort_contrast[i] == Q1_contrast_score) {
            Q1_contrast_count++;
        } else {
            break;
        }
    }
    int Q2_contrast_score = sort_contrast[q2];
    int Q2_contrast_count = q2;
    for (i = q2 + 1; i < row_cell_size * col_cell_size; i++) {
        if (sort_contrast[i] == Q2_contrast_score) {
            Q2_contrast_score++;
        } else {
            break;
        }
    }
    FREE(sort_contrast);

    switch (_modeltype) {
        case MODEL_LG:
            _light_index = mean_thres + 300;
            break;
        default:
            break;
    }

#ifdef DEBUG
    if (fopen_s(&file, "hist_log.txt", "a") == 0) {
        fprintf(file, "max_contrast_score %i\n", max_contrast_score);
        fprintf(file, "max_contrast_minus_count %i\n", max_contrast_minus_count);
        fprintf(file, "mean_thres %f, mean_thres_range %f\n", mean_thres,
                (_light_index - mean_thres));
        fprintf(file, "sun_condition %i\n", sun_condition);
        fprintf(file, "avg_max_profile_valley %f\n", avg_max_profile_valley);
        fprintf(file, "Q1_contrast_score %i Q1_contrast_count %i\n", Q1_contrast_score,
                Q1_contrast_count);
        fprintf(file, "Q2_contrast_score %i Q2_contrast_count %i\n", Q2_contrast_score,
                Q2_contrast_count);
        fclose(file);
    }
#endif

#ifdef EGIS_DEBUG
    egislog_d("Egis_debug partial 306");
#endif

    if (_p8bit != NULL && splitResult != NULL && _pPartialDetectorResult != NULL) {
        get_cell_result(_p8bit, _width, _height, _cx, _cy, _radius_boundary, _stride, _cell_width,
                        row_cell_size, col_cell_size, cell_xshift, cell_yshift, inner_width_offset,
                        inner_height_offset, splitResult, total_area, total_inner_area,
                        max_contrast_score, Q1_contrast_score, Q2_contrast_score,
                        // int max_contrast_minus_count,
                        // int contrast_4_count,
                        // int sum,
                        mean_thres, _light_index,
                        // int sun_condition,
                        // float avg_max_profile_valley,
                        // int dry_constrast_count,
                        mean_thres_0, mean_thres_1, mean_thres_2, mean_thres_3, mean_thres_4,
                        mean_thres_5, mean_thres_boundary_max, mean_thres_boundary_min,
                        // float valley_ratio_thres,
                        sun_factor_total_thres,
                        // int max_profile_valley_thres,
                        noisy_finger_max_contrast_score, second_peaks_num_thres, LT_RB_a0, LT_RB_b0,
                        LT_RB_a1, LT_RB_b1, _pPartialDetectorResult);

#ifdef EGIS_DEBUG
        egislog_d("Egis_debug partial 307");
#endif

        FREE(splitResult);
    }
}

static void smooth_1Darray(int* _pInput, int _input_size, int _kernel_size, int* _pOutput) {
    int i, j;
    // smooth profile
    float* kernel = malloc(_kernel_size * sizeof(float));
    for (i = 0; i < _kernel_size; i++) {
        kernel[i] = (double)1 / _kernel_size;
    }

    int kernel_half = _kernel_size >> 1;

    int src_pos = kernel_half;

    for (i = kernel_half; i < _input_size - kernel_half; ++i) {
        float convolve = 0.0;

        int conv_pos = src_pos - kernel_half;
        for (j = 0; j < _kernel_size; j++) {
            convolve += _pInput[conv_pos] * kernel[j];
            ++conv_pos;
        }
        _pOutput[src_pos] = convolve;

        ++src_pos;
    }

    FREE(kernel);

#ifdef DEBUG
    FILE* file;
    if (fopen_s(&file, "hist.csv", "a") == 0) {
        int need_comma = 0;
        // for (i = kernel_half; i < _input_size - kernel_half; ++i)
        //{
        //	if (need_comma)
        //		fprintf(file, ", ");
        //	else
        //		need_comma = 1;
        //	fprintf(file, "%i", _pInput[i]);
        //}
        // fprintf(file, "\n");
        //
        // need_comma = 0;
        for (i = kernel_half; i < _input_size - kernel_half; ++i) {
            if (need_comma)
                fprintf(file, ", ");
            else
                need_comma = 1;
            fprintf(file, "%i", _pOutput[i]);
        }
        fprintf(file, "\n");
        fclose(file);
    }
#endif
}

static void find_hillside(int* _pInput, int _input_size, int _kernel_size, int _max_height_thres,
                          struct HillSideInfo* _pHillSideInfo, int* _pHillside_size) {
    int i, j, k, l;
    int kernel_half = _kernel_size >> 1;

#ifdef EGIS_DEBUG
    egislog_d("Egis_debug partial 445");
#endif

    int max = -0xFFFF;
    int min = 0xFFFF;
    float mean = 0.0;
    for (i = kernel_half; i < _input_size - kernel_half; ++i) {
        max = max > _pInput[i] ? max : _pInput[i];
        min = min < _pInput[i] ? min : _pInput[i];
        mean += _pInput[i];
    }
    mean = (double)mean / (_input_size - kernel_half * 2);

#ifdef EGIS_DEBUG
    egislog_d("Egis_debug partial 446");
#endif

#ifdef DEBUG
    FILE* file;
    if (fopen_s(&file, "hist_log.txt", "a") == 0) {
        fprintf(file, "max %i min %i mean %f\n", max, min, mean);
        fclose(file);
    }
#endif

    // get local peaks and valley
    struct HistogramInfoPeak histogramInfoPeak[500];
    int iHistogramInfoPeak_index = 0;
    struct HistogramInfoValley histogramInfoValley[500];
    int iHistogramInfoValley_index = 0;

    float local_max;
    float local_min;
    float thres_p = _max_height_thres;
    float thres_v = _max_height_thres;
    int search = 5;
    for (i = kernel_half; i < _input_size - kernel_half; ++i) {
        int debug = _pInput[i];

        if (i == kernel_half)  // initial
        {
            local_max = _pInput[i];
            local_min = _pInput[i];

            if (_pInput[i] > (max + min) * 0.5) {
                histogramInfoPeak[iHistogramInfoPeak_index].index = i;
                histogramInfoPeak[iHistogramInfoPeak_index].intensity = _pInput[i];
                iHistogramInfoPeak_index++;
            } else {
                histogramInfoValley[iHistogramInfoValley_index].index = i;
                histogramInfoValley[iHistogramInfoValley_index].intensity = _pInput[i];
                iHistogramInfoValley_index++;
            }
        }

        if (_pInput[i] > local_max) {
            local_max = _pInput[i];
        } else if (_pInput[i] < local_min) {
            local_min = _pInput[i];
        }

        // if (local_min == 2611.0)
        //{
        //	printf("");
        //}

        int local_peak = 1;
        for (j = i - search; j < i + search; j++)
        // for (j = i - kernel_half - extend_search; j < i + kernel_half + extend_search; j++)
        {
            if (j < kernel_half || j >= _input_size - kernel_half) {
                continue;
            }
            if (_pInput[i] < _pInput[j]) {
                local_peak = 0;
                break;
            }
        }

        int local_valley = 1;
        for (j = i - search; j < i + search; j++)
        // for (j = i - kernel_half - extend_search; j < i + kernel_half + extend_search; j++)
        {
            if (j < kernel_half || j >= _input_size - kernel_half) {
                continue;
            }
            if (_pInput[i] > _pInput[j]) {
                local_valley = 0;
                break;
            }
        }

        if (local_peak == 1) {
            printf("");
        }

        if (local_peak == 1 && _pInput[i] >= local_max && (local_max - local_min) > thres_p) {
            // last index is peak not valley, replace it
            if (iHistogramInfoPeak_index > 0 && iHistogramInfoValley_index > 0 &&
                histogramInfoValley[iHistogramInfoValley_index - 1].index <
                    histogramInfoPeak[iHistogramInfoPeak_index - 1].index) {
                histogramInfoPeak[iHistogramInfoPeak_index - 1].index = i;
                histogramInfoPeak[iHistogramInfoPeak_index - 1].intensity = _pInput[i];
            } else if (iHistogramInfoPeak_index > 0 && iHistogramInfoValley_index == 0) {
                histogramInfoPeak[iHistogramInfoPeak_index - 1].index = i;
                histogramInfoPeak[iHistogramInfoPeak_index - 1].intensity = _pInput[i];
            } else {
                histogramInfoPeak[iHistogramInfoPeak_index].index = i;
                histogramInfoPeak[iHistogramInfoPeak_index].intensity = _pInput[i];
                iHistogramInfoPeak_index++;
                thres_v = _max_height_thres;
            }
            local_min = local_max;
            thres_p = _max_height_thres;
        }

        if (local_valley == 1 && _pInput[i] <= local_min &&
            (local_max - local_min) > thres_v)  // no valley on the peak
        {
            if (iHistogramInfoPeak_index > 0 && iHistogramInfoValley_index > 0 &&
                histogramInfoPeak[iHistogramInfoPeak_index - 1].index <
                    histogramInfoValley[iHistogramInfoValley_index - 1].index) {
                histogramInfoValley[iHistogramInfoValley_index - 1].index = i;
                histogramInfoValley[iHistogramInfoValley_index - 1].intensity = _pInput[i];
            } else if (iHistogramInfoPeak_index == 0 && iHistogramInfoValley_index > 0) {
                histogramInfoValley[iHistogramInfoValley_index - 1].index = i;
                histogramInfoValley[iHistogramInfoValley_index - 1].intensity = _pInput[i];
            } else {
                histogramInfoValley[iHistogramInfoValley_index].index = i;
                histogramInfoValley[iHistogramInfoValley_index].intensity = _pInput[i];
                iHistogramInfoValley_index++;
                thres_p = _max_height_thres;
            }
            local_max = local_min;
            thres_v = _max_height_thres;
        }

        if (i == _input_size - kernel_half - 1) {
            if (_pInput[i] - local_min >= _pInput[i] - local_max && _pInput[i] >= local_max) {
                if (iHistogramInfoPeak_index > 0 && iHistogramInfoValley_index > 0 &&
                    histogramInfoValley[iHistogramInfoValley_index - 1].index <
                        histogramInfoPeak[iHistogramInfoPeak_index - 1].index) {
                    histogramInfoPeak[iHistogramInfoPeak_index - 1].index = i;
                    histogramInfoPeak[iHistogramInfoPeak_index - 1].intensity = _pInput[i];
                } else if (iHistogramInfoPeak_index > 0 && iHistogramInfoValley_index == 0) {
                    histogramInfoPeak[iHistogramInfoPeak_index - 1].index = i;
                    histogramInfoPeak[iHistogramInfoPeak_index - 1].intensity = _pInput[i];
                } else {
                    histogramInfoPeak[iHistogramInfoPeak_index].index = i;
                    histogramInfoPeak[iHistogramInfoPeak_index].intensity = _pInput[i];
                    iHistogramInfoPeak_index++;
                }
            } else if (_pInput[i] - local_min < _pInput[i] - local_max && _pInput[i] <= local_min) {
                if (iHistogramInfoPeak_index > 0 && iHistogramInfoValley_index > 0 &&
                    histogramInfoPeak[iHistogramInfoPeak_index - 1].index <
                        histogramInfoValley[iHistogramInfoValley_index - 1].index) {
                    histogramInfoValley[iHistogramInfoValley_index - 1].index = i;
                    histogramInfoValley[iHistogramInfoValley_index - 1].intensity = _pInput[i];
                } else if (iHistogramInfoPeak_index == 0 && iHistogramInfoValley_index > 0) {
                    histogramInfoValley[iHistogramInfoValley_index - 1].index = i;
                    histogramInfoValley[iHistogramInfoValley_index - 1].intensity = _pInput[i];
                } else {
                    histogramInfoValley[iHistogramInfoValley_index].index = i;
                    histogramInfoValley[iHistogramInfoValley_index].intensity = _pInput[i];
                    iHistogramInfoValley_index++;
                }
            }
        }

        if (iHistogramInfoPeak_index >= 500) {
            break;
        }
        if (iHistogramInfoValley_index >= 500) {
            break;
        }
    }

#ifdef EGIS_DEBUG
    egislog_d("Egis_debug partial 447");
#endif

    if (iHistogramInfoPeak_index + iHistogramInfoValley_index - 1 >= 500) {
        return;
    }

#ifdef EGIS_DEBUG
    egislog_d("Egis_debug partial 448");
#endif

    int ip = 0;
    int iv = 0;
    int hillside_size = 0;
    int extend = 40;
    for (i = 0; i < iHistogramInfoPeak_index + iHistogramInfoValley_index - 1; i++) {
        if (histogramInfoValley[iv].index > histogramInfoPeak[ip].index) {
            if (ABS(histogramInfoPeak[ip].intensity - histogramInfoValley[iv].intensity) < 300) {
                ip++;
                continue;
            }

            // check might be noisy boundary
            _pHillSideInfo[hillside_size].is_noisy_boundary = 0;
            if (histogramInfoValley[iv].index < _input_size - kernel_half - 6) {
                int min = 0xFFFF;
                for (j = histogramInfoValley[iv].index - 5;
                     j >= MAX(histogramInfoValley[iv].index - extend, kernel_half); j--) {
                    min = min < _pInput[j] ? min : _pInput[j];
                }

                if (histogramInfoValley[iv].intensity > min) {
                    _pHillSideInfo[hillside_size].is_noisy_boundary = 1;
                }
            }

            if (histogramInfoPeak[ip].index > kernel_half + 5) {
                int max = -0xFFFF;
                for (j = histogramInfoPeak[ip].index + 5;
                     j <= MIN(histogramInfoPeak[ip].index + extend, _input_size - kernel_half - 1);
                     j++) {
                    max = max > _pInput[j] ? max : _pInput[j];
                }

                if (histogramInfoPeak[ip].intensity < max) {
                    _pHillSideInfo[hillside_size].is_noisy_boundary = 1;
                }
            }

            // find half index
            int thres = (histogramInfoPeak[ip].intensity + histogramInfoValley[iv].intensity) * 0.5;
            for (l = histogramInfoPeak[ip].index + 1; l < histogramInfoValley[iv].index - 1; l++) {
                if ((_pInput[l] >= thres && _pInput[l + 1] < thres) ||
                    (_pInput[l] <= thres && _pInput[l + 1] > thres)) {
                    _pHillSideInfo[hillside_size].index = l;
                    break;
                }
            }
            _pHillSideInfo[hillside_size].width =
                histogramInfoValley[iv].index - histogramInfoPeak[ip].index;
            _pHillSideInfo[hillside_size].height =
                histogramInfoPeak[ip].intensity - histogramInfoValley[iv].intensity;
            _pHillSideInfo[hillside_size].direction = 0;
            _pHillSideInfo[hillside_size].pos_index = histogramInfoValley[iv].index;
            _pHillSideInfo[hillside_size].pos_intensity = histogramInfoValley[iv].intensity;
            _pHillSideInfo[hillside_size].neg_index = histogramInfoPeak[ip].index;
            _pHillSideInfo[hillside_size].neg_intensity = histogramInfoPeak[ip].intensity;
            _pHillSideInfo[hillside_size].mean = mean;
            ip++;
            hillside_size = hillside_size + 1;
        } else  // histogramInfoPeak[ip].index > histogramInfoValley[iv].index
        {
            if (ABS(histogramInfoPeak[ip].intensity - histogramInfoValley[iv].intensity) < 300) {
                iv++;
                continue;
            }

            // check might be noisy boundary
            _pHillSideInfo[hillside_size].is_noisy_boundary = 0;
            if (histogramInfoPeak[ip].intensity - mean < 300 &&
                histogramInfoPeak[ip].index < _input_size - kernel_half - 6) {
                int max = -0xFFFF;
                for (j = histogramInfoPeak[ip].index + 5;
                     j <= MIN(histogramInfoPeak[ip].index + extend, _input_size - kernel_half - 1);
                     j++) {
                    max = max > _pInput[j] ? max : _pInput[j];
                }

                if (histogramInfoPeak[ip].intensity < max) {
                    _pHillSideInfo[hillside_size].is_noisy_boundary = 1;
                }
            }

            if (histogramInfoValley[iv].index > kernel_half + 5) {
                int min = 0xFFFF;
                for (j = histogramInfoValley[iv].index - 5;
                     j >= MAX(histogramInfoValley[iv].index - extend, kernel_half); j--) {
                    min = min < _pInput[j] ? min : _pInput[j];
                }

                if (histogramInfoValley[iv].intensity > min) {
                    _pHillSideInfo[hillside_size].is_noisy_boundary = 1;
                }
            }

            // find half index
            int thres = (histogramInfoPeak[ip].intensity + histogramInfoValley[iv].intensity) * 0.5;
            for (l = histogramInfoValley[iv].index + 1; l < histogramInfoPeak[ip].index - 1; l++) {
                if ((_pInput[l] >= thres && _pInput[l + 1] < thres) ||
                    (_pInput[l] <= thres && _pInput[l + 1] > thres)) {
                    _pHillSideInfo[hillside_size].index = l;
                    break;
                }
            }
            _pHillSideInfo[hillside_size].width =
                histogramInfoPeak[ip].index - histogramInfoValley[iv].index;
            _pHillSideInfo[hillside_size].height =
                histogramInfoPeak[ip].intensity - histogramInfoValley[iv].intensity;
            _pHillSideInfo[hillside_size].direction = 1;
            _pHillSideInfo[hillside_size].pos_index = histogramInfoPeak[ip].index;
            _pHillSideInfo[hillside_size].pos_intensity = histogramInfoPeak[ip].intensity;
            _pHillSideInfo[hillside_size].neg_index = histogramInfoValley[iv].index;
            _pHillSideInfo[hillside_size].neg_intensity = histogramInfoValley[iv].intensity;
            _pHillSideInfo[hillside_size].mean = mean;
            iv++;
            hillside_size = hillside_size + 1;
        }
    }

#ifdef EGIS_DEBUG
    egislog_d("Egis_debug partial 449");
#endif

#ifdef DEBUG

    if (fopen_s(&file, "hist_log.txt", "a") == 0) {
        fprintf(file, "_pHillSideInfo result\n");
        for (i = 0; i < hillside_size; i++) {
            fprintf(file,
                    "_pHillSideInfo index %i width %f height %f %i pos %i %i neg %i %i noisy %i\n",
                    _pHillSideInfo[i].index, _pHillSideInfo[i].width, _pHillSideInfo[i].height,
                    _pHillSideInfo[i].direction, _pHillSideInfo[i].pos_index,
                    _pHillSideInfo[i].pos_intensity, _pHillSideInfo[i].neg_index,
                    _pHillSideInfo[i].neg_intensity, _pHillSideInfo[i].is_noisy_boundary);
        }
        fclose(file);
    }
#endif

    // printf("%i ", hillside_size);

    if (hillside_size == 1 && _pHillSideInfo[0].width >= (_input_size - _kernel_size - 5)) {
        *_pHillside_size = 0;
    } else {
        *_pHillside_size = hillside_size;
    }

#ifdef EGIS_DEBUG
    egislog_d("Egis_debug partial 450");
#endif

    return;
}

static void linear_curve_fit(int* input_x, int* input_y, int size, float* a, float* b) {
    int i, n = 0;
    float sumx = 0.0, sumy = 0.0, sumxy = 0.0, sumx2 = 0.0;
    for (i = 0; i < size; i++) {
        if (input_y[i] == NAN) {
            continue;
        }
        sumx = sumx + input_x[i];
        sumx2 = sumx2 + input_x[i] * input_x[i];
        sumy = sumy + input_y[i];
        sumxy = sumxy + input_x[i] * input_y[i];
        n++;
    }

    if ((n * sumx2 - sumx * sumx) == 0) {
        *a = 0;
        *b = 0;
    } else {
        *a = ((sumx2 * sumy - sumx * sumxy) * 1.0 / (n * sumx2 - sumx * sumx) * 1.0);
        *b = ((n * sumxy - sumx * sumy) * 1.0 / (n * sumx2 - sumx * sumx) * 1.0);
    }

#ifdef DEBUG
    FILE* file;
    if (fopen_s(&file, "hist_log.txt", "a") == 0) {
        fprintf(file, "The line is Y = %3.3f + %3.3f X\n", *a, *b);
        fclose(file);
    }
#endif
}

static void linear_curve_fit_thres(int* input_x, int* input_y, int size, int max, int min, float* a,
                                   float* b) {
    int i, n = 0;
    float sumx = 0.0, sumy = 0.0, sumxy = 0.0, sumx2 = 0.0;
    for (i = 0; i < size; i++) {
        if (input_y[i] == NAN) {
            continue;
        }
        if (input_y[i] > max || input_y[i] < min) {
            continue;
        }
        sumx = sumx + input_x[i];
        sumx2 = sumx2 + input_x[i] * input_x[i];
        sumy = sumy + input_y[i];
        sumxy = sumxy + input_x[i] * input_y[i];
        n++;
    }

    if ((n * sumx2 - sumx * sumx) == 0) {
        *a = 0;
        *b = 0;
    } else {
        *a = ((sumx2 * sumy - sumx * sumxy) * 1.0 / (n * sumx2 - sumx * sumx) * 1.0);
        *b = ((n * sumxy - sumx * sumy) * 1.0 / (n * sumx2 - sumx * sumx) * 1.0);
    }

#ifdef DEBUG
    FILE* file;
    if (fopen_s(&file, "hist_log.txt", "a") == 0) {
        fprintf(file, "The line is Y = %3.3f + %3.3f X\n", *a, *b);
        fclose(file);
    }
#endif
}

static int parse_curve_direction(int* _pInput, int _input_size, int _kernel_size,
                                 struct HillSideInfo* hillSideInfo, int* hillSideInfo_size,
                                 int _cut_size, int _cell_stride, int _cell_shift, int* pEdge) {
    int i, j, k, l;
    int kernel_half = _kernel_size >> 1;

#ifdef EGIS_DEBUG
    egislog_d("Egis_debug partial 451");
#endif

    // get max hight
    int max_height = 0;
    float max_slope = 0;
    int max_index = 0;
    int max_group = 0;
    int max_direction = 0;
    int max_pos = 0, max_neg = 0;
    int max_pos_intensity = 0, max_neg_intensity = 0;
    int max_mean = 0;

    // int max1_height = 0;
    // float max1_slope = 0;
    // int max1_index = 0;
    // int max1_group = 0;
    // int max1_direction = 0;
    // int max1_pos, max1_neg;

#ifdef DEBUG
    FILE* file;
#endif

    float slope;
    for (i = 0; i < _cut_size; i += 2) {
        for (j = 0; j < hillSideInfo_size[i]; j++) {
            if (hillSideInfo_size[i] > 1) {
                if (hillSideInfo[i * 50 + j].width > 100) {
                    continue;
                }
            }

            if (hillSideInfo[i * 50 + j].width == 0) {
                continue;
            }

            slope = (double)hillSideInfo[i * 50 + j].height / hillSideInfo[i * 50 + j].width;
            if (hillSideInfo[i * 50 + j].pos_index == _input_size - kernel_half ||
                hillSideInfo[i * 50 + j].neg_index == kernel_half) {
                slope = (double)slope / 1.5;
            }
            if (hillSideInfo[i * 50 + j].is_noisy_boundary) {
                slope = (double)slope / 2;
            }

            if (max_slope < slope && hillSideInfo[i * 50 + j].height > 800 &&
                hillSideInfo[i * 50 + j].width < 100) {
                max_slope = slope;
                max_height = hillSideInfo[i * 50 + j].height;
                max_index = hillSideInfo[i * 50 + j].index;
                max_direction = hillSideInfo[i * 50 + j].direction;
                max_group = i;
                max_pos = hillSideInfo[i * 50 + j].pos_index;
                max_neg = hillSideInfo[i * 50 + j].neg_index;
                max_pos_intensity = hillSideInfo[i * 50 + j].pos_intensity;
                max_neg_intensity = hillSideInfo[i * 50 + j].neg_intensity;
                max_mean = hillSideInfo[i * 50 + j].mean;

#ifdef DEBUG
                if (fopen_s(&file, "hist_log.txt", "a") == 0) {
                    fprintf(file,
                            "slope %f max_height %i index %i max_group %i max_direction %i %i %i\n",
                            slope, max_height, max_index, max_group, max_direction, max_pos,
                            max_neg);
                    fclose(file);
                }
#endif
            } else if (max_slope < slope && hillSideInfo[i * 50 + j].height > 3000) {
                max_slope = slope;
                max_height = hillSideInfo[i * 50 + j].height;
                max_index = hillSideInfo[i * 50 + j].index;
                max_direction = hillSideInfo[i * 50 + j].direction;
                max_group = i;
                max_pos = hillSideInfo[i * 50 + j].pos_index;
                max_neg = hillSideInfo[i * 50 + j].neg_index;
                max_pos_intensity = hillSideInfo[i * 50 + j].pos_intensity;
                max_neg_intensity = hillSideInfo[i * 50 + j].neg_intensity;
                max_mean = hillSideInfo[i * 50 + j].mean;

#ifdef DEBUG
                if (fopen_s(&file, "hist_log.txt", "a") == 0) {
                    fprintf(file,
                            "slope %f max_height %i index %i max_group %i max_direction %i %i %i\n",
                            slope, max_height, max_index, max_group, max_direction, max_pos,
                            max_neg);
                    fclose(file);
                }
#endif
            }
        }
    }

#ifdef EGIS_DEBUG
    egislog_d("Egis_debug partial 452");
#endif

    // if (max1_height >= max_height * 0.9)
    //{
    //	max_slope = max1_slope;
    //	max_height = max1_height;
    //	max_index = max1_index;
    //	max_direction = max1_direction;
    //	max_group = max1_group;
    //	max_pos = max1_pos;
    //	max_neg = max1_neg;
    //}

    if (max_height < 800) {
        for (i = 0; i < _cut_size; i++) {
            pEdge[i] = 0;
        }
        return 1;
    }

    int use_pn = 0;  // 0: no use, 1: use ps, -1: use ns
    if (max_pos == _input_size - kernel_half &&
        (double)ABS(max_neg_intensity - max_mean) / ABS(max_pos_intensity - max_neg_intensity) >
            0.8) {
        use_pn = -1;
    } else if (max_neg == kernel_half && (double)ABS(max_pos_intensity - max_mean) /
                                                 ABS(max_pos_intensity - max_neg_intensity) >
                                             0.8) {
        use_pn = 1;
    }

#ifdef EGIS_DEBUG
    egislog_d("Egis_debug partial 453");
#endif

    // search positive neighborhood
    int curve_p[50];
    int curve_p_index[50];
    int curve_p_direction[50];
    int curve_p_ps[50];
    int curve_p_ns[50];
    int curve_p_size = 0;
    int ps_index = max_index;
    int ns_index = max_index;

    // add max height
    curve_p[curve_p_size] = max_index;
    curve_p_index[curve_p_size] = max_group * _cell_stride + _cell_shift;
    curve_p_direction[curve_p_size] = max_direction;
    curve_p_ps[curve_p_size] = max_pos;
    curve_p_ns[curve_p_size] = max_neg;
    curve_p_size++;

    // int search = kernel_half;
    int search = 5;
    float pre_slope = 0.0;
    slope = 0.0;
    float slope_thres = 1.5;

#ifdef EGIS_DEBUG
    egislog_d("Egis_debug partial 454");
#endif

    for (i = max_group + 1; i < _cut_size; i++) {
        // positive search
        for (j = ps_index; j < _input_size - kernel_half; j++) {
            int local_peak = 1;
            for (l = j - search; l < j + search; l++) {
                if (l < kernel_half || l >= _input_size - kernel_half) {
                    continue;
                }

                if (_pInput[i * _input_size + j] < _pInput[i * _input_size + l]) {
                    local_peak = 0;
                    break;
                }
            }

            int local_valley = 1;
            for (l = j - search; l < j + search; l++) {
                if (l < kernel_half || l >= _input_size - kernel_half) {
                    continue;
                }

                if (_pInput[i * _input_size + j] > _pInput[i * _input_size + l]) {
                    local_valley = 0;
                    break;
                }
            }

            if (local_peak == 1 || local_valley == 1) {
                break;
            }
        }
        if (j > _input_size - kernel_half - 1) {
            j = _input_size - kernel_half - 1;
        }
        ps_index = j;

        // negative search
        for (k = ns_index; k >= kernel_half; k--) {
            int local_peak = 1;
            for (l = k - search; l < k + search; l++) {
                if (l < kernel_half || l >= _input_size - kernel_half) {
                    continue;
                }

                if (_pInput[i * _input_size + k] < _pInput[i * _input_size + l]) {
                    local_peak = 0;
                    break;
                }
            }

            int local_valley = 1;
            for (l = k - search; l < k + search; l++) {
                if (l < kernel_half || l >= _input_size - kernel_half) {
                    continue;
                }

                if (_pInput[i * _input_size + k] > _pInput[i * _input_size + l]) {
                    local_valley = 0;
                    break;
                }
            }

            if (local_peak == 1 || local_valley == 1) {
                break;
            }
        }
        if (k < kernel_half) {
            k = kernel_half;
        }
        ns_index = k;

        // find half index
        curve_p[curve_p_size] = 0;
        int thres =
            (_pInput[i * _input_size + ps_index] + _pInput[i * _input_size + ns_index]) >> 1;
        for (l = ns_index; l < ps_index - 1; l++) {
            if ((_pInput[i * _input_size + l] >= thres &&
                 _pInput[i * _input_size + l + 1] < thres) ||
                (_pInput[i * _input_size + l] <= thres &&
                 _pInput[i * _input_size + l + 1] > thres)) {
                curve_p[curve_p_size] = l;
                break;
            }
        }

        curve_p_ps[curve_p_size] = ps_index;
        curve_p_ns[curve_p_size] = ns_index;

        // get direction
        if (_pInput[i * _input_size + ps_index] >= _pInput[i * _input_size + ns_index]) {
            curve_p_direction[curve_p_size] = 1;
        } else {
            curve_p_direction[curve_p_size] = 0;
        }

        curve_p_index[curve_p_size] = i * _cell_stride + _cell_shift;
#ifdef DEBUG
        if (fopen_s(&file, "hist_log.txt", "a") == 0) {
            fprintf(file, "%i\t%i\t%i w %i h %i ps %i %i ns %i %i\n", i,
                    curve_p_index[curve_p_size], curve_p[curve_p_size], ps_index - ns_index,
                    ABS(_pInput[i * _input_size + ps_index] - _pInput[i * _input_size + ns_index]),
                    ps_index, _pInput[i * _input_size + ps_index], ns_index,
                    _pInput[i * _input_size + ns_index]);
            fclose(file);
        }
#endif

        if (curve_p_size > 0) {
            slope = (double)(curve_p[curve_p_size] - curve_p[curve_p_size - 1]) /
                    (curve_p_index[curve_p_size] - curve_p_index[curve_p_size - 1]);
        }

        if (i == max_group + 1 && ABS(_pInput[i * _input_size + ps_index] -
                                      _pInput[i * _input_size + ns_index]) < max_height * 0.5) {
#ifdef DEBUG
            if (fopen_s(&file, "hist_log.txt", "a") == 0) {
                fprintf(file,
                        "ABS(_pInput[i * _input_size + ps_index] - _pInput[i * _input_size + "
                        "ns_index]) < max_height * 0.5\n");
                fclose(file);
            }
#endif
            break;
        } else if (ps_index == ns_index)  // error
        {
#ifdef DEBUG
            if (fopen_s(&file, "hist_log.txt", "a") == 0) {
                fprintf(file, "ps_index == ns_index\n");
                fclose(file);
            }
#endif
            break;
        } else if (curve_p_size > 0 && ABS(curve_p[curve_p_size] - curve_p[curve_p_size - 1]) >
                                           20)  // new point is too far away
        {
#ifdef DEBUG
            if (fopen_s(&file, "hist_log.txt", "a") == 0) {
                fprintf(file, "ABS(curve_p[curve_p_size] - curve_p[curve_p_size - 1]) > 20\n");
                fclose(file);
            }
#endif
            continue;
        } else if (curve_p_size > 0 &&
                   curve_p_direction[curve_p_size] !=
                       curve_p_direction[curve_p_size - 1])  // new point's direction is opposite
        {
#ifdef DEBUG
            if (fopen_s(&file, "hist_log.txt", "a") == 0) {
                fprintf(file,
                        "curve_p_direction[curve_p_size] != curve_p_direction[curve_p_size - 1]\n");
                fclose(file);
            }
#endif
            break;
        } else if (ABS(_pInput[i * _input_size + ps_index] - _pInput[i * _input_size + ns_index]) <
                   500)  // hight too low, position error increase
        {
#ifdef DEBUG
            if (fopen_s(&file, "hist_log.txt", "a") == 0) {
                fprintf(file,
                        "ABS(_pInput[i * _input_size + ps_index] - _pInput[i * _input_size + "
                        "ns_index]) < 500\n");
                fclose(file);
            }
#endif
            continue;
        } else if (curve_p_size > 0 &&
                   ABS(curve_p[curve_p_size] - curve_p[curve_p_size - 1]) > 10 &&
                   (ABS(curve_p_ps[curve_p_size] - curve_p_ps[curve_p_size - 1]) > 20 ||
                    ABS(curve_p_ns[curve_p_size] - curve_p_ns[curve_p_size - 1]) > 20)) {
#ifdef DEBUG
            if (fopen_s(&file, "hist_log.txt", "a") == 0) {
                fprintf(file,
                        "ABS(curve_p_ps[curve_p_size] - curve_p_ps[curve_p_size - 1]) > 20 || "
                        "ABS(curve_p_ns[curve_p_size] - curve_p_ns[curve_p_size - 1]) > 20\n");
                fclose(file);
            }
#endif
            continue;
        } else if (curve_p_size > 1 && ABS(slope - pre_slope) > slope_thres) {
#ifdef DEBUG
            if (fopen_s(&file, "hist_log.txt", "a") == 0) {
                fprintf(file, "ABS(slope - pre_slope) = %f > slope_thres\n",
                        ABS(slope - pre_slope));
                fclose(file);
            }
#endif
            continue;
        }

        ps_index = curve_p[curve_p_size];
        ns_index = curve_p[curve_p_size];
        pre_slope = slope;
        curve_p_size++;

        if (curve_p_size >= 50) {
            break;
        } else if (curve_p_size > 0 &&
                   (curve_p[curve_p_size - 1] < kernel_half + 1 ||
                    curve_p[curve_p_size - 1] > _input_size - kernel_half - 1))  // get the boundary
        {
            break;
        } else if (curve_p_size > 0 &&
                   (i * _cell_stride + _cell_shift) - curve_p_index[curve_p_size - 1] >
                       (5 * _cell_stride + _cell_shift)) {
            break;
        }
    }

#ifdef EGIS_DEBUG
    egislog_d("Egis_debug partial 455");
#endif

    // search negative neighborhood
    int curve_n[50];
    int curve_n_index[50];
    int curve_n_direction[50];
    int curve_n_ps[50];
    int curve_n_ns[50];
    int curve_n_size = 0;
    ps_index = max_index;
    ns_index = max_index;

    slope = 0.0;
    pre_slope = 0.0;
    for (i = max_group - 1; i >= 0; i--) {
        // positive search
        for (j = ps_index; j < _input_size - kernel_half; j++) {
            int local_peak = 1;
            for (l = j - search; l < j + search; l++) {
                if (l < kernel_half || l >= _input_size - kernel_half) {
                    continue;
                }

                if (_pInput[i * _input_size + j] < _pInput[i * _input_size + l]) {
                    local_peak = 0;
                    break;
                }
            }

            int local_valley = 1;
            for (l = j - search; l < j + search; l++) {
                if (l < kernel_half || l >= _input_size - kernel_half) {
                    continue;
                }

                if (_pInput[i * _input_size + j] > _pInput[i * _input_size + l]) {
                    local_valley = 0;
                    break;
                }
            }

            if (local_peak == 1 || local_valley == 1) {
                break;
            }
        }
        if (j > _input_size - kernel_half - 1) {
            j = _input_size - kernel_half - 1;
        }
        ps_index = j;

        // negative search
        for (k = ns_index; k >= kernel_half; k--) {
            int local_peak = 1;
            for (l = k - search; l < k + search; l++) {
                if (l < kernel_half || l >= _input_size - kernel_half) {
                    continue;
                }

                if (_pInput[i * _input_size + k] < _pInput[i * _input_size + l]) {
                    local_peak = 0;
                    break;
                }
            }

            int local_valley = 1;
            for (l = k - search; l < k + search; l++) {
                if (l < kernel_half || l >= _input_size - kernel_half) {
                    continue;
                }

                if (_pInput[i * _input_size + k] > _pInput[i * _input_size + l]) {
                    local_valley = 0;
                    break;
                }
            }

            if (local_peak == 1 || local_valley == 1) {
                break;
            }
        }
        if (k < kernel_half) {
            k = kernel_half;
        }
        ns_index = k;

        // find half index
        curve_n[curve_n_size] = 0;
        int thres =
            (_pInput[i * _input_size + ps_index] + _pInput[i * _input_size + ns_index]) >> 1;
        for (l = ns_index; l < ps_index - 1; l++) {
            if ((_pInput[i * _input_size + l] >= thres &&
                 _pInput[i * _input_size + l + 1] < thres) ||
                (_pInput[i * _input_size + l] <= thres &&
                 _pInput[i * _input_size + l + 1] > thres)) {
                curve_n[curve_n_size] = l;
                break;
            }
        }

        curve_n_ps[curve_n_size] = ps_index;
        curve_n_ns[curve_n_size] = ns_index;

        // get direction
        if (_pInput[i * _input_size + ps_index] >= _pInput[i * _input_size + ns_index]) {
            curve_n_direction[curve_n_size] = 1;
        } else {
            curve_n_direction[curve_n_size] = 0;
        }

        curve_n_index[curve_n_size] = i * _cell_stride + _cell_shift;
#ifdef DEBUG

        if (fopen_s(&file, "hist_log.txt", "a") == 0) {
            fprintf(file, "%i\t%i\t%i w %i h %i ps %i %i ns %i %i\n", i,
                    curve_n_index[curve_n_size], curve_n[curve_n_size], ps_index - ns_index,
                    ABS(_pInput[i * _input_size + ps_index] - _pInput[i * _input_size + ns_index]),
                    ps_index, _pInput[i * _input_size + ps_index], ns_index,
                    _pInput[i * _input_size + ns_index]);
            fclose(file);
        }
#endif

        if (curve_n_size > 0) {
            slope = (double)(curve_n[curve_n_size] - curve_n[curve_n_size - 1]) /
                    (curve_n_index[curve_n_size] - curve_n_index[curve_n_size - 1]);
        }

        if (i == max_group - 1 && ABS(_pInput[i * _input_size + ps_index] -
                                      _pInput[i * _input_size + ns_index]) < max_height * 0.5) {
#ifdef DEBUG
            if (fopen_s(&file, "hist_log.txt", "a") == 0) {
                fprintf(file,
                        "ABS(_pInput[i * _input_size + ps_index] - _pInput[i * _input_size + "
                        "ns_index]) < max_height * 0.5\n");
                fclose(file);
            }
#endif
            break;
        } else if (ps_index == ns_index)  // error
        {
#ifdef DEBUG
            if (fopen_s(&file, "hist_log.txt", "a") == 0) {
                fprintf(file, "ps_index == ns_index\n");
                fclose(file);
            }
#endif
            break;
        } else if (curve_n_size > 0 && ABS(curve_n[curve_n_size] - curve_n[curve_n_size - 1]) >
                                           20)  // new point is too far away
        {
#ifdef DEBUG
            if (fopen_s(&file, "hist_log.txt", "a") == 0) {
                fprintf(file, "ABS(curve_n[curve_n_size] - curve_n[curve_n_size - 1]) > 20\n");
                fclose(file);
            }
#endif
            continue;
        } else if (curve_n_size > 0 &&
                   curve_n_direction[curve_n_size] !=
                       curve_n_direction[curve_n_size - 1])  // new point's direction is opposite
        {
#ifdef DEBUG
            if (fopen_s(&file, "hist_log.txt", "a") == 0) {
                fprintf(file,
                        "curve_n_direction[curve_n_size] != curve_n_direction[curve_n_size - 1]\n");
                fclose(file);
            }
#endif
            break;
        } else if (ABS(_pInput[i * _input_size + ps_index] - _pInput[i * _input_size + ns_index]) <
                   500)  // hight too low, position error increase
        {
#ifdef DEBUG
            if (fopen_s(&file, "hist_log.txt", "a") == 0) {
                fprintf(file,
                        "ABS(_pInput[i * _input_size + ps_index] - _pInput[i * _input_size + "
                        "ns_index]) < 500\n");
                fclose(file);
            }
#endif
            continue;
        } else if (curve_n_size > 0 &&
                   ABS(curve_n[curve_n_size] - curve_n[curve_n_size - 1]) > 10 &&
                   (ABS(curve_n_ps[curve_n_size] - curve_n_ps[curve_n_size - 1]) > 20 ||
                    ABS(curve_n_ns[curve_n_size] - curve_n_ns[curve_n_size - 1]) > 20)) {
#ifdef DEBUG
            if (fopen_s(&file, "hist_log.txt", "a") == 0) {
                fprintf(file,
                        "ABS(curve_n_ps[curve_n_size] - curve_n_ps[curve_n_size - 1]) > 20 || "
                        "ABS(curve_n_ns[curve_n_size] - curve_n_ns[curve_n_size - 1]) > 20\n");
                fclose(file);
            }
#endif
            continue;
        } else if (curve_n_size > 1 && ABS(slope - pre_slope) > slope_thres) {
#ifdef DEBUG
            if (fopen_s(&file, "hist_log.txt", "a") == 0) {
                fprintf(file, "ABS(slope - pre_slope) = %f > slope_thres\n",
                        ABS(slope - pre_slope));
                fclose(file);
            }
#endif
            continue;
        }

        ps_index = curve_n[curve_n_size];
        ns_index = curve_n[curve_n_size];
        pre_slope = slope;
        curve_n_size++;

        if (curve_n_size >= 50) {
            break;
        } else if (curve_n_size > 0 &&
                   (curve_n[curve_n_size - 1] < kernel_half + 1 ||
                    curve_n[curve_n_size - 1] > _input_size - kernel_half - 1))  // get the boundary
        {
            break;
        } else if (curve_n_size > 0 &&
                   curve_n_index[curve_n_size - 1] - (i * _cell_stride + _cell_shift) >
                       (5 * _cell_stride + _cell_shift)) {
            break;
        }
    }

#ifdef EGIS_DEBUG
    egislog_d("Egis_debug partial 456");
#endif

    int* curve = (int*)malloc((curve_p_size + curve_n_size) * sizeof(int));
    int* curve_index = (int*)malloc((curve_p_size + curve_n_size) * sizeof(int));
    int curve_size = 0;

    for (i = curve_n_size - 1; i >= 0; i--) {
        if (use_pn == 1) {
            curve[curve_size] = curve_n_ps[i];
        } else if (use_pn == -1) {
            curve[curve_size] = curve_n_ns[i];
        } else {
            curve[curve_size] = curve_n[i];
        }
        curve_index[curve_size] = curve_n_index[i];
        curve_size++;
    }
    for (i = 0; i < curve_p_size; i++) {
        if (use_pn == 1) {
            curve[curve_size] = curve_p_ps[i];
        } else if (use_pn == -1) {
            curve[curve_size] = curve_p_ns[i];
        } else {
            curve[curve_size] = curve_p[i];
        }
        curve_index[curve_size] = curve_p_index[i];
        curve_size++;
    }

#ifdef EGIS_DEBUG
    egislog_d("Egis_debug partial 457");
#endif

#ifdef DEBUG

    if (fopen_s(&file, "hist_log.txt", "a") == 0) {
        fprintf(file, "linear_curve_fit input\n");
        for (i = 0; i < curve_size; i++) {
            fprintf(file, "%i\t%i\n", curve_index[i], curve[i]);
        }
        fclose(file);
    }
#endif

    int half_size = curve_size >> 1;
    float a0, b0;
    linear_curve_fit(curve_index, curve, curve_size, &a0, &b0);
    float a1, b1;
    int finger_pn = 0;

    if (curve_size <= 4) {
        finger_pn = 1;
        a0 = 0;
        b0 = 0;

        for (i = 0; i < curve_size; i++) {
            curve[i] = 0;
        }
    } else if (curve_size <= 8) {
        if (curve[half_size] < (_cut_size * _cell_stride + _cell_shift * 2) * 0.5) {
            finger_pn = 1;
        } else {
            finger_pn = 0;
        }
    } else if (curve_size < _cut_size * 0.5 &&
               ((a0 < 30 && b0 < 0.1) || (a0 > _input_size - 30 && b0 > -0.1)))  // edge in boundary
    {
        if (a0 < 30) {
            finger_pn = 1;
        } else if (a0 > _input_size - 30) {
            finger_pn = 0;
        }
    } else {
        int quarter_cut0 = curve_size * 0.25;
        int quarter_cut2 = curve_size * 0.75;
        float ideal;
        float middle = 0;
        float side = 0;
        for (i = 0; i < curve_size; i++) {
            ideal = a0 + curve_index[i] * b0;
            if (i < quarter_cut0 || i >= quarter_cut2) {
                side += curve[i] - ideal;
            } else {
                middle += curve[i] - ideal;
            }
        }

#ifdef DEBUG

        if (fopen_s(&file, "hist_log.txt", "a") == 0) {
            fprintf(file, "middle %f side %f\n", middle, side);
            fclose(file);
        }
#endif

        if (ABS(middle) < 10) {
            if (curve[half_size] < (_cut_size * _cell_stride + _cell_shift * 2) * 0.5) {
                finger_pn = 1;
            } else {
                finger_pn = 0;
            }
        } else if (middle > side) {
            finger_pn = 0;
        } else {
            finger_pn = 1;
        }
    }
    a1 = a0;
    b1 = b0;

#ifdef EGIS_DEBUG
    egislog_d("Egis_debug partial 458");
#endif

#ifdef DEBUG

    if (fopen_s(&file, "hist_log.txt", "a") == 0) {
        fprintf(file, "finger_pn %i\n", finger_pn);
        fclose(file);
    }
#endif

    int index;
    for (i = 0; i < _cut_size; i++) {
        index = i * _cell_stride + _cell_shift;
        for (j = 0; j < curve_size; j++) {
            if (index == curve_index[j]) {
                pEdge[i] = curve[j];
                break;
            }
        }

        if (pEdge[i] == -1) {
            if (index < curve_index[half_size]) {
                pEdge[i] = a0 + index * b0;
            } else {
                pEdge[i] = a1 + index * b1;
            }
        }

#ifdef DEBUG
        FILE* file;
        if (fopen_s(&file, "hist_log.txt", "a") == 0) {
            fprintf(file, "%i\t%i\n", index, pEdge[i]);
            fclose(file);
        }
#endif
    }

#ifdef EGIS_DEBUG
    egislog_d("Egis_debug partial 459");
#endif

    FREE(curve);
    FREE(curve_index);
    return finger_pn;
}

static int parse_curve_direction_1(int* _pInput, int _input_size, int _kernel_size,
                                   struct HillSideInfo* hillSideInfo, int* hillSideInfo_size,
                                   int _cut_size, int _cell_stride, int _cell_shift, int is_hv,
                                   int _max_contrast_score, int _max_x, int _max_y, int* pEdge) {
    int i, j, k, l;
    int kernel_half = _kernel_size >> 1;

#ifdef EGIS_DEBUG
    egislog_d("Egis_debug partial 451");
#endif

    // get max hight
    int max_height = 0;
    float max_slope = 0;
    int max_index = 0;
    int max_group = 0;
    int max_direction = 0;
    int max_pos = 0, max_neg = 0;
    int max_pos_intensity = 0, max_neg_intensity = 0;
    int max_mean = 0;

    // int max1_height = 0;
    // float max1_slope = 0;
    // int max1_index = 0;
    // int max1_group = 0;
    // int max1_direction = 0;
    // int max1_pos, max1_neg;

#ifdef DEBUG
    FILE* file;
#endif

    float slope;
    for (i = 0; i < _cut_size; i += 2) {
        for (j = 0; j < hillSideInfo_size[i]; j++) {
            // printf("%i %i\n", i, j);

            if (hillSideInfo_size[i] > 1) {
                if (hillSideInfo[i * 50 + j].width > 100) {
                    continue;
                }
            }

            if (hillSideInfo[i * 50 + j].width == 0) {
                continue;
            }

            slope = (double)hillSideInfo[i * 50 + j].height / hillSideInfo[i * 50 + j].width;
            if (hillSideInfo[i * 50 + j].pos_index == _input_size - kernel_half ||
                hillSideInfo[i * 50 + j].neg_index == kernel_half) {
                slope = (double)slope / 1.5;
            }
            if (hillSideInfo[i * 50 + j].is_noisy_boundary) {
                slope = (double)slope / 2;
            }

            if (max_slope < slope && hillSideInfo[i * 50 + j].height > 800 &&
                hillSideInfo[i * 50 + j].width < 100) {
                max_slope = slope;
                max_height = hillSideInfo[i * 50 + j].height;
                max_index = hillSideInfo[i * 50 + j].index;
                max_direction = hillSideInfo[i * 50 + j].direction;
                max_group = i;
                max_pos = hillSideInfo[i * 50 + j].pos_index;
                max_neg = hillSideInfo[i * 50 + j].neg_index;
                max_pos_intensity = hillSideInfo[i * 50 + j].pos_intensity;
                max_neg_intensity = hillSideInfo[i * 50 + j].neg_intensity;
                max_mean = hillSideInfo[i * 50 + j].mean;

#ifdef DEBUG
                if (fopen_s(&file, "hist_log.txt", "a") == 0) {
                    fprintf(file,
                            "slope %f max_height %i index %i max_group %i max_direction %i %i %i\n",
                            slope, max_height, max_index, max_group, max_direction, max_pos,
                            max_neg);
                    fclose(file);
                }
#endif
            } else if (max_slope < slope && hillSideInfo[i * 50 + j].height > 3000) {
                max_slope = slope;
                max_height = hillSideInfo[i * 50 + j].height;
                max_index = hillSideInfo[i * 50 + j].index;
                max_direction = hillSideInfo[i * 50 + j].direction;
                max_group = i;
                max_pos = hillSideInfo[i * 50 + j].pos_index;
                max_neg = hillSideInfo[i * 50 + j].neg_index;
                max_pos_intensity = hillSideInfo[i * 50 + j].pos_intensity;
                max_neg_intensity = hillSideInfo[i * 50 + j].neg_intensity;
                max_mean = hillSideInfo[i * 50 + j].mean;

#ifdef DEBUG
                if (fopen_s(&file, "hist_log.txt", "a") == 0) {
                    fprintf(file,
                            "slope %f max_height %i index %i max_group %i max_direction %i %i %i\n",
                            slope, max_height, max_index, max_group, max_direction, max_pos,
                            max_neg);
                    fclose(file);
                }
#endif
            }
        }
    }

#ifdef EGIS_DEBUG
    egislog_d("Egis_debug partial 452");
#endif

    // if (max1_height >= max_height * 0.9)
    //{
    //	max_slope = max1_slope;
    //	max_height = max1_height;
    //	max_index = max1_index;
    //	max_direction = max1_direction;
    //	max_group = max1_group;
    //	max_pos = max1_pos;
    //	max_neg = max1_neg;
    //}

    if (max_height < 800) {
        for (i = 0; i < _cut_size; i++) {
            pEdge[i] = 0;
        }
        return 1;
    }

    int use_pn = 0;  // 0: no use, 1: use ps, -1: use ns
    if (max_pos == _input_size - kernel_half &&
        (double)ABS(max_neg_intensity - max_mean) / ABS(max_pos_intensity - max_neg_intensity) >
            0.8) {
        use_pn = -1;
    } else if (max_neg == kernel_half && (double)ABS(max_pos_intensity - max_mean) /
                                                 ABS(max_pos_intensity - max_neg_intensity) >
                                             0.8) {
        use_pn = 1;
    }

#ifdef EGIS_DEBUG
    egislog_d("Egis_debug partial 453");
#endif

    // search positive neighborhood
    int curve_p[50];
    int curve_p_index[50];
    int curve_p_direction[50];
    int curve_p_ps[50];
    int curve_p_ns[50];
    int curve_p_size = 0;
    int ps_index = max_index;
    int ns_index = max_index;

    // add max height
    curve_p[curve_p_size] = max_index;
    curve_p_index[curve_p_size] = max_group * _cell_stride + _cell_shift;
    curve_p_direction[curve_p_size] = max_direction;
    curve_p_ps[curve_p_size] = max_pos;
    curve_p_ns[curve_p_size] = max_neg;
    curve_p_size++;

    // int search = kernel_half;
    int search = 5;
    float pre_slope = 0.0;
    slope = 0.0;
    float slope_thres = 1.5;

#ifdef EGIS_DEBUG
    egislog_d("Egis_debug partial 454");
#endif

    for (i = max_group + 1; i < _cut_size; i++) {
        // positive search
        for (j = ps_index; j < _input_size - kernel_half; j++) {
            int local_peak = 1;
            for (l = j - search; l < j + search; l++) {
                if (l < kernel_half || l >= _input_size - kernel_half) {
                    continue;
                }

                if (_pInput[i * _input_size + j] < _pInput[i * _input_size + l]) {
                    local_peak = 0;
                    break;
                }
            }

            int local_valley = 1;
            for (l = j - search; l < j + search; l++) {
                if (l < kernel_half || l >= _input_size - kernel_half) {
                    continue;
                }

                if (_pInput[i * _input_size + j] > _pInput[i * _input_size + l]) {
                    local_valley = 0;
                    break;
                }
            }

            if (local_peak == 1 || local_valley == 1) {
                break;
            }
        }
        if (j > _input_size - kernel_half - 1) {
            j = _input_size - kernel_half - 1;
        }
        ps_index = j;

        // negative search
        for (k = ns_index; k >= kernel_half; k--) {
            int local_peak = 1;
            for (l = k - search; l < k + search; l++) {
                if (l < kernel_half || l >= _input_size - kernel_half) {
                    continue;
                }

                if (_pInput[i * _input_size + k] < _pInput[i * _input_size + l]) {
                    local_peak = 0;
                    break;
                }
            }

            int local_valley = 1;
            for (l = k - search; l < k + search; l++) {
                if (l < kernel_half || l >= _input_size - kernel_half) {
                    continue;
                }

                if (_pInput[i * _input_size + k] > _pInput[i * _input_size + l]) {
                    local_valley = 0;
                    break;
                }
            }

            if (local_peak == 1 || local_valley == 1) {
                break;
            }
        }
        if (k < kernel_half) {
            k = kernel_half;
        }
        ns_index = k;

        // find half index
        curve_p[curve_p_size] = 0;
        int thres =
            (_pInput[i * _input_size + ps_index] + _pInput[i * _input_size + ns_index]) >> 1;
        for (l = ns_index; l < ps_index - 1; l++) {
            if ((_pInput[i * _input_size + l] >= thres &&
                 _pInput[i * _input_size + l + 1] < thres) ||
                (_pInput[i * _input_size + l] <= thres &&
                 _pInput[i * _input_size + l + 1] > thres)) {
                curve_p[curve_p_size] = l;
                break;
            }
        }

        curve_p_ps[curve_p_size] = ps_index;
        curve_p_ns[curve_p_size] = ns_index;

        // get direction
        if (_pInput[i * _input_size + ps_index] >= _pInput[i * _input_size + ns_index]) {
            curve_p_direction[curve_p_size] = 1;
        } else {
            curve_p_direction[curve_p_size] = 0;
        }

        curve_p_index[curve_p_size] = i * _cell_stride + _cell_shift;
#ifdef DEBUG
        if (fopen_s(&file, "hist_log.txt", "a") == 0) {
            fprintf(file, "%i\t%i\t%i w %i h %i ps %i %i ns %i %i\n", i,
                    curve_p_index[curve_p_size], curve_p[curve_p_size], ps_index - ns_index,
                    ABS(_pInput[i * _input_size + ps_index] - _pInput[i * _input_size + ns_index]),
                    ps_index, _pInput[i * _input_size + ps_index], ns_index,
                    _pInput[i * _input_size + ns_index]);
            fclose(file);
        }
#endif

        if (curve_p_size > 0) {
            slope = (double)(curve_p[curve_p_size] - curve_p[curve_p_size - 1]) /
                    (curve_p_index[curve_p_size] - curve_p_index[curve_p_size - 1]);
        }

        if (i == max_group + 1 && ABS(_pInput[i * _input_size + ps_index] -
                                      _pInput[i * _input_size + ns_index]) < max_height * 0.5) {
#ifdef DEBUG
            if (fopen_s(&file, "hist_log.txt", "a") == 0) {
                fprintf(file,
                        "ABS(_pInput[i * _input_size + ps_index] - _pInput[i * _input_size + "
                        "ns_index]) < max_height * 0.5\n");
                fclose(file);
            }
#endif
            break;
        } else if (ps_index == ns_index)  // error
        {
#ifdef DEBUG
            if (fopen_s(&file, "hist_log.txt", "a") == 0) {
                fprintf(file, "ps_index == ns_index\n");
                fclose(file);
            }
#endif
            break;
        } else if (curve_p_size > 0 && ABS(curve_p[curve_p_size] - curve_p[curve_p_size - 1]) >
                                           20)  // new point is too far away
        {
#ifdef DEBUG
            if (fopen_s(&file, "hist_log.txt", "a") == 0) {
                fprintf(file, "ABS(curve_p[curve_p_size] - curve_p[curve_p_size - 1]) > 20\n");
                fclose(file);
            }
#endif
            continue;
        } else if (curve_p_size > 0 &&
                   curve_p_direction[curve_p_size] !=
                       curve_p_direction[curve_p_size - 1])  // new point's direction is opposite
        {
#ifdef DEBUG
            if (fopen_s(&file, "hist_log.txt", "a") == 0) {
                fprintf(file,
                        "curve_p_direction[curve_p_size] != curve_p_direction[curve_p_size - 1]\n");
                fclose(file);
            }
#endif
            break;
        } else if (ABS(_pInput[i * _input_size + ps_index] - _pInput[i * _input_size + ns_index]) <
                   500)  // hight too low, position error increase
        {
#ifdef DEBUG
            if (fopen_s(&file, "hist_log.txt", "a") == 0) {
                fprintf(file,
                        "ABS(_pInput[i * _input_size + ps_index] - _pInput[i * _input_size + "
                        "ns_index]) < 500\n");
                fclose(file);
            }
#endif
            continue;
        } else if (curve_p_size > 0 &&
                   ABS(curve_p[curve_p_size] - curve_p[curve_p_size - 1]) > 10 &&
                   (ABS(curve_p_ps[curve_p_size] - curve_p_ps[curve_p_size - 1]) > 20 ||
                    ABS(curve_p_ns[curve_p_size] - curve_p_ns[curve_p_size - 1]) > 20)) {
#ifdef DEBUG
            if (fopen_s(&file, "hist_log.txt", "a") == 0) {
                fprintf(file,
                        "ABS(curve_p_ps[curve_p_size] - curve_p_ps[curve_p_size - 1]) > 20 || "
                        "ABS(curve_p_ns[curve_p_size] - curve_p_ns[curve_p_size - 1]) > 20\n");
                fclose(file);
            }
#endif
            continue;
        } else if (curve_p_size > 1 && ABS(slope - pre_slope) > slope_thres) {
#ifdef DEBUG
            if (fopen_s(&file, "hist_log.txt", "a") == 0) {
                fprintf(file, "ABS(slope - pre_slope) = %f > slope_thres\n",
                        ABS(slope - pre_slope));
                fclose(file);
            }
#endif
            continue;
        }

        ps_index = curve_p[curve_p_size];
        ns_index = curve_p[curve_p_size];
        pre_slope = slope;
        curve_p_size++;

        if (curve_p_size >= 50) {
            break;
        } else if (curve_p_size > 0 &&
                   (curve_p[curve_p_size - 1] < kernel_half + 1 ||
                    curve_p[curve_p_size - 1] > _input_size - kernel_half - 1))  // get the boundary
        {
            break;
        } else if (curve_p_size > 0 &&
                   (i * _cell_stride + _cell_shift) - curve_p_index[curve_p_size - 1] >
                       (5 * _cell_stride + _cell_shift)) {
            break;
        }
    }

#ifdef EGIS_DEBUG
    egislog_d("Egis_debug partial 455");
#endif

    // search negative neighborhood
    int curve_n[50];
    int curve_n_index[50];
    int curve_n_direction[50];
    int curve_n_ps[50];
    int curve_n_ns[50];
    int curve_n_size = 0;
    ps_index = max_index;
    ns_index = max_index;

    slope = 0.0;
    pre_slope = 0.0;
    for (i = max_group - 1; i >= 0; i--) {
        // positive search
        for (j = ps_index; j < _input_size - kernel_half; j++) {
            int local_peak = 1;
            for (l = j - search; l < j + search; l++) {
                if (l < kernel_half || l >= _input_size - kernel_half) {
                    continue;
                }

                if (_pInput[i * _input_size + j] < _pInput[i * _input_size + l]) {
                    local_peak = 0;
                    break;
                }
            }

            int local_valley = 1;
            for (l = j - search; l < j + search; l++) {
                if (l < kernel_half || l >= _input_size - kernel_half) {
                    continue;
                }

                if (_pInput[i * _input_size + j] > _pInput[i * _input_size + l]) {
                    local_valley = 0;
                    break;
                }
            }

            if (local_peak == 1 || local_valley == 1) {
                break;
            }
        }
        if (j > _input_size - kernel_half - 1) {
            j = _input_size - kernel_half - 1;
        }
        ps_index = j;

        // negative search
        for (k = ns_index; k >= kernel_half; k--) {
            int local_peak = 1;
            for (l = k - search; l < k + search; l++) {
                if (l < kernel_half || l >= _input_size - kernel_half) {
                    continue;
                }

                if (_pInput[i * _input_size + k] < _pInput[i * _input_size + l]) {
                    local_peak = 0;
                    break;
                }
            }

            int local_valley = 1;
            for (l = k - search; l < k + search; l++) {
                if (l < kernel_half || l >= _input_size - kernel_half) {
                    continue;
                }

                if (_pInput[i * _input_size + k] > _pInput[i * _input_size + l]) {
                    local_valley = 0;
                    break;
                }
            }

            if (local_peak == 1 || local_valley == 1) {
                break;
            }
        }
        if (k < kernel_half) {
            k = kernel_half;
        }
        ns_index = k;

        // find half index
        curve_n[curve_n_size] = 0;
        int thres =
            (_pInput[i * _input_size + ps_index] + _pInput[i * _input_size + ns_index]) >> 1;
        for (l = ns_index; l < ps_index - 1; l++) {
            if ((_pInput[i * _input_size + l] >= thres &&
                 _pInput[i * _input_size + l + 1] < thres) ||
                (_pInput[i * _input_size + l] <= thres &&
                 _pInput[i * _input_size + l + 1] > thres)) {
                curve_n[curve_n_size] = l;
                break;
            }
        }

        curve_n_ps[curve_n_size] = ps_index;
        curve_n_ns[curve_n_size] = ns_index;

        // get direction
        if (_pInput[i * _input_size + ps_index] >= _pInput[i * _input_size + ns_index]) {
            curve_n_direction[curve_n_size] = 1;
        } else {
            curve_n_direction[curve_n_size] = 0;
        }

        curve_n_index[curve_n_size] = i * _cell_stride + _cell_shift;
#ifdef DEBUG

        if (fopen_s(&file, "hist_log.txt", "a") == 0) {
            fprintf(file, "%i\t%i\t%i w %i h %i ps %i %i ns %i %i\n", i,
                    curve_n_index[curve_n_size], curve_n[curve_n_size], ps_index - ns_index,
                    ABS(_pInput[i * _input_size + ps_index] - _pInput[i * _input_size + ns_index]),
                    ps_index, _pInput[i * _input_size + ps_index], ns_index,
                    _pInput[i * _input_size + ns_index]);
            fclose(file);
        }
#endif

        if (curve_n_size > 0) {
            slope = (double)(curve_n[curve_n_size] - curve_n[curve_n_size - 1]) /
                    (curve_n_index[curve_n_size] - curve_n_index[curve_n_size - 1]);
        }

        if (i == max_group - 1 && ABS(_pInput[i * _input_size + ps_index] -
                                      _pInput[i * _input_size + ns_index]) < max_height * 0.5) {
#ifdef DEBUG
            if (fopen_s(&file, "hist_log.txt", "a") == 0) {
                fprintf(file,
                        "ABS(_pInput[i * _input_size + ps_index] - _pInput[i * _input_size + "
                        "ns_index]) < max_height * 0.5\n");
                fclose(file);
            }
#endif
            break;
        } else if (ps_index == ns_index)  // error
        {
#ifdef DEBUG
            if (fopen_s(&file, "hist_log.txt", "a") == 0) {
                fprintf(file, "ps_index == ns_index\n");
                fclose(file);
            }
#endif
            break;
        } else if (curve_n_size > 0 && ABS(curve_n[curve_n_size] - curve_n[curve_n_size - 1]) >
                                           20)  // new point is too far away
        {
#ifdef DEBUG
            if (fopen_s(&file, "hist_log.txt", "a") == 0) {
                fprintf(file, "ABS(curve_n[curve_n_size] - curve_n[curve_n_size - 1]) > 20\n");
                fclose(file);
            }
#endif
            continue;
        } else if (curve_n_size > 0 &&
                   curve_n_direction[curve_n_size] !=
                       curve_n_direction[curve_n_size - 1])  // new point's direction is opposite
        {
#ifdef DEBUG
            if (fopen_s(&file, "hist_log.txt", "a") == 0) {
                fprintf(file,
                        "curve_n_direction[curve_n_size] != curve_n_direction[curve_n_size - 1]\n");
                fclose(file);
            }
#endif
            break;
        } else if (ABS(_pInput[i * _input_size + ps_index] - _pInput[i * _input_size + ns_index]) <
                   500)  // hight too low, position error increase
        {
#ifdef DEBUG
            if (fopen_s(&file, "hist_log.txt", "a") == 0) {
                fprintf(file,
                        "ABS(_pInput[i * _input_size + ps_index] - _pInput[i * _input_size + "
                        "ns_index]) < 500\n");
                fclose(file);
            }
#endif
            continue;
        } else if (curve_n_size > 0 &&
                   ABS(curve_n[curve_n_size] - curve_n[curve_n_size - 1]) > 10 &&
                   (ABS(curve_n_ps[curve_n_size] - curve_n_ps[curve_n_size - 1]) > 20 ||
                    ABS(curve_n_ns[curve_n_size] - curve_n_ns[curve_n_size - 1]) > 20)) {
#ifdef DEBUG
            if (fopen_s(&file, "hist_log.txt", "a") == 0) {
                fprintf(file,
                        "ABS(curve_n_ps[curve_n_size] - curve_n_ps[curve_n_size - 1]) > 20 || "
                        "ABS(curve_n_ns[curve_n_size] - curve_n_ns[curve_n_size - 1]) > 20\n");
                fclose(file);
            }
#endif
            continue;
        } else if (curve_n_size > 1 && ABS(slope - pre_slope) > slope_thres) {
#ifdef DEBUG
            if (fopen_s(&file, "hist_log.txt", "a") == 0) {
                fprintf(file, "ABS(slope - pre_slope) = %f > slope_thres\n",
                        ABS(slope - pre_slope));
                fclose(file);
            }
#endif
            continue;
        }

        ps_index = curve_n[curve_n_size];
        ns_index = curve_n[curve_n_size];
        pre_slope = slope;
        curve_n_size++;

        if (curve_n_size >= 50) {
            break;
        } else if (curve_n_size > 0 &&
                   (curve_n[curve_n_size - 1] < kernel_half + 1 ||
                    curve_n[curve_n_size - 1] > _input_size - kernel_half - 1))  // get the boundary
        {
            break;
        } else if (curve_n_size > 0 &&
                   curve_n_index[curve_n_size - 1] - (i * _cell_stride + _cell_shift) >
                       (5 * _cell_stride + _cell_shift)) {
            break;
        }
    }

#ifdef EGIS_DEBUG
    egislog_d("Egis_debug partial 456");
#endif

    int* curve = (int*)malloc((curve_p_size + curve_n_size) * sizeof(int));
    int* curve_index = (int*)malloc((curve_p_size + curve_n_size) * sizeof(int));
    int curve_size = 0;

    for (i = curve_n_size - 1; i >= 0; i--) {
        if (use_pn == 1) {
            curve[curve_size] = curve_n_ps[i];
        } else if (use_pn == -1) {
            curve[curve_size] = curve_n_ns[i];
        } else {
            curve[curve_size] = curve_n[i];
        }
        curve_index[curve_size] = curve_n_index[i];
        curve_size++;
    }
    for (i = 0; i < curve_p_size; i++) {
        if (use_pn == 1) {
            curve[curve_size] = curve_p_ps[i];
        } else if (use_pn == -1) {
            curve[curve_size] = curve_p_ns[i];
        } else {
            curve[curve_size] = curve_p[i];
        }
        curve_index[curve_size] = curve_p_index[i];
        curve_size++;
    }

#ifdef EGIS_DEBUG
    egislog_d("Egis_debug partial 457");
#endif

#ifdef DEBUG

    if (fopen_s(&file, "hist_log.txt", "a") == 0) {
        fprintf(file, "linear_curve_fit input\n");
        for (i = 0; i < curve_size; i++) {
            fprintf(file, "%i\t%i\n", curve_index[i], curve[i]);
        }
        fclose(file);
    }
#endif

    int half_size = curve_size >> 1;
    float a0, b0;
    linear_curve_fit(curve_index, curve, curve_size, &a0, &b0);
    float a1, b1;
    int finger_pn = 0;

    if (curve_size <= 4)  // fail to detect finger boundary
    {
        finger_pn = 1;
        a0 = 0;
        b0 = 0;

        for (i = 0; i < curve_size; i++) {
            curve[i] = 0;
        }
    } else if (curve_size <= 8) {
        if (_max_contrast_score >= 10) {
            if (is_hv == 0 && a0 + _max_x * b0 >= _max_y) {
                finger_pn = 0;
            } else if (is_hv == 1 && a0 + _max_y * b0 >= _max_x) {
                finger_pn = 0;
            } else {
                finger_pn = 1;  // highrt is finger
            }
        } else {
            if (curve[half_size] < (_cut_size * _cell_stride + _cell_shift * 2) * 0.5) {
                finger_pn = 1;
            } else {
                finger_pn = 0;
            }
        }
    } else if (curve_size < _cut_size * 0.5 &&
               ((a0 < 30 && b0 < 0.1) || (a0 > _input_size - 30 && b0 > -0.1)))  // edge in boundary
    {
        if (_max_contrast_score >= 10) {
            if (is_hv == 0 && a0 + _max_x * b0 >= _max_y) {
                finger_pn = 0;
            } else if (is_hv == 1 && a0 + _max_y * b0 >= _max_x) {
                finger_pn = 0;
            } else {
                finger_pn = 1;  // highrt is finger
            }
        } else {
            if (a0 < 30) {
                finger_pn = 1;
            } else if (a0 > _input_size - 30) {
                finger_pn = 0;
            }
        }
    } else {
        int quarter_cut0 = curve_size * 0.25;
        int quarter_cut2 = curve_size * 0.75;
        float ideal;
        float middle = 0;
        float side = 0;
        for (i = 0; i < curve_size; i++) {
            ideal = a0 + curve_index[i] * b0;
            if (i < quarter_cut0 || i >= quarter_cut2) {
                side += curve[i] - ideal;
            } else {
                middle += curve[i] - ideal;
            }
        }

#ifdef DEBUG

        if (fopen_s(&file, "hist_log.txt", "a") == 0) {
            fprintf(file, "middle %f side %f\n", middle, side);
            fclose(file);
        }
#endif

        if (_max_contrast_score >= 10) {
            if (is_hv == 0 && a0 + _max_x * b0 >= _max_y) {
                finger_pn = 0;
            } else if (is_hv == 1 && a0 + _max_y * b0 >= _max_x) {
                finger_pn = 0;
            } else {
                finger_pn = 1;  // highrt is finger
            }
        } else {
            if (ABS(middle) < 10) {
                if (curve[half_size] < (_cut_size * _cell_stride + _cell_shift * 2) * 0.5) {
                    finger_pn = 1;
                } else {
                    finger_pn = 0;
                }
            } else if (middle > side) {
                finger_pn = 0;
            } else {
                finger_pn = 1;
            }
        }
    }
    a1 = a0;
    b1 = b0;

#ifdef EGIS_DEBUG
    egislog_d("Egis_debug partial 458");
#endif

#ifdef DEBUG

    if (fopen_s(&file, "hist_log.txt", "a") == 0) {
        fprintf(file, "finger_pn %i\n", finger_pn);
        fclose(file);
    }
#endif

    int index;
    for (i = 0; i < _cut_size; i++) {
        index = i * _cell_stride + _cell_shift;
        for (j = 0; j < curve_size; j++) {
            if (index == curve_index[j]) {
                pEdge[i] = curve[j];
                break;
            }
        }

        if (pEdge[i] == -1) {
            if (index < curve_index[half_size]) {
                pEdge[i] = a0 + index * b0;
            } else {
                pEdge[i] = a1 + index * b1;
            }
        }

#ifdef DEBUG
        FILE* file;
        if (fopen_s(&file, "hist_log.txt", "a") == 0) {
            fprintf(file, "%i\t%i\n", index, pEdge[i]);
            fclose(file);
        }
#endif
    }

#ifdef EGIS_DEBUG
    egislog_d("Egis_debug partial 459");
#endif

    FREE(curve);
    FREE(curve_index);
    return finger_pn;
}

static int determine_hv(int* _pImg, int _width, int _height, int _kernel_size, float* a, float* b,
                        int* score) {
    int i, j;

#ifdef EGIS_DEBUG
    egislog_d("Egis_debug partial 432");
#endif

    // cross two vertial
    float* pProfile_h_avg = (float*)malloc(sizeof(float) * _width);
    float* pProfile_v_avg = (float*)malloc(sizeof(float) * _height);
    memset(pProfile_h_avg, 0, sizeof(float) * _width);
    memset(pProfile_v_avg, 0, sizeof(float) * _height);
    for (i = 0; i < _height; i++) {
        for (j = 0; j < _width; j++) {
            pProfile_h_avg[j] += (double)_pImg[i * _width + j] / _height;
            pProfile_v_avg[i] += (double)_pImg[i * _width + j] / _width;
        }
    }

#ifdef EGIS_DEBUG
    egislog_d("Egis_debug partial 433");
#endif

    // smooth profile
    float* kernel = malloc(_kernel_size * sizeof(float));
    for (i = 0; i < _kernel_size; i++) {
        kernel[i] = (double)1 / _kernel_size;
    }

#ifdef EGIS_DEBUG
    egislog_d("Egis_debug partial 434");
#endif

    int kernel_half = _kernel_size >> 1;

    int* pProfile_h_avg_smooth = malloc((_width - kernel_half * 2) * sizeof(int));
    int src_pos = 0;
    float convolve;
    int mean_h = 0;
    int mean_v = 0;

    for (i = kernel_half; i < _width - kernel_half; ++i) {
        convolve = 0.0;
        int conv_pos = src_pos;
        for (j = 0; j < _kernel_size; j++) {
            convolve += pProfile_h_avg[conv_pos] * kernel[j];
            ++conv_pos;
        }

        pProfile_h_avg_smooth[src_pos] = convolve;
        mean_h += convolve;
        ++src_pos;
    }
    mean_h = mean_h / src_pos;

#ifdef EGIS_DEBUG
    egislog_d("Egis_debug partial 435");
#endif

    int* pProfile_v_avg_smooth = malloc((_height - kernel_half * 2) * sizeof(int));
    src_pos = 0;
    for (i = kernel_half; i < _height - kernel_half; ++i) {
        convolve = 0.0;
        int conv_pos = src_pos;
        for (j = 0; j < _kernel_size; j++) {
            convolve += pProfile_v_avg[conv_pos] * kernel[j];
            ++conv_pos;
        }

        pProfile_v_avg_smooth[src_pos] = convolve;
        mean_v += convolve;
        ++src_pos;
    }
    mean_v = mean_v / src_pos;

#ifdef EGIS_DEBUG
    egislog_d("Egis_debug partial 436");
#endif

    FREE(kernel);
    FREE(pProfile_h_avg);
    FREE(pProfile_v_avg);

    // get stdev
    int std_h = 0;
    int std_v = 0;
    for (i = 0; i < _width - kernel_half * 2; ++i) {
        std_h += (pProfile_h_avg_smooth[i] - mean_h) * (pProfile_h_avg_smooth[i] - mean_h);
    }
    std_h = int_sqrt(std_h / (_width - kernel_half * 2));

#ifdef EGIS_DEBUG
    egislog_d("Egis_debug partial 437");
#endif

    for (i = 0; i < _height - kernel_half * 2; ++i) {
        std_v += (pProfile_v_avg_smooth[i] - mean_v) * (pProfile_v_avg_smooth[i] - mean_v);
    }
    std_v = int_sqrt(std_v / (_height - kernel_half * 2));

#ifdef EGIS_DEBUG
    egislog_d("Egis_debug partial 438");
#endif

    int* index =
        (int*)malloc(sizeof(float) * MAX(_width - kernel_half * 2, _height - kernel_half * 2));
    for (i = 0; i < MAX(_width - kernel_half * 2, _height - kernel_half * 2); i++) {
        index[i] = i + kernel_half;
    }

#ifdef EGIS_DEBUG
    egislog_d("Egis_debug partial 439");
#endif

#ifdef DEBUG
    FILE* file;
    if (fopen_s(&file, "hist.csv", "a") == 0) {
        int need_comma = 0;
        for (i = 0; i < MAX(_width - kernel_half * 2, _height - kernel_half * 2); ++i) {
            if (need_comma)
                fprintf(file, ", ");
            else
                need_comma = 1;
            fprintf(file, "%i", index[i]);
        }
        fprintf(file, "\n");

        need_comma = 0;
        for (i = 0; i < _width - kernel_half * 2; ++i) {
            if (need_comma)
                fprintf(file, ", ");
            else
                need_comma = 1;
            fprintf(file, "%i", pProfile_h_avg_smooth[i]);
        }
        fprintf(file, "\n");

        need_comma = 0;
        for (i = 0; i < _height - kernel_half * 2; ++i) {
            if (need_comma)
                fprintf(file, ", ");
            else
                need_comma = 1;
            fprintf(file, "%i", pProfile_v_avg_smooth[i]);
        }
        fprintf(file, "\n");
        fclose(file);
    }
#endif

    // use R2 error
    float ideal = 0;
    float a0 = 0.0, b0 = 0.0;
    float a0_ = 0.0, b0_ = 0.0;
    float a1 = 0.0, b1 = 0.0;
    float a1_ = 0.0, b1_ = 0.0;
    float diff_h = 0;
    float diff_h_ = 0;
    float diff_v = 0;
    float diff_v_ = 0;
    float diff_h_max = 0;
    float diff_v_max = 0;
    if (_height > _width) {
#ifdef EGIS_DEBUG
        egislog_d("Egis_debug partial 531");
#endif
        // linear_curve_fit(index, pProfile_h_avg_smooth, _width - kernel_half * 2, &a0, &b0);
        linear_curve_fit_thres(index, pProfile_h_avg_smooth, _width - kernel_half * 2,
                               mean_h + std_h, mean_h - std_h, &a0, &b0);
        for (i = 0; i < _width - kernel_half * 2; i++) {
            ideal = a0 + index[i] * b0;
            diff_h += (ideal - pProfile_h_avg_smooth[i]) * (ideal - pProfile_h_avg_smooth[i]);
            diff_h_max = diff_h_max > (ideal - pProfile_h_avg_smooth[i])
                             ? diff_h_max
                             : (ideal - pProfile_h_avg_smooth[i]);
        }
        diff_h = int_sqrt(diff_h / (_width - kernel_half * 2));

        // linear_curve_fit(index, pProfile_v_avg_smooth, _width - kernel_half * 2, &a1, &b1);
        linear_curve_fit_thres(index, pProfile_v_avg_smooth, _width - kernel_half * 2,
                               mean_v + std_v, mean_v - std_v, &a1, &b1);
        for (i = 0; i < _width - kernel_half * 2; i++) {
            ideal = a1 + index[i] * b1;
            diff_v += (ideal - pProfile_v_avg_smooth[i]) * (ideal - pProfile_v_avg_smooth[i]);
            diff_v_max = diff_v_max > ABS(ideal - pProfile_v_avg_smooth[i])
                             ? diff_v_max
                             : ABS(ideal - pProfile_v_avg_smooth[i]);
        }
        diff_v = int_sqrt(diff_v / (_width - kernel_half * 2));

        // linear_curve_fit(index + (_height - _width), pProfile_v_avg_smooth + (_height - _width),
        // _width - kernel_half * 2, &a1_, &b1_);
        linear_curve_fit_thres(index + (_height - _width),
                               pProfile_v_avg_smooth + (_height - _width), _width - kernel_half * 2,
                               mean_v + std_v, mean_v - std_v, &a1_, &b1_);
        for (i = (_height - _width); i < _height - kernel_half * 2; i++) {
            ideal = a1_ + index[i] * b1_;
            diff_v_ += (ideal - pProfile_v_avg_smooth[i]) * (ideal - pProfile_v_avg_smooth[i]);
            diff_v_max = diff_v_max > ABS(ideal - pProfile_v_avg_smooth[i])
                             ? diff_v_max
                             : ABS(ideal - pProfile_v_avg_smooth[i]);
        }
        diff_v_ = int_sqrt(diff_v_ / (_width - kernel_half * 2));

#ifdef DEBUG
        if (fopen_s(&file, "hist_log.txt", "a") == 0) {
            fprintf(file, "diff_h %f diff_v %f diff_v_ %f diff_h_max %f diff_v_max %f\n", diff_h,
                    diff_v, diff_v_, diff_h_max, diff_v_max);
            fclose(file);
        }
#endif
    } else {
#ifdef EGIS_DEBUG
        egislog_d("Egis_debug partial 441");
#endif
        // TODO
    }

#ifdef EGIS_DEBUG
    egislog_d("Egis_debug partial 442");
#endif

    // determine h or v cut
    int is_hv = 0;
    if (MAX(diff_v, diff_v_) > diff_h + 15) {
        is_hv = 0;
        if (diff_v >= diff_v_) {
            *a = a1_;  // use lower diff
            *b = b1_;
        } else {
            *a = a1;
            *b = b1;
        }
    } else {
        is_hv = 1;
        *a = a0;
        *b = b0;
    }

#ifdef EGIS_DEBUG
    egislog_d("Egis_debug partial 443");
#endif

    if (MAX(diff_h_max, diff_v_max) < 90 && MAX(diff_h, diff_v) < 80) {
        is_hv = -1;
    }

    if (ABS(*b) > 10) {
        *a = 0;
        *b = 0;
    }

#ifdef DEBUG
    if (fopen_s(&file, "hist_log.txt", "a") == 0) {
        fprintf(file, "is_hv = %i\n", is_hv);
        fclose(file);
    }
#endif

    *score = MAX(diff_h, diff_v);

    FREE(pProfile_h_avg_smooth);
    FREE(pProfile_v_avg_smooth);
    FREE(index);

#ifdef EGIS_DEBUG
    egislog_d("Egis_debug partial 444");
#endif
    return is_hv;
}

static void split_cells_702(unsigned short* _pImg, unsigned short* _pBkg, unsigned char* _p8bit,
                            int _width, int _height, int _cx, int _cy, int _radius_boundary,
                            enum fp_type _fptype, enum model_type _modeltype,
                            enum lens_type _lenstype, int _cell_width, int _stride,
                            int _light_thres,
                            struct PartialDetectorResult* _pPartialDetectorResult) {
    int dark_thres = 100;
    switch (_modeltype) {
        case MODEL_702:
            dark_thres = 7000;
            break;
        default:
            break;
    }

    int max_contrast_score = 0;
    int max_contrast_minus_count = 0;
    int contrast_4_count = 0;
    int sum = 0;
    float mean_thres = 0;
    int sun_condition = 0;
    float avg_max_profile_valley = 0;
    int dry_constrast_count = 0;
    float mean_thres_0 = 0.8f;
    float mean_thres_1 = 1.3f;
    float mean_thres_2 = 0.5f;
    float mean_thres_3 = 1.3f;
    float mean_thres_4 = 0.7f;
    float mean_thres_5 = 1.3f;
    float mean_thres_boundary_min = 0.4f;
    float mean_thres_boundary_max = 2.5f;
    float valley_ratio_thres = 0.4f;
    int sun_factor_total_thres = 50;
    int max_profile_valley_thres = 40;
    int noisy_finger_max_contrast_score = 5;
    int second_peaks_num_thres = 4;
    int corner_thres_LT = 110;
    int corner_thres_RB = 310;
    int finger_light_thres = 2000;

    switch (_modeltype) {
        case MODEL_UNKNOW:
            break;
        case MODEL_A50:
            break;
        case MODEL_A70:
            break;
        case MODEL_A80:
            break;
        case MODEL_TAB_S6:
            break;
        case MODEL_A90:
            break;
        case MODEL_A30S:
            break;
        case MODEL_A50S:
            break;
        case MODEL_A70S:
            break;
        case MODEL_A51:
            break;
        case MODEL_A71:
            break;
        case MODEL_A91:
            max_profile_valley_thres = 7;
            sun_factor_total_thres = 100;
            mean_thres_0 = 0.5f;
            mean_thres_1 = 2.5f;
            mean_thres_2 = 0.5f;
            mean_thres_3 = 2.5f;
            mean_thres_4 = 0.5f;
            mean_thres_5 = 2.5f;
            mean_thres_boundary_min = 0.4f;
            mean_thres_boundary_max = 4.5f;
            noisy_finger_max_contrast_score = 6;
            second_peaks_num_thres = 3;
            break;
        case MODEL_A_NOTE:
            break;
        case MODEL_702:
            finger_light_thres = 16000;
            mean_thres_0 = 0.8f;
            mean_thres_1 = 1.3f;
            mean_thres_2 = 0.5f;
            mean_thres_3 = 1.3f;
            mean_thres_4 = 0.7f;
            mean_thres_5 = 1.3f;
            mean_thres_boundary_min = 0.4f;
            mean_thres_boundary_max = 4.5f;
            break;
        case MODEL_LG:
            break;
        case MODEL_HW:
            break;
        case MODEL_END:
            break;
        default:
            break;
    }

#ifdef EGIS_DEBUG
    egislog_d("Egis_debug partial 301 %i", dark_thres);
#endif

    int i, j, k, l;
    int col_cell_size = (_width - _cell_width) / _stride + 1;
    int row_cell_size = (_height - _cell_width) / _stride + 1;
    int cell_xshift = (_width - _stride * (col_cell_size - 1) - _cell_width) / 2;
    int cell_yshift = (_height - _stride * (row_cell_size - 1) - _cell_width) / 2;

    struct SplitResult* splitResult =
        (struct SplitResult*)malloc(col_cell_size * row_cell_size * sizeof(struct SplitResult));

    int crop_x = 0;
    int crop_y = 0;
    int* img_crop = (int*)malloc(_cell_width * _cell_width * sizeof(int));
    int* bit8_crop = (int*)malloc(_cell_width * _cell_width * sizeof(int));

#ifdef OUTPUT_MASK
    mask = (unsigned char*)malloc(_width * _height * sizeof(unsigned char));
#ifdef DEBUG
    for (i = 0; i < _height * _width; i++) {
        mask[i] = _p8bit[i] * 0.3;
    }
#else
    memset(mask, 1, _width * _height * sizeof(unsigned char));
#endif
#endif  // DEBUG

#ifdef EGIS_DEBUG
    egislog_d("Egis_debug partial 302");
#endif

#ifdef DEBUG
    FILE* file;
#endif

    int is_boundary = 0;
    int dist = 0;
    int total_area = 0;
    int total_inner_area = 0;
    int inner_width_offset = col_cell_size / 4;
    int inner_height_offset = row_cell_size / 4;
    for (i = 0; i < row_cell_size; i++) {
        for (j = 0; j < col_cell_size; j++) {
#ifdef EGIS_DEBUG
            egislog_d("Egis_debug partial 3020 %i %i", i, j);
#endif

            crop_x = cell_xshift + j * _stride;
            crop_y = cell_yshift + i * _stride;

            for (k = 0; k < _height; k++) {
                for (l = 0; l < _width; l++) {
                    if (k >= crop_y && k < crop_y + _cell_width && l >= crop_x &&
                        l < crop_x + _cell_width) {
                        img_crop[(k - crop_y) * _cell_width + (l - crop_x)] = _pImg[k * _width + l];
                        bit8_crop[(k - crop_y) * _cell_width + (l - crop_x)] =
                            _p8bit[k * _width + l];
                    }
                }
            }

#ifdef EGIS_DEBUG
            egislog_d("Egis_debug partial 3021");
#endif

#ifdef DEBUG
            if (fopen_s(&file, "hist_log.txt", "a") == 0) {
                fprintf(file, "i %i j %i\n", i, j);
                fclose(file);
            }

            if (fopen_s(&file, "hist.csv", "a") == 0) {
                fprintf(file, "i %i j %i\n", i, j);
                fclose(file);
            }

            if (i == 4 && j == 4) {
                printf("");
            }
#endif

            if (i == 0 || j == 0 || i == row_cell_size - 1 || j == col_cell_size - 1) {
                is_boundary = 1;
            } else {
                is_boundary = 0;
            }

#ifdef EGIS_DEBUG
            egislog_d("Egis_debug partial 3022 %i", i * col_cell_size + j);
#endif

            splitResult[i * col_cell_size + j].sharp_score = 0;
            splitResult[i * col_cell_size + j].contrast_score = 0;
            splitResult[i * col_cell_size + j].max_peaks_num = 0;
            splitResult[i * col_cell_size + j].max_valleys_num = 0;
            splitResult[i * col_cell_size + j].max_profile_valley = 0;
            splitResult[i * col_cell_size + j].second_peaks_num = 0;
            splitResult[i * col_cell_size + j].second_valleys_num = 0;
            splitResult[i * col_cell_size + j].second_profile_valley = 0;
            splitResult[i * col_cell_size + j].weak_max_peaks_num = 0;
            splitResult[i * col_cell_size + j].weak_second_peaks_num = 0;
            splitResult[i * col_cell_size + j].mean = 0;
            splitResult[i * col_cell_size + j].sun_factor = 0;
            splitResult[i * col_cell_size + j].is_period = 0;
            splitResult[i * col_cell_size + j].is_light = 0;
            splitResult[i * col_cell_size + j].is_out_boundary = 0;

            dist = int_sqrt(
                (_cx - (crop_x + _cell_width * 0.5)) * (_cx - (crop_x + _cell_width * 0.5)) +
                (_cy - (crop_y + _cell_width * 0.5)) * (_cy - (crop_y + _cell_width * 0.5)));
            if (dist > _radius_boundary) {
                splitResult[i * col_cell_size + j].is_out_boundary = 1;
            } else {
                get_small_area_result_avg(img_crop, bit8_crop, _cell_width, _cell_width,
                                          is_boundary, _light_thres, sun_factor_total_thres,
                                          _fptype, _modeltype, _lenstype,
                                          splitResult + i * col_cell_size + j);

#ifdef EGIS_DEBUG
                egislog_d("Egis_debug partial 3025");
#endif

                splitResult[i * col_cell_size + j].is_out_boundary = 0;
                total_area++;
                if (j >= inner_width_offset && j < col_cell_size - inner_width_offset &&
                    i >= inner_height_offset && i < row_cell_size - inner_height_offset) {
                    total_inner_area++;
                }

#ifdef EGIS_DEBUG
                egislog_d("Egis_debug partial 3023");
#endif
            }

#ifdef EGIS_DEBUG
            egislog_d("Egis_debug partial 3024");
#endif
        }
    }
    FREE(img_crop);
    FREE(bit8_crop);

#ifdef EGIS_DEBUG
    egislog_d("Egis_debug partial 303");
#endif

    float partial_score = 0.0;
    float inner_partial_score = 0.0;
    float light_score = 0.0;
    float inner_light_score = 0.0;

    // get mean from max contrast_score
    // int max_peak = 0;
    // int second_peak = 0;
    for (i = 0; i < row_cell_size; i++) {
        for (j = 0; j < col_cell_size; j++) {
            if (max_contrast_score < splitResult[i * col_cell_size + j].contrast_score) {
                max_contrast_score = splitResult[i * col_cell_size + j].contrast_score;
            }

            avg_max_profile_valley += splitResult[i * col_cell_size + j].max_profile_valley;
            sun_condition += splitResult[i * col_cell_size + j].sun_factor;
        }
    }

    int max_x = 0;
    int max_y = 0;
    int count = 0;
    for (i = 0; i < row_cell_size; i++) {
        for (j = 0; j < col_cell_size; j++) {
            if (splitResult[i * col_cell_size + j].contrast_score >= max_contrast_score - 1) {
                max_x += cell_xshift + j * _stride + _cell_width * 0.5;
                max_y += cell_yshift + i * _stride + _cell_width * 0.5;
                count++;
            }
        }
    }
    max_x = max_x / count;
    max_y = max_y / count;

    avg_max_profile_valley = (double)avg_max_profile_valley / (row_cell_size * col_cell_size);

#ifdef EGIS_DEBUG
    egislog_d("Egis_debug partial 304");
#endif

    for (i = 0; i < row_cell_size; i++) {
        for (j = 0; j < col_cell_size; j++) {
            if (splitResult[i * col_cell_size + j].contrast_score >= max_contrast_score - 1) {
                sum += splitResult[i * col_cell_size + j].mean;
                max_contrast_minus_count++;
            }
            if (splitResult[i * col_cell_size + j].contrast_score >= 4) {
                contrast_4_count++;
            }
            if (splitResult[i * col_cell_size + j].contrast_score >=
                noisy_finger_max_contrast_score) {
                dry_constrast_count++;
            }
        }
    }
    mean_thres = (double)sum / max_contrast_minus_count;

#ifdef EGIS_DEBUG
    egislog_d("Egis_debug partial 305");
#endif

    // sort contrast
    int* sort_contrast = (int*)malloc(row_cell_size * col_cell_size * sizeof(int));

    // copy
    for (i = 0; i < row_cell_size; i++) {
        for (j = 0; j < col_cell_size; j++) {
            sort_contrast[i * col_cell_size + j] =
                splitResult[i * col_cell_size + j].contrast_score;
        }
    }

    // sort
    int temp = 0;
    for (i = 0; i < row_cell_size * col_cell_size - 1; i++) {
        for (j = i + 1; j < row_cell_size * col_cell_size; j++) {
            if (sort_contrast[j] > sort_contrast[i]) {
                temp = sort_contrast[i];
                sort_contrast[i] = sort_contrast[j];
                sort_contrast[j] = temp;
            }
        }
    }

    int q1 = (int)row_cell_size * col_cell_size * 0.25;
    int q2 = (int)row_cell_size * col_cell_size * 0.5;
    int Q1_contrast_score = sort_contrast[q1];
    int Q1_contrast_count = q1;
    for (i = q1 + 1; i < row_cell_size * col_cell_size; i++) {
        if (sort_contrast[i] == Q1_contrast_score) {
            Q1_contrast_count++;
        } else {
            break;
        }
    }
    int Q2_contrast_score = sort_contrast[q2];
    int Q2_contrast_count = q2;
    for (i = q2 + 1; i < row_cell_size * col_cell_size; i++) {
        if (sort_contrast[i] == Q2_contrast_score) {
            Q2_contrast_score++;
        } else {
            break;
        }
    }
    FREE(sort_contrast);

#ifdef DEBUG
    if (fopen_s(&file, "hist_log.txt", "a") == 0) {
        fprintf(file, "max_contrast_score %i max_x %i max_y %i\n", max_contrast_score, max_x,
                max_y);
        fprintf(file, "max_contrast_minus_count %i\n", max_contrast_minus_count);
        fprintf(file, "mean_thres %f\n", mean_thres);
        fprintf(file, "sun_condition %i\n", sun_condition);
        fprintf(file, "avg_max_profile_valley %f\n", avg_max_profile_valley);
        fprintf(file, "Q1_contrast_score %i Q1_contrast_count %i\n", Q1_contrast_score,
                Q1_contrast_count);
        fprintf(file, "Q2_contrast_score %i Q2_contrast_count %i\n", Q2_contrast_score,
                Q2_contrast_count);
        fclose(file);
    }
#endif

#ifdef EGIS_DEBUG
    egislog_d("Egis_debug partial 306");
#endif

    int* diff = (int*)malloc(_width * _height * sizeof(int));
    for (i = 0; i < _width * _height; i++) {
        diff[i] = _pImg[i] - _pBkg[i];
    }

#ifdef EGIS_DEBUG
    egislog_d("Egis_debug partial 406");
#endif

    // determine h or v cut
    float a, b;
    int score;
    int is_hv = determine_hv(diff, _width, _height, 20, &a, &b, &score);

#ifdef EGIS_DEBUG
    egislog_d("Egis_debug partial 407");
#endif

    if (sun_condition <= 500 &&
        (dry_constrast_count < 10 || ((max_contrast_score < 15 || max_contrast_minus_count < 20) &&
                                      score > 150)))  // dry finger
    {
#ifdef DEBUG
        if (fopen_s(&file, "hist_log.txt", "a") == 0) {
            fprintf(file, "use find boundary\n");
            fclose(file);
        }
#endif

#ifdef EGIS_DEBUG
        egislog_d("Egis_debug partial 408");
#endif
        _cell_width = 10;
        int _cell_stride = 10;

        if (is_hv == -1) {
            _pPartialDetectorResult->partial_ratio = 100;
            _pPartialDetectorResult->inner_partial_ratio = 100;
#ifdef OUTPUT_MASK
            for (i = 0; i < _height * _width; i++) {
                mask[i] = _p8bit[i];
            }
#ifdef DEBUG
#else
            memset(mask, 0, _width * _height * sizeof(unsigned char));
#endif
#endif  // DEBUG
            FREE(diff);
            FREE(splitResult);
            return;
        }

#ifdef EGIS_DEBUG
        egislog_d("Egis_debug partial 409");
#endif

        // 2 fine search the edge
        int* pProfile = NULL;
        int* pProfile_smooth = NULL;
        int cut_size;
        int cell_shift;
        int profile_size;
        int ideal;

        if (is_hv == 1) {
#ifdef EGIS_DEBUG
            egislog_d("Egis_debug partial 410");
#endif

            // horizonal cut
            cut_size = (_height - _cell_width) / _cell_stride + 1;
            cell_shift = (_height - _cell_stride * (cut_size - 1)) / 2;
            profile_size = _width;
            pProfile = (int*)malloc(sizeof(int) * cut_size * _width);
            pProfile_smooth = (int*)malloc(sizeof(int) * cut_size * _width);
            memset(pProfile_smooth, 0, sizeof(int) * cut_size * _width);

#ifdef DEBUG
            if (fopen_s(&file, "hist.csv", "a") == 0) {
                int need_comma = 0;
                for (i = 10; i < _width - 10; ++i) {
                    if (need_comma)
                        fprintf(file, ", ");
                    else
                        need_comma = 1;
                    fprintf(file, "%i", i);
                }
                fprintf(file, "\n");
                fclose(file);
            }
#endif

            for (i = 0; i < cut_size; i++) {
                for (j = 0; j < _width; j++) {
                    ideal = a + j * b;
                    pProfile[i * _width + j] =
                        diff[(i * _cell_stride + cell_shift) * _width + j] - ideal;
                }
                smooth_1Darray(pProfile + i * _width, _width, 20, pProfile_smooth + i * _width);
            }
        } else  // is_hv == 0
        {
#ifdef EGIS_DEBUG
            egislog_d("Egis_debug partial 411");
#endif
            // vertical cut
            cut_size = (_width - _cell_width) / _cell_stride + 1;
            cell_shift = (_width - _cell_stride * (cut_size - 1)) / 2;
            profile_size = _height;
            pProfile = (int*)malloc(sizeof(int) * cut_size * _height);
            pProfile_smooth = (int*)malloc(sizeof(int) * cut_size * _height);
            memset(pProfile_smooth, 0, sizeof(int) * cut_size * _height);

#ifdef DEBUG
            if (fopen_s(&file, "hist.csv", "a") == 0) {
                int need_comma = 0;
                for (i = 10; i < _height - 10; ++i) {
                    if (need_comma)
                        fprintf(file, ", ");
                    else
                        need_comma = 1;
                    fprintf(file, "%i", i);
                }
                fprintf(file, "\n");
                fclose(file);
            }
#endif

            for (i = 0; i < cut_size; i++) {
                for (j = 0; j < _height; j++) {
                    ideal = a + j * b;
                    pProfile[i * _height + j] =
                        diff[j * _width + (i * _cell_stride + cell_shift)] - ideal;
                }
                smooth_1Darray(pProfile + i * _height, _height, 20, pProfile_smooth + i * _height);
            }
        }
#ifdef EGIS_DEBUG
        egislog_d("Egis_debug partial 412");
#endif

        struct HillSideInfo* hillSideInfo =
            (struct HillSideInfo*)malloc(cut_size * 50 * sizeof(struct HillSideInfo));
        int* hillSideInfo_size = (int*)malloc(cut_size * sizeof(int));
        memset(hillSideInfo_size, 0, cut_size * sizeof(int));
        for (i = 2; i < cut_size - 2; i += 2) {
#ifdef DEBUG
            if (fopen_s(&file, "hist_log.txt", "a") == 0) {
                fprintf(file, "%i,", i);
                fclose(file);
            }
#endif
            find_hillside(pProfile_smooth + i * profile_size, profile_size, 20, 50,
                          hillSideInfo + 50 * i, hillSideInfo_size + i);
        }

#ifdef EGIS_DEBUG
        egislog_d("Egis_debug partial 413");
#endif

        // 3 parse curve direction
        int* pEdge = (int*)malloc(cut_size * sizeof(int));
        memset(pEdge, -1, cut_size * sizeof(int));
        int finger_pn = parse_curve_direction_1(
            pProfile_smooth, profile_size, 20, hillSideInfo, hillSideInfo_size, cut_size,
            _cell_stride, cell_shift, is_hv, max_contrast_score, max_x, max_y, pEdge);

#ifdef EGIS_DEBUG
        egislog_d("Egis_debug partial 414");
#endif

        // 4 output
        int edge = 0;
        int cut_str = 0;
        int cut_end = 0;
        if (is_hv == 1)  // horizontal cut
        {
#ifdef EGIS_DEBUG
            egislog_d("Egis_debug partial 415");
#endif
            inner_width_offset = _width / 4;
            inner_height_offset = cut_size / 4;

            for (i = 0; i < cut_size; i++) {
                edge = pEdge[i];

                if (i == 0) {
                    cut_str = 0;
                } else {
                    cut_str = i * _cell_stride + cell_shift - _cell_stride / 2;
                }

                if (i == cut_size - 1) {
                    cut_end = _height;
                } else {
                    cut_end = i * _cell_stride + cell_shift + _cell_stride / 2;
                }

                if (finger_pn == 1)  // higher is finger
                {
                    partial_score += (double)100 / cut_size / profile_size * (profile_size - edge);

                    int inner_edge = profile_size - inner_width_offset * 2 -
                                     MIN(MAX(edge - inner_width_offset, 0),
                                         profile_size - inner_width_offset * 2);
                    if (i >= inner_height_offset && i < cut_size - inner_height_offset) {
                        inner_partial_score += (double)100 / (cut_size - inner_height_offset * 2) /
                                               (profile_size - inner_width_offset * 2) *
                                               (inner_edge);
                    }

#ifdef OUTPUT_MASK
                    for (k = 0; k < _height; k++) {
                        for (l = 0; l < _width; l++) {
                            if (k >= cut_str && k <= cut_end && l > edge) {
#ifdef DEBUG
                                mask[k * _width + l] = _p8bit[k * _width + l];
#else
                                mask[k * _width + l] = 0;
#endif
                            }
                        }
                    }
#endif                  // OUTPUT_MASK
                } else  // lower is finger
                {
                    if (edge == 0) {
                        edge = profile_size;
                    }
                    partial_score += (double)100 / cut_size / profile_size * edge;

                    int inner_edge = MIN(MAX(edge - inner_width_offset, 0),
                                         profile_size - inner_width_offset * 2);
                    if (i >= inner_height_offset && i < cut_size - inner_height_offset) {
                        inner_partial_score += (double)100 / (cut_size - inner_height_offset * 2) /
                                               (profile_size - inner_width_offset * 2) *
                                               (inner_edge);
                    }

#ifdef OUTPUT_MASK
                    for (k = 0; k < _height; k++) {
                        for (l = 0; l < _width; l++) {
                            if (k >= cut_str && k <= cut_end && l < edge) {
#ifdef DEBUG
                                mask[k * _width + l] = _p8bit[k * _width + l];
#else
                                mask[k * _width + l] = 0;
#endif
                            }
                        }
                    }
#endif  // OUTPUT_MASK
                }
            }
        } else  // vertical cut
        {
#ifdef EGIS_DEBUG
            egislog_d("Egis_debug partial 416");
#endif
            inner_width_offset = cut_size / 4;
            inner_height_offset = _height / 4;

            for (i = 0; i < cut_size; i++) {
                edge = pEdge[i];

                if (i == 0) {
                    cut_str = 0;
                } else {
                    cut_str = i * _cell_stride + cell_shift - _cell_stride / 2;
                }

                if (i == cut_size - 1) {
                    cut_end = _width;
                } else {
                    cut_end = i * _cell_stride + cell_shift + _cell_stride / 2;
                }

                if (finger_pn == 1)  // higher is finger
                {
                    partial_score += (double)100 / cut_size / profile_size * (profile_size - edge);

                    int inner_edge = profile_size - inner_height_offset * 2 -
                                     MIN(MAX(edge - inner_height_offset, 0),
                                         profile_size - inner_height_offset * 2);
                    if (i >= inner_width_offset && i < cut_size - inner_width_offset) {
                        inner_partial_score += (double)100 / (cut_size - inner_width_offset * 2) /
                                               (profile_size - inner_height_offset * 2) *
                                               (inner_edge);
                    }

#ifdef OUTPUT_MASK
                    for (k = 0; k < _height; k++) {
                        for (l = 0; l < _width; l++) {
                            if (l >= cut_str && l <= cut_end && k > edge) {
#ifdef DEBUG
                                mask[k * _width + l] = _p8bit[k * _width + l];
#else
                                mask[k * _width + l] = 0;
#endif
                            }
                        }
                    }
#endif                  // OUTPUT_MASK
                } else  // lower is finger
                {
                    if (edge == 0) {
                        edge = profile_size;
                    }
                    partial_score += (double)100 / cut_size / profile_size * edge;

                    int inner_edge = MIN(MAX(edge - inner_height_offset, 0),
                                         profile_size - inner_height_offset * 2);
                    if (i >= inner_width_offset && i < cut_size - inner_width_offset) {
                        inner_partial_score += (double)100 / (cut_size - inner_width_offset * 2) /
                                               (profile_size - inner_height_offset * 2) *
                                               (inner_edge);
                    }

#ifdef OUTPUT_MASK
                    for (k = 0; k < _height; k++) {
                        for (l = 0; l < _width; l++) {
                            if (l >= cut_str && l <= cut_end && k < edge) {
#ifdef DEBUG
                                mask[k * _width + l] = _p8bit[k * _width + l];
#else
                                mask[k * _width + l] = 0;
#endif
                            }
                        }
                    }
#endif  // OUTPUT_MASK
                }
            }
        }
        FREE(pProfile);
        FREE(pProfile_smooth);
        FREE(hillSideInfo);
        FREE(hillSideInfo_size);
        FREE(pEdge);
    } else {
        int is_LT_RB_corner = 0;  // is left-top or right-bottom corner

        for (i = 0; i < row_cell_size; i++) {
            for (j = 0; j < col_cell_size; j++) {
                crop_x = cell_xshift + j * _stride + (_cell_width - _stride) / 2;
                crop_y = cell_yshift + i * _stride + (_cell_width - _stride) / 2;

                int top, bottom, left, right;
#ifdef DEBUG
                top = crop_y;
                bottom = crop_y + _cell_width - 1;
                left = crop_x;
                right = crop_x + _cell_width - 1;
#else
                if (i == 0) {
                    top = 0;
                } else {
                    top = crop_y;
                }

                if (i == row_cell_size - 1) {
                    bottom = _height - 1;
                } else {
                    bottom = crop_y + _cell_width - 1;
                }

                if (j == 0) {
                    left = 0;
                } else {
                    left = crop_x;
                }

                if (j == col_cell_size - 1) {
                    right = _width - 1;
                } else {
                    right = crop_x + _cell_width - 1;
                }
#endif  // DEBUG

                if (splitResult[i * col_cell_size + j].is_out_boundary == 1) {
#ifdef OUTPUT_MASK
                    for (k = 0; k < _height; k++) {
                        for (l = 0; l < _width; l++) {
                            if (k >= top && k <= bottom && l >= left && l <= right) {
#ifdef DEBUG
                                mask[k * _width + l] = 0;
#else
                                mask[k * _width + l] = 1;
#endif  // DEBUG
                            }
                        }
                    }
#endif  // OUTPUT_MASK
                } else if (splitResult[i * col_cell_size + j].sun_factor > sun_factor_total_thres) {
#ifdef DEBUG
                    if (fopen_s(&file, "hist_log.txt", "a") == 0) {
                        fprintf(file, "%i %i sun_factor %i\n", i, j,
                                splitResult[i * col_cell_size + j].sun_factor);
                        fclose(file);
                    }
#endif

#ifdef OUTPUT_MASK
                    for (k = 0; k < _height; k++) {
                        for (l = 0; l < _width; l++) {
                            if (k >= top && k <= bottom && l >= left && l <= right) {
#ifdef DEBUG
                                mask[k * _width + l] = 255;
#else
                                mask[k * _width + l] = 1;
#endif  // DEBUG
                            }
                        }
                    }
#endif  // OUTPUT_MASK
                    light_score += (double)100 / total_area;

                    if (j >= inner_width_offset && j < col_cell_size - inner_width_offset &&
                        i >= inner_height_offset && i < row_cell_size - inner_height_offset) {
                        inner_light_score += (double)100 / total_inner_area;
                    }

                } else if (splitResult[i * col_cell_size + j].contrast_score <= 1) {
#ifdef DEBUG
                    if (fopen_s(&file, "hist_log.txt", "a") == 0) {
                        fprintf(file, "%i %i contrast_score %i <= 1\n", i, j,
                                splitResult[i * col_cell_size + j].contrast_score);
                        fclose(file);
                    }
#endif
                } else if (Q1_contrast_score >= 15) {
                    if (is_LT_RB_corner == 0 && max_contrast_score - Q2_contrast_score > 6 &&
                        splitResult[i * col_cell_size + j].contrast_score < Q1_contrast_score - 6) {
#ifdef DEBUG
                        if (fopen_s(&file, "hist_log.txt", "a") == 0) {
                            fprintf(file, "%i %i contrast_score %i < %i\n", i, j,
                                    splitResult[i * col_cell_size + j].contrast_score,
                                    Q1_contrast_score - 6);
                            fclose(file);
                        }
#endif
                    } else if (is_LT_RB_corner == 0 && Q2_contrast_score >= 15 &&
                               splitResult[i * col_cell_size + j].contrast_score <
                                   Q1_contrast_score - 10) {
#ifdef DEBUG
                        if (fopen_s(&file, "hist_log.txt", "a") == 0) {
                            fprintf(file, "%i %i contrast_score %i < %i\n", i, j,
                                    splitResult[i * col_cell_size + j].contrast_score,
                                    Q1_contrast_score - 10);
                            fclose(file);
                        }
#endif
                    } else if (is_LT_RB_corner == 0 && Q2_contrast_score >= 10 &&
                               splitResult[i * col_cell_size + j].contrast_score <
                                   Q1_contrast_score - 8) {
#ifdef DEBUG
                        if (fopen_s(&file, "hist_log.txt", "a") == 0) {
                            fprintf(file, "%i %i contrast_score %i < %i\n", i, j,
                                    splitResult[i * col_cell_size + j].contrast_score,
                                    Q1_contrast_score - 8);
                            fclose(file);
                        }
#endif
                    } else if (is_LT_RB_corner == 0 && Q2_contrast_score < 10 &&
                               splitResult[i * col_cell_size + j].contrast_score <
                                   Q1_contrast_score - 6) {
#ifdef DEBUG
                        if (fopen_s(&file, "hist_log.txt", "a") == 0) {
                            fprintf(file, "%i %i contrast_score %i < %i\n", i, j,
                                    splitResult[i * col_cell_size + j].contrast_score,
                                    Q1_contrast_score - 6);
                            fclose(file);
                        }
#endif
                    } else if ((i != 0 && i != row_cell_size - 1 && j != 0 &&
                                j != col_cell_size - 1) &&
                               splitResult[i * col_cell_size + j].sun_factor <= 50 &&
                               (splitResult[i * col_cell_size + j].mean <
                                    mean_thres * mean_thres_0 ||
                                splitResult[i * col_cell_size + j].mean >
                                    MIN(mean_thres * mean_thres_1, finger_light_thres))) {
#ifdef DEBUG
                        if (fopen_s(&file, "hist_log.txt", "a") == 0) {
                            fprintf(
                                file,
                                "%i %i max_contrast_score %i mean %f < %f > %f sun_condition %i\n",
                                i, j, max_contrast_score, splitResult[i * col_cell_size + j].mean,
                                mean_thres * mean_thres_0,
                                MIN(mean_thres * mean_thres_1, finger_light_thres),
                                splitResult[i * col_cell_size + j].sun_factor);
                            fclose(file);
                        }
#endif
                    } else if ((i == 0 || i == row_cell_size - 1 || j == 0 ||
                                j == col_cell_size - 1) &&
                               splitResult[i * col_cell_size + j].sun_factor <= 50 &&
                               (splitResult[i * col_cell_size + j].mean <
                                    mean_thres * mean_thres_boundary_min ||
                                splitResult[i * col_cell_size + j].mean >
                                    mean_thres * mean_thres_boundary_max)) {
#ifdef DEBUG
                        if (fopen_s(&file, "hist_log.txt", "a") == 0) {
                            fprintf(
                                file,
                                "%i %i max_contrast_score %i mean %f < %f > %f sun_condition %i\n",
                                i, j, max_contrast_score, splitResult[i * col_cell_size + j].mean,
                                mean_thres * mean_thres_boundary_min,
                                mean_thres * mean_thres_boundary_max,
                                splitResult[i * col_cell_size + j].sun_factor);
                            fclose(file);
                        }
#endif
                    } else if (splitResult[i * col_cell_size + j].is_period == 1) {
#ifdef OUTPUT_MASK
                        for (k = 0; k < _height; k++) {
                            for (l = 0; l < _width; l++) {
                                if (k >= top && k <= bottom && l >= left && l <= right) {
#ifdef DEBUG
                                    mask[k * _width + l] = _p8bit[k * _width + l];
#else
                                    mask[k * _width + l] = 0;
#endif  // DEBUG
                                }
                            }
                        }
#endif  // OUTPUT_MASK
                        partial_score += (double)100 / total_area;

                        if (j >= inner_width_offset && j < col_cell_size - inner_width_offset &&
                            i >= inner_height_offset && i < row_cell_size - inner_height_offset) {
                            inner_partial_score += (double)100 / total_inner_area;
                        }
                    }
					else if ((splitResult[i * col_cell_size + j].max_peaks_num >= 4 &&
						splitResult[i * col_cell_size + j].second_peaks_num >= 4) ||
						(splitResult[i * col_cell_size + j].max_valleys_num >= 4 &&
							splitResult[i * col_cell_size + j].second_valleys_num >= 4) ||
							(splitResult[i * col_cell_size + j].max_peaks_num >= 4 &&
								splitResult[i * col_cell_size + j].max_valleys_num >= 4)/* ||
						splitResult[i * col_cell_size + j].max_peaks_num >= 5 ||
						splitResult[i * col_cell_size + j].max_valleys_num >= 5*/)
					{
#ifdef OUTPUT_MASK
                        for (k = 0; k < _height; k++) {
                            for (l = 0; l < _width; l++) {
                                if (k >= top && k <= bottom && l >= left && l <= right) {
#ifdef DEBUG
                                    mask[k * _width + l] = _p8bit[k * _width + l];
#else
                                    mask[k * _width + l] = 0;
#endif  // DEBUG
                                }
                            }
                        }
#endif  // OUTPUT_MASK
                        partial_score += (double)100 / total_area;

                        if (j >= inner_width_offset && j < col_cell_size - inner_width_offset &&
                            i >= inner_height_offset && i < row_cell_size - inner_height_offset) {
                            inner_partial_score += (double)100 / total_inner_area;
                        }
                    } else {
#ifdef DEBUG
                        if (fopen_s(&file, "hist_log.txt", "a") == 0) {
                            fprintf(file, "%i %i\n", i, j);
                            fclose(file);
                        }
#endif
                    }
                } else if (max_contrast_score >= 15 && Q1_contrast_score >= 10)  // few finger print
                {
                    if (is_LT_RB_corner == 0 && max_contrast_score - Q2_contrast_score > 6 &&
                        Q2_contrast_score < 9 &&
                        splitResult[i * col_cell_size + j].contrast_score < 10) {
#ifdef DEBUG
                        if (fopen_s(&file, "hist_log.txt", "a") == 0) {
                            fprintf(file, "%i %i contrast_score %i < %i\n", i, j,
                                    splitResult[i * col_cell_size + j].contrast_score, 10);
                            fclose(file);
                        }
#endif
                    } else if (is_LT_RB_corner == 0 &&
                               max_contrast_score - Q2_contrast_score <= 6 &&
                               Q2_contrast_score >= 9 &&
                               splitResult[i * col_cell_size + j].contrast_score < 8) {
#ifdef DEBUG
                        if (fopen_s(&file, "hist_log.txt", "a") == 0) {
                            fprintf(file, "%i %i contrast_score %i < %i\n", i, j,
                                    splitResult[i * col_cell_size + j].contrast_score, 8);
                            fclose(file);
                        }
#endif
                    } else if (is_LT_RB_corner == 0 && max_contrast_score - Q2_contrast_score > 6 &&
                               Q2_contrast_score >= 9 &&
                               splitResult[i * col_cell_size + j].contrast_score < 9) {
#ifdef DEBUG
                        if (fopen_s(&file, "hist_log.txt", "a") == 0) {
                            fprintf(file, "%i %i contrast_score %i < %i\n", i, j,
                                    splitResult[i * col_cell_size + j].contrast_score, 9);
                            fclose(file);
                        }
#endif
                    } else if (is_LT_RB_corner == 0 && Q2_contrast_score < 9 &&
                               splitResult[i * col_cell_size + j].contrast_score < 10) {
#ifdef DEBUG
                        if (fopen_s(&file, "hist_log.txt", "a") == 0) {
                            fprintf(file, "%i %i contrast_score %i < %i\n", i, j,
                                    splitResult[i * col_cell_size + j].contrast_score, 10);
                            fclose(file);
                        }
#endif
                    } else if ((i != 0 && i != row_cell_size - 1 && j != 0 &&
                                j != col_cell_size - 1) &&
                               splitResult[i * col_cell_size + j].sun_factor <= 50 &&
                               (splitResult[i * col_cell_size + j].mean <
                                    mean_thres * mean_thres_2 ||
                                splitResult[i * col_cell_size + j].mean >
                                    MIN(mean_thres * mean_thres_3, finger_light_thres))) {
#ifdef DEBUG
                        if (fopen_s(&file, "hist_log.txt", "a") == 0) {
                            fprintf(
                                file,
                                "%i %i max_contrast_score %i mean %f < %f > %f sun_condition %i\n",
                                i, j, max_contrast_score, splitResult[i * col_cell_size + j].mean,
                                mean_thres * mean_thres_2,
                                MIN(mean_thres * mean_thres_3, finger_light_thres),
                                splitResult[i * col_cell_size + j].sun_factor);
                            fclose(file);
                        }
#endif
                    } else if ((i == 0 || i == row_cell_size - 1 || j == 0 ||
                                j == col_cell_size - 1) &&
                               splitResult[i * col_cell_size + j].sun_factor <= 50 &&
                               (splitResult[i * col_cell_size + j].mean <
                                    mean_thres * mean_thres_boundary_min ||
                                splitResult[i * col_cell_size + j].mean >
                                    mean_thres * mean_thres_boundary_max)) {
#ifdef DEBUG
                        if (fopen_s(&file, "hist_log.txt", "a") == 0) {
                            fprintf(
                                file,
                                "%i %i max_contrast_score %i mean %f < %f > %f sun_condition %i\n",
                                i, j, max_contrast_score, splitResult[i * col_cell_size + j].mean,
                                mean_thres * mean_thres_boundary_min,
                                mean_thres * mean_thres_boundary_max,
                                splitResult[i * col_cell_size + j].sun_factor);
                            fclose(file);
                        }
#endif
                    }
					else if ((splitResult[i * col_cell_size + j].max_peaks_num >= 4 &&
						splitResult[i * col_cell_size + j].second_peaks_num >= 4) ||
						(splitResult[i * col_cell_size + j].max_valleys_num >= 4 &&
							splitResult[i * col_cell_size + j].second_valleys_num >= 4) ||
							(splitResult[i * col_cell_size + j].max_peaks_num >= 4 &&
								splitResult[i * col_cell_size + j].max_valleys_num >= 4)/* ||
						splitResult[i * col_cell_size + j].max_peaks_num >= 5 ||
						splitResult[i * col_cell_size + j].max_valleys_num >= 5*/)
					{
#ifdef OUTPUT_MASK
                        for (k = 0; k < _height; k++) {
                            for (l = 0; l < _width; l++) {
                                if (k >= top && k <= bottom && l >= left && l <= right) {
#ifdef DEBUG
                                    mask[k * _width + l] = _p8bit[k * _width + l];
#else
                                    mask[k * _width + l] = 0;
#endif  // DEBUG
                                }
                            }
                        }
#endif  // OUTPUT_MASK
                        partial_score += (double)100 / total_area;

                        if (j >= inner_width_offset && j < col_cell_size - inner_width_offset &&
                            i >= inner_height_offset && i < row_cell_size - inner_height_offset) {
                            inner_partial_score += (double)100 / total_inner_area;
                        }
                    } else if (splitResult[i * col_cell_size + j].is_period == 1) {
#ifdef OUTPUT_MASK
                        for (k = 0; k < _height; k++) {
                            for (l = 0; l < _width; l++) {
                                if (k >= top && k <= bottom && l >= left && l <= right) {
#ifdef DEBUG
                                    mask[k * _width + l] = _p8bit[k * _width + l];
#else
                                    mask[k * _width + l] = 0;
#endif  // DEBUG
                                }
                            }
                        }
#endif  // OUTPUT_MASK
                        partial_score += (double)100 / total_area;

                        if (j >= inner_width_offset && j < col_cell_size - inner_width_offset &&
                            i >= inner_height_offset && i < row_cell_size - inner_height_offset) {
                            inner_partial_score += (double)100 / total_inner_area;
                        }
                    } else {
#ifdef DEBUG
                        if (fopen_s(&file, "hist_log.txt", "a") == 0) {
                            fprintf(file, "%i %i\n", i, j);
                            fclose(file);
                        }
#endif
                    }
                } else if (max_contrast_score < 15 && Q1_contrast_score >= 10) {
                    if (is_LT_RB_corner == 0 && max_contrast_score - Q1_contrast_score > 5 &&
                        splitResult[i * col_cell_size + j].contrast_score < Q1_contrast_score) {
#ifdef DEBUG
                        if (fopen_s(&file, "hist_log.txt", "a") == 0) {
                            fprintf(file, "%i %i contrast_score %i < %i\n", i, j,
                                    splitResult[i * col_cell_size + j].contrast_score,
                                    Q1_contrast_score);
                            fclose(file);
                        }
#endif
                    } else if (is_LT_RB_corner == 0 && Q2_contrast_score >= 11 &&
                               splitResult[i * col_cell_size + j].contrast_score <
                                   MIN(max_contrast_score - 5, 10)) {
#ifdef DEBUG
                        if (fopen_s(&file, "hist_log.txt", "a") == 0) {
                            fprintf(file, "%i %i contrast_score %i < %i\n", i, j,
                                    splitResult[i * col_cell_size + j].contrast_score,
                                    MIN(max_contrast_score - 5, 10));
                            fclose(file);
                        }
#endif
                    } else if (is_LT_RB_corner == 0 && Q2_contrast_score >= 10 &&
                               splitResult[i * col_cell_size + j].contrast_score <
                                   MIN(max_contrast_score - 6, 7)) {
#ifdef DEBUG
                        if (fopen_s(&file, "hist_log.txt", "a") == 0) {
                            fprintf(file, "%i %i contrast_score %i < %i\n", i, j,
                                    splitResult[i * col_cell_size + j].contrast_score,
                                    MIN(max_contrast_score - 6, 7));
                            fclose(file);
                        }
#endif
                    } else if ((i != 0 && i != row_cell_size - 1 && j != 0 &&
                                j != col_cell_size - 1) &&
                               splitResult[i * col_cell_size + j].sun_factor <= 50 &&
                               (splitResult[i * col_cell_size + j].mean <
                                    mean_thres * mean_thres_2 ||
                                splitResult[i * col_cell_size + j].mean >
                                    MIN(mean_thres * mean_thres_3, finger_light_thres))) {
#ifdef DEBUG
                        if (fopen_s(&file, "hist_log.txt", "a") == 0) {
                            fprintf(
                                file,
                                "%i %i max_contrast_score %i mean %f < %f > %f sun_condition %i\n",
                                i, j, max_contrast_score, splitResult[i * col_cell_size + j].mean,
                                mean_thres * mean_thres_2,
                                MIN(mean_thres * mean_thres_3, finger_light_thres),
                                splitResult[i * col_cell_size + j].sun_factor);
                            fclose(file);
                        }
#endif
                    } else if ((i == 0 || i == row_cell_size - 1 || j == 0 ||
                                j == col_cell_size - 1) &&
                               splitResult[i * col_cell_size + j].sun_factor <= 50 &&
                               (splitResult[i * col_cell_size + j].mean <
                                    mean_thres * mean_thres_boundary_min ||
                                splitResult[i * col_cell_size + j].mean >
                                    mean_thres * mean_thres_boundary_max)) {
#ifdef DEBUG
                        if (fopen_s(&file, "hist_log.txt", "a") == 0) {
                            fprintf(
                                file,
                                "%i %i max_contrast_score %i mean %f < %f > %f sun_condition %i\n",
                                i, j, max_contrast_score, splitResult[i * col_cell_size + j].mean,
                                mean_thres * mean_thres_boundary_min,
                                mean_thres * mean_thres_boundary_max,
                                splitResult[i * col_cell_size + j].sun_factor);
                            fclose(file);
                        }
#endif
                    }
					else if ((splitResult[i * col_cell_size + j].max_peaks_num >= 4 &&
						splitResult[i * col_cell_size + j].second_peaks_num >= 4) ||
						(splitResult[i * col_cell_size + j].max_valleys_num >= 4 &&
							splitResult[i * col_cell_size + j].second_valleys_num >= 4) ||
							(splitResult[i * col_cell_size + j].max_peaks_num >= 4 &&
								splitResult[i * col_cell_size + j].max_valleys_num >= 4)/* ||
						splitResult[i * col_cell_size + j].max_peaks_num >= 5 ||
						splitResult[i * col_cell_size + j].max_valleys_num >= 5*/)
					{
#ifdef OUTPUT_MASK
                        for (k = 0; k < _height; k++) {
                            for (l = 0; l < _width; l++) {
                                if (k >= top && k <= bottom && l >= left && l <= right) {
#ifdef DEBUG
                                    mask[k * _width + l] = _p8bit[k * _width + l];
#else
                                    mask[k * _width + l] = 0;
#endif  // DEBUG
                                }
                            }
                        }
#endif  // OUTPUT_MASK
                        partial_score += (double)100 / total_area;

                        if (j >= inner_width_offset && j < col_cell_size - inner_width_offset &&
                            i >= inner_height_offset && i < row_cell_size - inner_height_offset) {
                            inner_partial_score += (double)100 / total_inner_area;
                        }
                    } else if (splitResult[i * col_cell_size + j].is_period == 1) {
#ifdef OUTPUT_MASK
                        for (k = 0; k < _height; k++) {
                            for (l = 0; l < _width; l++) {
                                if (k >= top && k <= bottom && l >= left && l <= right) {
#ifdef DEBUG
                                    mask[k * _width + l] = _p8bit[k * _width + l];
#else
                                    mask[k * _width + l] = 0;
#endif  // DEBUG
                                }
                            }
                        }
#endif  // OUTPUT_MASK
                        partial_score += (double)100 / total_area;

                        if (j >= inner_width_offset && j < col_cell_size - inner_width_offset &&
                            i >= inner_height_offset && i < row_cell_size - inner_height_offset) {
                            inner_partial_score += (double)100 / total_inner_area;
                        }
                    } else {
#ifdef DEBUG
                        if (fopen_s(&file, "hist_log.txt", "a") == 0) {
                            fprintf(file, "%i %i\n", i, j);
                            fclose(file);
                        }
#endif
                    }
                } else  // noisy finger
                {
                    if (splitResult[i * col_cell_size + j].contrast_score <
                        MIN(Q1_contrast_score - 4, noisy_finger_max_contrast_score)) {
#ifdef DEBUG
                        if (fopen_s(&file, "hist_log.txt", "a") == 0) {
                            fprintf(file, "%i %i contrast_score %i < %i\n", i, j,
                                    splitResult[i * col_cell_size + j].contrast_score,
                                    MIN(Q1_contrast_score, noisy_finger_max_contrast_score));
                            fclose(file);
                        }
#endif
                    } else if ((i != 0 && i != row_cell_size - 1 && j != 0 &&
                                j != col_cell_size - 1) &&
                               (splitResult[i * col_cell_size + j].mean <
                                    mean_thres * mean_thres_4 ||
                                (splitResult[i * col_cell_size + j].mean >
                                 MIN(mean_thres * mean_thres_5, finger_light_thres)))) {
#ifdef DEBUG
                        if (fopen_s(&file, "hist_log.txt", "a") == 0) {
                            fprintf(file,
                                    "max_contrast_score >= 5: %i %i max_contrast_score %i mean %f "
                                    "< %f or > %f\n",
                                    i, j, max_contrast_score,
                                    splitResult[i * col_cell_size + j].mean,
                                    mean_thres * mean_thres_4,
                                    MIN(mean_thres * mean_thres_5, finger_light_thres));
                            fclose(file);
                        }
#endif
                    } else if ((i == 0 || i == row_cell_size - 1 || j == 0 ||
                                j == col_cell_size - 1) &&
                               splitResult[i * col_cell_size + j].sun_factor <= 50 &&
                               (splitResult[i * col_cell_size + j].mean <
                                    mean_thres * mean_thres_boundary_min ||
                                splitResult[i * col_cell_size + j].mean >
                                    mean_thres * mean_thres_boundary_max)) {
#ifdef DEBUG
                        if (fopen_s(&file, "hist_log.txt", "a") == 0) {
                            fprintf(
                                file,
                                "%i %i max_contrast_score %i mean %f < %f > %f sun_condition %i\n",
                                i, j, max_contrast_score, splitResult[i * col_cell_size + j].mean,
                                mean_thres * mean_thres_boundary_min,
                                mean_thres * mean_thres_boundary_max,
                                splitResult[i * col_cell_size + j].sun_factor);
                            fclose(file);
                        }
#endif
                    } else if ((splitResult[i * col_cell_size + j].max_peaks_num >= 4 &&
                                splitResult[i * col_cell_size + j].second_peaks_num >= 4) ||
                               (splitResult[i * col_cell_size + j].max_valleys_num >= 4 &&
                                splitResult[i * col_cell_size + j].second_valleys_num >= 4) ||
                               (splitResult[i * col_cell_size + j].max_peaks_num >= 4 &&
                                splitResult[i * col_cell_size + j].max_valleys_num >= 4)) {
#ifdef OUTPUT_MASK
                        for (k = 0; k < _height; k++) {
                            for (l = 0; l < _width; l++) {
                                if (k >= top && k <= bottom && l >= left && l <= right) {
#ifdef DEBUG
                                    mask[k * _width + l] = _p8bit[k * _width + l];
#else
                                    mask[k * _width + l] = 0;
#endif  // DEBUG
                                }
                            }
                        }
#endif  // OUTPUT_MASK
                        partial_score += (double)100 / total_area;

                        if (j >= inner_width_offset && j < col_cell_size - inner_width_offset &&
                            i >= inner_height_offset && i < row_cell_size - inner_height_offset) {
                            inner_partial_score += (double)100 / total_inner_area;
                        }
                    } else if (splitResult[i * col_cell_size + j].is_period == 1) {
#ifdef OUTPUT_MASK
                        for (k = 0; k < _height; k++) {
                            for (l = 0; l < _width; l++) {
                                if (k >= top && k <= bottom && l >= left && l <= right) {
#ifdef DEBUG
                                    mask[k * _width + l] = _p8bit[k * _width + l];
#else
                                    mask[k * _width + l] = 0;
#endif  // DEBUG
                                }
                            }
                        }
#endif  // OUTPUT_MASK
                        partial_score += (double)100 / total_area;

                        if (j >= inner_width_offset && j < col_cell_size - inner_width_offset &&
                            i >= inner_height_offset && i < row_cell_size - inner_height_offset) {
                            inner_partial_score += (double)100 / total_inner_area;
                        }
                    } else {
#ifdef DEBUG
                        if (fopen_s(&file, "hist_log.txt", "a") == 0) {
                            fprintf(file, "%i %i\n", i, j);
                            fclose(file);
                        }
#endif
                    }
                }
            }
        }
    }

#ifdef EGIS_DEBUG
    egislog_d("Egis_debug partial 418");
#endif

    _pPartialDetectorResult->partial_ratio = partial_score;
    _pPartialDetectorResult->inner_partial_ratio = inner_partial_score;
    _pPartialDetectorResult->room_light_partial_ratio = light_score;
    _pPartialDetectorResult->inner_room_light_partial_ratio = inner_light_score;

    FREE(diff);
    FREE(splitResult);
    return;
}

static void find_finger_boundary(int* _pImg, int _width, int _height,
                                 // enum fp_type _fptype,
                                 // enum model_type _modeltype,
                                 // enum lens_type _lenstype,
                                 int _cell_width, int _cell_stride,
                                 struct PartialDetectorResult* _pPartialDetectorResult) {
    int i, j, k, l;

    // determine h or v cut
    float a, b;
    int score;
    int is_hv = determine_hv(_pImg, _width, _height, 20, &a, &b, &score);

    if (is_hv == -1) {
        _pPartialDetectorResult->partial_ratio = 100;
#ifdef OUTPUT_MASK
        int max_ = -0xFFFF;
        int min_ = 0xFFFF;
        for (i = 0; i < _height; i++) {
            for (j = 0; j < _width; j++) {
                max_ = max_ > _pImg[i * _width + j] ? max_ : _pImg[i * _width + j];
                min_ = min_ < _pImg[i * _width + j] ? min_ : _pImg[i * _width + j];
            }
        }
        mask = (unsigned char*)malloc(_width * _height * sizeof(unsigned char));
        for (i = 0; i < _height * _width; i++) {
            mask[i] = (double)(_pImg[i] - min_) * 255 / (max_ - min_);
        }
#endif  // DEBUG
        return;
    }

#ifdef OUTPUT_MASK
    int max_ = -0xFFFF;
    int min_ = 0xFFFF;
    for (i = 0; i < _height; i++) {
        for (j = 0; j < _width; j++) {
            max_ = max_ > _pImg[i * _width + j] ? max_ : _pImg[i * _width + j];
            min_ = min_ < _pImg[i * _width + j] ? min_ : _pImg[i * _width + j];
        }
    }
    mask = (unsigned char*)malloc(_width * _height * sizeof(unsigned char));
    for (i = 0; i < _height * _width; i++) {
        mask[i] = (double)(_pImg[i] - min_) * 255 / (max_ - min_) * 0.1;
    }
#endif  // DEBUG

    // 2 fine search the edge
    int* pProfile = NULL;
    int* pProfile_smooth = NULL;
    int cut_size;
    int cell_shift;
    int profile_size;
    int ideal;

    if (is_hv == 1) {
        // horizonal cut
        cut_size = (_height - _cell_width) / _cell_stride + 1;
        cell_shift = (_height - _cell_stride * (cut_size - 1)) / 2;
        profile_size = _width;
        pProfile = (int*)malloc(sizeof(int) * cut_size * _width);
        pProfile_smooth = (int*)malloc(sizeof(int) * cut_size * _width);
        memset(pProfile_smooth, 0, sizeof(int) * cut_size * _width);

#ifdef DEBUG
        FILE* file;
        if (fopen_s(&file, "hist.csv", "a") == 0) {
            int need_comma = 0;
            for (i = 10; i < _width - 10; ++i) {
                if (need_comma)
                    fprintf(file, ", ");
                else
                    need_comma = 1;
                fprintf(file, "%i", i);
            }
            fprintf(file, "\n");
            fclose(file);
        }
#endif

        for (i = 0; i < cut_size; i++) {
            for (j = 0; j < _width; j++) {
                ideal = a + j * b;
                pProfile[i * _width + j] =
                    _pImg[(i * _cell_stride + cell_shift) * _width + j] - ideal;
            }
            smooth_1Darray(pProfile + i * _width, _width, 20, pProfile_smooth + i * _width);
        }
    } else {
        // vertical cut
        cut_size = (_width - _cell_width) / _cell_stride + 1;
        cell_shift = (_width - _cell_stride * (cut_size - 1)) / 2;
        profile_size = _height;
        pProfile = (int*)malloc(sizeof(int) * cut_size * _height);
        pProfile_smooth = (int*)malloc(sizeof(int) * cut_size * _height);
        memset(pProfile_smooth, 0, sizeof(int) * cut_size * _height);

#ifdef DEBUG
        FILE* file;
        if (fopen_s(&file, "hist.csv", "a") == 0) {
            int need_comma = 0;
            for (i = 10; i < _height - 10; ++i) {
                if (need_comma)
                    fprintf(file, ", ");
                else
                    need_comma = 1;
                fprintf(file, "%i", i);
            }
            fprintf(file, "\n");
            fclose(file);
        }
#endif

        for (i = 0; i < cut_size; i++) {
            for (j = 0; j < _height; j++) {
                ideal = a + j * b;
                pProfile[i * _height + j] =
                    _pImg[j * _width + (i * _cell_stride + cell_shift)] - ideal;
            }
            smooth_1Darray(pProfile + i * _height, _height, 20, pProfile_smooth + i * _height);
        }
    }

    FREE(pProfile);

    struct HillSideInfo* hillSideInfo =
        (struct HillSideInfo*)malloc(cut_size * 50 * sizeof(struct HillSideInfo));
    int* hillSideInfo_size = (int*)malloc(cut_size * sizeof(int));
    memset(hillSideInfo_size, 0, cut_size * sizeof(int));
    for (i = 2; i < cut_size - 2; i += 2) {
#ifdef DEBUG
        FILE* file;
        if (fopen_s(&file, "hist_log.txt", "a") == 0) {
            fprintf(file, "%i,", i);
            fclose(file);
        }
#endif
        find_hillside(pProfile_smooth + i * profile_size, profile_size, 20, 50,
                      hillSideInfo + 50 * i, hillSideInfo_size + i);
    }

    // 3 parse curve direction
    int* pEdge = (int*)malloc(cut_size * sizeof(int));
    memset(pEdge, -1, cut_size * sizeof(int));
    int finger_pn =
        parse_curve_direction(pProfile_smooth, profile_size, 20, hillSideInfo, hillSideInfo_size,
                              cut_size, _cell_stride, cell_shift, pEdge);

    // 4 output
    float partial_score = 0.0;
    int edge = 0;
    int cut_str;
    int cut_end;
    if (is_hv == 1)  // horizontal cut
    {
        for (i = 0; i < cut_size; i++) {
            edge = pEdge[i];

            if (finger_pn == 1)  // higher is finger
            {
                partial_score += (double)100 / profile_size * (profile_size - edge) / cut_size;

#ifdef OUTPUT_MASK
                cut_str = i * _cell_stride + cell_shift - _cell_stride / 2;
                cut_end = i * _cell_stride + cell_shift + _cell_stride / 2;
                for (k = 0; k < _height; k++) {
                    for (l = 0; l < _width; l++) {
                        if (k >= cut_str && k <= cut_end && l > edge) {
                            mask[k * _width + l] =
                                (double)(_pImg[k * _width + l] - min_) * 255 / (max_ - min_);
                        }
                    }
                }
#endif              // OUTPUT_MASK
            } else  // lower is finger
            {
                if (edge == 0) {
                    edge = profile_size;
                }
                partial_score += (double)100 / cut_size * (1 / profile_size * (edge));

#ifdef OUTPUT_MASK
                cut_str = i * _cell_stride + cell_shift - _cell_stride / 2;
                cut_end = i * _cell_stride + cell_shift + _cell_stride / 2;
                for (k = 0; k < _height; k++) {
                    for (l = 0; l < _width; l++) {
                        if (k >= cut_str && k <= cut_end && l < edge) {
                            mask[k * _width + l] =
                                (double)(_pImg[k * _width + l] - min_) * 255 / (max_ - min_);
                        }
                    }
                }
#endif  // OUTPUT_MASK
            }
        }
    } else  // vertical cut
    {
        for (i = 0; i < cut_size; i++) {
            edge = pEdge[i];

            if (finger_pn == 1)  // higher is finger
            {
                partial_score +=
                    (double)100 / cut_size * (1 / profile_size * (profile_size - edge));

#ifdef OUTPUT_MASK
                cut_str = i * _cell_stride + cell_shift - _cell_stride / 2;
                cut_end = i * _cell_stride + cell_shift + _cell_stride / 2;
                for (k = 0; k < _height; k++) {
                    for (l = 0; l < _width; l++) {
                        if (l >= cut_str && l <= cut_end && k > edge) {
                            mask[k * _width + l] =
                                (double)(_pImg[k * _width + l] - min_) * 255 / (max_ - min_);
                        }
                    }
                }
#endif              // OUTPUT_MASK
            } else  // lower is finger
            {
                if (edge == 0) {
                    edge = profile_size;
                }
                partial_score += (double)100 / cut_size * (1 / profile_size * (edge));

#ifdef OUTPUT_MASK
                cut_str = i * _cell_stride + cell_shift - _cell_stride / 2;
                cut_end = i * _cell_stride + cell_shift + _cell_stride / 2;
                for (k = 0; k < _height; k++) {
                    for (l = 0; l < _width; l++) {
                        if (l >= cut_str && l <= cut_end && k < edge) {
                            mask[k * _width + l] =
                                (double)(_pImg[k * _width + l] - min_) * 255 / (max_ - min_);
                        }
                    }
                }
#endif  // OUTPUT_MASK
            }
        }
    }

    _pPartialDetectorResult->partial_ratio = partial_score;

    FREE(hillSideInfo);
    FREE(hillSideInfo_size);
    FREE(pProfile_smooth);
}

unsigned char* detect_partial_cell(unsigned short* _pImg, unsigned short* _pBkg,
                                   unsigned char* _p8bit, int _width, int _height, int _cx, int _cy,
                                   int _radius_boundary, float _Et, int _HwInt,
                                   enum fp_type _fptype, enum model_type _modeltype,
                                   enum lens_type _lenstype,
                                   struct PartialDetectorResult* _pPartialDetectorResult) {
    int i, j, k;

    _pPartialDetectorResult->partial_ratio = 0;
    _pPartialDetectorResult->inner_partial_ratio = 0;
    _pPartialDetectorResult->room_light_partial_ratio = 0;
    _pPartialDetectorResult->inner_room_light_partial_ratio = 0;
    _pPartialDetectorResult->sun_light_score = 0;
    _pPartialDetectorResult->room_light_score = 0;

    int cell_width = 40;
    int stride = 20;
    float fEt_HwInt = 1;
    int shading_ratio = 0;
    switch (_lenstype) {
        case LENS_UNKNOW:
            break;
        case LENS_2PA:
            break;
        case LENS_3PA:
            break;
        case LENS_3PC:
            break;
        case LENS_3PD:
            break;
        case LENS_3PE:
            break;
        case LENS_3PF:
            break;
        case LENS_END:
            break;
        default:
            break;
    }

    switch (_modeltype) {
        case MODEL_UNKNOW:
            break;
        case MODEL_A50:
            break;
        case MODEL_A70:
            break;
        case MODEL_A80:
            break;
        case MODEL_TAB_S6:
            break;
        case MODEL_A90:
            break;
        case MODEL_A30S:
            break;
        case MODEL_A50S:
            break;
        case MODEL_A70S:
            break;
        case MODEL_A51:
            break;
        case MODEL_A71:
            break;
        case MODEL_A91:
            cell_width = 40;
            stride = 20;
            break;
        case MODEL_A_NOTE:
            break;
        case MODEL_702:
            break;
        case MODEL_LG:
            cell_width = 40;
            stride = 15;
            shading_ratio = 5;
            break;
        case MODEL_END:
            break;
        default:
            break;
    }

    int* img = (int*)malloc(_width * _height * sizeof(int));
    int* bkg = (int*)malloc(_width * _height * sizeof(int));
    if (img != NULL && bkg != NULL && _pImg != NULL && _pBkg != NULL) {
        for (i = 0; i < _width * _height; i++) {
            img[i] = _pImg[i];
            bkg[i] = _pBkg[i];
        }
    }

    int* diff = (int*)malloc(_width * _height * sizeof(int));
    int dist = 0;
    if (img != NULL && bkg != NULL && diff != NULL) {
        for (i = 0; i < _height; i++) {
            for (j = 0; j < _width; j++) {
                dist = int_sqrt((_cx - j) * (_cx - j) + (_cy - i) * (_cy - i));
                diff[i * _width + j] =
                    img[i * _width + j] - bkg[i * _width + j] + dist * shading_ratio;
            }
        }
    }

    // int2CSV("diff.csv", diff, _width, _height);

    // analyze diff bkg ratio
    int hist_threshold = 20;
    float diff_bkg_ratio_pos_thres = 0;
    float diffmax_bkg_ratio_pos_thres = 0;
    float diff_bkg_ratio_neg_thres = 0;
    int* hist_diff = NULL;
    int* hist_diff_index = NULL;
    int hist_diff_size = 0;
    int hist_diff_min = 0, hist5_diff_max = 0, hist5_diff_min = 0;
    int bkg_org_min = 0, bkg_size = 0, bkg_max, bkg_min = 0;
    int room_light_score = 0;
    int room_light_index = 0;
    int hist_diff_intensity_max = 0;

    {
        hist_diff = hist_maxmin(diff, _width, _height, fEt_HwInt, hist_threshold, &hist_diff_min,
                                &hist_diff_size, &hist5_diff_max, &hist5_diff_min, BAND_WIDTH);

        hist_diff_index = malloc(hist_diff_size * sizeof(int));

        if (hist_diff != NULL && hist_diff_index != NULL) {
            for (i = 0; i < hist_diff_size; i++) {
                hist_diff_index[i] = hist_diff_min + i * BAND_WIDTH * fEt_HwInt;
                hist_diff_intensity_max =
                    hist_diff_intensity_max > hist_diff[i] ? hist_diff_intensity_max : hist_diff[i];
            }

            int* hist_bkg = hist_maxmin(bkg, _width, _height, fEt_HwInt, hist_threshold,
                                        &bkg_org_min, &bkg_size, &bkg_max, &bkg_min, BAND_WIDTH);

            if (hist_bkg != NULL) {
                diffmax_bkg_ratio_pos_thres =
                    (double)hist_diff_index[hist_diff_size - 1] / (bkg_max - bkg_min);
                diff_bkg_ratio_pos_thres = (double)hist5_diff_max / (bkg_max - bkg_min);

                room_light_score = 0;
                for (i = hist_diff_size - 1; i >= 0; i--) {
                    if (hist_diff[i] > MAX(hist_diff_intensity_max / 10, 50)) {
                        room_light_score = 100 - (double)i / hist_diff_size * 100;
                        room_light_index = i;
                        break;
                    }
                }
                FREE(hist_bkg);
            }
        }
    }

    int light_thres = 0;
    int light_index = hist_diff_index[room_light_index];

    switch (_modeltype) {
        case MODEL_A91:
            if (hist_diff_index[room_light_index] < 1000) {
                light_thres = 1500;
            } else if (hist_diff_index[room_light_index] < 2000) {
                light_thres = 2500;
            } else if (diff_bkg_ratio_pos_thres > 3 ||
                       (diffmax_bkg_ratio_pos_thres > 3 && diff_bkg_ratio_pos_thres > 1)) {
                light_thres = MAX(bkg_max, 4000);
            } else if (diff_bkg_ratio_pos_thres > 1.5 ||
                       (diffmax_bkg_ratio_pos_thres > 1.5 && diff_bkg_ratio_pos_thres > 1)) {
                light_thres = MAX(bkg_max, 2000);
            } else {
                light_thres = bkg_max;
            }

            light_index = MIN(hist_diff_index[room_light_index], 1500);

            break;
        case MODEL_LG:
            if (hist_diff_index[room_light_index] < 1000) {
                light_thres = 2000;
            } else if (hist_diff_index[room_light_index] < 2000) {
                light_thres = 2500;
            } else if (diff_bkg_ratio_pos_thres > 3 ||
                       (diffmax_bkg_ratio_pos_thres > 3 && diff_bkg_ratio_pos_thres > 1)) {
                light_thres = MAX(bkg_max, 4000);
            } else if (diff_bkg_ratio_pos_thres > 1.5 ||
                       (diffmax_bkg_ratio_pos_thres > 1.5 && diff_bkg_ratio_pos_thres > 1)) {
                light_thres = MAX(bkg_max, 2000);
            } else {
                light_thres = bkg_max;
            }

            light_index = MIN(hist_diff_index[room_light_index], 2000);

            break;
        default:
            break;
    }

#ifdef DEBUG
    FILE* file;
    if (fopen_s(&file, "hist_log.txt", "a") == 0) {
        fprintf(file,
                "diff_bkg_ratio_pos_thres %f diffmax_bkg_ratio_pos_thres %f room_light_score %i "
                "room_light_index %i light_thres %i\n",
                diff_bkg_ratio_pos_thres, diffmax_bkg_ratio_pos_thres, room_light_score,
                hist_diff_index[room_light_index], light_thres);
        fclose(file);
    }
#endif

    FREE(img);
    FREE(bkg);
    FREE(hist_diff);
    FREE(hist_diff_index);

    if (diff != NULL && _p8bit != NULL && _pPartialDetectorResult != NULL) {
        split_cells(diff, _p8bit, _width, _height, _cx, _cy, _radius_boundary, _fptype, _modeltype,
                    _lenstype, cell_width, stride, light_thres, light_index,
                    _pPartialDetectorResult);
        FREE(diff);
    }

    _pPartialDetectorResult->room_light_score = room_light_score;

    if (diff_bkg_ratio_pos_thres > 0) {
        _pPartialDetectorResult->sun_light_score += diff_bkg_ratio_pos_thres * 45;
    } else {
        _pPartialDetectorResult->sun_light_score = 0;
    }

    unsigned char* out = (unsigned char*)malloc(_width * _height * sizeof(unsigned char));

#ifdef OUTPUT_MASK
    memcpy(out, mask, _width * _height * sizeof(unsigned char));
    FREE(mask);
#endif

    return out;
}

void detect_partial_cell_SDK(unsigned short* _pImg, unsigned short* _pBkg, unsigned char* _p8bit,
                             int _width, int _height, int _cx, int _cy, int _radius_boundary,
                             float _Et, int _HwInt, enum fp_type _fptype,
                             enum model_type _modeltype, enum lens_type _lenstype,
                             struct PartialDetectorResult* _pPartialDetectorResult) {
    // int i, j, k;

    //_pPartialDetectorResult->partial_ratio = 0;
    //_pPartialDetectorResult->inner_partial_ratio = 0;
    //_pPartialDetectorResult->room_light_partial_ratio = 0;
    //_pPartialDetectorResult->inner_room_light_partial_ratio = 0;
    //_pPartialDetectorResult->sun_light_score = 0;
    //_pPartialDetectorResult->room_light_score = 0;

    // int cell_width = 40;
    // int stride = 20;
    // float fEt_HwInt = 1;
    // switch (_lenstype)
    //{
    // case LENS_UNKNOW:
    //	break;
    // case LENS_2PA:
    //	break;
    // case LENS_3PA:
    //	break;
    // case LENS_3PC:
    //	break;
    // case LENS_3PD:
    //	break;
    // case LENS_3PE:
    //	break;
    // case LENS_3PF:
    //	break;
    // case LENS_END:
    //	break;
    // default:
    //	break;
    //}

    // switch (_modeltype)
    //{
    // case MODEL_UNKNOW:
    //	break;
    // case MODEL_A50:
    //	break;
    // case MODEL_A70:
    //	break;
    // case MODEL_A80:
    //	break;
    // case MODEL_TAB_S6:
    //	break;
    // case MODEL_A90:
    //	break;
    // case MODEL_A30S:
    //	break;
    // case MODEL_A50S:
    //	break;
    // case MODEL_A70S:
    //	break;
    // case MODEL_A51:
    //	break;
    // case MODEL_A71:
    //	break;
    // case MODEL_A91:
    //	cell_width = 40;
    //	stride = 20;
    //	break;
    // case MODEL_A_NOTE:
    //	break;
    // case MODEL_702:
    //	break;
    // case MODEL_LG:
    //	break;
    // case MODEL_END:
    //	break;
    // default:
    //	break;
    //}

    // int* img = (int*)malloc(_width * _height * sizeof(int));
    // int* bkg = (int*)malloc(_width * _height * sizeof(int));
    // if (img != NULL && bkg != NULL && _pImg != NULL && _pBkg != NULL)
    //{
    //	for (i = 0; i < _width * _height; i++)
    //	{
    //		img[i] = _pImg[i];
    //		bkg[i] = _pBkg[i];
    //	}
    //}

    // int* diff = (int*)malloc(_width * _height * sizeof(int));

    // if (img != NULL && bkg != NULL && diff != NULL)
    //{
    //	for (i = 0; i < _height; i++)
    //	{
    //		for (j = 0; j < _width; j++)
    //		{
    //			diff[i * _width + j] = img[i * _width + j] - bkg[i * _width + j];
    //		}
    //	}
    //}

    ////analyze diff bkg ratio
    // int hist_threshold = 20;
    // float diff_bkg_ratio_pos_thres = 0;
    // float diffmax_bkg_ratio_pos_thres = 0;
    // float diff_bkg_ratio_neg_thres = 0;
    // int* hist_diff = NULL;
    // int* hist_diff_index = NULL;
    // int hist_diff_size = 0;
    // int hist_diff_min = 0, hist5_diff_max = 0, hist5_diff_min = 0;
    // int bkg_org_min = 0, bkg_size = 0, bkg_max, bkg_min = 0;
    // int room_light_score = 0;
    // int room_light_index = 0;
    // int hist_diff_intensity_max = 0;

    //{
    //	hist_diff = hist_maxmin(diff, _width, _height, fEt_HwInt, hist_threshold, &hist_diff_min,
    //&hist_diff_size, &hist5_diff_max, &hist5_diff_min, BAND_WIDTH);

    //	hist_diff_index = malloc(hist_diff_size * sizeof(int));

    //	if (hist_diff != NULL && hist_diff_index != NULL)
    //	{
    //		for (i = 0; i < hist_diff_size; i++)
    //		{
    //			hist_diff_index[i] = hist_diff_min + i * BAND_WIDTH * fEt_HwInt;
    //			hist_diff_intensity_max = hist_diff_intensity_max > hist_diff[i] ?
    // hist_diff_intensity_max : hist_diff[i];
    //		}

    //		int* hist_bkg = hist_maxmin(bkg, _width, _height, fEt_HwInt, hist_threshold,
    //&bkg_org_min, &bkg_size, &bkg_max, &bkg_min, BAND_WIDTH);

    //		if (hist_bkg != NULL)
    //		{
    //			diffmax_bkg_ratio_pos_thres = (double)hist_diff_index[hist_diff_size - 1] / (bkg_max
    //-  bkg_min); 			diff_bkg_ratio_pos_thres = (double)hist5_diff_max / (bkg_max - bkg_min);

    //			room_light_score = 0;
    //			for (i = hist_diff_size - 1; i >= 0; i--)
    //			{
    //				if (hist_diff[i] > MAX(hist_diff_intensity_max / 10, 50))
    //				{
    //					room_light_score = 100 - (double)i / hist_diff_size * 100;
    //					room_light_index = i;
    //					break;
    //				}
    //			}
    //			FREE(hist_bkg);
    //		}
    //	}
    //}

    // int light_thres = 0;
    // if (hist_diff_index[room_light_index] < 1000)
    //{
    //	light_thres = 1500;
    //}
    // else if (hist_diff_index[room_light_index] < 2000)
    //{
    //	light_thres = 2500;
    //}
    // else if (diff_bkg_ratio_pos_thres > 3 || (diffmax_bkg_ratio_pos_thres > 3 &&
    // diff_bkg_ratio_pos_thres > 1))
    //{
    //	light_thres = MAX(bkg_max, 4000);
    //}
    // else if (diff_bkg_ratio_pos_thres > 1.5 || (diffmax_bkg_ratio_pos_thres > 1.5 &&
    // diff_bkg_ratio_pos_thres > 1))
    //{
    //	light_thres = MAX(bkg_max, 2000);
    //}
    // else
    //{
    //	light_thres = bkg_max;
    //}

    // FREE(img);
    // FREE(bkg);
    // FREE(hist_diff);
    // FREE(hist_diff_index);

    // if (diff != NULL && _p8bit != NULL && _pPartialDetectorResult != NULL)
    //{
    //	split_cells(
    //		diff,
    //		_p8bit,
    //		_width, _height,
    //		_cx, _cy, _radius_boundary,
    //		_fptype,
    //		_modeltype,
    //		_lenstype,
    //		cell_width,
    //		stride,
    //		light_thres,
    //		_pPartialDetectorResult);
    //	FREE(diff);
    //}

    //_pPartialDetectorResult->room_light_score = room_light_score;

    // if (diff_bkg_ratio_pos_thres > 0)
    //{
    //	_pPartialDetectorResult->sun_light_score += diff_bkg_ratio_pos_thres * 45;
    //}
    // else
    //{
    //	_pPartialDetectorResult->sun_light_score = 0;
    //}
}

unsigned char* detect_partial_cell_A91(unsigned short* _pImg, unsigned short* _pBkg,
                                       unsigned char* _p8bit, int _width, int _height, int _cx,
                                       int _cy, int _radius_boundary, float _Et, int _HwInt,
                                       enum fp_type _fptype, enum model_type _modeltype,
                                       enum lens_type _lenstype,
                                       struct PartialDetectorResult* _pPartialDetectorResult) {
    int i, j, k;

    _pPartialDetectorResult->partial_ratio = 0;
    _pPartialDetectorResult->inner_partial_ratio = 0;
    _pPartialDetectorResult->room_light_partial_ratio = 0;
    _pPartialDetectorResult->inner_room_light_partial_ratio = 0;
    _pPartialDetectorResult->sun_light_score = 0;
    _pPartialDetectorResult->room_light_score = 0;

    int cell_width = 40;
    int stride = 20;
    float fEt_HwInt = 1;
    switch (_lenstype) {
        case LENS_UNKNOW:
            break;
        case LENS_2PA:
            break;
        case LENS_3PA:
            break;
        case LENS_3PC:
            break;
        case LENS_3PD:
            break;
        case LENS_3PE:
            break;
        case LENS_3PF:
            break;
        case LENS_END:
            break;
        default:
            break;
    }

    switch (_modeltype) {
        case MODEL_UNKNOW:
            break;
        case MODEL_A50:
            break;
        case MODEL_A70:
            break;
        case MODEL_A80:
            break;
        case MODEL_TAB_S6:
            break;
        case MODEL_A90:
            break;
        case MODEL_A30S:
            break;
        case MODEL_A50S:
            break;
        case MODEL_A70S:
            break;
        case MODEL_A51:
            break;
        case MODEL_A71:
            break;
        case MODEL_A91:
            cell_width = 40;
            stride = 20;
            break;
        case MODEL_A_NOTE:
            break;
        case MODEL_702:
            break;
        case MODEL_LG:
            break;
        case MODEL_END:
            break;
        default:
            break;
    }

    int* img = (int*)malloc(_width * _height * sizeof(int));
    int* bkg = (int*)malloc(_width * _height * sizeof(int));
    if (img != NULL && bkg != NULL && _pImg != NULL && _pBkg != NULL) {
        for (i = 0; i < _width * _height; i++) {
            img[i] = _pImg[i];
            bkg[i] = _pBkg[i];
        }
    }

    int* diff = (int*)malloc(_width * _height * sizeof(int));

    if (img != NULL && bkg != NULL && diff != NULL) {
        for (i = 0; i < _height; i++) {
            for (j = 0; j < _width; j++) {
                diff[i * _width + j] = img[i * _width + j] - bkg[i * _width + j];
            }
        }
    }

    // analyze diff bkg ratio
    int hist_threshold = 20;
    float diff_bkg_ratio_pos_thres = 0;
    float diffmax_bkg_ratio_pos_thres = 0;
    float diff_bkg_ratio_neg_thres = 0;
    int* hist_diff = NULL;
    int* hist_diff_index = NULL;
    int hist_diff_size = 0;
    int hist_diff_min = 0, hist5_diff_max = 0, hist5_diff_min = 0;
    int bkg_org_min = 0, bkg_size = 0, bkg_max, bkg_min = 0;
    int room_light_score = 0;
    int room_light_index = 0;
    int hist_diff_intensity_max = 0;

    {
        hist_diff = hist_maxmin(diff, _width, _height, fEt_HwInt, hist_threshold, &hist_diff_min,
                                &hist_diff_size, &hist5_diff_max, &hist5_diff_min, BAND_WIDTH);

        hist_diff_index = malloc(hist_diff_size * sizeof(int));

        if (hist_diff != NULL && hist_diff_index != NULL) {
            for (i = 0; i < hist_diff_size; i++) {
                hist_diff_index[i] = hist_diff_min + i * BAND_WIDTH * fEt_HwInt;
                hist_diff_intensity_max =
                    hist_diff_intensity_max > hist_diff[i] ? hist_diff_intensity_max : hist_diff[i];
            }

            int* hist_bkg = hist_maxmin(bkg, _width, _height, fEt_HwInt, hist_threshold,
                                        &bkg_org_min, &bkg_size, &bkg_max, &bkg_min, BAND_WIDTH);

            if (hist_bkg != NULL) {
                diffmax_bkg_ratio_pos_thres =
                    (double)hist_diff_index[hist_diff_size - 1] / (bkg_max - bkg_min);
                diff_bkg_ratio_pos_thres = (double)hist5_diff_max / (bkg_max - bkg_min);

                room_light_score = 0;
                for (i = hist_diff_size - 1; i >= 0; i--) {
                    if (hist_diff[i] > MAX(hist_diff_intensity_max / 10, 50)) {
                        room_light_score = 100 - (double)i / hist_diff_size * 100;
                        room_light_index = i;
                        break;
                    }
                }
                FREE(hist_bkg);
            }
        }
    }

    int light_thres = 0;
    if (hist_diff_index[room_light_index] < 1000) {
        light_thres = 1500;
    } else if (hist_diff_index[room_light_index] < 2000) {
        light_thres = 2500;
    } else if (diff_bkg_ratio_pos_thres > 3 ||
               (diffmax_bkg_ratio_pos_thres > 3 && diff_bkg_ratio_pos_thres > 1)) {
        light_thres = MAX(bkg_max, 4000);
    } else if (diff_bkg_ratio_pos_thres > 1.5 ||
               (diffmax_bkg_ratio_pos_thres > 1.5 && diff_bkg_ratio_pos_thres > 1)) {
        light_thres = MAX(bkg_max, 2000);
    } else {
        light_thres = bkg_max;
    }

#ifdef DEBUG
    FILE* file;
    if (fopen_s(&file, "hist_log.txt", "a") == 0) {
        fprintf(file,
                "diff_bkg_ratio_pos_thres %f diffmax_bkg_ratio_pos_thres %f room_light_score %i "
                "room_light_index %i light_thres %i\n",
                diff_bkg_ratio_pos_thres, diffmax_bkg_ratio_pos_thres, room_light_score,
                hist_diff_index[room_light_index], light_thres);
        fclose(file);
    }
#endif

    FREE(img);
    FREE(bkg);
    FREE(hist_diff);
    FREE(hist_diff_index);

    if (diff != NULL && _p8bit != NULL && _pPartialDetectorResult != NULL) {
        split_cells_a91(diff, _p8bit, _width, _height, _cx, _cy, _radius_boundary, _fptype,
                        _modeltype, _lenstype, cell_width, stride, light_thres,
                        _pPartialDetectorResult);
        FREE(diff);
    }

    _pPartialDetectorResult->room_light_score = room_light_score;

    if (diff_bkg_ratio_pos_thres > 0) {
        _pPartialDetectorResult->sun_light_score += diff_bkg_ratio_pos_thres * 45;
    } else {
        _pPartialDetectorResult->sun_light_score = 0;
    }

    unsigned char* out = (unsigned char*)malloc(_width * _height * sizeof(unsigned char));

#ifdef OUTPUT_MASK
    memcpy(out, mask, _width * _height * sizeof(unsigned char));
    FREE(mask);
#endif

    return out;
}

void detect_partial_cell_A91_SDK(unsigned short* _pImg, unsigned short* _pBkg,
                                 unsigned char* _p8bit, int _width, int _height, int _cx, int _cy,
                                 int _radius_boundary, float _Et, int _HwInt, enum fp_type _fptype,
                                 enum model_type _modeltype, enum lens_type _lenstype,
                                 struct PartialDetectorResult* _pPartialDetectorResult) {
    int i, j, k;

    _pPartialDetectorResult->partial_ratio = 0;
    _pPartialDetectorResult->inner_partial_ratio = 0;
    _pPartialDetectorResult->room_light_partial_ratio = 0;
    _pPartialDetectorResult->inner_room_light_partial_ratio = 0;
    _pPartialDetectorResult->sun_light_score = 0;
    _pPartialDetectorResult->room_light_score = 0;

    int cell_width = 40;
    int stride = 20;
    float fEt_HwInt = 1;
    switch (_lenstype) {
        case LENS_UNKNOW:
            break;
        case LENS_2PA:
            break;
        case LENS_3PA:
            break;
        case LENS_3PC:
            break;
        case LENS_3PD:
            break;
        case LENS_3PE:
            break;
        case LENS_3PF:
            break;
        case LENS_END:
            break;
        default:
            break;
    }

    switch (_modeltype) {
        case MODEL_UNKNOW:
            break;
        case MODEL_A50:
            break;
        case MODEL_A70:
            break;
        case MODEL_A80:
            break;
        case MODEL_TAB_S6:
            break;
        case MODEL_A90:
            break;
        case MODEL_A30S:
            break;
        case MODEL_A50S:
            break;
        case MODEL_A70S:
            break;
        case MODEL_A51:
            break;
        case MODEL_A71:
            break;
        case MODEL_A91:
            cell_width = 40;
            stride = 20;
            break;
        case MODEL_A_NOTE:
            break;
        case MODEL_702:
            break;
        case MODEL_LG:
            break;
        case MODEL_END:
            break;
        default:
            break;
    }

    int* img = (int*)malloc(_width * _height * sizeof(int));
    int* bkg = (int*)malloc(_width * _height * sizeof(int));
    if (img != NULL && bkg != NULL && _pImg != NULL && _pBkg != NULL) {
        for (i = 0; i < _width * _height; i++) {
            img[i] = _pImg[i];
            bkg[i] = _pBkg[i];
        }
    }

    int* diff = (int*)malloc(_width * _height * sizeof(int));

    if (img != NULL && bkg != NULL && diff != NULL) {
        for (i = 0; i < _height; i++) {
            for (j = 0; j < _width; j++) {
                diff[i * _width + j] = img[i * _width + j] - bkg[i * _width + j];
            }
        }
    }

    // analyze diff bkg ratio
    int hist_threshold = 20;
    float diff_bkg_ratio_pos_thres = 0;
    float diffmax_bkg_ratio_pos_thres = 0;
    float diff_bkg_ratio_neg_thres = 0;
    int* hist_diff = NULL;
    int* hist_diff_index = NULL;
    int hist_diff_size = 0;
    int hist_diff_min = 0, hist5_diff_max = 0, hist5_diff_min = 0;
    int bkg_org_min = 0, bkg_size = 0, bkg_max, bkg_min = 0;
    int room_light_score = 0;
    int room_light_index = 0;
    int hist_diff_intensity_max = 0;

    {
        hist_diff = hist_maxmin(diff, _width, _height, fEt_HwInt, hist_threshold, &hist_diff_min,
                                &hist_diff_size, &hist5_diff_max, &hist5_diff_min, BAND_WIDTH);

        hist_diff_index = malloc(hist_diff_size * sizeof(int));

        if (hist_diff != NULL && hist_diff_index != NULL) {
            for (i = 0; i < hist_diff_size; i++) {
                hist_diff_index[i] = hist_diff_min + i * BAND_WIDTH * fEt_HwInt;
                hist_diff_intensity_max =
                    hist_diff_intensity_max > hist_diff[i] ? hist_diff_intensity_max : hist_diff[i];
            }

            int* hist_bkg = hist_maxmin(bkg, _width, _height, fEt_HwInt, hist_threshold,
                                        &bkg_org_min, &bkg_size, &bkg_max, &bkg_min, BAND_WIDTH);

            if (hist_bkg != NULL) {
                diffmax_bkg_ratio_pos_thres =
                    (double)hist_diff_index[hist_diff_size - 1] / (bkg_max - bkg_min);
                diff_bkg_ratio_pos_thres = (double)hist5_diff_max / (bkg_max - bkg_min);

                room_light_score = 0;
                for (i = hist_diff_size - 1; i >= 0; i--) {
                    if (hist_diff[i] > MAX(hist_diff_intensity_max / 10, 50)) {
                        room_light_score = 100 - (double)i / hist_diff_size * 100;
                        room_light_index = i;
                        break;
                    }
                }
                FREE(hist_bkg);
            }
        }
    }

    int light_thres = 0;
    if (hist_diff_index[room_light_index] < 1000) {
        light_thres = 1500;
    } else if (hist_diff_index[room_light_index] < 2000) {
        light_thres = 2500;
    } else if (diff_bkg_ratio_pos_thres > 3 ||
               (diffmax_bkg_ratio_pos_thres > 3 && diff_bkg_ratio_pos_thres > 1)) {
        light_thres = MAX(bkg_max, 4000);
    } else if (diff_bkg_ratio_pos_thres > 1.5 ||
               (diffmax_bkg_ratio_pos_thres > 1.5 && diff_bkg_ratio_pos_thres > 1)) {
        light_thres = MAX(bkg_max, 2000);
    } else {
        light_thres = bkg_max;
    }

    FREE(img);
    FREE(bkg);
    FREE(hist_diff);
    FREE(hist_diff_index);

    if (diff != NULL && _p8bit != NULL && _pPartialDetectorResult != NULL) {
        split_cells_a91(diff, _p8bit, _width, _height, _cx, _cy, _radius_boundary, _fptype,
                        _modeltype, _lenstype, cell_width, stride, light_thres,
                        _pPartialDetectorResult);
        FREE(diff);
    }

    _pPartialDetectorResult->room_light_score = room_light_score;

    if (diff_bkg_ratio_pos_thres > 0) {
        _pPartialDetectorResult->sun_light_score += diff_bkg_ratio_pos_thres * 45;
    } else {
        _pPartialDetectorResult->sun_light_score = 0;
    }
}

unsigned char* detect_partial_cell_b(unsigned short* _pImg, unsigned short* _pBkg, int _width,
                                     int _height, float _Et, int _HwInt, enum fp_type _fptype,
                                     enum model_type _modeltype, enum lens_type _lenstype,
                                     struct PartialDetectorResult* _pPartialDetectorResult) {
    int i, j, k;

    int* img = (int*)malloc(_width * _height * sizeof(int));
    for (i = 0; i < _width * _height; i++) {
        img[i] = _pImg[i] - _pBkg[i];
    }

    find_finger_boundary(img, _width, _height,
                         //_fptype,
                         //_modeltype,
                         //_lenstype,
                         5, 5, _pPartialDetectorResult);

    FREE(img);

    unsigned char* out = (unsigned char*)malloc(_width * _height * sizeof(unsigned char));

#ifdef OUTPUT_MASK
    memcpy(out, mask, _width * _height * sizeof(unsigned char));
    FREE(mask);
#endif

    return out;
}

unsigned char* detect_partial_cell_702(unsigned short* _pImg, unsigned short* _pBkg,
                                       unsigned char* _p8bit, int _width, int _height, int _cx,
                                       int _cy, int _radius_boundary,
                                       // float _Et, int _HwInt,
                                       enum fp_type _fptype, enum model_type _modeltype,
                                       enum lens_type _lenstype,
                                       struct PartialDetectorResult* _pPartialDetectorResult) {
    int i, j, k;

    _pPartialDetectorResult->partial_ratio = 0;
    _pPartialDetectorResult->inner_partial_ratio = 0;
    _pPartialDetectorResult->room_light_partial_ratio = 0;
    _pPartialDetectorResult->inner_room_light_partial_ratio = 0;
    _pPartialDetectorResult->sun_light_score = 0;
    _pPartialDetectorResult->room_light_score = 0;

    int cell_width = 40;
    int stride = 20;

#ifdef EGIS_DEBUG
    egislog_d("Egis_debug partial 400");
#endif
    if (_pImg != NULL && _pBkg != NULL && _p8bit != NULL && _pPartialDetectorResult != NULL) {
        split_cells_702(_pImg, _pBkg, _p8bit, _width, _height, _cx, _cy, _radius_boundary, _fptype,
                        _modeltype, _lenstype, cell_width, stride, 16000, _pPartialDetectorResult);
    }

#ifdef EGIS_DEBUG
    egislog_d("Egis_debug partial 401");
#endif

    unsigned char* out = (unsigned char*)malloc(_width * _height * sizeof(unsigned char));

#ifdef OUTPUT_MASK
    memcpy(out, mask, _width * _height * sizeof(unsigned char));
    FREE(mask);
#endif

    return out;
}
