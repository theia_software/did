#include "ipsys.h"
#include "utility.h"

//int *IPgaussian_blur(int *img, int *kernel_2e15, int window, int width, int height){
//	int *img_copy = (int*)IPAlloc_760(width * height * sizeof(int));
//	IPCopyMem(img_copy, img, width * height * sizeof(int));
//	int *img_smooth_16 = IPconvolution_symmetric_760(img_copy, width, height, kernel_2e15, window);
//	IPFree(img_copy);
//	return img_smooth_16;
//}



void IPconvolution_symmetric_buf_2e4_760(int *img16_src, int *img16_dst, int width, int height, int *kernel_2e4, int ksize) {
	int i, j, *idx, k, *row = img16_src, *p, sum, pos = 0;
	idx = build_kernel_idx_760(width, ksize);

	for (i = 0; i < height; i++, row += width) {
		for (j = 0, p = idx; j < width; j++, p++) {
			for (k = 0, sum = 0; k < ksize; k++)
				sum += row[p[k]] * kernel_2e4[k];
			img16_dst[pos++] = (sum + (1 << 3)) >> 4;
		}
	}
	IPFree_760((void*)idx);
	IPCopyMem(img16_src, img16_dst, width*height*sizeof(int));
	idx = build_kernel_idx_760(height, ksize);
	for (i = 0; i < height + (ksize & 0xFE); i++) idx[i] *= width; //convert idx to offset
	for (j = 0; j < width; j++, img16_src++) {
		for (i = 0, p = idx; i < height; i++, p++) {
			for (k = 0, sum = 0; k < ksize; k++)
				sum += img16_src[p[k]] * kernel_2e4[k];
			img16_dst[j + i*width] = (sum + (1 << 3)) >> 4;
		}
	}
	IPFree_760((void*)idx);
}
int *IPconvolution_symmetric_2e4_760(int *img16, int width, int height, int *kernel_2e4, int ksize) {
	int *img16_dst = IPAllocIntRow_760(width*height);
	IPconvolution_symmetric_buf_2e4_760(img16, img16_dst, width, height, kernel_2e4, ksize);
	return img16_dst;
}
int *IPgaussian_blur_2e4_760(int *img, int *kernel_2e4, int window, int width, int height){
	int *img_copy = (int*)IPAlloc_760(width * height * sizeof(int));
	IPCopyMem(img_copy, img, width * height * sizeof(int));
	int *img_smooth_16 = IPconvolution_symmetric_2e4_760(img_copy, width, height, kernel_2e4, window);
	IPFree_760(img_copy);
	return img_smooth_16;
}



void IPconvolution_symmetric_buf_2e8_760(int *img16_src, int *img16_dst, int width, int height, int *kernel_2e8, int ksize) {
	int i, j, *idx, k, *row = img16_src, *p, sum, pos = 0;
	idx = build_kernel_idx_760(width, ksize);

	for (i = 0; i < height; i++, row += width) {
		for (j = 0, p = idx; j < width; j++, p++) {
			for (k = 0, sum = 0; k < ksize; k++)
				sum += row[p[k]] * kernel_2e8[k];
			img16_dst[pos++] = (sum + (1 << 7)) >> 8;
		}
	}
	IPFree_760((void*)idx);
	IPCopyMem(img16_src, img16_dst, width*height*sizeof(int));
	idx = build_kernel_idx_760(height, ksize);
	for (i = 0; i < height + (ksize & 0xFE); i++) idx[i] *= width; //convert idx to offset
	for (j = 0; j < width; j++, img16_src++) {
		for (i = 0, p = idx; i < height; i++, p++) {
			for (k = 0, sum = 0; k < ksize; k++)
				sum += img16_src[p[k]] * kernel_2e8[k];
			img16_dst[j + i*width] = (sum + (1 << 7)) >> 8;
		}
	}
	IPFree_760((void*)idx);
}
int *IPconvolution_symmetric_2e8_760(int *img16, int width, int height, int *kernel_2e8, int ksize) {
	int *img16_dst = IPAllocIntRow_760(width*height);
	IPconvolution_symmetric_buf_2e8_760(img16, img16_dst, width, height, kernel_2e8, ksize);
	return img16_dst;
}
int *IPgaussian_blur_2e8_760(int *img, int *kernel_2e8, int window, int width, int height){
	int *img_copy = (int*)IPAlloc_760(width * height * sizeof(int));
	IPCopyMem(img_copy, img, width * height * sizeof(int));
	int *img_smooth_16 = IPconvolution_symmetric_2e8_760(img_copy, width, height, kernel_2e8, window);
	IPFree_760(img_copy);
	return img_smooth_16;
}