#ifdef __cplusplus
extern "C"
{
#endif

#ifndef ISP_TMAP_ROI_H
#define ISP_TMAP_ROI_H

void tmp_roi_v2e(int *img16_src, int *dst, int w, int h, int roi_win, int ext_rate_2e10, int tmp_low, int tmp_high, int gSigmaHigh, int gSigmaLow, int thSigmaOutlier);

#endif


#ifdef __cplusplus
}
#endif