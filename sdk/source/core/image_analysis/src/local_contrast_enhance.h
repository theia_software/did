#ifdef __cplusplus
extern "C"
{
#endif

#ifndef ISP_LOCAL_CONTRAST_ENHANCE_H
    #define ISP_LOCAL_CONTRAST_ENHANCE_H
	void local_contrast_enhance_v1_760(int *img, int *dst, int w, int h, int window, const int alpha_2e4, const int contrast_gain_max_2e12, const int gobal_std, int max_val);
#endif


#ifdef __cplusplus
}
#endif