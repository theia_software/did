/*********************************************
* @file    IPimage_760_IPP6_61b.c
*
* @author  Cliff Lo
* @version v6.61a
* @date    2020/09
* @brief   Fingerprint image processing
*          Adoptive project: ET760=200x200 (height x width)
*                            This function avoids the bad/dead pixel and dust make dynamic range incorrect.
* @fn      void IPimage_760_IPP6_61a(int *img, unsigned char *dst, int w, int h, int gSigmaHigh, int gSigmaLow, int thSigmaOutlier)
* @param   int *img => Image input (16-bit)
* @param   unsigned char *dst => Image output (8-bit)
* @param   int w, int h => Image width and height (cols x rows)
* @param   int gSigmaHigh,  int gSigmaLow => the gain of high and low boundary is the times of sigma.
* @param   int thSigmaOutlier => a threshold to the boundary is a times of sigma. 
*
* \note    v6.61b modified
*******    @brief   Based on ET760 fingerprint reflection lighting estimates the dynamic range of fingerprint.
					The function of tmp_roi_v2e adjusts the major mapping range when the standard deviation was changed. 
					Simplify the value of kernel at IPgaussian_blur_2e4_760
*******    @fn      IPimage_760_IPP6_61b(int *img, unsigned char *dst, int w, int h, int gSigmaHigh, int gSigmaLow, int thSigmaOutlier, 
*                                        int lce_gain_max_2e12, int lce_alpha_2e4, int clip_limit_param_2e10,
*                                        int tmp_low, int tmp_high)
*******    @param   int lce_gain_max_2e12 => max. gain limitation in function "local_contrast_enhance_v1"
*******    @param   int lce_alpha_2e4 => local signal gain in function "local_contrast_enhance_v1"
*******    @param   int clip_limit_param_2e10 => input parameter "clip_limit_param" multiple with 1024 in function "clahe" 
*******    @param   int tmp_low, tmp_high => max. and min. value of tone mapping output in function "tmp_roi_v2e"
*
**********************************************************************/
#include "utility.h"
#include "ipsys.h"
#include "tmp_roi_v2e.h"
#include "local_contrast_enhance.h"
#include "clahe.h"
#include "IPgaussian_blur.h"

void IPimage_760_IPP6_61b(int *img, unsigned char *dst, int w, int h, int gSigmaHigh, int gSigmaLow, 
	                      int thSigmaOutlier, int lce_gain_max_2e12, int lce_alpha_2e4, int clip_limit_param_2e10, 
						  int tmp_low, int tmp_high, int idx)
{
	const int roi_win = 100, ext_rate_2e10 = 1024 / 100;
	const int lce_gobal_std = 30;
	int glpf_win3 = 3, lce_win = 9;
	
	const int KERNEL_3_2E4[] = { 4, 8, 4 };
	int *img_src = (int*)IPAlloc_760(w * h * sizeof(int));
	int *img_lce = (int*)IPAlloc_760(w * h * sizeof(int));

	char name[512] = {0};
	// REMOVING SCALE_1 
	tmp_roi_v2e(img, img_src, w, h, roi_win, ext_rate_2e10, tmp_low, tmp_high, gSigmaHigh, gSigmaLow, thSigmaOutlier);
//	sprintf(name, "/mnt/sdcard/760files/roi_%02d.bin", idx);
//	plat_save_file(name, (unsigned char*)img_src, w * h * sizeof(int));

	local_contrast_enhance_v1_760(img_src, img_lce, w, h, lce_win / 2, lce_alpha_2e4, lce_gain_max_2e12, lce_gobal_std, 0xFF);
//	sprintf(name, "/mnt/sdcard/760files/lce_%02d.bin", idx);
//	plat_save_file(name, (unsigned char*)img_lce, w * h * sizeof(int));

	IPFree_760(img_src);
	int *img_lce_flt = IPgaussian_blur_2e4_760(img_lce, (int*)KERNEL_3_2E4, glpf_win3, w, h);
//    sprintf(name, "/mnt/sdcard/760files/gaussian_%02d.bin", idx);
//    plat_save_file(name, (unsigned char*)img_lce_flt, w * h * sizeof(int));
	IPFree_760(img_lce);
	clahe(img_lce_flt, w, h, 0xFF, 25, 128, clip_limit_param_2e10); //8 bit data range
//	sprintf(name, "/mnt/sdcard/760files/clahe_%02d.bin", idx);
//	plat_save_file(name, (unsigned char*)img_lce_flt, w * h * sizeof(int));

	int *img_claht_flt = IPgaussian_blur_2e4_760(img_lce_flt, (int*)KERNEL_3_2E4, glpf_win3, w, h);
	IPFree_760(img_lce_flt);
	IPcast16_to8_760(dst, img_claht_flt, w*h);
	IPFree_760(img_claht_flt);
}
