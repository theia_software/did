/*********************************************
* @file    local_contrast_enhance_v1.c
*
* @author  Cliff Lo
* @version v1.0
* @date    2019/12
* @brief   Local contrast enhancement and background replacement (target mean)
* @fn      void local_contrast_enhance_v1(int *img, int *dst, int w, int h, int window, const int alpha_2e4, const int contrast_gain_max_2e12, const int gobal_std, int max_val)
* @param   int *img                            => Image input (8-bit)
* @param   int *dst                            => Image output (8-bit)
* @param   int w, int h                        => Image width and height (cols x rows)
* @param   int window                          => local window size (default=9)
* @param   int alpha_2e15                      => local signal gain
* @param   int contrast_gain_max_2e15          => max. gain limitation
* @param   int gobal_mean                      => target mean after enhancement
* @param   int gobal_std                       => target std after enhancement
*
* \note    v0.1 1st version
*
* \note    v1.0 modified
*******    keep the signal dynamic if the signal std is bigger than "gobal_std."
*
************************************************/
#include "ipsys.h"
#include "utility.h"

void local_contrast_enhance_v1_760(int *img, int *dst, int w, int h, int window, const int alpha_2e4, const int contrast_gain_max_2e12, const int gobal_std, int max_val){
	const int gobal_mean = (max_val + 1) / 2, gain_max_sqr = IPSqr(contrast_gain_max_2e12) >> 24, gobal_std_sqr = IPSqr(gobal_std);
	const int alpha_gain_max = alpha_2e4 * contrast_gain_max_2e12, alpha_std = alpha_2e4 * (gobal_std << 12);
	int i, diff, value, alpha_gain_2e16;
	int *img_local_mean = IPaverage_to_v2_760(img, w, h, window);
	int *img_src_sqr = matrix_dot_760(img, img, w, h);
	int *img_local_variance = IPaverage_to_v2_760(img_src_sqr, w, h, window);
	int stdLocal = 1;
	IPFree_760(img_src_sqr);
	for (i = 0; i < w * h; i++){
		int local_variance = img_local_variance[i] - IPSqr(img_local_mean[i]); //TBD, bug for unsigned int
		diff = img[i] - img_local_mean[i];
		if (local_variance <= 0 || diff == 0) {
			dst[i] = gobal_mean;
		}
		else {
			/* before optimize
			std = IPsqrt_2e10(local_variance);
			gain = (gobal_std <<12) / std;
			if (gain > contrast_gain_max_2e12) {
			gain_limit = contrast_gain_max_2e12 * diff;
			}
			else{
			gain_limit = gain;
			}
			gain_limit_sqr = gain_limit * diff;
			int value = gobal_mean + (alpha_2e4 * gain_limit_sqr +(1<<8)) / (1<<16);
			*/
			if (gobal_std_sqr > gain_max_sqr*local_variance) {
				alpha_gain_2e16 = alpha_gain_max * diff;
			}
			else {
				//cliff_20191201 
				stdLocal = IPsqrt_2e10_760(local_variance);
				if (stdLocal>gobal_std){
					//                    cout<<"stdLocal, gobal_std = "<<stdLocal<<", "<<gobal_std<<endl;
					alpha_gain_2e16 = alpha_2e4* (1 << 12) * diff;
				}
				else
					alpha_gain_2e16 = alpha_std * diff / stdLocal;
				//cliff_20191201 end
			}
			value = gobal_mean + (alpha_gain_2e16 + (1 << 15)) / (1 << 16);
			if (value > max_val)
				dst[i] = max_val;
			else if (value < 0)
				dst[i] = 0;
			else dst[i] = value;
		}
	}
	IPFree_760(img_local_mean);
	IPFree_760(img_local_variance);
}