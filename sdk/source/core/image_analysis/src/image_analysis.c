#include "image_analysis.h"
#include "ImageProcessingLib.h"
#include "egis_definition.h"
#include "plat_log.h"
#include "plat_mem.h"
#include "string.h"
#include "type_definition.h"

#define _Max(a, b) ((b) > (a) ? (b) : (a))
#define _Min(a, b) ((b) < (a) ? (b) : (a))
#define RESAMPLE_DPI(x) \
    ((((x * 1434 / 1024) + 15) / 16) * 16)  // dpi resample ratio, 508/363 ~ 1.4 ~ 1434/1024

int resample_IsFPImage_Lite(unsigned char* image, int width, int height, int* corner_count,
                            int* cover_count, int is_raw_image, int* intensity) {
    int qty;
#ifdef G3_MATCHER
    int widthResample = RESAMPLE_DPI(width);
    int heightResample = RESAMPLE_DPI(height);
    unsigned char* out_img = plat_alloc(widthResample * heightResample);
    memcpy(out_img, image, width * height);
    IPresample(out_img, width, height, widthResample, heightResample);
    qty = IPis_fp_image_lite_core(out_img, widthResample, heightResample, corner_count, cover_count,
                                  0);

    if (intensity != NULL) {
        *intensity =
            IPcount_average_intensity(out_img, widthResample, heightResample, widthResample, 0);
    }
    plat_free(out_img);
#else
    qty = 0;
    ex_log(LOG_ERROR, "%s not supported", __func__);
#endif
    return qty;
}

void image_16bitsTo8bits(unsigned short* raw_16bits, unsigned char* raw_8bits, int width,
                         int height) {
    int min = 256 * 256;
    int max = 0;
    int i, ratio;
    unsigned short value;

    const int number_ignore_line = 3;
    for (int i = number_ignore_line; i < height - number_ignore_line; i++) {
        for (int j = number_ignore_line; j < width - number_ignore_line; j++) {
            value = raw_16bits[i * width + j];
            if (min > value) min = value;
            if (max < value) max = value;
        }
    }
    if (max == min) {
        ratio = 1;
    } else {
        ratio = max - min;
    }
    for (i = 0; i < width * height; i++) {
        raw_8bits[i] = _Min(255, _Max(0, 256 * (raw_16bits[i] - min) / ratio));
    }
}