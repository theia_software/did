/*********************************************
* @file    tmp_roi_v2e.c
*
* @author  Cliff Lo
* @version v2.0b
* @date    2019/12
* @brief   tone mapping the signal from 16-bit to 8-bit
* @fn      void tmp_roi_v2b(int *img16_src_2e11, int *dst, int w, int h, int roi_win, int ext_rate_2e10, int tmp_low_2e11, int tmp_high_2e11, int gSigmaHigh, int gSigmaLow, int thSigmaOutlayer)
* @param   int *img16_src                      => Image input (16-bit)
* @param   int *dst                            => Image input (8-bit)
* @param   int w, int h                        => Image width and height (cols x rows)
* @param   int roi_win                         => roi window size (default=100)
* @param   int ext_rate_2e10                   => Extending percentage of dynamic range in ROI region
* @param   int tmp_low_2e11, int tmp_high_2e11 => max. and min. value of tone mapping output
* @param   int gSigmaHigh, int gSigmaLow       => boundary of dynamic range = mean - gSigmaLow x std < x < mean - gSigmaHigh x std
* @param   int thSigmaOutlayer                 => A threshold of dynamic range std = mean +- thSigmaOutlayer x std 
*
* \note    v2.0a 
*
* \note    v2.0b
*******     removed the multiplying factor "SCALE_1" 
* \note    v2.0c
*******     make a limitation to transformed ratio (slope) less than 1 in ROI region. 
* \note    v2.0d
*******     make a maxium limitation to std in ROI region. 
* \note    v2.0e for ET760+AUO's TFT sensor
*******     based on v2b, this method is going to estimate fingerprint dynamic range in ROI region. 
*
************************************************/
#include "ipsys.h"
#include "utility.h"
//#define _DEBUG //ELSE
void tmp_roi_v2e(int *img16_src, int *dst, int w, int h, int roi_win, int ext_rate_2e10, int tmp_low, int tmp_high, int gSigmaHigh, int gSigmaLow, int thSigmaOutlier) {
	int i, j, *img16;
	int max_src = 0, min_src = 65535, max_roi = 0, min_roi = 65535;
	int roi_left_col = (w - roi_win) / 2;
	int roi_top_row = (h - roi_win) / 2;
	int roi_right_col = roi_left_col + roi_win - 1;
	int roi_bottom_row = roi_top_row + roi_win - 1;
	int roi_mean, roi_std;
	int sum = 0;
	long long sum_square = 0;
	int var;
	int roi_size = roi_win * roi_win;
	for (i = 0, img16 = img16_src; i < h; i++, img16 += w) {
		for (j = 0; j < w; j++) {
			if (img16[j] > max_src) max_src = img16[j];
			if (img16[j] < min_src) min_src = img16[j];
			if (i >= roi_top_row && i <= roi_bottom_row && j >= roi_left_col && j <= roi_right_col) {
				if (img16[j] > max_roi) max_roi = img16[j];
				if (img16[j] < min_roi) min_roi = img16[j];
				sum += img16[j];
				sum_square += img16[j] * img16[j];
			}
		}
	}

	if (max_src == min_src) return;

	roi_mean = (int)(sum + (roi_size >> 1)) / roi_size;
	var = (unsigned int)((sum_square - (long long)(roi_mean)* sum) / (roi_size - 1));
	roi_std = (int) isqrt_760(var);
	
	if (roi_std>600){ //500*(4-2)+500*(4-1) = 2500, sunlight/chamber_partial
		max_roi = roi_mean + 2500;
		min_roi = roi_mean - 1500;
	}else if (roi_std>500){ //500*(4-2)+500*(4-1) = 2500, sunlight/chamber_partial
		max_roi = roi_mean + 1500;
		min_roi = roi_mean - 1500;
	}else if (roi_std>400){ //400*(4)+400*(4-1) = 2800, sunlight/chamber_partial
		max_roi = roi_mean + (gSigmaHigh ) * roi_std;
		min_roi = roi_mean - (gSigmaLow  - 1)* roi_std;
	}else if (roi_std>300){ //300*(4+1)+300*4 = 2700, sunlight condition/chamber_partial
		max_roi = roi_mean + (gSigmaHigh+1)  * roi_std;
		min_roi = roi_mean - gSigmaLow * roi_std;
	}else if (roi_std>200){ //200*(4+2)+200*4= 2000, normal/wash /chamber
		max_roi = roi_mean + (gSigmaHigh +2 ) * roi_std;
		min_roi = roi_mean - gSigmaLow * roi_std;
	}else if (roi_std>150){ //150*(4+2)+150*(4+1)=1650, normal/wash/chamber
		max_roi = roi_mean + (gSigmaHigh + 2) * roi_std;
		min_roi = roi_mean - (gSigmaLow + 1)* roi_std;
	}else { //50*(4+1)*2=500, chamber
		max_roi = roi_mean + 800;
		min_roi = roi_mean - 800;
	}
	max_roi = (max_roi > max_src) ? max_src : max_roi;
	min_roi = (min_roi < min_src) ? min_src : min_roi;

#ifdef _DEBUG
	printf("roi_mean, roi_std = %d, %d\n", roi_mean, roi_std);
	printf("max_roi = %d\n", max_roi);
	printf("min_roi = %d\n", min_roi); 
#endif
	
	//    cout<<"big_diff_max,  big_diff_min= "<<big_diff_max<<", "<<big_diff_min<<endl;
	//    cout<<"max_roi, min_roi, roi_mean, roi_std = "<<max_roi<<", "<<min_roi<<", "<<roi_mean*SCALE_1<<", "<<roi_std*SCALE_1<<endl;

	int n_ext = ext_rate_2e10 * (max_roi - min_roi) / 1024;
	int src_low = min_roi - n_ext;
	int src_high = max_roi + n_ext;
	int coef_rate1_2e16 = 65536, coef_rate3_2e16 = 65536;

	if (src_low <= min_src) {
		src_low = min_roi;
		if (min_roi > min_src)
			coef_rate1_2e16 = (tmp_low * 65536) / (min_roi - min_src);
		else
			tmp_low = 0;
	}
	else
		coef_rate1_2e16 = (tmp_low * 65536) / (src_low - min_src);

	if (src_high >= max_src) {
		src_high = max_roi;
		if (max_roi < max_src)
			coef_rate3_2e16 = (255 - tmp_high) * 65536 / (max_src - max_roi);
		else
			tmp_high = 255;
	}
	else {
		coef_rate3_2e16 = (255 - tmp_high) * 65536 / (max_src - src_high);
	}

	int coef_rate2_2e16 = (src_high <= src_low) ? 65536 : (tmp_high - tmp_low) * 65536 / (src_high - src_low);
	int px;
	for (i = 0; i < w * h; i++) {
		if (img16_src[i] < src_low) {
			px = coef_rate1_2e16 * (img16_src[i] - min_src) / 65536;
		}
		else if (img16_src[i] > src_high) {
			px = coef_rate3_2e16 * (img16_src[i] - src_high) / 65536 + tmp_high;
		}
		else {
			px = coef_rate2_2e16 * (img16_src[i] - src_low) / 65536 + tmp_low;
		}
		dst[i] = px > 255 ? 255 : px;
	}
}