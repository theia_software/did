#include "detect_scratch.h"
#include "EgisAlgorithmAPI.h"
#include "core_config.h"
#include "egis_sprintf.h"
#include "fp_custom.h"
#include "ini_definition.h"
#include "plat_file.h"
#include "plat_log.h"
#include "plat_mem.h"

static void* g_scratch_info = NULL;
static unsigned int g_scratch_info_len = 0;
static int g_scratch_ratio = 0;
#define SCRATCH_RATIO_THRESHOLD 3

#define SCRATCH_NAME "scratch.bin"

int detect_scratch_update(uint16_t* img16bit, int width, int height, int hw_count) {
    RBS_CHECK_IF_NULL(g_scratch_info, EGIS_COMMAND_FAIL);
    int level = 3;
    int* img32bit = plat_alloc(width * height * sizeof(int));
    for (int i = 0; i < width * height; i++) img32bit[i] = img16bit[i];

    ex_log(LOG_DEBUG, "%s (%p)  %d, %d:%d, level=%d", __func__, g_scratch_info, hw_count, width,
           height, level);
    int retval = api_scratch_detect(img32bit, width, height, hw_count, level, g_scratch_info,
                                    &g_scratch_ratio);
    ex_log(LOG_DEBUG, "%s (%p) ratio=%d", __func__, g_scratch_info, g_scratch_ratio);
    if (retval != FP_OK) {
        ex_log(LOG_ERROR, "api_scratch_detect failed %d", retval);
        if (retval == FP_SCRATCH_INT_ERROR) {
            ex_log(LOG_ERROR, "scratch int error, do uninit and init");
            detect_scratch_remove();
            detect_scratch_uninit();
            detect_scratch_init(width, height);
        }
    }
    plat_free(img32bit);
    return 0;
}

int detect_scratch_do_mask(unsigned char* feat, int* len, int width, int height) {
    int retval;
    if (!detect_scratch_is_detected()) {
        ex_log(LOG_DEBUG, "%s skip %d (%d) %d:%d", __func__, g_scratch_ratio,
               SCRATCH_RATIO_THRESHOLD, width, height);
        return EGIS_OK;
    }
    ex_log(LOG_DEBUG, "%s run %d (%d) %d:%d", __func__, g_scratch_ratio, SCRATCH_RATIO_THRESHOLD,
           width, height);
    retval = api_scratch_mask(feat, len, width / 2, height / 2);
    if (retval != EGIS_OK) {
        ex_log(LOG_ERROR, "api_scratch_mask failed %d", retval);
    }

    // reset g_scratch_ratio
    g_scratch_ratio = 0;
    return retval;
}

BOOL detect_scratch_is_detected() {
    int enable_sratch_mask =
        core_config_get_int(INI_SECTION_VERIFY, KEY_DETECT_SCRATCH_MASK, INID_DETECT_SCRATCH_MASK);
    if (!enable_sratch_mask) {
        return FALSE;
    }

    return (g_scratch_ratio > SCRATCH_RATIO_THRESHOLD) ? TRUE : FALSE;
}

int detect_scratch_init(int width, int height) {
#ifndef __TRUSTONIC__
    char filename[PATH_MAX];
    int scratch_len = api_get_scratch_data_size(width, height);
    int retSize = 0;
    ex_log(LOG_DEBUG, "%s, scratch_len=%d (%d:%d)", __func__, scratch_len, width, height);
    if (g_scratch_info == NULL) {
        unsigned char* pscratch = plat_alloc(scratch_len);
        RBS_CHECK_IF_NULL(pscratch, EGIS_OUT_OF_MEMORY);

        memset(pscratch, 0, scratch_len);
        g_scratch_info = (void*)pscratch;
        g_scratch_info_len = scratch_len;

        egist_snprintf(filename, PATH_MAX, "%s%s", FILE_DATA_BASE, SCRATCH_NAME);
        retSize = plat_load_file(filename, (unsigned char*)g_scratch_info, scratch_len,
                                 (unsigned int*)&scratch_len);
        if ((retSize <= 0) || (api_scratch_data_check(g_scratch_info) != FP_OK)) {
            ex_log(LOG_ERROR, "scratch data not exist or check error");
            memset(pscratch, 0, g_scratch_info_len);
            api_scratch_data_init(g_scratch_info);
        }
    }
#endif
    return 0;
}
int detect_scratch_save() {
    int retSize = 0;
#ifndef __TRUSTONIC__
    if (g_scratch_info_len > 0) {
        char filename[255];
        egist_snprintf(filename, 255, "%s%s", FILE_DATA_BASE, SCRATCH_NAME);
        retSize = plat_save_file(filename, (unsigned char*)g_scratch_info, g_scratch_info_len);
        if (retSize > 0) {
            ex_log(LOG_DEBUG, "%s is saved (%d)", __func__, retSize);
            return EGIS_OK;
        }
    }
#endif
    ex_log(LOG_ERROR, "%s %d ", __func__, retSize);
    return EGIS_COMMAND_FAIL;
}

int detect_scratch_remove() {
    char filename[255];
    egist_snprintf(filename, 255, "%s%s", FILE_DATA_BASE, SCRATCH_NAME);
    plat_remove_file(filename);
    return EGIS_OK;
}

void detect_scratch_uninit() {
    PLAT_FREE(g_scratch_info);
    g_scratch_info_len = 0;
}