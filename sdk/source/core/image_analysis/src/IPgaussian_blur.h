#ifdef __cplusplus
extern "C"
{
#endif

#ifndef ISP_GAUSSIAN_BLUR_H
#define ISP_GAUSSIAN_BLUR_H
	int *IPgaussian_blur_2e4_760(int *img, int *kernel_2e4, int window, int width, int height);
#endif


#ifdef __cplusplus
}
#endif