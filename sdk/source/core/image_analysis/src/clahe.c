/*********************************************
* @file    clahe.c
*
* @author  Cliff Lo
* @version v1.1
* @date    2019/03/28
* @brief   Contrast Limited Adaptive histogram equalization (CLAHE)
* @fn      int clahe(int *image, int width, int height, int max, int block_sz, int nr_bins, int clip_limit_param)
* @param   int *image => Image input and output
* @param   int width, int height => Image width and height (cols x rows)
* @param   int max => maximum value of output image
* @param   int block_sz => block size of histogram
* @param   int nr_bins => Number of greybins for histogram ("dynamic range")
* @param   int clip_limit_param => Normalized cliplimit (higher values give more contrast)
* \note    v1.0 first version
* 
* \note    v1.1 modified
*******    @fn      int clahe(int *image, int width, int height, int max, int block_sz, int nr_bins, int clip_limit_param_2e10)
*******    @param   int clip_limit_param_2e10 => input parameter "clip_limit_param" multiple with 1024
************************************************/
#include "ipsys.h"
#include "utility.h"

unsigned short *create_lookup_tbl(int max, int nr_bins){
	unsigned short *tbl = (unsigned short *)IPAllocShortRow(max + 1);
	int i, bin_size = (1 + max) / nr_bins;

	for (i = 0; i <= max; i++) tbl[i] = i / bin_size;
	return tbl;
}
void make_block_histogram(int *image, int width, int wb_sz, int hb_sz, int* hist,int nr_gray_levels, unsigned short* lut){
	int i, j;

	IPFillMem(hist, 0, nr_gray_levels*sizeof(int));
	for (i = 0; i < hb_sz; i++, image += width) {
		for (j = 0; j < wb_sz; j++) {
			hist[lut[image[j]&0xFFFF]]++;
		}
	}
}
void clip_histogram(int* hist, int nr_gray_levels, int clip_limit) {
	int* bin_ptr, *end_ptr, *histo;
	int nr_excess, upper, bin_incr, step_size, i;
	int bin_excess;

	nr_excess = 0;  bin_ptr = hist;
	for (i = 0; i < nr_gray_levels; i++) {
		bin_excess = (int)bin_ptr[i] - (int)clip_limit;
		if (bin_excess > 0) nr_excess += bin_excess;
	}

	bin_incr = nr_excess / nr_gray_levels;
	upper = clip_limit - bin_incr;

	for (i = 0; i < nr_gray_levels; i++) {
		if (hist[i] > clip_limit) hist[i] = clip_limit;
		else {
			if (hist[i] > upper) {
				nr_excess -= hist[i] - upper; hist[i] = clip_limit;
			}
			else {
				nr_excess -= bin_incr; hist[i] += bin_incr;
			}
		}
	}

	while (nr_excess) {
		end_ptr = &hist[nr_gray_levels]; histo = hist;

		while (nr_excess && histo < end_ptr) {
			step_size = nr_gray_levels / nr_excess;
			if (step_size < 1) step_size = 1;
			for (bin_ptr = histo; bin_ptr < end_ptr && nr_excess; bin_ptr += step_size) {
				if (*bin_ptr < clip_limit) {
					(*bin_ptr)++;
					nr_excess--;
				}
			}
			histo++;
		}
	}
}
void map_histogram(int* hist, int max, int nr_gray_levels, int nr_of_pixels){
	int i;  int sum = 0;
	int scale_2e15 = (max * 32768 + nr_of_pixels /2) / nr_of_pixels;

	for (i = 0; i < nr_gray_levels; i++) {
		sum += hist[i]; 
		hist[i] = (sum * scale_2e15 + 16384) / 32768;
		if (hist[i] > max) hist[i] = max;
	}
}
void interpolate(int *image, int width, int * map_lu,
	int * map_ru, int * map_lb, int * map_rb,
	int x_size, int y_size, unsigned short *lut){
	const unsigned int incr = width - x_size;
	unsigned short gray_value; 
	int num = x_size*y_size;

	int x_coef, y_coef, x_inv_coef, y_inv_coef, shift = 0;

	if (num & (num - 1)) {
		for (y_coef = 0, y_inv_coef = y_size; y_coef < y_size;
			y_coef++, y_inv_coef--, image += incr) {
			for (x_coef = 0, x_inv_coef = x_size; x_coef < x_size;
				x_coef++, x_inv_coef--) {
				gray_value = lut[(*image)&0xFFFF];
				*image++ = (int)((y_inv_coef * (x_inv_coef*map_lu[gray_value]
					+ x_coef * map_ru[gray_value])
					+ y_coef * (x_inv_coef * map_lb[gray_value]
					+ x_coef * map_rb[gray_value])) / num);
			}
		}
	}
	else {
		while (num >>= 1) shift++;
		for (y_coef = 0, y_inv_coef = y_size; y_coef < y_size;
			y_coef++, y_inv_coef--, image += incr) {
			for (x_coef = 0, x_inv_coef = x_size; x_coef < x_size;
				x_coef++, x_inv_coef--) {
				gray_value = lut[(*image)&0xFFFF];
				*image++ = (int)((y_inv_coef* (x_inv_coef * map_lu[gray_value]
					+ x_coef * map_ru[gray_value])
					+ y_coef * (x_inv_coef * map_lb[gray_value]
					+ x_coef * map_rb[gray_value])) >> shift);
			}
		}
	}
}
int clahe(int *image, int width, int height, int max, int block_sz, int nr_bins, int clip_limit_param_2e10){
	int x, y;
	int sub_x, sub_y;
	int xl, xr, yu, yb;
	int clip_limit, nr_pixels;
	int *img_ptr;
	unsigned short *lut;
	int* hist, *map_array;
	int* lu, *lb, *ru, *rb;
	int wb_cnt = width / block_sz, hb_cnt = height / block_sz;
	map_array = (int *)IPAlloc_760(sizeof(int)* wb_cnt * hb_cnt * nr_bins);
	if (map_array == 0) return IMAGE_POOL_ALLOC_MEM_FAIL;

	nr_pixels = block_sz * block_sz;

	//if (clip_limit_param > 0) {
	clip_limit = (int)(clip_limit_param_2e10 * nr_pixels / nr_bins / 1024);
	clip_limit = (clip_limit < 1) ? 1 : clip_limit;
	//}
	//else clip_limit = 1UL << 14;
	lut = create_lookup_tbl(max, nr_bins);

	for (y = 0, img_ptr = image, hist = map_array; y < hb_cnt; y++, img_ptr += width * block_sz) {
		for (x = 0; x < wb_cnt; x++, hist += nr_bins) {
			make_block_histogram(img_ptr + x*block_sz, width, block_sz, block_sz, hist, nr_bins, lut);
			clip_histogram(hist, nr_bins, clip_limit);
			map_histogram(hist, max, nr_bins, nr_pixels);
		}
	}

	for (img_ptr = image, y = yu = yb = 0, sub_y = block_sz / 2; y < height; yu++, yb++) {
		if (yb >= hb_cnt) {
			sub_y = height - y;
			yb = yu;
		}
		for (x = xl = xr = 0, sub_x = block_sz / 2; x < width; xr++, xl++) {
			if (xr >= wb_cnt) {
				sub_x = width - x;
				xr = xl;
			}

			lu = &map_array[nr_bins * (yu * wb_cnt + xl)];
			ru = &map_array[nr_bins * (yu * wb_cnt + xr)];
			lb = &map_array[nr_bins * (yb * wb_cnt + xl)];
			rb = &map_array[nr_bins * (yb * wb_cnt + xr)];
			interpolate(img_ptr + x, width, lu, ru, lb, rb, sub_x, sub_y, lut);
			if (x == 0) xl--; //keep xl at first column
			x += sub_x;
			sub_x = block_sz; //change to full first column
		}
		if (y == 0) yu--; //keep yu at first row
		y += sub_y;
		img_ptr += sub_y * width;
		sub_y = block_sz;
	}
	IPFree_760(lut);
	IPFree_760(map_array);
	return 0;
}