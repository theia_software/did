#ifdef __cplusplus
extern "C"
{
#endif

#ifndef ISP_CLAHE_H
    #define ISP_CLAHE_H
    int clahe(int *image, int width, int height, int max, int block_sz, int nr_bins, int clip_limit_param_2e10);
#endif

#ifdef __cplusplus
}
#endif