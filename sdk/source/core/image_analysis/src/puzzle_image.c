#include "puzzle_image.h"
#include "plat_log.h"
#include "plat_mem.h"
#include "qm_lib.h"
#include "type_definition.h"

#define IS_FINGER_QTY 100

static QMOption* g_opt = NULL;
static QMFeatures g_base_feat, g_new_feat;
unsigned char* g_new_raw = NULL;
int g_in_img_width, g_in_img_height;

int init_qm(int width, int height) {
    g_in_img_width = width;
    g_in_img_height = height;

    g_opt = qm_alloc_option();
    g_opt->return_qty = TRUE;

    g_new_raw = plat_alloc(width * height);

    if (g_opt == NULL || g_new_raw == NULL) {
        return QM_ERROR;
    }

    return QM_OK;
}

void uninit_qm() {
    if (g_opt != NULL) {
        qm_free_option(g_opt);
        g_opt = NULL;
    }

    PLAT_FREE(g_new_raw);
}

int count_offset_qm(unsigned char* img, int* score, int* dx, int* dy) {
    QMMatchDetail md;

    if (img == NULL || score == NULL || dx == NULL || dy == NULL) {
        return QM_ERROR;
    }

    memcpy(g_new_raw, img, g_in_img_width * g_in_img_height);

    qm_extract_partial(g_new_raw, g_in_img_width, g_in_img_height, g_in_img_width, g_in_img_height,
                       &g_new_feat, g_opt);

    if (g_new_feat.qty < IS_FINGER_QTY) {
        LOGD("get navi qty %d", g_new_feat.qty);
        return QM_NOT_FINGER;
    }

    *score = qm_match(&g_base_feat, &g_new_feat, &md, g_opt);
    *dx = md.dx;
    *dy = md.dy;

    return QM_OK;
}

int qm_update_feat() {
    memcpy(&g_base_feat, &g_new_feat, sizeof(QMFeatures));

    return QM_OK;
}