#ifndef _TYPES_H_
#define _TYPES_H_

/*typedef unsigned char __u8;
typedef unsigned short __u16;
typedef unsigned int __u32;
typedef unsigned long __u64;*/
typedef unsigned char BYTE;
typedef signed char SBYTE;
typedef unsigned short WORD;
typedef unsigned short USHORT;
typedef short SHORT;
typedef int INT;
typedef unsigned int UINT;
// typedef unsigned int DWORD;
typedef char CHAR;
typedef int BOOL;
typedef float FLOAT;
typedef double DOUBLE;
typedef unsigned char* PUCHAR;
// typedef void		VOID;

#ifndef FALSE
#define FALSE 0
#endif

#ifndef TRUE
#define TRUE 1
#endif

#endif  //_TYPES_H_

// end of file
