//Revise Date: 2019/08/12                                                   *|

#ifndef __GO_IMAGEPROCESSINGLIB_H__
#define __GO_IMAGEPROCESSINGLIB_H__
#include <stdint.h>

#define GO_IP_VERSION "1.0.1.4"

#define RBS_UK_FLOW_MODE (99)
#define RBS_IPP_IMAGE (100)
#define EVTOOL_8BIT_MODE (200)
#define EVTOOL_16BIT_MODE (201)

#ifdef __cplusplus
extern "C" {
#endif

#define IMAGE_POOL_OK	0
#define IMAGE_POOL_INVALID_PARAMETER			-1000
#define IMAGE_POOL_ALLOC_MEM_FAIL				-1001
#define IMAGE_POOL_VERSION_NON_SYNC				-1002
#define IMAGE_POOL_NOT_READY					-1003

void get_ipp_lib_version(unsigned char* buf);
//typedef void (*ipp_sp_callback)(int *img16, int *bkg, unsigned char *dst, int w, int h);
typedef struct gotagIpp711param {
	int method;
	int k_min;
	int k_max;
	int integral_count;
	int width;
	int height;
	/*int save_16bits;
	int *save_16bits_img;*/
	void *img_pool;
	int img_pool_max_count;
	void *ipp_sp;
} GoIpp711param, *pGoIpp711param;

void go_generate_calib(unsigned short **input_images, int image_num, int width, int height, unsigned short *output_image);
void go_input_proc(unsigned short *img16_input, unsigned short *img16_bkg, unsigned char *outimg, int w, int h, int integral_count, int enable_quality_check, int* quality, int mode);
void go_init_ipp(GoIpp711param *ipp711param, unsigned short *img_bkg, int raw_img_bit);
void go_uninit_ipp(GoIpp711param *ipp711param);
int go_IPcheck_img_pool(void *data);
int go_IPadd_BDS(GoIpp711param *ipp711param, unsigned short *img, int temperature);
int go_IPget_img_pool_size(int width, int height);
unsigned short* go_IPBDS_get(GoIpp711param *ipp711param);
void go_IPBDS_set_uk(unsigned char *uk_bkg, int w, int h, int bit_per_pixel);
#ifdef __cplusplus
};
#endif

#endif
