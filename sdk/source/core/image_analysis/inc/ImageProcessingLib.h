/******************************************************************************\
|*                                                                            *|
|*  ImageProcseeingLib.c                                                      *|
|*  Version: 1.1.0.25                                                         *|
|*  Date: 2018/02/22                                                          *|
|*  Revise Date: 2020/02/07                                                   *|
|*  Copyright (C) 2007-2019 Egis Technology Inc.                              *|
|*                                                                            *|
\******************************************************************************/

#ifndef __IMAGEPROCESSINGLIB_H__
#define __IMAGEPROCESSINGLIB_H__

#define IP_VERSION "1.2.12.8"

#define IS_FP_IMAGE_AVG_INTENSITY_TH 245

#define N_FEATURE 8
#define N_CLASS 6
#define N_RHO_SIZE (N_CLASS * (N_CLASS - 1) / 2)

#define BLOCK_FEATURE_LENGTH 16

#ifdef __cplusplus
extern "C" {
#endif
#include "vgg.h"
#include "dl_api.h"
enum IP_ImageType{
	IP_ImageUnknown,
	IP_ImageEmpty,
	IP_ImageWaterMark,
	IP_ImageFinger,
	IP_ImageFingerPartial,
	IP_ImageFingerTooPartial
};
#define IP_ImagePush IP_ImageUnknown
enum IP_VDMIdentifyType{
	IP_ImageFingerOff,
	IP_ImageFingerOn
};
typedef struct IPBlockExtractResult{
	int desc_count;
	int desc[BLOCK_FEATURE_LENGTH];
}IPBlockExtractResult;

#define IPP_OFF 0
#define IPP_ET516M 516
#define IPP_ET613_CS1 613
#define IPP_ET613_CS1_ONLY_16_TO_8BITS 6130
#define IPP_ET613_CS1_ADD_BKG 6131
#define IPP_ET613_CS1_GAIN14 6132
#define IPP_ET613_CS1_GAIN14_TEST_ALPHA 6133
#define IPP_ET613_CS1_GAIN14_TEST_BETA 6134
#define IPP_ET613_CS1_GAIN14_TEST_GAMMA 6135
#define IPP_ET613_CS1_GAIN14_VBT8 6136
#define IPP_ET613_SONY 614
#define IPP_ET613_SONY_ADD_BKG 6141
#define IPP_ET613_SONY_GAIN8 6142
#define IPP_ET613_SONY_GAIN8_v02 6143
#define IPP_ET711 7110
#define IPP_ET713 7130
#define IPP_ET715 7150
#define IPP_ET528 528
#define IPP_ET525 525
#define IPP_ET523 523
#define IPP_ET522 522

//Image Pool
#define IMG_POOL_VERSION	'D'
//#define IMG_POOL_TEMPERATURE_INTERVAL	2
//#define IMG_POOL_TEMPERATURE_UPPRE_BOUND	44
//#define IMG_POOL_TEMPERATURE_LOWER_BOUND	(-15)
#define IMG_POOL_TEMPERATURE_INTERVAL	3
#define IMG_POOL_TEMPERATURE_UPPRE_BOUND	64
#define IMG_POOL_TEMPERATURE_LOWER_BOUND	(-25)
#define IMG_LIST_LENGTH	(IMG_POOL_TEMPERATURE_UPPRE_BOUND - IMG_POOL_TEMPERATURE_LOWER_BOUND + 1)/(IMG_POOL_TEMPERATURE_INTERVAL)
#define IMG_BYTE_PER_PIXEL	2
#define IMG_POOL_MIN_COUNT	16
#define IMG_POOL_RAW_IMG_BIT	10
#define IMG_POOL_VARIANT_THRESHOLD 90
#define IMG_POOL_MIN_DISTANCE 4
#define IMG_POOL_VAR_MODE_ON	1
#define IMG_POOL_VAR_MODE_OFF	0
typedef struct _IP_ImagePool{
	unsigned char version;
	unsigned char tmp_interval;
	short tmp_upper_bound;
	short tmp_lower_bound;
	int img_width;
	int img_height;
	int checksum;
	unsigned short variant_th;
	unsigned short min_distance;
	unsigned short var_mode;
	unsigned char img_list_count_array[IMG_LIST_LENGTH];
	//unsigned short img_list[IMG_LIST_LENGTH];
}IP_ImagePool;
//Error Code
#define IMAGE_POOL_OK	0
#define IMAGE_POOL_UPDATE_NOT_READY	1
#define IMAGE_POOL_INVALID_PARAMETER			-1000
#define IMAGE_POOL_ALLOC_MEM_FAIL				-1001
#define IMAGE_POOL_VERSION_NON_SYNC				-1002
#define IMAGE_POOL_NOT_READY					-1003
#define IMAGE_POOL_IMAGE_VARIANT				-1004
#define IMAGE_POOL_QTY_SIZE						-2001
#define IMAGE_POOL_ET516_SIZE					-2002
#define IMAGE_POOL_IPpalm_SIZE					-2003

#define DETECT_WITHOUT_TABLE -101

//BDS3
#define	GEN_BDS_ML_DATA	0
#define BDS3_IMG_POOL_VERSION	'G'
#define BDS3_IMG_LIST_LENGTH	(GEN_BDS_ML_DATA ? 1 : 16)
#define BDS3_IMG_MIX_SLOT_IDX	(BDS3_IMG_LIST_LENGTH - 1)
//#define BDS3_IMG_TMP_LIST_LENGTH	2
//#define BDS3_IMG_TOT_LIST_LENGTH	(BDS3_IMG_LIST_LENGTH + BDS3_IMG_TMP_LIST_LENGTH)
//#define BDS3_IMG_NORMAL_TMP_IDX		(BDS3_IMG_LIST_LENGTH)
//#define BDS3_IMG_LOW_TMP_IDX		(BDS3_IMG_LIST_LENGTH + 1)

typedef struct _IP_BDS3_ImagePool{
	unsigned char version;
	char enable_mix_pool;
	int img_width;
	int img_height;
	int checksum;
	int integral_count;
	int raw_img_bit;
	int neighbor_moire_th;
	int min_moire_idx;
	int update_idx;
	int img_tot_cnt;
	unsigned char img_list_count_array[BDS3_IMG_LIST_LENGTH];
	unsigned char img_list_max_count_array[BDS3_IMG_LIST_LENGTH];
	unsigned char img_list_attribute_array[BDS3_IMG_LIST_LENGTH];
}IP_BDS3_ImagePool;

//BDS3 Error Code
#define BDS3_IMAGE_POOL_OK	0
#define BDS3_IMAGE_POOL_INVALID_PARAMETER			-2000
#define BDS3_IMAGE_POOL_ALLOC_MEM_FAIL				-2001
#define BDS3_IMAGE_POOL_VERSION_NON_SYNC			-2002
#define BDS3_IMAGE_POOL_NOT_READY					-2003
#define BDS3_IMAGE_POOL_IMAGE_VARIANT				-2004
#define BDS3_IMAGE_POOL_INDEX_FULL					-2005
#define BDS3_IMAGE_POOL_NO_AVAILABLE_SLOT			-2006
#define BDS3_IMAGE_POOL_CREATE_FAIL					-2007
#define BDS3_IMAGE_POOL_INVALID_ATTRIBUTE			-2008

//BDS3 config
#define BDS3_CONFIG_NEIGHBOR_MOIRE_THRESHOLD		1000

//BDS3 attribute
#define BDS3_ATTRIBUTE_READ		0x01
#define BDS3_ATTRIBUTE_WRITE	0x02
#define BDS3_ATTRIBUTE_DELETE	0x04
#define BDS3_ATTRIBUTE_ALL		0xFF

// ET711_partial_check_th
#define PARTIAL_DETECT_711_W	96
#define PARTIAL_DETECT_711_H	144
#define PARTIAL_DETECT_713_W	96
#define PARTIAL_DETECT_713_H	96
#define LIGHT_RATIO_TH	30

// ET713 ML_partial is_light issue check
#define IMAGE_ISSUE_NORMAL_IMAGE	0
#define IMAGE_ISSUE_HIGH_LEAK_OF_LIGHT	1
#define IMAGE_ISSUE_POTENTIAL_LEAK_OF_LIGHT	2
#define IMAGE_ISSUE_FUZZY_IMAGE	4

#define BDS3_MIN_COUNT_CHECK_SCRATCH_RESIDUE 3
//void SWGain(unsigned char *img, int w, int h, int gain);
//void Otsu(unsigned char *img, int width, int height, int gain);
//int AutoGainByOtsu(unsigned char *img, int width, int height, int expect);
//void VRBfoot(unsigned char *img, int w, int h, int part);
void IPhist_eq(unsigned char *raw_data, int width, int height);
unsigned char IPotsu_th_w(unsigned char *img, int width, int height);
unsigned char IPotsu_gap_w(unsigned char *img, int width, int height);
void IPvrb(unsigned char *img, int w, int h, int add);
void IPvrb_foot(unsigned char *img, int w, int h, int part);
void IPvrb_foot2(unsigned char *img, int w, int h, int part);
void IPsw_gain(unsigned char *img, int w, int h, int gain);
void IPotsu(unsigned char *img, int width, int height, int gain);
int IPauto_gain_by_otsu(unsigned char *img, int width, int height, int expect);
int IPauto_gain_by_otsu_w(unsigned char *img, int width, int height, int expect);
int IPauto_gap_by_otsu(unsigned char *img, int width, int height, unsigned char target_gap, unsigned char target_mb_base);
int IPauto_gap_by_otsu_w(unsigned char *img, int width, int height, unsigned char target_gap, unsigned char target_mb_base);
int IPvertical_flip(unsigned char *raw_data, int width, int height);
int IPimage_medium_filter(unsigned char* img, int w, int h);
int IPimage_medium_filter_16bits(int* img, int w, int h);
unsigned int IPeuclidean_distance(unsigned char *raw_data_1, int raw_data_1_width, unsigned char *raw_data_2, int width, int height, int test_width);
int IPbadpointfix(unsigned char *img, int w, int h, unsigned char *bp_table, int pass);
unsigned char *IPresample(unsigned char* img, int width, int height, int new_width, int new_height);
int IPis_fp_image_lite(unsigned char* img, int width, int height, int* corner_count, int ret_img);
int IPis_fp_image(unsigned char* img, int width, int height, int* black_percent, int* white_percent, int* amplitude, int* bo_dir_consistency, int* coh_avg, int *corner);
int IPimage_type_identifier(unsigned char* in_img, int w, int h, int* out_amp, int* out_score, int* sv_table, int* rh_table);
int IPvdm_finger_identifier(unsigned char* in_img, int w, int h, int* sv_table, int* rh_table);
int IPis_fp_image_lite_core(unsigned char* img, int width, int height, int* corner_count, int* cover_count, int ret_img);
int IPis_fp_image_raw(unsigned char* img, int w, int h, int wrap_size);
int IPis_fp_image_raw_info(unsigned char* img, int w, int h, int* percentage);
void IPnavi_is_fp_image_percentage(unsigned char * img, int w, int h, int * percentage);
int IPcount_percentageBSD2(unsigned char* in_img, int w, int h);
int IPcount_average_intensity(unsigned char *raw_data, int width, int height, int test_width, int min_request);
unsigned char* IPreconstruct_image_int(unsigned char *in_img, int w, int h, int bkg_iteration);
int IPresample_is_fp_image_lite(unsigned char *image, int width, int height, int *corner_count, int *cover_count, int is_raw_image, int *intensity);
void IPaverage(int *img16, int width, int height, int window);
int *IPaverage_to(int *img16, int width, int height, int window);
int *IPaverage_to_v2(int *img16, int width, int height, int window);
int* IPbackground_subtract(unsigned char *img, int h, int w, int window, int iteration);
void IPcast_to(int *dst, unsigned char *src, int size);
void IPcast16_to8(unsigned char *dst, int *src, int size);
int* IPclone(int *src, int size);
unsigned short* IPclone_ushort(unsigned short *src, int size);
unsigned short *IPint_to_ushort(int* img, int size);
int* IPushort_to_int(unsigned short*src, int size);
void IPsobel(int *img16, int width, int height, int *dst);
int *IPconvert_to(unsigned char *image, int size, int max, int min);
void IPcount_max_min(int *img16, int size, int *max, int *min, int cut_ratio_x100);
void IPcount_max_min_ex(int *img16, int size, int *max, int *min, int cut_ratio_x10000);
void IPcount_max_min8(unsigned char *image, int size, unsigned char *max, unsigned char *min, int cut_ratio_x100);
void IPcount_max_min_ROI(int *img16, int width, int height, int *max, int *min, int cut_ratio_x100, int x0, int y0, int w_ROI, int h_ROI);
void IPcount_max_min_ROI_ex(int *img16, int width, int height, int *max, int *min, int cut_ratio_x10000, int x0, int y0, int w_ROI, int h_ROI);
unsigned short *IPcreate_hist_ROI(int *img16, int width, int height, int x0, int y0, int w_ROI, int h_ROI);
void IPdiv(int *img_sum, int size, int divisor);
const int *IPgaussian_kernel_tbl(int ksize, int sigma_x100);
void IPgaussian_normalize8(unsigned char *image, int width, int height, int sigma_x100, int ksize, int cut_ratio_x100);
void IPgaussian_normalize(unsigned char *dst, int *img16_src, int width, int height, int sigma_x100, int ksize, int cut_ratio_x100);
unsigned char **IPnormalize_to_8bit(int *img16, int width, int height, int cut_ratio_x100);
int *IPconvolution_symmetric(int *img16, int width, int height, int *kernel_2e15, int ksize);
void IPconvolution_symmetric_buf(int *img16_src, int *img16_dst, int width, int height, int *kernel_2e15, int ksize);
void IPconvolution_symmetric_buf_2e4(int *img16_src, int *img16_dst, int width, int height, int *kernel_2e15, int ksize);
//void IPlog_normalize(unsigned char *image, int width, int height, int cut_ratio_x100);
void IPmax_min_normalize(unsigned char *dst, int *img16, int width, int height, int cut_ratio_x100);
void IPmax_min_normalize8(unsigned char *dst, unsigned char *img8, int width, int height, int cut_ratio_x100);
void IPmax_min_normalize_fix_window(unsigned char *dst, int *img16, int width, int height, int window);
void IPmax_min_normalize_ROI(unsigned char *dst, int *img16, int width, int height, int cut_ratio_x100, int x0, int y0, int w_ROI, int h_ROI);
void IPmoving_normalize(unsigned char *dst, int *img16, int width, int height, int window);
void IPmoving_normalize8(unsigned char *dst, unsigned char **src_img, int width, int height, int window);
void IPsigmoid_normalize(unsigned char *dst, int *img16, int width, int height);
void IPnormalize_to(int *img16, int size, int max, int min);
void IPreverse_to(unsigned char *dst8, int *src16, int size);
void IPsum(int *img_sum, int *img16, int size);
void IPsum8(int *img_sum, unsigned char*img8, int size);
void IPsubtract(int *img16, int *from, int size);
int *IPsum_column(int *img16, int w, int h);
int *IPmean_column(int *img16, int w, int h);
int **IPremove_vertical_line(int *img16, int w, int h);
void IPremove_vertical_line_byte(unsigned char* in_img, int w, int h);
unsigned char **ipp_602(int **img16_input, int **img_bkg, int w, int h, int ksize, int sigma_x100, int *bkg_total_count);
unsigned char **ipp_516m(int **img16_input, int **img_bkg, int w, int h, int update_ratio, int *bkg_total_count, int bkg_threshold);
unsigned char **ipp_520(int *img16_input, int *img_bkg, int w, int h, int ksize, int sigma_x100, int update_ratio, int *bkg_total_count, int bkg_threshold);
void IPet613_sony_gain8_for_MT_01(int *img16_input, int *dst16, int w, int h, int method);
//int ipp_711(int *img16_input, int *img16_bkg, unsigned char *dst, int w, int h, int k_x1000, int z_x100, int temperature, 
// unsigned char normalize_mode, int save_16bits, int save_16bits_bkg, int *save_16bits_img)
#define ipp_713(param1, param2, param3, param4, param5, param6, param7, param8, param9, param10, param11, param12, param13) ipp_711(param1, param2, param3, param4, param5, param6, param7, param8, param9, param10, param11, param12, param13)
unsigned char **ipp_725(int **img16, int **img_bkg, int w, int h, int ksize, int sigma_x100, int window, int iteration, int dir_smooth, int update_ratio);
unsigned char **ipp_736(unsigned char **img, int **img_bkg, int *width, int *height, int ksize, int sigma_x100, int iteration, int *bkg_total_count);
unsigned char **ipp_736_fixed_bkg_v0911(unsigned char *img, unsigned char *img_bkg, int w, int h, int ksize, int sigma_x100, int window, int iteration);
unsigned char **ipp_736_fixed_bkg_v1(unsigned char *img, unsigned char *img_bkg, int width, int height, int ksize, int sigma_x100, int window, int iteration);
int IPP_sensor(int sensor_type, int *img16_src, int *img16_bkg, unsigned char *img8_dst, int w, int h, int param_1, int param_2, int param_3, unsigned char param8_1);
//void update_bkg(int *img_bkg, int *img16, int size, int update_ratio);
void IPadjust_background(int *img_bkg, int *img16_input, int size, int *bkg_total_count, int bkg_threshold);
void IPintensity_invert(int *img16, int w, int h);
int IPsum_array_element(int *img16, int size);
void IPmultiply_array(int *img16_a, int *img16_b, int *img16_res, int size);
void IPvertical_convolution(int *img16_src, int *img16_dst, int width, int height, int *kernel_2e15, int ksize);
void IPvertical_deconvolution(int *img16_src, int *img16_dst, int w, int h, int ksize, int sigma_x100, int num_iterations);
void IPdeconvolution(int *img16_src, int *img16_dst, int w, int h, int ksize, int sigma_x100, int num_iterations);
void IPhist_match(int *hist, int *tar_hist, int *map_fun, int pre_max);
void IPGaussianOrient(int width, int  height, int * * dstimage, int * * image, unsigned char * * orientImage, int mask_size, int table_num);
int **IPconvert_to_int_array(int *src, int w, int h);
unsigned char **IPconvert_to_byte_array(unsigned char *src, int w, int h);
void binning_image(unsigned char *img, int w, int h);
void binning_image_16(int *src, int w, int h, int *dst);
void binning_image_int(int *img, int w, int h);
int IPvgg_finger_detect(int sensor_type, unsigned char *image, int width, int height, VggResult *vgg_ret);
int IPres_image_type_identifier_filter_522(unsigned char *image, int width, int height);
int IPres_image_type_identifier_filter_528(unsigned char *image, int width, int height, int *wgt);
int IPres_image_type_identifier_filter_613(unsigned char *image, int width, int height);
// For ET727 & ET736
//void IPgenerate_calib_8bit(unsigned char **input_images, int image_num, int width, int height, int *output_image);
//void IPinput_seq_proc_8bit_BIG(unsigned char *img, unsigned char *calib, int width, int height, int num, int enable_quality_check, int* quality, int* quality_img, unsigned char *outimg, int mode, int type_panel);
unsigned short *find_ET711_bkg_fixed_pattern(unsigned short *bkg, int height, int width, int *fpi_len);
int generate_adaptive_bkg_predict_value(unsigned short *img, unsigned short *bkg, int height, int width, unsigned short *fixed_pattern_info, int fpi_len, int k_min, int k_max, int k_step);
int IPcoverage_check_ET711(unsigned short* in_img, int w, int h, unsigned char* rgb, int* is_light);
int IPcoverage_check_ET713(unsigned short* in_img, int w, int h, unsigned char* rgb, int* is_light);
int IP613_check_vdm_coverage(unsigned char *img, int width, int height, int w_step, int h_step);
int IPcoverage_check_ET711_is_light_value(unsigned short* in_img, int w, int h, unsigned char* rgb, int* is_light);
int IPcoverage_check_ET713_is_light_value(unsigned short* in_img, int w, int h, unsigned char* rgb, int* is_light);
//int IPcoverage_check_ET711_old(unsigned short* in_img, unsigned short* in_bkg, int w, int h, unsigned char* rgb, int* is_light);
void get_ipp_lib_version(unsigned char* buf);
typedef void (*ipp_sp_callback)(int *img16, int *bkg, unsigned char *dst, int w, int h);
typedef struct tagIpp711param {
	int method;
	int k_min;
	int k_max;
	int integral_count;
	int width;
	int height;
	/*int save_16bits;
	int *save_16bits_img;*/
	void *img_pool;
	int img_pool_max_count;
	ipp_sp_callback *ipp_sp;
} Ipp711param, *pIpp711param;
void init_ipp_711(int predict, int integral_count, int img_pool_max_count, unsigned short *img_bkg, void *img_pool, ipp_sp_callback *ipp_sp, int raw_img_bit);
void uninit_ipp_711();
#define init_ipp_713(param1, param2, param3, param4, param5, param6, param7) init_ipp_711(param1, param2, param3, param4, param5, param6, param7)
#define uninit_ipp_713() uninit_ipp_711()
void egis_generate_calib(unsigned short **input_images, int image_num, int width, int height, unsigned short *output_image);
/*	egis_input_proc:
		-img16_input: raw image
		-img16_bkg: bkg
		-outimg: output img, memory allocate is needed
		-w: image width
		-h: image height
		-integral_count: raw image integral count; for ipp mode 67(default:2)/671(default:2)/672(default:1.8 * 4096)/673(default:1.8 * 4096)
		-enable_quality_check: default 1; for ipp mode 67(default:3)/671(default:3)/672(default:1.6 * 16)/673(default:1.6 * 16)
		-quality: for ipp mode 66/661/67(default:3)/671(default:3)/901
		-mode: ipp mode
			>-1:  ipp off, only max min normalize
			>5:   ipp5
			>6:	  ipp6
			>61:  ipp61
			>62:  ipp62
			>63:  ipp6_v2
			>64:  ipp64
			>65:  ipp6 3b
			>66:  ipp6 5b
			>661: ipp6 5c
			>67:  ipp6 0a
			>671: ipp6 0b
			>672: ipp6.0d
			>673: ipp6.0e
		-kmask: default NULL
*/
void egis_input_proc(unsigned short *img16_input, unsigned short *img16_bkg, unsigned char *outimg, int w, int h, int integral_count, int enable_quality_check, int* quality, int mode, unsigned char *kmask);
void ipp_702_stage_post_fake(unsigned char *img8, unsigned short *img, int width, int height, int cut_ratio_x100);
unsigned short* ipp_711_stage_preprocessing(unsigned short *img16_input, unsigned short *img16_bkg, int w, int h);
unsigned short* ipp_71x_stage_preprocessing(unsigned short *img16_input, unsigned short *img16_bkg, int w, int h);
void init_ipp_711bg(Ipp711param *ipp711param, unsigned short *img_bkg, int raw_img_bit);
unsigned short* ipp_711bg(Ipp711param *ipp711param, int temperature, unsigned short *img, unsigned short *img_bkg);
unsigned short* ipp_711bg_debug(Ipp711param *ipp711param, int temperature, unsigned short *img, unsigned short *img_bkg, int* debug_path, int *k_value);
unsigned short* ipp_711bg_debug_fastBDS(Ipp711param *ipp711param, int temperature, unsigned short *img, unsigned short *img_bkg, int* debug_path, int *k_value, int min_bkg_cnt);
unsigned short* ipp_711bg_debug_v2_fastBDS(Ipp711param *ipp711param, int current_temperature, int *valid_temperature, int *debug_path, int min_bkg_cnt);
void uninit_ipp_711bg(Ipp711param *ipp711param);
#define init_ipp_713bg(param1, param2, param3) init_ipp_711bg(param1, param2, param3)
#define ipp_713bg(param1, param2, param3, param4) ipp_711bg(param1, param2, param3, param4)
#define ipp_713bg_debug(param1, param2, param3, param4, param5, param6) ipp_711bg_debug(param1, param2, param3, param4, param5, param6)
#define ipp_713bg_debug_fastBDS(param1, param2, param3, param4, param5, param6, param7) ipp_711bg_debug_fastBDS(param1, param2, param3, param4, param5, param6, param7)
#define ipp_713bg_debug_v2_fastBDS(param1, param2, param3, param4, param5) ipp_711bg_debug_v2_fastBDS(param1, param2, param3, param4, param5)
#define uninit_ipp_713bg(param1) uninit_ipp_711bg(param1)
int IPcheck_img_pool(void *data);
int IPadd_BDS(Ipp711param *ipp711param, unsigned short *img, int temperature);
int IPget_img_pool_size(int width, int height);
int IPadd_img_into_pool(void *data, unsigned short *img, int width, int height, int temperature, int integral_count);
int IPupdate_img_into_pool(void *data, unsigned short *img, int width, int height, int temperature, int integral_count);
unsigned short *IPget_img_from_pool(void *data, int width, int height, int temperature, int integral_count);
int IPget_valid_tmp_from_pool(void *data, int temperature, int *valid_temperature);
int IPget_valid_tmp_from_pool_fastBDS(void *data, int temperature, int *valid_temperature, int min_bkg_cnt);
int IPreset_img_pool_by_temperature(void *data, int temperature);
void IPupdate_BDS(void *data, int *detect_img, int width, int height, int temperature, int integral_count);
void crop_central_image8(unsigned char *src, int w, int h, int nw, int nh);
int vgg_finger_detect_three_type(unsigned char *image, int width, int height, int col_scan, int row_scan, int *wgt, VggResult *vgg_ret);
int vgg_finger_detect_three_type_circle(unsigned char *image, int width, int height, int col_scan, int row_scan, int radius, int *wgt, VggResult *vgg_ret);
int vgg_finger_detect_qty(unsigned char *image, int width, int height, int col_scan, int row_scan, int *wgt, VggResult *vgg_ret);
int make_marked_image_three_type(unsigned char *img, int w, int h, VggResult *vr, unsigned char* rgb);
void IPet613cs1_update_bkg(int* img, int *bkg_img, int w, int h, int bkg_reset_count);
int IPet613cs1_contrast_score(int *img16, int width, int height);
int IPet613sony_contrast_score(int *img16, int w, int h);
int IPet613cs1_vdm_finger_touch_area(int *img16, int w, int h, int *mask);
int IPfixpattern_check_713(unsigned char *img8, int width, int height, int *wgt);
void mark_circle_bkg(unsigned char *img, int w, int h, int radius_ratio, int tar_cx, int tar_cy);

void ipp_713_stage_post_edge(unsigned char *img8, unsigned short *img, int width, int height, int cut_ratio_x100, int cx, int cy, int crop_radius);

//
int IPfake_detect_ET520(unsigned char *img8, int width, int height, int *wgt);
int fake_fingerprint_test_516(unsigned char *img8, int width, int height, int *wgt);
int fake_fingerprint_test_71x(unsigned char *img8, int width, int height, int *wgt);
int IPsilicon_case_71x(unsigned char *img8, int width, int height, int *wgt);
int IPsilicon_case_71x_lite(unsigned char *img8, int width, int height, int *wgt);
int fake_fingerprint_test_715(unsigned char *img8, int width, int height, int *wgt);
int edge_detect_ET713(unsigned char *img8, int width, int height, int *wgt);
int IPcircle_direct(unsigned short *in_img, int w, int h, int *wgt);

int IPfake_check_ET702(unsigned short *in_img, int w, int h, int *wgt);
int IPfake_check_ET713(unsigned short *in_img, int w, int h, int *wgt);
int IPblack_edge_detect_ET713(unsigned short *in_img, int w, int h, int *wgt, int cx, int cy, int crop_radius);
int IPito_detect_ET713(unsigned short *in_img, int w, int h, int *wgt);

int IPfake_detect(unsigned short *in_img, int w, int h, int *wgt, int sensor);
int IPblack_edge_detect(unsigned short *in_img, int w, int h, int *wgt, int cx, int cy, int sensor);
int IPblack_edge_detect2(unsigned short *in_img, int w, int h, int *wgt, int cx, int cy, int sensor, int crop_radius);
int IPito_detect(unsigned short *in_img, int w, int h, int *wgt, int sensor);
int IPqty_block(int *img8, int w, int h, int *wgt);
int IPqty_block_16(int *img8, int w, int h, int *wgt);
int IPpalm_touch(unsigned char *img8, int width, int height, int *wgt);
int IPpalm_touch_ET528(unsigned char *img8, int width, int height, int *wgt);
int IPpalm_touch_ET523(unsigned char *img8, int width, int height, int *wgt);
int IPpalm_touch_ET522(unsigned char *img8, int width, int height, int *wgt);
int IPfake_detect_ET516(unsigned char *img8, int width, int height, int *wgt);
int IPfake_detect_ET613(unsigned char *img8, int width, int height, int *wgt);
int IPclassfication_label(unsigned char  *img8, int w, int h, int *wgt);
int IPchcek_partial_labeling(unsigned short *img16_src, unsigned short *img16_avgbkg, int *weight_table, int w, int h);
int IPpartial_percent(unsigned char *img8, int width, int height, int *wgt);
int IPlatent_detect(unsigned char *img_a, unsigned char *img_b, int width, int height, int *wgt);
int IPfake_detect_ET523(unsigned char *img8, int width, int height, int *wgt);
//void *IPdescriptor_Init();
//void IPdescriptor(unsigned char *img8, int w, int h, int *wgt, unsigned char *desc, G6DescContex *ctx);

//int IPmatch_predict_ET702(int *img_a, unsigned char *img_b, int width, int height, int *wgt);
//int IPmatch_predict_ET528(unsigned short *img_a, unsigned short  *img_b, int width, int height, int *wgt);
//int IPmatch_predict_ET713(unsigned short *img_a, unsigned short  *img_b, int width, int height, int *wgt);
//int IPmatch_predict_ET713_51p(unsigned short *img_a, unsigned short  *img_b, int width, int height, int *wgt);
//int IPmatch_predict_ET713_46p(unsigned short *img_a, unsigned short  *img_b, int width, int height, int *wgt);
//int IPmatch_predict_ET713_36p(unsigned short *img_a, unsigned short  *img_b, int width, int height, int *wgt);
//int IPmatch_predict_ET713_35p(unsigned short *img_a, unsigned short  *img_b, int width, int height, int *wgt);
//int IPmatch_predict_ET713_learning(short *img_a, short *img_b, int width, int height, int *wgt);
//int IPmatch_enroll_predict_ET713(unsigned short *img_a, unsigned short  *img_b, int width, int height, int *wgt);
//int IPmatch_predict_ET528_50x9(unsigned short *img_a, unsigned short  *img_b, int width, int height, int *wgt);
//int IPmatch_predict_ET528_56x8(unsigned short *img_a, unsigned short  *img_b, int width, int height, int *wgt);
//int IPmatch_predict_ET528_56p(unsigned short *img_a, unsigned short  *img_b, int width, int height, int *wgt);

//void *IPgenerate_bottom_ET528(unsigned char *img, int *wgt);
//unsigned char *IPgenerate_both_edge_ET528(unsigned char *img, int width, int height, int *wgt);
//int IPgenerate_edge(unsigned char *img, unsigned char *dst_img, int *width, int *height, int h_extend, int w_extend, int *wgt, int *mem_buf);
void IPextract_block_feature(unsigned char *img8, int width, int height, int *wgt, IPBlockExtractResult *ex_ret);
int IPmatch_block_desc(IPBlockExtractResult ger1, IPBlockExtractResult ger2);

void IPpartial_map(unsigned char *img8, unsigned char *partial_map, int width, int height, int *wgt);

unsigned char *IPreconstruct_fp(unsigned char *img8, int w, int h, int *wgt);

#define ENABLE_POOL_PARAMETER	0x80000000
#define init_ipp_variant(param1, value1, value2, value3) \
	((Ipp711param *)param1)->img_pool_max_count = value1; \
	((Ipp711param *)param1)->img_pool_max_count |= (value2 << 16); \
	((Ipp711param *)param1)->img_pool_max_count |= (value3 << 24); \
	((Ipp711param *)param1)->img_pool_max_count |= ENABLE_POOL_PARAMETER;

//#endif
int bkg_slot_utilization(void *data);
int IP_BDS3_add_img_into_pool(void *data, unsigned short *img, int slot_id, unsigned char *kmask);
int IP_BDS3_create_img_pool(void *data, unsigned short *img_input, int iteration, unsigned char *kmask);
int IP_BDS3_get_moire_strength(unsigned short *img1, unsigned short *img2, int width, int height, int roi, int integrate_count);
int IP_BDS3_get_img_pool_size(int width, int height);
int IP_BDS3_init_img_pool(void *data, unsigned short *calibration_bkg, int width, int height, int integral_count, int min_bkg_cnt, int raw_img_bit, int enable_mix_pool);
int IP_BDS3_check_img_pool(void *data);
unsigned short *IP_BDS3_output_bkg(void *data, unsigned short *img_input, int min_bkg_cnt, int max_bkg_cnt, int *min_moire, int *min_moire_index, int *max_moire, int *max_moire_index, int *min_moire_index_cnt);
int IP_BDS3_update_bkg(void *data, unsigned short *img_input, int min_moire, int min_moire_index, int max_moire, int max_moire_index, int max_bkg_cnt, int moire_th, unsigned char *kmask);
void IP_BDS3_uninit_img_pool();
int IP_BDS3_get_pool_counter(void *data, int *total, int *count_array, int array_size);
unsigned short *IP_BDS3_get_bkg(void *data, int slot_id, unsigned short *img_input, int *slot_moire, int mth, int min_bkg_cnt);
int IP_BDS3_del_pool(void *data, int slot_id);
int IP_BDS3_set_config(void *data, int param, int value);
int IP_BDS3_set_pool_attribute(void *data, int slot_id, int attribute, int timeout);
int IP_BDS3_change_pool_count(void *data, int slot_id, int *curr_count, int new_count, int max_count);
int IP_BDS3_out_moire_value(void *data, unsigned short *img_input, unsigned short *img_bkg, int *moire_value);

int apply_k_mask(int** raw, int w, int h, unsigned short* k_mask_h, unsigned short* k_mask_l, unsigned char* kmask);
void make_k_mask(unsigned short* kbkg, int w, int h, unsigned short* k_mask_h, unsigned short* k_mask_l);
unsigned char *IP_k_mask(int **img16, unsigned short *kbkg, int w, int h, int *k_mask_cnt);

typedef struct _MLBDSData {
	int bkg_slot;
	int bkg_score;
	int choose_slot;
	int choose_score;
	int update_slot;
}MLBDSData;
int mlbds_update_bkg(void *data, unsigned short *img_input, MLBDSData *mldata);
unsigned short *mlbds_output_bkg(void *data, unsigned short *img_input, MLBDSData *mldata, unsigned short **bkg_list, int *bkg_score_list);

void IPimage_713_IPP6(int *img, unsigned char *dst, int w, int h);
void IPimage_713_IPP6_0d(int *img, unsigned char *dst, int w, int h, int nRollingBallRadius, int bRollingBall_Bilateral, int lce_gain_max_2e12, int lce_alpha_2e4, int* gamma_table);
void IPimage_713_IPP6_0e(int *img, unsigned char *dst, int w, int h, int nRollingBallRadius, int bRollingBall_Bilateral, int lce_gain_max_2e12, int lce_alpha_2e4, int* gamma_table, int kStdMax);

unsigned short *IPPerfectGen(unsigned short *raw, unsigned short *b_bkg, unsigned short *w_bkg, int w, int h, int ic, int model_sel);
int IPLatentPredict4idx(LatentInput *li, int *wgt);
int IPLatentPredictDiagonal(unsigned short *raw1, unsigned short *raw2, int w, int h, int *wgt);

void minus_bkg(short *output_img, int *input_img, int *bkg_img, int width, int height);
#ifdef __cplusplus
};
#endif

#endif
