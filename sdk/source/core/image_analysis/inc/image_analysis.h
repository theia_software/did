#ifndef __IMAGE_ANALYSIS_H__
#define __IMAGE_ANALYSIS_H__

#include "ImageProcessingLib.h"
#include "type_definition.h"

#ifdef _WINDOWS
#ifdef _ACTIVEX
#define CALLBACK
#else
#define CALLBACK __stdcall
#endif
#else
#define CALLBACK
#endif

#ifdef __cplusplus
extern "C" {
#endif

int resample_IsFPImage_Lite(unsigned char* image, int width, int height, int* corner_count,
                            int* cover_count, int is_raw_image, int* intensity);

void image_16bitsTo8bits(unsigned short* raw_16bits, unsigned char* raw_8bits, int width,
                         int height);

#ifdef PROJECT_CS1_ET613
#define DEFAULT_IPP IPP_ET613_CS1
#else
#define DEFAULT_IPP IPP_ET613_SONY
#endif

#ifdef __cplusplus
}
#endif
#endif