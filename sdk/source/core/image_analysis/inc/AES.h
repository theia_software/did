#ifndef __AES_INCLUDED__
#define __AES_INCLUDED__

//#include "TypeDef.h"

//#include "fp_types.h"
#ifdef __cplusplus
extern "C" {
#endif

enum { ECB_MODE = 1, CBC_MODE, OFB_MODE, CFB_MODE, CTR_MODE };

//--------------key expansion--------------------------//
void KeyExpan(unsigned char* key, unsigned char** w, unsigned char key_length, int decipher);
// void KeyExpan(unsigned char* key, unsigned char** w, unsigned char KeyLength, BOOL decipher);

//-------------some tool------------------------------//
unsigned char Multiplication(unsigned char a, unsigned char b);
void MatrixMultiplication(unsigned char** w, int Nb, int Nr);
// void ArrayIncrement(unsigned char* buffer, unsigned int ArraySize);

//-----------------AES kernel---------------------------//
void RijndaelCipher(unsigned char* buffer, unsigned char** w, int Nb, int Nk, int Nr);
void RijndaelDecipher(unsigned char* buffer, unsigned char** w, int Nb, int Nk, int Nr);

// bool FileEncode(CHAR* FileName, CHAR* EncodeFileName, unsigned char* key);
// bool FileDecode(CHAR* FileName, CHAR* DecodeFileName, unsigned char* key);

void AESEncrypt(unsigned char* buffer, unsigned char* key, unsigned int buffer_length,
                unsigned char key_length, unsigned char* IV, int mode);
void AESDecrypt(unsigned char* buffer, unsigned char* key, unsigned int buffer_length,
                unsigned char key_length, unsigned char* IV, int mode);

//------------------AES Encryption Mode operation----------------------------------------//
void AESEncrypt_ECB(unsigned char* buffer, unsigned char** w, unsigned int encode_size, int Nb,
                    int Nk, int Nr);
void AESEncrypt_CBC(unsigned char* buffer, unsigned char** w, unsigned int encode_size,
                    unsigned char* IV, int Nb, int Nk, int Nr);
void AESEncrypt_OFB(unsigned char* buffer, unsigned char** w, unsigned int encode_size,
                    unsigned char* IV, int Nb, int Nk, int Nr);
void AESEncrypt_CFB(unsigned char* buffer, unsigned char** w, unsigned int encode_size,
                    unsigned char* IV, int Nb, int Nk, int Nr);
void AESEncrypt_CTR(unsigned char* buffer, unsigned char** w, unsigned int encode_size,
                    unsigned char* IV, int Nb, int Nk, int Nr);

//-----------------AES Eecryption Mode operation----------------------------------//
void AESDecrypt_ECB(unsigned char* buffer, unsigned char** w, unsigned int decode_size, int Nb,
                    int Nk, int Nr);
void AESDecrypt_CBC(unsigned char* buffer, unsigned char** w, unsigned int decode_size,
                    unsigned char* IV, int Nb, int Nk, int Nr);
void AESDecrypt_CFB(unsigned char* buffer, unsigned char** w, unsigned int decode_size,
                    unsigned char* IV, int Nb, int Nk, int Nr);

//--------------------------------------------------------------------------//
// BOOL LTTAESCipher(unsigned char* buffer, unsigned char* key, unsigned char key_length, int
// buffer_length);  BOOL LTTAESDeCipher(unsigned char* buffer, unsigned char* key, unsigned char
// key_length, int buffer_length);
int LTTAESCipher(unsigned char* buffer, unsigned char* key, unsigned char key_length,
                 int buffer_length);
int LTTAESDeCipher(unsigned char* buffer, unsigned char* key, unsigned char key_length,
                   int buffer_length);
#ifdef __cplusplus
}
#endif
#endif  //__AES_INCLUDED__
