#ifndef _TINYVGG_H_
#define _TINYVGG_H_
#include "ml_type.h"
//#define VGG_BLOCK_SZ 16
#define VGG_BLOCK_SZ32 32
//#define VGG_BLOCK_SZ 32
#define VGG_SCALE_WEIGHT 16
#define VGG_SCALE_INPUT 8
//#define VGG_MAX_BLOCK 128
//#define VGG_MAX_BLOCK 256
//typedef struct VggResult VggResult;
//struct VggResult{
//	unsigned char percentage;
//	unsigned char block_count;
//	unsigned char block_label[VGG_MAX_BLOCK];
//	unsigned short block_address[VGG_MAX_BLOCK];
//};
typedef struct _VggInput{
	unsigned char* image;
	int width;
	int height;
	int col_scan;
	int row_scan;
	int block_sz;
}VggInput;
typedef struct _G6DescContex {
	int offset_16[16 * 16 * 9];
	int offset_8[8 * 8 * 9];
	int ch1_padding[18 * 18];
	int ch2_padding[18 * 18];
	int mem_buf[5000];
	unsigned char desc[128];
} G6DescContex;
int vgg_sample_blocks_circle(unsigned char* image, int width, int height, int col_scan, int row_scan, int radius, int *blocks, unsigned short *address);
int vgg_sample_blocks(VggInput *vgg_input, int* blocks, unsigned short *address);
int vgg_is_finger_block(int *input, int th, int *wgt);
int vgg_finger_detect(unsigned char *image,int width, int height, int col_scan, int row_scan, int th, int *wgt, int model, VggResult *vgg_ret);
int rn_inference(int *input, int w, int h, int *ret, int *wgt);
int rn_inference_new(int *input, int w, int h, int *ret, int *wgt);
int rn_fake_detect_ET520(int *input, int w, int h, int *wgt);
int rn_fake_detect_ET523(int *input, int w, int h, int *ret, int *wgt);
int rn_fake_detect_ET613(int *input, int w, int h, int *wgt);
int rn_inference_six_classes_522(int *input, int w, int h, int *ret, int *wgt);
int rn_inference_five_classes_528(int *input, int w, int h, int *wgt);
int rn_inference_six_classes_613(int *input, int w, int h, int *ret, int *wgt);
int rn_inference_res9_case(int *input, int w, int h, int *wgt);
int rn_inference_res9_case_lite(int *input, int w, int h, int *wgt);
int rn_inference_new_711(int *input, int w, int h, int *wgt);
int rn_inference_ito_713(int *input, int w, int h, int *wgt);
int vgg_is_finger_block_three_type(int *input, int *wgt);
int rn_vgg_qty(int *input, int w, int h, int *wgt);
int vgg_qty_16(int *input, int w, int h, int *wgt, int *ret);
int vgg_qty_16_2(int *input, int w, int h, int *wgt, int *ret);
int rn_inference_palm_522(int *input, int w, int h, int *wgt);
int rn_inference_palm_525(int *input, int w, int h, int *wgt);
int rn_inference_palm_528(int *input, int w, int h, int *wgt);
int rn_inference_new_ET516(int *input, int w, int h, int *wgt);
int rn_inference_classfication_label4(int *input, int w, int h, int *wgt);
int rn_inference_partial(int *input, int w, int h, int *wgt);
int rn_inference_block_match(int *input, int w, int h, int *ret, int *wgt);
int *un_inference_recon(int *input, int w, int h, int *wgt);
int vgg_inference_latent(int *input, int w, int h, int *wgt);
//int vgg_inference_predict_ET702(int *input, int w, int h, int *wgt);
//int vgg_inference_predict_ET528(int *input, int w, int h, int *wgt);
//int vgg_inference_predict_ET528_50x9(int *input, int w, int h, int *wgt);
//int vgg_inference_predict_ET528_56x8(int *input, int w, int h, int *wgt);
//int vgg_inference_predict_ET528_450(int *input, int w, int h, int *wgt);
//int vgg_inference_predict_ET528_56p(int *input, int w, int h, int *wgt);
//int vgg_inference_predict_ET713(int *input, int w, int h, int *wgt);
//int vgg_inference_predict_ET713_51p(int *input, int w, int h, int *wgt);
//int vgg_inference_predict_ET713_46p(int *input, int w, int h, int *wgt);
//int vgg_inference_predict_ET713_36p(int *input, int w, int h, int *wgt);
//int vgg_inference_predict_ET713_35p(int *input, int w, int h, int *wgt);
//int vgg_inference_predict_ET713_learning(int *input, int w, int h, int *wgt);
//void vgg_inference_generate_edge(int *input, int w, int h, int *wgt, int *mem_buf);
//int *un_inference_generate_edge_ET522(int *input, int w, int h, int *wgt);
void vgg_inference_partial_map(int *input, int w, int h, int *wgt);
//void vgg_inference_descriptor(int *input, int w, int h, int *wgt, unsigned char *descriptor);
//void vgg_inference_descriptor_typical(int *input, int w, int h, int *wgt, G6DescContex *ctx);

#endif
