#ifndef __PUZZLE_IMAGE_H__
#define __PUZZLE_IMAGE_H__

#define QM_OK 0
#define QM_ERROR 1
#define QM_NOT_FINGER 2

int init_qm(int width, int height);
void uninit_qm();
int count_offset_qm(unsigned char* img, int* score, int* dx, int* dy);
int calculation_dx_dy(unsigned char* image_data, int* dx, int* dy);

int qm_update_feat();

#endif