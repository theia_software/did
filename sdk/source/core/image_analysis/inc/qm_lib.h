/******************************************************************************\
|*                                                                            *|
|*  qm_lib.h                                                                  *|
|*  Version: 0.1.5.18                                                         *|
|*  Date: 2018/01/12                                                          *|
|*  Revise Date: 2018/11/06                                                   *|
|*  Copyright (C) 2007-2018 Egis Technology Inc.                              *|
|*                                                                            *|
\******************************************************************************/
#ifndef QM_LIB_H
#define QM_LIB_H
#define QM_MATCHER_VER "0.1.5.18"
#define QM_DESC_SIZE 16
#define QM_MAX_FEATURE_COUNT 100
typedef struct _QMFeature {
    short x, y;
    unsigned char type;
    unsigned char desc[QM_DESC_SIZE];
} QMFeature;
typedef struct _QMFeatures {
    int size;
    unsigned char percentage;
    unsigned char diff;
    unsigned char qty;
    QMFeature kf[QM_MAX_FEATURE_COUNT];
} QMFeatures;
typedef struct _QMMatchPair {
    unsigned char ridx;
    unsigned char vidx;
    unsigned char dist;
    unsigned char padding;
} QMMatchPair;
typedef struct _QMMatchDetail {
    short size;
    short dx, dy;
    QMMatchPair match_pair[QM_MAX_FEATURE_COUNT];
} QMMatchDetail;
typedef struct _QMOption {
    // extraction option
    int dthreshold;
    int min_dr_value;
    int return_image;
    int count_percentage;
    int blur_method;  // 0(average 3x3), 1(gaussian)
    // matching option
    int nndr_distance;
    int min_hamming_distance;
    int matching_threshold;
    int return_qty;
} QMOption;
#ifdef __cplusplus
extern "C" {
#endif
unsigned char** alloc_2d_image(int width, int height);
unsigned char** clone_raw_image(unsigned char** raw, int width, int height);
QMOption* qm_alloc_option(void);
void qm_free_option(QMOption* opt);
int qm_extract(unsigned char* img, int w, int h, QMFeatures* feat, QMOption* opt);
int qm_extract_partial(unsigned char* img, int w, int h, int crop_w, int crop_h, QMFeatures* feat,
                       QMOption* opt);
int qm_match(QMFeatures* f1, QMFeatures* f2, QMMatchDetail* md, QMOption* opt);
int qm_copy_features(QMFeatures* dst, QMFeatures* src);
QMFeatures* qm_clone_features(QMFeatures* src);
#ifdef __cplusplus
}
#endif
#endif
