/******************************************************************************\
|*                                                                            *|
|*  ipsys.h                                                                   *|
|*  Version: 1.0.4.1                                                          *|
|*  Date: 2018/02/22                                                          *|
|*  Revise Date: 2019/01/12                                                   *|
|*  Copyright (C) 2007-2018 Egis Technology Inc.                              *|
|*                                                                            *|
\******************************************************************************/


//#include "windows.h"
#ifdef __cplusplus
extern "C"
{
#endif
#if !defined(TEEI) && !defined(TEEOS)
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#else
#include "ets_plat_heap.h"
#endif

#ifndef TRUE
#define TRUE 1
#endif

#ifndef FALSE
#define FALSE 0
#endif

#ifndef NULL
#define NULL ((void *)0)
#endif

#define DT_BAD 0x01
#define DT_FIXED 0x02

#define TREND_ORIENTATION(a) ((a + 7) * 274 / 4096 % 8)

void *IPAlloc_760(int size);
void *IPCAlloc_760(int count, int size);
void IPFree_760(void *data);

// Objects
#define IPAllocObject(type) ((type *)IPAlloc_760(sizeof(type)))
#define IPCAllocObject(type) ((type *)IPCAlloc_760(1, sizeof(type)))
#define IPFreeObject(object) IPFree_760((void *)object)

#define IPAllocRow_760(type, length) ((type *)IPAlloc_760((length) * sizeof(type)))
#define IPAllocByteRow(length) IPAllocRow_760(unsigned char, length)
#define IPCAllocIntRow_760(length) IPCAllocRow_760(int, length)
#define IPCAllocRow_760(type, length) ((type *)IPCAlloc_760((length), sizeof(type)))
#define IPCopyRow(type, dstRow, srcRow, length) IPCopyMem(dstRow, srcRow, (length) * sizeof(type))
#define IPCopyByteRow(dstRow, srcRow, length) IPCopyRow(unsigned char, dstRow, srcRow, length)
#define IPCopyIntRow(dstRow, srcRow, length) IPCopyRow(int, dstRow, srcRow, length)
#define IPFreeRow(row) IPFree_760((void *)(row))
#define IPAbs(x) ((x) >= 0 ? (x) : -(x))
#define IPFreeAnImage(image, height) IPFreeImage((void * *)image, height)
#define IPFullWindow(window) ((window) * 2 + 1)
#define IPAllocIntRow_760(length) IPAllocRow_760(int, length)
#define IPAllocShortRow(length) IPAllocRow_760(short, length)


/* IS_FPIMAGE_RAW definitions */
#define CHECK_ROW_FROM_BOUND 5
#define SLIDE_WINDOW 8
#define RIDGE_THRESHOLD 18
#define RIDGE_COUNT_THRESHOLD 4


#define IPBOUND_VALUE(a,b,c) ((a)>(b)?a:((b)>(c)?c:b))

#define IPDIR_BACKGROUND 255

#define IPINGER_API

#define IP_BLOCK_SIZE 16
#define IPGRAY_BLACK   0
#define IPGRAY_WHITE 255

/////////////////////////////////
//                             //
// Directions and orientations //
//                             //
/////////////////////////////////

#define IPDIR_0    0
#define IPDIR_45  30
#define IPDIR_90  (IPDIR_45 * 2)
#define IPDIR_135 (IPDIR_45 * 3)
#define IPDIR_180 (IPDIR_45 * 4)
#define IPDIR_225 (IPDIR_45 * 5)
#define IPDIR_270 (IPDIR_45 * 6)
#define IPDIR_315 (IPDIR_45 * 7)
#define IPDIR_360 (IPDIR_45 * 8)
#define IPDIR_UNKNOWN    127
#define IPDIR_BACKGROUND 255

#define IPMax(a, b) ((b) > (a) ? (b) : (a))
#define IPMin(a, b) ((b) < (a) ? (b) : (a))
#define IPSqr(x) ((x) * (x))

#define IP_SQRT_8_MAX (1 << 8)
#define IP_SQRT_10_MAX (1 << 10)
#define IP_SQRT_12_MAX (1 << 12)
#define IP_SQRT_14_MAX (1 << 14)
#define IP_SQRT_16_MAX (1 << 16)
#define IP_SQRT_10_SHIFT 4
#define IP_SQRT_12_SHIFT 5
#define IP_SQRT_14_SHIFT 6
#define IP_SQRT_16_SHIFT 7
#define IP_SQRT_10_DELTA (1 << IP_SQRT_10_SHIFT)
#define IP_SQRT_12_DELTA (1 << IP_SQRT_12_SHIFT)
#define IP_SQRT_14_DELTA (1 << IP_SQRT_14_SHIFT)
#define IP_SQRT_16_DELTA (1 << IP_SQRT_16_SHIFT)
#define IP_SQRT_8_LENGTH IP_SQRT_8_MAX
#define IP_SQRT_10_LENGTH ((IP_SQRT_10_MAX - IP_SQRT_8_MAX) >> IP_SQRT_10_SHIFT)
#define IP_SQRT_12_LENGTH ((IP_SQRT_12_MAX - IP_SQRT_10_MAX) >> IP_SQRT_12_SHIFT)
#define IP_SQRT_14_LENGTH ((IP_SQRT_14_MAX - IP_SQRT_12_MAX) >> IP_SQRT_14_SHIFT)
#define IP_SQRT_16_LENGTH ((IP_SQRT_16_MAX - IP_SQRT_14_MAX) >> IP_SQRT_16_SHIFT)
#define IP_SQRT_8_INDEX(x) (x)
#define IP_SQRT_10_INDEX(x) ((x >> IP_SQRT_10_SHIFT) - IP_SQRT_10_DELTA)
#define IP_SQRT_12_INDEX(x) ((x >> IP_SQRT_12_SHIFT) - IP_SQRT_12_DELTA)
#define IP_SQRT_14_INDEX(x) ((x >> IP_SQRT_14_SHIFT) - IP_SQRT_14_DELTA)
#define IP_SQRT_16_INDEX(x) ((x >> IP_SQRT_16_SHIFT) - IP_SQRT_16_DELTA)

extern const unsigned char IPSqrt8_760[];
extern const unsigned char IPSqrt10_760[];
extern const unsigned char IPSqrt12_760[];
extern const unsigned char IPSqrt14_760[];
extern const unsigned char IPSqrt16_760[];
//#define IPSqrt255Value(x, result) \
//	(result) = (\
//	(x) < IP_SQRT_8_MAX ? IPSqrt8[IP_SQRT_8_INDEX(x)]\
//	: (x) < IP_SQRT_10_MAX ? IPSqrt10[IP_SQRT_10_INDEX(x)]\
//	: (x) < IP_SQRT_12_MAX ? IPSqrt12[IP_SQRT_12_INDEX(x)]\
//	: (x) < IP_SQRT_14_MAX ? IPSqrt14[IP_SQRT_14_INDEX(x)]\
//	: IPSqrt16[IP_SQRT_16_INDEX(x)]\
//	)

#define IPSqrt255_760(x, result) \
{\
if (x >= 255 * 255)\
	result = 255; \
else {\
	IPSqrt255Value(x, result); \
	if (IPSqr((result)+1) - (x) < (x)-IPSqr(result)) ++(result); \
}\
	}

#ifdef TEEOS
	#define IPCopyMem(dst, src, count) memcpy_s(dst, count, src, count)
	#define IPMoveMem(dst, src, count) memcpy_s(dst, count, src, count)
	#define IPFillMem(data, value, count) memset_s(data, count, value, count)
#else
	#define IPCopyMem(dst, src, count) memcpy(dst, src, count)
	#define IPMoveMem(dst, src, count) memmove(dst, src, count)
	#define IPFillMem(data, value, count) memset(data, value, count)
#endif
#define IPClearMem(data, count) IPFillMem(data, 0, count)

#define IP_IS_FP_IMAGE_STATUS_WATER_MARKS	201
#define IP_IS_FP_IMAGE_STATUS_WATER_DROP	203

#define IMAGE_TYPES	6
#define FP_IMAGE_IDENTIFIDER_OUT_OF_MEMORY -1;
#define FP_IMAGE_IDENTIFIER_BAD_IMAGE	-2;

#define IPTheOrient_760(orient) ((orient) & 127)
#define IPIsOrient(orient) ((orient) < IPDIR_180)
#define IPMakeBadArea(orient) ((orient) |= 128)

unsigned char **IPAllocByteImage_760(int width, int height);
int **IPAllocIntImage_760(int width, int height);
//void IPFreeImage(void **image, int height);
void IPFree2DImage_760(void **image);
//int IPComputeBadPointOrientImage(unsigned char *img, unsigned char *bp_map, int x, int y, int w, int h, const int window);
//int IPFixByDir(unsigned char *img, unsigned char* bp_table, int w, int h, int x, int y, int ori);
//void IPINGER_API IPSmoothImage(int width, int height, unsigned char * * image, int window);
//void IPINGER_API IPPuttyImage(int width, int height, unsigned char * * image);
//void IPINGER_API IPNormalizeImage(int width, int height, unsigned char * * image, unsigned char * * smoothedImage, int window);
//void IPINGER_API IPNormalizeImage2(int width, int height, unsigned char * * image, unsigned char * * smoothedImage, int window, unsigned char delta_th, unsigned char **delta_map);
//int ipcount_dir(int *hist, int torlent);
//void IPCopyByteImage(unsigned char ** target, const unsigned char * * source, int width, int height);
//void IPINGER_API IPComputeBlockedImageSize(int width, int height, int * pWidthB, int * pHeightB);
//void IPINGER_API IPComputeBlockedOrientImage(int width, int height, unsigned char * * image, int widthB, int heightB, unsigned char * * orientImageB, unsigned char * * cohImageB, unsigned char moduleThreshold);
//int IPINGER_API IPComputeBlockedBadArea(int widthB, int heightB, unsigned char * * orientImageB, unsigned char * * cohImageB, unsigned char cohThreshold);
//void IPINGER_API IPComputeOrientImage(int  width, int  height, unsigned char * * image, unsigned char * * orientImage, int * * cohSqrImage, int  window, unsigned char moduleThreshold);
//void IPINGER_API IPOrientSmoothImage(int  width, int  height, unsigned char * * image, unsigned char * * orientImage);
//int IPINGER_API IPComputeBadArea(int  width, int  height, unsigned char * * orientImage, int * * cohSqrImage,/*IPSingularPoint s * pPoint s,*/ int  window, int cohSqrThreshold/*, int  spDistance*/);
//int IPCountCornerIntensity(unsigned char*img, int w, int check_size);
//int IPCheckImageCornerByIntensity(unsigned char** img, int w, int h, int intensity_th);
//void IPsum_block(unsigned char *img, int w, int h, int qty, int *sum, int *count);
//int IPCheckRowRidge(unsigned char* img, int w, int h, int check_row_idx, int warp_size, int ridge_th);
//int IPCheckColumnRidge_760(unsigned char* img, int w, int h, int check_col_idx, int wipe_size, int ridge_th);
//int IPnormalize_vdm_converage_score(int score, int w, int h, int borderoffset);
//int IPmask_area(unsigned char* img, int w, int h);
//int IPcount_percentage_bsd_opt(unsigned char** in_img, int w, int h, unsigned char** map, int color, int color_th);
//int IPcount_avg_delta(unsigned char** delta_map, int w, int h, unsigned char** coh_table);
//void IPvdm_to_gray(unsigned char* img, int w, int h);
//int IPcount_percentage_bsd_opt2(unsigned char** in_img, int w, int h, unsigned char** map, int color, int color_th, int avg_color_check);
//int IPcheck_edge_length(unsigned char* mask, int w, int h);
#ifdef MEM_CHECK
void IPDumpMemory();
#endif
#ifdef __cplusplus
}
#endif
