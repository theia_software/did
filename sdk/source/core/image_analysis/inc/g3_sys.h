/******************************************************************************\
|*                                                                            *|
|*  g3_sys.h                                                                  *|
|*  Version: 1.4.3.11                                                          *|
|*  Date: 2017/06/30                                                          *|
|*  Revise Date: 2018/09/18                                                   *|
|*  Copyright (C) 2007-2017 Egis Technology Inc.                              *|
|*                                                                            *|
\******************************************************************************/

// Avoid multiple includes
#ifndef __G3_SYS_INCLUDED__
#define __G3_SYS_INCLUDED__

////////////////////////
//                    //
// Standard C headers //
//                    //
////////////////////////

// If compiled in C++ use C++ versions of standard headers

#ifdef __cplusplus
extern "C" {
#endif

#ifndef VFINGER_WOER
/*
#ifdef INT_MAX
#undef INT_MAX
#endif
#ifdef INT_MIN
#undef INT_MIN
#endif
*/
#ifndef TEEI
//#include <malloc.h>
#include <string.h>
//#include <math.h>
#include <stdlib.h>
#endif

#endif

/////////////////
//             //
// Basic types //
//             //
/////////////////
#ifndef BYTE_DEFINED
#define BYTE_DEFINED
typedef unsigned char BYTE;
#endif
#ifndef SBYTE_DEFINED
#define SBYTE_DEFINED
typedef signed char SBYTE;
#endif

// Pointer stuff

#ifndef NULL
#define NULL 0
#endif

#if !defined(_SIZE_T) && !defined(_SIZE_T_DEFINED) && !defined(__size_t)
#define _SIZE_T_DEFINED
#define __size_t
typedef unsigned int size_t;
#endif

// Linux detection

#if defined(linux) || defined(__LINUX__)
#define VFINGER_LINUX
#endif

// BOOL stuff
#ifndef FALSE
#define FALSE 0
#endif
#ifndef TRUE
#define TRUE 1
#endif

// G3Finger API and callbacks calling convention
#if defined(G3FINGER_LINUX)
#define G3FINGER_API
#define G3FINGER_CALLBACK
#else
#define G3FINGER_API __stdcall
#define G3FINGER_CALLBACK __stdcall
#endif

#ifdef LOG_TIME
DWORD VFINGER_API GetTickCount();
#endif
/////////////////////
//                 //
// Basic functions //
//                 //
/////////////////////

// Words
//#define VFMakeDWord(low, high) ((UINT)(((low) & 0xFFFF) | (((high) & 0xFFFF) << 16)))

// Math

#define G3Max(a, b) ((b) > (a) ? (b) : (a))
#define G3Min(a, b) ((b) < (a) ? (b) : (a))
#define G3Round(x) ((int)((x) >= 0 ? (x) + 0.5 : (x)-0.5))
#define G3RoundP(x) ((int)((x) + 0.5))
#define G3Sqr(x) ((x) * (x))

// Window
#define G3FullWindow(window) ((window)*2 + 1)

#define G3Div2E14(val) ((val + (val > 0 ? 8192 : -8192)) / 16384)

#ifdef VFINGER_WOER
int G3Abs(int x);
#else
#define G3Abs(x) ((x) >= 0 ? (x) : -(x))
#endif

// Memory
#ifdef VFINGER_WOER
typedef void*(VFINGER_CALLBACK* VF_MM_ALLOC)(size_t size);
typedef void*(VFINGER_CALLBACK* VF_MM_REALLOC)(void* data, size_t size);
typedef void(VFINGER_CALLBACK* VF_MM_FREE)(void* data);

int VFINGER_API VFSetMemoryManager(VF_MM_ALLOC alloc, VF_MM_REALLOC realloc, VF_MM_FREE free);

void* VFAlloc(size_t size);
void* VFCAlloc(size_t count, size_t size);
void* VFReAlloc(void* data, size_t size);
void VFFree(void* data);
#else

#ifdef LOG_MEM
extern int g_max_heap;
#endif
void* G3Alloc(size_t size);
void* G3CAlloc(size_t count, size_t size);
void* G3ReAlloc(void* data, size_t size);
void G3Free(void* data);
#endif

// Memory copying and filling

#ifdef VFINGER_WOER
void* VFCopyMem(void* dst, const void* src, size_t count);
void* VFMoveMem(void* dst, const void* src, size_t count);
void* VFFillMem(void* data, int value, size_t count);
#else
#define G3CopyMem(dst, src, count) memcpy(dst, src, count)
#define G3MoveMem(dst, src, count) memmove(dst, src, count)
#define G3FillMem(data, value, count) memset(data, value, count)
#endif

#define G3ClearMem(data, count) G3FillMem(data, 0, count)

// Strings

#ifdef VFINGER_WOER
size_t VFStrLen(const CHAR* str);
CHAR* VFCopyStr(CHAR* dst, const CHAR* src);
#else
#define G3StrLen(str) strlen(str)
#define G3CopyStr(dst, src) strcpy(dst, src)
#endif

// Objects
#define G3AllocObject(type) ((type*)G3Alloc(sizeof(type)))
#define G3CAllocObject(type) ((type*)G3CAlloc(1, sizeof(type)))
#define G3ClearObject(type, object) G3ClearMem(object, sizeof(type))
#define G3FreeObject(object) G3Free((void*)object)

// Rows
#define G3AllocRow(type, length) ((type*)G3Alloc((length) * sizeof(type)))
#define G3CAllocRow(type, length) ((type*)G3CAlloc((length), sizeof(type)))
#define G3ReAllocRow(type, row, length) ((type*)G3ReAlloc(row, (length) * sizeof(type)))
#define G3CopyRow(type, dstRow, srcRow, length) G3CopyMem(dstRow, srcRow, (length) * sizeof(type))
#define G3MoveRow(type, dstRow, srcRow, length) G3MoveMem(dstRow, srcRow, (length) * sizeof(type))
#define G3ClearRow(type, row, length) G3ClearMem(row, (length) * sizeof(type))
#define G3FreeRow(row) G3Free((void*)(row))

// Typed rows

#define G3AllocByteRow(length) G3AllocRow(BYTE, length)
#define G3AllocSByteRow(length) G3AllocRow(SBYTE, length)
#define G3AllocUShortRow(length) G3AllocRow(unsigned short, length)
#define G3AllocShortRow(length) G3AllocRow(short, length)
#define G3AllocUIntRow(length) G3AllocRow(unsigned int, length)
#define G3AllocIntRow(length) G3AllocRow(int, length)

#define G3CAllocByteRow(length) G3CAllocRow(BYTE, length)
#define G3CAllocSByteRow(length) G3CAllocRow(SBYTE, length)
#define G3CAllocUShortRow(length) G3CAllocRow(unsigned short, length)
#define G3CAllocShortRow(length) G3CAllocRow(short, length)
#define G3CAllocUIntRow(length) G3CAllocRow(unsigned int, length)
#define G3CAllocIntRow(length) G3CAllocRow(int, length)

#define G3ReAllocByteRow(row, length) G3ReAllocRow(BYTE, row, length)
#define G3ReAllocSByteRow(row, length) G3ReAllocRow(SBYTE, row, length)
#define G3ReAllocUShortRow(row, length) G3ReAllocRow(unsigned short, row, length)
#define G3ReAllocShortRow(row, length) G3ReAllocRow(short, row, length)
#define G3ReAllocUIntRow(row, length) G3ReAllocRow(unsigned int, row, length)
#define G3ReAllocIntRow(row, length) G3ReAllocRow(int, row, length)

#define G3CopyByteRow(dstRow, srcRow, length) G3CopyRow(BYTE, dstRow, srcRow, length)
#define G3CopySByteRow(dstRow, srcRow, length) G3CopyRow(SBYTE, dstRow, srcRow, length)
#define G3CopyUShortRow(dstRow, srcRow, length) G3CopyRow(unsigned short, dstRow, srcRow, length)
#define G3CopyShortRow(dstRow, srcRow, length) G3CopyRow(short, dstRow, srcRow, length)
#define G3CopyUIntRow(dstRow, srcRow, length) G3CopyRow(unsigned int, dstRow, srcRow, length)
#define G3CopyIntRow(dstRow, srcRow, length) G3CopyRow(int, dstRow, srcRow, length)

#define G3MoveByteRow(dstRow, srcRow, length) G3MoveRow(BYTE, dstRow, srcRow, length)
#define G3MoveSByteRow(dstRow, srcRow, length) G3MoveRow(SBYTE, dstRow, srcRow, length)
#define G3MoveUShortRow(dstRow, srcRow, length) G3MoveRow(unsigned short, dstRow, srcRow, length)
#define G3MoveShortRow(dstRow, srcRow, length) G3MoveRow(short, dstRow, srcRow, length)
#define G3MoveUIntRow(dstRow, srcRow, length) G3MoveRow(unsigned int, dstRow, srcRow, length)
#define G3MoveIntRow(dstRow, srcRow, length) G3MoveRow(int, dstRow, srcRow, length)

#define G3ClearByteRow(row, length) G3ClearRow(BYTE, row, length)
#define G3ClearSByteRow(row, length) G3ClearRow(SBYTE, row, length)
#define G3ClearUShortRow(row, length) G3ClearRow(unsigned short, row, length)
#define G3ClearShortRow(row, length) G3ClearRow(short, row, length)
#define G3ClearUIntRow(row, length) G3ClearRow(unsigned int, row, length)
#define G3ClearIntRow(row, length) G3ClearRow(int, row, length)

// Byte rows
#define G3FillByteRow(row, value, length) G3FillMem(row, value, (length) * sizeof(BYTE))

// Sorting

#define G3Sort(type, data, count)                                                                  \
    {                                                                                              \
        type* G3Sort_s = data;                                                                     \
        type* G3Sort_e = (data) + (count)-1;                                                       \
        while (G3Sort_e > G3Sort_s) {                                                              \
            type *G3Sort_p, *G3Sort_max;                                                           \
            type G3Sort_tmp;                                                                       \
            for (G3Sort_max = G3Sort_s, G3Sort_p = G3Sort_s + 1; G3Sort_p <= G3Sort_e; G3Sort_p++) \
                if (*G3Sort_p > *G3Sort_max) G3Sort_max = G3Sort_p;                                \
            G3Sort_tmp = *G3Sort_e;                                                                \
            *(G3Sort_e--) = *G3Sort_max;                                                           \
            *G3Sort_max = G3Sort_tmp;                                                              \
        }                                                                                          \
    }

#define G3SortReverse(type, data, count)                                                        \
    {                                                                                           \
        type* G3SortReverse_s = data;                                                           \
        type* G3SortReverse_e = (data) + (count)-1;                                             \
        while (G3SortReverse_s < G3SortReverse_e) {                                             \
            type *G3SortReverse_p, *G3SortReverse_max;                                          \
            type G3SortReverse_tmp;                                                             \
            for (G3SortReverse_max = G3SortReverse_s, G3SortReverse_p = G3SortReverse_s + 1;    \
                 G3SortReverse_p <= G3SortReverse_e; G3SortReverse_p++)                         \
                if (*G3SortReverse_p > *G3SortReverse_max) G3SortReverse_max = G3SortReverse_p; \
            G3SortReverse_tmp = *G3SortReverse_s;                                               \
            *(G3SortReverse_s++) = *G3SortReverse_max;                                          \
            *G3SortReverse_max = G3SortReverse_tmp;                                             \
        }                                                                                       \
    }

// Typed sorting

#define G3ByteSort(data, count) G3Sort(BYTE, data, count)
#define G3SByteSort(data, count) G3Sort(SBYTE, data, count)
#define G3UShortSort(data, count) G3Sort(short, data, count)
#define G3ShortSort(data, count) G3Sort(short, data, count)
#define G3UIntSort(data, count) G3Sort(unsigned int, data, count)
#define G3IntSort(data, count) G3Sort(int, data, count)

#define G3ByteSortReverse(data, count) G3SortReverse(BYTE, data, count)
#define G3SByteSortReverse(data, count) G3SortReverse(SBYTE, data, count)
#define G3UShortSortReverse(data, count) G3SortReverse(short, data, count)
#define G3ShortSortReverse(data, count) G3SortReverse(short, data, count)
#define G3UIntSortReverse(data, count) G3SortReverse(unsigned int, data, count)
#define G3IntSortReverse(data, count) G3SortReverse(int, data, count)

/////////////////////
//                 //
// Image functions //
//                 //
/////////////////////

BYTE** G3AllocByteImage(int width, int height);
BYTE** G3CAllocByteImage(int width, int height);
void G3CopyByteImage(BYTE** target, const BYTE** source, int width, int height);
void G3FreeImage(void** image);
#define G3FreeAnImage(image) G3FreeImage((void**)image)

#ifdef MEM_CHECK
void G3DumpMemory();
#endif

void G3SmoothImage(int width, int height, BYTE** image, int window);
void G3NormalizeImage(int width, int height, BYTE** image, BYTE** smoothedImage, int window);

/////////////////////////////////
//                             //
// Directions and orientations //
//                             //
/////////////////////////////////

#define G3DIR_0 0
#define G3DIR_45 30
#define G3DIR_90 (G3DIR_45 * 2)
#define G3DIR_135 (G3DIR_45 * 3)
#define G3DIR_180 (G3DIR_45 * 4)
#define G3DIR_225 (G3DIR_45 * 5)
#define G3DIR_270 (G3DIR_45 * 6)
#define G3DIR_315 (G3DIR_45 * 7)
#define G3DIR_360 (G3DIR_45 * 8)
#define G3DIR_UNKNOWN 127
#define G3DIR_BACKGROUND 255

#define G3GRAY_BLACK 0
#define G3GRAY_WHITE 255

#ifdef __cplusplus
}
#endif

#endif  //!__VFSYS_INCLUDED__
