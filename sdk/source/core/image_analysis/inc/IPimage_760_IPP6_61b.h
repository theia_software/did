/*********************************************
* @file    IPimage_760_IPP6_61b.h
*
* @author  Cliff Lo
* @version v6.61a
* @date    2020/09
* @brief   Fingerprint image processing
*          Adoptive project: ET760=200x200 (height x width)
*                            This function avoids the bad/dead pixel and dust make dynamic range incorrect.
* @fn      void IPimage_760_IPP6_61a(int *img, unsigned char *dst, int w, int h, int gSigmaHigh, int gSigmaLow, int thSigmaOutlier)
* @param   int *img => Image input (16-bit)
* @param   unsigned char *dst => Image output (8-bit)
* @param   int w, int h => Image width and height (cols x rows)
* @param   int gSigmaHigh,  int gSigmaLow => the gain of high and low boundary is the times of sigma.
* @param   int thSigmaOutlier => a threshold to the boundary is a times of sigma. 
*
* \note    v6.61b modified
*******    @brief   Based on ET760 fingerprint reflection lighting estimates the dynamic range of fingerprint.
					The function of tmp_roi_v2e adjusts the major mapping range when the standard deviation was changed. 
					Simplify the value of kernel at IPgaussian_blur_2e4
*******    @fn      IPimage_760_IPP6_61b(int *img, unsigned char *dst, int w, int h, int gSigmaHigh, int gSigmaLow, int thSigmaOutlier, 
*                                        int lce_gain_max_2e12, int lce_alpha_2e4, int clip_limit_param_2e10,
*                                        int tmp_low, int tmp_high)
*******    @param   int lce_gain_max_2e12 => max. gain limitation in function "local_contrast_enhance_v1"
*******    @param   int lce_alpha_2e4 => local signal gain in function "local_contrast_enhance_v1"
*******    @param   int clip_limit_param_2e10 => input parameter "clip_limit_param" multiple with 1024 in function "clahe" 
*******    @param   int tmp_low, tmp_high => max. and min. value of tone mapping output in function "tmp_roi_v2e"
*
**********************************************************************/

#ifndef ISP_IPP6_61B_H
#define ISP_IPP6_61B_H
	int gSigmaHigh=4, gSigmaLow=4, thSigmaOutlier=4;
	int lce_gain_max_2e12 = (int)(2.25 * 4096);
	int lce_alpha_2e4 = (int)(2 * 16);
	int clip_limit_param_2e10 = (int)(2 * 1024);
	int tmp_low = 10, tmp_high = 245;
	
	void IPimage_760_IPP6_61b(
		int *img, unsigned char *dst, int w, int h, 
		int gSigmaHigh, int gSigmaLow, int thSigmaOutlier, int lce_gain_max_2e12, 
		int lce_alpha_2e4, int clip_limit_param_2e10, int tmp_low, int tmp_high, int idx);
#endif