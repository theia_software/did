/******************************************************************************\
|*                                                                            *|
|*  InlineLib.h																  *|
|*  Version: 1.0.0.7                                                          *|
|*  Date: 2019/01/28                                                          *|
|*  Revise Date: 2019/03/21                                                   *|
|*  Copyright (C) 2007-2019 Egis Technology Inc.                              *|
|*                                                                            *|
\******************************************************************************/

#ifndef __INLINELIB_H__
#define __INLINELIB_H__
#define INLINELIB_VERSION "1.0.1.1"

#ifdef __cplusplus
extern "C" {
#endif

float cal_period_api(unsigned short* img, unsigned short* bkg, int width, int height, int cx,
                     int cy, int roi_width_half, int roi_height_half, int rot_ang1, int rot_ang2,
                     int period_1, int period_2, int* best_angle);
float cal_period(int* img, int* bkg, int width, int height, int cx, int cy, int roi_width_half,
                 int roi_height_half, int rot_ang1, int rot_ang2, int period_1, int period_2,
                 int* best_angle);

float cal_snr_level_api(unsigned short* img_set, unsigned short* bkg, int width, int height,
                        int img_num, int cx, int cy, int roi_width_half, int roi_height_half,
                        int rot_ang1, int rot_ang2, int period_1, int period_2, float* val_signal,
                        float* val_noise, float* best_angle);
float cal_snr_level_v2(int* img_set, int* bkg, int width, int height, int img_num, int cx, int cy,
                       int roi_width_half, int roi_height_half, int rot_ang1, int rot_ang2,
                       int period_1, int period_2, float* val_signal, float* val_noise,
                       float* best_angle);

void cal_show_snr_image_api(unsigned short* img, unsigned short* bkg, unsigned char* dst8_crop,
                            int w, int h, int w_crop, int h_crop, int ksize, int sigma_x100,
                            int angle);
int cal_charter_partial_api(unsigned short* charter, unsigned short* bk_img, int w, int h,
                            int threshold);
int cal_check_charter_order_api(unsigned short* charter, unsigned short* bk_img,
                                unsigned short* wk_img, int width, int height);
void cal_get_vertical_charter_api(unsigned short* charter, unsigned short* bk_img,
                                  unsigned short* vertical_img, int width, int height, int cx,
                                  int cy, int roi_width_half, int roi_height_half, int rot_ang1,
                                  int rot_ang2, int* best_angle);
int cal_check_charter_direction_api(unsigned short* img, unsigned short* bkg, int width, int height,
                                    int cx, int cy, int roi_width_half, int roi_height_half,
                                    int rot_ang1, int rot_ang2, int* best_angle);
// int cal_check_charter_roi_value(int *img, int *bkg, int width, int height);
int cal_check_charter_roi_value_api(unsigned short* img, unsigned short* bkg, int width,
                                    int height);

void BPgravity(unsigned short* src, int rows, int cols, int* cx, int* cy);

void BPdetection(unsigned short* img_set, unsigned char* ret, int img_num, int Rows, int Cols,
                 int cy, int cx, int roi_h, int roi_w, int startIndex, int steps, int expoTimeStar,
                 float threshold1, float threshold2, float threshold3, int windowStrides,
                 int* badBlockNum, int* continuBadPixelMax, int* totalBadPixelNum);

void IPexposure_estimate(float exp_begin, float percentage, int integral_count,
                         float exp_increase_by, float exp_stop, int bitnum, int* centerx,
                         int* centery, unsigned short* img, int w, int h, int count,
                         float* res_stoppedexp);

// mandatory parameters hwint hardware integration, radius set to 80 by default
// center_col == 0 && center_row == 0 use search algo, otherwise use specific value
void IPcal_moire_strength_number(unsigned short* img16, int width, int height, int hwint,
                                 int radius, int* center_col, int* center_row,
                                 float* moire_strength, int* moire_number);

void find_circle_center(int* bkg, int w, int h, int THR, int* cx, int* cy, int* radius);
unsigned char* circle_center_verify(int* bkg, int w, int h, int cx, int cy, int radius);
void moire_strength_detection_api(unsigned short* white_k_box, unsigned short* black_k_box,
                                  int width, int height, int hw_int, int* ans);
// void moire_strength_detection_kernel(int *raw, int *bkg, int width, int height, int HwInt, int
// *Top2);

int cal_img_avg_intensity_api(unsigned short* img, int w, int h, int cx, int cy, int roi_width_half,
                              int roi_height_half);

int cal_et713_check_wb_box_order_api(unsigned short* bk_img, unsigned short* wk_img, int width,
                                     int height);

#ifdef __cplusplus
};
#endif

#endif