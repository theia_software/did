#ifndef PARTIAL_CELL_H
#define PARTIAL_CELL_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdlib.h>

#include "partial_hist.h"

struct SplitResult {
    float sharp_score;
    int contrast_score;

    int max_peaks_num;
    int max_valleys_num;
    int max_profile_valley;

    int second_peaks_num;
    int second_valleys_num;
    int second_profile_valley;

    int weak_max_peaks_num;
    int weak_second_peaks_num;
    float mean;
    int sun_factor;
    int is_period;
    int is_light;
    int is_out_boundary;
};

unsigned char* detect_partial_cell(unsigned short* _pImg, unsigned short* _pBkg,
                                   unsigned char* _p8bit, int _width, int _height, int _cx, int _cy,
                                   int _radius_boundary, float _Et, int _HwInt,
                                   enum fp_type _fptype, enum model_type _modeltype,
                                   enum lens_type _lenstype,
                                   struct PartialDetectorResult* _pPartialDetectorResult);

void detect_partial_cell_SDK(unsigned short* _pImg, unsigned short* _pBkg, unsigned char* _p8bit,
                             int _width, int _height, int _cx, int _cy, int _radius_boundary,
                             float _Et, int _HwInt, enum fp_type _fptype,
                             enum model_type _modeltype, enum lens_type _lenstype,
                             struct PartialDetectorResult* _pPartialDetectorResult);

unsigned char* detect_partial_cell_A91(unsigned short* _pImg, unsigned short* _pBkg,
                                       unsigned char* _p8bit, int _width, int _height, int _cx,
                                       int _cy, int _radius_boundary, float _Et, int _HwInt,
                                       enum fp_type _fptype, enum model_type _modeltype,
                                       enum lens_type _lenstype,
                                       struct PartialDetectorResult* _pPartialDetectorResult);

void detect_partial_cell_A91_SDK(unsigned short* _pImg, unsigned short* _pBkg,
                                 unsigned char* _p8bit, int _width, int _height, int _cx, int _cy,
                                 int _radius_boundary, float _Et, int _HwInt, enum fp_type _fptype,
                                 enum model_type _modeltype, enum lens_type _lenstype,
                                 struct PartialDetectorResult* _pPartialDetectorResult);

unsigned char* detect_partial_cell_b(unsigned short* _pImg, unsigned short* _pBkg, int _width,
                                     int _height, float _Et, int _HwInt, enum fp_type _fptype,
                                     enum model_type _modeltype, enum lens_type _lenstype,
                                     struct PartialDetectorResult* _pPartialDetectorResult);

unsigned char* detect_partial_cell_702(unsigned short* _pImg, unsigned short* _pBkg,
                                       unsigned char* _p8bit, int _width, int _height, int _cx,
                                       int _cy, int _radius_boundary,
                                       // float _Et, int _HwInt,
                                       enum fp_type _fptype, enum model_type _modeltype,
                                       enum lens_type _lenstype,
                                       struct PartialDetectorResult* _pPartialDetectorResult);

#ifdef __cplusplus
}
#endif

#endif