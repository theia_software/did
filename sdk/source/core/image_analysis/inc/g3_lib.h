/******************************************************************************\
|*                                                                            *|
|*  g3_lib.h                                                                  *|
|*  Version: 1.4.4.2                                                          *|
|*  Date: 2017/05/12                                                          *|
|*  Revise Date: 2018/10/31                                                   *|
|*  Copyright (C) 2007-2018 Egis Technology Inc.                              *|
|*                                                                            *|
\******************************************************************************/
#ifndef G3_LIB_H
#define G3_LIB_H

#include "g3_sys.h"

#define G3_MATCHER_VER "1.4.4.2"

#ifndef _EMBEDDED
#define G3_ENROLL_FEATURE_ID 'E'
#define G3_FEATURE_ID 'G'
#define G3_ENROLL_ENVIRONMENT_ID 'R'
#define G3_FEATURE_VERSION 01
#define G3_ENROLL_FEATURE_HEADER_LEN 32
#else
#define G3_ENROLL_FEATURE_ID 'e'
#define G3_FEATURE_ID 'g'
#define G3_ENROLL_ENVIRONMENT_ID 'r'
#define G3_FEATURE_VERSION 00
#define G3_ENROLL_FEATURE_HEADER_LEN 4
#endif
#define G3_FEATURE_ROWDATA_MASK 0x02
#define G3_MAX_ENROLL 40
#define G3_MAX_FEATURE_COUNT 4096
#ifdef __cplusplus
extern "C" {
#endif
#define X2E14 16384
#define CELL_MAX 8192
#define CELL_MAX_H (CELL_MAX / 2)
#define CellNormalize(v) ((v + (v >= 0 ? CELL_MAX_H : -CELL_MAX_H)) / CELL_MAX)
#define FEATURES_HEADER_LEN 11
#define ENROLL_ENVIRONMENT_HEADER_LEN 42

#define MEM_POOL_SIZE(p) (*(short*)p)
#define MEM_POOL_BUFFER(p) ((G3Feature*)(p + sizeof(short)))

#ifndef _EMBEDDED
#define CellType int
#define MAT_MAX 0x7FFFFFFF
#define DESC_SIZE 64
#define DESCRIPTORS_SIZE(x) ((162 * x + 7) / 8)
#else
#define CellType short
#define MAT_MAX 0x7FFF
#define DESC_SIZE 16
#define DESCRIPTORS_SIZE(x) (DESC_SIZE)
#endif
#define FEATURE_PACK_SIZE(x) (DESCRIPTORS_SIZE(x) + 8)
typedef struct _Mat {
    int cols;
    int rows;
    CellType** data;
} Mat;

typedef struct _G3ExtractInfo {
    Mat* Lt;
    Mat* Lx;
    Mat* Ly;
} G3ExtractInfo;
typedef struct _G3Option {
    // extraction

    int esigma;      // x10 (10~20), Base scale offset (sigma units)
    int dthreshold;  // xCELL_MAX , Detector response threshold to accept point
    int extract_ext_data;
    int descriptor_channels;      ///< Number of channels in the descriptor (1, 2, 3)
    int descriptor_pattern_size;  ///< Actual patch size is 2*pattern_size*point.scale
    int resample_ratio;           // x1024
    // int single_border;
    int partial_method;
    int normalize_method;   // 0(none), 1(max-min), 2(moving normalize)
    int blurr_method;       // 0(none), 1(gaussian), 2(average 3x3)
    int descriptor_method;  // 0,1
    int derivative_method;  // 0,1(sharr),2(sobel
    int ipp_method;         // For 516 M Logo
    int main_angle_method;  // 0:(6*step)^2 gaussian blur rate=2.3 1:9^2 gaussian blur rate=3.25
    int min_vector_length;  // 0:disable
    int multiple_angle;     // 0:disable, max 1024
    int angle_search;
    // matching
    int nndr;          // x1024
    int nndr_recycle;  // x1024
    int min_hamming_distance;
    int rotation_tolerance;
    int offset_tolerance;
    int matching_threshold;
    int min_matching_count;
    int join_offset;
    int join_ref_diff;
    int nndr_recycle_method;
    int learning_coverage_threshold;
    int learning_upper_bound;

    // enrollment
    int min_enroll_count;
    int max_enroll_count;
    int redundant_level;
    int enroll_threshold;
    int redundant_method;
    int redundant_threshold;

    int learning_threshold;
    int max_feature_size;
    int enroll_mode;  // 0:touch 1:swipe
    int enroll_quick;
    // descriptor matching
    int least_score;
    int fix_pattern_tolerance;

    int free_image;
    G3ExtractInfo* dbg_info;

} G3Option;

typedef struct _G3Feature {
    CellType x, y;
    unsigned CellType response;
    BYTE type;
    SBYTE ref;
    short angle;
    BYTE desc[DESC_SIZE];
} G3Feature;
typedef struct _G3Features {
    int size;
    unsigned short width;
    unsigned short height;
    BYTE channel;
    BYTE percentage;
    BYTE qty;
    BYTE* raw_data;
    G3Feature** kf;
#ifdef _EMBEDDED
    BYTE pool_size;
    BYTE** mem_pool;
#endif
} G3Features;

typedef struct _G3EnrollFeature {
    int size;
    G3Features** feat;
} G3EnrollFeature;

typedef struct _G3CompressState {
    BYTE enroll_idx;
    int feat_header;
    short feat_idx;
    short size;
    int total_size;
} G3CompressState;

typedef struct _G3MatchDetail {
    int size;
    int* dist;
    int score;
    short rotation;
    short offset_x, offset_y;
    G3Feature** kf1;
    G3Feature** kf2;
    short dup_score;
    short dup_count;
} G3MatchDetail;
typedef struct _G3MatchDetailList {
    int size;
    int match_count;
    int match_percentage;
    G3MatchDetail** md_list;
    G3Features** vfeat_list;
} G3MatchDetailList;
#define Div2E16(val) ((val + (val > 0 ? 32768 : 32767)) >> 16)
#define Div2E12(val) ((val + (val > 0 ? 2048 : 2047)) >> 12)
#define Div2E10(val) ((val + (val > 0 ? 512 : 511)) >> 10)
#define Div2E6(val) ((val + (val > 0 ? 32 : 31)) >> 6)
#define Div2E4(val) ((val + (val > 0 ? 8 : 7)) >> 4)
int Ceil2E14(int val);
G3Option* g3_alloc_option(void);
G3Features* g3_alloc_features(int count);
G3EnrollFeature* g3_clone_enroll_feature(G3EnrollFeature* src);
G3Features* g3_clone_features(G3Features* src);
int g3_alloc_match_detail(G3MatchDetail* md, int size);
G3MatchDetailList* g3_alloc_match_detail_list(int size);
void g3_copy_features(G3Features* dst, G3Features* src);
BYTE** g3_clone_image(BYTE** img, int width, int height);
G3Features* g3_realloc_features(G3Features* src, int new_size);
G3EnrollFeature* g3_alloc_enroll_feature(int count);
int g3_remove_revoked_feature(G3Features* g3f);
int g3_compress_feature(G3Features* src, BYTE** dst, int* size);
int g3_decompress_feature(G3Features* dst, BYTE* src);
int g3_compress_enroll_feature(G3EnrollFeature* src, BYTE** dst, int* size, G3Option* opt);
int g3_compress_enroll_feature_multiple_step(G3EnrollFeature* src, BYTE* buf, int buf_size,
                                             G3CompressState* state, G3Option* opt);
int g3_decompress_enroll_feature(G3EnrollFeature* dst, BYTE* src);
int g3_enroll_feature_length(BYTE* compressed_enroll_feat);
int g3_feature_length(BYTE* compressed_feat);
int g3_is_valid_enroll_size(int size);
int g3_is_valid_feature_size(int size);
int estimate_feature_length(G3Features* src);
int estimate_enroll_feature_length(G3EnrollFeature* src, unsigned short* group_count, int min_cnt);
int g3_compress_feature_to_buffer(G3Features* src, BYTE* buf);

int g3_feature_size(G3Features* feat);
void g3_free_enroll_feature(G3EnrollFeature* enroll);
void g3_free_features(G3Features* g3f);
void g3_free_match_detail(G3MatchDetail* md);
void g3_free_match_detail_list(G3MatchDetailList* mdl);
void g3_free_option(G3Option* opt);

void matrix_average(Mat* src, Mat* dst, int window);
Mat* matrix_create(int w, int h);
void matrix_free(Mat* m);
void matrix_copy(Mat* dst, Mat* src);
void matrix_copy_partial(Mat* src, int x0, int y0, Mat* dst);
void matrix_normalize(Mat* m);
void matrix_horizontal_flip(Mat* m);
void matrix_vertical_flip(Mat* m);
void matrix_paste(Mat* dst, int x0, int y0, Mat* src);
void matrix_fill_value(Mat* dst, CellType val);
int matrix_in_range(Mat* m, int x, int y);
Mat* matrix_clone(Mat* m);
void matrix_resize(Mat* src, Mat* dst);
void matrix_quick_blur(Mat* src, Mat* dst);
CellType matrix_max(Mat* m);
CellType matrix_min(Mat* m);
void matrix_scharr_row(Mat* src, CellType* dx_row, CellType* dy_row, int y);
void matrix_scharr(Mat* src, Mat* dx, Mat* dy);
void matrix_sobel(Mat* src, Mat* dx, Mat* dy);
#define G3STS_OK 0
#define G3STS_CONTINUE 1
#define G3STS_MERGE_ENROLL_FINISH 2
#define G3STS_REDUNDANT 3
#define G3STS_REDUNDANT_ACCEPT 4
#define G3STS_NOT_CHAIN 5
#define G3STS_REDUNDANT_REJECT -8
#define G3STS_OUT_OF_MEMORY -1000
#define G3STS_UNKNOWN_FORMAT -1001
#define G3STS_INVALID_PARAMETER -1002
#define G3STS_LEARNING_FAIL -1003

#define REDUNDANT_METHOD_KEEP 100
#define REDUNDANT_METHOD_NOT_KEEP 101

#ifdef __cplusplus
}
#endif
#endif
