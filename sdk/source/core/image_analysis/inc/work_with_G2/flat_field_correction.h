//
//  flat_field_correction.h
//  AUO_BDS_ISP
//
//  Created by Luan Liu on 2020/8/11.
//  Copyright © 2020 Theia. All rights reserved.
//

#ifndef flat_field_correction_h
#define flat_field_correction_h

void et760_FlatFieldCorrection(int *img, int *bkg, int *w_bkg, int *result, int width, int height);

#endif /* flat_field_correction_h */
