#ifndef __LA_BDS_H__
#define __LA_BDS_H__

#include <stdint.h>

void labkg_init_bkg_data(unsigned char *bkg_pool, unsigned short *init_bkg, int width, int height);
void labkg_free_bkg_data();
//unsigned char *labkg_encode_bkg_data_stream(int *data_size);
//void labkg_decode_bkg_data_stream(unsigned char * data_stream);
int labkg_gen_bbkg(uint16_t *bbkg, int width, int height, int frame_num);
int labkg_gen_wbkg(uint16_t *wbkg, int width, int height, int frame_num);
int labkg_gen_bds_bkg(uint16_t *raw16, int width, int height, int frame_num);
int labkg_get_ffc_bds_data(unsigned short *bbkg, unsigned short *wbkg, 
                           unsigned char *bds_raw_data, int *bds_raw_data_len,
                           int width, int height);
int labkg_subtract_bkg(unsigned short *img, unsigned short *best_bkg, unsigned short *output_img, int width, int height);
int labkg_get_bkg(unsigned short *img, unsigned short *best_bkg, int width, int height);
unsigned short *labkg_get_bds_bkg(unsigned short *img, int width, int height);
void labkg_ISP(unsigned short *input_img, unsigned short *bkg, unsigned char *result_img, int width, int height);
int labkg_get_pool_size(int width, int height);
int labkg_check_pool(void *data);
int labkg_update(unsigned char *bkg_pool);

int labkg_flat_field_correction(unsigned short *img, unsigned short *best_bkg, unsigned short *output_img, int width, int height);
int labkg_denoise_normalize_8bits(
        unsigned short *input_img, unsigned char *result_img, int width, int height);
int labkg_set_image_pool(int type, unsigned short *image, int width, int height);
int labkg_get_image_pool(int type, unsigned short *image, int width, int height);
void labkg_convert_short_endian(unsigned short *src, unsigned short *dst, 
                                 int width, int height);
#endif
