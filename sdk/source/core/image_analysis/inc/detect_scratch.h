#ifndef __DETECT_SCRATCH_HEADER__
#define __DETECT_SCRATCH_HEADER__

#include "egis_definition.h"

int detect_scratch_update(uint16_t* img16bit, int width, int height, int hw_count);
int detect_scratch_do_mask(unsigned char* feat, int* len, int width, int height);
BOOL detect_scratch_is_detected();
int detect_scratch_init(int width, int height);
int detect_scratch_save();
int detect_scratch_remove();
void detect_scratch_uninit();

#endif