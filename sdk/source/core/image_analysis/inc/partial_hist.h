#ifndef PARTIAL_HIST_H
#define PARTIAL_HIST_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdlib.h>

//#define DEBUG
#define OUTPUT_MASK
//#define EGIS_DEBUG
//#define HW

#define BAND_WIDTH 10
#define SUM_KERNEL_SIZE 5
#define MASK_NUM -9999
#define SUN_NUM 100000
#define NAN -9999
#define ABS(x) (x > 0 ? x : -(x))
#define MIN10(x) (x > 10 ? x : 10)
#define MIN(x, y) (x > y ? y : x)
#define MAX(x, y) (x > y ? x : y)
#define FREE(x)      \
    if (x != NULL) { \
        free(x);     \
        x = NULL;    \
    }

enum fp_type {
    FP_NONE = 0,
    FP_ET711,
    FP_ET713,
    FP_ET715,
};

enum model_type {
    MODEL_UNKNOW = -1,
    MODEL_A50 = 0,
    MODEL_A70,
    MODEL_A80,
    MODEL_TAB_S6,
    MODEL_A90,
    MODEL_A30S,
    MODEL_A50S,
    MODEL_A70S,
    MODEL_A51,
    MODEL_A71,
    MODEL_A91,
    MODEL_A_NOTE,
    MODEL_702,
    MODEL_LG,
    MODEL_HW,
    MODEL_END
};

enum lens_type {
    LENS_UNKNOW = 0,
    LENS_2PA,
    LENS_3PA,
    LENS_3PC,
    LENS_3PD,
    LENS_3PE,
    LENS_3PF,
    LENS_3PG,
    LENS_END,
};

struct HistogramInfoPeak {
    int index;
    float intensity;
    float width;
};

struct HistogramInfoValley {
    int index;
    float intensity;
    float width;
};

struct HistogramInfo {
    float hist_max;
    int hist_max_position;

    int hist_HalfWidth_MedianPosition;
    int hist_HalfWidth;
    int hist_HalfWidth_noreset;

    int hist_HlafDozenWidth_MedianPosition;
    int hist_HlafDozenWidth;
    int hist_HlafDozenWidth_noreset;

    int hist_main_dark_peak;
    int waive_dark_peak_index;
    // int waive_light_peak_index;
};

struct HillSideInfo {
    int index;
    float width;
    float height;
    int direction;
    int mean;

    int pos_index;
    int pos_intensity;

    int neg_index;
    int neg_intensity;

    int is_noisy_boundary;
};

struct PartialDetectorResult {
    int partial_ratio;
    int inner_partial_ratio;
    int room_light_partial_ratio;
    int inner_room_light_partial_ratio;
    int sun_light_score;
    int room_light_score;
};

struct PartialResult {
    int maxThres;
    int minThres;
    int peakCenter;
    int room_light_score;

    int main_dark_peak;
    // int main_light_peak;
};

void normalize_raw(int* src_img, int w, int h, int roi_window, int target, int integrate_count);

int* hist_maxmin(int* img, int width, int height, float fEt_HwInt, int hist_num_thres,
                 int* hist_org_min, int* hist_size, int* hist5_max, int* hist5_min, int band_width);

unsigned char* detect_partial(unsigned short* usimg, unsigned short* usbkg, int width, int height,
                              int _cx, int _cy,  // int _radius_boundary,
                              float _Et, int _HwInt, enum fp_type _fptype,
                              enum model_type _modeltype, enum lens_type _lenstype,
                              struct PartialDetectorResult* partialDetectorResult);

void detect_partial_SDK(unsigned short* usimg, unsigned short* usbkg, int width, int height,
                        int _cx, int _cy,  // int _radius_boundary,
                        float _Et, int _HwInt, enum fp_type _fptype, enum model_type _modeltype,
                        enum lens_type _lenstype,
                        struct PartialDetectorResult* partialDetectorResult);

unsigned char* detect_partial_light(unsigned short* usimg, unsigned short* usbkg, int width,
                                    int height, int _cx, int _cy, int _radius_boundary, float _Et,
                                    int _HwInt, enum fp_type _fptype, enum model_type _modeltype,
                                    enum lens_type _lenstype,
                                    struct PartialDetectorResult* partialDetectorResult);

void detect_partial_light_SDK(unsigned short* usimg, unsigned short* usbkg, int width, int height,
                              int _cx, int _cy, int _radius_boundary, float Et, int HwInt,
                              enum fp_type _fptype, enum model_type _modeltype,
                              enum lens_type _lenstype,
                              struct PartialDetectorResult* partialDetectorResult);

#ifdef __cplusplus
}
#endif

#endif