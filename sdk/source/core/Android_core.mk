LOCAL_PATH := $(call my-dir)
$(info LOCAL_PATH=$(LOCAL_PATH))
GIT_PROJECT_RELATIVE := ../..
#---------------------------------------------
#
#	Prebuilt Matching Algorithm
#
COMMON_PLATFORM=linux
ifeq ($(SENSOR_TYPE),ET0XX)
ifeq ($(ET0XX_FLOW_ET7XX),true)
ALGORITHM_VERSION=$(BUILD_ALGORITHM_VERSION)
OPTICAL_FINGER_DETECT=true
else
ALGORITHM_VERSION=3
endif
ET7XX_SENSOR_TYPE=NA
endif

#ET760 is only used for selecting sensor control source files
ifeq ($(SENSOR_TYPE),ET7XX)
OPTICAL_FINGER_DETECT=true
ET7XX_SENSOR_TYPE=LARGE_AREA
ET760=true
ifeq ($(BUILD_ALGORITHM_VERSION),)
ALGORITHM_VERSION=3Plus
else
ALGORITHM_VERSION=$(BUILD_ALGORITHM_VERSION)
endif

else
#Not ET7XX
ET7XX_SENSOR_TYPE=NA
ET760=false
endif

ifeq ($(ALGORITHM_VERSION),)
# Capacity sensor
ALGORITHM_VERSION=3
BUILD_FEATURE_NAVIGATION=true
endif

ifeq ($(BUILD_CUSTOMER),)
CUSTOMER=internal
else ifeq ($(BUILD_CUSTOMER), 1)
CUSTOMER=internal
else ifeq ($(BUILD_CUSTOMER), 2)
CUSTOMER=google
else
CUSTOMER=internal
endif

$(info SENSOR_TYPE=$(SENSOR_TYPE)  PLATFORM_OTG=$(PLATFORM_OTG))

include $(CLEAR_VARS)
LOCAL_MODULE := MatchAlgo

$(info "ALGORITHM_VERSION = $(ALGORITHM_VERSION)")
$(info "ET7XA_SENSOR_TYPE = $(ET7XA_SENSOR_TYPE)")
$(info "ET7XX_SENSOR_TYPE = $(ET7XX_SENSOR_TYPE)")
$(info "ET7XX_SENSOR_TYPE = $(ET7XX_SENSOR_TYPE)")
$(info "CUSTOMER = $(CUSTOMER)")

ifeq ($(ALGORITHM_VERSION),2)
NEED_IMAGEPROCESS_LIB=true
LOCAL_SRC_FILES := matching_algo/G2AlgoLib/NONTZ/LogOff/libbiosign_$(TARGET_ARCH_ABI).a
endif

ifeq ($(ALGORITHM_VERSION),3)
LOCAL_SRC_FILES := matching_algo/G3AlgoLib/NONTZ/libEgisG3Algorithm_$(TARGET_ARCH_ABI).a
endif

ifeq ($(ALGORITHM_VERSION),3Plus)
$(info ALGORITHM_VERSION=3Plus)
ifeq ($(TARGET_ARCH_ABI),arm64-v8a)
LOCAL_SRC_FILES := matching_algo/G3PlusAlgoLib/NONTZ/libEgisG3Algorithm_$(TARGET_ARCH_ABI)_neon.a
else
LOCAL_SRC_FILES := matching_algo/G3PlusAlgoLib/NONTZ/libEgisG3Algorithm_$(TARGET_ARCH_ABI).a
endif
endif

ifeq ($(ALGORITHM_VERSION),5)
NEED_IMAGEPROCESS_LIB=true
LOCAL_SRC_FILES := matching_algo/G5AlgoLib/NONTZ/libBMF_$(TARGET_ARCH_ABI).a
endif

ifeq ($(ALGORITHM_VERSION),7)
NEED_IMAGEPROCESS_LIB=true
LOCAL_SRC_FILES := matching_algo/G7AlgoLib/NONTZ/libEgisGOAlgorithm_$(TARGET_ARCH_ABI).a
endif

ifeq ($(ALGORITHM_VERSION),4)
NEED_IMAGEPROCESS_LIB=true
LOCAL_SRC_FILES := matching_algo/G4AlgoLib/libmegtee.so
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_CLASS := SHARED_LIBRARIES
include $(PREBUILT_SHARED_LIBRARY)

else

LOCAL_MODULE_SUFFIX := .a
LOCAL_MODULE_CLASS := STATIC_LIBRARIES
include $(PREBUILT_STATIC_LIBRARY)

endif

#------------------------------
#  ImageProcess lib
#------------------------------
ifeq ($(NEED_IMAGEPROCESS_LIB),true)

ifeq ($(ET7XX_SENSOR_TYPE), LARGE_AREA)
include $(CLEAR_VARS)
LOCAL_MODULE := LargeAreaISP
LOCAL_SRC_FILES := image_analysis/inc/work_with_G2/NONTZ/libLargeAreaISP_$(TARGET_ARCH_ABI).a
include $(PREBUILT_STATIC_LIBRARY)
endif

include $(CLEAR_VARS)
LOCAL_MODULE := ImageProcess

ifeq ($(ET7XX_DISPLAY),)
LOCAL_SRC_FILES := image_analysis/inc/work_with_G2/NONTZ/libImageProcessingLib_$(TARGET_ARCH_ABI).a
else
$(info ET7XX_DISPLAY=$(ET7XX_DISPLAY))
LOCAL_SRC_FILES := image_analysis/inc/work_with_G2/NONTZ/libImageProcessingLib_$(ET7XX_DISPLAY)_$(TARGET_ARCH_ABI).a
endif
LOCAL_MODULE_SUFFIX := .a
LOCAL_MODULE_CLASS := STATIC_LIBRARIES
include $(PREBUILT_STATIC_LIBRARY)
endif

#------------------------------
#  Inline lib
#------------------------------
include $(CLEAR_VARS)
LOCAL_MODULE := InlineLib

ifeq ($(ET7XX_DISPLAY),A70)
ET7XX_DISPLAY := A50
endif

ifeq ($(ET7XX_DISPLAY),)
LOCAL_SRC_FILES := image_analysis/InlineLib/NONTZ/libInlineLib_$(TARGET_ARCH_ABI).a
else
$(info ET7XX_DISPLAY=$(ET7XX_DISPLAY))
LOCAL_SRC_FILES := image_analysis/InlineLib/NONTZ/libInlineLib_$(ET7XX_DISPLAY)_$(TARGET_ARCH_ABI).a
endif
LOCAL_MODULE_SUFFIX := .a
LOCAL_MODULE_CLASS := STATIC_LIBRARIES
include $(PREBUILT_STATIC_LIBRARY)

#------------------------------
#  rbs_MT lib
#------------------------------

ifeq ($(SENSOR_TYPE),ET7XX)
include $(CLEAR_VARS)

LOCAL_MODULE :=	rbs_MT
LOCAL_MODULE_FILENAME := librbs_MT
LOCAL_C_INCLUDES += \
	$(LOCAL_PATH)/../common/platform/inc \
	$(LOCAL_PATH)/../common/definition \
	$(LOCAL_PATH)/../common/utility/inih \
	$(LOCAL_PATH)/sensor_test/src/7XX_inline_test \
	$(LOCAL_PATH)/sensor_test/src/7xx_MT \
	$(LOCAL_PATH)/sensor_control/src/et7series/common/inc \
	$(LOCAL_PATH)/image_analysis/inc

LOCAL_CFLAGS := -O3 -D__LINUX__ -DNORMAL_WORLD -DSECURE_WORLD \
				-DMEMORY_SMALL -DEGIS_DBG \
				-DDEBUG -DENABLE_SUPREMA_LIB \
				-DRBS_SDK_USE

LOCAL_CFLAGS += -DBDS_MODE
LOCAL_CFLAGS += -Wall -Werror -Wno-unused-function -Wno-unused-variable -Wno-unused-label

ifeq ($(PLATFORM_OTG),true)
LOCAL_CFLAGS += -D__OTG_SENSOR__
endif

LOCAL_SRC_FILES += \
	sensor_test/src/7xx_MT/Sensor.c \
	sensor_test/src/7xx_MT/func.c \
	sensor_test/src/7xx_MT/MTFCounting.c \
	sensor_test/src/7xx_MT/egis_fp_config.c \
	sensor_test/src/7xx_MT/egis_fp_sensor_test.c \
	sensor_test/src/7xx_MT/ini_helper.c \
	sensor_test/src/7xx_MT/egisfp_et7xx_normal_scan.c

include $(BUILD_STATIC_LIBRARY)

endif

#------------------------------
#
#	Build egis core.
#
include $(CLEAR_VARS)


LOCAL_MODULE := egis_fp_core
LOCAL_LDLIBS := -L$(SYSROOT)/usr/lib -llog
LOCAL_EXPORT_LDLIBS := $(LOCAL_LDLIBS)
LOCAL_CFLAGS := -D__LINUX__  -DDEVICE_DRIVER_HAS_SPI -DRBS
LOCAL_CFLAGS += -DMTK_EVB
LOCAL_CFLAGS += -DxHEAP_LOG -DSIMULATE_LOAD_IMAGE
# LOCAL_CFLAGS += -DENROLL_POLICY_Y6
LOCAL_CFLAGS += -DENROLL_POLICY_REDUNDANT_ACCPET
LOCAL_CFLAGS += -DxFOD_CALIBRATION_DCFG
LOCAL_CFLAGS += -DEGIS_SPEED_DBG
LOCAL_CFLAGS += -Wall -Werror -Wno-unused-function -Wno-unused-variable -Wno-unused-label
ifeq ($(BUILD_RBS_RELEASE),true)
else
LOCAL_CFLAGS += -DEGIS_DBG \
		-DUSE_CORE_CONFIG_INI
ifeq ($(OPEN_DEBUG_MEMORY),true)
$(info debug build open EGIS_DEBUG_MEMORY)
LOCAL_CFLAGS += -DEGIS_DEBUG_MEMORY
LOCAL_SRC_FILES += ../common/platform/src/egis_mem_debug.c
endif
endif

LOCAL_EXPORT_CFLAGS := $(LOCAL_CFLAGS)

LOCAL_STATIC_LIBRARIES := MatchAlgo LargeAreaISP
ifeq ($(ALGORITHM_VERSION),2)
LOCAL_SRC_FILES += \
	../common/platform/src/$(COMMON_PLATFORM)/plat_thread_$(COMMON_PLATFORM).c
endif

ifeq ($(NEED_IMAGEPROCESS_LIB),true)
LOCAL_STATIC_LIBRARIES += ImageProcess
endif

ifeq ($(ALGORITHM_VERSION),2)
LOCAL_CFLAGS += -DALGO_GEN_2
LOCAL_C_INCLUDES := $(LOCAL_PATH)/matching_algo/inc/G2
endif
ifeq ($(ALGORITHM_VERSION),3Plus)
LOCAL_CFLAGS += -DG3_MATCHER -DG3PLUS_MATCHER
LOCAL_SRC_FILES += image_analysis/src/detect_scratch.c
LOCAL_C_INCLUDES := $(LOCAL_PATH)/matching_algo/inc/G3Plus
endif
ifeq ($(ALGORITHM_VERSION),3)
LOCAL_CFLAGS += -DG3_MATCHER
LOCAL_C_INCLUDES := $(LOCAL_PATH)/matching_algo/inc/G3
endif
ifeq ($(ALGORITHM_VERSION),4)
LOCAL_CFLAGS += -DALGO_GEN_4
LOCAL_CFLAGS += -DMATCHER_TAKE_RAW_IMAGE
LOCAL_CFLAGS += -DALGO_OUT_BPP_32
LOCAL_C_INCLUDES := $(LOCAL_PATH)/matching_algo/inc/G4AlgoLib
LOCAL_SRC_FILES += matching_algo/G4AlgoLib/faceplusutil.c
endif

ifeq ($(ALGORITHM_VERSION),5)
LOCAL_CFLAGS += -DALGO_GEN_5
LOCAL_C_INCLUDES := $(LOCAL_PATH)/matching_algo/G5AlgoLib/header
LOCAL_SRC_FILES += matching_algo/G5AlgoLib/header/g5_wrapper.c
endif
ifeq ($(ALGORITHM_VERSION),7)
LOCAL_CFLAGS += -DALGO_GEN_7
LOCAL_CFLAGS += -D__PC_x86_64__
#LOCAL_CFLAGS += -DSUPPORT_SAVE_DECISION_DATA
LOCAL_C_INCLUDES :=  \
	$(LOCAL_PATH)/matching_algo/G7AlgoLib/header \
	$(LOCAL_PATH)/image_analysis/inc/work_with_G7
LOCAL_SRC_FILES += matching_algo/G7AlgoLib/header/g7_wrapper.c
LOCAL_SRC_FILES += matching_algo/G7AlgoLib/header/detect_fa_attack.c
endif
LOCAL_STATIC_LIBRARIES += InlineLib

LOCAL_C_INCLUDES += \
	$(LOCAL_PATH)/../common/platform/inc \
	$(LOCAL_PATH)/../common/definition \
	$(LOCAL_PATH)/fp_api/include \
	$(LOCAL_PATH)/fp_api/inc \
	$(LOCAL_PATH)/enroll_verify/inc \
	$(LOCAL_PATH)/../common/utility/config/inc \
	$(LOCAL_PATH)/sensor_control/inc \
	$(LOCAL_PATH)/finger_detect/inc \
	$(LOCAL_PATH)/image_analysis/inc \
	$(LOCAL_PATH)/sensor_test/inc \
	$(LOCAL_PATH)/image_analysis/inc/work_with_G2 \

LOCAL_EXPORT_C_INCLUDES += $(LOCAL_C_INCLUDES)

# Use EGIS_IMAGE_V2
LOCAL_CFLAGS += -DEGIS_IMAGE_V2
LOCAL_SRC_FILES += fp_api/src/egis_image_v2.c
# Use EGIS_IMAGE_V1
# LOCAL_SRC_FILES += fp_api/src/egis_image_v1.c

ifeq ($(ET7XX_SENSOR_TYPE), LARGE_AREA)
LOCAL_CFLAGS += -DLARGE_AREA
endif

LOCAL_SRC_FILES += \
	../common/platform/src/$(COMMON_PLATFORM)/plat_log_$(COMMON_PLATFORM).c \
	../common/platform/src/$(COMMON_PLATFORM)/plat_mem_$(COMMON_PLATFORM).c \
	../common/platform/src/$(COMMON_PLATFORM)/plat_spi_$(COMMON_PLATFORM).c \
	../common/platform/src/$(COMMON_PLATFORM)/plat_file_$(COMMON_PLATFORM).c \
	../common/platform/src/$(COMMON_PLATFORM)/plat_time_$(COMMON_PLATFORM).c \
	../common/platform/src/$(COMMON_PLATFORM)/plat_std_$(COMMON_PLATFORM).c \
	../common/platform/src/$(COMMON_PLATFORM)/plat_reset_$(COMMON_PLATFORM).c \
	../common/platform/src/$(COMMON_PLATFORM)/plat_eeprom_$(COMMON_PLATFORM).c \
	../common/platform/src/egis_sprintf.c \
	fp_api/src/fp_sensormodule.c \
	fp_api/src/fp_algomodule.c \
	enroll_verify/src/egis_fp_algmodule.c \
	enroll_verify/src/matcher_feature.c \
	enroll_verify/src/template_manager.c \
	enroll_verify/src/template_file.c \
	enroll_verify/src/uk_box.c \
	enroll_verify/src/uk_manager.c \
	enroll_verify/src/detect_fa_attack.c \
	enroll_verify/src/verify_accuracy.c \
	finger_detect/src/egis_fp_get_image.c \
	finger_detect/src/fd_process.c \
	finger_detect/src/fd_process_16bit.c \
	finger_detect/src/fimage_check_qty.c \
	finger_detect/src/fimage_check_qm.c \
	sensor_control/src/isensor_api.c \
	sensor_control/src/egis_sensormodule.c \
	sensor_control/src/fp_capture_count.c \
	sensor_control/src/egis_handle_esd.c \
	sensor_test/src/mmi_sensor_test.c \
	image_analysis/src/flat_field_correction.c \
	image_analysis/src/image_analysis.c \
	image_analysis/src/IPimage_760_IPP6_61b.c \
	image_analysis/src/utility.c \
	image_analysis/src/tmp_roi_v2e.c \
	image_analysis/src/local_contrast_enhance_v1.c \
	image_analysis/src/ipsys.c \
	image_analysis/src/IPgaussian_blur.c \
	image_analysis/src/AES.c \
	command_handler/command_handler.c \
	command_handler/extra_operations.c \
	inline_handler/inline_handler.c

ifeq ($(findstring $(ALGORITHM_VERSION),4),)
LOCAL_SRC_FILES += \
	finger_detect/src/fd_process/weight_res.c
endif

ifeq ($(BUILD_FEATURE_NAVIGATION),true)
LOCAL_CFLAGS += -DFEATURE_NAVIGATION

LOCAL_C_INCLUDES += \
	$(LOCAL_PATH)/navi/inc

LOCAL_SRC_FILES += \
	navi/src/egis_navi.c \
	navi/src/navi_frame.c \
	navi/src/navi.c \
	image_analysis/src/puzzle_image.c
endif

ifeq ($(COMMON_PLATFORM),linux)
LOCAL_SRC_FILES += ../common/platform/src/linux/plat_otg_linux.c
endif

ifeq ($(SENSOR_TYPE),ET7XX)
LOCAL_CFLAGS += -D__ET7XX__
LOCAL_CFLAGS += -DSUPPORT_KEEP_RAW_16BIT
LOCAL_CFLAGS += -DKEEP_ENROLL_RETRY_IMAGES
#LOCAL_CFLAGS += -DEEPROM_USE

ifeq ($(PLATFORM_OTG),true)
LOCAL_CFLAGS += -D__OTG_SENSOR__
else
LOCAL_CFLAGS += -DxHIKEY960_EVB
endif

ifeq ($(ET760),true)
LOCAL_SRC_FILES += \
	sensor_control/src/et760/calibration_manager_760.c \
	sensor_test/src/7XX_inline_test/rbs_7xx_inline_handler.c
LOCAL_C_INCLUDES += \
	$(LOCAL_PATH)/sensor_control/src/et760/inc \
	$(LOCAL_PATH)/sensor_test/src/7XX_inline_test 
else
LOCAL_SRC_FILES += \
	sensor_control/src/et7series/common/calibration_manager.c \
	sensor_control/src/et7series/common/image_process_common.c \
    sensor_control/src/et7series/common/sensor_control_common.c
LOCAL_C_INCLUDES += $(LOCAL_PATH)/sensor_control/src/et7series/et7xa/inc \
	$(LOCAL_PATH)/sensor_control/src/et7series/common/inc
endif

ifeq ($(ET7XX_SENSOR_TYPE), LARGE_AREA)
LOCAL_CFLAGS += -DFAKE_IMAGE_FLOW
LOCAL_CFLAGS += -DLA_BDS 

ifneq ($(CUSTOMER), internal)
LOCAL_CFLAGS += -DG_INC_DEMO
endif

LOCAL_SRC_FILES += \
	sensor_control/src/et760/auo_oled_panel.c \
	sensor_control/src/et760/et760_sensor_control_common.c \
	sensor_control/src/et760/isensor_et760.c \
	sensor_control/src/et760/et760_frames.c \
	sensor_control/src/et760/et760_sensor_control.c
else
LOCAL_SRC_FILES += \
    sensor_control/src/et7series/isensor_et7xa.c \
	sensor_control/src/et7series/et7xa/et7xa_frames.c \
	sensor_control/src/et7series/et7xa/et7xa_sensor_control.c
endif

LOCAL_SRC_FILES := $(filter-out finger_detect/src/fd_process.c,$(LOCAL_SRC_FILES))
LOCAL_SRC_FILES += \
	finger_detect/src/fd_process/fd_process_common.c \
	finger_detect/src/fd_process/fd_process_et7xa.c \
	finger_detect/src/bkg_manager.c \
	finger_detect/src/fd_process/fd_post_ipp.c

else ifeq ($(SENSOR_TYPE),ET6XX)

LOCAL_C_INCLUDES += \
	$(LOCAL_PATH)/sensor_control/src/et6xx \
	$(LOCAL_PATH)/sensor_control/src
LOCAL_CFLAGS += -D__ET6XX__
LOCAL_CFLAGS += -Dx__SENSOR_ET601__
LOCAL_CFLAGS += -D__SENSOR_ET603A__
LOCAL_CFLAGS += -DSUPPORT_KEEP_RAW_16BIT
LOCAL_CFLAGS += -DPROJECT_CS1_ET613

LOCAL_SRC_FILES += \
	sensor_control/src/et6xx/egis_fp_calibration.c \
	sensor_control/src/et6xx/egis_fp_common_6XX.c \
	sensor_control/src/et6xx/et6xx_dump.c \
	sensor_control/src/et6xx/et6xx_rw_reg.c \
	sensor_control/src/et6xx/et6xx_recovery.c \
	sensor_control/src/et6xx/sensor_param.c


else ifeq ($(SENSOR_TYPE),ET0XX)

LOCAL_CFLAGS += -D__ET0XX__
LOCAL_STATIC_LIBRARIES += sensorc_et0xx

ifeq ($(BUILD_RBS_EVTOOL),true)
LOCAL_CFLAGS += -DRBS_EVTOOL
endif

ifeq ($(ET0XX_FLOW_ET7XX),true)
LOCAL_CFLAGS += -D__ET7XX__
LOCAL_CFLAGS += -DSUPPORT_KEEP_RAW_16BIT
LOCAL_C_INCLUDES += $(LOCAL_PATH)/sensor_control/src/et7series/et7xa/inc \
					$(LOCAL_PATH)/sensor_control/src/et7series/common/inc

LOCAL_SRC_FILES := $(filter-out finger_detect/src/fd_process.c,$(LOCAL_SRC_FILES))
LOCAL_SRC_FILES += \
	finger_detect/src/fd_process/fd_process_common.c \
	finger_detect/src/fd_process/fd_process_et7xa.c \
	finger_detect/src/bkg_manager.c \
	finger_detect/src/fd_process/fd_post_ipp.c

ifneq ($(ALGORITHM_VERSION),4)
ifneq ($(ET760),true)
LOCAL_SRC_FILES +=	sensor_control/src/et7series/common/bkimg_pool.c \
					sensor_control/src/et7series/common/bkimg_pool-header.c \
					finger_detect/src/fd_process/update_sensor.c
endif
endif
endif

else
LOCAL_CFLAGS += -D__ET5XX__
LOCAL_SRC_FILES += \
	sensor_control/src/et5xx/ICTeam/ET538LDOCalibrationFlow.c \
	sensor_control/src/et5xx/fingerprint_library.c \
	sensor_control/src/et5xx/egis_fp_calibration.c \
	sensor_control/src/et5xx/egis_fp_common_5XX.c \
	sensor_control/src/et5xx/sensor_param.c \
	sensor_control/src/et5xx/et5xx_calibration.c \
	sensor_test/src/egis_sensor_test.c \
	sensor_test/src/sensor_function.c
endif

ifeq ($(OPTICAL_FINGER_DETECT),true)
LOCAL_SRC_FILES += \
	finger_detect/src/fimage_check_optical.c \
	image_analysis/src/partial_hist.c \
	image_analysis/src/partial_cell.c
else
LOCAL_CFLAGS += -DxUSE_QTY_FOR_FINGER_DETECT
LOCAL_CFLAGS += -DUSE_QM_FOR_FINGER_DETECT
endif

ifeq ($(BUILD_AUTHTOKEN_HMAC),true)
# AUTHTOKEN_HMAC
LOCAL_CFLAGS += -DAUTHTOKEN_HMAC
LOCAL_SRC_FILES += \
	../common/platform/src/$(COMMON_PLATFORM)/plat_hmac_$(COMMON_PLATFORM).c \
	../common/platform/src/$(COMMON_PLATFORM)/plat_prng_$(COMMON_PLATFORM).c \
	command_handler/group_manager.c \
	command_handler/permission_ops.c
endif

# CONFIG_INI start
LOCAL_C_INCLUDES += \
	$(LOCAL_PATH)/../common/utility/inih \
	$(LOCAL_PATH)/../common/utility/cutimage/inc

LOCAL_SRC_FILES += \
	../common/utility/inih/ini.c \
	../common/utility/inih/ini_parser.c \
	../common/utility/config/src/core_config.c \
	../common/utility/cutimage/src/image_cut.c
# CONFIG_INI end

#------------------------------
#  7XX MT lib from CS1 optical branch
#------------------------------
ifeq ($(ET760), false)
ifeq ($(SENSOR_TYPE),ET7XX)
LOCAL_STATIC_LIBRARIES += rbs_MT

LOCAL_C_INCLUDES += \
	$(LOCAL_PATH)/sensor_test/src/7XX_inline_test \
	$(LOCAL_PATH)/image_analysis/inc/work_with_G2 \
	$(LOCAL_PATH)/sensor_test/src/7xx_MT

LOCAL_CFLAGS += -DOPTICAL_INLINE_TOOL -DRBS_SDK_USE
LOCAL_CFLAGS += -Wno-unused-function -Wno-unused-variable -Wno-unused-label -Wno-return-type

LOCAL_SRC_FILES += \
	sensor_test/src/7XX_inline_test/inline_obj_et7xx.c \
	sensor_test/src/7XX_inline_test/egisfp_test_et7xx_SNR.c \
	sensor_test/src/7XX_inline_test/rbs_7xx_inline_handler.c \
	sensor_test/src/7XX_inline_test/LensMFactorCounting.c \
	sensor_test/src/7XX_inline_test/LensFOVCounting.c

ifneq ($(ALGORITHM_VERSION),4)
ifneq ($(ET760),true)
LOCAL_SRC_FILES +=	sensor_control/src/et7series/common/bkimg_pool.c \
					sensor_control/src/et7series/common/bkimg_pool-header.c \
					finger_detect/src/fd_process/update_sensor.c
endif					
endif
endif
else
#TODO:add mt for et760
endif

LOCAL_LDFLAGS += -shared -nodefaultlibs -lc -lm -ldl

include $(BUILD_SHARED_LIBRARY)

ifeq ($(SENSOR_TYPE),ET0XX)
include $(LOCAL_PATH)/sensor_control/src/et0xx/Android_et0xx.mk
endif
