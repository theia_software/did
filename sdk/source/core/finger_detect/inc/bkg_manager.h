#ifndef __BKG_MANAGER_H__
#define __BKG_MANAGER_H__
#include <stdint.h>
#include "type_definition.h"

typedef enum bkg_pointer_type {
    BKG_TYPE_NORMALIZE = 0,
    BKG_TYPE_BDS,
    BKG_TYPE_CALIBRATION_BKG,
    BKG_TYPE_CALIBRATION_WK,
} bkg_pointer_type;

int bkm_get_bkg_image(enum bkg_pointer_type bkg_type, uint16_t* bkg_buffer, int bkg_buffer_size);
int bkm_init();
int bkm_uninit();
int bkm_save_bds();
int bkm_load_bds();
int bkm_reset_bds();
int bkm_renew_bkg(uint16_t* raw16, int temperature, float exp_time, int hw_integrate_count);
int bkm_update_bds(uint16_t* raw16, uint16_t* img_detect);
void bkm_set_folder_path(const char* folder_path);

int bkm_img_bbkg(uint16_t* raw16, int msg_size, unsigned char* out_data, int* out_data_size);
int bkm_img_wbkg(uint16_t* raw16, int msg_size, unsigned char* out_data, int* out_data_size);
int bkm_img_ffc(uint16_t* raw16, int msg_size, unsigned char* out_data, int* out_data_size);

#endif
