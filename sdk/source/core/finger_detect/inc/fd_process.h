#ifndef __FD_PROCESS_H__
#define __FD_PROCESS_H__

#include <stdint.h>

#include "egis_definition.h"

typedef int (*fn_fetch_image)(BYTE* frame, UINT height, UINT width, UINT number_of_frames);
typedef int (*fn_fetch_image_16bit)(unsigned short* frame, UINT height, UINT width,
                                    UINT number_of_frames);

void fd_set_fetch_image_fn(fn_fetch_image fetch_img, BOOL skip_process);
void fd_reset_fetch_image_fn();

void fd_set_fetch_single_image(BOOL single);
BOOL fd_get_fetch_single_image();

enum fd_process_option {
    FD_PROCESS_OPTION_NORMAL = 0,
    FD_PROCESS_OPTION_ONLY_GET_RAW = 1,
    FD_PROCESS_OPTION_USE_OLD_RAW = 2
};
int fd_process_create(uint8_t* ext_buf, uint32_t ext_buf_size, uint16_t num_image,
                      enum fd_process_option option);
void fd_process_destroy();

#define FD_ACQUIREI_OPTION_FROM_INLINE -10
#define FD_ACQUIREI_OPTION_GET_FFC_IMAGE -11
#define FD_ACQUIREI_OPTION_GET_FFC_IMAGE_NO_UPDATE_BDS -12

int fd_acquire_image(uint8_t* out_image, int out_width, int out_height, int option);
uint8_t* fd_get_current_raw_pointer(int* raw_bpp);
int fd_get_raw_bpp();

enum proc_type_t { PROC_INVERT = 1, PROC_CROP = 2 };

/**
 * @brief Do simple image process by in place buffer
 *
 * @param proc_type_t
 * @param intensity (8 bit)
 * @param in_width
 * @param in_height
 * @param out_width
 * @param out_height
 * @return Success 0
 */
int process_image_in_place(enum proc_type_t proc_type, uint8_t* intensity, int in_width,
                           int in_height, int out_width, int out_height);

typedef enum fd_param_type {
    FD_PARAM_INT_QUALITY = 10,
    FD_PARAM_INT_PARTIAL_PERCENTAGE,
    FD_PARAM_INT_CURRENT_INTEGRATED_COUNT,
    FD_PARAM_INT_IS_FAKE_FINGER,
    FD_PARAM_INT_IMAGE_IS_LIGHT,
    FD_PARAM_INT_G2_PARTIAL_QTY,
    FD_PARAM_INT_HAS_RESIDUAL,
} fd_param_type_t;

int fd_process_get_int(fd_param_type_t param_type, int* value);

#endif
