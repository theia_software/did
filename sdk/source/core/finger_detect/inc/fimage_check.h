#ifndef __FIMAGE_CHECK_H__
#define __FIMAGE_CHECK_H__

#include "egis_fp_get_image.h"
#include "type_definition.h"
#define FINGER_STABLE 0
#define FINGER_UNSTABLE 1
#define FINGER_ERROR 2

#define FCHECK_NON_FINGER 0
#define FCHECK_IS_FINGER 1
#define FCHECK_IS_NOISE 2

#define QM_GOOD 1000
#define QM_LOWER 1001
#define QM_NONE 1002
#define QM_ALWAYS_NONE 1003

BOOL fcheck_open_session(int fcheck_flow, int width, int height);
void fcheck_close_session();

void fcheck_update_image(uint8_t* img);
void fcheck_get_finger_status(FINGER_QUALITY* finger_on_status);
void fcheck_get_best_img_info(fd_img_analysis_info_t* bestImgInfo);

BOOL fcheck_is_finger();
BOOL fcheck_is_finger_checkremove();
BOOL fcheck_is_good_to_keep();
int fcheck_is_not_moving();
BOOL fcheck_is_similar();
BOOL fcheck_is_fake();
BOOL fcheck_is_light();
int fcheck_get_finger_is_malisious(int32_t* is_malisious);
#endif
