#pragma once
#include <stdint.h>
#include "algomodule.h"
#include "egis_definition.h"
#include "fd_process.h"
#include "fp_algomodule.h"
//
//	Finger quality
//
typedef UINT FINGER_QUALITY;
#define FINGER_QUALITY_NONE 0xFFFF
#define FINGER_QUALITY_GOOD 0
#define FINGER_QUALITY_BAD 1
#define FINGER_QUALITY_WET 2
#define FINGER_QUALITY_FAST 4
#define FINGER_QUALITY_PARTIAL 8
#define FINGER_QUALITY_RESIDUAL 16

enum FCHECK_FLOW {
    FCHECK_FLOW_VERIFY,
    FCHECK_FLOW_ENROLL,
    FCHECK_FLOW_NAVI,
    FCHECK_FLOW_VERIFY_REMOVE,
    FCHECK_FLOW_ENROLL_REMOVE,
    FCHECK_FLOW_NAVI_REMOVE,
    FCHECK_FLOW_CALI_VDM,
    FCHECK_FLOW_CALI_IMG,

    FCHECK_FLOW_VERIFY_NO_STABLE = 20,
    FCHECK_FLOW_ENROLL_NO_STABLE,
};

#define FETCH_RETRY_MAX_COUNT 5  // re-fetch image times limit
#define NO_FINGER_ADJUST_INTERRUPT_MAX 5
#define MAX_INCREASE_DETECT_DC_OFFSET 5
#define FINGER_DETECT_THRESHOLD 120
#define FINGER_DETECT_PERCENTAGE_THRESHOLD 30

#define SW_GAIN 48
#define SW_DC_OFFSET 0
#define SW_VRT 255
#define SW_VRB 0
#define AUTOSWGAIN 140

typedef struct {
    int bsd2;
    int qty;
    int cover_count;
    int corner_count;
    int cover_corner_diff;
    int img_level;
    int percentage;
    int intensity;
} fd_img_analysis_info_t;

typedef struct _IsFingerDatath {
    fd_img_analysis_info_t is_finger;
    fd_img_analysis_info_t not_finger;
} IsFingerDatath;

typedef struct _qmlib_threshold {
    int is_finger_qty;
    int is_finger_featsize;
    int is_finger_percentage;

    int not_finger_qty;
    int not_finger_featsize;
    int not_finger_percentage;

    int feat_crop_w;
    int feat_crop_h;
    int img_level;
} qmlib_threshold;

int fpsGetRawImage(BYTE* frame, int left, int top, uint32_t width, uint32_t height,
                   uint32_t number_of_frames);

BOOL IsTwoImageSimilar(unsigned char* in_img1, int w1, int h1, unsigned char* in_img2, int w2,
                       int h2, int option);
//
//  Get multiple frames with single big buffer
//
int get_multi_frames(unsigned char* buffer, unsigned int width, unsigned int height,
                     unsigned long image_counts);

/**
 * @brief Fetch sequtial frames (either from real sensor or from image files)
 * and get good images via egis_image
 *
 * @param FCHECK_FLOW [in]
 * @param egis_image [in]
 * @param bestImgInfo [out]
 * @param finger_on_status [out]
 * @return EGIS_FINGER_TOUCH
 *         EGIS_FINGER_NOT_TOUCH
 */
int fd_finger_detect(enum FCHECK_FLOW fcheck_flow, egis_image_t* egis_image,
                     fd_img_analysis_info_t* bestImgInfo, FINGER_QUALITY* finger_on_status,
                     enum fd_process_option fd_option);

int fd_check_finger_remove(enum FCHECK_FLOW fcheck_flow);

int fd_finger_detect_only_get_raw(unsigned char* ext_buffer, int ext_buffer_size,
                                  egis_image_t* egis_image);

int fd_add_current_raw_to_egis_image(void* pFingerPrintImage);

int fd_egis_image_init();

int fd_egis_image_deinit();

void fd_egis_set_capture_roi(int x0_ratio_x100, int y0_ratio_x100);
