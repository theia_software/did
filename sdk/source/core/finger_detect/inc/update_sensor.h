#ifndef __UPDATE_SENSOR_HEADER__
#define __UPDATE_SENSOR_HEADER__

#include "type_definition.h"

void update_sensor_BDS_in_fd_acquire(uint16_t* img16_ptr, uint16_t* img_detect, int size,
                                     int try_index, BOOL is_partial, BOOL has_residual);

#endif