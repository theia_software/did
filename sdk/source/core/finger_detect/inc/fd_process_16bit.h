#ifndef __FD_PROCESS_16BIT_H__
#define __FD_PROCESS_16BIT_H__

#include "egis_definition.h"

void copy_crop_image_16(int* src, int width, int height, int* dst, int crop_w, int crop_h);
int minus_background(unsigned short* frame, int image_size);
void img16_normalize(int* src, int width, int height, unsigned char* dst);
void raw16bitsTo8bits(unsigned short* raw_16bits, unsigned char* raw_8bits, INT width, INT height,
                      int scale_down);

void raw16bitsTo32(unsigned short* u16_raw, int* i32_raw, int width, int height);
void Remove_T_dummy(unsigned short* src_frame, int width, int height, unsigned short* out_frame);

#endif