#include <stdio.h>

#include "EgisAlgorithmAPI.h"
#include "ImageProcessingLib.h"
#include "bkg_manager.h"
#include "common_definition.h"
#include "core_config.h"
#include "egis_algomodule.h"
#include "egis_fp_get_image.h"
#include "fd_process.h"
#include "fimage_check.h"
#include "isensor_api.h"
#include "object_def_image.h"
#include "plat_mem.h"
#include "plat_time.h"
#include "uk_manager.h"

#include "plat_log.h"
#define LOG_TAG "RBS-EGISFP"

int g_reduce_no_finger_count = 0;

BYTE* g_image = NULL;
#define VERIFY_MAX_FINGER_STABLE_TRY 40
#define VERIFY_MIN_FINGER_STABLE_TRY 6
#define ENROLL_MAX_FINGER_STABLE_TRY 10
#define ENROLL_MIN_FINGER_STABLE_TRY 2
#define START_CHECK_SIMILAR \
    4  // start check similar image, get all images before this because first images are valuable.

#define FINGER_STABLE_TIMEOUT 600  // ms
#define DEFAULT_BURST_IMAGE_COUNT 1
#define FINGER_OFF_RETRY_MAX_COUNT 15
#define FINGER_OFF_RETRY_MAX_COUNT_TOUCH 150
#define FINGER_OFF_RETRY_MAX_COUNT_SWIPE 1

static unsigned long long g_flow_trymatch_start_time = 0;
static void _flow_trymatch_reset_timer(void);
static BOOL _flow_trymatch_is_timeout(void);
static int get_egis_image_size();
static int g_roi_x0 = 0;
static int g_roi_y0 = 0;

/* Function prototypes */
int image_range(int data);
int image_range(int data) {
    return data < 0 ? 0 : (data > 0xff ? 0xff : data);
}
int fpsGetRawImage(BYTE* frame, int left, int top, uint32_t width, uint32_t height,
                   uint32_t number_of_frames) {
    int ret = EGIS_OK;
    if (frame == NULL) {
        egislog_e("%s has wrong parameter (NULL frame)");
        return -1;
    }
    if (left >= 0 && top >= 0) {
        egislog_e("%s does not support to change (left,top)", __func__);
        return -2;
    }
    if (g_fp_mode != FP_SENSOR_MODE) {
        ret = isensor_set_sensor_mode();
        if (ret != EGIS_OK) {
            egislog_e("isensor_set_sensor_mode, ret = %d", ret);
            return ret;
        }
    }
    ret = isensor_get_frame(frame, height, width, number_of_frames);

    // static int testCount = 0;
    // if ((testCount % 2) == 1)
    // {
    // 	egislog_i("running IPbadpointfix");
    // 	IPbadpointfix(frame, SENSOR_FULL_WIDTH, SENSOR_HEIGHT,g_sensor_parameter.bad_pixel,5);
    // }
    // testCount++;
    return ret;
}

int fd_check_finger_remove(enum FCHECK_FLOW fcheck_flow) {
    int ret, i, result;
    int sensor_width, sensor_height;
    int image_width, image_height, image_dpi;
    int burst_image_count = DEFAULT_BURST_IMAGE_COUNT;
    unsigned char* frame_images = NULL;
    int enroll_method;
    int finger_off_retry_max_count = FINGER_OFF_RETRY_MAX_COUNT;
    //
    //	We capture X frames and confirm finger off
    //

    if (fcheck_flow == FCHECK_FLOW_ENROLL_REMOVE) {
        enroll_method =
            core_config_get_int(INI_SECTION_ENROLL, KEY_ENROLL_METHOD, ENROLL_METHOD_TOUCH);
        finger_off_retry_max_count = enroll_method == ENROLL_METHOD_TOUCH
                                         ? FINGER_OFF_RETRY_MAX_COUNT_TOUCH
                                         : FINGER_OFF_RETRY_MAX_COUNT_SWIPE;
    }

    ret = isensor_get_sensor_roi_size(&sensor_width, &sensor_height);
    if (ret != EGIS_OK) goto exit;

    fp_getImageSize(&image_width, &image_height, &image_dpi);

    if (burst_image_count > 1) {
        frame_images = NULL;
    } else {
        burst_image_count = 1;
        frame_images = g_image;
    }

    // if navi is interrupted by other command, Need to set sensing area to
    // normal size.
    ret = isensor_set_sensor_mode();
    if (ret != EGIS_OK) goto exit;

    int frame_image_size = get_egis_image_size();
    fd_process_create(frame_images, frame_image_size, burst_image_count, FD_PROCESS_OPTION_NORMAL);
    fcheck_open_session(fcheck_flow, image_width, image_height);

    for (i = 0; i < finger_off_retry_max_count; i++) {
        ret = fd_acquire_image(g_image, image_width, image_height, 0);
        if (ret != EGIS_OK) {
            goto exit;
        }

        fcheck_update_image(g_image);
        ret = fcheck_is_finger_checkremove();

        if (ret == FCHECK_NON_FINGER) break;
        if (i == (finger_off_retry_max_count - 1)) {
            result = isensor_calibrate(FPS_CALI_SDK_CHECK_FINGER_REMOVE);
            if (result == EGIS_OK)
                ret = FCHECK_NON_FINGER;
            else
                ret = EGIS_FINGER_NOT_REMOVED;
        }
        plat_wait_time(10);
    }

exit:
    fcheck_close_session();
    fd_process_destroy();

    egislog_d("finger removed ret:%d", ret);
    return ret;
}

static BOOL _check_img_size(egis_image_t* image, int w, int h) {
    if (image->img_data.format.width == w && image->img_data.format.height == h)
        return TRUE;
    else
        return FALSE;
}

static int _keep_in_buffer(egis_image_t* egis_image, uint8_t* curr_image, int width, int height) {
    int curent_index = egis_image->img_data.frame_count;
    unsigned char* img_ptr = egis_image_get_pointer(egis_image, curent_index);
    unsigned char* img_ptr_bpp = NULL;
    unsigned char* bkg_img_ptr_bpp = NULL;

    if (img_ptr == NULL && egis_image->img_data.frame_count > 0) {
        curent_index--;
        img_ptr = egis_image_get_pointer(egis_image, curent_index);
    }

    if (img_ptr != NULL) {
        egislog_v("try_match add [%d](%p)", egis_image->img_data.frame_count, img_ptr);
        // set current image is last image
        EGIS_IMAGE_KEEP_PARAM(is_last_image, 1);
#ifdef EGIS_IMAGE_V2
        if (curent_index > 0) {
            // set previous image is not last image
            egis_image_set_keep_param_index(curent_index - 1);
            EGIS_IMAGE_KEEP_PARAM(is_last_image, 0);
            egis_image_set_keep_param_index(curent_index);
        }
#endif
#if defined(EGIS_IMAGE_V2)
        int bpp = fp_getAlgoBpp();
        if (curr_image == NULL) {
            egis_image_erase_buffer(egis_image, curent_index, IMGTYPE_BIN);
        } else {
            egis_image_set_buffer(egis_image, curent_index, IMGTYPE_BIN, curr_image,
                                  width * height * bpp / 8);
        }
#else
        memcpy(img_ptr, curr_image, width * height);
#endif

#ifdef SUPPORT_KEEP_RAW_16BIT
        int raw_bpp;
        uint8_t* raw_img = fd_get_current_raw_pointer(&raw_bpp);
        img_ptr_bpp = egis_image_get_pointer_raw(egis_image, curent_index);
        int size = egis_image_get_buffer_size(egis_image, IMGTYPE_RAW);
        memcpy(img_ptr_bpp, raw_img, size);
#ifdef __ET7XX__
        bkg_img_ptr_bpp = egis_image_get_bkg_pointer_raw(egis_image, curent_index);
        int bkg_img_size = egis_image_get_buffer_size(egis_image, IMGTYPE_BKG);
        if (ukm_is_enabled() && egis_fp_is_enroll()) {
            int result = ukm_get_uk_bkg(bkg_img_ptr_bpp, bkg_img_size);
            if (result != EGIS_OK) {
                egis_image_erase_buffer(egis_image, curent_index, IMGTYPE_BKG);
            }
        } else {
            int bkg_use_option = core_config_get_int(INI_SECTION_SENSOR, KEY_BKG_IMG_USE_OPTION, 0);
#if 0
            int bkg_param =
                bkg_use_option > 0 ? PARAM_BUF_BDS_BKG_IMAGE : PARAM_BUF_CALI_NORMALIZE_BKG_IMAGE;
            isensor_get_buffer(bkg_param, (uint8_t*)bkg_img_ptr_bpp, &bkg_img_size);
#else
            bkg_pointer_type type = bkg_use_option > 0 ? BKG_TYPE_BDS : BKG_TYPE_NORMALIZE;
            bkm_get_bkg_image(type, (uint16_t*)bkg_img_ptr_bpp, bkg_img_size);
#endif
        }
#endif
#endif
        egis_image->img_data.frame_count = curent_index + 1;
    } else {
        egislog_e("img_data.capacity could be wrong");
    }
    return EGIS_OK;
}

static int _keep_bad_buffer(egis_image_t* egis_image, uint8_t* curr_image, int width, int height) {
#if defined(EGIS_IMAGE_V2)
    // EGIS_IMAGE_KEEP_PARAM(is_bad, 1);
    int retval = _keep_in_buffer(egis_image, curr_image, width, height);
    return retval;
#else
    unsigned char* img_ptr = egis_image_get_bad_pointer(egis_image, IMGTYPE_BIN);
    if (img_ptr != NULL) {
        egislog_v("%s (%p)", __func__, img_ptr);
        memcpy(img_ptr, curr_image, width * height);
    } else {
        egislog_e("no bad image pointer to keep(IMGTYPE_BIN)");
    }

#ifdef SUPPORT_KEEP_RAW_16BIT
    int raw_bpp;
    uint8_t* raw_img = fd_get_current_raw_pointer(&raw_bpp);
    img_ptr = egis_image_get_bad_pointer(egis_image, IMGTYPE_RAW);
    if (img_ptr != NULL) {
        int size = egis_image_get_buffer_size(egis_image, IMGTYPE_RAW);
        memcpy(img_ptr, raw_img, size);
    } else {
        egislog_e("no bad image pointer to keep");
    }
#endif
    return EGIS_OK;
#endif
}
int fd_finger_detect_only_get_raw(unsigned char* ext_buffer, int ext_buffer_size,
                                  egis_image_t* egis_image) {
    egislog_v("fd_finger_detect_only_get_raw");
    // For saving debug image
    static int index_fingeron = 0;
    static int index_image_series = 0;

    int image_width, image_height, image_dpi;
    int ret = fd_process_create(ext_buffer, ext_buffer_size, 1, FD_PROCESS_OPTION_ONLY_GET_RAW);
    if (ret != EGIS_OK) {
        return ret;
    }
#ifdef EGIS_IMAGE_V2
    egis_image_set_keep_param_index(egis_image->img_data.frame_count);
#endif
    if (ext_buffer != NULL) {
        return EGIS_OK;
    }
    if (egis_image->img_data.frame_count == 0) index_fingeron++;
    EGIS_IMAGE_KEEP_PARAM(index_fingeron, index_fingeron);
    EGIS_IMAGE_KEEP_PARAM(index_series, index_image_series++);
    egislog_d("%s, index_fingeron %d, index_series %d.", __func__, index_fingeron,
              index_image_series - 1);

    fp_getImageSize(&image_width, &image_height, &image_dpi);

    ret = fd_acquire_image(g_image, image_width, image_height, egis_image->img_data.frame_count);
    return ret;
}

int fd_egis_image_init(){
    int size = get_egis_image_size();
    PLAT_FREE(g_image);
    g_image = plat_alloc(size);
    RBS_CHECK_IF_NULL(g_image, EGIS_OUT_OF_MEMORY);
    return EGIS_OK;
}

int fd_egis_image_deinit(){
    PLAT_FREE(g_image);
    return EGIS_OK;
}

// this function will erase BIN and BKG buffer with same index
int fd_add_current_raw_to_egis_image(void* pFingerPrintImage) {
    RBS_CHECK_IF_NULL(pFingerPrintImage, EGIS_INCORRECT_PARAMETER);
    egis_image_t* images = (egis_image_t*)pFingerPrintImage;
    int image_width, image_height, image_dpi;
    fp_getImageSize(&image_width, &image_height, &image_dpi);
    _keep_in_buffer(images, NULL, image_width, image_height);
    return EGIS_OK;
}

int fd_finger_detect(enum FCHECK_FLOW fcheck_flow, egis_image_t* egis_image,
                     fd_img_analysis_info_t* bestImgInfo, FINGER_QUALITY* finger_on_status,
                     enum fd_process_option fd_option) {
    const int RETRY_IS_FINGER = 3;

    int try_count, ret;
    BOOL isFinger = FALSE;
    int detectRet = EGIS_FINGER_NOT_TOUCH;
    unsigned long long time_start = 0;
    unsigned char* scnd_img = NULL;
    unsigned char* finger_on_img = NULL;
    egislog_v("@egis-IsFP, fcheck flow %d.", fcheck_flow);
    int max_stable_count, min_stable_count;
    int burst_image_count = DEFAULT_BURST_IMAGE_COUNT;
    int enroll_method;
    int sensor_width, sensor_height;
    int image_width, image_height, image_dpi;
    unsigned char* frame_images;
    BOOL is_good_to_keep = FALSE;
    BOOL is_similar_finger = TRUE;
    int is_not_moving = FINGER_UNSTABLE;

    burst_image_count = core_config_get_int(INI_SECTION_SENSOR, KEY_BURST_IMAGE_COUNT, 1);
    enroll_method = core_config_get_int(INI_SECTION_ENROLL, KEY_ENROLL_METHOD, ENROLL_METHOD_TOUCH);

    ret = isensor_get_sensor_roi_size(&sensor_width, &sensor_height);
    if (ret != EGIS_OK) {
        return EGIS_FINGER_NOT_TOUCH;
    }

    fp_getImageSize(&image_width, &image_height, &image_dpi);

    if (((fcheck_flow == FCHECK_FLOW_ENROLL_NO_STABLE || fcheck_flow == FCHECK_FLOW_ENROLL) &&
         (enroll_method == ENROLL_METHOD_PAINT || enroll_method == ENROLL_METHOD_SWIPE))) {
#ifdef KEEP_ENROLL_RETRY_IMAGES
    } else if (fcheck_flow == FCHECK_FLOW_ENROLL_NO_STABLE) {
        // Do not reset frame count
        egislog_v("enroll - keep enroll images [%d]", egis_image->img_data.frame_count);
#endif
    } else if (fcheck_flow == FCHECK_FLOW_VERIFY_NO_STABLE) {
#ifndef DISABLE_TRY_MATCH_LEARNING
        if (g_flow_trymatch_start_time == 0)
            _flow_trymatch_reset_timer();
        else {
            if (_flow_trymatch_is_timeout()) {
                EGIS_IMAGE_RESET_FRAME_COUNT(egis_image);
                _flow_trymatch_reset_timer();
            }
        }
#endif
    } else {
        EGIS_IMAGE_RESET_FRAME_COUNT(egis_image);
    }

    if (fcheck_flow == FCHECK_FLOW_VERIFY_NO_STABLE ||
        fcheck_flow == FCHECK_FLOW_ENROLL_NO_STABLE) {
        max_stable_count = 0;
    } else if (fcheck_flow == FCHECK_FLOW_VERIFY) {
        max_stable_count = VERIFY_MAX_FINGER_STABLE_TRY;
        min_stable_count = VERIFY_MIN_FINGER_STABLE_TRY;
    } else if (fcheck_flow == FCHECK_FLOW_ENROLL) {
        max_stable_count = ENROLL_MAX_FINGER_STABLE_TRY;
        min_stable_count = ENROLL_MIN_FINGER_STABLE_TRY;
    }

    if (burst_image_count > 1) {
        frame_images = NULL;
    } else {
        burst_image_count = 1;
        frame_images = g_image;
    }
    int frame_image_size = get_egis_image_size();
    fd_process_create(frame_images, frame_image_size, burst_image_count, fd_option);
    fcheck_open_session(fcheck_flow, image_width, image_height);

#ifdef __ET7XX__
#ifdef EGIS_IMAGE_V2
    egis_image_set_keep_param_index(egis_image->img_data.frame_count);
#endif
#endif
    for (try_count = 0; try_count < RETRY_IS_FINGER; try_count++) {
#ifdef __ET7XX__
        ret =
            fd_acquire_image(g_image, image_width, image_height, egis_image->img_data.frame_count);
#else
        fd_acquire_image(g_image, image_width, image_height, 0);
#endif
        if (ret != EGIS_OK) {
            egislog_e("%s, fetch_image failed ret = %d ", __func__, ret);
            detectRet = ret;
            break;
        }

        fcheck_update_image(g_image);

        isFinger = fcheck_is_finger();

        if (isFinger || fd_get_fetch_single_image()) {
            detectRet = EGIS_FINGER_TOUCH;
            _keep_in_buffer(egis_image, g_image, image_width, image_height);
            break;
        }
    }

    if (isFinger && max_stable_count > 0 && !fd_get_fetch_single_image()) {
        try_count = 0;
        time_start = plat_get_time();
        for (try_count = 1; try_count <= max_stable_count; try_count++) {
            ret = fd_acquire_image(g_image, image_width, image_height,
                                   egis_image->img_data.frame_count);
            if (ret != EGIS_OK) {
                egislog_e("break, (check stable)[%d] fetch_image failed. ret=%d ", try_count, ret);
                break;
            }
#ifdef RBS_EVTOOL
            _keep_in_buffer(egis_image, g_image, image_width, image_height);
            continue;
#endif

            fcheck_update_image(g_image);

            if (fcheck_is_finger_checkremove() == FCHECK_NON_FINGER) {
                is_good_to_keep = FALSE;
                egislog_d("%s, break, FCHECK_NON_FINGER", __func__);
                break;
            }

            is_not_moving = fcheck_is_not_moving();
            is_similar_finger = fcheck_is_similar();
            is_good_to_keep = fcheck_is_good_to_keep();

            if (is_good_to_keep) {
                if (try_count >= START_CHECK_SIMILAR) {
                    if (!is_similar_finger) {
                        egislog_d("fcheck_is_good_to_keep");
                        _keep_in_buffer(egis_image, g_image, image_width, image_height);
                    }
                } else {
                    egislog_d("fcheck_is_good_to_keep");
                    _keep_in_buffer(egis_image, g_image, image_width, image_height);
                }
            }

            if (is_not_moving == FINGER_STABLE && try_count >= min_stable_count) {
                egislog_d("%s, break, FINGER_STABLE, try_count=[%d]", __func__, try_count);
                break;
            }
            if (plat_get_diff_time(time_start) > FINGER_STABLE_TIMEOUT) {
                egislog_d("%s, break, plat_get_diff_time over %d ms", __func__,
                          FINGER_STABLE_TIMEOUT);
                break;
            }
        }
        if (is_good_to_keep) {
            egislog_d("check to save last image");
            _keep_in_buffer(egis_image, g_image, image_width, image_height);
        }

        egislog_d("===== %s check stable end =====", __func__);
        egislog_d("%s, try_count = %d , frame_count = %d", __func__, try_count,
                  egis_image->img_data.frame_count);
    }

    fcheck_get_finger_is_malisious(&egis_image->quality.spoof);
    fcheck_get_finger_status(finger_on_status);
    fcheck_get_best_img_info(bestImgInfo);

#ifdef USE_QTY_FOR_FINGER_DETECT
    if (*finger_on_status == FINGER_QUALITY_GOOD && bestImgInfo->corner_count < 1) {
        egislog_e("corner_count = 0 !!! set bad quality");
        *finger_on_status = FINGER_QUALITY_BAD;
    }
#endif

    if (fd_get_fetch_single_image() && *finger_on_status != FINGER_QUALITY_GOOD) {
        egislog_v("fetch_single_image: set finger_on_status %d to GOOD", *finger_on_status);
        *finger_on_status = FINGER_QUALITY_GOOD;
    }

    if (detectRet == EGIS_FINGER_NOT_TOUCH) {
        _keep_bad_buffer(egis_image, g_image, image_width, image_height);
    }

    fcheck_close_session();
    fd_process_destroy();

    return detectRet;
}

BOOL IsTwoImageSimilar(unsigned char* in_img1, int w1, int h1, unsigned char* in_img2, int w2,
                       int h2, int forEnroll) {
#ifdef G3_MATCHER
    int score = 0, dx, dy, rotation;
    int ret =
        g3api_algo_match_two_image(in_img1, w1, h1, in_img2, w2, h2, &score, &dx, &dy, &rotation);
    if (ret < 0) {
        egislog_e("%s, algo ret=%d", __func__, ret);
        return FALSE;
    }
    int threshold = forEnroll ? 15 : 3;
    BOOL result = (score > 240 && abs(dx) < threshold && abs(dy) < threshold &&
                   (rotation < 20 || rotation > 340))
                      ? TRUE
                      : FALSE;
    egislog_d("%s [%d] s=%d, (%d,%d), r=%d, similar=%d", __func__, threshold, score, dx, dy,
              rotation, result);
    return result;
#else
    return 0;
#endif
}
//
//  Get multiple frames with single big buffer
// isensor_get_dynamic_frame
int get_multi_frames(unsigned char* buffer, unsigned int width, unsigned int height,
                     unsigned long image_counts) {
    unsigned long i;
    egislog_d("0907-1355 get_multi_frames %p, counts=%d, %d:%d", buffer, image_counts, width,
              height);
    if (buffer == NULL || image_counts == 0) return EGIS_INCORRECT_PARAMETER;

    unsigned long image_size = width * height;
    unsigned char* pImage = buffer;
    for (i = 0; i < image_counts; i++) {
        egislog_d("getting frame [%d]", i);
        if (isensor_get_dynamic_frame(pImage, height, width, 1)) {
            egislog_e("get_frame fail [%d]", i);
            return EGIS_COMMAND_FAIL;
        }
        pImage += image_size;
    }
    return EGIS_OK;
}

static void _flow_trymatch_reset_timer(void) {
    g_flow_trymatch_start_time = plat_get_time();
    egislog_d("%s", __func__);
}

static BOOL _flow_trymatch_is_timeout(void) {
    BOOL timeout = FALSE;
    unsigned long time_diff = plat_get_diff_time(g_flow_trymatch_start_time);
    if (time_diff > FLOW_TRY_MATCH_TIMEOUT) {
        egislog_d("%s, time_diff:%lu", __func__, time_diff);
        timeout = TRUE;
    }
    return timeout;
}

static int get_egis_image_size(){
    int width, height;
    int ret = isensor_get_sensor_roi_size(&width, &height);
    if(ret != EGIS_OK){
        width = 640;
        height = 500;
    }
    int size = width * height * fp_getAlgoBpp() / 8;
    egislog_d("%s, %d x %d bpp %d, %d bytes allocated 0x%08x", __func__, width, height, fp_getAlgoBpp(), size, g_image);
    return size;
}

void fd_egis_set_capture_roi(int x0_ratio_x100, int y0_ratio_x100){
    int roi_start_point_type = core_config_get_int(INI_SECTION_SENSOR, KEY_CAPTURE_ROI_START_POINT, INID_CAPTURE_ROI_START_POINT);
    int width, height, width_hw, height_hw;
    isensor_get_sensor_roi_size(&width, &height);
    isensor_get_sensor_full_size(&width_hw, &height_hw) ;


//    if(width == width_hw && height == height_hw) {
//        g_roi_x0 = 0;
//        g_roi_y0 = 0;
//        return;
//    }
    int sensor_orientation = core_config_get_int(INI_SECTION_SENSOR, KEY_SENSOR_ORIENTATION, INID_SENSOR_ORIENTATION);
    egislog_d("PAPA ORIENTATION sensor_orientation = %d", sensor_orientation);
    int touchx = 0 , touchy = 0, uix0 = 0, uiy0 = 0, x0 = 0, y0 = 0;
    switch (roi_start_point_type) {
        case VAL_START_FROM_LEFT_TOP:
            uix0 = 0;
            uiy0 = 0;
            break;
        case VAL_START_FROM_LEFT_BOTTOM:
            uix0 = 0;
            uiy0 = height_hw - height;
            break;
        case VAL_START_FROM_RIGHT_TOP:
            uix0 = width_hw - width;
            uiy0 = 0;
            break;
        case VAL_START_FROM_RIGHT_BOTTOM:
            uix0 = width_hw - width;
            uiy0 = height_hw - height;
            break;
        case VAL_START_FROM_CENTER:
            uix0 = (width_hw - width)/2;
            uiy0 = (height_hw - height)/2;
            break;
        case VAL_START_FROM_TOUCH_POINT:
            touchx = width_hw * x0_ratio_x100 / 100;
            touchy = height_hw * y0_ratio_x100 / 100;

            uix0 = touchx - width / 2;
            uiy0 = touchy - height / 2;
            if(uix0 < 0) {
                uix0 = 0;
            }
            if(uiy0 < 0) {
                uiy0 = 0;
            }
            if(uix0 > (width_hw - width)) {
                uix0 = width_hw - width;
            }
            if(uiy0 > (height_hw - height)) {
                uiy0 = height_hw - height;
            }
            break;
    }
    switch(sensor_orientation) {
        case 0:
            x0 = uix0;
            y0 = uiy0;
            break;
        case 90:
            x0 = uiy0;
            y0 = width_hw - uix0 - width;
            break;
        case 180:
            x0 = width_hw - uix0 - width;
            y0 = height_hw - uiy0 - height;
            break;
        case 270:
            x0 = height_hw - uiy0 - height;
            y0 = uix0;
            break;
    }

    egislog_i("ROI type %d, x0 %d y0 %d, w %d, h %d touchx %d touchy %d"
    , roi_start_point_type, x0, y0, width, height, x0_ratio_x100, y0_ratio_x100);
    egislog_i("darkresearch large_area_multi_finger type %d, x0 %d y0 %d, w %d, h %d  w_hw %d h_hw %d touchx %d touchy %d"
    , roi_start_point_type, x0, y0, width, height, width_hw, height_hw, x0_ratio_x100, y0_ratio_x100);

    g_roi_x0 = x0;
    g_roi_y0 = y0;
    EGIS_IMAGE_KEEP_PARAM_v2(x0, "x0", g_roi_x0);
    EGIS_IMAGE_KEEP_PARAM_v2(y0, "y0", g_roi_y0);
    isensor_set_int(PARAM_INT_ROI_X, g_roi_x0);
    isensor_set_int(PARAM_INT_ROI_Y, g_roi_y0);
    isensor_set_int(PARAM_INT_SENSOR_ORIENTATION, sensor_orientation) ;
}
