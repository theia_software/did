#ifdef USE_QM_FOR_FINGER_DETECT

#include <stdint.h>

#include "core_config.h"
#include "egis_definition.h"
#include "fimage_check.h"
#include "image_analysis.h"
#include "ini_definition.h"
#include "plat_log.h"
#include "plat_mem.h"
#include "qm_lib.h"
#ifdef __ET6XX__
#include "isensor_api.h"
#endif

#define LOG_TAG "RBS-EGISFP"

#define FD_STABLE_MATCH_THRESHOLD 200
#define PARTIAL_ENROLL_THRESHOLD 75
#define PARTIAL_VERIFY_THRESHOLD 65
static unsigned char* g_raw = NULL;
static QMOption* g_opt = NULL;
QMFeatures g_base_feat, g_new_feat;
static int g_last_result;
static int g_quality;
qmlib_threshold g_qmlib_th;
static int g_last_qm_match_score = 0;

static enum FCHECK_FLOW g_fcheck_flow;
static int g_img_width = 0;
static int g_img_height = 0;

static FINGER_QUALITY g_finger_on_status;

static BOOL g_analyzed;

#ifdef __ET6XX__
static int g_ipp_sensor_type;
static int g_use_vgg;
static int g_vgg_value;
static VggResult g_vgg_result;
static int g_refline_is_bad;
#endif

typedef enum {
    OPT_BLUR_AVERAGE = 0,
    OPT_BLUR_GAUSSIAN,
} qm_blur_method_t;

#define CHECK_SENSSION                                                             \
    if (g_img_width == 0 || g_img_height == 0 || g_opt == NULL || g_raw == NULL) { \
        egislog_e("session is not opened");                                        \
        return FALSE;                                                              \
    }

static void _get_threshold_by_sensor(qmlib_threshold* th);

BOOL fcheck_open_session(int fcheck_flow, int width, int height) {
    g_fcheck_flow = fcheck_flow;

    g_img_width = width;
    g_img_height = height;

    g_finger_on_status = FINGER_QUALITY_NONE;

    g_opt = qm_alloc_option();
    if (g_opt == NULL) {
        return FALSE;
    }
    g_raw = plat_alloc(width * height);
    if (g_raw == NULL) {
        egislog_e("%s, failed to alloc image %d:%d", __func__, width, height);
        return FALSE;
    }

    _get_threshold_by_sensor(&g_qmlib_th);
    g_last_result = QM_GOOD;

#ifdef __ET6XX__
    g_use_vgg = core_config_get_int(INI_SECTION_SENSOR, KEY_FIMAGE_CHECK_VGG, 0);
    g_vgg_value = 0;
    memset(&g_vgg_result, 0, sizeof(g_vgg_result));

    g_ipp_sensor_type = core_config_get_int(INI_SECTION_SENSOR, KEY_IPP_SENSOR_TYPE, DEFAULT_IPP);
#endif
    g_opt->blur_method =
        core_config_get_int(INI_SECTION_SENSOR, KEY_QM_OPT_BLUR_METHOD, OPT_BLUR_GAUSSIAN);

    return TRUE;
}

void fcheck_close_session() {
    if (g_opt) {
        qm_free_option(g_opt);
        g_opt = NULL;
    }
    PLAT_FREE(g_raw);

    g_img_width = 0;
    g_img_height = 0;
}

void fcheck_update_image(uint8_t* img) {
    g_analyzed = FALSE;

    if (g_raw != NULL) memcpy(g_raw, img, g_img_width * g_img_height);

#ifdef __ET6XX__
    isensor_get_int(PARAM_INT_REF_LINE_IS_BAD, &g_refline_is_bad);
#endif
}

void fcheck_get_finger_status(FINGER_QUALITY* finger_on_status) {
    *finger_on_status = g_finger_on_status;
}

void fcheck_get_best_img_info(fd_img_analysis_info_t* bestImgInfo) {
    egislog_d("%s entry,qmlib return directly", __func__);
    return;
}

static void _analyze_img() {
    QMFeatures qm_feat;
    BOOL ret = FALSE, img_level;
    BOOL extract_partial;

    if (g_analyzed) return;

#ifdef __ET6XX__
    if (g_use_vgg > 0) {
        g_vgg_value =
            IPvgg_finger_detect(g_ipp_sensor_type, g_raw, g_img_width, g_img_height, &g_vgg_result);
        egislog_d("IPvgg (%d) finger %d, percentage %d", g_fcheck_flow, g_vgg_value,
                  g_vgg_result.percentage);
    }
#endif

#define QM_MIN_DR_VALUR 30

    if (g_qmlib_th.img_level > 0) {
        img_level = IPis_fp_image_raw_info(g_raw, g_img_width, g_img_height, NULL);
        egislog_d("_IsFingerprint fp img_level %d", img_level);
        if (img_level < g_qmlib_th.img_level) {
            egislog_d("_IsFingerprint img_level %d < %d", img_level, g_qmlib_th.img_level);
            memset(&g_new_feat, 0, sizeof(QMFeatures));
            return;
        }
    }

    g_opt->count_percentage = TRUE;
    g_opt->min_dr_value = QM_MIN_DR_VALUR;
    g_opt->return_qty = TRUE;

    if (g_fcheck_flow == FCHECK_FLOW_CALI_VDM && g_opt->min_dr_value > QM_MIN_DR_VALUR)
        g_opt->min_dr_value = QM_MIN_DR_VALUR;

    extract_partial = g_qmlib_th.feat_crop_w > 0 && g_qmlib_th.feat_crop_w < g_img_width &&
                      g_qmlib_th.feat_crop_h > 0 && g_qmlib_th.feat_crop_h < g_img_height;
    egislog_d("%s, %d:%d  crop (%d) %d:%d", __func__, g_img_width, g_img_height, extract_partial,
              g_qmlib_th.feat_crop_w, g_qmlib_th.feat_crop_h);

    if (extract_partial) {
        qm_extract_partial(g_raw, g_img_width, g_img_height, g_qmlib_th.feat_crop_w,
                           g_qmlib_th.feat_crop_h, &qm_feat, g_opt);
    } else {
        qm_extract(g_raw, g_img_width, g_img_height, &qm_feat, g_opt);
    }
    memcpy(&g_new_feat, &qm_feat, sizeof(QMFeatures));

    egislog_d("_IsFingerprint (%d) qty %d diff %d,feat %d, percentage %d", g_fcheck_flow,
              g_new_feat.qty, g_new_feat.diff, g_new_feat.size, g_new_feat.percentage);

    g_analyzed = TRUE;
}
BOOL _IsFingerprint(enum FCHECK_FLOW fcheck_flow) {
    BOOL ret = FALSE;

    CHECK_SENSSION
    _analyze_img();

    switch (fcheck_flow) {
        case FCHECK_FLOW_ENROLL_NO_STABLE:
        case FCHECK_FLOW_VERIFY_NO_STABLE:
        case FCHECK_FLOW_VERIFY:
        case FCHECK_FLOW_ENROLL:
        case FCHECK_FLOW_CALI_VDM:
        case FCHECK_FLOW_CALI_IMG:
            if (g_new_feat.size > g_qmlib_th.is_finger_featsize) {
                ret = TRUE;
            } else if (g_new_feat.qty > g_qmlib_th.is_finger_qty &&
                       g_new_feat.size > g_qmlib_th.not_finger_featsize) {
                ret = TRUE;
            } else {
                ret = FALSE;
            }
            break;
        case FCHECK_FLOW_ENROLL_REMOVE:
        case FCHECK_FLOW_VERIFY_REMOVE:
            if (g_new_feat.size > g_qmlib_th.is_finger_featsize - 3) {
                ret = TRUE;
            } else if (g_new_feat.qty > g_qmlib_th.not_finger_qty &&
                       g_new_feat.size > g_qmlib_th.not_finger_featsize - 1) {
                ret = TRUE;
            } else {
                ret = FALSE;
            }
            break;
        case FCHECK_FLOW_NAVI:  // Todo
            break;
        case FCHECK_FLOW_NAVI_REMOVE:  // Todo
            break;
        default:
            if (g_new_feat.qty <= g_qmlib_th.is_finger_qty ||
                g_new_feat.size <= g_qmlib_th.not_finger_featsize)
                ret = FALSE;
            else
                ret = TRUE;
            break;
    }

#ifdef __ET6XX__
    if (g_use_vgg > 0) {
        egislog_d("IPvgg, is_finger = %d -> %d", ret, g_vgg_value);
        ret = (g_vgg_value == 0) ? FALSE : TRUE;
    }
    if (g_refline_is_bad) {
        egislog_d("refline, is_finger = %d -> %d", ret, g_vgg_value);
        ret = FALSE;
    }
#endif

    return ret;
}

static BOOL _is_partial() {
    int threshold, percentage = g_new_feat.percentage;
    if (g_fcheck_flow == FCHECK_FLOW_VERIFY || g_fcheck_flow == FCHECK_FLOW_VERIFY_REMOVE ||
        g_fcheck_flow == FCHECK_FLOW_VERIFY_NO_STABLE) {
        threshold = PARTIAL_VERIFY_THRESHOLD;
    } else {
        threshold = PARTIAL_ENROLL_THRESHOLD;
    }

#ifdef __ET6XX__
    if (g_use_vgg > 0) {
        percentage = g_vgg_result.percentage;
    }
#endif

    if (percentage < threshold) {
        egislog_d("%s, percentage: %d < %d", __func__, percentage, threshold);
        return TRUE;
    }
    return FALSE;
}

static void get_img_sensortype(int* series, int* sensor_type) {
    if (52 == g_img_height && 103 == g_img_width) {
        *sensor_type = ET510;
        *series = ET5XX_SERIES_ID;
    } else if (69 == g_img_height && 69 == g_img_width) {
        *sensor_type = ET511;
        *series = ET5XX_SERIES_ID;
    } else if (40 == g_img_height && 137 == g_img_width) {
        *sensor_type = ET512;
        *series = ET5XX_SERIES_ID;
    } else if (57 == g_img_height && 70 == g_img_width) {
        *sensor_type = ET516;
        *series = ET5XX_SERIES_ID;
    } else if (46 == g_img_height && 57 == g_img_width) {
        *sensor_type = ET520;
        *series = ET5XX_SERIES_ID;
    } else if (41 == g_img_height && 41 == g_img_width) {
        *sensor_type = ET522;
        *series = ET5XX_SERIES_ID;
    } else if (28 == g_img_height && 120 == g_img_width) {
        *series = g_egis_sensortype.series;
        if (g_egis_sensortype.series == ET6XX_SERIES_ID) {
            *sensor_type = ET613;
        } else {
            *sensor_type = ET523;
        }
    } else if (74 == g_img_height && 114 == g_img_width) {
        *sensor_type = ET601;
        *series = ET6XX_SERIES_ID;
    } else if (40 == g_img_height && 40 == g_img_width) {
        *sensor_type = ET602;
        *series = ET6XX_SERIES_ID;
    } else if (108 == g_img_width) {
        *sensor_type = ET605;
        *series = ET6XX_SERIES_ID;
    }
}

static void _get_threshold_by_sensor(qmlib_threshold* th) {
    int sensor_type = g_egis_sensortype.type;
    int series = g_egis_sensortype.series;

    int enable_cut_image = core_config_get_int(INI_SECTION_SENSOR, KEY_CUT_IMAGE, 0);
    if (enable_cut_image) {
        get_img_sensortype(&series, &sensor_type);
    }

    egislog_v("threshold setting qm (%d - %d)", series, sensor_type);
    if (series == ET5XX_SERIES_ID) {
        switch (sensor_type) {
            case ET518:
            case ET510:
            case ET511:
            case ET512:  // need to modify
                th->is_finger_qty = 100;
                th->is_finger_featsize = 20;
                th->is_finger_percentage = 60;

                th->not_finger_qty = 80;
                th->not_finger_featsize = 5;
                th->not_finger_percentage = 40;

                th->feat_crop_w = 52;
                th->feat_crop_h = 52;
                th->img_level = 3;
                break;
            case ET516:
                th->is_finger_qty = 100;
                th->is_finger_featsize = 20;
                th->is_finger_percentage = 60;

                th->not_finger_qty = 80;
                th->not_finger_featsize = 5;
                th->not_finger_percentage = 40;

                th->feat_crop_w = 52;
                th->feat_crop_h = 52;
                th->img_level = 3;
                break;
            case ET520:
            case ET522:
                th->is_finger_qty = 100;
                th->is_finger_featsize = 20;
                th->is_finger_percentage = 60;

                th->not_finger_qty = 80;
                th->not_finger_featsize = 5;
                th->not_finger_percentage = 40;

                th->feat_crop_w = 52;
                th->feat_crop_h = 52;
                th->img_level = 0;
                break;
            case ET523:
                th->is_finger_qty = 75;
                th->is_finger_featsize = 8;
                th->is_finger_percentage = 60;

                th->not_finger_qty = 70;
                th->not_finger_featsize = 3;
                th->not_finger_percentage = 40;

                th->feat_crop_w = 120;
                th->feat_crop_h = 28;
                th->img_level = 3;
                break;

            default:
                egislog_e("sensor type error, IC=%d ", g_egis_sensortype.type);
                break;
        }
    } else if (series == ET6XX_SERIES_ID) {
        switch (sensor_type) {
            case ET601:
                th->is_finger_qty = 100;
                th->is_finger_featsize = 20;
                th->is_finger_percentage = 60;

                th->not_finger_qty = 80;
                th->not_finger_featsize = 5;
                th->not_finger_percentage = 40;

                th->feat_crop_w = 52;
                th->feat_crop_h = 52;
                th->img_level = 0;
                break;
            case ET613:
                th->is_finger_qty = 75;
                th->is_finger_featsize = 18;
                th->is_finger_percentage = 60;

                th->not_finger_qty = 45;
                th->not_finger_featsize = 3;
                th->not_finger_percentage = 40;

                th->feat_crop_w = 96;
                th->feat_crop_h = 24;
                th->img_level = 0;
                break;
            case ET605:
                th->is_finger_qty = 75;
                th->is_finger_featsize = 18;
                th->is_finger_percentage = 60;

                th->not_finger_qty = 70;
                th->not_finger_featsize = 3;
                th->not_finger_percentage = 40;

                th->feat_crop_w = 120;
                th->feat_crop_h = 28;
                th->img_level = 0;
                break;

            default:
                egislog_e("sensor type error, IC=%d ", g_egis_sensortype.type);
                break;
        }
    } else {
        egislog_e("unknow sensor : %d-%d", g_egis_sensortype.series, g_egis_sensortype.type);
    }
}

BOOL fcheck_is_finger() {
    CHECK_SENSSION
    BOOL is_finger = FALSE;

    is_finger = _IsFingerprint(g_fcheck_flow);

#ifdef __ET6XX__
    if (g_use_vgg == 1) {
        if (is_finger) {
            g_finger_on_status = FINGER_QUALITY_GOOD;
        } else {
            g_finger_on_status = FINGER_QUALITY_BAD;
        }
        return is_finger;
    }
#endif

    if (is_finger) {
        if (_is_partial())
            g_finger_on_status = FINGER_QUALITY_PARTIAL;
        else
            g_finger_on_status = FINGER_QUALITY_GOOD;
    } else
        g_finger_on_status = FINGER_QUALITY_BAD;

    egislog_d("fcheck_is_finger, is_finger = %d", is_finger);
    return is_finger;
}

BOOL fcheck_is_finger_checkremove() {
    CHECK_SENSSION
    BOOL is_finger = FALSE;

    is_finger = _IsFingerprint(g_fcheck_flow);

    return is_finger;
}

BOOL fcheck_is_similar() {
    if (g_last_qm_match_score >= FD_STABLE_MATCH_THRESHOLD) {
        egislog_v("too similar: %d, threshold:%d", g_last_qm_match_score,
                  FD_STABLE_MATCH_THRESHOLD);
        return TRUE;
    }

    return FALSE;
}

BOOL fcheck_is_good_to_keep() {
    CHECK_SENSSION
    _analyze_img();

#ifdef __ET6XX__
    if (g_use_vgg > 0) {
        return (g_vgg_value == 0) ? FALSE : TRUE;
    }
#endif

    if (g_new_feat.size < g_qmlib_th.not_finger_featsize) {
        g_quality = QM_NONE;
    } else if (g_new_feat.size < g_qmlib_th.is_finger_featsize) {
        g_quality = QM_LOWER;
    } else {
        g_quality = QM_GOOD;
    }

    if (g_quality == QM_NONE && g_last_result == g_quality) {
        g_quality = QM_ALWAYS_NONE;
    }

    g_last_result = g_quality;

    if (g_quality == QM_NONE || g_quality == QM_ALWAYS_NONE) {
        return FALSE;
    }
    return TRUE;
}

int fcheck_is_not_moving() {
    int retval = FINGER_UNSTABLE;

    CHECK_SENSSION
    _analyze_img();

    g_last_qm_match_score = 0;

    QMMatchDetail md;

    g_last_qm_match_score = qm_match(&g_base_feat, &g_new_feat, &md, g_opt);
    egislog_d("qm_match score [%d] (%d,%d)", g_last_qm_match_score, md.dx, md.dy);
    if (g_last_qm_match_score > FD_STABLE_MATCH_THRESHOLD && md.dx == 0 && md.dy == 0) {
        egislog_d("==FINGER_STABLE== score[%d]", g_last_qm_match_score);
        retval = FINGER_STABLE;
    }

    memcpy(&g_base_feat, &g_new_feat, sizeof(QMFeatures));
    return retval;
}

int fcheck_get_finger_is_malisious(int32_t* is_malisious) {
    *is_malisious = 0;
    return FALSE;
}

#endif
