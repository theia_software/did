#ifdef USE_QTY_FOR_FINGER_DETECT

#include <stdint.h>

#include "ImageProcessingLib.h"
#include "core_config.h"
#include "egis_definition.h"
#include "fimage_check.h"
#include "image_analysis.h"
#include "ini_definition.h"
#include "plat_log.h"
#include "plat_mem.h"

#define LOG_TAG "RBS-EGISFP"

static enum FCHECK_FLOW g_fcheck_flow;
static uint8_t* g_img = NULL;
static int g_img_width = 0;
static int g_img_height = 0;
static int g_img_count = 0;

static fd_img_analysis_info_t g_bestImgInfo;
static FINGER_QUALITY g_finger_on_status;

#define FINGER_DETECT_BSD2_THRESHOLD 400
#define FINGER_DETECT_BSD2_NOT_FINGER_THRESHOLD 999

#define FINGER_PERCENTAGE_IS_PARTIAL 65

#define USE_QM_CHECK_FINGER_IS_NOT_MOVING
#ifdef USE_QM_CHECK_FINGER_IS_NOT_MOVING
#include "qm_lib.h"
#define FD_STABLE_MATCH_THRESHOLD 300
static unsigned char* g_raw = NULL;
static QMFeatures g_base_feat;
static int g_last_qm_match_score = 0;
#endif

static int minBSD2 = FINGER_DETECT_BSD2_NOT_FINGER_THRESHOLD;
static BOOL g_analyzed;
static int g_img_qty = 0;
static int g_corner_count = 0, g_cover_count = 0, g_img_level, g_percentage = 0;
static int g_img_intensity, g_bsd2;
static int g_qty_diff;

#define FCHECK_FAIL -1

#define CHECK_SENSSION                           \
    if (g_img_width == 0 || g_img_height == 0) { \
        egislog_e("session is not opened");      \
        return FALSE;                            \
    }

BOOL fcheck_open_session(int fcheck_flow, int width, int height) {
    g_fcheck_flow = fcheck_flow;

    g_img_width = width;
    g_img_height = height;
    g_img_count = 0;

    g_img_qty = 0;
    g_qty_diff = 255;
    minBSD2 = FINGER_DETECT_BSD2_NOT_FINGER_THRESHOLD;

    g_bestImgInfo.qty = 0;
    g_bestImgInfo.cover_count = 0;
    g_bestImgInfo.bsd2 = FINGER_DETECT_BSD2_NOT_FINGER_THRESHOLD;
    g_finger_on_status = FINGER_QUALITY_NONE;
#ifdef USE_QM_CHECK_FINGER_IS_NOT_MOVING
    if (g_raw == NULL) g_raw = plat_alloc(width * height);
    if (g_raw == NULL) return FALSE;
#endif
    return TRUE;
}

void fcheck_close_session() {
    g_img_width = 0;
    g_img_height = 0;
    g_img_count = 0;
#ifdef USE_QM_CHECK_FINGER_IS_NOT_MOVING
    PLAT_FREE(g_raw);
#endif
}

void fcheck_update_image(uint8_t* img) {
    g_analyzed = FALSE;

    g_img_count++;
    g_img = img;
}

void fcheck_get_finger_status(FINGER_QUALITY* finger_on_status) {
    *finger_on_status = g_finger_on_status;
}

void fcheck_get_best_img_info(fd_img_analysis_info_t* bestImgInfo) {
    egislog_d("%s entry", __func__);
    if (bestImgInfo == NULL) {
        egislog_e("bestImgInfo is null");
        return;
    }
    memcpy(bestImgInfo, &g_bestImgInfo, sizeof(g_bestImgInfo));
}

static void _update_best_img_info() {
    g_bestImgInfo.intensity = g_img_intensity;
    g_bestImgInfo.bsd2 = g_bsd2;
    g_bestImgInfo.qty = g_img_qty;
    g_bestImgInfo.cover_count = g_cover_count;
    g_bestImgInfo.corner_count = g_corner_count;
    g_bestImgInfo.img_level = g_img_level;
    g_bestImgInfo.percentage = g_percentage;
}

static void _analyze_img() {
    if (g_analyzed) return;

    g_corner_count = 0;
    g_cover_count = 0;
    g_percentage = 0;

    int img_qty = resample_IsFPImage_Lite(g_img, g_img_width, g_img_height, &g_corner_count,
                                          &g_cover_count, FALSE, &g_img_intensity);
    g_qty_diff = abs(img_qty - g_img_qty);
    g_img_qty = img_qty;

    g_img_level = IPis_fp_image_raw_info(g_img, g_img_width, g_img_height, NULL);

    IPnavi_is_fp_image_percentage(g_img, g_img_width, g_img_height, &g_percentage);
    g_bsd2 = IPcount_percentageBSD2(g_img, g_img_width, g_img_height);

    egislog_v("@egis-IsFP[qty:%d,corner:%d,cover:%d,level:%d]", g_img_qty, g_corner_count,
              g_cover_count, g_img_level);
    egislog_v("@egis-IsFP[percentage:%d,intensity:%d,bsd2:%d]", g_percentage, g_img_intensity,
              g_bsd2);

    if (g_bsd2 < minBSD2) {
        minBSD2 = g_bsd2;
    }

    g_analyzed = TRUE;
}

static BOOL _is_partial() {
    BOOL partial = FALSE;
    if (g_percentage < FINGER_PERCENTAGE_IS_PARTIAL) {
        partial = TRUE;
    }

    if (partial) egislog_v("@egis-IsFP[PARTIAL %d]", g_percentage);

    return partial;
}

static void _get_ET512_threshold(enum FCHECK_FLOW fcheck_flow, IsFingerDatath* th) {
    int th_not_finger_percentage = 20;  // less than this is partial
    int th_is_finger_percentage = 30;
    int th_not_finger_qty = 100;
    int th_is_finger_qty = 120;
    int th_is_finger_enroll_bsd2 = 160;
    int th_is_finger_verify_bsd2 = 390;

    switch (fcheck_flow) {
        case FCHECK_FLOW_ENROLL_NO_STABLE:
        case FCHECK_FLOW_ENROLL:
            th->not_finger.percentage = th_not_finger_percentage;

            th->is_finger.qty = th_is_finger_qty;
            th->is_finger.percentage = th_is_finger_percentage;
            th->is_finger.cover_corner_diff = 2;
            th->is_finger.img_level = 0;
            th->is_finger.bsd2 = th_is_finger_enroll_bsd2;
            break;

        case FCHECK_FLOW_VERIFY_NO_STABLE:
        case FCHECK_FLOW_VERIFY:
            th->not_finger.percentage = th_not_finger_percentage;

            th->is_finger.qty = th_is_finger_qty;
            th->is_finger.percentage = th_is_finger_percentage;
            th->is_finger.cover_corner_diff = 2;
            th->is_finger.img_level = 0;
            th->is_finger.bsd2 = th_is_finger_verify_bsd2;
            break;

        case FCHECK_FLOW_ENROLL_REMOVE:
            th->not_finger.qty = th_not_finger_qty - 10;
            th->not_finger.percentage = 20;
            th->not_finger.cover_count = 1;

            th->is_finger.qty = 200;
            th->is_finger.cover_corner_diff = 2;
            th->is_finger.img_level = 5;
            th->is_finger.percentage = 40;
            th->is_finger.bsd2 = th_is_finger_enroll_bsd2;
            break;

        case FCHECK_FLOW_VERIFY_REMOVE:
            th->not_finger.qty = th_not_finger_qty - 10;
            th->not_finger.percentage = 20;
            th->not_finger.cover_count = 1;

            th->is_finger.qty = 235;
            th->is_finger.cover_corner_diff = 3;
            th->is_finger.img_level = 10;
            th->is_finger.percentage = 80;
            th->is_finger.bsd2 = th_is_finger_enroll_bsd2;
            break;

        case FCHECK_FLOW_NAVI:
            th->is_finger.img_level = 0;
            th->is_finger.bsd2 = th_is_finger_verify_bsd2;
            break;

        case FCHECK_FLOW_NAVI_REMOVE:
            th->not_finger.percentage = 20;
            th->not_finger.img_level = 0;
            break;

        case FCHECK_FLOW_CALI_VDM:
            th->is_finger.qty = 20;
            th->is_finger.cover_corner_diff = 2;
            th->is_finger.percentage = 10;
            break;

        case FCHECK_FLOW_CALI_IMG:
            th->is_finger.qty = 1;
            th->is_finger.percentage = 1;
            break;
        default:
            egislog_e("sensor type error, IC=%d, mode = %d", g_egis_sensortype.type, fcheck_flow);
            break;
    }
}

static void _get_ET516_threshold(enum FCHECK_FLOW fcheck_flow, IsFingerDatath* th) {
    int th_not_finger_percentage = 30;
    int th_is_finger_percentage = 40;
    int th_not_finger_qty = 100;
    int th_is_finger_qty = 120;
    int th_is_finger_enroll_bsd2 = 180;
    int th_is_finger_verify_bsd2 = 460;

    switch (fcheck_flow) {
        case FCHECK_FLOW_ENROLL_NO_STABLE:
        case FCHECK_FLOW_ENROLL:
            th->not_finger.percentage = th_not_finger_percentage;

            th->is_finger.qty = th_is_finger_qty;
            th->is_finger.percentage = th_is_finger_percentage;
            th->is_finger.cover_corner_diff = 2;
            th->is_finger.img_level = 0;
            th->is_finger.bsd2 = th_is_finger_enroll_bsd2;
            break;

        case FCHECK_FLOW_VERIFY_NO_STABLE:
        case FCHECK_FLOW_VERIFY:
            th->not_finger.percentage = th_not_finger_percentage;

            th->is_finger.qty = th_is_finger_qty;
            th->is_finger.percentage = th_is_finger_percentage;
            th->is_finger.cover_corner_diff = 2;
            th->is_finger.img_level = 0;
            th->is_finger.bsd2 = th_is_finger_verify_bsd2;
            break;

        case FCHECK_FLOW_ENROLL_REMOVE:
            th->not_finger.qty = th_not_finger_qty;
            th->not_finger.percentage = 30;
            th->not_finger.cover_count = 1;

            th->is_finger.qty = 200;
            th->is_finger.cover_corner_diff = 2;
            th->is_finger.img_level = 4;
            th->is_finger.percentage = th_is_finger_percentage;
            th->is_finger.bsd2 = th_is_finger_enroll_bsd2;
            break;

        case FCHECK_FLOW_VERIFY_REMOVE:
            th->not_finger.qty = th_not_finger_qty;
            th->not_finger.percentage = 30;
            th->not_finger.cover_count = 1;

            th->is_finger.qty = 235;
            th->is_finger.cover_corner_diff = 3;
            th->is_finger.img_level = 7;
            th->is_finger.percentage = 80;
            th->is_finger.bsd2 = th_is_finger_enroll_bsd2;
            break;

        case FCHECK_FLOW_NAVI:
            th->is_finger.img_level = 0;
            th->is_finger.bsd2 = 400;
            break;

        case FCHECK_FLOW_NAVI_REMOVE:
            th->not_finger.percentage = 20;
            th->not_finger.img_level = 0;
            break;

        case FCHECK_FLOW_CALI_VDM:
            th->is_finger.qty = 30;
            th->is_finger.cover_corner_diff = 3;
            th->is_finger.percentage = 10;
            break;

        case FCHECK_FLOW_CALI_IMG:
            th->is_finger.qty = 10;
            th->is_finger.percentage = 1;
            break;
        default:
            egislog_e("sensor type error, IC=%d, mode = %d", g_egis_sensortype.type, fcheck_flow);
            break;
    }
}

static void _get_ET520_threshold(enum FCHECK_FLOW fcheck_flow, IsFingerDatath* th) {
    int th_not_finger_percentage = 60;  // less than this is partial
    int th_is_finger_percentage = 70;
    int th_not_finger_qty = 100;
    int th_is_finger_qty = 120;
    int th_is_finger_enroll_bsd2 = 180;  // 160;
    int th_is_finger_verify_bsd2 = 430;  // 390;

    switch (fcheck_flow) {
        case FCHECK_FLOW_ENROLL_NO_STABLE:
        case FCHECK_FLOW_ENROLL:
            th->not_finger.percentage = th_not_finger_percentage;

            th->is_finger.qty = th_is_finger_qty;
            th->is_finger.percentage = th_is_finger_percentage;
            th->is_finger.cover_corner_diff = 2;
            th->is_finger.img_level = 0;
            th->is_finger.bsd2 = th_is_finger_enroll_bsd2;
            break;

        case FCHECK_FLOW_VERIFY_NO_STABLE:
        case FCHECK_FLOW_VERIFY:
            th->not_finger.percentage = th_not_finger_percentage;

            th->is_finger.qty = th_is_finger_qty;
            th->is_finger.percentage = th_is_finger_percentage;
            th->is_finger.cover_corner_diff = 2;
            th->is_finger.img_level = 0;
            th->is_finger.bsd2 = th_is_finger_verify_bsd2;
            break;

        case FCHECK_FLOW_ENROLL_REMOVE:
            th->not_finger.qty = th_not_finger_qty - 10;
            th->not_finger.percentage = 20;
            th->not_finger.cover_count = 1;

            th->is_finger.qty = 200;
            th->is_finger.cover_corner_diff = 2;
            th->is_finger.img_level = 4;
            th->is_finger.percentage = 40;
            th->is_finger.bsd2 = th_is_finger_enroll_bsd2;
            break;

        case FCHECK_FLOW_VERIFY_REMOVE:
            th->not_finger.qty = th_not_finger_qty - 10;
            th->not_finger.percentage = 20;
            th->not_finger.cover_count = 1;

            th->is_finger.qty = 235;
            th->is_finger.cover_corner_diff = 3;
            th->is_finger.img_level = 7;
            th->is_finger.percentage = 80;
            th->is_finger.bsd2 = th_is_finger_enroll_bsd2;
            break;

        case FCHECK_FLOW_NAVI:
            th->is_finger.img_level = 0;
            th->is_finger.bsd2 = th_is_finger_verify_bsd2;
            break;

        case FCHECK_FLOW_NAVI_REMOVE:
            th->not_finger.percentage = 20;
            th->not_finger.img_level = 0;
            break;

        case FCHECK_FLOW_CALI_VDM:
            th->is_finger.qty = 20;
            th->is_finger.cover_corner_diff = 2;
            th->is_finger.percentage = 10;
            break;

        case FCHECK_FLOW_CALI_IMG:
            th->is_finger.qty = 1;
            th->is_finger.percentage = 1;
            break;
        default:
            egislog_e("sensor:%d ; parse flow error, unknow flow ,mode = %d",
                      g_egis_sensortype.type, fcheck_flow);
            break;
    }
}
static void _get_ET523_threshold(enum FCHECK_FLOW fcheck_flow, IsFingerDatath* th) {
    int th_not_finger_percentage = 20;  // less than this is partial
    int th_is_finger_percentage = 80;
    int th_not_finger_qty = 80;
    int th_is_finger_qty = 90;
    int th_is_finger_enroll_bsd2 = 180;  // 160;
    int th_is_finger_verify_bsd2 = 400;  // 390;

    switch (fcheck_flow) {
        case FCHECK_FLOW_ENROLL_NO_STABLE:
        case FCHECK_FLOW_ENROLL:
            th->not_finger.percentage = th_not_finger_percentage;

            th->is_finger.qty = th_is_finger_qty;
            th->is_finger.percentage = th_is_finger_percentage;
            th->is_finger.cover_corner_diff = 1;
            th->is_finger.img_level = 1;
            th->is_finger.bsd2 = th_is_finger_enroll_bsd2;
            break;

        case FCHECK_FLOW_VERIFY_NO_STABLE:
        case FCHECK_FLOW_VERIFY:
            th->not_finger.percentage = th_not_finger_percentage;

            th->is_finger.qty = th_is_finger_qty;
            th->is_finger.percentage = th_is_finger_percentage;
            th->is_finger.cover_corner_diff = 2;
            th->is_finger.img_level = 1;
            th->is_finger.bsd2 = th_is_finger_verify_bsd2;
            break;

        case FCHECK_FLOW_ENROLL_REMOVE:
            th->not_finger.qty = th_not_finger_qty - 10;
            th->not_finger.percentage = 20;
            th->not_finger.cover_count = 1;

            th->is_finger.qty = 200;
            th->is_finger.cover_corner_diff = 2;
            th->is_finger.img_level = 1;
            th->is_finger.percentage = 40;
            th->is_finger.bsd2 = th_is_finger_enroll_bsd2;
            break;

        case FCHECK_FLOW_VERIFY_REMOVE:
            th->not_finger.qty = th_not_finger_qty - 10;
            th->not_finger.percentage = 20;
            th->not_finger.cover_count = 1;

            th->is_finger.qty = 235;
            th->is_finger.cover_corner_diff = 3;
            th->is_finger.img_level = 1;
            th->is_finger.percentage = 80;
            th->is_finger.bsd2 = th_is_finger_enroll_bsd2;
            break;

        case FCHECK_FLOW_NAVI:
            th->is_finger.img_level = 0;
            th->is_finger.bsd2 = th_is_finger_verify_bsd2;
            break;

        case FCHECK_FLOW_NAVI_REMOVE:
            th->not_finger.percentage = 20;
            th->not_finger.img_level = 0;
            break;

        case FCHECK_FLOW_CALI_VDM:
            th->is_finger.qty = 20;
            th->is_finger.cover_corner_diff = 2;
            th->is_finger.percentage = 10;
            break;

        case FCHECK_FLOW_CALI_IMG:
            th->is_finger.qty = 1;
            th->is_finger.percentage = 1;
            break;
        default:
            egislog_e("sensor:%d ; parse flow error, unknow flow ,mode = %d",
                      g_egis_sensortype.type, fcheck_flow);
            break;
    }
}
static void _get_ET601_threshold(enum FCHECK_FLOW fcheck_flow, IsFingerDatath* th) {
    int th_not_finger_percentage = 20;  // less than this is partial
    int th_is_finger_percentage = 30;
    int th_not_finger_qty = 100;
    int th_is_finger_qty = 120;
    int th_is_finger_enroll_bsd2 = 0xFFFF;  // todo; skip bsd2 compare by Lei
    int th_is_finger_verify_bsd2 = 0xFFFF;  // todo; skip bsd2 compare by Lei

    switch (fcheck_flow) {
        case FCHECK_FLOW_ENROLL_NO_STABLE:
        case FCHECK_FLOW_ENROLL:
            th->not_finger.percentage = th_not_finger_percentage;

            th->is_finger.qty = th_is_finger_qty;
            th->is_finger.percentage = th_is_finger_percentage;
            th->is_finger.cover_corner_diff = 2;
            th->is_finger.img_level = 0;
            th->is_finger.bsd2 = th_is_finger_enroll_bsd2;
            break;

        case FCHECK_FLOW_VERIFY_NO_STABLE:
        case FCHECK_FLOW_VERIFY:
            th->not_finger.percentage = th_not_finger_percentage;

            th->is_finger.qty = th_is_finger_qty;
            th->is_finger.percentage = th_is_finger_percentage;
            th->is_finger.cover_corner_diff = 2;
            th->is_finger.img_level = 0;
            th->is_finger.bsd2 = th_is_finger_verify_bsd2;
            break;

        case FCHECK_FLOW_ENROLL_REMOVE:
            th->not_finger.qty = th_not_finger_qty - 10;
            th->not_finger.percentage = 20;
            th->not_finger.cover_count = 1;

            th->is_finger.qty = 140;
            th->is_finger.cover_corner_diff = 2;
            th->is_finger.img_level = 5;
            th->is_finger.percentage = 40;
            th->is_finger.bsd2 = th_is_finger_enroll_bsd2;
            break;

        case FCHECK_FLOW_VERIFY_REMOVE:
            th->not_finger.qty = th_not_finger_qty - 10;
            th->not_finger.percentage = 20;
            th->not_finger.cover_count = 1;

            th->is_finger.qty = 235;
            th->is_finger.cover_corner_diff = 3;
            th->is_finger.img_level = 10;
            th->is_finger.percentage = 80;
            th->is_finger.bsd2 = th_is_finger_enroll_bsd2;
            break;

        case FCHECK_FLOW_NAVI:
            th->is_finger.img_level = 0;
            th->is_finger.bsd2 = th_is_finger_verify_bsd2;
            break;

        case FCHECK_FLOW_NAVI_REMOVE:
            th->not_finger.percentage = 20;
            th->not_finger.img_level = 0;
            break;

        case FCHECK_FLOW_CALI_VDM:
            th->is_finger.qty = 20;
            th->is_finger.cover_corner_diff = 2;
            th->is_finger.percentage = 10;
            break;

        case FCHECK_FLOW_CALI_IMG:
            th->is_finger.qty = 1;
            th->is_finger.percentage = 1;
            break;
        default:
            egislog_e("sensor type error, IC=%d, mode = %d", g_egis_sensortype.type, fcheck_flow);
            break;
    }
}

static void _get_ET602_threshold(enum FCHECK_FLOW fcheck_flow, IsFingerDatath* th) {
    int th_not_finger_percentage = 60;  // less than this is partial
    int th_is_finger_percentage = 70;
    int th_not_finger_qty = 100;
    int th_is_finger_qty = 120;
    int th_is_finger_enroll_bsd2 = 300;  // 160;
    int th_is_finger_verify_bsd2 = 430;  // 390;

    switch (fcheck_flow) {
        case FCHECK_FLOW_ENROLL_NO_STABLE:
        case FCHECK_FLOW_ENROLL:
            th->not_finger.percentage = th_not_finger_percentage;

            th->is_finger.qty = th_is_finger_qty;
            th->is_finger.percentage = th_is_finger_percentage;
            th->is_finger.cover_corner_diff = 2;
            th->is_finger.img_level = 0;
            th->is_finger.bsd2 = th_is_finger_enroll_bsd2;
            break;

        case FCHECK_FLOW_VERIFY_NO_STABLE:
        case FCHECK_FLOW_VERIFY:
            th->not_finger.percentage = th_not_finger_percentage;

            th->is_finger.qty = th_is_finger_qty;
            th->is_finger.percentage = th_is_finger_percentage;
            th->is_finger.cover_corner_diff = 2;
            th->is_finger.img_level = 0;
            th->is_finger.bsd2 = th_is_finger_verify_bsd2;
            break;

        case FCHECK_FLOW_ENROLL_REMOVE:
            th->not_finger.qty = th_not_finger_qty - 10;
            th->not_finger.percentage = 20;
            th->not_finger.cover_count = 1;

            th->is_finger.qty = 140;
            th->is_finger.cover_corner_diff = 2;
            th->is_finger.img_level = 0;
            th->is_finger.percentage = 40;
            th->is_finger.bsd2 = th_is_finger_enroll_bsd2;
            break;

        case FCHECK_FLOW_VERIFY_REMOVE:
            th->not_finger.qty = th_not_finger_qty - 10;
            th->not_finger.percentage = 20;
            th->not_finger.cover_count = 1;

            th->is_finger.qty = 235;
            th->is_finger.cover_corner_diff = 3;
            th->is_finger.img_level = 0;
            th->is_finger.percentage = 80;
            th->is_finger.bsd2 = th_is_finger_enroll_bsd2;
            break;

        case FCHECK_FLOW_NAVI:
            th->is_finger.img_level = 0;
            th->is_finger.bsd2 = th_is_finger_verify_bsd2;
            break;

        case FCHECK_FLOW_NAVI_REMOVE:
            th->not_finger.percentage = 20;
            th->not_finger.img_level = 0;
            break;

        case FCHECK_FLOW_CALI_VDM:
            th->is_finger.qty = 20;
            th->is_finger.cover_corner_diff = 2;
            th->is_finger.percentage = 10;
            break;

        case FCHECK_FLOW_CALI_IMG:
            th->is_finger.qty = 1;
            th->is_finger.percentage = 1;
            break;
        default:
            egislog_e("sensor:%d ; parse flow error, unknow flow ,mode = %d",
                      g_egis_sensortype.type, fcheck_flow);
            break;
    }
}

static void _get_ET603_threshold(enum FCHECK_FLOW fcheck_flow, IsFingerDatath* th) {
    int th_not_finger_percentage = 60;  // less than this is partial
    int th_is_finger_percentage = 70;
    int th_not_finger_qty = 100;
    int th_is_finger_qty = 120;
    int th_is_finger_enroll_bsd2 = 300;  // 160;
    int th_is_finger_verify_bsd2 = 430;  // 390;

    switch (fcheck_flow) {
        case FCHECK_FLOW_ENROLL_NO_STABLE:
        case FCHECK_FLOW_ENROLL:
            th->not_finger.percentage = th_not_finger_percentage;

            th->is_finger.qty = th_is_finger_qty;
            th->is_finger.percentage = th_is_finger_percentage;
            th->is_finger.cover_corner_diff = 2;
            th->is_finger.img_level = 0;
            th->is_finger.bsd2 = th_is_finger_enroll_bsd2;
            break;

        case FCHECK_FLOW_VERIFY_NO_STABLE:
        case FCHECK_FLOW_VERIFY:
            th->not_finger.percentage = th_not_finger_percentage;

            th->is_finger.qty = th_is_finger_qty;
            th->is_finger.percentage = th_is_finger_percentage;
            th->is_finger.cover_corner_diff = 2;
            th->is_finger.img_level = 0;
            th->is_finger.bsd2 = th_is_finger_verify_bsd2;
            break;

        case FCHECK_FLOW_ENROLL_REMOVE:
            th->not_finger.qty = th_not_finger_qty - 10;
            th->not_finger.percentage = 20;
            th->not_finger.cover_count = 1;

            th->is_finger.qty = 140;
            th->is_finger.cover_corner_diff = 2;
            th->is_finger.img_level = 0;
            th->is_finger.percentage = 40;
            th->is_finger.bsd2 = th_is_finger_enroll_bsd2;
            break;

        case FCHECK_FLOW_VERIFY_REMOVE:
            th->not_finger.qty = th_not_finger_qty - 10;
            th->not_finger.percentage = 20;
            th->not_finger.cover_count = 1;

            th->is_finger.qty = 235;
            th->is_finger.cover_corner_diff = 3;
            th->is_finger.img_level = 0;
            th->is_finger.percentage = 80;
            th->is_finger.bsd2 = th_is_finger_enroll_bsd2;
            break;

        case FCHECK_FLOW_NAVI:
            th->is_finger.img_level = 0;
            th->is_finger.bsd2 = th_is_finger_verify_bsd2;
            break;

        case FCHECK_FLOW_NAVI_REMOVE:
            th->not_finger.percentage = 20;
            th->not_finger.img_level = 0;
            break;

        case FCHECK_FLOW_CALI_VDM:
            th->is_finger.qty = 20;
            th->is_finger.cover_corner_diff = 2;
            th->is_finger.percentage = 10;
            break;

        case FCHECK_FLOW_CALI_IMG:
            th->is_finger.qty = 1;
            th->is_finger.percentage = 1;
            break;
        default:
            egislog_e("sensor:%d ; parse flow error, unknow flow ,mode = %d",
                      g_egis_sensortype.type, fcheck_flow);
            break;
    }
}

static void _get_ET605_threshold(enum FCHECK_FLOW fcheck_flow, IsFingerDatath* th) {
    int th_not_finger_percentage = 20;  // less than this is partial
    int th_is_finger_percentage = 80;
    int th_not_finger_qty = 80;
    int th_is_finger_qty = 90;
    int th_is_finger_enroll_bsd2 = 180;  // 160;
    int th_is_finger_verify_bsd2 = 400;  // 390;

    switch (fcheck_flow) {
        case FCHECK_FLOW_ENROLL_NO_STABLE:
        case FCHECK_FLOW_ENROLL:
            th->not_finger.percentage = th_not_finger_percentage;

            th->is_finger.qty = th_is_finger_qty;
            th->is_finger.percentage = th_is_finger_percentage;
            th->is_finger.cover_corner_diff = 1;
            th->is_finger.img_level = 1;
            th->is_finger.bsd2 = th_is_finger_enroll_bsd2;
            break;

        case FCHECK_FLOW_VERIFY_NO_STABLE:
        case FCHECK_FLOW_VERIFY:
            th->not_finger.percentage = th_not_finger_percentage;

            th->is_finger.qty = th_is_finger_qty;
            th->is_finger.percentage = th_is_finger_percentage;
            th->is_finger.cover_corner_diff = 2;
            th->is_finger.img_level = 1;
            th->is_finger.bsd2 = th_is_finger_verify_bsd2;
            break;

        case FCHECK_FLOW_ENROLL_REMOVE:
            th->not_finger.qty = th_not_finger_qty - 10;
            th->not_finger.percentage = 20;
            th->not_finger.cover_count = 1;

            th->is_finger.qty = 140;
            th->is_finger.cover_corner_diff = 2;
            th->is_finger.img_level = 1;
            th->is_finger.percentage = 40;
            th->is_finger.bsd2 = th_is_finger_enroll_bsd2;
            break;

        case FCHECK_FLOW_VERIFY_REMOVE:
            th->not_finger.qty = th_not_finger_qty - 10;
            th->not_finger.percentage = 20;
            th->not_finger.cover_count = 1;

            th->is_finger.qty = 235;
            th->is_finger.cover_corner_diff = 3;
            th->is_finger.img_level = 1;
            th->is_finger.percentage = 80;
            th->is_finger.bsd2 = th_is_finger_enroll_bsd2;
            break;

        case FCHECK_FLOW_NAVI:
            th->is_finger.img_level = 0;
            th->is_finger.bsd2 = th_is_finger_verify_bsd2;
            break;

        case FCHECK_FLOW_NAVI_REMOVE:
            th->not_finger.percentage = 20;
            th->not_finger.img_level = 0;
            break;

        case FCHECK_FLOW_CALI_VDM:
            th->is_finger.qty = 20;
            th->is_finger.cover_corner_diff = 2;
            th->is_finger.percentage = 10;
            break;

        case FCHECK_FLOW_CALI_IMG:
            th->is_finger.qty = 1;
            th->is_finger.percentage = 1;
            break;
        default:
            egislog_e("sensor:%d ; parse flow error, unknow flow ,mode = %d",
                      g_egis_sensortype.type, fcheck_flow);
            break;
    }
}

static void get_img_sensortype(int* series, int* sensor_type) {
    if (52 == g_img_height && 103 == g_img_width) {
        *sensor_type = ET510;
        *series = ET5XX_SERIES_ID;
    } else if (69 == g_img_height && 69 == g_img_width) {
        *sensor_type = ET511;
        *series = ET5XX_SERIES_ID;
    } else if (40 == g_img_height && 137 == g_img_width) {
        *sensor_type = ET512;
        *series = ET5XX_SERIES_ID;
    } else if (57 == g_img_height && 70 == g_img_width) {
        *sensor_type = ET516;
        *series = ET5XX_SERIES_ID;
    } else if (46 == g_img_height && 57 == g_img_width) {
        *sensor_type = ET520;
        *series = ET5XX_SERIES_ID;
    } else if (41 == g_img_height && 41 == g_img_width) {
        *sensor_type = ET522;
        *series = ET5XX_SERIES_ID;
    } else if (28 == g_img_height && 120 == g_img_width) {
        *sensor_type = ET523;
        *series = ET5XX_SERIES_ID;
    } else if (74 == g_img_height && 114 == g_img_width) {
        *sensor_type = ET601;
        *series = ET6XX_SERIES_ID;
    } else if (40 == g_img_height && 40 == g_img_width) {
        *sensor_type = ET602;
        *series = ET6XX_SERIES_ID;
    } else if (108 == g_img_width) {
        *sensor_type = ET605;
        *series = ET6XX_SERIES_ID;
    }
}

static void _get_threshold_by_sensor(enum FCHECK_FLOW fcheck_flow, IsFingerDatath* th) {
    int sensor_type = g_egis_sensortype.type;
    int series = g_egis_sensortype.series;

    int enable_cut_image = core_config_get_int(INI_SECTION_SENSOR, KEY_CUT_IMAGE, 0);
    if (enable_cut_image) {
        get_img_sensortype(&series, &sensor_type);
    }
    egislog_d("threshold qty setting (%d - %d)", series, sensor_type);

    if (series == ET5XX_SERIES_ID) {
        switch (sensor_type) {
            case ET510:
            case ET511:
            case ET512:
                _get_ET512_threshold(fcheck_flow, th);
                break;
            case ET516:
                _get_ET516_threshold(fcheck_flow, th);
                break;
            case ET520:
            case ET522:
                _get_ET520_threshold(fcheck_flow, th);
                break;
            case ET523:
                _get_ET523_threshold(fcheck_flow, th);
                break;

            default:
                egislog_e("sensor type error, IC=%d, mode = %d", g_egis_sensortype.type,
                          fcheck_flow);
                break;
        }
    } else if (series == ET6XX_SERIES_ID) {
        switch (sensor_type) {
            case ET601:
                _get_ET601_threshold(fcheck_flow, th);
                break;

            case ET602:
                _get_ET602_threshold(fcheck_flow, th);
                break;

            case ET613:
                _get_ET603_threshold(fcheck_flow, th);
                break;

            case ET605:
                _get_ET605_threshold(fcheck_flow, th);
                break;

            default:
                egislog_e("sensor type error, IC=%d, mode = %d", g_egis_sensortype.type,
                          fcheck_flow);
                break;
        }
    } else {
        egislog_e("unknow sensor : %d-%d", g_egis_sensortype.series, g_egis_sensortype.type);
    }
}

static BOOL _IsFingerprint(enum FCHECK_FLOW fcheck_flow) {
    int img_intensity = 0, img_bsd = 0;
    int ret = FCHECK_NON_FINGER;
    IsFingerDatath th;
    memset(&th, 0, sizeof(IsFingerDatath));
    egislog_d("IsFingerprint enter, mode = %d", fcheck_flow);
    _get_threshold_by_sensor(fcheck_flow, &th);
    switch (fcheck_flow) {
        case FCHECK_FLOW_ENROLL_NO_STABLE:
        case FCHECK_FLOW_VERIFY_NO_STABLE:
        case FCHECK_FLOW_VERIFY:
        case FCHECK_FLOW_ENROLL:
            if (g_percentage <= th.not_finger.percentage)
                ret = FCHECK_NON_FINGER;  // FINGER_RESULT_TOO_PARTIAL;
            else if ((g_img_qty >= th.is_finger.qty) && (g_percentage > th.is_finger.percentage) &&
                     (g_cover_count - g_corner_count >= th.is_finger.cover_corner_diff) &&
                     (g_img_level >= th.is_finger.img_level) && (g_bsd2 < th.is_finger.bsd2))
                ret = FCHECK_IS_FINGER;
            else
                ret = FCHECK_NON_FINGER;
            break;
        case FCHECK_FLOW_ENROLL_REMOVE:
            if ((g_img_qty <= th.not_finger.qty) || (g_cover_count <= th.not_finger.cover_count) ||
                (g_percentage <= th.not_finger.percentage))
                ret = FCHECK_NON_FINGER;
            else if ((g_img_qty >= th.is_finger.qty) &&
                     (g_cover_count - g_corner_count >= th.is_finger.cover_corner_diff) &&
                     (g_img_level > th.is_finger.img_level) &&
                     (g_percentage > th.is_finger.percentage) && (g_bsd2 < th.is_finger.bsd2))
                ret = FCHECK_IS_FINGER;
            else
                ret = FCHECK_IS_NOISE;  // noise;
            break;
        case FCHECK_FLOW_VERIFY_REMOVE:
            if ((g_img_qty <= th.not_finger.qty) || (g_cover_count <= th.not_finger.cover_count) ||
                (g_percentage <= th.not_finger.percentage))
                ret = FCHECK_NON_FINGER;
            else if ((g_img_qty >= th.is_finger.qty) &&
                     (g_cover_count - g_corner_count >= th.is_finger.cover_corner_diff) &&
                     (g_img_level > th.is_finger.img_level) &&
                     (g_percentage > th.is_finger.percentage) && (g_bsd2 < th.is_finger.bsd2))
                ret = FCHECK_IS_FINGER;
            else
                ret = FCHECK_IS_FINGER;  // noise;
            break;
        case FCHECK_FLOW_NAVI:  // Todo
            break;
        case FCHECK_FLOW_NAVI_REMOVE:  // Todo
            break;
        case FCHECK_FLOW_CALI_VDM:  // Todo
            if ((g_img_qty > th.is_finger.qty) ||
                (g_cover_count - g_corner_count >= th.is_finger.cover_corner_diff) ||
                (g_percentage > th.is_finger.percentage))
                ret = FCHECK_IS_FINGER;
            else
                ret = FCHECK_NON_FINGER;
            break;
        case FCHECK_FLOW_CALI_IMG:  // Todo
            if ((g_img_qty > th.is_finger.qty) || (g_percentage > th.is_finger.percentage))
                ret = FCHECK_IS_FINGER;
            else
                ret = FCHECK_NON_FINGER;
            break;
        default:
            ret = FCHECK_FAIL;
            break;
    }
    egislog_d("IsFingerprint leave , ret = %d", ret);
    return ret;
}

BOOL fcheck_is_finger() {
    CHECK_SENSSION
    BOOL is_finger = FALSE;
    _analyze_img();
    is_finger = _IsFingerprint(g_fcheck_flow);

    if (is_finger) {
        _update_best_img_info();
        if (_is_partial())
            g_finger_on_status = FINGER_QUALITY_PARTIAL;
        else
            g_finger_on_status = FINGER_QUALITY_GOOD;
    } else
        g_finger_on_status = FINGER_QUALITY_BAD;

    egislog_d("fcheck_is_finger, is_finger = %d", is_finger);
    return is_finger;
}

BOOL fcheck_is_finger_checkremove() {
    CHECK_SENSSION
    BOOL is_finger = FALSE;

    _analyze_img();
    is_finger = _IsFingerprint(g_fcheck_flow);

    return is_finger;
}
BOOL fcheck_is_similar() {
#ifdef USE_QM_CHECK_FINGER_IS_NOT_MOVING
    if (g_last_qm_match_score >= FD_STABLE_MATCH_THRESHOLD) {
        egislog_v("@egis-IsFP: too similar: %d, threshold:%d", g_last_qm_match_score,
                  FD_STABLE_MATCH_THRESHOLD);
        return TRUE;
    } else
        return FALSE;
#endif
    return FALSE;
}
BOOL fcheck_is_good_to_keep() {
    CHECK_SENSSION

    _analyze_img();
    if (g_finger_on_status != FINGER_QUALITY_GOOD && !_is_partial()) {
        g_finger_on_status = FINGER_QUALITY_GOOD;
    }

    if ((g_img_qty > FINGER_DETECT_THRESHOLD) && (g_cover_count - g_corner_count >= 2)) {
        if ((g_img_qty >= g_bestImgInfo.qty) && (g_cover_count >= g_bestImgInfo.cover_count)) {
            egislog_v("@egis-IsFP[good_to_keep]");
            _update_best_img_info();
            return TRUE;
        } else
            egislog_v("@egis-IsFP: not better");
    }
    return FALSE;
}

int fcheck_is_not_moving() {
    CHECK_SENSSION

#ifdef USE_QM_CHECK_FINGER_IS_NOT_MOVING
    int retval = FINGER_UNSTABLE;
    g_last_qm_match_score = 0;
    QMMatchDetail md;
    QMFeatures feat;

    if (g_raw == NULL) return FINGER_ERROR;

    memcpy(g_raw, g_img, g_img_width * g_img_height);
    qm_extract(g_raw, g_img_width, g_img_height, &feat, NULL);
    g_last_qm_match_score = qm_match(&g_base_feat, &feat, &md, NULL);
    egislog_d("qm_match score [%d] (%d,%d)", g_last_qm_match_score, md.dx, md.dy);
    if (g_last_qm_match_score > FD_STABLE_MATCH_THRESHOLD && md.dx == 0 && md.dy == 0) {
        egislog_d("==FINGER_STABLE== [%d]", g_last_qm_match_score);
        retval = FINGER_STABLE;
    } else
        qm_copy_features(&g_base_feat, &feat);
    return retval;
#else
    _analyze_img();
    if (g_qty_diff <= 8) {
        return FINGER_STABLE;
    }
    return FINGER_UNSTABLE;
#endif
}

int fcheck_get_finger_is_malisious(int32_t* is_malisious) {
    *is_malisious = 0;
    return TRUE;
}

#endif
