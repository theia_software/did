#include <egis_algomodule.h>
#include <stdint.h>

#include "core_config.h"
#include "egis_definition.h"
#include "fd_process.h"
#include "fimage_check.h"
#include "ini_definition.h"
#include "plat_log.h"

#define LOG_TAG "RBS-EGISFP"

static unsigned char* g_fimage = NULL;
static enum FCHECK_FLOW g_fcheck_flow;
static int g_img_width = 0;
static int g_img_height = 0;
static FINGER_QUALITY g_finger_on_status;

static int g_fimage_qty = 0;
static int g_fimage_percentage = 0;
static int g_fimage_is_light = 0;
static int g_fimage_G2_partial_qty = 0;
static BOOL g_fimage_is_fake = 0;
static int g_fimage_qty_threshold = -1;
static int g_fimage_has_residual = 0;

// config start
//

// TODO: Default value sync
static int enable_light_detect = INID_LIGHT_DETECTION;
static int enable_fake_detect = INID_FAKE_FINGER_DETECTION;  // FAKE_FINGER_DETECTION
static int enroll_partial_threshold = 80;
static int verify_partial_threshold = 70;
static int bad_partial_threshold = 30;
static int g_try_match_even_bad;

BOOL _fcheck_is_light() {
    return enable_light_detect ? g_fimage_is_light : FALSE;
}

BOOL _fcheck_is_partial() {
    int has_black_edge = -1;
#ifdef EGIS_IMAGE_V2
    has_black_edge = EGIS_IMAGE_OBTAIN_PARAM(black_edge);
    egislog_d("%s has_black_edge %d", __func__, has_black_edge);
#endif
    int threshold = egis_fp_is_enroll() ? enroll_partial_threshold : verify_partial_threshold;
    return (g_fimage_percentage < threshold || has_black_edge > 0) ? TRUE : FALSE;
}

BOOL _fcheck_is_fake() {
    if (!enable_fake_detect) return FALSE;
    if (_fcheck_is_light()) return FALSE;
    if (_fcheck_is_partial()) {
        return FALSE;  // Jenson Logic  ??
    }
    return g_fimage_is_fake;
}

BOOL fcheck_open_session(int fcheck_flow, int width, int height) {
    egislog_d("%s for optical", __func__);

    g_fcheck_flow = fcheck_flow;

    enable_light_detect =
        core_config_get_int(INI_SECTION_VERIFY, KEY_LIGHT_DETECTION, enable_light_detect);
    enable_fake_detect =
        core_config_get_int(INI_SECTION_VERIFY, KEY_FAKE_FINGER_DETECTION, enable_fake_detect);
    g_fimage_qty_threshold =
        core_config_get_int(INI_SECTION_VERIFY, KEY_IMAGE_QTY_THRESHOLD, g_fimage_qty_threshold);
    enroll_partial_threshold = core_config_get_int(INI_SECTION_ENROLL, KEY_ENROLL_PARTIAL_THRESHOLD,
                                                   INID_ENROLL_PARTIAL_THRESHOLD);
    verify_partial_threshold = core_config_get_int(INI_SECTION_VERIFY, KEY_VERIFY_PARTIAL_THRESHOLD,
                                                   INID_VERIFY_PARTIAL_THRESHOLD);
    bad_partial_threshold = core_config_get_int(INI_SECTION_SENSOR, KEY_BAD_PARTIAL_THRESHOLD,
                                                INID_BAD_PARTIAL_THRESHOLD);
    g_try_match_even_bad =
        core_config_get_int(INI_SECTION_VERIFY, KEY_TRY_MATCH_EVEN_TOO_PARTIAL, INID_TRY_MATCH_EVEN_TOO_PARTIAL);
    g_img_width = width;
    g_img_height = height;
    g_fimage_is_fake = 0;
    return TRUE;
}

void fcheck_close_session() {}

void fcheck_update_image(uint8_t* img) {
    g_fimage = img;
    fd_process_get_int(FD_PARAM_INT_QUALITY, &g_fimage_qty);
    fd_process_get_int(FD_PARAM_INT_PARTIAL_PERCENTAGE, &g_fimage_percentage);
    fd_process_get_int(FD_PARAM_INT_IMAGE_IS_LIGHT, &g_fimage_is_light);
    fd_process_get_int(FD_PARAM_INT_G2_PARTIAL_QTY, &g_fimage_G2_partial_qty);
    fd_process_get_int(FD_PARAM_INT_HAS_RESIDUAL, &g_fimage_has_residual);

    int is_fake;
    fd_process_get_int(FD_PARAM_INT_IS_FAKE_FINGER, &is_fake);
    /**
     * @brief keep g_fimage_is_fake if one frame was detected as fake.
     */
    egislog_d("%s , fimage_qty=%d, is_fake=%d, residual=%d", __func__, g_fimage_qty, is_fake,
              g_fimage_has_residual);
    g_fimage_is_fake = g_fimage_is_fake || is_fake;
}

void fcheck_get_finger_status(FINGER_QUALITY* finger_on_status) {
    switch (g_fcheck_flow) {
        case FCHECK_FLOW_ENROLL:
        case FCHECK_FLOW_ENROLL_NO_STABLE:
            if (g_finger_on_status == FINGER_QUALITY_GOOD && _fcheck_is_partial()) {
                egislog_d("%s , enroll partial", __func__);
                *finger_on_status = FINGER_QUALITY_PARTIAL;
                return;
            }
            break;
        default:
            break;
    }
    *finger_on_status = g_finger_on_status;
}

void fcheck_get_best_img_info(fd_img_analysis_info_t* bestImgInfo) {
    egislog_d("%s entry, return directly", __func__);
    return;
}

BOOL fcheck_is_finger() {
    int retval;
    int is_partial = _fcheck_is_partial();

    if (g_fimage_G2_partial_qty < 0) {
        g_finger_on_status = FINGER_QUALITY_BAD;
        retval = FCHECK_NON_FINGER;
    } else if (g_fimage_qty >= g_fimage_qty_threshold &&
               g_fimage_percentage > bad_partial_threshold) {
        if (g_fimage_has_residual) {
            g_finger_on_status = FINGER_QUALITY_RESIDUAL;
        } else {
            g_finger_on_status = is_partial ? FINGER_QUALITY_PARTIAL : FINGER_QUALITY_GOOD;
        }
        retval = FCHECK_IS_FINGER;
    } else {
        g_finger_on_status = FINGER_QUALITY_BAD;
        retval = (g_try_match_even_bad > 0) ? FCHECK_IS_FINGER : FCHECK_NON_FINGER;
    }

    egislog_d("%s ,g_fimage_has_residual = %d", __func__, g_fimage_has_residual);
    egislog_d("%s ,isLight = %d", __func__, enable_light_detect ? g_fimage_is_light : FALSE);
    egislog_d(
        "%s, isPartial = %d, g_fimage_G2_partial_qty = %d e partial thres =%d v partial thres %d "
        "bad_partial_thres = %d g_finger_on_status = %d ",
        __func__, is_partial, g_fimage_G2_partial_qty, enroll_partial_threshold,
        verify_partial_threshold, bad_partial_threshold, g_finger_on_status);

    egislog_d("%s, isFinger=%d, thres %d, %d", __func__, retval, g_fimage_qty_threshold,
              bad_partial_threshold);
    return retval;
}

BOOL fcheck_is_finger_checkremove() {
    return FCHECK_NON_FINGER;
}

BOOL fcheck_is_good_to_keep() {
    return TRUE;
}

int fcheck_is_not_moving() {
    return TRUE;
}

BOOL fcheck_is_similar() {
    return TRUE;
}

int fcheck_get_finger_is_malisious(int32_t* is_malisious) {
    *is_malisious = _fcheck_is_fake();
    return TRUE;
}
