
#ifdef __ET6XX__

#include "ImageProcessingLib.h"
#include "egis_definition.h"
#include "isensor_api.h"
#include "plat_log.h"
#include "plat_mem.h"

#define LOG_TAG "RBS-EGISFP"

void copy_crop_image_16(int* src, int width, int height, int* dst, int crop_w, int crop_h) {
    int i, cx = (width - crop_w) / 2, cy = (height - crop_h) / 2;
    src += (cy * width + cx);
    for (i = 0; i < crop_h; i++, dst += crop_w, src += width) {
        memcpy(dst, src, crop_w * sizeof(int));
    }
}

int minus_background(unsigned short* frame, int image_size) {
    int min = 65535;

    unsigned short* et6xx_background;
    int out_buffer_size = sizeof(unsigned short**);
    isensor_get_buffer(PARAM_BUF_BKG_IMAGE_POINTER, (unsigned char*)&et6xx_background,
                       &out_buffer_size);

    for (int i = 0; i < image_size; i++) {
        *frame = *frame - *et6xx_background;
        frame++;
        et6xx_background++;
    }
    return 0;
}

void img16_normalize(int* src, int width, int height, unsigned char* dst) {
    int i_row, i, j, iFW, min = 65535, max = 0, diff, ratio_2e10;
    int lineCount = 0;
    int *line1 = (int*)plat_alloc(width * sizeof(int)),
        *line2 = (int*)plat_alloc(width * sizeof(int)),
        *line3 = (int*)plat_alloc(width * sizeof(int));
    int* sumLine = (int*)plat_alloc(width * sizeof(int));

    for (i_row = 0, i = -1, iFW = -3; i < height; i_row++, i++, iFW++) {
        int* lineT = line1;
        if (iFW >= 0) {
            for (j = 0; j < width; j++) sumLine[j] -= lineT[j];
            lineCount--;
        }
        line1 = line2;
        line2 = line3;
        line3 = lineT;
        if (i_row < height) {
            memcpy(line3, src + i_row * width, width * sizeof(int));
            for (j = 0; j < width; j++) sumLine[j] += line3[j];
            lineCount++;
        }
        if (i >= 0) {
            int jW, jFW;
            int sum = 0;
            int pixelCount = 0;
            // BYTE *img_row = dst[i];
            int* img_row = src + i * width;
            for (jW = 0, j = -1, jFW = -3; j < width; jW++, j++, jFW++) {
                if (jFW >= 0) {
                    sum -= sumLine[jFW];
                    pixelCount -= lineCount;
                }
                if (jW < width) {
                    sum += sumLine[jW];
                    pixelCount += lineCount;
                }
                if (j >= 0) {
                    if (pixelCount == 9) {
                        img_row[j] = (sum - img_row[j] + img_row[j] * 8 + 8) / 16;
                    } else
                        img_row[j] = sum / pixelCount;
                    if (img_row[j] < 65535 && img_row[j] > max) max = img_row[j];
                    if (img_row[j] > 0 && img_row[j] < min) min = img_row[j];
                }
            }
        }
    }
    diff = max - min;
    if (diff > 100) {
        ratio_2e10 = 255 * 1024 / diff;
        for (i = 0; i < width * height; i++) {
            dst[i] = (src[i] < min ? 0 : ((src[i] - min) * ratio_2e10 + 512) >> 10);
        }
    } else {
        memset(dst, 0, width * height);
    }
    plat_free(line1);
    plat_free(line2);
    plat_free(line3);
    plat_free(sumLine);
}

#define _Max(a, b) ((b) > (a) ? (b) : (a))
#define _Min(a, b) ((b) < (a) ? (b) : (a))
void raw16bitsTo8bits(unsigned short* raw_16bits, unsigned char* raw_8bits, INT width, INT height,
                      int scale_down) {
    int min = 256 * 256;
    int max = 0;
    int ratio;
    unsigned short value;

    const int number_ignore_line = 3;
    for (int i = number_ignore_line; i < height - number_ignore_line; i++) {
        for (int j = number_ignore_line; j < width - number_ignore_line; j++) {
            value = raw_16bits[i * width + j];
            if (min > value) min = value;
            if (max < value) max = value;
        }
    }

    if (max == min) {
        ratio = 1;
    } else {
        ratio = (max - min) * scale_down;
    }

    for (int i = 0; i < width * height; i++) {
        raw_8bits[i] = _Min(255, _Max(0, 256 * (raw_16bits[i] - min) / ratio));
    }
}

void raw16bitsTo32(unsigned short* u16_raw, int* i32_raw, int width, int height) {
    int i;
    int image_size = width * height;
    unsigned short* p_u16 = u16_raw;
    int* p_i32 = i32_raw;
    for (i = 0; i < image_size; i++) {
        *p_i32 = *p_u16;
        p_u16++;
        p_i32++;
    }
}

void Remove_T_dummy(unsigned short* src_frame, int width, int height, unsigned short* out_frame) {
    // Remove T dummy pixel and transfer Col-major to Row-major
    int i, j, middle = width / 2;
    unsigned short* data;
    int count = 0;
    for (i = 1; i < height; i++) {
        data = src_frame + i * width;
        for (j = 0; j < width; j++) {
            if (j == middle) {
                data++;
            } else {
                *out_frame++ = *data++;
                count++;
            }
        }
    }
    egislog_d("%s count=%d", __func__, count);
    return;
}

#endif