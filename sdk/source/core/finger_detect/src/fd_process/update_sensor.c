#include "algomodule.h"
#include "bkimg_pool.h"
#include "core_config.h"
#include "egis_definition.h"
#include "ini_definition.h"
#include "isensor_api.h"
#include "object_def_image.h"
#include "plat_log.h"
#ifndef RBS_EVTOOL
#include "bkg_manager.h"
#endif

static int _check_BDS_add(BOOL has_residual, BOOL is_partial) {
    int to_add = 1;
    int fake_score = EGIS_IMAGE_OBTAIN_PARAM(fake_score);
    int quality = EGIS_IMAGE_OBTAIN_PARAM(qty);
    int has_black_edge = -1;
#ifdef G3PLUS_MATCHER
    has_black_edge = EGIS_IMAGE_OBTAIN_PARAM(black_edge);
#endif
    if (has_residual || has_black_edge > 0) {
        to_add = 0;
    } else if (quality <= 0 || is_partial) {
        to_add = 0;
    } else if (fake_score > 0) {
        int enable_fake_detect = core_config_get_int(INI_SECTION_VERIFY, KEY_FAKE_FINGER_DETECTION,
                                                     INID_FAKE_FINGER_DETECTION);
        if (enable_fake_detect) {
            to_add = 0;
        }
    }
    return to_add;
}

static BOOL g_enable_update_bds = TRUE;
void update_sensor_BDS_in_fd_acquire(uint16_t* img16_ptr, uint16_t* img_detect, int size,
                                     int try_index, BOOL is_partial, BOOL has_residual) {
    //TODO
    ex_log(LOG_DEBUG, "debug_check update_sensor_BDS_in_fd_acquire");
    int ret = bkm_update_bds(img16_ptr, img_detect);
    return;
#ifdef RBS_EVTOOL
    return;
#else
    if (try_index == 0) g_enable_update_bds = TRUE;

    int to_add = _check_BDS_add(has_residual, is_partial);
    if (to_add) {
        if (g_enable_update_bds) {
            int ret = bkm_update_bds(img16_ptr, img_detect);
            if (ret == 0) {
                int slot_id = bkimg_pool_add_get_slot_id();
                int pol = slot_id * 100 + 11;
                ex_log(LOG_DEBUG, "[%d] set bds_pool_add %d", try_index, pol);
                g_enable_update_bds = FALSE;
                EGIS_IMAGE_KEEP_PARAM(bds_pool_add, pol);
            } else {
                ex_log(LOG_DEBUG, "[%d] set bds_pool_add 10", try_index);
                EGIS_IMAGE_KEEP_PARAM(bds_pool_add, 10);
            }
        } else {
            EGIS_IMAGE_KEEP_PARAM(bds_pool_add, 20);
        }
    } else {
        ex_log(LOG_DEBUG, "[%d] set bds_pool_add 00", try_index);
        EGIS_IMAGE_KEEP_PARAM(bds_pool_add, 0);
    }
#endif
}
