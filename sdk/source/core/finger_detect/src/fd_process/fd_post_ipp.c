#include "fd_post_ipp.h"
#include "egis_definition.h"
#include "plat_log.h"

/**
 * @brief
 *
 * @param raw_img [in] Raw image
 * @param bin_img [in/out] Bin image (after IPP)
 * @param width [in] Image width
 * @param height [in] Image height
 * @param threshold [in]
 * @param over_percentage [out] 0~100
 *
 * @return EGIS_OK: Success
 *         EGIS_CANCEL: Skip
 */
int fd_pipp_overflow_process(uint16_t* raw_img, uint8_t* bin_img, int width, int height,
                             uint16_t threshold, uint8_t new_value, int* over_percentange) {
    RBS_CHECK_IF_NULL(raw_img, EGIS_INCORRECT_PARAMETER);
    RBS_CHECK_IF_NULL(bin_img, EGIS_INCORRECT_PARAMETER);
    int i;
    uint16_t* p_raw = raw_img;
    uint8_t* p_bin = bin_img;
    int size = width * height;
    uint32_t over_count = 0;
    if (size <= 0) {
        return EGIS_COMMAND_FAIL;
    }

    for (i = 0; i < size; i++) {
        if (*p_raw >= threshold) {
            over_count++;
            *p_bin = new_value;
        }
        p_raw++;
        p_bin++;
    }
    if (over_percentange != NULL) {
        *over_percentange = over_count * 100 / size;
    }
    return EGIS_OK;
}