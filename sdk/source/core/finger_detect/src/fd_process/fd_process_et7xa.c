#include <plat_file.h>
#include "EgisAlgorithmAPI.h"
#include "bkg_manager.h"
#include "core_config.h"
#include "egis_algomodule.h"
#include "egis_definition.h"
#include "fd_post_ipp.h"
#include "fd_process.h"
#include "image_cut.h"
#include "ini_definition.h"
#include "isensor_api.h"
#include "matcher_feature.h"
#include "plat_log.h"
#include "plat_mem.h"
#include "plat_time.h"
#include "sensor_config.h"
#include "uk_manager.h"
#include "flat_field_correction.h"
#include "IPimage_760_IPP6_61b.h"
#ifdef G3_MATCHER
#include "ImageProcessingLib.h"
#else
#include "work_with_G2/ImageProcessingLib.h"
#include "work_with_G2/ET760_Collimator_ISP.h"
#if defined(LA_BDS)
#include "work_with_G2/la_bds.h"
//#include "unistd.h"
//#include "fcntl.h"
#endif
#endif
#include "algomodule.h"
#include "fp_err.h"
#include "update_sensor.h"
#ifdef G3PLUS_MATCHER
#include "detect_scratch.h"
#endif
#ifdef ALGO_GEN_7
#include "work_with_G7/GoImageProcessingLib.h"
#endif
#include "InlineLib.h"
#include "partial_cell.h"
#include "partial_hist.h"

#ifdef G3PLUS_MATCHER
#define COMPRESS_BUFFER
#endif
#ifdef COMPRESS_BUFFER
#include "image_cut.h"
#endif

#include "bkg_manager.h"

#define LOG_TAG "RBS-EGISFP"

#ifndef ALGO_GEN_4
extern int WGT_FAKE_DETECT[];
extern int WGT_EDGE_DETECT[];
#endif

#define FLOAT_TO_INT(x) ((int)(x + 0.5f))

int go_IPavergae_intensity(int *img16, int size) {
	int i, sum = 0;
	for (i = 0; i < size; i++) sum += img16[i];
	return sum;
}
void go_IPoffset(int *img16, int size, int diff) {
	int i;
	for (i = 0; i < size; i++) {
		img16[i] += diff;
		if (img16[i] < 0) img16[i] = 0;
		else if (img16[i] > 0xFFFF) img16[i] = 0xFFFF;
	}	
}
void go_IPsubtract(int *img16, int *from, int size) {
	int i, min = 0xFFFF, max = -0xFFFF;
	if (size <= 0) return;
	for (i = 0; i < size; i++) {
		img16[i] -= from[i];		
		if (img16[i] > max) max = img16[i];
		if (img16[i] < min) min = img16[i];
	}
	if (max - min <= 0xFFFF) { //no overflow
		if (min < 0) {
			for (i = 0; i < size; i++) img16[i] -= min;
		}
	}
	else {
		int avg = go_IPavergae_intensity(img16, size) / size;
		go_IPoffset(img16, size, 0xFFFF/2 - avg); //move average to 0xFFFF/2
	}
}
void SC_img16_to_img8(int *img16, unsigned char *dst_img8, int img_size){
	int i, v_max = -65535, v_min = 999999;
	for (i = 0; i < img_size; i++){
		if (img16[i] > v_max) v_max = img16[i];
		if (img16[i] < v_min) v_min = img16[i];
	}
	for (i = 0; i < img_size; i++){
		dst_img8[i] = (unsigned char)(255 * (img16[i] - v_min) / (v_max - v_min));
	}
}
extern fn_fetch_image_16bit g_fn_fetch_image_16bit;
extern int g_quality;
extern int g_partial_percentage;
extern int g_g2_partial_qty;
extern int g_is_fake_finger;
extern int g_is_light;
extern int g_has_residual;

extern uint16_t* g_raw_img_buf;
extern int g_raw_bpp;
extern enum fd_process_option g_fd_process_option;

#ifdef ALGO_GEN_4
static int ip_finger_detect(uint16_t* raw_img, uint16_t* bkg, uint8_t* bin_img,
                            uint16_t** p_img_detect, int try_match_indx, int width, int height,
                            int* is_fake_finger, int* ip_partial_qty, int* has_light,
                            int* black_edge_score) {
    *is_fake_finger = 0;
    EGIS_IMAGE_KEEP_PARAM(fake_score, -100);
    *has_light = 0;
    EGIS_IMAGE_KEEP_PARAM(is_light, *has_light);
    *ip_partial_qty = 100;
    EGIS_IMAGE_KEEP_PARAM(partial, *ip_partial_qty);
    *black_edge_score = -100;
    EGIS_IMAGE_KEEP_PARAM(black_edge, *black_edge_score);
    return EGIS_OK;
}
#else
static int ip_finger_detect(uint16_t* raw_img, uint16_t* bkg, uint8_t* bin_img,
                            uint16_t** p_img_detect, int try_match_indx, int width, int height,
                            int* is_fake_finger, int* ip_partial_qty, int* has_light,
                            int* black_edge_score) {
    uint8_t dev_id1 = g_egis_sensortype.dev_id1;
    int fake_score = 0;
    unsigned short* ipp_img = ipp_71x_stage_preprocessing(raw_img, bkg, width, height);
    *p_img_detect = ipp_img;
    RBS_CHECK_IF_NULL(ipp_img, EGIS_COMMAND_FAIL);
    *black_edge_score = 0;
    *ip_partial_qty = 100;
    switch (dev_id1) {
        default:
            //egislog_e("Skip ML detect (sensor dev_id1 %d)", dev_id1);
            fake_score = -1;
            *black_edge_score = -1;
            break;
        case 13: {
            TIME_MEASURE_START(finger_detect2);
            fake_score = IPfake_detect(ipp_img, width, height, WGT_FAKE_DETECT, IPP_ET713);
            TIME_MEASURE_STOP(finger_detect2, "fake_detect");
            int bkg_cx = 0, bkg_cy = 0;
            isensor_get_int(PARAM_INT_CALI_BKG_CX, &bkg_cx);
            isensor_get_int(PARAM_INT_CALI_BKG_CY, &bkg_cy);
            *black_edge_score = IPblack_edge_detect(ipp_img, width, height, WGT_EDGE_DETECT, bkg_cx,
                                                    bkg_cy, IPP_ET713);
            break;
        }
    }
    switch (dev_id1) {
        // case 13:
        case 2: {
            struct PartialDetectorResult partialDetectorResult;
            int bkg_cx = 0, bkg_cy = 0;
            isensor_get_int(PARAM_INT_CALI_BKG_CX, &bkg_cx);
            isensor_get_int(PARAM_INT_CALI_BKG_CY, &bkg_cy);
            unsigned char* mask_info =
                detect_partial_cell_702(raw_img, bkg, bin_img, width, height, bkg_cx, bkg_cy, 200,
                                        0, 12, 0, &partialDetectorResult);
            plat_free(mask_info);
            *ip_partial_qty = partialDetectorResult.partial_ratio;
            // print_PartialDetectorResult(&partialDetectorResult);
            break;
        }
    }
    if (fake_score > 0) {
        *is_fake_finger = TRUE;
    } else {
        *is_fake_finger = FALSE;
    }
    egislog_d("fake_score = %d", fake_score);
    EGIS_IMAGE_KEEP_PARAM(fake_score, fake_score);
    egislog_d("partial = %d", *ip_partial_qty);
    EGIS_IMAGE_KEEP_PARAM(partial, *ip_partial_qty);
    egislog_d("is_light = %d", *has_light);
    EGIS_IMAGE_KEEP_PARAM(is_light, *has_light);
    egislog_d("black_edge_score %d", *black_edge_score);
    EGIS_IMAGE_KEEP_PARAM(black_edge, *black_edge_score);

#ifdef G3PLUS_MATCHER
    if (try_match_indx == 0 && core_config_get_int(INI_SECTION_VERIFY, KEY_DETECT_SCRATCH_MASK,
                                                   INID_DETECT_SCRATCH_MASK)) {
        int integrate_hw_count = 0;
        isensor_get_int(PARAM_INT_HW_INTEGRATE_COUNT, &integrate_hw_count);

        // int use_scratch_mask = core_config_get_int(INI_SECTION_VERIFY, KEY_DETECT_SCRATCH_MASK,
        // INID_DETECT_SCRATCH_MASK);
        detect_scratch_update(ipp_img, width, height, integrate_hw_count);
    }
#endif
    return EGIS_OK;
}
#endif
static int _get_image_8bit_from_db(uint8_t* out_image, int out_width, int out_height) {
    int ret;

    egislog_d("%s, %d:%d", __func__, out_width, out_height);
    ret = isensor_get_dynamic_frame(out_image, out_width, out_height, 1);
    if (ret == EGIS_OK) {
        g_quality = 200;
        g_partial_percentage = 100;
        g_is_fake_finger = 0;
    }
    return ret;
}

extern BOOL g_stop_DQE;
extern BOOL g_enroll_flag;
BOOL g_this_image_use_quick_capture = FALSE;
static void _quick_capture_before_get_frame(int try_match_count, int temperature, int* hw_count,
                                            int* exp_time_x10) {
    BOOL quick_capture_count =
        core_config_get_int(INI_SECTION_SENSOR, KEY_QUICK_CAPTURE_COUNT, INID_QUICK_CAPTURE_COUNT);
    g_this_image_use_quick_capture = FALSE;

    if (quick_capture_count <= 0) {
        return;
    }
    BOOL enroll_also = FALSE;
    if (!egis_fp_is_enroll()) {
        enroll_also = TRUE;
    } else if (!g_stop_DQE && !ukm_is_enabled()) {
        enroll_also = core_config_get_int(INI_SECTION_ENROLL, KEY_ENROLL_EXTRA_THE_FIRST_ENABLE,
                                          INID_ENROLL_EXTRA_THE_FIRST_ENABLE);
    }
    // g_enroll_config.enroll_extra_1st_before_progress = core_config_get_int(INI_SECTION_ENROLL,
    // KEY_ENROLL_EXTRA_THE_FIRST_BEFORE_PROGRESS, 40);
    if (quick_capture_count - try_match_count > 0 && temperature > 10 && enroll_also) {
        int hw_int = core_config_get_int(INI_SECTION_SENSOR, KEY_QUICK_CAPTURE_HW_INT, 0);
        if (hw_int > 0) {
            egislog_d("quick capture hw_int = %d. t=%d", hw_int, temperature);
            *hw_count = hw_int;
            isensor_set_int(PARAM_INT_RUN_QUICK_CAPTURE_HW_INT, *hw_count);
            g_this_image_use_quick_capture = TRUE;
            return;
        }

        float hw_exp_time =
            core_config_get_double(INI_SECTION_SENSOR, KEY_QUICK_CAPTURE_EXP_TIME, 0);
        if (hw_exp_time > 0) {
            egislog_d("quick capture exp_time = %d. t=%d", hw_exp_time, temperature);
            *exp_time_x10 = hw_exp_time * 10;
            isensor_set_int(PARAM_INT_RUN_QUICK_CAPTURE_EXP_TIME_X10, *exp_time_x10);
            g_this_image_use_quick_capture = TRUE;
            return;
        }
    }
}

static void _quick_capture_after_get_frame(int local_hw_integrate_count, int integrate_hw_count,
                                           int local_exp_time_x10, int exp_time_x10,
                                           uint16_t* raw_img, int frame_sz) {
    if (local_hw_integrate_count != integrate_hw_count) {
        isensor_set_int(PARAM_INT_RUN_QUICK_CAPTURE_HW_INT, 0);
        int i;
        uint16_t* p_raw = raw_img;
        for (i = 0; i < frame_sz; i++) {
            *p_raw = *p_raw * integrate_hw_count / local_hw_integrate_count;
            p_raw++;
        }
        return;
    }

    if (local_exp_time_x10 != exp_time_x10) {
        isensor_set_int(PARAM_INT_RUN_QUICK_CAPTURE_EXP_TIME_X10, 0);
        int i;
        uint16_t* p_raw = raw_img;
        for (i = 0; i < frame_sz; i++) {
            *p_raw = *p_raw * exp_time_x10 / local_exp_time_x10;
            p_raw++;
        }
    }
}

static int _fd_get_raw(uint16_t* raw_img, int width, int height, int try_match_count,
                       int integrate_hw_count, int exp_time_x10, int temperature, BOOL from_inline) {
    int ret;
    const int integrate_count = 1;
    int local_hw_integrate_count = integrate_hw_count;
    int local_exp_time_x10 = exp_time_x10;
    int all_adc_dac;
    isensor_get_int(PARAM_INT_ALL_ADC_DAC, &all_adc_dac);

    int is_et0xx = 0;
    isensor_get_int(PARAM_INT_GET_IS_ET0XX, &is_et0xx);
    if (is_et0xx) {
        egislog_v("No quick capture flow");
    } else {
        _quick_capture_before_get_frame(try_match_count, temperature, &local_hw_integrate_count,
                                        &local_exp_time_x10);
    }
    EGIS_IMAGE_KEEP_PARAM(hw_integrate_count, local_hw_integrate_count);
    EGIS_IMAGE_KEEP_PARAM(exposure_x10, local_exp_time_x10);
    EGIS_IMAGE_KEEP_PARAM_v2(all_adc_dac, "dac", all_adc_dac);
    egislog_d("%s [%d] integrate count %d, exp_time_x10 %d all_adc_dac %d", __func__, try_match_count, local_hw_integrate_count,
        local_exp_time_x10, all_adc_dac);

    BOOL enable_pipeline_capture =
        !from_inline && !egis_fp_is_enroll() && core_config_get_int(INI_SECTION_VERIFY, KEY_PIPELINE_CAPTURE, FALSE);
    isensor_set_int(PARAM_INT_ENABLE_PIPELINE_CAPTURE, enable_pipeline_capture);
    isensor_set_int(PARAM_INT_TRY_MATCH_COUNT, try_match_count);
    if (!enable_pipeline_capture || try_match_count == 0 ) isensor_set_int(PARAM_INT_ENABLE_BAC, 1);
    TIME_MEASURE_START(g_fn_fetch_image_16bit);
    ret = g_fn_fetch_image_16bit(raw_img, height, width, integrate_count);
    TIME_MEASURE_STOP(g_fn_fetch_image_16bit, "g_fn_fetch_image_16bit = ");
    isensor_set_int(PARAM_INT_ENABLE_BAC, 0);

    _quick_capture_after_get_frame(local_hw_integrate_count, integrate_hw_count, local_exp_time_x10,
                                   exp_time_x10, raw_img, width * height);

    int roi_length = core_config_get_int(INI_SECTION_SENSOR, KEY_RAW_NORMALIZATION_ROI,
                                         INID_RAW_NORMALIZATION_ROI);
    image_raw_normalization(raw_img, width, height, integrate_hw_count, roi_length);

    if (ret != EGIS_OK) {
        egislog_e("isensor_get_frame error, ret = %d", ret);
        ret = EGIS_COMMAND_FAIL;
    }
    return ret;
}

static int _fd_get_bkg(uint16_t* raw_img, int width, int height, int byte_per_pixel,
                       uint16_t* bkg_img, uint16_t* wk_img) {
    int bkg_img_sz = width * height * byte_per_pixel;
    int expo_time_x10, hw_int_count, temperature;
    RBS_CHECK_IF_NULL(bkg_img, EGIS_NULL_POINTER);
    RBS_CHECK_IF_NULL(wk_img, EGIS_NULL_POINTER);
    isensor_get_int(PARAM_INT_EXPOSURE_TIME_X10, &expo_time_x10);
    isensor_get_int(PARAM_INT_HW_INTEGRATE_COUNT, &hw_int_count);
    isensor_get_int(PARAM_INT_TEMPERATURE, &temperature);
    int ret = bkm_renew_bkg(raw_img, FLOAT_TO_INT(temperature), (float)expo_time_x10 / (float)10,
                            hw_int_count);
    if (ret != EGIS_OK) {
        egislog_e("bkm_renew_bkg failed");
        return EGIS_COMMAND_FAIL;
    }

    if (ukm_is_enabled() && egis_fp_is_enroll() && ukm_is_bkg_ready()) {
        ukm_get_uk_bkg((unsigned char*)bkg_img, bkg_img_sz);
    } else {
        int bkg_use_option = core_config_get_int(INI_SECTION_SENSOR, KEY_BKG_IMG_USE_OPTION, 0);
        bkg_pointer_type type = bkg_use_option > 0 ? BKG_TYPE_BDS : BKG_TYPE_NORMALIZE;
        bkm_get_bkg_image(type, (uint16_t*)bkg_img, bkg_img_sz);
        RBS_CHECK_IF_NULL(bkg_img, EGIS_INCORRECT_PARAMETER);
#ifdef LA_BDS
        bkm_get_bkg_image(BKG_TYPE_CALIBRATION_WK, (uint16_t*)wk_img, bkg_img_sz);
        RBS_CHECK_IF_NULL(wk_img, EGIS_INCORRECT_PARAMETER);
#endif
    }
    if (bkg_img_sz == 0) {
        egislog_e("calibration bkg_data not exist!");
        return EGIS_COMMAND_FAIL;
    }

    return EGIS_OK;
}

int fd_acquire_image(uint8_t* out_image, int out_width, int out_height, int option) {

    int ret;
    int frame_sz = 0, width = 0, height = 0;
    int integrate_count = 0, integrate_hw_count = 0, bkg_img_sz = 0, max_merge_qty = 0,
        qty_merge_tmp = 0, qty_partial = 0;
    int exp_time_x10, temperature;
    BOOL is_good_finger = FALSE;
    uint16_t* img_detect = NULL;
    uint16_t* bkg_img = NULL;
    uint16_t* wk_img = NULL;
    uint8_t* merge_image = NULL;
    int byte_per_pixel = g_raw_bpp / 8;
    int try_match_count = option;
    BOOL skip_BDS_add = FALSE;
    if ((option == FD_ACQUIREI_OPTION_FROM_INLINE)
        || (option == FD_ACQUIREI_OPTION_GET_FFC_IMAGE)
        || (option == FD_ACQUIREI_OPTION_GET_FFC_IMAGE_NO_UPDATE_BDS)) {
        try_match_count = 0;
        if (option != FD_ACQUIREI_OPTION_GET_FFC_IMAGE)
            skip_BDS_add = TRUE;
    }
    int sensor_id;
    isensor_get_int(PARAM_INT_SENSOR_ID, &sensor_id);
    uint8_t dev_id1 = sensor_id % 100;
    ex_log(LOG_VERBOSE, "%s, sensor id %d, dev_id1 = %d", __func__, sensor_id, dev_id1);

    g_quality = 0;
    g_partial_percentage = 0;
    g_is_fake_finger = 1;
    g_g2_partial_qty = 1;

    int is_et0xx = 0;
    isensor_get_int(PARAM_INT_GET_IS_ET0XX, &is_et0xx);

    if (is_et0xx > 0) {
        isensor_set_int(PARAM_INT_TRY_INDEX, try_match_count);
        int db_is_raw_image = 0;
        isensor_get_int(PARAM_INT_DB_IS_RAW_IMAGE, &db_is_raw_image);
        if (!db_is_raw_image) {
            return _get_image_8bit_from_db(out_image, out_width, out_height);
        }
    }

    isensor_get_sensor_roi_size(&width, &height);
    if (width <= 0 || height <= 0) {
        egislog_e("parameter error");
        ret = EGIS_COMMAND_FAIL;
        goto exit;
    }
    frame_sz = width * height;

    isensor_get_int(PARAM_INT_INTEGRATE_COUNT, &integrate_count);
    if (integrate_count != 1) {
        //egislog_e("Workaround. because integrate_sw_count > 1 make system not stable (driver issue?) ");
        integrate_count = 1;
    }

    isensor_get_int(PARAM_INT_HW_INTEGRATE_COUNT, &integrate_hw_count);
    if (integrate_hw_count <= 0) {
        egislog_e("integrate_hw_count parameter error");
        ret = EGIS_COMMAND_FAIL;
        goto exit;
    }

    isensor_get_int(PARAM_INT_EXPOSURE_TIME_X10, &exp_time_x10);
    if (exp_time_x10 <= 0) {
        egislog_e("exp_time_x10 parameter error");
        ret = EGIS_COMMAND_FAIL;
        goto exit;
    }

    EGIS_IMAGE_KEEP_PARAM(sw_integrate_count, integrate_count);
    isensor_get_int(PARAM_INT_TEMPERATURE, &temperature);
    EGIS_IMAGE_KEEP_PARAM(temperature, temperature);
    EGIS_IMAGE_KEEP_PARAM(index_try_match, try_match_count);

    BOOL is_low_temp = (temperature < 10 ? TRUE : FALSE);
    egislog_d("%s temperature %d, exp_time_x10 %d, try_match=%d", __func__, temperature,
              exp_time_x10, try_match_count);

    bkg_img_sz = frame_sz * byte_per_pixel;
    bkg_img = plat_alloc(bkg_img_sz);
    wk_img = plat_alloc(bkg_img_sz);
    merge_image = plat_alloc(frame_sz);

    //egislog_e("frame_sz=%d, byte_per_pixel=%d", frame_sz,byte_per_pixel) ;


    switch (g_fd_process_option) {
        case FD_PROCESS_OPTION_USE_OLD_RAW:
            break;

        default:
            ret = _fd_get_raw(g_raw_img_buf, width, height, try_match_count, integrate_hw_count,
                              exp_time_x10, temperature, option == FD_ACQUIREI_OPTION_FROM_INLINE);
            if (ret != EGIS_OK) {
                egislog_e("%s, _fd_get_raw failed", __func__);
                memset(g_raw_img_buf, 0, frame_sz * byte_per_pixel);
                goto exit;
            }
            break;
    }

    if (g_fd_process_option == FD_PROCESS_OPTION_ONLY_GET_RAW) {
        goto exit;
    }
    ret = _fd_get_bkg(g_raw_img_buf, width, height, byte_per_pixel, bkg_img, wk_img);
    if (ret != EGIS_OK) {
        egislog_e("_fd_get_bkg failed");
        goto exit;
    }

    uint16_t* rawimg = g_raw_img_buf;

#if defined(G3PLUS_MATCHER)
    egis_input_proc(rawimg, bkg_img, merge_image, width, height, integrate_hw_count, 1,
                    &qty_merge_tmp, 6);
#elif defined(ALGO_GEN_5) || defined(ALGO_GEN_7)
#if defined(LA_BDS)
    int* img_int = plat_alloc(width * height * sizeof(int));
    int *bkg_int = plat_alloc(width * height * sizeof(int));
    int *wbkg_int = plat_alloc(width * height * sizeof(int));
    int *ffc_int = plat_alloc(width * height * sizeof(int));
    if (img_int == NULL || bkg_int == NULL || wbkg_int == NULL || ffc_int == NULL) {
        egislog_e("%s, mem alloc error", __func__);
        goto exit;
    }
    memset(bkg_int, 0, width * height * sizeof(int));
    memset(wbkg_int, 0, width * height * sizeof(int));
    memset(ffc_int, 0, width * height * sizeof(int));
    for (int i = 0; i < width * height; i++) {
        img_int[i] = rawimg[i];
        bkg_int[i] = bkg_img[i];
        wbkg_int[i] = wk_img[i];
    }
    static int idx = 0;
    idx++;
    et760_FlatFieldCorrection(img_int, bkg_int, wbkg_int, ffc_int, width, height);

    IPimage_760_IPP6_61b(
            ffc_int, merge_image, width, height,
            gSigmaHigh, gSigmaLow, thSigmaOutlier, lce_gain_max_2e12, lce_alpha_2e4, clip_limit_param_2e10, tmp_low, tmp_high, idx);

    if (option == FD_ACQUIREI_OPTION_GET_FFC_IMAGE
        || option == FD_ACQUIREI_OPTION_GET_FFC_IMAGE_NO_UPDATE_BDS ) {
        egislog_i("Option is FD_ACQUIREI_OPTION_GET_FFC_IMAGE(%d) return ffc image for et760 liveview only", FD_ACQUIREI_OPTION_GET_FFC_IMAGE) ;
        for (int i = 0; i < width * height; i++)
        {
            rawimg[i] = ffc_int[i];
        }
    }

    PLAT_FREE(img_int);
    PLAT_FREE(bkg_int);
    PLAT_FREE(wbkg_int);
    PLAT_FREE(ffc_int);
#else
    egis_input_proc(rawimg, bkg_img, merge_image, width, height, integrate_hw_count, 1,
                    &qty_merge_tmp, 6, NULL);
#endif
#elif defined(xALGO_GEN_7)
    go_input_proc(rawimg, bkg_img, merge_image, width, height, integrate_hw_count, 1,
                  &qty_merge_tmp, ukm_is_enabled() ? RBS_UK_FLOW_MODE : 62);
#endif
    egislog_d(
        "%s, egis_input_proc , rawimg = 0x%08x, bkg_img = 0x%08x, merge_image = 0x%08x, width = "
        "0x%08x, height = 0x%08x",
        __func__, rawimg, bkg_img, merge_image, width, height);

    TIME_MEASURE_START(finger_detect);
    int black_edge_score;
    ip_finger_detect(g_raw_img_buf, bkg_img, merge_image, &img_detect, try_match_count, width,
                     height, &g_is_fake_finger, &g_partial_percentage, &g_is_light,
                     &black_edge_score);
    TIME_MEASURE_STOP_AND_RESTART(finger_detect, "finger_detect total =");

    int bkg_cx = 0, bkg_cy = 0;
    isensor_get_int(PARAM_INT_CALI_BKG_CX, &bkg_cx);
    isensor_get_int(PARAM_INT_CALI_BKG_CY, &bkg_cy);
    int avg_raw = cal_img_avg_intensity_api(rawimg, width, height, width / 2, height / 2, width / 2,
                                            height / 2);
    int avg_raw_625 =
        cal_img_avg_intensity_api(rawimg, width, height, bkg_cx, bkg_cy, 25 / 2, 25 / 2);
    int avg_bkg_625 =
        cal_img_avg_intensity_api(bkg_img, width, height, bkg_cx, bkg_cy, 25 / 2, 25 / 2);
    EGIS_IMAGE_KEEP_PARAM(M, avg_raw);
    EGIS_IMAGE_KEEP_PARAM(Mc, avg_raw_625);
    EGIS_IMAGE_KEEP_PARAM(MBc, avg_bkg_625);
    egislog_d("avg_raw %d, avg_raw_625 %d", avg_raw, avg_raw_625);
#if defined(G3PLUS_MATCHER) || defined(ALGO_GEN_5) || defined(ALGO_GEN_7)
    memcpy(out_image, merge_image, frame_sz);
    if (qty_merge_tmp < 100) {
        //egislog_e("Workaround: qty_merge_tmp %d -> 100", qty_merge_tmp);
        qty_merge_tmp = 100;
    }
    // Check BAC result
    int bac_result;
    isensor_get_int(PARAM_INT_BAC_RESULT, &bac_result);
    if (bac_result == RESULT_BAC_DARK_IMAGE) {
        qty_merge_tmp = -2;
        egislog_d("BAC dark image: qty_merge_tmp = %d", qty_merge_tmp);
    }
    g_quality = qty_merge_tmp;
#elif defined(ALGO_GEN_2)
    BOOL is_bds_path_change = FALSE;
    isensor_get_int(PARAM_INT_BDS_PATH_CHANGE, &is_bds_path_change);
    int input_seq_proc_mode = core_config_get_int(INI_SECTION_SENSOR, KEY_INPUT_SEQ_PROC_MODE, 0);
    if (is_bds_path_change && is_low_temp) {
        seq_proc_update_init();
        egislog_d("%s, seq_proc_update_init, temperature = ", __func__, temperature);
    }
    for (int i = 0; i < integrate_count; i++) {
        switch (dev_id1) {
            case 26: {
                int qty;
                input_seq_proc(rawimg, bkg_img, width, height, i, 1, &qty, &qty_merge_tmp,
                               &qty_partial, merge_image, 0, 0, 0, 0);
                break;
            }
            case 29:
            case 11: {
                int result_bkgEst = 0;
                input_seq_proc_New(rawimg, bkg_img, width, height, i, integrate_hw_count, 1,
                                   &qty_merge_tmp, &qty_partial, merge_image, input_seq_proc_mode,
                                   0, 0, &result_bkgEst);
                break;
            }
            case 13:
            default: {
                input_seq_proc_New_v2(rawimg, bkg_img, width, height, i, try_match_count,
                                      integrate_hw_count, 1, &qty_merge_tmp, &qty_partial,
                                      merge_image, input_seq_proc_mode);
                break;
            }
        }
        egislog_d("input_seq_proc(%d)[%d] qty_merge_tmp = %d qty_partial = %d", dev_id1, i,
                  qty_merge_tmp, qty_partial);
        if (qty_merge_tmp >= max_merge_qty) {
            g_quality = qty_merge_tmp;
            max_merge_qty = qty_merge_tmp;
            memcpy(out_image, merge_image, frame_sz);
            egislog_d("max_merge_qty=%d ", max_merge_qty);
        } else if (g_quality == 0) {
            /* William Workaround for output bad image when qty_partial is -1 */
            g_quality = qty_merge_tmp;
            max_merge_qty = qty_merge_tmp;
            memcpy(out_image, merge_image, frame_sz);
            egislog_i("William Workaround , send SP [-1] max_merge_qty=%d, qty_merge_tmp=%d",
                      max_merge_qty, qty_merge_tmp);
        } else {
            egislog_d("%s, g_quality = %d ", __func__, g_quality);
        }
        g_g2_partial_qty = qty_partial;
        EGIS_IMAGE_KEEP_PARAM(g2_partial, g_g2_partial_qty);
        rawimg += frame_sz * byte_per_pixel;
    }
#elif defined(ALGO_GEN_4)
    // Check BAC result
    int bac_result;
    isensor_get_int(PARAM_INT_BAC_RESULT, &bac_result);
    if (bac_result == RESULT_BAC_DARK_IMAGE) {
        qty_merge_tmp = -2;
        egislog_d("BAC dark image: qty_merge_tmp = %d", qty_merge_tmp);
    }
    g_quality = qty_merge_tmp;

    algo_set_int(ALGO_PARAM_TYPE_RETRY_NUM, try_match_count);
    algo_set_int(ALGO_PARAM_TYPE_SYS_TEMP_X10, temperature * 10);
#else
#error
    egislog_e("No ALGO_GEN is set");
#endif
    TIME_MEASURE_STOP(finger_detect, "ipp_proc");

    BOOL overflow_skip_bds_add = FALSE;
    if (dev_id1 == 2) {
        int over_percentage = 0;
        int real_bpp = 14;
        // TODO: Let et0xx support the parameter
        // isensor_get_int(PARAM_INT_GET_REAL_RAW_BPP, &real_bpp);
        uint16_t threshold;
        uint32_t percentage = (uint32_t)core_config_get_int(
            INI_SECTION_SENSOR, KEY_IPP_FILTER_OVERFLOW_THRESHOLD_PERCENTAGE, 95);
        threshold = (uint16_t)((1 << real_bpp) * percentage / 100);
        fd_pipp_overflow_process(g_raw_img_buf, out_image, width, height, threshold, 0,
                                 &over_percentage);
        ex_log(LOG_DEBUG, "overflow_percentage %d. threshold = %d", over_percentage, threshold);
        int partial_thres = 10;
        if (over_percentage >= partial_thres) {
            ex_log(LOG_DEBUG, "overflow_percentage >= %d. set partial ", partial_thres);
            overflow_skip_bds_add = TRUE;
        }
    }

    EGIS_IMAGE_KEEP_PARAM(qty, g_quality);

    int check_residual = 0;
    g_has_residual = 0;
    if (!egis_fp_is_enroll()) {
#ifdef COMPRESS_BUFFER
        const uint8_t VER = 0x1;
        uint8_t my_info[8] = {0xE,         'G', 'I', 'S', VER, (uint8_t)(20 * 2),
                              (uint8_t)20, 0};  // Must be ended with zero
        my_info[5] = (uint8_t)(temperature * 2);
        my_info[6] = (uint8_t)(temperature);
        compress_info(out_image, width, height, my_info);
#endif

#ifdef MATCHER_TAKE_RAW_IMAGE
        mfeature_extract(try_match_count, (uint8_t*)rawimg, width, height);
        g_is_fake_finger = mfeature_get_result(try_match_count, MFEAT_RESULT_IS_FAKE);
        if (g_is_fake_finger) {
            EGIS_IMAGE_KEEP_PARAM(fake_score, 2);
        } else {
            EGIS_IMAGE_KEEP_PARAM(fake_score, -2);
        }
#else
        mfeature_extract(try_match_count, out_image, width, height);
#endif

#ifdef ALGO_GEN_4
        int img_len = 0;
        const unsigned char* debug_image = megvii_get_debug_image(&img_len);
        memcpy(out_image, debug_image, img_len);
#endif

        int mfeat_quality = mfeature_get_result(try_match_count, MFEAT_RESULT_EQTY);
        EGIS_IMAGE_KEEP_PARAM(extract_qty, mfeat_quality);

        check_residual = mfeature_get_result(try_match_count, MFEAT_RESULT_RESIDUAL);
        int enable_check_residual =
            core_config_get_int(INI_SECTION_VERIFY, KEY_VERIFY_CHECK_RESIDUAL, 0);
        if (!enable_check_residual || is_low_temp) {
            check_residual = 0;
        } else if (check_residual != 0) {
            egislog_d("residual is detected %d", check_residual);
            g_has_residual = 1;
        }
    }
#if !defined(ALGO_GEN_4) && !defined(LA_BDS)
    BOOL is_partial;
    int enroll_partial_threshold = core_config_get_int(
        INI_SECTION_ENROLL, KEY_ENROLL_PARTIAL_THRESHOLD, INID_ENROLL_PARTIAL_THRESHOLD);
    if (g_g2_partial_qty < 0 || overflow_skip_bds_add ||
        g_partial_percentage < enroll_partial_threshold) {
        is_partial = TRUE;
    } else {
        is_partial = FALSE;
    }

    if (!skip_BDS_add) {
        update_sensor_BDS_in_fd_acquire(g_raw_img_buf, img_detect, frame_sz * byte_per_pixel,
                                        try_match_count, is_partial, check_residual);
    }
#endif
    ret = EGIS_OK;
exit:
    if (merge_image) plat_free(merge_image);
    if (bkg_img) plat_free(bkg_img);
    if (wk_img) plat_free(wk_img);
    if (img_detect) plat_free(img_detect);
    return ret;
}
