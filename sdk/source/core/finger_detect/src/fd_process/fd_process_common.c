#include "fd_process.h"
#include "isensor_api.h"
#include "plat_log.h"
#include "plat_mem.h"
#ifdef DEBUG_SAVE_RAW_IMAGE
#include "plat_file.h"
extern int mkdir(const char*, uint32_t);
int g_detect_idx = 0;
#endif
#include "image_cut.h"

#define LOG_TAG "RBS-EGISFP"

BOOL g_skip_process = FALSE;
BOOL g_fetch_single_image = FALSE;
fn_fetch_image g_fn_fetch_image = isensor_get_dynamic_frame;
fn_fetch_image_16bit g_fn_fetch_image_16bit = isensor_get_dynamic_frame_16bit;

int g_quality = 100;
int g_quality_image = 100;
int g_partial_percentage = 100;
int g_g2_partial_qty = 100;
int g_is_fake_finger = 0;
int g_is_light = 0;
int g_has_residual = 0;

uint16_t* g_raw_img_buf = NULL;
uint32_t g_raw_img_buf_size = 0;
uint16_t g_raw_num = 0;
BOOL g_use_ext_buf = FALSE;
enum fd_process_option g_fd_process_option = FD_PROCESS_OPTION_NORMAL;

int g_fd_sensor_width = 0;
int g_fd_sensor_height = 0;
int g_raw_bpp = 16;

int g_integrating_count = 0;

int g_bds_debug_path = 0;

void fd_set_fetch_image_fn(fn_fetch_image fetch_img, BOOL skip_process) {
    g_fn_fetch_image = fetch_img;
    g_skip_process = skip_process;
}

void fd_set_fetch_image_16bit_fn(fn_fetch_image_16bit fetch_img, BOOL skip_process) {
    g_fn_fetch_image_16bit = fetch_img;
    g_skip_process = skip_process;
}

void fd_reset_fetch_image_fn() {
    g_fn_fetch_image = isensor_get_dynamic_frame;
}

void fd_set_fetch_single_image(BOOL single) {
    g_fetch_single_image = single;
}

BOOL fd_get_fetch_single_image() {
    return g_fetch_single_image;
}

int fd_process_create(uint8_t* ext_buf, uint32_t ext_buf_size, uint16_t num_image,
                      enum fd_process_option option) {
    int retval = EGIS_OK;

    if (g_fd_process_option == FD_PROCESS_OPTION_ONLY_GET_RAW) {
        if (option == FD_PROCESS_OPTION_USE_OLD_RAW) {
            g_fd_process_option = option;
            egislog_d("%s, FD_PROCESS_OPTION_USE_OLD_RAW", __func__);
            return EGIS_OK;
        } else if (option == FD_PROCESS_OPTION_ONLY_GET_RAW) {
            egislog_d("%s, only_get_raw twice", __func__);
            fd_process_destroy();
        }
    }

#ifdef DEBUG_SAVE_RAW_IMAGE
    g_detect_idx = 0;
    if (g_detect_idx == 0) {
        mkdir("/data/debug/1", 0777);
        mkdir("/data/debug/1/enroll", 0777);
        mkdir("/data/debug/1/verify", 0777);
    }
#endif

    if (num_image <= 0) {
        egislog_e("wrong num_image %d", num_image);
        num_image = 1;
    }

    retval = isensor_get_sensor_roi_size(&g_fd_sensor_width, &g_fd_sensor_height);
    isensor_get_int(PARAM_INT_RAW_IMAGE_BPP, &g_raw_bpp);
    g_raw_img_buf_size = (uint32_t)(g_fd_sensor_width * g_fd_sensor_height * (g_raw_bpp / 8));
    if (retval != EGIS_OK || g_raw_img_buf_size == 0) {
        egislog_e("Fail to get sensor size!");
        return EGIS_INCORRECT_PARAMETER;
    }

    switch (option) {
        case FD_PROCESS_OPTION_USE_OLD_RAW:
            if (g_fd_process_option == FD_PROCESS_OPTION_ONLY_GET_RAW) {
                break;
            }
            egislog_e("%s, wrong process!", __func__);
            option = FD_PROCESS_OPTION_NORMAL;
        case FD_PROCESS_OPTION_NORMAL:
        case FD_PROCESS_OPTION_ONLY_GET_RAW:
            if (ext_buf != NULL && ext_buf_size >= g_raw_img_buf_size) {
                g_use_ext_buf = TRUE;
            } else {
                g_use_ext_buf = FALSE;
            }
            egislog_d("%s, g_use_ext_buf = %d", __func__, g_use_ext_buf);
            if (g_use_ext_buf) {
                g_raw_img_buf = (uint16_t*)ext_buf;
            } else {
                g_raw_img_buf = plat_alloc(g_raw_img_buf_size * num_image);
            }
            if (g_raw_img_buf == NULL) {
                egislog_e("%s EGIS_OUT_OF_MEMORY", __func__);
                return EGIS_OUT_OF_MEMORY;
            }
            break;
        default:
            egislog_e("%s, unsupported option!", __func__);
            break;
    }
    g_fd_process_option = option;
    g_raw_num = num_image;
    return retval;
}

void fd_process_destroy() {
    if (!g_use_ext_buf) {
        PLAT_FREE(g_raw_img_buf);
    }
    g_raw_img_buf = NULL;
    g_raw_img_buf_size = 0;
    g_raw_num = 0;

    g_use_ext_buf = FALSE;

    g_fd_sensor_width = 0;
    g_fd_sensor_height = 0;
    g_raw_bpp = 16;

    g_fd_process_option = FD_PROCESS_OPTION_NORMAL;
}

uint16_t* fd_get_current_raw16_pointer() {
    if (g_raw_bpp == 16) {
        return g_raw_img_buf;
    } else {
        egislog_e("no raw 16bit");
        return NULL;
    }
}

int process_image_in_place(enum proc_type_t proc_type, uint8_t* intensity, int in_width,
                           int in_height, int out_width, int out_height) {
    egislog_d("%s, proc_type = %d", __func__, proc_type);
    int i = 0, image_size = 0;

    switch (proc_type) {
        case PROC_INVERT:
            image_size = in_width * in_height;
            for (i = 0; i < image_size; i++) {
                intensity[i] = 0xff - intensity[i];
            }
            break;
        case PROC_CROP:
            if (out_width == 0 || out_height == 0) break;
            if (out_width == in_width && out_height == in_height) break;
            if (out_width < in_width && out_height < in_height) {
                image_crop(intensity, in_width, in_height, out_width, out_height);
            }
            break;
        default:
            egislog_e("%s, not supported proc_type.", __func__);
            break;
    }
    return EGIS_OK;
}

int fd_process_get_int(fd_param_type_t param_type, int* value) {
    switch (param_type) {
        case FD_PARAM_INT_QUALITY:
            *value = g_quality;
            break;
        case FD_PARAM_INT_PARTIAL_PERCENTAGE:
            *value = g_partial_percentage;
            break;
        case FD_PARAM_INT_CURRENT_INTEGRATED_COUNT:
            *value = g_integrating_count;
            break;
        case FD_PARAM_INT_IS_FAKE_FINGER:
            *value = g_is_fake_finger;
            break;
        case FD_PARAM_INT_IMAGE_IS_LIGHT:
            *value = g_is_light;
            break;
        case FD_PARAM_INT_G2_PARTIAL_QTY:
            *value = g_g2_partial_qty;
            break;
        case FD_PARAM_INT_HAS_RESIDUAL:
            *value = g_has_residual;
            break;
        default:
            egislog_e("%s, not supported type = %d", __func__, param_type);
            break;
    }
    return EGIS_OK;
}

uint8_t* fd_get_current_raw_pointer(int* raw_bpp) {
    *raw_bpp = g_raw_bpp;
    return (uint8_t*)g_raw_img_buf;
}

int fd_get_raw_bpp() {
    return g_raw_bpp;
}
