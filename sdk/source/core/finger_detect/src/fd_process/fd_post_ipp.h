#ifndef __FD_POST_IPP_H__
#define __FD_POST_IPP_H__

#include <stdint.h>

int fd_pipp_overflow_process(uint16_t* raw_img, uint8_t* bin_img, int width, int height,
                             uint16_t threshold, uint8_t new_value, int* over_percentange);

#endif