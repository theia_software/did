#include "bkg_manager.h"
#include <stdio.h>
#ifndef LA_BDS
#include "bkimg_pool.h"
#endif
#include "core_config.h"
#include "detect_scratch.h"
#include "egis_definition.h"
#include "egis_sprintf.h"
#include "image_cut.h"
#include "ini_definition.h"
#include "isensor_api.h"
#include "plat_file.h"
#include "plat_log.h"
#include "plat_mem.h"
#include "plat_time.h"
#include "type_definition.h"

#ifdef LA_BDS
#include "work_with_G2/la_bds.h"
#endif

#define LOG_TAG "RBS-bkg"
#define FILENAME_BDS "calibration_bds.bin"

#ifdef ALGO_GEN_7
static char g_bkg_folder_path[PATH_MAX] = {"/sdcard/RbsG7Temp/"};
#else
static char g_bkg_folder_path[PATH_MAX] = {"/sdcard/RbsG5Temp/"};
#endif
static char g_bds_file_path[PATH_MAX] = {0};

static uint16_t* g_bds_bkg = NULL;

static uint16_t* g_calib_bkg = NULL;
static uint16_t* g_calib_bkg_normalized = NULL;
static int g_calib_hw_int_count;
static float g_calib_exp_time;
static BOOL g_is_bkm_initialized = FALSE;

#ifdef LA_BDS
static uint8_t* g_labkg_pool = NULL;
#endif

int on_calib_data_update(uint16_t* calib_bkg, uint16_t* calib_wkg, int hw_int_count, float expo_time);

static int generate_normalized_calib_bkg() {
    int roi_length = core_config_get_int(INI_SECTION_SENSOR, KEY_RAW_NORMALIZATION_ROI,
                                         INID_RAW_NORMALIZATION_ROI);
    if (roi_length <= 0) {
        return EGIS_INCORRECT_PARAMETER;
    }
    RBS_CHECK_IF_NULL(g_calib_bkg_normalized, EGIS_INCORRECT_STATUS);
    int sensor_full_width, sensor_full_height;
    isensor_get_sensor_full_size(&sensor_full_width, &sensor_full_height);
    memcpy(g_calib_bkg_normalized, g_calib_bkg, sensor_full_width * sensor_full_height * sizeof(int16_t));
    image_raw_normalization(g_calib_bkg_normalized, sensor_full_width, sensor_full_height,
                            g_calib_hw_int_count, roi_length);
    return EGIS_OK;
}

int bkm_get_bkg_image(enum bkg_pointer_type bkg_type, uint16_t* bkg_buffer, int bkg_buffer_size) {
    egislog_d("%s, bkg_type = %d g_is_bkm_initialized %d", __func__, bkg_type, g_is_bkm_initialized);
    if (!g_is_bkm_initialized) {
        int ret = bkm_init();
        if (ret != EGIS_OK) {
            egislog_e("%s, bkm_init fail!!", __func__);
            return EGIS_INCORRECT_STATUS;
        }
    }
    RBS_CHECK_IF_NULL(bkg_buffer, EGIS_INCORRECT_PARAMETER);
    int sensor_roi_width, sensor_roi_height;
    int ret = isensor_get_sensor_roi_size(&sensor_roi_width, &sensor_roi_height);
    if (ret != EGIS_OK) {
        egislog_e("%s get sensor roi size fail.", __func__);
        return EGIS_COMMAND_FAIL;
    }

    int sensor_full_width, sensor_full_height;
    ret = isensor_get_sensor_full_size(&sensor_full_width, &sensor_full_height);
    if (ret != EGIS_OK) {
        egislog_e("%s get sensor full size fail.", __func__);
        return EGIS_COMMAND_FAIL;
    }
    egislog_d("%s, sensor_roi_width = %d, sensor_roi_height = %d", __func__, sensor_roi_width, sensor_roi_height);
    int bkg_sz = sensor_roi_width * sensor_roi_height * sizeof(uint16_t);
    if (bkg_buffer_size < bkg_sz) {
        egislog_d("%s, bkg_buffer_size < bkg_sz %d %d", __func__, bkg_buffer_size, bkg_sz);
        return EGIS_INCORRECT_PARAMETER;
    }
    uint16_t* bkg;
#ifdef LA_BDS
    uint16_t* la_bkg = NULL;
    int labkg_size = sensor_full_width * sensor_full_height * sizeof(uint16_t);
#endif
    int retval;
    switch (bkg_type) {
        case BKG_TYPE_BDS:
#if !defined(ALGO_GEN_4) && !defined(LA_BDS)
            if (g_bds_bkg != NULL) {
                egislog_d("%s, has bds", __func__);
                bkg = g_bds_bkg;
                break;
            }
#endif
        case BKG_TYPE_NORMALIZE:
#ifdef LA_BDS
            la_bkg = plat_alloc(labkg_size);
            RBS_CHECK_IF_NULL(la_bkg, EGIS_OUT_OF_MEMORY);
            labkg_get_ffc_bds_data(la_bkg, NULL, NULL, NULL, sensor_full_width, sensor_full_height);
            RBS_CHECK_IF_NULL(la_bkg, EGIS_INCORRECT_PARAMETER);
            break;
#else
            retval = generate_normalized_calib_bkg();
            if (retval == EGIS_OK) {
                egislog_d("%s, has normalized", __func__);
                bkg = g_calib_bkg_normalized;
                break;
            }
#endif
        case BKG_TYPE_CALIBRATION_BKG:
            egislog_d("%s, use default bkg", __func__);
#ifdef LA_BDS
            la_bkg = plat_alloc(labkg_size);
            RBS_CHECK_IF_NULL(la_bkg, EGIS_OUT_OF_MEMORY);
            labkg_get_ffc_bds_data(la_bkg, NULL, NULL, NULL, sensor_full_width, sensor_full_height);
            RBS_CHECK_IF_NULL(la_bkg, EGIS_INCORRECT_PARAMETER);
            break;
#else
            bkg = g_calib_bkg;
#endif
            break;
#ifdef LA_BDS
        case BKG_TYPE_CALIBRATION_WK:
            la_bkg = plat_alloc(labkg_size);
            RBS_CHECK_IF_NULL(la_bkg, EGIS_OUT_OF_MEMORY);
            labkg_get_ffc_bds_data(NULL, la_bkg, NULL, NULL, sensor_full_width, sensor_full_height);
            RBS_CHECK_IF_NULL(la_bkg, EGIS_INCORRECT_PARAMETER);
            break;
#endif
        default:
            egislog_e("%s, unsupport value of bkg_type %d", __func__, bkg_type);
            return EGIS_INCORRECT_PARAMETER;
    }

#ifdef LA_BDS
    int x0, y0;
    isensor_get_int(PARAM_INT_ROI_X, &x0);
    isensor_get_int(PARAM_INT_ROI_Y, &y0);
    //x0 = sensor_full_width - (x0 + sensor_roi_width);
    //y0 = sensor_full_height - (y0 + sensor_roi_height);
    //if(x0 < 0) {
    //    x0 = 0;
    //}
    //if(y0 < 0) {
    //    y0 = 0;
    //}
    //if(x0 > (sensor_full_width - sensor_roi_width)) {
    //    x0 = (sensor_full_width - sensor_roi_width);
    //}
    //if(y0 > (sensor_full_height - sensor_roi_height)) {
    //    y0 = (sensor_full_height - sensor_roi_height);
    //}
    egislog_d("%s, image_crop_xy + %d %d %d %d %d %d"
    , __func__, x0, y0, sensor_full_width, sensor_full_height, sensor_roi_width,
              sensor_roi_height);
    egislog_d("ROI_CHECK %s, image_crop_xy + %d %d %d %d %d %d"
    , __func__, x0, y0, sensor_full_width, sensor_full_height, sensor_roi_width,
              sensor_roi_height);

    image_crop_xy(la_bkg, sensor_full_width, sensor_full_height, bkg_buffer, sensor_roi_width,
                      sensor_roi_height, x0, y0);
    PLAT_FREE(la_bkg);
#else
    RBS_CHECK_IF_NULL(bkg, EGIS_INCORRECT_PARAMETER);
    memcpy(bkg_buffer, bkg, bkg_sz);
#endif
    return EGIS_OK;
}

static void load_bds() {
#if !defined(ALGO_GEN_4) && !defined(LA_BDS)
        egist_snprintf(g_bds_file_path, PATH_MAX, "%s/%s", g_bkg_folder_path, FILENAME_BDS);
        int real_raw_bpp;
        isensor_get_int(PARAM_INT_GET_REAL_RAW_BPP, &real_raw_bpp);
        g_calib_hw_int_count =
            core_config_get_int(INI_SECTION_SENSOR, KEY_INTEGRATE_COUNT_HW, g_calib_hw_int_count);
        g_calib_exp_time =
            core_config_get_double(INI_SECTION_SENSOR, KEY_EXPOSURE_TIME, g_calib_exp_time);
        bkimg_pool_set_sensor_id(g_egis_sensortype.series, g_egis_sensortype.type);
        int sensor_full_width, sensor_full_height;
        isensor_get_sensor_full_size(&sensor_full_width, &sensor_full_height);
        int retval = bkimg_pool_load(g_bds_file_path, sensor_full_width, sensor_full_height, g_calib_hw_int_count,
                                    g_calib_bkg, real_raw_bpp);
        egislog_d("%s, bkimg_pool_load retval = %d", __func__, retval);
#else
    // disable bds
#endif
}

int bkm_init() {
    egislog_d("%s", __func__);
    int sensor_full_width = 0, sensor_full_height = 0;
    int ret = isensor_get_sensor_full_size(&sensor_full_width, &sensor_full_height);
    if (ret != EGIS_OK) {
        egislog_e("%s get sensor full size fail.", __func__);
        return EGIS_COMMAND_FAIL;
    }
#ifdef LA_BDS
    int pool_size = labkg_get_pool_size(sensor_full_width, sensor_full_height);
    if (g_labkg_pool == NULL) {
        g_labkg_pool = plat_alloc(pool_size);
    } else {
        ex_log(LOG_ERROR, "%s, db pointer isn't NULL. unexpected flow", __func__);
    }
    RBS_CHECK_IF_NULL(g_labkg_pool, EGIS_OUT_OF_MEMORY);
#endif
    int bkg_size = sensor_full_width * sensor_full_height * sizeof(uint16_t);
    egislog_d("%s, sensor_full_width %d sensor_full_height %d", __func__, sensor_full_width, sensor_full_height);
    if (bkg_size <= 0) {
        egislog_e("%s fail! bkg_size = %d", __func__, bkg_size);
        return EGIS_INCORRECT_STATUS;
    }
    if (g_is_bkm_initialized == TRUE) {
        egislog_d("%s already initialized", __func__);
        return EGIS_OK;
    }

    g_calib_bkg = plat_alloc(bkg_size);
    RBS_CHECK_IF_NULL(g_calib_bkg, EGIS_OUT_OF_MEMORY);

    // try to get current calib bkg and related info from sensor.
    // if it is not ready, set g_calib_bkg to 0
    int calib_bkg_size;
    isensor_get_buffer(PARAM_BUF_CALI_BKG_IMAGE, (unsigned char*)g_calib_bkg, &calib_bkg_size);
    if (calib_bkg_size == 0) {
        memset(g_calib_bkg, 0, bkg_size);
    }

#ifdef LA_BDS
    ret = labkg_check_pool(g_labkg_pool);
    if (ret != EGIS_OK) {
        ex_log(LOG_ERROR, "%s - check_img_pool failed %d", __func__, ret);
        // reset BDS
        memset(g_labkg_pool, 0, pool_size);
    }
    labkg_init_bkg_data(g_labkg_pool, g_calib_bkg, sensor_full_width, sensor_full_height);
#endif

    int exp_x10;
    isensor_get_int(PARAM_INT_CALI_BKG_EXPOSURE_TIME_X10, &exp_x10);
    g_calib_exp_time = (float)exp_x10 / 10;
    isensor_get_int(PARAM_INT_CALI_BKG_HW_INTEGRATE_COUNT, &g_calib_hw_int_count);
    // get hw int and expo from ini
    g_calib_hw_int_count =
        core_config_get_int(INI_SECTION_SENSOR, KEY_INTEGRATE_COUNT_HW, g_calib_hw_int_count);
    g_calib_exp_time =
        core_config_get_double(INI_SECTION_SENSOR, KEY_EXPOSURE_TIME, g_calib_exp_time);
    egislog_d("%s, g_calib_exp_time = %f, g_calib_hw_int_count = %d", __func__, g_calib_exp_time,
              g_calib_hw_int_count);

    g_calib_bkg_normalized = plat_alloc(bkg_size);
    if (g_calib_bkg_normalized == NULL) {
        PLAT_FREE(g_calib_bkg);
        return EGIS_OUT_OF_MEMORY;
    }
    memset(g_calib_bkg_normalized, 0, bkg_size);

    load_bds();
    isensor_set_calib_update_listener(on_calib_data_update);
    g_is_bkm_initialized = TRUE;
    return EGIS_OK;
}

int bkm_uninit() {
    egislog_e("%s", __func__);
    g_is_bkm_initialized = FALSE;
#if !defined(ALGO_GEN_4) && !defined(LA_BDS)
    if (bkimg_pool_is_initialized()) {
        bkimg_pool_uninit();
    }
#endif

#ifdef LA_BDS
    labkg_free_bkg_data();
    PLAT_FREE(g_labkg_pool);
#endif
    isensor_remove_calib_update_listener(on_calib_data_update);
    PLAT_FREE(g_bds_bkg);
    PLAT_FREE(g_calib_bkg);
    PLAT_FREE(g_calib_bkg_normalized);
    return EGIS_OK;
}

int bkm_save_bds() {
    egislog_d("%s", __func__);
    if (!g_is_bkm_initialized) {
        int ret = bkm_init();
        if (ret != EGIS_OK) {
            egislog_e("%s, bkm_init fail!!", __func__);
            return EGIS_INCORRECT_STATUS;
        }
    }
    int retval = EGIS_OK;
#if !defined(ALGO_GEN_4) && !defined(LA_BDS)
    if (bkimg_pool_is_initialized() == TRUE) {
        retval = bkimg_pool_save(g_bds_file_path);
#ifdef G3PLUS_MATCHER
        int use_scratch_mask = core_config_get_int(INI_SECTION_VERIFY, KEY_DETECT_SCRATCH_MASK,
                                                   INID_DETECT_SCRATCH_MASK);
        if (use_scratch_mask) {
            detect_scratch_save();
        }
#endif
    }
    return retval;
#endif
    return EGIS_OK;
}

int bkm_load_bds() {
    egislog_d("%s", __func__);
#if !defined(ALGO_GEN_4) && !defined(LA_BDS)
    if (!g_is_bkm_initialized) {
        int ret = bkm_init();
        if (ret != EGIS_OK) {
            egislog_e("%s, bkm_init fail!!", __func__);
            return EGIS_INCORRECT_STATUS;
        }
    }
    load_bds();
#endif    
    return EGIS_OK;
}

static void reset_calib_bkg() {
    if (!g_is_bkm_initialized) {
        int ret = bkm_init();
        if (ret != EGIS_OK) {
            egislog_e("%s, bkm_init fail!!", __func__);
            return;
        }
    }
    int sensor_full_width, sensor_full_height;
    isensor_get_sensor_full_size(&sensor_full_width, &sensor_full_height);
    int bkg_sz = sensor_full_width * sensor_full_height * sizeof(uint16_t);
    if (g_calib_bkg != NULL) {
        memset(g_calib_bkg, 0, bkg_sz);
    }
    if (g_calib_bkg_normalized != NULL) {
        memset(g_calib_bkg_normalized, 0, bkg_sz);
    }

    g_calib_hw_int_count =
        core_config_get_int(INI_SECTION_SENSOR, KEY_INTEGRATE_COUNT_HW, g_calib_hw_int_count);
    g_calib_exp_time =
        core_config_get_double(INI_SECTION_SENSOR, KEY_EXPOSURE_TIME, g_calib_exp_time);
}

static int bkm_on_defualt_bkg_removed() {
    egislog_d("%s", __func__);
    if (!g_is_bkm_initialized) {
        int ret = bkm_init();
        if (ret != EGIS_OK) {
            egislog_e("%s, bkm_init fail!!", __func__);
            return EGIS_INCORRECT_STATUS;
        }
    } else {
        reset_calib_bkg();
    }
    bkm_reset_bds();
    return EGIS_OK;
}

int bkm_reset_bds() {
    if (!g_is_bkm_initialized) {
        int ret = bkm_init();
        if (ret != EGIS_OK) {
            egislog_e("%s, bkm_init fail!!", __func__);
            return EGIS_INCORRECT_STATUS;
        }
    }
#if !defined(ALGO_GEN_4) && !defined(LA_BDS)
    egislog_i("%s", __func__);
    PLAT_FREE(g_bds_bkg);
    bkimg_pool_remove(g_bds_file_path);
    bkimg_pool_uninit();
    bkm_load_bds();
#endif
    return EGIS_OK;
}

int bkm_renew_bkg(uint16_t* raw16, int temperature, float exp_time, int hw_integrate_count) {
    if (!g_is_bkm_initialized) {
        int ret = bkm_init();
        if (ret != EGIS_OK) {
            egislog_e("%s, bkm_init fail!!", __func__);
            return EGIS_INCORRECT_STATUS;
        }
    }
#if !defined(ALGO_GEN_4) && !defined(LA_BDS)
    PLAT_FREE(g_bds_bkg);
    TIME_MEASURE_START(bkimg_pool_get);
    bkimg_pool_get(&g_bds_bkg, raw16, temperature, hw_integrate_count);
    TIME_MEASURE_STOP(bkimg_pool_get, "bkimg_pool_get = ");
#else
    ex_log(LOG_DEBUG, "%s not support bds", __func__);
#endif
    return EGIS_OK;
}

int bkm_update_bds(uint16_t* raw16, uint16_t* img_detect) {
    if (!g_is_bkm_initialized) {
        int ret = bkm_init();
        if (ret != EGIS_OK) {
            egislog_e("%s, bkm_init fail!!", __func__);
            return EGIS_INCORRECT_STATUS;
        }
    }
#if !defined(ALGO_GEN_4) && !defined(LA_BDS)
    int temperature;
    isensor_get_int(PARAM_INT_TEMPERATURE, &temperature);
    TIME_MEASURE_START(bkimg_pool_add);
    int ret = bkimg_pool_add(raw16, img_detect, temperature);

    TIME_MEASURE_STOP(bkimg_pool_add, "bkimg_pool_add = ");
    return ret;
#else
    ex_log(LOG_DEBUG, "%s not support bds", __func__);
    return EGIS_OK;
#endif
}

void bkm_set_folder_path(const char* folder_path) {
    if (folder_path != NULL) {
        egist_snprintf(g_bkg_folder_path, PATH_MAX, "%s", folder_path);
    }
    return;
}

int on_calib_data_update(uint16_t* calib_bkg, uint16_t* calib_wkg, int hw_int_count, float expo_time) {
    egislog_d("%s, bkg 0x%08x", __func__, calib_bkg);
    if (!g_is_bkm_initialized) {
        int ret = bkm_init();
        if (ret != EGIS_OK) {
            egislog_e("%s, bkm_init fail!!", __func__);
            return EGIS_INCORRECT_STATUS;
        }
    }
    if (calib_bkg == NULL && calib_wkg == NULL) {
        bkm_on_defualt_bkg_removed();
        return EGIS_OK;
    }

    g_calib_hw_int_count =
        core_config_get_int(INI_SECTION_SENSOR, KEY_INTEGRATE_COUNT_HW, hw_int_count);
    g_calib_exp_time = core_config_get_double(INI_SECTION_SENSOR, KEY_EXPOSURE_TIME, expo_time);
    int sensor_full_width, sensor_full_height;
    isensor_get_sensor_full_size(&sensor_full_width, &sensor_full_height);
    int bkg_sz = sensor_full_width * sensor_full_height * sizeof(uint16_t);
    memcpy(g_calib_bkg, calib_bkg, bkg_sz);

#ifdef LA_BDS
    labkg_gen_wbkg(calib_wkg, sensor_full_width, sensor_full_height, 1);
    labkg_gen_bbkg(calib_bkg, sensor_full_width, sensor_full_height, 1);
#endif

#if !defined(ALGO_GEN_4) && !defined(LA_BDS)
    bkm_load_bds();
#endif
    return EGIS_OK;
}
