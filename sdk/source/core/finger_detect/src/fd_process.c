#include "fd_process.h"
#include "common_definition.h"
#include "core_config.h"
#include "egis_definition.h"
#include "fd_process_16bit.h"
#include "image_analysis.h"
#include "image_cut.h"
#include "ipsys.h"
#include "isensor_api.h"
#include "plat_log.h"
#include "plat_mem.h"

#define LOG_TAG "RBS-EGISFP"

static BOOL g_fetch_single_image = FALSE;
static fn_fetch_image g_fn_fetch_image = isensor_get_dynamic_frame;
static fn_fetch_image_16bit g_fn_fetch_image_16bit = isensor_get_dynamic_frame_16bit;

static BOOL g_skip_process = FALSE;

void fd_set_fetch_image_fn(fn_fetch_image fetch_img, BOOL skip_process) {
    if (fetch_img != NULL) g_fn_fetch_image = fetch_img;

    g_skip_process = skip_process;
}

void fd_reset_fetch_image_fn() {
    g_fn_fetch_image = isensor_get_dynamic_frame;
}

void fd_set_fetch_single_image(BOOL single) {
    g_fetch_single_image = single;
}

BOOL fd_get_fetch_single_image() {
    return g_fetch_single_image;
}

static uint8_t* g_raw_img_buf = NULL;
static uint32_t g_raw_img_buf_size = 0;
static uint16_t g_raw_num = 0;
static BOOL g_use_ext_buf = FALSE;

static int g_sensor_width = 0;
static int g_sensor_height = 0;
static int g_raw_bpp = 8;

static int g_ipp_sensor_type;
static int g_keep_t_dummy;

int fd_process_create(uint8_t* ext_buf, uint32_t ext_buf_size, uint16_t num_image,
                      enum fd_process_option option) {
    int retval;

    g_ipp_sensor_type = core_config_get_int(INI_SECTION_SENSOR, KEY_IPP_SENSOR_TYPE, DEFAULT_IPP);
    g_keep_t_dummy = core_config_get_int(INI_SECTION_SENSOR, KEY_KEEP_T_DUMMY, 0);

    if (num_image <= 0) {
        egislog_e("wrong num_image %d", num_image);
        num_image = 1;
    }

    retval = isensor_get_sensor_roi_size(&g_sensor_width, &g_sensor_height);
    isensor_get_int(PARAM_INT_RAW_IMAGE_BPP, &g_raw_bpp);
    g_raw_img_buf_size = (uint32_t)(g_sensor_width * g_sensor_height * (g_raw_bpp / 8));
    if (retval != EGIS_OK || g_raw_img_buf_size == 0) {
        egislog_e("Failed to get sensor size");
        return EGIS_INCORRECT_PARAMETER;
    }
    if (ext_buf != NULL && ext_buf_size >= g_raw_img_buf_size && g_raw_bpp <= 8) {
        g_use_ext_buf = TRUE;
    } else {
        g_use_ext_buf = FALSE;
    }
    egislog_d("g_ipp_sensor_type=%d, use_ext_buf=%d", g_ipp_sensor_type, g_use_ext_buf);

    if (g_use_ext_buf) {
        g_raw_img_buf = ext_buf;
    } else {
        g_raw_img_buf = plat_alloc(g_raw_img_buf_size * num_image);
        if (ext_buf != NULL) {
            // egislog_e("ext_buf_size is too small. %d < %d", ext_buf_size, g_raw_img_buf_size);
        }
    }
    if (g_raw_img_buf == NULL) {
        egislog_e("%s EGIS_OUT_OF_MEMORY", __func__);
        return EGIS_OUT_OF_MEMORY;
    }
    g_raw_num = num_image;
    return EGIS_OK;
}

void fd_process_destroy() {
    if (!g_use_ext_buf) {
        PLAT_FREE(g_raw_img_buf);
    }
    g_raw_img_buf = NULL;
    g_raw_img_buf_size = 0;
    g_raw_num = 0;

    g_raw_bpp = 8;
    g_sensor_width = 0;
    g_sensor_height = 0;
    g_use_ext_buf = FALSE;
}

int process_image_in_place(enum proc_type_t proc_type, uint8_t* intensity, int in_width,
                           int in_height, int out_width, int out_height) {
    int i;
    int image_size;

    switch (proc_type) {
        case PROC_INVERT:
            image_size = in_width * in_height;
            for (i = 0; i < image_size; i++) {
                intensity[i] = 0xff - intensity[i];
            }
            break;
        case PROC_CROP:
            if (out_width == 0 || out_height == 0) break;
            if (out_width == in_width && out_height == in_height) break;

            if (out_width <= in_width && out_height <= in_height) {
                image_crop(intensity, in_width, in_height, out_width, out_height);
            }
            break;
        default:
            egislog_e("proc_type %d is not supported", proc_type);
            break;
    }

    return EGIS_OK;
}

static int get_average_image(int nFrame, unsigned char* images, int width, int height,
                             unsigned char* pAverimage) {
    int i = 0, j;
    int img_size = width * height;
    unsigned int pxl_sum = 0;
    unsigned char** pImage;

    if (images == NULL || pAverimage == NULL || nFrame <= 0) return EGIS_INCORRECT_PARAMETER;

    if (nFrame == 1) {
        memcpy(pAverimage, images, img_size);
        goto exit;
    }

    pImage = (unsigned char**)plat_alloc(nFrame * (sizeof(unsigned char*)));
    if (pImage == NULL) {
        memcpy(pAverimage, images, img_size);
        goto exit;
    }

    for (i = 0; i < nFrame; i++) {
        pImage[i] = images + i * img_size;
    }

    for (i = 0; i < img_size; i++) {
        pxl_sum = 0;
        for (j = 0; j < nFrame; j++) {
            pxl_sum += pImage[j][i];
        }
        pAverimage[i] = pxl_sum / nFrame;
    }

    plat_free(pImage);

exit:
    return EGIS_OK;
}

static int _preprocess_5xx(uint8_t* out_image, int out_width, int out_height, int option) {
    BYTE* p_bad_pixel = NULL;

    isensor_get_bad_pixels(&p_bad_pixel, NULL);

    if (g_raw_num > 1) {
        get_average_image(g_raw_num, g_raw_img_buf, g_sensor_width, g_sensor_height, out_image);
    } else if (!g_use_ext_buf) {
        memcpy(out_image, g_raw_img_buf, g_raw_img_buf_size);
    }
    egislog_d("%s, %d:%d %d:%d", __func__, g_sensor_width, g_sensor_height, out_width, out_height);

    IPbadpointfix(out_image, g_sensor_width, g_sensor_height, p_bad_pixel, 2);

    process_image_in_place(PROC_CROP, out_image, g_sensor_width, g_sensor_height, out_width,
                           out_height);
    process_image_in_place(PROC_INVERT, out_image, out_width, out_height, 0, 0);

    return EGIS_OK;
}

#define G3_IPP_OK 1

static int _preprocess_6xx(uint8_t* out_image, int out_width, int out_height, int option) {
#ifdef __ET6XX__
    int ipp_ret = G3_IPP_OK;
    egislog_d("%s, %d:%d %d:%d", __func__, g_sensor_width, g_sensor_height, out_width, out_height);
    int reconstruct = core_config_get_int(INI_SECTION_SENSOR, "RECONSTRUCT", 0);
    int waterBKG = core_config_get_int(INI_SECTION_SENSOR, "WATERBKG", 0);
    int et6xx_16bits_raw_image_temp[128 * 128];

    egislog_d("waterBKG=%d, reconstruct=%d", waterBKG, reconstruct);

    if (g_ipp_sensor_type == IPP_OFF) {
        minus_background((unsigned short*)g_raw_img_buf, g_sensor_width * g_sensor_height);
    }
    if (reconstruct) {
        BYTE* tmp_data = (BYTE*)plat_alloc(g_sensor_width * g_sensor_height);
        img16_normalize(et6xx_16bits_raw_image_temp, g_sensor_width, g_sensor_height, tmp_data);
        // raw16bitsTo8bits(frame_16bits_2, tmp_data, width, height);
        BYTE* tmp_img = IPreconstruct_image_int(tmp_data, out_width, out_height, 5);
        memcpy(out_image, tmp_img, out_width * out_height);
        PLAT_FREE(tmp_data);
        PLAT_FREE(tmp_img);
    } else if (g_ipp_sensor_type == IPP_OFF) {
        raw16bitsTo8bits((unsigned short*)g_raw_img_buf, out_image, out_width, out_height, 1);
        process_image_in_place(PROC_INVERT, out_image, out_width, out_height, 0, 0);
    } else {
        int* i32_img = (int*)plat_alloc(g_sensor_width * g_sensor_height * sizeof(int));
        int* img_bkg = NULL;
        const int pass_threshold = 10;
        egislog_d("pass_threshold=%d", pass_threshold);
        if (g_keep_t_dummy) {
            unsigned short* no_t_img = plat_alloc(out_width * out_height * sizeof(unsigned short));
            Remove_T_dummy((unsigned short*)g_raw_img_buf, g_sensor_width, g_sensor_height,
                           no_t_img);
            raw16bitsTo32(no_t_img, i32_img, out_width, out_height);
            PLAT_FREE(no_t_img);
        } else {
            raw16bitsTo32((unsigned short*)g_raw_img_buf, i32_img, g_sensor_width, g_sensor_height);
        }
        ipp_ret = IPP_sensor(g_ipp_sensor_type, i32_img, img_bkg, out_image, g_sensor_width,
                             g_sensor_height, 1, 200, 0, pass_threshold);
        process_image_in_place(PROC_CROP, out_image, g_sensor_width, g_sensor_height, out_width,
                               out_height);
        if (ipp_ret != G3_IPP_OK) {
            egislog_d("ipp_ret != G3_IPP_OK");
            raw16bitsTo8bits((unsigned short*)g_raw_img_buf, out_image, out_width, out_height,
                             pass_threshold);
        }
        PLAT_FREE(i32_img);
    }
#else
    egislog_e("%s, %d:%d %d:%d", __func__, g_sensor_width, g_sensor_height, out_width, out_height);
#endif
    return EGIS_OK;
}

int fd_acquire_image(uint8_t* out_image, int out_width, int out_height, int option) {
    int retval;
    egislog_d("%s, option %d", __func__, option);

    if (g_raw_bpp == 16) {
        retval = g_fn_fetch_image_16bit((unsigned short*)g_raw_img_buf, g_sensor_height,
                                        g_sensor_width, g_raw_num);
    } else {
        retval = g_fn_fetch_image(g_raw_img_buf, g_sensor_height, g_sensor_width, g_raw_num);
    }
    if (retval != EGIS_OK) {
        egislog_e("%s, fetch_image failed ret = %d ", __func__, retval);
        memset(g_raw_img_buf, 0, g_sensor_width * g_sensor_height);
        return retval;
    }

    if (g_skip_process) {
        egislog_d("skip pre-process");

        if (g_raw_img_buf != out_image) memcpy(out_image, g_raw_img_buf, out_width * out_height);

        return retval;
    }

    if (g_raw_bpp == 16) {
        retval = _preprocess_6xx(out_image, out_width, out_height, option);
    } else {
        retval = _preprocess_5xx(out_image, out_width, out_height, option);
    }

    return retval;
}

uint8_t* fd_get_current_raw_pointer(int* raw_bpp) {
    *raw_bpp = g_raw_bpp;
    return g_raw_img_buf;
}
