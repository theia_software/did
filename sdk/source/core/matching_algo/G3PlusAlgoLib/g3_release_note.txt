2019/06/03
[comments]:
- G3Plus_2.2.12.6
- rollback to qty 32 

2019/06/03
[comments]:
- G3Plus_2.2.12.5
- update speed-up code, now it sync to origin extraction result

2019/06/01
[comments]:
- G3Plus_2.2.12.4
- revert qty 32 to 16

2019/06/01
[comments]:
- G3Plus_2.2.12.3
- code optimize of extraction speed in circular extraction sensors
- modify border checking/setting

2019/05/31
[comments]:
- G3Plus_2.2.12.2
- Add debug log in g3_extract and g3_match

2019/05/31
[comments]:
- G3Plus_2.2.12.1
- Add QTY dynamic setting in global option

2019/05/30
[comments]:
- G3Plus_2.2.12.0
- Replace QTY checked mechanism to QTY32

2019/05/29
[comments]:
- G3Plus_2.2.11.6
- rollback to avoid extract crash

2019/05/29
[comments]:
- G3Plus_2.2.11.5
- add support of ET701 extraction
- code optimize of extraction speed in circular extraction sensors(713/711)

2019/05/27
[comments]:
- G3Plus_2.2.11.4
- add ET701 config

2019/05/24
[comments]:
- G3Plus_2.2.11.3
- Change the float and variable definition used in keypoint_relational_filtering()

2019/05/23
[comments]:
- G3Plus_2.2.11.2
- default enable keypoint_relation for keypoint relational filtering function

2019/05/22
[comments]:
- G3Plus_2.2.11.1
- change the atan2() used in function keypoint relational filtering to g3_atan2()

2019/05/22
[comments]:
- G3Plus_2.2.11.0
- add new function for g3 keypoint relational filtering

2019/05/19
[comments]:
- G3Plus_2.2.10.28
- add g3 learning back

2019/05/19
[comments]:
- G3Plus_2.2.10.27
- rollback g3 learning

2019/05/18
[comments]:
- G3Plus_2.2.10.26
- add new function for g3 learning (v1 + FIFO + 44 4) by different config (learning_method = 2)

2019/05/16
[comments]:
- G3Plus_2.2.10.24
- add new function for g3 learning by different config

2019/05/14
[comments]:
- G3Plus_2.2.10.23
- revise learning th = 800
- add new function for local distortion

2019/04/29
[comments]:
- G3Plus_2.2.10.22
- modify 713 min_hamming_distance to 123
- modify g3_modify_option() opt->min_hamming_distance threshold to 125

2019/04/28
[comments]:
- G3Plus_2.2.10.21
- modify 713 matching threshold to 710
- define low temp from T < 10 to T < 12

2019/04/25
[comments]:
- G3Plus_2.2.10.20
- fix error rot_offset_flag

2019/04/24
[comments]:
- G3Plus_2.2.10.19
- add not_rot_offset config for api_check_residue in API

2019/04/21
[comments]:
- G3Plus_2.2.10.18
- new config for false learning th

2019/04/20
[comments]:
- G3Plus_2.2.10.17
- new config for FA

2019/04/19
[comments]:
- G3Plus_2.2.10.16
- new config for FA

2019/04/19
[comments]:
- G3Plus_2.2.10.15
- update QTY table

2019/04/18
[comments]:
- G3Plus_2.2.10.14
- modify g3_rotate_and_shift_feature precise usage

2019/04/16
[comments]:
- G3Plus_2.2.10.13
- update QTY table ipp6

2019/04/16
[comments]:
- G3Plus_2.2.10.12
- do qty_check_function after image resampling 

2019/04/16
[comments]:
- G3Plus_2.2.10.11
- modify low temp minus one quarter minus_score

2019/04/11
[comments]:
- G3Plus_2.2.10.10
- modify ET713 learning threshold to 800

2019/04/10
[comments]:
- G3Plus_2.2.10.9
- modify: low-temp least score for vivo FA case

2019/04/10
[comments]:
- G3Plus_2.2.10.8
- modify: low-temp least score for marry FA case

2019/04/08
[comments]:
- G3Plus_2.2.10.7
- add new table VGG_WEIGHT_ET713_QTY_IPP6_740 in g3_table.c
- new define IPP6_740 for qty table (for IPP6) function of ml_qty_check_percentage in g3_extract.c

2019/04/02
[comments]:
- G3Plus_2.2.10.6
- modify low-temp least score & rotation tolerance

2019/04/01
[comments]:
- G3Plus_2.2.10.5
- modify low-temp least score & default max_normal_temp 28->32

2019/03/31
[comments]:
- G3Plus_2.2.10.4
- modify max normal_temp_feat control logic

2019/03/31
[comments]:
- G3Plus_2.2.10.3
- add max normal_temp_feat control in opt 

2019/03/27
[comments]:
- G3Plus_2.2.10.2
- update ML QTY table

2019/03/26
[comments]:
- G3Plus_2.2.10.1
- remove unnecessary test code

2019/03/24
[comments]:
- G3Plus_2.2.10.0
- modify g3_learning_plus check enroll size flow

2019/03/20
[comments]:
- G3Plus_2.2.9.12
- fix error extract temperature

2019/03/19
[comments]:
- G3Plus ver. 2.2.9.11
- modify scanning window 
  when scoring correlation select best rot, ox, oy
- fix enroll -dqe "always enroll one more feature bug"
- add modify_opt to g3_match s.t. test command can use mode 2

2019/03/18
[comments]:
- G3Plus ver. 2.2.9.10
- fix checking of remove_index

2019/03/18
[comments]:
- G3Plus ver. 2.2.9.9
- remove one DQE feat when enough low temp is learning
- adjust logic when checking temp-type enroll size

2019/03/18
[comments]:
- G3Plus ver. 2.2.9.8
- fix verify of low temp cannot match normal enroll features

2019/03/16
[comments]:
- G3Plus ver. 2.2.9.7
- fix DQE will enable second match bug

2019/03/16
[comments]:
- G3Plus ver. 2.2.9.6
- set feat->tmp = DQE_FEAT in DQE enroll
- g3_match_enroll_array skip while (verify->tmp < LOW_TEMP_TH && enroll->feat[i]->tmp != DQE_FEAT)
- score_correlation skip while (verify->tmp >= LOW_TEMP_TH && enroll->feat[i]->tmp == DQE_FEAT)

2019/03/05
[comments]:
- G3Plus ver. 2.2.9.2
- revise function name

2019/03/05
[comments]:
- G3Plus ver. 2.2.9.1
- modify g3_learning_plus, now it might return G3STS_LEARNING_FAIL

2019/03/05
[comments]:
- G3Plus ver. 2.2.9.0
remove feat->age
add feat->tmp(temperature) which is able to parse from 8bit image

2019/03/04
[comments]:
- G3Plus ver. 2.2.8.9
-re-organize code , add g3_modify_option. . . .
-add testq command 

2019/03/04
[comments]:
- G3Plus ver. 2.2.8.8
- update of MLQTY table . . . 

2019/03/02
[comments]:
- G3Plus ver. 2.2.8.7
- modify early out threshold to 1500

2019/03/01
[comments]:
- G3Plus ver. 2.2.8.6
- revise radius_ratio to 80

2019/02/27
[comments]:
- G3Plus ver. 2.2.8.5
- check mdl in g3_learning_plus() while redundant

2019/02/27
[comments]:
- G3Plus ver. 2.2.8.4
- decrease score when high QTY image's key point wrong match

2019/02/26
[comments]:
- G3Plus ver. 2.2.8.1
- fix redundant memory leak

2019/02/25
[comments]:
- G3Plus ver. 2.2.8.0
- add ARM_NEON_ASM

2019/02/24
[comments]:
- G3Plus ver. 2.2.7.14
- roll back to v2.2.7.12

2019/02/23
[comments]:
- G3Plus ver. 2.2.7.13
- diff_feature using static function

2019/02/23
[comments]:
- G3Plus ver. 2.2.7.12
- reset easy mode 2 method

2019/02/22
[comments]:
- G3Plus ver. 2.2.7.11
-ADD min_mask_count, default 20
-add enroll -dqe:n
-modify score_correation function 

2019/02/22
[comments]:
- G3Plus ver. 2.2.7.10
- add ENROLL_CTX_REDUNDANT_SINGLE_LEVEL
- remove ENROLL_CTX_REDUNDANT_IMAGE_POLICY setting
- add redundant_single_level config

2019/02/19
[comments]:
- G3Plus ver. 2.2.7.9
- using add_to_enroll_array to enroll DQE features
- fix DQE memory leak
- revise redundant_method 3

2019/02/18
[comments]:
- G3Plus ver. 2.2.7.8
- revise g3_enroll_add_feature_DQE

2019/02/18
[comments]:
- G3Plus ver. 2.2.7.7
- default redundant_method: 2
- vivo redundant_method: 3

2019/02/18
[comments]:
- G3Plus ver. 2.2.7.6
- extractcc fix cx,cy bug

2019/02/18
[comments]:
- G3Plus ver. 2.2.7.5
- fix cannot return redundant when ee->count <= opt->min_enroll_count
- add redundant_percent_level
- revise ALGO_CONFIG_MATCH_LOW_TEMP setting

2019/02/18
[comments]:
- G3Plus ver. 2.2.7.4
- add debug log for circle centre cx/cy

2019/02/18
[comments]:
- G3Plus ver. 2.2.7.3
- add append feat_DQE_list

2019/02/17
[comments]:
- G3Plus ver. 2.2.7.2
- add g3_enroll_add_feature_DQE

2019/02/17
[comments]:
- G3Plus ver. 2.2.7.1
- scoreing_method low_temp_mask turn off when enroll

2019/02/16
[comments]:
- G3Plus ver. 2.2.7.0
- nndr default 0
- code optimize

2019/02/16
[comments]:
- G3Plus ver. 2.2.6.9
- when low temperature, using set_algo_config to improve match

2019/02/15
[comments]:
- G3Plus ver. 2.2.6.8
- add scoreing_method low_temp_mask for low temp match

2019/02/15
[comments]:
- G3Plus ver. 2.2.6.7
- do cx/cy resample ratio from API to g3 extract

2019/02/14
[comments]:
- G3Plus ver. 2.2.6.6
- config cx, cy

2019/02/13
[comments]:
- G3Plus ver. 2.2.6.5
- modify easy mode threshold for v2.2.6.4 new qty weight

2019/02/12
[comments]:
- G3Plus ver. 2.2.6.4
- Update ET713 qty table

2019/02/09
[comments]:
- G3Plus ver. 2.2.6.3
- revise redundant level for match_percentage

2019/01/31
[comments]:
- G3Plus ver. 2.2.6.2
- Add DSP_HVX in count_hamming_distance()

2019/01/30
[comments]:
- G3Plus ver. 2.2.6.1
- add opt->smooth_window
- add opt->normalize_window 

2019/01/30
[comments]:
- G3Plus ver. 2.2.6.0
- add g3_enroll_duplicate_check()

2019/01/30
[comments]:
- G3Plus ver. 2.2.5.8
- reduce peak memory in enroll operation
- remove reduntant code

2019/01/29
[comments]:
- G3Plus ver. 2.2.5.7
- limit least_score upper bound

2019/01/29
[comments]:
- G3Plus ver. 2.2.5.4
- add MATCH_QTY_ENFORCE_MATCH_MASK for chamber high FR
- rollback VGG_WEIGHT_ET713_QTY

2019/01/29
[comments]:
- G3Plus ver. 2.2.5.3
- fix memory leak

2019/01/29
[comments]:
- G3Plus ver. 2.2.5.2
- reduce learning overhead

2019/01/29
[comments]:
- G3Plus ver. 2.2.5.1
- update of ML QTY table

2019/01/28
[comments]:
- G3Plus ver. 2.2.5.0
- add circle centre support during extration

2019/01/28
[comments]:
- G3Plus ver. 2.2.4.3
- modify count_match_percentage . . . . . . .
- modify dilation() instead of dialtion_complete()

2019/01/23
[comments]:
- G3Plus ver. 2.2.4.2
- update ML_QTY table

2019/01/23
[comments]:
- G3Plus ver. 2.2.4.1
- move VGG_WEIGHT_ET713_QTY[] table from IPP to g3_table.c
- update IPP lib

2019/01/23
[comments]:
- G3Plus ver. 2.2.4.0
- support QTY_EASY_MATCH
- fix bonus_match_percentage bug

2019/01/23
[comments]:
- G3Plus ver. 2.2.3.2
- revise threshold for decrease FA

2019/01/22
[comments]:
- G3Plus ver. 2.2.3.1
- add total score debug message at the end of _g3_match_enroll_array

2019/01/21
[comments]:
- G3Plus ver. 2.2.3.0
- Add scoring method 0x8 (able to combine with other score correlation method)

2019/01/21
[comments]:
- G3Plus ver. 2.2.2.3
- mark ocl_qty_score() temporarily due to CS1 mobicore platform will build fail

2019/01/21
[comments]:
- G3Plus ver. 2.2.2.2
- replace ocl qty by ml qty
- revise threshold which related with qty

2019/01/21
[comments]:
- G3Plus ver. 2.2.1.2
- implement bridge_group

2019/01/18
[comments]:
- G3Plus ver. 2.2.1.1
- fix opportunity of keep_feature_size 
- add match_percentage in redundant condition
- scoreing_method = 6

2019/01/18
[comments]:
- G3Plus ver. 2.2.1.0
- Remove scoring method 3
- Add scoring method 6 (least score bonus by qty)
- Add ML_qty support in extraction (default = ocl_qty)

2019/01/18
[comments]:
- G3Plus ver. 2.2.0.10
- support remove_enroll_item()
- add opt->confident_matching_count

2019/01/17
[comments]:
- G3Plus ver. 2.2.0.9
- change to normal config 

2019/01/16
[comments]:
- G3Plus ver. 2.2.0.8
- add ml_qty_check() into G3Plus

2019/01/16
[comments]:
- G3Plus ver. 2.2.0.7
- fix trust_tbl move row problem
- change default enroll_mode 0

2019/01/15
[comments]:
- G3Plus ver. 2.2.0.5
- bypass early_out in enroll/learning operation

2019/01/15
[comments]:
- G3Plus ver. 2.2.0.4
- fix learn to G3_ENROLL_MAX crash bug

2019/01/15
[comments]:
- G3Plus ver. 2.2.0.3
- add opt->early_out_threshold
- move new add learning template to array first position

2019/01/15
[comments]:
- G3Plus ver. 2.2.0.2
- add early out in _g3_match_enroll_array

2019/01/15
[comments]:
- G3Plus ver. 2.2.0.1
- modify trust_tbl

2019/01/14
[comments]:
- G3Plus ver. 2.2.0.0
- add trust_tbl to G3EnrollFeature structure

2019/01/14
[comments]:
- G3Plus ver. 2.1.3.4
- modify bridge_group

2019/01/11
[comments]:
- G3Plus ver. 2.1.3.3
- fix error handling of enroll crash issue

2019/01/11
[comments]:
- G3Plus ver. 2.1.3.2
- rewrite enroll mode 0

2019/01/11
[comments]:
- G3Plus ver. 2.1.3.1
- modify G3Features structure

2019/01/10
[comments]:
- G3Plus ver. 2.1.3.0
- add enroll redundant feature

2019/01/10
[comments]:
- G3Plus ver. 2.1.2.0
- implement bridge_group
- add G3MatchGroupDetail struct in mdl

2019/01/10
[comments]:
- G3Plus ver. 2.1.1.2
- fix enroll mode 0, add fine_match data into enroll problem

2019/01/09
[comments]:
- G3Plus ver. 2.1.1.1
- modify enroll_mode 0 learning bug
- update enrol base rotate/offset

2019/01/09
[comments]:
- G3Plus ver. 2.1.1.0
- support enroll_mode 0
- add group_count to G3EnrollFeature

2019/01/08
[comments]:
- G3Plus ver. 2.1.0.14
- rename isqrt to g3_isqrt

2019/01/08
[comments]:
- G3Plus ver. 2.1.0.13
- score correlation

2019/01/07
[comments]:
- G3Plus ver. 2.1.0.12
- code re-organize
- change opt->offset_threshold 9

2019/01/07
[comments]:
- G3Plus ver. 2.1.0.11
- fix enroll map group bug

2019/01/05
[comments]:
- G3Plus ver. 2.1.0.10
- Update ocl_qty_score()

2019/01/04
[comments]:
- G3Plus ver. 2.1.0.9
- revise scoreing_method 2

2019/01/03
[comments]:
- G3Plus ver. 2.1.0.8
- add rotation_method
- add G3Sqrt255Value

2019/01/02
[comments]:
- G3Plus ver. 2.1.0.7
- remove unnecessary calculation

2019/01/02
[comments]:
- G3Plus ver. 2.1.0.6
- Add function for new learning strategy: config 46 (default 0=no change)

2018/12/29
[comments]:
- G3Plus ver. 2.1.0.5
- add g_qty_dyth flag base on OCL qty method
- add low_matching_threshold, high_matching_threshold config

2018/12/28
[comments]:
- G3Plus ver. 2.1.0.4
- add features qty based on OCL method

2018/12/28
[comments]:
- G3Plus ver. 2.1.0.3
- rename g3_enroll_array() to g3_enroll_plus()
- increase features group ref while enroll array matched

2018/12/28
[comments]:
- G3Plus ver. 2.1.0.2
- map info use group to fill index instead qty

2018/12/27
[comments]:
- G3Plus ver. 2.1.0.1
- fix g3_decompress_feature() features ref
- map info use group to fill index instead qty

2018/12/27
[comments]:
- G3Plus ver. 2.1.0.0
- update ver. for G3features structure

2018/12/27
[comments]:
- G3Plus ver. 2.0.0.6
- add group, ref in G3features group

2018/12/25
[comments]:
- G3Plus ver. 2.0.0.5
- add scoreing_method

2018/12/25
[comments]:
- G3Plus ver. 2.0.0.4
- add map method when enroll and learning
- enable by setting enroll_mode = 3

2018/12/25
[comments]:
- G3Plus ver. 2.0.0.3
- increase precision of radius_ratio

2018/12/24
[comments]:
- G3Plus ver. 2.0.0.2
- fix extract_db_name bug
- if fine-match ==2 , update descriptor
- implement DIV_PRECISE

2018/12/23
[comments]:
- G3Plus ver. 2.0.0.1
- enable pine_match
- G3_PRECISE_BIT 2 

2018/09/10
[comments]:
- ver. 1.4.3.6
- revise opportunities of using increase_matched_ref() for sync API flow

2018/07/25
[comments]:
- ver. 1.4.2.6
- revise g3_free_enroll_environment
- add some of free function and remove clean_flag

2018/07/04
[comments]:
- ver. 1.4.1.4
- fix 523 easy mode config

2018/07/04
[comments]:
- ver. 1.4.1.3
- add 523 easy mode config

2018/06/26
[comments]:
- ver. 1.4.1.2
- fix set config error

2018/06/26
[comments]:
- ver. 1.4.1.1
- add easy_mode config
- remove G3_ATAN128X8, G3ComputeOrientation
- revise build error for "_inline"

2018/6/25
[comments]:
- ver. 1.4.1.0
- update choose rotation algorithm

2018/06/14
[comments]:
- ver. 1.4.0.0
- support fine_match operation

2018/06/07
[comments]:
- ver. 1.3.2.0
- compute atan2 into ang_tbl in main_angle function

2018/05/23
[comments]:
- ver. 1.3.1.3
- fix error of last version

2018/05/23
[comments]:
- ver. 1.3.1.2
- delete some typedef
- INT replaced by int, BOOL replaced by int

2018/05/15
[comments]:
- ver. 1.3.1.1
- revise ET523 sensor_config

2018/05/04
[comments]:
- ver. 1.3.0.0
-support g3_compress_enroll_feature_multiple_step()

2018/04/30
[comments]:
- ver. 1.2.9.2
- support multiple_angle option

2018/04/17
[comments]:
- ver. 1.2.8.0
- fix main angle bug
- reduce _EMBEDDED extract peak memory
- add opt->free_image
2018/03/19
[comments]:
- g3 release ver. 1.2.7.12
- add g3_fix_pattern_finder()

2018/03/07
[comments]:
- g3 release ver. 1.2.7.9
- revise condition _WINDOWS to WIN32

2018/03/01
[comments]:
- g3 release ver. 1.2.7.7
- fix memory leak in g3api_append_image_into_template
- change malloc and free to plat_alloc and plat_free on ETS non-tz platform

2018/02/22
[comments]:
- g3 release ver. 1.2.7.3
- add more error handle in case of out of memory

2018/02/10
[comments]:
- g3 release ver. 1.2.7.0
- add memmax command to limit maximum allocated memory 
- add more error handling in case of out of memory
- _EMBEDDED version work fine under 192 KB

2018/02/07
[comments]:
- g3 release ver. 1.2.6.3
- Modify risky functions handling
- revise g3_feature_area_length

2018/01/12
[comments]:
- modify version G3_MATCHER_VER "1.2.5.2"
- modify error handling 

2018/01/11
[comments]:
- release version 1.2.5.1
- modify error handling in decompress enroll feature 
- add count matching coverage to reduce the learning rate in opt
-- ET510 learning coverage default set to 85 
-- other Egis sensor keep 100

2018/01/10
[comments]:
G3 Kernel 1.2.5.0:
1. Add learning percentage control
2. Error handling for unexpected compress/decompress path
3. Change opencv related functions from _DEBUG to RD_TOOL degine

2018/01/08
[comments]:
- G3_MATCHER_VER "1.2.4.1"
- Modify ET516 noM learning threshold to avoid FA

2018/01/04
[comments]:
-release version 1.2.4.0
-fully support _EMBEDDED condition for extract/enroll/match/learning under 192 KB

2017/12/15
[comments]:
-release version 1.2.2.0
-code optimize for match speed & memory consumption
-code re-organize

2017/11/29
[comments]:
release version 1.2.1.3
-remove useless option fields
-reduce g3_extract memory consumption
-code re-organize

2017/11/24
[comments]:
release version 1.2.1.1
-support _EMBEDDED build option
-reduce g3_extract memory consumption
-improve extract time 3%

2017/11/22
---------------------------------------------
[comments]:
release version 1.2.0.3
-reduce g3_extract memory consumption
-no change anything

2017/11/20
---------------------------------------------
[comments]:
release version 1.2.0.1
- use variation to count threshold of count_percentage
- add init_len in ee structure
- too less feature size will not be enroll
- add option of ipp_method for 516 M_Logo

2017/11/08
---------------------------------------------
[comments]:
release version 1.2.0.0
-support enroll_mode(default off)
-support join filtering(default off)

2017/10/18
---------------------------------------------
[comments]:
release version 1.1.4.1
-add intensity_normalize feature for 516 M logo
-background_subtraction be replaced by intensity_normalize for 516 M logo

2017/10/17
---------------------------------------------
[comments]:
release version 1.1.4.0
-new feature : redundant method 2
redundant_method = 0 : without redundant
redundant_method = 1 : compare with last non-redundant features
redundant_method = 2 : compare with whole enroll template (configurable redundant_threshold)

2017/10/11
---------------------------------------------
[comments]:
release version 1.1.3.1
-add g3_feature_area_length, which return maximum x or y length of area
-rename from g3_enroll_feat_area to g3_feature_area
-use features of maximum size to calculate area length

2017/10/06
---------------------------------------------
[comments]:
release version 1.1.3.0
-new feature : quick match
-revise attachment name of "output_test_match_html".
-remove unused code of dilation_complete.
-include neon header in img_util.c


2017/09/29
---------------------------------------------
[comments]:
release version 1.1.2.4
-revise function name of dilation_complete

2017/09/29
---------------------------------------------
[comments]:
release version 1.1.2.3
-change return of g3_enroll_feat_area
-Ets tz use plat_alloc & plat_free instead of sys_alloc & sys_free
-fix g_match bug when inserted feature size equal to 0
-free opt when program is over


2017/09/28
---------------------------------------------
[comments]:
release version 1.1.2.2
-add g3_enroll_feat_area function, which calculate template area
-add Dilation function

2017/09/27
---------------------------------------------
[comments]:
release version 1.1.2.1
-add background_subtraction feature for 516 M logo
-new opt variable ipp_method to enable background_subtraction
-Ets log include file ets_egis_log.h

2017/09/25
---------------------------------------------
[comments]:
release version 1.1.2.0
-change redundant policy to "check previous one only".
-remove unused variables about redundant.
-replace "img_height & img_width in opt" with  "height & width in ee".
-new opt variable "min_matching_count" to configure "g3_match" condition.

2017/09/20
---------------------------------------------
[comments]:
release version 1.1.1.0
-extract with partial method
-resample before partial & normalization
-correct g3_remove_revoked_feature bug
-correct add_to_matched_list bug
-prioritize enroll feature
-improve partial method
-memory dump recognizes system environment

2017/09/10
---------------------------------------------
[comments]:
release version 1.1.0.0
-remove type check in feat_is_near_new()
-modify score counting
-modify learning policy
-modify Div2E* to speed up calculate
-modify config setting

2017/09/08
---------------------------------------------
[comments]:
release version 1.0.0.0
G3 kernel Version 0.9.4.2
-When nbl=3, lth_bound threshold is "1.5 x matching_threshold"
-Change data type of "ref" in feature. From char to SBYTE
-Add "qty" in features structure, 
-Increase "min_enroll_count" to 12
-In g3_match, filter match pair with type

2017/09/06
---------------------------------------------
[comments]:
G3 kernel Version 0.9.4.1
-optimize config parameters
-template feature after enroll multiplied by 4
-restructure g3_match_enroll

2017/08/31
---------------------------------------------
[comments]:
G3 kernel Version 0.9.4.0
1.	Modify enroll flow to reduce memory consumption
2.	Change g3_rotate_feature_f(float) to g3_rotate_feature_new10(int)

2017/08/29
---------------------------------------------
[comments]:
-version 0.9.3.29
-Correct "fabs" ,that only supported in Windows, to "G3Abs"

2017/08/28
---------------------------------------------
[comments]:
-version 0.9.3.28
-Base on 0.9.3.25
-If find_fa without parameter, excuted in root
-new nndr function
-rotate function with float number

2017/08/28
---------------------------------------------
[comments]:
-version 0.9.3.27
-If find_fa without parameter, excuted in root
-new nndr function
-rotate function with float number

2017/08/24
---------------------------------------------
[comments]:
-version 0.9.3.26
-Modify enroll flow to reduce memory consumption
-Modify partial-image mask method
-Fix a bug of ¡§rotation-angle > 360¡¨

2017/08/24
---------------------------------------------
[comments]:
-version 0.9.3.25
add memory dump for CS1

2017/08/24
---------------------------------------------
[comments]:
-version 0.9.3.24

2017/08/24
---------------------------------------------
[comments]:
-version 0.9.3.24
when user abort call g3_free_enroll_environment instead of g3_enroll_final

2017/08/22
---------------------------------------------
[comments]:
-version 0.9.3.23
-add new command "test", "test_one_to_all" and "test_all_to_all" which can output csv and html result
-revise command "fa" to "find_fa"
-revise command "ana" which required parameters

2017/08/18
---------------------------------------------
[comments]:
-version 0.9.3.22
-add new command "fa" to analyze where FA came from

2017/08/17
---------------------------------------------
[comments]:
-version 0.9.3.21
-fix bug of diff_feature

2017/08/16
---------------------------------------------
[comments]:
-version 0.9.3.20
-remove compiler warning

2017/08/15
---------------------------------------------
[comments]:
-version 0.9.3.19
-set default options to be optimized
-change nndr comparation radio from division to multiplication

2017/08/14
---------------------------------------------
[comments]:
-version 0.9.3.18
-add "normalize" command to config normalize method
-add "percent" command to config partial method
-logcsv records percentage of match image

2017/08/14
---------------------------------------------
[comments]:
-version 0.9.3.17
-add partial_method (default 0, do nothing)
-implement partial_method = 1

2017/08/11
---------------------------------------------
[comments]:
-version 0.9.3.15
-modify redundant check 
-correct the build error in ndk-build
-modify analysis report 
-change template format
-add percentage to features structure

2017/08/10
---------------------------------------------
[comments]:
-version 0.9.3.14
-correct situation of call output_result() 
-correct g_db_root unint
-revise max_redundant_count
-add version comments of files

2017/08/10
---------------------------------------------
[comments]:
-version 0.9.3.13
remove #include <stdio.h> and add spreadtrum's makefile

2017/08/08
---------------------------------------------
[comments]:
-version 0.9.3.12
-fix warning in delet_str
-rename functions that reveal kernel information
-correct variables' name in output_result
-large g_db_root buffer
[reviewer]:N/A

2017/08/07
---------------------------------------------
[comments]:
-version 0.9.3.11
-correct bug when build EV tool
[reviewer]:N/A

2017/08/07
---------------------------------------------
[comments]:
-version 0.9.3.10
-revise process of output result.csv
[reviewer]:N/A

2017/08/07
---------------------------------------------
[comments]:
-version 0.9.3.9
-set default config to optimized value
[reviewer]:N/A

2017/08/04
---------------------------------------------
[comments]:
-version 0.9.3.8
-output FA/FR report to result.csv
[reviewer]:N/A

2017/08/04
---------------------------------------------
-version 0.9.3.7
-optimize g3_keep_feature_size, compute_main_orientation (rename as compute_feature_angle)
-initialize G3_ENROLL_FEATURE_HEADER_LEN to "0"
-remove g3_write_enroll_feature in matching_callback. To avoid different result between two continual match

2017/08/03
---------------------------------------------
version 0.9.3.6 (test, no commit)
-reverse "NEON hamming distance function optimizatiom", that reduce variable assignment,  for speed test.

2017/08/02
---------------------------------------------
-version 0.9.3.5
-correct hamming_distance 64bit (rename as compute_feature_similarity)

2017/08/01
---------------------------------------------
-version 0.9.3.4
-correct g3_keep_feature_size dead lock
-optimize hamming distance & auto-recognize system