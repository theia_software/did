//
// Created by Ren yanbin on 2018/10/22.
//
#pragma once
#include <stddef.h>
#include <stdint.h>
#include "EgisAlgorithmApiV2.h"

#ifdef __cplusplus
extern "C" {
#endif

// param for enroll context
#define ENROLL_CTX_MIN_ENROLL_COUNT 1214
#define ENROLL_CTX_MAX_ENROLL_COUNT 1215
#define ENROLL_CTX_REDUNDANT_CHECK_START 1216
#define ENROLL_CTX_REDUNDANT_CONTINUOUS_BOUND 1217
#define ENROLL_CTX_EXTRACT_THRESHOLD 1221
#define ENROLL_CTX_GENERALIZATION_THRESHOLD 1223
#define ENROLL_CTX_DUPLICATE_THRESHOLD 1226
#define ENROLL_CTX_REDUNDANT_INPUT_THRESHOLD 1227
#define ENROLL_CTX_REDUNDANT_THRESHOLD 1228
#define ENROLL_CTX_REDUNDANT_IMAGE_POLICY 1229
#define ENROLL_CTX_REDUNDANT_LEVEL 1230
#define ENROLL_CTX_QUALITY_REJECT_LEVEL 1231

// enroll context information
#define ENROLL_CTX_ENROLL_PROGRESS 1251
#define ENROLL_CTX_PRE_IDX 1252
#define ENROLL_CTX_ENROLLED_COUNT 1254
// set ENROLL_CTX_REDUNDANT_IMAGE_POLICY
#define FP_REDUNDANT_POLICY_REJECT_IMAGE 0  // default
#define FP_REDUNDANT_POLICY_ACCPET_IMAGE 1

enum AlgoAPIDeviceType {
    FP_ALGOAPI_MODE_UNKNOWN_0,
    FP_ALGOAPI_MODE_EGIS_ET300,
    FP_ALGOAPI_MODE_EGIS_ET310,
    FP_ALGOAPI_MODE_EGIS_ET320,
    FP_ALGOAPI_MODE_EGIS_ET505,
    FP_ALGOAPI_MODE_EGIS_ET510,
    FP_ALGOAPI_MODE_EGIS_ET511,
    FP_ALGOAPI_MODE_EGIS_ET516,
    FP_ALGOAPI_MODE_EGIS_ET516_MLOGO,
    FP_ALGOAPI_MODE_EGIS_ET512,
    FP_ALGOAPI_MODE_EGIS_ET519,
    FP_ALGOAPI_MODE_EGIS_ET702_,
    FP_ALGOAPI_MODE_EGIS_ET517,
    FP_ALGOAPI_MODE_EGIS_ET518,
    FP_ALGOAPI_MODE_EGIS_ET538,
    FP_ALGOAPI_MODE_SUPREMA1,
    FP_ALGOAPI_MODE_EGIS_ET725,
    FP_ALGOAPI_MODE_EGIS_ET600,
    FP_ALGOAPI_MODE_EGIS_ET601,
    FP_ALGOAPI_MODE_EGIS_ET520,
    FP_ALGOAPI_MODE_EGIS_ET727,
    FP_ALGOAPI_MODE_EGIS_ET523,
    FP_ALGOAPI_MODE_EGIS_ET522,
    FP_ALGOAPI_MODE_EGIS_ET616,
    FP_ALGOAPI_MODE_EGIS_ET613,
    FP_ALGOAPI_MODE_EGIS_ET516_M0,
    FP_ALGOAPI_MODE_EGIS_ET520_M0,
    FP_ALGOAPI_MODE_EGIS_ET711,
    FP_ALGOAPI_MODE_EGIS_ET713,
    FP_ALGOAPI_MODE_EGIS_ET701_,
    FP_ALGOAPI_MODE_EGIS_ET715,
    FP_ALGOAPI_MODE_EGIS_ET719,
    FP_ALGOAPI_MODE_EGIS_EL720A,	
    FP_ALGOAPI_MODE_EGIS_ET901_,
    FP_ALGOAPI_MODE_EGIS_ET760_,
};

#define MAX_ENROLL_FINGERS 5

#define FP_ERR -1000
#define FP_TIMEOUT -1001
#define FP_BADIMAGE -1002
#define FP_FEATURELOW -1003
#define FP_DUPLICATE -1004
#define FP_NOTDUPLICATE -1005
//#define FP_MATCHFAIL                -1006
#define FP_NULL_DATA -1007
#define FP_INVALID_FEATURE_LEN -1008
#define FP_ALLOC_MEM_FAIL -1009
#define FP_INVALID_FORMAT -1010
#define FP_TUNING_FAIL -1011
#define FP_INVALID_BUFFER_SIZE -1012
#define FP_IO_ACCESS_FAIL -1013
#define FP_NOT_TUNING -1014
#define FP_CONNECT_SENSOR_FAIL -1015
#define FP_CALL_FAILED -1016
#define FP_NOT_CONNECT_SENSOR -1017
#define FP_NULL_ENROLL_DATA -1018
#define FP_NULL_FEATURE -1019
#define FP_GETIMAGEFAIL -1020
#define FP_DECRYPT_FAIL -1021
#define FP_STATE_ERR -1022
#define FP_INVALID_SENSOR_MODE -1023
#define FP_VERIFY_DATA_ERROR -1024
#define FP_TEMPLATE_CRC_ERROR -1025
#define FP_SET_EASY_MODE_ERROR -1026
#define FP_DECOMPRESS_FAIL -1027
#define FP_INVALID_PARAMETER -1028
#define FP_INVALID_FINGER_NUM -1029
#define FP_FEATURE_LEN_OVER_SIZE -1030
#define FP_RESIDUAL_DETECT -1031

#define FP_PARAMETER_NOT_VALID -1888
#define FP_PARAMETER_UNACCEPTABLE -1889

#define FP_ALGOAPI_GET_VERSION FP_OP_GET_VERSION_V2

// FP_MERGE_ENROLL_ERROR_CODE
#define FP_MERGE_ENROLL_IAMGE_OK 1
#define FP_MERGE_ENROLL_FINISH 2
#define FP_MERGE_ENROLL_IMAGE_HIGHLY_SIMILARITY 3
#define FP_MERGE_ENROLL_REDUNDANT_ACCEPT 4
#define FP_MERGE_ENROLL_PROCESSING 5
#define FP_MERGE_ENROLL_NON_ADD 6
#define FP_MERGE_ENROLL_OFFSET_REDUNDANT_INPUT 7
#define FP_MERGE_ENROLL_BAD_IMAGE -1
#define FP_MERGE_ENROLL_FEATURE_LOW -2
#define FP_MERGE_ENROLL_FAIL -3
#define FP_MERGE_ENROLL_OUT_OF_MEMORY -4
#define FP_MERGE_ENROLL_UNKNOWN_FAIL -5
#define FP_MERGE_ENROLL_IRREGULAR_CONTEXT -6
#define FP_MERGE_ENROLL_DUPLICATE_IMAGE_REMOVED -7
#define FP_MERGE_ENROLL_REDUNDANT_INPUT -8
#define FP_MERGE_ENROLL_TOO_FAST -9
#define FP_MERGE_ENROLL_LOW_QTY -10
#define FP_MERGE_ENROLL_REJECT_PARTIAL -11
#define FP_MERGE_ENROLL_REJECT_FAKE -12
#define FP_MERGE_ENROLL_REJECT_SIMPLE_PATTERN -13

#define MAX_TEMPLATE_SIZE 1000 * 1024

#define MAXCNT_IMAGE_TYPE_LEARNING_QUEUE 2

#define MIN_ENROLL_COUNT 12

#define MAX_FEATURE_LEN 0
#define MAX_ENROLL_TEMPLATE_LEN MAX_TEMPLATE_SIZE


enum egis_image_class_type {
    //FP_IMAGE_TYPE_NORMAL = 0x0,
    //FP_IMAGE_TYPE_ENROLL = 0xf,
    //FP_IMAGE_TYPE_DRY    = 0x10,
    FP_IMAGE_TYPE_LEARNING_QUEUE = 0x11,
};

enum FPExtractResult {
    exOK,
    exFeatureLow,
    exBadImage,
    exNotImplement,
    exImageTooSmall,
    exEncryptFail,
    exFail,
    exParital,
    exFakeFinger,
};

#define ENROLL_CTX_REDUNDANT_METHOD 1231
// set circle centre
#define ENROLL_CTX_SET_CIRCLE_CENTRE_CX 1260
#define ENROLL_CTX_SET_CIRCLE_CENTRE_CY 1261

typedef enum {
    ALGO_PARAM_TYPE_MODEL = 0,
    ALGO_PARAM_TYPE_HEAP = 1,
    ALGO_PARAM_TYPE_BASE_IMG_1 = 2,
    ALGO_PARAM_TYPE_RETRY_NUM = 3,
    ALGO_PARAM_TYPE_SYS_TEMP_X10 = 4,
    ALGO_PARAM_TYPE_HEAP_SIZE = 5,
    ALGO_PARAM_TYPE_UPGRADE_ENROLL = 6,
    ALGO_PARAM_TYPE_TEMPLATE_VERSION = 7,
    ALGO_PARAM_TYPE_HW_INTEGRATE = 8,
} algo_param_type_t;

typedef struct _verify_init_ {
    unsigned char** pEnroll_temp_array;
    int* enroll_temp_size_array;
    int enroll_temp_number;
    // int*    bad_enroll_number_array;
} VERIFY_INIT;

typedef struct _verify_info_ {
    unsigned char* pFeat;
    int feat_size;
    unsigned char* pEnroll_temp;
    int enroll_temp_size;
    int matchScore;
    int matchindex;
    int isLearningUpdate;
    int extract_ret;
    int target_template;
    int learning_mode;
    int try_count;
    int* match_score_array;
    int final_try;
    int static_pattern_area;
    int is_image_from_queue;
} VERIFY_INFO;

typedef struct _algo_api_info_ {
    char algo_api_version[30];
    char matchlib_version[30];
} ALGO_API_INFO;

int algo_set_buffer(algo_param_type_t param_type, unsigned char* in_buffer, int in_buffer_size);
int algo_set_int(algo_param_type_t param_type, int val);
int algo_get_int(algo_param_type_t param_type, int* val);

int algorithm_initialization_by_sensor(unsigned char* decision_data, int decision_data_len,
                                       int sensor_type, int reset_flag);

void algorithm_uninitialization(unsigned char* decision_data, int decision_data_len);

void* enroll_init(void);

int set_enroll_context(void* ctx, int param, int value);

int get_enroll_context(void* ctx, int param, int* value);

int enroll_setDB(void* ctx, VERIFY_INIT* verify_init_data);

int enroll(unsigned char* img, int width, int height, void* e_ctx, int* percentage);

unsigned char* get_enroll_template(void* ctx, int* len);

int enroll_uninit(void* e_ctx);

int verify_init(VERIFY_INIT* verify_info);

int extract_feature(unsigned char* img, int width, int height, unsigned char* feat, int* len,
                    int* quality);

int verify(VERIFY_INFO* verify_info);

int learning(VERIFY_INFO* verify_info, int matchidx);

int verify_uninit(void);

int algorithm_do_other(int opcode, unsigned char* indata, unsigned char* outdata);

int set_algo_config(int param, int value);

int set_algorithm_config(int param, int value);

void pb_free(void* ptr);

char* pb_malloc(uint32_t size);

int get_fingerscore(unsigned char* image, int width, int height);

#ifdef __cplusplus
}
#endif
