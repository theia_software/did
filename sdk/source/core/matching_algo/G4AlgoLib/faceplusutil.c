#include <stdio.h>
#include "EgisAlgorithmAPI.h"
#include "core_config.h"
#include "egis_definition.h"
#include "fp_custom.h"
#include "fp_definition.h"
#include "ini_definition.h"
#include "isensor_api.h"
#include "plat_file.h"
#include "plat_log.h"
#include "plat_mem.h"
#include "plat_time.h"
//#include "faceplusutil.h"

#define FILENAME_DEBASE_CTX FILE_DATA_BASE "debase_context"

extern int g_raw_bpp;
#ifdef __TRUSTONIC__
uint8_t* g_debase_db = NULL;
int g_debase_size = (200 * 200 * sizeof(uint16_t) * 10 + 64);
#endif

static BOOL g_is_algo_init = FALSE;
static char* g_heap_addr = NULL;
static int g_heap_size = 0;
int set_algorithm_config(int param, int value) {
    ex_log(LOG_DEBUG, "set_algorithm_config");
    return EGIS_OK;
}

int algorithm_heap_init_for_faceplus(void) {
    ex_log(LOG_DEBUG, "%s ", __func__);
    if (g_heap_addr != NULL) {
        return FP_LIB_OK;
    }
    int heap_size;
    char* heap_addr;
    algo_get_int(ALGO_PARAM_TYPE_HEAP_SIZE, &heap_size);
    ex_log(LOG_DEBUG, "%s, heap size %d bytes", __func__, heap_size);
    heap_addr = (char*)plat_alloc(heap_size);
    RBS_CHECK_IF_NULL(heap_addr, EGIS_OUT_OF_MEMORY);
    g_heap_size = heap_size;
    g_heap_addr = heap_addr;
    return FP_LIB_OK;
}

int algorithm_initialization_for_faceplus(void) {
    ex_log(LOG_DEBUG, "%s g_is_algo_init=%d", __func__, g_is_algo_init);
    if (g_is_algo_init == TRUE) {
        return FP_LIB_OK;
    }
    int ret;
    ret = algorithm_heap_init_for_faceplus();
    if (ret != FP_LIB_OK) {
        ex_log(LOG_ERROR, "%s algorithm_heap_init failed", __func__);
        return FP_LIB_ERROR_MEMORY;
    }

    int frame_sz = 0, width = 0, height = 0;
    int bkg_img_sz = 0;
    int byte_per_pixel = g_raw_bpp / 8;
    uint16_t* bkg_img = NULL;
#ifdef __TRUSTONIC__
    debase_init();
#endif
    isensor_get_sensor_roi_size(&width, &height);
    if (width <= 0 || height <= 0) {
        ex_log(LOG_ERROR, "parameter error");
        return EGIS_COMMAND_FAIL;
    }
    ex_log(LOG_DEBUG, "width = %d, height = %d", width, height);

    frame_sz = width * height;
    bkg_img_sz = frame_sz * byte_per_pixel;
    bkg_img = plat_alloc(bkg_img_sz);
    ex_log(LOG_DEBUG, "bkg_img_sz = %d", bkg_img_sz);

    // int bkg_use_option = core_config_get_int(INI_SECTION_SENSOR, KEY_BKG_IMG_USE_OPTION, 1);
    int bkg_use_option = 0;
    isensor_get_buffer(
        bkg_use_option > 0 ? PARAM_BUF_BDS_BKG_IMAGE : PARAM_BUF_CALI_NORMALIZE_BKG_IMAGE,
        (uint8_t*)bkg_img, &bkg_img_sz);
    if (bkg_img_sz == 0) {
        ex_log(LOG_ERROR, "calibration bkg_data not exist!");
        PLAT_FREE(bkg_img);
        return EGIS_COMMAND_FAIL;
    }

    // set model and heap
    ex_log(LOG_DEBUG, "ALGO_PARAM_TYPE_HEAP heap heap_addr = %p, heap_size = %d", g_heap_addr,
           g_heap_size);
    algo_set_buffer(ALGO_PARAM_TYPE_HEAP, (unsigned char*)g_heap_addr, g_heap_size);
    ex_log(LOG_DEBUG, "base base_data = %p, calibration_len = %d", bkg_img, bkg_img_sz);
    algo_set_buffer(ALGO_PARAM_TYPE_BASE_IMG_1, (unsigned char*)bkg_img, bkg_img_sz);

    ret = algorithm_initialization(NULL, 0, 0);
    if (ret != EGIS_OK) {
        ex_log(LOG_ERROR, "algorithm_initialization ret:%d", ret);
        return ret;
    }
    ALGO_API_INFO algo_api_version;
    algorithm_do_other(FP_ALGOAPI_GET_VERSION, NULL, (BYTE*)&algo_api_version);
    ex_log(LOG_DEBUG, "algo_api_version:%s,matchlib_version:%s", algo_api_version.algo_api_version,
           algo_api_version.matchlib_version);
    g_is_algo_init = TRUE;
    ex_log(LOG_INFO, "algorithm_initialization OK");
    return ret;
}

void algorithm_uninitialization_for_faceplus(unsigned char* data, const int data_size) {
    ex_log(LOG_DEBUG, "%s g_is_algo_init=%d", __func__, g_is_algo_init);
    if (!g_is_algo_init) return;
    algorithm_uninitialization(data, data_size);
    PLAT_FREE(g_heap_addr);
    g_heap_size = 0;
    g_is_algo_init = FALSE;
}
#ifdef __TRUSTONIC__
void debase_init() {
    if (!g_debase_db) g_debase_db = plat_alloc(g_debase_size);
}

void debase_uninit() {
    PLAT_FREE(g_debase_db);
}
#endif
int algorithm_initialization_by_sensor(unsigned char* decision_data, int decision_data_len,
                                       int sensor_type, int reset_flag) {
    ex_log(LOG_DEBUG, "algorithm_initialization_by_sensor");
    return FP_OK;
}

/*
 * The function get_time() is callback of G4 lib, and must be kept.
 */
double get_time() {
    return plat_get_time();
}

static int write_file(char* path, uint8_t* data, size_t data_size) {
    ex_log(LOG_DEBUG, "write_file");
#ifdef __TRUSTONIC__
    ex_log(LOG_ERROR, "workaround, skip write file");
    return data_size;
#else
    int retSize = 0;

    retSize = plat_save_file(path, (unsigned char*)data, data_size);
    if (retSize <= 0) {
        ex_log(LOG_ERROR, "failed to write %s\n", path);
        return -1;
    }
    return retSize;
#endif
}

static int read_file(char* path, unsigned char* pdata, size_t data_size) {
    ex_log(LOG_DEBUG, "read_file");

    int retval;
    unsigned int real_size;
    retval = plat_load_file(path, pdata, data_size, &real_size);

    if (retval == PLAT_FILE_NOT_EXIST || retval <= 0) {
        ex_log(LOG_ERROR, "%s config file is not exist! %d", __func__, retval);
        return -1;
    }
    return real_size;
}

/*
 * The function read_debase_context() is callback of G4 lib, and must be kept.
 */
int read_debase_context(void* buf, size_t buf_size) {
    ex_log(LOG_DEBUG, "read_debase_context");
#ifndef __TRUSTONIC__
    char* path = FILENAME_DEBASE_CTX;
    ex_log(LOG_DEBUG, "%s, %s, buf_size=%d", __func__, path, buf_size);
    unsigned char* content = (unsigned char*)plat_alloc(buf_size);
    if (content == NULL) {
        ex_log(LOG_ERROR, "content is NULL");
        return -1;
    }
    int content_size = read_file(path, content, buf_size);
    if (content_size != buf_size) {
        PLAT_FREE(content);
        ex_log(LOG_ERROR, "content size is not match");
        return -1;
    }
    memcpy(buf, content, content_size);
    PLAT_FREE(content);
    return content_size;
#else
    if (g_debase_size != buf_size) {
        ex_log(LOG_ERROR, "buf_size is error, buf_size = %d", buf_size);
        return -1;
    }
    mem_move(buf, g_debase_db, g_debase_size);
    return g_debase_size;
#endif
}

/*
 * The function write_debase_context() is callback of G4 lib, and must be kept.
 */
int write_debase_context(const char* data, size_t data_size) {
    ex_log(LOG_DEBUG, "write_debase_context");
#ifndef __TRUSTONIC__
    char* file_path = FILENAME_DEBASE_CTX;
    ex_log(LOG_DEBUG, "%s, %s, data_size=%d", __func__, file_path, data_size);
    return write_file(file_path, (uint8_t*)data, data_size);
#else
    g_debase_size = data_size;
    mem_move(g_debase_db, data, data_size);
    return g_debase_size;
#endif
}

int algorithm_do_other_for_faceplus(int opcode, unsigned char* indata, unsigned char* outdata) {
    if (outdata == NULL) {
        return EGIS_INCORRECT_PARAMETER;
    }
    if (!g_is_algo_init) {
        ex_log(LOG_ERROR, "%s, %d failed (algo not initialized)", __func__, opcode);
        if (opcode == FP_ALGOAPI_GET_VERSION) {
            ALGO_API_INFO* algo_api_version = (ALGO_API_INFO*)outdata;
            algo_api_version->algo_api_version[0] = '?';
            algo_api_version->algo_api_version[1] = '\0';
            algo_api_version->matchlib_version[0] = '?';
            algo_api_version->matchlib_version[1] = '\0';
        }
        return EGIS_COMMAND_FAIL;
    }
    return algorithm_do_other(opcode, indata, outdata);
}
