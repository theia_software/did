
#pragma once

/*
#define LOG(msg, v...)                          \
    do {                                        \
        printf("[%.4f] " msg, get_time(), ##v); \
        printf("\n"); \
    } while (0)
#define LOGE LOG
#define LOGD LOG*/

// struct _verify_info_;
// typedef struct _verify_info_ VERIFY_INFO;

int set_algorithm_config(int param, int value);
int algorithm_initialization_by_sensor(unsigned char* decision_data, int decision_data_len,
                                       int sensor_type, int reset_flag);
int algorithm_heap_init_for_faceplus(void);
int algorithm_initialization_for_faceplus(void);
void algorithm_uninitialization_for_faceplus(unsigned char* data, const int data_size);
int algorithm_do_other_for_faceplus(int opcode, unsigned char* indata, unsigned char* outdata);

#ifdef __TRUSTONIC__
void debase_uninit();
void debase_init();
#endif
void generate_calib(unsigned short** input_images, int image_num, int width, int height,
                    unsigned short* output_image);

void seq_proc_update_init(void);
void input_seq_proc(unsigned short* img, unsigned short* calib, int width, int height, int num,
                    int enable_quality_check, int* quality, int* quality_img, int* quality_partial,
                    unsigned char* outimg, int mode, int DC_offset_input, int DC_offset_calib,
                    int mode_sunlight);
void input_seq_proc_New(unsigned short* img, unsigned short* calib, int width, int height, int num,
                        int integ_num, int enable_quality_check, int* quality, int* quality_partial,
                        unsigned char* outimg, int mode, int type_panel, int mode_bkgEst,
                        int* result_bkgEst);
int input_seq_proc_New_v2(unsigned short* img, unsigned short* calib, int width, int height,
                          int num, int trymatch_num, int integ_num, int enable_quality_check,
                          int* quality, int* quality_partial, unsigned char* outimg, int mode);

int read_debase_context(void* buf, size_t buf_size);
int write_debase_context(const char* data, size_t data_size);
