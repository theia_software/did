//
// Created by Ren yanbin on 2018/10/22.
//
#pragma once
#include <stddef.h>
#include <stdint.h>
#include "../other_define_from_g3.h"
#include "faceplusutil.h"
#include "plat_log.h"
#include "plat_mem.h"

// param for enroll context
#define ENROLL_CTX_MIN_ENROLL_COUNT 1214
#define ENROLL_CTX_MAX_ENROLL_COUNT 1215
#define ENROLL_CTX_REDUNDANT_CHECK_START 1216
#define ENROLL_CTX_REDUNDANT_CONTINUOUS_BOUND 1217
#define ENROLL_CTX_EXTRACT_THRESHOLD 1221
#define ENROLL_CTX_GENERALIZATION_THRESHOLD 1223
#define ENROLL_CTX_DUPLICATE_THRESHOLD 1226
#define ENROLL_CTX_REDUNDANT_INPUT_THRESHOLD 1227
#define ENROLL_CTX_REDUNDANT_THRESHOLD 1228
#define ENROLL_CTX_REDUNDANT_IMAGE_POLICY 1229
#define ENROLL_CTX_REDUNDANT_LEVEL 1230
#define ENROLL_CTX_QUALITY_REJECT_LEVEL 1231

// enroll context information
#define ENROLL_CTX_ENROLL_PROGRESS 1251
#define ENROLL_CTX_PRE_IDX 1252
#define ENROLL_CTX_ENROLLED_COUNT 1254
// set ENROLL_CTX_REDUNDANT_IMAGE_POLICY
#define FP_REDUNDANT_POLICY_REJECT_IMAGE 0  // default
#define FP_REDUNDANT_POLICY_ACCPET_IMAGE 1

enum AlgoAPIDeviceType {
    FP_ALGOAPI_MODE_UNKNOWN,
    FP_ALGOAPI_MODE_EGIS_ET300,
    FP_ALGOAPI_MODE_EGIS_ET310,
    FP_ALGOAPI_MODE_EGIS_ET320,
    FP_ALGOAPI_MODE_EGIS_ET505,
    FP_ALGOAPI_MODE_EGIS_ET510,
    FP_ALGOAPI_MODE_EGIS_ET511,
    FP_ALGOAPI_MODE_EGIS_ET516,
    FP_ALGOAPI_MODE_EGIS_ET516_MLOGO,
    FP_ALGOAPI_MODE_EGIS_ET512,
    FP_ALGOAPI_MODE_EGIS_ET519,
    FP_ALGOAPI_MODE_EGIS_ET702,
    FP_ALGOAPI_MODE_EGIS_ET517,
    FP_ALGOAPI_MODE_EGIS_ET518,
    FP_ALGOAPI_MODE_EGIS_ET538,
    FP_ALGOAPI_MODE_SUPREMA1,
    FP_ALGOAPI_MODE_SUPREMA2,
    FP_ALGOAPI_MODE_EGIS_ET725,
    FP_ALGOAPI_MODE_SUPREMA4,
    FP_ALGOAPI_MODE_EGIS_ET735,
    FP_ALGOAPI_MODE_EGIS_ET727,
    FP_ALGOAPI_MODE_EGIS_ET728,
    FP_ALGOAPI_MODE_EGIS_ET736_EC3_NEW,
    FP_ALGOAPI_MODE_EGIS_ET711,
    FP_ALGOAPI_MODE_EGIS_ET713,
};

#ifdef __cplusplus
extern "C" {
#endif

#define FP_OK 0

#define FP_MATCHOK 101
#define FP_FINISH 102
#define SFS_OK 103
#define FP_MATCHFAIL 104

// Learning fail
#define FP_LEARNING_FAILED 405
#define FP_LEARNING_LIMIT 406
#define FP_ENCRYPT_FAIL 407
#define FP_TEMPLATE_TOO_YOUNG 408

#define FP_ERR -1000
#define FP_TIMEOUT -1001
#define FP_BADIMAGE -1002
#define FP_FEATURELOW -1003
#define FP_DUPLICATE -1004
#define FP_NOTDUPLICATE -1005
//#define FP_MATCHFAIL                -1006
#define FP_NULL_DATA -1007
#define FP_INVALID_FEATURE_LEN -1008
#define FP_ALLOC_MEM_FAIL -1009
#define FP_INVALID_FORMAT -1010
#define FP_TUNING_FAIL -1011
#define FP_INVALID_BUFFER_SIZE -1012
#define FP_IO_ACCESS_FAIL -1013
#define FP_NOT_TUNING -1014
#define FP_CONNECT_SENSOR_FAIL -1015
#define FP_CALL_FAILED -1016
#define FP_NOT_CONNECT_SENSOR -1017
#define FP_NULL_ENROLL_DATA -1018
#define FP_NULL_FEATURE -1019
#define FP_GETIMAGEFAIL -1020
#define FP_DECRYPT_FAIL -1021
#define FP_STATE_ERR -1022
#define FP_INVALID_SENSOR_MODE -1023
#define FP_VERIFY_DATA_ERROR -1024
#define FP_TEMPLATE_CRC_ERROR -1025
#define FP_SET_EASY_MODE_ERROR -1026
#define FP_DECOMPRESS_FAIL -1027
#define FP_INVALID_PARAMETER -1028
#define FP_INVALID_FINGER_NUM -1029
#define FP_FEATURE_LEN_OVER_SIZE -1030
#define FP_RESIDUAL_DETECT -1031

#define FP_PARAMETER_NOT_VALID -1888
#define FP_PARAMETER_UNACCEPTABLE -1889

// opcode
#define FP_ALGOAPI_GET_VERSION 901
#define FP_ALGOAPI_FIX_PATTERN 902
#define FP_ALGOAPI_IPP_OFF 980
#define FP_ALGOAPI_IPP_ET320 981
#define FP_ALGOAPI_FORCE_OFF 988
#define FP_ALGOAPI_FORCE_ON 989

// FP_MERGE_ENROLL_ERROR_CODE
#define FP_MERGE_ENROLL_IAMGE_OK 1
#define FP_MERGE_ENROLL_FINISH 2
#define FP_MERGE_ENROLL_IMAGE_HIGHLY_SIMILARITY 3
#define FP_MERGE_ENROLL_REDUNDANT_ACCEPT 4
#define FP_MERGE_ENROLL_PROCESSING 5
#define FP_MERGE_ENROLL_NON_ADD 6
#define FP_MERGE_ENROLL_OFFSET_REDUNDANT_INPUT 7
#define FP_MERGE_ENROLL_BAD_IMAGE -1
#define FP_MERGE_ENROLL_FEATURE_LOW -2
#define FP_MERGE_ENROLL_FAIL -3
#define FP_MERGE_ENROLL_OUT_OF_MEMORY -4
#define FP_MERGE_ENROLL_UNKNOWN_FAIL -5
#define FP_MERGE_ENROLL_IRREGULAR_CONTEXT -6
#define FP_MERGE_ENROLL_DUPLICATE_IMAGE_REMOVED -7
#define FP_MERGE_ENROLL_REDUNDANT_INPUT -8
#define FP_MERGE_ENROLL_TOO_FAST -9
#define FP_MERGE_ENROLL_LOW_QTY -10
#define FP_MERGE_ENROLL_REJECT_PARTIAL -11
#define FP_MERGE_ENROLL_REJECT_FAKE -12

//最大的底库集尺寸
#define MGULK_FEATURE_SET_SIZE (1502 * 1024)
#define MGULK_FEATURE_SIZE (60 * 1024)

#define MAX_FEATURE_LEN MGULK_FEATURE_SIZE
#define MAX_ENROLL_TEMPLATE_LEN MGULK_FEATURE_SET_SIZE

enum FPExtractResult {
    exOK,
    exFeatureLow,
    exBadImage,
    exNotImplement,
    exImageTooSmall,
    exEncryptFail,
    exFail,
    exParital,
    exFakeFinger,
};

#define ENROLL_CTX_REDUNDANT_METHOD 1231

typedef enum {
    ALGO_PARAM_TYPE_MODEL = 0,
    ALGO_PARAM_TYPE_HEAP = 1,
    ALGO_PARAM_TYPE_BASE_IMG_1 = 2,
    ALGO_PARAM_TYPE_RETRY_NUM = 3,
    ALGO_PARAM_TYPE_SYS_TEMP_X10 = 4,
    ALGO_PARAM_TYPE_HEAP_SIZE = 5,
    ALGO_PARAM_TYPE_UPGRADE_ENROLL = 6,
    ALGO_PARAM_TYPE_TEMPLATE_VERSION = 7,
    ALGO_PARAM_TYPE_HW_INTEGRATE = 8,
} algo_param_type_t;

typedef struct _verify_init_ {
    unsigned char** pEnroll_temp_array;
    int* enroll_temp_size_array;
    int enroll_temp_number;
    // int*    bad_enroll_number_array;
} VERIFY_INIT;

typedef struct _verify_info_ {
    unsigned char* pFeat;
    int feat_size;
    unsigned char* pEnroll_temp;
    int enroll_temp_size;
    int matchScore;
    int matchindex;
    int isLearningUpdate;
    int extract_ret;
    int target_template;
    int learning_mode;
    int try_count;
    int final_try;
} VERIFY_INFO;

typedef struct _algo_api_info_ {
    char algo_api_version[30];
    char matchlib_version[30];
} ALGO_API_INFO;

#define MGULK_CAPI __attribute__((visibility("default")))
/**********************************************************************/
/* API implemented by egis  for megvii                                */
/**********************************************************************/
MGULK_CAPI void output_log(LOG_LEVEL level, const char* tag, const char* file_name,
                           const char* func, int line, const char* format, ...);
MGULK_CAPI void set_debug_level(LOG_LEVEL level);
MGULK_CAPI int read_debase_context(void* buf, size_t buf_size);
MGULK_CAPI int write_debase_context(const char* data, size_t data_size);
/**********************************************************************/
/* API implemented by megvii for egis                                 */
/**********************************************************************/
MGULK_CAPI void megvii_set_debug_level(LOG_LEVEL level);
MGULK_CAPI int algo_set_buffer(algo_param_type_t param_type, unsigned char* in_buffer,
                               int in_buffer_size);
MGULK_CAPI int algo_set_int(algo_param_type_t param_type, int val);
MGULK_CAPI int algo_get_int(algo_param_type_t param_type, int* val);

MGULK_CAPI int algorithm_initialization(unsigned char* decision_data, int decision_data_len,
                                        int reset_flag);

MGULK_CAPI void algorithm_uninitialization(unsigned char* decision_data, int decision_data_len);

MGULK_CAPI void* enroll_init(void);

MGULK_CAPI int set_enroll_context(void* ctx, int param, int value);

MGULK_CAPI int get_enroll_context(void* ctx, int param, int* value);

MGULK_CAPI int enroll_setDB(void* ctx, VERIFY_INIT* verify_init_data);

MGULK_CAPI int enroll(unsigned char* img, int width, int height, void* e_ctx, int* percentage);

MGULK_CAPI unsigned char* get_enroll_template(void* ctx, int* len);

MGULK_CAPI int enroll_uninit(void* e_ctx);

MGULK_CAPI int verify_init(VERIFY_INIT* verify_info);

MGULK_CAPI int extract_feature(unsigned char* img, int width, int height, unsigned char* feat,
                               int* len, int* quality);

MGULK_CAPI int verify(VERIFY_INFO* verify_info);

MGULK_CAPI int verify_uninit(void);

MGULK_CAPI int algorithm_do_other(int opcode, unsigned char* indata, unsigned char* outdata);

MGULK_CAPI unsigned char* megvii_get_debug_image(int* len);
#ifdef __cplusplus
}
#endif
