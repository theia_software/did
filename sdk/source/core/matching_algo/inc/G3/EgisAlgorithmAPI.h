/******************************************************************************\
|*                                                                            *|
|*  EgisAlgorithmAPI.h                                                        *|
|*  Version: 2.0.1.19                                                         *|
|*  Date: 2015/05/24                                                          *|
|*  Revise Date: 2018/10/31                                                   *|
|*  Copyright (C) 2007-2018 Egis Technology Inc.                              *|
|*                                                                            *|
\******************************************************************************/

#pragma once

#include "../other_define_from_g2.h"

#define FP_OK 0

#define FP_MATCHOK 101
#define FP_FINISH 102
#define SFS_OK 103
#define FP_MATCHFAIL 104

// Learning fail
#define FP_LEARNING_FAILED 405
#define FP_LEARNING_LIMIT 406
#define FP_ENCRYPT_FAIL 407
#define FP_TEMPLATE_TOO_YOUNG 408

#define FP_ERR -1000
#define FP_TIMEOUT -1001
#define FP_BADIMAGE -1002
#define FP_FEATURELOW -1003
#define FP_DUPLICATE -1004
#define FP_NOTDUPLICATE -1005
//#define FP_MATCHFAIL                -1006
#define FP_NULL_DATA -1007
#define FP_INVALID_FEATURE_LEN -1008
#define FP_ALLOC_MEM_FAIL -1009
#define FP_INVALID_FORMAT -1010
#define FP_TUNING_FAIL -1011
#define FP_INVALID_BUFFER_SIZE -1012
#define FP_IO_ACCESS_FAIL -1013
#define FP_NOT_TUNING -1014
#define FP_CONNECT_SENSOR_FAIL -1015
#define FP_CALL_FAILED -1016
#define FP_NOT_CONNECT_SENSOR -1017
#define FP_NULL_ENROLL_DATA -1018
#define FP_NULL_FEATURE -1019
#define FP_GETIMAGEFAIL -1020
#define FP_DECRYPT_FAIL -1021
#define FP_STATE_ERR -1022
#define FP_INVALID_SENSOR_MODE -1023
#define FP_VERIFY_DATA_ERROR -1024
#define FP_TEMPLATE_CRC_ERROR -1025
#define FP_SET_EASY_MODE_ERROR -1026
#define FP_DECOMPRESS_FAIL -1027
#define FP_INVALID_PARAMETER -1028
#define FP_INVALID_FINGER_NUM -1029

#define FP_PARAMETER_NOT_VALID -1888
#define FP_PARAMETER_UNACCEPTABLE -1889

// opcode
#define FP_ALGOAPI_GET_VERSION 901
#define FP_ALGOAPI_FIX_PATTERN 902
#define FP_ALGOAPI_IPP_OFF 980
#define FP_ALGOAPI_IPP_ET320 981
#define FP_ALGOAPI_FORCE_OFF 988
#define FP_ALGOAPI_FORCE_ON 989
//
//#define FP_ALGOAPI_SET_REDUNDNAT_2ND_START			920
//#define FP_ALGOAPI_SET_REDUNDNAT_2ND_CONTINUOUS_BOUND	921

// FP_MERGE_ENROLL_ERROR_CODE
#define FP_MERGE_ENROLL_IAMGE_OK 1
#define FP_MERGE_ENROLL_FINISH 2
#define FP_MERGE_ENROLL_IMAGE_HIGHLY_SIMILARITY 3
#define FP_MERGE_ENROLL_REDUNDANT_ACCEPT 4
#define FP_MERGE_ENROLL_PROCESSING 5
#define FP_MERGE_ENROLL_NON_ADD 6
#define FP_MERGE_ENROLL_OFFSET_REDUNDANT_INPUT 7
#define FP_MERGE_ENROLL_BAD_IMAGE -1
#define FP_MERGE_ENROLL_FEATURE_LOW -2
#define FP_MERGE_ENROLL_FAIL -3
#define FP_MERGE_ENROLL_OUT_OF_MEMORY -4
#define FP_MERGE_ENROLL_UNKNOWN_FAIL -5
#define FP_MERGE_ENROLL_IRREGULAR_CONTEXT -6
#define FP_MERGE_ENROLL_DUPLICATE_IMAGE_REMOVED -7
#define FP_MERGE_ENROLL_REDUNDANT_INPUT -8
#define FP_MERGE_ENROLL_TOO_FAST -9

#define MAX_ENROLL_TRY MAX_PRE_ENROLL

#define MIN_TEMPLATE_SIZE EMPTY_FEATURE_SIZE
#define FEAT_HEADER_LEN (5)

#define MAX_ARRAY_FEATURE 40
#define MAX_ENROLL_FINGERS 5
#define VERIFY_ALL_FINGERS -1

#ifdef G1PLUSG3_FRAMEWORK
#undef G3_MATCHER
#define G13FRAMEWORK_OK 1
#define G13FRAMEWORK_FAIL 0
#define G13FRAMEWORK_G3_DATA_READY 2
#define G13FRAMEWORK_G3_DATA_NOT_READY 3
#define G13FRAMEWORK_ERROR -1
#define G13FRAMEWORK_UNKNOWN_FORMAT -2
#define G13FRAMEWORK_UNKNOWN_ERROR -3
#define G13FRAMEWORK_ERROR_FUNTCIONT_INPUT -4
#define G13FRAMEWORK_MEMORY_FAIL -5
#define G13FRAMEWORK_INCORRECT_FORMAT -6
#define G13FRAMEWORK_DECOMPRESS_FAIL -7
#define G13FRAMEWORK_COMPRESS_FAIL -8
#endif
#define G3_DATA_VERSION 'D'
#define G1PLUSG3_DATA_VERSION 'E'
#ifdef G3_MATCHER
#define MAX_FEATURE_LEN (40 * 1024)
#else
#ifdef G1PLUSG3_FRAMEWORK
#define MAX_FEATURE_LEN (17 * 1024)
#else
#define MAX_FEATURE_LEN 5000
#endif
#endif /* ENABLE_G3ALGO_LIB */
#define MAX_ENROLL_TEMPLATE_LEN (240 * 1024)

// enroll
#define MIN_ENROLL_PROGRESS 3
#define MIN_ENROLL_PROGRESS_LAST_PHASE 1

// enroll context
#define ENROLL_CTX_ENROLL_POLICY 1200
#define ENROLL_CTX_MAX_ENROLL_ROTATION 1201
#define ENROLL_CTX_EXTRACT_SKELETON 1203
#define ENROLL_CTX_REDUNDANT_TRYCOUNT_POLICY 1204
#define ENROLL_CTX_MIN_MINUTIAE_COUNT 1211
#define ENROLL_CTX_MAX_CANDIDATE_COUNT 1212
#define ENROLL_CTX_MAX_ENROLL_TRY 1213
#define ENROLL_CTX_MIN_ENROLL_COUNT 1214
#define ENROLL_CTX_MAX_ENROLL_COUNT 1215
#define ENROLL_CTX_REDUNDANT_CHECK_START 1216
#define ENROLL_CTX_REDUNDANT_CONTINUOUS_BOUND 1217
#define ENROLL_CTX_EXTRACT_THRESHOLD 1221
#define ENROLL_CTX_SELECT_COUNT_THRESHOLD 1222
#define ENROLL_CTX_GENERALIZATION_THRESHOLD 1223
#define ENROLL_CTX_ENROLL_PROGRESS_THRESHOLD 1224
#define ENROLL_CTX_SIMILARITY_THRESHOLD 1225
#define ENROLL_CTX_DUPLICATE_THRESHOLD 1226
#define ENROLL_CTX_REDUNDANT_INPUT_THRESHOLD 1227
//#define ENROLL_CTX_REDUNDANT_THRESHOLD			1228
#define ENROLL_CTX_REDUNDANT_IMAGE_POLICY 1229
#define ENROLL_CTX_REDUNDANT_LEVEL 1230
#define ENROLL_CTX_REDUNDANT_METHOD 1231
#define ENROLL_CTX_REDUNDANT_THRESHOLD 1232
#define ENROLL_CTX_SET_EASY_MODE_COUNT 1233
#define ENROLL_CTX_ENROLLEX_SWIPE_COUNT_X 1234
#define ENROLL_CTX_ENROLLEX_SWIPE_COUNT_Y 1235
#define ENROLL_CTX_ENROLLEX_SELECT_THRESHOLD 1236
#define ENROLL_CTX_ENROLLEX_TOO_FAST_LEVEL 1237
#define ENROLL_CTX_ENROLLEX_METHOD 1238
#define ENROLL_CTX_ENROLLEX_SWITCH_VERTICAL 1239
#define ENROLL_CTX_ENROLLEX_SWITCH_HORIZONTAL 1240
#define ENROLL_CTX_ENROLLEX_SWITCH_XY 1241
#define ENROLL_CTX_ENROLLEX_TOO_FAST_METHOD 1242
#define ENROLL_CTX_ENROLLEX_REDUNDANT_METHOD 1243
#define ENROLL_CTX_ENROLLEX_REDUNDANT_MATCH_THRESHOLD 1244
#define ENROLL_CTX_ENROLLEX_REDUNDANT_OFFSET_THRESHOLD 1245
#define ENROLL_CTX_ENROLLEX_SPEEDUP_PROGRESS 1246
#define ENROLL_CTX_ENROLLEX_USE_COUNT_PERCENT 1247
#define ENROLL_CTX_ENROLLEX_REDUNDANT_LIMIT 1248

// enroll context information
#define ENROLL_CTX_ENROLL_PROGRESS 1251
#define ENROLL_CTX_PRE_IDX 1252
#define ENROLL_CTX_SELECTCOUNT 1253
#define ENROLL_CTX_ENROLLED_COUNT 1254

// set ENROLL_CTX_REDUNDANT_IMAGE_POLICY
#define FP_REDUNDANT_POLICY_REJECT_IMAGE 0  // default
#define FP_REDUNDANT_POLICY_ACCPET_IMAGE 1

// set ENROLL_CTX_ENROLL_POLICY
#define FP_ENROLL_POLICY_TOUCH_ENROLL 0  // default
#define FP_ENROLL_POLICY_SWIPE_ENROLL 1
#define FP_ENROLL_POLICY_SWIPE_PRACTICE 2
#define FP_ENROLL_POLICY_SWIPE_ENROLL_V1 3
#define FP_ENROLL_POLICY_SWIPE_ENROLL_V2 4

// set ENROLL_CTX_ENROLLEX_METHOD
#define FP_ENOLLEX_METHOD_ADD_ENROLL 0
#define FP_ENOLLEX_METHOD_EXTRACT_EARLY 1

// set ENROLL_CTX_ENROLLEX_TOO_FAST_METHOD
#define FP_ENOLLEX_TOO_FAST_RET_MSG 0
#define FP_ENOLLEX_TOO_FAST_ROLLBACK 1

// set ENROLL_CTX_ENROLLEX_REDUNDANT_METHOD
#define FP_ENOLLEX_REDUNDANT_RET_MSG 0
#define FP_ENOLLEX_REDUNDANT_ROLLBACK 1

// QuickLearning
#define QUICK_LEARNING_TH 39

// enroll_ex flag
#define ENROLL_EX_FINGER_OFF NULL

// set_algo_config
#define ALGO_CONFIG_EXTRA_DATA 2000
#define ALGO_CONFIG_INIT_MAX_ENROLL 2001

// set EgisSensor
enum AlgoAPIDeviceType {
    FP_ALGOAPI_MODE_UNKNOWN,
    FP_ALGOAPI_MODE_EGIS_ET300,
    FP_ALGOAPI_MODE_EGIS_ET310,
    FP_ALGOAPI_MODE_EGIS_ET320,
    FP_ALGOAPI_MODE_EGIS_ET505,
    FP_ALGOAPI_MODE_EGIS_ET510,
    FP_ALGOAPI_MODE_EGIS_ET511,
    FP_ALGOAPI_MODE_EGIS_ET516,
    FP_ALGOAPI_MODE_EGIS_ET516_MLOGO,
    FP_ALGOAPI_MODE_EGIS_ET512,
    FP_ALGOAPI_MODE_EGIS_ET519,
    FP_ALGOAPI_MODE_EGIS_ET702,
    FP_ALGOAPI_MODE_EGIS_ET517,
    FP_ALGOAPI_MODE_EGIS_ET518,
    FP_ALGOAPI_MODE_EGIS_ET538,
    FP_ALGOAPI_MODE_SUPREMA1,
    FP_ALGOAPI_MODE_EGIS_ET725,
    FP_ALGOAPI_MODE_EGIS_ET600,
    FP_ALGOAPI_MODE_EGIS_ET601,
    FP_ALGOAPI_MODE_EGIS_ET520,
    FP_ALGOAPI_MODE_EGIS_ET727,
    FP_ALGOAPI_MODE_EGIS_ET523,
    FP_ALGOAPI_MODE_EGIS_ET522,
    FP_ALGOAPI_MODE_EGIS_ET616,
    FP_ALGOAPI_MODE_EGIS_ET613,
    FP_ALGOAPI_MODE_EGIS_ET516_M0,
    FP_ALGOAPI_MODE_EGIS_ET520_M0
};

extern int g_MinMinutiaeCount;
extern int g_EnrollProgressThreshold;
extern int g_MinEnrollCount;
extern int g_MaxCandidateCount;

#ifdef __cplusplus
extern "C" {
#endif

typedef struct _verify_init_ {
    unsigned char** pEnroll_temp_array;
    int* enroll_temp_size_array;
    int enroll_temp_number;
    // int*	bad_enroll_number_array;
} VERIFY_INIT;

typedef struct _verify_info_ {
    unsigned char* pFeat;
    int feat_size;
    unsigned char* pEnroll_temp;
    int enroll_temp_size;
    int matchScore;
    int matchindex;
    int isLearningUpdate;
    int extract_ret;
    int target_template;
} VERIFY_INFO;
// verfiy function : do matching & learning

typedef struct _fix_pattern_info {
    int feat_size;
    unsigned char* pFeat;
} FIX_PATTERN_INFO;

int algorithm_initialization(unsigned char* decision_data, int decision_data_len, int reset_flag);
int algorithm_initialization_by_sensor(unsigned char* decision_data, int decision_data_len,
                                       enum AlgoAPIDeviceType sensor_type, int reset_flag);
void algorithm_uninitialization(unsigned char* decision_data, int decision_data_len);

void* enroll_init(void);
int set_enroll_context(void* ctx, int param, int value);
int get_enroll_context(void* ctx, int param, int* value);

int enroll(unsigned char* img, int width, int height, void* ctx, int* percentage);
int enroll_practice(unsigned char* img, int width, int height, void* ctx, int forcibly);
int enroll_duplicate_check(unsigned char* enroll_feat1, unsigned char* enroll_feat2,
                           int enroll_feat1_size, int enroll_feat2_size, int* threshold);
int enroll_setDB(void* e_ctx, VERIFY_INIT* verify_init_data);
unsigned char* get_enroll_template(void* ctx, int* len);

int enroll_uninit(void* ctx);

int extract_feature(unsigned char* img, int width, int height, unsigned char* feat, int* len,
                    int* quality);

enum FPExtractResult {
    exOK,
    exFeatureLow,
    exBadImage,
    exNotImplement,
    exImageTooSmall,
    exEncryptFail,
    exNotGoodImage
};

int verify_init(VERIFY_INIT* verify_init);

int verify(VERIFY_INFO* verify_info);

int verify_uninit(void);

int set_accuracy_level(int level);

int algorithm_do_other(int opcode, unsigned char* indata, unsigned char* outdata);

int algo_wet_finger_detector(unsigned char* img, int w, int h);
int algo_enroll_setDB(void* e_ctx, VERIFY_INIT* enroll_init_data);
int algo_check_vdm_coverage(unsigned char* in_img, int w, int h, unsigned char* in_mask,
                            int method);
int algo_match_two_image(unsigned char* in_img1, int w1, int h1, unsigned char* in_img2, int w2,
                         int h2, int* score, int* dx, int* dy, int* rotation);
int algo_enroll_duplicate_check(unsigned char* enroll_feat1, unsigned char* enroll_feat2,
                                int enroll_feat1_size, int enroll_feat2_size, int* defThreshold);
int algo_check_enroll_template(unsigned char* pEnroll_temp, int pEnroll_temp_size);
int get_fix_pattern_ref(FIX_PATTERN_INFO* fix_pattern_info);
int set_fix_pattern_ref(FIX_PATTERN_INFO* fix_pattern_info);
int set_algo_config(int param, int value);

/**************************************************************************************************\
setting *swipe_dir:
policy is FP_ENROLL_POLICY_SWIPE_ENROLL_V1 :
1. input SWIPE_NULL_DIRECTION : api can auto detectoin, and *swipe_dir will output
SWIPE_X_DIRECTION or SWIPE_Y_DIRECTION after input some images.
After *swipe_dir  output SWIPE_X_DIRECTION or SWIPE_Y_DIRECTION, input any value is not allowed.
2. input SWIPE_X_DIRECTION or SWIPE_Y_DIRECTION : assignment swipe direction
policy is FP_ENROLL_POLICY_SWIPE_ENROLL_V2 :
1. input only SWIPE_NULL_DIRECTION
\**************************************************************************************************/
#define SWIPE_NULL_DIRECTION 0
#define SWIPE_X_DIRECTION 1
#define SWIPE_Y_DIRECTION 2
int enroll_ex(unsigned char* img, int width, int height, void* e_ctx, int* percentage, int map_w,
              int map_h, int* dx, int* dy, int* qm_score, int* swipe_dir);
void set_finger_on(unsigned char finger_on);
int disable_easy_mode(VERIFY_INIT* verify_init);

// Egis image info function
int count_isFPimage(unsigned char* img, int width, int height, int* black_percent,
                    int* white_percent, int* amplitude, int* bo_dir_consistency);
int count_isFPimage_lite(unsigned char* img, int width, int height, int* corner_count,
                         int ret_normalize_img);
unsigned int get_euclidean_distance(unsigned char* raw_data_1, int raw_data_1_width,
                                    unsigned char* raw_data_2, int width, int height,
                                    int test_width);
// FP_IMAGE_IDENTIFIDERL_ERROR_CODE
#define FP_IMG_IDENTIFIDER_OUT_OF_MEMORY -1;
#define FP_IMG_IDENTIFIER_BAD_IMAGE -2;
enum count_imageTypeIdentifier {
    ImageTypeUnknown,
    ImageTypeEmpty,
    ImageTypeWaterMark,
    ImageTypeFinger,
    ImageTypeFingerPartial,
    ImageTypeFingerTooPartial
};

int count_image_type_identifier(unsigned char* in_img, int w, int h, int* out_amp, int* out_score,
                                enum AlgoAPIDeviceType sensor_type);
int count_VDM_fingerIdentifier(unsigned char* in_img, int w, int h,
                               enum AlgoAPIDeviceType sensor_type);

typedef struct _algo_api_info_ {
    char algo_api_version[30];
    char matchlib_version[30];
} ALGO_API_INFO;

// g3 begin
#define EGIS_G3_ALGORITHM_VERSION "009.PSN.1.0.2"
#define G3_EMPTY_FEATURE_SIZE 7
#define G3_MIN_TEMPLATE_SIZE G3_EMPTY_FEATURE_SIZE

int g3_algoAPI_set_sensor_type(enum AlgoAPIDeviceType sensor_type);
int g3api_algorithm_initialization_by_sensor(unsigned char* decision_data, int decision_data_len,
                                             enum AlgoAPIDeviceType sensor_type, int reset_flag);
void g3api_algorithm_uninitialization(unsigned char* decision_data, int decision_data_len);

void* g3api_enroll_init(void);
int g3api_set_enroll_context(void* ctx, int param, int value);
int g3api_get_enroll_context(void* ctx, int param, int* value);

int g3api_enroll(unsigned char* img, int width, int height, void* ctx, int* percentage);
int g3api_enroll_practice(unsigned char* img, int width, int height, void* ctx, int forcibly);
// int enroll_duplicate_check(unsigned char* enroll_feat1, unsigned char* enroll_feat2, int
// enroll_feat1_size, int enroll_feat2_size, int* threshold);  int enroll_setDB(void *e_ctx,
// VERIFY_INIT *verify_init_data);
unsigned char* g3api_get_enroll_template(void* ctx, int* len);

int g3api_enroll_uninit(void* ctx);

int g3api_extract_feature(unsigned char* img, int width, int height, unsigned char* feat, int* len,
                          int* quality);

// enum FPExtractResult {exOK, exFeatureLow, exBadImage, exNotImplement,
// exImageTooSmall,exEncryptFail};

int g3api_verify_init(VERIFY_INIT* verify_init);

int g3api_verify(VERIFY_INFO* verify_info);

int g3api_verify_uninit(void);

int g3api_set_accuracy_level(int level);

int g3api_algorithm_do_other(int opcode, unsigned char* indata, unsigned char* outdata);
void g3api_clear_enroll_db(void);
void g3api_clear_verify_data(void);
int g3api_algo_wet_finger_detector(unsigned char* img, int w, int h);
int g3api_algo_count_percentage_bsd(unsigned char* img, int w, int h);
int g3api_algo_enroll_setDB(void* e_ctx, VERIFY_INIT* enroll_init_data);
int g3api_algo_check_vdm_coverage(unsigned char* in_img, int w, int h, unsigned char* in_mask,
                                  int method);
int g3api_algo_match_two_image(unsigned char* in_img1, int w1, int h1, unsigned char* in_img2,
                               int w2, int h2, int* score, int* dx, int* dy, int* rotation);
int g3api_algo_enroll_duplicate_check(unsigned char* enroll_feat1, unsigned char* enroll_feat2,
                                      int enroll_feat1_size, int enroll_feat2_size,
                                      int* defThreshold);
int g3api_algo_check_enroll_template(unsigned char* pEnroll_temp, int pEnroll_temp_size);
int g3api_try_match_learning(unsigned char* img_stream, int w, int h, int count,
                             unsigned char* feat, int* feat_size);
int g3api_append_image_into_template(unsigned char* img_stream, int w, int h, int count,
                                     unsigned char* feat, int* feat_size);
int g3api_find_try_index(unsigned char* img_array, int w, int h, int count,
                         unsigned char* out_rank);
int g3api_get_fix_pattern_ref(FIX_PATTERN_INFO* fix_pattern_info);
int g3api_set_fix_pattern_ref(FIX_PATTERN_INFO* fix_pattern_info);
int g3api_enroll_ex(unsigned char* img, int width, int height, void* e_ctx, int* percentage,
                    int map_w, int map_h, int* dx, int* dy, int* qm_score, int* swipe_dir);
void g3api_set_finger_on(unsigned char finger_on);
int g3api_disable_easy_mode(VERIFY_INIT* verify_init);
int g3api_encrypt_feature(unsigned char* feat, int* len, int enroll);
int g3api_decrypt_feature(unsigned char* feat, int* len, int enroll);
int g3api_evaluate_enroll_quality(unsigned char* enroll_template, int enroll_template_size,
                                  int* info_size, int* info_concentration, int* info_area);
int g3api_set_algo_config(int param, int value);

#define BOUND_VALUE(a, b, c) ((a) > (b) ? a : ((b) > (c) ? c : b))
#define VDM_METHOD_BSD 1
#define VDM_METHOD_SBSD 2
#define VDM_METHOD_BSD_C 3
#define VDM_METHOD_QM 4
#define VDM_METHOD_BSD_RV 5
#define VDM_METHOD_BSD_D 6
#define VDM_METHOD_BSD_E 7

#define SWIPE_ENROLL_FINGER_OFF_FLAG NULL

typedef struct _decision_data {
    unsigned char version;
    unsigned char magic_code;
    unsigned char matching_threshold;
    unsigned char accuracy_level;
    unsigned char second_match;
    unsigned char previous_matchidx;
    unsigned char previous_finer_nums;
    unsigned char match_temp[5];
} decision_data;
typedef struct _g3_decision_data {
    unsigned char version;
    unsigned char magic_code;
    unsigned short matching_threshold;
    unsigned short accuracy_level;
    unsigned short second_match;
    unsigned short match;
    unsigned short learning_th;
    int match_index_array[5];
} g3_decision_data;
enum { FmtUnknown, FmtG1, FmtG3, FmtG13Mix };
enum { G13MixTemplateUndefined, G13MixTemplateEnroll, G13MixTemplateVerify };
enum { DBStatusG1, DBStatusMix, DBStatusG3 };
unsigned char* mf_get_g3_ptr_from_mix_template(unsigned char* feat, int* type);
int mf_read_enroll_template_format(unsigned char* feat);
// read template type{g1, g3, g1+g3} from an enroll template
#ifdef __cplusplus
}
#endif
