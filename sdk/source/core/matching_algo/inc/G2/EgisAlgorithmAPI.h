/******************************************************************************\
|*                                                                            *|
|*  EgisAlgorithmAPI.h                                                        *|
|*  Version: 2.0.0.2                                                          *|
|*  Revise Date: 2017/09/13                                                   *|
|*  Copyright (C) 2007-2016 Egis Technology Inc.                              *|
|*                                                                            *|
\******************************************************************************/

#ifndef EGIS_ALGORITHM_API_H
#define EGIS_ALGORITHM_API_H

#include "../other_define_from_g3.h"

#define FP_OK 0

#define FP_MATCHOK 101
#define FP_FINISH 102

// Learning fail
#define FP_LEARNING_FAILED 405
#define FP_LEARNING_LIMIT 406
#define FP_ENCRYPT_FAIL 407
#define FP_TEMPLATE_TOO_YOUNG 408

#define FP_ERR -1000
#define FP_TIMEOUT -1001
#define FP_BADIMAGE -1002
#define FP_FEATURELOW -1003
#define FP_DUPLICATE -1004
#define FP_NOTDUPLICATE -1005
#define FP_MATCHFAIL -1006
#define FP_NULL_DATA -1007
#define FP_INVALID_FEATURE_LEN -1008
#define FP_ALLOC_MEM_FAIL -1009
#define FP_INVALID_FORMAT -1010
#define FP_TUNING_FAIL -1011
#define FP_INVALID_BUFFER_SIZE -1012
#define FP_IO_ACCESS_FAIL -1013
#define FP_NOT_TUNING -1014
#define FP_CONNECT_SENSOR_FAIL -1015
#define FP_CALL_FAILED -1016
#define FP_NOT_CONNECT_SENSOR -1017
#define FP_NULL_ENROLL_DATA -1018
#define FP_NULL_FEATURE -1019
#define FP_GETIMAGEFAIL -1020
#define FP_DECRYPT_FAIL -1021
#define FP_STATE_ERR -1022
#define FP_INVALID_SENSOR_MODE -1023

#define FP_PARAMETER_NOT_VALID -1888
#define FP_PARAMETER_UNACCEPTABLE -1889

// opcode
#define FP_ALGOAPI_GET_VERSION 901
#define FP_ALGOAPI_IPP_OFF 980
#define FP_ALGOAPI_IPP_ET320 981

// FP_MERGE_ENROLL_ERROR_CODE
#define FP_MERGE_ENROLL_IAMGE_OK 1
#define FP_MERGE_ENROLL_FINISH 2
#define FP_MERGE_ENROLL_IMAGE_HIGHLY_SIMILARITY 3
#define FP_MERGE_ENROLL_REDUNDANT_ACCEPT 4
#define FP_MERGE_ENROLL_BAD_IMAGE -1
#define FP_MERGE_ENROLL_FEATURE_LOW -2
#define FP_MERGE_ENROLL_FAIL -3
#define FP_MERGE_ENROLL_OUT_OF_MEMORY -4
#define FP_MERGE_ENROLL_UNKNOWN_FAIL -5
#define FP_MERGE_ENROLL_IRREGULAR_CONTEXT -6
#define FP_MERGE_ENROLL_DUPLICATE_IMAGE_REMOVED -7
#define FP_MERGE_ENROLL_REDUNDANT_INPUT -8

//#define MAX_FEATURE_LEN 			(8*1024) // real = 7328 bytes
//#define MAX_ENROLL_TEMPLATE_LEN		(290*1024) // real = 293120 bytes

//#define MAX_FEATURE_LEN 			(20*1024) // real = 18348 bytes --> ET711
//#define MAX_ENROLL_TEMPLATE_LEN		(1200*1024) // real = 1100880 bytes --> ET711

#define MAX_FEATURE_LEN (24 * 1024)
// ET711 : 21,198 bytes
// ET713(A50) : 21,022 bytes
// ET713(A70) : 19,950 bytes

#define MAX_ENROLL_TEMPLATE_LEN (1500 * 1024)
// ET711 : 1,271,880 bytes
// ET713(A50) ( Max. sub-templates = 60 ): 1,272,120 bytes
// ET713(A70) ( Max. sub-templates = 52 ): 1,037,400 bytes

// param for enroll context
#define ENROLL_CTX_MIN_ENROLL_COUNT 1214
#define ENROLL_CTX_MAX_ENROLL_COUNT 1215
#define ENROLL_CTX_REDUNDANT_CHECK_START 1216
#define ENROLL_CTX_REDUNDANT_CONTINUOUS_BOUND 1217
#define ENROLL_CTX_EXTRACT_THRESHOLD 1221
#define ENROLL_CTX_GENERALIZATION_THRESHOLD 1223
#define ENROLL_CTX_DUPLICATE_THRESHOLD 1226
#define ENROLL_CTX_REDUNDANT_INPUT_THRESHOLD 1227
#define ENROLL_CTX_REDUNDANT_THRESHOLD 1228
#define ENROLL_CTX_REDUNDANT_IMAGE_POLICY 1229
#define ENROLL_CTX_REDUNDANT_LEVEL 1230
#define ENROLL_CTX_QUALITY_REJECT_LEVEL 1231
//#define ENROLL_CTX_DYNAMIC_THRESHOLD_ENABLE 	1232 // It is replaced by "ALGO_COF_PROTECT_MODE"

// enroll context information
#define ENROLL_CTX_ENROLL_PROGRESS 1251
#define ENROLL_CTX_PRE_IDX 1252
#define ENROLL_CTX_ENROLLED_COUNT 1254
// set ENROLL_CTX_REDUNDANT_IMAGE_POLICY
#define FP_REDUNDANT_POLICY_REJECT_IMAGE 0  // default
#define FP_REDUNDANT_POLICY_ACCPET_IMAGE 1

// param for matching algorithm setting
#define ALGO_COF_RESOLUTION 1510                   // Real DPI of input image ( Default = 550 )
#define ALGO_COF_PROTECT_MODE 1511                 // 0:off, 1:on ( Default = off )
#define ALGO_COF_ENABLE_MOIRE_SCORE_DISCOUNT 1512  // 0:off, 1:on (Default = on)

#ifdef __cplusplus
extern "C" {
#endif

enum AlgoAPIDeviceType {
    FP_ALGOAPI_MODE_UNKNOWN,
    FP_ALGOAPI_MODE_EGIS_ET300,
    FP_ALGOAPI_MODE_EGIS_ET310,
    FP_ALGOAPI_MODE_EGIS_ET320,
    FP_ALGOAPI_MODE_EGIS_ET505,
    FP_ALGOAPI_MODE_EGIS_ET510,
    FP_ALGOAPI_MODE_EGIS_ET511,
    FP_ALGOAPI_MODE_EGIS_ET516,
    FP_ALGOAPI_MODE_EGIS_ET516_MLOGO,
    FP_ALGOAPI_MODE_EGIS_ET512,
    FP_ALGOAPI_MODE_EGIS_ET519,
    FP_ALGOAPI_MODE_EGIS_ET702,
    FP_ALGOAPI_MODE_EGIS_ET517,
    FP_ALGOAPI_MODE_EGIS_ET518,
    FP_ALGOAPI_MODE_EGIS_ET538,
    FP_ALGOAPI_MODE_SUPREMA1,
    FP_ALGOAPI_MODE_SUPREMA2,
    FP_ALGOAPI_MODE_EGIS_ET725,
    FP_ALGOAPI_MODE_SUPREMA4,
    FP_ALGOAPI_MODE_EGIS_ET735,
    FP_ALGOAPI_MODE_EGIS_ET727,
    FP_ALGOAPI_MODE_EGIS_ET728,
    FP_ALGOAPI_MODE_EGIS_ET736_EC3_NEW,
    FP_ALGOAPI_MODE_EGIS_ET711,
    FP_ALGOAPI_MODE_EGIS_ET713,
};

typedef struct _verify_init_ {
    unsigned char** pEnroll_temp_array;
    int* enroll_temp_size_array;
    int enroll_temp_number;
} VERIFY_INIT;

typedef struct _verify_info_ {
    unsigned char* pFeat;
    int feat_size;
    unsigned char* pEnroll_temp;
    int enroll_temp_size;
    int matchScore;
    int matchindex;
    int isLearningUpdate;
    int extract_ret;
    int* match_score_array;  // matching scores for each enrolled template
} VERIFY_INFO;
// verfiy function : do matching & learning

// Initialization
int algorithm_initialization(unsigned char* decision_data, int decision_data_len, int reset_flag);
int algorithm_initialization_by_sensor(unsigned char* decision_data, int decision_data_len,
                                       int sensor_type, int reset_flag);
void algorithm_uninitialization(unsigned char* decision_data, int decision_data_len);

// Enroll
void* enroll_init(void);
int set_enroll_context(void* ctx, int param, int value);
int get_enroll_context(void* ctx, int param, int* value);

int enroll(unsigned char* img, int width, int height, void* ctx, int* percentage);
int enroll_practice(unsigned char* img, int width, int height, void* ctx, int forcibly);
int enroll_setDB(void* ctx, VERIFY_INIT* verify_init_data);
unsigned char* get_enroll_template(void* ctx, int* len);

int enroll_uninit(void* ctx);

// Functions for matching algorithm setting
int set_algorithm_config(int param, int value);
int get_algorithm_config(int param, int* value);

// For ET725 & ET735
void generate_calib(unsigned short** input_images, int image_num, int width, int height,
                    unsigned short* output_image);
void input_seq_proc(unsigned short* img, unsigned short* calib, int width, int height, int num,
                    int enable_quality_check, int* quality, int* quality_img, int* quality_partial,
                    unsigned char* outimg, int mode, int DC_offset_input, int DC_offset_calib,
                    int mode_sunlight);

// For ET711
void input_seq_proc_New(unsigned short* img, unsigned short* calib, int width, int height, int num,
                        int integ_num, int enable_quality_check, int* quality, int* quality_partial,
                        unsigned char* outimg, int mode, int type_panel, int mode_bkgEst,
                        int* result_bkgEst);

// For ET713
// return = 0 : normal
// return = -1 : Invalid Fingerprint
// return = -2 : Partial Fingerprint
// return = -3 : Strong Moire Pattern
void seq_proc_update_init(void);
int seq_proc_update_backup(int width, int height);
int seq_proc_update_recover(int width, int height);
int input_seq_proc_New_v2(unsigned short* img, unsigned short* calib, int width, int height,
                          int num, int trymatch_num, int integ_num, int enable_quality_check,
                          int* quality, int* quality_partial, unsigned char* outimg, int mode);

// For ET727 & ET736
void generate_calib_8bit(unsigned char** input_images, int image_num, int width, int height,
                         unsigned char* output_image);
void input_seq_proc_8bit(unsigned char* img, unsigned char* calib, int width, int height, int num,
                         int enable_quality_check, int* quality, int* quality_img,
                         unsigned char* outimg, int mode);
void input_seq_proc_8bit_BIG(unsigned char* img, unsigned char* calib, int width, int height,
                             int num, unsigned char* outimg, int type_panel);

int extract_feature(unsigned char* img, int width, int height, unsigned char* feat, int* len,
                    int* quality);
int extract_feature_for_quality(unsigned char* img, int width, int height, unsigned char* feat,
                                int* len, int* quality);

enum FPExtractResult {
    exOK,
    exFeatureLow,
    exBadImage,
    exNotImplement,
    exImageTooSmall,
    exEncryptFail
};

int verify_init(VERIFY_INIT* verify_init);

#if defined(FPSTUDIO)
int verify(VERIFY_INFO* verify_info, unsigned char* overlapinform,
           unsigned int size_of_overlapinform);
#else
int verify(VERIFY_INFO* verify_info);
#endif
int quick_verify(VERIFY_INFO* verify_info);
int lowQty_verify(VERIFY_INFO* verify_info);

int verify_uninit(void);

int set_accuracy_level(int level);

int set_confidence_level(int added_level);

int algorithm_do_other(int opcode, unsigned char* indata, unsigned char* outdata);

int get_size_of_parameters();

#ifdef PRINT_PARAMETERS
int get_parameters(void* params);
int set_parameters(void* params);
#endif

typedef struct _algo_api_info_ {
    char algo_api_version[30];
    char matchlib_version[30];
} ALGO_API_INFO;

#ifdef __cplusplus
}
#endif

#endif