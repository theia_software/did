#include <stdio.h>

#include "EgisAlgorithmAPI.h"
#include "EgisAlgorithmApiV1.h"
#include "EgisAlgorithmApiV2.h"
#ifndef __PC_x86_64__
#include "algomodule.h"
#include "common_definition.h"
#include "core_config.h"
#include "fp_definition.h"
#endif
#include "egis_definition.h"
#include "plat_log.h"
#include "type_definition.h"
#include "plat_mem.h"
#include "plat_time.h"

#include "fp_custom.h"
#include "plat_file.h"
#include "verify_accuracy.h"
#include "detect_fa_attack.h"

#ifdef SUPPORT_SAVE_DECISION_DATA
#include "image_cut.h"
#define FILENAME_DECISION_DATA FILE_DATA_BASE "template/decision_data.bin"
#endif

#define MAX_VERIFY_COUNT 120

static void* g_algo_ctx = NULL;

static int g_ctx_id = 0;
struct enroll_context {
    int id;
    struct enroll_info_v2 info;
};
//static struct enroll_context g_enroll_context = {0};
static void *g_enroll_context = 0;
static int g_max_enroll_count = 0;
static int g_enroll_redundant_level = 80;
static int g_not_match_count = 0;
static int g_dpi = 0;
static int g_verify_enroll_number = 0;


static BOOL g_against_FA_attack_mode = FALSE;
static BOOL g_prev_temperature = 25;

typedef struct {
    int total;
    int fail;
} verify_count_t;

static verify_count_t g_verify_count = {
    .total = 0,
    .fail = 0,
};

void get_image_class_type_num(void* ctx)
{
    int value = 0;
    get_required_minimum_nbr_of_subtemplates_v2(ctx, FP_IMAGE_TYPE_NORMAL, &value);
    ex_log(LOG_DEBUG,"Algo FP_IMAGE_TYPE_NORMAL min= %d\n", value);
    get_required_minimum_nbr_of_subtemplates_v2(ctx, FP_IMAGE_TYPE_ENROLL, &value);
    ex_log(LOG_DEBUG,"Algo FP_IMAGE_TYPE_ENROLL min= %d\n", value);
    //get_required_minimum_nbr_of_subtemplates_v2(ctx, FP_IMAGE_TYPE_DRY, &value);
    //ex_log(LOG_DEBUG,"Algo FP_IMAGE_TYPE_DRY min= %d\n", value);
}

void get_image_class_type_num_from_template(void* ctx, unsigned char *template, int template_size)
{
    int value = 0;
    get_nbr_of_subtemplates_from_image_class(ctx, template, template_size, FP_IMAGE_TYPE_NORMAL, &value);
    ex_log(LOG_DEBUG,"%s FP_IMAGE_TYPE_NORMAL = %d\n", __func__, value);
    get_nbr_of_subtemplates_from_image_class(ctx, template, template_size, FP_IMAGE_TYPE_ENROLL, &value);
    ex_log(LOG_DEBUG,"%s FP_IMAGE_TYPE_ENROLL = %d\n",__func__, value);
    //get_nbr_of_subtemplates_from_image_class(ctx, template, template_size, FP_IMAGE_TYPE_DRY, &value);
    //ex_log(LOG_DEBUG,"%s FP_IMAGE_TYPE_DRY = %d\n",__func__, value);
    get_nbr_of_subtemplates_from_image_class(ctx, template, template_size, FP_IMAGE_TYPE_LEARNING_QUEUE, &value);
    ex_log(LOG_DEBUG,"%s FP_IMAGE_TYPE_LEARNING_QUEUE = %d\n",__func__, value);
}


static BYTE ** create_border_mask(int size, int width, int height) {

    int ptr_len = sizeof(BYTE*)*height;
    BYTE *mem_buf = (BYTE*)plat_alloc(ptr_len + width*height);
    BYTE ** image = (BYTE**)mem_buf;
    if (!image) return NULL;
    mem_buf += ptr_len;
    for (int i = 0; i < height; i++, mem_buf += width) {
        image[i] = mem_buf;
    }
    if (image == NULL) return NULL;
    for (int row = 0; row< height; row++) {
        for (int column = 0; column< width; column++){
            if (row < size) image[row][column] = FP_IMAGE_MASK_BACKGROUND;
            else if (row >= size && (column < size || column >= width - size)) image[row][column] = FP_IMAGE_MASK_BACKGROUND;
            else if (row >= height - size) image[row][column] = FP_IMAGE_MASK_BACKGROUND;
            else image[row][column] = FP_IMAGE_MASK_FINGERPRINT;
        }
    }
#ifndef TZ_MODE
    int retSize = 0;
    int retval;
    unsigned int read_len = 0;
    char mask_file_path[PATH_MAX] = {0};
    sprintf(mask_file_path, "%s/maskborder_%d_%d_%d.bin", FILE_DATA_BASE,width,height,size);
    if (!plat_file_is_exist(mask_file_path)){
        retSize = plat_save_file(mask_file_path, image[0], ptr_len + width*height);
        if (retSize <= 0) {
            ex_log(LOG_ERROR, "%s v2, failed to write %s", __func__, mask_file_path);
        }
    }
#endif
    return image;
}

int algorithm_initialization_by_sensor(unsigned char* decision_data, int decision_data_len,
                                       int sensor_type, int reset_flag) {
    //g_dpi = 552;
    //easy_mode_reset();
    //FA_attack_detect_reset();
//    switch (sensor_type) {
//        case FP_ALGOAPI_MODE_EGIS_ET713:
//            sensor_type = FP_ALGOAPI_MODE_EGIS_ET713_2Px;
//            break;
//        case FP_ALGOAPI_MODE_EGIS_ET701_:
//            sensor_type = FP_ALGOAPI_MODE_EGIS_ET701;
//            break;
//        case FP_ALGOAPI_MODE_EGIS_ET702_:
//#if defined(VENDOR_EGIS)
//            sensor_type = FP_ALGOAPI_MODE_EGIS_ET702;
//#elif defined(VENDOR_HW)
//            sensor_type = FP_ALGOAPI_MODE_EGIS_ET702_CH1M30;
//#elif defined(VENDOR_LGE)
//            sensor_type = FP_ALGOAPI_MODE_EGIS_ET702_CL1MH2_C230;
//#elif defined(VENDOR_CS1)
//            sensor_type = FP_ALGOAPI_MODE_EGIS_ET702;
//#endif
//            g_dpi = 508;
//            break;
//        case FP_ALGOAPI_MODE_EGIS_ET715:
//            sensor_type = FP_ALGOAPI_MODE_EGIS_ET715_3Px;
//            break;
//        case FP_ALGOAPI_MODE_EGIS_ET719:
//            sensor_type = FP_ALGOAPI_MODE_EGIS_ET713_2Px;
//            break;
//        case FP_ALGOAPI_MODE_EGIS_EL720A:
//            sensor_type = FP_ALGOAPI_MODE_EGIS_ET713_2Px;
//            break;
//        case FP_ALGOAPI_MODE_EGIS_ET713_2Px:
//        case FP_ALGOAPI_MODE_EGIS_ET713_3Px:
//        case FP_ALGOAPI_MODE_EGIS_ET715_3Px:
//        case FP_ALGOAPI_MODE_EGIS_ET715_3PE:
//        case FP_ALGOAPI_MODE_EGIS_ET715_3PF:
//            break;
//        default:
//            ex_log(LOG_ERROR, "unsupported sensor type");
//            return FP_ERR;
//    }
    ex_log(LOG_DEBUG, "%s v2, sensor type 0x%x", __func__, sensor_type);
#ifdef SUPPORT_SAVE_DECISION_DATA
    int retval;
    unsigned int read_len = 0;
    ex_log(LOG_DEBUG, "%s v2, allocated len=%d", __func__, decision_data_len);
    retval = plat_load_file(FILENAME_DECISION_DATA, decision_data,
                            decision_data_len, &read_len);
    if (retval == PLAT_FILE_NOT_EXIST || retval <= 0) {
        ex_log(LOG_ERROR, "%s v2 %s is not exist! %d", __func__,
               FILENAME_DECISION_DATA, retval);
    }
    uint32_t checksum = checksum_calculation(decision_data, read_len);
    ex_log(LOG_DEBUG, "%s v2, decision_data read_len=%d, checksum=%X", __func__,
           read_len, checksum);
    int ret = algorithm_initialization_v2(
        &g_algo_ctx, read_len > 0 ? decision_data : NULL,
        read_len > 0 ? read_len : 0, sensor_type);
#else
#ifdef __PC_x86_64__
    int ret = algorithm_initialization_v2(&g_algo_ctx, decision_data,
                                          decision_data_len, sensor_type);
#else
    int ret = algorithm_initialization_v2(&g_algo_ctx, NULL, 0, sensor_type);
#endif
#endif
    if (ret == 0 && g_algo_ctx != NULL) {
        set_algo_config_v2(g_algo_ctx, FP_OP_RESOLUTION, g_dpi);

        //Enable SPD
        set_algo_config_v2(g_algo_ctx, FP_OP_ENABLE_SPD, FP_ENABLE_SPD);
        //Set SPD learning threshold
        set_algo_config_v2(g_algo_ctx, ALGO_CONFIG_SPD_LEARN_THRESHOLD, 40);
        //Set SPD learning speed
        set_algo_config_v2(g_algo_ctx, ALGO_CONFIG_SPD_LEARN_SPEED, 100);
        //Set SPD reject threshold which determines reject this image
        //directly without verification or not
        set_algo_config_v2(g_algo_ctx, ALGO_CONFIG_SPD_REJECT_THRESHOLD, 70);
        //Set SPD ROI center x
        set_algo_config_v2(g_algo_ctx, ALGO_CONFIG_SPD_ROI_CENTER_X, 100);
        //Set SPD ROI center y
        set_algo_config_v2(g_algo_ctx, ALGO_CONFIG_SPD_ROI_CENTER_Y, 100);
        //Set SPD ROI radius
        set_algo_config_v2(g_algo_ctx, ALGO_CONFIG_SPD_ROI_RADIUS, 90);

        set_accuracy_level_v2(g_algo_ctx, FAR_RATIO);
        set_algo_config_v2(g_algo_ctx, FP_OP_ENROLL_QUALITY_REJECT_LEVEL, 20);
        set_algo_config_v2(g_algo_ctx, FP_OP_ENROLL_REDUNDANT_LEVEL, 80);
        set_algo_config_v2(g_algo_ctx, FP_OP_SET_FIRST_N_LOWER_FAR, 0);
        ex_log(LOG_INFO, "%s OK, RESOLUTION %d, FAR_RATIO %d", __func__, g_dpi, FAR_RATIO);
        return FP_OK;
    } else {
        ex_log(LOG_ERROR, "%s failed %d", __func__, ret);
        return FP_ERR;
    }
}
void algorithm_uninitialization(unsigned char* decision_data, int decision_data_len) {
    if (g_algo_ctx == NULL) {
        return;
    }
    int ret = algorithm_uninitialization_v2(g_algo_ctx, &decision_data, &decision_data_len);
    ex_log(ret == 0 ? LOG_DEBUG : LOG_ERROR, "%s return %d, decision_data_len=%d", __func__, ret,
           decision_data_len);
    g_algo_ctx = NULL;

    // TODO: pass decision_data for storing
#ifdef SUPPORT_SAVE_DECISION_DATA
    uint32_t checksum = checksum_calculation(decision_data, decision_data_len);
    ex_log(LOG_DEBUG, "%s v2, decision_data len=%d, checksum=%X", __func__, decision_data_len, checksum);
    int retSize = 0;
    retSize = plat_save_file(FILENAME_DECISION_DATA, decision_data, decision_data_len);
    if (retSize <= 0) {
        ex_log(LOG_ERROR, "%s v2, failed to write %s", __func__, FILENAME_DECISION_DATA);
    }
#endif
    //PLAT_FREE(decision_data);
    return;
}

void* enroll_init(void) {
#ifdef VENDOR_CS1
    _clear_sys_sol_flag();
#endif
    //if (g_enroll_context.id != 0) {
    //    ex_log(LOG_ERROR, "%s is repeated. id=%d", __func__, g_enroll_context.id);
    //    return NULL;
    //}
    if (g_algo_ctx == NULL) {
        ex_log(LOG_ERROR, "%s g_algo_ctx NULL", __func__);
        return NULL;
    }
    int ret = enroll_init_v2(g_algo_ctx, MAX_TEMPLATE_SIZE);
    if (ret == 0) {
        g_enroll_context = get_enroll_ctx_ptr();
        //memset(&g_enroll_context, 0, sizeof(g_enroll_context));
        g_ctx_id++;
        //g_enroll_context.id = g_ctx_id;
        //ex_log(LOG_DEBUG, "%s, ctx_id=%d", __func__, g_enroll_context.id);
        //return &g_enroll_context;
#ifndef __PC_x86_64__
        g_enroll_redundant_level = core_config_get_int(INI_SECTION_ENROLL, KEY_REDUNDANT_LEVEL, g_enroll_redundant_level);
        int enroll_redundant_check_number =
                core_config_get_int(INI_SECTION_ENROLL, KEY_REDUNDANT_CHECK_PREV_IMAGE_NUMBER, 1);
#else  //For the condition of G7 evtool
        int enroll_redundant_check_number = 1;
#endif
        set_enroll_context_v2(g_enroll_context, ENROLL_CTX_REDUNDANT_CHECK_NUMBER,
                enroll_redundant_check_number);
        return g_enroll_context;
    } else {
        return NULL;
    }
}

int set_enroll_context(void* ctx, int param, int value) {
    int ret = FP_OK;
    ex_log(LOG_DEBUG, "%s, param %d, value %d", __func__, param, value);
    switch (param) {
        case ENROLL_CTX_MAX_ENROLL_COUNT:
            g_max_enroll_count = value;
            ret = set_enroll_context_v2(ctx, ENROLL_CTX_MAX_ENROLL_COUNT, value);
            if (ret != FP_OK)
                return ret;
            //set_required_minimum_nbr_of_subtemplates_v2(g_algo_ctx, FP_IMAGE_TYPE_ENROLL, g_max_enroll_count);
#ifndef __PC_x86_64__
            //int ori_enroll_num =
            //        core_config_get_int(INI_SECTION_ENROLL, 
            //                            KEY_KEEP_ENROLL_IMAGE_FOR_LEARNING, g_max_enroll_count);
            int ori_enroll_num = g_max_enroll_count;
#else
            int ori_enroll_num = g_max_enroll_count;
#endif
            ex_log(LOG_DEBUG, "ori_enroll_num %d", ori_enroll_num);
            ret = set_enroll_context_v2(ctx, ENROLL_CTX_LR_ORI_ENROLL_NUM, ori_enroll_num);
            return ret;
    //    case ENROLL_CTX_QUALITY_REJECT_LEVEL:
    //        ret = set_algo_config_v2(g_algo_ctx, FP_OP_ENROLL_QUALITY_REJECT_LEVEL, value);
    //        break;
        case ENROLL_CTX_REDUNDANT_IMAGE_POLICY:
            param = ENROLL_CTX_REDUNDANT_LEVEL;
            if (value == FP_REDUNDANT_POLICY_REJECT_IMAGE) {
                value = g_enroll_redundant_level;
            } else {
                // 100 : The image will be reject when it is more than 100% similar to the previous
                // image.
                //       In other words, most of time it is accepted.
                value = 100;
            }
            break;
        default:
            break;
    }
    ret = set_enroll_context_v2(ctx, param, value);
    return ret;
}

int get_enroll_context(void* ctx, int param, int* value) {
    int ret;

    //ret = get_algo_config_v2(ctx, param, value);
    ret = get_enroll_context_v2(ctx, param, value);
    //switch (param) {
    //    case ENROLL_CTX_MAX_ENROLL_COUNT:
    //        *value = g_max_enroll_count;
    //        break;
    //    case ENROLL_CTX_ENROLLED_COUNT:
    //        *value = g_enroll_context.info.count;
    //        break;
    //    default:
    //        ex_log(LOG_ERROR, "%s, [%d] value=%d is not supported", __func__, param, value);
    //        break;
    //}
    return ret;
}

int enroll_setDB(void* ctx, VERIFY_INIT* verify_init_legacy) {
    struct verify_init_v2 verify_init_data;

    if (sizeof(VERIFY_INIT) != sizeof(verify_init_data)) {
        ex_log(LOG_ERROR, "%s different structure!", __func__);
        return FP_ERR;
    }
    //memcpy(&verify_init_data, verify_init_legacy, sizeof(verify_init_data));
    convert_verify_init_v1_to_v2(verify_init_legacy, &verify_init_data);
    //int ret = enroll_duplicate_check_v2(g_algo_ctx, &verify_init_data);
    int ret = enroll_duplicate_check_v2(ctx, &verify_init_data);
    convert_verify_init_v2_to_v1(&verify_init_data, verify_init_legacy);
    ex_log(LOG_DEBUG, "enroll_duplicate_check_v2, ret=%d", ret);
    return ret;
}

int enroll(unsigned char* img, int width, int height, void* e_ctx, int* percentage) {
    RBS_CHECK_IF_NULL(g_algo_ctx, EGIS_OUT_OF_MEMORY);
    //struct enroll_info_v2* p_enroll_info = &g_enroll_context.info;
    struct enroll_info_v2 enroll_info;
    struct enroll_info_v2* p_enroll_info = &enroll_info;
    p_enroll_info->image = img;
    p_enroll_info->width = width;
    p_enroll_info->height = height;
    p_enroll_info->image_class = FP_IMAGE_TYPE_ENROLL;

    int level = -1;
    get_enroll_context(e_ctx, ENROLL_CTX_REDUNDANT_LEVEL, &level);
    ex_log(LOG_DEBUG, "%s, ENROLL_CTX_REDUNDANT_LEVEL=%d", __func__, level);

    //int ret = enroll_v2(g_algo_ctx, p_enroll_info);
    int ret = enroll_v2(e_ctx, p_enroll_info);
    ex_log(LOG_DEBUG, "%s, ret=%d, count=%d, %d:%d. percentage %d, spd = %d"
            " img_qty = %d",
            __func__, ret,
           p_enroll_info->count, p_enroll_info->width, p_enroll_info->height,
           p_enroll_info->percentage, 
           p_enroll_info->image_quality_values.static_pattern_area,
           p_enroll_info->image_quality_values.fingerprint_quality);
    //TODO?
    //if (ret == FP_ENROLL_DUPLICATE_IMAGE_REMOVED) {
    //    ret = FP_DUPLICATE;
    //}
#ifndef __PC_x86_64__
    EGIS_IMAGE_KEEP_PARAM(sp_area, p_enroll_info->image_quality_values.static_pattern_area);
#endif
    if (p_enroll_info->percentage >= 100) {
        //TODO
        easy_mode_reset();
        FA_attack_detect_reset();
    }
    return ret;
}

unsigned char* get_enroll_template(void* ctx, int* len) {
    unsigned char* template_algo = NULL;
    //get_enroll_template_v2(g_algo_ctx, &template_algo, len);
    get_enroll_template_v2(ctx, &template_algo, len);
    return template_algo;
}

int enroll_uninit(void* e_ctx) {
    //int ret = enroll_uninit_v2(g_algo_ctx);
    int ret = enroll_uninit_v2(e_ctx);
    ex_log(ret == 0 ? LOG_DEBUG : LOG_ERROR, "%s return %d", __func__, ret);
    //memset(&g_enroll_context, 0, sizeof(g_enroll_context));
    g_enroll_context = NULL;
    return ret;
}

int verify_init(VERIFY_INIT* verify_init_legacy) {
    struct verify_init_v2 verify_init_data;

    //if (sizeof(VERIFY_INIT) != sizeof(verify_init_data)) {
    //    ex_log(LOG_ERROR, "%s different structure!", __func__);
    //    return FP_ERR;
    //}
    //memcpy(&verify_init_data, verify_init_legacy, sizeof(verify_init_data));
    convert_verify_init_v1_to_v2(verify_init_legacy, &verify_init_data);
    int ret = verify_init_v2(g_algo_ctx, &verify_init_data);
    convert_verify_init_v2_to_v1(&verify_init_data, verify_init_legacy);

    g_verify_enroll_number = verify_init_data.enroll_temp_number;
    ex_log(LOG_DEBUG, "g_verify_enroll_number = %d", g_verify_enroll_number);
    return ret;
}

static unsigned char* g_img_extracted = NULL;
static int g_img_width = 0;
static int g_img_height = 0;
int extract_feature(unsigned char* img, int width, int height, unsigned char* feat, int* len,
                    int* quality) {
    int ret;
    g_img_extracted = img;
    g_img_width = width;
    g_img_height = height;
    ret = extract_feature_v2(g_algo_ctx, img, width, height, FP_IMAGE_TYPE_NORMAL, &feat, len);
    //*quality = 100;
    if (g_algo_ctx)
        memcpy(quality, g_algo_ctx, sizeof(int));
#ifndef __PC_x86_64__
    EGIS_IMAGE_KEEP_PARAM(extract_qty, *quality);
#endif
    return ret;
}
int verify(VERIFY_INFO* verify_info) {
    struct verify_info_v2_1 verify_info2 = {0};
    int ret;
    int temperature = 25; //Set 25 degree as default temperature
    const int LOW_TEMP_THRESHOLD = 10;
    int img_class = FP_IMAGE_TYPE_NORMAL;
    ex_log(LOG_DEBUG, "verify_info->feat_size %d", verify_info->feat_size);

#ifndef __PC_x86_64__
    temperature = EGIS_IMAGE_OBTAIN_PARAM(temperature);
#endif
    ex_log(LOG_DEBUG, "%s, te=%d", __func__, temperature);

    if (g_img_extracted == NULL || g_img_width == 0 || g_img_height == 0) {
        ex_log(LOG_ERROR, "%s, invalid image", __func__);
        return FP_MATCHFAIL;
    }
    if (verify_info->is_image_from_queue == 1) {
        verify_info->try_count = 0;
        img_class = FP_IMAGE_TYPE_LEARNING_QUEUE;
    }
    verify_info2.try_match_count = verify_info->try_count;
    verify_info2.final_try_match = verify_info->final_try;
    verify_info2.image.pixels = g_img_extracted;
    verify_info2.image.width = g_img_width;
    verify_info2.image.height = g_img_height;
    verify_info2.feat = NULL;
    verify_info2.len = 0;
    verify_info2.match_score = 0;
    verify_info2.match_index = -1;
    verify_info2.is_learning_update = FALSE;
    verify_info2.enroll_temp = NULL;
    verify_info2.enroll_temp_size = 0;
    verify_info2.match_threshold = 0;
    verify_info2.match_score_array = verify_info->match_score_array;
    verify_info2.image.class = img_class;
    verify_info2.latency_adjustment = 0;
    verify_info2.image.resolution = g_dpi;
    verify_info2.image_type_flag = 0;

#ifndef __PC_x86_64__
    int verify_mask_border_size = core_config_get_int(INI_SECTION_VERIFY, KEY_VERIFY_IMAGE_MASK_BORDER_SIZE , INID_VERIFY_IMAGE_MASK_BORDER_SIZE);
    EGIS_IMAGE_KEEP_PARAM(verify_mask_border_size, verify_mask_border_size);
    BYTE **verify_mask_border = NULL;
    if (verify_mask_border_size > 0 && verify_mask_border_size * 2 < g_img_width && verify_mask_border_size * 2 < g_img_height){
        verify_mask_border = create_border_mask(verify_mask_border_size, g_img_width, g_img_height);
        verify_info2.image.mask = verify_mask_border[0];
    }
    else
        verify_info2.image.mask = NULL;

    int ret_update = easy_mode_do_update(verify_info2.try_match_count, get_is_FA_attack(),
                        g_verify_enroll_number, temperature);
#endif

#ifdef VENDOR_CS1
    if (ret_update == EASY_MODE_UPDATE_SKIP_MATCHER) {
        plat_sleep_time(100);
        ret = FP_MATCHFAIL;
        goto SKIP_MATCH;
    }
#else
    if (get_is_FA_attack() == TRUE) {  // FA protection
        ex_log(LOG_DEBUG, "%s, atk try_count=%d", __func__, verify_info->try_count);
        if (verify_info->try_count == 0) {
            set_accuracy_level_v2(g_algo_ctx, FAR_RATIO_FA_PROTECTION);
            ex_log(LOG_DEBUG, "%s, arr prot", __func__);
        } else {
            ex_log(LOG_DEBUG, "%s, skip match", __func__);
            plat_sleep_time(100);
            ret = FP_MATCHFAIL;
            goto SKIP_MATCH;
        }
    } else if (temperature < LOW_TEMP_THRESHOLD) {  // low temperature
        set_accuracy_level_v2(g_algo_ctx, FAR_RATIO_LOW_TEMP_LEVEL2);
        ex_log(LOG_DEBUG, "%s, te arr set to %d", __func__, FAR_RATIO_LOW_TEMP_LEVEL2);
    } else if (easy_mode_get_level(FAR_RATIO) != FAR_RATIO) {   // easy mode 1 & 2
        set_accuracy_level_v2(g_algo_ctx, easy_mode_get_level(FAR_RATIO));
    } else {
        set_accuracy_level_v2(g_algo_ctx, FAR_RATIO);
        ex_log(LOG_DEBUG, "%s, arr normal %d", __func__, FAR_RATIO);
    }
#endif
    g_prev_temperature = temperature;
    ex_log(LOG_DEBUG, "ptmp %d tmp %d", g_prev_temperature, temperature);
    convert_verify_info_v1_to_v2_1(verify_info, &verify_info2);
    ret = verify_v2_1(g_algo_ctx, &verify_info2);
    ex_log(LOG_DEBUG, "verify_info2.match_index %d ret %d", verify_info2.match_index, ret);
    convert_verify_info_v2_1_to_v1(&verify_info2, verify_info);
#ifndef __PC_x86_64__
    EGIS_IMAGE_KEEP_PARAM(match_image_class, verify_info2.match_image_class);
#endif

SKIP_MATCH:
    verify_info->matchScore = verify_info2.match_score;
    if (ret == FP_MATCHOK) {
        verify_info->matchindex = verify_info2.match_index;
        g_verify_count.total++;
        FA_attack_detect_add(FALSE, verify_info2.image_quality_values.fingerprint_quality);
#ifdef VENDOR_CS1
        fcheck_clear_system_solution();
#endif
    } else {
        verify_info->matchindex = -1;
        if (verify_info->final_try) {
            g_verify_count.fail++;
            g_verify_count.total++;
            FA_attack_detect_add(TRUE, verify_info2.image_quality_values.fingerprint_quality);
#ifdef VENDOR_CS1
            fcheck_set_verify_fail_count();
#endif
        }
    }

#ifndef VENDOR_CS1
    FA_attack_detect_update((easy_mode_is_on() || temperature < LOW_TEMP_THRESHOLD));
#endif

    ex_log(LOG_DEBUG, "%s, atk = %d",__func__ , get_is_FA_attack());
    ex_log(LOG_DEBUG, "%s, tc=%d, fc=%d", __func__, g_verify_count.total,
           g_verify_count.fail);

    if (g_verify_count.total == MAX_VERIFY_COUNT) {
        g_verify_count.total = 0;
        g_verify_count.fail = 0;
    }

    ex_log(LOG_DEBUG, "%s, nc %d, eqty=%d, area=%d", __func__, g_not_match_count,
           verify_info2.image_quality_values.fingerprint_quality,
           verify_info2.image_quality_values.fingerprint_area);
    ex_log(LOG_DEBUG, "%s, mt = %d", __func__, verify_info2.match_threshold);
#ifndef __PC_x86_64__
    EGIS_IMAGE_KEEP_PARAM(match_threshold, verify_info2.match_threshold);
    EGIS_IMAGE_KEEP_PARAM(extract_qty, verify_info2.image_quality_values.fingerprint_quality);
#endif
    verify_info->isLearningUpdate = verify_info2.is_learning_update;
    verify_info->enroll_temp_size = verify_info2.enroll_temp_size;
    verify_info->pEnroll_temp = verify_info2.enroll_temp;
    verify_info->static_pattern_area = verify_info2.image_quality_values.static_pattern_area;
#ifndef __PC_x86_64__
    EGIS_IMAGE_KEEP_PARAM(sp_area, verify_info->static_pattern_area);
#endif
    ex_log(LOG_DEBUG, "%s, spd %d", __func__, verify_info->static_pattern_area);
    get_image_class_type_num_from_template(g_algo_ctx, verify_info->pEnroll_temp, verify_info->enroll_temp_size);

    g_img_extracted = NULL;
    g_img_width = 0;
    g_img_height = 0;
#ifndef __PC_x86_64__
    if (verify_mask_border_size > 0 && verify_mask_border != NULL){
        PLAT_FREE(verify_mask_border);
    }
#endif
    return ret;
}

int verify_uninit(void) {
    return verify_uninit_v2(g_algo_ctx);
}

#define OUT_DATA_SIZE 40
int algorithm_do_other(int opcode, unsigned char* indata, unsigned char* outdata) {
    ex_log(LOG_DEBUG, "wrapper algorithm_do_other opcode = %d", opcode);
    int ret = FP_OK;
    int in_data_size = 0;
    int out_data_size = sizeof(ALGO_API_INFO);
    if (opcode == FP_ALGOAPI_GET_VERSION)
    ret = algorithm_do_other_v2(g_algo_ctx, opcode, indata, in_data_size, outdata, &out_data_size);
    //switch (opcode) {
    //    case FP_OP_GET_VERSION_V2: {
    //        int out_size = OUT_DATA_SIZE;
    //        BYTE* out_data = plat_alloc(OUT_DATA_SIZE);
    //        ret = algorithm_do_other_v2(g_algo_ctx, opcode, NULL, 0, out_data, &out_size);
    //        ex_log(LOG_DEBUG, "%s, [%d] ret=%d, out_size=%d", __func__, opcode, ret, out_size);
    //        out_data[OUT_DATA_SIZE - 1] = '\0';
    //        memcpy(outdata, out_data, OUT_DATA_SIZE);
    //        PLAT_FREE(out_data);
    //        break;
    //    }
    //    default:
    //        ex_log(LOG_ERROR, "%s, [%d] not supported", __func__, opcode);
    //        break;
    //}
    return ret;
}

int set_algo_config(int param, int value) {
//    switch (param) {
//#ifdef VENDOR_CS1
//        case VERIFY_CTX_ACCURACY_LEVEL:
//            ex_log(LOG_DEBUG, "set accuracy level %d", value);
//            int ret = set_accuracy_level_v2(g_algo_ctx, value);
//            ex_log(LOG_DEBUG, "set accuracy level ret = %d", ret);
//            return ret;
//#endif
//        case ENROLL_CTX_SET_CIRCLE_CENTRE_CX:
//            param = FP_OP_CENTROID_X;
//            break;
//        case ENROLL_CTX_SET_CIRCLE_CENTRE_CY:
//            param = FP_OP_CENTROID_Y;
//            break;
//        default:
//            ex_log(LOG_ERROR, "%s, unrecognized param %d, value %d", __func__, param, value);
//            return EGIS_COMMAND_FAIL;
//    }
    ex_log(LOG_DEBUG, "%s, param %d, value %d", __func__, param, value);
    int ret = set_algo_config_v2(g_algo_ctx, param, value);
    ex_log(ret == 0 ? LOG_VERBOSE : LOG_ERROR, "%s return %d", __func__, ret);
    return ret;
}

//void pb_free(void* ptr) {
//    PLAT_FREE(ptr);
//}
//
//char* pb_malloc(uint32_t size) {
//    return plat_alloc(size);
//}

int set_algorithm_config(int param, int value) {
    switch (param) {
        default:
            ex_log(LOG_VERBOSE, "set_algorithm_config skipped");
            break;
    }
    return EGIS_OK;
}

//void output_algo_log(LOG_LEVEL level, const char* tag, const char* file_name, const char* func,
//                     int line, const char* format, ...) {
//    // TODO: implement G5 print log
//}

//int egistec_clock() {
//    return 0;
//};

//int egistec_cpu_id() {
//    return 0;
//};

int get_fingerscore(unsigned char* image, int width, int height) {
    int g7_finger_score = 0;
    if (image == NULL || width == 0 || height == 0) {
        ex_log(LOG_ERROR, "%s, invalid image", __func__);
        return -1;
    }
    //TODO
    g7_finger_score = get_finger_score(g_algo_ctx, image, width, height);
    return g7_finger_score;
}

int learning(VERIFY_INFO *verify_info, int matchidx)
{
    int ret;
    struct verify_info_v2 verify_info_v2;

    convert_verify_info_v1_to_v2(verify_info, &verify_info_v2);
    ret = learning_v2(&verify_info_v2, matchidx);
    convert_verify_info_v2_to_v1(&verify_info_v2, verify_info);

    return ret;
}
