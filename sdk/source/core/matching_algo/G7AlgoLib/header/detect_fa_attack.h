#ifndef __DETECT_FA_ATTACK_H__
#define __DETECT_FA_ATTACK_H__

#define BOOL int
#ifndef FALSE
#define FALSE 0
#endif
#ifndef TRUE
#define TRUE 1
#endif

BOOL get_is_FA_attack();
void FA_attack_detect_add(BOOL isFail,int eqty);
void FA_attack_detect_reset();
void FA_attack_detect_update(BOOL isTestCase);
#endif