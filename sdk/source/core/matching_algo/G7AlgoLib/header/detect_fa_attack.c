#include "detect_fa_attack.h"
#include <stdint.h>
#include "plat_log.h"

#define MAX_VERIFY_COUNT 120
#define FA_ATTACK_COUNT_THRESHOLD 90
#define MAX_CONTINUOUS_FAIL_COUNT 30
#define FA_ATTACK_QTY_THRESHOLD 10
static int g_fail_count = 0;
static int g_fa_index = 0;
static int g_continuous_fail = 0;
static BOOL g_is_FA_attack_detect = FALSE;
static BOOL g_verify[MAX_VERIFY_COUNT] = {0};

static int g_eqtyTotal = 0;
static int g_eqty_count = 0;

#define IDX_NEXT(current_idx) ((current_idx + 1) % MAX_VERIFY_COUNT)

static void eqty_add(int eqty) {
    //g_eqtyTotal += eqty;
    //g_eqty_count++;
}
static int eqty_get_avg() {
    //if (g_eqty_count <= 0) {
    //    return 0;
    //}
    //int avg = g_eqtyTotal / g_eqty_count;
    //ex_log(LOG_DEBUG, "eqty avg = %d", avg);
    //return avg;
    return 0;
}
static void eqty_reset() {
    //g_eqtyTotal = 0;
    //g_eqty_count = 0;
}


static void printData() {
    //for (int i = 0; i < MAX_VERIFY_COUNT; ++i) {
    //    ex_log(LOG_DEBUG, "data [%d]  = %d", i, g_verify[i]);
    //}
}

BOOL get_is_FA_attack() {
    //return g_is_FA_attack_detect;
    return FALSE;
}

void FA_attack_detect_add(BOOL isFail,int eqty) {
    //if (isFail) {
    //    g_fail_count++;
    //    g_continuous_fail++;
    //    eqty_add(eqty);
    //} else {
    //    g_continuous_fail = 0;
    //    eqty_reset();
    //}

    //if (g_verify[g_fa_index] == TRUE) {
    //    g_fail_count--;
    //}

    //g_verify[g_fa_index] = isFail;
    //g_fa_index = IDX_NEXT(g_fa_index);

    //ex_log(LOG_DEBUG, "g_fail_count = %d ,g_fa_index = %d , g_continuous_fail = %d"
    //        , g_fail_count, g_fa_index , g_continuous_fail);

    //if (g_is_FA_attack_detect && !isFail){
    //    FA_attack_detect_reset();
    //}
}


void FA_attack_detect_update(BOOL isTestCase){
//    if ((g_continuous_fail == MAX_CONTINUOUS_FAIL_COUNT) || g_fail_count >= FA_ATTACK_COUNT_THRESHOLD) {
//#if defined(VENDOR_LGE)
//        g_is_FA_attack_detect = TRUE;
//#else
//        // set FA attack when average failed eqty > FA_ATTACK_QTY_THRESHOLD
//        g_is_FA_attack_detect = (eqty_get_avg() >= FA_ATTACK_QTY_THRESHOLD) ? TRUE : FALSE;
//#endif
//        ex_log(LOG_DEBUG, "%s, atk = %d, qty", __func__, g_is_FA_attack_detect, eqty_get_avg());
//        if (g_is_FA_attack_detect) {
//            if (isTestCase) {
//                g_is_FA_attack_detect = FALSE;
//                ex_log(LOG_DEBUG, "atk 0");
//            }
//        }
//    }
}


void FA_attack_detect_reset() {
    //for (int i = 0; i < MAX_VERIFY_COUNT; i++) {
    //    g_verify[i] = 0;
    //}
    //g_fa_index = 0;
    //g_fail_count = 0;
    //g_continuous_fail = 0;
    //g_is_FA_attack_detect = FALSE;
    //eqty_reset();
    //ex_log(LOG_DEBUG, "%s",__func__);
}
