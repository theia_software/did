#ifndef EGIS_ALGORITHM_API_V1_H
#define EGIS_ALGORITHM_API_V1_H

//typedef struct _verify_init_{
//	unsigned char**	pEnroll_temp_array;
//	int*	enroll_temp_size_array;
//	int 	enroll_temp_number;
//	//int*	bad_enroll_number_array;
//}VERIFY_INIT;
//
//typedef struct _verify_info_{
//	unsigned char*	pFeat;
//	int     feat_size;
//	unsigned char*	pEnroll_temp;
//	int	    enroll_temp_size;
//	int 	matchScore;
//	int 	matchindex;
//    int     matchResult;
//	int	isLearningUpdate;
//	int     extract_ret;
//	int		target_template;
//	int		learning_mode;
//	int		try_count;
//	int		final_try;
//}VERIFY_INFO;

int algorithm_initialization(unsigned char *decision_data, int decision_data_len,int reset_flag);
//int algorithm_initialization_by_sensor(unsigned char *decision_data, int decision_data_len, enum AlgoAPIDeviceType sensor_type, int reset_flag);
int algorithm_initialization_by_sensor(unsigned char *decision_data, int decision_data_len, int  sensor_type, int reset_flag);
void algorithm_uninitialization(unsigned char *decision_data, int decision_data_len);

void* enroll_init(void);
int set_enroll_context(void* ctx, int param, int value);
int get_enroll_context(void* ctx, int param, int *value);
int enroll( unsigned char *img, int width, int height, void *ctx, int *percentage);
int enroll_practice(unsigned char *img, int width, int height, void *ctx, int forcibly);
int enroll_setDB(void *e_ctx, VERIFY_INIT *verify_init_data);
unsigned char* get_enroll_template(void *ctx, int* len);
int enroll_uninit(void *ctx);

int extract_feature(unsigned char *img, int width, int height, unsigned char* feat, int* len, int* quality);

int verify_init(VERIFY_INIT *verify_init);
int verify(VERIFY_INFO *verify_info);
int verify_uninit(void);

int algorithm_do_other(int opcode, unsigned char* indata, unsigned char* outdata);

int set_algo_config(int param, int value);

int learning(VERIFY_INFO *verify_info, int matchidx);

//typedef struct _algo_api_info_{
//	char algo_api_version[30];
//	char matchlib_version[30];
//}ALGO_API_INFO;

#endif
