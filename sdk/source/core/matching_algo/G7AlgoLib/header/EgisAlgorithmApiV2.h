/******************************************************************************\
|*                                                                            *|
|*  EgisAlgorithmAPI.h                                                        *|
|*  Version: 3.0.1.33                                                         *|
|*  Revise Date: 2020/01/21                                                   *|
|*  Copyright (C) 2007-2019 Egis Technology Inc.                              *|
|*                                                                            *|
\******************************************************************************/

#ifndef EGIS_ALGORITHM_API_V2_H
#define EGIS_ALGORITHM_API_V2_H
#include "EgisAlgorithmAPI.h"

//#define FP_OK 0
//#define FP_MATCHOK 101
//
//#define FP_ERR -1000
//#define FP_TIMEOUT -1001
//#define FP_BADIMAGE -1002
//#define FP_FEATURELOW -1003
//#define FP_DUPLICATE -1004
//#define FP_NOTDUPLICATE -1005
//#define FP_MATCHFAIL -1006
//#define FP_NULL_DATA -1007
//#define FP_INVALID_FEATURE_LEN -1008
//#define FP_ALLOC_MEM_FAIL -1009
//#define FP_INVALID_FORMAT -1010
//#define FP_TUNING_FAIL -1011
//#define FP_INVALID_BUFFER_SIZE -1012
//#define FP_IO_ACCESS_FAIL -1013
//#define FP_NOT_TUNING -1014
//#define FP_CONNECT_SENSOR_FAIL -1015
//#define FP_CALL_FAILED -1016
//#define FP_NOT_CONNECT_SENSOR -1017
//#define FP_NULL_ENROLL_DATA -1018
//#define FP_NULL_FEATURE -1019
//#define FP_GETIMAGEFAIL -1020
//#define FP_DECRYPT_FAIL -1021
//#define FP_STATE_ERR -1022
//#define FP_INVALID_SENSOR_MODE -1023
//#define FP_PARAMETER_NOT_VALID -1888
//#define FP_PARAMETER_UNACCEPTABLE -1889
//
// op code for common usage
#define FP_OP_GET_VERSION_V2 0x385
#define FP_OP_MAX_ENROLL_COUNT ENROLL_CTX_MAX_ENROLL_COUNT
#define FP_OP_ENROLL_REDUNDANT_LEVEL 0x4CD
#define FP_OP_ENROLL_QUALITY_REJECT_LEVEL 0x4CF
/** Op code for setting a reject level for latent fingerprints during enrollment. 
  * Allowed values are 0 to UINT16_MAX, where 0 will reject all images and UINT16_MAX
  * will allow all images. */
#define FP_OP_ENROLL_LATENT_REJECT_LEVEL 0x4D0
#define FP_OP_RESOLUTION 0x5E6
#define FP_OP_CENTROID_X 0x5E7
#define FP_OP_CENTROID_Y 0x5E8
#define FP_OP_RADIUS 0x5E9
#define FP_OP_DYN_MASK_THRESHOLD 0x5EA
#define FP_OP_DYN_MASK_RADIUS 0x5EB

/* op code for vendor specific define */
#define FP_OP_VENDOR_BASE 0x1000

/* op code for precise biometrics */
#define FP_OP_VENDOR_PRECISE_BIOMETRICS_BASE 0x1000
#define FP_OP_ENABLE_SPD (FP_OP_VENDOR_PRECISE_BIOMETRICS_BASE + 1)

/**
 * 0:   NO lower FAR.
 * 1~N: first N verification with lower FAR
 * 
 */
#define FP_OP_SET_FIRST_N_LOWER_FAR (FP_OP_VENDOR_PRECISE_BIOMETRICS_BASE + 2)

#define FP_DISABLE_REDUNDANT 0
#define FP_ENABLE_REDUNDANT 1

// For FP_OP_ENROLL_REDUNDANT_IMAGE_POLICY
#define FP_DISABLE_SPD 0
#define FP_ENABLE_SPD 1

// For FP_OP_GET_VERSION_V2
#define FP_ALGO_VERSION_LEN (sizeof(ALGO_API_INFO))

//// FP_MERGE_ENROLL_ERROR_CODE
#define FP_ENROLL_IMAGE_OK 						FP_MERGE_ENROLL_IAMGE_OK
#define FP_ENROLL_FINISH 						FP_MERGE_ENROLL_FINISH
#define FP_ENROLL_IMAGE_HIGHLY_SIMILARITY 		FP_MERGE_ENROLL_IMAGE_HIGHLY_SIMILARITY
#define FP_ENROLL_REDUNDANT_ACCEPT 				FP_MERGE_ENROLL_REDUNDANT_ACCEPT
#define FP_ENROLL_BAD_IMAGE 					FP_MERGE_ENROLL_BAD_IMAGE
#define FP_ENROLL_FEATURE_LOW 					FP_MERGE_ENROLL_FEATURE_LOW
#define FP_ENROLL_FAIL 						    FP_MERGE_ENROLL_FAIL
#define FP_ENROLL_OUT_OF_MEMORY 				FP_MERGE_ENROLL_OUT_OF_MEMORY
#define FP_ENROLL_UNKNOWN_FAIL 					FP_MERGE_ENROLL_UNKNOWN_FAIL
#define FP_ENROLL_IRREGULAR_CONTEXT 			FP_MERGE_ENROLL_IRREGULAR_CONTEXT
#define FP_ENROLL_DUPLICATE_IMAGE_REMOVED 		FP_MERGE_ENROLL_DUPLICATE_IMAGE_REMOVED
#define FP_ENROLL_REDUNDANT_INPUT 				FP_MERGE_ENROLL_REDUNDANT_INPUT

#ifdef __cplusplus
extern "C" {
#endif

//enum algo_api_sensor_type {
//    FP_ALGOAPI_MODE_UNKNOWN = 0,
//    /*FP ALGORITHM API MODE for CAPACITOR SENSORS(0~0x400)*/
//    FP_ALGOAPI_MODE_EGIS_ET528= 0x30528,
//    /*FP ALGORITHM API MODE for OPTICAL SENSORS(0x40000~0xF0000)*/
//    FP_ALGOAPI_MODE_EGIS_ET713_2Px = 0x40000,	/* 200 X 200 support ET713_2PA */
//    FP_ALGOAPI_MODE_EGIS_ET713_2PA = 0x40001,	/* 200 X 200 552DPI */
//    FP_ALGOAPI_MODE_EGIS_ET713_2PA_S2PA4 = 0x40002, 	/* 200 X 200 552DPI */
//    FP_ALGOAPI_MODE_EGIS_ET713_2PA_NEW = 0x40003, /* 200 X 200 552DPI */
//    FP_ALGOAPI_MODE_EGIS_ET713_3Px = 0x40100, 	/* 200 X 200 support ET713_3PC, ET713_3PD */
//    FP_ALGOAPI_MODE_EGIS_ET713_3PC = 0x40101, 	/* 200 X 200 565DPI */
//    FP_ALGOAPI_MODE_EGIS_ET713_3PD = 0x40102, 	/* 200 X 200 500DPI (TabS)*/
//    FP_ALGOAPI_MODE_EGIS_ET713_3PG_S3PG1 = 0x40103, 	/* 200 X 200 705DPI */ 
//    FP_ALGOAPI_MODE_EGIS_ET713_3PG_S3PG2 = 0x40104, 	/* 200 X 200 705DPI */ 
//    FP_ALGOAPI_MODE_EGIS_ET713_3PG_S3PG3 = 0x40105, 	/* 200 X 200 720DPI */
//    FP_ALGOAPI_MODE_EGIS_ET713_3PG_S3PG4 = 0x40106, 	/* 200 X 200 705DPI */
//    FP_ALGOAPI_MODE_EGIS_ET713_3PG_CH1E_SV = 0x42104, 	/* 200 X 200 724DPI SOFT*/
//    FP_ALGOAPI_MODE_EGIS_ET713_3PG_CH1E_SB = 0x42107, /* 200 X 200 700DPI SOFT*/    
//    FP_ALGOAPI_MODE_EGIS_ET713_3PG_CH1J_SB = 0x42106, 	/* 200 X 200 743DPI SOFT*/
//    FP_ALGOAPI_MODE_EGIS_ET713_3PG_CH1E_H = 0x42105, 	/* 200 X 200 686DPI HARD*/
//    FP_ALGOAPI_MODE_EGIS_ET713_3PCLA_CH1LA = 0x42101, 	/* 200 X 200 530DPI */
//    FP_ALGOAPI_MODE_EGIS_ET713_3PCLB_CH1SEA = 0x42102,  /* 200 X 200 */
//    FP_ALGOAPI_MODE_EGIS_ET713_3PCLA_CH1LA_NEW = 0x42103, 	/* 200 X 200 530DPI */
//    FP_ALGOAPI_MODE_EGIS_ET713_3PC_CL1MH2 = 0x43101,  /* 200 X 200 550DPI */
//    FP_ALGOAPI_MODE_EGIS_ET713_3PCLC_CO1D151 = 0x44100,  /* 200 X 200 475DPI */
//    FP_ALGOAPI_MODE_EGIS_ET713_3PCLD_CO1A118 = 0x44101,  /* 200 X 200 510DPI */
//    FP_ALGOAPI_MODE_EGIS_ET713_3PC_CS3ZE2 = 0x45100,  /* 200 X 200 553DPI */
//    FP_ALGOAPI_MODE_EGIS_ET713_3PC_CV1CPD1960 = 0x46100,  /* 200 X 200 510DPI */
//    FP_ALGOAPI_MODE_EGIS_ET713_3PC_CL1MH2_CLT3 = 0x43102,  /* 200 X 200 550DPI */
//    FP_ALGOAPI_MODE_EGIS_ET715_3Px = 0x50100, 	/* 193 X 193 		*/
//    FP_ALGOAPI_MODE_EGIS_ET715_3PA = 0x50101, 	/* 193 X 193 576DPI */
//    FP_ALGOAPI_MODE_EGIS_ET715_3PE = 0x50102, 	/* 193 X 193 518DPI */
//    FP_ALGOAPI_MODE_EGIS_ET715_3PF = 0x50103, 	/* 193 X 193 700DPI */
//    FP_ALGOAPI_MODE_EGIS_ET715_3PF_S3PF5 = 0x50104,  /* 193 X 193 720DPI */
//    FP_ALGOAPI_MODE_EGIS_ET715_3PF_S3PF2 = 0x50105,  /* 193 X 193 740DPI */
//    FP_ALGOAPI_MODE_EGIS_ET715_3PF_CL1TIME = 0x53101,  /* 193 X 193 764DPI */
//    FP_ALGOAPI_MODE_EGIS_ET701 = 0x60000,    /* 139 X 116 578DPI */
//    FP_ALGOAPI_MODE_EGIS_ET702 = 0x70000,    /* 134 x 188 508DPI */
//    FP_ALGOAPI_MODE_EGIS_ET702_SXC210 = 0x70002,    /* 134 x 188 508DPI */
//    FP_ALGOAPI_MODE_EGIS_ET702_CH1M30 = 0x72101, /* 134 X 188 508DPI */
//    FP_ALGOAPI_MODE_EGIS_ET702_CL1MH2 = 0x73101,   /* 134 X 188 508DPI */
//    FP_ALGOAPI_MODE_EGIS_ET702_CH1M30_INV = 0x72102, /* 134 X 188 508DPI invert image */
//    FP_ALGOAPI_MODE_EGIS_ET702_CL1MH2_INV = 0x73102,   /* 134 X 188 508DPI invert image */
//    FP_ALGOAPI_MODE_EGIS_ET702_CL1MH2_C230 = 0x73103,   /* 134 X 188 508DPI */
//    FP_ALGOAPI_MODE_EGIS_ET702_INV = 0x70001,    /* 134 x 188 508DPI invert image */
//    FP_ALGOAPI_MODE_EGIS_ET901 = 0x90000,    /* 150 x 150 363DPI */
//    FP_ALGOAPI_MODE_EGIS_ET703_CH1M30 = 0x80000,    /* 132 x 120 508DPI */
//};

enum image_class_type {
    FP_IMAGE_TYPE_NORMAL = 0x0,
    FP_IMAGE_TYPE_ENROLL = 0xf,
    FP_IMAGE_TYPE_DRY    = 0x10,
};

enum egis_image_class_type {
    //FP_IMAGE_TYPE_NORMAL = 0x0,
    //FP_IMAGE_TYPE_ENROLL = 0xf,
    //FP_IMAGE_TYPE_DRY    = 0x10,
    FP_IMAGE_TYPE_LEARNING_QUEUE = 0x11,
};

/** Image mask flags. */
enum image_mask_flag {
    /** The pixel belongs to fingerprint. */
    FP_IMAGE_MASK_FINGERPRINT = 0x00,
    /** The pixel belongs to background (non-fingerprint). */
    FP_IMAGE_MASK_BACKGROUND = 0x01,
    /** 0x02, 0x04, ... reserved for future use. */
};

/** An image. */
struct image_v2 {
    /** The pixels of the image. */
    unsigned char *pixels;
    /** The width of the image. */
    int width;
    /** The height of the image. */
    int height;
    /** The class of the image, see image_class_type. */
    unsigned int class;
    /** The resolution of the image, in dpi. */
    int resolution;
    /** The mask of the image, see image_mask_flag. May be 0 if all pixels belongs to fingerprint. */
    unsigned char* mask;
};

/** Initialization */
int 
algorithm_initialization_v2 (void **ctx,
                             unsigned char *decision_data,
                             int decision_data_len,
                             int sensor_type);

int
algorithm_uninitialization_v2 (void *ctx,
                               unsigned char **decision_data,
                               int *decision_data_len);

/** Resets the history of the algorithm, including a reset of the static pattern history. */
int
algorithm_reset_v2 (void* ctx);

int
algorithm_recovery_decision_data(void);
int
algorithm_recovery_spd_decision_data(void);
int
algorithm_recovery_temp_db_decision_data(void);

struct verify_init_v2 {
    unsigned char **enroll_temp_array;
    int *enroll_temp_size_array;
    int enroll_temp_number;
};

void* get_enroll_ctx_ptr();

// Enrollment
int
enroll_init_v2 (void *ctx,
                int template_size);

int 
enroll_duplicate_check_v2 (void *ctx, 
                           struct verify_init_v2 *verify_init);

int set_enroll_context_v2(void* e_ctx, int param, int value);

int get_enroll_context_v2(void* e_ctx, int param, int *value);

struct image_quality_values_v2 {
     int fingerprint_quality;
     int fingerprint_area; // Fingerprint area in percentage of the total image area.
     int signal_to_noise_ratio;
     int signal;
     int noise;
     int static_pattern_area; // Static pattern area in percentage of the total image area.
};

struct enroll_info_v2 {
    unsigned char *image;
    int width;
    int height;
    unsigned int image_class;
    int percentage;
    int count;
    struct image_quality_values_v2 image_quality_values;
    /** The latent score, indicating the similarity against the previous image. Values range from
      * 0 to UINT16_MAX, with UINT16_MAX indicating highest similarity. */
    int latent_score;
    /** The fingerprint coverage, in percentage of a "default fingerprint" with an estimated size 
      * of 12.5x17.5mm, computed from all added images. */
    int finger_coverage;
    /** The match score of the added image against the enrolled (previously added) images. */
    int match_score;
    /** The static pattern detection coverage on the sensor during enroll. */
    int spd_coverage;
};

struct enroll_info_v2_1 {
    struct image_v2 image;
    int percentage;
    int count;
    struct image_quality_values_v2 image_quality_values;
    /** The latent score, indicating the similarity against the previous image. Values range from
      * 0 to UINT16_MAX, with UINT16_MAX indicating highest similarity. */
    int latent_score;
    /** The fingerprint coverage, in percentage of a "default fingerprint" with an estimated size
      * of 12.5x17.5mm, computed from all added images. */
    int finger_coverage;
    /** The match score of the added image against the enrolled (previously added) images. */
    int match_score;
    /** The static pattern detection coverage on the sensor during enroll. */
    int spd_coverage;
};

int
enroll_v2 (void *ctx,
           struct enroll_info_v2 *enroll_info);

int
enroll_v2_1 (void *ctx,
             struct enroll_info_v2_1 *enroll_info);

int
get_enroll_template_v2 (void *ctx,
                        unsigned char **enroll_temp,
                        int *enroll_temp_size);

int
enroll_uninit_v2 (void *ctx);

/** Finish enroll before MAX_ENROLL_COUNT is reached.
  * Note: call this function only when there is not enough images to be enrolled to  
  * prohibit bad images to be enrolled if reject level is lower down during enroll.
  */
int
enroll_finish_v2(void *ctx);

int
get_template_version_v2 (void *ctx,
                         unsigned char *template_data,
                         int template_data_size,
                         int *major_version,
                         int *minor_version);

// Verification

int
verify_init_v2 (void *ctx,
                struct verify_init_v2 *verify_init);

/** An alignment object describes translation and rotation alignment.
  * As such an alignment is used as part of fingerprint match
  * operations. The alignment can be expressed as a matrix multitplication:
  *
  *     V = T*R*E
  *
  * Where E and V are coordinates. The relation can also be expressed as:
  *
  *     Vx = Ex * cos(angle) - Ey * sin(angle) + dx
  *     Vy = Ex * sin(angle) + Ey * cos(angle) + dy
  *
  * Another way to understand alignment is to, starting at origo or the top left
  * corner of V, move template E (dx, dy) pixels, then rotate E a number of degrees
  * equal to angle.
  */
struct alignment_v2 {
    /** The translation in pixels along the x-axis, in 500dpi. */
    int dx;
    /** The translation in pixels along the y-axis, in 500dpi. */
    int dy;
    /** The rotation in binary radians [0, 255]. */
    unsigned char rotation;
    /** The overlap between the images, in percentage of the total image area. */
    int overlap;
};

struct verify_info_v2 {
    int try_match_count;
    unsigned char *image;
    int width;
    int height;
    unsigned int image_class;
    unsigned char *feat;
    int len;
    int match_score;
    int match_threshold;
    unsigned int match_image_class;
    int	far_min;
    int match_index;
    int *match_score_array;
    int is_learning_update;
    unsigned char *enroll_temp;
    int enroll_temp_size;
    int latency_adjustment;
    /** The alignment between the verification image and the best matching subtemplate. */
    struct alignment_v2 match_alignment;
	struct image_quality_values_v2 image_quality_values;
    int final_try_match;
    int improve_dry_finger_performance;
    /** Tells whether the match failed (0), matched with the original image (1) or matched 
      * with the scaled image (2). */
    int match_result;
    int image_type_flag;
};

struct verify_info_v2_1 {
    int try_match_count;
    struct image_v2 image;
    unsigned char *feat;
    int len;
    int match_score;
    int match_threshold;
    unsigned int match_image_class;
    int	far_min;
    int match_index;
    int *match_score_array;
    int is_learning_update;
    unsigned char *enroll_temp;
    int enroll_temp_size;
    int latency_adjustment;
    /** The alignment between the verification image and the best matching subtemplate. */
    struct alignment_v2 match_alignment;
	struct image_quality_values_v2 image_quality_values;
    int final_try_match;
    int improve_dry_finger_performance;
    /** Tells whether the match failed (0), matched with the original image (1) or matched 
      * with the scaled image (2). */
    int match_result;
    int image_type_flag;
};

/**
 * Verify, return the result and train finger (if needed) with the new data.
 * 
 * Returns FP_MATCHOK, FP_MATCHFAIL or an error code. The function will never return FP_OK.
 * 
 * This function is fully equivalent to calling verify_begin_v2, verify_end_v2 and returning
 * the match result (unless an error occurs, in which case the error is returned instead).
 * 
 */
int
verify_v2 (void *ctx,
           struct verify_info_v2 *verify_info);

int
verify_v2_1 (void *ctx,
             struct verify_info_v2_1 *verify_info);

/** Returns the template extracted from the verification image from the call to verify_v2. 
  * This function must be called after verify(_begin)_v2 but before verify_uninit_v2. It 
  * may be called before verify_end_v2. The memory referenced will be deleted at 
  * verify_uninit_v2. 
  * Note: If only verify_begin_v2 has been called prior to this, this function will perform
  * the same updating as the verify_end_v2 does. */
int
get_verify_template_v2 (void *ctx,
                        const unsigned char **verify_temp,
                        int *verify_temp_size);

/** Verifies a subtemplate temp_2 against an another subtemplate temp_1. This function can
  * be called separately, i.e. no specific calls to verify_init_v2 and verify_uninit_v2 is
  * required. */
int
verify_template_v2 (void* ctx,
                    const unsigned char* temp_1,
                    int temp_1_size,
                    const unsigned char* temp_2,
                    int temp_2_size,
                    struct verify_info_v2* verify_info);

int
verify_template_v2_1 (void* ctx,
                      const unsigned char* temp_1,
                      int temp_1_size,
                      const unsigned char* temp_2,
                      int temp_2_size,
                      struct verify_info_v2_1* verify_info);

/**
 * Begins a verification process and returns a match result
 * 
 * Some parts of verification_info is not fully filled out by this function. Final results are
 * available from verify_end_v2, which must be called after this function regardless of MATCHFAIL
 * or MATCHOK.
 * 
 * This function can return FP_MATCHFAIL or FP_MATCHOK. If an error occurs, it is returned. This function
 * will never return FP_OK.
 * 
 */
int
verify_begin_v2 (void *ctx,
                 struct verify_info_v2 *verify_info);

int
verify_begin_v2_1 (void *ctx,
                   struct verify_info_v2_1 *verify_info);

#define NBR_OF_LATENT_FEATURES 10

/**
 * Latent detect
 *
 * Returns a score indicating the probability that the last verified image (from verify_begin_v2) is a latent 
 * fingerprint. Higher scores means that the image is more likely a latent fingerprint. This function needs to 
 * be called after the verify_begin_v2 but before the verify_end_v2.
 *
 * This function also returns the latent features used to generate this score. The caller can choose to use the
 * latent score itself or to compute a score on their own using the the returned latent features as well as additional
 * external latent features. Note: These features are subject to change in later releases. 
 *
 * Scores are related to reject rate of real fingerprints. Adding and subtracting 2048 means doubling or halving
 * the reject rate. 32768 is tuned to give ~1% reject rate of real fingerprints.
 *
 * This gives the following table:
 *
 *    FRR           score
 * -------------------------
 *   0.01%          TBD
 *   0.05%          TBD
 *    0.1%          TBD
 *    0.5%          TBD
 *      1%          TBD
 *      5%          TBD
 *     10%          TBD
 *
 *
 * These are measured from reference databases, and actual scores may vary with the database.
 *
 * If an error occurs, an error code (negative number) is returned. Any positive return value indicates success.
 *
 */
int
get_latent_score (void* ctx,
                  int latent_features[NBR_OF_LATENT_FEATURES]);

/**
 * Ends the verification process, trains the matcher
 * 
 * This function will take care of all updating that was not made in verify_begin_v2, so that as much
 * time consuming processing as possible is placed in this function call.
 * 
 * Before a new verification can be made, this function must be called. Some parts of verification_info
 * is not fully filled out until this function has been called.
 * 
 * This function does not return MATCHOK or MATCHFAIL, only an error code or FP_OK.
 * 
 */
int
verify_end_v2 (void *ctx,
               struct verify_info_v2 *verify_info);

int
verify_end_v2_1 (void *ctx,
                 struct verify_info_v2_1 *verify_info);

int
verify_uninit_v2 (void *ctx);

int
set_accuracy_level_v2 (void *ctx,
                       int far_ratio);

int
extract_feature_v2 (void *ctx,
                    const unsigned char *image,
                    int width,
                    int height,
                    unsigned int image_class,
                    unsigned char **feature,
                    int *feat_size);

int
extract_feature_v2_1 (void *ctx,
                      const struct image_v2* image,
                      unsigned char **feature,
                      int *feat_size,
                      struct image_quality_values_v2* image_quality_values);

int
update_enroll_template_v2 (void *ctx,
                           const unsigned char *enroll_temp,
                           int enroll_temp_size,
                           const unsigned char *feature,
                           int feat_size,
                           unsigned char **new_enroll_temp,
                           int *new_enroll_temp_size);

int learning_v2(struct verify_info_v2 *verify_info, int matchidx);

//void
//free_data (unsigned char* data,
//           int data_size);

// Other
int
set_algo_config_v2 (void *ctx,
                    int param,
                    int value);
int
get_algo_config_v2 (void *ctx,
                    int param,
                    int *value);

int
algorithm_do_other_v2 (void *ctx,
                       int op_code,
                       unsigned char *in_data,
                       int in_data_size, 
                       unsigned char *out_data,
                       int *out_data_size);

/** Image class functionality. 
  *
  * SDK can use up to 32 classes (0-31), with the default class being 0 (will be assigned to any images not 
  * receiving a specific class). The class will be stored in the subtemplate of the multitemplate but the 
  * required minimum of subtemplates for each class is currently not stored. This means that you need to set 
  * the required minimum each time after you initialize the algorithm. If not, it will default back to no 
  * requirements at all, in which case all classes can replace each other. 
  * 
  * An image can be assigned a class using the pb_image_set_class_id function. The class id is copied
  * to all templates extracted from the image. Each extracted (sub)template may then be enrolled or updated
  * into the enrolled multitemplate. By specifying the required minimum number of subtemplates for different
  * classes the dynamic update functionality can then control which subtemplates to replace. 
  *
  * A subtemplate of class A may replace another subtemplate of class A or any other subtemplate of a different
  * class without any requirements on the minimum number of enrolled subtemplates. It may also replace any 
  * other subtemplate of a different class even if it has requirements on the minimum number of subtemplates 
  * given that the quota for that class is larger than the specified minimum. 
  *
  * Example:
  * Assume three classes A, B and C, with a required minimum nbr of subtemplates of 0, 2 and 4 respectively and 
  * the actual number of subtemplates enrolled being 4, 4 and 4 respectively, with a maximum of 12 subtemplates. 
  *   A (min: 0, current: 4)
  *   B (min: 2, current: 4)
  *   C (min: 4, current: 4)
  *
  * A new subtemplate of class A may then replace any of the 4 enrolled subtemplates of class A, since they are
  * from the same class, any of the 4 enrolled subtemplates of class B, since they have a higher quota (4) than 
  * required (2), but none of the 4 enrolled subtemplates of class C.
  *   - A may replace A (same class) or B (quota exceeded). 
  *
  * A new subtemplate of class B may then replace any of the 4 enrolled subtemplates of class A, since A has no
  * requirement on the minimum of subtemplates enrolled, any of the 4 enrolled subtemplates of class B since they
  * are from the same class, but none of the 4 enrolled subtemplates of class C. 
  *   - B may replace A (no min requirement) or B (same class)
  *
  * A new subtemplate of class C may then replace any of the 4 enrolled subtemplates of class A, since A has no
  * requirement on the minimum of subtemplates enrolled, any of the 4 enrolled subtemplates of class B since they 
  * have a higher quota (4) than required (2), and any of the 4 enrolled subtemplates of class C since they are 
  * from the same class. 
  *   - C may replace A (no min requirement) or B (quota exceeded) or C (same class). 
  */

int
set_required_minimum_nbr_of_subtemplates_v2 (void* ctx,
                                             unsigned int image_class,
                                             unsigned int minimum_nbr_of_subtemplates);

int
get_required_minimum_nbr_of_subtemplates_v2 (void* ctx,
                                             unsigned int image_class,
                                             int* minimum_nbr_of_subtemplates);

int
get_nbr_of_subtemplates_from_image_class (void* ctx,
                                          const unsigned char *enroll_temp,
                                          int enroll_temp_size,
                                          unsigned int image_class,
                                          int* nbr_of_subtemplates);

/**
 * Fake finger detect
 * 
 * Returns a score indicating how much the input image looks like a fingerprint. Higher scores means that the
 * image looks more like a real fingerprint.
 * 
 * Scores are related to reject rate of real fingerprints. Adding and subtracting 2048 means doubling or halving
 * the reject rate. 32768 is tuned to give ~1% reject rate of real fingerprints.
 * 
 * This gives the following table:
 * 
 *    FRR           score
 * -------------------------
 *   0.01%          19161
 *   0.05%          23917
 *    0.1%          25965
 *    0.5%          30720
 *      1%          32768
 *      5%          37523
 *     10%          39571
 * 
 * 
 * These are measured from reference databases, and actual scores may vary with the database.
 * 
 * If an error occurs, an error code (negative number) is returned. Any positive return value indicates success.
 *
 */
int
get_finger_score(void* ctx,
                 const unsigned char* pixels,
                 const int width,
                 const int height);


/** 
  * TEE + SE verification.
  *
  * For increased security, it is possible to split the verification between the TEE and the SE. The TEE
  * performs the initial verification and the SE performs the final (if TEE accepted).
  *
  * The call flow is as shown below:
  *
  *   ----------------------------------------------------------------------------------
  *
  *   // Verify begin in TEE:
  *
  *   unsigned char* verification_template_SE = 0;
  *   int verification_template_size_SE = 0;
  *   unsigned char* match_data_SE = 0;
  *   int match_data_size_SE = 0;
  *
  *   // Perform initial verification in TEE, returns the match result as well as a verification template 
  *   // and match data to be sent to the SE where the final verification is done.
  *   int result = verify_begin_TEE (..., &verification_template_SE, &verification_template_size_SE,
  *                                       &match_data_SE, &match_data_size_SE);
  *
  *   if (result == FP_MATCHOK) {
  *       // The TEE accepted the verification, trigger verification in SE to confirm accept.
  *       ... // Implemented by caller.
  *   }
  *
  *   ----------------------------------------------------------------------------------
  *
  *   // Verify in SE:
  *
  *   struct verify_info_SE verify_info_SE = { 0 };
  *   unsigned char* updated_enrolled_template_SE = 0;
  *   int updated_enrolled_template_size_SE = 0;
  *
  *   // Get enrolled template corresponding to the matched enrolled template in the TEE verification.
  *   unsigned char* enrolled_template_SE = ... // Implemented by caller.
  *   int enrolled_template_size_SE = ... // Implemented by caller.
  *   // Get verification template passed from TEE.
  *   unsigned char* verification_template_SE = ... // Implemented by caller.
  *   int verification_template_size_SE = ... // Implemented by caller.
  *
  *   int allow_update = 1;
  *
  *   // Verify.
  *   int result = verify_SE (enrolled_template_SE, enrolled_template_size_SE, verification_template_SE, verification_template_size_SE, 
  *                           allow_update, &verify_info_SE, &updated_enrolled_template_SE, &updated_enrolled_template_size_SE);
  *
  *   // Check if SE confirmed accept from TEE.
  *   if (result == FP_MATCHOK) {
  *       // Trigger unlock of phone.
  *       ... // Implemented by caller.
  *   }
  *
  *   // Update enrolled template in SE, if applicable.
  *   if (updated_enrolled_template_SE) {
  *       ... // Implemented by caller.
  *   }
  *
  *   ----------------------------------------------------------------------------------
  *
  *   // Verify end in TEE:
  *
  *   // Get verify info passed from SE.
  *   struct verify_info_SE verify_info_SE = ... // Implemented by caller.
  *
  *   // Finalize verification.
  *   verify_end_TEE (..., verify_info_SE, ...);
  *
  *   // Update enrolled template in TEE, if applicable.
  *   if (verify_info->enroll_temp) {
  *       ... // Implemented by caller.
  *   }
  *
  *   ----------------------------------------------------------------------------------
  *
  * TEE + SE Enrollment. 
  *
  * Enrollment is very similar to the TEE only solution. However, the following needs to be considered:
  *
  *  - The limitation of the enrolled templates must be specified as maximum number of subtemplates, maximum
  *    template size (in bytes) is not supported. Use the enroll_init_TEE_SE instead of enroll_init_v2.
  *    The reason for this is that the templates are stored both in the TEE and the SE and the same subtemplate
  *    needs to be replaced in both locations at dynamic update, hence the need for a well defined limitation. 
  *
  *  - The enrolled template to be stored in the SE must be created and passed to the SE after enrollment is 
  *    complete. Use the get_enrolled_template_SE function to create the enrolled template for the SE.
  *
  */

/* Initializes the enrollment to be used for TEE + SE solution. The limitation of the enrolled template needs 
 * to be specified as maximum number of subtemplates. */
int
enroll_init_TEE_SE (void *ctx,
                    int max_nbr_of_subtemplates);

/** Returns the part of the enrolled template (in the TEE) to be stored in the 
  * secure element (SE). May be called after an enrollment to pass the SE template
  * to the SE. */
int
get_enrolled_template_SE (unsigned char *enrolled_template_TEE,
                          int enrolled_template_size_TEE,
                          unsigned char **enrolled_template_SE,
                          int *enrolled_template_size_SE);

/** Performs the initial part of the TEE + SE verification. Should be called in the TEE.
  * 
  * @param[out] verification_template_SE is the verification template to be passed
  *             to the SE to perform the final verification in the SE.
  * @param[out] verification_template_size_SE is the size of the verification_template_SE.
  * @param[out] match_data_SE is the match data to be passed to the SE to perform the 
  *             final verification in the SE.
  * @param[out] match_data_size_SE is the size of the match_data_SE.
  */
int
verify_begin_TEE (void *ctx,
                  struct verify_info_v2_1 *verify_info,
                  unsigned char **verification_template_SE,
                  int *verification_template_size_SE,
                  unsigned char **match_data_SE,
                  int *match_data_size_SE);

/** Verify info for the verification in SE. */
struct verify_info_SE {
    /** Decision from the verification, != 0 if match was successful. */
    int decision;
    /** The tag of the subtemplate that was replaced in the SE, -1 if not applicable.
      * The SE will decide which subtemplate to replace and send this information back to 
      * the TEE so that the corresponding subtemplate can be replaced in the TEE as well. */
    int replaced_subtemplate_tag;
    /** The tag of the new subtemplate. */
    int new_subtemplate_tag;
};

/** Performs the final part of the TEE + SE verification. Should be called in the SE.
  *
  * @param[in] sensor_type is the sensor type, see algo_api_sensor_type. 
  * @param[in] enrolled_template_SE is the enrolled template in the SE corresponding to the 
  *            finger matched by the TEE verification, see match_index from the verify_info
  *            in the call to verify_begin_TEE.
  * @param[in] enrolled_template_size_SE is the size of the enrolled_template_SE.
  * @param[in] verification_template_SE is the verification template returned by the 
  *            verify_begin_TEE function. 
  * @param[in] verification_template_size_SE is the size of the verification_template_SE.
  * @param[in] match_data_SE is the match data returned by the verify_begin_TEE function. 
  * @param[in] match_data_size_SE is the size of the match_data_SE.
  * @param[in] allow_update tells if the SE should perform an update of the enrolled template
  *            in the case of an accept. 
  * @param[out] verify_info_SE is the verify info of the verification. 
  * @param[out] updated_enrolled_template_SE is the updated enrolled template for the SE, or 
  *             0 if no update was performed. 
  * @param[out] updated_enrolled_template_size_SE is the size of the updated_enrolled_template_SE.
  */
int
verify_SE (int sensor_type,
           unsigned char *enrolled_template_SE,
           int enrolled_template_size_SE,
           unsigned char *verification_template_SE,
           int verification_template_size_SE,
           unsigned char *match_data_SE,
           int match_data_size_SE,
           int allow_update,
           struct verify_info_SE* verify_info_SE,
           unsigned char **updated_enrolled_template_SE,
           int *updated_enrolled_template_size_SE);

/** Finalizes the verification. Should be called in the TEE. 
  *
  * @param[in] verify_info_SE is the verify info from the verification in the SE. 
  */
int
verify_end_TEE (void *ctx,
                struct verify_info_SE* verify_info_SE,
                struct verify_info_v2_1 *verify_info);


/** Helper functions. */
void
verify_info_v2_1_to_v2 (const struct verify_info_v2_1 *verify_info,
                        struct verify_info_v2 *verify_info_v2);

void
verify_info_v2_to_v2_1 (const struct verify_info_v2 *verify_info,
                        unsigned char* mask,
                        int resolution,
                        struct verify_info_v2_1 *verify_info_v2_1);

/**
 * Extra integration API. These functions need to be defined. An empty definition is included below inside
 * a dead code block for ease of integration.
 * 
 */
 
/** Returns the number of microseconds elapsed as measured by clock(). */
extern int
egistec_clock();

/** Returns the cpu id of the cpu on which the current thread is running. */
extern int
egistec_cpu_id();

void 
convert_verify_init_v1_to_v2(VERIFY_INIT *src, 
                            struct verify_init_v2 *dst);

void 
convert_verify_init_v2_to_v1(struct verify_init_v2 *src, 
                            VERIFY_INIT *dst);

void 
convert_verify_info_v1_to_v2(VERIFY_INFO *src, 
                            struct verify_info_v2 *dst);

void 
convert_verify_info_v1_to_v2_1(VERIFY_INFO *src, 
                            struct verify_info_v2_1 *dst);

void 
convert_verify_info_v2_to_v1(struct verify_info_v2 *src, 
                            VERIFY_INFO *dst);

void 
convert_verify_info_v2_1_to_v1(struct verify_info_v2_1 *src, 
                            VERIFY_INFO *dst);

/** Dummy implementation of the integration functions. If possible, use a proper implementation. */
#if 0
int egistec_clock() { return -1; }

int egistec_cpu_id() { return -1; }
#endif

#ifdef __cplusplus
}
#endif

#endif
