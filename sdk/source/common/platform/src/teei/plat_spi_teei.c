#include <string.h>
#include "egis_definition.h"
#include "libexynos7872_spi.h"
#include "plat_log.h"
#include "plat_mem.h"
#include "type_definition.h"

#define LOG_TAG "RBS-SENSORCONTROL"

#define SPI_DEFAULT_CMD_SIZE 2
#define ET5XX_OP_REG_BURST_READ 0x22
#define ET5XX_OP_REG_BURST_WRITE 0x26
#define ET5XX_OP_IMAGE_READ 0x50

#define ET5XX_OP_VDM_READ 0x60
#define ET5XX_OP_VDM_WRITE 0x62

#define DEVICE_CLOSED_HANDLE 0
#define DEVICE_OPENED_HANDLE 666
int g_device_handle = DEVICE_CLOSED_HANDLE;
static const spi_config_t g_spi_cfg = {.delay_usecs = 0,
                                       .speed_hz = 96000000,
                                       .bits_per_word = BYTE_SIZE,
                                       .dma_mode = DMAMODE,
                                       .spi_mode = SPI_TX_MODE_0};

int io_dispatch_spi_test(void) {
    return 0;
}
int io_dispatch_connect(HANDLE* device_handle) {
    int retval;
    spi_config_t spi_cfg;

    if (g_device_handle == DEVICE_OPENED_HANDLE) return EGIS_OK;

    mem_move(&spi_cfg, &g_spi_cfg, sizeof(spi_config_t));

    retval = sec_spi_init(SPI_PORT, &spi_cfg);
    if (retval) {
        return EGIS_COMMAND_FAIL;
    }

    retval = sec_spi_dma_init();
    if (retval) {
        return EGIS_COMMAND_FAIL;
    }

    g_device_handle = DEVICE_OPENED_HANDLE;

    return EGIS_OK;
}

int io_dispatch_disconnect(void) {
    int retval;

    if (g_device_handle != DEVICE_OPENED_HANDLE) return EGIS_OK;

    retval = sec_spi_exit(SPI_PORT);
    if (retval) return EGIS_COMMAND_FAIL;

    g_device_handle = DEVICE_CLOSED_HANDLE;

    return EGIS_OK;
}

int io_dispatch_read_eFuse(BYTE* buf, UINT len) {
    ex_log(LOG_ERROR, "not implemented yet");
    return EGIS_COMMAND_FAIL;
}

int io_5xx_dispatch_read_burst_register(BYTE start_addr, BYTE len, BYTE* pdata) {
    int result, retval;
    unsigned char* buf = NULL;
    unsigned int buf_len;
    spi_config_t spi_cfg;
    spi_transfer_t spi_tx;
    spi_transfer_t spi_rx;

    if (pdata == NULL) return EGIS_COMMAND_FAIL;

    mem_move(&spi_cfg, &g_spi_cfg, sizeof(spi_config_t));

    buf_len = len + SPI_DEFAULT_CMD_SIZE;
    buf = (unsigned char*)mem_alloc(buf_len);
    if (!buf) {
        ex_log(LOG_DEBUG, "malloc failed 11111");
        return EGIS_COMMAND_FAIL;
    }
    mem_set(buf, 0, buf_len);
    buf[0] = ET5XX_OP_REG_BURST_READ;
    buf[1] = start_addr;

    spi_tx.buf_addr = buf;
    spi_tx.buf_len = buf_len;  // SPI_DEFAULT_CMD_SIZE + len;
    spi_tx.total_len = 0;
    spi_rx.buf_addr = buf;
    spi_rx.buf_len = buf_len;
    spi_rx.total_len = 0;

    result = sec_spi_write_read(SPI_PORT, &spi_cfg, &spi_tx, &spi_rx);
    if (result == 0)
        mem_move(pdata, &buf[SPI_DEFAULT_CMD_SIZE], len);
    else
        mem_set(pdata, 0, len);

    if (buf) {
        mem_free(buf);
        buf = NULL;
    }

    return (result == 0) ? EGIS_OK : EGIS_COMMAND_FAIL;
}

int io_5xx_dispatch_write_burst_register(BYTE start_addr, BYTE len, BYTE* pvalue) {
    int result, retval;
    unsigned char* buf = NULL;
    unsigned int buf_len;
    spi_config_t spi_cfg;
    spi_transfer_t spi_tx;

    if (pvalue == NULL) return EGIS_COMMAND_FAIL;

    mem_move(&spi_cfg, &g_spi_cfg, sizeof(spi_config_t));

    buf_len = len + SPI_DEFAULT_CMD_SIZE;
    buf = (unsigned char*)mem_alloc(buf_len);
    if (!buf) {
        ex_log(LOG_DEBUG, "malloc failed");
        return EGIS_COMMAND_FAIL;
    }

    mem_set(buf, 0, buf_len);
    buf[0] = ET5XX_OP_REG_BURST_WRITE;
    buf[1] = start_addr;
    mem_move(&buf[2], pvalue, len);

    spi_tx.buf_addr = buf;
    spi_tx.buf_len = buf_len;
    spi_tx.total_len = 0;
    result = sec_spi_write(SPI_PORT, &spi_cfg, &spi_tx);
    if (buf) {
        mem_free(buf);
        buf = NULL;
    }

    return (result == 0) ? EGIS_OK : EGIS_COMMAND_FAIL;
}

int io_5xx_dispatch_read_register(BYTE address, BYTE* value) {
    if (!value) return EGIS_COMMAND_FAIL;
    return io_5xx_dispatch_read_burst_register(address, 1, value);
}

int io_5xx_dispatch_write_register(BYTE address, BYTE value) {
    return io_5xx_dispatch_write_burst_register(address, 1, &value);
}

int io_5xx_dispatch_write_reverse_register(BYTE start_addr, BYTE len, BYTE* pvalue) {
    int i = 0;

    start_addr -= (len - 1);
    for (i = 0; i < len / 2; i++) {
        pvalue[i] = pvalue[i] ^ pvalue[len - 1 - i];
        pvalue[len - 1 - i] = pvalue[i] ^ pvalue[len - 1 - i];
        pvalue[i] = pvalue[i] ^ pvalue[len - 1 - i];
    }

    return io_5xx_dispatch_write_burst_register(start_addr, len, pvalue);
}

int ET_5xx_dispatch_write_register(BYTE address, BYTE value) {
    return io_5xx_dispatch_write_register(address, value);
}

BYTE ET_5xx_dispatch_read_register(BYTE address, const char* func) {
    BYTE value = 0;
    int ret = io_5xx_dispatch_read_register(address, &value);
    if (ret != EGIS_OK) {
        egislog_e("%s, addr = 0x%x, ret = %d", func, address, ret);
    }
    return value;
}

#define IMAGE_SHIFT 0
int io_5xx_dispatch_get_frame(BYTE* frame, UINT height, UINT width, UINT multi,
                              UINT number_of_frames) {
    int result, retval;
    unsigned char* buf = NULL;
    unsigned int buf_len;
    unsigned int image_len;
    spi_config_t spi_cfg;
    spi_transfer_t spi_tx;
    spi_transfer_t spi_rx;

    if (!frame) return EGIS_COMMAND_FAIL;

    mem_move(&spi_cfg, &g_spi_cfg, sizeof(spi_config_t));

    image_len = (height) * (width);
    buf_len = SPI_DEFAULT_CMD_SIZE + image_len;
    buf = (unsigned char*)mem_alloc(buf_len + IMAGE_SHIFT);
    if (!buf) {
        ex_log(LOG_DEBUG, "malloc failed");
        return EGIS_COMMAND_FAIL;
    }

    mem_set(buf, 0x00, buf_len + IMAGE_SHIFT);
    buf[0] = ET5XX_OP_IMAGE_READ;

    spi_tx.buf_addr = buf;
    spi_tx.buf_len = buf_len;
    spi_tx.total_len = 0;
    spi_rx.buf_addr = buf;
    spi_rx.buf_len = buf_len;
    spi_rx.total_len = 0;
    result = sec_spi_write_read(SPI_PORT, &spi_cfg, &spi_tx, &spi_rx);
    if (!result) mem_move(frame, &buf[SPI_DEFAULT_CMD_SIZE - 1 + IMAGE_SHIFT], image_len);

    if (buf) {
        mem_free(buf);
        buf = NULL;
    }

    return (result == 0) ? EGIS_OK : EGIS_COMMAND_FAIL;
}

int polling_registry(BYTE addr, BYTE expect, BYTE mask) {
    int i;
    unsigned char value = 0x00;
    for (i = 0; i < 3000; i++) {
        if (io_5xx_dispatch_read_register(addr, &value) != EGIS_OK) return EGIS_COMMAND_FAIL;
        if ((value & mask) == expect) return EGIS_OK;
    }

    return EGIS_COMMAND_FAIL;
}

int io_5xx_read_vdm(BYTE* frame, UINT height, UINT width) {
    if (frame == NULL || width <= 0 || height <= 0) return EGIS_COMMAND_FAIL;

    int result, retval;
    unsigned char* buf = NULL;
    unsigned int buf_len;
    unsigned int data_len;
    spi_config_t spi_cfg;
    spi_transfer_t spi_tx;
    spi_transfer_t spi_rx;

    mem_move(&spi_cfg, &g_spi_cfg, sizeof(spi_config_t));

    data_len = (width) * (height);
    buf_len = SPI_DEFAULT_CMD_SIZE + data_len;
    buf = (unsigned char*)mem_alloc(buf_len);
    if (!buf) {
        ex_log(LOG_DEBUG, "malloc failed");
        return EGIS_COMMAND_FAIL;
    }
    mem_set(buf, 0, buf_len);

    buf[0] = ET5XX_OP_VDM_READ;
    buf[1] = 0;

    spi_tx.buf_addr = buf;
    spi_tx.buf_len = buf_len;  // SPI_DEFAULT_CMD_SIZE + len;
    spi_tx.total_len = 0;
    spi_rx.buf_addr = buf;
    spi_rx.buf_len = buf_len;
    spi_rx.total_len = 0;

    result = sec_spi_write_read(SPI_PORT, &spi_cfg, &spi_tx, &spi_rx);

    if (result == 0)
        mem_move(frame, &buf[SPI_DEFAULT_CMD_SIZE], data_len);
    else
        mem_move(frame, 0, data_len);

    if (buf) {
        mem_free(buf);
        buf = NULL;
    }

    return (result == 0) ? EGIS_OK : EGIS_COMMAND_FAIL;
}

int io_5xx_write_vdm(BYTE* frame, UINT height, UINT width) {
    if (frame == NULL || width <= 0 || height <= 0) return EGIS_COMMAND_FAIL;

    int result, retval;
    unsigned char* buf = NULL;
    unsigned int buf_len;
    unsigned int data_len;
    spi_config_t spi_cfg;
    spi_transfer_t spi_tx;

    mem_move(&spi_cfg, &g_spi_cfg, sizeof(spi_config_t));

    data_len = (width) * (height);
    buf_len = data_len + SPI_DEFAULT_CMD_SIZE;
    buf = (unsigned char*)mem_alloc(buf_len);
    if (!buf) {
        ex_log(LOG_DEBUG, "malloc failed");
        return EGIS_COMMAND_FAIL;
    }

    mem_set(buf, 0, buf_len);
    buf[0] = ET5XX_OP_VDM_WRITE;
    buf[1] = 0;
    mem_move(&buf[2], frame, data_len);

    spi_tx.buf_addr = buf;
    spi_tx.buf_len = buf_len;
    spi_tx.total_len = 0;

    result = sec_spi_write(SPI_PORT, &spi_cfg, &spi_tx);

    if (buf) {
        mem_free(buf);
        buf = NULL;
    }

    return (result == 0) ? EGIS_OK : EGIS_COMMAND_FAIL;
}

int io_5xx_read_nvm(BYTE* buffer) {
    egislog_e("%s is not supported !", __func__);
    return -1;
}

int io_dispatch_read_register(BYTE address, BYTE* value) {
    return io_5xx_dispatch_read_register(address, value);
}

int io_dispatch_write_register(BYTE address, BYTE value) {
    return io_5xx_dispatch_write_register(address, value);
}

int io_dispatch_get_frame(BYTE* frame, UINT height, UINT width, UINT number_of_frames) {
    return io_5xx_dispatch_get_frame(frame, height, width, 0, number_of_frames);
}

int io_dispatch_set_tgen(BOOL status) {
    return EGIS_OK;
}