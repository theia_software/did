#include <ut_pf_cp.h>
#include <ut_pf_km.h>

#include "plat_hmac.h"
#include "plat_log.h"
#include "plat_mem.h"

static struct PlatSecureKey g_key;
static unsigned int g_loaded = 0;
int hmac_setup_platform_sec_key(unsigned char* ext_data, unsigned int ext_data_len,
                                unsigned char* out_key, unsigned int* out_key_len) {
    int result = 0;

    ex_log(LOG_DEBUG, "hmac_setup_platform_sec_key enter\n");
    ex_log(LOG_DEBUG, "g_loaded = %d\n", g_loaded);
    ex_log(LOG_DEBUG, "ext_data = %d, out_key = %d\n", ext_data != NULL, out_key != NULL);
    /*
     *	{ @ext_data, @ext_data_len}
     *	In Plat. QSEE, it will be hold the encrypted data from
     *KeymasterTA
     *	//TODO: decrypt
     */

    if (!g_loaded) {
        result = ut_pf_km_get_hmac_key(g_key.key, &g_key.len);
        if (result < 0) {
            ex_log(LOG_ERROR, "ut_pf_km_get_hmac_key failed, result = %d\n", result);
            mem_set(&g_key, 0, sizeof(struct PlatSecureKey));
            return -1;
        } else if (g_key.len > MAX_SECURE_KEY_LEN) {
            ex_log(LOG_ERROR,
                   "ut_pf_km_get_hmac_key success but key_len = %d "
                   "is overflowing\n",
                   g_key.len);
            mem_set(&g_key, 0, sizeof(struct PlatSecureKey));
            return -1;
        } else {
            g_loaded = 1;
        }
    }
    /* the value of key.len, loaded 32 / unloaded 0 */
    if (out_key != NULL && out_key_len != NULL) {
        *out_key_len = g_key.len;
        mem_move(out_key, g_key.key, g_key.len);
    }

    return 0;
}

int hmac_sha256(unsigned char const* in, const unsigned int in_len, unsigned char* key,
                unsigned int key_len, unsigned char* out_hmac) {
    int result = 0;
    int retval;
    ut_pf_cp_context_t* ctx;
    unsigned int dst_len = HMAC_SHA256_BYTES;
    unsigned char dst_hmac[HMAC_SHA256_BYTES];

    ex_log(LOG_DEBUG, "hmac_sha256 enter\n");

    result = ut_pf_cp_open(&ctx, UT_PF_CP_CLS_MC, UT_PF_CP_ACT_MC_HMAC_SHA256);
    ex_log(LOG_DEBUG, "ut_pf_cp_open result = %d\n", result);
    if (result < 0) {
        return -1;
    }

    result = ut_pf_cp_mc_starts(ctx, key, key_len, NULL, 0);
    ex_log(LOG_DEBUG, "ut_pf_cp_mc_starts result = %d\n", result);
    if (result < 0) {
        retval = -1;
        goto EXIT;
    }

    result = ut_pf_cp_mc_update(ctx, in, in_len);
    ex_log(LOG_DEBUG, "ut_pf_cp_mc_update result = %d\n", result);
    if (result < 0) {
        retval = -1;
        goto EXIT;
    }

    result = ut_pf_cp_mc_finish(ctx, dst_hmac, &dst_len);
    ex_log(LOG_DEBUG, "ut_pf_cp_mc_finish result = %d, dst_len = %u\n", result, dst_len);
    if (result == 0 && dst_len == HMAC_SHA256_BYTES) {
        mem_move(out_hmac, dst_hmac, HMAC_SHA256_BYTES);
        retval = 0;
    }

EXIT:
    ut_pf_cp_close(ctx);
    return retval;
}
