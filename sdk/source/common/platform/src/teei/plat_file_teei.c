#include <ut_pf_ts.h>
#include <ut_sys_util.h>

#include "egis_sprintf.h"
#include "plat_file.h"
#include "plat_log.h"

#define LOG_TAG "RBS-FILE"

#define MAX_PATH 256

int plat_save_raw_image(char* path, unsigned char* pImage, unsigned int width,
                        unsigned int height) {
    int fd = 0;
    int written_len, retval = 0;
    char relative_path[MAX_PATH] = {0};

    if (NULL == path || NULL == pImage || width <= 0 || height <= 0) {
        egislog_e("plat_save_raw_image invalid parameters\n");
        return PLAT_FILE_CMD_INVALID_PARAM;
    }

    egist_snprintf(relative_path, MAX_PATH, "%s%s", "../../..", path);

    fd = ut_pf_ts_open(relative_path,
                       UT_PF_TS_O_RDWR | UT_PF_TS_O_CREAT | UT_PF_TS_O_TRUNC | UT_PF_TS_O_NONBLOCK);
    if (fd < 0) {
        retval = ut_pf_ts_error();
        egislog_e("ut_pf_ts_error error, fd = %d retval = %d\n", fd, retval);

        return PLAT_FILE_CMD_FAIL;
    }

    written_len = ut_pf_ts_write(fd, pImage, width * height);
    if (written_len != width * height) {
        egislog_e(
            "ut_pf_ts_write failed, written_len = %d, width = %d, "
            "height:%d\n",
            written_len, width, height);
        retval = PLAT_FILE_CMD_FAIL;
    } else
        retval = written_len;

    ut_pf_ts_close(fd);

    return retval;
}

int plat_load_raw_image(char* path, unsigned char* pImage, unsigned int width,
                        unsigned int height) {
    int fd, read_len;
    int result, retval = 0;
    char relative_path[MAX_PATH] = {0};

    if (NULL == path || NULL == pImage || width <= 0 || height <= 0) {
        egislog_e("invalid parameters\n");
        return PLAT_FILE_CMD_INVALID_PARAM;
    }

    egist_snprintf(relative_path, MAX_PATH, "%s%s", "../../..", path);

    result = ut_pf_ts_cp_exist(relative_path);
    if (result <= 0) {
        return PLAT_FILE_CMD_FAIL;
    }

    fd = ut_pf_ts_open(relative_path, UT_PF_TS_O_RDONLY);
    if (fd < 0) {
        egislog_e("ut_pf_ts_open failed, result = %d\n");
        return PLAT_FILE_CMD_FAIL;
    }

    read_len = ut_pf_ts_read(fd, pImage, width * height);
    if (read_len < 0) {
        retval = PLAT_FILE_CMD_FAIL;
        goto exit;
    } else
        retval = read_len;

exit:
    ut_pf_ts_close(fd);
    return retval;
}

int plat_save_file(char* path, unsigned char* buf, unsigned int len) {
    int fd = 0;
    int written_len, retval = 0;
    char relative_path[MAX_PATH] = {0};

    if (path == NULL || buf == NULL || len <= 0) {
        egislog_e("invalid parameters\n");
        return PLAT_FILE_CMD_INVALID_PARAM;
    }

    egist_snprintf(relative_path, MAX_PATH, "%s%s", "../../..", path);

    fd = ut_pf_ts_cp_open(
        relative_path, UT_PF_TS_O_RDWR | UT_PF_TS_O_CREAT | UT_PF_TS_O_TRUNC | UT_PF_TS_O_NONBLOCK);
    if (fd < 0) {
        egislog_e("ut_pf_ts_cp_open failed, fd = %d\n", fd);
        return PLAT_FILE_CMD_FAIL;
    }

    written_len = ut_pf_ts_cp_write(fd, buf, len);
    if (written_len != len) {
        egislog_e("ut_pf_ts_cp_write failed, written_len = %d, len = %d\n", written_len, len);
        retval = PLAT_FILE_CMD_FAIL;
    } else
        retval = written_len;

    ut_pf_ts_cp_close(fd);

    return retval;
}

int plat_load_file(char* path, unsigned char* buf, unsigned int len, unsigned int* real_size) {
    int fd, read_len;
    int result, retval = 0;
    char relative_path[MAX_PATH] = {0};

    if (NULL == path || NULL == buf || len <= 0 || real_size == NULL) {
        egislog_e("invalid parameters\n");
        return PLAT_FILE_CMD_INVALID_PARAM;
    }

    egist_snprintf(relative_path, MAX_PATH, "%s%s", "../../..", path);

    result = ut_pf_ts_cp_exist(relative_path);
    if (result <= 0) {
        return PLAT_FILE_CMD_FAIL;
    }

    fd = ut_pf_ts_cp_open(relative_path, UT_PF_TS_O_RDONLY);
    if (fd < 0) {
        egislog_e("ut_pf_ts_cp_open failed, result = %d\n");
        return PLAT_FILE_CMD_FAIL;
    }

    read_len = ut_pf_ts_cp_read(fd, buf, len);
    if (read_len < 0) {
        retval = PLAT_FILE_CMD_FAIL;
        goto exit;
    } else
        retval = read_len;

    *real_size = read_len;

exit:
    ut_pf_ts_cp_close(fd);
    return retval;
}

int plat_remove_file(char* path) {
    int result;
    char relative_path[MAX_PATH] = {0};

    if (NULL == path) {
        return PLAT_FILE_CMD_INVALID_PARAM;
    }

    egist_snprintf(relative_path, MAX_PATH, "%s%s", "../../..", path);

    result = ut_pf_ts_cp_exist(relative_path);
    if (result <= 0) {
        return PLAT_FILE_CMD_FAIL;
    }

    return ut_pf_ts_cp_unlink(relative_path) == PLAT_FILE_REMOVE_SUCCESS ? PLAT_FILE_REMOVE_SUCCESS
                                                                         : PLAT_FILE_CMD_FAIL;
}
