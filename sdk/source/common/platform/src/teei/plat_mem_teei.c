#include <string.h>
#include <ut_sys_util.h>

#include "plat_mem.h"
#include "ut_sys_stdio.h"

#define MEMORY_BLOCK_SAME 0
#define MEMORY_BLOCK_NOTSAME -1
#define INVALID_PARAM -2

void* mem_alloc(unsigned int size) {
    return ut_sys_malloc(size);
}
void* mem_realloc(void* ptr, unsigned int new_size) {
    return realloc(ptr, new_size);
}

void mem_free(void* ptr) {
    ut_sys_free(ptr);
}
void mem_move(void* dest, void* src, unsigned int size) {
    ut_sys_memcpy(dest, src, size);
}

void mem_set(void* dest, unsigned int data, unsigned int size) {
    ut_sys_memset(dest, data, size);
}

int mem_compare(void* buff1, void* buff2, unsigned int size) {
    int index = 0;

    if (NULL == buff1 || NULL == buff2 || size <= 0) {
        return INVALID_PARAM;
    }

    while (index < size) {
        if ((*((char*)buff1 + index)) != (*((char*)buff2 + index))) return MEMORY_BLOCK_NOTSAME;

        index++;
    }

    return MEMORY_BLOCK_SAME;
}

void* sys_alloc(unsigned int count, unsigned int size) {
    return mem_alloc(size * count);
}

void sys_free(void* ptr) {
    mem_free(ptr);
}
void* sys_realloc(void* ptr, unsigned int new_size) {
    return mem_realloc(ptr, new_size);
}
