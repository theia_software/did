#include <ut_pf_time.h>

#include "plat_log.h"
#include "plat_time.h"

unsigned long long plat_get_time() {
    int result;
    unsigned long long millisecond;
    unsigned long long seconds = 0, millis = 0;

    result = ut_pf_time_get_system_time(&seconds, &millis);

    millisecond = seconds * 1000 + millis;
    return result == 0 ? millisecond : 0;
}

unsigned long plat_get_diff_time(unsigned long long begin) {
    unsigned long long nowTime = plat_get_time();

    return (unsigned long)(nowTime - begin);
}

void plat_wait_time(unsigned long msecs) {
    unsigned long long times_now, times_start = plat_get_time();
    do {
        times_now = plat_get_time();
    } while (times_now - times_start < msecs);
    return;
}

void plat_sleep_time(unsigned long timeInMs) {
    ex_log(LOG_ERROR, "%s not supported !!", __func__);
    return;
}
