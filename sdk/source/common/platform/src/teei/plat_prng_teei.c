#include <ut_pf_cp.h>
#include "plat_log.h"
#include "plat_prng.h"

int plat_prn_generate(void* buf, unsigned int size) {
    int result = 0;
    ut_pf_cp_context_t* ctx;

    ex_log(LOG_DEBUG, "plat_prn_generate enter\n");
    ex_log(LOG_DEBUG, "size = %u\n", size);

    result = ut_pf_cp_open(&ctx, UT_PF_CP_CLS_RD, UT_PF_CP_ACT_RD_GENVEC);
    ex_log(LOG_DEBUG, "ut_pf_cp_open result = %d\n", result);
    if (result < 0) {
        result = ut_pf_cp_rd_random(NULL, (unsigned char*)buf, size);
        ex_log(LOG_DEBUG, "ut_pf_cp_rd_random with NULL CTX result = %d\n", result);
    } else {
        result = ut_pf_cp_rd_random(ctx, (unsigned char*)buf, size);
        ex_log(LOG_DEBUG, "ut_pf_cp_rd_random with CTX result = %d\n", result);
        ut_pf_cp_close(ctx);
    }

    return 0;
}
