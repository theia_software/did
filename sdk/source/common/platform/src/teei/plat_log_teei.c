#include <stdarg.h>
#include <stdio.h>
#include <string.h>
#include <ut_pf_ts.h>
#include <ut_sys_util.h>

#include "plat_log.h"
#include "plat_mem.h"

#define MAX_BUFLEN 1024

/*save log to file. easy to get TA log, only for debug*/
static int file_save_log(char* data, int data_len) {
    int fd = 0;
    int written_len, retval = 0;

    ut_sys_log("file_save_log enter\n");

    if (data == NULL || data_len <= 0) {
        ut_sys_log("file_save_log invalid parameters\n");
        return -1;
    }

    fd = ut_pf_ts_open("./rbs_teei_log.txt", UT_PF_TS_O_RDWR | UT_PF_TS_O_CREAT |
                                                 UT_PF_TS_O_APPEND | UT_PF_TS_O_NONBLOCK);
    if (fd < 0) {
        retval = ut_pf_ts_error();
        ut_sys_log("file_save_log ut_pf_ts_error error, retval = %d\n", retval);
        return -1;
    }

    written_len = ut_pf_ts_write(fd, data, data_len);
    if (written_len != data_len) {
        ut_sys_log(
            "file_save_log ut_pf_ts_write failed, written_len = %d, "
            "data_len = %d\n",
            written_len, data_len);
        retval = -1;
    }

    ut_pf_ts_close(fd);

    return retval;
}

#ifdef EGIS_DBG
LOG_LEVEL g_log_level = LOG_VERBOSE;
#else
LOG_LEVEL g_log_level = LOG_INFO;
#endif

void set_debug_level(LOG_LEVEL level) {
    g_log_level = level;
    output_log(LOG_ERROR, "RBS", "", "", 0, "set_debug_level %d", level);
}

void output_log(LOG_LEVEL level, const char* tag, const char* file_name, const char* func, int line,
                const char* format, ...) {
    char buffer[MAX_BUFLEN];
    va_list vl;

    if (format == NULL) return;
    if (g_log_level > level) return;

    va_start(vl, format);
    mem_set(buffer, 0, MAX_BUFLEN);
    vsprintf(buffer, format, vl);
    va_end(vl);

    ut_sys_log("%s : [%s] [%s:%d]%s\n", tag, file_name, func, line, buffer);
}
