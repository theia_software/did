#ifndef __BSP_TZ_SPI_H__
#define __BSP_TZ_SPI_H__

#include <stdint.h>

#include "plat_spi.h"

int bsp_tzspi_open(void);

int bsp_tzspi_read(bsp_tzspi_transfer_t* p_read_info);

int bsp_tzspi_write(bsp_tzspi_transfer_t* p_write_info);

int bsp_tzspi_full_duplex(bsp_tzspi_transfer_t* p_write_info, bsp_tzspi_transfer_t* p_read_info);

int bsp_tzspi_close(void);

uint32_t bsp_set_spi_clk(uint32_t clk);

int bsp_tzgpio_read(uint8_t* name, uint32_t* value);

#endif
