#include "plat_log.h"

#ifndef NULL
#define NULL 0
#endif
/**
 * @brief Get up time from bootup in ms.
 *
 * @param[in] None
 *
 * @return the uptime in ms from system bootup.
 *
 */
extern unsigned long long qsee_get_uptime(void);

unsigned long long plat_get_time() {
    return qsee_get_uptime();
}
unsigned long plat_get_diff_time(unsigned long long begin) {
    unsigned long long nowTime = plat_get_time();
    // TODO: consider overflow case
    return (unsigned long)(nowTime - begin);
}

void plat_wait_time(unsigned long msecs) {
    unsigned long long times_now, times_start = plat_get_time();
    do {
        times_now = plat_get_time();
    } while (times_now - times_start < msecs);
    return;
}

void plat_sleep_time(unsigned long timeInMs) {
    plat_wait_time(timeInMs);
    return;
}