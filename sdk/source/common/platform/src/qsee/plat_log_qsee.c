#include <stdarg.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>

#include "ext_inc/qsee_log.h"
#include "plat_log.h"
#define MAX_BUFLEN 1024

#ifdef EGIS_DBG
LOG_LEVEL g_log_level = LOG_VERBOSE;
#else
LOG_LEVEL g_log_level = LOG_INFO;
#endif

void set_debug_level(LOG_LEVEL level) {
    g_log_level = level;
    output_log(LOG_ERROR, "RBS", "", "", 0, "set_debug_level %d", level);
}

void output_log(LOG_LEVEL level, const char* tag, const char* file_name, const char* func, int line,
                const char* format, ...) {
    char buffer[MAX_BUFLEN];

    if (format == NULL) return;
    if (g_log_level > level) return;

    va_list vl;
    va_start(vl, format);
    vsnprintf(buffer, MAX_BUFLEN, format, vl);
    va_end(vl);

    switch (level) {
        case LOG_ERROR:
        case LOG_WARN:
        default:
            qsee_log(QSEE_LOG_MSG_FATAL, "ERROR! [%s] %s", tag, buffer);
            break;
        case LOG_INFO:
            qsee_log(QSEE_LOG_MSG_HIGH, "[%s] %s", tag, buffer);
            break;
        case LOG_DEBUG:
            qsee_log(QSEE_LOG_MSG_MED, "[%s] %s", tag, buffer);
            break;
        case LOG_VERBOSE:
            qsee_log(QSEE_LOG_MSG_LOW, "[%s] %s", tag, buffer);
            break;
    }
}
