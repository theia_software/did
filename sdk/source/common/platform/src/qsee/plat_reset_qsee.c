#include "ext_inc/qsee_spi.h"
#include "ext_inc/qsee_tlmm.h"
#include "plat_log.h"
#define LOG_TAG "RBS-SENSORCONTROL"
int eg_ta_sensor_reset_high(void) {
    uint32_t gpio_key;
    qsee_tlmm_config_t reset_gpio_config;
    int ret = 0;

    //    VOID_FUNC_ENTER();
    reset_gpio_config.drive = QSEE_GPIO_6MA;
    reset_gpio_config.pull = QSEE_GPIO_PULL_UP;
    reset_gpio_config.direction = QSEE_GPIO_OUTPUT;

    ret = qsee_tlmm_get_gpio_id("tlmm_gpio_rst_pin", &gpio_key);
    if (ret != 0) {
        egislog_e("failed qsee_tlmm_get_gpio_id....ret = %d", ret);
    }

    ret = qsee_tlmm_config_gpio_id(gpio_key, &reset_gpio_config);
    if (ret != 0) {
        egislog_e("failed qsee_tlmm_config_gpio_id....ret = %d", ret);
        return -1;
    }

    ret = qsee_tlmm_select_gpio_id_mode(gpio_key, QSEE_GPIO_MODE_GENERAL);
    if (ret != 0) {
        egislog_e("failed qsee_tlmm_select_gpio_id_mode....ret = %d", ret);
        return -1;
    }

    ret = qsee_tlmm_gpio_id_out(gpio_key, QSEE_GPIO_HIGH);
    if (ret != 0) {
        egislog_e("failed qsee_tlmm_gpio_id_out....ret = %d", ret);
        return -1;
    }
    //    egislog_d("qsee_tlmm_select_gpio_id_mode..gpio out put HIGH sucess..");

    ret = qsee_tlmm_release_gpio_id(gpio_key);
    if (ret != 0) {
        egislog_e("failed qsee_tlmm_release_gpio_id....ret = %d", ret);
    }
    //    VOID_FUNC_EXIT();

    return 0;
}

int eg_ta_sensor_reset_low(void) {
    uint32_t gpio_key;
    qsee_tlmm_config_t reset_gpio_config;
    int ret = 0;

    //    VOID_FUNC_ENTER();
    reset_gpio_config.drive = QSEE_GPIO_6MA;
    reset_gpio_config.pull = QSEE_GPIO_PULL_UP;
    reset_gpio_config.direction = QSEE_GPIO_OUTPUT;

    ret = qsee_tlmm_get_gpio_id("tlmm_gpio_rst_pin", &gpio_key);
    if (ret != 0) {
        egislog_e("failed qsee_tlmm_get_gpio_id....ret = %d", ret);
    }

    ret = qsee_tlmm_config_gpio_id(gpio_key, &reset_gpio_config);
    if (ret != 0) {
        egislog_e("failed qsee_tlmm_config_gpio_id....ret = %d", ret);
        return -1;
    }

    ret = qsee_tlmm_select_gpio_id_mode(gpio_key, QSEE_GPIO_MODE_GENERAL);
    if (ret != 0) {
        egislog_e("failed qsee_tlmm_select_gpio_id_mode....ret = %d", ret);
        return -1;
    }

    ret = qsee_tlmm_gpio_id_out(gpio_key, QSEE_GPIO_LOW);
    if (ret != 0) {
        egislog_e("failed qsee_tlmm_gpio_id_out....ret = %d", ret);
        return -1;
    }
    //    egislog_d("qsee_tlmm_select_gpio_id_mode..gpio out put LOW sucess..");

    ret = qsee_tlmm_release_gpio_id(gpio_key);
    if (ret != 0) {
        egislog_e("failed qsee_tlmm_release_gpio_id....ret = %d", ret);
    }
    //    VOID_FUNC_EXIT();

    return 0;
}