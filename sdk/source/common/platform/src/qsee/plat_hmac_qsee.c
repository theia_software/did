#include "egis_definition.h"
#include "ext_inc/qsee_hmac.h"
#include "ext_inc/qsee_message.h"
#include "plat_hmac.h"
#include "plat_log.h"
#include "plat_mem.h"

#define LOG_TAG "RBS-HMAC"

#define SHA256_HASH_SZ QSEE_HMAC_DIGEST_SIZE_SHA256
unsigned char g_hmac_key[SHA256_HASH_SZ] = {0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA,
                                            0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA,
                                            0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA};

BOOL g_is_hmac_key_set = FALSE;

#define PLAT_HMAC_ERROR_KEY -1
#define PLAT_HMAC_FAIL -2

int hmac_setup_platform_sec_key(unsigned char* ext_data, unsigned int ext_data_len,
                                unsigned char* out_key, unsigned int* out_key_len) {
    int retval = EGIS_INCORRECT_PARAMETER;

    if (ext_data != NULL && ext_data_len > 0) {
        char source_ta_name[32] = {0};
        uint32_t hmac_key_length = SHA256_HASH_SZ;

        retval = qsee_decapsulate_inter_app_message(source_ta_name, ext_data, ext_data_len,
                                                    g_hmac_key, &hmac_key_length);

        if (retval == EGIS_OK) {
            g_is_hmac_key_set = TRUE;

            if (memcmp(source_ta_name, "keymaster", 9) != 0)
                egislog_d("source_ta_name is wrong %d", retval);
        } else {
            retval = PLAT_HMAC_ERROR_KEY;
        }
    }

    if (out_key != NULL && out_key_len != NULL) {
        *out_key_len = 0;
        if (g_is_hmac_key_set == TRUE) {
            memcpy(out_key, g_hmac_key, SHA256_HASH_SZ);
            *out_key_len = SHA256_HASH_SZ;
            retval = EGIS_OK;
        } else {
            retval = PLAT_HMAC_ERROR_KEY;
        }
    }

    return retval;
}

int hmac_sha256(unsigned char const* in, const unsigned int in_len, unsigned char* key,
                unsigned int key_len, unsigned char* out_hmac) {
    int retval = EGIS_INCORRECT_PARAMETER;

    if (in == NULL || key == NULL) return retval;

    retval = qsee_hmac(QSEE_HMAC_SHA256, in, in_len, key, key_len, out_hmac);
    if (retval != EGIS_OK) return PLAT_HMAC_FAIL;

    return retval;
}