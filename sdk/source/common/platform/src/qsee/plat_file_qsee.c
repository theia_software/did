#include "ext_inc/platform_qsee.h"
#include "ext_inc/qsee_sfs.h"
#include "plat_file.h"
#include "plat_log.h"

#ifdef FS_USE_FTS
extern int f_remove_fts(const char* filename);
extern int f_write_fts(const char* filename, void* buf, size_t len);
extern int f_read_fts(const char* filename, void* buf, size_t len);
#else
#include "ext_inc/qsee_sfs.h"
#endif

#define LOG_TAG "RBS-FILE"

#ifndef NULL
#define NULL 0
#endif

int plat_save_raw_image(char* path, unsigned char* pImage, unsigned int width,
                        unsigned int height) {
    if (path == NULL || pImage == NULL) return PLAT_FILE_CMD_INVALID_PARAM;
    int fd;
    fd = open(path, O_RDWR | O_CREAT | O_TRUNC);
    if (fd > 0) {
        int32_t size = write(fd, pImage, width * height);
        close(fd);
        return size;
    }
    return PLAT_FILE_CMD_FAIL;
}

int plat_load_raw_image(char* path, unsigned char* pImage, unsigned int width,
                        unsigned int height) {
    if (path == NULL || pImage == NULL) return PLAT_FILE_CMD_INVALID_PARAM;
    int fd;
    fd = open(path, O_RDONLY);
    if (fd > 0) {
        int32_t size = read(fd, pImage, width * height);
        close(fd);
        return size;
    }
    return PLAT_FILE_CMD_FAIL;
}

#ifdef FS_USE_FTS
int plat_save_file(char* path, unsigned char* buf, unsigned int len) {
    int retval = PLAT_FILE_CMD_FAIL;
    int fd, write_len;
    egislog_d("%s: path %s", __func__, path);

    if (buf == NULL || len <= 0) {
        egislog_e("%s failed , invalid param", __func__);
        return PLAT_FILE_CMD_INVALID_PARAM;
    }

    write_len = f_write_fts(path, buf, len);
    if (write_len != len) {
        egislog_e("%s failed, write_len=%d (%d)", __func__, write_len, len);
        retval = PLAT_FILE_CMD_FAIL;
    } else {
        retval = write_len;
    }
    return retval;
}

int plat_load_file(char* path, unsigned char* buf, unsigned int len, unsigned int* real_size) {
    int retval = -1;
    int fd, read_len;
    egislog_d("%s: path %s", __func__, path);

    if (path == NULL || buf == NULL || len == 0) {
        egislog_e("%s failed , invalid param", __func__);
        return PLAT_FILE_CMD_INVALID_PARAM;
    }

    read_len = f_read_fts(path, (char*)buf, len);
    if (read_len < 0) {
        egislog_e("%s failed , read_len=%d", __func__, read_len);
        retval = PLAT_FILE_CMD_FAIL;
        read_len = 0;
    } else {
        retval = read_len;
    }
    if (real_size) {
        *real_size = read_len;
    }
    return retval;
}

int plat_remove_file(char* path) {
    if (path) {
        return f_remove_fts(path);
    }
    return PLAT_FILE_CMD_INVALID_PARAM;
}

#else

int plat_save_file(char* path, unsigned char* buf, unsigned int len) {
    int retval = PLAT_FILE_CMD_FAIL;
    int fd, write_len;
    egislog_d("%s: path %s", __func__, path);

    if (buf == NULL || len <= 0) {
        egislog_e("%s failed , invalid param", __func__);
        return PLAT_FILE_CMD_INVALID_PARAM;
    }
    fd = qsee_sfs_open(path, O_RDWR | O_CREAT | O_TRUNC);
    if (!fd) {
        egislog_e("%s failed , return fd = 0", __func__);
        return PLAT_FILE_CMD_FAIL;
    }

    write_len = qsee_sfs_write(fd, (const char*)buf, len);
    if (write_len != len) {
        egislog_e("%s failed, write_len=%d (%d)", __func__, write_len, len);
        retval = PLAT_FILE_CMD_FAIL;
    } else {
        retval = write_len;
    }
    qsee_sfs_close(fd);
    return retval;
}

int plat_load_file(char* path, unsigned char* buf, unsigned int len, unsigned int* real_size) {
    int retval = -1;
    int fd, read_len;
    egislog_d("%s: path %s", __func__, path);

    if (path == NULL || buf == NULL || len == 0) {
        egislog_e("%s failed , invalid param", __func__);
        return PLAT_FILE_CMD_INVALID_PARAM;
    }

    fd = qsee_sfs_open(path, O_RDONLY);
    if (!fd) {
        egislog_e("%s failed , fd=0", __func__);
        return PLAT_FILE_CMD_FAIL;
    }

    read_len = qsee_sfs_read(fd, (char*)buf, len);
    if (read_len < 0) {
        egislog_e("%s failed , read_len=%d", __func__, read_len);
        retval = PLAT_FILE_CMD_FAIL;
    } else {
        retval = read_len;
    }
    if (real_size) {
        *real_size = read_len;
    }
    qsee_sfs_close(fd);
    return retval;
}

int plat_remove_file(char* path) {
    if (path) {
        return qsee_sfs_rm(path);
    }
    return PLAT_FILE_CMD_INVALID_PARAM;
}
#endif
