#include "bsp_tz_spi.h"
#include "egis_definition.h"
#include "plat_eeprom.h"
#include "plat_heap.h"
#include "plat_log.h"
#include "plat_reset.h"
#include "plat_spi.h"
#include "plat_time.h"

#define LOG_TAG "RBS-SENSORCONTROL"
/* For microchip sst */
/* microchip sst communication opcode */
#define MICROCHIP_SST_WRSR 0x01            /* Write-Status-Register */
#define MICROCHIP_SST_PAGE_PROGRAM 0x02    /* To program up to 256 Bytes */
#define MICROCHIP_SST_READ 0x03            /* Read Memory */
#define MICROCHIP_SST_WRDI 0x04            /* Write-Disable */
#define MICROCHIP_SST_RDSR 0x05            /* Read-Status-Register */
#define MICROCHIP_SST_WREN 0x06            /* Write-Enable */
#define MICROCHIP_SST_CHIP_ERASE 0x60      /* Erase Full Memory Array */
#define MICROCHIP_SST_SECTOR_ERASE 0xD7    /* Erase 4 KByle sector */
#define MICROCHIP_SST_BLOCK_ERASE 0xD8     /* Erase 64 KByle block */
#define MICROCHIP_SST_READ_HIGH_SPEED 0x0B /* HIGH_SPEED Read Memory */
#define MICROCHIP_SST_DPD 0xB9             /* Enter deep power down */
#define MICROCHIP_SST_RELEASE_DPD 0xAB     /* RELEASE from deep power down */

#define MICROCHIP_SST_PAGE_SIZE 256        /* MICROCHIP_SST_PAGE_SIZE 256 byte */
#define MICROCHIP_SST_SECTOR_SIZE_4K 4096  /* MICROCHIP_SST_SECTOR_SIZE 4096 4K*/
#define MICROCHIP_SST_BLOCK_SIZE_64K 65536 /* MICROCHIP_SST_BLOCK_SIZE_64K 65536 64K*/

#define MICROCHIP_SST_ADDRESS_SIZE 3
#define MICROCHIP_SST_RW_OFFSET 4 /* OP code 8 bits and address 32bits */
/* The programmed data must be between 1 to 256 Bytes and in whole byte
 * increments; sending less than a full byte will cause the partial byte to
 * be ignored.
 */
#define MICROCHIP_SST_PAGE_PROGRAM_LIMITATION 256
#define MICROCHIP_SST_HIGH_SPEED_READ_DUMMY_LEN 1
/* Wait to write/erase finish */
#define MICROCHIP_SST_STATUS_MAX_RETRY_COUNT 3000

// function for flash
int io_eeprom_rdsr(BYTE* eeprom_status) {
    bsp_tzspi_transfer_t tx;
    bsp_tzspi_transfer_t rx;
    BYTE in_buffer[2];

    //	BYTE *out_buffer = (BYTE *)plat_alloc(total_size);

    int ret = 0;

    in_buffer[0] = MICROCHIP_SST_RDSR;

    tx.buf_addr = in_buffer;
    tx.buf_len = 2;

    rx.buf_addr = in_buffer;
    rx.buf_len = 2;
    eg_ta_sensor_reset_low();
    //    plat_sleep_time(1);

    ret = bsp_tzspi_full_duplex(&tx, &rx);
    if (ret != 0) {
        egislog_e("bsp_tzspi_full_duplex fail ret = %d", ret);
        ret = EGIS_COMMAND_FAIL;
        goto exit;
    }

    eg_ta_sensor_reset_high();
    //    plat_sleep_time(1);
    //    egislog_e("out_buffer= %d", in_buffer[1]);
    *eeprom_status = in_buffer[1];
exit:
    return ret;
}

int io_eeprom_wrsr(BYTE wrsr) {
    bsp_tzspi_transfer_t tx;
    bsp_tzspi_transfer_t rx;
    BYTE in_buffer[2];
    BYTE rdsr = 1;
    //	BYTE *out_buffer = (BYTE *)plat_alloc(total_size);
    int ret = 0;
    int status_retry_count = 0;
    in_buffer[0] = MICROCHIP_SST_WRSR;
    in_buffer[1] = wrsr;

    tx.buf_addr = in_buffer;
    tx.buf_len = 2;

    rx.buf_addr = in_buffer;
    rx.buf_len = 2;
    eg_ta_sensor_reset_low();
    plat_sleep_time(1);

    ret = bsp_tzspi_full_duplex(&tx, &rx);
    if (ret != 0) {
        egislog_e("bsp_tzspi_full_duplex fail ret = %d", ret);
        ret = EGIS_COMMAND_FAIL;
        goto exit;
    }

    eg_ta_sensor_reset_high();
    plat_sleep_time(1);

    do {
        io_eeprom_rdsr(&rdsr);
        //              egislog_d("io_eeprom_write status_retry_count = %D rdsr = 0x%X  \n",
        //              status_retry_count, rdsr);
        //      if (status < 0) {
        //          pr_err("%s get eeprom status fail status = %d\n",
        //              __func__, status);
        //          goto end;
        //      }
        if (++status_retry_count > MICROCHIP_SST_STATUS_MAX_RETRY_COUNT) {
            egislog_e("%s not finish erase eeprom\n", __func__);
            break;
        }
        plat_sleep_time(50);
    } while ((rdsr & 0x01) != 0);
    //      plat_sleep_time(1000);
    //        egislog_d("%s status_retry_count = %d -- \n",__func__, status_retry_count);

    //    egislog_e("out_buffer= %d", in_buffer[1]);

exit:
    return ret;
}

int io_eeprom_write_enable(int enable) {
    bsp_tzspi_transfer_t tx;
    bsp_tzspi_transfer_t rx;
    //       BYTE in_buffer;
    //       BYTE out_buffer = 0;
    BYTE in_buffer[1];
    BYTE rdsr;
    int ret = 0;
    int status_retry_count = 0;

    if (enable)
        in_buffer[0] = MICROCHIP_SST_WREN;
    else
        in_buffer[0] = MICROCHIP_SST_WRDI;

    tx.buf_addr = in_buffer;
    tx.buf_len = 1;

    rx.buf_addr = in_buffer;
    rx.buf_len = 1;
    eg_ta_sensor_reset_low();
    //    plat_sleep_time(1);

    ret = bsp_tzspi_full_duplex(&tx, &rx);
    if (ret != 0) {
        egislog_e("bsp_tzspi_full_duplex fail ret = %d", ret);
        ret = EGIS_COMMAND_FAIL;
        goto exit;
    }
    eg_ta_sensor_reset_high();
    //    plat_sleep_time(1);

    if (enable) {
        do {
            io_eeprom_rdsr(&rdsr);
            if (++status_retry_count > MICROCHIP_SST_STATUS_MAX_RETRY_COUNT) {
                egislog_e("%s not finish wrsr eeprom\n", __func__);
                break;
            }
        } while ((rdsr & 0x02) == 0);
    } else {
        do {
            io_eeprom_rdsr(&rdsr);
            if (++status_retry_count > MICROCHIP_SST_STATUS_MAX_RETRY_COUNT) {
                egislog_e("%s not finish rdsr eeprom\n", __func__);
                break;
            }
        } while ((rdsr & 0x02) != 0);
    }
    //    egislog_d("%s enable = %d  status_retry_count = %d -- \n",__func__, enable,
    //    status_retry_count);

exit:
    return ret;
}

int io_eeprom_write(BYTE* addr, int length, BYTE* pvalue) {
    bsp_tzspi_transfer_t tx;
    bsp_tzspi_transfer_t rx;
    BYTE rdsr = 1;
    int status_retry_count = 0;

    if (length > MICROCHIP_SST_PAGE_SIZE) {
        egislog_e("%s length = %d  can't be larger then 256 \n", length);
        return 0;
    }

    int total_size = 1 + 3 + length;
    BYTE* in_buffer = (BYTE*)plat_alloc(total_size);

    int ret = 0;
    in_buffer[0] = MICROCHIP_SST_PAGE_PROGRAM;
    in_buffer[1] = *addr;
    in_buffer[2] = *(addr + 1);
    in_buffer[3] = *(addr + 2);

    memcpy((in_buffer + 4), pvalue, length);
    //        egislog_d("io_eeprom_write in_buffer[2] =  0x%X , 0x%X 0x%X 0x%X 0x%X ----",
    //        in_buffer[2], pvalue[0], pvalue[1], pvalue[2], pvalue[3] );
    tx.buf_addr = in_buffer;
    tx.buf_len = total_size;

    rx.buf_addr = in_buffer;
    rx.buf_len = total_size;

    io_eeprom_write_enable(1);

    eg_ta_sensor_reset_low();
    //    plat_sleep_time(1);
    ret = bsp_tzspi_full_duplex(&tx, &rx);
    if (ret != 0) {
        egislog_e("bsp_tzspi_full_duplex fail ret = %d", ret);
        ret = EGIS_COMMAND_FAIL;
        goto exit;
    }
    eg_ta_sensor_reset_high();
    //    plat_sleep_time(1);

    //        io_eeprom_rdsr(&rdsr);
    //        egislog_d("io_eeprom_rdsr value =  0x%X  ----", rdsr);

    do {
        io_eeprom_rdsr(&rdsr);
        if (++status_retry_count > MICROCHIP_SST_STATUS_MAX_RETRY_COUNT) {
            egislog_e("%s not finish erase eeprom\n", __func__);
            break;
        }
        plat_sleep_time(1);
    } while ((rdsr & 0x01) != 0);
    //        egislog_d("%s status_retry_count = %d -- \n",__func__, status_retry_count);
exit:
    plat_free(in_buffer);
    return ret;
}

int io_eeprom_read_id() {
    bsp_tzspi_transfer_t tx;
    bsp_tzspi_transfer_t rx;
    BYTE out_buffer[5];
    BYTE in_buffer[1];
    int ret = 0;

    in_buffer[0] = 0x9F;

    tx.buf_addr = in_buffer;
    tx.buf_len = 5;

    rx.buf_addr = out_buffer;
    rx.buf_len = 5;

    eg_ta_sensor_reset_low();
    plat_sleep_time(1);

    ret = bsp_tzspi_full_duplex(&tx, &rx);
    if (ret != 0) {
        egislog_e("bsp_tzspi_full_duplex fail ret = %d", ret);
        ret = EGIS_COMMAND_FAIL;
        goto exit;
    }
    eg_ta_sensor_reset_high();
    plat_sleep_time(1);

    egislog_d("io_eeprom_read_id =  0x%X 0x%X 0x%X 0x%X ----  \n", out_buffer[1], out_buffer[2],
              out_buffer[3], out_buffer[4]);
exit:
    return ret;
}

int io_eeprom_block_write(BYTE* addr, int length, BYTE* pvalue) {
    BYTE addr_bw[3];
    memcpy(addr_bw, addr, 3);

    if (length > MICROCHIP_SST_BLOCK_SIZE_64K) {
        egislog_e("%s length = %d  can't be larger then 64k \n", length);
        return 0;
    }
    do {
        if (length < MICROCHIP_SST_PAGE_SIZE) {
            io_eeprom_write(addr_bw, length, pvalue);
        } else {
            io_eeprom_write(addr_bw, MICROCHIP_SST_PAGE_SIZE, pvalue);
            addr_bw[1] = addr_bw[1] + 1;
            pvalue = pvalue + MICROCHIP_SST_PAGE_SIZE;
        }
        length = length - MICROCHIP_SST_PAGE_SIZE;
    } while (length > 0);

    egislog_d("%s  addr_bw[1] =  0x%X ---", __func__, addr_bw[1]);

    return 0;
}

int io_eeprom_sector_block_erase(BYTE* addr, BYTE is_sector) {
    bsp_tzspi_transfer_t tx;
    bsp_tzspi_transfer_t rx;
    BYTE out_buffer = 0;
    BYTE rdsr = 1;
    int status_retry_count = 0;
    BYTE* in_buffer = (BYTE*)plat_alloc(4);
    int ret = 0;

    if (is_sector)
        in_buffer[0] = MICROCHIP_SST_SECTOR_ERASE;
    else
        in_buffer[0] = MICROCHIP_SST_BLOCK_ERASE;

    memcpy(in_buffer + 1, addr, 3);

    tx.buf_addr = in_buffer;
    tx.buf_len = 4;

    rx.buf_addr = in_buffer;
    rx.buf_len = 4;

    io_eeprom_write_enable(1);

    eg_ta_sensor_reset_low();
    //    plat_sleep_time(1);
    ret = bsp_tzspi_full_duplex(&tx, &rx);
    if (ret != 0) {
        egislog_e("bsp_tzspi_full_duplex fail ret = %d", ret);
        ret = EGIS_COMMAND_FAIL;
        goto exit;
    }
    eg_ta_sensor_reset_high();
    //    plat_sleep_time(1);

    do {
        io_eeprom_rdsr(&rdsr);
        if (++status_retry_count > MICROCHIP_SST_STATUS_MAX_RETRY_COUNT) {
            egislog_e("%s not finish erase eeprom----------------\n", __func__);
            break;
        }
        plat_sleep_time(2);
    } while ((rdsr & 0x01) != 0);
    //    plat_sleep_time(5);
    egislog_d("%s is_sector = %d status_retry_count = %d -- \n", __func__, is_sector,
              status_retry_count);

exit:
    plat_free(in_buffer);
    return ret;
}

int io_eeprom_sector_write(BYTE* addr, int length, BYTE* pvalue) {
    BYTE addr_sw[3];
    memcpy(addr_sw, addr, 3);

    if (length > MICROCHIP_SST_SECTOR_SIZE_4K) {
        egislog_e("%s length = %d  can't be larger then 64k \n", length);
        return 0;
    }

    io_eeprom_sector_block_erase(addr, 1);

    do {
        if (length < MICROCHIP_SST_PAGE_SIZE) {
            io_eeprom_write(addr_sw, length, pvalue);
        } else {
            io_eeprom_write(addr_sw, MICROCHIP_SST_PAGE_SIZE, pvalue);
            if (addr_sw[1] == 255) {
                addr_sw[0] = addr_sw[0] + 1;
                addr_sw[1] = 0;
            } else
                addr_sw[1] = addr_sw[1] + 1;
            pvalue = pvalue + MICROCHIP_SST_PAGE_SIZE;
        }
        length = length - MICROCHIP_SST_PAGE_SIZE;
    } while (length > 0);

    //    egislog_d("%s  addr_sw[1] =  0x%X ---", __func__, addr_sw[1] );

    return 0;
}

int io_eeprom_sector_write_anysize(BYTE* addr, int length, BYTE* pvalue) {
#ifndef EEPROM_USE
    return 0;
#endif
    BYTE addr_sw[3];
    memcpy(addr_sw, addr, 3);

    do {
        if (length < MICROCHIP_SST_SECTOR_SIZE_4K) {
            io_eeprom_sector_write(addr_sw, length, pvalue);
        } else {
            io_eeprom_sector_write(addr_sw, MICROCHIP_SST_SECTOR_SIZE_4K, pvalue);
            if (addr_sw[1] == 0xF0) {
                addr_sw[0] = addr_sw[0] + 1;
                addr_sw[1] = 0;
            } else
                addr_sw[1] = addr_sw[1] + 0x10;
            pvalue = pvalue + MICROCHIP_SST_SECTOR_SIZE_4K;
        }
        length = length - MICROCHIP_SST_SECTOR_SIZE_4K;
    } while (length > 0);

    io_eeprom_write_enable(0);

    egislog_d("%s  last sector addr_sw[0] =  0x%X addr_sw[1] =  0x%X ---", __func__, addr_sw[0],
              addr_sw[1]);

    return 0;
}

int io_eeprom_chip_erase() {
    bsp_tzspi_transfer_t tx;
    bsp_tzspi_transfer_t rx;
    BYTE in_buffer = MICROCHIP_SST_CHIP_ERASE;
    BYTE out_buffer = 0;
    BYTE rdsr = 1;
    int status_retry_count = 0;
    //	BYTE *out_buffer = (BYTE *)plat_alloc(total_size);
    int ret = 0;

    tx.buf_addr = &in_buffer;
    tx.buf_len = 1;

    rx.buf_addr = &out_buffer;
    rx.buf_len = 1;

    io_eeprom_write_enable(1);

    eg_ta_sensor_reset_low();
    plat_sleep_time(1);
    ret = bsp_tzspi_full_duplex(&tx, &rx);
    if (ret != 0) {
        egislog_e("bsp_tzspi_full_duplex fail ret = %d", ret);
        ret = EGIS_COMMAND_FAIL;
        goto exit;
    }
    eg_ta_sensor_reset_high();
    plat_sleep_time(1);

    do {
        io_eeprom_rdsr(&rdsr);
        if (++status_retry_count > MICROCHIP_SST_STATUS_MAX_RETRY_COUNT) {
            egislog_e("%s not finish erase eeprom----------------\n", __func__);
            break;
        }
        plat_sleep_time(100);  // must be 100 for chip erase
    } while ((rdsr & 0x01) != 0);

    plat_sleep_time(5);

//    egislog_d("%s status_retry_count = %d -- \n",__func__, status_retry_count);
exit:
    return ret;
}

int io_release_dpd(BYTE is_rdpd) {
    bsp_tzspi_transfer_t tx;
    bsp_tzspi_transfer_t rx;
    BYTE in_buffer;
    int ret = 0;

    if (is_rdpd)
        in_buffer = MICROCHIP_SST_RELEASE_DPD;
    else
        in_buffer = MICROCHIP_SST_DPD;

    tx.buf_addr = &in_buffer;
    tx.buf_len = 1;

    rx.buf_addr = &in_buffer;
    rx.buf_len = 1;

    eg_ta_sensor_reset_low();
    plat_sleep_time(1);
    ret = bsp_tzspi_full_duplex(&tx, &rx);
    if (ret != 0) {
        egislog_e("%s bsp_tzspi_full_duplex fail ret = %d", __func__, ret);
        ret = EGIS_COMMAND_FAIL;
        goto exit;
    }
    eg_ta_sensor_reset_high();
    plat_sleep_time(1);

    egislog_d("%s is_rdpd = %d ok ------\n", __func__, is_rdpd);

exit:
    return ret;
}

int io_eeprom_high_speed_read(BYTE* addr, int length, BYTE* pvalue) {
    bsp_tzspi_transfer_t tx;
    bsp_tzspi_transfer_t rx;

    if (length > MICROCHIP_SST_PAGE_SIZE) {
        egislog_e("%s length = %d  can't be larger then 256 \n", __func__, length);
        return 0;
    }
    int total_size = 1 + 4 + length;
    BYTE* in_buffer = (BYTE*)plat_alloc(total_size);
    int ret = 0;
    in_buffer[0] = MICROCHIP_SST_READ_HIGH_SPEED;
    in_buffer[1] = *addr;
    in_buffer[2] = *(addr + 1);
    in_buffer[3] = *(addr + 2);

    tx.buf_addr = in_buffer;
    tx.buf_len = total_size;
    rx.buf_addr = in_buffer;
    rx.buf_len = total_size;

    eg_ta_sensor_reset_low();
    plat_sleep_time(1);

    ret = bsp_tzspi_full_duplex(&tx, &rx);
    if (ret != 0) {
        egislog_e("bsp_tzspi_full_duplex fail ret = %d", ret);
        ret = EGIS_COMMAND_FAIL;
        goto exit;
    }
    memcpy(pvalue, in_buffer + 5, length);
    //        egislog_d("%s = 0x%X 0x%X 0x%X 0x%X 0x%X 0x%X 0x%X 0x%X 0x%X 0x%X ----", __func__,
    //        in_buffer[0], in_buffer[1], in_buffer[2], in_buffer[3], in_buffer[4], in_buffer[5],
    //        in_buffer[6], in_buffer[7], in_buffer[8], in_buffer[9]);

    eg_ta_sensor_reset_high();
    plat_sleep_time(1);
exit:
    plat_free(in_buffer);
    return ret;
}

int io_eeprom_read(BYTE* addr, int length, BYTE* pvalue) {
    bsp_tzspi_transfer_t tx;
    bsp_tzspi_transfer_t rx;
    int total_size = 1 + 3 + length;
    if (length > MICROCHIP_SST_PAGE_SIZE) {
        egislog_e("%s length = %d  can't be larger then 256 \n", length);
        return 0;
    }
    BYTE* in_buffer = (BYTE*)plat_alloc(total_size);
    int ret = 0;
    in_buffer[0] = MICROCHIP_SST_READ;
    in_buffer[1] = *addr;
    in_buffer[2] = *(addr + 1);
    in_buffer[3] = *(addr + 2);
    tx.buf_addr = in_buffer;
    tx.buf_len = total_size;
    rx.buf_addr = in_buffer;
    rx.buf_len = total_size;

    eg_ta_sensor_reset_low();
    plat_sleep_time(1);

    ret = bsp_tzspi_full_duplex(&tx, &rx);
    if (ret != 0) {
        egislog_e("bsp_tzspi_full_duplex fail ret = %d", ret);
        ret = EGIS_COMMAND_FAIL;
        goto exit;
    }
    memcpy(pvalue, in_buffer + 4, length);
    //    egislog_d("%s = 0x%X 0x%X 0x%X 0x%X 0x%X 0x%X 0x%X 0x%X 0x%X 0x%X ----", __func__,
    //    in_buffer[0], in_buffer[1], in_buffer[2], in_buffer[3], out_buffer[4], out_buffer[5],
    //    out_buffer[6], out_buffer[7], out_buffer[8], out_buffer[9]);
    eg_ta_sensor_reset_high();
    plat_sleep_time(1);
exit:
    plat_free(in_buffer);
    return ret;
}

int io_eeprom_read_anysize(BYTE* addr, int length, BYTE* pvalue) {
#ifndef EEPROM_USE
    return 0;
#endif
    BYTE addr_ra[3];
    memcpy(addr_ra, addr, 3);

#if FLASH_HIGH_SPEED_READ
    egislog_d("%s  FLASH_HIGH_SPEED_READ ----\n", __func__);
#else
    egislog_d("%s  Not FLASH_HIGH_SPEED_READ ----\n", __func__);
#endif

    do {
        if (length < MICROCHIP_SST_PAGE_SIZE)
#if FLASH_HIGH_SPEED_READ
            io_eeprom_high_speed_read(addr_ra, length, pvalue);
#else
            io_eeprom_read(addr_ra, length, pvalue);
#endif
        else {
#if FLASH_HIGH_SPEED_READ
            io_eeprom_high_speed_read(addr_ra, MICROCHIP_SST_PAGE_SIZE, pvalue);
#else
            io_eeprom_read(addr_ra, MICROCHIP_SST_PAGE_SIZE, pvalue);
#endif
            if (addr_ra[1] == 255) {
                addr_ra[0] = addr_ra[0] + 1;
                addr_ra[1] = 0;
            } else
                addr_ra[1] = addr_ra[1] + 1;
            pvalue = pvalue + MICROCHIP_SST_PAGE_SIZE;
        }
        length = length - MICROCHIP_SST_PAGE_SIZE;
    } while (length > 0);
    return 0;
}  // function for flash
