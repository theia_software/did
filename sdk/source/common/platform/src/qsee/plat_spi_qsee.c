#include "bsp_tz_spi.h"
#include "egis_definition.h"
#include "plat_heap.h"
#include "plat_log.h"
#include "plat_spi.h"
#include "plat_time.h"

#define GET_FRAME_SHIFT (2)
#define RW_EFUSE_SHIFT (1)

#define LOG_TAG "RBS-SENSORCONTROL"

HANDLE g_device_handle = 0;
int g_opcode = -1;
BOOL g_is_device_open = FALSE;

#define ET713_SPI_OP_EF_R 0x40
#define ET713_SPI_OP_EF_W 0x42
#define ET713_SPI_OP_ZAVG_R 0x60
#define ET713_SPI_OP_HSTG_R 0x62
#define ET713_EF_CSR 0xD0
//#define SPI_OTP_CLK 12000000

int io_dispatch_spi_test() {
    int ret = 0;
    return ret;
}

int io_dispatch_connect(HANDLE* device_handle) {
    egislog_d("%s start g_is_device_open=%d", __func__, g_is_device_open);
    int ret;
    if (g_is_device_open) return EGIS_OK;
    ret = bsp_tzspi_open();
    if (ret) {
        bsp_tzspi_close();
        ret = bsp_tzspi_open();
        if (ret) {
            egislog_e("%s start fail error = %d", __func__, ret);
            if (device_handle) *device_handle = 0;
            return EGIS_NO_DEVICE;
        }
    }

    if (device_handle) *device_handle = g_device_handle;
    g_is_device_open = TRUE;
    egislog_d("%s g_is_device_open=%d", __func__, g_is_device_open);
    return EGIS_OK;
}

int io_dispatch_disconnect(void) {
    egislog_d("%s start g_is_device_open=%d", __func__, g_is_device_open);
    int ret;
    if (!g_is_device_open) return EGIS_OK;
    ret = bsp_tzspi_close();
    if (ret) {
        egislog_e("%s start fail error = %d", __func__, ret);
        return EGIS_COMMAND_FAIL;
    }
    g_is_device_open = FALSE;
    egislog_d("%s g_is_device_open=%d", __func__, g_is_device_open);
    return EGIS_OK;
}

int io_dispatch_read_register(BYTE address, BYTE* value) {
    int ret;
    unsigned char write_buf[2];
    unsigned char read_buf[2];
    bsp_tzspi_transfer_t tr_write;
    bsp_tzspi_transfer_t tr_read;

    if (!value) return EGIS_COMMAND_FAIL;

    g_opcode = FP_REGISTER_READ;

    // Write 0xAC Address
    write_buf[0] = 0xAC;
    write_buf[1] = address;

    tr_write.buf_addr = write_buf;
    tr_write.buf_len = 2;
    tr_write.total_len = 0;

    ret = bsp_tzspi_write(&tr_write);
    if (ret != 0) {
        egislog_e("bsp_tzspi_write fail ret = %d", ret);
        ret = EGIS_COMMAND_FAIL;
        goto exit;
    }

    // Write 0xAF then read data

    write_buf[0] = 0xAF;
    write_buf[1] = 0;

    read_buf[0] = 0;
    read_buf[1] = 0;

    tr_write.buf_addr = write_buf;
    tr_write.buf_len = 2;
    tr_write.total_len = 0;

    tr_read.buf_addr = read_buf;
    tr_read.buf_len = 2;
    tr_read.total_len = 0;

    ret = bsp_tzspi_full_duplex(&tr_write, &tr_read);
    if (ret != 0) {
        egislog_e("bsp_tzspi_full_duplex fail ret = %d", ret);
        ret = EGIS_COMMAND_FAIL;
        goto exit;
    }

    value[0] = read_buf[1];

    ret = EGIS_OK;

exit:

    g_opcode = -1;

    return ret;
}

int io_dispatch_write_register(BYTE address, BYTE value) {
    int ret;
    unsigned char write_buf[2];
    bsp_tzspi_transfer_t tr_write;

    g_opcode = FP_REGISTER_WRITE;

    // Write 0xAC Address
    write_buf[0] = 0xAC;
    write_buf[1] = address;

    tr_write.buf_addr = write_buf;
    tr_write.buf_len = 2;
    tr_write.total_len = 0;

    ret = bsp_tzspi_write(&tr_write);
    if (ret != 0) {
        egislog_e("bsp_tzspi_write fail ret = %d", ret);
        ret = EGIS_COMMAND_FAIL;
        goto exit;
    }

    // Write 0xAE value
    write_buf[0] = 0xAE;
    write_buf[1] = value;

    tr_write.buf_addr = write_buf;
    tr_write.buf_len = 2;
    tr_write.total_len = 0;

    ret = bsp_tzspi_write(&tr_write);
    if (ret != 0) {
        ret = EGIS_COMMAND_FAIL;
        egislog_e("bsp_tzspi_write fail ret = %d", ret);
        goto exit;
    }
    ret = EGIS_OK;

exit:

    g_opcode = -1;

    return ret;
}

int io_dispatch_get_frame(BYTE* frame, UINT height, UINT width, UINT number_of_frames) {
    int ret;
    UINT size = height * width, total_data_len = size * number_of_frames + 1 + GET_FRAME_SHIFT;
    BYTE* write_buf = (BYTE*)plat_alloc(total_data_len);
    BYTE* read_buf = (BYTE*)plat_alloc(total_data_len);
    bsp_tzspi_transfer_t tr_write;
    bsp_tzspi_transfer_t tr_read;

    if (write_buf == 0) {
        ret = EGIS_COMMAND_FAIL;
        egislog_e("%s allocate memory fail", __func__);
        goto exit;
    }
    if (read_buf == 0) {
        ret = EGIS_COMMAND_FAIL;
        egislog_e("%s allocate memory fail", __func__);
        goto exit;
    }

    io_dispatch_set_tgen(TRUE);

    g_opcode = FP_GET_ONE_IMG;

    // Write 0xAC Address 0
    write_buf[0] = 0xAC;
    write_buf[1] = 0;

    tr_write.buf_addr = write_buf;
    tr_write.buf_len = 2;
    tr_write.total_len = 0;

    ret = bsp_tzspi_write(&tr_write);
    if (ret != 0) {
        egislog_e("bsp_tzspi_write fail ret = %d", ret);
        ret = EGIS_COMMAND_FAIL;
        goto exit;
    }

    // Write 0xAF then read data
    memset(write_buf, 0, total_data_len);
    memset(read_buf, 0, total_data_len);
    write_buf[0] = 0xAF;

    tr_write.buf_addr = write_buf;
    tr_write.buf_len = total_data_len;
    tr_write.total_len = 0;

    tr_read.buf_addr = read_buf;
    tr_read.buf_len = total_data_len;
    tr_read.total_len = 0;

    ret = bsp_tzspi_full_duplex(&tr_write, &tr_read);
    if (ret != 0) {
        egislog_e("bsp_tzspi_full_duplex fail ret = %d", ret);
        ret = EGIS_COMMAND_FAIL;
        goto exit;
    }
    memcpy(frame, read_buf + 1 + GET_FRAME_SHIFT, size * number_of_frames);
    ret = EGIS_OK;

exit:
    g_opcode = -1;

    plat_free(write_buf);
    plat_free(read_buf);
    io_dispatch_set_tgen(FALSE);

    return ret;
}

static int get_alignment_buf_size(int len) {
#define DIVISION_OF_IMAGE 4
    if (len > 64 && len % DIVISION_OF_IMAGE != 0)
        return (len + (DIVISION_OF_IMAGE - (len % DIVISION_OF_IMAGE)));
    else
        return len;
}

int io_dispatch_read_eFuse(BYTE* buf, UINT len) {
    int ret, buf_max_len;
    BYTE* buf_addr = NULL;
    bsp_tzspi_transfer_t tr_buf;

    buf_max_len = get_alignment_buf_size(len + RW_EFUSE_SHIFT);

    buf_addr = (BYTE*)plat_alloc(buf_max_len);
    if (buf_addr == NULL) {
        ret = EGIS_COMMAND_FAIL;
        egislog_e("buf_addr is NULL");
        goto exit;
    }

    memset(buf_addr, 0, buf_max_len);

    buf_addr[0] = 0x40;

    tr_buf.buf_addr = buf_addr;
    tr_buf.buf_len = buf_max_len;

    ret = bsp_tzspi_full_duplex(&tr_buf, &tr_buf);
    if (ret != 0) {
        egislog_e("ret = %d", ret);
        ret = EGIS_COMMAND_FAIL;
        goto exit;
    }

    memcpy(buf, buf_addr + RW_EFUSE_SHIFT, len);

exit:
    plat_free(buf_addr);
    return ret;
}

#define ET5XX_WRITE_OPCODE 0x24
#define ET5XX_READ_OPCODE 0x20
#define ET5XX_BURST_READ_OPCODE 0x22
#define ET5XX_BURST_WRITE_OPCODE 0x26
#define ET5XX_REVERSE_WRITE_OPCODE 0x27
#define ET5XX_GET_IMAGE_OPCODE 0x50

#define ET5_SERIES_WRITE_OP ET5XX_WRITE_OPCODE
#define ET5_SERIES_BURST_WRITE_OP ET5XX_BURST_WRITE_OPCODE
#define ET5_SERIES_REVERSE_WRITE_OP ET5XX_REVERSE_WRITE_OPCODE
#define ET5_SERIES_READ_OP ET5XX_READ_OPCODE
#define ET5_SERIES_BURST_READ_OP ET5XX_BURST_READ_OPCODE
#define ET5_SERIES_GET_IMAGE_OP ET5XX_GET_IMAGE_OPCODE

int io_dispatch_set_tgen(BOOL enable) {
    return 0;
}
#define OPCODE_5XX_SIZE 1

int io_5xx_dispatch_write_burst_register(BYTE start_addr, BYTE len, BYTE* pvalue);
int io_5xx_dispatch_write_register(BYTE address, BYTE value) {
    return io_5xx_dispatch_write_burst_register(address, 1, &value);
}

#define WRITE_REGISTER_COMMAND_SIZE 2
#define BURST_5XX_RD_WR_REG_SIZE 64
int io_5xx_dispatch_read_burst_register(BYTE start_addr, BYTE len, BYTE* pdata) {
    bsp_tzspi_transfer_t tx;
    bsp_tzspi_transfer_t rx;
    int in_len = WRITE_REGISTER_COMMAND_SIZE;
    int total_size = in_len + len;
    BYTE in_buffer[WRITE_REGISTER_COMMAND_SIZE] = {0};
    BYTE* out_buffer = (BYTE*)plat_alloc(total_size);
    int ret = 0;

    in_buffer[0] = ET5_SERIES_BURST_READ_OP;
    in_buffer[1] = start_addr;

    tx.buf_addr = in_buffer;
    tx.buf_len = total_size;

    rx.buf_addr = out_buffer;
    rx.buf_len = total_size;

    ret = bsp_tzspi_full_duplex(&tx, &rx);
    if (ret != 0) {
        egislog_e("bsp_tzspi_full_duplex fail ret = %d", ret);
        ret = EGIS_COMMAND_FAIL;
        goto exit;
    }

    memcpy(pdata, out_buffer + in_len, len);
exit:
    plat_free(out_buffer);
    return ret;
}

/*
 * Read Register workaround.
 */
int io_5xx_dispatch_read_register(BYTE address, BYTE* value) {
    if (!value) return EGIS_COMMAND_FAIL;
    return io_5xx_dispatch_read_burst_register(address, 1, value);
}

int ET_5xx_dispatch_write_register(BYTE address, BYTE value) {
    return io_5xx_dispatch_write_register(address, value);
}

BYTE ET_5xx_dispatch_read_register(BYTE address, const char* func) {
    BYTE value = 0;
    int ret = io_5xx_dispatch_read_register(address, &value);
    if (ret != EGIS_OK) {
        egislog_e("%s, addr = 0x%x, ret = %d", func, address, ret);
    }
    return value;
}

int io_5xx_dispatch_write_reverse_register(BYTE start_addr, BYTE len, BYTE* pvalue) {
    bsp_tzspi_transfer_t tx;
    bsp_tzspi_transfer_t rx;
    int in_len = WRITE_REGISTER_COMMAND_SIZE;
    int total_size = in_len + len;
    BYTE in_buffer[BURST_5XX_RD_WR_REG_SIZE] = {0};
    int ret = 0;

    in_buffer[0] = ET5_SERIES_REVERSE_WRITE_OP;
    in_buffer[1] = start_addr;
    memcpy(&in_buffer[2], pvalue, len);

    tx.buf_addr = in_buffer;
    tx.buf_len = total_size;

    rx.buf_addr = in_buffer;
    rx.buf_len = total_size;

    ret = bsp_tzspi_full_duplex(&tx, &rx);
    if (ret != 0) {
        egislog_e("bsp_tzspi_full_duplex fail ret = %d", ret);
        ret = EGIS_COMMAND_FAIL;
    }
    return ret;
}

int io_5xx_dispatch_write_burst_register(BYTE start_addr, BYTE len, BYTE* pvalue) {
    bsp_tzspi_transfer_t tx;
    bsp_tzspi_transfer_t rx;
    int in_len = WRITE_REGISTER_COMMAND_SIZE;
    int total_size = in_len + len;
    BYTE in_buffer[BURST_5XX_RD_WR_REG_SIZE] = {0};
    int ret = 0;

    in_buffer[0] = ET5_SERIES_BURST_WRITE_OP;
    in_buffer[1] = start_addr;
    memcpy(&in_buffer[2], pvalue, len);

    tx.buf_addr = in_buffer;
    tx.buf_len = total_size;

    rx.buf_addr = in_buffer;
    rx.buf_len = total_size;
    ret = bsp_tzspi_full_duplex(&tx, &rx);
    if (ret != 0) {
        egislog_e("bsp_tzspi_full_duplex fail ret = %d", ret);
        ret = EGIS_COMMAND_FAIL;
    }
    return ret;
}

int io_5xx_dispatch_get_frame(BYTE* frame, UINT height, UINT width, UINT multi,
                              UINT number_of_frames) {
    bsp_tzspi_transfer_t tx;
    bsp_tzspi_transfer_t rx;
    int in_len = 1;
    // BYTE in_buffer[1] = {ET5_SERIES_GET_IMAGE_OP};
    int total_size = 1 + height * width * number_of_frames;
    BYTE* in_buffer = (BYTE*)plat_alloc(total_size);
    BYTE* out_buffer = (BYTE*)plat_alloc(total_size);
    int ret = 0;

    *in_buffer = ET5_SERIES_GET_IMAGE_OP;

    tx.buf_addr = in_buffer;
    tx.buf_len = total_size;

    rx.buf_addr = out_buffer;
    rx.buf_len = total_size;

    ret = bsp_tzspi_full_duplex(&tx, &rx);
    if (ret != 0) {
        egislog_e("bsp_tzspi_full_duplex fail ret = %d", ret);
        ret = EGIS_COMMAND_FAIL;
        goto exit;
    }
    memcpy(frame, out_buffer + in_len, height * width * number_of_frames);
exit:
    plat_free(in_buffer);
    plat_free(out_buffer);
    return ret;
}

int polling_registry(BYTE addr, BYTE expect, BYTE mask) {
    int i;
    BYTE value;
    // egislog_d("%s",__func__);
    for (i = 0; i < 3000; i++) {
        if (io_5xx_dispatch_read_register(addr, &value) != EGIS_OK) return EGIS_COMMAND_FAIL;
        if ((value & mask) == expect) return EGIS_OK;
    }
    egislog_e("%s, value&mask = 0x%x, expect = 0x%x", __func__, value & mask, expect);

    return EGIS_COMMAND_FAIL;
}

int io_5xx_read_vdm(BYTE* frame, UINT height, UINT width) {
    bsp_tzspi_transfer_t tx;
    bsp_tzspi_transfer_t rx;
    int in_len = 1;
    // BYTE in_buffer[1] = {0x60};
    int total_size = 1 + height * width;
    BYTE* in_buffer = (BYTE*)plat_alloc(total_size);
    BYTE* out_buffer = (BYTE*)plat_alloc(total_size);
    int ret = 0;

    *in_buffer = 0x60;
    tx.buf_addr = in_buffer;
    tx.buf_len = total_size;

    rx.buf_addr = out_buffer;
    rx.buf_len = total_size;

    ret = bsp_tzspi_full_duplex(&tx, &rx);
    if (ret != 0) {
        egislog_e("bsp_tzspi_full_duplex fail ret = %d", ret);
        ret = EGIS_COMMAND_FAIL;
        goto exit;
    }

    memcpy(frame, out_buffer + in_len, height * width);
exit:
    plat_free(in_buffer);
    plat_free(out_buffer);
    return ret;
}

int io_5xx_write_vdm(BYTE* frame, UINT height, UINT width) {
    bsp_tzspi_transfer_t tx;
    bsp_tzspi_transfer_t rx;
    int ret = 0;
    int total_size = 1 + height * width;
    BYTE* in_buffer = (BYTE*)plat_alloc(total_size);

    if (in_buffer == NULL) {
        egislog_e("memory allocation fail");
        return EGIS_OUT_OF_MEMORY;
    }
    memcpy(in_buffer + 1, frame, height * width);

    in_buffer[0] = 0x62;

    tx.buf_addr = in_buffer;
    tx.buf_len = total_size;

    rx.buf_addr = in_buffer;
    rx.buf_len = total_size;

    ret = bsp_tzspi_full_duplex(&tx, &rx);
    if (ret != 0) {
        egislog_e("bsp_tzspi_full_duplex fail ret = %d", ret);
        ret = EGIS_COMMAND_FAIL;
    }
    plat_free(in_buffer);
    return ret;
}

int io_6xx_get_clb(BYTE* buffer, UINT length) {
    bsp_tzspi_transfer_t tx;
    bsp_tzspi_transfer_t rx;
    int in_len = 1;
    // BYTE in_buffer[1] = {0x60};
    int total_size = 1 + 2 * length;
    BYTE* in_buffer = (BYTE*)plat_alloc(total_size);
    BYTE* out_buffer = (BYTE*)plat_alloc(total_size);
    int ret = 0;

    *in_buffer = 0x64;
    tx.buf_addr = in_buffer;
    tx.buf_len = total_size;

    rx.buf_addr = out_buffer;
    rx.buf_len = total_size;

    ret = bsp_tzspi_full_duplex(&tx, &rx);
    if (ret != 0) {
        egislog_e("bsp_tzspi_full_duplex fail ret = %d", ret);
        ret = EGIS_COMMAND_FAIL;
        goto exit;
    }

    memcpy(buffer, out_buffer + in_len, 2 * length);
exit:
    plat_free(in_buffer);
    plat_free(out_buffer);
    return ret;
}

int io_6xx_set_clb(BYTE* buffer, UINT length) {
    bsp_tzspi_transfer_t tx;
    bsp_tzspi_transfer_t rx;
    int ret = 0;
    int total_size = 1 + 2 * length;
    BYTE* in_buffer = (BYTE*)plat_alloc(total_size);

    if (in_buffer == NULL) {
        egislog_e("memory allocation fail");
        return EGIS_OUT_OF_MEMORY;
    }
    memcpy(in_buffer + 1, buffer, 2 * length);

    in_buffer[0] = 0x66;

    tx.buf_addr = in_buffer;
    tx.buf_len = total_size;

    rx.buf_addr = in_buffer;
    rx.buf_len = total_size;

    ret = bsp_tzspi_full_duplex(&tx, &rx);
    if (ret != 0) {
        egislog_e("bsp_tzspi_full_duplex fail ret = %d", ret);
        ret = EGIS_COMMAND_FAIL;
    }
    plat_free(in_buffer);
    return ret;
}

#define CMD_NV_EN 0x44
#define CMD_NV_RD1 0x40
#define CMD_NV_RD2 0x00
#define CMD_NV_DIS 0x48
#define CMD_DUMMY 0x00
#define SPI_LOW_CLK 4000000
#define NVM_LEN 64
int io_5xx_read_nvm(BYTE* buffer) {
    int result = 0;
    uint32_t spi_freq = 0;
    uint8_t nvm_read_enable[2] = {CMD_NV_EN, CMD_DUMMY};
    uint8_t read_chip_op[3] = {CMD_NV_RD1, CMD_NV_RD2, CMD_DUMMY};
    uint8_t nvm_disable[2] = {CMD_NV_DIS, CMD_DUMMY};
    bsp_tzspi_transfer_t tx_transfer, rx_transfer;

    spi_freq = bsp_set_spi_clk(SPI_LOW_CLK);
    tx_transfer.buf_addr = nvm_read_enable;
    tx_transfer.buf_len = 2;
    result = bsp_tzspi_write(&tx_transfer);

    tx_transfer.buf_addr = read_chip_op;
    tx_transfer.buf_len = NVM_LEN + 3;
    rx_transfer.buf_addr = buffer;
    rx_transfer.buf_len = NVM_LEN + 3;
    result = bsp_tzspi_full_duplex(&tx_transfer, &rx_transfer);

    tx_transfer.buf_addr = nvm_disable;
    tx_transfer.buf_len = 2;
    result = bsp_tzspi_write(&tx_transfer);

    bsp_set_spi_clk(spi_freq);
    return result;
}

int io_713_dispatch_read_32byte_otp(BYTE* pdata) {
    bsp_tzspi_transfer_t tx;
    bsp_tzspi_transfer_t rx;
    uint32_t spi_freq = 0;
    BYTE ef_status;
    int in_len = 1;
    int total_size = in_len + 32;
    BYTE in_buffer[1] = {0};
    BYTE* out_buffer = (BYTE*)plat_alloc(total_size);
    int ret = 0;
    int i;

    io_5xx_dispatch_write_register(ET713_EF_CSR, 0x01);
    for (i = 0; i < 10; i++) {
        plat_wait_time(5);
        io_5xx_dispatch_read_register(ET713_EF_CSR, &ef_status);
        egislog_d("ET713_EF_CSR register value = 0x%x", ef_status);
        if ((ef_status >> 7) != 0) {
            egislog_e(
                "Delayed %d ms, Enableing eFuse failed. Check ET713_EF_CSR.busy, it should be 0",
                i * 5);
            ret = EGIS_COMMAND_FAIL;
            goto exit;
        } else
            break;
    }

    in_buffer[0] = ET713_SPI_OP_EF_R;

    tx.buf_addr = in_buffer;
    tx.buf_len = total_size;

    rx.buf_addr = out_buffer;
    rx.buf_len = total_size;

    ret = bsp_tzspi_full_duplex(&tx, &rx);
    if (ret != 0) {
        egislog_e("bsp_tzspi_full_duplex fail ret = %d", ret);
        ret = EGIS_COMMAND_FAIL;
        goto exit;
    }
    io_5xx_dispatch_write_register(ET713_EF_CSR, 0x00);
    memcpy(pdata, out_buffer + in_len, 32);
exit:
    plat_free(out_buffer);
    return ret;
}

int io_et713_lense_type_uuid(unsigned char* uuid) {
    int result = 0;
    BYTE et713_32byte_otp[32] = {0};
    memset(et713_32byte_otp, 0x17, 32);
    io_713_dispatch_read_32byte_otp(et713_32byte_otp);

    egislog_i("-  %s  et713_32byte_otp  0x%x 0x%x 0x%x 0x%x 0x%x 0x%x 0x%x 0x%x", __func__,
              et713_32byte_otp[0x00], et713_32byte_otp[0x01], et713_32byte_otp[0x02],
              et713_32byte_otp[0x03], et713_32byte_otp[0x04], et713_32byte_otp[0x05],
              et713_32byte_otp[0x06], et713_32byte_otp[0x07]);
    egislog_i("-  %s  et713_32byte_otp  0x%x 0x%x 0x%x 0x%x 0x%x 0x%x 0x%x 0x%x", __func__,
              et713_32byte_otp[0x08], et713_32byte_otp[0x09], et713_32byte_otp[0x0A],
              et713_32byte_otp[0x0B], et713_32byte_otp[0x0C], et713_32byte_otp[0x0D],
              et713_32byte_otp[0x0E], et713_32byte_otp[0x0F]);
    egislog_i("-  %s  et713_32byte_otp  0x%x 0x%x 0x%x 0x%x 0x%x 0x%x 0x%x 0x%x", __func__,
              et713_32byte_otp[0x10], et713_32byte_otp[0x11], et713_32byte_otp[0x12],
              et713_32byte_otp[0x13], et713_32byte_otp[0x14], et713_32byte_otp[0x15],
              et713_32byte_otp[0x16], et713_32byte_otp[0x17]);
    egislog_i("-  %s  et713_32byte_otp  0x%x 0x%x 0x%x 0x%x 0x%x 0x%x 0x%x 0x%x", __func__,
              et713_32byte_otp[0x18], et713_32byte_otp[0x19], et713_32byte_otp[0x1A],
              et713_32byte_otp[0x1B], et713_32byte_otp[0x1C], et713_32byte_otp[0x1D],
              et713_32byte_otp[0x1E], et713_32byte_otp[0x1F]);

    egislog_i("----  ET713 UUID==%x %02x %02x %02x %02x %02x %02x %02x %02x %02x ==",
              et713_32byte_otp[0x13], et713_32byte_otp[0x17], et713_32byte_otp[0x18],
              et713_32byte_otp[0x19], et713_32byte_otp[0x1A], et713_32byte_otp[0x1B],
              et713_32byte_otp[0x1C], et713_32byte_otp[0x1D], et713_32byte_otp[0x1E],
              et713_32byte_otp[0x1F]);
    uuid[0] = et713_32byte_otp[0x13];
    memcpy(uuid + 1, &et713_32byte_otp[0x17], 9);
    return result;
}

#define OP_CIS_REG_R 0x20
#define OP_CIS_REG_W 0x30
#define OP_ZAVG_R 0x60
#define OP_GET_FRAME 0x61
#define OP_PRE_CAPTURE 0x62

#define DRDY_PIN_NAME "tlmm_gpio_fps_drdy"

int32_t io_7xx_read_cis_register(uint16_t addr, uint8_t* buf) {
    int32_t result = 0;

    uint8_t tr[] = {OP_CIS_REG_R, 0x00, 0x00, 0x00};
    bsp_tzspi_transfer_t tx_transfer, rx_transfer;
    uint32_t in = 1;

    /*egislog_d("%s, addr(0x%04x), buf(0x%x)", __func__, addr, buf);*/

    plat_wait_time(5);

    tx_transfer.buf_addr = tr;
    tx_transfer.buf_len = 4;
    tr[1] = (addr & 0xFF00) >> 8;
    tr[2] = (addr & 0x00FF);
    /*egislog_d("%s tr[0 - 2](0x%02x, 0x%02x, 0x%02x) ",
            __func__, tr[0], tr[1], tr[2]);*/
    result = bsp_tzspi_write(&tx_transfer);
    if (result != 0) {
        egislog_e("bsp_tzspi_write ret = %d", result);
        result = EGIS_COMMAND_FAIL;
        goto exit;
    }

    plat_wait_time(5);

    tr[0] = 0x00;
    /*egislog_d("%s tr[0](0x%02x), before read.", __func__, tr[0]);*/
    rx_transfer.buf_addr = tr;
    rx_transfer.buf_len = 1;
    result = bsp_tzspi_read(&rx_transfer);
    if (result < 0) {
        egislog_e("bsp_tz_read ret = %d", result);
        goto exit;
    }

    /*egislog_d("%s tr[0](0x%02x), after read.", __func__, tr[0]);*/
    buf[0] = tr[0];
exit:
    return result;
}

int32_t io_7xx_write_cis_register(uint16_t addr, uint8_t value) {
    int32_t result = 0;

    uint8_t tr[] = {OP_CIS_REG_W, 0x00, 0x00, 0x00};
    bsp_tzspi_transfer_t tx_transfer;
    uint32_t in = 1;

    /*egislog_d("%s, addr(0x%04x), value(0x%02x)", __func__, addr, value);*/

    plat_wait_time(5);

    tx_transfer.buf_addr = tr;
    tx_transfer.buf_len = 4;
    tr[1] = (addr & 0xFF00) >> 8;
    tr[2] = (addr & 0x00FF);
    tr[3] = value;
    result = bsp_tzspi_write(&tx_transfer);
    if (result != 0) {
        egislog_e("bsp_tzspi_write ret = %d", result);
        result = EGIS_COMMAND_FAIL;
        goto exit;
    }
exit:
    return result;
}

int32_t io_7xx_dispatch_pre_capture() {
    int32_t result;
    uint32_t in;
    uint8_t tr[] = {OP_PRE_CAPTURE, 0x00, 0x00, 0x00};
    bsp_tzspi_transfer_t tx_transfer;

    plat_wait_time(5);

    tx_transfer.buf_addr = tr;
    tx_transfer.buf_len = 4;
    result = bsp_tzspi_write(&tx_transfer);
    if (result != 0) {
        egislog_e("bsp_tzspi_write ret = %d", result);
        result = EGIS_COMMAND_FAIL;
        goto exit;
    }
    plat_wait_time(140);

    tr[0] = 0x63;
    tx_transfer.buf_addr = tr;
    tx_transfer.buf_len = 4;
    result = bsp_tzspi_write(&tx_transfer);
    if (result != 0) {
        egislog_e("bsp_tzspi_write ret = %d", result);
        result = EGIS_COMMAND_FAIL;
        goto exit;
    }
    plat_wait_time(50);
exit:
    return result;
}

int32_t __io_7xx_dispatch_get_cis_frame_sector(uint32_t sector, uint32_t size, uint8_t* fb) {
    int32_t result;
    uint32_t in;
    uint8_t tr[] = {OP_GET_FRAME, 0x00, 0x00, 0x00};
    bsp_tzspi_transfer_t tx_transfer, rx_transfer;

    /*egislog_d("%s, size(%d), fb(0x%x), sector(%d)",
                   __func__, size, fb, sector);*/

    plat_wait_time(5);

    tx_transfer.buf_addr = tr;
    tx_transfer.buf_len = 4;
    tr[1] = (uint8_t)sector;
    tr[2] = (size & 0xFF00) >> 8;
    tr[3] = size & 0x00FF;
    /*egislog_d("%s tr[0 - 3](0x%02x, 0x%02x, 0x%02x, 0x%02x) ",
            __func__, tr[0], tr[1], tr[2], tr[3]);*/
    result = bsp_tzspi_write(&tx_transfer);
    if (result != 0) {
        egislog_e("bsp_tzspi_write ret = %d", result);
        result = EGIS_COMMAND_FAIL;
        goto exit;
    }

    plat_wait_time(5);

    /*egislog_d("%s tr[0](0x%02x), before read.", __func__, tr[0]);*/
    rx_transfer.buf_addr = fb;
    rx_transfer.buf_len = size;
    result = bsp_tzspi_read(&rx_transfer);
    if (result < 0) {
        egislog_e("bsp_tz_read ret = %d", result);
        goto exit;
    }
exit:
    return result;
}

int32_t io_7xx_dispatch_get_cis_frame(uint32_t size, uint8_t* fb) {
    int32_t result;
    uint32_t sector, last_sector_size = 0, i;

    sector = size / 0xFFFF;
    last_sector_size = size % 0xFFFF;

    /*egislog_d("%s, size(%d), fb(0x%x), sector(%d), last_sector_size(%d)",
                   __func__, size, fb, sector, last_sector_size);*/

    for (i = 0; i < sector; i++)
        result = __io_7xx_dispatch_get_cis_frame_sector(i, 0xFFFF, fb + i * 0xFFFF);

    if (sector == 0) {
        result = __io_7xx_dispatch_get_cis_frame_sector(0, size, fb);
    } else {
        result =
            __io_7xx_dispatch_get_cis_frame_sector(sector, last_sector_size, fb + sector * 0xFFFF);
    }
    return result;
}

int32_t io_7xx_dispatch_write_register(uint16_t addr, uint8_t value) {
    return io_5xx_dispatch_write_register(addr, value);
}

int32_t io_7xx_dispatch_read_register(uint16_t addr, uint8_t* value) {
    if (!value) return EGIS_COMMAND_FAIL;
    return io_5xx_dispatch_read_burst_register(addr, 1, value);
}

int32_t io_7xx_dispatch_get_frame(uint8_t* buffer, uint32_t length, uint32_t frames) {
    return io_5xx_dispatch_get_frame(buffer, length, 1, 0, frames);
}

#define ET713_ZONE_AVG_LENGTH 72

static int32_t __get_zone_average(BYTE* buffer) {
    bsp_tzspi_transfer_t tx;
    bsp_tzspi_transfer_t rx;
    int in_len = 1;
    // BYTE in_buffer[1] = {0x60};
    int total_size = 1 + ET713_ZONE_AVG_LENGTH;
    BYTE* in_buffer = (BYTE*)plat_alloc(total_size);
    BYTE* out_buffer = (BYTE*)plat_alloc(total_size);
    int ret = 0;

    *in_buffer = OP_ZAVG_R;
    tx.buf_addr = in_buffer;
    tx.buf_len = total_size;

    rx.buf_addr = out_buffer;
    rx.buf_len = total_size;

    ret = bsp_tzspi_full_duplex(&tx, &rx);
    if (ret != 0) {
        egislog_e("bsp_tzspi_full_duplex fail ret = %d", ret);
        ret = EGIS_COMMAND_FAIL;
        goto exit;
    }

    memcpy(buffer, out_buffer + in_len, ET713_ZONE_AVG_LENGTH);
exit:
    plat_free(in_buffer);
    plat_free(out_buffer);
    return ret;
}

int32_t io_dispatch_command_read(enum io_dispatch_cmd cmd, int param1, int param2, uint8_t* out_buf,
                                 int* out_buf_size) {
    int32_t ret = EGIS_OK;
    switch (cmd) {
        case IOCMD_READ_ZONE_AVERAGE:
            egislog_d("%s __get_zone_average", __func__, cmd);
            ret = __get_zone_average(out_buf);
            egislog_d("%s __get_zone_average end , %d", __func__, ret);
            break;

        case IOCMD_READ_HISTOGRAM:
        case IOCMD_READ_EFUSE:
            egislog_e("%s [%d] not supported yet", __func__, cmd);
            break;

        default:
            egislog_e("%s [%d] not supported", __func__, cmd);
            break;
    }
    return ret;
}

int32_t io_7xx_dispatch_sensor_select(uint16_t sensor_sel) {
    egislog_e("%s not supported yet", __func__);
    return EGIS_OK;
}

int32_t io_7xx_dispatch_spi_write_read_halfduplex(uint8_t* buffer, int writeLen, int readLen)
{
    egislog_e("%s not supported yet", __func__);
    return EGIS_OK;
}

int32_t io_7xx_dispatch_set_spi_clk(uint8_t mode, uint8_t clk)
{
    egislog_e("%s not supported yet", __func__);
    return EGIS_OK;
}
