#include "ext_inc/qsee_heap.h"
#include "plat_mem.h"

extern void* qsee_malloc(size_t size);
extern void* qsee_realloc(void* ptr, size_t size);
extern void qsee_free(void* ptr);

void* mem_alloc(uint32_t size) {
    return qsee_malloc(size);
}
void* mem_realloc(void* ptr, uint32_t new_size) {
    return qsee_realloc(ptr, new_size);
}
void mem_free(void* ptr) {
    qsee_free(ptr);
}
void mem_move(void* dest, void* src, uint32_t size) {
    memcpy(dest, src, size);
}
void mem_set(void* dest, uint32_t data, uint32_t size) {
    memset(dest, data, size);
}
int mem_compare(void* buff1, void* buff2, uint32_t size) {
    return memcmp(buff1, buff2, size);
}

#ifdef EGIS_DEBUG_MEMORY
#include "egis_mem_debug.h"
#include "plat_log.h"
#define LOG_TAG "RBS-MEM_DEBUG"
static int g_sys_mem_init = 0;
void sys_memory_init(void) {
    g_sys_mem_init = 1;
}
void* sys_alloc(uint32_t count, uint32_t size, const char* file_name, int line) {
    void* addr = NULL;
    if (size == 0)
        return NULL;
    else
        addr = qsee_malloc(size);

    if (count == 0) {
        // egislog_d("*-*-* %s, %p:%d", file_name, addr, size);
        char name[NAME_LENGTH];
        egis_strncpy(name, file_name, NAME_LENGTH);
        register_mem(addr, size, name, line);
    } else {
        // egislog_d("*-*-* G3, %p:%d", addr, size);
        if (g_sys_mem_init == 1)
            register_mem(addr, size, "ALGO", 1);
        else
            register_mem(addr, size, "ALGO", 0);
    }
    return addr;
}

void sys_free(void* ptr) {
    if (ptr == NULL) {
        // egislog_d("## sys_free addr=NULL");
        return;
    }
    qsee_free(ptr);
    unregister_mem(ptr);
}

void* sys_realloc(void* ptr, uint32_t new_size) {
    void* addr;
    addr = qsee_realloc(ptr, new_size);

    // egislog_d("*-*-* realloc, %x, %p:%d", ptr, buf, new_size);
    unregister_mem(ptr);
    if (new_size > 0 && addr != NULL) {
        if (g_sys_mem_init == 1)
            register_mem(addr, new_size, "realloc", 1);
        else
            register_mem(addr, new_size, "realloc", 0);
    }
    return addr;
}
#else
void* sys_alloc(uint32_t count, uint32_t size) {
    size *= count;
    return qsee_malloc(size);
}

void sys_free(void* ptr) {
    qsee_free(ptr);
}
void* sys_realloc(void* ptr, uint32_t new_size) {
    return qsee_realloc(ptr, new_size);
}
#endif