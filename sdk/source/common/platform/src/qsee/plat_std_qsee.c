#include "plat_std.h"
#include "type_definition.h"

int plat_atoi(const char* ptr) {
    int ret = 0;
    int sign = 1;

    if (ptr == NULL) {
        return 0;
    }

    while (*ptr && *ptr == ' ') {
        ptr++;
    }

    if (*ptr == '-') {
        sign = -1;
    }

    if (*ptr == '-' || *ptr == '+') {
        ptr++;
    }

    while (*ptr && *ptr >= '0' && *ptr <= '9') {
        ret = ret * 10 + *ptr - '0';
        ptr++;
    }

    ret = sign * ret;
    return ret;
}

double plat_atof(const char* ptr) {
    double s = 0.0;
    double d = 10.0;
    int jishu = 0;
    int sign = 1;

    while (*ptr && *ptr == ' ') {
        ptr++;
    }

    if (*ptr == '-') {
        sign = -1;
        ptr++;
    }

    if (*ptr == '-' || *ptr == '+') {
        ptr++;
    }

    if (!(*ptr >= '0' && *ptr <= '9')) {
        return s;
    }

    while (*ptr && *ptr >= '0' && *ptr <= '9' && *ptr != '.') {
        s = s * 10.0 + *ptr - '0';
        ptr++;
    }

    if (*ptr && *ptr == '.') {
        ptr++;
    }

    while (*ptr && *ptr >= '0' && *ptr <= '9') {
        s = s + (*ptr - '0') / d;
        d *= 10.0;
        ptr++;
    }

    return s * sign;
}