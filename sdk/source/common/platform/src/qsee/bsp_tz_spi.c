#include <comdef.h>
#include <qsee_log.h>
#include <qsee_spi.h>
#include <qsee_tlmm.h>
#include <stddef.h>
#include <stdint.h>
#include <string.h>

#include "plat_log.h"
#include "plat_spi.h"

#define LOG_TAG "RBS-SPI"

#define SPI_DEVICE_ID QSEE_SPI_DEVICE_16
#ifdef SPI_CLOCK_20M
#define MAX_CLOCK_SPEED 20000000
#else
#define MAX_CLOCK_SPEED 9600000
#endif

static qsee_spi_config_t spi_config;

//#define SPI_MODE_3
#define SPI_MODE_0
int bsp_tzspi_open(void) {
    int retval = 0;
    qsee_log(QSEE_LOG_MSG_ERROR, "bsp_tzspi_open");
#ifdef SPI_MODE_3
    spi_config.spi_bits_per_word = 8;
    spi_config.spi_clk_always_on = QSEE_SPI_CLK_NORMAL;
    spi_config.spi_clk_polarity = QSEE_SPI_CLK_IDLE_HIGH;
    spi_config.spi_cs_mode = QSEE_SPI_CS_KEEP_ASSERTED;  // Keep low between two byte in MODE 3
                                                         // while data transfering
    // spi_config.spi_cs_polarity = QSEE_SPI_CS_ACTIVE_HIGH;
    spi_config.spi_cs_polarity = QSEE_SPI_CS_ACTIVE_LOW;  // It maybe means CS is pull high while
                                                          // idle and pull down while data
                                                          // transfering
    spi_config.spi_shift_mode = QSEE_SPI_OUTPUT_FIRST_MODE;
#endif

#ifdef SPI_MODE_0
    spi_config.spi_bits_per_word = 8;
    spi_config.spi_clk_always_on = QSEE_SPI_CLK_NORMAL;
    spi_config.spi_clk_polarity = QSEE_SPI_CLK_IDLE_LOW;
    spi_config.spi_cs_mode = QSEE_SPI_CS_KEEP_ASSERTED;
    spi_config.spi_cs_polarity = QSEE_SPI_CS_ACTIVE_LOW;
    spi_config.spi_shift_mode = QSEE_SPI_INPUT_FIRST_MODE;
#endif

    spi_config.max_freq = MAX_CLOCK_SPEED;
    spi_config.hs_mode = 0;
    spi_config.loopback = 0;

    retval = qsee_spi_open(SPI_DEVICE_ID);
    if (retval != 0) {
        qsee_log(QSEE_LOG_MSG_ERROR, "%s: qsee_spi_open FAILED on device: %d with retval = %d",
                 __func__, SPI_DEVICE_ID, retval);
    } else {
        qsee_log(QSEE_LOG_MSG_DEBUG, "%s: qsee_spi_open OK on device: %d", __func__, SPI_DEVICE_ID);
    }
    return retval;
}

int bsp_tzspi_read(bsp_tzspi_transfer_t* p_read_info) {
    int retval = 0;
    qsee_spi_transaction_info_t r = {
        .buf_addr = NULL,
        .buf_len = 0,
        .total_bytes = 0,
    };
    r.buf_addr = p_read_info->buf_addr;
    r.buf_len = p_read_info->buf_len;
    retval = qsee_spi_read(SPI_DEVICE_ID, &spi_config, &r);
    if (retval != 0) {
        qsee_log(QSEE_LOG_MSG_DEBUG, "%s - qsee_spi_read FAILED: retval = %d\n", __func__, retval);
    }
    p_read_info->total_len = r.total_bytes;
    return retval;
}

int bsp_tzspi_write(bsp_tzspi_transfer_t* p_write_info) {
    int retval = 0;
    qsee_spi_transaction_info_t t = {
        .buf_addr = NULL,
        .buf_len = 0,
        .total_bytes = 0,
    };
    t.buf_addr = p_write_info->buf_addr;
    t.buf_len = p_write_info->buf_len;
    retval = qsee_spi_write(SPI_DEVICE_ID, &spi_config, &t);
    if (retval != 0) {
        qsee_log(QSEE_LOG_MSG_DEBUG, "%s - qsee_spi_write FAILED: retval = %d\n", __func__, retval);
    }
    p_write_info->total_len = t.total_bytes;
    return retval;
}

int bsp_tzspi_full_duplex(bsp_tzspi_transfer_t* p_write_info, bsp_tzspi_transfer_t* p_read_info) {
    int retval = 0;
    qsee_spi_transaction_info_t t =
                                    {
                                        .buf_addr = NULL,
                                        .buf_len = 0,
                                        .total_bytes = 0,
                                    },
                                r = {
                                    .buf_addr = NULL,
                                    .buf_len = 0,
                                    .total_bytes = 0,
                                };

    t.buf_addr = p_write_info->buf_addr;
    t.buf_len = p_write_info->buf_len;
    r.buf_addr = p_read_info->buf_addr;
    r.buf_len = p_read_info->buf_len;
    retval = qsee_spi_full_duplex(SPI_DEVICE_ID, &spi_config, &t, &r);
    if (retval != 0) {
        qsee_log(QSEE_LOG_MSG_DEBUG, "%s - qsee_spi_full_duplex FAILED: retval = %d\n", __func__,
                 retval);
    }
    p_write_info->total_len = t.total_bytes;
    p_read_info->total_len = r.total_bytes;
    return retval;
}

uint32_t bsp_set_spi_clk(uint32_t clk) {
    uint32_t originalClk = spi_config.max_freq;
    spi_config.max_freq = clk;
    return originalClk;
}

int bsp_tzspi_close(void) {
    int ret;
    ret = qsee_spi_close(SPI_DEVICE_ID);
    return ret;
}

int bsp_tzgpio_read(uint8* name, uint32* value) {
    uint32 gpio_key;
    qsee_tlmm_config_t my_config;
    qsee_gpio_value_t myval = 0;
    int index = 0;
    qsee_gpio_value_t in = 1;

    my_config.drive = QSEE_GPIO_6MA;
    my_config.pull = QSEE_GPIO_NO_PULL;
    my_config.direction = QSEE_GPIO_INPUT;

    egislog_d("%s, name(%s)", __func__, name);
    if (qsee_tlmm_get_gpio_id((const char*)name, &gpio_key) != 0) {
        egislog_e("qsee_tlmm_get_gpio_id: Failed");
        return -1;
    }
    if (qsee_tlmm_config_gpio_id(gpio_key, &my_config) != 0) {
        egislog_e("qsee_tlmm_config_gpio_id: Failed");
        return -1;
    }

    if (qsee_tlmm_select_gpio_id_mode(gpio_key, QSEE_GPIO_MODE_GENERAL) != 0) {
        egislog_e(
            "qsee_tlmm_select_gpio_id_mode to generic (2nd time): "
            "Failed");
        return -1;
    }
    if (qsee_tlmm_gpio_id_in(gpio_key, &in) != 0) {
        egislog_e("qsee_tlmm_gpio_id_out output-LOW: Failed");
        return -1;
    } else {
        *value = (uint32)in;
        egislog_d("%s = %d, in(%d)", __func__, in);
    }
    if (qsee_tlmm_release_gpio_id(gpio_key) != 0) {
        egislog_e("qsee_tlmm_release_gpio_id: Failed");
        return -1;
    }
    return 0;
}
