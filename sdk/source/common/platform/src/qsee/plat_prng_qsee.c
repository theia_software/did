#include "plat_log.h"
#include "plat_prng.h"

extern int qsee_get_random_bytes(void* buf, int len);

int plat_prn_generate(void* buf, unsigned int size) {
    return qsee_get_random_bytes(buf, size);
}