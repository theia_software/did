#ifndef __PLATFORM_QSEE_H__
#define __PLATFORM_QSEE_H__

// qsee_fs.h
#define O_ACCMODE 00000003
#define O_RDONLY 00000000
#define O_WRONLY 00000001
#define O_RDWR 00000002
#ifndef O_CREAT
#define O_CREAT 00000100
#endif
#define O_TRUNC 00001000
#include <stdint.h>

int open(const char* path, int mode, ...);
int dbg_open(void* cmd, const char* path, int mode, ...);

int openat(int fd, const char* path, int mode, ...);
int fcntl(int fd, int command, ...);
int creat(const char* path, uint32_t mode);

int32_t read(int, void*, uint32_t);
int32_t write(int, const void*, uint32_t);
int close(int);
int32_t lseek(int, int32_t, int);

int unlink(const char*);
int rmdir(const char*);

// int fstat(int, fs_stat *);
// int lstat(const char *, fs_stat *);
int mkdir(const char*, uint32_t);
int testdir(const char*);
int remove(const char*);
int fsync(int fd);

int file_end(void);

int file_get_partition_freee_size(const char* path, uint64_t* size);

int get_error_number(void);

#endif