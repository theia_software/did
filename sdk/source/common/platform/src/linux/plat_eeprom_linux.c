#include "plat_eeprom.h"
#include "plat_log.h"
#include "type_definition.h"

#define LOG_TAG "RBS-SENSORCONTROL"

int io_eeprom_sector_write_anysize(BYTE* addr, int length, BYTE* pvalue) {
    egislog_e("%s not supported flash", __func__);
    return 0;
}
int io_eeprom_read_anysize(BYTE* addr, int length, BYTE* pvalue) {
    egislog_e("%s not supported flash", __func__);
    return 0;
}
