
#include <stdio.h>
#ifdef ANDROID
#include <android/log.h>
#else
#include <stdarg.h>
#endif
#include <string.h>
#include <sys/types.h>
#include <unistd.h>

#include "plat_log.h"
//#include "demotooljni.h"

#define MAX_BUFLEN 1024

#define JNIAPI_CALLBACK_ON_LOG (4001)

#ifdef EGIS_DBG
LOG_LEVEL g_log_level = LOG_VERBOSE;
#else
LOG_LEVEL g_log_level = LOG_INFO;
#endif

#ifdef __OTG_SENSOR__
#include "plat_otg.h"
#define MAX_BUFLEN_NATIVELOG (2 * 1024)
#define MAX_BUFFER_SIZE (16 * 1024)
extern event_callback_t g_event_callback;
char buffer_nativelog[MAX_BUFLEN_NATIVELOG];
char g_log_keeper[MAX_BUFFER_SIZE];
int g_write_index = 0;
#endif

void set_debug_level(LOG_LEVEL level) {
    output_log(LOG_ERROR, "RBS", "", "", 0, "set_debug_level %d -> %d", g_log_level, level);
    g_log_level = level;
}

#ifdef ANDROID
void output_log(LOG_LEVEL level, const char* tag, const char* file_name, const char* func, int line,
                const char* format, ...) {
    char buffer[MAX_BUFLEN];

    if (format == NULL) return;
    if (g_log_level > level) return;
#ifdef DISABLE_ALGOAPI_LOG
    if (strcmp(tag, "ETS-ALGOAPI") == 0) {
        return;
    }
#endif

    va_list vl;
    va_start(vl, format);
    vsnprintf(buffer, MAX_BUFLEN, format, vl);
    va_end(vl);

    char* log_level = "D";

    int prio;
    switch (level) {
        case LOG_ASSERT:
        case LOG_ERROR:
            prio = ANDROID_LOG_ERROR;
            log_level = "E";
            break;
        case LOG_WARN:
            prio = ANDROID_LOG_WARN;
            log_level = "W";
            break;
        case LOG_INFO:
            prio = ANDROID_LOG_INFO;
            log_level = "I";
            break;
        case LOG_DEBUG:
            prio = ANDROID_LOG_DEBUG;
            log_level = "D";
            break;
        case LOG_VERBOSE:
            prio = ANDROID_LOG_VERBOSE;
            log_level = "V";
            break;
        default:
            prio = ANDROID_LOG_ERROR;
            break;
    }
    __android_log_print(prio, tag, "[%s] [%s:%d] %s \n", file_name, func, line, buffer);
#ifdef __OTG_SENSOR__
    int pid = getpid();
    int tid = gettid();
    snprintf(buffer_nativelog, MAX_BUFLEN_NATIVELOG, "%5d %5d %s %s [%s:%d] %s [%s]\n", pid, tid,
             log_level, tag, file_name, line, buffer, func);
    memcpy(g_log_keeper + g_write_index, buffer_nativelog, strlen(buffer_nativelog));
    g_write_index += strlen(buffer_nativelog);
    __android_log_print(ANDROID_LOG_DEBUG, tag, "[%s] [%s:%d] last_write_index=%d \n", file_name,
                        func, line, g_write_index);
    if (g_write_index + sizeof(buffer_nativelog) > MAX_BUFFER_SIZE - 1) {
        if(g_event_callback != NULL){
            g_event_callback(JNIAPI_CALLBACK_ON_LOG, 3, 0, (unsigned char*)g_log_keeper, g_write_index);
        }
        g_write_index = 0;
    }
#endif
}
#else
void output_log(LOG_LEVEL level, const char* tag, const char* file_name, const char* func, int line,
                const char* format, ...) {
    char buffer[MAX_BUFLEN];

    if (format == NULL) return;
    if (g_log_level > level) return;

    va_list vl;
    va_start(vl, format);
    vsnprintf(buffer, MAX_BUFLEN, format, vl);
    va_end(vl);

    switch (level) {
        case LOG_ERROR:
            printf("ERROR !! [%s] [%s:%d] %s \n", file_name, func, line, buffer);
            break;
        default:
            printf("[%s] [%s:%d] %s \n", file_name, func, line, buffer);
            break;
    }
}

#endif
