#ifdef __OTG_SENSOR__

#include <unistd.h>

#include "egis_definition.h"
#include "plat_heap.h"
#include "plat_log.h"
#include "plat_otg.h"
#include "type_definition.h"

#define LOG_TAG "RBS-PLAT-OTG"

event_callback_t g_event_callback = NULL;

static void event_callback(int event_id, int first_param, int second_param, unsigned char* data,
                           int data_size) {
    // egislog_v("event_callback enter g_event_callback = %p event_id = %d", g_event_callback,
    // event_id);
    if (NULL != g_event_callback) {
        g_event_callback(event_id, first_param, second_param, data, data_size);
    }
}

int32_t io_7xx_dispatch_write_register(uint16_t addr, uint8_t value) {
    // egislog_v("%s: is called", __func__);
    uint8_t data[2];
    data[0] = addr & 0xFF;
    data[1] = value;
    event_callback(EVENT_IO_DISPATCH_WRITE_REGISTER, 0, 0, data, 2);
    return EGIS_OK;
}

int32_t io_7xx_dispatch_read_register(uint16_t addr, uint8_t* value) {
    // egislog_v("%s: is called", __func__);
    uint8_t data[1];
    data[0] = addr & 0xFF;
    event_callback(EVENT_IO_DISPATCH_READ_REGISTER, 0, 0, data, 1);
    *value = data[0];
    return EGIS_OK;
}

int32_t io_7xx_dispatch_get_frame(uint8_t* buffer, uint32_t length, uint32_t frames) {
    // egislog_v("%s: is called", __func__);
    event_callback(EVENT_IO_DISPATCH_GET_FRAME, (int)frames, 0, buffer, (int)length);
    return EGIS_OK;
}

int32_t io_7xx_dispatch_wakeup() {
    egislog_d("%s: is called", __func__);
    event_callback(EVENT_IO_DISPATCH_WAKEUP, 0, 0, NULL, 0);
    return EGIS_OK;
}

int32_t io_7xx_dispatch_standby() {
    egislog_d("%s: is called", __func__);
    event_callback(EVENT_IO_DISPATCH_STANDBY, 0, 0, NULL, 0);
    return EGIS_OK;
}

enum io_dispatch_cmd {
    IOCMD_READ_ZONE_AVERAGE = 700,
    IOCMD_READ_HISTOGRAM,
    IOCMD_READ_EFUSE,
};

int32_t io_dispatch_command_read(enum io_dispatch_cmd cmd, int param1, int param2, uint8_t* out_buf,
                                 int* out_buf_size) {
    switch (cmd) {
        case IOCMD_READ_ZONE_AVERAGE:
        case IOCMD_READ_HISTOGRAM:
        case IOCMD_READ_EFUSE:
            egislog_e("%s [%d] not supported yet", __func__, cmd);
            break;

        default:
            egislog_e("%s [%d] not supported", __func__, cmd);
            break;
    }
    return EGIS_OK;
}

int32_t io_7xx_dispatch_sensor_select(uint16_t sensor_sel) {
    egislog_d("%s: is called", __func__);

    // io_7xx_dispatch_sensor_select(0xFF33, sensor_sel);
    // // io_7xx_dispatch_write_register(0xFF34, sensor_sel);

    uint8_t data[4];
    data[0] = 0xFF;
    data[1] = 0x33;
    data[2] = (sensor_sel >> 8) & 0xFF;
    data[3] = sensor_sel & 0xFF;
    event_callback(EVENT_IO_DISPATCH_SENSOR_SELECT, 0, 0, data, 4);
    return EGIS_OK;
}

int io_dispatch_connect(HANDLE* device_handle) {
    return EGIS_OK;
}

int32_t io_7xx_dispatch_spi_write_read_halfduplex(uint8_t* buffer, int writeLen, int readLen)
{
    egislog_d("%s: is called", __func__);
    event_callback(EVENT_IO_DISPATCH_SPI_WRITE_READ, writeLen, readLen, buffer, writeLen > readLen? writeLen: readLen);
    return EGIS_OK;
}

int32_t io_7xx_dispatch_set_spi_clk(uint8_t mode, uint8_t clk)
{
    egislog_d("%s: mode=%d, clk=%d", __func__, mode, clk);
    event_callback(EVENT_IO_DISPATCH_SET_SPI_CLK, mode, clk, NULL, 0);
    return EGIS_OK;
}

void combine_bytes_to_int(unsigned char *src, int *dst)
{
	unsigned char *index = src;
	int ret = 0;

	ret = ret | (int)(((*index) << 24) & 0xFF000000);
	++index;
	ret = ret | (int)(((*index) << 16) & 0x00FF0000);
	++index;
	ret = ret | (int)(((*index) << 8) & 0x0000FF00);
	++index;
	ret = ret | (int)(((*index)) & 0x000000FF);

	egislog_d("ret = %d 0x%x", ret, ret);

	*dst = ret;
}

void seperate_int_to_bytes(int src, unsigned char *dst)
{
	unsigned char *index = dst;
	int i;

	*index = (unsigned char)((src >> 24)&0xFF);
	++index;
	*index = (unsigned char)((src >> 16)&0xFF);
	++index;
	*index = (unsigned char)((src >> 8)&0xFF);
	++index;
	*index = (unsigned char)((src)&0xFF);

	for (i = 0; i < sizeof(int); i++) {
		egislog_d("dst[%d] = 0x%x", i, dst[i]);
	}
}

int32_t io_760_dispatch_get_id(uint8_t *dev_id0, uint8_t *dev_id1)
{
	egislog_i("%s", __func__);
	uint8_t dev_id[2] = {0};
    event_callback(EVENT_OTG_GET_SENSOR_ID, 0, 0, dev_id, 2);
	egislog_i("dev_id0 %lu dev_id1 %lu", dev_id[0], dev_id1[1]);
	*dev_id0 = dev_id[0];
	*dev_id1 = dev_id[1];

	return 0;
}
int32_t io_760_dispatch_init_sensor()
{
	egislog_i("%s", __func__);
	event_callback(EVENT_OTG_INIT_SENSOR, 0, 0, NULL, 0);
	egislog_i("%s", __func__);
#if 0
	if (0)
	{
		int a = 0xF5AC;
		int c = -1;
		unsigned char b[4];

		seperate_int_to_bytes(a, b);
		combine_bytes_to_int(b, &c);
		egislog_i("c = %d 0x%x", c, c);

		int value = 0;
		io_760_dispatch_set_sensor_param(ET760_PARAM_EXPO_TIME, 0);
		io_760_dispatch_get_sensor_param(ET760_PARAM_EXPO_TIME, &value);

		io_760_dispatch_write_register(10, 20);
		io_760_dispatch_read_register(10, &value);
		egislog_i("value = %d 0x%x", value, value);
	}
#endif
	return 0;
}
int32_t io_760_dispatch_set_sensor_param(int param, int value)
{
	egislog_i("%s", __func__);
#if 0
	if (0)
	{
		int a = 12;
		int c = -1;
		unsigned char b[4];

		seperate_int_to_bytes(a, b);
		combine_bytes_to_int(b, &c);
		egislog_i("c = %d", c);

		event_callback(EVENT_OTG_SET_SENSOR_PARAM, ET760_PARAM_EXPO_TIME, 100, NULL, 0);
		event_callback(EVENT_OTG_SET_SENSOR_PARAM, ET760_PARAM_OFFSET, 22, NULL, 0);
		event_callback(EVENT_OTG_SET_SENSOR_PARAM, ET760_PARAM_X_START, 10, NULL, 0);
		event_callback(EVENT_OTG_SET_SENSOR_PARAM, ET760_PARAM_X_END, 200, NULL, 0);
		event_callback(EVENT_OTG_SET_SENSOR_PARAM, ET760_PARAM_Y_START, 300, NULL, 0);
		event_callback(EVENT_OTG_SET_SENSOR_PARAM, ET760_PARAM_Y_END, 400, NULL, 0);
	}
#endif
	event_callback(EVENT_OTG_SET_SENSOR_PARAM, param, value, NULL, 0);
	return 0;
}
int32_t io_760_dispatch_get_sensor_param(int param, int *value)
{
	egislog_i("%s", __func__);
	int ret = 0;
	unsigned char data[4];
#if 0
	if (0)
	{
		int a = 12;
		int c = -1;
		unsigned char b[4];

		seperate_int_to_bytes(a, b);
		combine_bytes_to_int(b, &c);
		egislog_i("c = %d", c);

		int ret = 0;
		unsigned char data[4];

		event_callback(EVENT_OTG_GET_SENSOR_PARAM, ET760_PARAM_EXPO_TIME, ret, data, 4);
		combine_bytes_to_int(data, &ret);
		egislog_i("ET760_PARAM_EXPO_TIME %d %d", ET760_PARAM_EXPO_TIME, ret);

		event_callback(EVENT_OTG_GET_SENSOR_PARAM, ET760_PARAM_OFFSET, ret, data, 4);
		combine_bytes_to_int(data, &ret);
		egislog_i("ET760_PARAM_OFFSET %d %d", ET760_PARAM_OFFSET, ret);
		
		event_callback(EVENT_OTG_GET_SENSOR_PARAM, ET760_PARAM_X_START, ret, data, 4);
		combine_bytes_to_int(data, &ret);
		egislog_i("ET760_PARAM_X_START %d %d", ET760_PARAM_X_START, ret);

		event_callback(EVENT_OTG_GET_SENSOR_PARAM, ET760_PARAM_X_END, ret, data, 4);
		combine_bytes_to_int(data, &ret);
		egislog_i("ET760_PARAM_X_END %d %d", ET760_PARAM_X_END, ret);

		event_callback(EVENT_OTG_GET_SENSOR_PARAM, ET760_PARAM_Y_START, ret, data, 4);
		combine_bytes_to_int(data, &ret);
		egislog_i("ET760_PARAM_Y_START %d %d", ET760_PARAM_Y_START, ret);

		event_callback(EVENT_OTG_GET_SENSOR_PARAM, ET760_PARAM_Y_END, ret, data, 4);
		combine_bytes_to_int(data, &ret);
		egislog_i("ET760_PARAM_Y_END %d %d", ET760_PARAM_Y_END, ret);
	}
#endif
	event_callback(EVENT_OTG_GET_SENSOR_PARAM, param, ret, data, 4);
	combine_bytes_to_int(data, &ret);
	*value = ret;
	return 0;
}
int32_t io_760_dispatch_get_frame(uint8_t *buffer, int length)
{
	egislog_i("%s", __func__);
	event_callback(EVENT_OTG_GET_IMAGE, length, 0, buffer, length);
	return 0;
}

int32_t io_760_dispatch_write_register(int addr, int value)
{
	egislog_i("%s", __func__);
	event_callback(EVENT_OTG_ET760_WRITE_REGISTER, addr, value, NULL, 0);
	return 0;
}
int32_t io_760_dispatch_read_register(int addr, int *value)
{
	egislog_i("%s", __func__);
	int ret = 0;
	unsigned char data[4];
	event_callback(EVENT_OTG_ET760_READ_REGISTER, addr, 0, data, 4);
	combine_bytes_to_int(data, &ret);
	*value = ret;
	return 0;
}

#endif


int32_t io_760_dispatch_raw_spi(int param, uint8_t *buffer, int length)
{
	egislog_d("%s", __func__);

	event_callback(EVENT_OTG_ET760_RAW_SPI, param, 0, buffer, length);
	return 0;
}
