#include <stdint.h>
#include <stdlib.h>

#include "plat_mem.h"

void* sys_alloc(uint32_t count, uint32_t size) {
    return malloc(count * size);
}
void* sys_realloc(void* data, uint32_t size) {
    return realloc(data, size);
}
void sys_free(void* data) {
    free(data);
}
