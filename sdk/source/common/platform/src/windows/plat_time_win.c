#include <stdio.h>
#include <time.h>
#include <windows.h>

#include "plat_log.h"

#define LOG_TAG "egis"

static LARGE_INTEGER g_freq;

unsigned long long plat_get_time() {
    LARGE_INTEGER time_begin;

    QueryPerformanceFrequency(&g_freq);
    QueryPerformanceCounter(&time_begin);

    return (unsigned long long)((time_begin.QuadPart * 1.0 / g_freq.QuadPart) * 1000);
}
unsigned long plat_get_diff_time(unsigned long long begin) {
    LARGE_INTEGER time_now;

    QueryPerformanceCounter(&time_now);
    unsigned long long now =
        (unsigned long long)((time_now.QuadPart * 1.0 / g_freq.QuadPart) * 1000);

    return (unsigned long)(now - begin);
}

void plat_wait_time(unsigned long msecs) {
    unsigned long long times_now, times_start = plat_get_time();
    do {
        times_now = plat_get_time();
    } while (times_now - times_start < msecs);
    return;
}

void plat_sleep_time(unsigned long timeInMs) {
    Sleep(timeInMs);
    return;
}
