#include "util/Sensor_win.h"
#include "Comport.h"
#include "plat_spi.h"
#define READ_TIMEOUT 3000
#define MAX_REG_ADDR 0xD1

HANDLE m_hCom = NULL;
HANDLE m_Mux = NULL;

const char EGIS_TAG[4] = {'E', 'G', 'I', 'S'};
const char SIGE_TAG[4] = {'S', 'I', 'G', 'E'};

int target_board_type = -1;

typedef struct _SENSOR_REG {
    unsigned char RegAddr;
    unsigned char RegType;
    unsigned char RegValue;
    unsigned char RegValueDefault;
    unsigned char RegValueMask;
} SENSOR_REG;

SENSOR_REG sensor_reg_all[MAX_REG_ADDR + 1];

BOOL winsensor_OpenSensor(int iCom) {
    if (m_hCom != NULL && m_hCom != INVALID_HANDLE_VALUE) return TRUE;

    m_hCom = hOpenCompPort(iCom);

    if (m_hCom == INVALID_HANDLE_VALUE) {
        return FALSE;
    }

    m_Mux = CreateMutex(NULL, FALSE, NULL);
    if (m_Mux == NULL) {
        winsensor_CloseSensor();
        return FALSE;
    }

    return TRUE;
}

BOOL winsensor_CloseSensor() {
    if (m_hCom != INVALID_HANDLE_VALUE) {
        PurgeComm(m_hCom, PURGE_TXABORT | PURGE_RXABORT | PURGE_TXCLEAR | PURGE_RXCLEAR);
        CloseHandle(m_hCom);
        m_hCom = INVALID_HANDLE_VALUE;
    }

    return TRUE;
}

BOOL winsensor_ReadRegister(unsigned char addr, unsigned char* pValue) {
    BOOL r_value;
    unsigned char command[512];
    unsigned char buf[512];

    if (m_hCom == INVALID_HANDLE_VALUE) {
        return FALSE;
    }

    memset(command, 0x00, sizeof(command));
    memset(buf, 0x00, sizeof(buf));

    WaitForSingleObject(m_Mux, INFINITE);

    // Signature
    command[0] = 0x45;  // 'E'
    command[1] = 0x47;  // 'G'
    command[2] = 0x49;  // 'I'
    command[3] = 0x53;  // 'S'

    // Operation code
    command[4] = 0x00;  // Read Register

    // Parameter
    command[5] = addr;  // Register Address

    // Value
    command[6] = 0x00;  // Don't care

    // Write Command
    r_value = bWriteComPort(m_hCom, command, 7);
    if (!r_value) {
        ReleaseMutex(m_Mux);
        return FALSE;
    }

    // Read Response
    r_value = bReadComPort(m_hCom, buf, 7, READ_TIMEOUT);
    if (!r_value) {
        ReleaseMutex(m_Mux);
        return FALSE;
    }

    if ((buf[0] != 'S') || (buf[1] != 'I') || (buf[2] != 'G') || (buf[3] != 'E')) {
        ReleaseMutex(m_Mux);
        return FALSE;
    }

    // buf[4]: param
    *pValue = buf[5];

    if (buf[6] != 0x01) {
        ReleaseMutex(m_Mux);
        return FALSE;
    }

    ReleaseMutex(m_Mux);

    return TRUE;
}

BOOL winsensor_WriteRegister(unsigned char addr, unsigned char value) {
    BOOL r_value;
    unsigned char command[512];
    unsigned char buf[512];

    if (m_hCom == INVALID_HANDLE_VALUE) {
        return FALSE;
    }
    WaitForSingleObject(m_Mux, INFINITE);

    memset(command, 0x00, sizeof(command));
    memset(buf, 0x00, sizeof(buf));

    // Signature
    command[0] = 0x45;  // 'E'
    command[1] = 0x47;  // 'G'
    command[2] = 0x49;  // 'I'
    command[3] = 0x53;  // 'S'

    // Operation code
    command[4] = 0x01;  // Write Register

    // Parameter
    command[5] = addr;  // Register Address

    // Value
    command[6] = value;  // Register Value to write

    // Write Command
    r_value = bWriteComPort(m_hCom, command, 7);
    if (!r_value) {
        ReleaseMutex(m_Mux);
        return FALSE;
    }

    // Read Response
    r_value = bReadComPort(m_hCom, buf, 7, READ_TIMEOUT);
    if (!r_value) {
        ReleaseMutex(m_Mux);
        return FALSE;
    }

    if ((buf[0] != 'S') || (buf[1] != 'I') || (buf[2] != 'G') || (buf[3] != 'E')) {
        ReleaseMutex(m_Mux);
        return FALSE;
    }

    // buf[4]: param
    // buf[5]: value
    if (buf[6] != 0x01) {
        ReleaseMutex(m_Mux);
        return FALSE;
    }

    ReleaseMutex(m_Mux);

    return TRUE;
}

static int write_com_port(HANDLE hdev, BYTE* buffer, int buffer_size) {
    COMSTAT stat;
    DWORD errors, dwDataWritten;

    if (!ClearCommError((HANDLE)hdev, &errors, &stat)) {
        return RESULT_SPI_COMMAND_FAIL;
    }

    if (!WriteFile((HANDLE)hdev, buffer, buffer_size, &dwDataWritten, NULL)) {
        return RESULT_SPI_COMMAND_FAIL;
    }
    return RESULT_SPI_OK;
}

static int read_com_port(HANDLE hdev, BYTE* buffer, int buffer_size) {
#define READ_TIMEOUT 3000
    DWORD begin;
    DWORD read_count;

    while (buffer_size > 0) {
        begin = GetTickCount();
        if (!ReadFile((HANDLE)hdev, buffer, buffer_size, &read_count, NULL))
            return RESULT_SPI_COMMAND_FAIL;
        if (read_count > 0) {
            buffer += read_count;
            buffer_size -= read_count;
        }
        if (GetTickCount() - begin > READ_TIMEOUT) return RESULT_SPI_COMMAND_FAIL;
    }

    if (!PurgeComm((HANDLE)hdev, PURGE_TXABORT | PURGE_RXABORT | PURGE_TXCLEAR | PURGE_RXCLEAR)) {
        return RESULT_SPI_COMMAND_FAIL;
    }
    return RESULT_SPI_OK;
}
BOOL winsensor_GetImage(unsigned char* pData, int width, int height, int frame_count) {
    BYTE command[7];
    const int buffer_size = width * height * frame_count;
    *((int*)command) = *((int*)EGIS_TAG);   // move "EGIS" 4 bytes
    command[4] = 0x64;                      // GetImage (Burst Mode)
    command[5] = (BYTE)(buffer_size >> 8);  // Transfer Length High Byte
    command[6] = (BYTE)buffer_size;         // Transfer Length LOW Byte

    if (write_com_port(m_hCom, command, (int)7) != RESULT_SPI_OK) return FALSE;
    if (read_com_port(m_hCom, pData, (int)buffer_size) != RESULT_SPI_OK) return FALSE;

    return TRUE;
}

BOOL winsensor_GetImage_16Bits(unsigned short* pData, int width, int height, int frame_count) {
    BYTE command[7];
    const int buffer_size = width * height * 2 * frame_count;
    *((int*)command) = *((int*)EGIS_TAG);   // move "EGIS" 4 bytes
    command[4] = 0x64;                      // GetImage (Burst Mode)
    command[5] = (BYTE)(buffer_size >> 8);  // Transfer Length High Byte
    command[6] = (BYTE)buffer_size;         // Transfer Length LOW Byte

    if (write_com_port(m_hCom, command, (int)7) != RESULT_SPI_OK) return FALSE;
    if (read_com_port(m_hCom, (unsigned char*)pData, (int)buffer_size) != RESULT_SPI_OK)
        return FALSE;

    return TRUE;
}

void vRegDefault() {
    int i;

    for (i = 0; i <= MAX_REG_ADDR; i++) {
        sensor_reg_all[i].RegValueDefault = 0x00;
        sensor_reg_all[i].RegValueMask = 0xFF;
    }

    sensor_reg_all[0x01].RegType = REG_TYPE_READ;
    sensor_reg_all[0x01].RegValueDefault = 0x00;

    sensor_reg_all[0x02].RegType = REG_TYPE_READ | REG_TYPE_WRITE;
    sensor_reg_all[0x02].RegValueDefault = 0x00;

    sensor_reg_all[0x03].RegType = REG_TYPE_READ | REG_TYPE_WRITE;
    sensor_reg_all[0x03].RegValueDefault = 0x80;

    sensor_reg_all[0x04].RegType = REG_TYPE_READ | REG_TYPE_WRITE;
    sensor_reg_all[0x04].RegValueDefault = 0x00;

    sensor_reg_all[0x05].RegType = REG_TYPE_READ | REG_TYPE_WRITE;
    sensor_reg_all[0x05].RegValueDefault = 0x00;

    sensor_reg_all[0x06].RegType = REG_TYPE_READ | REG_TYPE_WRITE;
    sensor_reg_all[0x06].RegValueDefault = 0x00;

    sensor_reg_all[0x07].RegType = REG_TYPE_READ | REG_TYPE_WRITE;
    sensor_reg_all[0x07].RegValueDefault = 0x00;

    sensor_reg_all[0x08].RegType = REG_TYPE_READ | REG_TYPE_WRITE;
    sensor_reg_all[0x08].RegValueDefault = 0x00;

    sensor_reg_all[0x09].RegType = REG_TYPE_READ | REG_TYPE_WRITE;
    sensor_reg_all[0x09].RegValueDefault = 0x00;
    /*
    sensor_reg_all[0x0A].RegType = REG_TYPE_READ | REG_TYPE_WRITE;
    sensor_reg_all[0x0A].RegValueDefault = 0x00;
    sensor_reg_all[0x0A].RegValueMask = 0x0F;
    */

    sensor_reg_all[0x0B].RegType = REG_TYPE_READ | REG_TYPE_WRITE;
    sensor_reg_all[0x0B].RegValueDefault = 0x0F;

    sensor_reg_all[0x10].RegType = REG_TYPE_READ | REG_TYPE_WRITE;
    sensor_reg_all[0x10].RegValueDefault = 0x00;

    sensor_reg_all[0x11].RegType = REG_TYPE_READ | REG_TYPE_WRITE;
    sensor_reg_all[0x11].RegValueDefault = 0x3F;

    sensor_reg_all[0x12].RegType = REG_TYPE_READ | REG_TYPE_WRITE;
    sensor_reg_all[0x12].RegValueDefault = 0x00;
    sensor_reg_all[0x13].RegType = REG_TYPE_READ | REG_TYPE_WRITE;
    sensor_reg_all[0x13].RegValueDefault = 0x8F;
}

BOOL winsensor_ResetRegister() {
    int i;
    BOOL r_value;

    if (m_hCom == INVALID_HANDLE_VALUE) {
        return FALSE;
    }

    vRegDefault();

    // Reset Register Value Test
    for (i = 0; i <= MAX_REG_ADDR; i++) {
        if (sensor_reg_all[i].RegType & REG_TYPE_WRITE) {
            r_value = winsensor_WriteRegister(i, sensor_reg_all[i].RegValueDefault);
            if (!r_value) {
                return FALSE;
            }

            r_value = winsensor_ReadRegister(i, &sensor_reg_all[i].RegValue);
            if (!r_value) {
                return FALSE;
            }

            if (sensor_reg_all[i].RegValue != sensor_reg_all[i].RegValueDefault) {
                return FALSE;
            }
        }
    }
    return TRUE;
}

BOOL winsensor_ReadPA0(unsigned char* pStatus) {
    winsensor_BurstReadRegister(0x01, 1, pStatus);

    return TRUE;
}

BOOL winsensor_BurstReadRegister(unsigned char start_addr, int len, unsigned char* pValue) {
    BYTE command[7];
    BYTE buffer[512];

    memset(buffer, 0x00, sizeof(buffer));

    // Signature
    *((int*)command) = *((int*)EGIS_TAG);  // move "EGIS" 4 bytes
    // Operation code
    command[4] = 0x62;  // ET510 Burst Read Register
    // Parameter
    command[5] = start_addr;  // Register Begin Address
    // Value
    command[6] = len;  // Length to read

    if (write_com_port(m_hCom, command, (int)7) != RESULT_SPI_OK) return FALSE;
    // Read Response
    if (read_com_port(m_hCom, buffer, (int)len) != RESULT_SPI_OK) return FALSE;

    memcpy(pValue, buffer, len);

    return TRUE;
}

BOOL winsensor_ReverseBurstReadRegister(unsigned char start_addr, int len, unsigned char* pValue) {
    BYTE command[7];
    BYTE buffer[512];

    memset(buffer, 0x00, sizeof(buffer));

    // Signature
    *((int*)command) = *((int*)EGIS_TAG);  // move "EGIS" 4 bytes
    // Operation code
    command[4] = 0x70;  // ET510 Burst Read Register
    // Parameter
    command[5] = start_addr;  // Register Begin Address
    // Value
    command[6] = len;  // Length to read

    if (write_com_port(m_hCom, command, (int)7) != RESULT_SPI_OK) return FALSE;
    // Read Response
    if (read_com_port(m_hCom, buffer, (int)len) != RESULT_SPI_OK) return FALSE;

    memcpy(pValue, buffer, len);

    return TRUE;
}

BOOL winsensor_BurstWriteRegister(unsigned char start_addr, int len, unsigned char* pValue) {
    BYTE command[512], buffer[8];
    // Signature
    *((int*)command) = *((int*)EGIS_TAG);  // move "EGIS" 4 bytes
    // Operation code
    command[4] = 0x63;  // Burst Write Register
    // Parameter
    command[5] = start_addr;  // Register Begin Address
    // Value
    command[6] = len;  // Length to write
    // Value
    memcpy(command + 7, pValue, len);

    if (write_com_port(m_hCom, command, (int)7 + len) != RESULT_SPI_OK) return FALSE;
    if (read_com_port(m_hCom, buffer, (int)7) != RESULT_SPI_OK) return FALSE;
    if (*((int*)buffer) != *((int*)SIGE_TAG) || buffer[6] != 0x01) return FALSE;

    // buf[4]: param
    // buf[5]: value
    if (buffer[6] != 0x01) return FALSE;

    return TRUE;
}
BOOL winsensor_ReverseBurstWriteRegister(unsigned char start_addr, int len, unsigned char* pValue) {
    BYTE command[512], buffer[8];
    // Signature
    *((int*)command) = *((int*)EGIS_TAG);  // move "EGIS" 4 bytes
    // Operation code
    command[4] = 0x71;  //  Burst Write Register
    // Parameter
    command[5] = start_addr;  // Register Begin Address
    // Value
    command[6] = len;  // Length to write
    // Value
    memcpy(command + 7, pValue, len);

    if (write_com_port(m_hCom, command, (int)7 + len) != RESULT_SPI_OK) return FALSE;
    if (read_com_port(m_hCom, buffer, (int)7) != RESULT_SPI_OK) return FALSE;
    if (*((int*)buffer) != *((int*)SIGE_TAG) || buffer[6] != 0x01) return FALSE;

    // buf[4]: param
    // buf[5]: value
    if (buffer[6] != 0x01) return FALSE;

    return TRUE;
}

BOOL winsensor_ReadVDMCmd(unsigned char* pData, int width, int height) {
    unsigned char command[512];
    int transferlength;

    transferlength = width * height;

    memset(command, 0x00, sizeof(command));

    // Signature
    *((int*)command) = *((int*)EGIS_TAG);  // move "EGIS" 4 bytes

    // Operation code
    command[4] = 0x72;  // ET510 Read VDM

    // length
    command[5] = (transferlength & 0xFF00) >> 8;
    command[6] = (transferlength & 0xFF);

    if (write_com_port(m_hCom, command, (int)7) != RESULT_SPI_OK) return FALSE;
    // Read Response
    if (read_com_port(m_hCom, pData, (int)transferlength) != RESULT_SPI_OK) return FALSE;

    return TRUE;
}

BOOL winsensor_WriteVDMCmd(unsigned char* pData, int width, int height) {
    unsigned char command[512];
    unsigned char buf[512];
    int transferlength;

    transferlength = width * height;

    memset(command, 0x00, sizeof(command));
    memset(buf, 0x00, sizeof(buf));

    // Signature
    *((int*)command) = *((int*)EGIS_TAG);  // move "EGIS" 4 bytes

    // Operation code
    command[4] = 0x73;  // ET510 write VDM

    // length
    command[5] = (transferlength & 0xFF00) >> 8;
    command[6] = (transferlength & 0xFF);

    if (write_com_port(m_hCom, command, (int)7) != RESULT_SPI_OK) return FALSE;

    if (write_com_port(m_hCom, pData, (int)transferlength) != RESULT_SPI_OK) return FALSE;

    if (read_com_port(m_hCom, buf, (int)7) != RESULT_SPI_OK) return FALSE;

    if (*((int*)buf) != *((int*)SIGE_TAG) || buf[6] != 0x01) return FALSE;

    return TRUE;
}

BOOL winsensor_ReadNVMCmd(unsigned char* pData, int width, int height) {
    unsigned char command[512];
    int transferlength;

    transferlength = width * height;

    memset(command, 0x00, sizeof(command));

    // Signature
    *((int*)command) = *((int*)EGIS_TAG);  // move "EGIS" 4 bytes

    // Operation code
    command[4] = 0x67;  // ET510 Read NVM

    // length
    command[5] = 0;
    command[6] = 0;

    if (write_com_port(m_hCom, command, (int)7) != RESULT_SPI_OK) return FALSE;
    // Read Response
    if (read_com_port(m_hCom, pData, (int)transferlength) != RESULT_SPI_OK) return FALSE;

    return TRUE;
}

BOOL GetGPIO(BYTE* gpio)  // reset low
{
    BYTE command[10] = {0}, buffer[8] = {0};

    *((int*)command) = *((int*)EGIS_TAG);  // move "EGIS" 4 bytes
    command[5] = 0;                        // Register Address
    command[4] = 0x03;                     // OP Code : Read GPIO
    // command[6] = 0x00; // Don't care

    if (write_com_port(m_hCom, command, (int)7) != RESULT_SPI_OK) return FALSE;
    if (read_com_port(m_hCom, buffer, (int)7) != RESULT_SPI_OK) return FALSE;
    if (*((int*)buffer) != *((int*)SIGE_TAG) || buffer[6] != 0x01)
        return FALSE;  // compare "SIGE" 4 bytes & buffer[6]
    *gpio = buffer[5];

    return TRUE;
}

BOOL SetGPIO(BYTE gpio) {
    BYTE command[10] = {0}, buffer[8] = {0};

    *((int*)command) = *((int*)EGIS_TAG);  // move "EGIS" 4 bytes
    command[5] = 0x00;                     // Don't care
    command[4] = 0x04;                     // Set GPIO P1
    command[6] = gpio;                     // GPIO

    if (write_com_port(m_hCom, command, (int)7) != RESULT_SPI_OK) return FALSE;
    if (read_com_port(m_hCom, buffer, (int)7) != RESULT_SPI_OK) return FALSE;
    if (*((int*)buffer) != *((int*)SIGE_TAG) || buffer[6] != 0x01)
        return FALSE;  // compare "SIGE" 4 bytes & buffer[6]

    return TRUE;
}

BOOL set_spi_config(int mode, int speed) {
    static BYTE command[7], buffer[7];

    memset(buffer, 0x00, sizeof(buffer));

    *((int*)command) = *((int*)EGIS_TAG);  // move "EGIS" 4 bytes
    command[4] = 0x07;                     // OP Code: Set SPI Speed
    command[5] = mode;                     // SPI_MODE: 0, 1, 2, 3
    command[6] = speed;                    // SPI Speed. 0x01 = 1MB.....

    if (write_com_port(m_hCom, command, (int)7) != RESULT_SPI_OK) return FALSE;
    if (read_com_port(m_hCom, buffer, (int)7) != RESULT_SPI_OK) return FALSE;
    if (*((int*)buffer) != *((int*)SIGE_TAG) || buffer[6] != 0x01)
        return FALSE;  // compare "SIGE" 4 bytes & buffer[6]

    *((int*)command) = *((int*)EGIS_TAG);  // move "EGIS" 4 bytes
    command[4] = 0x09;                     // OP Code: Set SPI Speed
    command[5] = 0x00;                     // SPI_MODE: 0, 1, 2, 3
    command[6] = 0x00;                     // SPI Speed. 0x01 = 1MB.....
    if (write_com_port(m_hCom, command, (int)7) != RESULT_SPI_OK) return FALSE;
    if (read_com_port(m_hCom, buffer, (int)7) != RESULT_SPI_OK) return FALSE;
    if (*((int*)buffer) != *((int*)SIGE_TAG) || buffer[6] != 0x01)
        return FALSE;  // compare "SIGE" 4 bytes & buffer[6]

    return TRUE;
}

static BOOL winsensor_WritePA8(unsigned char value) {
    BOOL r_value;
    unsigned char GPIO;

    r_value = GetGPIO(&GPIO);
    if (!r_value) {
        return FALSE;
    }

    if (value) {
        GPIO |= (0x01 << 1);
    } else {
        GPIO &= ~(0x01 << 1);
    }

    r_value = SetGPIO(GPIO);
    if (!r_value) {
        return FALSE;
    }

    return TRUE;
}

BOOL winsensor_Reset() {
    BOOL r_value;

    r_value = winsensor_WritePA8(0x00);
    if (!r_value) {
        return FALSE;
    }

    Sleep(20);

    r_value = winsensor_WritePA8(0x01);
    if (!r_value) {
        return FALSE;
    }

    Sleep(20);

    return TRUE;
}
