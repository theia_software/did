#ifndef __COMPORT_H__
#define __COMPORT_H__

#include <Windows.h>

#define MAX_COM_PORTS 100
#define COM_PORT_NAME TEXT("Arduino Due")

typedef struct tagSERIALPORTINFO {
    int nPortNumber;
    LPTSTR pszPortName;
    LPTSTR pszFriendlyName;
    struct tagSERIALPORTINFO* next;
} SERIALPORTINFO, *LPSERIALPORTINFO;

HANDLE hOpenCompPort(int index);
void vCloseComPort(HANDLE hCom);

BOOL bWriteComPort(HANDLE hCom, unsigned char* pWrite_buf, int iWrite_buf_len);
BOOL bReadComPort(HANDLE hCom, unsigned char* pRead_buf, int iRead_buf_len, int iTimeout);

int iFindArduinoCDC(int iComPorts[MAX_COM_PORTS]);

BOOL GetSerialPortInfo(LPSERIALPORTINFO* ppInfo);
HANDLE SetupEnumeratePorts();
BOOL EndEnumeratePorts(HANDLE hDevices);

#endif