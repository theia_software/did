
#ifndef __SENSOR_H__
#define __SENSOR_H__

#include <windows.h>

#define REG_TYPE_NONE 0x00
#define REG_TYPE_READ 0x01
#define REG_TYPE_WRITE 0x02

#define RESULT_SPI_OK 0
#define RESULT_SPI_DISCONNECT 101
#define RESULT_SPI_COMMAND_FAIL 102
#define RESULT_SPI_NO_DEVICE 103

BOOL winsensor_OpenSensor(int iCom);
BOOL winsensor_CloseSensor();
BOOL winsensor_Reset();
BOOL winsensor_ReadRegister(unsigned char addr, unsigned char* pValue);
BOOL winsensor_WriteRegister(unsigned char addr, unsigned char value);
BOOL winsensor_GetImage(unsigned char* pData, int width, int height, int frame_count);
BOOL winsensor_GetImage_16Bits(unsigned short* pData, int width, int height, int frame_count);
BOOL winsensor_ResetRegister();
BOOL winsensor_ReadPA0(unsigned char* pStatus);
BOOL winsensor_BurstReadRegister(unsigned char start_addr, int len, unsigned char* pValue);
BOOL winsensor_ReverseBurstReadRegister(unsigned char start_addr, int len, unsigned char* pValue);
BOOL winsensor_BurstWriteRegister(unsigned char start_addr, int len, unsigned char* pValue);
BOOL winsensor_ReverseBurstWriteRegister(unsigned char start_addr, int len, unsigned char* pValue);
BOOL winsensor_ReadNVMCmd(unsigned char* pData, int width, int height);
BOOL winsensor_ReadVDMCmd(unsigned char* pData, int width, int height);
BOOL winsensor_WriteVDMCmd(unsigned char* pData, int width, int height);
BOOL GetGPIO(BYTE* gpio);
BOOL SetGPIO(BYTE gpio);
BOOL set_spi_config(int mode, int speed);

#endif