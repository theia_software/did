#ifndef __MESSAGE_HANDLER_H__
#define __MESSAGE_HANDLER_H__

#define OPERATION_TYPE 0
#define OPERATION_EDT 1

#ifdef __cplusplus
extern "C" {
#endif

int message_handler(unsigned char* buffer, int buffersize, unsigned char* outdata,
                    int* outdata_len);

#if defined(__cplusplus)
}
#endif

#endif  // MESSAGE_HANDLER_H__
