#include <SetupAPI.h>
#include <stdio.h>
#include <stdlib.h>
#include <windows.h>

#include "Comport.h"
BOOL GetSerialPortInfo(LPSERIALPORTINFO* ppInfo) {
    BOOL br = TRUE;
    LPSERIALPORTINFO pHead = 0, pTail = 0;
    DWORD nDevice = 0;
    DWORD cbData = 0;
    SP_DEVINFO_DATA deviceData;
    HANDLE hDevices;
    TCHAR sz[16] = {0};

    hDevices = SetupEnumeratePorts();

    for (nDevice = 0; br; nDevice++) {
        deviceData.cbSize = sizeof(SP_DEVINFO_DATA);
        br = SetupDiEnumDeviceInfo(hDevices, nDevice, &deviceData);
        if (br) {
            HKEY hkey = SetupDiOpenDevRegKey(hDevices, &deviceData, DICS_FLAG_GLOBAL, 0, DIREG_DEV,
                                             KEY_READ);
            if (hkey) {
                DWORD cbSize = 16 * sizeof(TCHAR);
                RegQueryValueEx(hkey, ("PortName"), NULL, NULL, (LPBYTE)sz, &cbSize);
                RegCloseKey(hkey);
            }
            CharUpper(sz);
            if (sz[0] == ('C') && sz[1] == ('O') && sz[2] == ('M')) {
                LPSERIALPORTINFO pInfo =
                    (LPSERIALPORTINFO)HeapAlloc(GetProcessHeap(), 0, sizeof(SERIALPORTINFO));
                pInfo->next = 0;
                pInfo->pszPortName =
                    (LPTSTR)HeapAlloc(GetProcessHeap(), 0, sizeof(TCHAR) * (lstrlen(sz) + 1));
                lstrcpy(pInfo->pszPortName, sz);
                pInfo->nPortNumber = atoi(&sz[3]);  //_ttoi(&sz[3]);

                SetupDiGetDeviceRegistryProperty(hDevices, &deviceData, SPDRP_FRIENDLYNAME, NULL,
                                                 NULL, 0, &cbData);
                if (cbData) {
                    pInfo->pszFriendlyName =
                        (LPTSTR)HeapAlloc(GetProcessHeap(), 0, cbData + sizeof(TCHAR));
                    memset(pInfo->pszFriendlyName, 0x00, cbData + sizeof(TCHAR));
                    br = SetupDiGetDeviceRegistryProperty(hDevices, &deviceData, SPDRP_FRIENDLYNAME,
                                                          NULL, (LPBYTE)pInfo->pszFriendlyName,
                                                          cbData, NULL);
                }

                if (pTail == 0) {
                    pHead = pTail = pInfo;
                } else {
                    pTail->next = pInfo;
                    pTail = pInfo;
                }
            }
        }
    }
    EndEnumeratePorts(hDevices);
    *ppInfo = pHead;
    return br;
}

int iFindArduinoCDC(int iComPorts[MAX_COM_PORTS]) {
    int ndevice = 0;
    LPSERIALPORTINFO pPortInfo = 0;
    char buf[MAX_PATH];

    memset(buf, 0, MAX_PATH);

    GetSerialPortInfo(&pPortInfo);

    while (pPortInfo) {
        LPSERIALPORTINFO p = pPortInfo;
        pPortInfo = pPortInfo->next;

        sprintf(buf, ("Port %d '%s'\n"), p->nPortNumber, p->pszFriendlyName);
        OutputDebugString(buf);

        // if(wcsncmp(p->pszFriendlyName, COM_PORT_NAME,
        // wcslen(COM_PORT_NAME))==0)
        if (strncmp(p->pszFriendlyName, COM_PORT_NAME, strlen(COM_PORT_NAME)) == 0) {
            if (ndevice < MAX_COM_PORTS) {
                iComPorts[ndevice] = p->nPortNumber;
                ndevice++;
            }
        }

        HeapFree(GetProcessHeap(), 0, p->pszPortName);
        HeapFree(GetProcessHeap(), 0, p->pszFriendlyName);
        HeapFree(GetProcessHeap(), 0, p);
    }

    return ndevice;
}

HANDLE hOpenCompPort(int index) {
    HANDLE hCom;
    CHAR port_name[MAX_PATH];
    DCB dcb;
    COMMTIMEOUTS TimeOuts;

    sprintf(port_name, "\\\\.\\COM%d", index);

    hCom = CreateFile(port_name, GENERIC_READ | GENERIC_WRITE, 0, NULL, OPEN_EXISTING, 0, NULL);

    if (hCom == INVALID_HANDLE_VALUE) {
        return INVALID_HANDLE_VALUE;
    }

    PurgeComm(hCom, PURGE_TXABORT | PURGE_RXABORT | PURGE_TXCLEAR | PURGE_RXCLEAR);

    // Set COM Port Queue Size
    SetupComm(hCom, 1024, 1024);

    // set read timeout
    TimeOuts.ReadIntervalTimeout = MAXDWORD;
    TimeOuts.ReadTotalTimeoutMultiplier = 0;
    TimeOuts.ReadTotalTimeoutConstant = 0;

    // set write timeout
    TimeOuts.WriteTotalTimeoutMultiplier = 100;
    TimeOuts.WriteTotalTimeoutConstant = 500;
    SetCommTimeouts(hCom, &TimeOuts);

    GetCommState(hCom, &dcb);
    dcb.BaudRate = 9600;
    dcb.ByteSize = 8;
    dcb.Parity = NOPARITY;
    dcb.StopBits = ONESTOPBIT;
    dcb.fDtrControl = 1;

    SetCommState(hCom, &dcb);

    return hCom;
}

HANDLE SetupEnumeratePorts() {
    HDEVINFO hDevices = INVALID_HANDLE_VALUE;
    DWORD dwGuids = 0;
    BOOL br = SetupDiClassGuidsFromName(("Ports"), 0, 0, &dwGuids);
    if (dwGuids) {
        LPGUID pguids = (LPGUID)HeapAlloc(GetProcessHeap(), 0, sizeof(GUID) * dwGuids);
        if (pguids) {
            br = SetupDiClassGuidsFromName(("Ports"), pguids, dwGuids, &dwGuids);
            if (br) {
                hDevices = SetupDiGetClassDevs(pguids, NULL, NULL, DIGCF_PRESENT);
            }
            HeapFree(GetProcessHeap(), 0, pguids);
        }
    }
    return hDevices;
}

BOOL EndEnumeratePorts(HANDLE hDevices) {
    if (SetupDiDestroyDeviceInfoList(hDevices)) {
        return TRUE;
    }
    return FALSE;
}

BOOL bWriteComPort(HANDLE hCom, unsigned char* pWrite_buf, int iWrite_buf_len) {
    COMSTAT ComStat;
    DWORD dwErrorFlags;
    DWORD dwDataWritten;
    BOOL bWriteStat;

    if (hCom == INVALID_HANDLE_VALUE) {
        return FALSE;
    }

    ClearCommError(hCom, &dwErrorFlags, &ComStat);

    bWriteStat = WriteFile(hCom, pWrite_buf, iWrite_buf_len, &dwDataWritten, NULL);
    if (!bWriteStat) {
        return FALSE;
    }

    return TRUE;
}

BOOL bReadComPort(HANDLE hCom, unsigned char* pRead_buf, int iRead_buf_len, int iTimeout) {
    DWORD st_begin;
    DWORD st_end;
    BOOL bTimeOut = FALSE;

    BYTE read_data[65536];
    DWORD read_count;

    BYTE DataQueue[65536];
    int DataQueueLen;

    BOOL r_value;
    int mille_time_passed;

    st_begin = GetTickCount();

    if (hCom == INVALID_HANDLE_VALUE) {
        return FALSE;
    }

    memset(DataQueue, 0x00, sizeof(DataQueue));
    DataQueueLen = 0;

    while (!bTimeOut) {
        memset(read_data, 0x00, sizeof(read_data));
        read_count = 65536;

        r_value = ReadFile(hCom, read_data, read_count, &read_count, NULL);
        if ((r_value) && (read_count > 0)) {
            memcpy(DataQueue + DataQueueLen, read_data, read_count);
            DataQueueLen += read_count;

            // Reset Timer
            st_begin = GetTickCount();

            if (DataQueueLen >= iRead_buf_len) {
                break;
            }
        }

        st_end = GetTickCount();

        mille_time_passed = st_end - st_begin;
        ;

        if (mille_time_passed > iTimeout) {
            bTimeOut = TRUE;
        }
    }

    PurgeComm(hCom, PURGE_TXABORT | PURGE_RXABORT | PURGE_TXCLEAR | PURGE_RXCLEAR);

    if (bTimeOut) {
        if (DataQueueLen) {
            OutputDebugString(TEXT("????\n"));
        }
        return FALSE;
    }

    memcpy(pRead_buf, DataQueue, iRead_buf_len);

    return TRUE;
}
