#include <windows.h>
#include "plat_log.h"
#include "plat_thread.h"
int plat_thread_create_ex(thread_handle_t* handle, void* routine, void* arg) {
    if (handle == NULL || routine == NULL) {
        ex_log(LOG_ERROR, "plat_thread_create invalid param");
        return THREAD_ERR_INVALID_PARAM;
    }

    if (handle->hwin != NULL) {
        ex_log(LOG_ERROR, "plat_thread_create handle->hwin != 0");
        return THREAD_RES_OK;
    }

    handle->hwin = CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)routine, arg, 0, NULL);
    if (handle->hwin == NULL) {
        ex_log(LOG_ERROR, "CreateThread failed ,error = %d", GetLastError());
        return THREAD_ERR_CREATE_FAILED;
    }
    return THREAD_RES_OK;
}

int plat_thread_create(thread_handle_t* handle, void* routine) {
    return plat_thread_create_ex(handle, routine, NULL);
}

int plat_thread_release(thread_handle_t* handle) {
    if (handle == NULL) {
        ex_log(LOG_ERROR, "plat_thread_release handle == NULL");
        return THREAD_ERR_INVALID_PARAM;
    }

    if (handle->hwin == NULL) {
        ex_log(LOG_ERROR, "plat_thread_release handle->hwin == NULL, thread has been closed");
        return THREAD_RES_OK;
    }

    WaitForSingleObject(handle->hwin, INFINITE);
    CloseHandle(handle->hwin);
    handle->hwin = NULL;
    return THREAD_RES_OK;
}

int plat_semaphore_create(semaphore_handle_t* handle, unsigned int initial_cnt,
                          unsigned int max_cnt) {
    if (handle == NULL || initial_cnt > max_cnt) {
        ex_log(LOG_ERROR, "plat_semaphore_create THREAD_ERR_INVALID_PARAM");
        return THREAD_ERR_INVALID_PARAM;
    }

    if (handle->sema != NULL) {
        ex_log(LOG_DEBUG,
               "plat_semaphore_create handle->sema != NULL, semaphore has already created");
        return THREAD_RES_OK;
    }

    handle->sema = CreateSemaphore(NULL, initial_cnt, max_cnt, NULL);
    if (handle->sema == NULL) {
        ex_log(LOG_ERROR, "CreateThread failed ,error = %d", GetLastError());
        return THREAD_ERR_CREATE_FAILED;
    }
    return THREAD_RES_OK;
}

int plat_semaphore_release(semaphore_handle_t* handle) {
    if (handle == NULL) return THREAD_ERR_INVALID_PARAM;

    if (handle->sema == NULL) {
        ex_log(LOG_ERROR, "plat_semaphore_release *handle == NULL, no semaphore needs release");
        return THREAD_RES_OK;
    }
    CloseHandle(handle->sema);
    handle->sema = NULL;
    return THREAD_RES_OK;
}

int plat_semaphore_wait(semaphore_handle_t handle, int wait_time) {
    int retval;
    if (handle.sema == NULL) {
        ex_log(LOG_ERROR, "one method was called before thread manager init");
        return THREAD_RES_OK;
    }
    retval = WaitForSingleObject(handle.sema, wait_time);
    if (retval == WAIT_OBJECT_0) {
        retval = THREAD_RES_OK;
    } else if (retval == WAIT_TIMEOUT) {
        retval = THREAD_RES_WAIT_TIMEOUT;
    } else {
        retval = THREAD_ERR_FAILED;
    }
    return retval;
}

int plat_semaphore_post(semaphore_handle_t handle) {
    return ReleaseSemaphore(handle.sema, 1, NULL);
}

int plat_mutex_create(mutex_handle_t* handle) {
    handle->mutex = CreateMutex(NULL, FALSE, NULL);
    if (handle->mutex == NULL) {
        ex_log(LOG_ERROR, "CreateMutex failed");
        return THREAD_ERR_FAILED;
    }
    return THREAD_RES_OK;
}

int plat_mutex_release(mutex_handle_t* handle) {
    int success;
    if (handle->mutex == NULL) {
        ex_log(LOG_ERROR, "handle = NULL");
        return THREAD_ERR_FAILED;
    }
    success = CloseHandle(handle->mutex);
    if (!success) {
        DWORD d = GetLastError();
        ex_log(LOG_ERROR, "release error %d", d);
        return THREAD_ERR_FAILED;
    }
    handle->mutex = NULL;

    return THREAD_RES_OK;
}

int plat_mutex_lock(mutex_handle_t handle) {
    int retval;
    if (handle.mutex == NULL) {
        ex_log(LOG_ERROR, "handle = NULL");
        return THREAD_ERR_FAILED;
    }
    retval = WaitForSingleObject(handle.mutex, INFINITE);
    if (retval == WAIT_OBJECT_0) {
        retval = THREAD_RES_OK;
    } else if (retval == WAIT_TIMEOUT) {
        retval = THREAD_RES_WAIT_TIMEOUT;
    } else {
        retval = THREAD_ERR_FAILED;
    }
    return retval;
}

int plat_mutex_trylock(mutex_handle_t handle) {
    int retval;
    if (handle.mutex == NULL) {
        ex_log(LOG_ERROR, "handle = NULL");
        return THREAD_ERR_FAILED;
    }
    retval = WaitForSingleObject(handle.mutex, 10);
    if (retval == WAIT_OBJECT_0) {
        retval = THREAD_RES_OK;
    } else if (retval == WAIT_TIMEOUT) {
        retval = THREAD_RES_WAIT_TIMEOUT;
    } else {
        retval = THREAD_ERR_FAILED;
    }
    return retval;
}
int plat_mutex_unlock(mutex_handle_t handle) {
    BOOL success;
    success = ReleaseMutex(handle.mutex);
    if (!success) {
        DWORD d = GetLastError();
        ex_log(LOG_ERROR, "ReleaseMutex error %d", d);
        return THREAD_ERR_FAILED;
    }

    return THREAD_RES_OK;
}
