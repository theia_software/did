#include "Comport.h"
#include "plat_log.h"
#include "util/Sensor_win.h"
#include "windows.h"

#define MAX_DEVICE_NUM 8
int g_device_handle = 0;

typedef struct _ARDUINO_INSTANCE {
    int ComPortNum;
    unsigned char bIsMaster;
    unsigned char bIsFound;
    unsigned char TestStatus;
    unsigned char unique_id[17];
} ARDUINO_INSTANCE, *pARDUINO_INSTANCE;

enum {
    STATUS_NONE = 0,
    STATUS_START = 1,
    STATUS_REG = 2,
    STATUS_DIRTY_DOTS = 3,
    STATUS_CALIBRATION = 4,
    STATUS_WAIT_FINGER = 5,
    STATUS_STIM_SENSED = 6,
    STATUS_FINGER_ON = 7,
    STATUS_END = 8
};

ARDUINO_INSTANCE ArduinoInstances[MAX_DEVICE_NUM];

int io_dispatch_connect(HANDLE* device_handle) {
    int i, nArduino = 0;
    int ArduinoComportUnsort[MAX_DEVICE_NUM];

    BOOL r_value;
    double QStd = 0.0;

    for (i = 0; i < MAX_DEVICE_NUM; i++) {
        ArduinoInstances[i].bIsFound = 0x00;
        ArduinoInstances[i].bIsMaster = 0x00;
        ArduinoInstances[i].ComPortNum = 0x00;
        ArduinoInstances[i].TestStatus = STATUS_NONE;
    }

    memset(ArduinoComportUnsort, 0x00, sizeof(ArduinoComportUnsort));

    nArduino = iFindArduinoCDC(ArduinoComportUnsort);
    if (nArduino > MAX_DEVICE_NUM) {
        nArduino = MAX_DEVICE_NUM;
    }

    for (i = 0; i < nArduino; i++) {
        r_value = winsensor_OpenSensor(ArduinoComportUnsort[i]);

        if (!r_value) {
            winsensor_CloseSensor();
            return RESULT_SPI_NO_DEVICE;
        }
    }

    g_device_handle = 1;
    return RESULT_SPI_OK;
}

int io_dispatch_disconnect(void) {
    if (winsensor_CloseSensor()) {
        return RESULT_SPI_OK;
    }

    return RESULT_SPI_COMMAND_FAIL;
}
int io_5xx_dispatch_read_register(BYTE address, BYTE* value) {
    if (winsensor_BurstReadRegister(address, 1, value)) {
        return RESULT_SPI_OK;
    }

    return RESULT_SPI_COMMAND_FAIL;
}

BYTE ET_5xx_dispatch_read_register(BYTE address, unsigned char* func_name) {
    BYTE value;
    if (winsensor_BurstReadRegister(address, 1, &value)) {
        return value;
    }

    return 0;
}

int io_5xx_dispatch_write_register(BYTE address, BYTE value) {
    if (winsensor_BurstWriteRegister(address, 1, &value)) {
        return RESULT_SPI_OK;
    }

    return RESULT_SPI_COMMAND_FAIL;
}
int ET_5xx_dispatch_write_register(BYTE address, BYTE value) {
    if (winsensor_BurstWriteRegister(address, 1, &value)) {
        return RESULT_SPI_OK;
    }

    return RESULT_SPI_COMMAND_FAIL;
}
int io_5xx_dispatch_get_frame(BYTE* frame, UINT height, UINT width, UINT multi,
                              UINT number_of_frames) {
    if (winsensor_GetImage(frame, height, width, number_of_frames)) {
        return RESULT_SPI_OK;
    }

    return RESULT_SPI_COMMAND_FAIL;
}

int io_5xx_dispatch_get_frame_16bits(unsigned short* frame, UINT width, UINT height, UINT multi,
                                     UINT number_of_frames) {
    if (winsensor_GetImage_16Bits(frame, width, height, number_of_frames)) {
        return RESULT_SPI_OK;
    } else {
        memset(frame, 0, width * height);
        return RESULT_SPI_COMMAND_FAIL;
    }
}

int spi_reserve_read_register(unsigned char start_addr, int len, unsigned char* pValue) {
    if (winsensor_ReverseBurstReadRegister(start_addr, len, pValue)) {
        return RESULT_SPI_OK;
    }

    return RESULT_SPI_COMMAND_FAIL;
}
int io_5xx_dispatch_write_reverse_register(BYTE start_addr, BYTE len, BYTE* pvalue) {
    if (winsensor_ReverseBurstWriteRegister(start_addr, len, pvalue)) {
        return RESULT_SPI_OK;
    }

    return RESULT_SPI_COMMAND_FAIL;
}

int io_5xx_dispatch_write_burst_register(unsigned char start_addr, int len, unsigned char* pValue) {
    if (winsensor_BurstWriteRegister(start_addr, len, pValue)) {
        return RESULT_SPI_OK;
    }

    return RESULT_SPI_COMMAND_FAIL;
}

int io_5xx_dispatch_read_burst_register(unsigned char start_addr, int len, unsigned char* pValue) {
    if (winsensor_BurstReadRegister(start_addr, len, pValue)) {
        return RESULT_SPI_OK;
    }

    return RESULT_SPI_COMMAND_FAIL;
}

int io_5xx_read_vdm(unsigned char* data, int width, int height) {
    if (winsensor_ReadVDMCmd(data, width, height)) {
        return RESULT_SPI_OK;
    }

    return RESULT_SPI_COMMAND_FAIL;
}
int io_5xx_read_nvm(unsigned char* data, int width, int height) {
    // if (winsensor_ReadNVMCmd(data, width, height)) {
    //	return RESULT_SPI_OK;
    //}

    return RESULT_SPI_COMMAND_FAIL;
}
int io_5xx_write_vdm(unsigned char* data, int width, int height) {
    if (winsensor_WriteVDMCmd(data, width, height) == TRUE) {
        return RESULT_SPI_OK;
    }

    return RESULT_SPI_COMMAND_FAIL;
}

int io_6xx_set_clb(BYTE* buffer, UINT length) {
    return RESULT_SPI_COMMAND_FAIL;
}

int io_6xx_get_clb(BYTE* buffer, UINT length) {
    return RESULT_SPI_COMMAND_FAIL;
}

int polling_registry(BYTE addr, BYTE expect, BYTE mask) {
    int i;
    BYTE value;
    for (i = 0; i < 3000; i++) {
        if (io_5xx_dispatch_read_burst_register(addr, 1, &value) != RESULT_SPI_OK)
            return RESULT_SPI_COMMAND_FAIL;
        if ((value & mask) == expect) return RESULT_SPI_OK;
    }

    return RESULT_SPI_COMMAND_FAIL;
}

int io_dispatch_reset() {
    if (winsensor_Reset()) {
        return RESULT_SPI_OK;
    }

    return RESULT_SPI_COMMAND_FAIL;
}
