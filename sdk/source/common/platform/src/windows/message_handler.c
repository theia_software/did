#include <stdio.h>
#include <string.h>

#include "command_handler.h"
#include "common_definition.h"
#include "egis_log.h"
#include "inline_handler.h"
#include "message_handler.h"

#define LOG_TAG "RBS"
#define RETURN_OK 0
#define RETURN_FAIL -1
#define RETURN_CHECK_FAIL -2

int message_handler(unsigned char* msg_data, int msg_size, unsigned char* out_data,
                    int* out_data_len) {
    if (msg_data == NULL || msg_size < sizeof(int) * 5 /*sizeof(transfer_package_t)*/
        || msg_size % BASE_TRANSFER_SIZE != 0)
        return RETURN_FAIL;

    int ret = RETURN_CHECK_FAIL, pid;

    memcpy(&pid, msg_data, sizeof(int));
    egislog_d("pid: %d", pid);

    if (pid == PID_COMMAND) {
        ret = command_handler(msg_data, msg_size, out_data, out_data_len);
    } else if (pid == PID_INLINETOOL) {
#ifndef __ET0XX__
        ret = inline_handler(msg_data, msg_size, out_data, out_data_len);
#endif
    }

    return ret;
}
