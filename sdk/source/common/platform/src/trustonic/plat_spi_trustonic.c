
#include <errno.h>
#include <stddef.h>
#include <stdint.h>
#include <time.h>
typedef unsigned int UINT32;
#include "drtlspi_api.h"
#include "tlStd.h"

#include "egis_definition.h"
#include "plat_heap.h"
#include "plat_log.h"
#include "plat_spi.h"

#include "type_definition.h"

#define GET_FRAME_SHIFT 2

#define LOG_TAG "RBS-SENSORCONTROL"

HANDLE g_device_handle = 0;
int g_opcode = -1;
BOOL g_is_device_open = FALSE;

struct mt_chip_conf spi_chip_config = {.setuptime = 20,
                                       .holdtime = 20,

                                       .high_time = 5,
                                       .low_time = 5,

                                       .cs_idletime = 2,
                                       .ulthgh_thrsh = 0,

                                       .cpol = 0,
                                       .cpha = 0,

                                       .rx_mlsb = 1,
                                       .tx_mlsb = 1,
                                       .tx_endian = 0,
                                       .rx_endian = 0,
                                       .com_mod = FIFO_TRANSFER,
                                       //    .com_mod = DMA_TRANSFER,
                                       .pause = 0,
                                       .finish_intr = 1,
                                       .deassert = 0,
                                       .ulthigh = 0,
                                       .tckdly = 0};
// extern struct arm_spi_ops *_spi;	//original
// struct arm_spi_ops *_spi;	//independent test
extern uint8_t g_debugflag;
#define DEBUGFLAG_EN 1

#define DEVICE_PATH_NAME "/dev/esfp0"
#define SPEED_HZ 14000000
#define DELAY_USECS 0
#define BITS_PER_WORD 8
#define CS_CHANGE 1

#define FP_ADDRESS_OPCODE 0xAC
#define FP_WRITE_OPCODE 0xAE
#define FP_READ_OPCODE 0xAF

typedef enum { STATE_NONE = 0, STATE_READ = 1, STATE_WRITE = 2, STATE_IMAGE = 3 } SPI_STATE;

extern int g_device_handle;
extern int g_opcode;
SPI_STATE g_spi_state = STATE_NONE;
unsigned char g_addr = 0xFF;
unsigned char* g_write_buf = NULL;
unsigned char* g_read_buf = NULL;
int g_write_len = -1;
int g_read_len = -1;

int egis_spi_write(uint8_t* buff, size_t num_bytes);
int egis_spi_writeread(uint8_t* tx, size_t tx_bytes, uint8_t* rx, size_t rx_bytes);
int sec_tzspi_open(void);
int sec_tzspi_close(void);

int egis_spi_write(uint8_t* buff, size_t num_bytes) {
    uint32_t tlRet;

    if (num_bytes >= 32)
        spi_chip_config.com_mod = DMA_TRANSFER;
    else
        spi_chip_config.com_mod = FIFO_TRANSFER;

    tlRet = drSpiSend(buff, buff, num_bytes, &spi_chip_config, 1);

    return tlRet;
}

//
// SPI Write & Read
//

int egis_spi_writeread(uint8_t* tx, size_t tx_bytes, uint8_t* rx, size_t rx_bytes) {
    uint32_t tlRet;
    uint32_t total_bytes = tx_bytes + rx_bytes;

    if (total_bytes >= 32)
        spi_chip_config.com_mod = DMA_TRANSFER;
    else
        spi_chip_config.com_mod = FIFO_TRANSFER;

    tlRet = drSpiSend(tx, rx, total_bytes, &spi_chip_config, 1);

    return tlRet;
}

int sec_tzspi_open() {
    /*
        if (_spi == NULL)
        {
    //		printf("=== egis _spi pointer is NULL  === \n");
            g_device_handle = 0;
            return EGIS_NO_DEVICE;
        }
    */
    // egis_spi_config( &spi_chip_config);
    g_device_handle = 1;

    return EGIS_OK;
}

int sec_tzspi_close(void) {
    if (g_device_handle != 0) {
        // int ret = close(g_device_handle);
        g_device_handle = 0;
        // return ret == 0 ? EGIS_OK : -EGIS_COMMAND_FAIL;
        return EGIS_OK;
    }
    return EGIS_OK;
}

int sec_tzspi_write(sec_tzspi_transfer_t* tr) {
    uint32_t ret;
    // uint8_t buff[2] = {0};
    // if(g_opcode != FP_REGISTER_WRITE) goto fail;
    // if(g_addr == 0xFF) goto fail;
    if (tr->buf_len != 2) goto fail;
    if (tr->buf_addr == NULL) goto fail;
    // if(((BYTE*)(tr->buf_addr))[0] != FP_WRITE_OPCODE) goto fail;

    egis_spi_write(tr->buf_addr, tr->buf_len);

    ret = EGIS_OK;
    tr->total_len = 2;
    goto exit;

fail:
    ret = EGIS_COMMAND_FAIL;
    if (tr) tr->total_len = -1;

exit:
    // show();
    // reset();
    return ret;
}

int sec_tzspi_read(sec_tzspi_transfer_t* tr) {
    return EGIS_COMMAND_FAIL;
}
int sec_tzspi_full_duplex(sec_tzspi_transfer_t* tr_write, sec_tzspi_transfer_t* tr_read) {
    int ret;  //, shift_size, i;
    // uint8_t tempdata;
    uint8_t datatemp[32] = {0};
    /*
    if((g_spi_state != STATE_WRITE)
            || tr_write == NULL || tr_read == NULL) goto fail;
    */
    if (g_opcode == FP_REGISTER_READ) {
        egis_spi_writeread(tr_write->buf_addr, 2, tr_read->buf_addr, 2);
        ret = EGIS_OK;
        tr_write->total_len = 2;
        tr_read->total_len = 2;

        goto exit;
    } else if (g_opcode == FP_GET_ONE_IMG) {
        if (tr_write->buf_len < 2) goto fail;
        if (tr_write->buf_addr == NULL) goto fail;
        if (tr_read->buf_len < 2) goto fail;
        if (tr_read->buf_addr == NULL) goto fail;

        datatemp[0] = 0xAF;
        datatemp[1] = 0xAF;
        datatemp[2] = 0xAF;
        datatemp[3] = 0xAF;

        ret = egis_spi_writeread(datatemp, 1, tr_read->buf_addr, tr_read->buf_len);

        ret = EGIS_OK;

        tr_read->total_len = tr_read->buf_len;

        goto exit;
    } else {
        ret = drSpiSend(tr_write->buf_addr, tr_read->buf_addr, (tr_read->total_len + 2),
                        &spi_chip_config, 1);
        if (ret == 0) goto exit;
    }

fail:

    //	printf("=== egis sec_command_duplex fail ===");
    ret = EGIS_COMMAND_FAIL;
    if (tr_write) tr_write->total_len = -1;
    if (tr_read) tr_read->total_len = -1;

exit:
    return ret;
}

#if 0
__inline int io_dispatch_call_driver(
	UINT ioctl_code,
	void *in_buffer,
	UINT in_buffer_size,
	void *out_buffer,
	UINT out_buffer_size,
	UINT *bytes_returned
	)
{
	static struct egis_ioc_transfer command = { NULL, NULL, 0, SPEED_HZ, DELAY_USECS, BITS_PER_WORD, CS_CHANGE };
	command.tx_buf = (__u8*)in_buffer;
	command.rx_buf = (__u8*)out_buffer;
	command.len = (UINT)in_buffer_size;
	command.opcode = ioctl_code;
	if (ioctl(g_device_handle, SPI_IOC_MESSAGE(1), &command) == -1) return EGIS_COMMAND_FAIL;
	return EGIS_OK;
}


__inline int io_dispatch_call_driver_ex(
	UINT ioctl_code,
	void *in_buffer,
	UINT in_buffer_size,
	void *out_buffer,
	UINT out_buffer_size,
	UINT *bytes_returned,
	void *pad
	)
{
	static struct egis_ioc_transfer command = { NULL, NULL, 0, SPEED_HZ, DELAY_USECS, BITS_PER_WORD, CS_CHANGE };
	command.tx_buf = (__u8*)in_buffer;
	command.rx_buf = (__u8*)out_buffer;
	command.len = (UINT)in_buffer_size;
	command.opcode = ioctl_code;
	command.pad[0] = ((__u8*)pad)[0];
	command.pad[1] = ((__u8*)pad)[1];
	command.pad[2] = ((__u8*)pad)[2];
	if (ioctl(g_device_handle, SPI_IOC_MESSAGE(1), &command) == -1) return EGIS_COMMAND_FAIL;
	return EGIS_OK;
}
#endif

int io_dispatch_spi_test() {
    int ret = 0;
    return ret;
}

int io_dispatch_connect(HANDLE* device_handle) {
    egislog_d("%s start g_is_device_open=%d", __func__, g_is_device_open);
    int ret;
    if (g_is_device_open) return EGIS_OK;
    ret = sec_tzspi_open();
    if (ret) {
        sec_tzspi_close();
        ret = sec_tzspi_open();
        if (ret) {
            egislog_e("%s start fail error = %d", __func__, ret);
            if (device_handle) *device_handle = 0;
            return EGIS_NO_DEVICE;
        }
    }

    if (device_handle) *device_handle = g_device_handle;
    g_is_device_open = TRUE;
    egislog_d("%s g_is_device_open=%d", __func__, g_is_device_open);
    return EGIS_OK;
}

int io_dispatch_disconnect(void) {
    egislog_d("%s start g_is_device_open=%d", __func__, g_is_device_open);
    int ret;
    if (!g_is_device_open) return EGIS_OK;
    ret = sec_tzspi_close();
    if (ret) {
        egislog_e("%s start fail error = %d", __func__, ret);
        return EGIS_COMMAND_FAIL;
    }
    g_is_device_open = FALSE;
    egislog_d("%s g_is_device_open=%d", __func__, g_is_device_open);
    return EGIS_OK;
}

int io_dispatch_read_register(BYTE address, BYTE* value) {
    int ret;
    unsigned char write_buf[2];
    unsigned char read_buf[2];
    sec_tzspi_transfer_t tr_write;
    sec_tzspi_transfer_t tr_read;

    if (!value) return EGIS_COMMAND_FAIL;

    g_opcode = FP_REGISTER_READ;

    // Write 0xAC Address
    write_buf[0] = 0xAC;
    write_buf[1] = address;

    tr_write.buf_addr = write_buf;
    tr_write.buf_len = 2;
    tr_write.total_len = 0;

    ret = sec_tzspi_write(&tr_write);
    if (ret != 0) {
        egislog_e("sec_tzspi_write fail ret = %d", ret);
        ret = EGIS_COMMAND_FAIL;
        goto exit;
    }

    // Write 0xAF then read data

    write_buf[0] = 0xAF;
    write_buf[1] = 0;

    read_buf[0] = 0;
    read_buf[1] = 0;

    tr_write.buf_addr = write_buf;
    tr_write.buf_len = 2;
    tr_write.total_len = 0;

    tr_read.buf_addr = read_buf;
    tr_read.buf_len = 2;
    tr_read.total_len = 0;

    ret = sec_tzspi_full_duplex(&tr_write, &tr_read);
    if (ret != 0) {
        egislog_e("sec_tzspi_full_duplex fail ret = %d", ret);
        ret = EGIS_COMMAND_FAIL;
        goto exit;
    }

    value[0] = read_buf[1];

    ret = EGIS_OK;

exit:

    g_opcode = -1;

    return ret;
}

int io_dispatch_write_register(BYTE address, BYTE value) {
    int ret;
    unsigned char write_buf[2];
    sec_tzspi_transfer_t tr_write;

    g_opcode = FP_REGISTER_WRITE;

    // Write 0xAC Address
    write_buf[0] = 0xAC;
    write_buf[1] = address;

    tr_write.buf_addr = write_buf;
    tr_write.buf_len = 2;
    tr_write.total_len = 0;

    ret = sec_tzspi_write(&tr_write);
    if (ret != 0) {
        egislog_e("sec_tzspi_write fail ret = %d", ret);
        ret = EGIS_COMMAND_FAIL;
        goto exit;
    }

    // Write 0xAE value
    write_buf[0] = 0xAE;
    write_buf[1] = value;

    tr_write.buf_addr = write_buf;
    tr_write.buf_len = 2;
    tr_write.total_len = 0;

    ret = sec_tzspi_write(&tr_write);
    if (ret != 0) {
        ret = EGIS_COMMAND_FAIL;
        egislog_e("sec_tzspi_write fail ret = %d", ret);
        goto exit;
    }
    ret = EGIS_OK;

exit:

    g_opcode = -1;

    return ret;
}

int io_dispatch_get_frame(BYTE* frame, UINT height, UINT width, UINT number_of_frames) {
    int ret;
    UINT size = height * width, total_data_len = size * number_of_frames + 1 + GET_FRAME_SHIFT;
    BYTE* write_buf = (BYTE*)plat_alloc(total_data_len);
    BYTE* read_buf = (BYTE*)plat_alloc(total_data_len);
    sec_tzspi_transfer_t tr_write;
    sec_tzspi_transfer_t tr_read;

    if (write_buf == 0) {
        ret = EGIS_COMMAND_FAIL;
        egislog_e("%s allocate memory fail", __func__);
        goto exit;
    }
    if (read_buf == 0) {
        ret = EGIS_COMMAND_FAIL;
        egislog_e("%s allocate memory fail", __func__);
        goto exit;
    }

    io_dispatch_set_tgen(TRUE);

    g_opcode = FP_GET_ONE_IMG;

    // Write 0xAC Address 0
    write_buf[0] = 0xAC;
    write_buf[1] = 0;

    tr_write.buf_addr = write_buf;
    tr_write.buf_len = 2;
    tr_write.total_len = 0;

    ret = sec_tzspi_write(&tr_write);
    if (ret != 0) {
        egislog_e("sec_tzspi_write fail ret = %d", ret);
        ret = EGIS_COMMAND_FAIL;
        goto exit;
    }

    // Write 0xAF then read data
    memset(write_buf, 0, total_data_len);
    memset(read_buf, 0, total_data_len);
    write_buf[0] = 0xAF;

    tr_write.buf_addr = write_buf;
    tr_write.buf_len = total_data_len;
    tr_write.total_len = 0;

    tr_read.buf_addr = read_buf;
    tr_read.buf_len = total_data_len;
    tr_read.total_len = 0;

    ret = sec_tzspi_full_duplex(&tr_write, &tr_read);
    if (ret != 0) {
        egislog_e("sec_tzspi_full_duplex(gf) fail ret = %d", ret);
        ret = EGIS_COMMAND_FAIL;
        goto exit;
    }
    memcpy(frame, read_buf + 1 + GET_FRAME_SHIFT, size * number_of_frames);
    ret = EGIS_OK;

exit:
    g_opcode = -1;

    plat_free(write_buf);
    plat_free(read_buf);
    io_dispatch_set_tgen(FALSE);

    return ret;
}

int io_dispatch_read_eFuse(BYTE* buf, UINT len) {
    egislog_e("not implemented yet");
    return EGIS_COMMAND_FAIL;
}

#define ET5XX_WRITE_OPCODE 0x24
#define ET5XX_READ_OPCODE 0x20
#define ET5XX_BURST_READ_OPCODE 0x22
#define ET5XX_BURST_WRITE_OPCODE 0x26
#define ET5XX_REVERSE_WRITE_OPCODE 0x27
#define ET5XX_GET_IMAGE_OPCODE 0x50

#define ET5_SERIES_WRITE_OP ET5XX_WRITE_OPCODE
#define ET5_SERIES_BURST_WRITE_OP ET5XX_BURST_WRITE_OPCODE
#define ET5_SERIES_REVERSE_WRITE_OP ET5XX_REVERSE_WRITE_OPCODE
#define ET5_SERIES_READ_OP ET5XX_READ_OPCODE
#define ET5_SERIES_BURST_READ_OP ET5XX_BURST_READ_OPCODE
#define ET5_SERIES_GET_IMAGE_OP ET5XX_GET_IMAGE_OPCODE

int io_dispatch_set_tgen(BOOL enable) {
    return 0;
}
#define OPCODE_5XX_SIZE 1

int io_5xx_dispatch_write_burst_register(BYTE start_addr, BYTE len, BYTE* pvalue);
int io_5xx_dispatch_write_register(BYTE address, BYTE value) {
    return io_5xx_dispatch_write_burst_register(address, 1, &value);
}

#define WRITE_REGISTER_COMMAND_SIZE 2
#define BURST_5XX_RD_WR_REG_SIZE 64

/*
 * Read Register workaround.
 */
int io_5xx_dispatch_read_register(BYTE address, BYTE* value) {
    if (!value) return EGIS_COMMAND_FAIL;
    return io_5xx_dispatch_read_burst_register(address, 1, value);
}

extern struct mt_chip_conf spi_chip_config;
int io_5xx_dispatch_get_frame(BYTE* frame, UINT height, UINT width, UINT multi,
                              UINT number_of_frames) {
    sec_tzspi_transfer_t tx;
    sec_tzspi_transfer_t rx;
    int in_len = 1;
    // BYTE in_buffer[1] = {ET5_SERIES_GET_IMAGE_OP};
    int total_size = 1 + height * width * number_of_frames;

    egislog_d("io_5xx_dispatch_get_frame (w %d h %d num %d)", width, height, number_of_frames);

    BYTE in_buffer[1];
    BYTE* out_buffer = (BYTE*)plat_alloc(total_size);
    int ret = 0;

    in_buffer[0] = ET5_SERIES_GET_IMAGE_OP;

    tx.buf_addr = in_buffer;
    tx.buf_len = total_size;

    rx.buf_addr = out_buffer;
    rx.buf_len = total_size;

    spi_chip_config.com_mod = DMA_TRANSFER;
    ret = drSpiSend(in_buffer, out_buffer, total_size, &spi_chip_config, 1);
    spi_chip_config.com_mod = FIFO_TRANSFER;

    if (ret != 0) {
        egislog_e(
            "io_5xx_dispatch_get_frame (w %d h %d num %d) fail ret = "
            "%d",
            width, height, number_of_frames, ret);
        ret = EGIS_COMMAND_FAIL;
        goto exit;
    }
    memcpy(frame, out_buffer + in_len, height * width * number_of_frames);
exit:
    // plat_free(in_buffer);
    plat_free(out_buffer);
    return ret;
}

int polling_registry(BYTE addr, BYTE expect, BYTE mask) {
    int i;
    BYTE value;
    // egislog_d("%s",__func__);
    for (i = 0; i < 3000; i++) {
        if (io_5xx_dispatch_read_register(addr, &value) != EGIS_OK) return EGIS_COMMAND_FAIL;
        if ((value & mask) == expect) return EGIS_OK;
    }
    egislog_e("%s, value&mask = 0x%x, expect = 0x%x", __func__, value & mask, expect);

    return EGIS_COMMAND_FAIL;
}

int io_5xx_read_vdm(BYTE* frame, UINT height, UINT width) {
    sec_tzspi_transfer_t tx;
    sec_tzspi_transfer_t rx;
    int in_len = 1;
    int total_size = in_len + height * width;
    BYTE* in_buffer = (BYTE*)plat_alloc(total_size);
    BYTE* out_buffer = (BYTE*)plat_alloc(total_size);
    int ret = 0;

    *in_buffer = 0x60;
    tx.buf_addr = in_buffer;
    tx.buf_len = total_size;

    rx.buf_addr = out_buffer;
    rx.buf_len = total_size;

    spi_chip_config.com_mod = DMA_TRANSFER;
    ret = drSpiSend(in_buffer, out_buffer, total_size, &spi_chip_config, 1);
    spi_chip_config.com_mod = FIFO_TRANSFER;

    if (ret != 0) {
        egislog_e("io_5xx_read_vdm fail ret = %d", ret);
        ret = EGIS_COMMAND_FAIL;
        goto exit;
    }

    memcpy(frame, out_buffer + in_len, height * width);
exit:
    plat_free(in_buffer);
    plat_free(out_buffer);
    return ret;
}

int io_5xx_write_vdm(BYTE* frame, UINT height, UINT width) {
    sec_tzspi_transfer_t tx;
    sec_tzspi_transfer_t rx;
    int ret = 0;
    int total_size = 1 + height * width;
    BYTE* in_buffer = (BYTE*)plat_alloc(total_size);
    BYTE* out_buffer = (BYTE*)plat_alloc(total_size);

    if (in_buffer == NULL) {
        egislog_e("memory allocation fail");
        return EGIS_OUT_OF_MEMORY;
    }
    memcpy(in_buffer + 1, frame, height * width);

    in_buffer[0] = 0x62;

    tx.buf_addr = in_buffer;
    tx.buf_len = total_size;

    rx.buf_addr = in_buffer;
    rx.buf_len = total_size;

    spi_chip_config.com_mod = DMA_TRANSFER;
    ret = drSpiSend(in_buffer, out_buffer, total_size, &spi_chip_config, 1);
    spi_chip_config.com_mod = FIFO_TRANSFER;

    if (ret != 0) {
        egislog_e("io_5xx_write_vdm fail ret = %d", ret);
        ret = EGIS_COMMAND_FAIL;
    }
    plat_free(in_buffer);
    plat_free(out_buffer);
    return ret;
}

static int _io_5xx_dispatch_read_burst_register(BYTE start_addr, BYTE len, BYTE* pValue) {
    int ret_value;
    int i;
    int in_len = WRITE_REGISTER_COMMAND_SIZE;
    int total_size = in_len + len;
    BYTE in_buffer[256] = {0};
    BYTE* out_buffer = (BYTE*)plat_alloc(total_size);
    in_buffer[0] = ET5_SERIES_BURST_READ_OP;
    in_buffer[1] = start_addr;

    ret_value = drSpiSend(in_buffer, out_buffer, total_size, &spi_chip_config, 1);

    for (i = 0; i < len; i++) {
        pValue[i] = out_buffer[i + 2];
    }
    plat_free(out_buffer);

    if (ret_value != 0) {
        egislog_e("Bad1 ret=%d", ret_value);
    }
    return ret_value;
}
#define SM32_READ_BURST_LIMIT 0x10
int io_5xx_dispatch_read_burst_register(BYTE start_addr, BYTE len, BYTE* pValue) {
    int i, ret = EGIS_OK;
    BYTE to_read, remaining = len;
    BYTE curr_start = start_addr;
    BYTE* output = pValue;
    while (remaining > 0 && ret == EGIS_OK) {
        to_read = remaining > SM32_READ_BURST_LIMIT ? SM32_READ_BURST_LIMIT : remaining;
        // egislog_v("==[0x%X] %d (%d)", curr_start, to_read,
        // remaining);
        ret = _io_5xx_dispatch_read_burst_register(curr_start, to_read, output);

        remaining -= to_read;
        curr_start += to_read;
        output += to_read;
    }
    return ret;
}

int io_5xx_dispatch_write_burst_register(BYTE start_addr, BYTE len, BYTE* pValue) {
    uint8_t tx_buf[64], rx_buf[64];
    int ret_value;
    int i;

    tx_buf[0] = ET5_SERIES_BURST_WRITE_OP;
    tx_buf[1] = start_addr;

    for (i = 0; i < len; i++) {
        tx_buf[2 + i] = pValue[i];
    }
    ret_value = drSpiSend(tx_buf, rx_buf, (len + 2), &spi_chip_config, 1);
    if (ret_value != 0) {
        egislog_e("write burst failed. ret=%d", ret_value);
    }
    return ret_value;
}
int io_5xx_dispatch_write_reverse_register(BYTE start_addr, BYTE len, BYTE* pValue) {
    uint8_t tx_buf[64], rx_buf[64];
    int ret_value;
    int i;

    tx_buf[0] = ET5_SERIES_REVERSE_WRITE_OP;
    tx_buf[1] = start_addr;

    for (i = 0; i < len; i++) {
        tx_buf[2 + i] = pValue[i];
    }
    ret_value = drSpiSend(tx_buf, rx_buf, (len + 2), &spi_chip_config, 1);
    if (ret_value != 0) {
        egislog_e("write reverse ret=%d", ret_value);
    }
    return ret_value;
}

int ET_5xx_dispatch_write_register(BYTE address, BYTE value) {
    return io_5xx_dispatch_write_register(address, value);
}

BYTE ET_5xx_dispatch_read_register(BYTE address, const char* func) {
    BYTE value = 0;
    int ret = io_5xx_dispatch_read_register(address, &value);
    if (ret != EGIS_OK) {
        egislog_e("%s, addr = 0x%x, ret = %d", func, address, ret);
    }
    return value;
}

#define CMD_NV_EN 0x44
#define CMD_NV_RD1 0x40
#define CMD_NV_RD2 0x00
#define CMD_NV_DIS 0x48
#define CMD_DUMMY 0x00
#define NVM_LEN 64
int io_5xx_read_nvm(BYTE* buffer) {
    int result = 0;
    uint8_t nvm_read_enable[2] = {CMD_NV_EN, CMD_DUMMY};
    uint8_t read_chip_op[3] = {CMD_NV_RD1, CMD_NV_RD2, CMD_DUMMY};
    uint8_t nvm_disable[2] = {CMD_NV_DIS, CMD_DUMMY};
    sec_tzspi_transfer_t tx, rx;

    tx.buf_addr = nvm_read_enable;
    tx.buf_len = 2;
    result = egis_spi_write(tx.buf_addr, tx.buf_len);

    tx.buf_addr = read_chip_op;
    tx.buf_len = 3;
    rx.buf_addr = buffer;
    rx.buf_len = NVM_LEN + 3;
    result = egis_spi_writeread(tx.buf_addr, tx.buf_len, rx.buf_addr, rx.buf_len);

    tx.buf_addr = nvm_disable;
    tx.buf_len = 2;
    result = egis_spi_write(tx.buf_addr, tx.buf_len);
    return result;
}
