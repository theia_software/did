// clang-format off
#include "tlStd.h"
#include "TlApi/TlApi.h"
// clang-format on
#include "TlApi/TlApiCrypto.h"
#include "egis_definition.h"
#include "plat_encryption.h"
#include "plat_log.h"

#ifdef TRUSTONIC_AES_KEY
uint8_t g_aes_key[AES_KEY_LENGTH] = {0};
#else
unsigned char g_aes_key[AES_KEY_LENGTH] = {
    0xbd, 0x6f, 0xa6, 0xca, 0x6f, 0xe8, 0x6b, 0xaa, 0x8b, 0xa4, 0x58, 0xf5, 0x67, 0xb4, 0xbd, 0x37,
    0xec, 0xbc, 0xe4, 0x4a, 0x14, 0xed, 0xd5, 0xb9, 0x13, 0x95, 0x53, 0xbe, 0xc,  0x64, 0x17, 0x8e};
#endif

bool g_aes_key_derived = false;

int plat_encryption(unsigned char* buffer_in, int in_len, unsigned char* buffer_out, int* out_len) {
    if (buffer_in == NULL || in_len == 0 || buffer_out == NULL || out_len == NULL)
        return EGIS_INCORRECT_PARAMETER;

    int ret = tlApiWrapObject(buffer_in, 0, in_len, buffer_out, out_len, MC_SO_CONTEXT_TLT,
                              MC_SO_LIFETIME_PERMANENT, NULL, TLAPI_WRAP_DEFAULT);
    if (ret != TLAPI_OK) {
        ex_log(LOG_ERROR, "tlApiWrapObject failed with ret=0x%08X, exit", ret);
    }

    return (ret == TLAPI_OK) ? EGIS_OK : EGIS_COMMAND_FAIL;
}

int plat_decryption(unsigned char* buffer_in, int in_len, unsigned char* buffer_out, int* out_len) {
    if (buffer_in == NULL || in_len == 0 || buffer_out == NULL || out_len == NULL)
        return EGIS_INCORRECT_PARAMETER;

    int ret = tlApiUnwrapObject(buffer_in, in_len, buffer_out, out_len, TLAPI_UNWRAP_DEFAULT);
    if (ret != TLAPI_OK) ex_log(LOG_ERROR, "tlApiUnwrapObject failed with ret=0x%08X, exit", ret);

    return (ret == TLAPI_OK) ? EGIS_OK : EGIS_COMMAND_FAIL;
}

int plat_get_key() {
    if (!g_aes_key_derived) {
        ex_log(LOG_DEBUG, "plat_get_key enter");
        const uint8_t salt[] = {'s', 'i', 'g', 'e', 'm', 'a', 's', 't', 'e', 'r', 0, 0};

        memset(g_aes_key, 0x00, AES_KEY_LENGTH);

        tlApiResult_t ret = tlApiDeriveKey(salt, sizeof(salt), g_aes_key, AES_KEY_LENGTH,
                                           MC_SO_CONTEXT_TLT, /* MC_SPID_SYSTEM */
                                           MC_SO_LIFETIME_PERMANENT);
        if (TLAPI_OK != ret) {
            ex_log(LOG_ERROR, "plat_get_key return %d", ret);
            memset(g_aes_key, 0, AES_KEY_LENGTH);
            return EGIS_COMMAND_FAIL;
        }
        g_aes_key_derived = true;
    }

    ex_log(LOG_DEBUG, "plat_get_key leave");
    return EGIS_OK;
}
