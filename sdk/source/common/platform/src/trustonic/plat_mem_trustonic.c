// clang-format off
#include "tlStd.h"
#include "TlApi/TlApiHeap.h"
// clang-format on

#include "plat_mem.h"

void* mem_alloc(uint32_t size) {
    return tlApiMalloc(size, 0);
}
void* mem_realloc(void* ptr, uint32_t new_size) {
    return tlApiRealloc(ptr, new_size);
}
void mem_free(void* ptr) {
    tlApiFree(ptr);
}
void mem_move(void* dest, void* src, uint32_t size) {
    memcpy(dest, src, size);
}
void mem_set(void* dest, uint32_t data, uint32_t size) {
    memset(dest, data, size);
}
int mem_compare(void* buff1, void* buff2, uint32_t size) {
    return memcmp(buff1, buff2, size);
}

void* sys_alloc(size_t count, size_t size) {
    size *= count;
    return tlApiMalloc(size, 0);
}

void sys_free(void* ptr) {
    tlApiFree(ptr);
}
void* sys_realloc(void* ptr, size_t new_size) {
    return tlApiRealloc(ptr, new_size);
}
