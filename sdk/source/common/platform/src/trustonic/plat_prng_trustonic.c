#include "TlApi/TlApi.h"
#include "egis_definition.h"
#include "plat_log.h"
#include "plat_prng.h"
#include "tlStd.h"

#define LOG_TAG "RBS-AUTHTOKEN"

int plat_prn_generate(void* buf, unsigned int size) {
    egislog_d("plat_prn_generate enter");

    tlApiResult_t ret = tlApiRandomGenerateData(TLAPI_ALG_SECURE_RANDOM, buf, &size);
    egislog_i("tlApiRandomGenerateData ret %d,size =%d", ret, size);
    if (ret != TLAPI_OK) {
        return EGIS_COMMAND_FAIL;
    }

    return 0;
}