// clang-format off
#include "tlStd.h"
#include "TlApi/TlApi.h"
// clang-format on

#include "plat_log.h"

#define MAX_BUFLEN 1024

#ifdef EGIS_DBG
LOG_LEVEL g_log_level = LOG_VERBOSE;
#else
LOG_LEVEL g_log_level = LOG_INFO;
#endif

void set_debug_level(LOG_LEVEL level) {
    g_log_level = level;
    output_log(LOG_ERROR, "RBS", "", "", 0, "set_debug_level %d", level);
}

void output_log(LOG_LEVEL level, const char* tag, const char* file_name, const char* func, int line,
                const char* format, ...) {
    char buffer[MAX_BUFLEN];

    if (format == NULL) return;
    if (g_log_level > level) return;

    va_list vl;
    va_start(vl, format);
    // vsnprintf(buffer,MAX_BUFLEN, format, vl);
    va_end(vl);

    switch (level) {
        case LOG_ERROR:
        default:
            tlApiLogPrintf("Error! [%s], ", tag);
            tlApiLogvPrintf(format, vl);
            va_end(vl);
            tlApiLogPrintf("\n", buffer);
            break;
        case LOG_INFO:
            tlApiLogPrintf("[%s], ", tag);
            tlApiLogvPrintf(format, vl);
            va_end(vl);
            tlApiLogPrintf("\n", buffer);

            break;
        case LOG_DEBUG:
        case LOG_VERBOSE:
            tlApiLogPrintf("[%s], ", tag);
            tlApiLogvPrintf(format, vl);
            va_end(vl);
            tlApiLogPrintf("\n", buffer);
            break;
    }
}
