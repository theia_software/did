// clang-format off
#include "tlStd.h"
#include "TlApi/TlApi.h"
// clang-format on

#include "plat_file.h"
#include "plat_log.h"

#define LOG_TAG "RBS-FILE"

int plat_save_raw_image(char* path, unsigned char* pImage, unsigned int width,
                        unsigned int height) {
    egislog_e("%s not supported !!", __func__);
    return -999;
}

int plat_load_raw_image(char* path, unsigned char* pImage, unsigned int width,
                        unsigned int height) {
    egislog_e("%s not supported !!", __func__);
    return -999;
}

static long get_file_size(char* file) {
    egislog_e("%s not supported !!", __func__);
    return 0;
}

int plat_save_file(char* path, unsigned char* buf, unsigned int len) {
    return plat_save_raw_image(path, buf, len, 1);
}

int plat_load_file(char* path, unsigned char* buf, unsigned int len, unsigned int* real_size) {
    egislog_e("%s not supported !!", __func__);
    return -999;
}

int plat_remove_file(char* path) {
    egislog_e("%s not supported !!", __func__);
    return -999;
}
