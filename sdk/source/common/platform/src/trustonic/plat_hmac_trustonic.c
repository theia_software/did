#include "TlApi/TlApi.h"
#include "egis_definition.h"
#include "plat_hmac.h"
#include "plat_log.h"
#include "tlStd.h"

#define LOG_TAG "RBS-AUTHTOKEN"

#define TRUSLET_AUTH_KEY_LENGTH 32
#define TRUSLET_AUTH_HMAC_LENGTH 32

static uint8_t truslet_hmac_key[TRUSLET_AUTH_KEY_LENGTH] = {0};
static bool hmac_key_derived = false;

int hmac_setup_platform_sec_key(unsigned char* ext_data, unsigned int ext_data_len,
                                unsigned char* out_key, unsigned int* out_key_len) {
    egislog_d("hmac_setup_platform_sec_key enter");

    if (!hmac_key_derived) {
        const uint8_t salt[] = {'k', 'e', 'y', 'm', 'a', 's', 't', 'e', 'r', '1', 0, 0};
        egislog_i("ready to call tlApiDeriveKey");

        tlApiResult_t ret =
            tlApiDeriveKey(salt, sizeof(salt), truslet_hmac_key, TRUSLET_AUTH_KEY_LENGTH,
                           MC_SO_CONTEXT_SP, /* MC_SPID_SYSTEM */
                           MC_SO_LIFETIME_POWERCYCLE);

        if (TLAPI_OK != ret) {
            memset(truslet_hmac_key, 0, TRUSLET_AUTH_KEY_LENGTH);
            egislog_e("tlApiDeriveKey return %d", ret);
            return EGIS_COMMAND_FAIL;
        }

        egislog_i("key is ready");
        hmac_key_derived = true;
    }

    if (out_key && out_key_len) {
        egislog_i("copy key len : %d", TRUSLET_AUTH_KEY_LENGTH);
        memcpy(out_key, truslet_hmac_key, TRUSLET_AUTH_KEY_LENGTH);
        *out_key_len = TRUSLET_AUTH_KEY_LENGTH;
    }

    egislog_d("hmac_setup_platform_sec_key leave");
    return EGIS_OK;
}

int hmac_sha256(unsigned char const* in, const unsigned int in_len, unsigned char* key,
                unsigned int key_len, unsigned char* out_hmac) {
    tlApiCrSession_t session = 0;
    tlApiResult_t res = TLAPI_OK;

    tlApiKey_t api_key = {0};
    tlApiSymKey_t sym_key = {(uint8_t*)key, key_len};
    size_t hmac_size = 32;

    egislog_d("hmac_sha256 enter");

    api_key.symKey = &sym_key;
    res = tlApiSignatureInit(&session, &api_key, TLAPI_MODE_SIGN, TLAPI_ALG_HMAC_SHA_256);
    if (TLAPI_OK != res) {
        egislog_i("tlApiSignatureInit return %d", res);
        return EGIS_COMMAND_FAIL;
    }

    res = tlApiSignatureSign(session, in, in_len, out_hmac, &hmac_size);
    if (TLAPI_OK != res) {
        egislog_i("tlApiSignatureSign return %d", res);
        return EGIS_COMMAND_FAIL;
    }

    egislog_d("hmac_sha256 leave");
    return EGIS_OK;
}