// clang-format off
#include "tlStd.h"
#include "TlApi/TlApi.h"
// clang-format on
#include "plat_log.h"

#define LOG_TAG "egis"

unsigned long long plat_get_time() {
    uint64_t time_stamp = 0;
    tlApiResult_t tl_ret = tlApiGetSecureTimestamp((timestamp_ptr)&time_stamp);
    if (tl_ret == TLAPI_OK) {
        return (time_stamp / 1000);  // us -> ms
    } else {
        return 0;
    }
}

unsigned long plat_get_diff_time(unsigned long long begin) {
    unsigned long long nowTime = plat_get_time();
    // TODO: consider overflow case
    return (unsigned long)(nowTime - begin);
}

void plat_wait_time(unsigned long msecs) {
    unsigned long long times_now, times_start = plat_get_time();
    do {
        times_now = plat_get_time();
    } while (times_now - times_start < msecs);
    return;
}

void plat_sleep_time(unsigned long timeInMs) {
    plat_wait_time(timeInMs);
    return;
}

void get_cur_ta_time_ms(uint64_t* cur_ta_time_ms) {
    *cur_ta_time_ms = plat_get_time();

    return;
}