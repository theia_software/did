#include <stdint.h>
#include <string.h>
#include <tee_api.h>
#include <tee_internal_api_extensions.h>
#include <tee_ta_api.h>

#include "plat_spi.h"
//#include <platform_api.h>

#define LOG_TAG "teei_ta"
#define TEE_SPI_ID 0

// ------------------------------------------------------------------------------------------------
//#include "et310_definition.h"
#include "egis_definition.h"
#include "egis_log.h"
#include "type_definition.h"

//#define FP_ADDRESS_OPCODE 0xAC
//#define FP_WRITE_OPCODE 0xAE
//#define FP_READ_OPCODE 0xAF

#include "VFSys.h"
#include "plat_heap.h"

#define GET_FRAME_SHIFT 3

#define LOG_TAG "RBS-EGISFP"
#define SPI_PACKET_SIZE 0x400
#define BUF_LEN                                                                                 \
    ((SENSOR_WIDTH * SENSOR_HEIGHT + GET_FRAME_SHIFT + SPI_PACKET_SIZE - 1) / SPI_PACKET_SIZE * \
     SPI_PACKET_SIZE)

#if 0
#define DEVICE_PATH_NAME "/dev/esfp0"
#define SPEED_HZ 14000000
#define DELAY_USECS 0
#define BITS_PER_WORD 8
#define CS_CHANGE 1
#endif

typedef enum { STATE_NONE = 0, STATE_READ = 1, STATE_WRITE = 2, STATE_IMAGE = 3 } SPI_STATE;

extern int g_device_handle;
extern int g_opcode;

void egis_spi_config(struct TEE_SPIConfig* chip_config);
int egis_spi_write(unsigned char* buff, size_t num_bytes);
int egis_spi_writeread(unsigned char* tx, size_t tx_bytes, unsigned char* rx, size_t rx_bytes);
int sec_tzspi_open(void);
int sec_tzspi_close(void);

static TEE_SPIConfig spi_chip_config = {.setuptime = 1,
                                        .holdtime = 1,

                                        .high_time = 4,
                                        .low_time = 4,

                                        .sample_sel = TEE_SPI_POSEDGE,
                                        .cs_pol = TEE_SPI_ACTIVE_LOW,

                                        .cs_idletime = 2,
                                        .ulthgh_thrsh = 0,

                                        .cpol = TEE_SPI_CPOL_1,
                                        .cpha = TEE_SPI_CPHA_1,

                                        .rx_mlsb = TEE_SPI_MSB,
                                        .tx_mlsb = TEE_SPI_MSB,

                                        .tx_endian = TEE_SPI_LENDIAN,
                                        .rx_endian = TEE_SPI_LENDIAN,

                                        .pause = TEE_SPI_PAUSE_MODE_DISABLE,
                                        .finish_intr = TEE_SPI_FINISH_INTR_DIS,
                                        .deassert = TEE_SPI_DEASSERT_DISABLE,
                                        .ulthigh = TEE_SPI_ULTRA_HIGH_DISABLE,
                                        .tckdly = TEE_SPI_TICK_DLY0,
                                        .issue_interval = 0};

int egis_spi_write(unsigned char* buff, size_t num_bytes) {
    unsigned int tlRet;

    if (num_bytes >= 32)
        spi_chip_config.mode = TEE_SPI_DMA_TRANSFER;
    else
        spi_chip_config.mode = TEE_SPI_FIFO_TRANSFER;

    TEE_SPISetup2(0, &spi_chip_config);
    tlRet = TEE_SPIWriteRead(0, buff, num_bytes);

    return tlRet;
}

int egis_spi_writeread(unsigned char* tx, size_t tx_bytes, unsigned char* rx, size_t rx_bytes) {
    unsigned int tlRet;
    unsigned int total_bytes = tx_bytes + rx_bytes;

    memcpy(rx, tx, tx_bytes);

    if (total_bytes >= 32)
        spi_chip_config.mode = TEE_SPI_DMA_TRANSFER;
    else
        spi_chip_config.mode = TEE_SPI_FIFO_TRANSFER;

    TEE_SPISetup2(0, &spi_chip_config);

    tlRet = TEE_SPIWriteRead(0, rx, rx_bytes);

    return tlRet;
}

int sec_tzspi_open() {
    TEE_Result ret;

    ret = TEE_SPISetup2(0, &spi_chip_config);
    if (ret != TEE_SUCCESS) {
        egislog_e("=== TEE_SPISetup2 Fail  === \n");
        g_device_handle = 0;
        return EGIS_NO_DEVICE;
    }
    g_device_handle = 1;

    return EGIS_OK;
}

int sec_tzspi_close(void) {
    if (g_device_handle != 0) {
        g_device_handle = 0;
        return EGIS_OK;
    }
    return EGIS_OK;
}

int sec_tzspi_write(sec_tzspi_transfer_t* tr) {
    unsigned int ret;
    if (tr->buf_len != 2) goto fail;
    if (tr->buf_addr == NULL) goto fail;

    egis_spi_write(tr->buf_addr, tr->buf_len);

    ret = EGIS_OK;
    tr->total_len = 2;
    goto exit;

fail:
    ret = EGIS_COMMAND_FAIL;
    if (tr) tr->total_len = -1;

exit:
    return ret;
}

int sec_tzspi_read(sec_tzspi_transfer_t* tr) {
    return EGIS_COMMAND_FAIL;
}
int sec_tzspi_full_duplex(sec_tzspi_transfer_t* tr_write, sec_tzspi_transfer_t* tr_read) {
    int ret;
    unsigned char datatemp[32] = {0};

    if (g_opcode == FP_REGISTER_READ) {
        egis_spi_writeread(tr_write->buf_addr, 2, tr_read->buf_addr, 2);
        ret = EGIS_OK;
        tr_write->total_len = 2;
        tr_read->total_len = 2;

        goto exit;
    } else if (g_opcode == FP_GET_ONE_IMG) {
        if (tr_write->buf_addr == NULL) goto fail;
        if (tr_read->buf_addr == NULL) goto fail;

        datatemp[0] = 0xAF;
        datatemp[1] = 0xAF;
        datatemp[2] = 0xAF;
        datatemp[3] = 0xAF;

        ret = egis_spi_writeread(datatemp, 1, tr_read->buf_addr, tr_read->buf_len);

        ret = EGIS_OK;

        tr_read->total_len = tr_read->buf_len;

        goto exit;
    }

fail:

    egislog_e("=== egis sec_command_duplex fail ===");
    ret = EGIS_COMMAND_FAIL;
    if (tr_write) tr_write->total_len = -1;
    if (tr_read) tr_read->total_len = -1;

exit:
    return ret;
}

HANDLE g_device_handle = 0;
int g_opcode = -1;

int io_dispatch_spi_test() {
    int ret;
    BYTE val, temp_val;
    int i;
    egislog_d("io_dispatch_spi_test()");

    ret = io_dispatch_read_register(V2IC0_ADDR, &temp_val);
    if (ret != EGIS_OK) return ret;

    for (i = 0; i <= 0xFF; i++) {
        ret = io_dispatch_write_register(V2IC0_ADDR, i);
        if (ret != EGIS_OK) {
            ret = EGIS_NO_DEVICE;
            goto end;
        }

        ret = io_dispatch_read_register(V2IC0_ADDR, &val);
        if (ret != EGIS_OK) {
            ret = EGIS_NO_DEVICE;
            goto end;
        }

        if (val != i) {
            egislog_e("register %d, write = 0x%x, read = 0x%x", V2IC0_ADDR, i, val);
            ret = EGIS_NO_DEVICE;
            goto end;
        }
    }

    ret = EGIS_OK;
    ret = io_dispatch_write_register(V2IC0_ADDR, temp_val);
    if (ret != EGIS_OK) return ret;
    ret = io_dispatch_read_register(V2IC0_ADDR, &val);
    if (ret != EGIS_OK) return ret;
end:

    return 0;
}

int io_dispatch_connect(HANDLE* device_handle) {
    int ret;

    ret = sec_tzspi_open();
    if (ret) {
        egislog_e("%s start fail error = %d", __func__, ret);
        if (device_handle) *device_handle = 0;
        return EGIS_NO_DEVICE;
    }

    if (device_handle) *device_handle = g_device_handle;

    return EGIS_OK;
}

int io_dispatch_disconnect(void) {
    int ret;

    ret = sec_tzspi_close();
    if (ret) {
        egislog_e("%s start fail error = %d", __func__, ret);
        return EGIS_COMMAND_FAIL;
    }

    return EGIS_OK;
}

int io_dispatch_read_register(BYTE address, BYTE* value) {
    int ret;
    unsigned char write_buf[2];
    unsigned char read_buf[2];
    sec_tzspi_transfer_t tr_write;
    sec_tzspi_transfer_t tr_read;

    if (!value) return EGIS_COMMAND_FAIL;

    g_opcode = FP_REGISTER_READ;

    // Write 0xAC Address
    write_buf[0] = 0xAC;
    write_buf[1] = address;

    tr_write.buf_addr = write_buf;
    tr_write.buf_len = 2;

    ret = sec_tzspi_write(&tr_write);
    if (ret) {
        egislog_e("sec_tzspi_write fail ret = %d", ret);
        ret = EGIS_COMMAND_FAIL;
        goto exit;
    }
    if (tr_write.total_len != 2) {
        egislog_e("sec_tzspi_write fail invalid len = %d ret = %d", tr_write.total_len, ret);
        ret = EGIS_COMMAND_FAIL;
        goto exit;
    }

    // Write 0xAF then read data

    write_buf[0] = 0xAF;
    write_buf[1] = 0;

    read_buf[0] = 0;
    read_buf[1] = 0;

    tr_write.buf_addr = write_buf;
    tr_write.buf_len = 2;

    tr_read.buf_addr = read_buf;
    tr_read.buf_len = 2;

    ret = sec_tzspi_full_duplex(&tr_write, &tr_read);
    if (ret) {
        egislog_e("sec_tzspi_full_duplex fail ret = %d", ret);
        ret = EGIS_COMMAND_FAIL;
        goto exit;
    }
    if (tr_read.total_len != 2) {
        egislog_e("sec_tzspi_write fail invalid len = %d ret = %d", tr_read.total_len, ret);
        ret = EGIS_COMMAND_FAIL;
        goto exit;
    }

    value[0] = read_buf[1];

    ret = EGIS_OK;

exit:

    g_opcode = -1;

    return ret;
}

int io_dispatch_write_register(BYTE address, BYTE value) {
    int ret;
    unsigned char write_buf[2];
    sec_tzspi_transfer_t tr_write;

    g_opcode = FP_REGISTER_WRITE;

    // Write 0xAC Address
    write_buf[0] = 0xAC;
    write_buf[1] = address;

    tr_write.buf_addr = write_buf;
    tr_write.buf_len = 2;

    ret = sec_tzspi_write(&tr_write);
    if (ret) {
        egislog_e("sec_tzspi_write fail ret = %d", ret);
        ret = EGIS_COMMAND_FAIL;
        goto exit;
    }
    if (tr_write.total_len != 2) {
        egislog_e("sec_tzspi_write fail invalid len = %d ret = %d", tr_write.total_len, ret);
        ret = EGIS_COMMAND_FAIL;
        goto exit;
    }

    // Write 0xAE value
    write_buf[0] = 0xAE;
    write_buf[1] = value;

    tr_write.buf_addr = write_buf;
    tr_write.buf_len = 2;

    ret = sec_tzspi_write(&tr_write);
    if (ret) {
        ret = EGIS_COMMAND_FAIL;
        egislog_e("sec_tzspi_write fail ret = %d", ret);
        goto exit;
    }
    if (tr_write.total_len != 2) {
        ret = EGIS_COMMAND_FAIL;
        egislog_e("sec_tzspi_write fail invalid len = %d ret = %d", tr_write.total_len, ret);
        goto exit;
    }

    ret = EGIS_OK;

exit:

    g_opcode = -1;

    return ret;
}

int io_dispatch_get_frame(BYTE* frame, UINT height, UINT width, UINT number_of_frames) {
    int ret;
    UINT size = height * width * number_of_frames;
    UINT total_data_len = size + GET_FRAME_SHIFT;
    sec_tzspi_transfer_t tr_write;
    sec_tzspi_transfer_t tr_read;
    BYTE write_buf[3] = {0};
    BYTE* read_buf = (BYTE*)plat_alloc(total_data_len);
    // TEE_Time t1,t2;

    // TEE_GetSystemTime(&t1);

    if (read_buf == NULL) {
        ret = EGIS_COMMAND_FAIL;
        egislog_e("%s allocate memory fail", __func__);
        goto exit;
    }

    io_dispatch_set_tgen(TRUE);

    g_opcode = FP_GET_ONE_IMG;

    // Write 0xAC Address 0
    write_buf[0] = 0xAC;
    write_buf[1] = 0;

    tr_write.buf_addr = write_buf;
    tr_write.buf_len = 2;

    ret = sec_tzspi_write(&tr_write);
    if (ret) {
        egislog_e("sec_tzspi_write fail ret = %d", ret);
        ret = EGIS_COMMAND_FAIL;
        goto exit;
    }
    if (tr_write.total_len != 2) {
        egislog_e("sec_tzspi_write fail invalid len = %d ret = %d", tr_write.total_len, ret);
        ret = EGIS_COMMAND_FAIL;
        goto exit;
    }

    // Write 0xAF then read data
    write_buf[2] = 0xAF;

    tr_write.buf_addr = write_buf;
    tr_write.buf_len = 1;  // size;

    tr_read.buf_addr = read_buf;
    tr_read.buf_len = total_data_len;

    ret = sec_tzspi_full_duplex(&tr_write, &tr_read);
    if (ret != EGIS_OK) {
        egislog_e("sec_tzspi_full_duplex fail ret = %d", ret);
        ret = EGIS_COMMAND_FAIL;
        goto exit;
    }
    if (tr_read.total_len != total_data_len) {
        egislog_e("sec_tzspi_write fail invalid len = %d ret = %d", tr_read.total_len, ret);
        ret = EGIS_COMMAND_FAIL;
        goto exit;
    }

    memcpy(frame, read_buf + GET_FRAME_SHIFT, size);
    ret = EGIS_OK;

exit:
    g_opcode = -1;
    plat_free(read_buf);
    io_dispatch_set_tgen(FALSE);

    // TEE_GetSystemTime(&t2);
    // egislog_d("io_dispatch_get_frame   [%d] ",(t2.seconds -
    // t1.seconds)*1000+(t2.millis - t1.millis));

    return ret;
}

int io_dispatch_set_tgen(BOOL enable) {
    BYTE value;
    int ret;
    ret = io_dispatch_read_register(TGENC_ADDR, &value);
    if (ret != EGIS_OK) return ret;
    value &= ~TGEN_GEN_ON;
    value |= enable ? TGEN_GEN_ON : TGEN_GEN_OFF;
    io_dispatch_write_register(TGENC_ADDR, value);
    ret = io_dispatch_read_register(TGENC_ADDR, &value);
    return ret;
}

int io_dispatch_read_eFuse(BYTE* buf, UINT len) {
    egislog_e("not implemented yet");
    return EGIS_COMMAND_FAIL;
}
