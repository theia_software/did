#include <stdbool.h>
#include <stdio.h>

#if !defined(TZ_MODE)
#define PTHREAD_IS_SUPPORTED
#include <pthread.h>
#define egis_malloc(Z) malloc(Z)
#define egis_realloc(P, Z) realloc(P, Z)
#define egis_free(P) free(P)
static pthread_mutex_t g_alloc_thread_lock;

#elif defined(QSEE)
#include "qsee/ext_inc/qsee_heap.h"
#define egis_malloc(Z) qsee_malloc(Z)
#define egis_realloc(P, Z) qsee_realloc(P, Z)
#define egis_free(P) qsee_free(P)

#elif defined(__TRUSTONIC__)  // To do: implement plat_mem_trustonic.c
#include "TlApi/TlApiHeap.h"
#include "tlStd.h"
#define egis_malloc(Z) tlApiMalloc(Z, 0)
#define egis_realloc(P, Z) tlApiRealloc(P, Z)
#define egis_free(P) tlApiFree(P)
#endif

#ifdef PTHREAD_IS_SUPPORTED
#define _PTHREAD_INIT pthread_mutex_init(&g_alloc_thread_lock, NULL)
#define _PTHREAD_LOCK pthread_mutex_lock(&g_alloc_thread_lock)
#define _PTHREAD_UNLOCK pthread_mutex_unlock(&g_alloc_thread_lock)
#else
#define _PTHREAD_INIT
#define _PTHREAD_LOCK
#define _PTHREAD_UNLOCK
#endif

#include "egis_log.h"
#include "egis_mem_debug.h"
#include "egis_sprintf.h"
#include "plat_mem.h"

#define LOG_TAG "RBS-MEM_DEBUG"

int g_mutex_init = 0;
int g_pool_idx = 0;
int g_pool_size = 2048;
unsigned int g_mem_total = 0;
unsigned int g_mem_peak = 0;
unsigned int* g_mem_size = NULL;
unsigned int* g_file_name_line = NULL;
char** g_file_name = NULL;
void** g_mem_pool = NULL;

#define EGIS_MEM_LOG_LEVEL0 0x00
#define EGIS_MEM_LOG_LEVEL1 0x01
#define EGIS_MEM_LOG_ALLOC 0x02
#define EGIS_MEM_LOG_FREE 0x04
#define EGIS_MEM_LOG_ALL (EGIS_MEM_LOG_LEVEL1 | EGIS_MEM_LOG_ALLOC | EGIS_MEM_LOG_FREE)

static unsigned int g_egis_mem_log_level = EGIS_MEM_LOG_LEVEL0;
#define egislog_alloc(format, ...)                                                       \
    do {                                                                                 \
        if (g_egis_mem_log_level & EGIS_MEM_LOG_ALLOC) egislog_i(format, ##__VA_ARGS__); \
    } while (0)
#define egislog_free(format, ...)                                                       \
    do {                                                                                \
        if (g_egis_mem_log_level & EGIS_MEM_LOG_FREE) egislog_i(format, ##__VA_ARGS__); \
    } while (0)
#define egislog_level1(format, ...)                                                       \
    do {                                                                                  \
        if (g_egis_mem_log_level & EGIS_MEM_LOG_LEVEL1) egislog_i(format, ##__VA_ARGS__); \
    } while (0)

void egis_memory_set_log_level(unsigned int log_level) {
    g_egis_mem_log_level |= log_level;
}

void egis_memory_check(bool force_free) {
    int i = 0, k = 0, j = 0;
    for (i = g_pool_idx - 1; i >= 0; i--) {
        if (g_mem_pool[i] != NULL && g_file_name[i] != NULL) {
            if (force_free && (g_file_name_line[i] == 1)) {
                egislog_d("## leak force FREE--%s--%d", g_file_name[i], g_file_name_line[i]);
                egislog_d("## 0x%x, %d", g_mem_pool[i], g_mem_size[i]);
                sys_free(g_mem_pool[i]);
                j++;
            } else {
                egislog_level1("## alloced memory----%s----%d", g_file_name[i],
                               g_file_name_line[i]);
                egislog_level1("## 0x%x, %d", g_mem_pool[i], g_mem_size[i]);
                k++;
            }
        }
        // else egislog_e("## g_mem_pool[%d] | g_file_name[%d] is NULL", i, i);
    }
    egislog_d("## total alloced memory (free=%d, %d)", j, k);
    egislog_i("## mem_total=%d, mem_peak=%d \n", g_mem_total, g_mem_peak);
}

void egis_strncpy(char* dest, const char* src, const int max_len) {
    plat_strncpy(dest, src, max_len);
}
// void sys_strncpy(char *dest, const char *src, const int max_len)
// {
// 	char *q = dest;
// 	const char *p = src;
// 	int str_count = 0;
// 	while (str_count < max_len - 1 && *p != 0) {
// 		*q++ = *p++;
// 		str_count++;
// 	}
// 	*q = '\0';
// }

void register_mem(void* addr, size_t size, const char* file_name, int line) {
    int i;
    char* p_name = NULL;
    if (addr != NULL && size > 0 && file_name != NULL) {
        if (g_mutex_init == 0) {
            g_mutex_init = 1;
            _PTHREAD_INIT;
        }
        _PTHREAD_LOCK;

        if (g_mem_pool == NULL) {
            g_mem_pool = (void**)egis_malloc(g_pool_size * sizeof(void*));
            g_mem_size = (unsigned int*)egis_malloc(g_pool_size * sizeof(size_t));
            g_file_name = (char**)egis_malloc(g_pool_size * sizeof(char*) +
                                              g_pool_size * NAME_LENGTH * sizeof(char));
            p_name = (char*)(g_file_name + g_pool_size);
            for (i = 0; i < g_pool_size; i++, p_name += NAME_LENGTH) g_file_name[i] = p_name;
            g_file_name_line = (unsigned int*)egis_malloc(g_pool_size * sizeof(size_t));
        }

        egislog_alloc("## alloc=0x%x, [%d], %s-%d", addr, size, file_name, line);

        g_mem_total += size;
        if (g_mem_total > g_mem_peak) {
            egislog_d("## memory PEAK=(%d), add %d=size, %s--%d", g_mem_peak, size, file_name,
                      line);
            g_mem_peak = g_mem_total;
        }
        for (i = 0; i < g_pool_idx; i++) {
            if (g_mem_pool[i] == NULL) break;
        }
        if (i < g_pool_size) {
            g_mem_pool[i] = addr;
            g_mem_size[i] = size;
            egis_strncpy(g_file_name[i], file_name, NAME_LENGTH);
            g_file_name_line[i] = line;
            // egislog_d("## 0x%x, %d + %d = %d", addr, oldsize, g_mem_size[i], g_mem_total);
            // egislog_d("## %s--%d", g_file_name[i], g_file_name_line[i]);

            if (i == g_pool_idx) g_pool_idx++;
        } else {
            // g_mem_pool too small
            egislog_e("## i=%d, g_pool_size=%d", i, g_pool_size);
            int new_size = g_pool_size + 1024;
            g_mem_pool = (void**)egis_realloc(g_mem_pool, new_size * sizeof(void*));
            g_mem_size = (unsigned int*)egis_realloc(g_mem_size, new_size * sizeof(size_t));
            g_file_name = (char**)egis_realloc(
                g_file_name, new_size * sizeof(char*) + new_size * NAME_LENGTH * sizeof(char));
            p_name = (char*)(g_file_name + new_size);
            for (i = 0; i < new_size; i++, p_name += NAME_LENGTH) g_file_name[i] = p_name;
            g_file_name_line =
                (unsigned int*)egis_realloc(g_file_name_line, new_size * sizeof(size_t));
            for (i = g_pool_size; i < new_size; i++) {
                g_mem_pool[i] = NULL;
                g_mem_size[i] = 0;
                g_file_name_line[i] = 0;
            }
            g_pool_idx = g_pool_size;
            g_mem_pool[g_pool_idx] = addr;
            g_mem_size[g_pool_idx] = size;

            egis_strncpy(g_file_name[g_pool_idx], file_name, NAME_LENGTH);
            g_file_name_line[g_pool_idx] = line;
            g_pool_idx++;
            g_pool_size = new_size;
        }
        _PTHREAD_UNLOCK;
    } else
        egislog_e("## register_mem parameter error");
}

void unregister_mem(void* addr) {
    int i;
    // int oldsize = g_mem_total;
    if (addr == NULL) return;
    _PTHREAD_LOCK;
    for (i = g_pool_idx - 1; i >= 0; i--) {
        if (g_mem_pool[i] == addr) {
            g_mem_total -= g_mem_size[i];
            // if(g_file_name_line[i] == 0)
            egislog_free("## free 0x%x, %d", g_mem_pool[i], g_mem_size[i]);
            g_mem_pool[i] = NULL;
            g_mem_size[i] = 0;
            g_file_name_line[i] = 0;

            if (i == g_pool_idx - 1) {
                while (g_mem_size[--g_pool_idx] == 0) {
                }
                g_pool_idx++;
            }
            if (g_mem_total == 0) {  // clean
                // g_mutex_init = 0;
                egis_free(g_mem_pool);
                egis_free(g_mem_size);
                egis_free(g_file_name);
                g_mem_pool = NULL;
                g_mem_size = NULL;
                g_file_name = NULL;
                g_file_name_line = NULL;
                g_pool_idx = 0;  //
                egislog_d("## memory clean");
            }
            _PTHREAD_UNLOCK;
            return;
        }
    }
    _PTHREAD_UNLOCK;
    egislog_e("## illegal address %p: ", addr);
}
