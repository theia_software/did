#pragma once

#include "type_definition.h"

int io_eeprom_sector_write_anysize(BYTE* addr, int length, BYTE* pvalue);
int io_eeprom_read_anysize(BYTE* addr, int length, BYTE* pvalue);
