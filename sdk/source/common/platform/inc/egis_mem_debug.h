#ifndef __EGIS_MEM_DEBUG_H__
#define __EGIS_MEM_DEBUG_H__
#include <stdbool.h>
#include <stddef.h>

void register_mem(void* addr, size_t size, const char* file_name, int line);
void unregister_mem(void* addr);
void egis_memory_check(bool force_free);
void egis_strncpy(char* dest, const char* src, const int max_len);
void egis_memory_set_log_level(unsigned int log_level);
// void sys_strncpy(char *dest, const char *src, const int max_len);

#endif
