#ifndef __PLAT_OTG__
#define __PLAT_OTG__

#include <stdint.h>

typedef void (*event_callback_t)(int event_id, int first_param, int second_param,
                                 unsigned char* data, int data_size);

typedef enum core_callback_event {
    // Used event id range is 10000 - 10999
    EVENT_IO_DISPATCH_WRITE_REGISTER = 10000,
    EVENT_IO_DISPATCH_READ_REGISTER = 10001,
    EVENT_IO_DISPATCH_GET_FRAME = 10002,
    EVENT_IO_DISPATCH_STANDBY = 10003,
    EVENT_IO_DISPATCH_WAKEUP = 10004,
    EVENT_IO_DISPATCH_GET_FRAME2 = 10005,
    EVENT_IO_DISPATCH_SENSOR_SELECT = 10006,
    EVENT_IO_DISPATCH_SPI_WRITE_READ = 10007,
    EVENT_IO_DISPATCH_SET_SPI_CLK = 10008,
    EVENT_OTG_GET_SENSOR_ID = 10900,
    EVENT_OTG_INIT_SENSOR = 10901,
    EVENT_OTG_SET_SENSOR_PARAM = 10902,
    EVENT_OTG_GET_IMAGE = 10903,
    EVENT_OTG_GET_SENSOR_PARAM = 10904,
	EVENT_OTG_ET760_READ_REGISTER = 10905,
	EVENT_OTG_ET760_WRITE_REGISTER = 10906,
	EVENT_OTG_ET760_RAW_SPI = 10907
} core_callback_event_t;

int32_t io_7xx_dispatch_wakeup();
int32_t io_7xx_dispatch_standby();
int32_t io_7xx_dispatch_sensor_select(uint16_t sensor_sel);

int32_t io_7xx_dispatch_spi_write_read_halfduplex(uint8_t* buffer, int writeLen, int readLen);
int32_t io_7xx_dispatch_set_spi_clk(uint8_t mode, uint8_t clk);

int32_t io_760_dispatch_get_id(uint8_t *dev_id0, uint8_t *dev_id1);
int32_t io_760_dispatch_init_sensor();
int32_t io_760_dispatch_set_sensor_param(int param, int value);
int32_t io_760_dispatch_get_sensor_param(int param, int *value);
int32_t io_760_dispatch_get_frame(uint8_t *buffer, int length);
int32_t io_760_dispatch_read_register(int param, int *value);
int32_t io_760_dispatch_write_register(int param, int value);
int32_t io_760_dispatch_raw_spi(int param, uint8_t *buffer, int length) ;

#endif
