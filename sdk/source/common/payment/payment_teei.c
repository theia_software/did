#include "payment.h"
#include "plat_log.h"
#include "plat_mem.h"
#define ALIPAY_SOLUTION_VERSION 0x01;
static struct LastIdentifyResult g_last_identify_result;
static unsigned int g_fingerprint_ids[SUPPORT_MAX_ENROLL_COUNT];
static unsigned int g_fingerprint_count = 0;

void payment_update_last_identify_result(struct LastIdentifyResult* result) {
    if (result == NULL) {
        mem_set(&g_last_identify_result, 0, sizeof(struct LastIdentifyResult));
        return;
    }
    g_last_identify_result.matched_fingerprint_id = result->matched_fingerprint_id;
}

unsigned int payment_update_fingerprint_ids(unsigned int* ids, unsigned int count, char* path) {
    if (count > SUPPORT_MAX_ENROLL_COUNT) {
        ex_log(LOG_ERROR, "payment_update_fingerprint_ids count > limit");
        return IFAA_ERR_BAD_PARAM;
    }
    if (ids == NULL) {
        ex_log(LOG_ERROR, "payment_update_fingerprint_ids intput data is NULL");
        return IFAA_ERR_BAD_PARAM;
    }
    g_fingerprint_count = count;
    mem_move(&g_fingerprint_ids, ids, sizeof(unsigned int) * count);

    return IFAA_ERR_SUCCESS;
}

unsigned int payment_get_last_identify_fingerid() {
    return g_last_identify_result.matched_fingerprint_id;
}

unsigned int payment_get_version() {
    return ALIPAY_SOLUTION_VERSION;
}
