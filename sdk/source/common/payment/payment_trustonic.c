#include <string.h>

#include "egis_error.h"
#include "payment.h"
#include "plat_file.h"
#include "plat_log.h"
#include "plat_mem.h"
#include "plat_time.h"
#ifdef SUPPORT_PAYMENT
#include "egis_alipay_api.h"
#include "egis_dr.h"
#include "ifaa_egis_interface.h"
#endif
#define MAX_FINGERS_PER_USER 5

void payment_update_last_identify_result(struct LastIdentifyResult* identify_result) {
    egis_error_t err = EGIS_SUCCESS;

    int fingerid = -1;
    int len = sizeof(fingerid);

    ex_log(LOG_DEBUG,
           "payment_update_last_identify_result identify_result->matched_fingerprint_id = %d",
           identify_result->matched_fingerprint_id);
    uint64_t time = 0;
#ifdef SUPPORT_PAYMENT
    egis_dr_get_timestamp(&time);
    err = egis_dr_sync_auth_result(identify_result->matched_fingerprint_id, time);

    ifaa_egis_getFpLastIdentifiedResult((uint8_t*)&fingerid, (uint32_t*)&len);
#endif
    ex_log(LOG_DEBUG, "payment_update_last_identify_result end,fingerid = %d , len = %d", fingerid,
           len);
}

unsigned int payment_setAuthenticatorVersion(unsigned int version, char* path) {
    egis_error_t err = EGIS_SUCCESS;
    int32_t ver = 0;
    ex_log(LOG_DEBUG, "payment_setAuthenticatorVersion enter version = %d", version);
#ifdef SUPPORT_PAYMENT
    err = egis_dr_sync_authenticator_version((uint32_t)version);
    ifaa_egis_getAuthenticatorVersion(&ver);
#endif
    ex_log(LOG_DEBUG, "payment_setAuthenticatorVersion end err = 0X%x,ver = %d", err, ver);
    return err;
}

unsigned int payment_update_fingerprint_ids(unsigned int* ids, unsigned int count, char* path) {
    egis_error_t err = EGIS_SUCCESS;
    int index = 0;
    int tempids[5];
    unsigned char* fignerid = mem_alloc(sizeof(int) * MAX_FINGERS_PER_USER * 2);
    int bufflen = sizeof(int) * MAX_FINGERS_PER_USER * 2;
    unsigned int* temp = NULL;

    if (fignerid == NULL) return -1;
    mem_set(fignerid, 0, bufflen);

    ex_log(LOG_DEBUG, "payment_update_fingerprint_ids 11 enter count = %d ,bufflen = %d", count,
           bufflen);
#ifdef SUPPORT_PAYMENT
    err = egis_dr_sync_finger_list((uint32_t*)ids, (uint32_t)count);
    ex_log(LOG_DEBUG, "payment_update_fingerprint_ids 1 err = %d", err);

    err = ifaa_egis_getIdList((uint8_t*)fignerid, (uint32_t*)&bufflen);
    ex_log(LOG_DEBUG, "payment_update_fingerprint_ids 2 err = %d,bufflen = %d", err, bufflen);
#endif
    /*
    count = 5;
    tempids[0] = 2001;
    tempids[1] = 2002;
    tempids[2] = 2003;
    tempids[3] = 2004;
    tempids[4] = 2005;

    for(index = 0; index < 5; index++)
    {
        ex_log(LOG_DEBUG, "payment_update_fingerprint_ids 1 before call err = 0X%x,tempids[%d] = %d
    ", err,index , tempids[index]);
    }

    err = egis_dr_sync_finger_list((uint32_t *)tempids, (uint32_t)count);

    temp = (unsigned int*)fignerid;
    for(index = 0; index < 5; index++)
    {
        ex_log(LOG_DEBUG, "payment_update_fingerprint_ids 2 before call err = %d,temp[%d] = %d ",
    err,index , temp[index]);
    }

    err = ifaa_egis_getIdList((uint8_t *)fignerid, (uint32_t *)&bufflen);
    */
    /*
    temp = (unsigned int *)fignerid;
    int i = 0;

    ex_log(LOG_DEBUG, "payment_update_fingerprint_ids 11 bufflen = %d" ,bufflen);

    ex_log(LOG_DEBUG, "payment_update_fingerprint_ids fingerid[%d] = %d", i, temp[i]);
    i++;
    ex_log(LOG_DEBUG, "payment_update_fingerprint_ids fingerid[%d] = %d", i, temp[i]);
    i++;
    ex_log(LOG_DEBUG, "payment_update_fingerprint_ids fingerid[%d] = %d", i, temp[i]);
    i++;
    ex_log(LOG_DEBUG, "payment_update_fingerprint_ids fingerid[%d] = %d", i, temp[i]);
    i++;
    ex_log(LOG_DEBUG, "payment_update_fingerprint_ids fingerid[%d] = %d", i, temp[i]);
    i++;
    ex_log(LOG_DEBUG, "payment_update_fingerprint_ids fingerid[%d] = %d", i, temp[i]);
    i++;
    ex_log(LOG_DEBUG, "payment_update_fingerprint_ids fingerid[%d] = %d", i, temp[i]);
    i++;
    ex_log(LOG_DEBUG, "payment_update_fingerprint_ids fingerid[%d] = %d", i, temp[i]);
    i++;
    ex_log(LOG_DEBUG, "payment_update_fingerprint_ids fingerid[%d] = %d", i, temp[i]);
    i++;
    ex_log(LOG_DEBUG, "payment_update_fingerprint_ids fingerid[%d] = %d", i, temp[i]);
    i++;
    */
    mem_free(fignerid);
    ex_log(LOG_DEBUG, "payment_update_fingerprint_ids end err = 0X%x, bufflen = %d", err, bufflen);
    return err;
}
