#include <string.h>

#include "AES.h"
#include "Table.h"
#include "fp_types.h"

#ifdef ARM_NEON
#include <arm_neon.h>
#endif

enum { ECB_MODE = 1, CBC_MODE, OFB_MODE, CFB_MODE, CTR_MODE };

#define NB 4
static void ArrayIncrement(unsigned char* buffer, unsigned int array_size) {
    int i;

    for (i = (int)array_size - 1; i >= 0; i--) {
        if (buffer[i] != 0xff) {
            buffer[i]++;
            break;
        } else {
            buffer[i]++;
        }
    }
}

static unsigned char Multiplication(unsigned char a, unsigned char b) {
    int i;
    unsigned char result = 0;
    unsigned char temp[8];
    temp[0] = a;
    if (a == 0x01) return b;
    if (b == 0x01) return a;

    for (i = 0; i < 7; i++) {
        if (temp[i] >= 0x80) {
            temp[i + 1] = (temp[i] << 1) ^ 0x1B;
        } else {
            temp[i + 1] = temp[i] << 1;
        }
    }

    for (i = 0; i < 8; i++) {
        unsigned char value;
        value = (b >> i) & 0x01;
        if (value == 0x01) {
            result ^= temp[i];
        }
    }

    return result;
}

static void MatrixMultiplication(unsigned char** w, int Nb, int Nr) {
    unsigned char p[4][4];
    unsigned char Round = Nb * Nr;
    int t, i, j, k, s;

    for (t = Nb; t < Round; t += Nb) {
        for (i = 0; i < Nb; i++) {
            for (j = 0; j < 4; j++) {
                for (k = 0; k < 4; k++) {
                    p[j][k] = Multiplication(w[t + i][j], InvMixColumn[j][k]);
                }
            }
            for (s = 0; s < 4; s++) {
                w[t + i][s] = p[0][s] ^ p[1][s] ^ p[2][s] ^ p[3][s];
            }
        }
    }
}

static void MatrixMultiplication32(unsigned int* w32, int Nr) {
    int t, i, j, k, buf_4;
    unsigned char* b4 = (unsigned char*)&buf_4;
    for (t = NB; t < NB * Nr; t += NB) {
        for (i = 0; i < NB; i++) {
            unsigned char* w = (unsigned char*)&(w32[t + i]);
            buf_4 = 0;
            for (j = 0; j < 4; j++) {
                for (k = 0; k < 4; k++) {
                    b4[k] ^= Multiplication(w[j], InvMixColumn[j][k]);
                }
            }
            w32[t + i] = buf_4;
        }
    }
}

void KeyExpan(unsigned char* key, unsigned char** w, unsigned char key_length, int decipher) {
    int Nk = key_length / 4;
    int Nr = NumRounds[Nk / 2 - 2][NB / 2 - 2];
    int i, j, iRound;
    unsigned char temp[4] = {0};
    unsigned char temp2;

    for (i = 0; i < Nk; i++) {
        w[i][0] = key[4 * i];
        w[i][1] = key[4 * i + 1];
        w[i][2] = key[4 * i + 2];
        w[i][3] = key[4 * i + 3];
    }

    iRound = NB * (Nr + 1);

    for (i = Nk; i < iRound; i++) {
        temp[0] = w[i - 1][0];
        temp[1] = w[i - 1][1];
        temp[2] = w[i - 1][2];
        temp[3] = w[i - 1][3];

        if (i % Nk == 0) {
            // Rot word
            temp2 = temp[0];
            temp[0] = temp[1];
            temp[1] = temp[2];
            temp[2] = temp[3];
            temp[3] = temp2;

            // SubWord
            for (j = 0; j < 4; j++) {
                temp[j] = S[temp[j]];
            }

            // Rcon
            temp[0] ^= Rcon[i / Nk - 1][0];
        } else {
            if ((Nk > 6) && (i % Nk == 4)) {
                // SubWord
                for (j = 0; j < 4; j++) {
                    temp[j] = S[temp[j]];
                }
            }
        }

        w[i][0] = w[i - Nk][0] ^ temp[0];
        w[i][1] = w[i - Nk][1] ^ temp[1];
        w[i][2] = w[i - Nk][2] ^ temp[2];
        w[i][3] = w[i - Nk][3] ^ temp[3];
    }

    if (decipher) {
        MatrixMultiplication(w, NB, Nr);
    }
}

static void KeyExpan32(unsigned char* key, unsigned int* w32, unsigned char key_length, int decipher) {
    int Nk = key_length / 4;
    int Nr = NumRounds[Nk / 2 - 2][NB / 2 - 2];
    int i, iRound;
    unsigned int *key32 = (unsigned int*)key, tmp;

    for (i = 0; i < Nk; i++) {
        w32[i] = key32[i];
    }

    iRound = NB * (Nr + 1);

    for (i = Nk; i < iRound; i++) {
        tmp = w32[i - 1];
        if (i % Nk == 0) {
            // Rot word
            tmp = (tmp >> 8) | (tmp << 24);
            // SubWord
            tmp = S[tmp & 0xFF] | (S[(tmp >> 8) & 0xFF] << 8) | (S[(tmp >> 16) & 0xFF] << 16) |
                  (S[(tmp >> 24) & 0xFF] << 24);
            // Rcon
            tmp ^= Rcon[i / Nk - 1][0];
        } else {
            if ((Nk > 6) && (i % Nk == 4)) {
                // SubWord
                tmp = S[tmp & 0xFF] | (S[(tmp >> 8) & 0xFF] << 8) | (S[(tmp >> 16) & 0xFF] << 16) |
                      (S[(tmp >> 24) & 0xFF] << 24);
            }
        }
        w32[i] = w32[i - Nk] ^ tmp;
    }

    if (decipher) {
        MatrixMultiplication32(w32, Nr);
    }
}

#ifdef ARM_NEON
static void RijndaelCipher(unsigned char* buffer, unsigned char** w, int Nb, int Nk, int Nr) {
    int i, j, t;
    unsigned int *p_store = (unsigned int*)buffer, inv_state32[4];
    unsigned int *p_key = (unsigned int*)w[0], p[4][NB];
    unsigned char* ptr = (unsigned char*)inv_state32;
    unsigned char column_shift;
    // unsigned char ShTableSelectRow = Nb / 2 - 2;
    uint32x4_t v_state = vld1q_u32(inv_state32);
    uint32x4_t v_store = vld1q_u32(p_store);
    uint32x4_t v_key = vld1q_u32(p_key);

    v_state = veorq_u32(v_store, v_key);
    vst1q_u32(inv_state32, v_state);

    p_key += Nb;
    for (t = 1; t < Nr; t++) {
        for (i = 0; i < Nb; i++) {
            column_shift = i << 2;
            for (j = 0; j < 4; j++) {
                p[j][i] = *(unsigned int*)T[j][ptr[InvSOffset[i][j]]];
            }  // end j

        }  // end i

        v_key = vld1q_u32(p_key);
        uint32x4_t vT1 = vld1q_u32(p[0]);
        uint32x4_t vT2 = vld1q_u32(p[1]);
        uint32x4_t vT3 = vld1q_u32(p[2]);
        uint32x4_t vT4 = vld1q_u32(p[3]);

        v_store = veorq_u32(veorq_u32(vT1, vT2), veorq_u32(vT3, vT4));
        v_store = veorq_u32(v_store, v_key);
        vst1q_u32(inv_state32, v_store);
        p_key += Nb;
    }

    vst1q_u32(p_store, v_store);

    ptr = (unsigned char*)inv_state32;
    for (i = 0; i < Nb; i++) {
        column_shift = ColumnShiftTable[i];
        for (j = 0; j < 4; j++) ptr[column_shift + j] = S[buffer[InvSOffset_ShiftPos[i][j]]];
    }

    v_state = vld1q_u32(inv_state32);
    v_key = vld1q_u32(p_key);

    v_store = veorq_u32(v_state, v_key);
    vst1q_u32(p_store, v_store);
}

#else
static void RijndaelCipher(unsigned char* buffer, unsigned char** w, int Nb, int Nk, int Nr) {
    int i, j, t;
    unsigned int *p_store = (unsigned int*)buffer, inv_state32[4];
    unsigned int *p_key = (unsigned int*)w[0], *p32[4];

    unsigned char *ptr = (unsigned char*)inv_state32, *ptr_temp;
    const unsigned char* p[4];
    unsigned char column_shift;
    Nk = 0;
    // unsigned char ShTableSelectRow = Nb / 2 - 2;

    for (i = 0; i < Nb; i++) {
        inv_state32[i] = p_store[i] ^ *p_key;
        p_key++;
    }

    //-------------------------------------------------------//

    for (t = 1; t < Nr; t++) {
        for (i = 0; i < Nb; i++) {
            column_shift = i << 2;
            for (j = 0; j < 4; j++) {
                p[j] = T[j][ptr[InvSOffset[i][j]]];
                p32[j] = (unsigned int*)p[j];
            }  // end j

            p_store[i] = *p32[0] ^ *p32[1] ^ *p32[2] ^ *p32[3] ^ *p_key;
            p_key++;
        }  // end i

        ptr_temp = ptr;
        ptr = (unsigned char*)p_store;
        p_store = (unsigned int*)ptr_temp;
    }

    ptr = (unsigned char*)inv_state32;
    for (i = 0; i < Nb; i++) {
        column_shift = ColumnShiftTable[i];
        for (j = 0; j < 4; j++) ptr[column_shift + j] = S[buffer[InvSOffset_ShiftPos[i][j]]];
    }

    p_store = (unsigned int*)buffer;
    for (i = 0; i < Nb; i++) {
        p_store[i] = inv_state32[i] ^ p_key[i];
    }
}
#endif

#ifdef ARM_NEON
static void RijndaelDecipher(unsigned char* buffer, unsigned char** w, int Nb, int Nk, int Nr) {
    int i, j, t;
    unsigned int *p_store = (unsigned int*)buffer, *p_key, inv_state32[4], *p32[4];
    unsigned int p[4][NB];
    unsigned char *ptr = (unsigned char*)inv_state32, *ptr_temp;
    unsigned char column_shift;
    unsigned char start = Nb * Nr;
    Nk = 0;
    // unsigned char ShTableSelectRow = Nb / 2 - 2;

    p_key = (unsigned int*)w[start];
    uint32x4_t v_state = vld1q_u32(inv_state32);
    uint32x4_t v_store = vld1q_u32(p_store);
    uint32x4_t v_key = vld1q_u32(p_key);

    v_state = veorq_u32(v_store, v_key);
    vst1q_u32(inv_state32, v_state);

    p_key -= Nb;

    for (t = 1; t < Nr; t++) {
        for (i = 0; i < Nb; i++) {
            for (j = 0; j < 4; j++) {
                p[j][i] = *(unsigned int*)InvT[j][ptr[InvTOffset[i][j]]];
            }
        }  // end i

        v_key = vld1q_u32(p_key);
        uint32x4_t vT1 = vld1q_u32(p[0]);
        uint32x4_t vT2 = vld1q_u32(p[1]);
        uint32x4_t vT3 = vld1q_u32(p[2]);
        uint32x4_t vT4 = vld1q_u32(p[3]);

        v_store = veorq_u32(veorq_u32(vT1, vT2), veorq_u32(vT3, vT4));
        v_store = veorq_u32(v_store, v_key);
        vst1q_u32(inv_state32, v_store);
        p_key -= Nb;
    }  // end t

    // SubByte operation using SBox, not TBox
    vst1q_u32(p_store, v_store);
    for (i = 0; i < Nb; i++) {
        column_shift = ColumnShiftTable[i];
        for (j = 0; j < 4; j++) {
            ptr[column_shift + j] = InvS[buffer[InvTOffset_ShiftPos[i][j]]];
        }
    }

    v_state = vld1q_u32(inv_state32);
    v_key = vld1q_u32(p_key);

    v_store = veorq_u32(v_state, v_key);
    vst1q_u32(p_store, v_store);
}

#else
static void RijndaelDecipher(unsigned char* buffer, unsigned char** w, int Nb, int Nk, int Nr) {
    int i, j, t;
    unsigned int *p_store = (unsigned int*)buffer, *p_key, inv_state32[4], *p32[4];
    unsigned char *ptr = (unsigned char*)inv_state32, *ptr_temp;
    unsigned char column_shift;
    unsigned char start = Nb * Nr;
    const unsigned char* p[4];
    Nk = 0;
    // unsigned char ShTableSelectRow = Nb / 2 - 2;

    p_key = (unsigned int*)w[start];
    for (i = 0; i < Nb; i++) {
        inv_state32[i] = p_store[i] ^ *p_key;
        p_key++;
    }

    for (t = 1; t < Nr; t++) {
        start -= Nb;
        p_key = (unsigned int*)w[start];
        for (i = 0; i < Nb; i++) {
            for (j = 0; j < 4; j++) {
                p[j] = InvT[j][ptr[InvTOffset[i][j]]];
                p32[j] = (unsigned int*)p[j];
            }

            p_store[i] = *p32[0] ^ *p32[1] ^ *p32[2] ^ *p32[3] ^ p_key[i];

        }  // end i

        ptr_temp = ptr;

        ptr = (unsigned char*)p_store;
        p_store = (unsigned int*)ptr_temp;
    }  // end t

    // SubByte operation using SBox, not TBox
    ptr = (unsigned char*)inv_state32;
    for (i = 0; i < Nb; i++) {
        column_shift = ColumnShiftTable[i];
        for (j = 0; j < 4; j++) {
            ptr[column_shift + j] = InvS[buffer[InvTOffset_ShiftPos[i][j]]];
        }
    }

    p_store = (unsigned int*)buffer;
    start -= Nb;
    p_key = (unsigned int*)w[start];
    for (i = 0; i < Nb; i++) {
        p_store[i] = inv_state32[i] ^ p_key[i];
    }
}
#endif

static void AESEncrypt_ECB(unsigned char* buffer, unsigned char** w, unsigned int encode_size, int Nb,
                    int Nk, int Nr) {
    unsigned int i;
    for (i = 0; i < encode_size; i += 16) {
        RijndaelCipher(&buffer[i], w, Nb, Nk, Nr);
    }
}

static void AESEncrypt_CBC(unsigned char* buffer, unsigned char** w, unsigned int encode_size,
                    unsigned char* IV, int Nb, int Nk, int Nr) {
    unsigned int i, j;
    unsigned int* pre_block_ptr = NULL;
    unsigned int* now_block_ptr;
    unsigned int* buffer32 = (unsigned int*)buffer;
    unsigned int* pIV32 = (unsigned int*)IV;

    for (i = 0; i < encode_size; i += 16) {
        if (i == 0) {
            for (j = 0; j < 4; j++) {
                buffer32[j] ^= pIV32[j];
            }
        } else {
            now_block_ptr = (unsigned int*)&buffer[i];
            for (j = 0; j < 4; j++) {
                now_block_ptr[j] ^= pre_block_ptr[j];
            }
        }

        pre_block_ptr = (unsigned int*)&buffer[i];
        RijndaelCipher(&buffer[i], w, Nb, Nk, Nr);  // block cipher(16bytes)
    }
}

static void AESEncrypt_OFB(unsigned char* buffer, unsigned char** w, unsigned int encode_size,
                    unsigned char* IV, int Nb, int Nk, int Nr) {
    unsigned int i, j;
    unsigned int cipher_temp[4];
    unsigned int* now_block;

    memcpy((void*)cipher_temp, IV, 16);
    for (i = 0; i < encode_size; i += 16) {
        now_block = (unsigned int*)&buffer[i];
        RijndaelCipher((unsigned char*)cipher_temp, w, Nb, Nk, Nr);  // block cipher(16bytes)
        for (j = 0; j < 4; j++) {
            now_block[j] ^= cipher_temp[j];
        }
    }
}

static void AESEncrypt_CFB(unsigned char* buffer, unsigned char** w, unsigned int encode_size,
                    unsigned char* IV, int Nb, int Nk, int Nr) {
    unsigned int i, j;
    unsigned int cipher_temp[4];
    unsigned int* now_block;

    memcpy((void*)cipher_temp, IV, 16);
    for (i = 0; i < encode_size; i += 16) {
        now_block = (unsigned int*)&buffer[i];
        RijndaelCipher((unsigned char*)cipher_temp, w, Nb, Nk, Nr);  // block cipher(16bytes)
        for (j = 0; j < 4; j++) {
            now_block[j] ^= cipher_temp[j];
        }
        memcpy((unsigned char*)cipher_temp, now_block, 16);
    }
}

static void AESEncrypt_CTR(unsigned char* buffer, unsigned char** w, unsigned int encode_size,
                    unsigned char* IV, int Nb, int Nk, int Nr) {
    unsigned int i, j;
    unsigned int cipher_temp[4];
    unsigned int counter[4];
    unsigned int* now_block;

    memcpy((void*)counter, IV, 16);
    for (i = 0; i < encode_size; i += 16) {
        now_block = (unsigned int*)&buffer[i];
        memcpy((void*)cipher_temp, counter, 16);
        RijndaelCipher((unsigned char*)cipher_temp, w, Nb, Nk, Nr);
        for (j = 0; j < 4; j++) {
            now_block[j] ^= cipher_temp[j];
        }
        ArrayIncrement((unsigned char*)counter, 16);
    }
}

static void AESDecrypt_ECB(unsigned char* buffer, unsigned char** w, unsigned int decode_size, int Nb,
                    int Nk, int Nr) {
    unsigned int i;

    for (i = 0; i < decode_size; i += 16) {
        RijndaelDecipher(&buffer[i], w, Nb, Nk, Nr);
    }
}

static void AESDecrypt_CBC(unsigned char* buffer, unsigned char** w, unsigned int decode_size,
                    unsigned char* IV, int Nb, int Nk, int Nr) {
    unsigned int i, j;
    unsigned int cipher_pre[4];
    unsigned int cipher_now[4];
    unsigned int* pre_temp = cipher_pre;
    unsigned int* now_temp = cipher_now;
    unsigned int* pIV = (unsigned int*)IV;
    unsigned int *now_block, *swap_temp;

    for (i = 0; i < decode_size; i += 16) {
        now_block = (unsigned int*)&buffer[i];
        memcpy((unsigned int*)now_temp, now_block, 16);
        RijndaelDecipher((unsigned char*)now_block, w, Nb, Nk, Nr);  // block decipher(16bytes)

        if (i == 0) {
            for (j = 0; j < 4; j++) {
                now_block[j] ^= pIV[j];
            }
        } else {
            for (j = 0; j < 4; j++) {
                now_block[j] ^= pre_temp[j];
            }
        }

        swap_temp = now_temp;
        now_temp = pre_temp;
        pre_temp = swap_temp;
    }
}

static void AESDecrypt_CFB(unsigned char* buffer, unsigned char** w, unsigned int decode_size,
                    unsigned char* IV, int Nb, int Nk, int Nr) {
    unsigned int i, j;
    unsigned int decipher_now[4];
    unsigned int decipher_next[4];
    unsigned int* cipher_data = decipher_now;
    unsigned int* next_cipher_data = decipher_next;
    unsigned int* now_block;
    unsigned int* ptr_temp;

    memcpy((void*)cipher_data, IV, 16);
    for (i = 0; i < decode_size; i += 16) {
        now_block = (unsigned int*)&buffer[i];

        RijndaelCipher((unsigned char*)cipher_data, w, Nb, Nk, Nr);

        memcpy((void*)next_cipher_data, now_block, 16);

        for (j = 0; j < 4; j++) {
            now_block[j] ^= cipher_data[j];
        }

        ptr_temp = cipher_data;
        cipher_data = next_cipher_data;
        next_cipher_data = ptr_temp;
    }
}

void AES_AESEncrypt(unsigned char* buffer, unsigned char* key, unsigned int buffer_length,
                unsigned char key_length, unsigned char* IV, int iMode) {
    int i;
    int Nb = NB;
    int Nk = key_length / 4;
    int Nr = NumRounds[Nk / 2 - 2][Nb / 2 - 2];
    unsigned int encode_size;
    unsigned char mem_buf[60 * 4];
    unsigned char* w[60];
    unsigned int* sub_key = (unsigned int*)mem_buf;
    memset(mem_buf, 0, sizeof(mem_buf));
    for (i = 0; i < Nb * (Nr + 1); i++) {
        w[i] = mem_buf + 4 * i;
    }

    KeyExpan32(key, sub_key, key_length, FALSE);

    encode_size = buffer_length;

    if (encode_size % 16 != 0) encode_size = (buffer_length / 16) * 16;

    switch (iMode) {
        case ECB_MODE:
            AESEncrypt_ECB(buffer, w, encode_size, Nb, Nk, Nr);
            break;

        case CBC_MODE:
            AESEncrypt_CBC(buffer, w, encode_size, IV, Nb, Nk, Nr);
            break;

        case OFB_MODE:
            AESEncrypt_OFB(buffer, w, encode_size, IV, Nb, Nk, Nr);
            break;

        case CFB_MODE:
            AESEncrypt_CFB(buffer, w, encode_size, IV, Nb, Nk, Nr);
            break;

        case CTR_MODE:
            AESEncrypt_CTR(buffer, w, encode_size, IV, Nb, Nk, Nr);
            break;

        default:
            break;
    }
}

void AES_AESDecrypt(unsigned char* buffer, unsigned char* key, unsigned int buffer_length,
                unsigned char key_length, unsigned char* IV, int mode) {
    int i;
    int Nb = NB;
    int Nk = key_length / 4;
    int Nr = NumRounds[Nk / 2 - 2][Nb / 2 - 2];
    unsigned int decode_size;
    unsigned char mem_buf[60 * 4];
    unsigned char* w[60];
    unsigned int* sub_key = (unsigned int*)mem_buf;
    memset(mem_buf, 0, sizeof(mem_buf));
    for (i = 0; i < Nb * (Nr + 1); i++) {
        w[i] = mem_buf + 4 * i;
    }

    decode_size = buffer_length;

    if (decode_size % 16 != 0) decode_size = (buffer_length / 16) * 16;

    switch (mode) {
        case ECB_MODE:
            KeyExpan32(key, sub_key, key_length, TRUE);
            AESDecrypt_ECB(buffer, w, decode_size, Nb, Nk, Nr);
            break;

        case CBC_MODE:
            KeyExpan32(key, sub_key, key_length, TRUE);
            AESDecrypt_CBC(buffer, w, decode_size, IV, Nb, Nk, Nr);
            break;

        case OFB_MODE:
            KeyExpan32(key, sub_key, key_length, FALSE);
            AESEncrypt_OFB(buffer, w, decode_size, IV, Nb, Nk, Nr);
            break;
        case CFB_MODE:
            KeyExpan32(key, sub_key, key_length, FALSE);
            AESDecrypt_CFB(buffer, w, decode_size, IV, Nb, Nk, Nr);
            break;

        case CTR_MODE:
            KeyExpan32(key, sub_key, key_length, FALSE);
            AESEncrypt_CTR(buffer, w, decode_size, IV, Nb, Nk, Nr);

            break;
        default:
            break;
    }
}

// BOOL LTTAESCipher(unsigned char* Buffer, unsigned char* key, unsigned char KeyLength, int
// BufferLength)
int LTTAESCipher(unsigned char* buffer, unsigned char* key, unsigned char key_length,
                 int buffer_length) {
    unsigned char IV[16] = {0x00, 0x22, 0x44, 0x66, 0x88, 0xaa, 0xcc, 0xee,
                            0xff, 0xdd, 0xbb, 0x99, 0x77, 0x55, 0x33, 0x11};
    AES_AESEncrypt(buffer, key, buffer_length, key_length, IV, CBC_MODE);
    return TRUE;
}

// BOOL LTTAESCipher(unsigned char* Buffer, unsigned char* key, unsigned char KeyLength, int
// BufferLength)
int LTTAESDeCipher(unsigned char* buffer, unsigned char* key, unsigned char key_length,
                   int buffer_length) {
    unsigned char IV[16] = {0x00, 0x22, 0x44, 0x66, 0x88, 0xaa, 0xcc, 0xee,
                            0xff, 0xdd, 0xbb, 0x99, 0x77, 0x55, 0x33, 0x11};
    AES_AESDecrypt(buffer, key, buffer_length, key_length, IV, CBC_MODE);
    return TRUE;
}

/*
bool FileEncode(CHAR* chFileName, CHAR* EncodeFileName, unsigned char* key) {
    HANDLE handle;
    HANDLE handle2;

    handle = CreateFile(chFileName, GENERIC_READ, FILE_SHARE_READ , NULL, OPEN_EXISTING,
FILE_ATTRIBUTE_NORMAL, NULL); handle2 = CreateFile(EncodeFileName, GENERIC_WRITE, FILE_SHARE_WRITE,
NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);

    if(handle == INVALID_HANDLE_VALUE || handle2 ==INVALID_HANDLE_VALUE)
        return FALSE;

    LARGE_INTEGER FileSize;

    if(!GetFileSizeEx(handle, &FileSize))
        return FALSE;
    unsigned int dwBufferSize = FileSize.QuadPart;

    DWORD dw;
    unsigned char* buffer = new unsigned char[dwBufferSize];

    if(!ReadFile(handle, buffer, dw, &dw, NULL)) {
        DWORD w = GetLastError();
        return FALSE;

    }
    CloseHandle(handle);
    DeleteFile(chFileName);

    unsigned char IV[16] = {0x01, 0x23, 0x45, 0x67, 0x89, 0xab, 0xcd, 0xef, 0x01, 0x23, 0x45, 0x67,
0x89, 0xab, 0xcd, 0xef};

    //AESEncrypt(buffer, key, dwBufferSize, 16, IV, ECB_MODE);
    //AESEncrypt(buffer, key, dwBufferSize, 16, IV, CBC_MODE);
    //AESEncrypt(buffer, key, dwBufferSize, 16, IV, OFB_MODE);
    //AESEncrypt(buffer, key, dwBufferSize, 16, IV, CFB_MODE);
    AESEncrypt(buffer, key, dwBufferSize, 16, IV, CTR_MODE);

    if(!WriteFile(handle2, buffer, dwBufferSize, &dw, NULL))
        return FALSE;

    CloseHandle(handle2);

    delete [] buffer;
    return true;
}

bool FileDecode(CHAR* chFileName, CHAR* DecodeFileName, unsigned char* key) {
    HANDLE handle;
    HANDLE handle2;

    handle = CreateFile(chFileName, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
FILE_ATTRIBUTE_NORMAL, NULL); handle2 = CreateFile(DecodeFileName, GENERIC_WRITE, FILE_SHARE_WRITE,
NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);

    if(handle == INVALID_HANDLE_VALUE || handle2 ==INVALID_HANDLE_VALUE)
        return FALSE;

    LARGE_INTEGER FileSize;

    if(!GetFileSizeEx(handle, &FileSize))
        return FALSE;
    unsigned int dwBufferSize = FileSize.QuadPart;

    DWORD dw;
    unsigned char* buffer = new unsigned char[dwBufferSize];

    if(!ReadFile(handle, buffer, dwBufferSize, &dw, NULL))
        return FALSE;

    unsigned char IV[16] = {0x01, 0x23, 0x45, 0x67, 0x89, 0xab, 0xcd, 0xef, 0x01, 0x23, 0x45, 0x67,
0x89, 0xab, 0xcd, 0xef}; AESDecrypt(buffer, key, dwBufferSize, 32, IV, ECB_MODE);
    //AESDecrypt(buffer, key, dwBufferSize, 16, IV, CBC_MODE);
    //AESDecrypt(buffer, key, dwBufferSize, 16, IV, OFB_MODE);
    //AESDecrypt(buffer, key, dwBufferSize, 16, IV, CFB_MODE);
    //AESDecrypt(buffer, key, dwBufferSize, 16, IV, CTR_MODE);

    if(!WriteFile(handle2, buffer, dwBufferSize, &dw, NULL))
        return FALSE;

    CloseHandle(handle);
    CloseHandle(handle2);
    delete [] buffer;
    return true;
}
*/
