#ifndef __AES_INCLUDED__
#define __AES_INCLUDED__

//#include "TypeDef.h"

//#include "fp_types.h"
#ifdef __cplusplus
extern "C" {
#endif

enum { AES_ECB_MODE = 1, AES_CBC_MODE, AES_OFB_MODE, AES_CFB_MODE, AES_CTR_MODE };


void AES_AESEncrypt(unsigned char* buffer, unsigned char* key, unsigned int buffer_length,
                unsigned char key_length, unsigned char* IV, int mode);
void AES_AESDecrypt(unsigned char* buffer, unsigned char* key, unsigned int buffer_length,
                unsigned char key_length, unsigned char* IV, int mode);


#ifdef __cplusplus
}
#endif
#endif  //__AES_INCLUDED__
