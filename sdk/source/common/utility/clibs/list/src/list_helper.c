/**
 * @file list_helper.c
 * @author Jack Fan
 * @brief
 * @version 0.1
 * @date 2019-03-22
 *
 * @copyright Egis Tech Copyright (c) 2019
 *
 */
// #include "plat_log.h"
#include "list.h"
#define LOG_TAG "RBS-fingerdb"

list_t* list_rpush_list(list_t* self, list_t* to_push) {
    if (!self || !to_push) {
        // egislog_d("%s done 1", __func__);
        return NULL;
    }
    if (to_push->len == 0) {
        // egislog_d("%s done 2", __func__);
        return self;
    }
    int i;
    list_iterator_t* it = list_iterator_new(to_push, LIST_HEAD);
    for (i = 0; i < to_push->len; i++) {
        list_node_t* a = list_iterator_next(it);
        list_rpush(self, a);
    }
    list_iterator_destroy(it);

    return self;
}
