#ifndef __IMAGE_CUT_H__
#define __IMAGE_CUT_H__

#include <stdint.h>

#include "type_definition.h"

int image_crop(unsigned char* src_img, int src_width, int src_height, int des_width,
               int des_height);
int image_crop_xy(uint16_t* src_img, int src_width, int src_height, uint16_t* des_img, int des_width,
                   int des_height, int x0, int y0);
int compress_info(uint8_t* image, int width, int height, uint8_t* text);
int extract_info(uint8_t* image, int width, int height, uint8_t* text, int size);

int image_raw_normalization(uint16_t* raw_img, int width, int height, int hw_integ, int roi_length);
uint32_t checksum_calculation(uint8_t* data, uint32_t data_size);
void covert_endian16(uint8_t* img16, int width, int height);

#endif
