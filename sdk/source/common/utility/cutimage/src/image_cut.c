#include <stdlib.h>
#include <string.h>

#include "ImageProcessingLib.h"
#include "egis_definition.h"
#include "image_cut.h"
#include "plat_log.h"
#include "plat_mem.h"

#define LOG_TAG "RBS"

int image_crop(unsigned char* src_img, int src_width, int src_height, int des_width,
               int des_height) {
    if (src_img == NULL || des_width > src_width || des_height > src_height) {
        return EGIS_INCORRECT_PARAMETER;
    }

    int i;
    int width_point, height_point;
    width_point = (src_width - des_width) / 2;
    height_point = (src_height - des_height) / 2;
    unsigned char* des_img = (unsigned char*)plat_alloc(des_width * des_height);

    if (des_img == NULL) {
        return EGIS_OUT_OF_MEMORY;
    }

    unsigned char* src = src_img;
    unsigned char* des = des_img;

    for (i = height_point; i < (des_height + height_point); i++) {
        memcpy(des, src + i * src_width + width_point, des_width);
        des += des_width;
    }

    memset(src_img, 0x00, src_width * src_height);
    memcpy(src_img, des_img, des_width * des_height);

    plat_free(des_img);

    return EGIS_OK;
}

int image_crop_xy(uint16_t * src_img, int src_width, int src_height, uint16_t* des_img, int des_width,
               int des_height, int x0, int y0) {
    if (src_img == NULL || des_width > src_width || des_height > src_height) {
        return EGIS_INCORRECT_PARAMETER;
    }

    int i;

    if (des_img == NULL) {
        return EGIS_OUT_OF_MEMORY;
    }

    uint16_t* src = src_img;
    uint16_t* des = des_img;

    for (i = y0; i < (des_height + y0); i++) {
        memcpy(des, src + i * src_width + x0, des_width * sizeof(uint16_t));
        des += des_width;
    }

    return EGIS_OK;
}

int compress_info(uint8_t* image, int width, int height, uint8_t* text) {
    int i, x = 0, y = 0, ret = 0;

    do {
        uint8_t c = *text;
        for (i = 0; i < 8; i++, c >>= 1, x++) {
            image[x] = (image[x] & 0xFE) | (c & 0x01);
        }

        if (x > width - 8) {
            if (y == height - 1) break;
            y = height - 1;
            image += width * y;
            x = 0;
        }
        ret++;
    } while (*text++);
    return ret;
}

int extract_info(uint8_t* image, int width, int height, uint8_t* text, int size) {
    int i, x = 0, y = 0, ret = 0;
    char c;
    do {
        c = 0;
        for (i = 0; i < 8; i++, x++) {
            c |= (image[x] & 0x01) << i;
        }
        *text++ = c;
        if (x > width - 8) {
            if (y == height - 1) break;

            y = height - 1;
            image += width * y;
            x = 0;
        }
        if (c == 0) break;
        ret++;
    } while (ret < size);
    return ret;
}

#define HIST_SIZE 65536

static unsigned short* IPcreate_hist(unsigned short* img16, int size) {
    unsigned short* hist = plat_alloc(HIST_SIZE * sizeof(unsigned short));
    int i;
    if (hist == NULL) return NULL;
    for (i = 0; i < size; i++) {
        hist[img16[i] & 0xFFFF]++;
    }
    return hist;
}

static void IPmax_min_from_hist_ignore(unsigned short* hist, int* max, int* min, int cut_count,
                                       int ignore_value) {
    int i, c;
    if (max) *max = ignore_value;
    if (min) *min = 0;
    for (i = 0, c = cut_count; i < HIST_SIZE && c >= 0; i++) c -= hist[i];
    if (min) *min = i - 1;
    for (i = ignore_value, c = cut_count; i >= 0 && c >= 0; i--) c -= hist[i];
    if (max) *max = i + 1;
}

static int IPcount_max_min_ignore(unsigned short* img16, int size, int* max, int* min,
                                  int cut_ratio_x100, int ignore_value) {
    unsigned short* hist = IPcreate_hist(img16, size);
    if (hist != NULL) {
        IPmax_min_from_hist_ignore(hist, max, min, size * cut_ratio_x100 / 100, ignore_value);
        plat_free(hist);
        return EGIS_OK;
    }
    return EGIS_COMMAND_FAIL;
}

static void IPre_normalize(unsigned short* img16, int size, int max, int min, int hw_integ) {
    int value, range = max - min;
    int INTEGRATION_MAX = ((1 << 11) * hw_integ - 1);
    int EV_TARGET_MAX = (1600 * hw_integ);

    for (int i = 0; i < size; i++) {
        value = ((img16[i] - min) * (EV_TARGET_MAX - min) + range / 2) / range + min;
        if (value > INTEGRATION_MAX) {
            egislog_e("%s, %d -> %d", __func__, value, INTEGRATION_MAX);
            value = INTEGRATION_MAX;
        }
        img16[i] = value;
    }
}

static int image_raw_normalization_1(uint16_t* raw_img, int width, int height, int hw_integ,
                                     int roi_length) {
    const int cut_ratio_x100 = 1;
    const int target_max = 1600;
    int i;
    int max = 0, min = 0;
    int size = width * height;
    if (roi_length < 1 || roi_length > width) {
        return EGIS_OK;
    }

    int* img32bit = plat_alloc(size * sizeof(int));
    RBS_CHECK_IF_NULL(img32bit, EGIS_OUT_OF_MEMORY);

    uint16_t* p_img16 = raw_img;
    int* p_img32 = img32bit;
    for (i = 0; i < size; i++) {
        *p_img32++ = *p_img16++;
    }
    int y0 = (height - roi_length) / 2, x0 = (width - roi_length) / 2;
    unsigned int ratio_2e16;
    IPcount_max_min_ROI(img32bit, width, height, &max, &min, cut_ratio_x100, x0, y0, roi_length,
                        roi_length);
    ex_log(LOG_DEBUG, "%s (%d) roi=%d, max %d min %d", __func__, hw_integ, roi_length, max, min);
    if (max < 1 || hw_integ < 1 || max < hw_integ) {
        ex_log(LOG_ERROR, "%s, wrong max %d or hw_integ %d", __func__, max, hw_integ);
        PLAT_FREE(img32bit);
        return EGIS_COMMAND_FAIL;
    }
    max /= hw_integ;
    ratio_2e16 = (target_max * 65536) / max;

    int v;
    for (i = 0; i < size; i++) {
        v = ((unsigned int)img32bit[i] * ratio_2e16 + 32768) >> 16;
        raw_img[i] = v > 0xFFFF ? 0xFFFF : v;
    }
    PLAT_FREE(img32bit);
    return EGIS_OK;
}

#define EV_SKIP_LT_VALUE (1900)
int image_raw_normalization(uint16_t* raw_img, int width, int height, int hw_integ,
                            int roi_length) {
#ifdef G3PLUS_MATCHER
#if 1
    return image_raw_normalization_1(raw_img, width, height, hw_integ, roi_length);
#else
    int ret;
    int size = width * height;
    int max, min;
    RBS_CHECK_IF_NULL(raw_img, EGIS_OUT_OF_MEMORY);

    ret = IPcount_max_min_ignore(raw_img, size, &max, &min, 1, EV_SKIP_LT_VALUE * hw_integ);
    if (ret != EGIS_OK) {
        egislog_e("%s, IPcount_max_min_ignore failed", __func__);
        return ret;
    }
    // egislog_d("%s, max %d, min %d", __func__, max, min);
    egislog_d("%s, max %d, min %d -> 0", __func__, max, min);
    min = 0;
    IPre_normalize(raw_img, size, max, min, hw_integ);
    return EGIS_OK;
#endif

#else
    egislog_d("%s is skipped.", __func__);
    return EGIS_OK;
#endif
}

uint32_t checksum_calculation(uint8_t* data, uint32_t data_size) {
    uint32_t i;
    uint32_t checksum = -1;
    if (data == NULL || data_size <= 0) {
        return checksum;
    }

    checksum = 0;
    for (i = 0; i < data_size; i++) {
        checksum += data[i];
    }

    // egislog_d("checksum=%04X,  %02X %02X %02X %02X %02X %02X %02X %02X.", checksum,
    // 	data[0], data[1], data[2], data[3], data[4], data[5], data[6], data[7]);
    return checksum;
}

void covert_endian16(uint8_t* img16, int width, int height) {
    int i;
    uint8_t tmp;
    uint8_t* p_buf = img16;
    int size = width * height;
    for (i = 0; i < size; i++) {
        tmp = *p_buf;
        *p_buf = p_buf[1];
        p_buf[1] = tmp;
        p_buf += 2;
    }
}