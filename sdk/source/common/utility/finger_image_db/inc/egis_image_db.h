#ifndef __EGIS_IMAGE_DB_HEADER__
#define __EGIS_IMAGE_DB_HEADER__

#include "clibs/list/src/list.h"
#include "type_definition.h"

void merge_PQ_folder(list_t* finger_folder_list, list_t** finger_img_enroll_list,
                     list_t** finger_img_verify_list, int* valid_count);

BOOL is_enroll_redundant(const char* enroll_filepath);
int db_parse_cx_cy(const char* filepath, int* cx, int* cy);

#endif
