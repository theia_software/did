#ifndef __FINGER_IMAGE_DB_HEADER__
#define __FINGER_IMAGE_DB_HEADER__

#include "type_definition.h"

enum FIMAGE_DB_TYPE {
    DB_EVTOOL,
    DB_PROJ_HUA,
    DB_SIMEPLE,
    // L1
    //  ├── 000.raw
    //  ├── 001.raw
    //  ...
    //  └── 059.raw
    // L2
    //  ├── 000.raw
    //  ├── 001.raw
    //  ...
    //  └── 059.raw
};

enum FIMAGE_TYPE { FIMAGE_ENROLL, FIMAGE_VERIFY, FIMAGE_SIMPLEDB };

int fimage_db_create(const char* root_path, enum FIMAGE_DB_TYPE db_type);
void fimage_db_destroy();
BOOL fimage_db_is_created();

int fimage_db_get_num_finger();
int fimage_db_get_num_fingerprint(enum FIMAGE_TYPE fingerprintType, int fingerIdx);

int fimage_db_get_path(enum FIMAGE_TYPE fingerprintType, int fingerIdx, int fingerprint_index,
                       char* imagePath);

int fimage_db_get_try_image_count(const char* main_path);
int fimage_db_get_tryfolder_image(const char* main_path, int tryfolder_index, char* path);

int fimage_db_get_path_start(enum FIMAGE_TYPE fingerprintType, int fingerIdx, int fingerprint_index,
                             char* imagePath);
int fimage_db_get_path_next(enum FIMAGE_TYPE fingerprintType, int fingerIdx, int fingerprint_index,
                            char* imagePath);

#define FIMAGE_DB_OK 0
#define FIMAGE_DB_NO_NEXT 9
#define FIMAGE_DB_TYPE_DIFFERENT 20

#define FIMAGE_DB_IS_EMPTY -1
#define FIMAGE_DB_WRONG_PARAMETER -2

#define FIMAGE_DB_ERROR -10

void extract_file_name(const char* path, char* file_name);

#endif