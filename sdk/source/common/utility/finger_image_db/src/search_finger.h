#ifndef __FOLDER_SEARCH_HEADER__
#define __FOLDER_SEARCH_HEADER__

/* limits.h defines "PATH_MAX". */
#include <limits.h>

#include "clibs/list/src/list.h"

#ifdef _MSC_VER
#ifndef PATH_MAX
#define PATH_MAX MAX_PATH
#endif
#endif

typedef struct _file_path {
    char path[PATH_MAX];
} file_path;

void free_file_path(void* val);
void free_file_list(void* val);

int search_finger_folders(const char* dir_name, int level, int* total_num, list_t* mylist);
int search_finger_images_in_sub_folder(const char* dir_name, list_t* output_list,
                                       BOOL* single_dimension);
int collect_sub_folder_file(const char* src_dir, BOOL is_folder, list_t* mylist);

#define SEARCHF_OK 0

#ifdef _WINDOWS
#define OS_SEP '\\'
#else
#define OS_SEP '/'
#endif

#endif
