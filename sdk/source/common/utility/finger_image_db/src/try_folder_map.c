#include <errno.h>
#include <limits.h>
#include <regex.h>

#include "clibs/list/src/list.h"
#include "egis_definition.h"
#include "hashmap.h"
#include "plat_log.h"
#include "plat_mem.h"
#include "search_finger.h"
#include "try_folder_map.h"

#define LOG_TAG "RBS-map"
#ifndef _WINDOWS
#include <dirent.h>
#else
#include <direct.h>
#include <io.h>
#endif

static map_t g_tryf_map = NULL;

int tryf_map_create() {
    egislog_d("%s", __func__);
    tryf_map_destroy();
    g_tryf_map = hashmap_new();
    return 0;
}

static void _print_list(const char* title, list_t* my_list) {
    int count = my_list->len;
    list_iterator_t* it = list_iterator_new(my_list, LIST_HEAD);
    while (it != NULL && count > 0) {
        list_node_t* a = list_iterator_next(it);
        if (a != NULL) {
            egislog_d("[%d]: %s", count, a->val);
            count--;
        }
    }
    list_iterator_destroy(it);
}

void List_destroy(any_t data) {
    list_destroy(data);
}

void tryf_map_destroy() {
    if (g_tryf_map == NULL) return;

    egislog_d("%s", __func__);
    if (g_tryf_map != NULL) {
        hashmap_free_ex(g_tryf_map, List_destroy);
        g_tryf_map = NULL;
    }
}

static int _search_target_folder(const char* dir_name, int level, const char* key, char* path,
                                 int path_size) {
    int i;
    if (path == NULL || path_size < 1) {
        egislog_e("path is NULL !!!");
        return -1;
    }
    if (level == 0) {
        strcpy(path, "");
    }
#ifndef _WINDOWS
    DIR* d;
    /* Open the directory specified by "dir_name". */
    d = opendir(dir_name);

    /* Check it was opened. */
    if (!d) {
        egislog_e("Cannot open directory '%s': %s", dir_name, strerror(errno));
        return -2;
    }
#else
    long Handle;
    struct _finddata_t file_info;
    char find_file_type[MAX_PATH];

    memset(find_file_type, 0, MAX_PATH);
    sprintf(find_file_type, "%s/*", dir_name);

    if ((Handle = _findfirst(find_file_type, &file_info)) == -1L) return -2;
#endif
    while (1) {
        const char* d_name;
        BOOL bIsDir = FALSE;
        BOOL foundValidItem = FALSE;
#ifndef _WINDOWS
        struct dirent* entry;
        /* "Readdir" gets subsequent entries from "d". */
        entry = readdir(d);
        if (!entry) {
            /* There are no more entries in this directory, so break
               out of the while loop. */
            break;
        }
        d_name = entry->d_name;
        if (entry->d_type & DT_DIR) bIsDir = TRUE;
#else
        if (_findnext(Handle, &file_info) != 0) break;

        d_name = file_info.name;
        if (file_info.attrib & _A_SUBDIR) bIsDir = TRUE;
#endif
        if (bIsDir) {
            // egislog_d("JackDebug %s, %s", d_name, key);
            const char* match_part = strstr(d_name, key);
            if (match_part != NULL) {
                egislog_d("JackDebug (%s) %s, %s", match_part, d_name, key);
                snprintf(path, path_size, "%s/%s", dir_name, d_name);
                break;
            }
            // egislog_d("JackDebug (X) %s, %s", d_name, key);
            if (strcmp(d_name, "..") != 0 && strcmp(d_name, ".") != 0) {
                int path_length;
                char next_folder[PATH_MAX];

                path_length = snprintf(next_folder, PATH_MAX, "%s/%s", dir_name, d_name);
                // printf ("%s\n", next_folder);
                if (path_length >= PATH_MAX) {
                    egislog_e(
                        "Path length has got too "
                        "long.");
                    return -100;
                }
                /* Recursively call "list_dir" with the new next_folder. */
                _search_target_folder(next_folder, level + 1, key, path, path_size);
            }
            if (strlen(path) > 0) {
                break;
            }
        }
    }
    /* After going through all the entries, close the directory. */
#ifndef _WINDOWS
    if (closedir(d)) {
        egislog_e("Could not close '%s': %s\n", dir_name, strerror(errno));
        return -2;
    }
#else
    _findclose(Handle);
#endif
    return 0;
}

static void extract_key_from_main_filename(const char* main_filename, char* key, int key_size) {
#define nmatch 1
    int i;
    int data_length = 0;
    int regex_flag = REG_EXTENDED | REG_ICASE;
    regex_t preg;
    char* pattern = "[0-9]{12,13}";
    regmatch_t pmatch[nmatch];

    if (regcomp(&preg, pattern, regex_flag) != 0) {
        egislog_e("regexp comp error.");
    }
    // egislog_v("main_filename %s\n", main_filename);
    memset(key, 0, key_size);
    if (regexec(&preg, main_filename, nmatch, pmatch, 0) == 0) {
        for (i = 0; i < nmatch && pmatch[i].rm_so >= 0; ++i) {
            data_length = pmatch[i].rm_eo - pmatch[i].rm_so;
            strncpy(key, main_filename + pmatch[i].rm_so,
                    key_size < data_length ? key_size : data_length);
            // egislog_v("JackDebug %s", key);
        }
    } else {
        egislog_e("key cannot be extracted %s", main_filename);
        strcpy(key, "");
    }
    regfree(&preg);
}

#define MAX_KEY_SIZE 256
int tryf_map_add(const char* main_filename, const char* root_folder) {
    char key[MAX_KEY_SIZE] = {0};
    char path[PATH_MAX] = {0};
    extract_key_from_main_filename(main_filename, key, MAX_KEY_SIZE);
    if (strlen(key) <= 0) {
        return TRYF_ADD_NO_TRY_FOLDER;
    }
    _search_target_folder(root_folder, 0, key, path, PATH_MAX);
    if (strlen(path) > 0) {
        egislog_d("_search_target_folder key=%s, path=%s", key, path);

        char* map_key = plat_alloc(MAX_KEY_SIZE);
        // char * map_value = plat_alloc(PATH_MAX);
        list_t* tryfile_list = list_new();
        tryfile_list->free = free_file_path;
        collect_sub_folder_file(path, FALSE, tryfile_list);
        egislog_d("_search_target_folder tryfile_list size %d", tryfile_list->len);
        if (tryfile_list->len > 0) {
            _print_list(key, tryfile_list);
            strncpy(map_key, key, MAX_KEY_SIZE);
            // strncpy(map_value)
            hashmap_put(g_tryf_map, map_key, tryfile_list);
            return TRYF_ADD_SUCCESS;
        }
    }
    return TRYF_ADD_NO_TRY_FOLDER;
}

static int _get_try_image_list(const char* filepath, list_t** image_list) {
    char key[MAX_KEY_SIZE] = {0};
    extract_key_from_main_filename(filepath, key, MAX_KEY_SIZE);
    if (strlen(key) < 1) {
        egislog_e("%s, failed to extract key %s", __func__, filepath);
        return 0;
    }
    egislog_d("%s, key=%s, filepath=%s", __func__, key, filepath);
    int ret = hashmap_get(g_tryf_map, key, (any_t*)image_list);
    if (ret != MAP_OK) {
        egislog_d("%s, map_get missing %d", __func__, ret);
        return 0;
    }
    return (*image_list)->len;
}

int tryf_map_get_try_image_count(const char* filepath) {
    list_t* try_image_list = NULL;
    int count = _get_try_image_list(filepath, &try_image_list);
    egislog_d("%s, count=%d", __func__, count);
    return count;
}

int tryf_map_get_try_image_path(const char* filepath, int tryfolder_idx, char* path) {
    list_t* try_image_list = NULL;
    int count = _get_try_image_list(filepath, &try_image_list);
    if (tryfolder_idx < 0 || tryfolder_idx >= count) {
        strcpy(path, "");
        return TRYF_GET_PATH_NONE;
    }
    list_node_t* node = list_at(try_image_list, tryfolder_idx);
    strncpy(path, node->val, PATH_MAX);
    return TRYF_GET_PATH_SUCCESS;
}