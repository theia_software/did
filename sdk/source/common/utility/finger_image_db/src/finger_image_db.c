/* limits.h defines "PATH_MAX". */
#include "../inc/finger_image_db.h"
#include <limits.h>

#include "../inc/finger_image_db.h"
#include "clibs/list/src/list.h"
#include "egis_definition.h"
#include "egis_image_db.h"
#include "plat_log.h"
#include "plat_mem.h"
#include "search_finger.h"

#ifdef __ET0XX__
#include "try_folder_map.h"
extern char g_bin_folder[PATH_MAX];
extern char g_try_folder[PATH_MAX];
#endif
#define LOG_TAG "RBS-fingerdb"

static char g_root_path[PATH_MAX] = {0};
static int _get_finger_image_list(list_t* finger_folder_list, list_t** finger_img_list,
                                  const char* sub_folder, BOOL* single_dimension) {
    unsigned int i;
    char valid_folder_path[PATH_MAX];

    list_iterator_t* it = list_iterator_new(finger_folder_list, LIST_HEAD);
    *single_dimension = TRUE;
    for (i = 0; i < finger_folder_list->len; i++) {
        list_node_t* a = list_iterator_next(it);
        if (a == NULL) {
            egislog_e("unexpected node");
            break;
        }
        finger_img_list[i] = list_new();
        finger_img_list[i]->free = free_file_path;
        snprintf(valid_folder_path, PATH_MAX, "%s/%s", a->val, sub_folder);
        BOOL curr_single_dimension;
        search_finger_images_in_sub_folder(valid_folder_path, finger_img_list[i],
                                           &curr_single_dimension);
        if (i > 0 && *single_dimension != curr_single_dimension) {
            egislog_e("DB has fingers in different structure!");
        }
        // printf("==[%d] %s, len=%d\n", i, valid_folder_path, finger_img_list[i]->len);
    }
    list_iterator_destroy(it);
    return 0;
}

static void _print_out_image_list(list_t** finger_img_list, int total_num_finger,
                                  BOOL single_dimension) {
    int i, j, k;
    // Log out all the path
    for (i = 0; i < total_num_finger; i++) {
        egislog_d("[%d] All fingerprints", i);
        list_t* mylist = finger_img_list[i];
        if (mylist == NULL) {
            egislog_e("[%d] All fingerprints: but list is NULL", i);
            continue;
        }
        // printf("List[%d] %d\n", i, mylist->len);
        list_iterator_t* it = list_iterator_new(mylist, LIST_HEAD);
        j = 0;
        while (it != NULL) {
            list_node_t* a = list_iterator_next(it);
            if (a) {
                if (single_dimension) {
                    egislog_v(" -> [%d][%d] %s", i, j, a->val);
                    // printf("List[%d][%d] %s\n", i, j, a->val);
#ifdef __ET0XX__
                    char tryf_path[PATH_MAX] = {0};
                    if (strlen(g_try_folder))
                        snprintf(tryf_path, PATH_MAX, "%s/%s", g_root_path, g_try_folder);
                    else
                        strcpy(tryf_path, g_root_path);
                    egislog_d("tryf_path=%s", tryf_path);
                    tryf_map_add(a->val, tryf_path);
#endif
                } else {
                    egislog_d("[%d][%d] total=%d", i, j, ((list_t*)a->val)->len);
                    list_iterator_t* sub_it = list_iterator_new((list_t*)a->val, LIST_HEAD);
                    k = 0;
                    while (sub_it != NULL) {
                        list_node_t* b = list_iterator_next(sub_it);
                        if (b)
                            egislog_v(
                                "-> [%d][%d][%d] "
                                "%s",
                                i, j, k, b->val);
                        else
                            break;

                        k++;
                    }
                    list_iterator_destroy(sub_it);
                }
            } else
                break;

            j++;
        }
        list_iterator_destroy(it);
    }
}

static list_t* g_finger_folder_list = NULL;
static list_t** g_finger_img_enroll_list = NULL;
static list_t** g_finger_img_verify_list = NULL;
static BOOL g_single_dimension_enroll;
static BOOL g_single_dimension_verify;
static int g_total_num_finger = 0;
static int g_total_finger_folder = 0;
enum FIMAGE_DB_TYPE g_db_type = DB_EVTOOL;

int fimage_db_create(const char* root_path, enum FIMAGE_DB_TYPE db_type) {
    int total_num = 0;
    int retval;
    fimage_db_destroy();
    g_db_type = db_type;

    g_finger_folder_list = list_new();
    g_finger_folder_list->free = free_file_path;
    char db_bin_path[PATH_MAX] = {0};
    strcpy(db_bin_path, root_path);
#ifdef __ET0XX__
    if (strlen(g_bin_folder)) snprintf(db_bin_path, PATH_MAX, "%s/%s", root_path, g_bin_folder);
#endif
    egislog_d("db_bin_path=%s", db_bin_path);
    search_finger_folders(db_bin_path, 0, &total_num, g_finger_folder_list);
    egislog_d("total_num = %d", total_num);
    g_total_finger_folder = total_num;
    g_total_num_finger = total_num;

    if (g_total_finger_folder <= 0) {
        egislog_e("DB is empty!");
        return FIMAGE_DB_IS_EMPTY;
    }
    strcpy(g_root_path, root_path);
    switch (db_type) {
        case DB_EVTOOL:
        case DB_PROJ_HUA:
#ifdef __ET0XX__
            retval = tryf_map_create();
            if (retval != EGIS_OK) {
                egislog_e("tryf_map_create failed");
            }
#endif
            egislog_i("g_finger_img_enroll_list");
            g_finger_img_enroll_list = plat_alloc(g_total_finger_folder * sizeof(list_t*));
            _get_finger_image_list(g_finger_folder_list, g_finger_img_enroll_list, "enroll",
                                   &g_single_dimension_enroll);

            egislog_i("g_finger_img_verify_list");
            g_finger_img_verify_list = plat_alloc(g_total_finger_folder * sizeof(list_t*));
            _get_finger_image_list(g_finger_folder_list, g_finger_img_verify_list, "verify",
                                   &g_single_dimension_verify);

            merge_PQ_folder(g_finger_folder_list, g_finger_img_enroll_list,
                            g_finger_img_verify_list, &g_total_num_finger);

            _print_out_image_list(g_finger_img_enroll_list, g_total_num_finger,
                                  g_single_dimension_enroll);
            _print_out_image_list(g_finger_img_verify_list, g_total_num_finger,
                                  g_single_dimension_verify);

            egislog_d("FIMAGE_DB_OK");
            break;
        case DB_SIMEPLE:

            egislog_i("g_finger_img_verify_list");
            g_finger_img_verify_list = plat_alloc(g_total_finger_folder * sizeof(list_t*));
            _get_finger_image_list(g_finger_folder_list, g_finger_img_verify_list, "",
                                   &g_single_dimension_verify);

            _print_out_image_list(g_finger_img_verify_list, g_total_num_finger,
                                  g_single_dimension_verify);

            break;
    }
    return FIMAGE_DB_OK;
}

void fimage_db_destroy() {
    int i;
    strcpy(g_root_path, "");

    if (g_finger_folder_list) {
        list_destroy(g_finger_folder_list);
        g_finger_folder_list = NULL;
    }

    if (g_finger_img_enroll_list) {
        for (i = 0; i < g_total_finger_folder; i++) {
            if (g_finger_img_enroll_list[i]) {
                list_destroy(g_finger_img_enroll_list[i]);
            }
        }
        plat_free(g_finger_img_enroll_list);
        g_finger_img_enroll_list = NULL;
    }
    if (g_finger_img_verify_list) {
        for (i = 0; i < g_total_finger_folder; i++) {
            if (g_finger_img_verify_list[i]) {
                list_destroy(g_finger_img_verify_list[i]);
            }
        }
        plat_free(g_finger_img_verify_list);
        g_finger_img_verify_list = NULL;
    }
    g_total_finger_folder = 0;
    g_total_num_finger = 0;
#ifdef __ET0XX__
    tryf_map_destroy();
#endif
}

BOOL fimage_db_is_created() {
    BOOL created = g_finger_folder_list != NULL ? TRUE : FALSE;
    egislog_i("%s, %d", __func__, created);
    return created;
}

int fimage_db_get_num_finger() {
    return g_total_num_finger;
}
int fimage_db_get_num_fingerprint(enum FIMAGE_TYPE fingerprintType, int fingerIdx) {
    if (fingerIdx < 0 || fingerIdx >= g_total_num_finger) return 0;

    switch (fingerprintType) {
        case FIMAGE_ENROLL:
            return g_finger_img_enroll_list[fingerIdx]->len;
        case FIMAGE_VERIFY:
            return g_finger_img_verify_list[fingerIdx]->len;
        case FIMAGE_SIMPLEDB:
            return g_finger_img_verify_list[fingerIdx]->len;
        default:
            egislog_e("unexpected fingerprintType");
    }
    return 0;
}

static list_node_t* _get_my_list_node(enum FIMAGE_TYPE fingerprintType, int fingerIdx,
                                      int fingerprint_index) {
    list_node_t* node = NULL;
    switch (fingerprintType) {
        case FIMAGE_ENROLL:
            node = list_at(g_finger_img_enroll_list[fingerIdx], fingerprint_index);
            break;
        case FIMAGE_VERIFY:
            node = list_at(g_finger_img_verify_list[fingerIdx], fingerprint_index);
            break;
        case FIMAGE_SIMPLEDB:
            node = list_at(g_finger_img_verify_list[fingerIdx], fingerprint_index);
            break;
        default:
            egislog_e("unexpected fingerprintType");
    }
    return node;
}

int fimage_db_get_path(enum FIMAGE_TYPE fingerprintType, int fingerIdx, int fingerprint_index,
                       char* imagePath) {
    if (imagePath == NULL) {
        egislog_e("imagePath is NULL");
        return FIMAGE_DB_WRONG_PARAMETER;
    }

    if (FIMAGE_SIMPLEDB != fingerprintType) {
        BOOL single_dimension = (fingerprintType == FIMAGE_ENROLL) ? g_single_dimension_enroll
                                                                   : g_single_dimension_verify;
        if (!single_dimension) {
            egislog_d("Expect g_single_dimension is TRUE");
            strcpy(imagePath, "");
            return FIMAGE_DB_TYPE_DIFFERENT;
        }
    }

    list_node_t* node = _get_my_list_node(fingerprintType, fingerIdx, fingerprint_index);
    if (node == NULL || node->val == NULL) {
        egislog_e("DB is wrong (1)");
        return FIMAGE_DB_ERROR;
    }
    strcpy(imagePath, (char*)(node->val));
    return FIMAGE_DB_OK;
}
#ifdef __ET0XX__
int fimage_db_get_try_image_count(const char* main_path) {
    return tryf_map_get_try_image_count(main_path);
}

int fimage_db_get_tryfolder_image(const char* main_path, int tryfolder_index, char* path) {
    return tryf_map_get_try_image_path(main_path, tryfolder_index, path);
}
#endif

static list_iterator_t* g_get_path_it = NULL;
static int _fimage_db_get_path_next(enum FIMAGE_TYPE fingerprintType, int fingerIdx,
                                    int fingerprint_index, BOOL from_head, char* imagePath) {
    BOOL single_dimension =
        (fingerprintType == FIMAGE_ENROLL) ? g_single_dimension_enroll : g_single_dimension_verify;
    if (single_dimension) {
        egislog_e("Expect g_single_dimension is FALSE");
        return FIMAGE_DB_ERROR;
    }

    if (imagePath == NULL) {
        egislog_e("imagePath is NULL");
        return FIMAGE_DB_WRONG_PARAMETER;
    }
    list_node_t* node = _get_my_list_node(fingerprintType, fingerIdx, fingerprint_index);
    if (node == NULL || node->val == NULL) {
        egislog_e("DB is wrong (start1)");
        return FIMAGE_DB_ERROR;
    }

    list_node_t* curr_node = NULL;
    if (from_head) {
        if (g_get_path_it != NULL) list_iterator_destroy(g_get_path_it);

        g_get_path_it = list_iterator_new((list_t*)node->val, LIST_HEAD);
    }
    if (g_get_path_it) {
        curr_node = list_iterator_next(g_get_path_it);
    }
    if (curr_node == NULL || curr_node->val == NULL) {
        if (from_head) {
            egislog_e("DB is wrong (start2)");
            return FIMAGE_DB_ERROR;
        } else {
            list_iterator_destroy(g_get_path_it);
            g_get_path_it = NULL;
            return FIMAGE_DB_NO_NEXT;
        }
    }
    strcpy(imagePath, (char*)(curr_node->val));
    return FIMAGE_DB_OK;
}

int fimage_db_get_path_start(enum FIMAGE_TYPE fingerprintType, int fingerIdx, int fingerprint_index,
                             char* imagePath) {
    BOOL from_head = TRUE;
    return _fimage_db_get_path_next(fingerprintType, fingerIdx, fingerprint_index, from_head,
                                    imagePath);
}

int fimage_db_get_path_next(enum FIMAGE_TYPE fingerprintType, int fingerIdx, int fingerprint_index,
                            char* imagePath) {
    BOOL from_head = FALSE;
    return _fimage_db_get_path_next(fingerprintType, fingerIdx, fingerprint_index, from_head,
                                    imagePath);
}

void extract_file_name(const char* path, char* file_name) {
    char* name = strrchr(path, (int)OS_SEP);
    if (name == NULL) {
        strncpy(file_name, path, PATH_MAX);
        return;
    }
    strncpy(file_name, name, strlen(name));
}
