#ifndef __TRY_FOLDER_MAP_HEADER__
#define __TRY_FOLDER_MAP_HEADER__

#define TRYF_ADD_EXTRACT_KEY_FAIL -1
#define TRYF_ADD_SUCCESS 0
#define TRYF_ADD_NO_TRY_FOLDER 1

#define TRYF_GET_PATH_SUCCESS 0
#define TRYF_GET_PATH_NONE -1

int tryf_map_create();
void tryf_map_destroy();
int tryf_map_add(const char* main_filename, const char* root_folder);

int tryf_map_get_try_image_count(const char* filepath);
int tryf_map_get_try_image_path(const char* filepath, int tryfolder_idx, char* path);

#endif
