#include <errno.h>
#include <stdio.h>
#include <string.h>
/* "readdir" etc. are defined here. */
#include "finger_image_db.h"
#include "plat_log.h"
#include "plat_mem.h"
#include "search_finger.h"
#include "type_definition.h"

#ifndef _WINDOWS
#include <dirent.h>
#else
#include <direct.h>
#include <io.h>
#endif
#define LOG_TAG "RBS-fingerdb"

#define VALID_EXT_SIZE 3
char* validExt[VALID_EXT_SIZE] = {".raw", ".RAW", ".bin"};
#define VALID_FOLDER_SIZE 2
char* validFolder[VALID_FOLDER_SIZE] = {"enroll", "verify"};
extern enum FIMAGE_DB_TYPE g_db_type;

char validEvToolFolder_st[] = {"st"};
char validEvToolFolder_45d[] = {"45d"};
char validEvToolFolder_90d[] = {"90d"};

char validSimepleDBFolder[] = {"image"};

#define ENDS_WITH(a, b, n) (strlen(a) > n && !strcmp(a + strlen(a) - n, b))
static int validExtIndex = 0;
static int isValidExt(const char* filename) {
    int i;
    for (i = 0; i < VALID_EXT_SIZE; i++) {
        if (ENDS_WITH(filename, validExt[i], 4)) {
            validExtIndex = i;
            return 1;
        }
    }
    return 0;
}
#ifndef _WINDOWS
static int _alpha_sort(const struct dirent** a, const struct dirent** b) {
    return strcmp((*a)->d_name, (*b)->d_name);
}
#endif

static int* g_num_enroll_fingerprint = NULL;
static int* g_num_verify_fingerprint = NULL;

static int _search_finger_folders(const char* dir_name, int level, int* total_num, list_t* mylist) {
    int i;
    if (total_num == NULL) {
        egislog_e("total_num is NULL !!!");
        return -1;
    }
    if (level == 0) {
        *total_num = 0;
    }
#ifndef _WINDOWS
    DIR* d;
    /* Open the directory specified by "dir_name". */
    struct dirent** namelist;
    int n = scandir(dir_name, &namelist, NULL, _alpha_sort);
    if (n < 0) {
        egislog_e("ERROR %d: Could not open directory %s", errno, dir_name);
        return -2;
    }
#else
    long Handle;
    struct _finddata_t file_info;
    char find_file_type[MAX_PATH];

    memset(find_file_type, 0, MAX_PATH);
    sprintf(find_file_type, "%s/*", dir_name);

    if ((Handle = _findfirst(find_file_type, &file_info)) == -1L) return -2;
#endif
    int idx = -1;
    while (1) {
        const char* d_name;
        BOOL bIsDir = FALSE;
        BOOL foundValidItem = FALSE;
#ifndef _WINDOWS
        idx++;
        if (idx >= n) break;

        if (namelist[idx]->d_name[0] == '.') continue;

        d_name = namelist[idx]->d_name;
        if (namelist[idx]->d_type & DT_DIR) {
            bIsDir = TRUE;
        }
#else
        if (_findnext(Handle, &file_info) != 0) break;

        d_name = file_info.name;
        if (file_info.attrib & _A_SUBDIR) bIsDir = TRUE;
#endif
        if (bIsDir) {
            for (i = 0; i < VALID_FOLDER_SIZE; i++) {
                if (DB_SIMEPLE == g_db_type) {
                    foundValidItem = TRUE;
                    break;
                }
                if (strcmp(d_name, validFolder[i]) == 0) {
                    foundValidItem = TRUE;
                    break;
                }
            }
            if (!foundValidItem) {
                if (strcmp(d_name, "..") != 0 && strcmp(d_name, ".") != 0) {
                    int path_length;
                    char path[PATH_MAX];

                    path_length = snprintf(path, PATH_MAX, "%s/%s", dir_name, d_name);
                    // printf ("%s\n", path);
                    if (path_length >= PATH_MAX) {
                        egislog_e(
                            "Path length has got too "
                            "long.");
                        return -100;
                    }
                    /* Recursively call "list_dir" with the
                     * new path. */
                    _search_finger_folders(path, level + 1, total_num, mylist);
                }
            }
        } else {
            // foundValidItem = isValidExt(d_name);
        }

        if (foundValidItem) {
            egislog_v("valid [%d][%d] %s  (%s)", idx, *total_num, dir_name, d_name);
            if (mylist != NULL) {
                list_node_t* node_t = list_node_new(plat_alloc(sizeof(file_path)));
                snprintf(node_t->val, PATH_MAX, "%s", dir_name);
                list_rpush(mylist, node_t);
            }
            *total_num = *total_num + 1;

            break;
        }
    }
    /* After going through all the entries, close the directory. */
#ifndef _WINDOWS
    for (idx = 0; idx < n; idx++) {
        plat_free(namelist[idx]);
    }
    plat_free(namelist);
#else
    _findclose(Handle);
#endif
    return 0;
}

static int _collect_sub_folder_file(const char* src_dir, BOOL is_folder, list_t* mylist) {
    int i = -1;
    BOOL matched;
    // find name list

#ifndef _WINDOWS
    struct dirent** namelist;
    int n = scandir(src_dir, &namelist, NULL, _alpha_sort);
    if (n < 0) {
        egislog_e("ERROR %d: Could not open directory %s", errno, src_dir);
        return -1;
    }
#else
    long Handle;
    struct _finddata_t file_info;
    char find_file_type[MAX_PATH];

    memset(find_file_type, 0, MAX_PATH);
    sprintf(find_file_type, "%s/*", src_dir);

    if ((Handle = _findfirst(find_file_type, &file_info)) == -1L) return -2;
#endif

    while (1) {
        const char* d_name;
        BOOL bIsDir = FALSE;

#ifndef _WINDOWS
        i++;
        if (i >= n) break;

        if (namelist[i]->d_name[0] == '.') continue;

        d_name = namelist[i]->d_name;
        if (namelist[i]->d_type & DT_DIR) bIsDir = TRUE;
#else
        if (_findnext(Handle, &file_info) != 0) break;

        d_name = file_info.name;
        if (file_info.attrib & _A_SUBDIR) bIsDir = TRUE;
#endif

        if (is_folder == bIsDir) {
            matched = TRUE;
        } else {
            matched = FALSE;
        }

        if (matched && !is_folder) {
            matched = isValidExt(d_name);
        }

        if (matched) {
            list_node_t* node_t = list_node_new(plat_alloc(sizeof(file_path)));
            snprintf(node_t->val, PATH_MAX, "%s/%s", src_dir, d_name);
            list_rpush(mylist, node_t);

            egislog_v("-- collecting %s [][%d] %s. total=%d", is_folder ? "folder" : "file", i,
                      node_t->val, mylist->len);
        }
    }

#ifndef _WINDOWS
    for (i = 0; i < n; i++) {
        plat_free(namelist[i]);
    }

    plat_free(namelist);
#else
    _findclose(Handle);
#endif
    return 0;
}

int collect_sub_folder_file(const char* src_dir, BOOL is_folder, list_t* mylist) {
    return _collect_sub_folder_file(src_dir, is_folder, mylist);
}

int search_finger_folders(const char* dir_name, int level, int* total_num, list_t* mylist) {
    return _search_finger_folders(dir_name, 0, total_num, mylist);
}

int search_finger_images_in_sub_folder(const char* dir_name, list_t* output_list,
                                       BOOL* single_dimension) {
    int j;
    list_iterator_t* it;
    list_node_t* a;
    BOOL correctFolder = FALSE;

    list_t* sub_folder_list = list_new();
    sub_folder_list->free = free_file_path;

    // Collecting sub folders (e.g. "st", "45d", "90d")
    _collect_sub_folder_file(dir_name, TRUE, sub_folder_list);

    if (sub_folder_list->len == 0) {
        list_destroy(sub_folder_list);
        return SEARCHF_OK;
    }

    // Check whether the DB is single_dimension or not
    *single_dimension = FALSE;
    it = list_iterator_new(sub_folder_list, LIST_HEAD);
    j = 0;
    while (it != NULL) {
        a = list_iterator_next(it);
        if (a) {
            egislog_v("subfolder names [%d] %s", j, a->val);
            switch (g_db_type) {
                case DB_SIMEPLE:
                    correctFolder = ENDS_WITH((char*)a->val, validSimepleDBFolder, 5);
                    break;
                case DB_EVTOOL:
                case DB_PROJ_HUA:
                    correctFolder = ENDS_WITH((char*)a->val, validEvToolFolder_st, 2);
                    if (correctFolder) break;
                    correctFolder = ENDS_WITH((char*)a->val, validEvToolFolder_45d, 3);
                    if (correctFolder) break;
                    correctFolder = ENDS_WITH((char*)a->val, validEvToolFolder_90d, 3);
                    break;
                default:
                    break;
            }
            if (TRUE == correctFolder) {
                *single_dimension = TRUE;
                break;
            }
        } else
            break;

        j++;
    }
    list_iterator_destroy(it);

    if (!correctFolder) {
        list_destroy(sub_folder_list);
        return SEARCHF_OK;
    }

    // Prepare output_list
    it = list_iterator_new(sub_folder_list, LIST_HEAD);
    j = 0;
    if (*single_dimension) {
        while (it != NULL) {
            a = list_iterator_next(it);
            if (a) {
                _collect_sub_folder_file(a->val, FALSE, output_list);
            } else
                break;

            j++;
        }
    } else {
        egislog_e("single_dimension is FALSE. wont be supported");
        while (it != NULL) {
            a = list_iterator_next(it);
            if (a) {
                list_t* file_list = list_new();
                file_list->free = free_file_path;
                egislog_v("collecting [%d] %s", j, a->val);

                _collect_sub_folder_file(a->val, FALSE, file_list);

                list_rpush(output_list, list_node_new(file_list));
            } else
                break;

            j++;
        }
        // Override free callback
        output_list->free = free_file_list;
    }
    list_iterator_destroy(it);

    list_destroy(sub_folder_list);
    return SEARCHF_OK;
}

void free_file_path(void* val) {
    if (val) plat_free(val);
}

void free_file_list(void* val) {
    if (val) list_destroy(val);
}