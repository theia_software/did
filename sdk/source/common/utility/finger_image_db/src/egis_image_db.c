#include <limits.h>
#include <regex.h>
#include <stdlib.h>

#include "clibs/list/src/list.h"
#include "egis_definition.h"
#include "egis_image_db.h"
#include "plat_log.h"
#include "plat_mem.h"

#define LOG_TAG "RBS-fingerdb"

#define nmatch 3
static BOOL _has_all_PQRS_folder(list_t* finger_folder_list, int total) {
    BOOL all_PQRS = TRUE;
    int i;
    int regex_flag = REG_EXTENDED;
    regex_t preg;
    char* pattern = "/[A-Z]/";
    regmatch_t pmatch[nmatch];

    if (regcomp(&preg, pattern, regex_flag) != 0) {
        egislog_e("regexp comp error.");
        return FALSE;
    }
    list_iterator_t* it = list_iterator_new(finger_folder_list, LIST_HEAD);
    for (i = 0; i < total; i++) {
        list_node_t* a = list_iterator_next(it);
        if (regexec(&preg, a->val, nmatch, pmatch, 0) == 0) {
        } else {
            all_PQRS = FALSE;
            break;
        }
    }
    list_iterator_destroy(it);
    regfree(&preg);
    return all_PQRS;
}
static void _extract_UID_FID(list_t* finger_folder_list, int total, char** p_folder_list) {
    int i, j;
    int data_length;
    int regex_flag = REG_EXTENDED;
    regex_t preg;
    char* pattern = "/[A-Z]/(.*/[0-9])";
    regmatch_t pmatch[nmatch];
    if (regcomp(&preg, pattern, regex_flag) != 0) {
        egislog_e("regexp comp error.");
        return;
    }
    if (total != finger_folder_list->len) {
        egislog_e("%s wrong size. %d != %d", total, finger_folder_list->len);
        return;
    }
    // /sdcard/FP7/image_bin/Q/jack/0
    egislog_d("%s pattern %s", __func__, pattern);
    list_iterator_t* it = list_iterator_new(finger_folder_list, LIST_HEAD);
    for (j = 0; j < total; j++) {
        list_node_t* a = list_iterator_next(it);
        egislog_d("%s [%d] %s", __func__, j, a->val);
        if (regexec(&preg, a->val, nmatch, pmatch, 0) == 0) {
            for (i = 1; i < nmatch && pmatch[i].rm_so >= 0; ++i) {
                data_length = pmatch[i].rm_eo - pmatch[i].rm_so;
                strncpy(p_folder_list[j], ((char*)(a->val) + pmatch[i].rm_so), data_length);
                egislog_d("%s [%d][%d] --> (%d)(%d) %s", __func__, j, i, pmatch[i].rm_so,
                          pmatch[i].rm_eo, p_folder_list[j]);
            }
        }
    }
    list_iterator_destroy(it);
    regfree(&preg);
}

void merge_PQ_folder(list_t* finger_folder_list, list_t** finger_img_enroll_list,
                     list_t** finger_img_verify_list, int* valid_count) {
    if (valid_count == NULL || *valid_count <= 0) {
        egislog_e("wrong input valid_count");
        return;
    }
    int i, j;
    int ori_count = *valid_count;
    egislog_d("%s start: %d", __func__, ori_count);
    BOOL all_PQRS = _has_all_PQRS_folder(finger_folder_list, ori_count);
    egislog_d("%s all PQRS %d", __func__, all_PQRS);
    if (!all_PQRS) {
        return;
    }

    char** p_folder_list = plat_alloc(ori_count * sizeof(char*));
    for (i = 0; i < ori_count; i++) {
        p_folder_list[i] = plat_alloc(PATH_MAX);
        memset(p_folder_list[i], 0, PATH_MAX);
    }
    _extract_UID_FID(finger_folder_list, ori_count, p_folder_list);

    for (i = 0; i < ori_count; i++) {
        for (j = i + 1; j < ori_count; j++) {
            if (strcmp(p_folder_list[i], p_folder_list[j]) == 0) {
                egislog_d("%s [%d][%d] are the same. len e(%d, %d) v(%d,%d)", __func__, i, j,
                          finger_img_enroll_list[i]->len, finger_img_enroll_list[j]->len,
                          finger_img_verify_list[i]->len, finger_img_verify_list[j]->len);

                if (finger_img_enroll_list[i]->len > 0 && finger_img_enroll_list[j]->len > 0) {
                    egislog_e("%s, error !! one finger has two enroll folder", __func__);
                }
                list_rpush_list(finger_img_enroll_list[i], finger_img_enroll_list[j]);
                finger_img_enroll_list[j] = NULL;
                egislog_d("%s enroll[%d] len=%d", __func__, i, finger_img_enroll_list[i]->len);

                list_rpush_list(finger_img_verify_list[i], finger_img_verify_list[j]);
                finger_img_verify_list[j] = NULL;
                egislog_d("%s verify[%d] len=%d", __func__, i, finger_img_verify_list[i]->len);
            }
        }
    }
    int new_count = 0;
    for (i = 0; i < ori_count; i++) {
        if (finger_img_verify_list[i] != NULL) {
            new_count++;
            continue;
        }
        BOOL move_up = FALSE;
        for (j = i + 1; j < ori_count; j++) {
            if (finger_img_verify_list[j] != NULL) {
                finger_img_verify_list[i] = finger_img_verify_list[j];
                finger_img_verify_list[j] = NULL;

                if (finger_img_enroll_list[i] == NULL && finger_img_enroll_list[j] != NULL) {
                    finger_img_enroll_list[i] = finger_img_enroll_list[j];
                    finger_img_enroll_list[j] = NULL;
                } else {
                    egislog_e("%s wrong DB for enroll. [%d][%d]", __func__, i, j);
                }
                move_up = TRUE;
                break;
            }
        }
        if (!move_up) {
            // All remaining verify_list are NULL
            break;
        } else {
            // Continue to check remaining verify_list
            i--;
        }
    }
    egislog_d("%s, new_count=%d", __func__, new_count);
    *valid_count = new_count;

    for (i = 0; i < ori_count; i++) {
        PLAT_FREE(p_folder_list[i]);
    }
    PLAT_FREE(p_folder_list);
}

BOOL is_enroll_redundant(const char* enroll_filepath) {
    BOOL is_redundant;
    int i, j;
    int regex_flag = REG_EXTENDED;
    regex_t preg;
    char* pattern = "/.*_RD_[0-9][0-9]";
    regmatch_t pmatch[nmatch];

    if (regcomp(&preg, pattern, regex_flag) != 0) {
        egislog_e("regexp comp error.");
        return FALSE;
    }

    char matchPart[PATH_MAX] = {0};
    if (regexec(&preg, enroll_filepath, nmatch, pmatch, 0) == 0) {
        int data_length = pmatch[0].rm_eo - pmatch[0].rm_so;
        strncpy(matchPart, enroll_filepath + pmatch[0].rm_so, data_length);
        egislog_d("%s is enroll redundant", matchPart);
        is_redundant = TRUE;
    } else {
        is_redundant = FALSE;
    }
    regfree(&preg);
    return is_redundant;
}

void _get_match_part(const char* the_string, regmatch_t* pmatch, int match_idx, char* matchPart,
                     int size) {
    int data_length = pmatch[match_idx].rm_eo - pmatch[match_idx].rm_so;
    if (data_length > size) {
        egislog_e("matchPart size is too small");
        data_length = size;
    }
    memset(matchPart, 0, size);
    strncpy(matchPart, the_string + pmatch[match_idx].rm_so, data_length);
}

int db_parse_cx_cy(const char* filepath, int* cx, int* cy) {
    int i, j;
    int retval = -1;
    int regex_flag = REG_EXTENDED | REG_ICASE;
    regex_t preg;
    char* pattern = ".*cx_([0-9]{2,3})_cy_([0-9]{2,3})";
    regmatch_t pmatch[nmatch];

    if (regcomp(&preg, pattern, regex_flag) != 0) {
        egislog_e("regexp comp error.");
        return FALSE;
    }
#define PART_SIZE 5
    char matchPart[PART_SIZE] = {0};
    if (regexec(&preg, filepath, nmatch, pmatch, 0) == 0) {
        _get_match_part(filepath, pmatch, 1, matchPart, PART_SIZE);
        *cx = atoi(matchPart);
        _get_match_part(filepath, pmatch, 2, matchPart, PART_SIZE);
        *cy = atoi(matchPart);
        retval = 0;
    } else {
        egislog_e("failed to extract cx, cy: %s", filepath);
    }
    regfree(&preg);
    return retval;
}
