#ifndef __OBJECT_DEFINITION_IMAGE_H__
#define __OBJECT_DEFINITION_IMAGE_H__

#include "object_def.h"
#include "egis_sprintf.h"

#define TOTAL_IMG_TYPE 3
// #define ONLY_REQUEST_IMGTYPE_BKG  // temporary use define to switch
#define RBS_IMG_MAJOR_VERSION 1
#define RBS_IMG_MINOR_VERSION 6
#define IMGTYPE_BIN 0
#define IMGTYPE_RAW 1
#define IMGTYPE_BKG 2

#ifdef _WINDOWS
#define PACKED_STRUCT(__Declaration__) \
    __pragma(pack(push, 1)) typedef struct __Declaration__ __pragma(pack(pop))
#else
#define PACKED_STRUCT(__Declaration__) typedef struct __Declaration__ __attribute__((packed))
#endif

#define VERIFY_TRY_MATCH_RESULT_LAST_NOT_MATCH -1
#define VERIFY_TRY_MATCH_RESULT_NOT_MATCH 0
#define VERIFY_TRY_MATCH_RESULT_MATCH 1

#define ACTION_NONE 9000
#define ACTION_ENROLL_FIRST 9001
#define ACTION_ERNOLL 9002
#define ACTION_VERIFY 9003


PACKED_STRUCT({
    //remember to sync any structure change to imo_tool
    //#rbs_obj_struct_start
    rbs_obj_desc_t obj_desc;
    uint16_t imgtype;
    uint16_t width;
    uint16_t height;
    uint16_t bpp;
    uint16_t param_type;        // the name should not be changed for param offset
    uint16_t index_fingeron;
    uint16_t index_try_match;
    uint16_t index_series;
    uint16_t is_bad;
    int16_t algo_flag;          //#visibility:EV #tag:mi #order:105
    uint16_t reject_reason;
    uint16_t match_score;       //#visibility:V #tag:ms #order:145
    int16_t try_match_result;
    int16_t qty;                // g2_qty for G2
    uint16_t partial;           // ML Partial for optical projects #visibility:EV #tag:egp #order:200
    int16_t fake_score;         //#visibility:EV #tag:fk #order:305
    uint16_t is_light;
    uint16_t stat_min;
    uint16_t stat_max;
    uint16_t stat_avg;
    uint16_t Mc;                //#visibility:EV #order:141
    uint16_t MBc;               //#visibility:EV #order:142
    uint16_t M;                 //#visibility:EV #order:140
    int16_t temperature;        //#visibility:EV #tag:Te #order:120
    uint16_t exposure_x10;      //#visibility:EV #tag:et #op:x0.1 #order:110
    uint16_t hw_integrate_count;//#visibility:EV #tag:hc #order:111
    uint16_t sw_integrate_count;
    uint16_t extract_qty;       //#visibility:EV #tag:eqty #order:135
    uint16_t bds_debug_path;
    uint16_t bds_pool_add;      //#visibility:EV #tag:Ba #op:%100 #order:115
    uint16_t is_learning;       //#visibility:V #tag:learn #order:125
    uint16_t algo_result;       // enrolled_cnt || VERIFY_TYPE_NORMAL_MATCH, VERIFY_TYPE_QUICK_MATCH, ..
    int16_t g2_partial;
    uint16_t is_last_image;
    int16_t black_edge;
    uint16_t finger_score;
    uint16_t match_threshold;   //#visibility:EV #tag:mt #order:130
    uint16_t match_score_0;     //#visibility:V 
    uint16_t match_score_1;     //#visibility:V
    uint16_t match_score_2;     //#visibility:V
    uint16_t match_score_3;     //#visibility:V
    uint16_t match_score_4;     //#visibility:V
    uint16_t is_uk_image;
    uint16_t c60_x100;
    uint16_t is_finger_off;    //#visibility:V
    uint16_t BAC_value_00;     //#visibility:V #op:BAC #tag:BAC #order:150
    uint16_t BAC_value_01;     //#visibility:V #op:BAC
    uint16_t BAC_value_02;     //#visibility:V #op:BAC
    uint16_t BAC_value_03;     //#visibility:V #op:BAC
    uint16_t BAC_value_04;     //#visibility:V #op:BAC
    uint16_t BAC_value_05;     //#visibility:V #op:BAC
    uint16_t BAC_value_06;     //#visibility:V #op:BAC
    uint16_t BAC_value_07;     //#visibility:V #op:BAC
    uint16_t BAC_value_08;     //#visibility:V #op:BAC
    uint16_t BAC_value_09;     //#visibility:V #op:BAC
    uint16_t BAC_value_10;     //#visibility:V #op:BAC
    uint16_t BAC_value_11;     //#visibility:V #op:BAC
    uint16_t BAC_value_12;     //#visibility:V #op:BAC
    uint16_t BAC_value_13;     //#visibility:V #op:BAC
    uint16_t BAC_value_14;     //#visibility:V #op:BAC
    uint16_t BAC_value_15;     //#visibility:V #op:BAC
    uint16_t BAC_value_16;     //#visibility:V #op:BAC
    uint16_t BAC_value_17;     //#visibility:V #op:BAC
    uint16_t BAC_value_18;     //#visibility:V #op:BAC
    uint16_t BAC_value_19;     //#visibility:V #op:BAC
    uint16_t BAC_value_20;     //#visibility:V #op:BAC
    uint16_t BAC_value_21;     //#visibility:V #op:BAC
    uint16_t BAC_value_22;     //#visibility:V #op:BAC
    uint16_t BAC_value_23;     //#visibility:V #op:BAC
    uint16_t BAC_value_24;     //#visibility:V #op:BAC
    uint16_t BAC_value_25;     //#visibility:V #op:BAC
    uint16_t BAC_value_26;     //#visibility:V #op:BAC
    uint16_t BAC_value_27;     //#visibility:V #op:BAC
    uint16_t BAC_value_28;     //#visibility:V #op:BAC
    uint16_t BAC_value_29;     //#visibility:V #op:BAC
    uint16_t BAC_value_30;     //#visibility:V #op:BAC
    uint16_t BAC_value_31;     //#visibility:V #op:BAC
    uint16_t BAC_value_32;     //#visibility:V #op:BAC
    uint16_t BAC_value_33;     //#visibility:V #op:BAC
    uint16_t BAC_value_34;     //#visibility:V #op:BAC
    uint16_t BAC_value_35;     //#visibility:V #op:BAC
    uint16_t overflow_percentage;
    uint16_t is_sunlight_detect;
    uint16_t verify_mask_border_size;
    int16_t sp_area;           //#visibility:V #tag:sp #order:155
    int16_t bkg_type;
    int16_t match_image_class; //#visibility:V #tag:stc #order:160
    uint16_t extract_time;
    uint16_t verify_time;
    uint16_t ipp_time;
    uint16_t learn_time;
    uint16_t McBlack;
    uint16_t rlight_partial; //#visibility:EV #tag:rlp #order:205
    uint16_t rlight_score; //#visibility:EV #tag:rls #order:206
    int16_t is_sp_area_enable;
    uint16_t sunlight_score; //#visibility:V #tag:sls #order:165
    uint16_t after_capture_bac_value; //#visibility:EV #tag:acb #order:166
    uint16_t M_zoneAvg;               //#visibility:EV #tag:mza #order:167
    int16_t ic_temperature;
    uint16_t MBlackBkg;
    uint16_t McBlackBkg;
    uint16_t low_light_th;
    uint16_t try_dpi_match_mode;
    uint16_t try_dpi_match_result;
    uint16_t STRING_AREA_START_OFFSET_ADDR;
    uint16_t STRING_AREA_END_OFFSET_ADDR;
    // param_v1 expand the struct here.
    // ---------rbs_obj_struct param_v1 end---------

    // ---------rbs_obj_struct param_v2 start ---------
    rbs_obj_string_t Group1xx;
    // add group 1xx here
    rbs_obj_string_t sig;
    rbs_obj_string_t x0;
    rbs_obj_string_t y0;
    rbs_obj_string_t all_adc_dac;
    rbs_obj_string_t Group2xx;
    rbs_obj_string_t afm;
    // rbs_obj_string_t ta_obj_size;
    rbs_obj_string_t testG2_param1;
    // add group 2xx here

    rbs_obj_string_t Group3xx;
    // add group 3xx here

    rbs_obj_string_t Group4xx;
    // add group 4xx here

    // param_v2 expand the struct here.
    uint16_t STRING_AREA_END_TAG;
    // ---------rbs_obj_struct param_v2 end---------
    // If restruct the the members, increase minor version below.
})
rbs_obj_image_v1_0_t;

#define RBSOBJ_set_IMAGE_v1_0(img_obj, the_imgtype, the_width, the_height, the_bpp) \
    OBJ_DESC_init((&(img_obj->obj_desc)), RBSOBJ_ID_IMAGE, "IMAGE", RBS_IMG_MAJOR_VERSION, RBS_IMG_MINOR_VERSION, 0);       \
    img_obj->obj_desc.header_size = sizeof(*img_obj) - sizeof(img_obj->obj_desc);   \
    img_obj->obj_desc.payload_size = the_width * the_height * the_bpp / 8;          \
    img_obj->imgtype = the_imgtype;                                                 \
    img_obj->width = the_width;                                                     \
    img_obj->height = the_height;                                                   \
    img_obj->bpp = the_bpp;

#define RBSOBJ_set_IMAGE_param(img_obj, param, value) \
    if (img_obj != NULL) img_obj->param = value;

#define RBSOBJ_get_IMAGE_param(img_obj, param) img_obj->param

#define RBSOBJ_copy_IMAGE_params(src_img_obj, dest_img_obj)                                     \
    {                                                                                           \
        int offset = (uint8_t*)(&src_img_obj->param_type) - (uint8_t*)(&src_img_obj->obj_desc); \
        if (dest_img_obj == NULL)                                                               \
            memset(((uint8_t*)(src_img_obj) + offset), 0, sizeof(*src_img_obj) - offset);       \
        else                                                                                    \
            memcpy(((uint8_t*)(dest_img_obj) + offset), ((uint8_t*)(src_img_obj) + offset),     \
                   sizeof(*src_img_obj) - offset);                                              \
    }

#define RBSOBJ_set_IMAGE_param_v2(img_obj, param, param_tag, param_value) \
    if (img_obj != NULL) { \
        img_obj->param.value = param_value; \
        memcpy(img_obj->param.tag, param_tag, egist_strnlen(param_tag, OBJ_TAG_LENS)); \
    }

#define RBSOBJ_get_IMAGE_param_v2(img_obj, param) img_obj->param.value

#endif
