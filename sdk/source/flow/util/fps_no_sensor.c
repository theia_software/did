#if defined(__ET0XX__) || defined(__OTG_SENSOR__)
#include <fcntl.h>
#include <stdlib.h>

#include "fps_normal.h"
#include "plat_log.h"
#include "response_def.h"

#ifndef _WINDOWS
#include <sys/ioctl.h>
#include <unistd.h>
#else
#include <Windows.h>
#define __unused
#endif
unsigned int fp_device_open(__unused int* device_handle) {
    return FINGERPRINT_RES_SUCCESS;
}

unsigned int fp_device_close(__unused int device_handle) {
    return FINGERPRINT_RES_SUCCESS;
}

unsigned int fp_device_reset(__unused int device_handle) {
    return FINGERPRINT_RES_SUCCESS;
}

unsigned int fp_device_clock_enable(__unused int device_handle, __unused BOOL enable) {
    return FINGERPRINT_RES_SUCCESS;
}

unsigned int fp_device_interrupt_enable(__unused int device_handle, __unused int cmd) {
    return FINGERPRINT_RES_SUCCESS;
}

unsigned int fp_device_interrupt_wait(__unused int device_handle, __unused unsigned int timeout) {
    return FINGERPRINT_RES_SUCCESS;
}

unsigned int egisFpDeviceFingerOnZero(__unused int handle) {
    return FINGERPRINT_RES_SUCCESS;
}

unsigned int fp_device_power_control(__unused int device_handle, __unused BOOL enable) {
    return FINGERPRINT_RES_SUCCESS;
}

void fp_set_interrupt_trigger_type(__unused int trigger_type) {
    return;
}
#if defined(__ET7XX__) && !defined(__ET0XX__)
int get_sys_temp(int* temperature) {
    int bat_temp;
    char temp_buf[5];

    memset(temp_buf, 0x00, sizeof(temp_buf));
    FILE* fdd = fopen("/sys/class/power_supply/battery/temp", "r");

    if (fdd == NULL) {
        ex_log(LOG_ERROR, "can not open the file");
        *temperature = 250;
        return -1;
    }
    fread(temp_buf, 4, 1, fdd);
    fclose(fdd);

    bat_temp = atoi(temp_buf);
    if (bat_temp > 650) {
        *temperature = 650;
    } else if (bat_temp < -200) {
        *temperature = -200;
    } else {
        *temperature = bat_temp;
    }
    ex_log(LOG_DEBUG, "sys temperature = %d", *temperature);
    return 0;
}

int set_brightness(int level) {
    if (level == 0) return 0;
    int fd, ret, bri_level;
    char bri_write[10] = {0}, bri_val[10] = {0};
    const char* dev_backlight_a50 = "/sys/class/lcd/panel/device/backlight/panel/brightness";
    // a70 a90 use same path
    const char* dev_backlight_a70 = "/sys/class/backlight/panel0-backlight/brightness";
    const char* dev_backlight_astar = "/sys/class/leds/lcd-backlight/brightness";

    fd = open(dev_backlight_a50, O_RDWR);

    if (fd < 0) {
        fd = open(dev_backlight_a70, O_RDWR);
    }

    if (fd < 0) {
        fd = open(dev_backlight_astar, O_RDWR);
    }

    if (fd < 0) {
        ex_log(LOG_ERROR, "%s open fail, fd = %d", __func__, fd);
        return fd;
    }

    ret = read(fd, bri_val, 10);
    if (ret < 0) {
        ex_log(LOG_ERROR, "%s read fail, ret = %d", __func__, ret);
        goto out;
    }

    bri_level = atoi(bri_val);
    ex_log(LOG_DEBUG, "%s level %d -> %d", __func__, bri_level, level);

    sprintf(bri_write, "%d", level);
    ret = write(fd, bri_write, strlen(bri_write));
    if (ret < 0) {
        ex_log(LOG_ERROR, "%s write fail, ret = %d", __func__, ret);
        goto out;
    }

    memset(bri_val, 0x0, 10);
    ret = read(fd, bri_val, 10);
    if (ret < 0) {
        ex_log(LOG_ERROR, "%s read fail, ret = %d", __func__, ret);
        goto out;
    }

    ret = 0;
    bri_level = atoi(bri_val);
    ex_log(LOG_DEBUG, "%s check level %d, %s", __func__, bri_level, bri_val);

out:
    close(fd);
    return ret;
}

#endif
#endif
