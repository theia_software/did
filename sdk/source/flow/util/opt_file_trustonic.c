#include <dirent.h>
#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <time.h>
#include <unistd.h>
#include "egis_definition.h"
#include "op_manager.h"
#include "opt_file.h"
#include "plat_log.h"
#include "response_def.h"

#define MAX_READ_LEN 1024
#define FRAME_WIDTH (200)
#define FRAME_HEIGHT (200)
#define SIZEOFSHORT sizeof(unsigned short)

#define MAX_CALIBRATION_DATA_SIZE (FRAME_WIDTH * FRAME_HEIGHT * SIZEOFSHORT + 100)
#define MAX_TRANSPORTER_DATA_SIZE 512 * 1024
#define MAX_IMAGE_SIZE 10 * 1024
#define MAX_TEMPLATE_DATA_STRUCT_SIZE (3 * 1024 * 1024 + 100)
#define RECEIVE_SEND_DATA_SIZE_ONCE (800 * 1024)
#define MAX_GROUP_INFO_SIZE 1024
#define MAX_BDS_SIZE (2464260)
#define MAX_SCRATCH_SIZE (FRAME_WIDTH * FRAME_HEIGHT * SIZEOFSHORT + 100)
#define MAX_ENCRY_IMAGE (FRAME_WIDTH * FRAME_HEIGHT * SIZEOFSHORT + 100)
#define MAX_DEBASE_SIZE (FRAME_WIDTH * FRAME_HEIGHT * SIZEOFSHORT * 10 + 164)
#define CALIBRATION_NAME "/data/vendor_de/0/fpdata/cb"
#define SCRATCH_NAME "/data/vendor_de/0/fpdata/sc"
#define DEBASE_NAME "/data/vendor_de/0/fpdata/debase"
#define TEMPLATE_NAME "tp"
#define USERINFO_NAME "gpinfo"
#define BDS_NAME "bds"
extern char g_user_path[MAX_PATH_LEN];

static int save_file(const char* file_name, unsigned char* data, int data_len);
static int get_file(const char* file_name, unsigned char* data, int* data_len);
static int remove_file(const char* file_name);

static int save_file(const char* file_name, unsigned char* data, int data_len) {
    int retval = FINGERPRINT_RES_SUCCESS;
    int write_len;
    FILE* file;

    if (NULL == data || data_len <= 0) {
        return FINGERPRINT_RES_INVALID_PARAM;
    }

    file = fopen(file_name, "wb+");
    if (NULL == file) {
        retval = FINGERPRINT_RES_OPEN_FILE_FAILED;
        goto EXIT;
    }

    write_len = fwrite(data, 1, data_len, file);
    if (write_len < data_len) {
        retval = FINGERPRINT_RES_FAILED;
    }

    fclose(file);
EXIT:
    return retval;
}

static int get_file(const char* file_name, unsigned char* buffer, int* buffer_len) {
    int retval = FINGERPRINT_RES_SUCCESS;
    FILE* file;
    int file_data_size;
    int read_len;
    int read_total_len = 0;
    unsigned char* pbuf;

    if (buffer == NULL || buffer_len == NULL || *buffer_len <= 0) {
        return FINGERPRINT_RES_INVALID_PARAM;
    }

    unsigned char* read_buffer = (unsigned char*)malloc(MAX_READ_LEN);
    if (NULL == read_buffer) {
        return FINGERPRINT_RES_ALLOC_FAILED;
    }

    file = fopen(file_name, "rb");
    if (NULL == file) {
        *buffer_len = 0;
        retval = FINGERPRINT_RES_OPEN_FILE_FAILED;
    }

    if (FINGERPRINT_RES_SUCCESS == retval) {
        while (1) {
            pbuf = buffer + read_total_len;

            read_len = fread(read_buffer, 1, MAX_READ_LEN, file);
            if (read_len > 0) {
                read_total_len += read_len;
                if (*buffer_len < read_total_len) {
                    retval = FINGERPRINT_RES_FAILED;
                    goto EXIT;
                }
                memcpy(pbuf, read_buffer, read_len);
            }
            if (read_len < MAX_READ_LEN) {
                retval = FINGERPRINT_RES_SUCCESS;
                break;
            }
        };

        *buffer_len = read_total_len;
    }

EXIT:
    if (NULL != file) {
        fclose(file);
    }

    if (NULL != read_buffer) {
        free(read_buffer);
        read_buffer = NULL;
    }

    return retval;
}

int trustonic_receive_data(int type, unsigned char* in_data, int in_data_len) {
    unsigned char* data = NULL;
    unsigned int fid;
    char file_name[MAX_PATH_LEN] = {0};
    char user_path[MAX_PATH_LEN] = {0};
    int data_len = 0;
    int retval;
    struct stat st;
    int out_buf[1] = {0};
    int out_size = 0;

    strncpy(user_path, g_user_path, MAX_PATH_LEN);

    switch (type) {
        case TYPE_RECEIVE_SCRATCH:
        case TYPE_RECEIVE_CALIBRATION_DATA:
        case TYPE_RECEIVE_DEBASE: {
            unsigned char* pname = NULL;
            if (type == TYPE_RECEIVE_CALIBRATION_DATA) {
                data_len = MAX_CALIBRATION_DATA_SIZE;
                pname = CALIBRATION_NAME;
            } else if (type == TYPE_RECEIVE_SCRATCH) {
                data_len = MAX_SCRATCH_SIZE;
                pname = SCRATCH_NAME;
            } else if (type == TYPE_RECEIVE_DEBASE) {
                data_len = MAX_DEBASE_SIZE;
                pname = DEBASE_NAME;
            } else
                break;

            data = (unsigned char*)malloc(data_len);
            if (NULL == data) {
                retval = FINGERPRINT_RES_ALLOC_FAILED;
                break;
            }
            retval = opm_get_data(type, NULL, 0, data, &data_len);
            if (retval != FINGERPRINT_RES_SUCCESS) {
                ex_log(LOG_DEBUG, "opm_get_data ret = %d", retval);
                if (retval == EGIS_RECEIVE_BUF_LEN_IS_NOT_ENOUGH) {
                    ex_log(LOG_DEBUG, "opm_get_data data_len = %d, real_len = %d", data_len,
                           ((int*)data)[0]);
                }
            }

            if (retval == FINGERPRINT_RES_SUCCESS && data_len != 0) {
                int ret = save_file(pname, data, data_len);
                ex_log(LOG_DEBUG, "opm_get_data ret = %d ,datalen %d ,%s ", ret, data_len, pname);
            }
        } break;
        case TYPE_RECEIVE_BDS: {
            int receive_pos = 0;
            unsigned int in_array = 0;
            unsigned char* pBuffer = NULL;
            int receive_size;

            data_len = MAX_BDS_SIZE;
            data = (unsigned char*)malloc(data_len);
            if (data == NULL) {
                retval = FINGERPRINT_RES_ALLOC_FAILED;
                break;
            }

            memset(data, 0x00, MAX_BDS_SIZE);
            out_buf[0] = data_len;
            out_size = sizeof(out_buf);
            retval = opm_get_data(TYPE_RECEIVE_BDS_START, NULL, 0, out_buf, &out_size);

            if (retval != FINGERPRINT_RES_SUCCESS || out_buf[0] > data_len || out_buf[0] <= 0) {
                ex_log(LOG_ERROR,
                       "TYPE_RECEIVE_BDS_START NO NEED UPDATE Or other Error,retval = %d, data_len "
                       "= %d, real_len = %d",
                       retval, data_len, out_buf[0]);
                break;
            }

            do {
                pBuffer = data + receive_pos;
                receive_size = (data_len - receive_pos) > RECEIVE_SEND_DATA_SIZE_ONCE
                                   ? RECEIVE_SEND_DATA_SIZE_ONCE
                                   : (data_len - receive_pos);
                in_array = receive_pos;
                retval = opm_get_data(TYPE_RECEIVE_BDS, &in_array, sizeof(unsigned int), pBuffer,
                                      &receive_size);
                ex_log(LOG_DEBUG, "opm_get_data ret = %d,pos %d ,receive_size = %d, data_len = %d",
                       retval, receive_pos, receive_size, data_len);

                receive_pos += receive_size;
                if (retval == FINGERPRINT_RES_SUCCESS) {
                    break;
                }

                if (retval != FINGERPRINT_RES_TEMPLATE_CONTINUE ||
                    receive_size > RECEIVE_SEND_DATA_SIZE_ONCE) {
                    retval = FINGERPRINT_RES_FAILED;
                    break;
                }

            } while (receive_pos < data_len);

            retval = opm_get_data(TYPE_RECEIVE_BDS_END, NULL, 0, NULL, 0);
            if (retval != FINGERPRINT_RES_SUCCESS) {
                break;
            }

            if (stat(user_path, &st) == -1) {
                mkdir(user_path, 0700);
            }

            data_len = receive_pos;

            snprintf(file_name, MAX_PATH_LEN, "%s/%s", user_path, BDS_NAME);
            retval = save_file(file_name, data, data_len);
            ex_log(LOG_DEBUG, "opm_get_data ret = %d ,datalen %d, receive len %d file_name %s ",
                   retval, data_len, receive_pos, file_name);

        } break;
        case TYPE_RECEIVE_TEMPLATE: {
            int receive_pos = 0;
            unsigned int fid;
            unsigned int in_array[2];
            unsigned char* pBuffer;
            int receive_size;
            if (in_data == NULL || in_data_len != sizeof(unsigned int)) {
                retval = FINGERPRINT_RES_INVALID_PARAM;
                break;
            }
            fid = *((unsigned int*)in_data);
            in_array[0] = fid;
            data_len = MAX_TEMPLATE_DATA_STRUCT_SIZE;
            data = (unsigned char*)malloc(data_len);
            if (data == NULL) {
                retval = FINGERPRINT_RES_ALLOC_FAILED;
                break;
            }

            memset(data, 0x00, MAX_TEMPLATE_DATA_STRUCT_SIZE);
            out_buf[0] = data_len;
            out_size = sizeof(out_buf);
            retval = opm_get_data(TYPE_RECEIVE_TEMPLATE_START, in_array, sizeof(in_array), out_buf,
                                  &out_size);

            if (retval != FINGERPRINT_RES_SUCCESS || out_buf[0] > data_len || out_buf[0] <= 0) {
                ex_log(LOG_DEBUG,
                       "TYPE_RECEIVE_TEMPLATE_START error, ret = %d, data_len = %d,real_len= %d",
                       retval, data_len, out_buf[0]);
                break;
            }
            do {
                pBuffer = data + receive_pos;
                receive_size = (data_len - receive_pos) > RECEIVE_SEND_DATA_SIZE_ONCE
                                   ? RECEIVE_SEND_DATA_SIZE_ONCE
                                   : (data_len - receive_pos);
                in_array[1] = receive_pos;
                ex_log(LOG_DEBUG, "TYPE_RECEIVE_TEMPLATE_START receive_pos = %d,receive_size=%d",
                       receive_pos, receive_size);
                retval = opm_get_data(TYPE_RECEIVE_TEMPLATE, in_array, sizeof(in_array), pBuffer,
                                      &receive_size);
                ex_log(LOG_DEBUG,
                       "opm_get_data ret = %d fid %d ,pos %d ,receive_size = %d, data_len = %d ",
                       retval, *((unsigned int*)in_data), receive_pos, receive_size, data_len);

                receive_pos += receive_size;
                if (retval == FINGERPRINT_RES_SUCCESS) {
                    break;
                }

                if (retval != FINGERPRINT_RES_TEMPLATE_CONTINUE ||
                    receive_size > RECEIVE_SEND_DATA_SIZE_ONCE) {
                    retval = FINGERPRINT_RES_FAILED;
                    break;
                }

            } while (receive_pos < data_len);

            opm_get_data(TYPE_RECEIVE_TEMPLATE_END, NULL, 0, NULL, 0);
            if (retval != FINGERPRINT_RES_SUCCESS) break;

            if (stat(user_path, &st) == -1) {
                mkdir(user_path, 0700);
            }

            data_len = receive_pos;

            snprintf(file_name, MAX_PATH_LEN, "%s/%s_%u", user_path, TEMPLATE_NAME, fid);
            retval = save_file(file_name, data, data_len);
            ex_log(LOG_DEBUG, "opm_get_data ret = %d ,datalen %d, receive len %d file_name %s ",
                   retval, data_len, receive_pos, file_name);

        } break;

        case TYPE_DELETE_TEMPLATE: {
            if (in_data == NULL || in_data_len != sizeof(unsigned int)) {
                retval = FINGERPRINT_RES_INVALID_PARAM;
                break;
            }

            fid = *((int*)in_data);
            if (fid == 0) {
                DIR* dir = opendir(user_path);
                struct dirent* ptr = NULL;
                int result;
                retval = FINGERPRINT_RES_SUCCESS;
                while ((ptr = readdir(dir)) != NULL) {
                    // is a template file
                    if (ptr->d_type != 8 || strncmp(ptr->d_name, "tp_", strlen("tp_")) != 0) {
                        continue;
                    }
                    // remove file
                    snprintf(file_name, MAX_PATH_LEN, "%s/%s", user_path, ptr->d_name);
                    result = remove(file_name);
                    if (result) retval = result;
                }
            } else {
                snprintf(file_name, MAX_PATH_LEN, "%s/%s_%u", user_path, TEMPLATE_NAME, fid);
                retval = remove(file_name);
            }
        } break;
        case TYPE_RECEIVE_USER_INFO: {
            ex_log(LOG_DEBUG, "TYPE_RECEIVE_USER_INFO enter");
            // char user_path[MAX_PATH_LEN] = {0};
            unsigned int user_id;
            data_len = MAX_GROUP_INFO_SIZE;
            data = (unsigned char*)malloc(data_len);
            if (NULL == data) {
                retval = FINGERPRINT_RES_ALLOC_FAILED;
                break;
            }

            retval = opm_get_data(TYPE_RECEIVE_USER_INFO, NULL, 0, data, &data_len);
            if (retval != FINGERPRINT_RES_SUCCESS) {
                retval = FINGERPRINT_RES_FAILED;
                ex_log(LOG_ERROR, "opm_get_data failed %d", retval);
                break;
            }

            user_id = *((unsigned int*)in_data);
            snprintf(file_name, MAX_PATH_LEN, "%s/%s_%u", user_path, USERINFO_NAME, user_id);
            retval = save_file(file_name, data, data_len);
            ex_log(LOG_DEBUG, "TYPE_RECEIVE_USER_INFO leave %d,file_name = %s", retval, file_name);
        } break;
        default:
            retval = FINGERPRINT_RES_INVALID_PARAM;
            break;
    }

    if (NULL != data) {
        free(data);
        data = NULL;
    }
    return retval;
}

static int _send_file_data(int type, int data_len, const char* path) {
    unsigned char* data = (unsigned char*)malloc(data_len);
    ex_log(LOG_DEBUG, "%s [%d] (%p) %d", __func__, type, data, data_len);

    int retval = get_file(path, data, &data_len);

    if (FINGERPRINT_RES_SUCCESS != retval) {
        ex_log(LOG_ERROR, "get_file retval = %d", retval);
        return retval;
    }
    retval = opm_set_data(type, data, data_len);
    ex_log(LOG_DEBUG, "opm_set_data retval = %d", retval);
    free(data);
    return retval;
}

static int _templ_upgrade() {
    char file_name[MAX_PATH_LEN] = {0};
    char user_path[MAX_PATH_LEN] = {0};
    int i, j, retval;
    int data_len = 200 * 200 * 2 + 12 + 100;
    strncpy(user_path, g_user_path, MAX_PATH_LEN);
    templ_info_t tmpl_info;
    int out_size = sizeof(tmpl_info);

    ex_log(LOG_DEBUG, "%s enter", __func__);
    retval = opm_get_data(TYPE_RECEIVE_TEMPL_INFO, NULL, 0, (unsigned char*)&tmpl_info, &out_size);
    ex_log(LOG_DEBUG, "%s tmpl_info.fingerid %u, max_count %d", __func__, tmpl_info.fingerid,
           tmpl_info.max_enroll_count);

    int num_encryp_images = 15;
    retval = opm_set_data(TYPE_SEND_ENCRY_IMAGE_START, (unsigned char*)&num_encryp_images,
                          sizeof(num_encryp_images));
    for (i = 0; i < tmpl_info.max_enroll_count + 5; i++) {
        j = i;
        if (j >= tmpl_info.max_enroll_count) j -= tmpl_info.max_enroll_count;

        snprintf(file_name, MAX_PATH_LEN, "%s/%d/%03d", user_path, tmpl_info.fingerid, j);
        retval = _send_file_data(TYPE_SEND_ENCRY_IMAGE, data_len, file_name);
        if (retval != FINGERPRINT_RES_SUCCESS) {
            ex_log(LOG_ERROR, "Failed to send data, %d", file_name);
            break;
        }
    }
    retval = opm_set_data(TYPE_SEND_ENCRY_IMAGE_END, NULL, 0);
    return retval;
}

int trustonic_send_data(int type, unsigned char* in_data, int in_data_len) {
    int retval;
    unsigned char* data = NULL;
    char file_name[MAX_PATH_LEN] = {0};
    char user_path[MAX_PATH_LEN] = {0};
    int data_len;

    strncpy(user_path, g_user_path, MAX_PATH_LEN);

    switch (type) {
        case TYPE_SEND_SCRATCH:
        case TYPE_SEND_CALIBRATION_DATA:
        case TYPE_SEND_DEBASE: {
            unsigned char* pname = NULL;
            if (type == TYPE_SEND_CALIBRATION_DATA) {
                data_len = MAX_CALIBRATION_DATA_SIZE;
                pname = CALIBRATION_NAME;
            } else if (type == TYPE_SEND_SCRATCH) {
                data_len = MAX_SCRATCH_SIZE;
                pname = SCRATCH_NAME;
            } else if (type == TYPE_SEND_DEBASE) {
                data_len = MAX_DEBASE_SIZE;
                pname = DEBASE_NAME;
            } else
                break;

            data = (unsigned char*)malloc(data_len);
            retval = get_file(pname, data, &data_len);

            ex_log(LOG_DEBUG, "get_file retval = %d", retval);
            if (FINGERPRINT_RES_SUCCESS != retval) return retval;

            retval = opm_set_data(type, data, data_len);
            ex_log(LOG_DEBUG, "opm_set_data retval = %d", retval);
        } break;
        case TYPE_SEND_BDS: {
            int offset = 0;
            int send_len = RECEIVE_SEND_DATA_SIZE_ONCE;
            int send_flag = 0;

            data_len = MAX_BDS_SIZE;
            data = (unsigned char*)malloc(data_len);
            if (NULL == data) {
                retval = FINGERPRINT_RES_ALLOC_FAILED;
                break;
            }

            DIR* dir = opendir(user_path);
            struct dirent* ptr = NULL;

            if (dir == NULL) {
                retval = FINGERPRINT_RES_FAILED;
                break;
            }
            ex_log(LOG_DEBUG, "user path = %s", user_path);

            snprintf(file_name, MAX_PATH_LEN, "%s/%s", user_path, BDS_NAME);
            offset = 0;
            data_len = MAX_BDS_SIZE;
            retval = get_file(file_name, data, &data_len);

            ex_log(LOG_DEBUG, "filename = %s,ret = %d,data_len = %d", file_name, retval, data_len);
            if (retval == FINGERPRINT_RES_SUCCESS) {
                retval = opm_set_data(TYPE_SEND_BDS_START, (unsigned char*)&data_len,
                                      sizeof(unsigned int));
                do {
                    send_len = (data_len - offset < RECEIVE_SEND_DATA_SIZE_ONCE)
                                   ? data_len - offset
                                   : RECEIVE_SEND_DATA_SIZE_ONCE;

                    retval = opm_set_data(TYPE_SEND_BDS, data + offset, send_len);

                    offset += send_len;

                    ex_log(LOG_DEBUG, "template offset  %d ,send_len %d,data_len=%d，retval=%d",
                           offset, send_len, data_len, retval);
                } while (data_len - offset > 0);
                retval = opm_set_data(TYPE_SEND_BDS_END, NULL, 0);
            }
            ex_log(LOG_DEBUG, "template filename  %s ,ret %d ,%d", file_name, retval, data_len);

        } break;
        case TYPE_SEND_TEMPLATE: {
            int offset = 0;
            int send_len = RECEIVE_SEND_DATA_SIZE_ONCE;
            int send_flag = 0;
            retval = opm_set_data(TYPE_DELETE_TEMPLATE, NULL, 0);
            if (retval != FINGERPRINT_RES_SUCCESS) {
                break;
            }

            data_len = MAX_TEMPLATE_DATA_STRUCT_SIZE;
            data = (unsigned char*)malloc(data_len);
            if (NULL == data) {
                retval = FINGERPRINT_RES_ALLOC_FAILED;
                break;
            }

            DIR* dir = opendir(user_path);
            struct dirent* ptr = NULL;

            if (dir == NULL) {
                retval = FINGERPRINT_RES_FAILED;
                break;
            }
            ex_log(LOG_DEBUG, "user path = %s", user_path);
            while ((ptr = readdir(dir)) != NULL) {
                if (strcmp(ptr->d_name, ".") == 0 || strcmp(ptr->d_name, "..") == 0) {
                    continue;
                }

                if (ptr->d_type != 8 || strncmp(ptr->d_name, "tp_", strlen("tp_")) != 0) {
                    continue;
                }

                snprintf(file_name, MAX_PATH_LEN, "%s/%s", user_path, ptr->d_name);
                offset = 0;
                data_len = MAX_TEMPLATE_DATA_STRUCT_SIZE;
                retval = get_file(file_name, data, &data_len);

                ex_log(LOG_DEBUG, "template filename  %s ,ret %d ,%d", file_name, retval, data_len);
                if (retval == FINGERPRINT_RES_SUCCESS) {
                    retval = opm_set_data(TYPE_SEND_TEMPLATE_START, (unsigned char*)&data_len,
                                          sizeof(unsigned int));
                    do {
                        send_len = (data_len - offset < RECEIVE_SEND_DATA_SIZE_ONCE)
                                       ? data_len - offset
                                       : RECEIVE_SEND_DATA_SIZE_ONCE;

                        retval = opm_set_data(TYPE_SEND_TEMPLATE, data + offset, send_len);

                        offset += send_len;

                        ex_log(LOG_DEBUG, "template offset  %d ,send_len %d,data_len=%d，retval=%d",
                               offset, send_len, data_len, retval);
                    } while (data_len - offset > 0);
                    retval = opm_set_data(TYPE_SEND_TEMPLATE_END, NULL, 0);
                    ex_log(LOG_DEBUG, "TYPE_SEND_TEMPLATE_END template filename  %s ,ret %d ,%d",
                           file_name, retval, data_len);
                    if (retval == EGIS_TEMPL_NEED_UPGRADED) {
                        retval = _templ_upgrade();
                        if (retval == EGIS_OK) {
                            ex_log(LOG_DEBUG, "_templ_upgrade succeed");
                            trustonic_receive_data(TYPE_RECEIVE_DEBASE, NULL, 0);
                            // TODO: TYPE_RECEIVE_TEMPLATE
                        }
                    } else if (retval != FINGERPRINT_RES_SUCCESS) {
                        ex_log(LOG_ERROR, "template filename  %s ,ret %d ,%d", file_name, retval,
                               data_len);
                    }
                } else {
                    ex_log(LOG_ERROR, "template filename  %s ,ret %d ,%d", file_name, retval,
                           data_len);
                }
            }
        } break;
        case TYPE_SEND_USER_INFO: {
            ex_log(LOG_DEBUG, "TYPE_SEND_USER_INFO enter");
            // char user_path[MAX_PATH_LEN] = {0};
            unsigned int user_id = 0;
            if (in_data == NULL || in_data_len < (int)sizeof(int)) {
                ex_log(LOG_ERROR, "TYPE_SEND_USER_INFO invalid param");
                return FINGERPRINT_RES_INVALID_PARAM;
            }

            user_id = *((unsigned int*)in_data);
            snprintf(file_name, MAX_PATH_LEN, "%s/%s_%u", user_path, USERINFO_NAME, user_id);

            ex_log(LOG_DEBUG, "TYPE_SEND_USER_INFO path : %s", file_name);
            data_len = MAX_GROUP_INFO_SIZE;
            data = (unsigned char*)malloc(data_len);
            if (NULL == data) {
                retval = FINGERPRINT_RES_ALLOC_FAILED;
                break;
            }

            retval = get_file(file_name, data, &data_len);
            if (retval == FINGERPRINT_RES_SUCCESS) {
                ex_log(LOG_DEBUG, " TYPE_SEND_USER_INFO send user info to ta");
                retval = opm_set_data(TYPE_SEND_USER_INFO, data, data_len);
            }
            ex_log(LOG_DEBUG, "TYPE_SEND_USER_INFO leave %d", retval);
        } break;
        default:
            retval = FINGERPRINT_RES_INVALID_PARAM;
            break;
    }

    if (NULL != data) {
        free(data);
        data = NULL;
    }

    return retval;
}
