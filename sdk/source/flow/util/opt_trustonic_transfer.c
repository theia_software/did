#include "opt_trustonic_transfer.h"
#include "opt_file.h"
#include "plat_log.h"
#include "response_def.h"

int opt_turstoinc_data_transfer(data_transfer_timing_t transfer_timing, unsigned char* in_data,
                                int in_data_len) {
    int retval = FINGERPRINT_RES_SUCCESS, retval_cali = FINGERPRINT_RES_SUCCESS;

    switch (transfer_timing) {
        case DATA_TRANSFER_TA_INIT: {
            retval_cali = opt_send_data(TYPE_SEND_CALIBRATION_DATA, NULL, 0);
#ifndef ALGO_GEN_4
            retval = opt_send_data(TYPE_SEND_BDS, NULL, 0);
            retval = opt_send_data(TYPE_SEND_SCRATCH, NULL, 0);
#else
            retval = opt_send_data(TYPE_SEND_DEBASE, NULL, 0);
#endif
            if (retval_cali == FINGERPRINT_RES_OPEN_FILE_FAILED)
                retval = FINGERPRINT_RES_OPEN_FILE_FAILED;
        } break;

        case DATA_TRANSFER_TA_INIT_FINISH: {
            retval = opt_receive_data(TYPE_RECEIVE_CALIBRATION_DATA, NULL, 0, NULL, 0);
        } break;

        case DATA_TRANSFER_VERIFY_FINISH: {
            opt_receive_data(TYPE_RECEIVE_TEMPLATE, in_data, in_data_len, NULL, 0);
        } break;

        case DATA_TRANSFER_ENROLL_FINISH: {
            opt_receive_data(TYPE_RECEIVE_USER_INFO, in_data, sizeof(unsigned int), NULL, 0);
            opt_receive_data(TYPE_RECEIVE_TEMPLATE, (in_data + sizeof(unsigned int)),
                             sizeof(unsigned int), NULL, 0);
#ifdef ALGO_GEN_4
            opt_receive_data(TYPE_RECEIVE_DEBASE, NULL, 0, NULL, NULL);
#endif
        } break;

        case DATA_TRANSFER_CALI_FINISH: {
#ifndef ALGO_GEN_4
            int retval_bds = opt_receive_data(TYPE_RECEIVE_BDS, NULL, 0, NULL, NULL);
            if (retval_bds != FINGERPRINT_RES_SUCCESS &&
                retval_bds != FINGERPRINT_RES_NO_NEED_UPDATE)
                retval = retval_bds;

            int retval_scratch = opt_receive_data(TYPE_RECEIVE_SCRATCH, NULL, 0, NULL, NULL);
            if (retval_scratch != FINGERPRINT_RES_SUCCESS &&
                retval_scratch != FINGERPRINT_RES_NO_NEED_UPDATE)
                retval = retval_scratch;
            ex_log(LOG_INFO, "%s, receive bds %d ,receive scratch %d, %d", __func__, retval_bds,
                   retval_scratch, retval);
#endif
        } break;
        case DATA_TRSNSFER_REMOVE_FINGER_FINISH: {
            retval =
                opt_receive_data(TYPE_RECEIVE_USER_INFO, in_data, sizeof(unsigned int), NULL, 0);
            retval = opt_receive_data(TYPE_DELETE_TEMPLATE, in_data + sizeof(unsigned int),
                                      sizeof(unsigned int), NULL, 0);
        } break;

        case DATA_TRSNSFER_CHK_SID: {
            retval = opt_receive_data(TYPE_RECEIVE_USER_INFO, in_data, in_data_len, NULL, 0);
        } break;

        default:
            break;
    }

    return retval;
}