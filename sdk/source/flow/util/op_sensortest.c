#include "op_sensortest.h"
#include <stdlib.h>

#include "common_definition.h"
#include "device_int.h"
#include "fp_definition.h"
#include "fps_normal.h"
#include "op_manager.h"
#include "op_sensortest.h"
#include "plat_file.h"
#include "plat_log.h"
#include "plat_mem.h"
#include "plat_time.h"
#include "response_def.h"
#ifdef __TRUSTONIC__
#include "opt_trustonic_transfer.h"
#endif
#if defined(__ET7XX__) && !defined(__ET0XX__)
#include <sys/stat.h>
#include <sys/types.h>
#include <time.h>
#include <unistd.h>
#include <fd_process.h>
#include "7xx_sensor_test_definition.h"
#include "captain.h"
extern event_callbck_t g_event_callback;
#endif
#define FP_SENSORTEST_FINGERON_COUNT 3
#define SENSORTEST_DEFAULT_TIEM_OUT 6 * 1000
#define SENSOR_NEED_RESET 21
#define LITTLE_TO_BIG_ENDIAN(x) (((x >> 8) & 0xFF) + ((x << 8) & 0xFF00))

extern int g_hdev;

static int sensortest_send_cmd(int cmd) {
    ex_log(LOG_DEBUG, "%s enter!", __func__);
    int retval = FP_LIB_ERROR_GENERAL;
    int in_data[2];
    memset(in_data, 0, 2);
    in_data[0] = cmd;
    int in_data_size = 2;
    unsigned char out_data[64] = {0};
    int out_data_size = 64;

    retval = opm_extra_command(PID_INLINETOOL, (unsigned char*)in_data, in_data_size, out_data,
                               &out_data_size);
    ex_log(LOG_DEBUG, "%s opm_extra_command retval = %d", __func__, retval);
    if (FP_LIB_OK == retval || SENSOR_NEED_RESET == retval) {
        memcpy(&retval, out_data, sizeof(int));
    } else {
        retval = MMI_TEST_FAIL;
    }
    ex_log(LOG_DEBUG, "%s retval = %d", __func__, retval);
    return retval;
}

static int sensortest_check_finger_on(int idev) {
    int retval = MMI_TEST_FAIL;
    int test_count = 0;

    retval = sensortest_send_cmd(FP_MMI_FOD_TEST);
    if (MMI_TEST_SUCCESS != retval) {
        retval = MMI_TEST_FAIL;
        goto EXIT;
    }

    retval = FINGERPRINT_RES_TIMEOUT;
    do {
        retval = fp_device_interrupt_enable(idev, FALSE);
        retval = fp_device_interrupt_enable(idev, TRUE);
        retval = fp_device_interrupt_wait(idev, SENSORTEST_DEFAULT_TIEM_OUT);
        test_count++;
        if (FINGERPRINT_RES_SUCCESS == retval) break;
    } while (test_count < FP_SENSORTEST_FINGERON_COUNT);

    if (FINGERPRINT_RES_SUCCESS == retval) {
        retval = sensortest_send_cmd(FP_MMI_GET_FINGER_IMAGE);
        if (MMI_TEST_SUCCESS != retval) retval = MMI_TEST_FAIL;
    } else {
        retval = MMI_TEST_FAIL;
    }

EXIT:
    ex_log(LOG_DEBUG, "%s retval = %d", __func__, retval);
    return retval;
}

static int sensortest_wait_finger_on(int times_ms) {
    int retval = MMI_TEST_FAIL;
    int test_count = times_ms / 30;

    retval = sensortest_send_cmd(FP_MMI_FOD_TEST);
    if (MMI_TEST_SUCCESS != retval) {
        retval = MMI_TEST_FAIL;
        goto EXIT;
    }

    if (!wait_trigger(test_count, 30, TIMEOUT_WAIT_FOREVER))
        retval = MMI_TEST_FAIL;
    else
        retval = MMI_TEST_SUCCESS;

EXIT:
    ex_log(LOG_DEBUG, "%s retval = %d", __func__, retval);
    return retval;
}

static int sensortest_wait_interrupt(trigger_info_t* trigger_info) {
    int retval = MMI_TEST_FAIL;
    int try_count = 0;
    int trigger_type = TRIGGER_RESET;
    int time_interval = 30;

    if (trigger_info != NULL) {
        try_count = trigger_info->wait_time / trigger_info->time_interval;
        trigger_type = trigger_info->trigger_type;
        time_interval = trigger_info->time_interval > 0 ? trigger_info->time_interval : 30;
    }
    ex_log(LOG_DEBUG, "%s start,try_count %d,type %d ,time_interval %d ", __func__, retval,
           trigger_type, time_interval);

    fp_set_interrupt_trigger_type(trigger_type);

    if (!wait_trigger(try_count, time_interval, TIMEOUT_WAIT_FOREVER))
        retval = MMI_TEST_FAIL;
    else
        retval = MMI_TEST_SUCCESS;

    fp_set_interrupt_trigger_type(TRIGGER_RESET);
    ex_log(LOG_DEBUG, "%s end ,wait result %d ", __func__, retval);
    return retval;
}

static int sensortest_get_image_info(unsigned char* buffer, int* buffer_size){
    if (NULL == buffer || NULL == buffer_size) {
        ex_log(LOG_DEBUG, "%s invalid param", __func__);
        return MMI_TEST_FAIL;
    }

    int retval;
    int in_data[1] = {FP_GET_IMAGE_SIZE_INFO};
    int in_data_size = sizeof(in_data);

    retval = opm_extra_command(PID_INLINETOOL, (unsigned char*)in_data, in_data_size, buffer,
                               buffer_size);
    if (FP_LIB_OK == retval) {
        retval = MMI_TEST_SUCCESS;
    } else {
        retval = MMI_TEST_FAIL;
    }

    ex_log(LOG_DEBUG, "%s retval = %d", __func__, retval);
    return retval;
}

static int sensortest_get_image(unsigned char* entry_data, unsigned char* buffer,
                                int* buffer_size) {
    if (NULL == buffer || NULL == buffer_size) {
        ex_log(LOG_DEBUG, "%s invalid param", __func__);
        return MMI_TEST_FAIL;
    }

    int entry_data0 = ((int*)entry_data)[0] ;
    int image_test_type = ((int*)entry_data)[1];
    int x_ratio_x100 = ((int*)entry_data)[2];
    int y_ratio_x100 = ((int*)entry_data)[3];
    int retval = FINGERPRINT_RES_FAILED;
    int in_data[4] = {0};


    in_data[0] = FP_CAPTURE_IMG;
    in_data[1] = image_test_type;
    in_data[2] = x_ratio_x100;
    in_data[3] = y_ratio_x100;



    int in_data_size = sizeof(in_data);

    if ( entry_data0 == FP_CAPTURE_IMG_RBS_760_FFC_IMAGE ) {
        // if entry_data0 is FP_CAPTURE_IMG_RBS_760_FFC_IMAGE, change in_data[0], for et760 liveview only
        in_data[0] = FP_CAPTURE_IMG_RBS_760_FFC_IMAGE;
    }
    else if (entry_data0 == FP_CAPTURE_IMG_RBS_760_FFC_IMAGE_NO_UPDATE_BDS ) {
        in_data[0] = FP_CAPTURE_IMG_RBS_760_FFC_IMAGE_NO_UPDATE_BDS ;
    }
    retval = opm_extra_command(PID_INLINETOOL, (unsigned char*)in_data, in_data_size, buffer,
                               buffer_size);
    if (FP_LIB_OK == retval) {
        retval = MMI_TEST_SUCCESS;
    } else {
        retval = MMI_TEST_FAIL;
    }

    ex_log(LOG_DEBUG, "%s retval = %d", __func__, retval);

    return retval;
}

static int sensortest_set_crop_info(unsigned char* entry_data) {
    int retval = FINGERPRINT_RES_FAILED;
    unsigned char out_data[64] = {0};
    int out_data_size = 64;
    int in_data[4] = {0};
    in_data[0] = FP_MMI_SET_CROP_INFO;
    in_data[1] = ((int*)entry_data)[1];
    in_data[2] = ((int*)entry_data)[3];
    in_data[3] = ((int*)entry_data)[4];
    int in_data_size = sizeof(in_data);

    retval = opm_extra_command(PID_INLINETOOL, (unsigned char*)in_data, in_data_size, out_data,
                               &out_data_size);
    if (FP_LIB_OK == retval) {
        retval = MMI_TEST_SUCCESS;
    } else {
        retval = MMI_TEST_FAIL;
    }

    ex_log(LOG_DEBUG, "%s retval = %d", __func__, retval);

    return retval;
}

static int sensortest_get_nvm_uid(unsigned char* buffer, int* buffer_size) {
    if (NULL == buffer || NULL == buffer_size) {
        ex_log(LOG_DEBUG, "%s invalid param", __func__);
        return MMI_TEST_FAIL;
    }

    int retval = FINGERPRINT_RES_FAILED;
    int in_data[2] = {0};

    memset(in_data, 0, 2);
    in_data[0] = FP_MMI_GET_NVM_UID;
    int in_data_size = sizeof(in_data);

    retval = opm_extra_command(PID_INLINETOOL, (unsigned char*)in_data, in_data_size, buffer,
                               buffer_size);
    if (FP_LIB_OK == retval) {
        retval = MMI_TEST_SUCCESS;
    } else {
        retval = MMI_TEST_FAIL;
    }

    ex_log(LOG_DEBUG, "%s retval = %d", __func__, retval);

    return retval;
}

int sensor_test_opation(int cid, int idev, unsigned char* in_data, int in_data_size,
                        unsigned char* buffer, int* buffer_size) {
    ex_log(LOG_DEBUG, "%s enter, dev = %d, in_data_size %d", __func__, idev, in_data_size);
    int retval = MMI_TEST_FAIL;
    switch (cid) {
        case SENSORTEST_DIRTYDOTS_TEST:
            retval = sensortest_send_cmd(FP_MMI_DIRTYDOTS_TEST);
            break;

        case SENSORTEST_READ_REV_TEST:
            retval = sensortest_send_cmd(FP_MMI_READ_REV_TEST);
            break;

        case SENSORTEST_REGISTER_RW_TEST: {
            retval = sensortest_send_cmd(FP_MMI_REGISTER_RW_TEST);
            if (SENSOR_NEED_RESET == retval) {
                retval = fp_device_reset(idev);
                retval = sensortest_send_cmd(FP_MMI_REGISTER_RW_TEST);
            }
            retval = fp_device_reset(idev);
            retval = sensortest_send_cmd(FP_MMI_REGISTER_RECOVERY);
        } break;

        case SENSORTEST_CHECK_FINGER_ON:
            retval = sensortest_check_finger_on(idev);
            break;
        case SEBSORTEST_GET_IMAGE_SIZE_INFO:
            retval = sensortest_get_image_info(buffer, buffer_size);
            break;
        case SENSORTEST_760_GET_FFC_IMAGE:
        case SENSORTEST_760_GET_FFC_IMAGE_NO_UPDATE_BDS:
        case SENSORTEST_GET_IMAGE:
            retval = sensortest_get_image(in_data, buffer, buffer_size);
            break;
        case SENSORTEST_WAIT_INTERRUTP: {
            // int in_0 = ((int *)in_data)[0];
            int in_1 = ((int*)in_data)[1];

            retval = sensortest_wait_finger_on(in_1);
        } break;
        case SENSORTEST_START_INTERRUTP:
            retval = sensortest_send_cmd(FP_MMI_FOD_TEST);
            break;
        case SENSORTEST_TEST_INTERRUTP: {
            trigger_info_t* trigger_info = (trigger_info_t*)&(((int*)in_data)[1]);

            retval = sensortest_wait_interrupt(trigger_info);
        } break;
        case SENSORTEST_STOP_INTERRUTP:

            break;
        case SENSORTEST_SET_CROP_INFO:
            retval = sensortest_set_crop_info(in_data);
            break;
        case SENSORTEST_GET_NVM_UID:
            retval = sensortest_get_nvm_uid(buffer, buffer_size);
            break;

        default:
            ex_log(LOG_DEBUG, "sensortest tool unkown cmd");
            break;
    }

    ex_log(LOG_DEBUG, "%s retval = %d", __func__, retval);

    return MMI_TEST_SUCCESS == retval ? FINGERPRINT_RES_SUCCESS : FINGERPRINT_RES_FAILED;
}

int flow_inline_legacy_cmd(int cmd, int param1, int param2, int param3, unsigned char* out_buf,
                           int* out_size) {
    int retval = FP_LIB_ERROR_GENERAL;
    int in_data_size;
    int in_data[4];
    in_data[0] = cmd;
    in_data[1] = param1;
    in_data[2] = param2;
    in_data[3] = param3;
    unsigned char* out_data = NULL;
    int out_data_size;
    in_data_size = sizeof(in_data);

    ex_log(LOG_DEBUG, "%s [%d] %d, %d, %d", __func__, cmd, param1, param2, param3);

    if (out_buf == NULL || out_size == NULL) {
        ex_log(LOG_DEBUG, "flow_inline_legacy_cmd out_data = plat_alloc(4);");
        out_data = plat_alloc(4);
        out_data_size = 4;
    } else {
        ex_log(LOG_DEBUG, "%s, out_size=%d", __func__, *out_size);
        out_data = out_buf;
        out_data_size = *out_size;
    }

    retval = opm_extra_command(PID_INLINETOOL, (unsigned char*)in_data, in_data_size, out_data,
                               &out_data_size);
    if (FP_LIB_OK != retval) {
        ex_log(LOG_ERROR, "%s, retval = %d", __func__, retval);
    }

#ifdef __TRUSTONIC__
    if (cmd == FP_INLINE_SENSOR_CALIBRATE) {
        retval = opt_turstoinc_data_transfer(DATA_TRANSFER_CALI_FINISH, NULL, 0);
    }
#endif

    ex_log(LOG_DEBUG, "%s, retval = %d", __func__, retval);
    if ((out_size != NULL) && (*out_size >= out_data_size)) {
        *out_size = out_data_size;
    }
    if (out_data != out_buf) {
        PLAT_FREE(out_data);
    }
    return retval;
}
#if defined(__ET7XX__) && !defined(__ET0XX__)

#define FACTORY_TEST_EVT_SNSR_TEST_SCRIPT_START 0x11000003
/* Output event to java AP. It means test is start. */
#define FACTORY_TEST_EVT_SNSR_TEST_SCRIPT_END 0x11000004
/* Output event to java AP. It means test is end. */
#define FACTORY_TEST_EVT_SNSR_TEST_PUT_WKBOX 0x11000006
#define FACTORY_TEST_EVT_SNSR_TEST_PUT_BKBOX 0x11000007
#define FACTORY_TEST_EVT_SNSR_TEST_PUT_CHART 0x11000008
#define FACTORY_TEST_EVT_SNSR_TEST_FLASH_TEST 0x11000009

/* Output event to java AP. It means put the robber on sensor. */
#define EVT_ERROR 2020
// Copy from CS1 jni\common\platform\inc\egis_definition.h
#define SAVE_AND_PRINT_LOG(format, ...)                \
    do {                                               \
        ex_log(LOG_INFO, format, ##__VA_ARGS__);       \
        __inline_save_log_data(format, ##__VA_ARGS__); \
    } while (0)
/* Output event to java AP. It means test is error. */

static void notify_callback(int event_id, int first_param, int second_param, unsigned char* data,
                            int data_size) {
    ex_log(LOG_DEBUG, "%s, event_id = %d", __func__, event_id);

    if (NULL != g_event_callback) {
#ifndef EGIS_DBG
        if (event_id == EVENT_RETURN_IMAGE || event_id == EVENT_RETURN_LIVE_IMAGE) return;
#endif
        g_event_callback(event_id, first_param, second_param, data, data_size);
    }
#ifdef __ENABLE_NAVIGATION__
    if (event_id == EVENT_NAVIGATION) send_navi_event_to_driver(first_param);
#endif
}

static void __inline_save_log_data(const char* pImage, ...) {
    static char buf[1000] = {0};
    if (pImage == NULL) {
        ex_log(LOG_INFO, "inline_save_log_data pImage = NULL");
    }
    va_list arg;
    va_start(arg, pImage);
    unsigned int pImage_size = vsnprintf((char*)buf, 100, pImage, arg);
    va_end(arg);

    FILE* fn = fopen(INLINE_LOG_PATH, "ab");
    if (fn != NULL) {
        unsigned int size = fwrite(buf, 1, pImage_size, fn);
        fclose(fn);
        ex_log(LOG_INFO, "inline_save_log_data done size = %d", size);
    }
}
static void init_7xx_test(int* result) {
    int ret = FP_LIB_ERROR_GENERAL, len = sizeof(int), path_len = 10;
    unsigned char path[10] = {0};

    ret = mkdir(DEFINE_FILE_PATH, S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
    ex_log(LOG_INFO, "init_7xx_test ret=%d, %s", ret, DEFINE_FILE_PATH);
    *result = 0;
    ret = flow_inline_legacy_cmd(FP_INLINE_7XX_CASE_INIT, 0, 0, 0, (unsigned char*)result, &len);
    ex_log(LOG_INFO, "FP_INLINE_7XX_CASE_INIT ret=%d, buffer.result = %d", ret, *result);

    ret = flow_inline_legacy_cmd(FP_INLINE_7XX_GET_UUID, 0, 0, 0, path, &path_len);
    ex_log(LOG_INFO, "FP_INLINE_7XX_GET_UUID ret=%d, path_len = %d", ret, path_len);

    SAVE_AND_PRINT_LOG("ET713_UUID==%x%02x%02x%02x%02x%02x%02x%02x%02x%02x\r\n", path[0], path[1],
                       path[2], path[3], path[4], path[5], path[6], path[7], path[8], path[9]);
    ex_log(LOG_INFO, "init_7xx_test ret end");

    // ret = set_hbm(TRUE); //customized by project
}

static void uninit_7xx_test(int* result) {
    int ret = FP_LIB_ERROR_GENERAL, len = sizeof(int);

    *result = 0;
    ret = flow_inline_legacy_cmd(FP_INLINE_7XX_CASE_UNINIT, 0, 0, 0, (unsigned char*)result, &len);
    ex_log(LOG_INFO, "FP_INLINE_7XX_CASE_UNINIT ret=%d, buffer.result = %d", ret, *result);
    // ret = set_hbm(FALSE); //customized by project
}

static void __inline_print_log_file(struct sensor_test_output* buffer_out_data) {
    ex_log(LOG_INFO, "[inline_print_log_file]");
    SAVE_AND_PRINT_LOG("[SNR_RESULT] expo:%.3f, hw_int:%d, Lens_Centroid=[%d,%d]\r\n",
                       buffer_out_data->data.expo, buffer_out_data->data.hw_int,
                       buffer_out_data->data.centroid_x, buffer_out_data->data.centroid_y);
    SAVE_AND_PRINT_LOG("[SNR_RESULT] calibration bkg_cx,y=(%d,%d)\r\n",
                       buffer_out_data->data.bkg_cx, buffer_out_data->data.bkg_cy);
    SAVE_AND_PRINT_LOG("[SNR_RESULT] FEA, x1x2 = (%.1f,%.1f), y1y2=(%.1f,%.1f)\r\n",
                       buffer_out_data->data.fov_x1_result, buffer_out_data->data.fov_x2_result,
                       buffer_out_data->data.fov_y1_result, buffer_out_data->data.fov_y2_result);
    SAVE_AND_PRINT_LOG("[SNR_RESULT] PERIOD = %.3f\r\n", buffer_out_data->data.period);
    SAVE_AND_PRINT_LOG("[SNR_RESULT] signal=%.2f, noise=%.2f, snr=%.2f\r\n",
                       buffer_out_data->data.signal, buffer_out_data->data.noise,
                       buffer_out_data->data.snr);
    SAVE_AND_PRINT_LOG(
        "[SNR_RESULT] BadBlockNum = %d, BadPixelMaxContinu = %d, BadPixelNum = %d\r\n",
        buffer_out_data->data.bad_block_cnt, buffer_out_data->data.bad_pxl_max_continu_cnt,
        buffer_out_data->data.bad_pxl_cnt);
    SAVE_AND_PRINT_LOG("[SNR_RESULT] avg_intensity=%d, bkg_avg_intensity=%d\r\n",
                       buffer_out_data->data.avg_intensity,
                       buffer_out_data->data.bkg_avg_intensity);
}
static void __send_7XX_sensortest_event(int event_id, unsigned char* test_result, int data_size) {
    ex_log(LOG_INFO, "event_id = %d", event_id);
    switch (event_id) {
        case FP_INLINE_7XX_SNR_INIT:
            notify_callback(FACTORY_TEST_EVT_SNSR_TEST_PUT_WKBOX, 0, 0, 0, 0);
            break;
        case FP_INLINE_7XX_SNR_WKBOX_ON:
            notify_callback(FACTORY_TEST_EVT_SNSR_TEST_PUT_BKBOX, 0, 0, 0, 0);
            break;
        case FP_INLINE_7XX_SNR_BKBOX_ON:
            notify_callback(FACTORY_TEST_EVT_SNSR_TEST_PUT_CHART, 0, 0, 0, 0);
            break;
        case FP_INLINE_7XX_SNR_CHART_ON:
            notify_callback(FACTORY_TEST_EVT_SNSR_TEST_FLASH_TEST, 0, 0, 0, 0);
            break;
        case FP_INLINE_7XX_FLASH_TEST:
            notify_callback(FACTORY_TEST_EVT_SNSR_TEST_SCRIPT_END, 0, 0, test_result, data_size);
            break;
        case EVT_ERROR:
            SAVE_AND_PRINT_LOG("[SNR_RESULT] ERROR, algo fail \r\n");
            notify_callback(EVT_ERROR, 0, 0, test_result, data_size);
            break;
        default:
            notify_callback(event_id, 0, 0, test_result, data_size);
            break;
    }
}

#define INLINE_CHECK_OVER_SPEC(val, max, min) ((val > max) ? 1 : ((val < min) ? 1 : 0))
static int __inline_check_result(struct sensor_test_output* buffer_out_data, int* err_index) {
    ex_log(LOG_INFO, "[inline_check_result]");
    if (INLINE_CHECK_OVER_SPEC(buffer_out_data->data.expo, EXPO_MAX, EXPO_MIN))
        *err_index |= (1 << EXPO_OVER_SPEC_SHIFT_BIT);

    if (INLINE_CHECK_OVER_SPEC(buffer_out_data->data.centroid_x, CENTROIDX_MAX, CENTROIDX_MIN))
        *err_index |= (1 << CENTROID_X_OVER_SPEC_SHIFT_BIT);

    if (INLINE_CHECK_OVER_SPEC(buffer_out_data->data.centroid_y, CENTROIDX_MAX, CENTROIDX_MIN))
        *err_index |= (1 << CENTROID_Y_OVER_SPEC_SHIFT_BIT);

    if (INLINE_CHECK_OVER_SPEC(buffer_out_data->data.fov_x1_result, FOVX1_MAX, FOVX1_MIN))
        *err_index |= (1 << FOV_X1_OVER_SPEC_SHIFT_BIT);

    if (INLINE_CHECK_OVER_SPEC(buffer_out_data->data.fov_x2_result, FOVX2_MAX, FOVX2_MIN))
        *err_index |= (1 << FOV_X2_OVER_SPEC_SHIFT_BIT);

    if (INLINE_CHECK_OVER_SPEC(buffer_out_data->data.fov_y1_result, FOVY1_MAX, FOVY1_MIN))
        *err_index |= (1 << FOV_Y1_OVER_SPEC_SHIFT_BIT);

    if (INLINE_CHECK_OVER_SPEC(buffer_out_data->data.fov_y2_result, FOVY2_MAX, FOVY2_MIN))
        *err_index |= (1 << FOV_Y2_OVER_SPEC_SHIFT_BIT);

    if (INLINE_CHECK_OVER_SPEC(buffer_out_data->data.period, PERIOD_MAX, PERIOD_MIN))
        *err_index |= (1 << PERIOD_OVER_SPEC_SHIFT_BIT);

    if (INLINE_CHECK_OVER_SPEC(buffer_out_data->data.signal, SIGNAL_MAX, SIGNAL_MIN))
        *err_index |= (1 << SIGNAL_OVER_SPEC_SHIFT_BIT);

    if (INLINE_CHECK_OVER_SPEC(buffer_out_data->data.noise, NOISE_MAX, NOISE_MIN))
        *err_index |= (1 << NOISE_OVER_SPEC_SHIFT_BIT);

    if (INLINE_CHECK_OVER_SPEC(buffer_out_data->data.snr, SNR_MAX, SNR_MIN))
        *err_index |= (1 << SNR_OVER_SPEC_SHIFT_BIT);

    if (INLINE_CHECK_OVER_SPEC(buffer_out_data->data.bad_block_cnt, BAD_BLOCK_CNT_MAX,
                               BAD_BLOCK_CNT_MIN))
        *err_index |= (1 << BAD_BLOCK_CNT_OVER_SPEC_SHIFT_BIT);

    if (INLINE_CHECK_OVER_SPEC(buffer_out_data->data.bad_pxl_max_continu_cnt,
                               BAD_PIXEL_MAC_CONTINU_CNT_MAX, BAD_PIXEL_MAC_CONTINU_CNT_MIN))
        *err_index |= (1 << BAD_PXL_MAX_CNT_OVER_SPEC_SHIFT_BIT);

    if (INLINE_CHECK_OVER_SPEC(buffer_out_data->data.bad_pxl_cnt, BAD_PIXEL_CNT_MAX,
                               BAD_PIXEL_CNT_MIN))
        *err_index |= (1 << BAD_PXL_CNT_OVER_SPEC_SHIFT_BIT);

    if (*err_index != 0) {
        ex_log(LOG_ERROR, "[SNR_RESULT] fail, test overspec, *err_index = %d", *err_index);
        SAVE_AND_PRINT_LOG("[SNR_RESULT] fail, test overspec = %d \r\n", *err_index);
    }
    return FP_LIB_OK;
}
#if defined(__INLINE_SAVE_LOG_ENABLE_DEFAULT__) || defined(__INLINE_SAVE_LOG_ENABLE_EXTRA_INFO__)
static void __inline_get_snr_images_v2(void) {
    char temp_path[FILE_NAME_LEN] = {0};
    int ret = 0;
    struct sensor_test_output* buffer_out_data;
    int buffer_out_length = sizeof(struct sensor_test_output);
    buffer_out_data = (struct sensor_test_output*)plat_alloc(sizeof(struct sensor_test_output));
    if (buffer_out_data == NULL) {
        ex_log(LOG_ERROR, "plat_alloc ERROR");
        return;
    }
    // 16bit
    ret = flow_inline_legacy_cmd(FP_INLINE_7XX_SNR_GET_ALL_IMAGE_16_BIT_INFO, 0, 0, 0,
                                 (unsigned char*)buffer_out_data, &buffer_out_length);

    while (buffer_out_data->picture_remaining_count) {
        ret = flow_inline_legacy_cmd(FP_INLINE_7XX_SNR_GET_ALL_IMAGE_16_BIT, 0, 0, 0,
                                     (unsigned char*)buffer_out_data, &buffer_out_length);
        if (ret != 0) ex_log(LOG_ERROR, "flow_inline_legacy_cmd ERROR, %d", ret);

        for (int j = 0; j < IMG_MAX_BUFFER_SIZE; j++) {
            buffer_out_data->picture_buffer_16[j] =
                LITTLE_TO_BIG_ENDIAN(buffer_out_data->picture_buffer_16[j]);
        }
        sprintf(temp_path, "%s%s", DEFINE_FILE_PATH, buffer_out_data->name);
        ex_log(LOG_DEBUG, "path=%s, count=%d", temp_path, buffer_out_data->picture_remaining_count);
        plat_save_raw_image(temp_path, (unsigned char*)buffer_out_data->picture_buffer_16,
                            IMG_MAX_BUFFER_SIZE * 2, 1);
        memset(temp_path, 0, sizeof(temp_path));
    }
    // 8bit
    ret = flow_inline_legacy_cmd(FP_INLINE_7XX_SNR_GET_ALL_IMAGE_8_BIT_INFO, 0, 0, 0,
                                 (unsigned char*)buffer_out_data, &buffer_out_length);

    while (buffer_out_data->picture_remaining_count) {
        ret = flow_inline_legacy_cmd(FP_INLINE_7XX_SNR_GET_ALL_IMAGE_8_BIT, 0, 0, 0,
                                     (unsigned char*)buffer_out_data, &buffer_out_length);
        if (ret != 0) ex_log(LOG_ERROR, "flow_inline_legacy_cmd ERROR, %d", ret);

        sprintf(temp_path, "%s%s", DEFINE_FILE_PATH, buffer_out_data->name);
        ex_log(LOG_DEBUG, "path=%s, count=%d", temp_path, buffer_out_data->picture_remaining_count);
        plat_save_raw_image(temp_path, (unsigned char*)buffer_out_data->picture_buffer_8,
                            IMG_MAX_BUFFER_SIZE, 1);
        memset(temp_path, 0, sizeof(temp_path));
    }

    PLAT_FREE(buffer_out_data);
}
#endif
static void __inline_get_snr_MT_cali_data(unsigned char* buffer, int* err_index) {
    int ret = 0;
    struct sensor_test_output* buffer_out_data;
    int buffer_out_length = sizeof(struct sensor_test_output);

    buffer_out_data = (struct sensor_test_output*)plat_alloc(sizeof(struct sensor_test_output));
    if (buffer_out_data == NULL) {
        ex_log(LOG_ERROR, "plat_alloc ERROR");
        return;
    }
    ret = flow_inline_legacy_cmd(FP_INLINE_7XX_SNR_GET_DATA, 0, 0, 0,
                                 (unsigned char*)buffer_out_data, &buffer_out_length);
    if (ret != 0) ex_log(LOG_ERROR, "flow_inline_legacy_cmd ERROR, %d", ret);

    __inline_check_result(buffer_out_data, err_index);
    __inline_print_log_file(buffer_out_data);
    if (buffer != NULL) {
        ex_log(LOG_DEBUG, "copy to out buffer");
        memcpy(buffer, (char*)&buffer_out_data->data, sizeof(final_MT_cali_data_t));
    }

    PLAT_FREE(buffer_out_data);
}

static void __inline_get_snr_data(int* err_index) {
    ex_log(LOG_INFO, "[inline_get_snr_data]GET_SNR_IMAGE_V2 ITEM");
    __inline_get_snr_MT_cali_data(NULL, err_index);
#if defined(__INLINE_SAVE_LOG_ENABLE_DEFAULT__) || defined(__INLINE_SAVE_LOG_ENABLE_EXTRA_INFO__)
    __inline_get_snr_images_v2();
#endif
}

#ifdef __INLINE_SAVE_LOG_ENABLE_DEFAULT__
static void __inline_get_norscan_data(void) {
    ex_log(LOG_INFO, "[inline_get_norscan_data]");
    char temp_path[FILE_NAME_LEN] = {0};
    int ret = 0;
    struct sensor_test_output* buffer_out_data = NULL;
    int buffer_out_length = sizeof(struct sensor_test_output);
    buffer_out_data = (struct sensor_test_output*)plat_alloc(sizeof(struct sensor_test_output));
    if (buffer_out_data == NULL) {
        ex_log(LOG_ERROR, "plat_alloc ERROR");
        return;
    }
    ret = flow_inline_legacy_cmd(FP_INLINE_7XX_NORMAL_GET_IMAGE, 0, 0, 0,
                                 (unsigned char*)buffer_out_data, &buffer_out_length);
    if (ret != 0) ex_log(LOG_ERROR, "flow_inline_legacy_cmd ERROR, %d", ret);

    for (int j = 0; j < IMG_MAX_BUFFER_SIZE; j++) {
        buffer_out_data->picture_buffer_16[j] =
            LITTLE_TO_BIG_ENDIAN(buffer_out_data->picture_buffer_16[j]);
    }
    sprintf(temp_path, "%s%s", DEFINE_FILE_PATH, buffer_out_data->name);
    plat_save_raw_image(temp_path, (unsigned char*)buffer_out_data->picture_buffer_16,
                        IMG_MAX_BUFFER_SIZE * 2, 1);
    memset(temp_path, 0, sizeof(temp_path));

    PLAT_FREE(buffer_out_data);
}
#endif
int do_7XX_sensortest(int cmd, int param1, int param2, int param3, unsigned char* out_buf,
                      int* out_size) {
    int ret = FP_LIB_ERROR_GENERAL;
    static unsigned long long time_diff_total = 0;
    int buffer_out_length = sizeof(int), test_result = 0, inline_err_index = 0;
    unsigned long long time_start = plat_get_time();
    ex_log(LOG_INFO, "cmd = %d", cmd);
    switch (cmd) {
        case FP_INLINE_7XX_NORMALSCAN:
            init_7xx_test(&test_result);
            fp_device_power_control(g_hdev, TRUE);
            fp_device_reset(g_hdev);
            usleep(500 * 000);
            ex_log(LOG_INFO, "FP_INLINE_7XX_NORMALSCAN Start");
            __send_7XX_sensortest_event(FACTORY_TEST_EVT_SNSR_TEST_SCRIPT_START, 0, 0);
            do {
                ret = flow_inline_legacy_cmd(cmd, param1, param2, param3,
                                             (unsigned char*)&test_result, &buffer_out_length);
                ex_log(LOG_INFO, "ret=%d, test_result = %d", ret, test_result);
                if (ret != FP_LIB_OK)
                    ex_log(LOG_ERROR, "ret=%d, test_result = %d", ret, test_result);

                switch (test_result) {
                    case EGIS_7XX_TEST_RESET:
                        ex_log(LOG_INFO, "EGIS_7XX_TEST_RESET");
                        fp_device_reset(g_hdev);
                        break;
                    case FP_LIB_OK:
#ifdef __INLINE_SAVE_LOG_ENABLE_DEFAULT__
                        __inline_get_norscan_data();
#endif
                        __send_7XX_sensortest_event(FACTORY_TEST_EVT_SNSR_TEST_SCRIPT_END,
                                                    (unsigned char*)&(inline_err_index),
                                                    sizeof(inline_err_index));
                        break;
                    case EGIS_7XX_TEST_REGISTER_TEST_FAIL:
                        inline_err_index |= (1 << REGISTER_TEST_SHIFT_BIT);
                        ex_log(LOG_ERROR, "EGIS_7XX_TEST_REGISTER_TEST_FAIL");
                        break;
                    case EGIS_7XX_TEST_OTP_TEST_FAIL:
                        inline_err_index |= (1 << OTP_TEST_SHIFT_BIT);
                        ex_log(LOG_ERROR, "EGIS_7XX_TEST_OTP_TEST_FAIL");
                        break;
                    case EGIS_7XX_TEST_TESTPATTERN_TEST_FAIL:
                        inline_err_index |= (1 << TESTPATTERN_TEST_SHIFT_BIT);
                        ex_log(LOG_ERROR, "EGIS_7XX_TEST_TESTPATTERN_TEST_FAIL");
                        break;
                }

                if (inline_err_index != FP_LIB_OK) {
                    __send_7XX_sensortest_event(EVT_ERROR, (unsigned char*)&(inline_err_index),
                                                sizeof(inline_err_index));
                }
            } while (!(test_result == FP_LIB_OK || inline_err_index != FP_LIB_OK));

            uninit_7xx_test(&test_result);
            fp_device_reset(g_hdev);
            opm_initialize_sensor();
            fp_device_power_control(g_hdev, FALSE);
            ex_log(LOG_INFO, "FP_INLINE_7XX_NORMALSCAN End");
            break;
        case FP_INLINE_7XX_SNR_INIT:
            init_7xx_test(&test_result);
            __send_7XX_sensortest_event(FP_INLINE_7XX_SNR_INIT, 0, 0);
#ifdef __INLINE_SAVE_LOG_ENABLE_EXTRA_INFO__
            ret = flow_inline_legacy_cmd(FP_INLINE_7XX_SNR_SAVE_LOG_ENABLE_EXTRA_INFO, 0, 0, 0,
                                         NULL, 0);
#endif
            break;
        case FP_INLINE_7XX_FLASH_TEST:
        case FP_INLINE_7XX_SNR_WKBOX_ON:
        case FP_INLINE_7XX_SNR_BKBOX_ON:
        case FP_INLINE_7XX_SNR_CHART_ON:
            fp_device_power_control(g_hdev, TRUE);
            ret = flow_inline_legacy_cmd(cmd, param1, param2, param3, (unsigned char*)&test_result,
                                         &buffer_out_length);
            if (ret != FP_LIB_OK)
                ex_log(LOG_ERROR, "FAIL, ret=%d, test_result=%d", ret, test_result);
            unsigned long long time_diff = plat_get_diff_time(time_start);
            SAVE_AND_PRINT_LOG("CMD = %d spent %d ms\r\n", cmd, time_diff);
            time_diff_total += time_diff;

            if ((test_result == FP_LIB_OK && cmd == FP_INLINE_7XX_FLASH_TEST) ||
                test_result == EGIS_7XX_TEST_SENSOR_TEST_FAIL) {
                __inline_get_snr_data(&inline_err_index);
                uninit_7xx_test(&test_result);
                time_diff = plat_get_diff_time(time_start);
                time_diff_total += time_diff;
                SAVE_AND_PRINT_LOG("get inline data, spent %d ms\r\n", time_diff);
                SAVE_AND_PRINT_LOG("Total Test spent %d ms\r\n", time_diff_total);
                time_diff_total = 0;
            }

            if (test_result != FP_LIB_OK) cmd = EVT_ERROR;
            __send_7XX_sensortest_event(cmd, (unsigned char*)&(inline_err_index),
                                        sizeof(inline_err_index));
            fp_device_power_control(g_hdev, FALSE);
            break;
        case FP_INLINE_7XX_SNR_GET_DATA:  // specific for demo apk calibration.
            ex_log(LOG_DEBUG, "FP_INLINE_7XX_SNR_GET_DATA Start");
            if (out_size != NULL) {
                ex_log(LOG_DEBUG, "out_size=%d", *out_size);
                if ((unsigned long)*out_size < sizeof(final_MT_cali_data_t)) {
                    ex_log(LOG_ERROR, "out_size is NULL");
                    break;
                }
            } else {
                ex_log(LOG_ERROR, "out_size is NULL");
                break;
            }
            __inline_get_snr_MT_cali_data(out_buf, &inline_err_index);
            ret = FP_LIB_OK;
            return ret;
        case FP_INLINE_7XX_CASE_UNINIT:
            uninit_7xx_test(&test_result);
            break;
        default:
            __send_7XX_sensortest_event(EVT_ERROR, (unsigned char*)&test_result, buffer_out_length);
            uninit_7xx_test(&test_result);
            ex_log(LOG_ERROR, "not support, cmd = %d", cmd);
            break;
    }

    ex_log(LOG_INFO, "ret = %d", ret);
    return ret;
}
#endif
