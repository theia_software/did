#ifndef __ET0XX__
#include "common_definition.h"
#include "fps_normal.h"
#include "packager.h"
#include "plat_log.h"
#include "plat_spi.h"
#include "response_def.h"
#include "windows.h"
/*
**	the differences between fps_interface_tz.c & fps_interface_nw.c is
**	egisFpDeviceOpen will go to exlib to get driver handle in normal world
*version
**	but it will open driver and hold a independent handle in tz version
*/

int spi_init_interrupt(int device_handle) {
    io_dispatch_connect(NULL);
    return 0;
}

static int _has_interrupt() {
    int ret;
    unsigned char value;

#ifdef __ET6XX__
    ret = io_5xx_dispatch_read_register(0x09, &value);

    if (ret != 0) return -3;
    // FOD:BIT2 to BIT5
    if (ret == 0 && (value & 0x3C)) {
        return 1;
    }
#else
    ret = io_5xx_dispatch_read_register(0x01, &value);
    if (ret != 0) return -3;

    if (ret == 0) {
        if ((value & 4) || (value & 8)) return 1;
    }
#endif

    // No interrupt, No error
    return 0;
}

int spi_poll_wait(int device_handle, int* interrupt, int timeout) {
    const int sleep_time = 30;
    int count;
    int got_interrupt;

    count = timeout / sleep_time;
    do {
        got_interrupt = _has_interrupt();
        // ex_log(LOG_DEBUG, "spi_poll_wait [%d] %d", count, got_interrupt);
        if (got_interrupt != 0) break;

        if (count > 0) {
            Sleep(sleep_time);
        }
    } while (count-- > 0);

    if (got_interrupt == 1) {
        *interrupt = 1;
        return 0;
    } else {
        *interrupt = 0;
        return got_interrupt;
    }
}

int spi_close_interrupt(int device_handle) {
    io_dispatch_disconnect();
    return 0;
}
unsigned int fp_device_open(int* device_handle) {
    int retval;
    unsigned int handle, len = sizeof(int);

    if (!device_handle) return FINGERPRINT_RES_INVALID_PARAM;

    retval = transfer_data(0, EX_CMD_GET_DEV_HANDLE, 0, 0, 0, NULL, &len, (unsigned char*)&handle);

    if (retval == FINGERPRINT_RES_SUCCESS) {
        *device_handle = handle;
    } else {
        *device_handle = 0;
    }

    return retval;
}

unsigned int fp_device_reset(int handle) {
    return io_dispatch_reset();
}

unsigned int fp_device_interrupt_enable(int handle, int cmd) {
    if (cmd == FLAG_INT_INIT) {
        return spi_init_interrupt(handle);
    } else if (cmd == FLAG_INT_CLOSE) {
        return spi_close_interrupt(handle);
    }

    return FINGERPRINT_RES_FAILED;
}

unsigned int fp_device_clock_enable(int handle, BOOL enable) {
    return 0;
}
unsigned int fp_device_interrupt_wait(int device_handle, unsigned int timeout) {
    int interrupt = 0;
    int ret;
    ret = spi_poll_wait(device_handle, &interrupt, timeout);

    if (ret == FINGERPRINT_RES_SUCCESS && interrupt != 0) {
        return FINGERPRINT_RES_SUCCESS;
    }

    return FINGERPRINT_RES_FAILED;
}

unsigned int fp_device_close(int handle) {
    return 0;
}
unsigned int egisFpDeviceFingerOnZero(int handle) {
    return 0;
}
unsigned int fp_device_power_control(int device_handle, BOOL enable) {
    return 0;
}
void fp_set_interrupt_trigger_type(int trigger_type) {
    return;
}
#endif
