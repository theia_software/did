#ifndef __OPT_FILE_TRUSTONIC_H__
#define __OPT_FILE_TRUSTONIC_H__

int trustonic_send_data(int type, unsigned char* in_data, int in_data_len);
int trustonic_receive_data(int type, unsigned char* in_data, int in_data_len);

#endif