
#include "canvas.h"

#define ECALLBACK_ENROLL_MAP 1000
#define ECALLBACK_ENROLL_TOO_FAST 1001

typedef void (*merge_map_callback_t)(unsigned int result, unsigned int percentage,
                                     unsigned char* map, unsigned int width, unsigned int height,
                                     int sum_x, int sum_y);

#ifdef __cplusplus
extern "C" {
#endif

struct MergeMapUpdateInfo {
    unsigned int finger_on_again;
    unsigned int rollback;
    unsigned int percentage;
    unsigned int similarity_score;
    int delta_x;
    int delta_y;
};

int mmap_start(merge_map_callback_t mm_callback);

int mmap_end(unsigned int immediately);
int mmap_set_config(struct MergeMapConfig* conf);
int mmap_update(struct MergeMapUpdateInfo* info);
int mmap_get_progress();

#ifdef __cplusplus
}
#endif