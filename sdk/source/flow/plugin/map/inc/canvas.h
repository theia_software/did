#ifndef __CANVAS_H__
#define __CANVAS_H__

#include "common_definition.h"

#ifndef TRUE
#define TRUE 1
#endif

#ifndef FALSE
#define FALSE 0
#endif

int canvas_setup(unsigned char* info_data);
void canvas_release();
int canvas_draw(unsigned char* map_img, unsigned int* map_percentage, int dx, int dy,
                unsigned int similarity_score, unsigned int enroll_percentage,
                unsigned int is_swipe_first_draw, unsigned int draw_flag,
                unsigned int show_location_flag, unsigned int rollback);
int canvas_get_sum_y();
int canvas_get_sum_x();

typedef struct block_size {
    int width;
    int height;
} block_size_t;

struct MergeMapConfig {
    unsigned int EnrollMethod;
    unsigned int SwipeDirection;
    unsigned int LoopCountX;  // 5
    unsigned int LoopCountY;  // 3 or 1
    unsigned int MergeMapWidth;
    unsigned int MergeMapHeight;
    unsigned int BackgroundHeight;
    unsigned int MergeMap_HW_Width;
    unsigned int MergeMap_HW_Height;
};

#endif