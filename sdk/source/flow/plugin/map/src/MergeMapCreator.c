#include <stdlib.h>

#include <stdlib.h>
#include "MergeMapCreator.h"
#include "plat_log.h"
#include "plat_mem.h"
#include "response_def.h"
#include "type_def.h"
#define MAX_QUEUE_SIZE 120
struct InfoQueue {
    struct MergeMapUpdateInfo item[MAX_QUEUE_SIZE];
    int data_index;
};

#ifdef _WINDOWS
#include <windows.h>

#define CANCEL_SEM 0
#define DATA_SEM 1
HANDLE g_work_thread = NULL;
HANDLE g_semaphores[2];
HANDLE g_cancel_sem;
HANDLE g_data_mutex;
#else
#include <pthread.h>
#include <semaphore.h>

pthread_mutex_t g_data_mutex = PTHREAD_MUTEX_INITIALIZER;
sem_t g_semaphore;
sem_t g_end_semaphore;
pthread_t g_thread_id = 0;
#endif

struct MergeMapConfig g_mmap_config;
struct InfoQueue g_queue;
merge_map_callback_t g_callback = NULL;
BOOL g_mmap_need_cancel = FALSE;

unsigned int g_loop_count = 0;

void* thread_proc(void* data) {
    int retval, keep = 1;
    struct MergeMapUpdateInfo item;
    int read_index = 0;
    unsigned int percentage = 0;
    // unsigned char map[MERGE_MAP_WIDTH * BACKGROUND_HEIGHT];
    unsigned char* map = plat_alloc(g_mmap_config.BackgroundHeight * g_mmap_config.MergeMapWidth);
    g_mmap_need_cancel = FALSE;
    if (data != NULL) {
        ex_log(LOG_DEBUG, "thread_proc data = %p", data);
    }

    if (map == NULL) goto EXIT;
    while (keep) {
#ifdef _WINDOWS
        int index;
        // wait Data or Cancel semaphore
        index = WaitForMultipleObjects(2, g_semaphores, FALSE, INFINITE);
        if (index < 0) {
            break;
        } else if ((index - WAIT_OBJECT_0) == CANCEL_SEM) {
            g_mmap_need_cancel = TRUE;
        }

        WaitForSingleObject(g_data_mutex, INFINITE);
#else
        sem_wait(&g_semaphore);

        pthread_mutex_lock(&g_data_mutex);
#endif
        ex_log(LOG_DEBUG, "read_index = %d, data_index = %d", read_index, g_queue.data_index);
        if (read_index > g_queue.data_index) {
            read_index = 0;
            g_queue.data_index = -1;
#ifdef _WINDOWS
            ReleaseMutex(g_data_mutex);
#else
            pthread_mutex_unlock(&g_data_mutex);
#endif
            if (g_mmap_need_cancel || g_callback == NULL) {
                g_mmap_need_cancel = FALSE;
                break;
            }

            continue;
        }

        item.finger_on_again = g_queue.item[read_index].finger_on_again;
        item.rollback = g_queue.item[read_index].rollback;
        item.percentage = g_queue.item[read_index].percentage;
        item.similarity_score = g_queue.item[read_index].similarity_score;
        item.delta_x = g_queue.item[read_index].delta_x;
        item.delta_y = g_queue.item[read_index].delta_y;
#ifdef _WINDOWS
        // unlock
        ReleaseMutex(g_data_mutex);
#else
        // unlock
        pthread_mutex_unlock(&g_data_mutex);
#endif

        read_index++;
        // draw
        if (g_mmap_config.EnrollMethod == ENROLL_METHOD_SWIPE) {
            retval = canvas_draw(map, &percentage, item.delta_x, item.delta_y, 52, item.percentage,
                                 item.finger_on_again, TRUE, TRUE, item.rollback);
        } else {
            retval =
                canvas_draw(map, &percentage, item.delta_x, item.delta_y, item.similarity_score,
                            item.percentage, item.finger_on_again, TRUE, FALSE, item.rollback);
        }

        // notify merge_map & percentage
        if (g_callback && retval == FINGERPRINT_RES_SUCCESS) {
            ex_log(LOG_DEBUG, "percentage= %d", percentage);
            if (g_mmap_config.EnrollMethod == ENROLL_METHOD_SWIPE) {
                g_callback(ECALLBACK_ENROLL_MAP, percentage, map, g_mmap_config.MergeMapWidth,
                           g_mmap_config.BackgroundHeight, canvas_get_sum_x(), canvas_get_sum_y());
            } else {
                g_callback(ECALLBACK_ENROLL_MAP, percentage, map, g_mmap_config.MergeMapWidth,
                           g_mmap_config.MergeMapHeight, canvas_get_sum_x(), canvas_get_sum_y());
            }

        } else if (g_callback && retval != FINGERPRINT_RES_SUCCESS) {
            g_callback(ECALLBACK_ENROLL_TOO_FAST, 0, 0, 0, 0, 0, 0);
        }
    }
    ex_log(LOG_DEBUG, "read_index = %d, data_index = %d", read_index, g_queue.data_index);

EXIT:
    if (map != NULL) {
        plat_free(map);
    }

    canvas_release();
    g_callback = NULL;

#ifdef _WINDOWS
    CloseHandle(g_data_mutex);
    CloseHandle(g_semaphores[DATA_SEM]);
    CloseHandle(g_semaphores[CANCEL_SEM]);
#else
    sem_destroy(&g_semaphore);
    sem_post(&g_end_semaphore);
#endif

    ex_log(LOG_DEBUG, "end");
    return NULL;
}

int mmap_start(merge_map_callback_t mm_callback) {
    int retval;

    g_callback = mm_callback;
    g_loop_count = 0;
#ifdef _WINDOWS
    g_data_mutex = CreateMutex(NULL, TRUE, "data");
    g_queue.data_index = -1;
    ReleaseMutex(g_data_mutex);
    g_semaphores[CANCEL_SEM] = CreateSemaphore(NULL, 0, 1, "cancel");
    g_semaphores[DATA_SEM] = CreateSemaphore(NULL, 0, MAX_QUEUE_SIZE, "queue");
#else
    pthread_mutex_lock(&g_data_mutex);
    g_queue.data_index = -1;
    pthread_mutex_unlock(&g_data_mutex);
    sem_init(&g_semaphore, 0, 0);
    sem_init(&g_end_semaphore, 0, 0);
#endif

    g_mmap_need_cancel = FALSE;

    retval = canvas_setup((unsigned char*)&g_mmap_config);
    if (retval) {
        return FINGERPRINT_RES_FAILED;
    }

#ifdef _WINDOWS
    g_work_thread = CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)thread_proc, NULL, 0, NULL);
    if (!g_work_thread) return FINGERPRINT_RES_FAILED;
#else
    pthread_attr_t attr;
    pthread_attr_init(&attr);
    pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED);
    retval = pthread_create(&g_thread_id, &attr, thread_proc, NULL);

    pthread_attr_destroy(&attr);
#endif

    return FINGERPRINT_RES_SUCCESS;
}

int mmap_end(unsigned int immediately) {
    int previous_count = 0;
    g_mmap_need_cancel = TRUE;
    // free callback
    if (immediately) {
        g_callback = NULL;
    }

#ifdef _WINDOWS
    // signal to close the thread
    ReleaseSemaphore(g_semaphores[CANCEL_SEM], 1, &previous_count);
    // close handle
    if (g_work_thread) {
        WaitForSingleObject(g_work_thread, INFINITE);
        CloseHandle(g_work_thread);
        g_work_thread = NULL;
    }

#else
    // g_mmap_need_cancel = TRUE;
    sem_post(&g_semaphore);
    sem_getvalue(&g_semaphore, &previous_count);
    if (g_thread_id) {
        sem_wait(&g_end_semaphore);
        sem_destroy(&g_end_semaphore);
        g_thread_id = 0;
    }
#endif
    return FINGERPRINT_RES_SUCCESS;
}

int mmap_set_config(struct MergeMapConfig* conf) {
    g_mmap_config.EnrollMethod = conf->EnrollMethod;
    g_mmap_config.SwipeDirection = conf->SwipeDirection;
    g_mmap_config.MergeMapHeight = conf->MergeMapHeight;
    g_mmap_config.MergeMapWidth = conf->MergeMapWidth;
    g_mmap_config.BackgroundHeight = conf->BackgroundHeight;
    g_mmap_config.LoopCountX = conf->LoopCountX;
    g_mmap_config.LoopCountY = conf->LoopCountY;
    g_mmap_config.MergeMap_HW_Width = conf->MergeMap_HW_Width;
    g_mmap_config.MergeMap_HW_Height = conf->MergeMap_HW_Height;
    return FINGERPRINT_RES_SUCCESS;
}

int mmap_update(struct MergeMapUpdateInfo* info) {
    struct MergeMapUpdateInfo* item = NULL;
    // lock
#ifdef _WINDOWS
    WaitForSingleObject(g_data_mutex, INFINITE);
#else
    pthread_mutex_lock(&g_data_mutex);
#endif
    if (g_mmap_need_cancel) goto RESULT;
    // insert new data
    if (g_queue.data_index >= MAX_QUEUE_SIZE - 1) {
        // bomb
        ex_log(LOG_DEBUG, " +++++++++++ g_queue.data_index = %d +++++++++++", g_queue.data_index);
    } else {
        g_queue.data_index++;
        item = &g_queue.item[g_queue.data_index];
        item->finger_on_again = info->finger_on_again;
        item->rollback = info->rollback;
        item->percentage = info->percentage;
        item->delta_x = info->delta_x;
        item->delta_y = info->delta_y;
        item->similarity_score = info->similarity_score;
    }

RESULT:
#ifdef _WINDOWS
    // notify
    ReleaseSemaphore(g_semaphores[DATA_SEM], 1, NULL);
    // unlock
    ReleaseMutex(g_data_mutex);
#else
    // notify
    sem_post(&g_semaphore);
    // unlock
    pthread_mutex_unlock(&g_data_mutex);
#endif

    return FINGERPRINT_RES_SUCCESS;
}

int mmap_get_progress() {
    return FINGERPRINT_RES_SUCCESS;
}
