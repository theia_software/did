#include "canvas.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "canvas.h"
#include "plat_log.h"
#include "plat_mem.h"
#include "response_def.h"
#include "type_def.h"
#define LOG_TAG "canvas"
static block_size_t g_block_size;
static unsigned int g_start_point[5];
static unsigned int g_start_new_line[5];
static int g_last_draw_point = 0;
static int g_map_width;
static int g_map_height;
static unsigned int g_background_height;
static int g_enroll_mode;
static int g_swipe_direction;
static unsigned char* g_merge_map_buf = NULL;
static int g_sum_draw_point_x = 0;
static int g_sum_draw_point_y = 0;
static int g_loop_count_x;
static int g_loop_count_y;
static unsigned int g_direction_detected = FALSE;
static unsigned int g_new_line_draw = FALSE;
static unsigned int g_enroll_percentage = 0;
static unsigned int g_swipe_auto_flag = FALSE;
static unsigned int g_set_draw_size_flag = TRUE;
static int g_last_circular_x, g_last_circular_y;
static int draw_swipe_enroll_image(unsigned char* map_img, unsigned int* map_percentage,
                                   unsigned int similarity_score, int dx, int dy,
                                   unsigned int is_swipe_first_draw, unsigned int draw_flag,
                                   unsigned int show_location_flag, unsigned int rollback);
static int draw_paint_enroll_image(unsigned char* map_img, unsigned int* map_percentage,
                                   unsigned int similarity_score, int dx, int dy,
                                   unsigned int is_swipe_first_draw, unsigned int draw_flag,
                                   unsigned int show_location_flag, unsigned int rollback);
static int draw_touch_enroll_image(unsigned char* map_img, unsigned int* map_percentage,
                                   unsigned int similarity_score, int dx, int dy,
                                   unsigned int is_swipe_first_draw, unsigned int draw_flag,
                                   unsigned int show_location_flag, unsigned int rollback);
static int check_img_size(int limit_size, int real_size, int* check_size);
static int get_start_point();
// static void get_percentage_from_map(unsigned int *map_percentage);
static void show_location(unsigned char* img);
static void draw_merge_map();
static void set_draw_size();
static void rollback_merge_map();

static int g_sum_draw_point_y_backup = 0;
int canvas_setup(unsigned char* info_data) {
    struct MergeMapConfig* merge_map_config;

    if (info_data == NULL) {
        return FINGERPRINT_RES_INVALID_PARAM;
    }

    merge_map_config = (struct MergeMapConfig*)info_data;
    g_enroll_mode = merge_map_config->EnrollMethod;

    if (g_enroll_mode == ENROLL_METHOD_SWIPE) {
        g_swipe_direction = merge_map_config->SwipeDirection;

        if (g_swipe_direction == SWIPE_DIRECTION_X) {
            g_block_size.height = merge_map_config->MergeMap_HW_Width;
        } else {
            g_block_size.height = merge_map_config->MergeMap_HW_Height;
        }
    }

    g_loop_count_x = merge_map_config->LoopCountX;
    g_loop_count_y = merge_map_config->LoopCountY;
    g_map_width = merge_map_config->MergeMapWidth;
    g_map_height = merge_map_config->MergeMapHeight;
    g_background_height = merge_map_config->BackgroundHeight;

    if (g_merge_map_buf == NULL) {
        g_merge_map_buf = (unsigned char*)plat_alloc(g_map_width * g_background_height);
    }

    if (g_merge_map_buf == NULL) {
        return FINGERPRINT_RES_ALLOC_FAILED;
    }

    memset(g_merge_map_buf, 0x00, g_map_width * g_background_height);
    memset(g_start_point, 0x00, sizeof(g_start_point));
    memset(g_start_new_line, TRUE, sizeof(g_start_new_line));
    g_sum_draw_point_x = 0;
    g_sum_draw_point_y = 0;
    g_enroll_percentage = 0;
    g_last_draw_point = 0;
    g_swipe_auto_flag = FALSE;
    g_set_draw_size_flag = TRUE;
    g_direction_detected = FALSE;
    g_new_line_draw = FALSE;
    g_last_circular_x = 0;
    g_last_circular_y = 0;
    return FINGERPRINT_RES_SUCCESS;
}

void canvas_release() {
    if (g_merge_map_buf != NULL) {
        plat_free(g_merge_map_buf);
        g_merge_map_buf = NULL;
    }
    g_enroll_percentage = 0;
}

int canvas_get_sum_x() {
    return g_sum_draw_point_x;
}

int canvas_get_sum_y() {
    if (g_enroll_mode == ENROLL_METHOD_SWIPE) {
        return g_sum_draw_point_y + g_block_size.height;
    }

    return g_sum_draw_point_y;
}
int canvas_draw(unsigned char* map_img, unsigned int* map_percentage, int dx, int dy,
                unsigned int similarity_score, unsigned int enroll_percentage,
                unsigned int is_swipe_first_draw, unsigned int draw_flag,
                unsigned int show_location_flag, unsigned int rollback) {
    int retval = FINGERPRINT_RES_SUCCESS;

    if (map_img == NULL || map_percentage == NULL) {
        return FINGERPRINT_RES_INVALID_PARAM;
    }
    g_enroll_percentage = enroll_percentage;
    switch (g_enroll_mode) {
        case ENROLL_METHOD_SWIPE:
            retval = draw_swipe_enroll_image(map_img, map_percentage, similarity_score, dx, dy,
                                             is_swipe_first_draw, draw_flag, show_location_flag,
                                             rollback);
            break;

        case ENROLL_METHOD_PAINT:
            retval = draw_paint_enroll_image(map_img, map_percentage, similarity_score, dx, dy,
                                             is_swipe_first_draw, draw_flag, show_location_flag,
                                             rollback);
            break;

        case ENROLL_METHOD_TOUCH:
            retval = draw_touch_enroll_image(map_img, map_percentage, similarity_score, dx, dy,
                                             is_swipe_first_draw, draw_flag, show_location_flag,
                                             rollback);
            break;

        default:
            retval = FINGERPRINT_RES_UNKNOWN_MODE;  // unknown mode
            break;
    }

    return retval;
}

static int draw_swipe_enroll_image(unsigned char* map_img, unsigned int* map_percentage,
                                   unsigned int similarity_score, int dx, int dy,
                                   unsigned int is_swipe_first_draw, unsigned int draw_flag,
                                   unsigned int show_location_flag, unsigned int rollback) {
    ex_log(LOG_DEBUG, "draw_swipe_enroll_image map_img addr = %p", map_img);
    ex_log(LOG_DEBUG, "similarity_score = %u, dx = %d, dy=%d, is_swipe_first_draw = %u",
           similarity_score, dx, dy, is_swipe_first_draw);
    ex_log(LOG_DEBUG, "draw_flag = %u, show_location_flag =%u, rollback = %u", draw_flag,
           show_location_flag, rollback);

    if (is_swipe_first_draw) {
        g_new_line_draw = TRUE;
    }
    /*
    if (g_swipe_direction == SWIPE_DIRECTION_AUTO && !g_direction_detected) {
        g_swipe_auto_flag = TRUE;

        if (similarity_score >= 50) {
            if (abs(dx) > abs(dy)) {
                g_swipe_direction = SWIPE_DIRECTION_X;
                g_direction_detected = TRUE;

            } else if (abs(dy) > abs(dx)) {
                g_swipe_direction = SWIPE_DIRECTION_Y;
                g_direction_detected = TRUE;
            } else {
                return 0;
            }
        } else {
            return 0;
        }
    }
*/
    if (g_set_draw_size_flag) {
        set_draw_size();
        g_set_draw_size_flag = FALSE;
    }

    if (rollback) {
        // g_sum_draw_point_y = g_sum_draw_point_y_backup;
    }

    if (is_swipe_first_draw || g_new_line_draw) {
        dx = 0;
        dy = 0;
        get_start_point();
        g_sum_draw_point_y_backup = g_sum_draw_point_y;
    }

    if (rollback) {
        rollback_merge_map();
    } else if (!is_swipe_first_draw) {
        if (g_new_line_draw) {
            dx = 0;
            dy = 0;
            g_new_line_draw = FALSE;
        }

        if (g_swipe_auto_flag) {
            if (g_swipe_direction == SWIPE_DIRECTION_X) {
                check_img_size(g_block_size.height, abs(dx), &g_sum_draw_point_y);
            } else {
                check_img_size(g_block_size.height, abs(dy), &g_sum_draw_point_y);
            }

        } else {
            if (g_swipe_direction == SWIPE_DIRECTION_X) {
                check_img_size(g_block_size.height, abs(dx), &g_sum_draw_point_y);
            } else if (g_swipe_direction == SWIPE_DIRECTION_Y) {
                check_img_size(g_block_size.height, abs(dy), &g_sum_draw_point_y);
            }
        }

        if (draw_flag) {
            draw_merge_map();
        }
    }

    memcpy(map_img, g_merge_map_buf, g_map_width * g_background_height);

    // get_percentage_from_map(map_percentage);

    if (show_location_flag) {
        show_location(map_img);
    }

    *map_percentage = g_enroll_percentage;
    return FINGERPRINT_RES_SUCCESS;
}

static int draw_paint_enroll_image(unsigned char* map_img, unsigned int* map_percentage,
                                   unsigned int similarity_score, int dx, int dy,
                                   unsigned int is_swipe_first_draw, unsigned int draw_flag,
                                   unsigned int show_location_flag, unsigned int rollback) {
    ex_log(LOG_DEBUG, "draw_paint_enroll_image map_img addr = %p", map_img);
    ex_log(LOG_DEBUG, "similarity_score = %u, dx = %d, dy=%d, is_swipe_first_draw = %u",
           similarity_score, dx, dy, is_swipe_first_draw);
    ex_log(LOG_DEBUG, "draw_flag = %u, show_location_flag =%u, rollback = %u", draw_flag,
           show_location_flag, rollback);
    if (g_set_draw_size_flag) {
        set_draw_size();
        g_set_draw_size_flag = FALSE;
    }

    if (is_swipe_first_draw) {
        get_start_point();
    }

    check_img_size(g_block_size.width, dx, &g_sum_draw_point_x);
    check_img_size(g_block_size.height, dy, &g_sum_draw_point_y);

    if (draw_flag && !is_swipe_first_draw) {
        draw_merge_map();
    }

    memcpy(map_img, g_merge_map_buf, g_map_width * g_map_height);

    *map_percentage = g_enroll_percentage;

    if (show_location_flag /*&& *map_percentage != 100*/ && g_direction_detected) {
        // show_location(map_img);
    }
    g_direction_detected = TRUE;
    return FINGERPRINT_RES_SUCCESS;
}

static int draw_touch_enroll_image(unsigned char* map_img, unsigned int* map_percentage,
                                   unsigned int similarity_score, int dx, int dy,
                                   unsigned int is_swipe_first_draw, unsigned int draw_flag,
                                   unsigned int show_location_flag, unsigned int rollback) {
    ex_log(LOG_DEBUG, "draw_touch_enroll_image map_img addr = %p", map_img);
    ex_log(LOG_DEBUG, "similarity_score = %u, dx = %d, dy=%d, is_swipe_first_draw = %u",
           similarity_score, dx, dy, is_swipe_first_draw);
    ex_log(LOG_DEBUG, "draw_flag = %u, show_location_flag =%u, rollback = %u", draw_flag,
           show_location_flag, rollback);
    /*if(g_set_draw_size_flag && g_enroll_percentage > 0) {
        set_draw_size();
        g_set_draw_size_flag = FALSE;
    }

    if (is_swipe_first_draw) {
        get_start_point();
    }

    if(draw_flag) {
        draw_merge_map();
    }

    memcpy(map_img, g_merge_map_buf, g_map_width * g_map_height);

    if(show_location_flag) {
        show_location(map_img);
    }*/

    *map_percentage = g_enroll_percentage;
    return FINGERPRINT_RES_SUCCESS;
}
// marked as a unuseful function NOW
/*static void get_percentage_from_map(unsigned int *map_percentage)
{
    int x, y;
    int count = 0;

    for (x = 0; x < g_map_width; x++) {
        for (y = 0; y < g_map_height; y++) {
            if (g_merge_map_buf[x + y * g_map_width] != 0) {
                count++;
            }
        }
    }

    if (count >= (g_map_width * g_map_height)) {
        *map_percentage = 100;
    } else {
        // *map_percentage = count / 327;
        *map_percentage = count * 100 / (g_map_width * g_map_height);
    }
}*/

static void show_location(unsigned char* img) {
    int x, y;
    int start_point_x, start_point_y;

    switch (g_enroll_mode) {
        case ENROLL_METHOD_SWIPE: {
            start_point_x = g_sum_draw_point_x;
            start_point_y = g_sum_draw_point_y;
        } break;

        case ENROLL_METHOD_PAINT: {
            start_point_x = (g_map_width - g_block_size.width) / 2 + g_sum_draw_point_x;
            start_point_y = (g_map_height - g_block_size.height) / 2 + g_sum_draw_point_y;
        } break;

        case ENROLL_METHOD_TOUCH: {
            start_point_x = g_sum_draw_point_x;
            start_point_y = g_sum_draw_point_y;
        } break;

        default:
            start_point_x = g_sum_draw_point_x;
            start_point_y = g_sum_draw_point_y;
            break;
    }

    for (x = start_point_x; x < start_point_x + g_block_size.width; x++) {
        for (y = start_point_y; y < start_point_y + g_block_size.height; y++) {
            if (((x == start_point_x) || (x == start_point_x + g_block_size.width - 1) ||
                 (y == start_point_y) || (y == start_point_y + g_block_size.height - 1))) {
                img[x + y * g_map_width] = 1;
            }
        }
    }
}

static void draw_merge_map() {
    int max_x, max_y;
    int x, y;
    int start_point_x, start_point_y;
    int circular_x, circular_y, circular_r, distance;

    switch (g_enroll_mode) {
        case ENROLL_METHOD_SWIPE: {
            max_y = g_background_height - g_block_size.height;
            // max_y = g_map_height - g_block_size.height;
            if (g_sum_draw_point_y < 0) {
                g_sum_draw_point_y = 0;
            } else if (g_sum_draw_point_y > max_y) {
                g_sum_draw_point_y = max_y;
            }

            start_point_x = g_sum_draw_point_x;
            start_point_y = g_sum_draw_point_y;
            g_last_draw_point = g_sum_draw_point_y;
        } break;

        case ENROLL_METHOD_PAINT: {
            max_x = (g_map_width - g_block_size.width) / 2;
            max_y = (g_map_height - g_block_size.height) / 2;

            if (g_sum_draw_point_x < -max_x) {
                g_sum_draw_point_x = -max_x;
            } else if (g_sum_draw_point_x > max_x) {
                g_sum_draw_point_x = max_x;
            }

            if (g_sum_draw_point_y < -max_y) {
                g_sum_draw_point_y = -max_y;
            } else if (g_sum_draw_point_y > max_y) {
                g_sum_draw_point_y = max_y;
            }

            start_point_x = (g_map_width - g_block_size.width) / 2 + g_sum_draw_point_x;
            start_point_y = (g_map_height - g_block_size.height) / 2 + g_sum_draw_point_y;
            circular_x = start_point_x + g_block_size.width / 2;
            circular_y = start_point_y + g_block_size.height / 2;
            circular_r = g_block_size.width / 2;
        } break;

        case ENROLL_METHOD_TOUCH: {
            max_y = g_map_height - g_block_size.height;

            if (g_sum_draw_point_y < 0) {
                g_sum_draw_point_y = 0;
            } else if (g_sum_draw_point_y > max_y) {
                g_sum_draw_point_y = max_y;
            }

            start_point_x = g_sum_draw_point_x;
            start_point_y = g_sum_draw_point_y;
        } break;

        default:
            break;
    }

    if (g_enroll_mode == ENROLL_METHOD_PAINT) {
        if (g_last_circular_x != 0 && g_last_circular_y != 0) {
            for (x = g_last_circular_x - circular_r; x <= g_last_circular_x + circular_r; x++) {
                for (y = g_last_circular_y - circular_r; y <= g_last_circular_y + circular_r; y++) {
                    distance = (x - g_last_circular_x) * (x - g_last_circular_x) +
                               (y - g_last_circular_y) * (y - g_last_circular_y);
                    if (distance <= circular_r * circular_r) {
                        if (g_merge_map_buf[x + y * g_map_width] == 0) {
                        } else if (g_merge_map_buf[x + y * g_map_width] < (0xF * 13)) {
                            g_merge_map_buf[x + y * g_map_width] =
                                g_merge_map_buf[x + y * g_map_width] / 0xF * 0xF;
                        } else {
                            g_merge_map_buf[x + y * g_map_width] = (0xF * 12);
                        }
                    }
                }
            }
        }

        for (x = circular_x - circular_r; x <= circular_x + circular_r; x++) {
            for (y = circular_y - circular_r; y <= circular_y + circular_r; y++) {
                distance =
                    (x - circular_x) * (x - circular_x) + (y - circular_y) * (y - circular_y);
                if (distance <= circular_r * circular_r) {
                    if (g_merge_map_buf[x + y * g_map_width] < (0xF * 13)) {
                        g_merge_map_buf[x + y * g_map_width] += 0xF + 1;
                    } else {
                        g_merge_map_buf[x + y * g_map_width] = (0xF * 12) + 1;
                    }
                }
            }
        }
        g_last_circular_x = circular_x;
        g_last_circular_y = circular_y;
    } else {
        for (x = start_point_x; x < start_point_x + g_block_size.width; x++) {
            for (y = start_point_y; y < start_point_y + g_block_size.height; y++) {
                g_merge_map_buf[x + y * g_map_width] = 0xFF;
            }
        }
    }
}

static void rollback_merge_map() {
    int x, y;
    int start_point_x, start_point_y;

    if (g_enroll_mode != ENROLL_METHOD_SWIPE) return;

    start_point_x = g_sum_draw_point_x;
    start_point_y =
        g_sum_draw_point_y_backup == 0 ? 0 : g_sum_draw_point_y_backup + g_block_size.height;
    // if (start_point_y > g_block_size.height){
    //	start_point_y = g_sum_draw_point_y_backup - g_block_size.height;
    //}
    // else{
    //	start_point_y = 0;
    //}
    if (start_point_y < 0) start_point_y = 0;

    for (x = start_point_x; x < start_point_x + g_block_size.width; x++) {
        for (y = start_point_y; y < g_sum_draw_point_y + g_block_size.height; y++) {
            g_merge_map_buf[x + y * g_map_width] = 0;
        }
    }
    g_sum_draw_point_y = g_sum_draw_point_y_backup;
    g_last_draw_point = g_sum_draw_point_y;
}

static int check_img_size(int limit_size, int real_size, int* check_size) {
    if (check_size == NULL) {
        return FINGERPRINT_RES_ALLOC_FAILED;
    }

    if (real_size < -limit_size) {
        real_size = -limit_size;
    } else if (real_size > limit_size) {
        real_size = limit_size;
    }

    *check_size += real_size;
    ex_log(LOG_DEBUG, "check_img_size real_size %d , total_y = %d,limit_size %d", real_size,
           *check_size, limit_size);

    return FINGERPRINT_RES_SUCCESS;
}

static void set_draw_size() {
    switch (g_enroll_mode) {
        case ENROLL_METHOD_SWIPE: {
            int loop_count =
                (g_swipe_direction == SWIPE_DIRECTION_X ? g_loop_count_x : g_loop_count_y);

            if (loop_count == 1) {
                g_start_point[0] = 0;
            } else if (loop_count == 3) {
                g_start_point[0] = (g_map_width / loop_count) * 1;  // mid
                g_start_point[1] = (g_map_width / loop_count) * 0;  // left
                g_start_point[2] = (g_map_width / loop_count) * 2;  // right
            } else {
                g_start_point[0] = (g_map_width / loop_count) * 2;  // mid
                g_start_point[1] = (g_map_width / loop_count) * 1;  // left
                g_start_point[2] = (g_map_width / loop_count) * 3;  // right
                g_start_point[3] = (g_map_width / loop_count) * 0;  // leftmost
                g_start_point[4] = (g_map_width / loop_count) * 4;  // rightmost
            }

            g_block_size.width = (g_map_width / loop_count) + (g_map_width % loop_count);
        } break;

        case ENROLL_METHOD_PAINT: {
            g_block_size.width = (g_map_width / 3);
            g_block_size.height = (g_map_width / 3);
        } break;

        case ENROLL_METHOD_TOUCH: {
            g_block_size.width = g_map_width;
            g_block_size.height = (g_map_height / (100 / g_enroll_percentage));
        } break;

        default:
            break;
    }
}

static int get_start_point() {
    int y;

    switch (g_enroll_mode) {
        case ENROLL_METHOD_SWIPE: {
            int loop_index = 0;
            int loop_count =
                g_swipe_direction == SWIPE_DIRECTION_X ? g_loop_count_x : g_loop_count_y;

            if (loop_count == 1) {
                loop_index = 0;
            } else if (loop_count == 3) {
                if (g_enroll_percentage < 33) {
                    loop_index = 0;
                } else if (g_enroll_percentage >= 33 && g_enroll_percentage < 66) {
                    loop_index = 1;
                } else if (g_enroll_percentage >= 66) {
                    loop_index = 2;
                }
            } else if (loop_count == 5) {
                if (g_enroll_percentage < 20) {
                    loop_index = 0;
                } else if (g_enroll_percentage >= 20 && g_enroll_percentage < 40) {
                    loop_index = 1;
                } else if (g_enroll_percentage >= 40 && g_enroll_percentage < 60) {
                    loop_index = 2;
                } else if (g_enroll_percentage >= 60 && g_enroll_percentage < 80) {
                    loop_index = 3;
                } else if (g_enroll_percentage >= 80) {
                    loop_index = 4;
                }
            }

            if (g_start_new_line[loop_index]) {
                g_last_draw_point = 0;
                g_start_new_line[loop_index] = FALSE;
            }

            g_sum_draw_point_y = g_last_draw_point;
            g_sum_draw_point_x = g_start_point[loop_index];
        } break;

        case ENROLL_METHOD_PAINT: {
            g_sum_draw_point_x = 0;
            g_sum_draw_point_y = 0;
        } break;

        case ENROLL_METHOD_TOUCH: {
            for (y = 0; y < g_map_height; y++) {
                if (g_merge_map_buf[(g_map_width / 2) + y * g_map_width] == 0) {
                    g_sum_draw_point_y = y;
                    g_sum_draw_point_x = 0;
                    goto exit;
                }
            }
        } break;

        default:
            break;
    }

exit:
    return FINGERPRINT_RES_SUCCESS;
}