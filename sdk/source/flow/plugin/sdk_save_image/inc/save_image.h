#ifndef __SAVE_IMAGE_H__
#define __SAVE_IMAGE_H__

#include "common_definition.h"
#include "type_def.h"

int transfer_frames_to_client(transfer_image_type_t type, int img_quality, BOOL force_to_good,
                              int match_result);
#endif