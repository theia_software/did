#include "captain.h"
#include "common_definition.h"
#include "core_config.h"
#include "device_int.h"
#include "fp_definition.h"
#include "op_manager.h"
#include "plat_log.h"
#include "response_def.h"
#include "save_image.h"
#include "swipe_enroll.h"
#include "update_map.h"

extern fingerprint_enroll_info_t g_enroll_info;
extern void setSpiState(BOOL spi_on, BOOL forced);
extern host_device_info_t g_host_device;
extern const int TIMEOUT_FOR_ONE_TRY;
extern int g_enroll_timeout;

int paint_enroll(unsigned int* percentage, unsigned int enrolled_count) {
    int retval;
    unsigned int estate = ESTATE_WAIT_INT;
    unsigned int img_quality = 0;
    cmd_enrollresult_t enroll_result;
    BOOL is_too_fast = FALSE;
    BOOL finger_on_again = TRUE;
    int SWIPE_FAST_THREASHOLD = 500;
    int SWIPE_FAST_CONTROL = FALSE;
    int enroll_option = ENROLL_OPTION_NORMAL;
    memset(&enroll_result, 0, sizeof(cmd_enrollresult_t));

    SWIPE_FAST_THREASHOLD = core_config_get_int(INI_SECTION_ENROLL, KEY_SWIPE_QUICK_TIMEOUT, 500);
    SWIPE_FAST_CONTROL = core_config_get_int(INI_SECTION_ENROLL, KEY_CHECK_TOO_FAST, TRUE);

    do {
        switch (estate) {
            case ESTATE_WAIT_INT:
                ex_log(LOG_DEBUG, "estate == ESTATE_WAIT_INT");

                if (check_cancelable()) {
                    retval = FINGERPRINT_RES_CANCEL;
                    break;
                }
                retval = opm_set_work_mode(DETECT_MODE);
                if (check_cancelable()) {
                    retval = FINGERPRINT_RES_CANCEL;
                    break;
                }
                if (retval != FINGERPRINT_RES_SUCCESS) {
                    ex_log(LOG_ERROR, "ESTATE_WAIT_INT set detect mode failed return = %d", retval);
                }

                notify(EVENT_FINGER_WAIT, 0, 0, NULL, 0);

                setSpiState(FALSE, FALSE);
                if (!wait_trigger(0, TIMEOUT_FOR_ONE_TRY, g_enroll_timeout)) {
                    if (get_wait_trigger_status() == FINGERPRINT_RES_SECURE_ENROLL_TIMEOUT) {
                        notify(EVENT_ENROLL_TIMEOUT, 0, 0, NULL, 0);
                        enroll_cancel_recovery_value();
                    }
                    ex_log(LOG_DEBUG, "wait_trigger is not success, status = %d",
                           get_wait_trigger_status());
                    retval = get_wait_trigger_status();
                    break;
                }
                setSpiState(TRUE, TRUE);

                enroll_option = ENROLL_OPTION_FINGERON;

            case ESTATE_GET_IMG:
                ex_log(LOG_DEBUG, "estate == ESTATE_GET_IMG");

                if (check_cancelable()) {
                    retval = FINGERPRINT_RES_CANCEL;
                    break;
                }

                retval = opm_get_image(&img_quality, g_host_device);
#ifdef __SHOW_LIVEIMAGE__
                transfer_frames_to_client(TRANSFER_LIVE_IMAGE, img_quality, FALSE, 0);
#endif
                if (check_cancelable()) {
                    retval = FINGERPRINT_RES_CANCEL;
                    break;
                }
                ex_log(LOG_DEBUG, "opm_get_image retval = %d", retval);
                if (retval != FINGERPRINT_RES_SUCCESS) {
                    ex_log(LOG_ERROR, "opm_get_image failed retval = %d", retval);
                    break;
                }

                ex_log(LOG_DEBUG, "opm_get_image img_quality = %d", img_quality);
                if (img_quality == FP_LIB_ENROLL_SUCCESS) {
                    estate = ESTATE_ENROLL;
                    notify(EVENT_FINGER_READY, 0, 0, NULL, 0);

                } else if (img_quality == FP_LIB_ENROLL_FAIL_LOW_QUALITY ||
                           img_quality == FP_LIB_ENROLL_FAIL_LOW_QUALITY_AND_LOW_COVERAGE) {
                    estate = ESTATE_FINGER_OFF;

                } else if (img_quality == FP_LIB_ENROLL_FAIL_LOW_COVERAGE) {
                    estate = ESTATE_ENROLL;

                } else if (img_quality == FP_LIB_ENROLL_HELP_TOO_WET) {
                    estate = ESTATE_FINGER_OFF;
                    notify(EVENT_IMG_WATER, 0, 0, NULL, 0);
                } else
                    estate = ESTATE_WAIT_INT;

                break;

            case ESTATE_ENROLL:
                ex_log(LOG_DEBUG, "estate == ESTATE_ENROLL");

                if (check_cancelable()) {
                    retval = FINGERPRINT_RES_CANCEL;
                    break;
                }
                retval = opm_do_enroll(&enroll_result, enroll_option, enrolled_count);
                notify(EVENT_SWIPE_DX_DY, enroll_result.swipe_info.dx, enroll_result.swipe_info.dy,
                       NULL, 0);
                if (check_cancelable()) {
                    retval = FINGERPRINT_RES_CANCEL;
                    break;
                }
                ex_log(LOG_DEBUG, "opm_do_enroll retval = %d", retval);
                if (retval != FINGERPRINT_RES_SUCCESS) {
                    break;
                }

                notify(EVENT_ENROLL_OK, g_enroll_info.fingerprint_info.fingerprint_id,
                       enroll_result.percentage, NULL, 0);

                enroll_option = ENROLL_OPTION_NORMAL;
                *percentage = enroll_result.percentage;
                if (enroll_result.percentage >= 100) {
                    enroll_option = ENROLL_OPTION_MERGE;
                    opm_do_enroll(&enroll_result, enroll_option, 0);
                    if (enroll_result.status == FP_LIB_ENROLL_HELP_ALREADY_EXIST) {
                        notify(EVENT_ENROLL_DUPLICATE_FINGER, 0, 0, NULL, 0);
                        estate = ESTATE_FINGER_OFF;
                        break;
                    } else if (enroll_result.status == FP_LIB_ENROLL_TOO_FAST) {
                        enroll_option = ENROLL_OPTION_MERGE;
                        opm_do_enroll(&enroll_result, enroll_option, 0);

                        notify(EVENT_ENROLL_TOO_FAST, 0, 0, NULL, 0);
                        estate = ESTATE_FINGER_OFF;
                        break;
                    }

                    retval = FINGERPRINT_RES_SUCCESS;
                    goto exit;
                }

                ex_log(LOG_DEBUG, "percentage = %d, status = %d, swipe_dir = %d",
                       enroll_result.percentage, enroll_result.status,
                       enroll_result.swipe_info.swipe_dir);

                ex_log(LOG_DEBUG, "opm_do_enroll dx = %d, dy = %d, score = %d",
                       enroll_result.swipe_info.dx, enroll_result.swipe_info.dy,
                       enroll_result.swipe_info.similarity_score);

                update_map(finger_on_again, FALSE, enroll_result.percentage,
                           enroll_result.swipe_info, FALSE);
                finger_on_again = FALSE;

                if (enroll_result.status == FP_LIB_ENROLL_SUCCESS ||
                    enroll_result.status == FP_LIB_ENROLL_HELP_SAME_AREA) {
                    estate = ESTATE_GET_IMG;

                } else if (enroll_result.status == FP_LIB_ENROLL_FAIL_LOW_QUALITY) {
                    estate = ESTATE_FINGER_OFF;

                } else if (enroll_result.status == FP_LIB_ENROLL_FAIL_LOW_COVERAGE) {
                    estate = ESTATE_GET_IMG;

                } else if (enroll_result.status == FP_LIB_ENROLL_TOO_FAST) {
                    estate = ESTATE_GET_IMG;
                    enroll_option = ENROLL_OPTION_MERGE;
                    opm_do_enroll(&enroll_result, enroll_option, 0);

                    ex_log(LOG_ERROR, "opm_identify_enroll status %u", enroll_result.status);
                    if (SWIPE_FAST_CONTROL) {
                        is_too_fast = TRUE;
                        notify(EVENT_ENROLL_TOO_FAST, 0, 0, NULL, 0);
                        estate = ESTATE_FINGER_OFF;
                    }

                } else if (enroll_result.status == FP_LIB_ENROLL_HELP_ALREADY_EXIST) {
                    notify(EVENT_ENROLL_DUPLICATE_FINGER, 0, 0, NULL, 0);
                } else {
                    retval = FINGERPRINT_RES_ALGORITHM_ERROR;
                }

                break;

            case ESTATE_FINGER_OFF:
                ex_log(LOG_DEBUG, "estate == ESTATE_FINGER_OFF");

                opm_set_work_mode(DETECT_MODE);

                if (!wait_trigger(3, 30, TIMEOUT_WAIT_FOREVER)) {
                    estate = ESTATE_WAIT_INT;

                    if (is_too_fast) {
                        update_map(finger_on_again, TRUE, enroll_result.percentage,
                                   enroll_result.swipe_info, FALSE);
                        is_too_fast = FALSE;
                    }

                    if (!finger_on_again)
                        update_map(TRUE, FALSE, enroll_result.percentage, enroll_result.swipe_info,
                                   FALSE);

                    notify(EVENT_FINGER_LEAVE, 0, 0, NULL, 0);
                } else {
                    estate = ESTATE_GET_IMG;
                }
                break;
        }
    } while (retval == FINGERPRINT_RES_SUCCESS);

exit:
    // flow exit, close spi
    setSpiState(FALSE, FALSE);
    update_map(FALSE, FALSE, enroll_result.percentage, enroll_result.swipe_info, TRUE);

#ifdef __SUPPORT_SAVE_IMAGE__
    transfer_frames_to_client(TRANSFER_ENROLL_IMAGE, img_quality, FALSE, 0);
#endif

    return retval;
}
