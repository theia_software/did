#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "captain.h"
#include "common_definition.h"
#include "constant_def.h"
#include "core_config.h"
#include "device_int.h"
#include "egis_rbs_api.h"
#include "ex_define.h"
#include "extra_def.h"
#include "fp_definition.h"
#include "fps_normal.h"
#include "liver_image.h"
#include "navi_operator.h"
#include "op_manager.h"
#include "op_sensortest.h"
#include "opt_file.h"
#include "plat_file.h"
#include "plat_log.h"
#include "plat_mem.h"
#include "plat_time.h"
#include "response_def.h"
#include "save_image.h"
#include "thread_manager.h"
#include "transporter_test.h"
#include "type_def.h"

#ifdef SWIPE_ENROLL
#include "paint_enroll.h"
#include "swipe_enroll.h"
#include "update_map.h"
#endif
#ifdef __TRUSTONIC__
#include "opt_trustonic_transfer.h"
#endif
#define LOG_TAG "RBS-CAPTAIN"

#define AUTH_TOKEN_LEN 256

#define FPS_CALI_ET7XX_BKG 7
#define FPS_SAVE_ET7XX_CALI_DATA 8
#define FPS_LOAD_ET7XX_CALI_DATA 9
#define FPS_REMOVE_ET7XX_CALI_DATA 10
#define FPS_SAVE_ET7XX_CALI_DATA_TO_FLASH 11
#define FPS_CALI_UNINIT 12
#define FPS_REMOVE_ET7XX_BDS 13
#define FPS_SET_CALI_TYPE_BK 14
#define FPS_SET_CALI_TYPE_WK 15

cache_info_t g_cache_info;
fingerprint_enroll_info_t g_enroll_info;
fingerprint_verify_info_t g_verify_info;
enroll_config_t g_enroll_config;
static navigation_config_t g_navi_config;
static cut_img_config_t g_crop_config;

static unsigned int g_has_enroll_count = 0;
static int g_enroll_percentage_tmp = 0;

static BOOL g_need_cancel = FALSE;
static BOOL g_hardware_ready = FALSE;
extern BOOL g_navi_need_cancel;

int g_hdev = 0;
int g_temp_remaining = 0;
char g_user_path[MAX_PATH_LEN] = {0};
int g_enroll_timeout = TIMEOUT_WAIT_FOREVER;

const int TIMEOUT_FOR_ONE_TRY = 500;
event_callbck_t g_event_callback;

host_device_info_t g_host_device = {
    .temperature_x10 = 250,
};

static int g_touch_count = 0;
static const int MAX_TOUCH_POINT_COUNT = 5;
static touch_info_t g_touch_info[MAX_TOUCH_POINT_COUNT];

void set_host_device_info(host_device_info_t device) {
    g_host_device = device;
}
host_device_info_t get_host_device_info(void) {
    return g_host_device;
}

static int do_sensortest(int cid, unsigned char* in_data, int in_data_size,
                         unsigned char* out_buffer, int* out_buffer_size);
void captain_cancel(BOOL cancel_flag);
static void get_ca_version(unsigned char* version, int* len);
static int enroll_percentage_to_remaining(int percentage);
#if defined(__ET7XX__) && !defined(__ET0XX__)
int get_sys_temp(int* temperature);
int set_brightness(int level);
#endif

void notify(int event_id, int first_param, int second_param, unsigned char* data, int data_size) {
    if (NULL != g_event_callback) {
#ifndef EGIS_DBG
        if (event_id == EVENT_RETURN_IMAGE || event_id == EVENT_RETURN_LIVE_IMAGE) return;
#endif
        g_event_callback(event_id, first_param, second_param, data, data_size);
        if (event_id == EVENT_ENROLL_OK) {
            int remaining = enroll_percentage_to_remaining(second_param);
            if (g_temp_remaining != remaining)
                g_temp_remaining = remaining;
            else
                return;
            g_event_callback(EVENT_ENROLL_REMAINING, first_param, remaining, data, data_size);
        }
    }
#ifdef __ENABLE_NAVIGATION__
    if (event_id == EVENT_NAVIGATION) send_navi_event_to_driver(first_param);
#endif
}

static int create_ini_config(BOOL need_reinit, unsigned char* in_data, int in_data_size) {
    int retval = FINGERPRINT_RES_SUCCESS;

    retval = opt_send_data(TYPE_SEND_INI_CONFIG, in_data, in_data_size);
    if (retval != FINGERPRINT_RES_SUCCESS) {
        ex_log(LOG_ERROR, "create_ini_config return %d", retval);
        return FINGERPRINT_RES_FAILED;
    }

    g_enroll_config.enroll_method =
        core_config_get_int(INI_SECTION_ENROLL, KEY_ENROLL_METHOD, ENROLL_METHOD_TOUCH);
    g_enroll_config.swipe_dir =
        core_config_get_int(INI_SECTION_ENROLL, KEY_SWIPE_DIRECTION_MODE, SWIPE_DIRECTION_AUTO);
    g_enroll_config.swipe_count_y =
        core_config_get_int(INI_SECTION_ENROLL, KEY_SWIPE_COUNT_Y, DEFAULT_SWIPE_COUNT_Y);
    g_enroll_config.swipe_count_x =
        core_config_get_int(INI_SECTION_ENROLL, KEY_SWIPE_COUNT_X, DEFAULT_SWIPE_COUNT_X);
    g_enroll_config.enroll_too_fast_rollback =
        core_config_get_int(INI_SECTION_ENROLL, KEY_ENROLLEX_TOO_FAST_ROLLBACK, 0);
#ifdef LARGE_AREA
    g_enroll_config.enroll_max_count =
        core_config_get_int(INI_SECTION_ENROLL, KEY_MAX_ENROLL_COUNT, INID_MAX_ENROLL_COUNT);
#endif

    g_enroll_config.capture_delay_enroll_enable =
        core_config_get_int(INI_SECTION_ENROLL, KEY_CAPTURE_DELAY_ENABLE, 1);
    g_enroll_config.capture_delay_enroll_start_progress = core_config_get_int(
        INI_SECTION_ENROLL, KEY_CAPTURE_DELAY_START_PROGRESS, 80);  // last 5 of total 24, ~= 20%

    //g_enroll_config.enroll_extra_1st_enable = core_config_get_int(
    //    INI_SECTION_ENROLL, KEY_ENROLL_EXTRA_THE_FIRST_ENABLE, INID_ENROLL_EXTRA_THE_FIRST_ENABLE);
    g_enroll_config.enroll_extra_1st_enable = 0;
#ifdef LARGE_AREA
    g_enroll_config.enroll_extra_1st_before_progress =
        core_config_get_int(INI_SECTION_ENROLL, KEY_ENROLL_EXTRA_THE_FIRST_BEFORE_PROGRESS, 90);
#else
    g_enroll_config.enroll_extra_1st_before_progress =
        core_config_get_int(INI_SECTION_ENROLL, KEY_ENROLL_EXTRA_THE_FIRST_BEFORE_PROGRESS, 40);
#endif

    g_navi_config.navi_mode = core_config_get_int(INI_SECTION_NAVI, KEY_NAVI_MODE, 1);
    g_navi_config.change_x_y = core_config_get_int(INI_SECTION_NAVI, KEY_NAVI_CHANGE_X_Y, 0);
    g_navi_config.change_up_down =
        core_config_get_int(INI_SECTION_NAVI, KEY_NAVI_CHANGE_UP_DOWN, 0);
    g_navi_config.change_left_right =
        core_config_get_int(INI_SECTION_NAVI, KEY_NAVI_CHANGE_LEFT_RIGHT, 0);

    cut_img_config_t crop_config = {0};
    crop_config.enable_cut_img = core_config_get_int(INI_SECTION_SENSOR, KEY_CUT_IMAGE, 0);
    crop_config.algo_sensor_type =
        core_config_get_int(INI_SECTION_SENSOR, KEY_ALGO_INIT_SENSOR_TYPE, 0);
    crop_config.crop_width = core_config_get_int(INI_SECTION_SENSOR, KEY_CUT_IMAGE_WIDTH, 0);
    crop_config.crop_height = core_config_get_int(INI_SECTION_SENSOR, KEY_CUT_IMAGE_HEIGHT, 0);

    if (memcmp(&crop_config, &g_crop_config, sizeof(cut_img_config_t))) {
        if (!need_reinit) {
            opm_uninitialize_sensor();
            retval = opm_initialize_sensor();
            if (FINGERPRINT_RES_SUCCESS != retval) {
                return FINGERPRINT_RES_FAILED;
            }

            opm_uninitialize_algo();
            retval = opm_initialize_algo();
            if (FINGERPRINT_RES_SUCCESS != retval) {
                return FINGERPRINT_RES_FAILED;
            }
        }

        memcpy(&g_crop_config, &crop_config, sizeof(cut_img_config_t));
    }

    ex_log(LOG_DEBUG, "enroll_method = 0x%x", g_enroll_config.enroll_method);
    return retval;
}

static int destroy_ini_config(void) {
    return opt_send_data(TYPE_DESTROY_INI_CONFIG, NULL, 0);
}

static BOOL check_fingerprint_id_available(unsigned int fingerprint_id) {
    unsigned int index;

    ex_log(LOG_DEBUG, "check_fingerprint_id_available enter");

    for (index = 0; index < g_cache_info.fingerprint_ids_count; index++) {
        if (g_cache_info.fingerprint_ids[index] == fingerprint_id) {
            ex_log(LOG_DEBUG,
                   "check_fingerprint_id_available return false, "
                   "the same fingerprint id alredy exist");
            return FALSE;
        }
    }

    ex_log(LOG_DEBUG, "check_fingerprint_id_available return true");
    return TRUE;
}

static BOOL check_enrollment_space_available() {
    return g_cache_info.fingerprint_ids_count < __SINGLE_UPPER_LIMITS__ ? TRUE : FALSE;
}

static int sync_user_cache(unsigned int user_id) {
    int retval;
    unsigned int i;
    fingerprint_ids_t fps;

    ex_log(LOG_DEBUG, "sync_user_cache enter");

    if (user_id == g_cache_info.user_id) {
        return FINGERPRINT_RES_SUCCESS;
    }

    fps.fingerprint_ids_count = 0;
    retval = opm_get_fingerprint_ids(user_id, &fps);
    if (EX_RES_SUCCESS != retval) {
        ex_log(LOG_ERROR, "opm_get_fingerprint_ids return = %d", retval);
        return FINGERPRINT_RES_FAILED;
    }

    memcpy(g_cache_info.fingerprint_ids, fps.fingerprint_ids,
           fps.fingerprint_ids_count * sizeof(unsigned int));
    g_cache_info.fingerprint_ids_count = fps.fingerprint_ids_count;
    g_cache_info.user_id = user_id;

    if (fps.fingerprint_ids_count > 0) {
        g_cache_info.authenticator_id = 123;  // hard code magic number
        for (i = 0; i < fps.fingerprint_ids_count; i++) {
            g_cache_info.authenticator_id += fps.fingerprint_ids[i];
        }
    } else {
        g_cache_info.authenticator_id = (unsigned long long)-1;
    }

    ex_log(LOG_DEBUG, "opm_get_fingerprint_ids return = %d", retval);
    return FINGERPRINT_RES_SUCCESS;
}

BOOL check_cancelable() {
    return g_need_cancel;
}
static void power_off(void) {
    int ret = 0;
    ret = opm_set_work_mode(POWEROFF_MODE);
    if (ret != 0) {
        ex_log(LOG_ERROR, "power_off Fail = %d", ret);
    }
}

static BOOL g_spi_connected = FALSE;
void setSpiState(BOOL spi_on, BOOL forced) {
    if (!spi_on && (forced || g_spi_connected)) {
        opm_close_spi();
        g_spi_connected = FALSE;
    } else if (spi_on && (forced || !g_spi_connected)) {
        opm_open_spi();
        g_spi_connected = TRUE;
    } else {
        ex_log(LOG_DEBUG, "setSpiState do nothing, spi_on = %d, forced = %d, g_spi_connected = %d",
               spi_on, forced, g_spi_connected);
    }
    ex_log(LOG_VERBOSE, "setSpiState: spi_on = %d, forced = %d, g_spi_connected = %d", spi_on,
           forced, g_spi_connected);
}

static int touch_enroll(unsigned int* percentage, unsigned int enrolled_count) {
    int retval;
    int enroll_option = ENROLL_OPTION_NORMAL;
    unsigned int status = FP_LIB_FINGER_LOST;
    unsigned int estate = ESTATE_WAIT_INT;
    unsigned int img_quality;
    cmd_enrollresult_t enroll_result;
    memset(&enroll_result, 0, sizeof(cmd_enrollresult_t));

    host_touch_set_finger_reset();

    do {
        switch (estate) {
            case ESTATE_WAIT_INT:
                ex_log(LOG_DEBUG, "estate == ESTATE_WAIT_INT");
                if (check_cancelable()) return FINGERPRINT_RES_CANCEL;
                retval = opm_set_work_mode(DETECT_MODE);
                if (check_cancelable()) return FINGERPRINT_RES_CANCEL;

                if (FINGERPRINT_RES_SUCCESS != retval) {
                    ex_log(LOG_ERROR, "ESTATE_WAIT_INT set detect mode failed return = %d", retval);
                }
                notify(EVENT_FINGER_WAIT, 0, 0, NULL, 0);

                if (!wait_trigger(0, TIMEOUT_FOR_ONE_TRY, g_enroll_timeout)) {
                    if (get_wait_trigger_status() == FINGERPRINT_RES_SECURE_ENROLL_TIMEOUT) {
                        notify(EVENT_ENROLL_TIMEOUT, 0, 0, NULL, 0);
                        enroll_cancel_recovery_value();
                    }
                    ex_log(LOG_DEBUG, "wait_trigger is not success, status = %d",
                           get_wait_trigger_status());
                    return get_wait_trigger_status();
                }

                if ((enroll_result.percentage >
                     (uint32_t)g_enroll_config.capture_delay_enroll_start_progress) &&
                    g_enroll_config.capture_delay_enroll_enable) {
                    ex_log(LOG_INFO, "percentage = %d , enroll still triggered at %d",
                           enroll_result.percentage, g_enroll_config.capture_delay_enroll_enable);
                    estate = ESTATE_CAPTURE_DELAY;
                    break;
                }  // else go down to case ESTATE_GET_IMG;

            case ESTATE_GET_RAW:
                ex_log(LOG_DEBUG, "estate == ESTATE_GET_RAW");

                if (check_cancelable()) return FINGERPRINT_RES_CANCEL;
                retval = opm_get_raw_image(get_host_device_info(), g_touch_info[0].x_ratio_x100, g_touch_info[0].y_ratio_x100);
                if (host_touch_is_finger_off()) {
                    if (host_touch_is_finger_on_last()) {
                        host_touch_set_finger_reset();
                        ex_log(LOG_ERROR, "touch event not stable!?");
                        estate = ESTATE_GET_RAW;
                        break;
                    }
                    host_touch_set_finger_reset();
                    estate = ESTATE_FINGER_OFF;
                    notify(EVENT_IMG_FAST, 0, 0, NULL, 0);
                    break;  // don't output image
                }

                if (check_cancelable()) return FINGERPRINT_RES_CANCEL;
#ifdef __SHOW_LIVEIMAGE__
                transfer_frames_to_client(TRANSFER_LIVE_IMAGE, img_quality, FALSE, 0);
#endif
                if (retval != FINGERPRINT_RES_SUCCESS) {
                    ex_log(LOG_ERROR, "get raw image failed retval = %d", retval);
                    break;
                }
                estate = ESTATE_DO_IPP;
                break;
            case ESTATE_DO_IPP:
                ex_log(LOG_DEBUG, "estate == ESTATE_DO_IPP");
                retval = opm_get_image_ipp(&img_quality, g_touch_info[0].x_ratio_x100, g_touch_info[0].y_ratio_x100);
                if (retval != FINGERPRINT_RES_SUCCESS) {
                    ex_log(LOG_ERROR, "opm_get_image_ipp failed retval = %d", retval);
                    break;
                }
                ex_log(LOG_DEBUG, "opm_get_image quality = %d", img_quality);
                if (img_quality == FP_LIB_ENROLL_SUCCESS) {
                    estate = ESTATE_ENROLL;
                    notify(EVENT_FINGER_READY, 0, 0, NULL, 0);

                } else if (img_quality == FP_LIB_ENROLL_FAIL_LOW_QUALITY ||
                           img_quality == FP_LIB_ENROLL_FAIL_LOW_QUALITY_AND_LOW_COVERAGE) {
                    estate = ESTATE_FINGER_OFF;
                    notify(EVENT_IMG_BAD_QLTY, 0, 0, NULL, 0);
                } else if (img_quality == FP_LIB_ENROLL_FAIL_LOW_COVERAGE ||
                           host_touch_is_low_coverage()) {
                    estate = ESTATE_FINGER_OFF;
                    host_touch_set_low_coverage(FALSE);
                    notify(EVENT_IMG_PARTIAL, 0, 0, NULL, 0);
                } else if (img_quality == FP_LIB_ENROLL_HELP_TOO_WET) {
                    estate = ESTATE_FINGER_OFF;
                    notify(EVENT_IMG_WATER, 0, 0, NULL, 0);
                } else {
                    estate = ESTATE_WAIT_INT;
                }
                break;
            case ESTATE_ENROLL:
                ex_log(LOG_DEBUG, "estate == ESTATE_ENROLL");

                if (check_cancelable()) return FINGERPRINT_RES_CANCEL;
                retval = opm_do_enroll(&enroll_result, enroll_option, enrolled_count);
                if (check_cancelable()) return FINGERPRINT_RES_CANCEL;
                if (retval != FINGERPRINT_RES_SUCCESS) {
                    ex_log(LOG_ERROR, "opm_identify_enroll failed, return %d", retval);
                    ex_log(LOG_ERROR, "debug_check_enroll opm_identify_enroll failed, return %d", retval);
                    break;
                }

                ex_log(LOG_DEBUG, "opm_do_enroll status %u, (%d,%d) score=%d", enroll_result.status,
                       enroll_result.swipe_info.dx, enroll_result.swipe_info.dy,
                       enroll_result.swipe_info.similarity_score);

                *percentage = enroll_result.percentage;

                estate = ESTATE_FINGER_OFF;
                if (enroll_result.status == FP_LIB_ENROLL_HELP_SAME_AREA) {
                    notify(EVENT_ENROLL_HIGHLY_SIMILAR, 0, 0, NULL, 0);

                } else if (enroll_result.status == FP_LIB_ENROLL_SUCCESS) {
#ifdef __ET7XX__  // ENROLL_THE_FIRST
                    if (enroll_result.percentage < 100) {
                        ex_log(LOG_DEBUG, "enroll_extra_1st (%d) %d",
                               g_enroll_config.enroll_extra_1st_enable,
                               g_enroll_config.enroll_extra_1st_before_progress);
                        if (g_enroll_config.enroll_extra_1st_enable) {
#ifdef RBS_EVTOOL
                            // Skip the first reject-retry image of DQE flow
                            enroll_option = ENROLL_OPTION_REJECT_RETRY;
#endif
                            if (enroll_result.percentage >=
                                (uint32_t)g_enroll_config.enroll_extra_1st_before_progress) {
                                opm_do_enroll(&enroll_result, ENROLL_OPTION_STOP_DQE, 0);
                            } else if (enroll_option == ENROLL_OPTION_REJECT_RETRY) {
                                opm_do_enroll(&enroll_result, ENROLL_OPTION_ENROLL_THE_FIRST, 0);
                                notify(EVENT_ENROLL_LQ,
                                       g_enroll_info.fingerprint_info.fingerprint_id,
                                       enroll_result.percentage, NULL, 0);
                            }
                        }
                    }
#endif
                    if (enroll_option != ENROLL_OPTION_STOP_DQE) {
                        notify(EVENT_ENROLL_OK, g_enroll_info.fingerprint_info.fingerprint_id,
                               enroll_result.percentage, NULL, 0);
                    }
                } else if (enroll_result.status == FP_LIB_ENROLL_FAIL_LOW_QUALITY) {
                    notify(EVENT_IMG_BAD_QLTY, g_enroll_info.fingerprint_info.fingerprint_id,
                           enroll_result.percentage, NULL, 0);
#ifdef __ET7XX__
                } else if (enroll_result.status ==
                           FP_LIB_ENROLL_FAIL_LOW_QUALITY_DYNAMIC_REJECT_LEVEL) {
                    // notify(EVENT_IMG_BAD_QLTY, g_enroll_info.fingerprint_info.fingerprint_id,
                    //    enroll_result.percentage, NULL, 0);
                    ex_log(LOG_INFO, "debug_check_enroll break");
                    estate = ESTATE_GET_RAW;
                    enroll_option = ENROLL_OPTION_REJECT_RETRY;
                    break;
                } else if (enroll_result.status == FP_LIB_ENROLL_FAIL_SPOOF_FINGER) {
                    ex_log(LOG_INFO, "verify, finger is fake.");
                    estate = VSTATE_FINGER_OFF;
                    notify(EVENT_IMG_FAKE_FINGER, 0, 0, NULL, 0);
#endif
                } else if (enroll_result.status == FP_LIB_ENROLL_FAIL_LOW_COVERAGE) {
                    notify(EVENT_IMG_PARTIAL, g_enroll_info.fingerprint_info.fingerprint_id,
                           enroll_result.percentage, NULL, 0);

                } else if (enroll_result.status == FP_LIB_ENROLL_TOO_FAST) {
                    // Todo
                } else if (enroll_result.status == FP_LIB_ENROLL_HELP_SCRATCH_DETECTED) {
                    ex_log(LOG_DEBUG, "SCRATCH_DETECTED");
                    notify(EVENT_ENROLL_SCRATCH_DETECTED, 0, 0, NULL, 0);
                } else if (enroll_result.status == FP_LIB_ENROLL_HELP_ALREADY_EXIST) {
                    notify(EVENT_ENROLL_DUPLICATE_FINGER, 0, 0, NULL, 0);

                } else {
                    ex_log(LOG_ERROR, "FINGERPRINT_RES_ALGORITHM_ERROR");
                    retval = FINGERPRINT_RES_ALGORITHM_ERROR;
                }

#ifdef __SUPPORT_SAVE_IMAGE__
                transfer_frames_to_client(TRANSFER_ENROLL_IMAGE, img_quality, FALSE, 0);
#endif
#ifdef __ET7XX__  // KEEP_ENROLL_RETRY_IMAGES
                opm_do_enroll(&enroll_result, ENROLL_OPTION_CLEAR_IMAGES, 0);
#endif
                break;

            case ESTATE_CAPTURE_DELAY: {
                ex_log(LOG_DEBUG, "estate == ESTATE_CAPTURE_DELAY");
                event_check_fd check[2];

                /*ESTATE_CAPTURE_DELAY will delay for awhile.*/
                setSpiState(FALSE, FALSE);

                if (check_cancelable()) return FINGERPRINT_RES_CANCEL;
                check[0].checkerFunc = &check_cancelable;
                check[1].checkerFunc = &host_touch_is_finger_off;
                event_poll_wait(
                    check, 2,
#ifdef LARGE_AREA
                    core_config_get_int(INI_SECTION_ENROLL, KEY_CAPTURE_DELAY_WAITING_TIME, 200)
#else
                    core_config_get_int(INI_SECTION_ENROLL, KEY_CAPTURE_DELAY_WAITING_TIME, 450)
#endif
                );
                if (check[0].revent) return FINGERPRINT_RES_CANCEL;

                setSpiState(TRUE, FALSE);
                estate = ESTATE_GET_RAW;

                if (check[1].revent) {
                    estate = ESTATE_WAIT_INT;
                    notify(EVENT_IMG_FAST, 0, 0, NULL, 0);
                    break;
                }

                break;
            }
            case ESTATE_FINGER_OFF:
                ex_log(LOG_DEBUG, "estate == ESTATE_FINGER_OFF");
                enroll_option = ENROLL_OPTION_NORMAL;
                /*ESTATE_FINGER_OFF will be excuted many times until the finger leaving
                calling setSpiState(on) to keep the spi open for a while to avoid continual opening
                or closing the spi */
                setSpiState(TRUE, FALSE);

                if (check_cancelable()) {
                    retval = FINGERPRINT_RES_CANCEL;
                    break;
                }
                if (host_touch_is_enable()) {
                    status = FP_LIB_FINGER_LOST;
                } else {
                    opm_check_finger_lost(30, &status);
                }
                if (check_cancelable()) {
                    retval = FINGERPRINT_RES_CANCEL;
                    break;
                }

                // When finger is not lost, CORE change to detect mode already
                // opm_set_work_mode(DETECT_MODE);

                if (status == FP_LIB_FINGER_LOST) {
                    estate = ESTATE_WAIT_INT;
                    notify(EVENT_FINGER_LEAVE, 0, 0, NULL, 0);
                    setSpiState(FALSE, FALSE);
                }
                break;
        }
    } while (retval == FINGERPRINT_RES_SUCCESS && enroll_result.percentage < 100);

    setSpiState(FALSE, FALSE);
    return retval;
}

static int do_enroll() {
    int retval;
    unsigned int percentage = 0, enrolled_count = 0;
    retval = opm_enroll_initialize();
    if (retval != 0) {
        notify(EVENT_ENROLL_FAILED, 0, 0, NULL, 0);
        return retval;
    }

    if (core_config_get_int(INI_SECTION_ENROLL, KEY_ENROLL_DUPLICATE, 0)) {
        retval = opm_get_enrolled_count(&enrolled_count);
        if (retval) {
            ex_log(LOG_ERROR, "opm_get_enrolled_count failed, return %d", retval);
            goto finish;
        }
    }

    switch (g_enroll_config.enroll_method) {
        case ENROLL_METHOD_TOUCH:
            retval = touch_enroll(&percentage, enrolled_count);
            break;
#ifdef SWIPE_ENROL
        case ENROLL_METHOD_PAINT:
            retval = paint_enroll(&percentage, enrolled_count);
            break;
        case ENROLL_METHOD_SWIPE:
            retval = swipe_enroll(&percentage, enrolled_count);
            break;
#endif
    }

    ex_log(LOG_DEBUG, "percentage = %d,enroll mode 0x%x", percentage,
           g_enroll_config.enroll_method);
    ex_log(LOG_DEBUG, "ENROLL loop end!!!!!! retval = %d", retval);

finish:
    opm_enroll_uninitialize();

    if (percentage >= 100) {
        retval = opm_save_enrolled_fingerprint(g_enroll_info);
        if (FINGERPRINT_RES_SUCCESS != retval) {
            ex_log(LOG_ERROR, "saved fingerprint failed");
        } else {
#ifdef __TRUSTONIC__
            unsigned int temp[2];
            temp[0] = g_enroll_info.fingerprint_info.user_id;
            temp[2] = g_enroll_info.fingerprint_info.fingerprint_id;
            opt_turstoinc_data_transfer(DATA_TRANSFER_ENROLL_FINISH, (unsigned char*)temp,
                                        sizeof(temp));
#endif
            g_cache_info.user_id = -1;
            sync_user_cache(g_enroll_info.fingerprint_info.user_id);
            notify(EVENT_ENROLL_SUCCESS, 0, 0, NULL, 0);
        }
    }

    if (retval == FINGERPRINT_RES_CANCEL) {
        notify(EVENT_ENROLL_CANCELED, 0, 0, NULL, 0);
    } else if (retval != FINGERPRINT_RES_SUCCESS) {
        notify(EVENT_ERR_ENROLL, 0, 0, NULL, 0);
    }
#ifdef __ET7XX__
    retval = opm_save_bds();
    if (retval != FP_LIB_OK) ex_log(LOG_ERROR, "%s, Save BDS Pool Fail", __func__);
#endif
    return FINGERPRINT_RES_SUCCESS;
}

static int do_verify() {
    const uint32_t LIVENESS_AUTHENTICATION = 0;
    int retval;
    unsigned int status;
    unsigned int match_id;
    BOOL has_result = FALSE;
    unsigned int quality;
    unsigned int vstate = VSTATE_WAIT_INT;
    unsigned char auth_token[AUTH_TOKEN_LEN];
    unsigned int auth_token_len = AUTH_TOKEN_LEN;
    int flow_trymatch_count =
        core_config_get_int(INI_SECTION_VERIFY, KEY_FLOW_TRY_MATCH, INID_FLOW_TRY_MATCH);
    int flow_try_match = flow_trymatch_count;
    int ext_feat_quality_trymatch_th =
        core_config_get_int(INI_SECTION_VERIFY, KEY_EXT_FEAT_QUALITY_TRYMATCH_THRESHOLD,
                            INID_EXT_FEAT_QUALITY_TRYMATCH_THRESHOLD);
    int ext_feat_quality_badimg_th =
        core_config_get_int(INI_SECTION_VERIFY, KEY_EXT_FEAT_QUALITY_BADIMG_THRESHOLD, 10);
    int try_match_even_bad =
        core_config_get_int(INI_SECTION_VERIFY, KEY_TRY_MATCH_EVEN_TOO_PARTIAL, INID_TRY_MATCH_EVEN_TOO_PARTIAL);

	int touch_count = 0;
	int verified_finger_count = 0;
	touch_info_t touch_info[MAX_TOUCH_POINT_COUNT];
	touch_info_t current_touch_info;
	int mt_match_result[MAX_TOUCH_POINT_COUNT] = {0};
	int mt_user_id = 0;
	int mt_match_id = 0;
    unsigned char mt_auth_token[AUTH_TOKEN_LEN];
    unsigned int mt_auth_token_len = AUTH_TOKEN_LEN;

    unsigned int enrolled_count = 0;
    unsigned char is_tmpl_updated = 0;
    int ext_feat_quality = 0;
    int verify_finger_count = core_config_get_int(INI_SECTION_VERIFY, KEY_VERIFY_FINGER_COUNT, INID_VERIFY_FINGER_COUNT);
    ex_log(LOG_DEBUG, "PAPA MT verify_finger_count is %d", verify_finger_count);

    ex_log(LOG_DEBUG, "%s, flow_try_match=%d", __func__, flow_try_match);

    host_touch_set_finger_reset();

    retval = opm_identify_start(LIVENESS_AUTHENTICATION);
    if (retval != FINGERPRINT_RES_SUCCESS) return retval;
#ifdef EGIS_DEBUG_MEMORY
    retval = opm_print_memory(0);
    if (retval != FINGERPRINT_RES_SUCCESS) {
        ex_log(LOG_ERROR, "opm_print_memory error");
        return retval;
    }
#endif
VERIFY_AGAIN:
	//init verify parameters
	{
		retval = opm_identify_start(LIVENESS_AUTHENTICATION);
		if (retval != FINGERPRINT_RES_SUCCESS) 
			return retval;

		has_result = FALSE;
		//vstate = VSTATE_WAIT_INT;
		if ((verify_finger_count > 1) && (verified_finger_count > 0)) {
			if (verified_finger_count < touch_count)
				vstate = VSTATE_GET_RAW;
		}
		enrolled_count = 0;//?
		is_tmpl_updated = 0;//?
		ext_feat_quality = 0;//?
        flow_try_match = flow_trymatch_count;
	}
    do {
        switch (vstate) {
            case VSTATE_WAIT_INT: {
                ex_log(LOG_DEBUG, "vstate == VSTATE_WAIT_INT");
                flow_try_match = flow_trymatch_count;
                if (check_cancelable()) goto EXIT;
                retval = opm_set_work_mode(DETECT_MODE);
                if (check_cancelable()) goto EXIT;
                if (FINGERPRINT_RES_SUCCESS != retval) {
                    ex_log(LOG_ERROR, "VSTATE_WAIT_INT set detect mode failed, retval = %d",
                           retval);
                    break;
                }
                notify(EVENT_FINGER_WAIT, 0, 0, NULL, 0);
				egislog_d("darkresearch large_area_multi_finger wait_trigger");
                if (!wait_trigger(0, TIMEOUT_FOR_ONE_TRY, TIMEOUT_WAIT_FOREVER)) goto EXIT;
				egislog_d("darkresearch large_area_multi_finger after wait_trigger");

				int i;
				touch_count = g_touch_count;
				for (i = 0; i < g_touch_count; i++) {
					touch_info[i].x_ratio_x100 = g_touch_info[i].x_ratio_x100;
					touch_info[i].y_ratio_x100 = g_touch_info[i].y_ratio_x100;
				}

				//For test, fix positions of finger touch
				////verify_finger_count = 2;
				//touch_count = 2;
				//touch_info[0].x_ratio_x100 = 17;
				//touch_info[0].y_ratio_x100 = 50;
				//touch_info[1].x_ratio_x100 = 81;
				//touch_info[1].y_ratio_x100 = 58;
				{
					int i;

					egislog_d("darkresearch large_area_multi_finger verify_finger_count %d", verify_finger_count);
					egislog_d("darkresearch large_area_multi_finger touch_count %d", touch_count);
					for (i = 0; i < touch_count; i++) {
						egislog_d("darkresearch large_area_multi_finger finger %d x_ratio_x100 %d y_ratio_x100 %d",
												i, touch_info[i].x_ratio_x100, touch_info[i].y_ratio_x100);
					}
				}
            }

            case VSTATE_GET_RAW:
                ex_log(LOG_DEBUG, "vstate == VSTATE_GET_RAW");
                TIME_MEASURE_START(total_verify);

				current_touch_info.x_ratio_x100 = touch_info[verified_finger_count].x_ratio_x100;
				current_touch_info.y_ratio_x100 = touch_info[verified_finger_count].y_ratio_x100;

                if (check_cancelable()) goto EXIT;
                retval = opm_get_raw_image(get_host_device_info(), current_touch_info.x_ratio_x100, current_touch_info.y_ratio_x100);
                if (check_cancelable()) goto EXIT;

                if (retval != FINGERPRINT_RES_SUCCESS) {
                    ex_log(LOG_ERROR, "opm_get_raw_image failed retval = %d", retval);
                    break;
                }

                if (host_touch_is_finger_off()) {
                    if (host_touch_is_finger_on_last()) {
                        host_touch_set_finger_reset();
                        ex_log(LOG_ERROR, "touch event not stable!?");
                        vstate = VSTATE_GET_RAW;
                        break;
                    }
                    host_touch_set_finger_reset();
                    vstate = VSTATE_WAIT_INT;
                    notify(EVENT_IMG_FAST, 0, 0, NULL, 0);
                    opt_send_data(TYPE_SEND_RESET_IMAGE_FRAME_COUNT, NULL, 0);
                    break;  // don't output image
                }
                vstate = VSTATE_DO_IPP;
                break;

            case VSTATE_DO_IPP:
                ex_log(LOG_DEBUG, "vstate == VSTATE_DO_IPP");
                retval = opm_get_image_ipp(&quality, current_touch_info.x_ratio_x100, current_touch_info.y_ratio_x100);
                if (retval != FINGERPRINT_RES_SUCCESS) {
                    ex_log(LOG_ERROR, "opm_get_image_ipp failed retval = %d", retval);
                    break;
                }
                ex_log(LOG_DEBUG, "opm_get_raw_image retval = %d, quality = %d", retval, quality);

                if (FP_LIB_ENROLL_SUCCESS == quality) {
                    vstate = VSTATE_VERIFY;
                    notify(EVENT_FINGER_READY, 0, 0, NULL, 0);

                } else if (FP_LIB_ENROLL_FAIL_LOW_QUALITY == quality ||
                           FP_LIB_ENROLL_FAIL_LOW_QUALITY_AND_LOW_COVERAGE == quality ||
                           FP_LIB_ENROLL_FAIL_RESIDUAL == quality) {
                    ex_log(LOG_INFO, "verify, notify EVENT_IMG_BAD_QLTY");
                    if (try_match_even_bad) {
                      vstate = VSTATE_VERIFY;
                    } else {
                      vstate = VSTATE_FINGER_OFF;
                      notify(EVENT_IMG_BAD_QLTY, 0, 0, NULL, 0);
                    }
                } else if (FP_LIB_ENROLL_FAIL_LOW_COVERAGE == quality) {
                    ex_log(LOG_INFO, "verify, FP_LIB_ENROLL_FAIL_LOW_COVERAGE");
                    vstate = VSTATE_VERIFY;
                    break;
                } else if (FP_LIB_ENROLL_HELP_TOO_WET == quality) {
                    vstate = VSTATE_FINGER_OFF;
                    notify(EVENT_IMG_WATER, 0, 0, NULL, 0);

                } else if (FP_LIB_ENROLL_FAIL_SPOOF_FINGER == quality) {
                    ex_log(LOG_INFO, "verify, finger is fake.");
                    vstate = VSTATE_FINGER_OFF;
                    notify(EVENT_IMG_FAKE_FINGER, 0, 0, NULL, 0);

                } else {
                    vstate = VSTATE_WAIT_INT;
                    break;
                }
#ifdef __SUPPORT_SAVE_IMAGE__
                if (vstate == VSTATE_FINGER_OFF) {
					egislog_i("darkresearch large_area_multi_finger call transfer_frames_to_client");
                    transfer_frames_to_client(TRANSFER_VERIFY_IMAGE_V2, quality, FALSE, 0);
                }
#endif
                break;

            case VSTATE_VERIFY:
                ex_log(LOG_DEBUG, "vstate == VSTATE_VERIFY");

                retval = opm_get_enrolled_count(&enrolled_count);
                if (retval) {
                    ex_log(LOG_ERROR, "opm_get_enrolled_count failed, return %d", retval);
                }
                if (enrolled_count == 0) {
                    ex_log(LOG_ERROR, "no fingerprints, identify will be failed");
                }

                if (check_cancelable()) goto EXIT;
                auth_token_len = AUTH_TOKEN_LEN;
                {
                    unsigned char verify_info[2];
                    verify_info[0] = (unsigned char)verify_finger_count;
                    verify_info[1] = (unsigned char)verified_finger_count;
				    //opm_set_data(TYPE_SEND_CURRENT_VERIFY_FINGER, verify_info, sizeof(verify_info));
                }
                opm_set_verify_finger_info(verify_finger_count, verified_finger_count);
                retval = opm_identify(&g_verify_info, &match_id, &status, auth_token,
                                      &auth_token_len, &ext_feat_quality);
                ex_log(LOG_DEBUG,
                       "opm_do_verify ret=%d, status=%u, match_id=%d, ext_feat_quality=%d", retval,
                       status, match_id, ext_feat_quality);
                if (check_cancelable()) goto EXIT;

                if (retval != FINGERPRINT_RES_SUCCESS) {
                    break;
                }

                int is_good_extract_qty = (ext_feat_quality > ext_feat_quality_trymatch_th) ? 1 : 0;
                if (FP_LIB_IDENTIFY_NO_MATCH == status || FP_LIB_IDENTIFY_RESIDUAL == status) {
                    ex_log(LOG_DEBUG, "(%d) threshold %d, %d", is_good_extract_qty,
                           ext_feat_quality_trymatch_th, ext_feat_quality_badimg_th);
                    if (FP_LIB_IDENTIFY_RESIDUAL == status) {
                        flow_try_match = 0;
                        ex_log(LOG_DEBUG, "FP_LIB_IDENTIFY_RESIDUAL");
                    }

                    if ((flow_try_match-- > 0) && !is_good_extract_qty) {
                        ex_log(LOG_DEBUG, "flow_try_match=%d", flow_try_match);
                        vstate = VSTATE_GET_RAW;
						//if (verify_finger_count > 1) {
						//	opt_send_data(TYPE_SEND_RESET_IMAGE_FRAME_COUNT, NULL, 0);
						//}
                        break;
                    } else {
                        TIME_MEASURE_STOP(total_verify, "match Fail");
                        if (quality == FP_LIB_ENROLL_FAIL_LOW_COVERAGE) {
                            notify(EVENT_IMG_PARTIAL, 0, 0, NULL, 0);
#ifdef __ET7XX__
                        } else if ((ext_feat_quality < ext_feat_quality_badimg_th) ||
                            quality == FP_LIB_ENROLL_FAIL_LOW_QUALITY) {
                            ex_log(LOG_DEBUG, "bad ext_feat_quality %d < %d", ext_feat_quality,
                                   ext_feat_quality_badimg_th);
                            quality = FP_LIB_ENROLL_FAIL_LOW_QUALITY;
                            notify(EVENT_IMG_BAD_QLTY, 0, 0, NULL, 0);
#endif
                        } else {
							if (verify_finger_count == 1)
								notify(EVENT_VERIFY_NOT_MATCHED, 0, 0, NULL, 0);
							else {
								egislog_d("darkresearch large_area_multi_finger not match, keep result");
							}
                        }
#if defined(SIMULATE_NO_SENSOR) || defined(RBS_EVTOOL)
                        ex_log(LOG_DEBUG, "evtool has_result");
                        has_result = TRUE;
#endif
                    }
                } else if (FP_LIB_IDENTIFY_MATCH == status ||
                           FP_LIB_IDENTIFY_MATCH_UPDATED_TEMPLATE == status) {
                    TIME_MEASURE_STOP(total_verify, "matched");
					if (verify_finger_count == 1) {
						notify(EVENT_VERIFY_MATCHED, g_verify_info.user_id, match_id, auth_token,
							   auth_token_len);
					} else {
						mt_match_result[verified_finger_count] = 1;
						mt_user_id = g_verify_info.user_id;
						mt_match_id = match_id;
						mt_auth_token_len = auth_token_len;
						memcpy(mt_auth_token, auth_token, auth_token_len);

						egislog_d("darkresearch large_area_multi_finger match, not send result directly");
						egislog_d("darkresearch large_area_multi_finger mt_match_result[%d] %d", verified_finger_count, mt_match_result[verified_finger_count]);
						egislog_d("darkresearch large_area_multi_finger mt_user_id %d", mt_user_id);
						egislog_d("darkresearch large_area_multi_finger mt_match_id %d", mt_match_id);
					}
                    has_result = TRUE;
                    retval = opm_identify_template_update(&is_tmpl_updated);
                    if (retval == FINGERPRINT_RES_SUCCESS && is_tmpl_updated) {
                        opm_identify_template_save();
#ifdef __TRUSTONIC__
                        int fid_size = sizeof(match_id);
                        opt_turstoinc_data_transfer(DATA_TRANSFER_VERIFY_FINISH,
                                                    (unsigned char*)&match_id, fid_size);
#endif
                    }
                } else {
                    ex_log(LOG_ERROR, "unknown identify result %d", status);
                }

				//For latest finger, send result
				if (((verify_finger_count > 1) && (verified_finger_count == (touch_count - 1)))) {
					int is_match = 0;
					int i;
					egislog_d("darkresearch large_area_multi_finger latest finger");
					for (i = 0; i < touch_count; i++) {
						egislog_d("darkresearch large_area_multi_finger mt_match_result[%d] %d", i, mt_match_result[i]);
						if (mt_match_result[i] == 1) {
							++is_match;
						}
					}
					egislog_d("darkresearch large_area_multi_finger is_match %d touch_count %d", 
								is_match, touch_count);
					if (is_match == touch_count) {
						has_result = 1;
						notify(EVENT_VERIFY_MATCHED, mt_user_id, mt_match_id, mt_auth_token,
							   mt_auth_token_len);
					} else {
						has_result = 0;
						notify(EVENT_VERIFY_NOT_MATCHED, 0, 0, NULL, 0);
					}
				}

                vstate = VSTATE_FINGER_OFF;
#ifdef __SUPPORT_SAVE_IMAGE__
				egislog_i("darkresearch large_area_multi_finger call transfer_frames_to_client");
                transfer_frames_to_client(TRANSFER_VERIFY_IMAGE_V2, quality, FALSE, status);
#endif
                break;

            case VSTATE_FINGER_OFF: {
                ex_log(LOG_DEBUG, "vstate == VSTATE_FINGER_OFF");

                setSpiState(TRUE, FALSE);

                if (check_cancelable()) goto EXIT;
                if (host_touch_is_enable()) {
					egislog_d("darkresearch large_area_multi_finger host_touch_is_enable true");
                    status = FP_LIB_FINGER_LOST;
                    opt_send_data(TYPE_SEND_RESET_IMAGE_FRAME_COUNT, NULL, 0);
                } else {
					egislog_d("darkresearch large_area_multi_finger host_touch_is_enable false");
                    opm_check_finger_lost(30, &status);
                }
                if (check_cancelable()) goto EXIT;

				//For multi finger, skip finger off detect if not verify lastest finger
				egislog_d("darkresearch large_area_multi_finger verify_finger_count %d touch_count %d", 
									verify_finger_count, touch_count);
				egislog_d("darkresearch large_area_multi_finger has_result %d ", has_result);
				if ((verify_finger_count > 1) && (verified_finger_count < (touch_count - 1))) {
					//Exit without waiting finger off if multi finger and there is any finger not verified yet
					egislog_d("darkresearch large_area_multi_finger skip finger off check if still fingers should be verified");
					goto CHECK_VERIFY_AGAIN;
				}

                // When finger is not lost, CORE change to detect mode already
                // opm_set_work_mode(DETECT_MODE);

				egislog_d("darkresearch large_area_multi_finger status %d FP_LIB_FINGER_LOST %d", 
								status, FP_LIB_FINGER_LOST);
                if (status == FP_LIB_FINGER_LOST || !wait_trigger(3, 30, TIMEOUT_WAIT_FOREVER)) {
                    vstate = ESTATE_WAIT_INT;
                    notify(EVENT_FINGER_LEAVE, 0, 0, NULL, 0);
                    setSpiState(FALSE, FALSE);
					if (verify_finger_count > 1) {
						if (has_result) {
							goto EXIT;
						} else {
							//Multi finger with lastest verification result is not matched
							egislog_d("darkresearch large_area_multi_finger not has_result finger off, exit");
							goto CHECK_VERIFY_AGAIN;
						}
					} else {
						if (has_result) {
							egislog_d("darkresearch large_area_multi_finger has_result and finger off, exit");
							goto EXIT;
						} 
					}
                }
            } break;
        }
    } while (retval == FINGERPRINT_RES_SUCCESS);

CHECK_VERIFY_AGAIN:
	if (verify_finger_count > 1) {
		egislog_d("darkresearch large_area_multi_finger has_result %d", has_result);
		egislog_d("darkresearch large_area_multi_finger verify_finger_count %d", verify_finger_count);
		egislog_d("darkresearch large_area_multi_finger touch_count %d", touch_count);
		egislog_d("darkresearch large_area_multi_finger verified_finger_count %d", verified_finger_count);
		egislog_d("darkresearch large_area_multi_finger match_id %d", match_id);
		if ((++verified_finger_count < touch_count)) {
			egislog_d("darkresearch large_area_multi_finger verify again");
			opm_identify_finish();
			goto VERIFY_AGAIN;
		} else if (verified_finger_count == touch_count) {
			//Multi finger and all fingers are verified, reset it the lastest finger is not matched
            vstate = ESTATE_WAIT_INT;
			verified_finger_count = 0;
			touch_count = 0;
			//mt_match_result = -1;
			memset(mt_match_result, 0, sizeof(int)*MAX_TOUCH_POINT_COUNT);
			mt_user_id = 0;
			mt_match_id = 0;
			memset(mt_auth_token, 0, AUTH_TOKEN_LEN);
			opm_identify_finish();
			goto VERIFY_AGAIN;
		}
	}

EXIT:
	egislog_d("darkresearch large_area_multi_finger EXIT");
    setSpiState(FALSE, FALSE);
    opm_identify_finish();

#ifdef ALGO_GEN_4
    opt_receive_data(TYPE_RECEIVE_DEBASE, NULL, 0, NULL, NULL);
#endif

    if (retval != FINGERPRINT_RES_SUCCESS) {
        notify(EVENT_ERR_IDENTIFY, 0, 0, NULL, 0);
    }
    notify(EVENT_IDENTIFY_FINISH, 0, 0, NULL, 0);
#ifdef __ET7XX__
    retval = opm_save_bds();
    if (retval != FP_LIB_OK) ex_log(LOG_ERROR, "%s, Save BDS Pool Fail", __func__);
#endif
    return FINGERPRINT_RES_SUCCESS;
}

#ifdef __ENABLE_NAVIGATION__
static int do_navigation_event_in_flow() {
    int retval;
    int event_size;
    unsigned int nstate = NSTATE_WAIT_INT;

#define OUT_COUNT 5
#define CLICK_TIME 700
#define LONG_CLICK 700
#define DOUBLE_INTERVAL 200

    struct NaviStatusInfo event;
    event_size = sizeof(struct NaviStatusInfo);
    int navi_up = 0;
    int navi_down = 0;
    int navi_left = 0;
    int navi_right = 0;
    int navi_mode = NAVI_MODE_NORMAL;
    int navi_status = NAVI_CMD_START;

    unsigned long long time_start_waitfinger;
    unsigned long cost_fingeron;
    BOOL has_fingeron = FALSE;
    BOOL is_check_click = TRUE;
    BOOL is_check_long_click = FALSE;
    BOOL is_check_double_wait = FALSE;

    navi_mode = g_navi_config.navi_mode;

    retval = opm_navi_control(0, (unsigned char*)&navi_status, sizeof(int), NULL, &event_size);

    do {
        switch (nstate) {
            case NSTATE_WAIT_INT: {
                if (check_cancelable()) goto exit;
                retval = opm_set_work_mode(DETECT_MODE);
                if (check_cancelable()) goto exit;

                if (FINGERPRINT_RES_SUCCESS != retval) {
                    ex_log(LOG_ERROR, "ESTATE_WAIT_INT set detect mode failed return = %d", retval);
                }

                navi_up = 0;
                navi_down = 0;
                navi_left = 0;
                navi_right = 0;

                ex_log(LOG_ERROR,
                       "USE_CORE_CONFIG_INI status_fingeron %d,is_check_click "
                       "%d,is_check_double_wait %d,longlick %d ",
                       has_fingeron, is_check_click, is_check_double_wait, is_check_long_click);

                if (has_fingeron && is_check_click && !is_check_long_click) {
                    cost_fingeron = plat_get_diff_time(time_start_waitfinger);
                    ex_log(LOG_ERROR, "do_navigation fingeron time %d ", cost_fingeron);
                    if (cost_fingeron < CLICK_TIME) {
                        if (is_check_double_wait) {
                            ex_log(LOG_ERROR, "do_navigation notify double click");
                            notify(EVENT_NAVIGATION, EVENT_DCLICK, 0, NULL, 0);
                            is_check_double_wait = FALSE;
                        } else {
                            if (wait_trigger(DOUBLE_INTERVAL / 30, 30, TIMEOUT_WAIT_FOREVER)) {
                                ex_log(LOG_ERROR, "do_navigation ready for double click");
                                time_start_waitfinger = plat_get_time();
                                is_check_double_wait = TRUE;
                                has_fingeron = FALSE;
                            } else {
                                ex_log(LOG_ERROR, "do_navigation notify click");
                                notify(EVENT_NAVIGATION, EVENT_CLICK, 0, NULL, 0);
                            }
                        }
                    } else {
                        is_check_double_wait = FALSE;
                    }
                } else {
                    is_check_double_wait = FALSE;
                }

                if (!is_check_double_wait) {
                    has_fingeron = FALSE;
                    is_check_click = TRUE;
                    is_check_long_click = FALSE;
                    notify(EVENT_FINGER_WAIT, 0, 0, NULL, 0);
                    if (!wait_trigger(0, TIMEOUT_FOR_ONE_TRY, TIMEOUT_WAIT_FOREVER)) goto exit;

                    time_start_waitfinger = plat_get_time();
                }

                nstate = NSTATE_NAVI;
            } break;

            case NSTATE_NAVI: {
                event_size = sizeof(struct NaviStatusInfo);
                if (check_cancelable()) goto exit;
                retval = opm_get_navi_event((unsigned char*)&event, &event_size);
                if (check_cancelable()) goto exit;

                if (!event.is_finger) {
                    nstate = NSTATE_WAIT_INT;
                    break;
                }
                ex_log(LOG_DEBUG, "EGIS_NAVIGATION NAVI_DX:%d, NAVI_DY:%d", event.navi_dx,
                       event.navi_dy);
                if (navi_mode == NAVI_MODE_NORMAL) {
                    has_fingeron = TRUE;
                    if (abs(event.navi_dx) > abs(event.navi_dy)) {
                        if (event.navi_dx > 0) {
                            navi_right++;
                        } else {
                            navi_left++;
                        }
                    } else if (abs(event.navi_dx) < abs(event.navi_dy)) {
                        if (event.navi_dy > 0) {
                            navi_up++;
                        } else {
                            navi_down++;
                        }
                    }

                    if (navi_up >= OUT_COUNT) {
                        ex_log(LOG_ERROR, "do_navigation notify up");
                        notify(EVENT_NAVIGATION, EVENT_UP, 0, NULL, 0);
                        navi_up = 0;
                        is_check_click = FALSE;
                    } else if (navi_down >= OUT_COUNT) {
                        ex_log(LOG_ERROR, "do_navigation notify down");
                        notify(EVENT_NAVIGATION, EVENT_DOWN, 0, NULL, 0);
                        navi_down = 0;
                        is_check_click = FALSE;
                    } else if (navi_left >= OUT_COUNT) {
                        ex_log(LOG_ERROR, "do_navigation notify left");
                        notify(EVENT_NAVIGATION, EVENT_LEFT, 0, NULL, 0);
                        navi_left = 0;
                        is_check_click = FALSE;
                    } else if (navi_right >= OUT_COUNT) {
                        ex_log(LOG_ERROR, "do_navigation notify right");
                        notify(EVENT_NAVIGATION, EVENT_RIGHT, 0, NULL, 0);
                        navi_right = 0;
                        is_check_click = FALSE;
                    }
                    if (is_check_click) {
                        cost_fingeron = plat_get_diff_time(time_start_waitfinger);
                        if (cost_fingeron > LONG_CLICK) {
                            ex_log(LOG_ERROR, "do_navigation notify hold");
                            notify(EVENT_NAVIGATION, EVENT_HOLD, 0, NULL, 0);
                            time_start_waitfinger = plat_get_time();
                            is_check_long_click = TRUE;
                            ex_log(LOG_ERROR, "do_navigation notify long click ");
                        }
                    }
                } else {
                    notify(EVENT_NAVI_DX_DY, event.navi_dx, event.navi_dy, NULL, 0);
                }

            } break;
        }
    } while (1);

exit:
    ex_log(LOG_DEBUG, "navigation loop end!!!!!! retval = %d", retval);

    navi_status = NAVI_CMD_STOP;
    retval = opm_navi_control(0, (unsigned char*)&navi_status, sizeof(int), NULL, &event_size);

    opm_set_work_mode(POWEROFF_MODE);
    return FINGERPRINT_RES_SUCCESS;
}
#if 0
static int do_navigation()
{
	int retval;
	int event, event_size;
	unsigned int status;
	ex_log(LOG_DEBUG, "do_navigation enter");

	do {
		if (check_cancelable()) goto exit;
		retval = opm_set_work_mode(NAVI_DETECT_MODE);
		if (check_cancelable()) goto exit;
		if (FINGERPRINT_RES_SUCCESS != retval) {
			ex_log(LOG_ERROR, "set detect mode failed, retval = %d",
			       retval);
			break;
		}

		ex_log(LOG_DEBUG, "waiting for interrupt");
		if (!wait_trigger(0, TIMEOUT_FOR_ONE_TRY, TIMEOUT_WAIT_FOREVER)) goto exit;

		ex_log(LOG_DEBUG, "finger touch");

		event = 0;
		event_size = sizeof(event);
		if (check_cancelable()) goto exit;
		retval =
		    opm_get_navi_event((unsigned char *)&event, &event_size);
		if (check_cancelable()) goto exit;

		ex_log(LOG_DEBUG, "opm_get_navi_event retval = %d, event = %d", retval, event);
		notify(EVENT_NAVIGATION, event, 0, NULL, 0);

		while (1) {
			if (check_cancelable()) goto exit;
			opm_check_finger_lost(30, &status);
			if (check_cancelable()) goto exit;

			if (status == FP_LIB_FINGER_LOST ||
			    !wait_trigger(3, 30, TIMEOUT_WAIT_FOREVER)) {
				ex_log(LOG_DEBUG, "finger leave");
				break;
			}
		}
	} while (1);

exit:
	ex_log(LOG_DEBUG, "navigation loop end!!!!!! retval = %d", retval);

	return FINGERPRINT_RES_SUCCESS;
}
#endif
#endif  // #ifdef __ENABLE_NAVIGATION__

static int do_navigation_setting() {
#ifdef __ENABLE_NAVIGATION__
    if (navi_is_enable()) {
        do_navigation_event_in_flow();
    } else {
        power_off();
    }
#else
    power_off();
#endif
    return FINGERPRINT_RES_SUCCESS;
}

int cpt_initialize(unsigned char* in_data, unsigned int in_data_len) {
    int retval;
#ifdef __TRUSTONIC__
    int retval_cali;
#endif
    g_hardware_ready = FALSE;

#ifdef HOST_TOUCH_CONTROL
    host_touch_set_enable(TRUE);
#endif

#ifdef EGIS_DBG
    ex_log(LOG_INFO, "EGIS_DBG %s", __func__);
#else
    ex_log(LOG_INFO, "EGIS_RELEASE %s", __func__);
#endif

    if (g_hdev <= 0) {
        retval = fp_device_open(&g_hdev);
        if (FINGERPRINT_RES_SUCCESS != retval) {
            ex_log(LOG_ERROR, "cpt_initialize fp_device_open failed");
            retval = FINGERPRINT_RES_HW_UNAVALABLE;
            goto EXIT;
        }
    }

    retval = fp_device_power_control(g_hdev, TRUE);
    if (FINGERPRINT_RES_SUCCESS != retval) {
        ex_log(LOG_ERROR, "cpt_initialize fp_device_power_control failed");
        retval = FINGERPRINT_RES_HW_UNAVALABLE;
        goto EXIT;
    }

    retval = fp_device_reset(g_hdev);
    if (FINGERPRINT_RES_SUCCESS != retval) {
        ex_log(LOG_ERROR, "cpt_initialize fp_device_reset failed");
        retval = FINGERPRINT_RES_HW_UNAVALABLE;
        goto EXIT;
    }

    retval = fp_device_clock_enable(g_hdev, TRUE);
    if (FINGERPRINT_RES_SUCCESS != retval) {
        ex_log(LOG_ERROR, "cpt_initialize fp_device_clock_enable failed");
        retval = FINGERPRINT_RES_HW_UNAVALABLE;
        goto EXIT;
    }

#ifdef __TRUSTONIC__
    retval_cali = opt_turstoinc_data_transfer(DATA_TRANSFER_TA_INIT, NULL, 0);
#endif

    retval = opm_initialize_sdk(in_data, in_data_len);
    if (FINGERPRINT_RES_SUCCESS != retval) {
        ex_log(LOG_ERROR, "cpt_initialize opm_initialize_sdk return = %d", retval);
        return FINGERPRINT_RES_HW_UNAVALABLE;
    }

    retval = create_ini_config(TRUE, NULL, 0);
    if (FINGERPRINT_RES_SUCCESS != retval) {
        ex_log(LOG_ERROR, "cpt_initialize create_ini_config return = %d", retval);

        return FINGERPRINT_RES_HW_UNAVALABLE;
    }

    retval = opm_initialize_sensor();
    if (FINGERPRINT_RES_SUCCESS != retval) {
        ex_log(LOG_ERROR, "cpt_initialize opm_initialize_sensor return = %d", retval);
        return FINGERPRINT_RES_HW_UNAVALABLE;
    }
#ifdef __TRUSTONIC__
    else if (retval_cali == FINGERPRINT_RES_OPEN_FILE_FAILED) {
        opt_turstoinc_data_transfer(DATA_TRANSFER_TA_INIT_FINISH, NULL, 0);
    }
#endif

    retval = opm_initialize_algo();
    if (FINGERPRINT_RES_SUCCESS != retval) {
        ex_log(LOG_ERROR, "cpt_initialize opm_initialize_algo return = %d", retval);
        return FINGERPRINT_RES_HW_UNAVALABLE;
    }

    retval = opm_calibration(0, PID_COMMAND, NULL, 0);
    if (EX_RES_SUCCESS == retval || EX_RES_NOT_NEED_RECALIBRATION == retval ||
        EX_RES_USE_OLD_CALIBRATION_DATA == retval) {
        g_hardware_ready = TRUE;
        retval = FINGERPRINT_RES_SUCCESS;
    } else {
        ex_log(LOG_ERROR, "cpt_initialize opm_calibration return = %d\n", retval);
        retval = FINGERPRINT_RES_HW_UNAVALABLE;
        goto EXIT;
    }

    thread_manager_init();
    thread_manager_set_cancel_func((rbsCancelFunc)captain_cancel);
    thread_manager_set_idle_task((do_operation_callback)do_navigation_setting);
EXIT:
    return retval;
}

int cpt_uninitialize() {
    int retval;
    cpt_cancel();

    g_hardware_ready = FALSE;

    destroy_ini_config();

    retval = opm_uninitialize_sensor();
    if (FINGERPRINT_RES_SUCCESS != retval) {
        ex_log(LOG_ERROR, "cpt_uninitialize opm_uninitialize_sensor return = %d", retval);
    }
    retval = opm_uninitialize_algo();
    if (FINGERPRINT_RES_SUCCESS != retval) {
        ex_log(LOG_ERROR, "cpt_uninitialize opm_uninitialize_algo return = %d", retval);
    }
    retval = opm_uninitialize_sdk();
    if (FINGERPRINT_RES_SUCCESS != retval) {
        ex_log(LOG_ERROR, "cpt_uninitialize opm_uninitialize_sdk return = %d", retval);
    }

    if (0 != g_hdev) {
        fp_device_clock_enable(g_hdev, FALSE);
        fp_device_power_control(g_hdev, FALSE);
        fp_device_close(g_hdev);
        g_hdev = 0;
    }

    thread_manager_uninitialize();

    return 0;
}

int cpt_cancel() {
    ex_log(LOG_DEBUG, "cpt_cancel enter");
    enroll_cancel_recovery_value();
    thread_manager_cancel_task();
    ex_log(LOG_DEBUG, "cpt_cancel end");

    return FINGERPRINT_RES_SUCCESS;
}

void enroll_cancel_recovery_value(void) {
    ex_log(LOG_DEBUG, "enroll_cancel_recovery_value enter");
    g_has_enroll_count = 0;
    g_enroll_percentage_tmp = 0;
    g_temp_remaining = 0;
    ex_log(LOG_DEBUG, "enroll_cancel_recovery_value end");
}

void captain_cancel(BOOL cancel_flag) {
    ex_log(LOG_DEBUG, "captain_cancel enter [%d]", cancel_flag);

    g_need_cancel = cancel_flag;
}

int cpt_set_active_group(unsigned int user_id, const char* data_path) {
    int retval, result_sync_cache;

    ex_log(LOG_DEBUG, "cpt_set_active_group user_id = %u, data_path = %s", user_id, data_path);

    if (FINGERPRINT_RES_SUCCESS != thread_try_lock_operation()) {
        ex_log(LOG_ERROR, "cpt_set_active_group another operation is doing ,return not idle");
        return FINGERPRINT_RES_NOT_IDLE;
    }

    opt_send_data(TYPE_SEND_USER_INFO, (unsigned char*)&user_id, sizeof(user_id));

    retval = opm_set_active_group(user_id, data_path);
    ex_log(LOG_DEBUG, "opm_set_active_group return = %d", retval);
    if (FINGERPRINT_RES_SUCCESS == retval) {
        g_cache_info.user_id = -1;

        strncpy(g_user_path, data_path, MAX_PATH_LEN);
        opt_send_data(TYPE_SEND_TEMPLATE, NULL, 0);

        result_sync_cache = sync_user_cache(user_id);
        ex_log(LOG_DEBUG, "sync_user_cache return = %d", result_sync_cache);
    }
    thread_unlock_operation();
    return FINGERPRINT_RES_SUCCESS == retval ? FINGERPRINT_RES_SUCCESS : FINGERPRINT_RES_FAILED;
}

int cpt_set_data_path(unsigned int data_type, const char* data_path, unsigned int path_len) {
    int retval;

    ex_log(LOG_DEBUG, "cpt_set_data_path data_type = %u, data_path = %s, path_len = %u", data_type,
           data_path, path_len);

    if (FINGERPRINT_RES_SUCCESS != thread_try_lock_operation()) {
        ex_log(LOG_ERROR, "cpt_set_data_path another operation is doing ,return not idle");
        return FINGERPRINT_RES_NOT_IDLE;
    }

    if (1 == data_type) {
        ex_log(LOG_DEBUG, "cpt_set_data_path set_log_path");
    }

    retval = opm_set_data_path(data_type, data_path, path_len);
    if (FINGERPRINT_RES_SUCCESS != retval) {
        ex_log(LOG_ERROR, "opm_set_data_path return = %d", retval);
    }
    thread_unlock_operation();

    return FINGERPRINT_RES_SUCCESS == retval ? FINGERPRINT_RES_SUCCESS : FINGERPRINT_RES_FAILED;
}

int cpt_chk_secure_id(unsigned int user_id, unsigned long long secure_id) {
    int retval = 0;
    retval = opm_chk_secure_id(user_id, secure_id);
    if (retval != FINGERPRINT_RES_SUCCESS) {
        ex_log(LOG_ERROR, "check secure id failed!");
        return retval;
    }
#ifdef __TRUSTONIC__
    retval = opt_turstoinc_data_transfer(DATA_TRSNSFER_CHK_SID, NULL, 0);
#endif
    ex_log(LOG_DEBUG, "receive user info return %d", retval);
    return retval;
}

int cpt_pre_enroll(fingerprint_enroll_info_t enroll_info) {
    BOOL check_res;
    int retval;

    if (FINGERPRINT_RES_SUCCESS != thread_try_lock_operation()) {
        ex_log(LOG_ERROR, "cpt_pre_enroll another operation is doing ,return not idle");
        return FINGERPRINT_RES_NOT_IDLE;
    }

    retval = sync_user_cache(enroll_info.fingerprint_info.user_id);
    if (retval != FINGERPRINT_RES_SUCCESS) {
        goto EXIT;
    }

    check_res = check_fingerprint_id_available(enroll_info.fingerprint_info.fingerprint_id);
    if (TRUE != check_res) {
        ex_log(LOG_ERROR, "fingerprint_id is not available");
        retval = FINGERPRINT_RES_DUPLICATE_ID;
        goto EXIT;
    }

    check_res = check_enrollment_space_available();
    if (TRUE != check_res) {
        ex_log(LOG_ERROR, "there's no space for enrollment");
        retval = FINGERPRINT_RES_NO_SPACE;
    }

    g_enroll_info = enroll_info;

EXIT:
    thread_unlock_operation();

    return retval;
}

int cpt_enroll() {
    if (TRUE != g_hardware_ready) {
        return FINGERPRINT_RES_HW_UNAVALABLE;
    }

    thread_manager_run_task((do_operation_callback)do_enroll, TASK_PROCESS);

    return FINGERPRINT_RES_SUCCESS;
}

int cpt_post_enroll() {
    if (FINGERPRINT_RES_SUCCESS != thread_try_lock_operation()) {
        ex_log(LOG_ERROR, "cpt_post_enroll another operation is doing ,return not idle");
        return FINGERPRINT_RES_NOT_IDLE;
    }
    thread_unlock_operation();

    return FINGERPRINT_RES_SUCCESS;
}

int cpt_chk_auth_token(unsigned char* token, unsigned int len) {
    return opm_chk_auth_token(token, len);
}

int cpt_get_authenticator_id(unsigned long long* id) {
    int retval = opm_get_authenticator_id(id);
    if (retval != FINGERPRINT_RES_SUCCESS || *id == 0) {
        ex_log(LOG_DEBUG, "opm_get_authenticator_id %d", retval);
        *id = g_cache_info.authenticator_id;
    }
    return FINGERPRINT_RES_SUCCESS;
}

int cpt_authenticate(fingerprint_verify_info_t verify_info) {
    if (g_cache_info.fingerprint_ids_count <= 0) {
        return FINGERPRINT_RES_SUCCESS;
    }

    if (TRUE != g_hardware_ready) {
        return FINGERPRINT_RES_HW_UNAVALABLE;
    }

    g_verify_info = verify_info;
    memcpy(g_verify_info.fingerprints.fingerprint_ids, verify_info.fingerprints.fingerprint_ids,
           sizeof(int) * verify_info.fingerprints.fingerprint_ids_count);

    thread_manager_run_task((do_operation_callback)do_verify, TASK_PROCESS);

    return FINGERPRINT_RES_SUCCESS;
}

int cpt_remove_fingerprint(fingerprint_remove_info_t remove_info) {
    int retval, result_sync_cache;

    if (FINGERPRINT_RES_SUCCESS != thread_try_lock_operation()) {
        ex_log(LOG_ERROR, "cpt_remove_fingerprint another operation is doing ,return not idle");
        return FINGERPRINT_RES_NOT_IDLE;
    }

    retval = opm_remove_fingerprint(remove_info);

    if ((retval == EX_RES_SUCCESS) || (retval == FP_LIB_ERROR_INVALID_FINGERID))
        retval = FINGERPRINT_RES_SUCCESS;
    else
        retval = FINGERPRINT_RES_FAILED;

    /*
    ** reset cache user_id to default
    ** then sync the fingerprints info to the local cache
    */
    g_cache_info.user_id = -1;

    result_sync_cache = sync_user_cache(remove_info.fingerprint_info.user_id);
    if (FINGERPRINT_RES_SUCCESS != result_sync_cache) {
        ex_log(LOG_ERROR, "cpt_remove_fingerprint sync_user_cache return = %d", result_sync_cache);
    }

#ifdef __TRUSTONIC__
    unsigned int temp[2];

    temp[0] = remove_info.fingerprint_info.user_id;
    temp[1] = remove_info.fingerprint_info.fingerprint_id;
    opt_turstoinc_data_transfer(DATA_TRSNSFER_REMOVE_FINGER_FINISH, (unsigned char*)temp,
                                sizeof(temp));
#endif

    thread_unlock_operation();

    return retval;
}

int cpt_get_fingerprint_ids(unsigned int user_id, fingerprint_ids_t* fps) {
    if (FINGERPRINT_RES_SUCCESS != thread_try_lock_operation()) {
        ex_log(LOG_ERROR, "cpt_get_fingerprint_ids another operation is doing ,return not idle");
        return FINGERPRINT_RES_NOT_IDLE;
    }

    /* checks if local cache is updated
    ** if it isnt updated, make it synchronized*/
    int retval = sync_user_cache(user_id);
    if (retval != FINGERPRINT_RES_SUCCESS) {
        ex_log(LOG_ERROR, "cpt_get_fingerprint_ids sync_user_cache return = %d", retval);
        goto EXIT;
    }

    fps->fingerprint_ids_count = g_cache_info.fingerprint_ids_count;
    memcpy(fps->fingerprint_ids, g_cache_info.fingerprint_ids,
           g_cache_info.fingerprint_ids_count * sizeof(unsigned int));
EXIT:
    thread_unlock_operation();

    return FINGERPRINT_RES_SUCCESS == retval ? FINGERPRINT_RES_SUCCESS : FINGERPRINT_RES_FAILED;
}
#if 0
// ToDo:
void cpt_disable_navigation(BOOL onoff)
{
	if (onoff) thread_manager_set_idle_task(do_navigation_event_in_flow);
	else       thread_manager_set_idle_task(power_off);
}
#endif

int cpt_navigation() {
    ex_log(LOG_ERROR, "cpt_navigation not support this interface");

    return FINGERPRINT_RES_SUCCESS;
}

void cpt_set_event_callback(event_callbck_t on_event_callback) {
    g_event_callback = on_event_callback;
    ex_log(LOG_DEBUG, "cpt_set_event_callback = %p, size = %d", g_event_callback,
           sizeof(event_callbck_t));
    opm_set_data(TYPE_SEND_CALLBACK_FUNCTION, (unsigned char*)&g_event_callback,
                 sizeof(event_callbck_t));
}

static void get_ca_version(unsigned char* version, int* len) {
    if (version == NULL || len == NULL) return;

    int version_len = 0;
    version_len = sizeof("UNKNOWN");
    if (version_len <= *len) {
        *len = version_len;
        memcpy(version, "UNKNOWN", version_len);
    }

    ex_log(LOG_DEBUG, "version_len = %d , version = %s", *len, version);
}

int cpt_extra_api(int type, unsigned char* in_data, int in_data_size, unsigned char* out_buffer,
                  int* out_buffer_size) {
    int retval = FINGERPRINT_RES_SUCCESS, cid = 0;
    unsigned char* new_in_data;

    if (in_data == NULL || in_data_size < 1) {
        // Bypass to  opm_extra_command();
        ex_log(LOG_DEBUG, "%s [%d] in_data_size %d", __func__, type, in_data_size);
        retval = thread_try_lock_operation();
        if (retval != 0) {
            ex_log(LOG_ERROR, "Failed try lock. retval=%d", retval);
            return FINGERPRINT_RES_NOT_IDLE;
        }
        retval = opm_extra_command(type, in_data, in_data_size, out_buffer, out_buffer_size);
        thread_unlock_operation();
        ex_log(retval == 0 ? LOG_DEBUG : LOG_ERROR, "cpt_extra_api end, cid = %d, retval=%d", cid,
               retval);
        return retval;
    }

    uint8_t* extra_data = NULL;
    int extra_data_size = 0;
    if (in_data_size < 4) {
        ex_log(LOG_ERROR, "%s, in_data_size %d < 4", __func__, in_data_size);
        cid = in_data[0];
    } else {
        cid = *(int*)in_data;
        if (in_data_size > 4) {
            extra_data = in_data + 4;
            extra_data_size = in_data_size - 4;
        }
    }
    ex_log(LOG_DEBUG, "cpt_extra_api [%d], cid = %d, in_data_size = %d", type, cid, in_data_size);
    ex_log(LOG_DEBUG, "debug_check cpt_extra_api in_data %p in_data_size %d",
                       in_data, in_data_size);

    BOOL needLockThread;
    switch (type) {
        case PID_HOST_TOUCH:
            needLockThread = FALSE;
            break;
        default:
            needLockThread = TRUE;
            break;
    }

    if (needLockThread && FINGERPRINT_RES_SUCCESS != thread_try_lock_operation()) {
        ex_log(LOG_ERROR, "cpt_extra_api another operation is doing ,return not idle");
        return FINGERPRINT_RES_NOT_IDLE;
    }

    switch (type) {
#if defined(__ET7XX__) && !defined(__ET0XX__)
        case PID_7XX_INLINETOOL: {
            int* param = (int*)in_data;
            retval =
                do_7XX_sensortest(cid, param[1], param[2], param[3], out_buffer, out_buffer_size);
            ex_log(LOG_DEBUG, "cid=%d, retval=%d", cid, retval);
            goto EXIT;
        }
#endif
        case PID_INLINETOOL:
        case PID_FAETOOL: {
            new_in_data = in_data;
            if( (cid == SENSORTEST_GET_IMAGE)
                || (cid == SENSORTEST_760_GET_FFC_IMAGE)
                    || (cid == SENSORTEST_760_GET_FFC_IMAGE_NO_UPDATE_BDS)) {
                in_data_size += 8;
                new_in_data = plat_alloc(in_data_size);
                if (in_data != NULL && in_data_size > 0) {
                    memcpy(new_in_data, in_data, in_data_size - 8);
                }
                int* new_in_data_start = (int*)(new_in_data + in_data_size - 8);
                new_in_data_start[0] = g_touch_info[0].x_ratio_x100;
                new_in_data_start[1] = g_touch_info[0].y_ratio_x100;
            }
            if ( cid == SENSORTEST_760_GET_FFC_IMAGE ) {
                int *p = (int*) new_in_data ;
                *p = FP_CAPTURE_IMG_RBS_760_FFC_IMAGE ;
                egislog_d("get ffc image (et760 only) data[0]=%d", *p) ;
            }
            if ( cid == SENSORTEST_760_GET_FFC_IMAGE_NO_UPDATE_BDS ) {
                int *p = (int*) new_in_data ;
                *p = FP_CAPTURE_IMG_RBS_760_FFC_IMAGE_NO_UPDATE_BDS ;
                egislog_d("get ffc image (et760 only but no bds) data[0]=%d", *p) ;
            }
            retval = do_sensortest(cid, new_in_data, in_data_size, out_buffer, out_buffer_size);
            goto EXIT;
        }

        case PID_COMMAND: {
            if (CMD_UPDATE_CONFIG == cid) {
                destroy_ini_config();
                if ((uint32_t)in_data_size > sizeof(int)) {
                    retval =
                        create_ini_config(FALSE, in_data + sizeof(int), in_data_size - sizeof(int));
                } else {
                    retval = create_ini_config(FALSE, NULL, 0);
                }

                goto EXIT;
            } else if (CMD_GET_CONFIG == cid) {
                if (out_buffer != NULL && out_buffer_size != NULL) {
                    retval = opt_receive_data(TYPE_RECEIVE_INI_CONFIG, NULL, 0, out_buffer,
                                              out_buffer_size);
                }
                goto EXIT;
            } else if (CMD_UPDATE_DB_CONFIG == cid) {
                if ((uint32_t)in_data_size > sizeof(int))
                    retval = opt_send_data(TYPE_SEND_DB_INI_CONFIG, in_data + sizeof(int),
                                           in_data_size - sizeof(int));
                else {
                    retval = FINGERPRINT_RES_FAILED;
                    ex_log(LOG_ERROR, "Failed to update db config ini");
                }
                goto EXIT;
            }

            if (CMD_SET_NAVI_CONFIG == cid) {
                g_navi_config.navi_mode = ((int*)in_data)[0];
                g_navi_config.change_x_y = ((int*)in_data)[1];
                g_navi_config.change_up_down = ((int*)in_data)[2];
                g_navi_config.change_left_right = ((int*)in_data)[3];
                goto EXIT;
            }

            if (CMD_REMOVE_INI_FILE == cid) {
                retval = opt_send_data(TYPE_REMOVE_INI_FILE, NULL, 0);
                goto EXIT;
            }
        }

        case PID_DEMOTOOL: {
            switch (cid) {
                case CMD_VERSION_CA:
                    get_ca_version(out_buffer, out_buffer_size);
                    break;
                case CMD_VERSION_TA:
                    retval =
                        opm_get_data(TYPE_RECEIVE_TA_VERSION, NULL, 0, out_buffer, out_buffer_size);
                    break;
                case CMD_VERSION_ALGO:
                    retval = opm_get_data(TYPE_RECEIVE_ALGO_VERSION, NULL, 0, out_buffer,
                                          out_buffer_size);
                    break;
                case CMD_VERSION_IP:
                    retval =
                        opm_get_data(TYPE_RECEIVE_IP_VERSION, NULL, 0, out_buffer, out_buffer_size);
                    break;
                case EXTRA_DT_SET_INI_CONFIG_PATH:
                    retval = opt_send_data(TYPE_SEND_INI_CONFIG_PATH, extra_data, extra_data_size);
                    break;
                default:
                    break;
            }
            goto EXIT;
        }
        case PID_BKG_IMG: {
            switch (cid) {
                case CMD_BKG_IMG_RUN_CALI_PROC:
                    ex_log(LOG_DEBUG, "CMD_BKG_IMG_RUN_CALI_PROC");
                    retval = flow_inline_legacy_cmd(FP_INLINE_SENSOR_CALIBRATE, FPS_CALI_ET7XX_BKG,
                                                    0, 0, NULL, NULL);
                    if (retval == FP_LIB_OK) {
                        retval = flow_inline_legacy_cmd(FP_INLINE_SENSOR_CALIBRATE,
                                                        FPS_SAVE_ET7XX_CALI_DATA, 0, 0, NULL, NULL);
                    }
                    break;
                case CMD_BKG_IMG_SAVE_CALI_DATA:
                    ex_log(LOG_DEBUG, "CMD_BKG_IMG_SAVE_CALI_DATA file path = %s",
                           in_data + sizeof(int));
                    retval = opm_set_data(TYPE_IC_SAVE_CALIBRATION_DATA, in_data + sizeof(int),
                                          in_data_size - sizeof(int));
                    break;
                case CMD_BKG_IMG_LOAD_CALI_DATA:
                    ex_log(LOG_DEBUG, "CMD_BKG_IMG_LOAD_CALI_DATA file path = %s",
                           in_data + sizeof(int));
                    retval = opm_set_data(TYPE_IC_LOAD_CALIBRATION_DATA, in_data + sizeof(int),
                                          in_data_size - sizeof(int));
                    break;
                case CMD_BKG_IMG_GET_CALI_IMAGE: {
                    ex_log(LOG_DEBUG, "CMD_BKG_IMG_GET_CALI_IMAGE");
                    int width, height, rawbpp, binbpp;
                    //int ret = cpt_get_image_size_info(&width, &height, &rawbpp, &binbpp);
                    //if(ret != FINGERPRINT_RES_SUCCESS){
                        // todo: ref ET760 + A3 Define header file
                        width = 600;
                        height = 800;
                        rawbpp = 16;
                        binbpp = 8;
                    //}
/*                    int cali_image_size =
                        sizeof(liver_image_out_header_t) + width * height * (rawbpp + binbpp) / 8;*/
                    int cali_image_size =
                            sizeof(liver_image_out_header_t) + width * height * (rawbpp + binbpp) / 8;
                    unsigned char* cali_image = plat_alloc(cali_image_size);
                    if (cali_image != NULL) {
                        retval = flow_inline_legacy_cmd(FP_INLINE_SENSOR_GET_CALI_IMAGE, 0, 0, 0,
                                                        cali_image, &cali_image_size);
                        notify(EVENT_RETURN_LIVE_IMAGE_OUT, 0, 0, cali_image, cali_image_size);
                        PLAT_FREE(cali_image);
                    } else {
                        ex_log(LOG_ERROR, "failed to allocate %d", cali_image_size);
                        retval = FINGERPRINT_RES_ALLOC_FAILED;
                    }
                    break;
                }
                case CMD_BKG_IMG_RETURN_IMAGE: {
                    ex_log(LOG_DEBUG, "CMD_BKG_IMG_RETURN_IMAGE");
                    retval = flow_inline_legacy_cmd(FP_INLINE_SENSOR_GET_IMAGE, g_touch_info[0].x_ratio_x100, g_touch_info[0].y_ratio_x100, 0, out_buffer,
                                                    out_buffer_size);
                    break;
                }
                case CMD_BKG_IMG_REMOVE_CALI_DATA:
                    ex_log(LOG_DEBUG, "CMD_BKG_IMG_REMOVE_CALI_DATA");
                    retval = flow_inline_legacy_cmd(FP_INLINE_SENSOR_CALIBRATE,
                                                    FPS_REMOVE_ET7XX_CALI_DATA, 0, 0, NULL, NULL);
                    break;
                case CMD_BKG_IMG_REMOVE_BDS:
                    ex_log(LOG_DEBUG, "CMD_BKG_IMG_REMOVE_BDS");
                    retval = opm_remove_bds();
                    break;
                case CMD_BKG_BBKG_FFC:
                    ex_log(LOG_DEBUG, "==>CMD_BKG_BBKG_FFC");
                    ex_log(LOG_DEBUG, "debug_check CMD_BKG_BBKG_FFC");
                    retval = flow_inline_legacy_cmd(FP_INLINE_SENSOR_CALIBRATE,
                                                    FPS_SET_CALI_TYPE_BK, 0, 0, NULL, NULL);
                    break;
                case CMD_BKG_WBKG_FFC:
                    ex_log(LOG_DEBUG, "==>CMD_BKG_WBKG_FFC");
                    ex_log(LOG_DEBUG, "debug_check CMD_BKG_WBKG_FFC");
                    retval = flow_inline_legacy_cmd(FP_INLINE_SENSOR_CALIBRATE,
                                                    FPS_SET_CALI_TYPE_WK, 0, 0, NULL, NULL);
                    break;
                case CMD_BKG_IMG_SAVE_BDS:
                    ex_log(LOG_DEBUG, "CMD_BKG_IMG_SAVE_BDS");
                    ex_log(LOG_DEBUG, "debug_check CMD_BKG_IMG_SAVE_BDS");
                    retval = opm_save_bds();
                    break;
                default:
                    ex_log(LOG_ERROR, "unsupported cid : %d", cid);
                    break;
            }
            goto EXIT;
        }
        case PID_HOST_TOUCH: {
            switch (cid) {
                case CMD_HOST_TOUCH_ENABLE:
                    host_touch_set_enable(TRUE);
                    break;
                case CMD_HOST_TOUCH_PARTIAL_TOUCHING:
					egislog_d("darkresearch large_area_multi_finger CMD_HOST_TOUCH_PARTIAL_TOUCHING");
                    host_touch_set_finger_on(TRUE);
                    host_touch_set_low_coverage(TRUE);
                    break;
                case CMD_HOST_TOUCH_SET_TOUCHING: {
					egislog_d("darkresearch large_area_multi_finger CMD_HOST_TOUCH_SET_TOUCHING");
#if defined(__ET7XX__) && !defined(__ET0XX__)
                    int sys_temp;
                    get_sys_temp(&sys_temp);
                    host_device_info_t info;
                    info.temperature_x10 = sys_temp;
                    set_host_device_info(info);
                    if (out_buffer != NULL && out_buffer_size != NULL && *out_buffer_size >= 4) {
                        memcpy(out_buffer, &sys_temp, sizeof(int));
                    }
#endif
                    host_touch_set_finger_on(TRUE);
                    break;
                }
                case CMD_HOST_TOUCH_SET_TOUCH_POS: {
                    if(extra_data_size >= (int)sizeof(int)){
                        int* int_extra_data = (int*) extra_data;
                        int touch_point_count = int_extra_data[0];
                        if(touch_point_count < 0 || touch_point_count > MAX_TOUCH_POINT_COUNT) {
                            egislog_e("CMD_HOST_TOUCH_SET_TOUCH_POS touch count %d > MAX_TOUCH_POINT_COUNT", touch_point_count);
                            retval = FINGERPRINT_RES_FAILED;
                            break;
                        }
                        g_touch_count = int_extra_data[0];
                        memset(g_touch_info, 0, MAX_TOUCH_POINT_COUNT * sizeof(touch_info_t));
                        egislog_d("PAPA CMD_HOST_TOUCH_SET_TOUCH_POS touch point count %d", g_touch_count);
                        egislog_d("darkresearch large_area_multi_finger  CMD_HOST_TOUCH_SET_TOUCH_POS touch point count %d", g_touch_count);
                        for(int i=0;i<g_touch_count;i++) {
                            g_touch_info[i].x_ratio_x100 = int_extra_data[2*i+1];
                            g_touch_info[i].y_ratio_x100 = int_extra_data[2*i+2];
                            egislog_d("PAPA CMD_HOST_TOUCH_SET_TOUCH_POS g_touch_count[%d] = %d, %d"
                                    , i, g_touch_info[i].x_ratio_x100, g_touch_info[i].y_ratio_x100);
                            egislog_d("darkresearch large_area_multi_finger CMD_HOST_TOUCH_SET_TOUCH_POS g_touch_count[%d] = %d, %d"
                                    , i, g_touch_info[i].x_ratio_x100, g_touch_info[i].y_ratio_x100);
                        }
                    }
                    break;
                }
                case CMD_HOST_TOUCH_RESET_TOUCHING:
					egislog_d("darkresearch large_area_multi_finger CMD_HOST_TOUCH_RESET_TOUCHING");
                    host_touch_set_finger_off(TRUE);
                    break;
                case CMD_HOST_TOUCH_SEND_TEMPERATURE: {
                    host_device_info_t info;
                    info.temperature_x10 = *((int*)in_data + 1);
                    ex_log(LOG_DEBUG, "ignore app temperature: %d", info.temperature_x10);
                    // set_host_device_info(info);
                    break;
                }
                case CMD_DEMOTOOL_SET_SCREEN_BRIGHTNESS: {
#if defined(__ET7XX__) && !defined(__ET0XX__)
                    int value = *((int*)in_data + 1);
                    ex_log(LOG_DEBUG, "set brightness: %d", value);
                    retval = set_brightness(value);
#endif
                    goto EXIT;
                }
                default:
                    ex_log(LOG_ERROR, "unsupported cid : %d", cid);
                    break;
            }
            retval = FINGERPRINT_RES_SUCCESS;
            goto EXIT;
        }
        case PID_SET_ENROLL_TIMEOUT: {
            g_enroll_timeout = cid;
            ex_log(LOG_DEBUG, "set enroll timeout = %d ", g_enroll_timeout);
            goto EXIT;
        }
        case PID_EVTOOL: {
            const int PARAM_BUF_ENROLLED_PATH = 1000;
            const int PARAM_BUF_VERIFIED_PATH = 1001;
            const int PARAM_INT_DB_TOTAL_FINGER = 730;
            const int PARAM_BUF_DB_TOTAL_FINGERPRINT = 731;
            switch (cid) {
                case CMD_GET_ENROLL_FILE_NAME: {
                    int param = PARAM_BUF_ENROLLED_PATH;
                    retval = opm_get_data(TYPE_RECEIVE_SENSOR_BUF, (uint8_t*)&param, sizeof(param),
                                          out_buffer, out_buffer_size);
                } break;
                case CMD_GET_VERIFY_FILE_NAME: {
                    int param = PARAM_BUF_VERIFIED_PATH;
                    retval = opm_get_data(TYPE_RECEIVE_SENSOR_BUF, (uint8_t*)&param, sizeof(param),
                                          out_buffer, out_buffer_size);
                } break;
                case CMD_GET_MATCH_SCORE: {
                    retval = opm_get_data(TYPE_EVTOOL_RECEIVE_MATCH_SCORE, NULL, 0, out_buffer,
                                          out_buffer_size);
                } break;
                case CMD_INT_DB_TOTAL_FINGER: {
                    int param = PARAM_INT_DB_TOTAL_FINGER;
                    retval = opm_get_data(TYPE_RECEIVE_SENSOR_INT_PARAM, (uint8_t*)&param,
                                          sizeof(param), out_buffer, out_buffer_size);
                } break;
                case CMD_BUF_DB_TOTAL_FINGERPRINT: {
                    int param = PARAM_BUF_DB_TOTAL_FINGERPRINT;
                    retval = opm_get_data(TYPE_RECEIVE_SENSOR_BUF, (uint8_t*)&param, sizeof(param),
                                          out_buffer, out_buffer_size);
                } break;
                default:
                    ex_log(LOG_ERROR, "unsupported pid : %d, cid : %d", type, cid);
                    break;
            }
            goto EXIT;
        }
    }

    ex_log(LOG_VERBOSE, "opm_extra_command");
    retval = opm_extra_command(type, in_data, in_data_size, out_buffer, out_buffer_size);

EXIT:
    if (needLockThread) {
        thread_unlock_operation();
    }
    ex_log(LOG_DEBUG, "cpt_extra_api end, cid = %d, retval=%d", cid, retval);

    return retval;
}

static int touch_enroll_remaining(int percentage) {
    ex_log(LOG_DEBUG, "%s enter! percentage = %d", __func__, percentage);

    if (g_enroll_percentage_tmp != percentage) {
        g_has_enroll_count++;
        g_enroll_percentage_tmp = percentage;
    }

    int remaining;
    if (percentage >= 100) {
        g_enroll_percentage_tmp = 0;
        g_has_enroll_count = 0;
        remaining = 0;
    } else if (percentage > 0) {
        int total = (int)(100 / (float)percentage * g_has_enroll_count);
        remaining = total - g_has_enroll_count;
        ex_log(LOG_DEBUG, "%s has_enroll_count=%d, total=%d, remaining=%d", __func__,
               g_has_enroll_count, total, remaining);
        if (remaining < 1) remaining = 1;
    } else {
        remaining = g_enroll_config.enroll_max_count;
    }

    return remaining;
}

static int swipe_enroll_remaining(int percentage) {
    ex_log(LOG_DEBUG, "%s enter! percentage = %d", __func__, percentage);

    int remaining;
    if (percentage >= 100) {
        remaining = 0;
    } else if (percentage > 0) {
        remaining = 100 - percentage;
        ex_log(LOG_DEBUG, "%s = %d", __func__, remaining);
        if (remaining < 1) remaining = 1;
    } else {
        remaining = 100;
    }

    return remaining;
}

static int enroll_percentage_to_remaining(int percentage) {
    int retval = -1;
    switch (g_enroll_config.enroll_method) {
        case ENROLL_METHOD_TOUCH:
            retval = touch_enroll_remaining(percentage);
            break;
        case ENROLL_METHOD_PAINT:
        case ENROLL_METHOD_SWIPE:
            retval = swipe_enroll_remaining(percentage);
            break;
        default:
            break;
    }
    ex_log(LOG_DEBUG, "%s retval = %d", __func__, retval);

    return retval;
}

int cpt_get_image_size_info(int* width, int* height, int* rawbpp, int* binbpp){
    int info_size = sizeof(int) * 4;
    unsigned char* size_info_buffer = plat_alloc(info_size);
    memset(size_info_buffer, 0, info_size);
    sensor_test_opation(SEBSORTEST_GET_IMAGE_SIZE_INFO, g_hdev, NULL, 0, size_info_buffer, &info_size);
    image_size_info_t* result = (image_size_info_t*)size_info_buffer;
    *width = result->img_width;
    *height = result->img_height;
    *rawbpp = result->raw_bpp;
    *binbpp = result->img_bpp;
    PLAT_FREE(size_info_buffer);
    if(width <= 0 || height <= 0 || rawbpp<= 0 || binbpp <= 0){
        return FINGERPRINT_RES_FAILED;
    }
    return FINGERPRINT_RES_SUCCESS;
}

static int do_sensortest(int cid, unsigned char* in_data, int in_data_size,
                         unsigned char* out_buffer, int* out_buffer_size) {
    ex_log(LOG_DEBUG, "%s enter! cid = %d", __func__, cid);
    int retval = FINGERPRINT_RES_FAILED;

    int buffer_size;

    ex_log(LOG_DEBUG, "%s, out buffer (%p) %d", __func__, out_buffer,
           out_buffer_size ? *out_buffer_size : 0);

    unsigned char* buffer;
    BOOL use_local_buffer;
    if (out_buffer != NULL && out_buffer_size != NULL && *out_buffer_size > 0) {
        buffer = out_buffer;
        buffer_size = *out_buffer_size;
        use_local_buffer = FALSE;
    } else {
        int width, height, rawbpp, binbpp;
        int ret = cpt_get_image_size_info(&width, &height, &rawbpp, &binbpp);
        if(ret != FINGERPRINT_RES_SUCCESS){
            width = 255;
            height = 255;
            rawbpp = 16;
            binbpp = 32;
        }
        int bpp = (rawbpp > binbpp) ? rawbpp : binbpp;
        int max_image_size = width * height * bpp / 8;
        buffer_size = max_image_size * 2;
        buffer = plat_alloc(buffer_size);
        use_local_buffer = TRUE;
    }

    if (buffer != NULL) {
        g_need_cancel = FALSE;

        retval = sensor_test_opation(cid, g_hdev, in_data, in_data_size, buffer, &buffer_size);
        if (SENSORTEST_GET_IMAGE == cid && FINGERPRINT_RES_SUCCESS == retval) {
            if (use_local_buffer) {
                notify(EVENT_RETURN_IMAGE, 0, 0, buffer, buffer_size);
            }
        }

        if (SENSORTEST_GET_NVM_UID == cid && FINGERPRINT_RES_SUCCESS == retval) {
            notify(EVENT_SENSROR_TEST_NVM_UID, buffer_size, 0, buffer, buffer_size);
        }
    }

    if (use_local_buffer) {
        PLAT_FREE(buffer);
    }
    ex_log(LOG_DEBUG, "%s retval = %d", __func__, retval);

    return retval;
}
