#include "update_map.h"
#include "captain.h"
#include "plat_log.h"
#include "response_def.h"

extern enroll_config_t g_enroll_config;
extern event_callbck_t g_event_callback;
#ifdef _INLINE_MERGE_MAP
#include "MergeMapCreator.h"
static void map_callback(unsigned int result, unsigned int percentage, unsigned char* map,
                         unsigned int width, unsigned int height, int sum_x, int sum_y) {
    if (NULL != g_event_callback) {
        switch (result) {
            case ECALLBACK_ENROLL_MAP:
                g_event_callback(EVENT_ENROLL_MAP, sum_x, sum_y, map, width * height);
                ex_log(LOG_DEBUG, "ECALLBACK_ENROLL_MAP sum_y = %d. percentage=%d", sum_y,
                       percentage);
                break;

            case ECALLBACK_ENROLL_TOO_FAST:
                notify(EVENT_ENROLL_TOO_FAST, 0, 0, NULL, 0);
                break;

            default:
                ex_log(LOG_ERROR, "map_callback error, default case");
                break;
        }
    }
}
#endif

void update_map(BOOL finger_on_again, BOOL fast, int percentage, algo_swipe_info_t swipe_info,
                BOOL finish) {
#ifdef _INLINE_MERGE_MAP
    static BOOL started = FALSE;
    struct MergeMapUpdateInfo map_info;
    struct MergeMapConfig config;
    int map_size_data[4] = {0};
    int swipe_direction;
    ex_log(LOG_DEBUG, "update_map enter");
    if (!started) {
        config.MergeMapWidth = MERGE_MAP_WIDTH;
        config.MergeMapHeight = MERGE_MAP_HEIGHT;
        config.BackgroundHeight = MERGE_MAP_HEIGHT;
        ex_log(LOG_DEBUG, "swipe_dir = %d", swipe_info.swipe_dir);
        if (swipe_info.swipe_dir == 1) {
            swipe_direction = SWIPE_DIRECTION_X;
        } else if (swipe_info.swipe_dir == 2) {
            swipe_direction = SWIPE_DIRECTION_Y;
        } else {
            swipe_direction = SWIPE_DIRECTION_AUTO;
        }

        config.EnrollMethod = g_enroll_config.enroll_method;
        config.LoopCountX = g_enroll_config.swipe_count_x;
        config.LoopCountY = g_enroll_config.swipe_count_y;

        if (config.EnrollMethod == ENROLL_METHOD_PAINT) {
            config.BackgroundHeight = BACKGROUND_HEIGHT_PAINT;
        } else if (config.EnrollMethod == ENROLL_METHOD_SWIPE) {
            config.BackgroundHeight = BACKGROUND_HEIGHT_SWIPE;
        }

        config.SwipeDirection = swipe_direction;
        config.MergeMap_HW_Width = swipe_info.mergemap_hw_width;
        config.MergeMap_HW_Height = swipe_info.mergemap_hw_height;

        if ((swipe_direction != SWIPE_DIRECTION_AUTO) ||
            (config.EnrollMethod == ENROLL_METHOD_PAINT)) {
            map_size_data[0] = config.MergeMapWidth;
            map_size_data[1] = config.BackgroundHeight;

            if (config.EnrollMethod == ENROLL_METHOD_SWIPE) {
                map_size_data[2] =
                    swipe_direction == SWIPE_DIRECTION_X ? config.LoopCountX : config.LoopCountY;
                map_size_data[3] = swipe_direction == SWIPE_DIRECTION_X ? config.MergeMap_HW_Width
                                                                        : config.MergeMap_HW_Height;
            } else if (config.EnrollMethod == ENROLL_METHOD_PAINT) {
                map_size_data[2] = config.MergeMapWidth / 3;
                map_size_data[3] = config.MergeMapWidth / 3;
            }

            notify(EVENT_MERGE_MAP_SIZE, 4, sizeof(int), (unsigned char*)map_size_data,
                   sizeof(int) * 4);

            mmap_set_config(&config);
            mmap_start(map_callback);
            finger_on_again = TRUE;
            started = TRUE;
            ex_log(LOG_DEBUG, "update_map start");
        }
    }

    if (finish) {
        mmap_end(percentage == 100 ? 0 : 1);
        started = FALSE;
    }

    if (started) {
        ex_log(LOG_DEBUG, "update_map update mergemap");
        map_info.finger_on_again = finger_on_again;
        map_info.rollback = g_enroll_config.enroll_too_fast_rollback ? fast : FALSE;
        map_info.percentage = percentage;
        map_info.similarity_score = swipe_info.similarity_score;
        map_info.delta_x = swipe_info.dx;
        map_info.delta_y = swipe_info.dy;

        mmap_update(&map_info);
    }

#else
    notify(EVENT_ENROLL_OK, g_enroll_info.fingerprint_info.fingerprint_id, percentage, NULL, 0);
#endif
}
