#ifndef __DEVICE_INT_H__
#define __DEVICE_INT_H__

#include "type_def.h"

#define TIMEOUT_WAIT_FOREVER -1

void host_touch_set_enable(BOOL enable);

void host_touch_set_finger_reset();

void host_touch_set_finger_on(BOOL on);

void host_touch_set_low_coverage(BOOL flag);

void host_touch_set_finger_off(BOOL off);

BOOL host_touch_is_enable();

BOOL host_touch_is_finger_on();
BOOL host_touch_is_finger_on_last();

BOOL host_touch_is_low_coverage();

BOOL host_touch_is_finger_off();
BOOL host_touch_is_finger_off_last();

BOOL wait_trigger(int retry_count, int timeout_for_one_try, int total_timeout);
int get_wait_trigger_status();

// implementation is on captain.c
BOOL check_cancelable();

typedef BOOL (*eventCheckerFunc)();

typedef struct _event_check_fd {
    eventCheckerFunc checkerFunc;
    BOOL revent;
} event_check_fd;

typedef enum {
    REVENT_TIMEOUT = 0,
    REVENT_EVENT_POLLIN,
} poll_revents;

poll_revents event_poll_wait(event_check_fd* check_group, uint32_t fd_count, int timeout);

#endif
