#ifndef __UPDATE_MAP__
#define __UPDATE_MAP__
#include "common_definition.h"
#include "type_def.h"

void update_map(BOOL finger_on_again, BOOL fast, int percentage, algo_swipe_info_t swipe_info,
                BOOL finish);
#endif