#include "device_int.h"
#include "fps_normal.h"
#include "plat_log.h"
#include "plat_time.h"
#include "response_def.h"

#if defined(SIMULATE_NO_SENSOR) || defined(RBS_EVTOOL)
#define POLL_TIME 1
#else
#define POLL_TIME 10
#endif

extern int g_hdev;
static int g_wait_trigger_status;

struct HostTouchEvent {
    BOOL is_enable;
    BOOL is_finger_on;
    uint64_t timestamp_finger_on;
    BOOL is_finger_off;
    uint64_t timestamp_finger_off;
    BOOL is_low_coverage;
};

struct HostTouchEvent g_host_touch = {
#ifdef HOST_TOUCH_CONTROL
    .is_enable = TRUE,
#else
    .is_enable = FALSE,
#endif
    .is_finger_on = FALSE,
    .is_finger_off = FALSE,
    .is_low_coverage = FALSE};
void host_touch_set_enable(BOOL enable) {
    ex_log(LOG_DEBUG, "%s, %d", __func__, enable);
    g_host_touch.is_enable = enable;
    g_host_touch.is_finger_on = FALSE;
    g_host_touch.is_finger_off = FALSE;
    g_host_touch.is_low_coverage = FALSE;
}

void host_touch_set_finger_reset() {
    ex_log(LOG_DEBUG, "%s", __func__);
    g_host_touch.is_finger_on = FALSE;
    g_host_touch.is_finger_off = FALSE;
    g_host_touch.timestamp_finger_on = 0;
    g_host_touch.timestamp_finger_off = 0;
}

void host_touch_set_finger_on(BOOL on) {
    g_host_touch.is_finger_on = on;
    g_host_touch.timestamp_finger_on = plat_get_time();
    ex_log(LOG_DEBUG, "%s, %d", __func__, on);
    // ex_log(LOG_DEBUG, "timestamp finger on %lld", g_host_touch.timestamp_finger_on);
}

void host_touch_set_low_coverage(BOOL flag) {
    ex_log(LOG_DEBUG, "%s, %d", __func__, flag);
    g_host_touch.is_low_coverage = flag;
}

void host_touch_set_finger_off(BOOL off) {
    g_host_touch.is_finger_off = off;
    g_host_touch.timestamp_finger_off = plat_get_time();
    ex_log(LOG_DEBUG, "%s, %d", __func__, off);
    // ex_log(LOG_DEBUG, "timestamp finger off %lld", g_host_touch.timestamp_finger_off);
}

BOOL host_touch_is_enable() {
    return g_host_touch.is_enable;
}

BOOL host_touch_is_finger_on() {
    return g_host_touch.is_finger_on;
}

BOOL host_touch_is_finger_on_last() {
    if (g_host_touch.timestamp_finger_on > g_host_touch.timestamp_finger_off) {
        return TRUE;
    } else {
        return FALSE;
    }
}

BOOL host_touch_is_finger_off_last() {
    if (g_host_touch.timestamp_finger_off > g_host_touch.timestamp_finger_on) {
        return TRUE;
    } else {
        return FALSE;
    }
}

BOOL host_touch_is_low_coverage() {
    return g_host_touch.is_low_coverage;
}

BOOL host_touch_is_finger_off() {
    return g_host_touch.is_finger_off;
}

BOOL wait_trigger(int retry_count, int timeout_for_one_try, int total_timeout) {
    int retval = FINGERPRINT_RES_TIMEOUT;
    BOOL bWait = TRUE;
    int icount = retry_count;
    unsigned long long time_start_waitfinger = plat_get_time();
    ex_log(LOG_DEBUG, "time start = %llu, total_timeout = %d", time_start_waitfinger,
           total_timeout);

    g_wait_trigger_status = retval;  // Reset global variable

    // Init interrupt first
    if (!host_touch_is_enable()) {
#ifdef ENABLE_POLL
        plat_sleep_time(POLL_TIME);
        ex_log(LOG_DEBUG, "poll trigger");
        g_wait_trigger_status = FINGERPRINT_RES_SUCCESS;
        return true;
#endif
        retval = fp_device_interrupt_enable(g_hdev, FLAG_INT_CLOSE);
        retval = fp_device_interrupt_enable(g_hdev, FLAG_INT_INIT);

        if (retval != FINGERPRINT_RES_SUCCESS) {
            g_wait_trigger_status = retval;
            return false;
        }
    }

    while (bWait) {
        // Check enroll timeout
        if (total_timeout >= 0) {
            if ((int)(plat_get_diff_time(time_start_waitfinger) / 1000) > total_timeout) {
                ex_log(LOG_INFO, "!!! enroll timeout !!! , total_timeout = %d", total_timeout);
                retval = FINGERPRINT_RES_SECURE_ENROLL_TIMEOUT;
                break;
            }
        }

        // Check finger on
        if (check_cancelable()) {
            retval = FINGERPRINT_RES_CANCEL;
            break;
        }
        if (host_touch_is_enable()) {
            plat_sleep_time(POLL_TIME);
            retval = host_touch_is_finger_on() ? FINGERPRINT_RES_SUCCESS : FINGERPRINT_RES_TIMEOUT;
        } else {
            retval = fp_device_interrupt_wait(g_hdev, timeout_for_one_try);
        }
        if (check_cancelable()) {
            retval = FINGERPRINT_RES_CANCEL;
            break;
        }

        if (retval == FINGERPRINT_RES_SUCCESS) {
            if (host_touch_is_enable()) {
                if (host_touch_is_finger_off_last()) {
                    host_touch_set_finger_reset();
                    ex_log(LOG_DEBUG, "poll touch event skipped.");
                    retval = FINGERPRINT_RES_TIMEOUT;  // skip result, set to default value
                } else {
                    host_touch_set_finger_reset();
                    ex_log(LOG_DEBUG, "poll touch event trigger.");
                    break;
                }
            } else {
                ex_log(LOG_DEBUG, "interrupt trigger");
#ifdef __DEVICE_DRIVER_MEIZU_1712__
                // MEIZU_1712 workaround, need abort
                fp_device_interrupt_enable(g_hdev, FLAG_INT_ABORT);
                return true;
#endif
            }
            break;
        }

        if (retry_count != 0 && icount-- <= 0) bWait = FALSE;
    }

#ifndef _WINDOWS
    if (!host_touch_is_enable()) {
        fp_device_interrupt_enable(g_hdev, FLAG_INT_CLOSE);
    }
#endif
    g_wait_trigger_status = retval;
    ex_log(LOG_DEBUG, "exit, g_wait_trigger_status = %d", g_wait_trigger_status);
    if (g_wait_trigger_status == FINGERPRINT_RES_SUCCESS) {
        return true;
    } else {
        return false;
    }
}

int get_wait_trigger_status() {
    return g_wait_trigger_status;
}

poll_revents event_poll_wait(event_check_fd* check_group, uint32_t fd_count, int timeout) {
    const int sleep_time = 30;
    int count;
    unsigned int i;
    event_check_fd* check;
    poll_revents got_event = REVENT_TIMEOUT;

    if (NULL == check_group) {
        ex_log(LOG_ERROR, "event_poll_wait NULL check_group");
        return -1;
    }

    count = timeout / sleep_time;
    do {
        for (i = 0; i < fd_count; i++) {
            check = check_group + i;
            if (NULL == check->checkerFunc) return -1;
            check->revent = check->checkerFunc();
            if (check->revent) {
                got_event = REVENT_EVENT_POLLIN;
                // ex_log(LOG_DEBUG, "check, %d, %d",i, check->events);
            }
        }
        ex_log(LOG_DEBUG, "spi_poll_wait [%d] ", count);
        if (got_event != REVENT_TIMEOUT) break;

        if (count > 0) {
            plat_sleep_time(sleep_time);
        }
    } while (count-- > 0);

    return got_event;
}
