#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "plat_log.h"
#include "response_def.h"
#include "tee_client_api.h"
#include "transporter.h"

/* the value of uuid is defined by a SPECIFIC PROJECT */
TEEC_UUID uuid = {0X03F4FCC2, 0XBBC4, 0X4AB4, {0XA7, 0X6F, 0XB2, 0X62, 0X7A, 0X7E, 0X72, 0XDE}};

TEEC_Context g_context;
TEEC_Session g_session;

static int check_connection_open() {
    TEEC_Result result;

    result = TEEC_InvokeCommand(&g_session, TEE_CONFIRM_COMMAND, NULL, NULL);
    if (TEEC_SUCCESS == result) {
        return FINGERPRINT_RES_SUCCESS;
    }

    return FINGERPRINT_RES_FAILED;
}

static int reset_connection() {
    TEEC_CloseSession(&g_session);
    TEEC_FinalizeContext(&g_context);

    memset(&g_session, 0, sizeof(TEEC_Session));
    memset(&g_context, 0, sizeof(TEEC_Context));

    return FINGERPRINT_RES_SUCCESS;
}

static int keep_connection_open() {
    TEEC_Result result;
    unsigned int return_origin;

    if (g_session.fd > 0 && g_context.fd > 0) {
        return FINGERPRINT_RES_SUCCESS;
    }

    reset_connection();

    result = TEEC_InitializeContext(NULL, &g_context);
    if (TEEC_SUCCESS != result) {
        ex_log(LOG_ERROR, "TEEC_InitializeContext failed with return value:%x\n", result);
        return FINGERPRINT_RES_FAILED;
    }

    result = TEEC_OpenSession(&g_context, &g_session, &uuid, TEEC_LOGIN_PUBLIC, NULL, NULL,
                              &return_origin);
    if (TEEC_SUCCESS != result) {
        ex_log(LOG_ERROR,
               "TEEC_OpenSession failed with return value:%x, return "
               "origin:%x\n",
               result, return_origin);
        return FINGERPRINT_RES_FAILED;
    }

    return check_connection_open();
}

int transporter(unsigned char* msg_data, unsigned int msg_data_len, unsigned char* out_data,
                unsigned int* out_data_len) {
    int retval;
    TEEC_Result result;
    TEEC_Operation operation;
    unsigned int return_origin;
    unsigned char padding;

    if (NULL == msg_data || msg_data_len == 0) {
        ex_log(LOG_ERROR, "invalid input params\n");
        return FINGERPRINT_RES_INVALID_PARAM;
    }

    retval = keep_connection_open();
    if (FINGERPRINT_RES_SUCCESS != retval) {
        ex_log(LOG_ERROR, "keep_connection_open failed\n");
        return FINGERPRINT_RES_FAILED;
    }

    operation.started = 1;
    /* prepare request buffer */
    operation.params[0].tmpref.buffer = msg_data;
    operation.params[0].tmpref.size = msg_data_len;
    /* prepare response buffer */
    if (NULL != out_data && NULL != out_data_len) {
        operation.params[1].tmpref.buffer = out_data;
        operation.params[1].tmpref.size = *out_data_len;
    } else {
        operation.params[1].tmpref.buffer = &padding;
        operation.params[1].tmpref.size = 1;
    }

    operation.paramTypes = TEEC_PARAM_TYPES(TEEC_MEMREF_TEMP_INPUT, TEEC_MEMREF_TEMP_OUTPUT,
                                            TEEC_VALUE_OUTPUT, TEEC_NONE);

    result = TEEC_InvokeCommand(&g_session, TEE_DEFAULT_COMMAND, &operation, &return_origin);
    if (TEEC_SUCCESS != result) {
        ex_log(LOG_ERROR,
               "TEEC_InvokeCommand failed with return value: 0x%x\n, "
               "return origin = 0%x",
               result, return_origin);
        reset_connection();
        return FINGERPRINT_RES_FAILED;
    }

    if (NULL != out_data && NULL != out_data_len) {
        *out_data_len = operation.params[1].tmpref.size;
    }

    retval = operation.params[2].value.a;

    return retval;
}
