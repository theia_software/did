#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <trusty/tipc.h>

#include "plat_log.h"
#include "response_def.h"
#include "transporter.h"

#define TRUSTY_DEVICE_NAME "/dev/trusty-ipc-dev0"
#define RBS_FINGERPRINT_TA_PORT "com.android.trusty.tademo"

typedef struct request_message {
    /*tos spec, operation & command_id [ FORCED ]*/
    unsigned int command_id;
    /* repected out_buf_len
    ** let our TA knows that how much space should be prepared for outbuf*/
    unsigned int out_buf_len;
    /*the key message data length*/
    unsigned int data_len;
    /*the key message data*/
    unsigned char data[0];
} request_message_t;

typedef struct response_message {
    unsigned int command_id;
    int retval;
    unsigned int data_len;
    unsigned char data[0];
} response_message_t;

int g_handle = 0;

static int check_connection_open() {
    g_handle = tipc_connect(TRUSTY_DEVICE_NAME, RBS_FINGERPRINT_TA_PORT);
    if (g_handle < 0) {
        ex_log(LOG_ERROR, "tipc_connect fail\n");
        return FINGERPRINT_RES_FAILED;
    }

    return FINGERPRINT_RES_SUCCESS;
}

static int reset_connection() {
    if (0 != g_handle) {
        tipc_close(g_handle);
        g_handle = 0;
    }

    return FINGERPRINT_RES_SUCCESS;
}

static int keep_connection_open() {
    if (g_handle > 0) {
        return FINGERPRINT_RES_SUCCESS;
    }

    reset_connection();

    return check_connection_open();
}

int transporter(unsigned char* msg_data, unsigned int msg_data_len, unsigned char* out_data,
                unsigned int* out_data_len) {
    int result, retval;
    request_message_t* req_msg = NULL;
    unsigned int req_msg_size;
    response_message_t* rsp_msg = NULL;
    unsigned int rsp_msg_size;

    if (NULL == msg_data || msg_data_len == 0) {
        ex_log(LOG_ERROR, "invalid input params\n");
        return FINGERPRINT_RES_INVALID_PARAM;
    }

    retval = keep_connection_open();
    if (FINGERPRINT_RES_SUCCESS != retval) {
        ex_log(LOG_ERROR, "keep_connection_open failed\n");
        return FINGERPRINT_RES_FAILED;
    }

    req_msg_size = msg_data_len + sizeof(request_message_t);
    req_msg = (request_message_t*)malloc(req_msg_size);
    if (NULL == req_msg) {
        return FINGERPRINT_RES_ALLOC_FAILED;
    }

    req_msg->command_id = TEE_DEFAULT_COMMAND;
    req_msg->data_len = msg_data_len;
    memcpy(req_msg->data, msg_data, req_msg->data_len);

    if (NULL != out_data && NULL != out_data_len && *out_data_len > 0) {
        ex_log(LOG_DEBUG, "msg_data_len = %d, out_data_len = %d\n", msg_data_len, *out_data_len);
        req_msg->out_buf_len = *out_data_len;
        rsp_msg_size = *out_data_len + sizeof(response_message_t);
    } else {
        req_msg->out_buf_len = 0;
        rsp_msg_size = sizeof(response_message_t);
    }

    rsp_msg = (response_message_t*)malloc(rsp_msg_size);
    if (NULL == rsp_msg) {
        retval = FINGERPRINT_RES_ALLOC_FAILED;
        goto EXIT;
    }

    memset(rsp_msg, 0, rsp_msg_size);
    rsp_msg->data_len = req_msg->out_buf_len;

    result = write(g_handle, req_msg, req_msg_size);
    if (result <= 0) {
        ex_log(LOG_ERROR, "failed to write: %s\n", strerror(errno));
        retval = FINGERPRINT_RES_FAILED;
        goto EXIT;
    }

    result = read(g_handle, rsp_msg, rsp_msg_size);
    if (result <= 0) {
        ex_log(LOG_ERROR, "failed to read: %s\n", strerror(errno));
        retval = FINGERPRINT_RES_FAILED;
        goto EXIT;
    }

    if (NULL != out_data && NULL != out_data_len && rsp_msg->data_len <= *out_data_len) {
        *out_data_len = rsp_msg->data_len;
        memcpy(out_data, rsp_msg->data, rsp_msg->data_len);
    }

    retval = rsp_msg->retval;

EXIT:

    if (NULL != req_msg) {
        free(req_msg);
        req_msg = NULL;
    }

    if (NULL != rsp_msg) {
        free(rsp_msg);
        rsp_msg = NULL;
    }

    return retval;
}
