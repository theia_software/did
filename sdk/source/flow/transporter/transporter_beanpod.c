#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "plat_log.h"
#include "response_def.h"
#include "tee_client_api.h"
#include "transporter.h"

TEEC_UUID uuid = {0X03F4FCC2, 0XBBC4, 0X4AB4, {0XA7, 0X6F, 0XB2, 0X62, 0X7A, 0X7E, 0X72, 0XDE}};

TEEC_Context g_context;
TEEC_Session g_session;
TEEC_Context* g_pcontext = NULL;
TEEC_Session* g_psession = NULL;

static int check_connection_open() {
    TEEC_Result result;

    if (NULL != g_pcontext && NULL != g_psession) {
        ex_log(LOG_DEBUG,
               "check_connection_open g_pcontext != NULL and "
               "g_psession != NULL enter\n");
        result = TEEC_InvokeCommand(&g_session, TEE_CONFIRM_COMMAND, NULL, NULL);
        if (TEEC_SUCCESS == result) {
            return FINGERPRINT_RES_SUCCESS;
        }
    }

    return FINGERPRINT_RES_FAILED;
}

static int reset_connection() {
    if (NULL != g_psession) {
        TEEC_CloseSession(&g_session);
        g_psession = NULL;
    }

    if (NULL != g_pcontext) {
        TEEC_FinalizeContext(&g_context);
        g_pcontext = NULL;
    }

    memset(&g_session, 0, sizeof(TEEC_Session));
    memset(&g_context, 0, sizeof(TEEC_Context));

    return FINGERPRINT_RES_SUCCESS;
}

static int keep_connection_open() {
    TEEC_Result result;
    int retval;
    unsigned int return_origin;

    retval = check_connection_open();
    if (FINGERPRINT_RES_SUCCESS != retval) {
        return FINGERPRINT_RES_FAILED;
    }

    reset_connection();

    result = TEEC_InitializeContext(NULL, &g_context);
    if (TEEC_SUCCESS != result) {
        ex_log(LOG_ERROR, "TEEC_InitializeContext failed with return value:%x\n", result);
        return FINGERPRINT_RES_FAILED;
    }

    g_pcontext = &g_context;
    result = TEEC_OpenSession(&g_context, &g_session, &uuid, TEEC_LOGIN_PUBLIC, NULL, NULL,
                              &return_origin);

    if (TEEC_SUCCESS != result) {
        ex_log(LOG_ERROR,
               "TEEC_OpenSession failed with return value:%x, return "
               "origin:%x\n",
               result, return_origin);
        return FINGERPRINT_RES_FAILED;
    }

    g_psession = &g_session;

    return FINGERPRINT_RES_SUCCESS;
}

int transporter(unsigned char* msg_data, unsigned int msg_data_len, unsigned char* out_data,
                unsigned int* out_data_len) {
    int retval;
    TEEC_Result result;
    TEEC_Operation operation;
    unsigned int return_origin;
    unsigned char padding;

    if (NULL == msg_data || msg_data_len == 0) {
        ex_log(LOG_ERROR, "invalid input params\n");
        return FINGERPRINT_RES_INVALID_PARAM;
    }

    retval = keep_connection_open();
    if (FINGERPRINT_RES_SUCCESS != retval) {
        ex_log(LOG_ERROR, "keep_connection_open failed\n");
        return FINGERPRINT_RES_FAILED;
    }

    operation.started = 1;

    operation.params[0].tmpref.buffer = msg_data;
    operation.params[0].tmpref.size = msg_data_len;

    if (NULL != out_data && NULL != out_data_len) {
        operation.params[1].tmpref.buffer = out_data;
        operation.params[1].tmpref.size = *out_data_len;
    } else {
        operation.params[1].tmpref.buffer = &padding;
        operation.params[1].tmpref.size = 1;
    }

    operation.paramTypes = TEEC_PARAM_TYPES(TEEC_MEMREF_TEMP_INPUT, TEEC_MEMREF_TEMP_OUTPUT,
                                            TEEC_VALUE_OUTPUT, TEEC_NONE);

    result = TEEC_InvokeCommand(&g_session, TEE_DEFAULT_COMMAND, &operation, &return_origin);
    if (TEEC_SUCCESS != result) {
        ex_log(LOG_ERROR,
               "TEEC_InvokeCommand failed with return value: 0x%x\n, "
               "return origin = 0%x",
               result, return_origin);
        return FINGERPRINT_RES_FAILED;
    }

    if (NULL != out_data && NULL != out_data_len) {
        *out_data_len = operation.params[1].tmpref.size;
    }

    retval = operation.params[2].value.a;

    return retval;
}
