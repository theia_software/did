#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ioctl.h>
#include <unistd.h>

#include "plat_log.h"
#include "response_def.h"
#include "transporter.h"

#define UT_FP_ENTRY_IOCTL_DEVICE "/dev/teei_fp"
#define MAGIC_NUMMBER 'T'
#define UT_FP_ENTRY_IOCTL_MAGIC_NO _IO(MAGIC_NUMMBER, 0x2)
#define MESSAGE_HEADER_DEFAULT_SIZE sizeof(request_message_t)

typedef struct teei_common_header {
    unsigned int reserve1;
    unsigned int reserve2;
    unsigned int reserve3;
    unsigned int data_len;
} teei_common_header_t;

typedef struct request_message {
    teei_common_header_t header;
    unsigned int command_id;
    unsigned int in_data_len;
    unsigned int out_data_len;
    unsigned char in_data[0];
} request_message_t;

typedef struct response_message {
    teei_common_header_t header;
    unsigned int retval;
    unsigned int out_data_len;
    unsigned char out_data[0];
} response_message_t;

int g_handle = 0;

static int check_connection_open() {
    request_message_t req_msg;
    req_msg.command_id = TEE_CONFIRM_COMMAND;
    req_msg.header.data_len = MESSAGE_HEADER_DEFAULT_SIZE;
    req_msg.in_data_len = 0;

    return ioctl(g_handle, UT_FP_ENTRY_IOCTL_MAGIC_NO, req_msg);
}

static int reset_connection() {
    if (0 != g_handle) {
        close(g_handle);
        g_handle = 0;
    }

    return FINGERPRINT_RES_SUCCESS;
}

static int keep_connection_open() {
    if (g_handle > 0) {
        return FINGERPRINT_RES_SUCCESS;
    }

    reset_connection();

    g_handle = open(UT_FP_ENTRY_IOCTL_DEVICE, O_RDWR);
    if (g_handle <= 0) {
        return FINGERPRINT_RES_FAILED;
    }

    return check_connection_open();
}

int transporter(unsigned char* msg_data, unsigned int msg_data_len, unsigned char* out_data,
                unsigned int* out_data_len) {
    int retval = FINGERPRINT_RES_FAILED;
    unsigned char* buffer = NULL;
    unsigned int buffer_len;
    request_message_t* req_msg = NULL;
    response_message_t* rsp_msg = NULL;
    unsigned int req_msg_size;
    unsigned int rsp_msg_size;

    ex_log(LOG_DEBUG, "transporter enter\n");

    if (NULL == msg_data || msg_data_len == 0 || NULL == out_data_len) {
        ex_log(LOG_ERROR, "invalid input params\n");
        return FINGERPRINT_RES_INVALID_PARAM;
    }

    retval = keep_connection_open();
    if (FINGERPRINT_RES_SUCCESS != retval) {
        ex_log(LOG_ERROR, "keep_connection_open failed\n");
        return FINGERPRINT_RES_FAILED;
    }

    if (msg_data_len >= *out_data_len) {
        buffer_len = MESSAGE_HEADER_DEFAULT_SIZE + msg_data_len;
    } else {
        buffer_len = MESSAGE_HEADER_DEFAULT_SIZE + (*out_data_len);
    }

    buffer = (unsigned char*)malloc(buffer_len);
    if (NULL == buffer) {
        return FINGERPRINT_RES_ALLOC_FAILED;
    }

    memset(buffer, 0, buffer_len);

    /* set request message */
    req_msg = (request_message_t*)buffer;
    req_msg->command_id = TEE_DEFAULT_COMMAND;
    req_msg->header.data_len = buffer_len;
    req_msg->in_data_len = req_msg_size = msg_data_len;
    req_msg->out_data_len = (out_data_len == NULL) ? 0 : *out_data_len;
    memcpy(req_msg->in_data, msg_data, req_msg_size);

    /* prepare response buffer */
    rsp_msg = (response_message_t*)buffer;

    retval = ioctl(g_handle, UT_FP_ENTRY_IOCTL_MAGIC_NO, req_msg);
    if (FINGERPRINT_RES_SUCCESS != retval) {
        ex_log(LOG_ERROR, "send message failed, errno:%d, %s\n", errno, strerror(errno));
        retval = FINGERPRINT_RES_FAILED;
        goto EXIT;
    }

    /* checks if the output buffer is compatible */
    if (NULL != out_data && *out_data_len >= rsp_msg->out_data_len) {
        *out_data_len = rsp_msg->out_data_len;
        memcpy(out_data, rsp_msg->out_data, rsp_msg->out_data_len);
    } else {
        *out_data_len = 0;
    }

    retval = rsp_msg->retval;

EXIT:
    if (buffer) {
        free(buffer);
        buffer = NULL;
    }

    return retval;
}
