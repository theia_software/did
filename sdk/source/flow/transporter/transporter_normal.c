#include <stdio.h>
#include <string.h>

#include "command_handler.h"
#include "common_definition.h"
#include "egis_log.h"
#include "inline_handler.h"
#include "message_handler.h"

#define LOG_TAG "RBS"
#define RETURN_OK 0
#define RETURN_FAIL -1
#define RETURN_CHECK_FAIL -2

#define SHELL_VERSION "0.11"

int message_handler(unsigned char* msg_data, int msg_size, unsigned char* out_data,
                    int* out_data_len) {
    egislog_v("shell_version: %s", SHELL_VERSION);

    if (msg_data == NULL || (uint32_t)msg_size < sizeof(int) * 5 /*sizeof(transfer_package_t)*/
        || msg_size % BASE_TRANSFER_SIZE != 0)
        return RETURN_FAIL;

    int ret = RETURN_FAIL, pid;

    memcpy(&pid, msg_data, sizeof(int));
    egislog_v("pid: %d", pid);

    if (pid == PID_COMMAND) {
        ret = command_handler(msg_data, msg_size, out_data, out_data_len);
    } else if (pid == PID_INLINETOOL) {
        ret = inline_handler(msg_data, msg_size, out_data, out_data_len);
    }

    return ret;
}

int transporter(unsigned char* msg_data, int msg_data_len, unsigned char* out_data,
                int* out_data_len) {
    return message_handler(msg_data, msg_data_len, out_data, out_data_len);
}
