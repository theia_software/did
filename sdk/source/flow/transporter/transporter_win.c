#include "message_handler.h"

int transporter(unsigned char* msg_data, int msg_data_len, unsigned char* out_data,
                int* out_data_len) {
    return message_handler(msg_data, msg_data_len, out_data, out_data_len);
}