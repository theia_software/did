/** @addtogroup SECDRV
 * @{
 * Secure Driver
 *
 * Copyright (c) 2012 Samsung Electronics Co., Ltd.
 */
#ifndef __SECDRV_HW_HAL_H__
#define __SECDRV_HW_HAL_H__

#include <ut_sys_type.h>

#define MAXIMUM_DATA_LENGTH 1024

#undef DR_DEBUG
#undef CRYPTO_INTR_MODE

#define SPI_MAX_PORT_NUM 5

#define SPI_PORT (0x1)

#define BUF_SIZE (32 * 1024)
#define MAX_SPEED (15000000)
#define CPUMODE (0x0)
#define DMAMODE (0x1)

#define IRAM_SECURE_OS_BASE (0x02073000)
#define IRAM_SECURE_OS_OFFSET (0x848)
#define OEM_FLAG_OFFSET (0x46008)

/* DMA buffer size in the memory zone of Secure OS */
#define DMA_MICRO_CODE_SIZE (0x2000)
#define DMA_TX_BUF_SIZE (0x8000)
#define DMA_RX_BUF_SIZE (0x8000)
//#define DMA_SECURE_FLAG		(0x13000)
//#define SIZE_OF_EL3_CONTEXT	0x80000
//#define SIZE_OF_SECDMA		0x40000

/* bits per word */
#define BYTE_SIZE (0)
#define HWORD_SIZE (1)
#define WORD_SIZE (2)

/* Interrupt Thread Source */
#define EINT_THREAD 0

typedef enum {
    SECDRV_OK = 0x00000,
    E_SECDRV_INITIALIZATION = 0x40001,
    E_SECDRV_FINALIZATION = 0x40002,
    E_SECDRV_ATTACH_CRYPTO_HW = 0x40003,
    E_SECDRV_DETACH_CRYPTO_HW = 0x40004,
    E_SECDRV_AES_CIPHER = 0x40005,
    E_SECDRV_START_CONTENT_PATH_PROTECTION = 0x40006,
    E_SECDRV_STOP_CONTENT_PATH_PROTECTION = 0x40007,
    E_SECDRV_LOAD_MFC_FW = 0x40008,
    E_SECDRV_LOAD_VIDEO_DATA = 0x40009,
    E_SECDRV_SYSMMU_INIT = 0x4000A,
    E_SECDRV_SYSMMU_EXIT = 0x4000B,
    E_SECDRV_TZPC_INIT = 0x4000C,
    E_SECDRV_TZPC_EXIT = 0x4000D,
    E_SECDRV_TZASC_INIT = 0x4000E,
    E_SECDRV_TZASC_EXIT = 0x4000F,
    E_SECDRV_OUT_OF_SECURE_RANGE = 0x40010,
    E_SECDRV_OUT_OF_NONSECURE_RANGE = 0x40011,

    E_SECDRV_I2C_INVALID_CHANNEL = 0x40030,
    E_SECDRV_I2C_TRANSFER_INVALID_DATA = 0x40031,
    E_SECDRV_I2C_TIMEOUT = 0x40032,
    E_SECDRV_I2C_GPIO_SET = 0x40033,
    E_SECDRV_I2C_CLOCKRATE_SET = 0x40034,
    E_SECDRV_I2C_READ_FAILED = 0x40035,
    E_SECDRV_I2C_WRITE_FAILED = 0x40036,

    E_SECDRV_SPI_INVALID_CHANNEL = 0x40050,
    E_SECDRV_SPI_TRANSFER_INVALID_DATA = 0x40051,
    E_SECDRV_SPI_TIMEOUT = 0x40052,
    E_SECDRV_SPI_GPIO_SET = 0x40053,
    E_SECDRV_SPI_CLOCKRATE_SET = 0x40054,
    E_SECDRV_SPI_INIT_FAILED = 0x40055,
    E_SECDRV_SPI_DMA_INIT_FAILED = 0x40056,
    E_SECDRV_SPI_READ_FAILED = 0x40057,
    E_SECDRV_SPI_WRITE_FAILED = 0x40058,
    E_SECDRV_SPI_WRITE_READ_FAILED = 0x40059,
    E_SECDRV_SPI_CLOCK_INIT_FAILED = 0x40060,
    E_SECDRV_SPI_CLOCK_ENABLE_FAILED = 0x40061,
    E_SECDRV_SPI_CLOCK_DISABLE_FAILED = 0x40062,

    E_SECDRV_DMA_TEST_FAILED = 0x40063,

    E_SECDRV_INVALID_OEM_FLAG_ADDR = 0x40070,

} secDrvResult_t;

typedef enum {
    SW_CRYPTO_OK = 0x00000,
    E_SW_CRYPTO_INVALID_OFFSET = 0x50001,
    E_SW_CRYPTO_INVALID_LENGTH = 0x50002,
    E_SW_CRYPTO_INVAILD_IV_LEN = 0x50003,
    E_SW_CRYPTO_INVAILD_KEY_LEN = 0x50004,
    E_SW_CRYPTO_INVAILD_DATA = 0x50005,
    E_SW_CRYPTO_AES_SET_ALGO = 0x50006,
    E_SW_CRYPTO_AES_ECB_INIT = 0x50007,
    E_SW_CRYPTO_AES_ECB_UPDATE = 0x50008,
    E_SW_CRYPTO_AES_ECB_FINAL = 0x50009,
    E_SW_CRYPTO_AES_CBC_INIT = 0x5000A,
    E_SW_CRYPTO_AES_CBC_UPDATE = 0x5000B,
    E_SW_CRYPTO_AES_CBC_FINAL = 0x5000C,
    E_SW_CRYPTO_ENCRYPT = 0x5000D,
    E_SW_CRYPTO_DECRYPT = 0x5000E,
} swCryptoResult_t;

enum spi_tx_format {
    SPI_TX_MODE_0,
    SPI_TX_MODE_1,
    SPI_TX_MODE_2,
    SPI_TX_MODE_3,
};

/* Device Struct */
typedef struct {
    void* buf_addr;
    ut_uint32_t buf_len;
    ut_uint32_t total_len;
} spi_transfer_t;

typedef struct {
    ut_uint16_t delay_usecs;
    ut_uint32_t speed_hz;
    ut_uint8_t bits_per_word;
    ut_uint8_t dma_mode;
    enum spi_tx_format spi_mode;
} spi_config_t;

enum flag_lists {
    OEM_FLAG,
    NUM_FLAGS,
};

struct secFlag_t {
    ut_uint32_t* addr;
    ut_uint32_t value;
};

struct secDrm_ctx_t {
    struct secFlag_t secFlag[NUM_FLAGS];
};

/*
 * SEC XXX function
 */
secDrvResult_t sec_spi_init(ut_uint32_t port, spi_config_t* spi_cfg);
secDrvResult_t sec_spi_dma_init();
secDrvResult_t sec_spi_write(ut_uint32_t port, spi_config_t* spi_cfg, spi_transfer_t* spi_tx);
secDrvResult_t sec_spi_read(ut_uint32_t port, spi_config_t* spi_cfg, spi_transfer_t* spi_rx);
secDrvResult_t sec_spi_write_read(ut_uint32_t port, spi_config_t* spi_cfg, spi_transfer_t* spi_tx,
                                  spi_transfer_t* spi_rx);
secDrvResult_t sec_spi_exit(ut_uint32_t port);
secDrvResult_t sec_spi_protect(ut_uint32_t port);

secDrvResult_t sec_spi_clock_init(ut_uint32_t port, spi_config_t* spi_cfg);
secDrvResult_t sec_spi_clock_enable(ut_uint32_t port);
secDrvResult_t sec_spi_clock_disable(ut_uint32_t port);

#endif /* __SECDRV_HW_HAL_H__ */
