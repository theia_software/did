#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>

#include <ut_pf_fp.h>
#include <ut_pf_spi.h>
#include <ut_sys_util.h>
#include "command_handler.h"
#include "common_definition.h"
#include "inline_handler.h"
#include "plat_mem.h"

int open(const char* path, int flags);
int close(int fd);
ssize_t read(int fd, void* buf, size_t count);
ssize_t write(int fd, const void* buf, size_t count);
off_t lseek(int fd, off_t offset, int whence);

typedef struct request_message {
    unsigned int command_id;
    unsigned int in_data_len;
    unsigned int out_data_len;
    unsigned char in_data[0];
} request_message_t;

typedef struct response_message {
    unsigned int retval;
    unsigned int out_data_len;
    unsigned char out_data[0];
} response_message_t;

struct TransferPackage {
    uint32_t process;
    uint32_t command;
    uint32_t args1;
    uint32_t args2;
    uint32_t indata_len;
    uint8_t indata[0];
};

/**
 * This is Fingerprint template project.
 * In main function, should invoke "ut_pf_fp_main" which will initialize the
 * fingerprint service.
 * After initializing the fingerprint, as ca invoke the fingerprint ta, the
 * "ut_pf_fp_invoke_command" will
 * be invoked.
 */
int main(void) {
    ut_int32_t ret = 0;
    ut_sys_log("FP TA MAIN SUCCESS!\n");
    ret = ut_pf_fp_main();

    return ret;
}

/**
 * This function will be invoked when fp ta init.
 *
 */
void ut_pf_fp_init(void) {
    ut_sys_log("ut_pf_fp_init success!\n");
}
/**
 * @param header
 * Header struct is shown as belows(This paramter is reserved, no used):
 *----------------------------------------------------------------------
  | reserve 4 bytes | reserve 4 bytes | reserve 4 bytes | data length  |
  ----------------------------------------------------------------------
 *
 * @param data
 *  The data trans from CA
 * @param data_length
 *   The length of data
 **/
ut_int32_t ut_pf_fp_invoke_command(void* header, void* data, ut_uint32_t param_length) {
    ut_sys_log("ut_pf_fp_invoke_command enter!\n");

    if (data == NULL) return -1;

    unsigned int* pHeader = NULL;
    unsigned char* in_data = NULL;

    unsigned int ret = 0;
    unsigned int header1;
    unsigned int header2;
    unsigned int header3;
    unsigned int header4;
    unsigned int in_data_len;
    unsigned int out_data_len;
    unsigned int data_len;

    if (header == NULL) goto exit;

    pHeader = (unsigned int*)header;

    header1 = *pHeader;
    header2 = *(pHeader + 1);
    header3 = *(pHeader + 2);
    header4 = *(pHeader + 3);

    data_len = header4;

    response_message_t* rsp_msg = (response_message_t*)data; /*return*/
    request_message_t* req_msg = (request_message_t*)data;   /*send*/

    ut_sys_log(
        "ut_pf_fp_invoke_command header1 = %d , header2 = %d, header3 = "
        "%d, header4 = %d \n",
        header1, header2, header3, header4);

    ut_sys_log("ut_pf_fp_invoke_command data_len:%d\n", data_len);
    ut_sys_log("ut_pf_fp_invoke_command req_msg->command_id:%d\n", req_msg->command_id);
    ut_sys_log("ut_pf_fp_invoke_command req_msg->in_data_len:%d \n", req_msg->in_data_len);

    in_data_len = req_msg->in_data_len;
    ut_sys_log("ut_pf_fp_invoke_command in_data_len: %d \n", in_data_len);

    in_data = (unsigned int*)mem_alloc(in_data_len);
    if (in_data == NULL) {
        goto exit;
    }

    out_data_len = req_msg->out_data_len;
    ut_sys_log("ut_pf_fp_invoke_command out_data_len: %d \n", out_data_len);

    mem_set(in_data, 0x00, in_data_len);
    mem_move(in_data, req_msg->in_data, in_data_len);

    struct TransferPackage* package;
    package = (struct TransferPackage*)in_data;

    switch (package->process) {
        case PID_COMMAND:

            ret = command_handler(in_data, in_data_len, rsp_msg->out_data, &out_data_len);
            ut_sys_log("ut_pf_fp_invoke_command case PID_COMMAND! ret: %d\n", ret);

            rsp_msg->retval = ret;
            rsp_msg->out_data_len = out_data_len;
            *(pHeader + 3) = out_data_len + sizeof(unsigned int) * 2;

            break;

        case PID_INLINETOOL:
            ret = inline_handler(in_data, in_data_len, rsp_msg->out_data, &out_data_len);
            ut_sys_log("ut_pf_fp_invoke_commad out_data_len = %d\n", out_data_len);
            ut_sys_log("ut_pf_fp_invoke_command case PID_INLINETOOL! ret: %d\n", ret);

            rsp_msg->retval = ret;
            rsp_msg->out_data_len = out_data_len;
            *(pHeader + 3) = out_data_len + sizeof(unsigned int) * 2;

            break;

        default:
            break;
    }

exit:
    if (NULL != in_data) {
        mem_free(in_data);
        in_data = NULL;
    }
    pHeader = NULL;

    return 0;
}