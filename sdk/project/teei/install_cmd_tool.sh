#!/bin/bash
adb root
adb wait-for-device
adb remount

adb shell rm  /system/thh/fp_server_egistec
adb shell rm  /data/thh/tee_05/tee
adb shell rm /system/bin/rbs_test
adb shell rm /vendor/lib64/libRbsFlow.so
adb shell rm /system/lib64/libRbsFlow.so

adb push ./TA/fp_server_egistec /system/thh
adb push ./TA/fp_server_egistec /data/thh/tee_05/tee
adb push ./tools/rbs_test /system/bin
adb push ./flow/libRbsFlow.so /vendor/lib64

adb reboot
adb wait-for-device
adb root
adb wait-for-device
adb remount
adb shell setenforce 0