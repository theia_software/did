#!/bin/bash
adb root
if [ $? -ne 0 ]; then
    echo ''
    echo '!!!  adb root FAILED  !!!'
    echo ''
    exit 12
fi
adb wait-for-device
adb remount

adb shell rm -rf /data/fpdata
adb shell mkdir /data/fpdata
adb shell chmod 777 /data/fpdata

adb shell rm /system/bin/rbs_daemon
adb shell rm /vendor/lib64/libRbsFlow.so
adb shell rm /system/lib64/libRbsFlow.so
adb shell rm /vendor/lib64/libegis_fp_core.so
adb shell rm /system/lib64/libegis_fp_core.so

echo 'Push rbs_daemon_6 to /system/bin/rbs_daemon'
adb push ./tools/rbs_daemon_6 /system/bin/rbs_daemon
adb push ./flow/for-demotool/libRbsFlow.so /vendor/lib64
adb push ./core/libegis_fp_core.so /vendor/lib64
adb install -r ./tools/EgisDemoTool*.apk


echo '---'
echo 'Done Time: '$(date +"%T")
echo '---'

adb shell setenforce 0

echo 'Check if rbs_daemon is running ...'
echo 'adb shell ps | grep rbs'
adb shell ps | grep rbs

echo ''

echo 'kill rbs_daemon, and run it again by the commands below'
echo '------------'
echo 'adb shell'
echo '> setenforce 0'
echo '> rbs_daemon'
