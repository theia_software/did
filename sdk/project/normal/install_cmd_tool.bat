adb root
adb wait-for-device
adb remount

adb shell rm /system/bin/rbs_test
adb shell rm /vendor/lib64/libRbsFlow.so
adb shell rm /system/lib64/libRbsFlow.so
adb shell rm /vendor/lib64/libegis_fp_core.so
adb shell rm /system/lib64/libegis_fp_core.so

adb push ./tools/rbs_test /system/bin
adb push ./flow/libRbsFlow.so /vendor/lib64
adb push ./core/libegis_fp_core.so /vendor/lib64
pause

adb reboot
adb wait-for-device
adb root
adb wait-for-device
adb remount
adb shell setenforce 0