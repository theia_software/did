LOCAL_PATH := $(call my-dir)
$(info LOCAL_PATH=$(LOCAL_PATH))
GIT_PROJECT_RELATIVE := ../../../../..
SDK_SOURCE_CORE := $(GIT_PROJECT_RELATIVE)/sdk/source/core
#---------------------------------------------
#
#	Prebuilt Matching Algorithm
#
COMMON_PLATFORM=linux
ALGORITHM_VERSION=3

include $(CLEAR_VARS)
LOCAL_MODULE := MatchAlgo

ifeq ($(ALGORITHM_VERSION),3)
$(info ALGORITHM_VERSION=3)
LOCAL_SRC_FILES := $(SDK_SOURCE_CORE)/matching_algo/G3AlgoLib/NONTZ/libEgisG3Algorithm_$(TARGET_ARCH_ABI).a
else
$(error ALGORITHM_VERSION is not 3)
endif

LOCAL_MODULE_SUFFIX := .a
LOCAL_MODULE_CLASS := STATIC_LIBRARIES
include $(PREBUILT_STATIC_LIBRARY)

#--------------------------------
#
#	Build egis cmd exe & core.
#
include $(CLEAR_VARS)

LOCAL_MODULE := EgisMatcher
LOCAL_LDLIBS := -L$(SYSROOT)/usr/lib -llog
LOCAL_CFLAGS := -D__LINUX__  -DDEVICE_DRIVER_HAS_SPI -D__ET538__ -DRBS -DEGIS_DBG
LOCAL_CFLAGS += -DMTK_EVB

LOCAL_EXPORT_LDLIBS := $(LOCAL_LDLIBS)
LOCAL_CFLAGS += -DxHEAP_LOG
LOCAL_CFLAGS += -DENROLL_POLICY_REDUNDANT_ACCPET
LOCAL_CFLAGS += -DxFOD_CALIBRATION_DCFG
LOCAL_CFLAGS += -DBUILD_MATCHER
LOCAL_CFLAGS += -DUSE_QTY_FOR_FINGER_DETECT
LOCAL_CFLAGS += -DxUSE_QM_FOR_FINGER_DETECT
LOCAL_EXPORT_CFLAGS := $(LOCAL_CFLAGS)


LOCAL_STATIC_LIBRARIES := MatchAlgo

ifeq ($(ALGORITHM_VERSION),3)
LOCAL_CFLAGS += -DG3_MATCHER
endif

LOCAL_C_INCLUDES := \
	$(SDK_SOURCE_CORE)/fp_api/include \
	$(SDK_SOURCE_CORE)/fp_api/inc \
	$(SDK_SOURCE_CORE)/../common/platform/inc \
	$(SDK_SOURCE_CORE)/../common/definition \
	$(SDK_SOURCE_CORE)/enroll_verify/inc \
	$(SDK_SOURCE_CORE)/sensor_control/inc \
	$(SDK_SOURCE_CORE)/matching_algo/inc \
	$(SDK_SOURCE_CORE)/image_analysis/inc \
	$(SDK_SOURCE_CORE)/sensor_test/inc \
	$(SDK_SOURCE_CORE)/../common/utility/inih \
	$(SDK_SOURCE_CORE)/../common/utility/config/inc \
	$(SDK_SOURCE_CORE)/../common/utility/cutimage/inc  \
	$(SDK_SOURCE_CORE)/sensor_control/src/et5xx \
	$(SDK_SOURCE_CORE)/finger_detect/inc \
	$(SDK_SOURCE_CORE)/navi/inc \
	$(SDK_SOURCE_CORE)/sensor_test/inc \

LOCAL_SRC_FILES := \
	egis_fp_matcher.c \
	$(SDK_SOURCE_CORE)/../common/platform/src/$(COMMON_PLATFORM)/plat_log_$(COMMON_PLATFORM).c  \
	$(SDK_SOURCE_CORE)/../common/platform/src/$(COMMON_PLATFORM)/plat_mem_$(COMMON_PLATFORM).c  \
	$(SDK_SOURCE_CORE)/../common/platform/src/$(COMMON_PLATFORM)/plat_std_$(COMMON_PLATFORM).c  \
	$(SDK_SOURCE_CORE)/../common/platform/src/$(COMMON_PLATFORM)/plat_spi_$(COMMON_PLATFORM).c  \
	$(SDK_SOURCE_CORE)/../common/platform/src/$(COMMON_PLATFORM)/plat_time_$(COMMON_PLATFORM).c  \
	$(SDK_SOURCE_CORE)/../common/platform/src/$(COMMON_PLATFORM)/plat_file_$(COMMON_PLATFORM).c  \
	$(SDK_SOURCE_CORE)/../common/platform/src/egis_sprintf.c \
	$(SDK_SOURCE_CORE)/enroll_verify/src/egis_fp_algmodule.c \
	$(SDK_SOURCE_CORE)/enroll_verify/src/matcher_feature.c \
	$(SDK_SOURCE_CORE)/sensor_control/src/et5xx/egis_fp_common_5XX.c \
	$(SDK_SOURCE_CORE)/sensor_control/src/et5xx/ICTeam/ET538LDOCalibrationFlow.c \
	$(SDK_SOURCE_CORE)/sensor_control/src/et5xx/fingerprint_library.c \
	$(SDK_SOURCE_CORE)/sensor_control/src/et5xx/egis_fp_calibration.c \
	$(SDK_SOURCE_CORE)/sensor_control/src/et5xx/sensor_param.c \
	$(SDK_SOURCE_CORE)/sensor_control/src/et5xx/et5xx_calibration.c \
	$(SDK_SOURCE_CORE)/sensor_control/src/isensor_api.c \
	$(SDK_SOURCE_CORE)/sensor_control/src/egis_sensormodule.c \
	$(SDK_SOURCE_CORE)/sensor_control/src/fp_capture_count.c \
	$(SDK_SOURCE_CORE)/sensor_control/src/egis_handle_esd.c \
	$(SDK_SOURCE_CORE)/sensor_test/src/egis_sensor_test.c \
	$(SDK_SOURCE_CORE)/sensor_test/src/sensor_function.c \
	$(SDK_SOURCE_CORE)/sensor_test/src/mmi_sensor_test.c \
	$(SDK_SOURCE_CORE)/finger_detect/src/egis_fp_get_image.c \
	$(SDK_SOURCE_CORE)/finger_detect/src/fd_process.c \
	$(SDK_SOURCE_CORE)/finger_detect/src/fd_process_16bit.c \
	$(SDK_SOURCE_CORE)/finger_detect/src/fimage_check_qty.c \
	$(SDK_SOURCE_CORE)/finger_detect/src/fimage_check_qm.c \
	$(SDK_SOURCE_CORE)/fp_api/src/fp_sensormodule.c \
	$(SDK_SOURCE_CORE)/navi/src/egis_navi.c \ \
	$(SDK_SOURCE_CORE)/navi/src/navi_frame.c \ \
	$(SDK_SOURCE_CORE)/navi/src/navi.c \ \
	$(SDK_SOURCE_CORE)/image_analysis/src/AES.c \
	$(SDK_SOURCE_CORE)/image_analysis/src/image_analysis.c \
	$(SDK_SOURCE_CORE)/image_analysis/src/puzzle_image.c \
	$(SDK_SOURCE_CORE)/../common/utility/inih/ini.c \
	$(SDK_SOURCE_CORE)/../common/utility/inih/ini_parser.c \
	$(SDK_SOURCE_CORE)/../common/utility/config/src/core_config.c \
	$(SDK_SOURCE_CORE)/../common/utility/cutimage/src/image_cut.c \
	
LOCAL_CFLAGS += -fPIC -O1 -fsigned-char -mfloat-abi=softfp  

include $(BUILD_SHARED_LIBRARY)
