#include <stdlib.h>
#include "EgisAlgorithmAPI.h"
#include "algomodule_matcher.h"
#include "egis_algomodule.h"
#include "egis_log.h"
#include "fp_sensormodule.h"
#include "plat_heap.h"

#define LOG_TAG "RBS"

enrolment_session_t g_enroll_session = {0};
static enrolment_session_t* egis_enrol_start() {
    egislog_d("%s enter", __func__);
    int ret = FP_LIB_OK;

    egis_fp_startEnroll();

    return &g_enroll_session;
};

static int egis_enroll_add_image(enrolment_session_t* session, fp_image_t* image,
                                 fp_image_quality_t* image_quality) {
    int ret = FP_LIB_OK;
    egislog_d("%s enter", __func__);

    if (image == NULL || session == NULL || image_quality == NULL) {
        egislog_e("%s, session = %p", __func__, session);
        egislog_e("%s, image = %p", __func__, image);
        egislog_e("%s, image_quality = %p", __func__, image_quality);
        return FP_LIB_ERROR_PARAMETER;
    }

    // int result;
    fp_lib_enroll_data_t tmp_enrol_data;
    egis_image_t tmpImage;
    tmpImage.img_data.buffer = image->buffer;
    tmpImage.img_data.format.width = image->format.width;
    tmpImage.img_data.format.height = image->format.height;

    tmpImage.img_data.frame_count = 1;
    tmpImage.img_data.capacity = image->format.width * image->format.height;

    memset(&tmpImage.quality, 0, sizeof(tmpImage.quality));
    tmpImage.quality.quality = 100;
    tmpImage.quality.coverage = 100;

    ret = egis_fp_updateEnrolData(&tmp_enrol_data, &tmpImage);
    egislog_d("@egis-hfai-ret:%d", ret);

    image_quality->coverage = tmp_enrol_data.coverage;
    image_quality->quality = tmp_enrol_data.quality;
    return tmp_enrol_data.progress;
    // return ret;
}

fp_lib_template_t g_local_tpl;
static fp_lib_template_t* g_tplArray[1];

static int egis_enroll_finish(enrolment_session_t* session) {
    int ret = FP_LIB_OK;
    egislog_d("%s enter", __func__);

    /*
     *	DO NOTHING HERE;
     */
    egislog_d("%s leave", __func__);
    return ret;
}

static int egis_get_template(enrolment_session_t* session, fp_template_t** tpl) {
    int result = FP_LIB_OK;
    egislog_d("%s enter", __func__);

    fp_lib_template_t* ptemplate = (fp_lib_template_t*)plat_alloc(sizeof(fp_lib_template_t));
    if (ptemplate == NULL) goto done;

    egis_fp_getTemplateSize(&ptemplate->size);
    egislog_d("@egis-hfai-ptemplate->size %d", ptemplate->size);

    ptemplate->tpl = plat_alloc(ptemplate->size);

    // size to 0 to oberserver finish enroll did finish.
    ptemplate->size = 0;
    if (NULL == ptemplate->tpl) {
        egislog_e("@egis-hfai-ERROR_MEMORY");
        result = FP_LIB_ERROR_MEMORY;
        goto exit;
    }

    result = egis_fp_finishEnroll(ptemplate);
    egislog_d("@egis-hfai-ptemplate.2size %d", ptemplate->size);
    egislog_d("@egis-hfai-ret:%d", result);
    if (result == FP_LIB_OK) {
        /*
         * TODO!!!!
         * CHECK template size.
         */
        goto done;
    }

    if (ptemplate->tpl != NULL) {
        plat_free(ptemplate->tpl);
        // ptemplate->tpl = NULL;
    }

exit:
    if (ptemplate != NULL) {
        plat_free(ptemplate);
        // ptemplate = NULL;
    }

done:
    *tpl = (void*)ptemplate;
    egislog_d("%s template: %p ", __func__, *tpl);
    egislog_d("%s leave", __func__);
    return result;
}

static unsigned int egis_get_packed_size(fp_template_t* tpl) {
    egislog_d("%s enter", __func__);

    unsigned int size = 0;
    egis_fp_getTemplateSize(&size);
    egislog_d("%s packed Sized = %d", __func__, size);

    egislog_d("%s leave", __func__);
    return size;
}

static void egis_template_delete(fp_template_t* tpl) {
    egislog_d("%s enter", __func__);
    fp_lib_template_t* template = (fp_lib_template_t*)tpl;

    if (template == NULL) {
        egislog_e("%s, Error! tpl = %p", __func__, tpl);
        return;
    }

    if (template->tpl != NULL) plat_free(template->tpl);

    plat_free(template);

    egislog_d("%s leave", __func__);
    return;
}

int egis_identify_image(fp_image_t* img, fp_template_t** candidates, uint32_t candidate_count,
                        int32_t* result, int32_t* score, fp_image_quality_t* image_quality) {
    int ret = FP_LIB_OK;
    int candidate_index = 0;
    egislog_d("%s enter", __func__);

    if (img == NULL || candidates == NULL || image_quality == NULL || score == NULL) {
        egislog_e("%s, candidates = %p", __func__, candidates);
        egislog_e("%s, image = %p", __func__, img);
        egislog_e("%s, score = %p", __func__, score);
        egislog_e("%s, image_quality = %p", __func__, image_quality);
        return FP_LIB_ERROR_PARAMETER;
    }

    if (candidate_count <= 0) {
        egislog_e("%s, candidate_count: %d <= 0", __func__, candidate_count);
        return FP_LIB_ERROR_PARAMETER;
    }
    egis_fp_identifyStart(0, candidates, candidate_count);

    fp_lib_identify_data_t tmp_identify_data;
    egis_image_t tmpImage;
    tmpImage.img_data.buffer = img->buffer;
    tmpImage.img_data.format.width = img->format.width;
    tmpImage.img_data.format.height = img->format.height;

    tmpImage.img_data.frame_count = 1;
    tmpImage.img_data.capacity = img->format.width * img->format.height;

    memset(&tmpImage.quality, 0, sizeof(tmpImage.quality));
    tmpImage.quality.quality = 100;
    tmpImage.quality.coverage = 100;
    ret = egis_fp_identifyImage_identify(&tmpImage, &tmp_identify_data, &candidate_index);
    egislog_d("@egis-hfai-ret:%d", ret);

    image_quality->coverage = 100;
    image_quality->quality = 100;

    /*
     * TODO:
     *		???? what is the result usage.....??????????
     */

    //*result =  ret;
    if (tmp_identify_data.result == FP_LIB_IDENTIFY_MATCH) {
        *result = 0;
    } else {
        *result = 1;
    }
    //*result =  candidate_index;
    *score = tmp_identify_data.score;
    egis_fp_identifyFinish();
    egislog_d("%s leave. *result=%d", __func__, *result);
    return ret;
}

//
// NOTE. Here use different struct from ialgorithm_t in algomodule.h
//
typedef struct _ialgorithm_t_matcher {
    enrolment_session_t* (*enrolStart)();
    int32_t (*enrolAddImage)(enrolment_session_t* session, fp_image_t* image,
                             fp_image_quality_t* image_quality);
    int32_t (*enrolGetTemplate)(enrolment_session_t* session, fp_template_t** tpl);
    int32_t (*enrolFinish)(enrolment_session_t* session);
    int32_t (*identifyImage)(fp_image_t* img, fp_template_t** candidates, uint32_t candidate_count,
                             int32_t* result, int32_t* score, fp_image_quality_t* image_quality);
    uint32_t (*templateGetPackedSize)(fp_template_t* tpl);
    int32_t (*templatePack)(fp_template_t* tpl, uint8_t* dst);
    int32_t (*templateUnPack)(uint8_t* src, uint32_t length, fp_template_t** tpl);
    void (*templateDelete)(fp_template_t* tpl);
} ialgorithm_t_matcher;

void getIalgorithm(ialgorithm_t_matcher* ialgorithm) {
    egislog_d("%s enter (use ialgorithm_t_matcher)", __func__);

    if (ialgorithm == NULL) {
        egislog_e("%s ialgorithm = NULL", __func__);
    }
    egis_fp_initAlgAndPPLib(FP_ALGOAPI_MODE_EGIS_ET538);
    ialgorithm->enrolStart = egis_enrol_start;
    ialgorithm->enrolAddImage = egis_enroll_add_image;
    ialgorithm->enrolGetTemplate = egis_get_template;
    ialgorithm->enrolFinish = egis_enroll_finish;
    ialgorithm->identifyImage = egis_identify_image;
    ialgorithm->templateGetPackedSize = egis_get_packed_size;
    /*
    ialgorithm->templatePack = egis_template_pack;
    ialgorithm->templateUnPack = egis_template_unpack;
    */
    ialgorithm->templatePack = NULL;
    ialgorithm->templateUnPack = NULL;
    ialgorithm->templateDelete = egis_template_delete;

    egislog_d("%s leave", __func__);
}
