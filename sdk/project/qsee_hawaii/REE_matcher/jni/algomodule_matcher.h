﻿#ifndef _IALGORITHM_H_
#define _IALGORITHM_H_
#include <tee_internal_api.h>
#include "algomodule.h"
#include "fp_algomodule.h"

struct _HuaweiEnrolSession {
    int16_t nMaxNum;
    int16_t nUsedNum;
    // wb modify the variable name
    uint8_t* pSessionData;
};

struct _huawei_template_t {
    void* pFingerTemplate;
};

typedef struct {
    uint32_t g_fpMaxTemplateNumUpdate;
} updateFingerTempInfo_t;

// it is fatal
typedef struct _HuaweiEnrolSession enrolment_session_t;
typedef struct _huawei_template_t huawei_template_t;

typedef struct fpalgorithm_config_t {
    int sensor_type;
    int enr_max;
    int upd_max;
    int upd_exc;
    int security;
    int enr_coverage_min;
    int enr_quality_min;
    int enr_overlay_max;
    int enr_preoverlay_max;
} fpalgorithm_config_t;

int32_t preprocessor_init(const Fp_FrameFormat* format, int8_t* base);
void preprocessor_cleanup();
int32_t preprocessor(fp_image_t* src, fp_image_t* result, fp_image_quality_t* image_quality);
uint32_t preprocess_get_calidata_len(void);
int32_t preprocess_save_calidata(uint8_t* buffer, uint32_t* len);
int32_t preprocess_load_calidata(uint8_t* buffer, uint32_t len);

typedef struct {
    int y;
    int x;
} fp_point_t;

typedef struct {
    fp_point_t bottom_left;
    fp_point_t bottom_right;
    fp_point_t top_left;
    fp_point_t top_right;
} fp_rect_t;

typedef struct {
    fp_rect_t* masks;
    int nr_of_masks;
} fp_mask_list_t;

typedef enum {
    FP_ENROLL_SUCCESS,
    // huawei define start
    FP_ENROLL_HELP_SAME_AREA,
    FP_ENROLL_HELP_TOO_WET,
    FP_ENROLL_HELP_ALREADY_EXIST,
    // huawei define en
    FP_ENROLL_TOO_MANY_ATTEMPTS,
    FP_ENROLL_TOO_MANY_FAILED_ATTEMPTS,
    FP_ENROLL_FAIL_NONE,
    FP_ENROLL_FAIL_LOW_QUALITY,
    FP_ENROLL_FAIL_LOW_COVERAGE,
    FP_ENROLL_FAIL_LOWQUALITY_AND_LOW_COVERAGE,
} fp_enroll_result_t;

typedef struct {
    uint32_t progress;                /*progress of the current enroll process in percent */
    uint32_t quality;                 /*Quality for the image*/
    fp_enroll_result_t result;        /*Status of current enroll attempt*/
    uint32_t nr_successful;           /*number of successful enroll attempts so far*/
    uint32_t nr_failed;               /*number of failed enroll attempts so far*/
    uint32_t enrolled_template_size;  /*size of the enrolled template*/
    uint32_t extended_enroll_size;    /*size of the data part in the structure
                         used for extended enroll*/
    uint32_t coverage;                /*coverage of the image*/
    int8_t user_touches_too_immobile; /*used to indicate that touches are
                         too similar*/
    int8_t guide_direction;
} fp_enroll_data_t;

typedef struct {
    fp_rect_t masks[10];
    int number;
} HwMaskList;

typedef struct {
    fp_enroll_data_t guide_enroll_result;
    fp_rect_t last_touch;
    fp_rect_t next_touch;
    HwMaskList mask_list;
} HwGuideData;

enum TA_GUIDE_DATA_TYPE {
    GUIDE_DATA_NULL = 0,
    GUIDE_DATA_SUGGEST_DIRECT_NEXT = 1,
    GUIDE_DATA_MAIN_CLUSTER = 2,
    GUIDE_DATA_TOUCH_LAST = 4,
    GUIDE_DATA_TOUCH_NEXT = 8,
    GUIDE_DATA_STICHED_MASK_LIST = 16,
};

typedef struct ialgorithm_t {
    enrolment_session_t* (*enrolStart)();
    int32_t (*sensorCheck)(const fp_image_t* sensor_raw, const fp_image_t* last_baseRaw,
                           uint8_t* pFlagWhiteSet, int32_t* defectPixelNum);

    int32_t (*enrolAddImage)(enrolment_session_t* session, fp_image_t* image,
                             fp_image_quality_t* image_quality);

    int8_t (*enrolGetProgress)(enrolment_session_t* session, int8_t* touches_very_immobile);

    int32_t (*enrolGetSuggestedDirectionOfNextTouch)(enrolment_session_t* session);

    int8_t (*enrolMainClusterIdentified)(enrolment_session_t* session);

    int32_t (*enrolGetLastTouch)(enrolment_session_t* session, fp_rect_t* rect);

    int32_t (*enrolGetNextTouch)(enrolment_session_t* session, fp_rect_t* rect);

    int32_t (*enrolGetStitchedMaskList)(enrolment_session_t* session, fp_mask_list_t* rectList);

    int8_t (*enrolEstimateFingerSize)(enrolment_session_t* session);

    int32_t (*enrolGetTemplate)(enrolment_session_t* session, fp_template_t** tpl);

    int32_t (*enrolFinish)(enrolment_session_t* session);

    int32_t (*identifyImage)(fp_image_t* img, fp_template_t** candidates, uint32_t candidate_count,
                             int32_t* result, int32_t* score, fp_image_quality_t* image_quality);

    int32_t (*templateStudy)(int32_t* pnUpdate);

    uint32_t (*templateGetPackedSize)(fp_template_t* tpl);

    int32_t (*templatePack)(fp_template_t* tpl, uint8_t* dst);

    int32_t (*templateUnPack)(uint8_t* src, uint32_t length,
                              updateFingerTempInfo_t* pstupdataFingerTempInfo, fp_template_t** tpl);

    void (*templateDelete)(fp_template_t* tpl);

    void (*getImageQuality)(fp_image_t* image, fp_image_quality_t* image_quality);
} ialgorithm_t;

long long getIalgorithmVersion(void);

void applyConfiguration(const fpalgorithm_config_t* config);

void initConfiguration(fpalgorithm_config_t* config);

#endif
