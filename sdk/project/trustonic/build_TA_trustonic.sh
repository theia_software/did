#!/bin/bash
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
me=`basename $0`
echo $me':'$DIR
cd $DIR

if [ "$#" -lt 1 ]; then
    echo "!!! One argument T_DDK_PATH is required."
	echo "e.g." 
    echo $0" /home/jackfan/Work/downloadbin/t-ddk-r10"
	echo $0" /home/jackfan/Work/downloadbin/Trustonic_ZQ20170621/t-ddk-r3"
	exit
fi

BUILD_CONTINUE=false
if [ "$#" -gt 1 ]; then
    BUILD_CONTINUE=$2
fi
echo BUILD_CONTINUE=$BUILD_CONTINUE
if [ "$2" == "clean" ]; then
    cd $T_DDK_PATH
    rm -rf SM32_TA
    echo "... clean done ..."
    exit
fi

T_DDK_PATH="$( cd $1 && pwd)"
ETS_SOURCE_PATH=$DIR/../../..

cd $T_DDK_PATH
if [ $BUILD_CONTINUE == "continue" ]; then
    cd SM32_TA
    rsync -v $ETS_SOURCE_PATH/sdk/project/trustonic/TA/egis/makefile.mk ./Locals/Code/
    rsync -avzh $ETS_SOURCE_PATH/sdk/project/trustonic/TA/Locals/Build/ ./Locals/Build/
    rsync -avzh $ETS_SOURCE_PATH/sdk/project/trustonic/TA/Locals/Code/public/ ./Locals/Code/public/
    rsync -avzh $ETS_SOURCE_PATH/sdk/project/trustonic/TA/Locals/Code/libs/ ./Locals/Code/libs/
    
    # read -n1 -r -p "Press space to continue..." tmpkey
else
    rm -rf SM32_TA
    mkdir -p SM32_TA/Locals/Build
    mkdir -p SM32_TA/Locals/Code

    cd SM32_TA
    cp -v $ETS_SOURCE_PATH/sdk/project/trustonic/TA/egis/makefile.mk ./Locals/Code/
    cp -v $ETS_SOURCE_PATH/sdk/project/trustonic/TA/Locals/Build/* ./Locals/Build/
    cp -v $ETS_SOURCE_PATH/sdk/project/trustonic/TA/Locals/Code/*.c ./Locals/Code/
    cp -v $ETS_SOURCE_PATH/sdk/project/trustonic/TA/Locals/Code/*.h ./Locals/Code/
    cp -afpR $ETS_SOURCE_PATH/sdk/project/trustonic/TA/Locals/Code/public ./Locals/Code/
    cp -afpR $ETS_SOURCE_PATH/sdk/project/trustonic/TA/Locals/Code/libs ./Locals/Code/
    ln -s $ETS_SOURCE_PATH/sdk ./Locals/Code/sdk
fi


cd ./Locals/Build
./build.sh



