/*
 * Copyright (C) 2019-2021, Nanjing Egistec Technology Co., Ltd.
 * All Rights Reserved.
 */

#include "TlApiDriver.h"
#include "tlStd.h"

#include "egis_alipay_api.h"
#include "egis_dr.h"
#include "egis_dr_common.h"
#include "egis_error.h"
#define LOG_TAG "[egis_alipay_api] "

#define TEE_SUCCESS ((TEE_Result)0x00000000)
#define TEE_ERROR_BAD_PARAMETERS ((TEE_Result)0xFFFF0006)
#define TEE_ERROR_TIMEOUT ((TEE_Result)0xFFFF3001)

TEE_Result egis_alipay_api_get_auth_id(int32_t* finger_id) {
    TEE_Result err = TEE_SUCCESS;

    do {
        if (NULL == finger_id) {
            err = TEE_ERROR_BAD_PARAMETERS;
            break;
        }

        egis_dr_cmd_t cmd = {0};
        cmd.cmd_id = EGIS_DR_CMD_ALIPAY_API_GET_AUTH_ID;

        tlApiResult_t ret = tlApi_callDriver(DRIVER_ID, &cmd);
        if (ret != TLAPI_OK) {
            tlApiLogPrintf(LOG_TAG "[%s]: tlApi_callDriver fails, error=0x%X\n", __func__, ret);
            err = TEE_ERROR_TIMEOUT;
            break;
        }

        uint64_t current_time = 0;
        egis_dr_get_timestamp(&current_time);
        if (current_time - cmd.data.get_auth_id.time < 1000 * 1000) {
            *finger_id = cmd.data.get_auth_id.finger_id;
        } else {
            err = TEE_ERROR_TIMEOUT;
        }
    } while (0);

    return err;
}
