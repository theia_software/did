/*
 * Copyright (C) 2019-2021, Nanjing Egistec Technology Co., Ltd.
 * All Rights Reserved.
 */

#ifndef _IFAA_EGIS_INTERFACE_H_
#define _IFAA_EGIS_INTERFACE_H_

#include "ifaa_ta_common.h"

#ifdef __cplusplus__
extern C {
#endif  //  __cplusplus__

#ifndef MAX_FINGERS_PER_USER
#define MAX_FINGERS_PER_USER 5  // previous the max finger count;
#endif

#ifndef MIN_GET_ID_LIST_BUFFER_LENGTH
#define MIN_GET_ID_LIST_BUFFER_LENGTH ((sizeof(uint32_t) + sizeof(int32_t)) * MAX_FINGERS_PER_USER)
#endif

#ifndef TA_VERSION_INFO_LEN
#define TA_VERSION_INFO_LEN 64  // max authenticator version length
#endif

#ifndef FINGER_ID_LENGTH
#define FINGER_ID_LENGTH sizeof(int32_t)
#endif

    IFAA_Result ifaa_egis_getAuthenticatorVersion(int32_t * version);

    IFAA_Result ifaa_egis_getFpLastIdentifiedResult(uint8_t * pIdBuf, uint32_t * pBufLen);

    IFAA_Result ifaa_egis_getIdList(uint8_t * pIdListBuf, uint32_t * pBufLen);

    IFAABoolean ifaa_egis_compareId(const uint8_t* pBuf_l, const uint8_t* pBuf_r, uint32_t buf_len);

#ifdef __cplusplus__
}
#endif

#endif