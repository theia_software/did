/*
 * Copyright (C) 2019-2021, Nanjing Egistec Technology Co., Ltd.
 * All Rights Reserved.
 */

#ifndef _EGIS_ERROR_H_
#define _EGIS_ERROR_H_

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif  // #ifdef __cplusplus

#define EGIS_ERROR_BREAK(err)        \
    {                                \
        if (EGIS_SUCCESS != (err)) { \
            break;                   \
        }                            \
    }

typedef enum egis_error {
    EGIS_SUCCESS = 0,

    // ta error
    EGIS_ERROR_BASE = 1000,
    EGIS_ERROR_BAD_PARAMS,
    EGIS_ERROR_GENERIC,
} egis_error_t;

typedef struct {
    egis_error_t err;
    const char* strerror;
} egis_strerror_t;

extern egis_strerror_t err_table[];

const char* egis_strerror(const egis_error_t err);

uint32_t egis_get_err_table_len(void);

#ifdef __cplusplus
}
#endif  // #ifdef __cplusplus

#endif  // _EGIS_ERROR_H_
