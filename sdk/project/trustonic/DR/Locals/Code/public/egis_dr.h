/*
 * Copyright (C) 2019-2021, Nanjing Egistec Co., Ltd.
 * All Rights Reserved.
 */

#ifndef _EGIS_DRIVER_H_
#define _EGIS_DRIVER_H_

#include "egis_error.h"

egis_error_t egis_dr_get_timestamp(uint64_t* timestamp);

egis_error_t egis_dr_sync_auth_result(uint32_t finger_id, uint64_t time);

egis_error_t egis_dr_sync_finger_list(uint32_t* fingerIDs, uint32_t count);

egis_error_t egis_dr_sync_authenticator_version(uint32_t version);

#endif
