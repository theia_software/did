/*
 * Copyright (C) 2019-2021, NanJing Egistec Technology Co., Ltd.
 * All Rights Reserved.
 */

#ifndef _EGIS_ALIPAY_API_H_
#define _EGIS_ALIPAY_API_H_
typedef uint32_t TEE_Result;
extern TEE_Result EGIS_alipay_api_get_auth_id(int32_t* finger_id);

#endif  // __EGIS_ALIPAY_API_H__
