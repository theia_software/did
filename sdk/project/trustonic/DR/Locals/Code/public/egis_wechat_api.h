/*
 * Copyright (C) 2019-2021, Nanjing Egistec Technology Co., Ltd.
 * All Rights Reserved.
 */

#ifndef _EGIS_WECHAT_API_H_
#define _EGIS_WECHAT_API_H_

// WeChat Pay
extern TEE_Result egis_tlApiFpGetFingerId(int32_t* finger_id);

#endif
