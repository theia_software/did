/*
 * Copyright (C) 2019-2021, Nanjing Egistec Technology Co., Ltd.
 * All Rights Reserved.
 */

#ifndef _EGIS_DR_IPCH_H_
#define _EGIS_DR_IPCH_H_

#include "DrApi/DrApi.h"
#include "DrApi/DrApiError.h"

#if TBASE_API_LEVEL >= 5
#define drUtilsUnmapTaskBuffers(clientTaskId) drApiUnmapTaskBuffers((clientTaskId))
#else
#define drUtilsUnmapTaskBuffers(clientTaskId)
#endif

void* drUtilsMapTaskBuffer(const taskid_t taskId, const void* startVirtClient, const size_t len,
                           const uint32_t attr);

extern drApiResult_t egis_dr_invoke_cmd_entry_point(egis_dr_cmd_t* cmd, threadid_t ipcClient);

#endif
