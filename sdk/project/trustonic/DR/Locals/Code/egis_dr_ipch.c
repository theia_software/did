/*
 * Copyright (C) 2019-2021, Nanjing Egistec Technology Co., Ltd.
 * All Rights Reserved.
 */

#include <string.h>
#include "DrApi/DrApi.h"
#include "drStd.h"

#include "egis_dr_common.h"
#include "egis_dr_ipch.h"

#define LOG_TAG "[egis_dr_ipch]"

static uint32_t g_auth_finger_id = 0;  // last_auth_finger_id
static uint64_t g_auth_time = 0;       // last_auth_time

static uint32_t m_finger_list[MAX_FINGER_COUNT];  // finger_list
static uint32_t m_current_finger_count = 0;       // current_finger_count
static uint32_t m_auth_version = 0;               // auth_version

drApiResult_t egis_dr_invoke_cmd_entry_point(egis_dr_cmd_t* cmd, threadid_t ipcClient) {
    if (NULL == cmd) {
        return E_DRAPI_INVALID_PARAMETER;
    }

    drApiResult_t ret = DRAPI_OK;

    switch (cmd->cmd_id) {
        case EGIS_DR_CMD_GET_TIMESTAMP: {
            ret = drApiGetSecureTimestamp(&cmd->data.get_timestamp.timestamp);
            break;
        }
        case EGIS_DR_CMD_ALIPAY_API_GET_AUTH_ID: {
            cmd->data.get_auth_id.finger_id = g_auth_finger_id;
            cmd->data.get_auth_id.time = g_auth_time;
            break;
        }
        case EGIS_DR_CMD_SYNC_AUTH_RESULT: {
            g_auth_finger_id = cmd->data.auth_result.finger_id;
            g_auth_time = cmd->data.auth_result.time;
            break;
        }
        case EGIS_DR_CMD_WECHAT_API_GET_AUTH_ID: {
            cmd->data.wechat_get_auth_id.finger_id = g_auth_finger_id;
            cmd->data.wechat_get_auth_id.time = g_auth_time;
            break;
        }
        case EGIS_DR_CMD_SYNC_FINGER_LIST: {
            if (cmd->data.sync_finger_list.ids != NULL) {
                uint32_t min_size = cmd->data.sync_finger_list.count > MAX_FINGER_COUNT
                                        ? MAX_FINGER_COUNT
                                        : cmd->data.sync_finger_list.count;

                memset(m_finger_list, 0, sizeof(m_finger_list));
                memcpy(m_finger_list, cmd->data.sync_finger_list.ids, min_size * sizeof(uint32_t));

                m_current_finger_count = min_size;
            }

            break;
        }
        case EGIS_DR_CMD_IFAA_GET_FINGER_LIST: {
            if ((cmd->data.get_finger_list.ids != NULL) && (m_current_finger_count > 0)) {
                memcpy(cmd->data.get_finger_list.ids, m_finger_list,
                       m_current_finger_count * sizeof(uint32_t));
                cmd->data.get_finger_list.count = m_current_finger_count;
            }

            break;
        }
        case EGIS_DR_CMD_SYNC_AUTHENTICATOR_VERSION: {
            m_auth_version = cmd->data.sync_authenticator_version.version;
            break;
        }

        case EGIS_DR_CMD_IFAA_GET_AUTHENTICATOR_VERSION: {
            cmd->data.get_authenticator_version.version = m_auth_version;
            break;
        }
        default: { break; }
    }

    return ret;
}
