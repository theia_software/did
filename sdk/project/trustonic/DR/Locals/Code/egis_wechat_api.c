/*
 * Copyright (C) 2019-2021, Nanjing Egistec Technology Co., Ltd.
 * All Rights Reserved.
 */

#include "TlApiDriver.h"
#include "tlStd.h"

#include "egis_dr.h"
#include "egis_dr_common.h"
#include "egis_error.h"

typedef uint32_t TEE_Result;
#define TEE_SUCCESS ((TEE_Result)0x00000000)

#define TEE_ERROR_TIMEOUT ((TEE_Result)0xFFFF3001)

#define TEE_ERROR_CALL_DRIVER_FAILED ((TEE_Result)0xFFFF3002)

#define TEE_ERROR_BAD_PARAMETERS ((TEE_Result)0xFFFF0006)

#define MAX_CHECK_TIME (3000 * 1000)

#define LOG_TAG "[egis_wechat_api]"

TEE_Result egis_tlApiFpGetFingerId(int32_t* finger_id) {
    TEE_Result err = TEE_SUCCESS;
    uint64_t current_time = 0;
    do {
        if (NULL == finger_id) {
            err = TEE_ERROR_BAD_PARAMETERS;
            break;
        }

        egis_dr_cmd_t cmd = {0};
        cmd.cmd_id = EGIS_DR_CMD_WECHAT_API_GET_AUTH_ID;
        tlApiResult_t ret = tlApi_callDriver(DRIVER_ID, &cmd);

        if (ret != TLAPI_OK) {
            err = TEE_ERROR_CALL_DRIVER_FAILED;
            break;
        }

        egis_dr_get_timestamp(&current_time);

        if (current_time - cmd.data.wechat_get_auth_id.time > MAX_CHECK_TIME) {
            tlApiLogPrintf(
                LOG_TAG
                "egis_tlApiFpGetFingerId time check fails, curr_time=0x%x, identify_time=0x%x\n",
                current_time, cmd.data.wechat_get_auth_id.time);
            err = TEE_ERROR_TIMEOUT;
            break;
        }

        *finger_id = cmd.data.wechat_get_auth_id.finger_id;
    } while (0);

    return err;
}
