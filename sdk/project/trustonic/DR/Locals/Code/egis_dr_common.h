/*
 * Copyright (C) 2019-2021, Nanjing Egistec Technology Co., Ltd.
 * All Rights Reserved.
 */

#ifndef _EGIS_DR_COMMON_H_
#define _EGIS_DR_COMMON_H_

#define MAX_FINGER_COUNT 6

typedef enum {
    EGIS_DR_CMD_GET_TIMESTAMP = 2000,
    EGIS_DR_CMD_SYNC_AUTH_RESULT,
    EGIS_DR_CMD_ALIPAY_API_GET_AUTH_ID,
    EGIS_DR_CMD_WECHAT_API_GET_AUTH_ID,

    EGIS_DR_CMD_SYNC_FINGER_LIST,
    EGIS_DR_CMD_SYNC_AUTHENTICATOR_VERSION,
    EGIS_DR_CMD_IFAA_GET_FINGER_LIST,
    EGIS_DR_CMD_IFAA_GET_AUTHENTICATOR_VERSION,

} egis_dr_cmd_id_t;

typedef struct {
    uint64_t timestamp;
} egis_dr_get_timestamp_t;

typedef struct {
    uint32_t finger_id;
    uint64_t time;
} egis_dr_sync_auth_result_t;

typedef struct {
    uint32_t finger_id;
    uint64_t time;
} egis_alipay_api_get_auth_id_t;

typedef struct {
    uint32_t ids[MAX_FINGER_COUNT];
    uint32_t count;
} egis_dr_sync_finger_list_t;

typedef struct {
    uint32_t finger_id;
    uint64_t time;
} egis_wechat_api_get_auth_id_t;

typedef struct {
    uint32_t version;
} egis_dr_sync_authenticator_version_t;

typedef struct {
    uint32_t version;
} egis_ifaa_api_get_authenticator_version_t;

typedef struct {
    uint32_t ids[MAX_FINGER_COUNT];
    uint32_t count;
} egis_ifaa_api_get_get_finger_list_t;

typedef struct {
    egis_dr_cmd_id_t cmd_id;
    union {
        egis_dr_get_timestamp_t get_timestamp;

        egis_dr_sync_auth_result_t auth_result;

        egis_dr_sync_finger_list_t sync_finger_list;
        egis_dr_sync_authenticator_version_t sync_authenticator_version;

        egis_alipay_api_get_auth_id_t get_auth_id;
        egis_wechat_api_get_auth_id_t wechat_get_auth_id;

        egis_ifaa_api_get_get_finger_list_t get_finger_list;
        egis_ifaa_api_get_authenticator_version_t get_authenticator_version;
    } data;
} egis_dr_cmd_t;

#endif
