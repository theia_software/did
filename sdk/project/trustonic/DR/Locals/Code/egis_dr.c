/*
 * Copyright (C) 2019-2021, Nanjing Egistec Technology Co., Ltd.
 * All Rights Reserved.
 */

#include "egis_dr.h"
#include "TlApiDriver.h"
#include "egis_dr_common.h"
#include "egis_error.h"
#include "tlStd.h"

#define LOG_TAG "[egis_dr] "

egis_error_t egis_dr_get_timestamp(uint64_t* timestamp) {
    egis_error_t err = EGIS_SUCCESS;

    do {
        if (NULL == timestamp) {
            err = EGIS_ERROR_BAD_PARAMS;
            break;
        }

        egis_dr_cmd_t cmd = {0};
        cmd.cmd_id = EGIS_DR_CMD_GET_TIMESTAMP;

        tlApiResult_t ret = tlApi_callDriver(DRIVER_ID, &cmd);
        if (ret != TLAPI_OK) {
            tlApiLogPrintf(LOG_TAG "egis_dr_get_timestamp tlApi_callDriver fails, error=0x%X\n",
                           ret);
            err = EGIS_ERROR_GENERIC;
            break;
        }

        *timestamp = cmd.data.get_timestamp.timestamp;
    } while (0);

    return err;
}

egis_error_t egis_dr_sync_auth_result(uint32_t finger_id, uint64_t time) {
    egis_error_t err = EGIS_SUCCESS;

    do {
        egis_dr_cmd_t cmd = {0};
        cmd.cmd_id = EGIS_DR_CMD_SYNC_AUTH_RESULT;
        cmd.data.auth_result.finger_id = finger_id;
        cmd.data.auth_result.time = time;

        tlApiResult_t ret = tlApi_callDriver(DRIVER_ID, &cmd);
        if (ret != TLAPI_OK) {
            tlApiLogPrintf(LOG_TAG "egis_dr_sync_auth_result tlApi_callDriver fails, error=0x%X\n",
                           ret);
            err = EGIS_ERROR_GENERIC;
            break;
        }
    } while (0);

    return err;
}

egis_error_t egis_dr_sync_finger_list(uint32_t* fingerIDs, uint32_t count) {
    egis_error_t err = EGIS_SUCCESS;
    uint32_t min_size = count > MAX_FINGER_COUNT ? MAX_FINGER_COUNT : count;

    do {
        egis_dr_cmd_t cmd = {0};
        cmd.cmd_id = EGIS_DR_CMD_SYNC_FINGER_LIST;
        memcpy(cmd.data.sync_finger_list.ids, fingerIDs, min_size * sizeof(uint32_t));
        cmd.data.sync_finger_list.count = min_size;

        tlApiResult_t ret = tlApi_callDriver(DRIVER_ID, &cmd);
        if (ret != TLAPI_OK) {
            tlApiLogPrintf(LOG_TAG "egis_dr_sync_finger_list tlApi_callDriver fails, error=0x%X\n",
                           ret);
            err = EGIS_ERROR_GENERIC;
            break;
        }
    } while (0);

    return err;
}

egis_error_t egis_dr_sync_authenticator_version(uint32_t version) {
    egis_error_t err = EGIS_SUCCESS;

    do {
        egis_dr_cmd_t cmd = {0};
        cmd.cmd_id = EGIS_DR_CMD_SYNC_AUTHENTICATOR_VERSION;
        cmd.data.sync_authenticator_version.version = version;
        tlApiResult_t ret = tlApi_callDriver(DRIVER_ID, &cmd);

        if (ret != TLAPI_OK) {
            tlApiLogPrintf(
                LOG_TAG "egis_dr_sync_authenticator_version tlApi_callDriver fails, error=0x%X\n",
                ret);
            err = EGIS_ERROR_GENERIC;
            break;
        }
    } while (0);

    return err;
}