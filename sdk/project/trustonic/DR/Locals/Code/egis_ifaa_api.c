/*
 * Copyright (C) 2019-2021, Nanjing Egistec Technology Co., Ltd.
 * All Rights Reserved.
 */

#include "TlApiDriver.h"
#include "egis_dr.h"
#include "egis_dr_common.h"
#include "egis_error.h"
#include "ifaa_egis_interface.h"
#include "tlStd.h"

typedef uint32_t TEE_Result;

extern TEE_Result egis_alipay_api_get_auth_id(int32_t* finger_id);

#define TEE_SUCCESS ((TEE_Result)0x00000000)

IFAABoolean ifaa_egis_compareId(const uint8_t* pBuf_l, const uint8_t* pBuf_r, uint32_t buf_len) {
    uint32_t i = 0;
    IFAABoolean ret = IFAA_YES;

    do {
        if (NULL == pBuf_l || NULL == pBuf_r || buf_len != sizeof(int32_t)) {
            ret = IFAA_NO;
            break;
        }

        while (i < buf_len) {
            if (*(pBuf_l + i) != *(pBuf_r + i)) {
                ret = IFAA_NO;
                break;
            }

            i++;
        }
    } while (0);

    return ret;
}

IFAA_Result ifaa_egis_getFpLastIdentifiedResult(uint8_t* pIdBuf, uint32_t* pBufLen) {
    IFAA_Result ret = IFAA_ERR_SUCCESS;
    int32_t id = 0;

    do {
        TEE_Result teeResult;
        if (NULL == pIdBuf) {
            ret = IFAA_ERR_BAD_PARAM;
            break;
        }

        if (NULL == pBufLen || *pBufLen < sizeof(int32_t)) {
            ret = IFAA_ERR_BAD_PARAM;
            break;
        }

        teeResult = egis_alipay_api_get_auth_id(&id);
        if (TEE_SUCCESS != teeResult) {
            ret = IFAA_ERR_GET_LAST_IDENTIFIED_RESULT;
            break;
        }

        memcpy(pIdBuf, &id, sizeof(int32_t));
        *pBufLen = sizeof(int32_t);
    } while (0);

    return ret;
}

IFAA_Result ifaa_egis_getIdList(uint8_t* pIdListBuf, uint32_t* pBufLen) {
    IFAA_Result ret = IFAA_ERR_SUCCESS;
    uint32_t i = 0;
    uint32_t len = sizeof(uint32_t);

    do {
        uint32_t* temp = NULL;
        if (NULL == pIdListBuf || NULL == pBufLen) {
            ret = IFAA_ERR_BAD_PARAM;
            break;
        }

        egis_dr_cmd_t cmd = {0};
        cmd.cmd_id = EGIS_DR_CMD_IFAA_GET_FINGER_LIST;
        tlApiResult_t tlResult = tlApi_callDriver(DRIVER_ID, &cmd);

        if (tlResult != TLAPI_OK) {
            ret = IFAA_ERR_BAD_PARAM;
            break;
        }

        ret = IFAA_ERR_SUCCESS;
        temp = (uint32_t*)cmd.data.get_finger_list.ids;

        while (i < cmd.data.get_finger_list.count) {
            /* one fingerID length */
            memcpy(pIdListBuf, &len, sizeof(uint32_t));
            pIdListBuf += sizeof(uint32_t);

            /* fingerID */
            memcpy(pIdListBuf, temp + i, sizeof(uint32_t));
            pIdListBuf += sizeof(uint32_t);
            i++;
        }

        *pBufLen = cmd.data.get_finger_list.count * sizeof(uint32_t) * 2;
    } while (0);

    return ret;
}

IFAA_Result ifaa_egis_getAuthenticatorVersion(int32_t* version) {
    IFAA_Result ret = IFAA_ERR_SUCCESS;

    do {
        if (NULL == version) {
            ret = IFAA_ERR_BAD_PARAM;
            break;
        }

        egis_dr_cmd_t cmd = {0};
        cmd.cmd_id = EGIS_DR_CMD_IFAA_GET_AUTHENTICATOR_VERSION;
        tlApiResult_t tlResult = tlApi_callDriver(DRIVER_ID, &cmd);

        if (tlResult != TLAPI_OK) {
            ret = IFAA_ERR_BAD_PARAM;
            break;
        }

        ret = IFAA_ERR_SUCCESS;
        *version = cmd.data.get_authenticator_version.version;
    } while (0);

    return ret;
}