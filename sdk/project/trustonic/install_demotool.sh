#!/bin/bash
adb root
if [ $? -ne 0 ]; then
    echo ''
    echo '!!!  adb root FAILED  !!!'
    echo ''
    exit 12
fi
adb wait-for-device
adb remount

adb shell rm -rf /data/fpdata
adb shell mkdir /data/fpdata
adb shell chmod 777 /data/fpdata

adb shell rm /system/vendor/app/mcRegistry/08080000000000000000000000000000.tlbin
adb shell rm /system/bin/rbs_daemon
adb shell rm /vendor/lib64/libRbsFlow.so
adb shell rm /system/lib64/libRbsFlow.so

adb push ./TA/08080000000000000000000000000000.tlbin /system/vendor/app/mcRegistry/
adb push ./tools/rbs_daemon /system/bin/
adb push ./flow/for-demotool/libRbsFlow.so /vendor/lib64
adb install -r ./tools/EgisDemoTool*.apk

echo '---'
echo 'Done Time: '$(date +"%T")
echo '---'
echo 'Rebooting ...'
adb reboot
echo 'wait until adb is ready to connect ...'
adb wait-for-device
adb root
adb wait-for-device
adb remount
adb shell setenforce 0

echo 'Run rbs_daemon ...'
adb shell rbs_daemon