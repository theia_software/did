adb root
adb wait-for-device
adb remount

adb shell rm -rf /data/fpdata
adb shell mkdir /data/fpdata
adb shell chmod 777 /data/fpdata

adb shell rm /system/vendor/app/mcRegistry/08080000000000000000000000000000.tlbin
adb shell rm /system/bin/rbs_daemon
adb shell rm /vendor/lib64/libRbsFlow.so
adb shell rm /system/lib64/libRbsFlow.so

adb push ./TA/08080000000000000000000000000000.tlbin /system/vendor/app/mcRegistry/
adb push ./tools/rbs_daemon /system/bin
adb push ./flow/for-demotool/libRbsFlow.so /vendor/lib64
@echo off
setlocal enabledelayedexpansion
for /r %%a in (EgisDemoTool*.apk) do (
	set "s=%%a"
	set "s=!s:%~dp0=!"
	echo,!s!
	adb install -r ./!s!
)

pause

adb reboot
adb wait-for-device
adb root
adb wait-for-device
adb remount
adb shell setenforce 0
adb shell rbs_daemon