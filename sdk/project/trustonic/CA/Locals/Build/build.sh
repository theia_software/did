#!/bin/bash

# Source setup.sh
APP_ABI=arm64-v8a
setup_script=""
search_dir=$(dirname $(readlink -f ${BASH_SOURCE[0]}))
echo search_dir=$search_dir
while [[ ${setup_script} == "" ]]
do
  setup_script=$( find $search_dir -name "setup.sh" )
  search_dir=$(dirname $search_dir)
  echo search_dir=$search_dir
done
if [[ ${setup_script} == "" ]]; then
  echo "ERROR: setup.sh not found"
  exit 1
fi
source $setup_script
t_sdk_root=$( dirname $setup_script )

if [ "$MODE" == "Release" ]; then
  echo -e "Mode\t\t: Release"
  OUT_DIR=Out/Bin/$APP_ABI/Release
  OPTIM=release
else
  echo -e "Mode\t\t: Debug"
  OUT_DIR=Out/Bin/$APP_ABI/Debug
  OPTIM=debug
fi

# go to project root
cd $(dirname $(readlink -f $0))/../..

### ---------------- Generic Build Command ----------------

# run NDK build
${NDK_BUILD} \
    -B \
    NDK_DEBUG=1 \
    NDK_PROJECT_PATH=Locals/Code \
    NDK_APPLICATION_MK=Locals/Code/Application.mk \
    NDK_MODULE_PATH=${t_sdk_root} \
    NDK_APP_OUT=Out/_build \
    APP_BUILD_SCRIPT=Locals/Code/Android_trustonic.mk \
    APP_OPTIM=$OPTIM \
    APP_ABI=$APP_ABI

if [ $? -ne 0 ]; then
    echo "!!! ndk-build failed !!!"
	exit
fi
echo '--'
echo 'Done. Time:'$(date +"%T")
echo '--'
mkdir -p $OUT_DIR

cp -r $PWD/Out/_build/local/$APP_ABI/* $OUT_DIR

echo
echo Output directory of build is $PWD/$OUT_DIR

if [[ $RELEASE_FOLDER != "" ]]; then
  mkdir $RELEASE_FOLDER/TA
	cp -fv $OUT_DIR/libets_trustlet.so $RELEASE_FOLDER/TA/
  cp -fv $OUT_DIR/libMcClient.so $RELEASE_FOLDER/TA/
	exit
fi

adb root
if [ $? -ne 0 ]; then
    echo "!!! adb root - failed !!!"
	exit
fi

adb remount
adb shell setenforce 0
adb shell chmod 777 /dev/esfp0
adb push $OUT_DIR/libets_trustlet.so /system/lib64/
adb push $OUT_DIR/libMcClient.so /system/lib64/


