#ifndef __TRUSTLET_COMM_H_
#define __TRUSTLET_COMM_H_

#include "MobiCoreDriverApi.h"
#define LOG_TAG "RBS-TRUSTLET"
#include "log.h"

#ifdef __cplusplus
extern "C" {
#endif

uint32_t trustlet_start_app(uint8_t* pTAData, const uint32_t nTASize, const uint32_t buffer_size);
uint32_t trustlet_send_cmd_req(void* transfer_data, const int32_t transfer_data_len);
uint32_t trustlet_mcMap(void* chunk_data, const int32_t chunk_data_len, void** bulk_map);
void trustlet_mcUnmap(void* chunk_data, void* bulk_map);
uint32_t trustlet_shutdown_app(void);

#ifdef __cplusplus
}
#endif

#endif
