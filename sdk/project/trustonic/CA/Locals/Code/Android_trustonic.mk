# =============================================================================
#
# =============================================================================

# Do not remove this - Android build needs the definition
LOCAL_PATH	:= $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE	:= ets_trustlet

LOCAL_C_INCLUDES := $(LOCAL_PATH)

# Add new source files here
LOCAL_SRC_FILES	+= trustlet_comm.c
ifeq ($(BUILD_TDDK_R10),true)
LOCAL_SHARED_LIBRARIES := libMcClient
else
LOCAL_SHARED_LIBRARIES := MobiCoreDriver
include $(COMP_PATH_Logwrapper)/Android.mk
endif

include $(BUILD_SHARED_LIBRARY)

# adding the root folder to the search path appears to make absolute paths
# work for import-module - lets see how long this works and what surprises
# future developers get from this.
$(call import-add-path,/)
ifeq ($(BUILD_TDDK_R10),true)
$(call import-module,$(COMP_PATH_MobiCoreClientLib_module))
else
$(call import-module,$(COMP_PATH_MobiCoreDriverLib))
endif
