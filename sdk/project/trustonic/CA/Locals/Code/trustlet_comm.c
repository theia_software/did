#include "trustlet_comm.h"

#ifndef LOG_D
#define LOG_D(...) __android_log_print(ANDROID_LOG_DEBUG, LOG_TAG, __VA_ARGS__)
#endif

static const uint32_t DEVICE_ID = MC_DEVICE_ID_DEFAULT;
mcSessionHandle_t g_sessionHandle;
void* g_buffer_data;

uint32_t trustlet_start_app(uint8_t* pTAData, const uint32_t nTASize, const uint32_t buffer_size) {
    LOG_I("%s start", __func__);
    mcResult_t mcRet;
    mcVersionInfo_t versionInfo;

    LOG_D("Opening <t-base device.");
    mcRet = mcOpenDevice(DEVICE_ID);
    if (MC_DRV_OK != mcRet) {
        LOG_E("Error opening device: %d.", mcRet);
        return mcRet;
    }

    mcRet = mcGetMobiCoreVersion(MC_DEVICE_ID_DEFAULT, &versionInfo);
    if (MC_DRV_OK != mcRet) {
        LOG_E("mcGetMobiCoreVersion failed %d.", mcRet);
        mcCloseDevice(DEVICE_ID);
        return mcRet;
    }
    LOG_D("productId        = %s", versionInfo.productId);
    LOG_D("versionMci       = 0x%08X", versionInfo.versionMci);
    LOG_D("versionSo        = 0x%08X", versionInfo.versionSo);
    LOG_D("versionMclf      = 0x%08X", versionInfo.versionMclf);
    LOG_D("versionContainer = 0x%08X", versionInfo.versionContainer);
    LOG_D("versionMcConfig  = 0x%08X", versionInfo.versionMcConfig);
    LOG_D("versionTlApi     = 0x%08X", versionInfo.versionTlApi);
    LOG_D("versionDrApi     = 0x%08X", versionInfo.versionDrApi);
    LOG_D("versionCmp       = 0x%08X", versionInfo.versionCmp);

    g_buffer_data = malloc(buffer_size);
    if (g_buffer_data == NULL) {
        LOG_E("Allocation of TCI failed.");
        mcCloseDevice(DEVICE_ID);
        return MC_DRV_ERR_NO_FREE_MEMORY;
    }
    memset(g_buffer_data, 0, buffer_size);

    LOG_I("Opening the session.");
    memset(&g_sessionHandle, 0, sizeof(mcSessionHandle_t));
    // The device ID (default device is used).
    g_sessionHandle.deviceId = DEVICE_ID;
    // LOG_I_BUF("tlcSampleRot13 Trusted Application:", pTAData, nTASize);
    mcRet = mcOpenTrustlet(&g_sessionHandle, MC_SPID_TRUSTONIC_OTA, pTAData, nTASize,
                           (uint8_t*)g_buffer_data, buffer_size);

    if (MC_DRV_OK != mcRet) {
        LOG_E("Open session failed: %d.", mcRet);
        free(g_buffer_data);
        g_buffer_data = NULL;
        mcCloseDevice(DEVICE_ID);
    } else {
        LOG_I("open() succeeded.");
    }
    return mcRet;
}

uint32_t trustlet_send_cmd_req(void* transfer_data, const int32_t transfer_data_len) {
    LOG_D("%s start. transfer_data_len=%d.", __func__, transfer_data_len);
    mcResult_t mcRet;
    memcpy(g_buffer_data, transfer_data, transfer_data_len);
    LOG_D("Notifying the Trusted Application");
    mcRet = mcNotify(&g_sessionHandle);
    if (MC_DRV_OK != mcRet) {
        LOG_E("Notify failed: %d", mcRet);
        goto exit;
    }

    LOG_D("Waiting for the Trusted Application response");
    mcRet = mcWaitNotification(&g_sessionHandle, -1);
    if (MC_DRV_OK != mcRet) {
        LOG_E("Wait for response notification failed: %d", mcRet);
        goto exit;
    }
exit:
    memcpy(transfer_data, g_buffer_data, transfer_data_len);
    return mcRet;
}

uint32_t trustlet_mcMap(void* chunk_data, const int32_t chunk_data_len, void** bulk_map) {
    LOG_D("trustlet_mcMap start. chunk_data_len=%d", chunk_data_len);
    mcResult_t mcRet;
    *bulk_map = malloc(sizeof(mcBulkMap_t));
    // alloc shared memory
    if (chunk_data != NULL && chunk_data_len > 0) {
        mcRet = mcMap(&g_sessionHandle, chunk_data, chunk_data_len, *bulk_map);
        if (MC_DRV_OK != mcRet) {
            LOG_E("mcMap failed: %d", mcRet);
            free(*bulk_map);
        }
    }
    return mcRet;
}

void trustlet_mcUnmap(void* chunk_data, void* bulk_map) {
    LOG_I("%s start", __func__);
    (void)mcUnmap(&g_sessionHandle, chunk_data, bulk_map);
    free(bulk_map);
}

uint32_t trustlet_shutdown_app(void) {
    LOG_I("%s start", __func__);
    mcResult_t mcRet;

    LOG_D("Closing the session.");
    mcRet = mcCloseSession(&g_sessionHandle);
    if (MC_DRV_OK != mcRet) {
        LOG_E("Closing session failed: %d.", mcRet);
        // Continue even in case of error.
    }

    LOG_D("Closing <t-base device.");
    mcRet = mcCloseDevice(DEVICE_ID);
    if (MC_DRV_OK != mcRet) {
        LOG_E("Closing <t-base device failed: %d.", mcRet);
        // Continue even in case of error.
    }
    if (g_buffer_data != NULL) {
        free(g_buffer_data);
        g_buffer_data = NULL;
    }
    return mcRet;
}
