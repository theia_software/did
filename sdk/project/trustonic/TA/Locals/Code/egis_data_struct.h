
#ifndef __EGIS_DATA_STRUCT_H__
#define __EGIS_DATA_STRUCT_H__

typedef struct {
    unsigned char* in_data;
    unsigned int in_data_len;
    unsigned char* out_data;
    unsigned int out_data_len;
} EGIS_DATA_T;

typedef struct {
    unsigned int in_data;
    unsigned int in_data_len;
    unsigned int out_data;
    unsigned int out_data_len;
} EGIS_MESSAGE_T;

#endif
