/*
 * Copyright (c) 2013 TRUSTONIC LIMITED
 * All rights reserved
 *
 * The present software is the confidential and proprietary information of
 * TRUSTONIC LIMITED. You shall not disclose the present software and shall
 * use it only in accordance with the terms of the license agreement you
 * entered into with TRUSTONIC LIMITED. This software may be subject to
 * export or import laws in certain countries.
 */
// clang-format off
#include "tlStd.h"
#include "TlApi/TlApi.h"
// clang-format on

#include "command_handler.h"
#include "common_definition.h"
#include "egis_data_struct.h"
#include "inline_handler.h"
#include "plat_mem.h"

DECLARE_TRUSTED_APPLICATION_MAIN_STACK(6145728)

/**
 * TA entry.
 */
_TLAPI_ENTRY void tlMain(const addr_t tciBuffer, const uint32_t tciBufferLen) {
    unsigned int ret;
    tlDbgPrintf("tlMain start\n");

    // Show tbase version.
    uint32_t tlApiVersion;
    mcVersionInfo_t versionInfo;
    // uint8_t* cmdData;

    ret = tlApiGetVersion(&tlApiVersion);
    if (TLAPI_OK != ret) {
        tlApiLogPrintf(
            "TA tlSampleEgis: Error, tlApiGetVersion failed with "
            "ret=0x%08X, exit.\n",
            ret);
        tlApiExit(ret);
    }
    tlApiLogPrintf("tlApi version    =  0x%08X\n", tlApiVersion);

    ret = tlApiGetMobicoreVersion(&versionInfo);
    if (TLAPI_OK != ret) {
        tlApiLogPrintf(
            "TA tlSampleEgis: Error, tlApiGetMobicoreVersion "
            "failed with ret=0x%08X, exit.\n",
            ret);
        tlApiExit(ret);
    }
    tlApiLogPrintf("productId        = %s\n", versionInfo.productId);
    tlApiLogPrintf("versionMci       = 0x%08X\n", versionInfo.versionMci);
    tlApiLogPrintf("versionSo        = 0x%08X\n", versionInfo.versionSo);
    tlApiLogPrintf("versionMclf      = 0x%08X\n", versionInfo.versionMclf);
    tlApiLogPrintf("versionContainer = 0x%08X\n", versionInfo.versionContainer);
    tlApiLogPrintf("versionMcConfig  = 0x%08X\n", versionInfo.versionMcConfig);
    tlApiLogPrintf("versionTlApi     = 0x%08X\n", versionInfo.versionTlApi);
    tlApiLogPrintf("versionDrApi     = 0x%08X\n", versionInfo.versionDrApi);
    tlApiLogPrintf("versionCmp       = 0x%08X\n", versionInfo.versionCmp);

    // Check if the size of the given TCI is sufficient.
    if ((NULL == tciBuffer) || (sizeof(EGIS_MESSAGE_T) > tciBufferLen)) {
        tlApiLogPrintf("TA tlSampleEgis: Error, invalid TCI size.\n");
        tlApiLogPrintf("TCI buffer: %x.\n", tciBuffer);
        tlApiLogPrintf("TCI buffer length: %d.\n", tciBufferLen);
        // tlApiExit(EXIT_ERROR);
    }

    EGIS_MESSAGE_T* message = (EGIS_MESSAGE_T*)tciBuffer;
    // TA main loop running infinitely.
    for (;;) {
        // Wait for a notification to arrive
        // (INFINITE timeout is recommended -> not polling!)
        tlApiLogPrintf("[egis] TA start wait notify V1.2\n");
        tlApiWaitNotification(TLAPI_INFINITE_TIMEOUT);

        tlApiLogPrintf("[egis] TA got a message\n");

        tlApiLogPrintf("[egis] TA depack message start\n");

        uint8_t* in_data = (uint8_t*)message->in_data;
        int in_data_len = message->in_data_len;
        tlApiLogPrintf("[egis] TA in_data_len = %d, out_data_len = %d\n", in_data_len,
                       message->out_data_len);

        uint32_t indata = 0;
        uint32_t pid = 0;
        uint32_t cid = 0;
        uint32_t uid = 0;
        uint32_t fid = 0;
        uint32_t in_len = 0;

        uint32_t* regular_data = (uint32_t*)in_data;

        pid = regular_data[0];
        cid = regular_data[1];
        uid = regular_data[2];
        fid = regular_data[3];
        in_len = regular_data[4];

        tlApiLogPrintf("[egis] TA pid = %d\n", pid);
        tlApiLogPrintf("[egis] TA cid = %d\n", cid);
        tlApiLogPrintf("[egis] TA uid = %d\n", uid);
        tlApiLogPrintf("[egis] TA fid = %d\n", fid);
        tlApiLogPrintf("[egis] TA in_len = %x\n", in_len);

        tlApiLogPrintf("[egis] TA depack message end\n");

        if (in_data != NULL && in_data_len > 0) {
            if (!tlApiIsNwdBufferValid(in_data, in_data_len)) {
                tlDbgPrintf(
                    "call failed due to invalid buffer "
                    "RET_ERR_INVALID_BUFFERS, exit\n");
                continue;
            }
        }

        // Process command message.
        uint8_t* out_data = NULL;
        int out_data_len = message->out_data_len - sizeof(uint32_t);

        if (out_data_len > 0) {
            out_data_len = message->out_data_len - sizeof(uint32_t);
            out_data = mem_alloc(out_data_len);
            if (out_data == NULL) {
                tlApiLogPrintf("[egis] TA alloc fail, cid = %d\n", cid);
                goto NOTIFY;
            }
            mem_set(out_data, 0, out_data_len);
        }

        int retval = -1;

        if (pid == PID_COMMAND) {
            retval = command_handler(in_data, in_data_len, out_data, &out_data_len);
        } else if (pid == PID_INLINETOOL) {
            retval = inline_handler(in_data, in_data_len, out_data, &out_data_len);
        }

        tlApiLogPrintf("[egis] TA cid = %d, ret = %d\n", cid, retval);
        tlApiLogPrintf("[egis] out_data_len: %d\n", out_data_len);

        mem_set(in_data, 0, message->in_data_len);
        mem_move(in_data, &retval, sizeof(int));

        if (out_data_len > 0 && out_data != NULL) {
            mem_move(in_data + sizeof(int), out_data, out_data_len);
            message->out_data_len = out_data_len + sizeof(int);
            mem_free(out_data);
            out_data = NULL;
        }

    // Notify back the TLC
    NOTIFY:
        tlApiNotify();
    }
}
