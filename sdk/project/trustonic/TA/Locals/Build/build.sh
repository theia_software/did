#!/bin/bash
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
me=`basename $0`
echo $me':'$DIR


# Source setup.sh
setup_script=""
search_dir=$(dirname $(readlink -f ${BASH_SOURCE[0]}))
while [[ ${setup_script} == "" ]]
do
  setup_script=$( find $search_dir -name "setup.sh" )
  search_dir=$(dirname $search_dir)
done
if [[ ${setup_script} == "" ]]; then
  echo "ERROR: setup.sh not found"
  exit 1
fi
source $setup_script

cd $(dirname $(readlink -f $0))
cd ../..

if [ ! -d Locals ]; then
  exit 1
fi

export TLSDK_DIR_SRC=${COMP_PATH_TlSdk}
export TLSDK_DIR=${COMP_PATH_TlSdk}
export TASDK_DIR_SRC=${COMP_PATH_TlSdk}
export TASDK_DIR=${COMP_PATH_TlSdk}
mkdir -p Out/Public
cp -f \
	Locals/Code/public/* \
	Out/Public/

export SecAlgo=true
echo "export SecAlgo="$SecAlgo 
echo "Running make..."
make -f Locals/Code/makefile.mk "$@"

if [ $? -ne 0 ]; then
    echo "!!!Build failed !!!"
	exit 1
fi

cd Out/Bin/GNU/Debug
TABIN_FILE=08080000000000000000000000000000.tlbin
if [ ! -f $TABIN_FILE ]; then
  echo "!! Not exist "$TABIN_FILE
  exit 2
fi
echo ''
ls -l $TABIN_FILE
echo ''

if [[ $RELEASE_FOLDER != "" ]]; then
  mkdir $RELEASE_FOLDER/TA
	cp -fv $TABIN_FILE $RELEASE_FOLDER/TA/
	exit
fi

adb root
if [ $? -ne 0 ]; then
    echo "!!! adb root - failed !!!"
	exit
fi
adb remount
adb shell setenforce 0
adb shell chmod 777 /dev/esfp0
echo "Remove "$TABIN_FILE" and Push the new one."
adb shell rm -r /system/vendor/app/mcRegistry/$TABIN_FILE
adb push $TABIN_FILE /system/vendor/app/mcRegistry/
cd -
