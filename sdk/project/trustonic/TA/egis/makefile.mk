################################################################################
#
# <t-sdk Sample Rot13 Trusted Application
#
################################################################################


# output binary name without path or extension
OUTPUT_NAME := tlSampleEgis


#-------------------------------------------------------------------------------
# MobiConvert parameters, see manual for details
#-------------------------------------------------------------------------------
TRUSTLET_UUID := 08080000000000000000000000000000
TRUSTLET_MEMTYPE := 2
TRUSTLET_NO_OF_THREADS := 1
TRUSTLET_SERVICE_TYPE := 3
TRUSTLET_KEYFILE := Locals/Build/pairVendorTltSig.pem
TA_UUIDKEYFILE := Locals/Build/pairUUIDKeyFile.pem

# This flag must never be set for a commercial version of the Trusted Application.
TRUSTLET_FLAGS := 4

TRUSTLET_INSTANCES := 1
HEAP_SIZE_INIT := 3145728 	# (3*1024*1024)
HEAP_SIZE_MAX :=  10485760 	# (10*1024*1024)
ifeq ($(TOOLCHAIN),GNU)
   #TRUSTLET_OPTS += -Werror
endif
ifeq ($(TOOLCHAIN),ARM)
   #TRUSTLET_OPTS += --diag_error=warning
endif
#-------------------------------------------------------------------------------

ifeq ($(BUILD_TDDK_R10),true)
TBASE_API_LEVEL:=8
else
TBASE_API_LEVEL:=5
endif
#-------------------------------------------------------------------------------
# C Compiler Flags - Add the condiction defines here
#-------------------------------------------------------------------------------

TRUSTLET_OPTS += \
-D__VIGIS_LOG_D_ \
-DxQSEE \
-D__TRUSTONIC__ \
-DTZ_MODE \
-DTZ_ENV \
-DIGNORE_PERMISSION \
-DTRUSTLET_LINK_TEST \
-DSECURE_OBJECT \
-DG3_MATCHER \
-DxUSE_QM_CHECK_STABLE \
-DxUSE_QTY_FOR_FINGER_DETECT \
-DUSE_QM_FOR_FINGER_DETECT \

#For egislib
TRUSTLET_OPTS += \
-DxMEM_MGR \
-DMEM_ALIGN8 \
-DxMEM_DUMP \
-DMORE_OPTION \
-DMEMORY_SMALL \
-DHW_MUL \
-DDISABLE_MERGE_MAP \
-DRBS \

#For egisfp
TRUSTLET_OPTS += \
-D__LINUX__ \
-DxFOD_CALIBRATION_DCFG \
-DSTEAM_ON_SENSOR \
-DMORE_OPTION \
-DHW_MUL \
-DDISABLE_MERGE_MAP \
-DMEM_ALIGN8 \
-DMEMORY_SMALL \
-DDEVICE_DRIVER_HAS_SPI \
-DxNON_SENSOR_TEST \
-DxPOLLING \
-DxMEM_DUMP \
-Dx_SIZE_T \
-DISFPIMAGE_MEDIUM_FILTER \
-DINI_HASH_MAP \
-DxTRUSTONIC_AES_KEY \
-D__ET5XX__

ifeq ($(BUILD_RBS_RELEASE),true)
else
TRUSTLET_OPTS += -DEGIS_DBG \
		-DUSE_CORE_CONFIG_INI
endif
#-------------------------------------------------------------------------------
# Files and include paths - Add your files here
#-------------------------------------------------------------------------------

COMMON_PLATFORM=trustonic
### Add include path here
INCLUDE_DIRS += \
    Locals/Code/public \
    $(COMP_PATH_Drmem_Export)/public \
    $(COMP_PATH_Drsec_Export)/public \
    $(DRSPI_DIR)/Out/Public \
    Locals/Code/sdk/source/core/command_handler/ \
    Locals/Code/sdk/source/core/inline_handler/ \
	Locals/Code/sdk/source/core/fp_api/include \
	Locals/Code/sdk/source/core/fp_api/inc \
	Locals/Code/sdk/source/core/finger_detect/inc \
	Locals/Code/sdk/source/core/sensor_control/inc \
	Locals/Code/sdk/source/common/platform/inc \
	Locals/Code/sdk/source/core/sensor_test/inc \
	Locals/Code/sdk/source/core/sensor_control/src/ICTeam \
	Locals/Code/sdk/source/core/enroll_verify/src \
	Locals/Code/sdk/source/core/enroll_verify/inc \
	Locals/Code/sdk/source/core/navi/inc \
	Locals/Code/sdk/source/core/image_analysis/inc \
	Locals/Code/sdk/source/core/image_analysis/QMLib \
	Locals/Code/sdk/source/core/matching_algo/inc/G3 \
	Locals/Code/sdk/source/common/definition \
	Locals/Code/sdk/source/common/utility/inih \
	Locals/Code/sdk/source/common/utility/config/inc \
	Locals/Code/sdk/source/common/utility/cutimage/inc \
	Locals/Code/sdk/source/common/utility/hash_map \
	
### Add source code files for C compiler here
SRC_C += \
	Locals/Code/trustlet_ta_egis.c \
	Locals/Code/sdk/source/core/command_handler/command_handler.c \
	Locals/Code/sdk/source/core/command_handler/extra_operations.c \
	Locals/Code/sdk/source/core/command_handler/group_manager.c \
	Locals/Code/sdk/source/core/inline_handler/inline_handler.c \
	Locals/Code/sdk/source/core/fp_api/src/fp_algomodule.c \
	Locals/Code/sdk/source/core/fp_api/src/fp_sensormodule.c \
	Locals/Code/sdk/source/core/fp_api/src/egis_image_v1.c \
	Locals/Code/sdk/source/common/platform/src/$(COMMON_PLATFORM)/plat_log_$(COMMON_PLATFORM).c \
	Locals/Code/sdk/source/common/platform/src/$(COMMON_PLATFORM)/plat_mem_$(COMMON_PLATFORM).c \
	Locals/Code/sdk/source/common/platform/src/$(COMMON_PLATFORM)/plat_spi_$(COMMON_PLATFORM).c \
	Locals/Code/sdk/source/common/platform/src/$(COMMON_PLATFORM)/plat_file_$(COMMON_PLATFORM).c \
	Locals/Code/sdk/source/common/platform/src/$(COMMON_PLATFORM)/plat_time_$(COMMON_PLATFORM).c \
	Locals/Code/sdk/source/common/platform/src/$(COMMON_PLATFORM)/plat_encryption_$(COMMON_PLATFORM).c \
	Locals/Code/sdk/source/common/platform/src/$(COMMON_PLATFORM)/plat_std_$(COMMON_PLATFORM).c \
	Locals/Code/sdk/source/common/platform/src/egis_sprintf.c \
	Locals/Code/sdk/source/core/finger_detect/src/egis_fp_get_image.c \
	Locals/Code/sdk/source/core/finger_detect/src/fd_process.c \
	Locals/Code/sdk/source/core/finger_detect/src/fd_process_16bit.c \
	Locals/Code/sdk/source/core/finger_detect/src/fimage_check_qty.c \
	Locals/Code/sdk/source/core/finger_detect/src/fimage_check_qm.c \
	Locals/Code/sdk/source/core/sensor_control/src/isensor_api.c \
	Locals/Code/sdk/source/core/sensor_control/src/egis_sensormodule.c \
	Locals/Code/sdk/source/core/sensor_control/src/egis_handle_esd.c \
	Locals/Code/sdk/source/core/sensor_control/src/fp_capture_count.c \
	Locals/Code/sdk/source/core/sensor_control/src/et5xx/egis_fp_common_5XX.c \
	Locals/Code/sdk/source/core/sensor_control/src/et5xx/fingerprint_library.c \
	Locals/Code/sdk/source/core/sensor_control/src/et5xx/egis_fp_calibration.c \
	Locals/Code/sdk/source/core/sensor_control/src/et5xx/et5xx_calibration.c \
	Locals/Code/sdk/source/core/sensor_control/src/et5xx/sensor_param.c \
	Locals/Code/sdk/source/core/sensor_control/src/et5xx/ICTeam/ET538LDOCalibrationFlow.c \
	Locals/Code/sdk/source/core/sensor_test/src/sensor_function.c \
	Locals/Code/sdk/source/core/sensor_test/src/mmi_sensor_test.c \
	Locals/Code/sdk/source/core/sensor_test/src/egis_sensor_test.c \
	Locals/Code/sdk/source/core/enroll_verify/src/egis_fp_algmodule.c \
	Locals/Code/sdk/source/core/enroll_verify/src/matcher_feature.c \
	Locals/Code/sdk/source/core/enroll_verify/src/template_file.c \
	Locals/Code/sdk/source/core/enroll_verify/src/template_manager.c \
	Locals/Code/sdk/source/core/enroll_verify/src/uk_box.c \
	Locals/Code/sdk/source/core/enroll_verify/src/uk_manager.c \
	Locals/Code/sdk/source/core/enroll_verify/src/enroll_policy.c \
	Locals/Code/sdk/source/core/enroll_verify/src/verify_accuracy.c \
	Locals/Code/sdk/source/core/navi/src/egis_navi.c \
	Locals/Code/sdk/source/core/navi/src/navi_frame.c \
	Locals/Code/sdk/source/core/navi/src/navi.c \
	Locals/Code/sdk/source/core/image_analysis/src/puzzle_image.c \
	Locals/Code/sdk/source/core/image_analysis/src/image_analysis.c \
	Locals/Code/sdk/source/core/image_analysis/src/AES.c \
	Locals/Code/sdk/source/common/utility/config/src/core_config.c \
	Locals/Code/sdk/source/common/utility/inih/ini.c \
	Locals/Code/sdk/source/common/utility/inih/ini_parser.c \
	Locals/Code/sdk/source/common/utility/cutimage/src/image_cut.c \
	Locals/Code/sdk/source/common/utility/hash_map/hashmap.c \

CUSTOMER_DRIVER_LIBS += Locals/Code/sdk/source/core/matching_algo/G3AlgoLib/TZ/libEgisG3Algorithm_trustonic_rel.a

# for SECURY DRIVER(payment)
#CUSTOMER_DRIVER_LIBS += $(EGIS_DR_OUT_DIR)/$(TEE_MODE)/egis_dr.lib


CUSTOMER_DRIVER_LIBS += Locals/Code/libs/drspi.lib
CUSTOMER_DRIVER_LIBS += Locals/Code/libs/drutils.lib
CUSTOMER_DRIVER_LIBS += Locals/Code/libs/drsec.lib

CUSTOMER_DRIVER_LIBS += $(DRSEC_LIB_PATH) $(DRMEM_LIB_PATH)

#-------------------------------------------------------------------------------
# use generic make file
TRUSTLET_DIR ?= Locals/Code
TLSDK_DIR_SRC ?= $(TLSDK_DIR)
include $(TLSDK_DIR)/trustlet.mk
