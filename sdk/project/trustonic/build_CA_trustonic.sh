#!/bin/bash
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
me=`basename $0`
echo $me':'$DIR
cd $DIR

if [ "$#" -lt 1 ]; then
    echo "!!! One argument T_DDK_PATH is required."
	echo "e.g." 
	echo $0" /home/jackfan/Work/downloadbin/Trustonic_ZQ20170621/t-ddk-r3"
	exit
fi
APP_FOLDER_NAME=SM32_CA
BUILD_CONTINUE=false
if [ "$#" -gt 1 ]; then
    BUILD_CONTINUE=$2
fi
echo BUILD_CONTINUE=$BUILD_CONTINUE
if [ "$2" == "clean" ]; then
    cd $T_DDK_PATH
    rm -rf $APP_FOLDER_NAME
    echo "... clean done ..."
    exit
fi

T_DDK_PATH="$( cd $1 && pwd)"
ETS_SOURCE_PATH=$DIR/../../..

cd $T_DDK_PATH
rm -rf $APP_FOLDER_NAME
mkdir -p $APP_FOLDER_NAME/Locals/Build
mkdir -p $APP_FOLDER_NAME/Locals/Code

cd $APP_FOLDER_NAME
cp -v $ETS_SOURCE_PATH/sdk/project/trustonic/CA/Locals/Code/* ./Locals/Code/
cp -v $ETS_SOURCE_PATH/sdk/project/trustonic/CA/Locals/Build/* ./Locals/Build/

cd ./Locals/Build
./build.sh



