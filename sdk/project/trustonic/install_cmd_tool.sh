#!/bin/bash
adb root
adb wait-for-device
adb remount

adb shell rm /system/vendor/app/mcRegistry/08080000000000000000000000000000.tlbin
adb shell rm /system/bin/rbs_test
adb shell rm /vendor/lib64/libRbsFlow.so
adb shell rm /system/lib64/libRbsFlow.so

adb push ./TA/08080000000000000000000000000000.tlbin /system/vendor/app/mcRegistry/
adb push ./tools/rbs_test /system/bin
adb push ./flow/libRbsFlow.so /vendor/lib64

adb reboot
adb wait-for-device
adb root
adb wait-for-device
adb remount
adb shell setenforce 0