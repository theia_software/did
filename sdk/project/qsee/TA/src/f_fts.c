#include <comdef.h>
#include <gpPersistObjCrypto.h>
#include <gpPersistObjFileIO.h>
#include <qsee_heap.h>
#include <qsee_log.h>

#define UINT8_A uint8 __attribute__((aligned(128)))

#define FTS_ERROR_SUCCESS 0
#define FTS_ERROR_FAILED -1

// clang-format off
const UINT8_A g_key[]=
{
	0x60, 0x3d, 0xeb, 0x10, 0x15, 0xca, 0x71, 0xbe,
	0x2b, 0x73, 0xae, 0xf0, 0x85, 0x7d, 0x77, 0x81,
	0x1f, 0x35, 0x2c, 0x07, 0x3b, 0x61, 0x08, 0xd7,
	0x2d, 0x98, 0x10, 0xa3, 0x09, 0x14, 0xdf, 0xf4
};

const UINT8_A g_iv[] =
{
	0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07,
	0x08, 0x09, 0x0A, 0x0B, 0x0C, 0x0D, 0x0E, 0x0F
};
// clang-format on

typedef struct {
    uint8_t hmac[GPCRYPTO_HMAC_SIZE];
    uint8_t iv[GPCRYPTO_IV_SIZE];
} dataObj;

#define QSEE_LOG qsee_log

static int data_cipher_initialize(qsee_cipher_ctx** ctx) {
    QSEE_CIPHER_MODE_ET mode = QSEE_CIPHER_MODE_CBC;
    QSEE_CIPHER_ALGO_ET alg = QSEE_CIPHER_ALGO_AES_256;

    if (qsee_cipher_init(alg, ctx) < 0) {
        QSEE_LOG(QSEE_LOG_MSG_ERROR, "f_fs.c::data_cipher_initialize qsee_cipher_init failed");
        return FTS_ERROR_FAILED;
    }

    if (qsee_cipher_set_param(*ctx, QSEE_CIPHER_PARAM_KEY, g_key, sizeof(g_key)) < 0) {
        QSEE_LOG(QSEE_LOG_MSG_ERROR,
                 "f_fs.c::data_cipher_initialize qsee_cipher_set_parm API Key failed");
        return FTS_ERROR_FAILED;
    }

    if (qsee_cipher_set_param(*ctx, QSEE_CIPHER_PARAM_MODE, &mode, sizeof(mode)) < 0) {
        QSEE_LOG(QSEE_LOG_MSG_ERROR,
                 "f_fs.c::data_cipher_initialize qsee_cipher_set_parm API Mode failed");
        return FTS_ERROR_FAILED;
    }

    if (qsee_cipher_set_param(*ctx, QSEE_CIPHER_PARAM_IV, g_iv, QSEE_AES128_IV_SIZE) < 0) {
        QSEE_LOG(QSEE_LOG_MSG_ERROR,
                 "f_fs.c::data_cipher_encrypt qsee_cipher_set_parm API IV failed");
        return FTS_ERROR_FAILED;
    }
    return FTS_ERROR_SUCCESS;
}

int data_cipher_encrypt(uint8_t* pt, size_t pt_len, uint8_t* ct, size_t* ct_len) {
    qsee_cipher_ctx* ctx;
    if (data_cipher_initialize(&ctx) < 0 || ctx == NULL) {
        QSEE_LOG(QSEE_LOG_MSG_ERROR, "f_fs.c::data_cipher_encrypt data_cipher_initialize failed!");
        return FTS_ERROR_FAILED;
    }

    if (qsee_cipher_encrypt(ctx, pt, pt_len, ct, ct_len) < 0) {
        QSEE_LOG(QSEE_LOG_MSG_ERROR, "f_fs.c::data_cipher_encrypt qsee_cipher_encrypt API failed");
        return FTS_ERROR_FAILED;
    }

    if (qsee_cipher_free_ctx(ctx) < 0) {
        QSEE_LOG(QSEE_LOG_MSG_ERROR, "f_fs.c::data_cipher_encrypt qsee_cipher_free_ctx API failed");
        return FTS_ERROR_FAILED;
    }
    return FTS_ERROR_SUCCESS;
}

static int data_cipher_decrypt(uint8_t* ct, size_t ct_len, uint8_t* pt, size_t* pt_len) {
    qsee_cipher_ctx* ctx;
    if (data_cipher_initialize(&ctx) < 0 || ctx == NULL) {
        QSEE_LOG(QSEE_LOG_MSG_ERROR, "f_fs.c::data_cipher_decrypt data_cipher_initialize failed!");
        return FTS_ERROR_FAILED;
    }

    if (qsee_cipher_decrypt(ctx, ct, ct_len, pt, pt_len) < 0) {
        QSEE_LOG(QSEE_LOG_MSG_ERROR, "f_fs.c::data_cipher_decrypt qsee_cipher_decrypt API failed");
        return FTS_ERROR_FAILED;
    }

    if (qsee_cipher_free_ctx(ctx) < 0) {
        QSEE_LOG(QSEE_LOG_MSG_ERROR, "f_fs.c::data_cipher_decrypt qsee_cipher_free_ctx API failed");
        return FTS_ERROR_FAILED;
    }
    return FTS_ERROR_SUCCESS;
}

static int do_encrypt(uint8_t* data, size_t dataSize, uint8_t* ivData, size_t ivSize, uint8_t* hmac,
                      size_t hmacSize, const char* filename) {
    uint8_t *ct_iv = NULL, *ct_hmac = NULL, *hmac_src = NULL;
    uint32_t fts_retval, hmac_src_len;
    size_t ct_iv_len = ivSize;
    size_t ct_hmac_len = hmacSize;

    QSEE_LOG(QSEE_LOG_MSG_DEBUG, "f_fs.c do_encrypt enter!");
    fts_retval = gpCrypto_Encrypt(NULL, data, dataSize, ivData, ivSize);
    if (fts_retval != TEE_SUCCESS) {
        QSEE_LOG(QSEE_LOG_MSG_ERROR, "f_fs.c::do_encrypt gpCrypto_Encrypt failed");
        goto exit;
    }
    hmac_src_len = dataSize + strlen(filename);
    hmac_src = (uint8_t*)qsee_malloc(hmac_src_len);
    memcpy(hmac_src, data, dataSize);
    memcpy(hmac_src + dataSize, filename, strlen(filename));
    fts_retval = gpCrypto_Integrity_Protect(NULL, hmac_src, hmac_src_len, NULL, 0, hmac, hmacSize);
    if (fts_retval != TEE_SUCCESS) {
        QSEE_LOG(QSEE_LOG_MSG_ERROR, "f_fs.c::do_encrypt gpCrypto_Integrity_Protect failed");
        goto exit;
    }

    ct_iv = (uint8_t*)qsee_malloc(ivSize);
    if (ct_iv == NULL) {
        QSEE_LOG(QSEE_LOG_MSG_ERROR, "f_fs.c::do_encrypt ct_iv malloc failed");
        fts_retval = FTS_ERROR_FAILED;
        goto exit;
    }

    fts_retval = data_cipher_encrypt(ivData, ivSize, ct_iv, &ct_iv_len);
    if (fts_retval != FTS_ERROR_SUCCESS) {
        QSEE_LOG(QSEE_LOG_MSG_ERROR, "f_fs.c::do_encrypt data_cipher_encrypt iv failed");
        goto exit;
    }
    memcpy(ivData, ct_iv, ivSize);

    ct_hmac = (uint8_t*)qsee_malloc(hmacSize);
    if (ct_hmac == NULL) {
        QSEE_LOG(QSEE_LOG_MSG_ERROR, "f_fs.c::do_encrypt ct_hmac malloc failed");
        fts_retval = FTS_ERROR_FAILED;
        goto exit;
    }

    fts_retval = data_cipher_encrypt(hmac, hmacSize, ct_hmac, &ct_hmac_len);
    if (fts_retval != FTS_ERROR_SUCCESS) {
        QSEE_LOG(QSEE_LOG_MSG_ERROR, "f_fs.c::do_encrypt data_cipher_encrypt hmac failed");
        goto exit;
    }
    memcpy(hmac, ct_hmac, hmacSize);

exit:
    if (ct_iv) qsee_free(ct_iv);
    if (ct_hmac) qsee_free(ct_hmac);
    if (hmac_src) qsee_free(hmac_src);

    QSEE_LOG(QSEE_LOG_MSG_DEBUG, "f_fs.c::[do_encrypt] End!");
    return fts_retval;
}

static int do_decrypt(uint8_t* data, size_t dataSize, uint8_t* ivData, size_t ivSize, uint8_t* hmac,
                      size_t hmacSize, const char* filename) {
    uint8_t *ct_iv = NULL, *ct_hmac = NULL, *hmac_src = NULL;
    uint32_t fts_retval, hmac_src_len;
    size_t ct_iv_len = ivSize;
    size_t ct_hmac_len = hmacSize;

    QSEE_LOG(QSEE_LOG_MSG_DEBUG, "f_fs.c::[do_decrypt] Start!");
    ct_iv = (uint8_t*)qsee_malloc(ivSize);
    if (ct_iv == NULL) {
        QSEE_LOG(QSEE_LOG_MSG_ERROR, "f_fs.c::dodo_decrypt_encrypt ct_iv malloc failed");
        fts_retval = FTS_ERROR_FAILED;
        goto exit;
    }

    fts_retval = data_cipher_decrypt(ivData, ivSize, ct_iv, &ct_iv_len);
    if (fts_retval != FTS_ERROR_SUCCESS) {
        QSEE_LOG(QSEE_LOG_MSG_ERROR, "f_fs.c::do_decrypt data_cipher_decrypt iv failed");
        goto exit;
    }
    memcpy(ivData, ct_iv, ivSize);

    ct_hmac = (uint8_t*)qsee_malloc(hmacSize);
    if (ct_hmac == NULL) {
        QSEE_LOG(QSEE_LOG_MSG_ERROR, "f_fs.c::do_decrypt ct_hmac malloc failed");
        fts_retval = FTS_ERROR_FAILED;
        goto exit;
    }

    fts_retval = data_cipher_decrypt(hmac, hmacSize, ct_hmac, &ct_hmac_len);
    if (fts_retval != FTS_ERROR_SUCCESS) {
        QSEE_LOG(QSEE_LOG_MSG_ERROR, "f_fs.c::do_decrypt data_cipher_decrypt hmac failed");
        goto exit;
    }
    memcpy(hmac, ct_hmac, hmacSize);

    hmac_src_len = dataSize + strlen(filename);
    hmac_src = (uint8_t*)qsee_malloc(hmac_src_len);
    if (hmac_src == NULL) {
        QSEE_LOG(QSEE_LOG_MSG_ERROR, "f_fs.c::do_decrypt hmac_src malloc failed");
        fts_retval = FTS_ERROR_FAILED;
        goto exit;
    }
    memcpy(hmac_src, data, dataSize);
    memcpy(hmac_src + dataSize, filename, strlen(filename));
    fts_retval = gpCrypto_Integrity_Verify(NULL, hmac_src, hmac_src_len, NULL, 0, hmac, hmacSize);
    if (fts_retval != TEE_SUCCESS) {
        QSEE_LOG(QSEE_LOG_MSG_ERROR, "f_fs.c::do_decrypt gpCrypto_Integrity_Verify failed");
        goto exit;
    }

    fts_retval = gpCrypto_Decrypt(NULL, data, dataSize, ivData, ivSize);
    if (fts_retval != TEE_SUCCESS) {
        QSEE_LOG(QSEE_LOG_MSG_ERROR, "f_fs.c::do_decrypt gpCrypto_Decrypt failed");
        goto exit;
    }

exit:
    if (ct_iv) qsee_free(ct_iv);
    if (ct_hmac) qsee_free(ct_hmac);
    if (hmac_src) qsee_free(hmac_src);

    QSEE_LOG(QSEE_LOG_MSG_DEBUG, "f_fs.c::[do_decrypt] End!");
    return fts_retval;
}

//================== FTS BEGIN======================
int f_remove_fts(const char* filename) {
    if (filename == NULL) {
        QSEE_LOG(QSEE_LOG_MSG_ERROR, "f_fs.c::[f_remove_fs] file is NULL!");
        return FTS_ERROR_FAILED;
    }
    return gpFileIO_Remove_File(NULL, filename);
}

int f_write_fts(const char* filename, void* buf, size_t len) {
    uint32_t fts_retval = FTS_ERROR_FAILED;
    uint32_t count = 0;
    dataObj* obj = NULL;
    void* obj_space = NULL;
    uint32_t obj_size = len + sizeof(dataObj);
    uint8_t* data = NULL;

    QSEE_LOG(QSEE_LOG_MSG_DEBUG, "f_fs.c::[f_write_fs] start!");
    if (buf == NULL || filename == NULL) {
        QSEE_LOG(QSEE_LOG_MSG_ERROR, "f_fs.c::[f_write_fs] parameter error");
        goto exit;
    }

    obj_space = qsee_malloc(sizeof(uint8_t) * obj_size);
    if (obj_space == NULL) {
        QSEE_LOG(QSEE_LOG_MSG_ERROR, "f_fs.c::[f_write_fs] obj malloc failed!");
        goto exit;
    }

    memset(obj_space, 0, sizeof(obj_space));
    obj = (dataObj*)obj_space;
    data = obj_space + sizeof(dataObj);
    memcpy(data, buf, len);

    fts_retval =
        do_encrypt(data, len, obj->iv, sizeof(obj->iv), obj->hmac, sizeof(obj->hmac), filename);
    if (fts_retval != TEE_SUCCESS) {
        QSEE_LOG(QSEE_LOG_MSG_ERROR, "f_fs.c::[f_write_fs] do_encrypt error, file is:%s", filename);
        goto exit;
    }

    fts_retval = gpFileIO_Write_File(NULL, filename, obj_space, obj_size, 0, &count);
    QSEE_LOG(QSEE_LOG_MSG_DEBUG, "f_fs.c::f_write_fts count : %d", count);
    if (fts_retval != TEE_SUCCESS) {
        QSEE_LOG(QSEE_LOG_MSG_ERROR, "f_fs.c::[f_write_fs] gpFileIO_Write_File error, file is:%s",
                 filename);
        goto exit;
    }

exit:
    if (obj) qsee_free(obj);
    return fts_retval ? fts_retval : len;
}

int f_read_fts(const char* filename, void* buf, size_t len) {
    uint32_t fts_retval = FTS_ERROR_FAILED;
    uint32_t count = 0;
    dataObj* obj = NULL;
    void* obj_space = NULL;
    uint32_t obj_size = len + sizeof(dataObj);
    uint8_t* data = NULL;

    QSEE_LOG(QSEE_LOG_MSG_DEBUG, "f_fs.c::[f_read_fs] start!");
    if (buf == NULL || filename == NULL) {
        QSEE_LOG(QSEE_LOG_MSG_ERROR, "f_fs.c::[f_read_fs] parameter error");
        goto exit;
    }

    obj_space = qsee_malloc(obj_size);
    if (obj_space == NULL) {
        QSEE_LOG(QSEE_LOG_MSG_ERROR, "f_fs.c::[f_read_fs] obj malloc failed!");
        goto exit;
    }

    memset(obj_space, 0, sizeof(obj_space));
    obj = (dataObj*)obj_space;
    data = obj_space + sizeof(dataObj);

    QSEE_LOG(QSEE_LOG_MSG_DEBUG, "f_fs.c::f_read_fts len : %d", len);
    QSEE_LOG(QSEE_LOG_MSG_DEBUG, "f_fs.c::f_read_fts obj_size : %d", obj_size);
    fts_retval = gpFileIO_Read_File(NULL, filename, obj_space, obj_size, 0, &count);
    QSEE_LOG(QSEE_LOG_MSG_DEBUG, "f_fs.c::f_read_fts count : %d", count);
    if (fts_retval != TEE_SUCCESS) {
        QSEE_LOG(QSEE_LOG_MSG_ERROR, "f_fs.c::f_read_fts gpFileIO_Read_File failed");
        goto exit;
    }

    fts_retval =
        do_decrypt(data, len, obj->iv, sizeof(obj->iv), obj->hmac, sizeof(obj->hmac), filename);
    if (fts_retval != TEE_SUCCESS) {
        QSEE_LOG(QSEE_LOG_MSG_ERROR, "f_fs.c::f_read_fts do_decrypt failed");
        goto exit;
    }
    memcpy(buf, data, len);

exit:
    if (obj) qsee_free(obj);
    return fts_retval ? fts_retval : len;
}
//================== FTS END=========================
