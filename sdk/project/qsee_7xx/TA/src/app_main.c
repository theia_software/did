/*
@file app_main.c
@brief App main entry point.

*/
/*===========================================================================
   Copyright (c) 2000-2017 by Egis Tec, Incorporated.  All Rights Reserved.
===========================================================================*/

#include <comdef.h>
#include <string.h>
#include "qsee_fuse.h"
#include "qsee_heap.h"
#include "qsee_log.h"
#include "qsee_services.h"
#include "qsee_timer.h"
// #include "app_content_protection.h"
#include "bsp_tz_spi.h"
#include "command_handler.h"
#include "common_definition.h"
#include "inline_handler.h"

/* commands supported from sample client */
#define CLIENT_CMD0_GET_VERSION 0
#define CLIENT_CMD1_BASIC_DATA 1
#define CLIENT_CMD2_REGISTER_SB_PTR 2
#define CLIENT_CMD3_RUN_CRYTPO_TEST 3
#define CLIENT_CMD4_RUN_CRYTPO_PERF 4
#define CLIENT_CMD5_RUN_SFS_TEST 5
#define CLIENT_CMD6_RUN_FS_TEST 6
#define CLIENT_CMD7_RUN_RSA_TEST 7
#define CLIENT_CMD8_RUN_RSA_PERF_TEST 8
#define CLIENT_CMD9_RUN_CMNLIB_TEST 9
#define CLIENT_CMD10_RUN_CORE_TEST 10
#define CLIENT_CMD11_RUN_SECURECHANNEL_TEST 11
#define CLIENT_CMD12_RUN_MESSAGE_PASSING 12
#define CLIENT_CMD13_RUN_MISC_TEST \
    13  // 14 and 15 are used for RPMB operations in the sample client.
#define CLIENT_CMD16_RUN_BUSES_TEST 16
#define CLIENT_CMD17_RUN_STOR_TEST 17
#define CLIENT_CMD18_RUN_FUSE_TEST 18
#define CLIENT_CMD19_RUN_BUSES_SPI_TEST 19
#define CLIENT_CMD20_RUN_HANDLE_TEST 20
#define CLIENT_CMD21_RUN_CRYPTO_COPY 21
#define CLIENT_CMD25_RUN_SAFE_TEST 25
#define CLIENT_CMD26_RUN_ABORT_TEST 26

#define CLIENT_CMD_ETS_MESSAGE (0xe0 + PID_COMMAND)
#define CLIENT_CMD_ETS_INLINE_MESSAGE (CLIENT_CMD_ETS_MESSAGE + PID_INLINETOOL)

#define __64KB__ 0x10000
#define __32KB__ 0x8000
#define __16KB__ 0x4000
#define __8KB__ 0x2000
#define __4KB__ 0x1000
#define __2KB__ 0x800
#define __1KB__ 0x400

/* Error code: status sent as response to command from sample client*/

#define SUCCESS 0
#define FAILED -1
#define FAIL_REGISTER_SB -2
#define FAIL_DEREGISTER_SB -3
#define FAIL_PREP_SB_NON_SECURE_READ -4
#define FAIL_PREP_SB_SECURE_READ -5
#define FAIL_CMD_BUF_TOO_SMALL_FOR_CMD_ID -6
#define FAIL_CMD_RSP_BUFS_TOO_SMALL -7
#define FAIL_SHARED_BUFF_TOO_SMALL -8

#define SAMPLE_APP_VERSION_MAJOR 0x1
#define SAMPLE_APP_VERSION_MINOR 0x2

#define SHARED_BUF_PATTERN_LEN 16

/**
  @brief
    Add any app specific initialization code here
    QSEE will call this function after secure app is loaded and
    authenticated
*/
void tz_app_init(void) {
#ifdef EGIS_DBG
    qsee_log_set_mask(QSEE_LOG_MSG_ERROR | QSEE_LOG_MSG_FATAL | QSEE_LOG_MSG_HIGH |
                      QSEE_LOG_MSG_MED);
#endif
    qsee_log(QSEE_LOG_MSG_FATAL, "Egistec Inc.");
}

/**
  @brief
    Data structure

  @param[in]   cmd_id      Requested command
  @param[in]   data        information (could be data or a pointer to the memory
  that holds the data
  @param[in]   len         if data is ptr to some buffer, len indicates length
  of the buffer
  @param[in]   test_buf_size  When running crypto test, this indicates the test
  packet size
*/
typedef struct send_cmd {
    uint32 cmd_id;
    uint32 data;
    uint32 data2;
    uint32 len;
    uint32 start_pkt;
    uint32 end_pkt;
    uint32 test_buf_size;
    uint8_t buffer[64];
    uint8_t egis_data[2048];
} send_cmd_t;

typedef struct send_cmd_rsp {
    uint32 data;
    uint32 data1;
    uint32 data2;
    uint32 data3;
    int32 status;
    uint8_t egis_data[2048];
} send_cmd_rsp_t;

/*Timing vars used for crypto performance test*/
// unsigned long long b4 = 0;
// unsigned long long after = 0;
// unsigned long long time_diff = 0;
// unsigned long long average_time_diff = 0;
static int (*g_pf_command_handler)(unsigned char* msg_data, int msg_size, unsigned char* outdata,
                                   int* outdata_size);

void tz_app_cmd_handler(void* cmd, uint32 cmdlen, void* rsp, uint32 rsplen) {
    /* Request-response buffers are allocated by non-secure side*/
    /* They are MPU protected by QSEE kernel before reaching here*/
    /* Add code to process requests and set response (if any)*/
    int retval = SUCCESS;
    uint32 cmd_id;
    uint32 expected_cmdlen;
    uint32 expected_rpslen;
    struct send_cmd* cmd_ptr = (struct send_cmd*)cmd;
    struct send_cmd_rsp* rsp_ptr = (struct send_cmd_rsp*)rsp;

    /*First we check if the response pointer is large enough to support a pass/fail response*/
    if (rsplen < sizeof(send_cmd_rsp_t)) {
        qsee_log(QSEE_LOG_MSG_FATAL, "Response buffer len insufficient, ERROR OUT");
        return;
    }

    /*Determine the command id*/
    /*We check if the command buffer is large enough to support the uint32 read for cmd_id*/
    /*It is assumed that the first member of the command buffer is the cmd_id*/
    if (cmdlen < sizeof(uint32)) {
        qsee_log(QSEE_LOG_MSG_FATAL,
                 "Command buffer len insufficient for reading cmd_id, ERROR OUT");
        rsp_ptr->status = FAIL_CMD_BUF_TOO_SMALL_FOR_CMD_ID;
        qsee_log(QSEE_LOG_MSG_FATAL, "tz_app_cmd_handler rsp_ptr->status = %d", rsp_ptr->status);
        return;
    }
    cmd_id = cmd_ptr->cmd_id;
    qsee_log(QSEE_LOG_MSG_ERROR, "tz_app_cmd_handler cmd_id %d", cmd_id);

    /*Identify which command and response buffers we are using based on command ID*/
    expected_cmdlen = sizeof(send_cmd_t);
    expected_rpslen = sizeof(send_cmd_rsp_t);

    /*Validate the command buffer and response buffer are the correct size.
      If not, the the MPU protection and ns_range checks done by QSEE kernel might be insufficient*/
    if (cmdlen < expected_cmdlen || rsplen < expected_rpslen) {
        qsee_log(QSEE_LOG_MSG_FATAL, "Cmd/rsp buffer lens insufficient - %x, %x, ERROR OUT", cmdlen,
                 rsplen);
        rsp_ptr->status = FAIL_CMD_RSP_BUFS_TOO_SMALL;
        qsee_log(QSEE_LOG_MSG_FATAL, "tz_app_cmd_handler rsp_ptr->status = %d", rsp_ptr->status);
        return;
    }

    g_pf_command_handler = command_handler;

    switch (cmd_id) {
        case CLIENT_CMD0_GET_VERSION:
            /* Return major version of the sample test app*/
            rsp_ptr->data = SAMPLE_APP_VERSION_MAJOR;
            rsp_ptr->status = SUCCESS;
            break;

        case CLIENT_CMD1_BASIC_DATA:
            /*Modify response by 10 */
            // run_buses_spi_test();
            // io_dispatch_spi_test();
            // fp_egis_command_handler_test();
            rsp_ptr->data = cmd_ptr->data * 10;
            rsp_ptr->data = 112233;
            rsp_ptr->status = SUCCESS;
            break;

        case CLIENT_CMD_ETS_INLINE_MESSAGE:
            g_pf_command_handler = inline_handler;
        case CLIENT_CMD_ETS_MESSAGE: {
            if (cmd_ptr->data && cmd_ptr->len > 0) {
                qsee_log(QSEE_LOG_MSG_DEBUG, "-- CLIENT_CMD_ETS_MESSAGE, cmd_ptr->len = %d",
                         cmd_ptr->len);
                retval = qsee_register_shared_buffer((void*)cmd_ptr->data, cmd_ptr->len);
                if (retval) {
                    qsee_log(QSEE_LOG_MSG_FATAL, "qsee_register_shared_buffer err = %d", retval);
                    rsp_ptr->status = FAIL_REGISTER_SB;
                    qsee_log(QSEE_LOG_MSG_FATAL, "tz_app_cmd_handler rsp_ptr->status = %d",
                             rsp_ptr->status);
                    return;
                }

                retval =
                    qsee_prepare_shared_buf_for_secure_read((void*)cmd_ptr->data, cmd_ptr->len);
                if (retval) {
                    qsee_log(QSEE_LOG_MSG_FATAL, "qsee_prepare_shared_buf_for_secure_read err = %d",
                             retval);
                    rsp_ptr->status = FAIL_PREP_SB_SECURE_READ;
                    retval = qsee_deregister_shared_buffer((void*)cmd_ptr->data);
                    if (retval)
                        qsee_log(QSEE_LOG_MSG_FATAL, "Cqsee_deregister_shared_buffer err = %d",
                                 retval);
                    qsee_log(QSEE_LOG_MSG_FATAL, "tz_app_cmd_handler rsp_ptr->status = %d",
                             rsp_ptr->status);
                    return;
                }
            } else
                qsee_log(QSEE_LOG_MSG_DEBUG, "-- CLIENT_CMD_ETS_MESSAGE, *cmd_ptr->len = %d",
                         cmd_ptr->len);

            retval = g_pf_command_handler(cmd_ptr->egis_data, sizeof(cmd_ptr->egis_data),
                                          (unsigned char*)(cmd_ptr->data), (int*)&cmd_ptr->len);

            qsee_log(QSEE_LOG_MSG_DEBUG, "-- retval = %d", retval);
            memcpy(rsp_ptr->egis_data, cmd_ptr->egis_data, sizeof(cmd_ptr->egis_data));
            rsp_ptr->status = (retval) ? FAILED : SUCCESS;

            qsee_log(QSEE_LOG_MSG_DEBUG, "-- rsp_ptr->status = %d", rsp_ptr->status);

            if (cmd_ptr->data && cmd_ptr->len > 0) {
                retval =
                    qsee_prepare_shared_buf_for_nosecure_read((void*)cmd_ptr->data, cmd_ptr->len);
                if (retval) {
                    qsee_log(QSEE_LOG_MSG_FATAL,
                             "qsee_prepare_shared_buf_for_nosecure_read err = %d", retval);
                    rsp_ptr->status = FAIL_PREP_SB_NON_SECURE_READ;
                    qsee_log(QSEE_LOG_MSG_FATAL, "tz_app_cmd_handler rsp_ptr->status = %d",
                             rsp_ptr->status);
                    return;
                }

                retval = qsee_deregister_shared_buffer((void*)cmd_ptr->data);
                if (retval) {
                    qsee_log(QSEE_LOG_MSG_FATAL, "qsee_deregister_shared_buffer err = %d", retval);
                    rsp_ptr->status = FAIL_DEREGISTER_SB;
                    qsee_log(QSEE_LOG_MSG_FATAL, "tz_app_cmd_handler rsp_ptr->status = %d",
                             rsp_ptr->status);
                    return;
                }
            }
        } break;

        default:
            rsp_ptr->status = SUCCESS;
            break;
    }
    qsee_log(QSEE_LOG_MSG_DEBUG, "tz_app_cmd_handler END rsp_ptr->status=%d, retval=%d",
             rsp_ptr->status, retval);
}

/**
  @brief
    App specific shutdown
    App will be given a chance to shutdown gracefully
*/
void tz_app_shutdown(void) {
    qsee_log(QSEE_LOG_MSG_FATAL, "======== egis shutdown ========");
    return;
}
