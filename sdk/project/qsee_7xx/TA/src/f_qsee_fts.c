#include <comdef.h>
#include <gpPersistObjCrypto.h>
#include <gpPersistObjFileIO.h>
#include <qsee_heap.h>
#include <qsee_log.h>

typedef struct {
    uint8_t hmac[GPCRYPTO_HMAC_SIZE];
    uint8_t iv[GPCRYPTO_IV_SIZE];
} dataObj;

#define QSEE_LOG qsee_log

//================== FTS BEGIN======================
int f_remove_fts(const char* filename) {
    if (filename == NULL) {
        QSEE_LOG(QSEE_LOG_MSG_ERROR, "f_fs.c::[f_remove_fs] file is NULL!");
        return FTS_ERROR_FAILED;
    }
    return qsee_fts_remove_file(filename);
}

int f_write_fts(const char* filename, void* buf, size_t len) {
    uint32_t fts_retval = FTS_ERROR_FAILED;
    uint32_t count = 0;
    dataObj* obj = NULL;
    void* obj_space = NULL;
    uint32_t obj_size = len + sizeof(dataObj);
    uint8_t* data = NULL;

    QSEE_LOG(QSEE_LOG_MSG_DEBUG, "f_fs.c::[f_write_fs] start!");
    if (buf == NULL || filename == NULL) {
        QSEE_LOG(QSEE_LOG_MSG_ERROR, "f_fs.c::[f_write_fs] parameter error");
        goto exit;
    }

    obj_space = qsee_malloc(sizeof(uint8_t) * obj_size);
    if (obj_space == NULL) {
        QSEE_LOG(QSEE_LOG_MSG_ERROR, "f_fs.c::[f_write_fs] obj malloc failed!");
        goto exit;
    }

    memset(obj_space, 0, sizeof(obj_space));
    obj = (dataObj*)obj_space;
    data = obj_space + sizeof(dataObj);
    memcpy(data, buf, len);

    fts_retval = qsee_fts_encrypt(data, len, obj->iv, sizeof(obj->iv));
    if (fts_retval != TEE_SUCCESS) {
        QSEE_LOG(QSEE_LOG_MSG_ERROR, "f_fs.c::[f_write_fs] do_encrypt error, file is:%s", filename);
        goto exit;
    }

    fts_retval = qsee_fts_integrity_protect(obj->iv, len + sizeof(obj->iv), NULL, 0, obj->hmac,
                                            sizeof(obj->hmac));
    if (fts_retval != TEE_SUCCESS) {
        QSEE_LOG(QSEE_LOG_MSG_ERROR,
                 "f_fs.c::[f_write_fs] qsee_fts_integrity_protect error, file is:%s", filename);
        goto exit;
    }

    fts_retval = qsee_fts_write_file(filename, obj_space, obj_size, 0, &count);
    QSEE_LOG(QSEE_LOG_MSG_DEBUG, "f_fs.c::f_write_fts count : %d", count);
    if (fts_retval != TEE_SUCCESS) {
        QSEE_LOG(QSEE_LOG_MSG_ERROR, "f_fs.c::[f_write_fs] gpFileIO_Write_File error, file is:%s",
                 filename);
        goto exit;
    }

exit:
    if (obj) qsee_free(obj);
    return fts_retval ? fts_retval : len;
}

int f_read_fts(const char* filename, void* buf, size_t len) {
    uint32_t fts_retval = FTS_ERROR_FAILED;
    uint32_t count = 0;
    dataObj* obj = NULL;
    void* obj_space = NULL;
    uint32_t obj_size = len + sizeof(dataObj);
    uint8_t* data = NULL;

    QSEE_LOG(QSEE_LOG_MSG_DEBUG, "f_fs.c::[f_read_fs] start!");
    if (buf == NULL || filename == NULL) {
        QSEE_LOG(QSEE_LOG_MSG_ERROR, "f_fs.c::[f_read_fs] parameter error");
        goto exit;
    }

    obj_space = qsee_malloc(obj_size);
    if (obj_space == NULL) {
        QSEE_LOG(QSEE_LOG_MSG_ERROR, "f_fs.c::[f_read_fs] obj malloc failed!");
        goto exit;
    }

    memset(obj_space, 0, sizeof(obj_space));
    obj = (dataObj*)obj_space;
    data = obj_space + sizeof(dataObj);

    QSEE_LOG(QSEE_LOG_MSG_DEBUG, "f_fs.c::f_read_fts len : %d", len);
    QSEE_LOG(QSEE_LOG_MSG_DEBUG, "f_fs.c::f_read_fts obj_size : %d", obj_size);
    fts_retval = qsee_fts_read_file(filename, obj_space, obj_size, 0, &count);
    QSEE_LOG(QSEE_LOG_MSG_DEBUG, "f_fs.c::f_read_fts count : %d", count);
    if (fts_retval != TEE_SUCCESS) {
        QSEE_LOG(QSEE_LOG_MSG_ERROR, "f_fs.c::f_read_fts gpFileIO_Read_File failed");
        goto exit;
    }

    fts_retval = qsee_fts_integrity_verify(obj->iv, (count - sizeof(dataObj)) + sizeof(obj->iv),
                                           NULL, 0, obj->hmac, sizeof(obj->hmac));
    if (fts_retval != TEE_SUCCESS) {
        QSEE_LOG(QSEE_LOG_MSG_ERROR, "f_fs.c::f_read_fts qsee_fts_integrity_verify failed");
        goto exit;
    }

    fts_retval = qsee_fts_decrypt(data, len, obj->iv, sizeof(obj->iv));
    if (fts_retval != TEE_SUCCESS) {
        QSEE_LOG(QSEE_LOG_MSG_ERROR, "f_fs.c::f_read_fts do_decrypt failed");
        goto exit;
    }
    memcpy(buf, data, len);

exit:
    if (obj) qsee_free(obj);
    return fts_retval ? fts_retval : len;
}
//================== FTS END=========================
