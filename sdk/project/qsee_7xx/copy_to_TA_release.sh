#!/bin/bash
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
me=`basename $0`
echo $me':'$DIR
cd $DIR

if [ "$1" == "llvm" ];then
    TOOLCHAIN="llvm"
else
    TOOLCHAIN="ds5"
fi

SDK_SOURCE_FOLDER="../../source"
SDK_COMMOM_FOLDER="../../source/common"
TA_RELEASE_FOLDER="TA_Release/trustzone_images/ssg/securemsm/trustzone/qsapps/egista"

if [ -d $TA_RELEASE_FOLDER ]; then
    echo ''
    if [ "$CI" == "" ]; then
    read -n1 -r -p "Press any key to remove ${TA_RELEASE_FOLDER}" tmpkey
    fi
    rm -rf $TA_RELEASE_FOLDER
fi

mkdir -p $TA_RELEASE_FOLDER
mkdir $TA_RELEASE_FOLDER/src
mkdir $TA_RELEASE_FOLDER/inc

####################
# build MT lib
cp Makefile_MT $SDK_SOURCE_FOLDER/core/sensor_test/src/7xx_MT/Makefile
cd $SDK_SOURCE_FOLDER/core/sensor_test/src/7xx_MT/
make clean TOOLCHAIN=$TOOLCHAIN
make TOOLCHAIN=$TOOLCHAIN
rm Makefile
cd $DIR
####################

cp -v TA/src/app_main.c $TA_RELEASE_FOLDER/src/
# cp -v TA/src/f_fts.c $TA_RELEASE_FOLDER/src/
cp -v TA/src/SConscript $TA_RELEASE_FOLDER/src/

cp -v $SDK_COMMOM_FOLDER/platform/src/qsee/bsp_tz_spi.c $TA_RELEASE_FOLDER/src/
cp -v $SDK_COMMOM_FOLDER/platform/src/qsee/bsp_tz_spi.h $TA_RELEASE_FOLDER/inc/

cp -v $SDK_SOURCE_FOLDER/core/command_handler/command_handler.h $TA_RELEASE_FOLDER/inc/
cp -v $SDK_SOURCE_FOLDER/core/inline_handler/inline_handler.h $TA_RELEASE_FOLDER/inc/
cp -v $SDK_COMMOM_FOLDER/definition/common_definition.h $TA_RELEASE_FOLDER/inc/
cp -v $SDK_COMMOM_FOLDER/definition/ini_definition.h $TA_RELEASE_FOLDER/inc/
cp -v $SDK_COMMOM_FOLDER/platform/inc/plat_log.h $TA_RELEASE_FOLDER/inc/
cp -v $SDK_COMMOM_FOLDER/platform/inc/plat_spi.h $TA_RELEASE_FOLDER/inc/
cp -v $SDK_COMMOM_FOLDER/platform/inc/type_definition.h $TA_RELEASE_FOLDER/inc/
cp -v $SDK_COMMOM_FOLDER/platform/inc/egis_definition.h $TA_RELEASE_FOLDER/inc/

cp -v $SDK_COMMOM_FOLDER/payment/payment.h $TA_RELEASE_FOLDER/inc/
cp -v $SDK_COMMOM_FOLDER/payment/payment_qsee.c $TA_RELEASE_FOLDER/src/

echo ''
if [ "$CI" == "" ]; then
    read -n1 -r -p "Press any key to build TA lib" tmpkey
fi

make clean TOOLCHAIN=$TOOLCHAIN
make debug TOOLCHAIN=$TOOLCHAIN
make release TOOLCHAIN=$TOOLCHAIN
if [ $? -ne 0 ]; then
    echo "!!! Failed to build TA lib"
    exit 11
fi

cd $DIR
mkdir $TA_RELEASE_FOLDER/libs
mkdir $TA_RELEASE_FOLDER/libs/Debug
mkdir $TA_RELEASE_FOLDER/libs/Release

find obj/Debug/libs -name "*.a" -exec cp -v {} $TA_RELEASE_FOLDER/libs/Debug \;
find obj/Debug/libs -name "*.lib" -exec cp -v {} $TA_RELEASE_FOLDER/libs/Debug \;
find obj/Release/libs -name "*.a" -exec cp -v {} $TA_RELEASE_FOLDER/libs/Release \;
find obj/Release/libs -name "*.lib" -exec cp -v {} $TA_RELEASE_FOLDER/libs/Release \;
